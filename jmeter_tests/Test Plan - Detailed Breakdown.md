# What is JMeter?

JMeter is used to perform load/performance testing for an application, attempting to simulate multiple users using the web application all at once. This could involve common processes such as logging into the system, or taking a test.

JMeter tests measure the response time of each HTTP request, which is indicative of how quickly the web application's backend systems process each request.

# The Test Plan

In general, JMeter's test plan is organized in a tree structure. For the most part, it runs each test in sequence.

The root of the tree is the "Test Plan" element, and multiple sub-elements can be placed into the tree.

## View Results Tree

**This element should be disabled before running a non-GUI test - otherwise, it will block execution of the non-GUI test.**

The `View Results Tree` element is a listener that samples the test results of all tests within its scope & all subscopes of it. The scope is defined by where it sits in the tree (so if it sits at the root level, it will listen to all events in the Test Plan).

More info about this element: https://www.blazemeter.com/blog/jmeter-listeners-part-1-basic-display-formats

## Scope in JMeter

In case the topic about scoping isn't clear, this article explains it pretty well: https://www.blazemeter.com/blog/jmeter-scoping-rules-the-ultimate-guide

# HTTP Request Defaults

It is tedious to switch the server name/IP address if we wish to test another environment (or the server has migrated to another IP address, etc.). The `HTTP Request Defaults` element makes this much easier - if the server name/IP is not specified in an `HTTP Request` element, then it will try to find the nearest `HTTP Request Defaults` element (scope-wise) and use that server name/IP address instead.

For this reason, most `HTTP Request` elements do not have a server name/IP address defined.

# Basic Elements for JMeter

The original website does a great job explaining the basic elements for JMeter: https://jmeter.apache.org/usermanual/test_plan.html

Some of the common basic elements used in this test plan will still have a summary of the documentation above & will pertain more to our specific use cases.

# Thread Groups

## Thread Groups (general)

Defines and controls the number of threads your computer will use to run the actions specified in the thread group. Each thread corresponds to 1 user.

Properties of thread groups include (but are not limited to):

- Number of threads (users)
- Ramp-up period (seconds): this is the time JMeter waits before running tests on a new thread (if more than 1 thread is supposed to be run for tests on this thread group)
- Loop count
- "Same user on each iteration" tickbox

## Setup Thread Groups

Setup Thread Groups are thread groups that always execute before all other types of thread groups. They are used to run setup actions (like logging in as a user, capturing variables, etc.) before running other tests in the Test Plan.

## Teardown Thread Groups

After all other test groups are run, teardown thread groups are ran to execute post-test actions of some kind.

Currently, we use the Teardown Thread Group to delete the Test Access Code that was generated as part of the Test Plan.

## CSV Data Set Config

JMeter can read a .csv file and extract the values (separated by the comma delimiter) as thread-scoped variables for each run of the thread by using a `CSV Data Set Config` element. It's important to emphasize that the lifetime of these variables only exist for as long as the thread exists.

There is an option in each `CSV Data Set Config` element to set the Sharing Mode. In general, this should be set to `Current Thread Group` in all Thread Groups (except for the Setup Thread Groups). This ensures that the login credentials are synchronized with the appropriate users from the CSV login credentials file for each thread group.

Note that JMeter will parse through the .csv file and treat each row as "1 user (or 1 thread)".

For a more thorough reference on how to run tests with multiple users, please refer to this link:

- https://www.blazemeter.com/blog/jmeter-parameterization-the-complete-guide

# User Defined Variables

This is a global variable storage element. Note that the scope of these variables will always be global no matter where you put it.

**It is highly recommended that you only have 1 of these config elements in the root level of the tree at all times! Adding more than 1 of these storage elements can cause unexpected bugs in the application!**

# User Elements

This is a scoped variable storage element (not a global variable storage element)! It should be used for thread groups in particular if you want to use variables for each thread group run.

## HTTP Request

The most common basic element used, this sends an HTTP request to the target URL. You can append other attributes to it like parameters required for a GET/POST payload, as well as the payload body (e.g. JSON).

Each HTTP Request should have an `HTTP Header Manager` element in it.

## HTTP Header Manager

This element manages the headers that are sent in the HTTP request.

One critical use for our application is the addition of an `Authorization` header once a JWT (Java Web Token) has been obtained properly. This is pertinent to the next basic element!

## Transaction Controller

This is an organizational element when running tests. They are useful to have when grouping different HTTP requests together, especially if you tick their `Generate parent sample` tickbox, as that causes the test results to be grouped in an expandable list of results when viewing them in a `View Results Tree` element or HTML report.

This element also defines scope, as all elements within the Transaction Controller element only exist/apply within the Transaction Controller's scope.

# Complex JMeter elements

## Variables

In general, you can set/define a new variable in some other element (e.g. a `CSV Data Set Config` element, a `JSON Extractor` element, or a `Regular Expression Extractor` element). You can then access the variable in other elements by typing in:

```
${variable_name}
```

## Regular Expression Extractor

There is also a ton of documentation on the Regular Expression Extractor: https://guide.blazemeter.com/hc/en-us/articles/207421325-Using-RegEx-Regular-Expression-Extractor-with-JMeter-Using-RegEx-(Regular-Expression-Extractor)-with-JMeter

The Regular Expression Extractor is another powerful tool that can be used to extract specific information from an HTTP Response body (e.g. a simple JSON body).

In our application, we use this to extract the `access` and `refresh` JWTs to use in the `Authorization` header of an `HTTP Header Manager` element in future requests, as well as being used in the JSON request bodies of the `verify_token` and `refresh_token` when sending API requests to those endpoints.

For more complex JSON responses, it is recommended to use a JSON Extractor instead.

## JSON Extractor

The JSON Extractor uses JSONPath Expressions to match desired data in the JSON tree.

In particular, we typically use expressions like `$[-1:].key` to get the last element of the unnamed array received in the JSON response and search for a desired key. This can cascade as deep as we need it to, but keeping the expressions simple is best for flexibility later.

References:

- JSON Extractor detailed example usage: https://blog.knoldus.com/jmeter-extract-data-using-postprocessor-part-i/
- JSONPath online evaluator (for testing JSONPath expression validation): https://jsonpath.com/
- https://goessner.net/articles/JsonPath/index.html#e2 (for technical info regarding JSONPath)

## Beanshell Post Processor

There is an abundance of documentation on the Beanshell Post Processor: https://www.blazemeter.com/blog/how-use-beanshell-jmeters-favorite-built-component

For the Test Plans we are running now, the Beanshell Post Processor executes some Java code snippets after running a test. Right now, we are using it to set global properties in JMeter ("global variables" in JMeter that are defined after runtime - these are different from the global variables in the `User Defined Variables` element).

In the future, Beanshell Post Processors can execute powerful logic and operations after certain operations are finished in a thread group, transaction controller, etc.

# JMeter CLI

To run a test in CLI/non-GUI mode (the proper way to do performance tests), run `jmeter -n -Jjmeterengine.force.system.exit=true -t "cat_test_plan.jmx" -l "cat_test_outputs.jtl"`

- **Make sure you disable "View Results Tree" before running a performance test via CLI! Otherwise it won't work**

- Run the load test and then generate an HTML report after:
  `jmeter -n -Jjmeterengine.force.system.exit=true -t "cat_test_plan.jmx" -l "cat_test_outputs.jtl" -e -o "html_report"`

- If you want to generate an HTML report from a pre-existing .jtl file: `jmeter -g <log file> -o <Path to output folder>`

References:

- Headless (CLI/non-GUI) mode commands: https://stackoverflow.com/a/44918375
- Exit JMeter properly after running in CLI mode: https://stackoverflow.com/questions/52987152/the-jvm-should-have-exited-but-did-not
