# Settings/Variables to change when testing other environments

In the `GLOBAL_CONFIG` User Defined Variables element:

- `host` field: change the host URL, make sure to include the `/oec-cat/`suffix
- `scheme` field: change to `https` or `http` as appropriate
- `USER_THREAD_COUNT` field: change to a desired number (so long as the count is less than or equal to the number of test accounts available in `./credentials/cat_test_creds.csv`). Default value is 1.
- `RAMP_UP_PERIOD_SECONDS` field: this controls the stagger duration between the start times of each thread group (in seconds). Set this to a low value to simulate testing with no/low stagger times. Default value is 0.1.

In the `HTTP Request Defaults` element:

- `protocol [http]` field: change to `https` or `http` as appropriate
- `Server Name or IP` field: if inputting a server name here, only include the base URL (i.e. there should be no `/` characters in the URL). Doing so will cause JMeter to throw a `Malformed URL` Exception.

# Creating new accounts when testing other environments

- In the `setUp Thread Group (Users)` and `setUp Thread Group (Admin)` thread groups, enable the `Create Account (if it does not exist)` Transaction Controller elements.

- In the `GLOBAL_CONFIG` User Defined Variables element, change `USER_THREAD_COUNT` to whatever number you want.

  - From this document's current directory, go to `./credentials/cat_test_creds.csv` and ensure that the number of accounts in there is equal to or less than the value of `USER_THREAD_COUNT` that you just set.
  - If it is not, then go to `./credentials/cat_test_creds_generator.xlsx` and follow the instructions in the file to generate more accounts until the number exceeds the new value of `USER_THREAD_COUNT`. Copy those values to `./credentials/cat_test_creds.csv`.

- Disable all other Thread Groups if possible and then run the Test Plan. You should see each HTTP Request for `Create Account` be successful, which means that all of those user accounts should be successfully created.

## Cleanup steps after mass-creating new accounts

After all of that is complete, in the `setUp Thread Group (Users)` and `setUp Thread Group (Admin)` thread groups, disable the `Create Account (if it does not exist)` Transaction Controller elements. This ensures they will not run in a real test run (as the accounts have been created already).

# Test Administrator account - further setup

## Setting permissions for the new Admin account

There should be 1 Administrator account that is created from the `setUp Thread Group (Admin)` thread group. Go to the CAT domain you will be testing on and log into that account regularly through the login page. Afterwards, navigate to the Profile page by clicking on `Menu` -> `My Profile`. Afterwards, request Test Administrator permissions by going to the Permissions page and filling out the appropriate fields.

![request_permissions_page](img/request_permissions_profile_page.png)

![test_admin_request_dialog](img/test_admin_request_dialog.png)

Speak with your supervisor and get them to approve the permission request from this account.

## Grant Test Administrator Test Accesses

Speak with your supervisor and get them to grant the Test Administrator access to at least one of the Test Accesses.

---

# Future Tasks for JMeter

By default, JMeter's Recorder does not work with Websockets. Because of this issue, we are unable to properly record the actual E639 test on the Test server.

## Steps to take to finish the JMeter Test Plan

- Re-record the actual E639 test in the Test environment
- To do this, look into using JMeter Websocket plugin (3rd party plugin)
  - Installation/usage reference: https://www.blazemeter.com/blog/jmeter-websocket-samplers-a-practical-guide
  - Source code/repo: https://bitbucket.org/pjtr/jmeter-websocket-samplers/src
- Once this has been implemented, record the HTTP Requests with JMeter's Recorder when taking the actual E639 test

## Refactor HTTP Request elements

After the recording has been done, refactor the HTTP Requests appropriately. General steps to take:

- For each `HTTP Header Manager` element in each `HTTP Request` element:
  - If the API endpoint is the `verify-token` endpoint, its Body Data should be `{"token":"${refresh}"}`
  - If the API endpoint is the `refresh-token` endpoint, its Body Data should be `{"refresh":"${refresh}"}`
  - Otherwise, add in a row with `Authorization` as the key and `JWT ${access}` as the value

For HTTP requests that have dynamic values already defined from previous HTTP requests/thread groups (e.g. `assigned_test_id`, `test_id`), change the hardcoded value to the corresponding variable (e.g. `${assigned_test_id}`, `${test_id}`).

- Make sure to look at the URL params too (not just the parameters table) - it is a bug that some of CAT's POST API Endpoints only accept parameters in the URL and not in the POST body. For some reason, defining Parameters in the parameters table in JMeter for a POST request will place those parameters in the request's body, not the URL.

If other values can be dynamically determined and this pattern is easy to detect & consistent for other tests, then make those dynamic with the use of a helper element of some kind (e.g. a JSON Extractor and assign those values to a variable).

- Potential variables to make dynamic: `question_id`, `test_section_id`
