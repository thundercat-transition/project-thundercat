# JMeter Setup

- Download the latest binaries (.zip file for Windows) from the official website: https://jmeter.apache.org/download_jmeter.cgi
- Extract the JMeter files to a convenient location
- Stable version used for testing: 5.3
- Start JMeter by running either `ApacheJMeter.jar` or `jmeter.bat` in the JMeter folder location.

# Open the Test Plan file

Press `Ctrl+O` and navigate to the JMeter test plan file (with a `.jmx` extension). For this project, the relative file path with respect to the project root is `./jmeter_tests/cat_test_plan.jmx` - you will have to use the absolute path when navigating to the actual file.

# Setup to import .csv files for login credentials

For security reasons, all login credentials are imported into the JMeter test plan. The appropriate .csv files should go into the `./credentials` folder.

To ensure that the import path is correct, open JMeter and then open `./jmeter_tests/cat_test_plan.jmx`. Look for the `CSV Data Set Config` entry in each threadGroup and Transaction Controller, and make sure the path is set to the `./credentials` with the correct absolute path on your computer.

**It is important that you look at each Thread Group and change all of the file paths for CSV credentials to the correct file paths. Otherwise, your tests will fail!**

# Starting the Performance Tests (GUI-mode)

To start a Performance Test after getting the above set up, press `Ctrl+R` or click on the green "Play" button in the top bar of the UI.

Doing this via the JMeter GUI runs the Performance Tests in GUI-mode. This method of running tests is best for regular usage, but running tests in non-GUI mode is best for accurate results for a serious test.

![play_button](img/play_button.png)

## Proxy settings in Firefox to Enable/Disable Recording for Network Traffic

- In your Firefox web browser, navigate to Options -> "Privacy & Security" -> "Network Settings" (or just go to Options and search for "proxy"): ![firefox_settings_1](img/firefox_network_settings_1.png)
- For the proxy settings, follow the same configuration as outlined in the following screenshot: ![firefox_settings_2](img/firefox_network_settings_2.png)
- Then, run JMeter (either `ApacheJMeter.jar` or `jmeter.bat`).
- Load `./cat_test_plan.jmx` and then go to the Recording Controller `HTTP(S) Test Script Recorder`.
- Press the `Start` button in the middle of the page (not the one in the top bar). JMeter should generate a temporary certificate for you that you have to import.
- To import the certificate, In your Firefox web browser, navigate to Options -> "Privacy & Security" -> "Certificates" (near the bottom of the page) and then Select "View Certificates": ![firefox_settings_3](img/firefox_network_settings_3.png)
- In the current window, click on `Import...` and then navigate to your JMeter directory's bin path (should look something like `apache-jmeter-5.3\bin`). Select the `.crt` certificate file found in this foler.
- in JMeter, ensure that the `Recording Thread Group` and `HTTP(S) Test Script Recorder` elements are enabled (toggle their enabled status with `Ctrl+T`, or right-click on them and click `Enable`)

You should be able to start recording now without any issue!

- Note that doing these steps will prevent your Firefox web browser from working properly without turning on JMeter's recorder.
- To disable this & return Firefox to normal functionality, simply use another proxy option in the Proxy Settings window (we suggest selecting `Use system proxy settings` as that is the default proxy option).

References

- PSC's QA Wiki on JMeter: https://intranet5.psc-cfp.gc.ca/wiki/index.php/JMeter_Tutorial

# Recording HTTP traffic

Ensure that the `Recording Thread Group` and `HTTP(S) Test Script Recorder` elements are enabled, and that your Firefox proxy settings are set properly from the earlier setup instructions.

- Click on the `HTTP(S) Test Script Recorder` element and then click on the green "Play" button: ![test_script_recorder](img/test_script_recorder.png)
- Press "OK" in the dialog that pops up and then you should see a Recorder dialog window overlay on your computer: ![recorder_dialog](img/recorder_dialog.png)
- Open Firefox and then navigate to the website of interest - JMeter will record each HTTP Request in Transaction Controllers with empty names as you browse through your website of interest.
- Rename each Transaction Controller at appropriate times to make sure that you know which Transaction Controller corresponds to which general action you took for those corresponding sets of HTTP Requests.
- Once you are finished, simply press the "Stop" button to stop recording.
- Make sure you disable the `Recording Thread Group` and `HTTP(S) Test Script Recorder` elements once you are finished recording.

# Running the Performance Test in non-GUI Mode using test.sh

Running the Performance Test in non-GUI mode is most appropriate for a serious test, as it generates more accurate results as JMeter is not using more memory and processing power than required.

- **NB: Make sure you disable "View Results Tree" in the Test Plan before running a performance test via CLI! Otherwise it won't work**

Simply open a Linux terminal in this directory and run `./test.sh` to run the JMeter test and generate an HTML Report after the JMeter test is run. The HTML report will be in the `./html_report/index.html` file.

- If using a Windows terminal, run `sh test.sh` instead.

**NOTE**: If you get an error that it cannot find a cmdlet named "jmeter", then you need to add it to your system's Environment Path.

- To do this, set JMeter's `bin` folder to your system's `Path` list as shown here: ![set_jmeter_env_variable](img/set_jmeter_env_variable.png)

- Re-enable the `View Results Tree` element when you are ready to run the JMeter tests in GUI mode again.

# Recording traffic on localhost

In Firefox, type in `about:config` in the address bar, then search for `network.proxy.allow_hijacking_localhost` and set that to true.
