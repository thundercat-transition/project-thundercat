# NODE_MODULES Rebuild

## Destroy node_modules folder and related files/folders

1.  Execute "docker compose down"
2.  Execute "docker system prune"
3.  Quit Docker Desktop
4.  Close VS Code
5.  Go to "...\thundercat\project-thundercat\frontend" and delete the following folders/files:
    - .yarn (folder)
    - node_modules (folder)
    - .yarnrc.yml (file)
    - yarn.lock (file)<br>

## Rebuild node_modules and start back the application

1.  Go to "...\thundercat\project-thundercat\frontend>" and create a brand new "yarn.lock" file
2.  Go to "...\thundercat\project-thundercat\frontend>" and execute "yarn"
    - if you are getting the "self-signed certificate in certificate chain" error, simply disconnect from the VPN or execute "yarn config set "strict-ssl" false -g" before running the yarn command
3.  Go to "...\thundercat\project-thundercat\frontend>" and execute "npm install --force"
4.  Open Docker Desktop
5.  Execute "docker compose up --build"
    - if you are getting the "<b>DeprecationWarning: 'onAfterSetupMiddleware' option is deprecated. Please use the 'setupMiddlewares' option.</b>" error, simply re-execute the following commands:
      - yarn (in "...\thundercat\project-thundercat\frontend>" folder)
      - npm install --force (in "...\thundercat\project-thundercat\frontend>" folder)
      - docker compose down
      - docker system prune
      - docker compose up --build
