#!/bin/bash
# Script to trigger sonatype on the server
# $1 = user token
# $2 = user password
# Constants to add colour to text
BLUE='\033[0;34m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
printf "**** ${BLUE}Begin backend nexus tests${NC} ***\n"
C:/_dev/nexus-iq-cli/nexus-iq-cli.exe -a $1:$2 -i cat_backend --stage $3 -s https://lifecycle.psc-cfp.gc.ca:8443 backend/requirements.txt
printf "**** ${GREEN}Complete backend nexus tests${NC} ***\n"

printf "**** ${BLUE}Begin frontend nexus tests${NC} ***\n"
C:/_dev/nexus-iq-cli/nexus-iq-cli.exe -a $1:$2 -i cat_frontend --stage $3 -s https://lifecycle.psc-cfp.gc.ca:8443 frontend/yarn.lock
printf "**** ${GREEN}Complete frontend nexus tests${NC} ***\n"