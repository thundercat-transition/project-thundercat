USE [master]

GO

-- CONTENT TYPE
SET IDENTITY_INSERT [dbo].[django_content_type] ON
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (1, N'auth', N'permission')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2, N'auth', N'group')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (3, N'contenttypes', N'contenttype')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (4, N'user_management_models', N'user')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (5, N'user_management_models', N'custompermissions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (6, N'user_management_models', N'customuserpermissions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (7, N'admin', N'logentry')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (8, N'authtoken', N'token')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (9, N'sessions', N'session')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (10, N'cms_models', N'addressbookcontact')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (11, N'cms_models', N'addressbookcontactdetails')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (12, N'cms_models', N'answer')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (13, N'cms_models', N'answerdetails')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (14, N'cms_models', N'emailquestion')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (15, N'cms_models', N'emailquestiondetails')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (16, N'cms_models', N'fileresourcedetails')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (17, N'cms_models', N'multiplechoicequestion')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (18, N'cms_models', N'newquestion')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (19, N'cms_models', N'nextsectionbuttontypepopup')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (20, N'cms_models', N'nextsectionbuttontypeproceed')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (21, N'cms_models', N'pagesection')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (22, N'cms_models', N'pagesectiontypeimagezoom')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (23, N'cms_models', N'pagesectiontypemarkdown')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (24, N'cms_models', N'pagesectiontypesampleemail')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (25, N'cms_models', N'pagesectiontypesampleemailresponse')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (26, N'cms_models', N'pagesectiontypesampletaskresponse')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (27, N'cms_models', N'pagesectiontypetreedescription')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (28, N'cms_models', N'questionblocktype')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (29, N'cms_models', N'questionlistrule')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (30, N'cms_models', N'questionsection')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (31, N'cms_models', N'questionsectiontypemarkdown')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (32, N'cms_models', N'reducercontrol')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (33, N'cms_models', N'sectioncomponentpage')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (34, N'cms_models', N'testdefinition')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (35, N'cms_models', N'testsection')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (36, N'cms_models', N'testsectionreducer')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (37, N'cms_models', N'testsectioncomponent')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (38, N'cms_models', N'testpermissions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (39, N'cms_models', N'competencytype')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (40, N'cms_models', N'questionsituation')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (41, N'cms_models', N'situationexamplerating')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (42, N'cms_models', N'situationexampleratingdetails')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (43, N'cms_models', N'questionsectiontypemarkdownend')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (44, N'cms_models', N'histestpermissions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (45, N'custom_models', N'assignedtest')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (46, N'custom_models', N'assignedtestsection')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (47, N'custom_models', N'candidateanswers')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (48, N'custom_models', N'channelsroom')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (49, N'custom_models', N'databasecheckmodel')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (50, N'custom_models', N'language')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (51, N'custom_models', N'taactiontypes')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (52, N'custom_models', N'notepad')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (53, N'custom_models', N'testscorerassignment')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (54, N'custom_models', N'testaccesscode')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (55, N'custom_models', N'taactions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (56, N'custom_models', N'pausetestactions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (57, N'custom_models', N'locktestactions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (58, N'custom_models', N'channelspresence')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (59, N'custom_models', N'candidatemultiplechoiceanswers')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (60, N'custom_models', N'assignedtestsectionaccesstimes')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (61, N'custom_models', N'assignedtestanswerscore')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (62, N'custom_models', N'candidatetaskresponseanswers')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (63, N'custom_models', N'candidateemailresponseanswers')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (64, N'user_management_models', N'permissionrequest')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (65, N'user_management_models', N'accommodations')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (66, N'user_management_models', N'userextendedprofile')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (67, N'user_management_models', N'hiscustomuserpermissions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (68, N'ref_table_views', N'cateducation')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (69, N'ref_table_views', N'catminority')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (70, N'ref_table_views', N'catemployerstsvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (71, N'ref_table_views', N'oltforganisationvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (72, N'ref_table_views', N'oltfclassificationvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (73, N'ref_table_views', N'oltfgendervw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (74, N'ref_table_views', N'oltfprovincevw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (75, N'user_management_models', N'userextendedprofileminority')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (76, N'authtoken', N'tokenproxy')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (77, N'django_rest_passwordreset', N'resetpasswordtoken')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (78, N'user_management_models', N'userpasswordresettracking')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (79, N'user_management_models', N'historicaluserpasswordresettracking')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (80, N'user_management_models', N'historicaluserextendedprofileminority')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (81, N'user_management_models', N'historicaluserextendedprofile')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (82, N'user_management_models', N'historicaluser')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (83, N'user_management_models', N'historicalpermissionrequest')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (84, N'user_management_models', N'historicalhiscustomuserpermissions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (85, N'user_management_models', N'historicalcustomuserpermissions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (86, N'user_management_models', N'historicalcustompermissions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (87, N'user_management_models', N'historicalaccommodations')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (88, N'cms_models', N'scoringmethods')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (89, N'cms_models', N'scoringmethodtypes')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (90, N'cms_models', N'scoringthreshold')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (91, N'cms_models', N'scoringpassfail')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (92, N'cms_models', N'historicaltestsectionreducer')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (93, N'cms_models', N'historicaltestsectioncomponent')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (94, N'cms_models', N'historicaltestsection')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (95, N'cms_models', N'historicaltestpermissions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (96, N'cms_models', N'historicaltestdefinition')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (97, N'cms_models', N'historicalsituationexampleratingdetails')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (98, N'cms_models', N'historicalsituationexamplerating')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (99, N'cms_models', N'historicalsectioncomponentpage')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (100, N'cms_models', N'historicalreducercontrol')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (101, N'cms_models', N'historicalquestionsituation')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (102, N'cms_models', N'historicalquestionsectiontypemarkdownend')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (103, N'cms_models', N'historicalquestionsectiontypemarkdown')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (104, N'cms_models', N'historicalquestionsection')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (105, N'cms_models', N'historicalquestionlistrule')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (106, N'cms_models', N'historicalquestionblocktype')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (107, N'cms_models', N'historicalpagesectiontypetreedescription')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (108, N'cms_models', N'historicalpagesectiontypesampletaskresponse')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (109, N'cms_models', N'historicalpagesectiontypesampleemailresponse')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (110, N'cms_models', N'historicalpagesectiontypesampleemail')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (111, N'cms_models', N'historicalpagesectiontypemarkdown')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (112, N'cms_models', N'historicalpagesectiontypeimagezoom')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (113, N'cms_models', N'historicalpagesection')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (114, N'cms_models', N'historicalnextsectionbuttontypeproceed')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (115, N'cms_models', N'historicalnextsectionbuttontypepopup')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (116, N'cms_models', N'historicalnewquestion')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (117, N'cms_models', N'historicalmultiplechoicequestion')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (118, N'cms_models', N'historicalfileresourcedetails')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (119, N'cms_models', N'historicalemailquestiondetails')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (120, N'cms_models', N'historicalemailquestion')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (121, N'cms_models', N'historicalcompetencytype')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (122, N'cms_models', N'historicalanswerdetails')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (123, N'cms_models', N'historicalanswer')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (124, N'cms_models', N'historicaladdressbookcontactdetails')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (125, N'cms_models', N'historicaladdressbookcontact')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (126, N'cms_models', N'historicalscoringthreshold')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (127, N'cms_models', N'historicalscoringpassfail')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (128, N'cms_models', N'historicalscoringmethodtypes')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (129, N'cms_models', N'historicalscoringmethods')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (130, N'custom_models', N'historicaltestscorerassignment')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (131, N'custom_models', N'historicaltestaccesscode')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (132, N'custom_models', N'historicaltaactiontypes')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (133, N'custom_models', N'historicaltaactions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (134, N'custom_models', N'historicalpausetestactions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (135, N'custom_models', N'historicalnotepad')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (136, N'custom_models', N'historicallocktestactions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (137, N'custom_models', N'historicallanguage')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (138, N'custom_models', N'historicalchannelsroom')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (139, N'custom_models', N'historicalchannelspresence')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (140, N'custom_models', N'historicalcandidatetaskresponseanswers')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (141, N'custom_models', N'historicalcandidatemultiplechoiceanswers')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (142, N'custom_models', N'historicalcandidateemailresponseanswers')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (143, N'custom_models', N'historicalcandidateanswers')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (144, N'custom_models', N'historicalassignedtestsectionaccesstimes')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (145, N'custom_models', N'historicalassignedtestsection')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (146, N'custom_models', N'historicalassignedtestanswerscore')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (147, N'custom_models', N'historicalassignedtest')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (148, N'ref_table_views', N'historicaloltfprovincevw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (149, N'ref_table_views', N'historicaloltforganisationvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (150, N'ref_table_views', N'historicaloltfgendervw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (151, N'ref_table_views', N'historicaloltfclassificationvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (152, N'ref_table_views', N'historicalcatminority')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (153, N'ref_table_views', N'historicalcatemployerstsvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (154, N'ref_table_views', N'historicalcateducation')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (155, N'custom_models', N'historicalettaactiontypes')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (156, N'custom_models', N'historicalettaactions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (157, N'custom_models', N'ettaactiontypes')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (158, N'custom_models', N'ettaactions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (159, N'custom_models', N'historicalassignedquestionslist')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (160, N'custom_models', N'assignedquestionslist')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (161, N'custom_models', N'unsupervisedtestaccesscode')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (162, N'custom_models', N'historicalunsupervisedtestaccesscode')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (163, N'custom_models', N'uitinvites')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (164, N'custom_models', N'historicaluitinvites')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (165, N'custom_models', N'uitinviterelatedcandidates')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (166, N'custom_models', N'historicaluitinviterelatedcandidates')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (1165, N'custom_models', N'uitreasonsfordeletion')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (1166, N'custom_models', N'historicaluitreasonsfordeletion')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2165, N'custom_models', N'historicaluitreasonsformodification')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2166, N'custom_models', N'uitreasonsformodification')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2167, N'cms_models', N'historicalitembank')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2168, N'cms_models', N'itembank')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2169, N'cms_models', N'itembankattributes')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2170, N'cms_models', N'itembankattributevalues')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2171, N'cms_models', N'historicalitembankattributevalues')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2172, N'cms_models', N'historicalitembankattributes')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2173, N'cms_models', N'viewedquestions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2174, N'cms_models', N'historicalviewedquestions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2175, N'custom_models', N'historicalassignedanswerchoices')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2176, N'custom_models', N'assignedanswerchoices')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2177, N'user_management_models', N'historicaltaextendedprofile')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2178, N'user_management_models', N'taextendedprofile')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2179, N'cms_models', N'orderlesstestpermissions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2180, N'cms_models', N'historicalorderlesstestpermissions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2181, N'custom_models', N'reasonsfortesting')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2182, N'custom_models', N'historicalreasonsfortesting')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2183, N'custom_models', N'orderlessfinancialdata')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2184, N'custom_models', N'historicalorderlessfinancialdata')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2185, N'ref_table_views', N'cataboriginalvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2186, N'ref_table_views', N'catvisibleminorityvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2187, N'ref_table_views', N'catdisabilityvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2188, N'ref_table_views', N'historicalcatrefvisibleminorityvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2189, N'ref_table_views', N'historicalcatrefdisabilityvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2190, N'ref_table_views', N'historicalcatrefaboriginalvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2191, N'user_management_models', N'userextendedprofilevisibleminority')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2192, N'user_management_models', N'userextendedprofiledisability')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2193, N'user_management_models', N'userextendedprofileaboriginal')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2194, N'user_management_models', N'historicaluserextendedprofilevisibleminority')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2195, N'user_management_models', N'historicaluserextendedprofiledisability')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2196, N'user_management_models', N'historicaluserextendedprofileaboriginal')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2197, N'user_management_models', N'eeinfooptions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2198, N'user_management_models', N'historicaleeinfooptions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2199, N'custom_models', N'systemalert')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2200, N'custom_models', N'historicalsystemalert')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2201, N'custom_models', N'criticality')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2202, N'custom_models', N'historicalcriticality')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2203, N'ref_table_views', N'oltfdepartementsvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2204, N'ref_table_views', N'historicaloltfdepartementsvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2205, N'custom_models', N'historicaladditionaltime')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2206, N'custom_models', N'breakbankactions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2207, N'custom_models', N'breakbank')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2208, N'custom_models', N'historicalbreakbankactions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2209, N'custom_models', N'additionaltime')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2210, N'custom_models', N'historicalaccommodationrequest')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2211, N'custom_models', N'accommodationrequest')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2212, N'custom_models', N'historicalbreakbank')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2213, N'cms_models', N'historicalitembankaccesstypes')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2214, N'cms_models', N'historicalitembankpermissions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2215, N'cms_models', N'itembankaccesstypes')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2216, N'cms_models', N'itembankpermissions')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2217, N'cms_models', N'itemactivestatuses')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2218, N'cms_models', N'itemdevelopmentstatuses')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2219, N'cms_models', N'itemresponseformats')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2220, N'cms_models', N'items')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2221, N'cms_models', N'itemhistoricalids')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2222, N'cms_models', N'itemcomments')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2223, N'cms_models', N'historicalitems')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2224, N'cms_models', N'historicalitemresponseformats')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2225, N'cms_models', N'historicalitemhistoricalids')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2226, N'cms_models', N'historicalitemdevelopmentstatuses')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2227, N'cms_models', N'historicalitemcomments')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2228, N'cms_models', N'historicalitemactivestatuses')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2229, N'cms_models', N'itemdrafts')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2230, N'cms_models', N'historicalitemdrafts')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2231, N'db_view_models', N'financialreportvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2232, N'db_view_models', N'testresultreportvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2233, N'db_view_models', N'activetestsvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2234, N'db_view_models', N'userlookupvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2235, N'db_view_models', N'botestaccesscodesvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2236, N'db_view_models', N'testorderandreferencenumbersvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2237, N'db_view_models', N'testdatavw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2238, N'db_view_models', N'allitemsdatavw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2239, N'db_view_models', N'itemlatestversionsdatavw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2240, N'db_view_models', N'itemdraftsvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2241, N'cms_models', N'itemcontent')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2242, N'cms_models', N'itemcontentdrafts')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2243, N'cms_models', N'itemoption')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2244, N'cms_models', N'itemoptiondrafts')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2245, N'cms_models', N'itemoptiontextdrafts')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2246, N'cms_models', N'itemoptiontext')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2247, N'cms_models', N'itemcontenttextdrafts')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2248, N'cms_models', N'itemcontenttext')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2249, N'cms_models', N'historicalitemoptiontextdrafts')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2250, N'cms_models', N'historicalitemoptiontext')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2251, N'cms_models', N'historicalitemoptiondrafts')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2252, N'cms_models', N'historicalitemoption')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2253, N'cms_models', N'historicalitemcontenttextdrafts')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2254, N'cms_models', N'historicalitemcontenttext')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2255, N'cms_models', N'historicalitemcontentdrafts')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2256, N'cms_models', N'historicalitemcontent')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2257, N'custom_models', N'languagetext')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2258, N'custom_models', N'historicallanguagetext')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2259, N'db_view_models', N'itemcontentvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2260, N'db_view_models', N'itemoptionvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2261, N'db_view_models', N'itemcontentdraftsvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2262, N'db_view_models', N'itemoptiondraftsvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2263, N'cms_models', N'itembankattributestext')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2264, N'cms_models', N'historicalitembankattributestext')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2265, N'cms_models', N'itembankattributevaluestext')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2266, N'cms_models', N'itemattributevalue')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2267, N'cms_models', N'historicalitembankattributevaluestext')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2268, N'cms_models', N'historicalitemattributevalue')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2269, N'cms_models', N'itemattributevaluedrafts')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2270, N'cms_models', N'historicalitemattributevaluedrafts')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2271, N'db_view_models', N'itembankattributevaluesvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2272, N'cms_models', N'bundles')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2273, N'cms_models', N'historicalbundles')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2274, N'cms_models', N'historicalbundleitemsassociation')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2275, N'cms_models', N'historicalbundlebundlesassociation')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2276, N'cms_models', N'bundleitemsassociation')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2277, N'cms_models', N'bundlebundlesassociation')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2278, N'db_view_models', N'bundleassociationvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2279, N'ref_table_views', N'catrefclassificationvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2280, N'ref_table_views', N'catrefdepartmentsvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2281, N'ref_table_views', N'catrefgendervw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2282, N'ref_table_views', N'catrefprovincevw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2283, N'ref_table_views', N'historicalcatrefprovincevw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2284, N'ref_table_views', N'historicalcatrefgendervw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2285, N'ref_table_views', N'historicalcatrefdepartmentsvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2286, N'ref_table_views', N'historicalcatrefclassificationvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2287, N'cms_models', N'bundlebundlesrule')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2288, N'cms_models', N'historicalbundlerule')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2289, N'cms_models', N'bundleruletype')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2290, N'cms_models', N'historicalbundleruletype')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2291, N'cms_models', N'historicalbundlebundlesruleassociations')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2292, N'cms_models', N'historicalbundleitemsbasicrule')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2293, N'cms_models', N'historicalbundlebundlesrule')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2294, N'cms_models', N'bundlerule')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2295, N'cms_models', N'bundleitemsbasicrule')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2296, N'cms_models', N'bundlebundlesruleassociations')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2297, N'ref_table_views', N'catrefaboriginalvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2298, N'ref_table_views', N'catrefdisabilityvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2299, N'ref_table_views', N'catrefemployerstsvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2300, N'ref_table_views', N'catrefvisibleminorityvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2301, N'db_view_models', N'bundledatavw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2302, N'db_view_models', N'bundlebundlesruledatavw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2303, N'ref_table_views', N'historicalcatrefemployerstsvw')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2304, N'cms_models', N'itembankrule')
INSERT [dbo].[django_content_type]
    ([id], [app_label], [model])
VALUES
    (2305, N'cms_models', N'historicalitembankrule')
SET IDENTITY_INSERT [dbo].[django_content_type] OFF

GO

-- CUSTOM PERMISSIONS
SET IDENTITY_INSERT [dbo].[user_management_models_custompermissions] ON
INSERT [dbo].[user_management_models_custompermissions]
    ([permission_id], [en_name], [en_description], [fr_description], [codename], [date_assigned], [expiry_date], [content_type_id], [fr_name])
VALUES
    (1, N'Test Administrator', N'A public servant responsible for the administration of standardized PSC tests to candidates. Help candidates resolve technological issues during the test session, if needed. Produce reports relevant to the staffing process.', N'Un fonctionnaire responsable de l’administration des tests standardisés de la CFP aux candidats. Aide les candidats à résoudre des enjeux technologiques au cours de la session de test, au besoin. Produit des rapports pertinents pour le processus de dotation.', N'is_test_administrator', CAST(N'2020-11-23T15:21:51.5186650' AS DateTime2), NULL, 2, N'Administrateur de tests')
INSERT [dbo].[user_management_models_custompermissions]
    ([permission_id], [en_name], [en_description], [fr_description], [codename], [date_assigned], [expiry_date], [content_type_id], [fr_name])
VALUES
    (2, N'Operations Level 1', N'Responsible for managing users’ accounts. May produce reports related to business operations.', N'Responsable de la gestion des comptes des utilisateurs. Peut générer des rapports en lien avec les activités d’affaires.', N'is_etta', CAST(N'2020-11-23T15:21:51.5639590' AS DateTime2), NULL, 2, N'Opérations niveau 1')
INSERT [dbo].[user_management_models_custompermissions]
    ([permission_id], [en_name], [en_description], [fr_description], [codename], [date_assigned], [expiry_date], [content_type_id], [fr_name])
VALUES
    (3, N'PPC R&D Level 1', N'Responsible for data extraction or perform statistical analysis and generate various reports. Create and upload test content.', N'Responsable de l''extraction des données pour effectuer des analyses statistiques et générer divers rapports. Crée et télécharge du contenu de test.', N'is_ppc', CAST(N'2020-11-23T15:21:51.5677920' AS DateTime2), NULL, 2, N'CPP R&D niveau 1')
INSERT [dbo].[user_management_models_custompermissions]
    ([permission_id], [en_name], [en_description], [fr_description], [codename], [date_assigned], [expiry_date], [content_type_id], [fr_name])
VALUES
    (4, N'Test Scorer', N'Test scorers are responsible for scoring some of the tests taken in the Candidate Assessment Tool.', N'Les correcteurs de tests sont responsables de corriger certains tests effectués dans l''Outil d''évaluation des candidats.', N'is_scorer', CAST(N'2020-11-23T15:21:53.2422270' AS DateTime2), NULL, 2, N'Correcteur de test')
INSERT [dbo].[user_management_models_custompermissions]
    ([permission_id], [en_name], [en_description], [fr_description], [codename], [date_assigned], [expiry_date], [content_type_id], [fr_name])
VALUES
    (5, N'Test Builder', N'Responsible for creating and uploading test content.', N'Responsable de la création et du téléversement du contenu de test.', N'is_tb', CAST(N'2022-01-19T15:01:17.2932630' AS DateTime2), NULL, 2, N'Constructeur de tests')
INSERT [dbo].[user_management_models_custompermissions]
    ([permission_id], [en_name], [en_description], [fr_description], [codename], [date_assigned], [expiry_date], [content_type_id], [fr_name])
VALUES
    (6, N'Test Adaptations', N'Responsible for managing accommodations for tests, including adjusting test parameters following the analysis of accommodation requests.', N'Responsable de la gestion des mesures d’adaptation pour les tests, y compris l’ajustement des paramètres de test suite à l’analyse des demandes de mesures d’adaptation.', N'is_aae', CAST(N'2022-09-29T13:28:12.1044980' AS DateTime2), NULL, 2, N'Adaptation des tests')
INSERT [dbo].[user_management_models_custompermissions]
    ([permission_id], [en_name], [en_description], [fr_description], [codename], [date_assigned], [expiry_date], [content_type_id], [fr_name])
VALUES
    (7, N'R&D Operations', N'Responsible for managing permissions and accesses related to test content and test development.', N'Responsable de la gestion des permissions et des accès liés au contenu de test et au développement de tests.', N'is_rd_operations', CAST(N'2022-11-24T13:18:05.3018420' AS DateTime2), NULL, 2, N'Activités R&D')
INSERT [dbo].[user_management_models_custompermissions]
    ([permission_id], [en_name], [en_description], [fr_description], [codename], [date_assigned], [expiry_date], [content_type_id], [fr_name])
VALUES
    (8, N'Test Developer', N'Responsible for managing test content, test assembly, data extractions, and statistical analyses.', N'Responsable de la gestion du contenu de test, des règles d’assemblage des tests, des extractions de données et des analyses statistiques.', N'is_td', CAST(N'2022-11-24T14:01:36.7239280' AS DateTime2), NULL, 2, N'Développeur de test')
INSERT [dbo].[user_management_models_custompermissions]
    ([permission_id], [en_name], [en_description], [fr_description], [codename], [date_assigned], [expiry_date], [content_type_id], [fr_name])
VALUES
    (9, N'HR Coordinator', N'A Public Servant responsible for coordinating supervised tests.', N'Un fonctionnaire responsable de la coordination des tests supervisés.', N'is_hr_coordinator', CAST(N'2023-11-10T13:22:38.8852920' AS DateTime2), NULL, 2, N'Coordonnateur RH')
INSERT [dbo].[user_management_models_custompermissions]
    ([permission_id], [en_name], [en_description], [fr_description], [codename], [date_assigned], [expiry_date], [content_type_id], [fr_name])
VALUES
    (10, N'Test Centre Coordinator', N'A public servant responsible for the management of a test centre.', N'Un fonctionnaire responsable de la gestion de centre de tests.', N'is_tcm', CAST(N'2023-11-10T13:22:38.8957300' AS DateTime2), NULL, 2, N'Coordonnateur de centre de tests')
INSERT [dbo].[user_management_models_custompermissions]
    ([permission_id], [en_name], [en_description], [fr_description], [codename], [date_assigned], [expiry_date], [content_type_id], [fr_name])
VALUES
    (11, N'Consultation Services', N'Responsible for ...', N'FR Responsible for ...', N'is_consultation_services', CAST(N'2024-05-15T12:34:58.3745220' AS DateTime2), NULL, 2, N'FR Consultation Services')
SET IDENTITY_INSERT [dbo].[user_management_models_custompermissions] OFF

GO

-- EE INFO OPTIONS
INSERT [dbo].[user_management_models_eeinfooptions]
    ([opt_id], [opt_value])
VALUES
    (1, N'YES')
INSERT [dbo].[user_management_models_eeinfooptions]
    ([opt_id], [opt_value])
VALUES
    (2, N'NO')
INSERT [dbo].[user_management_models_eeinfooptions]
    ([opt_id], [opt_value])
VALUES
    (3, N'PREFER_NOT_TO_ANSWER')

GO

-- REF ABORIGINAL VW
INSERT [dbo].[CAT_REF_ABORIGINAL_VW]
    ([ABRG_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (1, N'1', NULL, NULL, N'Inuit', N'Inuit', CAST(N'2023-07-19T18:33:17.1785110' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_ABORIGINAL_VW]
    ([ABRG_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (2, N'1', NULL, NULL, N'Métis', N'Métis', CAST(N'2023-07-19T18:33:17.1785110' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_ABORIGINAL_VW]
    ([ABRG_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (3, N'1', NULL, NULL, N'North-American Indian/First Nation', N'Indien de l''Amérique du Nord/Première nation', CAST(N'2023-07-19T18:33:17.1785110' AS DateTime2), NULL)

GO

-- REF CLASSIFICATION VW
INSERT [dbo].[CAT_REF_CLASSIFICATION_VW]
    ([CLASSIF_ID], [ACTIVE_FLG], [CLASS_GRP_CD], [CLASS_SBGRP_CD], [CLASS_LVL_CD], [EFDT], [XDT], [PSC_CLASS_IND], [DFLT_BUD_CD], [EQLZN_AMT], [EQLZN_AMT_EFDT], [EQLZN_AMT_XDT], [OFCR_LVL_IND])
VALUES
    (1, N'1', N'AA', N'', N'01', CAST(N'2023-07-19T18:33:16.4065360' AS DateTime2), NULL, 0, N'40212', 0, NULL, NULL, 0)
INSERT [dbo].[CAT_REF_CLASSIFICATION_VW]
    ([CLASSIF_ID], [ACTIVE_FLG], [CLASS_GRP_CD], [CLASS_SBGRP_CD], [CLASS_LVL_CD], [EFDT], [XDT], [PSC_CLASS_IND], [DFLT_BUD_CD], [EQLZN_AMT], [EQLZN_AMT_EFDT], [EQLZN_AMT_XDT], [OFCR_LVL_IND])
VALUES
    (2, N'1', N'AA', N'', N'02', CAST(N'2023-07-19T18:33:16.4065360' AS DateTime2), NULL, 0, N'40212', 0, NULL, NULL, 0)
INSERT [dbo].[CAT_REF_CLASSIFICATION_VW]
    ([CLASSIF_ID], [ACTIVE_FLG], [CLASS_GRP_CD], [CLASS_SBGRP_CD], [CLASS_LVL_CD], [EFDT], [XDT], [PSC_CLASS_IND], [DFLT_BUD_CD], [EQLZN_AMT], [EQLZN_AMT_EFDT], [EQLZN_AMT_XDT], [OFCR_LVL_IND])
VALUES
    (3, N'1', N'BB', N'', N'01', CAST(N'2023-07-19T18:33:16.4065360' AS DateTime2), NULL, 0, N'40212', 0, NULL, NULL, 0)
INSERT [dbo].[CAT_REF_CLASSIFICATION_VW]
    ([CLASSIF_ID], [ACTIVE_FLG], [CLASS_GRP_CD], [CLASS_SBGRP_CD], [CLASS_LVL_CD], [EFDT], [XDT], [PSC_CLASS_IND], [DFLT_BUD_CD], [EQLZN_AMT], [EQLZN_AMT_EFDT], [EQLZN_AMT_XDT], [OFCR_LVL_IND])
VALUES
    (4, N'1', N'BB', N'', N'02', CAST(N'2023-07-19T18:33:16.4065360' AS DateTime2), NULL, 0, N'40212', 0, NULL, NULL, 0)
INSERT [dbo].[CAT_REF_CLASSIFICATION_VW]
    ([CLASSIF_ID], [ACTIVE_FLG], [CLASS_GRP_CD], [CLASS_SBGRP_CD], [CLASS_LVL_CD], [EFDT], [XDT], [PSC_CLASS_IND], [DFLT_BUD_CD], [EQLZN_AMT], [EQLZN_AMT_EFDT], [EQLZN_AMT_XDT], [OFCR_LVL_IND])
VALUES
    (5, N'1', N'BB', N'', N'03', CAST(N'2023-07-19T18:33:16.4065360' AS DateTime2), NULL, 0, N'40212', 0, NULL, NULL, 0)

GO

-- REF DEPARTMENTS VW
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (1, N'1', N'AAA', N'FR AAA', N'AAA Full Name', N'FR AAA Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (2, N'1', N'BBB', N'FR BBB', N'BBB Full Name', N'FR BBB Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (3, N'1', N'CCC', N'FR CCC', N'CCC Full Name', N'FR CCC Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (4, N'1', N'DDD', N'FR DDD', N'DDD Full Name', N'FR DDD Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (5, N'1', N'EEE', N'FR EEE', N'EEE Full Name', N'FR EEE Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (6, N'1', N'FFF', N'FR FFF', N'FFF Full Name', N'FR FFF Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (7, N'1', N'GGG', N'FR GGG', N'GGG Full Name', N'FR GGG Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (8, N'1', N'HHH', N'FR HHH', N'HHH Full Name', N'FR HHH Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (9, N'1', N'III', N'FR III', N'III Full Name', N'FR III Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (10, N'1', N'JJJ', N'FR JJJ', N'JJJ Full Name', N'FR JJJ Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (11, N'1', N'KKK', N'FR KKK', N'KKK Full Name', N'FR KKK Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (12, N'1', N'LLL', N'FR LLL', N'LLL Full Name', N'FR LLL Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (13, N'1', N'MMM', N'FR MMM', N'MMM Full Name', N'FR MMM Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (14, N'1', N'NNN', N'FR NNN', N'NNN Full Name', N'FR NNN Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (15, N'1', N'OOO', N'FR OOO', N'OOO Full Name', N'FR OOO Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (16, N'1', N'PPP', N'FR PPP', N'PPP Full Name', N'FR PPP Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (17, N'1', N'QQQ', N'FR QQQ', N'QQQ Full Name', N'FR QQQ Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (18, N'1', N'RRR', N'FR RRR', N'RRR Full Name', N'FR RRR Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (19, N'1', N'SSS', N'FR SSS', N'SSS Full Name', N'FR SSS Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (20, N'1', N'TTT', N'FR TTT', N'TTT Full Name', N'FR TTT Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (21, N'1', N'UUU', N'FR UUU', N'UUU Full Name', N'FR UUU Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (22, N'1', N'VVV', N'FR VVV', N'VVV Full Name', N'FR VVV Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (23, N'1', N'WWW', N'FR WWW', N'WWW Full Name', N'FR WWW Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (24, N'1', N'XXX', N'FR XXX', N'XXX Full Name', N'FR XXX Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (25, N'1', N'YYY', N'FR YYY', N'YYY Full Name', N'FR YYY Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DEPARTMENTS_VW]
    ([DEPT_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (26, N'1', N'ZZZ', N'FR ZZZ', N'ZZZ Full Name', N'FR ZZZ Full Name', CAST(N'2002-04-22T04:56:00.0000000' AS DateTime2), NULL)

GO

-- REF DISABILITY VW
INSERT [dbo].[CAT_REF_DISABILITY_VW]
    ([DSBL_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [FULL_EDESC], [FULL_FDESC], [EFDT], [XDT])
VALUES
    (1, N'1', NULL, NULL, N'Co-ordination or dexterity', N'Coordination ou dextérité', N'(difficulty using hands or arms, for example, grasping or handling a stapler or using a keyboard)', N'(difficulté à se servir des mains ou bras, par exemple pour saisir ou utiliser une agrafeuse ou pour travailler au clavier)', CAST(N'2023-07-19T18:33:17.4612210' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DISABILITY_VW]
    ([DSBL_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [FULL_EDESC], [FULL_FDESC], [EFDT], [XDT])
VALUES
    (2, N'1', NULL, NULL, N'Mobility', N'Mobilité', N'(difficulty moving around, for example, from one office to another or up and down stairs)', N'(incluant : difficulté à se déplacer d''un local à l''autre, à monter ou à descendre les escaliers, etc.)', CAST(N'2023-07-19T18:33:17.4612210' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DISABILITY_VW]
    ([DSBL_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [FULL_EDESC], [FULL_FDESC], [EFDT], [XDT])
VALUES
    (3, N'1', NULL, NULL, N'Speech impairment', N'Élocution', N'(unable to speak or difficulty speaking and being understood)', N'(incapacité à parler ou difficulté à parler et à se faire comprendre)', CAST(N'2023-07-19T18:33:17.4612210' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DISABILITY_VW]
    ([DSBL_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [FULL_EDESC], [FULL_FDESC], [EFDT], [XDT])
VALUES
    (4, N'1', NULL, NULL, N'Blind or Visual impairment', N'Cécité ou déficience visuelle', N'(unable to see or difficulty seeing)', N'(incapacité ou difficulté à voir)', CAST(N'2023-07-19T18:33:17.4612210' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DISABILITY_VW]
    ([DSBL_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [FULL_EDESC], [FULL_FDESC], [EFDT], [XDT])
VALUES
    (5, N'1', NULL, NULL, N'Deaf or hard of hearing', N'Surdité ou malentendance', N'(unable to hear or difficulty hearing)', N'(incapacité ou difficulté à entendre)', CAST(N'2023-07-19T18:33:17.4612210' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_DISABILITY_VW]
    ([DSBL_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [FULL_EDESC], [FULL_FDESC], [EFDT], [XDT])
VALUES
    (6, N'1', NULL, NULL, N'Other Disability', N'Autre handicap', N'(including: learning disabilities, developmental disabilities and all other types of disabilities)', N'(difficultés d''apprentissage ou de développement et tout autre type de handicap)', CAST(N'2023-07-19T18:33:17.4612210' AS DateTime2), NULL)

GO

-- REF EMPLOYER STS VW
INSERT [dbo].[CAT_REF_EMPLOYER_STS_VW]
    ([EMPSTS_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (1, N'1', N'Government employee', N'Employé(e) de l''État', CAST(N'2023-07-19T18:33:17.6461530' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_EMPLOYER_STS_VW]
    ([EMPSTS_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (2, N'1', N'Crown corporation or agency employee', N'Employé(e) d''une société de la couronne ou d''un organ. féd.', CAST(N'2023-07-19T18:33:17.6461530' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_EMPLOYER_STS_VW]
    ([EMPSTS_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (3, N'1', N'Other', N'Autre', CAST(N'2023-07-19T18:33:17.6461530' AS DateTime2), NULL)

GO

-- REF GENDER VW
INSERT [dbo].[CAT_REF_GENDER_VW]
    ([GND_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (1, N'1', NULL, NULL, N'Male', N'Homme', CAST(N'2023-07-19T18:33:16.5699700' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_GENDER_VW]
    ([GND_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (2, N'1', NULL, NULL, N'Female', N'Femme', CAST(N'2023-07-19T18:33:16.5699700' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_GENDER_VW]
    ([GND_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (3, N'1', NULL, NULL, N'Other gender', N'Un autre genre', CAST(N'2023-07-19T18:33:16.5699700' AS DateTime2), NULL)

GO

-- REF PROVINCE VW
INSERT [dbo].[CAT_REF_PROVINCE_VW]
    ([PROV_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (1, N'1', N'H', N'H', N'Here', N'FR Here', CAST(N'2023-07-19T18:33:16.8437070' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_PROVINCE_VW]
    ([PROV_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (2, N'1', N'T', N'T', N'There', N'FR There', CAST(N'2023-07-19T18:33:16.8437070' AS DateTime2), NULL)

GO

-- REF VISIBLE MINORITY VW
INSERT [dbo].[CAT_REF_VISIBLE_MINORITY_VW]
    ([VISMIN_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (1, N'1', N'Black', N'Noir', CAST(N'2023-07-19T18:33:17.8099270' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_VISIBLE_MINORITY_VW]
    ([VISMIN_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (2, N'1', N'Non-White Latin American', N'Latino Américain non-blanc', CAST(N'2023-07-19T18:33:17.8099270' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_VISIBLE_MINORITY_VW]
    ([VISMIN_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (3, N'1', N'Person of Mixed Origin', N'Personnes d''origine mixte', CAST(N'2023-07-19T18:33:17.8099270' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_VISIBLE_MINORITY_VW]
    ([VISMIN_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (4, N'1', N'Chinese', N'Chinois', CAST(N'2023-07-19T18:33:17.8099270' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_VISIBLE_MINORITY_VW]
    ([VISMIN_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (5, N'1', N'Japanese', N'Japonais', CAST(N'2023-07-19T18:33:17.8099270' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_VISIBLE_MINORITY_VW]
    ([VISMIN_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (6, N'1', N'Korean', N'Coréen', CAST(N'2023-07-19T18:33:17.8099270' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_VISIBLE_MINORITY_VW]
    ([VISMIN_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (7, N'1', N'Filipino', N'Philippin', CAST(N'2023-07-19T18:33:17.8099270' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_VISIBLE_MINORITY_VW]
    ([VISMIN_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (8, N'1', N'South Asian/East Indian', N'Asiatique du Sud/Indien de l''Est', CAST(N'2023-07-19T18:33:17.8099270' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_VISIBLE_MINORITY_VW]
    ([VISMIN_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (9, N'1', N'Non-White West Asian, North African or Arab', N'Asiatique de l''Ouest non-blanc, Nord Africain ou Arabe', CAST(N'2023-07-19T18:33:17.8099270' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_VISIBLE_MINORITY_VW]
    ([VISMIN_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (10, N'1', N'Southeast Asian', N'Asiatique du Sud-Est', CAST(N'2023-07-19T18:33:17.8099270' AS DateTime2), NULL)
INSERT [dbo].[CAT_REF_VISIBLE_MINORITY_VW]
    ([VISMIN_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (22, N'1', N'Other Visible Minority Groups', N'Autres groupes de minorités visibles', CAST(N'2023-07-19T18:33:17.8099270' AS DateTime2), NULL)

GO

-- LANGUAGE
SET IDENTITY_INSERT [dbo].[custom_models_language] ON
INSERT [dbo].[custom_models_language]
    ([language_id], [ISO_Code_1], [ISO_Code_2], [date_created], [date_from], [date_to])
VALUES
    (1, N'en', N'en-ca', CAST(N'2021-01-26T15:14:47.2521490' AS DateTime2), CAST(N'2021-01-26T15:14:47.2521940' AS DateTime2), NULL)
INSERT [dbo].[custom_models_language]
    ([language_id], [ISO_Code_1], [ISO_Code_2], [date_created], [date_from], [date_to])
VALUES
    (2, N'fr', N'fr-ca', CAST(N'2021-01-26T15:14:47.2577820' AS DateTime2), CAST(N'2021-01-26T15:14:47.2578110' AS DateTime2), NULL)
SET IDENTITY_INSERT [dbo].[custom_models_language] OFF

GO

-- LANGUAGE TEXT
SET IDENTITY_INSERT [dbo].[custom_models_languagetext] ON
INSERT [dbo].[custom_models_languagetext]
    ([id], [text], [language_id], [language_ref_id])
VALUES
    (1, N'English', 1, 1)
INSERT [dbo].[custom_models_languagetext]
    ([id], [text], [language_id], [language_ref_id])
VALUES
    (2, N'Anglais', 2, 1)
INSERT [dbo].[custom_models_languagetext]
    ([id], [text], [language_id], [language_ref_id])
VALUES
    (3, N'French', 1, 2)
INSERT [dbo].[custom_models_languagetext]
    ([id], [text], [language_id], [language_ref_id])
VALUES
    (4, N'Français', 2, 2)
SET IDENTITY_INSERT [dbo].[custom_models_languagetext] OFF

GO

-- UIT REASON FOR DELETION
SET IDENTITY_INSERT [dbo].[custom_models_uitreasonsfordeletion] ON
INSERT [dbo].[custom_models_uitreasonsfordeletion]
    ([id], [en_name], [fr_name])
VALUES
    (1, N'Candidate withdrawal', N'Retrait du candidat')
INSERT [dbo].[custom_models_uitreasonsfordeletion]
    ([id], [en_name], [fr_name])
VALUES
    (2, N'Wrong test sent', N'Mauvais test envoyé')
INSERT [dbo].[custom_models_uitreasonsfordeletion]
    ([id], [en_name], [fr_name])
VALUES
    (3, N'Confirmed valid results', N'Résultats valides confirmés')
INSERT [dbo].[custom_models_uitreasonsfordeletion]
    ([id], [en_name], [fr_name])
VALUES
    (4, N'Requested by department', N'Annulation demandée par le département')
INSERT [dbo].[custom_models_uitreasonsfordeletion]
    ([id], [en_name], [fr_name])
VALUES
    (5, N'Requested accommodation', N'Mesures d’adaptation demandée')
INSERT [dbo].[custom_models_uitreasonsfordeletion]
    ([id], [en_name], [fr_name])
VALUES
    (6, N'Process Expired', N'Processus Expiré')
INSERT [dbo].[custom_models_uitreasonsfordeletion]
    ([id], [en_name], [fr_name])
VALUES
    (7, N'Other', N'Autre')
SET IDENTITY_INSERT [dbo].[custom_models_uitreasonsfordeletion] OFF

GO

-- UIT REASON FOR MODIFICATION
SET IDENTITY_INSERT [dbo].[custom_models_uitreasonsformodification] ON
INSERT [dbo].[custom_models_uitreasonsformodification]
    ([id], [en_name], [fr_name])
VALUES
    (1, N'Technical Issue', N'Problème technique')
INSERT [dbo].[custom_models_uitreasonsformodification]
    ([id], [en_name], [fr_name])
VALUES
    (2, N'Invitation Error', N'Erreur d''invitation')
INSERT [dbo].[custom_models_uitreasonsformodification]
    ([id], [en_name], [fr_name])
VALUES
    (3, N'Requested by client', N'Demande du client')
INSERT [dbo].[custom_models_uitreasonsformodification]
    ([id], [en_name], [fr_name])
VALUES
    (4, N'Others', N'Autres')
SET IDENTITY_INSERT [dbo].[custom_models_uitreasonsformodification] OFF

GO

-- SCORING METHOD TYPES
SET IDENTITY_INSERT [dbo].[cms_models_scoringmethodtypes] ON
INSERT [dbo].[cms_models_scoringmethodtypes]
    ([id], [method_type_codename], [en_name], [fr_name])
VALUES
    (9, N'THRESHOLD', N'Score Bands', N'Bandes de scores')
INSERT [dbo].[cms_models_scoringmethodtypes]
    ([id], [method_type_codename], [en_name], [fr_name])
VALUES
    (10, N'PASS_FAIL', N'Pass/Fail', N'Succès/Échec')
INSERT [dbo].[cms_models_scoringmethodtypes]
    ([id], [method_type_codename], [en_name], [fr_name])
VALUES
    (11, N'RAW', N'Raw Score', N'Score brut')
INSERT [dbo].[cms_models_scoringmethodtypes]
    ([id], [method_type_codename], [en_name], [fr_name])
VALUES
    (12, N'PERCENTAGE', N'Percentage', N'Pourcentage')
INSERT [dbo].[cms_models_scoringmethodtypes]
    ([id], [method_type_codename], [en_name], [fr_name])
VALUES
    (1004, N'NONE', N'None', N'Aucun')
SET IDENTITY_INSERT [dbo].[cms_models_scoringmethodtypes] OFF

GO

-- ETTA ACTION TYPES
INSERT [dbo].[custom_models_ettaactiontypes]
    ([action_type])
VALUES
    (N'UN_ASSIGN_CANDIDATE')
INSERT [dbo].[custom_models_ettaactiontypes]
    ([action_type])
VALUES
    (N'VALIDATE_CANDIDATE')

GO

-- TA ACTION TYPES
INSERT [dbo].[custom_models_taactiontypes]
    ([action_type])
VALUES
    (N'APPROVE')
INSERT [dbo].[custom_models_taactiontypes]
    ([action_type])
VALUES
    (N'LOCK')
INSERT [dbo].[custom_models_taactiontypes]
    ([action_type])
VALUES
    (N'PAUSE')
INSERT [dbo].[custom_models_taactiontypes]
    ([action_type])
VALUES
    (N'RE_SYNC')
INSERT [dbo].[custom_models_taactiontypes]
    ([action_type])
VALUES
    (N'REPORT')
INSERT [dbo].[custom_models_taactiontypes]
    ([action_type])
VALUES
    (N'REVOKE')
INSERT [dbo].[custom_models_taactiontypes]
    ([action_type])
VALUES
    (N'UN_ASSIGN')
INSERT [dbo].[custom_models_taactiontypes]
    ([action_type])
VALUES
    (N'UNLOCK')
INSERT [dbo].[custom_models_taactiontypes]
    ([action_type])
VALUES
    (N'UNPAUSE')
INSERT [dbo].[custom_models_taactiontypes]
    ([action_type])
VALUES
    (N'UPDATE_TIME')

GO

-- CRITICALITY
SET IDENTITY_INSERT [dbo].[custom_models_criticality] ON
INSERT [dbo].[custom_models_criticality]
    ([id], [description_en], [description_fr], [active], [codename], [priority])
VALUES
    (1, N'Danger', N'Danger', 1, N'danger', 1)
INSERT [dbo].[custom_models_criticality]
    ([id], [description_en], [description_fr], [active], [codename], [priority])
VALUES
    (2, N'Warning', N'Avertissement', 1, N'warning', 2)
INSERT [dbo].[custom_models_criticality]
    ([id], [description_en], [description_fr], [active], [codename], [priority])
VALUES
    (3, N'Information', N'Information', 1, N'information', 3)
SET IDENTITY_INSERT [dbo].[custom_models_criticality] OFF

GO

-- EDUCATION VW
INSERT [dbo].[ref_table_views_cateducation]
    ([education_id], [active_flg], [edesc], [fdesc], [efdt], [xdt])
VALUES
    (1, N'1', N'Secondary Less than 2 years', N'Secondaires Moins de 2 ans', CAST(N'2021-01-26T15:15:04.9278650' AS DateTime2), NULL)
INSERT [dbo].[ref_table_views_cateducation]
    ([education_id], [active_flg], [edesc], [fdesc], [efdt], [xdt])
VALUES
    (2, N'1', N'Secondary 2 years or more ', N'Secondaires Plus de 2 ans', CAST(N'2021-01-26T15:15:04.9278650' AS DateTime2), NULL)
INSERT [dbo].[ref_table_views_cateducation]
    ([education_id], [active_flg], [edesc], [fdesc], [efdt], [xdt])
VALUES
    (3, N'1', N'Secondary Diploma', N'Secondaires Diplômé', CAST(N'2021-01-26T15:15:04.9278650' AS DateTime2), NULL)
INSERT [dbo].[ref_table_views_cateducation]
    ([education_id], [active_flg], [edesc], [fdesc], [efdt], [xdt])
VALUES
    (4, N'1', N'Community College / CEGEP 1 year', N'Collège communautaire/CÉGEP 1 année', CAST(N'2021-01-26T15:15:04.9278650' AS DateTime2), NULL)
INSERT [dbo].[ref_table_views_cateducation]
    ([education_id], [active_flg], [edesc], [fdesc], [efdt], [xdt])
VALUES
    (5, N'1', N'Community College / CEGEP 2 years', N'Collège communautaire/CÉGEP 2 années', CAST(N'2021-01-26T15:15:04.9278650' AS DateTime2), NULL)
INSERT [dbo].[ref_table_views_cateducation]
    ([education_id], [active_flg], [edesc], [fdesc], [efdt], [xdt])
VALUES
    (6, N'1', N'Community College / CEGEP Diploma', N'Collège communautaire/CÉGEP Diplômé', CAST(N'2021-01-26T15:15:04.9278650' AS DateTime2), NULL)
INSERT [dbo].[ref_table_views_cateducation]
    ([education_id], [active_flg], [edesc], [fdesc], [efdt], [xdt])
VALUES
    (7, N'1', N'University 1 year', N'Université 1 année', CAST(N'2021-01-26T15:15:04.9278650' AS DateTime2), NULL)
INSERT [dbo].[ref_table_views_cateducation]
    ([education_id], [active_flg], [edesc], [fdesc], [efdt], [xdt])
VALUES
    (8, N'1', N'University 2 years', N'Université 2 années', CAST(N'2021-01-26T15:15:04.9278650' AS DateTime2), NULL)
INSERT [dbo].[ref_table_views_cateducation]
    ([education_id], [active_flg], [edesc], [fdesc], [efdt], [xdt])
VALUES
    (9, N'1', N'University 3 years', N'Université 3 années', CAST(N'2021-01-26T15:15:04.9278650' AS DateTime2), NULL)
INSERT [dbo].[ref_table_views_cateducation]
    ([education_id], [active_flg], [edesc], [fdesc], [efdt], [xdt])
VALUES
    (10, N'1', N'University Degree', N'Université Diplômé', CAST(N'2021-01-26T15:15:04.9278650' AS DateTime2), NULL)
INSERT [dbo].[ref_table_views_cateducation]
    ([education_id], [active_flg], [edesc], [fdesc], [efdt], [xdt])
VALUES
    (11, N'1', N'University Master''s Degree', N'Université Maîtrise', CAST(N'2021-01-26T15:15:04.9278650' AS DateTime2), NULL)
INSERT [dbo].[ref_table_views_cateducation]
    ([education_id], [active_flg], [edesc], [fdesc], [efdt], [xdt])
VALUES
    (12, N'1', N'University Doctoral Degree', N'Université Doctorat', CAST(N'2021-01-26T15:15:04.9278650' AS DateTime2), NULL)

GO

-- TEST SKILL TYPE
SET IDENTITY_INSERT [dbo].[cms_models_testskilltype] ON
INSERT [dbo].[cms_models_testskilltype]
    ([id], [codename])
VALUES
    (1, N'none')
INSERT [dbo].[cms_models_testskilltype]
    ([id], [codename])
VALUES
    (3, N'occupational')
INSERT [dbo].[cms_models_testskilltype]
    ([id], [codename])
VALUES
    (2, N'sle')
SET IDENTITY_INSERT [dbo].[cms_models_testskilltype] OFF

GO

-- TEST SKILL TYPE TEXT
SET IDENTITY_INSERT [dbo].[cms_models_testskilltypetext] ON
INSERT [dbo].[cms_models_testskilltypetext]
    ([id], [text], [language_id], [test_skill_type_id])
VALUES
    (1, N'None', 1, 1)
INSERT [dbo].[cms_models_testskilltypetext]
    ([id], [text], [language_id], [test_skill_type_id])
VALUES
    (2, N'FR None', 2, 1)
INSERT [dbo].[cms_models_testskilltypetext]
    ([id], [text], [language_id], [test_skill_type_id])
VALUES
    (3, N'Second Language Evaluation', 1, 2)
INSERT [dbo].[cms_models_testskilltypetext]
    ([id], [text], [language_id], [test_skill_type_id])
VALUES
    (4, N'FR Second Language Evaluation', 2, 2)
INSERT [dbo].[cms_models_testskilltypetext]
    ([id], [text], [language_id], [test_skill_type_id])
VALUES
    (5, N'Occupational', 1, 3)
INSERT [dbo].[cms_models_testskilltypetext]
    ([id], [text], [language_id], [test_skill_type_id])
VALUES
    (6, N'FR Occupational', 2, 3)
SET IDENTITY_INSERT [dbo].[cms_models_testskilltypetext] OFF

GO

-- TEST SKILL SLE DESC
SET IDENTITY_INSERT [dbo].[cms_models_testskillsledesc] ON
INSERT [dbo].[cms_models_testskillsledesc]
    ([id], [codename])
VALUES
    (5, N'oe')
INSERT [dbo].[cms_models_testskillsledesc]
    ([id], [codename])
VALUES
    (6, N'of')
INSERT [dbo].[cms_models_testskillsledesc]
    ([id], [codename])
VALUES
    (1, N're')
INSERT [dbo].[cms_models_testskillsledesc]
    ([id], [codename])
VALUES
    (2, N'rf')
INSERT [dbo].[cms_models_testskillsledesc]
    ([id], [codename])
VALUES
    (3, N'we')
INSERT [dbo].[cms_models_testskillsledesc]
    ([id], [codename])
VALUES
    (4, N'wf')
SET IDENTITY_INSERT [dbo].[cms_models_testskillsledesc] OFF

GO

-- TEST SKILL SLE DESC TEXT
SET IDENTITY_INSERT [dbo].[cms_models_testskillsledesctext] ON
GO
INSERT [dbo].[cms_models_testskillsledesctext] ([id], [text], [language_id], [test_skill_sle_desc_id]) VALUES (1, N'English - Reading Comprehension', 1, 1)
GO
INSERT [dbo].[cms_models_testskillsledesctext] ([id], [text], [language_id], [test_skill_sle_desc_id]) VALUES (2, N'Anglais - Compréhension de l''écrit', 2, 1)
GO
INSERT [dbo].[cms_models_testskillsledesctext] ([id], [text], [language_id], [test_skill_sle_desc_id]) VALUES (3, N'French - Reading Comprehension', 1, 2)
GO
INSERT [dbo].[cms_models_testskillsledesctext] ([id], [text], [language_id], [test_skill_sle_desc_id]) VALUES (4, N'Français - Compréhension de l''écrit', 2, 2)
GO
INSERT [dbo].[cms_models_testskillsledesctext] ([id], [text], [language_id], [test_skill_sle_desc_id]) VALUES (5, N'English - Written Expression', 1, 3)
GO
INSERT [dbo].[cms_models_testskillsledesctext] ([id], [text], [language_id], [test_skill_sle_desc_id]) VALUES (6, N'Anglais - Expression écrite', 2, 3)
GO
INSERT [dbo].[cms_models_testskillsledesctext] ([id], [text], [language_id], [test_skill_sle_desc_id]) VALUES (7, N'French - Written Expression', 1, 4)
GO
INSERT [dbo].[cms_models_testskillsledesctext] ([id], [text], [language_id], [test_skill_sle_desc_id]) VALUES (8, N'Français - Expression écrite', 2, 4)
GO
INSERT [dbo].[cms_models_testskillsledesctext] ([id], [text], [language_id], [test_skill_sle_desc_id]) VALUES (9, N'English - Oral', 1, 5)
GO
INSERT [dbo].[cms_models_testskillsledesctext] ([id], [text], [language_id], [test_skill_sle_desc_id]) VALUES (10, N'Anglais - Oral', 2, 5)
GO
INSERT [dbo].[cms_models_testskillsledesctext] ([id], [text], [language_id], [test_skill_sle_desc_id]) VALUES (11, N'French - Oral', 1, 6)
GO
INSERT [dbo].[cms_models_testskillsledesctext] ([id], [text], [language_id], [test_skill_sle_desc_id]) VALUES (12, N'Français - Oral', 2, 6)
GO
SET IDENTITY_INSERT [dbo].[cms_models_testskillsledesctext] OFF

GO

-- ITEM TYPE
SET IDENTITY_INSERT [dbo].[cms_models_itemtype] ON
INSERT [dbo].[cms_models_itemtype]
    ([id], [codename])
VALUES
    (2, N'is_instruction')
INSERT [dbo].[cms_models_itemtype]
    ([id], [codename])
VALUES
    (1, N'is_question')
SET IDENTITY_INSERT [dbo].[cms_models_itemtype] OFF

GO

-- ITEM TYPE TEXT
SET IDENTITY_INSERT [dbo].[cms_models_itemtypetext] ON
INSERT [dbo].[cms_models_itemtypetext]
    ([id], [text], [modify_date], [item_type_id], [language_id])
VALUES
    (1, N'Questions', CAST(N'2023-12-06T15:20:17.0976800+00:00' AS DateTimeOffset), 1, 1)
INSERT [dbo].[cms_models_itemtypetext]
    ([id], [text], [modify_date], [item_type_id], [language_id])
VALUES
    (2, N'Instructions', CAST(N'2023-12-06T15:20:17.1023400+00:00' AS DateTimeOffset), 2, 1)
INSERT [dbo].[cms_models_itemtypetext]
    ([id], [text], [modify_date], [item_type_id], [language_id])
VALUES
    (3, N'FR Questions', CAST(N'2023-12-06T15:20:17.1057200+00:00' AS DateTimeOffset), 1, 2)
INSERT [dbo].[cms_models_itemtypetext]
    ([id], [text], [modify_date], [item_type_id], [language_id])
VALUES
    (4, N'FR Instructions', CAST(N'2023-12-06T15:20:17.1079400+00:00' AS DateTimeOffset), 2, 2)
SET IDENTITY_INSERT [dbo].[cms_models_itemtypetext] OFF

GO

-- CONSUMED RESERVATION CODE STATUS
SET IDENTITY_INSERT [dbo].[custom_models_consumedreservationcodestatus] ON
INSERT [dbo].[custom_models_consumedreservationcodestatus]
    ([id], [codename])
VALUES
    (1, N'booked')
INSERT [dbo].[custom_models_consumedreservationcodestatus]
    ([id], [codename])
VALUES
    (2, N'withdrawn')
INSERT [dbo].[custom_models_consumedreservationcodestatus]
    ([id], [codename])
VALUES
    (3, N'expired')
INSERT [dbo].[custom_models_consumedreservationcodestatus]
    ([id], [codename])
VALUES
    (4, N'reserved')
INSERT [dbo].[custom_models_consumedreservationcodestatus]
    ([id], [codename])
VALUES
    (6, N'requested_acco')
SET IDENTITY_INSERT [dbo].[custom_models_consumedreservationcodestatus] OFF

GO

-- CONSUMED RESERVATION CODE STATUS TEXT
SET IDENTITY_INSERT [dbo].[custom_models_consumedreservationcodestatustext] ON
INSERT [dbo].[custom_models_consumedreservationcodestatustext]
    ([id], [text], [consumed_reservation_code_status_id], [language_id])
VALUES
    (1, N'Booked', 1, 1)
INSERT [dbo].[custom_models_consumedreservationcodestatustext]
    ([id], [text], [consumed_reservation_code_status_id], [language_id])
VALUES
    (2, N'FR Booked', 1, 2)
INSERT [dbo].[custom_models_consumedreservationcodestatustext]
    ([id], [text], [consumed_reservation_code_status_id], [language_id])
VALUES
    (3, N'Withdrawn', 2, 1)
INSERT [dbo].[custom_models_consumedreservationcodestatustext]
    ([id], [text], [consumed_reservation_code_status_id], [language_id])
VALUES
    (4, N'FR Withdrawn', 2, 2)
INSERT [dbo].[custom_models_consumedreservationcodestatustext]
    ([id], [text], [consumed_reservation_code_status_id], [language_id])
VALUES
    (5, N'Expired', 3, 1)
INSERT [dbo].[custom_models_consumedreservationcodestatustext]
    ([id], [text], [consumed_reservation_code_status_id], [language_id])
VALUES
    (6, N'FR Expired', 3, 2)
INSERT [dbo].[custom_models_consumedreservationcodestatustext]
    ([id], [text], [consumed_reservation_code_status_id], [language_id])
VALUES
    (7, N'Booklater', 3, 1)
INSERT [dbo].[custom_models_consumedreservationcodestatustext]
    ([id], [text], [consumed_reservation_code_status_id], [language_id])
VALUES
    (8, N'FR Booklater', 3, 2)
INSERT [dbo].[custom_models_consumedreservationcodestatustext]
    ([id], [text], [consumed_reservation_code_status_id], [language_id])
VALUES
    (11, N'Requested Accommodations', 6, 1)
INSERT [dbo].[custom_models_consumedreservationcodestatustext]
    ([id], [text], [consumed_reservation_code_status_id], [language_id])
VALUES
    (12, N'Requested Accommodations FR', 6, 2)
SET IDENTITY_INSERT [dbo].[custom_models_consumedreservationcodestatustext] OFF

GO

-- REASONS FOR TESTING
SET IDENTITY_INSERT [dbo].[custom_models_reasonsfortesting] ON
INSERT [dbo].[custom_models_reasonsfortesting]
    ([id], [description_en], [description_fr], [active])
VALUES
    (1, N'Imperative staffing', N'Dotation impérative', 0)
INSERT [dbo].[custom_models_reasonsfortesting]
    ([id], [description_en], [description_fr], [active])
VALUES
    (2, N'Non-imperative staffing', N'Dotation non impérative', 0)
INSERT [dbo].[custom_models_reasonsfortesting]
    ([id], [description_en], [description_fr], [active])
VALUES
    (3, N'Full-time language training from the Canada School of Public Service', N'Formation linguistique à temps plein offerte par l''École de la fonction publique du Canada', 0)
INSERT [dbo].[custom_models_reasonsfortesting]
    ([id], [description_en], [description_fr], [active])
VALUES
    (4, N'Part-time Language training from the Canada School of Public Service', N'Formation linguistique à temps partiel dispensée par l''École de la fonction publique du Canada', 0)
INSERT [dbo].[custom_models_reasonsfortesting]
    ([id], [description_en], [description_fr], [active])
VALUES
    (5, N'Full-time language training from departmental program', N'Formation linguistique à temps plein offerte par les ministères', 0)
INSERT [dbo].[custom_models_reasonsfortesting]
    ([id], [description_en], [description_fr], [active])
VALUES
    (6, N'Part-time language training from departmental program', N'Formation linguistique à temps partiel offerte par les ministères', 0)
INSERT [dbo].[custom_models_reasonsfortesting]
    ([id], [description_en], [description_fr], [active])
VALUES
    (7, N'Full-time language training from private language school', N'Form. ling. à temps plein disp. par un établissement ling.', 0)
INSERT [dbo].[custom_models_reasonsfortesting]
    ([id], [description_en], [description_fr], [active])
VALUES
    (8, N'Part-time language training from private language school', N'Form. ling. à temps part. disp. par un établissement ling.', 0)
INSERT [dbo].[custom_models_reasonsfortesting]
    ([id], [description_en], [description_fr], [active])
VALUES
    (9, N'Re-identification', N'Réidentification', 0)
INSERT [dbo].[custom_models_reasonsfortesting]
    ([id], [description_en], [description_fr], [active])
VALUES
    (10, N'Bilingualism bonus', N'Prime au bilinguisme', 0)
INSERT [dbo].[custom_models_reasonsfortesting]
    ([id], [description_en], [description_fr], [active])
VALUES
    (11, N'Record or other purposes', N'Fins documentaires ou autres', 0)
INSERT [dbo].[custom_models_reasonsfortesting]
    ([id], [description_en], [description_fr], [active])
VALUES
    (12, N'COVID-19 or Essential Service', N'COVID-19 ou Service Essential', 0)
INSERT [dbo].[custom_models_reasonsfortesting]
    ([id], [description_en], [description_fr], [active])
VALUES
    (13, N'Staffing / Dotation', N'Dotation / Staffing', 1)
INSERT [dbo].[custom_models_reasonsfortesting]
    ([id], [description_en], [description_fr], [active])
VALUES
    (14, N'Language Training / Formation linguistique', N'Formation linguistique / Language Training', 1)
INSERT [dbo].[custom_models_reasonsfortesting]
    ([id], [description_en], [description_fr], [active])
VALUES
    (15, N'Other / Autre', N'Autre / Other', 1)
SET IDENTITY_INSERT [dbo].[custom_models_reasonsfortesting] OFF

GO

-- ASSIGNED TEST STATUS
SET IDENTITY_INSERT [dbo].[custom_models_assignedteststatus] ON
INSERT [dbo].[custom_models_assignedteststatus]
    ([id], [codename])
VALUES
    (5, N'active')
INSERT [dbo].[custom_models_assignedteststatus]
    ([id], [codename])
VALUES
    (1, N'assigned')
INSERT [dbo].[custom_models_assignedteststatus]
    ([id], [codename])
VALUES
    (2, N'checked_in')
INSERT [dbo].[custom_models_assignedteststatus]
    ([id], [codename])
VALUES
    (7, N'locked')
INSERT [dbo].[custom_models_assignedteststatus]
    ([id], [codename])
VALUES
    (8, N'paused')
INSERT [dbo].[custom_models_assignedteststatus]
    ([id], [codename])
VALUES
    (4, N'pre_test')
INSERT [dbo].[custom_models_assignedteststatus]
    ([id], [codename])
VALUES
    (10, N'quit')
INSERT [dbo].[custom_models_assignedteststatus]
    ([id], [codename])
VALUES
    (9, N'submitted')
INSERT [dbo].[custom_models_assignedteststatus]
    ([id], [codename])
VALUES
    (6, N'transition')
INSERT [dbo].[custom_models_assignedteststatus]
    ([id], [codename])
VALUES
    (3, N'unassigned')
SET IDENTITY_INSERT [dbo].[custom_models_assignedteststatus] OFF

GO

-- SITE ADMIN SETTINGS
SET IDENTITY_INSERT [dbo].[custom_models_siteadminsetting] ON
INSERT [dbo].[custom_models_siteadminsetting]
    ([id], [en_description], [fr_description], [codename], [is_active])
VALUES
    (1, N'Disable user logins', N'FR Disable user logins', N'locklogin', 0)
INSERT [dbo].[custom_models_siteadminsetting]
    ([id], [en_description], [fr_description], [codename], [is_active])
VALUES
    (2, N'Send emails with GC Notify', N'FR Send emails with GC Notify', N'enablegcnotify', 0)
SET IDENTITY_INSERT [dbo].[custom_models_siteadminsetting] OFF

GO

-- GC Notify Templates
SET IDENTITY_INSERT [dbo].[custom_models_gcnotifytemplate] ON 
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (1, N'2fa', N'2fa', N'a6cd6697-3713-4513-8525-5f6c02774a7d')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (2, N'UIT', N'uitinvitebasic', N'35a7c396-6a15-414a-9391-2577e2e491af')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (3, N'deprecated', N'uitinvitebb', N'7fbd1ed7-bd9e-486c-874e-57702872dd79')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (4, N'deprecated', N'uitinviteet', N'6004dfa9-aff0-4577-a6f2-f644e6374838')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (5, N'deprecated', N'uitinviteetbb', N'2537b795-f5c2-4f86-b816-ac7ef852cf46')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (6, N'Supervised', N'reservationinvite', N'f29d07df-5298-4f13-880c-a1266c1d2d0c')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (7, N'Misc', N'shareresult', N'f71aa6a8-2dcc-4863-8a4b-5ff2e4241055')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (8, N'Supervised', N'failedsupdelivery', N'a5b8af32-26dd-4f64-b0d4-716b4b52ba44')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (9, N'UIT', N'faileduitdelivery', N'f7e72c80-f483-4749-a278-12d40a759ff4')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (12, N'Supervised', N'resendalreadyassignedtesttoadminister', N'a435c1bd-7574-415d-ae15-c70e98b14d72')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (13, N'Supervised', N'sendnewassignedtesttoadminister', N'02c8316f-bf4f-4088-ba4e-914f690a92c8')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (14, N'Supervised', N'revokeassignedtesttoadminister', N'10ada26d-ff5f-48f0-b09c-e5167726a7b3')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (17, N'Supervised', N'deletespecifictestsession', N'c35d701e-455e-48b3-94d9-1fb7f97ebeed')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (18, N'Supervised', N'reservationcodewithdrawnobooking', N'7d3da5c2-7a8d-4b74-94d0-054b4e40628b')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (19, N'Supervised', N'reservationcodewithdrawfrombookedsession', N'6341fa0e-973a-4670-9a7b-1c65c989f443')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (20, N'Supervised', N'reservationcodebooking', N'e3094ad8-bc39-4231-a9d3-e1bbb848b535')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (21, N'Supervised', N'reservationcodebooklater', N'6a2d3b20-84ce-4fc8-a095-fcada40d0291')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (22, N'Misc', N'passwordreset', N'8fc1065a-9fd3-4a58-b3f6-6eb6a0f01b85')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (23, N'Misc', N'testsessioncandidatestosecurity', N'aae684c4-8942-4768-b304-ed9658665c72')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (24, N'UIT', N'modifyuitvalidityenddate', N'461bb914-79c2-4ec3-80b5-26b104fb70a7')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (25, N'UIT', N'deactivateuittestcancellation', N'd847a131-4a82-4638-bcd8-a087a6bae1f2')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (26, N'Supervised', N'updateactiveassessmentprocessdata', N'7c2a7e86-59c3-4a20-ab72-1da8d7137590')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (27, N'Supervised', N'testsessioncancellationbasedonassessmentprocessdataupdates', N'00c415c3-5bab-4bbd-ad0a-0624f0f7bcf7')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (28, N'Supervised', N'testsessioncancellationinformrelatedhrcoordinator', N'39ede176-4168-4bf7-b025-b40a07168809')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (30, N'Supervised', N'failedsendassignedtesttoadministerdelivery', N'bbc7212d-c85c-4ccf-b26f-a52b8ce82d64')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (31, N'Misc', N'failedsendtestsessioncandidatestosecurity', N'1d492ff0-0e98-43d6-bf99-35752e56c902')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (32, N'Supervised', N'invitecandidatetononstandardtestsession', N'22404c9f-1ce5-4727-8d2b-554ac54b50a0')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (33, N'Supervised', N'updatenonstandardtestsessioninvite', N'378629e7-018c-43b8-9c61-3c1507fd9d16')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (34, N'Supervised', N'deletenonstandardtestsession', N'be9c24a4-23bc-44dc-b14e-b7256407d576')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (35, N'Accomodations', N'newaccomodationsrequesthr', N'35da1696-b79e-4a86-a5fd-4daa343c3e01')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (36, N'Accomodations', N'canceledaccomodationsrequest', N'ebb4492e-19d3-4996-b023-10d604aa4472')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (37, N'Accomodations', N'canceledaccomodationsrequestcandidate', N'27bae4a5-576f-45d7-bd5d-458daa0abe7b')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (38, N'Accomodations', N'completedaccomodationsrequest', N'8af31265-dd5e-4f98-827a-298dd4baa2a2')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (39, N'Accomodations', N'reopenedaccomodationsrequest', N'896c33e2-df93-46d3-aa86-0a9c375a1762')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (41, N'Accommodations', N'pendingapprovalaccomodationsrequest', N'bb4891f8-6648-4d95-bd9d-29fabcd6c926')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (42, N'Accommodations', N'expiredaccommodationrequest', N'1e5a05ff-885a-47b3-bd39-4421d121d8ac')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (43, N'OLA', N'twodayreminder', N'a12630c9-fd3f-405c-891a-a748f3bd4be0')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (44, N'OLA', N'sevendayreminder', N'e4e508c0-ec32-4de9-a334-015f9aa1e203')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (45, N'OLA', N'candidatewithdrawlnotification', N'44ca089e-d46a-424a-8f26-fe4f42e15d10')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (46, N'Supervised', N'hrtriggeredwithdraw', N'a681c650-4a3e-430a-bb13-59e3dc430054')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (47, N'Supervised', N'testbumpedbytc', N'1fe38fba-acbf-4fb6-b89d-7a6e3a45563e')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (48, N'OLA', N'olacodebooking', N'40b68965-476e-45d0-8de4-c18f3f84f2c3')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (49, N'OLA', N'olacodebooklater', N'6f9a0c6d-4b78-4e29-84d7-53d4ddf5acfa')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (50, N'OLA', N'olainvitecandidatetononstandardtestsessioninperson', N'a67610ef-07ac-4c4c-afcf-551744594e9d')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (51, N'OLA', N'olainvitecandidatetononstandardtestsessionvirtual', N'a0f407fe-aaa4-49fe-b38c-da87607816a7')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (52, N'OLA', N'olaupdatenonstandardtestsessioninviteinperson', N'397ee57a-5434-4529-902b-0be51efa50e4')
INSERT [dbo].[custom_models_gcnotifytemplate] ([id], [folder], [codename], [template_id]) VALUES (53, N'OLA', N'olaupdatenonstandardtestsessioninvitevirtual', N'893b7cef-949d-4be7-93ac-d6622339f18c')
SET IDENTITY_INSERT [dbo].[custom_models_gcnotifytemplate] OFF

GO

-- CAT_REF_COUNTRY_VW
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (1, N'1', N'Aruba', N'Aruba', N'Aruba', N'Aruba', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (2, N'1', N'Afghanistan', N'Afghanistan', N'Islamic State of Afghanistan', N'État islamique dAfghanistan', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (3, N'1', N'Anla', N'Anla', N'Republic of Anla', N'République dAnla', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (4, N'1', N'Anguilla', N'Anguilla', N'Anguilla', N'Anguilla', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (5, N'1', N'Albania', N'Albanie', N'Republic of Albania', N'République dAlbanie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (6, N'1', N'Andorra', N'Andorra', N'Principality of Andorra', N'Principauté dAndorra', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (7, N'1', N'Netherlands Antilles', N'Antilles Néerlandaises', N'Netherlands Antilles', N'Antilles Néerlandaises', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (8, N'1', N'United arab Emirates', N'Émirats arabes unis', N'United arab Emirates', N'Émirats arabes unis', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (9, N'1', N'Argentina', N'Argentine', N'Argentine Republic', N'République argentine', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (10, N'1', N'Armenia', N'Arménie', N'Republic of Armenia', N'République dArménie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (11, N'1', N'American Samoa', N'Samoa occidentales', N'American Samoa', N'Samoa occidentales', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (12, N'1', N'Antarctica', N'Antarctique', N'Antarctica', N'Antarctique', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (13, N'1', N'French Southern Territories', N'Terres australes françaises', N'French Southern Territories', N'Terres australes françaises', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (14, N'1', N'Antigua and Barbuda', N'Antigua-et-Barbuda', N'Antigua and Barbuda', N'Antigua-et-Barbuda', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (15, N'1', N'Australia', N'Australie', N'Australia', N'Australie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (16, N'1', N'Austria', N'Autriche', N'Republic of Austria', N'République dAutriche', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (17, N'1', N'Azerbaijani Republic', N'Azerbaïdjan', N'Azerbaijani Republic', N'République dAzerbaïdjan', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (18, N'1', N'Burundi', N'Burundi', N'Republic of Burundi', N'République du Burundi', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (19, N'1', N'Belgium', N'Belgique', N'Kingdom of Belgium', N'Royaume de Belgique', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (20, N'1', N'Benin', N'Bénin', N'Republic of Benin', N'République du Bénin', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (21, N'1', N'Burkina Faso', N'Burkina Faso', N'Burkina Faso', N'Burkina Faso', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (22, N'1', N'Bangladesh', N'Bangladesh', N'Peoples Republic of Bangladesh', N'République populaire du Bangladesh', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (23, N'1', N'Bulgaria', N'Bulgarie', N'Republic of Bulgaria', N'République de Bulgarie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (24, N'1', N'Bahrain', N'Bahreïn', N'State of Bahrain', N'État du Bahreïn', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (25, N'1', N'Bahamas', N'Bahamas', N'Commonwealth of the Bahamas', N'Commonwealth des Bahamas', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (26, N'1', N'Bosnia and Hercevina', N'Bosnie-Herzévine', N'Republic of Bosnia and Hercevina', N'République de Bosnie-Herzévine', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (27, N'1', N'Belarus', N'Bélarus', N'Republic of Belarus', N'République du Bélarus', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (28, N'1', N'Belize', N'Belize', N'Belize', N'Belize', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (29, N'1', N'Bermuda', N'Bermudes', N'Bermuda', N'Bermudes', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (30, N'1', N'Bolivia', N'Bolivie', N'Republic of Bolivia', N'République de Bolivie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (31, N'1', N'Federative Brazil', N'Brézil', N'Federative Republic of Brazil', N'République fédérative du Brézil', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (32, N'1', N'Barbados', N'Barbade', N'Barbados', N'Barbade', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (33, N'1', N'Brunei Darussalam', N'Brunéi Darussalam', N'Brunei Darussalam', N'Brunéi Darussalam', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (34, N'1', N'Bhutan', N'Bhoutan', N'Kingdom of Bhutan', N'Royaume du Bhoutan', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (35, N'1', N'Bouvet Island', N'Île Bouvet', N'Bouvet Island', N'Île Bouvet', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (36, N'1', N'Botswana', N'Botswana', N'Republic of Botswana', N'République du Botswana', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (37, N'1', N'Central African Republic', N'République centrafricaine', N'Central African Republic', N'République centrafricaine', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (38, N'1', N'Canada', N'Canada', N'Canada', N'Canada', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (39, N'1', N'Cocos (Keeling) Island', N'Île Cocos (Keeling)', N'Cocos (Keeling) Island', N'Île Cocos (Keeling)', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (40, N'1', N'Switzerland', N'Suisse', N'Swiss Confederation', N'Confédération suisse', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (41, N'1', N'Chile', N'Chili', N'Republic of Chilie', N'République du Chili', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (42, N'1', N'China', N'Chine', N'Peoples Republic of China', N'République populaire de Chine', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (43, N'1', N'Côte dIvoire', N'Côte dIvoire', N'Republic of Côte dIvoire', N'République de la Côte dIvoire', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (44, N'1', N'Cameroon', N'Cameroun', N'Republic of Cameroon', N'République du Cameroun', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (45, N'1', N'the Con', N'Con', N'Republic of the Con', N'République du Con', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (46, N'1', N'Cook Islands', N'Îles Cook', N'Cook Islands', N'Îles Cook', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (47, N'1', N'Columbia', N'Columbie', N'Republic of Columbia', N'République de Columbie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (48, N'1', N'Islamic Federal Republic', N'République fédérale islamique', N'Islamic Federal Republic', N'République fédérale islamique', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (49, N'1', N'Cape Verde', N'Cap-Vert', N'Republic of Cape Verde', N'République du Cap-Vert', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (50, N'1', N'Costa Rica', N'Costa Rica', N'Republic of Costa Rica', N'République du Costa Rica', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (51, N'1', N'Cuba', N'Cuba', N'Republic of Cuba', N'République de Cuba', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (52, N'1', N'Christmas Island', N'Île Christmas', N'Christmas Island', N'Île Christmas', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (53, N'1', N'Cayman Islands', N'Îles Caïmans', N'Cayman Islands', N'Îles Caïmans', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (54, N'1', N'Cyprus', N'Chypre', N'Republic of Cyprus', N'République de Chypre', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (55, N'1', N'Czech Republic', N'République tchèque', N'Czech Republic', N'République tchèque', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (56, N'1', N'Germany', N'Allemagne', N'Federal Republic of Germany', N'République fédérale dAllemagne', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (57, N'1', N'Djibouti', N'Djibouti', N'Republic of Djibouti', N'République de Djibouti', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (58, N'1', N'Commonwealth of Dominic', N'Commonwealth de la Dominique', N'Commonwealth of Dominic', N'Commonwealth de la Dominique', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (59, N'1', N'Denmark', N'Danemark', N'Kingdom of Denmark', N'Royaume du Danemark', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (60, N'1', N'Dominican Republic', N'République dominicaine', N'Dominican Republic', N'République dominicaine', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (61, N'1', N'Algeria', N'Algérie', N'Peoples Democratic of Algeria', N'République démocratique dAlgérie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (62, N'1', N'Ecuador', N'Équateur', N'Republic of Ecuador', N'République de lÉquateur', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (63, N'1', N'Egypt', N'Égypte', N'Arab Republic of Egypt', N'République arabe dÉgypte', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (64, N'1', N'Eritrea', N'Érythrée', N'Eritrea', N'Érythrée', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (65, N'1', N'Western Sahara', N'Sahara occidental', N'Western Sahara', N'Sahara occidental', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (66, N'1', N'Spain', N'Espagne', N'Kingdom of Spain', N'Royaume dEspagne', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (67, N'1', N'Estonia', N'Estonie', N'Republic of Estonia', N'République dEstonie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (68, N'1', N'Ethiopia', N'Éthiopie', N'Ethiopia', N'Éthiopie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (69, N'1', N'Finland', N'Finlande', N'Republic of Finland', N'République de Finlande', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (70, N'1', N'Fiji Islands', N'Îles Fidji', N'Republic of Fiji Islands', N'République des Îles Fidji', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (71, N'1', N'Falkland Islands (Malvinas)', N'Îles Falkland (Malouines)', N'Falkland Islands (Malvinas)', N'Îles Falkland (Malouines)', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (72, N'1', N'French Republic', N'République française', N'French Republic', N'République française', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (73, N'1', N'Faroe Islands', N'Îles Féréo', N'Faroe Islands', N'Îles Féréo', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (74, N'1', N'Federated States of Micronesia', N'États fédérés de Micronésie', N'Federated States of Micronesia', N'États fédérés de Micronésie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (75, N'1', N'France', N'France', N'France Metropolitan', N'France métropolitaine', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (76, N'1', N'Gabonese Republic', N'République gabonaise', N'Gabonese Republic', N'République gabonaise', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (77, N'1', N'Great Britain', N'Grande-Bretagne', N'United Kingdom of Great-Britain', N'Royaume-Uni de Grande-Bretagne', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (78, N'1', N'Georgia', N'Géorgie', N'Republic of Georgia', N'République de Géorgie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (79, N'1', N'Ghana', N'Ghana', N'Republic of Ghana', N'République du Ghana', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (80, N'1', N'Gibraltar', N'Gibraltar', N'Gibraltar', N'Gibraltar', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (81, N'1', N'Guinea', N'Guinée', N'Republic of Guinea', N'République de Guinée', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (82, N'1', N'Guadeloupe', N'Guadeloupe', N'Department of Guadeloupe', N'Département de la Guadeloupe', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (83, N'1', N'Gambia', N'Gambie', N'Republic of Gambia', N'République de Gambie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (84, N'1', N'Guinea-Bissau', N'Guinée-Bissau', N'Republic of Guinea-Bissau', N'République de Guinée-Bissau', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (85, N'1', N'Equatorial Guinea', N'Guinée équatoriale', N'Republic of Equatorial Guinea', N'République de Guinée équatoriale', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (86, N'1', N'Greece', N'République hellénique', N'Hellenic Republic', N'République hellénique', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (87, N'1', N'Grenada', N'Grenade', N'Grenada', N'Grenade', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (88, N'1', N'Greenland', N'Groenland', N'Greenland', N'Groenland', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (89, N'1', N'Guatemala', N'Guatemala', N'Republic of Guatemala', N'République du Guatemala', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (90, N'1', N'Department of Guiana', N'Guiane', N'Department of Guiana', N'Département de la Guiane', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (91, N'1', N'Guam', N'Guam', N'Guam', N'Guam', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (92, N'1', N'Guyana', N'Guyana', N'Republic of Guyana', N'République de Guyana', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (93, N'1', N'Hong Kong', N'Hong Kong', N'Hong Kong', N'Hong Kong', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (94, N'1', N'Heard Islands and Macdonald', N'Îles de Heard et Macdonald', N'Heard Islands and Macdonald', N'Îles de Heard et Macdonald', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (95, N'1', N'Honduras', N'Honduras', N'Republic of Honduras', N'République du Honduras', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (96, N'1', N'Croatia', N'Croatie', N'Republic of Croatia', N'République de Croatie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (97, N'1', N'Haiti', N'Haïti', N'Republic of Haiti', N'République dHaïti', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (98, N'1', N'Hungary', N'Hongrie', N'Republic of Hungary', N'République de Hongrie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (99, N'1', N'Indonesia', N'Indonésie', N'Republic of Indonesia', N'République dIndonésie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (100, N'1', N'India', N'Inde', N'Republic of India', N'République de lInde', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (101, N'1', N'British Indian Ocean Territory', N'Territoire britannique de lOcéan indien', N'British Indian Ocean Territory', N'Territoire britannique de lOcéan indien', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (102, N'1', N'Ireland', N'Irlande', N'Ireland', N'Irlande', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (103, N'1', N'Iran', N'Iran', N'Islamic Republic of Iran', N'République islamique dIran', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (104, N'1', N'Iraq', N'Iraq', N'Republic of Iraq', N'République dIraq', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (105, N'1', N'Iceland', N'Islande', N'Republic of Iceland', N'République dIslande', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (106, N'1', N'Israel', N'Israël', N'State of Israel', N'État dIsraël', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (107, N'1', N'Italy', N'Italie', N'Italian Republic', N'République italienne', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (108, N'1', N'Jamaica', N'Jamaïque', N'Jamaica', N'Jamaïque', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (109, N'1', N'Hashemite', N'Hachémite', N'Kingdom of Hashemite', N'Royaume hachémite', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (110, N'1', N'Japan', N'Japon', N'Japan', N'Japon', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (111, N'1', N'Kazakhstan', N'Kazakhstan', N'Republic of Kazakhstan', N'République du Kazakhstan', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (112, N'1', N'Kenya', N'Kenya', N'Republic of Kenya', N'République du Kenya', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (113, N'1', N'Kyrgyz Republic', N'République Kirghize', N'Kyrgyz Republic', N'République Kirghize', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (114, N'1', N'Cambodia', N'Cambodge', N'Kingdom of Cambodia', N'Royaume du Cambodge', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (115, N'1', N'Kiribati', N'Kiribati', N'Kiribati', N'Kiribati', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (116, N'1', N'Saint Kitts and Nevi', N'Saint-Kitts-et-Nevis', N'Saint Kitts and Nevi', N'Saint-Kitts-et-Nevis', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (117, N'1', N'Korea', N'Corée', N'Republic of Korea', N'République de Corée', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (118, N'1', N'Kuwait', N'Koweït', N'State of Kuwait', N'État du Koweït', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (119, N'1', N'Laos', N'Laos', N'Laos Peoples Democratic Republic', N'République démocratique du Laos', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (120, N'1', N'Lebanon', N'Liban', N'Lebanese Republic', N'République libanaise', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (121, N'1', N'Liberia', N'Libéria', N'Republic of Liberia', N'République du Libéria', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (122, N'1', N'Libya', N'Libye', N'Socialist Peoples Libyan Arab Jamahiriya', N'Jamahiriya arabe libyenne populaire et socialiste', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (123, N'1', N'Saint Lucia', N'Sainte-Lucie', N'Saint Lucia', N'Sainte-Lucie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (124, N'1', N'Liechtenstein', N'Liechtenstein', N'Principality of Liechtenstein', N'Principauté du Liechtenstein', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (125, N'1', N'Sri Lanka', N'Sri Lanka', N'Democratic Socialist Republic of Sri Lanka', N'République socialiste démocratique de Sri Lanka', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (126, N'1', N'Lesotho', N'Lesotho', N'Kingdom of Lesotho', N'Royaume du Lesotho', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (127, N'1', N'Lithuania', N'Lituanie', N'Republic of Lithuania', N'République de Lituanie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (128, N'1', N'Luxembourg', N'Luxembourg', N'Grand Duchy of Luxembourg', N'Grand-Duché de Luxembourg', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (129, N'1', N'Latvia', N'Lettonie', N'Republic of Latvia', N'République de Lettonie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (130, N'1', N'Macau', N'Macao', N'Macau', N'Macao', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (131, N'1', N'Morocco', N'Maroc', N'Kingdom of Morocco', N'Royaume du Maroc', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (132, N'1', N'Monaco', N'Monaco', N'Principality of Monaco', N'Principauté de Monaco', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (133, N'1', N'Moldova', N'Moldavie', N'Republic of Moldova', N'République de Moldavie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (134, N'1', N'Madagascar', N'Madagascar', N'Republic of Madagascar', N'République de Madagascar', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (135, N'1', N'Maldives', N'Maldives', N'Republic of Maldives', N'République des Maldives', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (136, N'1', N'Mexico', N'Mexique', N'United Mexican States', N'États-Unis du Mexique', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (137, N'1', N'Marshall Islands', N'Îles Marshall', N'Republic of the Marshall Islands', N'République des Îles Marshall', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (138, N'1', N'Mali', N'Mali', N'Republic of Mali', N'République du Mali', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (139, N'1', N'Malta', N'Malte', N'Republic of Malta', N'République de Malte', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (140, N'1', N'Myanmar', N'Myanmar', N'Union of Myanmar', N'Union du Myanmar', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (141, N'1', N'Monlia', N'Monlie', N'Monlia', N'Monlie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (142, N'1', N'Commonwealth of the', N'Commonwealth des Îles', N'Commonwealth of the', N'Commonwealth des Îles', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (143, N'1', N'Mozambique', N'Mozambique', N'Republic of Mozambique', N'République du Mozambique', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (144, N'1', N'Department of Martinique', N'Martinique', N'Department of Martinique', N'Département de la Martinique', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (145, N'1', N'Islamic Mauritania', N'Mauritanie', N'Islamic Republic of Mauritania', N'République islamique de Mauritanie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (146, N'1', N'Montserrat', N'Montserrat', N'Montserrat', N'Montserrat', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (147, N'1', N'Mauritius', N'Île Maurice', N'Republic of Mauritius', N'République de lÎle Maurice', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (148, N'1', N'Malawi', N'Malawi', N'Republic of Malawi', N'République du Malawi', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (149, N'1', N'Malaysia', N'Malaisie', N'Malaysia', N'Malaisie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (150, N'1', N'Territorial collectivity of Mayotte', N'Mayotte', N'Territorial collectivity of Mayotte', N'Collectivité territoriale de Mayotte', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (151, N'1', N'Namibia', N'Namibi', N'Republic of Namibia', N'République de Namibi', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (152, N'1', N'New Caledonia', N'Nouvelle-Calédonie', N'New Caledonia', N'Nouvelle-Calédonie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (153, N'1', N'Niger', N'Niger', N'Republic of Niger', N'République du Niger', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (154, N'1', N'Norfolk Islands', N'Îles Norfolk', N'Norfolk Islands', N'Îles Norfolk', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (155, N'1', N'Nigeria', N'Nigéria', N'Federal Republic of Nigeria', N'République fédérale du Nigéria', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (156, N'1', N'Nicaragua', N'Nicaragua', N'Republic of Nicaragua', N'République du Nicaragua', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (157, N'1', N'Niue', N'Nioué', N'Niue', N'Nioué', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (158, N'1', N'Netherlands', N'Pays-Bas', N'Kingdom of the Netherlands', N'Royaume des Pays-Bas', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (159, N'1', N'Norway', N'Norvège', N'Kingdom of Norway', N'Royaume de Norvège', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (160, N'1', N'Nepal', N'Népal', N'Kingdom of Nepal', N'Royaume du Népal', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (161, N'1', N'Nauru', N'Nauru', N'Republic of Nauru', N'République de Nauru', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (162, N'1', N'New Zealand', N'Nouvelle-Zélande', N'New Zealand', N'Nouvelle-Zélande', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (163, N'1', N'Oman', N'Oman', N'Sultanate of Oman', N'Sultanat dOman', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (164, N'1', N'Pakistan', N'Pakistan', N'Islamic Republic of Pakistan', N'République islamique du Pakistan', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (165, N'1', N'Panama', N'Panama', N'Republic of Panama', N'République du Panama', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (166, N'1', N'Pitcairn', N'Pitcairn', N'Pitcairn', N'Pitcairn', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (167, N'1', N'Peru', N'Pérou', N'Republic of Peru', N'République du Pérou', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (168, N'1', N'Philippines', N'Philippines', N'Republic of Philippine', N'République des Philippines', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (169, N'1', N'Korea', N'Corée', N'Democratic Peoples of Korea', N'République populaire de Corée', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (170, N'1', N'Palau', N'Palau', N'Republic of Palau', N'République de Palau', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (171, N'1', N'Papua New Guinea', N'Papouasie-Nouvelle-Guinée', N'Papua New Guinea', N'Papouasie-Nouvelle-Guinée', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (172, N'1', N'Poland', N'Pologne', N'Republic of Poland', N'République de Pologne', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (173, N'1', N'Puerto Rico', N'Porto Rico', N'Puerto Rico', N'Porto Rico', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (174, N'1', N'Portugal', N'Portugal', N'Portuguese Republic', N'République portuguaise', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (175, N'1', N'Paraguay', N'Paraguay', N'Republic of Paraguay', N'République du Paraguay', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (176, N'1', N'French Polynesia', N'Polynésie française', N'French Polynesia', N'Polynésie française', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (177, N'1', N'Qatar', N'Qatar', N'State of Qatar', N'État du Qatar', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (178, N'1', N'Réunion', N'Réunion', N'Department of Réunion', N'Département de la Réunion', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (179, N'1', N'Romania', N'Roumanie', N'Romania', N'Roumanie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (180, N'1', N'Russia', N'Russie', N'Russian Federation', N'Fédération de Russie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (181, N'1', N'Rwanda', N'Rwanda', N'Rwanda Republic', N'République rwandaise', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (182, N'1', N'Saudi Arabia', N'Arabie saoudite', N'Kingdom of Saudi Arabia', N'Royaume dArabie saoudite', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (183, N'1', N'the Sudan', N'Soudan', N'Republic of the Sudan', N'République du Soudan', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (184, N'1', N'Senegal', N'Sénégal', N'Republic of Senegal', N'République du Sénégal', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (185, N'1', N'Singapor', N'Singapour', N'Republic of Singapor', N'République de Singapour', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (186, N'1', N'South Georgia and the South Sandwich Islands', N'Géorgie du Sud et les Îles Sandwich du Sud', N'South Georgia and the South Sandwich Islands', N'Géorgie du Sud et les Îles Sandwich du Sud', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (187, N'1', N'Svalbard and Jan Mayen Islands', N'Svalbard et Îles Jan Mayen', N'Svalbard and Jan Mayen Islands', N'Svalbard et Îles Jan Mayen', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (188, N'1', N'Solomon Islands', N'Îles Salomon', N'Solomon Islands', N'Îles Salomon', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (189, N'1', N'Sierra Leone', N'Sierra Leone', N'Republic of Sierra Leone', N'République de Sierra Leone', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (190, N'1', N'EL Salvador', N'El Salvador', N'Republic of EL Salvador', N'République du El Salvador', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (191, N'1', N'San Marino', N'Saint-Marin', N'Republic of San Marino', N'République de Saint-Marin', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (192, N'1', N'Saint Helena', N'Sainte-Hélène', N'Saint Helena', N'Sainte-Hélène', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (193, N'1', N'Somalia', N'Somalie', N'Somali Democratic Republic', N'République démocratique de Somalie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (194, N'1', N'Saint Pierre and Miquelon', N'Saint-Pierre-et-Miquelon', N'Territorial collectivity of Saint Pierre and Miquelon', N'Collectivité territoriale de Saint-Pierre-et-Miquelon', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (195, N'1', N'Sao Tome and Principe', N'Sao Tomé-et-Principe', N'Democratic Republic of Sao Tome and Principe', N'République démocratique de Sao Tomé-et-Principe', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (196, N'1', N'Suriname', N'Surinam', N'Republic of Suriname', N'République du Surinam', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (197, N'1', N'Slovakia Republic', N'République slovaque', N'Slovakia Republic', N'République slovaque', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (198, N'1', N'Slovenia', N'Slovénie', N'Republic of Slovenia', N'République de Slovénie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (199, N'1', N'Sweden', N'Suède', N'Kingdom of Sweden', N'Royaume de Suède', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (200, N'1', N'Seychelles', N'Îles Seychelles', N'Republic of Seychelles', N'République des Îles Seychelles', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (201, N'1', N'Syria', N'Syrie', N'Syrian Arab Republic', N'République syrienne', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (202, N'1', N'Swaziland', N'Swaziland', N'Kingdom of Swaziland', N'Royaume du Swaziland', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (203, N'1', N'Turks and Caicos Islands', N'Îles Turks et Caicos', N'Turks and Caicos Islands', N'Îles Turks et Caicos', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (204, N'1', N'Chad', N'Tchad', N'Republic of Chad', N'République du Tchad', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (205, N'1', N'To', N'To', N'Tolese Republic', N'République tolaise', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (206, N'1', N'Thailand', N'Thaïlande', N'Kingdom of Thailand', N'Royaume de Thaïlande', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (207, N'1', N'Tajikistan', N'Tadjikistan', N'Republic of Tajikistan', N'République du Tadjikistan', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (208, N'1', N'Tokelau', N'Tokélaou', N'Tokelau', N'Tokélaou', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (209, N'1', N'Turkmenistan', N'Turkménistan', N'Turkmenistan', N'Turkménistan', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (210, N'1', N'East Timor', N'Timor oriental', N'East Timor', N'Timor oriental', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (211, N'1', N'Tonga Islands', N'Îles Tonga', N'Kingdom of Tonga Islands', N'République des Îles Tonga', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (212, N'1', N'Trinidad', N'Trinité', N'Republic of Trinidad', N'République de la Trinité', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (213, N'1', N'Tunisia', N'Tunisie', N'Republic of Tunisia', N'République tunisienne', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (214, N'1', N'Turkey', N'Turque', N'Republic of Turkey', N'République turque', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (215, N'1', N'Tuvalu', N'Tuvalu', N'Tuvalu', N'Tuvalu', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (216, N'1', N'Taiwan', N'Taïwan', N'Taiwan Province of China', N'Taïwan Province de Chine', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (217, N'1', N'Tanzania', N'Tanzanie', N'Tanzania United Republic', N'République unie de Tanzanie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (218, N'1', N'Uganda', N'Ougada', N'Republic of Uganda', N'République de lOugada', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (219, N'1', N'Ukraine', N'Ukraine', N'Ukraine', N'Ukraine', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (220, N'1', N'United States Minor Outlying Islands', N'Îles mineures éloignées des États-Unis', N'United States Minor Outlying Islands', N'Îles mineures éloignées des États-Unis', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (221, N'1', N'Uruguay', N'Uruguay', N'Eastern Republic of Uruguay', N'République orientale de lUruguay', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (222, N'1', N'USA', N'USA', N'United States Of America', N'États-Unis dAmérique', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (223, N'1', N'Ukbekistan', N'Ouzbékistan', N'Republic of Ukbekistan', N'République dOuzbékistan', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (224, N'1', N'Vatican City', N'Cité du Vatican', N'Vatican City State', N'État de la Cité du Vatican', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (225, N'1', N'Saint Vincent and the Grenadines', N'Saint-Vincent-et-les-Grenadines', N'Saint Vincent and the Grenadines', N'Saint-Vincent-et-les-Grenadines', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (226, N'1', N'Venezuela', N'Venezuela', N'Republic of Venezuela', N'République du Venezuela', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (227, N'1', N'British Virgin Islands', N'Îles Vierges (Britanniques)', N'Virgin Islands (British)', N'Îles Vierges (Britanniques)', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (228, N'1', N'US Virgin Islands', N'Îles Vierges des États-Unis', N'Virgin Islands of the United States', N'Îles Vierges des États-Unis', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (229, N'1', N'Viet Nam', N'Viet Nam', N'Socialist Republic of Viet Nam', N'République socialiste du Viet Nam', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (230, N'1', N'Vanuatu', N'Vanuatu', N'Republic of Vanuatu', N'République de Vanuatu', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (231, N'1', N'Wallis and Futuna Islands', N'Îles Wallis et Futuna', N'Wallis and Futuna Islands', N'Îles Wallis et Futuna', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (232, N'1', N'Independent Western Samoa', N'État indépendant du Samoa occidental', N'Independent State of Western Samoa', N'État indépendant du Samoa occidental', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (233, N'1', N'Yemen', N'Yémen', N'Republic of Yemen', N'République du Yémen', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (234, N'1', N'Federal Yuslavia', N'Youslavie', N'Federal Republic of Yuslavia', N'République fédérative de Youslavie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (235, N'1', N'South Africa', N'République sud-africaine', N'Republic of South Africa', N'République sud-africaine', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (236, N'1', N'Zaire', N'Zaïre', N'Republic of Zaire', N'République du Zaïre', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (237, N'1', N'Zambia', N'Zambie', N'Republic of Zambia', N'République de Zambie', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO
INSERT [dbo].[CAT_REF_COUNTRY_VW]
    ([CNTRY_ID], [ACTIVE_FLG], [EABRV], [FABRV], [EDESC], [FDESC], [EFDT], [XDT])
VALUES
    (238, N'1', N'Zimbabwe', N'Zimbabwe', N'Republic of Zimbabwe', N'République du Zimbabwe', CAST(N'1993-05-31T00:00:00.0000000+00:00' AS DateTimeOffset), NULL)
GO

-- CAT_REF_DAY_OF_WEEK_VW
INSERT [dbo].[CAT_REF_DAY_OF_WEEK_VW] ([id], [day_of_week_id], [text], [language_id]) VALUES (1, 1, N'Sunday', 1)
INSERT [dbo].[CAT_REF_DAY_OF_WEEK_VW] ([id], [day_of_week_id], [text], [language_id]) VALUES (2, 1, N'Dimanche', 2)
INSERT [dbo].[CAT_REF_DAY_OF_WEEK_VW] ([id], [day_of_week_id], [text], [language_id]) VALUES (3, 2, N'Monday', 1)
INSERT [dbo].[CAT_REF_DAY_OF_WEEK_VW] ([id], [day_of_week_id], [text], [language_id]) VALUES (4, 2, N'Lundi', 2)
INSERT [dbo].[CAT_REF_DAY_OF_WEEK_VW] ([id], [day_of_week_id], [text], [language_id]) VALUES (5, 3, N'Tuesday', 1)
INSERT [dbo].[CAT_REF_DAY_OF_WEEK_VW] ([id], [day_of_week_id], [text], [language_id]) VALUES (6, 3, N'Mardi', 2)
INSERT [dbo].[CAT_REF_DAY_OF_WEEK_VW] ([id], [day_of_week_id], [text], [language_id]) VALUES (7, 4, N'Wednesday', 1)
INSERT [dbo].[CAT_REF_DAY_OF_WEEK_VW] ([id], [day_of_week_id], [text], [language_id]) VALUES (8, 4, N'Mercredi', 2)
INSERT [dbo].[CAT_REF_DAY_OF_WEEK_VW] ([id], [day_of_week_id], [text], [language_id]) VALUES (9, 5, N'Thursday', 1)
INSERT [dbo].[CAT_REF_DAY_OF_WEEK_VW] ([id], [day_of_week_id], [text], [language_id]) VALUES (10, 5, N'Jeudi', 2)
INSERT [dbo].[CAT_REF_DAY_OF_WEEK_VW] ([id], [day_of_week_id], [text], [language_id]) VALUES (11, 6, N'Friday', 1)
INSERT [dbo].[CAT_REF_DAY_OF_WEEK_VW] ([id], [day_of_week_id], [text], [language_id]) VALUES (12, 6, N'Vendredi', 2)
INSERT [dbo].[CAT_REF_DAY_OF_WEEK_VW] ([id], [day_of_week_id], [text], [language_id]) VALUES (13, 7, N'Saturday', 1)
INSERT [dbo].[CAT_REF_DAY_OF_WEEK_VW] ([id], [day_of_week_id], [text], [language_id]) VALUES (14, 7, N'Samedi', 2)

GO

-- CAT_REF_LANGUAGE_VW
INSERT [dbo].[CAT_REF_LANGUAGE_VW] ([language_id], [iso_code_1], [iso_code_2], [edesc], [fdesc], [date_from], [date_to]) VALUES (1, N'en', N'en-ca', N'English', N'Anglais', CAST(N'2000-01-01T05:00:00.0000000+00:00' AS DateTimeOffset), NULL)
INSERT [dbo].[CAT_REF_LANGUAGE_VW] ([language_id], [iso_code_1], [iso_code_2], [edesc], [fdesc], [date_from], [date_to]) VALUES (2, N'fr', N'fr-ca', N'French', N'Français', CAST(N'2000-01-01T05:00:00.0000000+00:00' AS DateTimeOffset), NULL)

GO

-- USER ACCOMMODATION FILE STATUS
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilestatus] ON
INSERT [dbo].[custom_models_useraccommodationfilestatus] ([id], [codename], [active], [order]) VALUES (1, N'new', 1, 1)
INSERT [dbo].[custom_models_useraccommodationfilestatus] ([id], [codename], [active], [order]) VALUES (2, N'in_progress', 1, 6)
INSERT [dbo].[custom_models_useraccommodationfilestatus] ([id], [codename], [active], [order]) VALUES (3, N'on_hold', 1, 7)
INSERT [dbo].[custom_models_useraccommodationfilestatus] ([id], [codename], [active], [order]) VALUES (4, N'completed', 1, 11)
INSERT [dbo].[custom_models_useraccommodationfilestatus] ([id], [codename], [active], [order]) VALUES (5, N'cancelled', 1, 12)
INSERT [dbo].[custom_models_useraccommodationfilestatus] ([id], [codename], [active], [order]) VALUES (18, N'estimating_service_cost', 1, 2)
INSERT [dbo].[custom_models_useraccommodationfilestatus] ([id], [codename], [active], [order]) VALUES (19, N'waiting_for_financial_coding', 1, 3)
INSERT [dbo].[custom_models_useraccommodationfilestatus] ([id], [codename], [active], [order]) VALUES (20, N'waiting_for_documentation', 1, 4)
INSERT [dbo].[custom_models_useraccommodationfilestatus] ([id], [codename], [active], [order]) VALUES (21, N'ready', 1, 5)
INSERT [dbo].[custom_models_useraccommodationfilestatus] ([id], [codename], [active], [order]) VALUES (22, N're_opened', 1, 8)
INSERT [dbo].[custom_models_useraccommodationfilestatus] ([id], [codename], [active], [order]) VALUES (23, N're_opened_hold', 1, 9)
INSERT [dbo].[custom_models_useraccommodationfilestatus] ([id], [codename], [active], [order]) VALUES (24, N'cancelled_by_candidate', 1, 13)
INSERT [dbo].[custom_models_useraccommodationfilestatus] ([id], [codename], [active], [order]) VALUES (25, N'pending_approval', 1, 10)
INSERT [dbo].[custom_models_useraccommodationfilestatus] ([id], [codename], [active], [order]) VALUES (26, N'expired', 1, 0)
INSERT [dbo].[custom_models_useraccommodationfilestatus] ([id], [codename], [active], [order]) VALUES (27, N'administered', 1, 14)
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilestatus] OFF

GO

-- USER ACCOMMODATION FILE STATUS TEXT
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilestatustext] ON
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (1, N'NEW', 1, 1)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (2, N'NOUVELLE', 2, 1)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (3, N'IN PROGRESS', 1, 2)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (4, N'EN COURS', 2, 2)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (5, N'ON HOLD', 1, 3)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (6, N'EN ATTENTE', 2, 3)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (7, N'COMPLETED', 1, 4)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (8, N'COMPLÉTÉE', 2, 4)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (9, N'CANCELLED BY SPECIALIST', 1, 5)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (10, N'ANNULÉE PAR LE SPÉCIALISTE', 2, 5)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (35, N'ESTIMATING SERVICE COST', 1, 18)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (36, N'ESTIMATION DU COÛT DU SERVICE', 2, 18)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (37, N'WAITING FOR FINANCIAL CODING', 1, 19)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (38, N'EN ATTENTE DES CODES FINANCIERS', 2, 19)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (39, N'WAITING FOR DOCUMENTATION', 1, 20)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (40, N'EN ATTENTE DE DOCUMENTATION', 2, 20)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (41, N'READY', 1, 21)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (42, N'PRÊTE', 2, 21)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (43, N'RE-OPENED', 1, 22)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (44, N'RÉOUVERTE', 2, 22)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (45, N'RE-OPENED HOLD', 1, 23)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (46, N'RÉOUVERTE ET EN ATTENTE', 2, 23)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (47, N'CANCELLED BY CANDIDATE', 1, 24)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (48, N'ANNULÉE PAR LE CANDIDAT', 2, 24)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (49, N'PENDING CANDIDATE AGREEMENT', 1, 25)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (50, N'EN ATTENTE DE L''ACCORD DU CANDIDAT', 2, 25)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (51, N'EXPIRED', 1, 26)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (52, N'EXPIRÉE', 2, 26)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (53, N'ADMINISTERED', 1, 27)
INSERT [dbo].[custom_models_useraccommodationfilestatustext] ([id], [text], [language_id], [user_accommodation_file_status_id]) VALUES (54, N'ADMINISTRÉE', 2, 27)
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilestatustext] OFF

GO

-- USER ACCOMMODATION FILE CANDIDATE LIMITATIONS OPTION
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption] ON
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption]
    ([id], [codename], [active])
VALUES
    (1, N'attention_disorders', 1)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption]
    ([id], [codename], [active])
VALUES
    (2, N'chronic_medical_disabilities', 1)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption]
    ([id], [codename], [active])
VALUES
    (3, N'hearing_disabilities', 1)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption]
    ([id], [codename], [active])
VALUES
    (4, N'learning_disabilities', 1)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption]
    ([id], [codename], [active])
VALUES
    (5, N'mobility_physical_disabilities', 1)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption]
    ([id], [codename], [active])
VALUES
    (6, N'neurological_disorders', 1)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption]
    ([id], [codename], [active])
VALUES
    (7, N'psychiatric_psychological_emotional_disabilities', 1)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption]
    ([id], [codename], [active])
VALUES
    (8, N'speech_and_language_disabilities', 1)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption]
    ([id], [codename], [active])
VALUES
    (9, N'temporary_conditions_and_other', 1)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption]
    ([id], [codename], [active])
VALUES
    (10, N'visual_condition', 1)
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption] OFF

GO

-- USER ACCOMMODATION FILE CANDIDATE LIMITATIONS OPTION TEXT
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext] ON
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (1, N'Attention Disorders', 1, 1)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (2, N'Troubles de l''attention', 2, 1)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (3, N'Chronic/Medical Disabilities', 1, 2)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (4, N'Maladies chroniques et troubles médicaux', 2, 2)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (5, N'Hearing Disabilities', 1, 3)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (6, N'Handicap auditif', 2, 3)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (7, N'Learning Disabilities', 1, 4)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (8, N'Trouble d''apprentissage', 2, 4)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (9, N'Mobility/Physical Disabilities', 1, 5)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (10, N'Déficience physique ou de mobilité', 2, 5)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (11, N'Neurological Disorders', 1, 6)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (12, N'Troubles neurologiques', 2, 6)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (13, N'Psychiatric/Psychological/Emotional Disabilities', 1, 7)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (14, N'Handicap psychiatrique/psychologique/émotionnel', 2, 7)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (15, N'Speech and Language Disabilities', 1, 8)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (16, N'Troubles phonologiques et troubles du langage', 2, 8)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (17, N'Temporary Conditions and Other', 1, 9)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (18, N'Conditions temporaires et autres', 2, 9)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (19, N'Visual Condition', 1, 10)
INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]
    ([id], [text], [language_id], [user_accommodation_file_candidate_limitations_option_id])
VALUES
    (20, N'Condition visuelle', 2, 10)
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext] OFF

GO

-- USER ACCOMMODATION FILE RECOMMENDATIONS OPTION
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] ON
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id], [codename], [active]) VALUES (1, N'adapted_scoring', 0)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id], [codename], [active]) VALUES (2, N'adaptive_technology', 1)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id], [codename], [active]) VALUES (3, N'assessor_or_test_administrator', 1)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id], [codename], [active]) VALUES (4, N'communication_support', 1)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id], [codename], [active]) VALUES (5, N'environment', 1)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id], [codename], [active]) VALUES (6, N'extra_time', 1)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id], [codename], [active]) VALUES (7, N'formats', 1)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id], [codename], [active]) VALUES (8, N'individual_test_session', 0)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id], [codename], [active]) VALUES (9, N'material', 1)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id], [codename], [active]) VALUES (10, N'preparation_to_test', 1)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id], [codename], [active]) VALUES (11, N'scheduling', 1)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id], [codename], [active]) VALUES (12, N'sle_uit_leveling', 1)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id], [codename], [active]) VALUES (13, N'breaks', 1)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id], [codename], [active]) VALUES (14, N'repetition', 1)
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoption] OFF

GO

-- USER ACCOMMODATION FILE RECOMMENDATIONS OPTION TEXT
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ON
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (1, N'Adapted Scoring', 1, 1)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (2, N'Correction adaptée', 2, 1)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (3, N'Adaptive Technology', 1, 2)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (4, N'Technologie adaptée', 2, 2)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (5, N'Assessor or Test Administrator', 1, 3)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (6, N'Évaluateur ou administrateur de test', 2, 3)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (7, N'Communication Support', 1, 4)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (8, N'Soutien à la communication', 2, 4)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (9, N'Environment', 1, 5)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (10, N'Environnement', 2, 5)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (11, N'Extra Time', 1, 6)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (12, N'Temps additionnel', 2, 6)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (13, N'Formats', 1, 7)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (14, N'Formats', 2, 7)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (15, N'Individual Test Session', 1, 8)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (16, N'Séance d''examen individuelle', 2, 8)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (17, N'Material', 1, 9)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (18, N'Matériel', 2, 9)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (19, N'Preparation to Test', 1, 10)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (20, N'Préparation au test', 2, 10)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (21, N'Scheduling', 1, 11)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (22, N'Horaire', 2, 11)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (23, N'SLE-UIT Leveling', 1, 12)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (24, N'ÉLS-TELNS mise à niveau', 2, 12)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (25, N'Breaks', 1, 13)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (26, N'Pauses', 2, 13)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (27, N'Repetition', 1, 14)
INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] ([id], [text], [language_id], [user_accommodation_file_recommendations_option_id]) VALUES (28, N'Répétition', 2, 14)
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] OFF

GO

-- USER ACCOMMODATION FILE COMPLEXITY
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilecomplexity] ON
INSERT [dbo].[custom_models_useraccommodationfilecomplexity]
    ([id], [codename], [active], [order])
VALUES
    (1, N'low', 1, 1)
INSERT [dbo].[custom_models_useraccommodationfilecomplexity]
    ([id], [codename], [active], [order])
VALUES
    (2, N'medium', 1, 2)
INSERT [dbo].[custom_models_useraccommodationfilecomplexity]
    ([id], [codename], [active], [order])
VALUES
    (3, N'high', 1, 3)
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilecomplexity] OFF

GO

-- USER ACCOMMODATION FILE COMPLEXITY TEXT
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilecomplexitytext] ON
INSERT [dbo].[custom_models_useraccommodationfilecomplexitytext]
    ([id], [text], [language_id], [user_accommodation_file_complexity_id])
VALUES
    (1, N'Low', 1, 1)
INSERT [dbo].[custom_models_useraccommodationfilecomplexitytext]
    ([id], [text], [language_id], [user_accommodation_file_complexity_id])
VALUES
    (2, N'Basse', 2, 1)
INSERT [dbo].[custom_models_useraccommodationfilecomplexitytext]
    ([id], [text], [language_id], [user_accommodation_file_complexity_id])
VALUES
    (3, N'Medium', 1, 2)
INSERT [dbo].[custom_models_useraccommodationfilecomplexitytext]
    ([id], [text], [language_id], [user_accommodation_file_complexity_id])
VALUES
    (4, N'Moyenne', 2, 2)
INSERT [dbo].[custom_models_useraccommodationfilecomplexitytext]
    ([id], [text], [language_id], [user_accommodation_file_complexity_id])
VALUES
    (5, N'High', 1, 3)
INSERT [dbo].[custom_models_useraccommodationfilecomplexitytext]
    ([id], [text], [language_id], [user_accommodation_file_complexity_id])
VALUES
    (6, N'Élevée', 2, 3)
SET IDENTITY_INSERT [dbo].[custom_models_useraccommodationfilecomplexitytext] OFF

GO

-- REASON FOR TESTING TYPE
SET IDENTITY_INSERT [dbo].[custom_models_reasonfortestingtype] ON
INSERT [dbo].[custom_models_reasonfortestingtype]
    ([id], [codename])
VALUES
    (3, N'oral')
INSERT [dbo].[custom_models_reasonfortestingtype]
    ([id], [codename])
VALUES
    (1, N'reading')
INSERT [dbo].[custom_models_reasonfortestingtype]
    ([id], [codename])
VALUES
    (2, N'writing')
SET IDENTITY_INSERT [dbo].[custom_models_reasonfortestingtype] OFF

GO

-- REASON FOR TESTING PRIORITY
SET IDENTITY_INSERT [dbo].[custom_models_reasonfortestingpriority] ON
INSERT [dbo].[custom_models_reasonfortestingpriority]
    ([id], [codename])
VALUES
    (1, N'high')
INSERT [dbo].[custom_models_reasonfortestingpriority]
    ([id], [codename])
VALUES
    (3, N'low')
INSERT [dbo].[custom_models_reasonfortestingpriority]
    ([id], [codename])
VALUES
    (2, N'medium')
SET IDENTITY_INSERT [dbo].[custom_models_reasonfortestingpriority] OFF

GO

-- REASON FOR TESTING PRIORITY TEXT
SET IDENTITY_INSERT [dbo].[custom_models_reasonfortestingprioritytext] ON
INSERT [dbo].[custom_models_reasonfortestingprioritytext]
    ([id], [text], [language_id], [reason_for_testing_priority_id])
VALUES
    (1, N'High', 1, 1)
INSERT [dbo].[custom_models_reasonfortestingprioritytext]
    ([id], [text], [language_id], [reason_for_testing_priority_id])
VALUES
    (2, N'FR High', 2, 1)
INSERT [dbo].[custom_models_reasonfortestingprioritytext]
    ([id], [text], [language_id], [reason_for_testing_priority_id])
VALUES
    (3, N'Medium', 1, 2)
INSERT [dbo].[custom_models_reasonfortestingprioritytext]
    ([id], [text], [language_id], [reason_for_testing_priority_id])
VALUES
    (4, N'FR Medium', 2, 2)
INSERT [dbo].[custom_models_reasonfortestingprioritytext]
    ([id], [text], [language_id], [reason_for_testing_priority_id])
VALUES
    (5, N'Low', 1, 3)
INSERT [dbo].[custom_models_reasonfortestingprioritytext]
    ([id], [text], [language_id], [reason_for_testing_priority_id])
VALUES
    (6, N'FR Low', 2, 3)
SET IDENTITY_INSERT [dbo].[custom_models_reasonfortestingprioritytext] OFF

GO

-- REASON FOR TESTING
SET IDENTITY_INSERT [dbo].[custom_models_reasonfortesting] ON
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (1, N'imp_staffing', NULL, 0, 1, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (2, N'non_imp_staffing', NULL, 0, 1, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (3, N'ft_language_training_from_psc', NULL, 0, 1, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (4, N'pt_language_training_from_psc', NULL, 0, 1, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (5, N'ft_language_training_from_dep_prog', NULL, 0, 1, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (6, N'pt_language_training_from_dep_prog', NULL, 0, 1, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (7, N'ft_language_training_from_private', NULL, 0, 1, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (8, N'pt_language_training_from_private', NULL, 0, 1, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (9, N're_identification', NULL, 0, 1, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (10, N'bilingualism_bonus', NULL, 0, 1, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (11, N'record_or_other_purposes', NULL, 0, 1, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (12, N'covid_19_or_essential_service', NULL, 0, 1, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (13, N'staffing_dotation', NULL, 1, 1, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (14, N'language_training', NULL, 1, 1, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (15, N'other', NULL, 1, 1, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (16, N'imp_staffing', NULL, 0, 2, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (17, N'non_imp_staffing', NULL, 0, 2, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (18, N'ft_language_training_from_psc', NULL, 0, 2, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (19, N'pt_language_training_from_psc', NULL, 0, 2, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (20, N'ft_language_training_from_dep_prog', NULL, 0, 2, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (21, N'pt_language_training_from_dep_prog', NULL, 0, 2, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (22, N'ft_language_training_from_private', NULL, 0, 2, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (23, N'pt_language_training_from_private', NULL, 0, 2, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (24, N're_identification', NULL, 0, 2, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (25, N'bilingualism_bonus', NULL, 0, 2, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (26, N'record_or_other_purposes', NULL, 0, 2, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (27, N'covid_19_or_essential_service', NULL, 0, 2, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (28, N'staffing_dotation', NULL, 1, 2, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (29, N'language_training', NULL, 1, 2, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (30, N'other', NULL, 1, 2, NULL, NULL)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (31, N'imp_staffing_ola', 15, 1, 3, 0, 1)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (32, N'language_training_ola', 30, 1, 3, 15, 2)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (33, N'creation_of_partially_assessed_pool', 60, 1, 3, 30, 3)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (34, N'non_imp_staffing_ola', 60, 1, 3, 30, 3)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (35, N're_identification_ola', 60, 1, 3, 30, 3)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (36, N'record_or_other_purposes_ola', 60, 1, 3, 30, 3)
INSERT [dbo].[custom_models_reasonfortesting]
    ([id], [codename], [minimum_process_length], [active], [reason_for_testing_type_id], [waiting_period], [reason_for_testing_priority_id])
VALUES
    (37, N'bilingualism_bonus_ola', 60, 1, 3, 30, 3)
SET IDENTITY_INSERT [dbo].[custom_models_reasonfortesting] OFF

GO

-- REASON FOR TESTING TEXT
SET IDENTITY_INSERT [dbo].[custom_models_reasonfortestingtext] ON
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (1, N'Imperative staffing', 1, 1)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (2, N'Dotation impérative', 2, 1)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (3, N'Non-imperative staffing', 1, 2)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (4, N'Dotation non impérative', 2, 2)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (5, N'Full-time language training from the Canada School of Public Service', 1, 3)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (6, N'Formation linguistique à temps plein offerte par l''École de la fonction publique du Canada', 2, 3)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (7, N'Part-time Language training from the Canada School of Public Service', 1, 4)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (8, N'Formation linguistique à temps partiel dispensée par l''École de la fonction publique du Canada', 2, 4)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (9, N'Full-time language training from departmental program', 1, 5)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (10, N'Formation linguistique à temps plein offerte par les ministères', 2, 5)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (11, N'Part-time language training from departmental program', 1, 6)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (12, N'Formation linguistique à temps partiel offerte par les ministères', 2, 6)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (13, N'Full-time language training from private language school', 1, 7)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (14, N'Form. ling. à temps plein disp. par un établissement ling.', 2, 7)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (15, N'Part-time language training from private language school', 1, 8)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (16, N'Form. ling. à temps part. disp. par un établissement ling.', 2, 8)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (17, N'Re-identification', 1, 9)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (18, N'Réidentification', 2, 9)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (19, N'Bilingualism bonus', 1, 10)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (20, N'Prime au bilinguisme', 2, 10)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (21, N'Record or other purposes', 1, 11)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (22, N'Fins documentaires ou autres', 2, 11)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (23, N'COVID-19 or Essential Service', 1, 12)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (24, N'COVID-19 ou Service Essential', 2, 12)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (25, N'Staffing / Dotation', 1, 13)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (26, N'Dotation / Staffing', 2, 13)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (27, N'Language Training / Formation linguistique', 1, 14)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (28, N'Formation linguistique / Language Training', 2, 14)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (29, N'Other / Autre', 1, 15)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (30, N'Autre / Other', 2, 15)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (31, N'Imperative staffing', 1, 16)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (32, N'Dotation impérative', 2, 16)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (33, N'Non-imperative staffing', 1, 17)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (34, N'Dotation non impérative', 2, 17)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (35, N'Full-time language training from the Canada School of Public Service', 1, 18)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (36, N'Formation linguistique à temps plein offerte par l''École de la fonction publique du Canada', 2, 18)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (37, N'Part-time Language training from the Canada School of Public Service', 1, 19)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (38, N'Formation linguistique à temps partiel dispensée par l''École de la fonction publique du Canada', 2, 19)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (39, N'Full-time language training from departmental program', 1, 20)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (40, N'Formation linguistique à temps plein offerte par les ministères', 2, 20)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (41, N'Part-time language training from departmental program', 1, 21)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (42, N'Formation linguistique à temps partiel offerte par les ministères', 2, 21)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (43, N'Full-time language training from private language school', 1, 22)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (44, N'Form. ling. à temps plein disp. par un établissement ling.', 2, 22)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (45, N'Part-time language training from private language school', 1, 23)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (46, N'Form. ling. à temps part. disp. par un établissement ling.', 2, 23)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (47, N'Re-identification', 1, 24)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (48, N'Réidentification', 2, 24)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (49, N'Bilingualism bonus', 1, 25)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (50, N'Prime au bilinguisme', 2, 25)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (51, N'Record or other purposes', 1, 26)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (52, N'Fins documentaires ou autres', 2, 26)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (53, N'COVID-19 or Essential Service', 1, 27)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (54, N'COVID-19 ou Service Essential', 2, 27)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (55, N'Staffing / Dotation', 1, 28)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (56, N'Dotation / Staffing', 2, 28)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (57, N'Language Training / Formation linguistique', 1, 29)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (58, N'Formation linguistique / Language Training', 2, 29)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (59, N'Other / Autre', 1, 30)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (60, N'Autre / Other', 2, 30)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (61, N'Imperative staffing: Impending appointment', 1, 31)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (62, N'FR Imperative staffing: Impending appointment', 2, 31)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (63, N'Language training', 1, 32)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (64, N'FR Language training', 2, 32)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (65, N'Creation of Partially Assessed Pool', 1, 33)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (66, N'FR Creation of Partially Assessed Pool', 2, 33)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (67, N'Non-imperative staffing', 1, 34)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (68, N'FR Non-imperative staffing', 2, 34)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (69, N'Re-identification', 1, 35)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (70, N'FR Re-identification', 2, 35)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (71, N'Record or other purposes', 1, 36)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (72, N'FR Record or other purposes', 2, 36)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (73, N'Bilingualism bonus', 1, 37)
INSERT [dbo].[custom_models_reasonfortestingtext]
    ([id], [text], [language_id], [reason_for_testing_id])
VALUES
    (74, N'FR Bilingualism bonus', 2, 37)
SET IDENTITY_INSERT [dbo].[custom_models_reasonfortestingtext] OFF

GO

-- CTAB AREA RESIDENCES
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (-1, N'0', N'Unknown', N'Inconnu', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 100)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (5, N'1', N'Newfoundland', N'Terre-Neuve', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 5)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (6, N'1', N'Prince-Edward-Island', N'l''Île du Prince Édouard', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 6)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (7, N'1', N'Nova Scotia', N'Nouvelle-Écosse', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 7)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (9, N'1', N'New Brunswick', N'Nouveau-Brunswick', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 9)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (10, N'1', N'Québec, NCR', N'Québec, RCN', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 10)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (11, N'1', N'Ontario, NCR', N'Ontario, RCN', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 11)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (12, N'1', N'Montréal, Québec', N'Montréal, Québec', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 12)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (13, N'1', N'Québec City, Québec', N'La ville de Québec, Québec', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 13)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (14, N'1', N'Other, Québec', N'Autre, Québec', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 14)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (15, N'1', N'Toronto, Ontario', N'Toronto, Ontario', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 15)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (16, N'1', N'Northern Ontario', N'le nord de l''Ontario', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 16)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (17, N'1', N'Other, Ontario', N'Autre, Ontario', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 17)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (18, N'1', N'Manitoba', N'Manitoba', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 18)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (19, N'1', N'Saskatchewan', N'Saskatchewan', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 19)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (25, N'1', N'Alberta', N'Alberta', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 25)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (26, N'1', N'British Columbia', N'Colombie-Britannique', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 26)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (27, N'1', N'Yukon District', N'Distrique du Yukon', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 27)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (28, N'1', N'Northwest Territories', N'Territoires du nord ouest', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 28)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (29, N'1', N'Abroad', N'Outre-mer', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 29)
INSERT [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
    ([AREA_RES_ID], [ACTIVE_FLG], [EDESC], [FDESC], [EFDT], [XDT], [PRESENTATION_ORDER])
VALUES
    (30, N'1', N'Nunavut', N'Nunavut', CAST(N'2024-10-01T00:00:00.0000000+00:00' AS DateTimeOffset), NULL, 30)
GO


-- SCORER OLA TEST SESSION SKIP OPTION
SET IDENTITY_INSERT [dbo].[custom_models_scorerolatestsessionskipoption] ON
INSERT [dbo].[custom_models_scorerolatestsessionskipoption]
    ([id], [codename], [active])
VALUES
    (1, N'conflict_family', 1)
INSERT [dbo].[custom_models_scorerolatestsessionskipoption]
    ([id], [codename], [active])
VALUES
    (2, N'tested_recently', 1)
SET IDENTITY_INSERT [dbo].[custom_models_scorerolatestsessionskipoption] OFF
GO

-- SCORER OLA TEST SESSION SKIP OPTION TEXT
GO
SET IDENTITY_INSERT [dbo].[custom_models_scorerolatestsessionskipoptiontext] ON
INSERT [dbo].[custom_models_scorerolatestsessionskipoptiontext]
    ([id], [text], [language_id], [scorer_ola_test_session_skip_option_id])
VALUES
    (1, N'Conflict of interest or acquaintance', 1, 1)
INSERT [dbo].[custom_models_scorerolatestsessionskipoptiontext]
    ([id], [text], [language_id], [scorer_ola_test_session_skip_option_id])
VALUES
    (2, N'Conflit d’intérêt ou connaissance', 2, 1)
INSERT [dbo].[custom_models_scorerolatestsessionskipoptiontext]
    ([id], [text], [language_id], [scorer_ola_test_session_skip_option_id])
VALUES
    (3, N'Tested this candidate recently', 1, 2)
INSERT [dbo].[custom_models_scorerolatestsessionskipoptiontext]
    ([id], [text], [language_id], [scorer_ola_test_session_skip_option_id])
VALUES
    (4, N'Déjà testé ce candidat récemment', 2, 2)
SET IDENTITY_INSERT [dbo].[custom_models_scorerolatestsessionskipoptiontext] OFF
GO

-- OLA GLOBAL CONFIG DEFAULT OPTIONS
GO
SET IDENTITY_INSERT [dbo].[custom_models_olaglobalconfigs] ON
GO
INSERT [dbo].[custom_models_olaglobalconfigs]
    ([id], [codename], [value])
VALUES
    (1, N'cancellation_window', 96)
INSERT [dbo].[custom_models_olaglobalconfigs]
    ([id], [codename], [value])
VALUES
    (2, N'open_booking_window', 96)
GO
SET IDENTITY_INSERT [dbo].[custom_models_olaglobalconfigs] OFF
GO
