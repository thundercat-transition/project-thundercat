USE [master]
GO
/****** Object:  UserDefinedFunction [dbo].[get_ola_available_time_slots_fn]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[get_ola_available_time_slots_fn] ()
                RETURNS @ola_available_time_slots table(id int Primary Key IDENTITY(1,1), date date, start_time time, end_time time, day_of_week_id int, assessed_language_id int, available_test_assessors_total int)
                AS
                BEGIN
                    -- DECLARING ALL NEEDED TABLES/VARIABLES
                    -- TABLES
                    DECLARE @test_center_availability table(id int Primary Key IDENTITY(1,1), test_center_id int, nbr_of_days int)
                    -- VARIABLES
                    DECLARE @i int
                    DECLARE @test_center_availability_id int
                    DECLARE @nbr_of_days int
                    DECLARE @current_date date
                    DECLARE @test_center_id int

                    -- CREATING @test_center_availability TABLE
                    INSERT INTO @test_center_availability 
                    SELECT tcoc.test_center_id, tcoc.advanced_booking_delay 
                    FROM master..custom_models_testcenterolaconfigs tcoc

                    -- INFINITE LOOP
                    WHILE 1 = 1
                    BEGIN
                        -- SETTING NEEDED VARIABLES
                        SET @i = 0
                        SET @current_date = GETDATE()
                        SET @test_center_availability_id = NULL
                    
                        -- SETTING @test_center_availability_id, @test_center_id AND @nbr_of_days BASED ON @test_center_availability TABLE
                        SELECT TOP 1 @test_center_availability_id = id, @test_center_id = test_center_id, @nbr_of_days = nbr_of_days 
                        FROM @test_center_availability 
                        ORDER BY id

                        -- NO MORE DATA
                        IF @test_center_availability_id IS NULL
                            -- BREAKING THE LOOP
                            BREAK

                        -- LOOPING FROM 0 TO PROVIDED NUMBER OF DAYS (TEST CENTER OLA CONFIGS - ADVANCED BOOKING DELAY)
                        WHILE @i < @nbr_of_days
                        BEGIN
                            -- POPULATING @ola_available_time_slots TABLE
                            INSERT INTO @ola_available_time_slots
                            SELECT 
                                @current_date,
                                start_time,
                                end_time,
                                day_of_week_id,
                                assessed_language_id,
								(SELECT available_test_assessors FROM master..handle_vacation_block_availability_by_date_fn(@test_center_id, @current_date, assessed_language_id, nbr_of_available_assessors)) as 'available_test_assessors_total'
                            FROM master..get_test_assessor_availabilities_by_date_fn(@current_date, @test_center_id)

                            -- ADDING 1 TO @i VARIABLE
                            SET @i = @i + 1
                            -- ADDING 1 DAY TO @current_date VARIABLE
                            SET @current_date = DATEADD(DAY, 1, @current_date)
                        END

                        -- DELETING RESPECTIVE ROW FROM @test_center_availability TABLE
                        DELETE FROM @test_center_availability WHERE id = @test_center_availability_id
                    END
                    RETURN
                END
GO
/****** Object:  UserDefinedFunction [dbo].[status2str]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[status2str] 
                ( 
                    @lang_code varchar(2),
                    @status_id numeric
                )
                RETURNS varchar(100)
                AS 
                BEGIN
                    DECLARE @Result		varchar(100)
                    IF (@lang_code = 'fr')
                        BEGIN
                            SELECT @Result	= CASE 
                            WHEN @status_id = 1 THEN 'ATTRIBUÉ'
                            WHEN @status_id = 2 THEN 'ENREGISTRÉ'
                            WHEN @status_id = 3 THEN 'NON ATTRIBUÉ'
                            WHEN @status_id = 4 THEN 'PRÉ_TEST'
                            WHEN @status_id = 5 THEN 'ACTIF'
                            WHEN @status_id = 6 THEN 'TRANSITION'
                            WHEN @status_id = 7 THEN 'VERROUILLÉ'
                            WHEN @status_id = 8 THEN 'EN PAUSE'
                            WHEN @status_id = 9 THEN 'SOUMIS'
                            WHEN @status_id = 10 THEN 'ABANDONNÉ'
                            ELSE CAST(@status_id AS varchar(4))
                            END
                        END
                    ELSE
                        BEGIN
                            SELECT @Result	= CASE 
                            WHEN @status_id = 1 THEN 'ASSIGNED'
                            WHEN @status_id = 2 THEN 'CHECKED_IN'
                            WHEN @status_id = 3 THEN 'UNASSIGNED'
                            WHEN @status_id = 4 THEN 'PRE_TEST'
                            WHEN @status_id = 5 THEN 'ACTIVE'
                            WHEN @status_id = 6 THEN 'TRANSITION'
                            WHEN @status_id = 7 THEN 'LOCKED'
                            WHEN @status_id = 8 THEN 'PAUSED'
                            WHEN @status_id = 9 THEN 'SUBMITTED'
                            WHEN @status_id = 10 THEN 'QUIT'

                            ELSE CAST(@status_id AS varchar(4))
                            END
                        END
                    RETURN @Result
                END
GO
/****** Object:  Table [dbo].[cms_models_bundlerule]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundlerule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[bundle_id] [int] NOT NULL,
	[rule_type_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_bundlebundlesruleassociations]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundlebundlesruleassociations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[display_order] [int] NOT NULL,
	[bundle_id] [int] NOT NULL,
	[bundle_bundles_rule_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_bundlebundlesrule]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundlebundlesrule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[number_of_bundles] [int] NULL,
	[shuffle_bundles] [bit] NOT NULL,
	[keep_items_together] [bit] NOT NULL,
	[bundle_rule_id] [int] NOT NULL,
	[display_order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_bundles]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[version_text] [nvarchar](50) NULL,
	[shuffle_items] [bit] NOT NULL,
	[shuffle_bundles] [bit] NOT NULL,
	[active] [bit] NOT NULL,
	[item_bank_id] [int] NOT NULL,
	[shuffle_between_bundles] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [bundle_name_must_be_unique_in_item_bank] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[item_bank_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_bundleitemsassociation]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundleitemsassociation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[system_id_link] [nvarchar](15) NOT NULL,
	[version] [int] NULL,
	[display_order] [int] NOT NULL,
	[bundle_source_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_bundlebundlesassociation]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundlebundlesassociation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[display_order] [int] NOT NULL,
	[bundle_link_id] [int] NOT NULL,
	[bundle_source_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[bundle_data_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[bundle_data_vw] AS
                SELECT * FROM
                    (SELECT
                        b.id,
                        b.name,
                        b.version_text,
                        b.shuffle_items,
                        b.shuffle_bundles,
                        b.shuffle_between_bundles,
                        b.active,
                        b.item_bank_id,
                        bundle_items_data.item_count
                    FROM master..cms_models_bundles b
                    OUTER APPLY (SELECT count(id) as 'item_count' 
                                FROM master..cms_models_bundleitemsassociation bia 
                                WHERE bia.bundle_source_id= b.id
                                ) bundle_items_data
                    ) a

                    OUTER APPLY (SELECT TOP 1 * FROM (
                                                SELECT count(bbra.id) as 'bundle_count' 
                                                    FROM master..cms_models_bundlebundlesruleassociations bbra 
                                                    JOIN master..cms_models_bundlerule br on br.bundle_id = a.id
                                                    JOIN master..cms_models_bundlebundlesrule bbr on bbr.bundle_rule_id = br.id
                                                    WHERE bbra.bundle_bundles_rule_id = bbr.id
                                                UNION 
                                                SELECT count(bundle_source_id) as 'bundle_count' 
                                                    FROM master..cms_models_bundlebundlesassociation bba 
                                                    WHERE bba.bundle_source_id= a.id
                                                ) bundle_rule_data
                                ORDER BY bundle_count DESC
                                ) bundle_count
        
GO
/****** Object:  Table [dbo].[cms_models_bundleruletype]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundleruletype](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[bundle_bundles_rule_data_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[bundle_bundles_rule_data_vw] AS
                SELECT 
                    br.id,
                    br.bundle_id,
                    bbr.id as 'bundle_bundles_rule_id',
                    bbr.number_of_bundles,
                    bbr.shuffle_bundles,
                    bbr.keep_items_together,
                    bbr.display_order as 'bundle_bundles_rule_display_order',
                    bbra.id as 'bundle_bundles_rule_associations_id',
                    bbra.bundle_id as 'bundle_bundles_rule_associations_bundle_id',
                    bbra.display_order
                FROM master..cms_models_bundlerule br
                JOIN master..cms_models_bundlebundlesrule bbr on bbr.bundle_rule_id = br.id
                JOIN master..cms_models_bundlebundlesruleassociations bbra on bbra.bundle_bundles_rule_id = bbr.id
                WHERE br.rule_type_id = (SELECT id from master..cms_models_bundleruletype WHERE codename = 'bundle_bundles_rule')
        
GO
/****** Object:  Table [dbo].[custom_models_testcentertestsessions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcentertestsessions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_center_id] [int] NULL,
	[test_session_data_id] [int] NOT NULL,
	[is_standard] [bit] NOT NULL,
	[user_accommodation_file_id] [int] NULL,
	[assessment_process_assigned_test_specs_id] [int] NULL,
	[test_session_order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcenterolaassessorunavailability]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenterolaassessorunavailability](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[start_date] [datetimeoffset](7) NOT NULL,
	[end_date] [datetimeoffset](7) NOT NULL,
	[test_center_ola_test_assessor_id] [int] NOT NULL,
	[reason] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcenterolatestassessor]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenterolatestassessor](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[supervisor] [bit] NOT NULL,
	[test_center_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcenterolatimeslot]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenterolatimeslot](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[start_time] [time](7) NOT NULL,
	[end_time] [time](7) NOT NULL,
	[availability] [int] NOT NULL,
	[assessed_language_id] [int] NOT NULL,
	[test_center_id] [int] NOT NULL,
	[utc_offset] [int] NOT NULL,
	[slots_to_prioritize] [int] NOT NULL,
	[day_of_week_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcenter]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenter](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](150) NOT NULL,
	[postal_code] [nvarchar](15) NOT NULL,
	[security_email] [nvarchar](254) NULL,
	[department_id] [int] NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[booking_delay] [int] NOT NULL,
	[country_id] [int] NOT NULL,
	[accommodations_friendly] [bit] NOT NULL,
	[ola_authorized] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcenterolatimeslotassessoravailability]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenterolatimeslotassessoravailability](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_center_ola_test_assessor_id] [int] NOT NULL,
	[test_center_ola_time_slot_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcentertestsessiondata]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcentertestsessiondata](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[open_to_ogd] [bit] NULL,
	[date] [date] NOT NULL,
	[start_time] [datetimeoffset](7) NOT NULL,
	[end_time] [datetimeoffset](7) NOT NULL,
	[spaces_available] [int] NULL,
	[test_skill_sub_type_id] [int] NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[test_center_room_id] [int] NULL,
	[test_skill_type_id] [int] NULL,
	[test_assessor_user_id] [int] NULL,
	[candidate_phone_number] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[get_test_assessor_availabilities_by_date_fn]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[get_test_assessor_availabilities_by_date_fn] (@date date, @test_center_id int)
                RETURNS TABLE
                AS RETURN
                SELECT
					TOP 100 PERCENT
					tcts.start_time,
					tcts.end_time,
					tcts.day_of_week_id,
					tcts.assessed_language_id,
                    -- AVAILABLE ASSESSORS - UNAVAILABLE ASSESSORS - UNAVAILABLE ASSESSORS DUE TO ACCOMMODATIONS
					SUM(available_assessors.count) - SUM(unavailable_assessors.count) - SUM(unavailable_assessors_due_to_accommodations.count) as 'nbr_of_available_assessors'
				FROM master..custom_models_testcenterolatimeslot tcts
				JOIN master..custom_models_testcenter tc on tc.id = tcts.test_center_id
				-- GETTING NUMBER OF AVAILABLE ASSESSORS
				OUTER APPLY (SELECT COUNT(*) as 'count'
							 FROM master..custom_models_testcenterolatimeslotassessoravailability tcoaa
							 WHERE tcoaa.test_center_ola_time_slot_id = tcts.id
							) available_assessors
				-- GETTING NUMBER OF UNAVAILABLE ASSESSORS
				OUTER APPLY (SELECT COUNT(* ) as 'count'
							 FROM master..custom_models_testcenterolaassessorunavailability tcoau
							 WHERE tcoau.test_center_ola_test_assessor_id IN (SELECT tcoaa_2.test_center_ola_test_assessor_id
																			  FROM master..custom_models_testcenterolatimeslotassessoravailability tcoaa_2
																			  WHERE tcoaa_2.test_center_ola_time_slot_id = tcts.id
																			 )
							 -- Unavailability start_date (DateTime) < Formatted DateTime based on Test Session End Time
							 AND tcoau.start_date < CAST(CONCAT(@date, ' ', CAST(tcts.end_time AS TIME), ' +00:00') AS DATETIMEOFFSET)
							 -- Unavailability end_date (DateTime) > Formatted DateTime based on Test Session Start Time
							 AND tcoau.end_date > CAST(CONCAT(@date, ' ', CAST(tcts.start_time AS TIME), ' +00:00') AS DATETIMEOFFSET)
							 ) unavailable_assessors
				-- GETTING NUMBER OF UNAVAILABLE ASSESSORS DUE TO ACCOMMODATIONS
				OUTER APPLY (SELECT COUNT(*) as 'count'
							 FROM master..custom_models_testcentertestsessions ts
							 JOIN master..custom_models_testcentertestsessiondata tsd
							 ON tsd.id = ts.test_session_data_id
							 WHERE ts.is_standard = 0
							 -- tsd.test_assessor_user_id IS A USER_ID NOT A test_center_ola_test_assessor_id
							 AND tsd.test_assessor_user_id IN (SELECT tcota.user_id
															   FROM master..custom_models_testcenterolatestassessor tcota
															   WHERE id IN (SELECT tcoaa_3.test_center_ola_test_assessor_id
																			FROM master..custom_models_testcenterolatimeslotassessoravailability tcoaa_3
																			WHERE tcoaa_3.test_center_ola_time_slot_id = tcts.id
																		   )
															  )
							 AND (CAST(tsd.start_time as TIME) < tcts.end_time AND CAST(tsd.end_time as TIME) > tcts.start_time)
							) unavailable_assessors_due_to_accommodations
				-- ONLY CONSIDERING RESPECTIVE AUTHORIZED OLA TEST CENTERS WHERE DAY OF WEEK MATCHES THE PROVIDED DATE
				WHERE tc.ola_authorized = 1 AND tcts.test_center_id = @test_center_id AND tcts.day_of_week_id = DATEPART(dw, @date)
				GROUP BY tcts.start_time, tcts.end_time, tcts.day_of_week_id, tcts.assessed_language_id
				ORDER BY tcts.day_of_week_id
    
GO
/****** Object:  Table [dbo].[custom_models_testcenterolavacationblock]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenterolavacationblock](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date_from] [date] NOT NULL,
	[date_to] [date] NOT NULL,
	[test_center_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcenterolavacationblockavailability]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenterolavacationblockavailability](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[availability] [int] NOT NULL,
	[language_id] [int] NOT NULL,
	[test_center_ola_vacation_block_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[handle_vacation_block_availability_by_date_fn]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[handle_vacation_block_availability_by_date_fn] (@test_center_id int, @date date, @assessed_language_id int, @available_test_assessors int)
                RETURNS TABLE
                AS RETURN
                SELECT TOP 1 IIF(
                                -- NO VACATION BLOCK DATA FOUND
                                vacation_block_availability_data.availability is NULL,
                                -- RETURN PROVIDED AVAILABLE TEST ASSESSORS (SAME DATA AS BEFORE - NO CHANGES)
                                @available_test_assessors,
                                -- RETURN PROVIDED AVAILABLE TEST ASSESSORS X VACATION BLOCK AVAILABILITY (%)
                                @available_test_assessors * vacation_block_availability_data.availability/100
                                ) as 'available_test_assessors'
                FROM custom_models_testcenter tc
                -- GETTING VACATION BLOCK AVAILABILITY DATA (IF EXISTS) BASED ON PROVIDED TEST CENTER ID, ASSESSED LANGUAGE ID AND DATE
                OUTER APPLY (SELECT TOP 1 availability as 'availability'
                            FROM master..custom_models_testcenterolavacationblockavailability vba 
                            WHERE vba.test_center_ola_vacation_block_id IN (SELECT id 
                                                                            FROM master..custom_models_testcenterolavacationblock vb 
                                                                            WHERE vb.test_center_id = tc.id
                                                                            AND vba.language_id = @assessed_language_id
                                                                            AND @date >= vb.date_from 
                                                                            AND @date <= vb.date_to
                                                                            )
                            
                            ORDER BY vba.availability ASC
                            ) vacation_block_availability_data
                -- GETTING DATA BASED ON PROVIDED TEST CENTER ID
                WHERE tc.id = @test_center_id 

    
GO
/****** Object:  Table [dbo].[user_management_models_user]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[password] [nvarchar](128) NOT NULL,
	[last_login] [datetime2](7) NULL,
	[is_superuser] [bit] NOT NULL,
	[username] [nvarchar](150) NOT NULL,
	[email] [nvarchar](254) NOT NULL,
	[is_staff] [bit] NOT NULL,
	[is_active] [bit] NOT NULL,
	[date_joined] [datetime2](7) NOT NULL,
	[first_name] [nvarchar](30) NOT NULL,
	[last_name] [nvarchar](150) NOT NULL,
	[birth_date] [date] NOT NULL,
	[pri] [nvarchar](10) NOT NULL,
	[secondary_email] [nvarchar](254) NULL,
	[last_password_change] [date] NULL,
	[psrs_applicant_id] [nvarchar](8) NULL,
	[locked] [bit] NOT NULL,
	[locked_until] [datetime2](7) NULL,
	[login_attempts] [int] NOT NULL,
	[last_login_attempt] [datetime2](7) NULL,
	[is_profile_complete] [bit] NOT NULL,
	[last_profile_update] [datetime2](7) NULL,
	[military_nbr] [nvarchar](9) NOT NULL,
	[phone_number] [nvarchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [user_management_models_user_email_099a855c_uniq] UNIQUE NONCLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcenterolatestassessorapprovedlanguage]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenterolatestassessorapprovedlanguage](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[language_id] [int] NOT NULL,
	[test_center_ola_test_assessor_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[test_center_ola_test_assessors_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[test_center_ola_test_assessors_vw] AS
                SELECT
                    ta.id,
                    ta.test_center_id,
                    u.id as 'user_id',
                    u.first_name as 'user_first_name',
                    u.last_name as 'user_last_name',
                    u.email as 'user_email',
                    ta.supervisor,
                    lang.certified_language_ids
                FROM master..custom_models_testcenterolatestassessor ta
                JOIN master..user_management_models_user u on u.id = ta.user_id
                OUTER APPLY (SELECT string_agg(language_certifications.language_id, ',') within group (ORDER BY language_certifications.language_id) as 'certified_language_ids' FROM (SELECT DISTINCT taal.language_id FROM master..custom_models_testcenterolatestassessorapprovedlanguage taal WHERE taal.test_center_ola_test_assessor_id = ta.id) language_certifications) lang
    
GO
/****** Object:  Table [dbo].[custom_models_testcenterolaconfigs]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenterolaconfigs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[booking_delay] [int] NOT NULL,
	[advanced_booking_delay] [int] NOT NULL,
	[test_center_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_reasonfortestingpriority]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_reasonfortestingpriority](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_reason_for_testing_priority_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testskillsledesc]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testskillsledesc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_assessmentprocessassignedtestspecs]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assessmentprocessassignedtestspecs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_skill_sub_type_id] [int] NULL,
	[level_required] [nvarchar](30) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[assessment_process_id] [int] NOT NULL,
	[billing_contact_id] [int] NULL,
	[test_skill_type_id] [int] NOT NULL,
	[email] [nvarchar](254) NOT NULL,
	[first_name] [nvarchar](30) NOT NULL,
	[last_name] [nvarchar](150) NOT NULL,
	[reason_for_testing_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_email_assessment_process_and_test_skill_and_test_skill_sub_type_combination] UNIQUE NONCLUSTERED 
(
	[email] ASC,
	[assessment_process_id] ASC,
	[test_skill_type_id] ASC,
	[test_skill_sub_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_reasonfortesting]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_reasonfortesting](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[minimum_process_length] [int] NULL,
	[active] [bit] NOT NULL,
	[reason_for_testing_type_id] [int] NOT NULL,
	[waiting_period] [int] NULL,
	[reason_for_testing_priority_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_reason_for_testing_codename_and_reason_for_testing_type_id_combination] UNIQUE NONCLUSTERED 
(
	[codename] ASC,
	[reason_for_testing_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[get_nbr_of_reserved_test_assessors_by_date_fn]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[get_nbr_of_reserved_test_assessors_by_date_fn] (@date date, @language_id int, @reason_for_testing_priority_codename varchar(50))
                RETURNS TABLE
                AS RETURN
                SELECT
                    TOP 100 PERCENT
                    COUNT(ts.id) as 'nbr_of_reserved_assessors'
                FROM master..custom_models_testcentertestsessions ts
                JOIN master..custom_models_testcentertestsessiondata tsd on tsd.id = ts.test_session_data_id
				JOIN master..cms_models_testskillsledesc sle_desc on sle_desc.id = tsd.test_skill_sub_type_id
				JOIN master..custom_models_assessmentprocessassignedtestspecs apats on apats.id = ts.assessment_process_assigned_test_specs_id
				JOIN master..custom_models_reasonfortesting rft on rft.id = apats.reason_for_testing_id
				JOIN master..custom_models_reasonfortestingpriority rftp on rftp.id = rft.reason_for_testing_priority_id
                -- MATCHING PROVIDED DATE
				WHERE tsd.date = @date 
				-- MATCHING TEST SKILL SUB TYPE ID
				AND tsd.test_skill_sub_type_id = IIF(@language_id = 1, (SELECT id FROM master..cms_models_testskillsledesc WHERE codename = 'oe'), (SELECT id FROM master..cms_models_testskillsledesc WHERE codename = 'of'))
				-- MATCHING PROVIDED RESON FOR TESTING
				AND rftp.codename = @reason_for_testing_priority_codename
				-- ONLY CONSIDERING STANDARD SESSIONS
				AND ts.is_standard = 1
    
GO
/****** Object:  Table [dbo].[custom_models_olaprioritization]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_olaprioritization](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[value] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[get_ola_prioritization_value_by_codename_fn]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[get_ola_prioritization_value_by_codename_fn] (@codename char(50))
                RETURNS TABLE
                AS RETURN
                SELECT
                    CONVERT(DECIMAL(10, 2),op.value) as 'value'
                FROM master..custom_models_olaprioritization op
                WHERE op.codename = @codename
    
GO
/****** Object:  UserDefinedFunction [dbo].[get_nbr_of_reserved_test_assessors_by_date_and_time_fn]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[get_nbr_of_reserved_test_assessors_by_date_and_time_fn] (@date date, @start_time time, @end_time time, @language_id int, @reason_for_testing_priority_codename varchar(50))
                RETURNS TABLE
                AS RETURN
                SELECT
                    TOP 100 PERCENT
                    COUNT(ts.id) as 'nbr_of_reserved_assessors'
                FROM master..custom_models_testcentertestsessions ts
                JOIN master..custom_models_testcentertestsessiondata tsd on tsd.id = ts.test_session_data_id
				JOIN master..cms_models_testskillsledesc sle_desc on sle_desc.id = tsd.test_skill_sub_type_id
				JOIN master..custom_models_assessmentprocessassignedtestspecs apats on apats.id = ts.assessment_process_assigned_test_specs_id
				JOIN master..custom_models_reasonfortesting rft on rft.id = apats.reason_for_testing_id
				JOIN master..custom_models_reasonfortestingpriority rftp on rftp.id = rft.reason_for_testing_priority_id
                WHERE tsd.date = @date AND tsd.test_skill_sub_type_id = IIF(@language_id = 1, (SELECT id FROM master..cms_models_testskillsledesc WHERE codename = 'oe'), (SELECT id FROM master..cms_models_testskillsledesc WHERE codename = 'of'))
                AND CAST(tsd.start_time as time) = @start_time 
                AND CAST(tsd.end_time as time) = @end_time
				AND rftp.codename = @reason_for_testing_priority_codename
    
GO
/****** Object:  View [dbo].[test_center_ola_available_time_slots_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[test_center_ola_available_time_slots_vw] AS
                SELECT
                    TOP 100 PERCENT
                    -- generating ID by converting the datetime in timestamp
                    DATEDIFF(SECOND, '1970-01-01', (CONCAT(CONCAT(fn.date, ' '), fn.start_time))) as 'id',
                    fn.date,
                    fn.start_time,
                    fn.end_time,
                    fn.day_of_week_id,
                    fn.assessed_language_id,
					nbr_of_reserved_assessors_high_priority.count as 'nbr_of_reserved_assessors_high_priority',
					SUM(nbr_of_reserved_assessors_per_day_high_priority.count) as 'nbr_of_reserved_assessors_per_date_high_priority',
					-- available_test_assessors_total_per_date (high) - nbr_of_reserved_assessors_per_day_high_priority
					CONVERT(INT, (available_test_assessors_total_per_date.count * (SELECT * FROM master..get_ola_prioritization_value_by_codename_fn('high')) / 100) - SUM(nbr_of_reserved_assessors_per_day_high_priority.count)) as 'remaining_available_test_assessors_per_date_high',
					nbr_of_reserved_assessors_medium_priority.count 'nbr_of_reserved_assessors_medium_priority',
					SUM(nbr_of_reserved_assessors_per_day_medium_priority.count) as 'nbr_of_reserved_assessors_per_date_medium_priority',
					-- available_test_assessors_total_per_date (medium) - nbr_of_reserved_assessors_per_day_medium_priority
					CONVERT(INT, (available_test_assessors_total_per_date.count * (SELECT * FROM master..get_ola_prioritization_value_by_codename_fn('medium')) / 100) - SUM(nbr_of_reserved_assessors_per_day_medium_priority.count)) as 'remaining_available_test_assessors_per_date_medium',
					nbr_of_reserved_assessors_low_priority.count as 'nbr_of_reserved_assessors_low_priority',
					SUM(nbr_of_reserved_assessors_per_day_low_priority.count) as 'nbr_of_reserved_assessors_per_date_low_priority',
					-- available_test_assessors_total_per_date (low) - nbr_of_reserved_assessors_per_day_low_priority
					CONVERT(INT, (available_test_assessors_total_per_date.count * (SELECT * FROM master..get_ola_prioritization_value_by_codename_fn('low')) / 100) - SUM(nbr_of_reserved_assessors_per_day_low_priority.count)) as 'remaining_available_test_assessors_per_date_low',
					-- number of assessors selected in the app for respective time slot
					SUM(fn.available_test_assessors_total) as 'available_test_assessors_total',
					-- available_test_assessors_total_per_date (high + medium + low)
					CONVERT(INT, ((available_test_assessors_total_per_date.count * (SELECT * FROM master..get_ola_prioritization_value_by_codename_fn('high')) / 100) + (available_test_assessors_total_per_date.count * (SELECT * FROM master..get_ola_prioritization_value_by_codename_fn('medium')) / 100) + (available_test_assessors_total_per_date.count * (SELECT * FROM master..get_ola_prioritization_value_by_codename_fn('low')) / 100))) as 'reservable_available_test_assessors_per_date',
					-- available_test_assessors_total - nbr_of_reserved_assessors_per_day_high_priority - nbr_of_reserved_assessors_per_day_medium_priority - nbr_of_reserved_assessors_per_day_low_priority
					CONVERT(INT, (SUM(fn.available_test_assessors_total) * (SELECT * FROM master..get_ola_prioritization_value_by_codename_fn('high')) / 100) + (SUM(fn.available_test_assessors_total) * (SELECT * FROM master..get_ola_prioritization_value_by_codename_fn('medium')) / 100) + (SUM(fn.available_test_assessors_total) * (SELECT * FROM master..get_ola_prioritization_value_by_codename_fn('low')) / 100) - nbr_of_reserved_assessors_high_priority.count - nbr_of_reserved_assessors_medium_priority.count - nbr_of_reserved_assessors_low_priority.count) as 'remaining_available_test_assessors_total'
				FROM master..get_ola_available_time_slots_fn() fn
                OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM master..get_nbr_of_reserved_test_assessors_by_date_and_time_fn(date, start_time, end_time, fn.assessed_language_id, 'high')) nbr_of_reserved_assessors_high_priority
				OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM master..get_nbr_of_reserved_test_assessors_by_date_fn(date, fn.assessed_language_id, 'high')) nbr_of_reserved_assessors_per_day_high_priority
				OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM master..get_nbr_of_reserved_test_assessors_by_date_and_time_fn(date, start_time, end_time, fn.assessed_language_id, 'medium')) nbr_of_reserved_assessors_medium_priority
				OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM master..get_nbr_of_reserved_test_assessors_by_date_fn(date, fn.assessed_language_id, 'medium')) nbr_of_reserved_assessors_per_day_medium_priority
				OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM master..get_nbr_of_reserved_test_assessors_by_date_and_time_fn(date, start_time, end_time, fn.assessed_language_id, 'low')) nbr_of_reserved_assessors_low_priority
                OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM master..get_nbr_of_reserved_test_assessors_by_date_fn(date, fn.assessed_language_id, 'low')) nbr_of_reserved_assessors_per_day_low_priority
				OUTER APPLY (SELECT MAX(tcoc.booking_delay) as 'booking_delay' FROM master..custom_models_testcenterolaconfigs tcoc WHERE tcoc.test_center_id in (SELECT tcots.test_center_id FROM master..custom_models_testcenterolatimeslot tcots WHERE tcots.start_time = fn.start_time AND tcots.end_time = fn.end_time AND tcots.day_of_week_id = fn.day_of_week_id)) greatest_booking_delay_found
				OUTER APPLY (SELECT fn2.date, fn2.assessed_language_id, SUM(fn2.available_test_assessors_total) as 'count' FROM master..get_ola_available_time_slots_fn() fn2 WHERE fn2.date = fn.date AND fn2.assessed_language_id = fn.assessed_language_id GROUP BY date, assessed_language_id) available_test_assessors_total_per_date
				-- slots that have/had at least one available test assessor (one available spot)
                WHERE fn.available_test_assessors_total > 0
                /* KNOWN ISSUE:
                    - depending on the timezone, the date might be wrong ==> 2024-09-24 - 02:00-03:00 UTC | 2024-09-24 - 22:00-00:00 -4 !== 2024-09-24 - 01:00-02:00 -1 (should have been the 25th - 01:00-02:00)
                    - if booking delay is less than 24 hours and the candidate tries to get late sessions on the same day (01:00-02:00 UTC for example), the logic might filter it out because the date is not converted in UTC, just the start time and end time
                */
                -- start_time_as_datetime (ex: CAST('2024-09-18 19:00:00' as datetime)) >= current UTC datetime + greatest_booking_delay_found
                AND CAST(CONCAT(CONCAT(fn.date, ' '), LEFT (fn.start_time, 8)) as datetime) >= DATEADD(HOUR, greatest_booking_delay_found.booking_delay, GETUTCDATE())
				GROUP BY fn.date, fn.start_time, fn.end_time, fn.day_of_week_id, fn.assessed_language_id, nbr_of_reserved_assessors_high_priority.count, nbr_of_reserved_assessors_medium_priority.count, nbr_of_reserved_assessors_low_priority.count, greatest_booking_delay_found.booking_delay, available_test_assessors_total_per_date.count
                ORDER BY fn.date, fn.start_time ASC
    
GO
/****** Object:  Table [dbo].[custom_models_virtualtestroom]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_virtualtestroom](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](40) NOT NULL,
	[user_id] [nvarchar](36) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_virtualteamsmeetingsession]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_virtualteamsmeetingsession](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[meeting_id] [nvarchar](175) NOT NULL,
	[meeting_link] [nvarchar](255) NOT NULL,
	[date] [date] NOT NULL,
	[start_time] [datetimeoffset](7) NOT NULL,
	[end_time] [datetimeoffset](7) NOT NULL,
	[candidate_id] [int] NOT NULL,
	[test_administrator_id] [int] NULL,
	[virtual_test_room_id] [int] NULL,
	[test_session_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[virtual_meeting_room_availability_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[virtual_meeting_room_availability_vw] AS
                SELECT
                    vtr.id as room_id,
                    vtr.user_id as room_user_id,
                    vml.date,
                    vml.start_time,
                    vml.end_time,
                COUNT(vml.id) count
                FROM master..custom_models_virtualtestroom vtr
                LEFT JOIN master..custom_models_virtualteamsmeetingsession vml on vtr.id=vml.virtual_test_room_id
                GROUP BY vtr.id, user_id, date, start_time, end_time
    
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfilestatus]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfilestatus](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[active] [bit] NOT NULL,
	[order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_user_accommodation_file_status_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfilestatustext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfilestatustext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](50) NOT NULL,
	[language_id] [int] NOT NULL,
	[user_accommodation_file_status_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfile]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfile](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[comments] [nvarchar](max) NOT NULL,
	[is_uit] [bit] NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[created_date] [date] NOT NULL,
	[last_modified_by_user_id] [int] NOT NULL,
	[status_id] [int] NOT NULL,
	[test_center_id] [int] NULL,
	[user_id] [int] NOT NULL,
	[assigned_to_user_id] [int] NULL,
	[reason_for_action] [nvarchar](500) NULL,
	[rationale] [nvarchar](max) NOT NULL,
	[accommodation_request_id] [int] NULL,
	[complexity_id] [int] NULL,
	[is_alternate_test_request] [bit] NOT NULL,
	[candidate_phone_number] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_billingcontact]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_billingcontact](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [nvarchar](30) NOT NULL,
	[last_name] [nvarchar](150) NOT NULL,
	[email] [nvarchar](254) NOT NULL,
	[department_id] [int] NOT NULL,
	[fis_organisation_code] [nvarchar](16) NOT NULL,
	[fis_reference_code] [nvarchar](20) NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testskilloccupationaldesc]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testskilloccupationaldesc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testskilltype]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testskilltype](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testskilltypetext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testskilltypetext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[language_id] [int] NOT NULL,
	[test_skill_type_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testskillsledesctext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testskillsledesctext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[language_id] [int] NOT NULL,
	[test_skill_sle_desc_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_assignedteststatus]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assignedteststatus](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_assigned_test_status_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testskilloccupationaldesctext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testskilloccupationaldesctext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[language_id] [int] NOT NULL,
	[test_skill_occupational_desc_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_consumedreservationcodestatus]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_consumedreservationcodestatus](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](14) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfiletesttoadminister]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfiletesttoadminister](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[specified_test_description] [nvarchar](175) NULL,
	[test_id] [int] NULL,
	[user_accommodation_file_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_consumedreservationcodes]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_consumedreservationcodes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[reservation_code] [nvarchar](14) NOT NULL,
	[modify_date] [date] NOT NULL,
	[assessment_process_assigned_test_specs_id] [int] NOT NULL,
	[candidate_id] [int] NULL,
	[status_id] [int] NOT NULL,
	[test_session_id] [int] NULL,
	[assigned_test_id] [int] NULL,
	[user_accommodation_file_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [consumed_reservation_must_have_a_unique_assessment_process_assigned_test_specs_id] UNIQUE NONCLUSTERED 
(
	[assessment_process_assigned_test_specs_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_reasonfortestingtext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_reasonfortestingtext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[language_id] [int] NOT NULL,
	[reason_for_testing_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_reason_for_testing_id_and_language_id_combination] UNIQUE NONCLUSTERED 
(
	[reason_for_testing_id] ASC,
	[language_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_assessmentprocess]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assessmentprocess](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[reference_number] [nvarchar](30) NOT NULL,
	[department_id] [int] NOT NULL,
	[closing_date] [date] NULL,
	[allow_booking_external_tc] [bit] NOT NULL,
	[default_billing_contact_id] [int] NULL,
	[user_id] [int] NOT NULL,
	[request_sent] [bit] NOT NULL,
	[contact_email_for_candidates] [nvarchar](254) NOT NULL,
	[duration] [int] NOT NULL,
	[allow_last_minute_cancellation_tc] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_DEPARTMENTS_VW]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_DEPARTMENTS_VW](
	[DEPT_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](20) NULL,
	[FABRV] [nvarchar](20) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[DEPT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[assessment_process_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[assessment_process_vw] AS
                SELECT
                    ap.id,
                    u.id as 'user_id',
                    u.email as 'user_email',
                    ap.reference_number,
                    ap.duration,
                    CASE
                        -- request has been sent (closing date should be defined)
                        WHEN ap.request_sent = 1 THEN ap.closing_date
                        -- request has not been sent yet (adding "duration" amount of days to today's date)
                        ELSE CONVERT(VARCHAR(10), DATEADD(DAY, ap.duration, GETDATE()), 23)
                    END as 'closing_date',
                    CASE
                        WHEN ap.request_sent = 1 THEN sent_assessment_process_data.calculated_initial_date
                        ELSE NULL
                    END as 'sent_date',
                    ap.allow_booking_external_tc,
                    ap.allow_last_minute_cancellation_tc,
                    ap.contact_email_for_candidates,
                    ap.request_sent,
                    d1.dept_id,
                    d1.eabrv as 'dept_eabrv',
                    d1.fabrv as 'dept_fabrv',
                    d1.edesc as 'dept_edesc',
                    d1.fdesc as 'dept_fdesc',
                    IIF(bc.id IS NOT NULL, bc.id, NULL) as 'default_billing_contact_id',
                    IIF(bc.id IS NOT NULL, bc.first_name, NULL) as 'default_billing_contact_first_name',
                    IIF(bc.id IS NOT NULL, bc.last_name, NULL) as 'default_billing_contact_last_name',
                    IIF(bc.id IS NOT NULL, bc.email, NULL) as 'default_billing_contact_email',
                    IIF(bc.id IS NOT NULL, bc.fis_organisation_code, NULL) as 'default_billing_contact_fis_organisation_code',
                    IIF(bc.id IS NOT NULL, bc.fis_reference_code, NULL) as 'default_billing_contact_fis_reference_code',
                    IIF(bc.id IS NOT NULL, d2.dept_id, NULL) as 'default_billing_contact_dept_id',
                    IIF(bc.id IS NOT NULL, d2.eabrv, NULL) as 'default_billing_contact_dept_eabrv',
                    IIF(bc.id IS NOT NULL, d2.fabrv, NULL) as 'default_billing_contact_dept_fabrv',
                    IIF(bc.id IS NOT NULL, d2.edesc, NULL) as 'default_billing_contact_dept_edesc',
                    IIF(bc.id IS NOT NULL, d2.fdesc, NULL) as 'default_billing_contact_dept_fdesc'
                FROM master..custom_models_assessmentprocess ap
                JOIN master..user_management_models_user u on u.id = ap.user_id
                JOIN master..CAT_REF_DEPARTMENTS_VW d1 on d1.dept_id = ap.department_id
                LEFT JOIN master..custom_models_billingcontact bc on bc.id = ap.default_billing_contact_id
                LEFT JOIN master..CAT_REF_DEPARTMENTS_VW d2 on d2.dept_id = bc.department_id
                OUTER APPLY (SELECT TOP 1 CONVERT(VARCHAR(10), DATEADD(DAY, -ap2.duration, ap2.closing_date), 23) as 'calculated_initial_date' FROM master..custom_models_assessmentprocess ap2 WHERE ap2.id = ap.id AND closing_date is not NULL) sent_assessment_process_data
        
GO
/****** Object:  Table [dbo].[cms_models_testdefinition]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testdefinition](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_code] [nvarchar](25) NOT NULL,
	[version] [int] NOT NULL,
	[en_name] [nvarchar](150) NOT NULL,
	[fr_name] [nvarchar](150) NOT NULL,
	[is_public] [bit] NOT NULL,
	[active] [bit] NOT NULL,
	[parent_code] [nvarchar](25) NOT NULL,
	[retest_period] [int] NOT NULL,
	[archived] [bit] NOT NULL,
	[version_notes] [nvarchar](500) NOT NULL,
	[count_up] [bit] NOT NULL,
	[is_uit] [bit] NOT NULL,
	[show_result] [bit] NOT NULL,
	[show_score] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_assignedtest]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assignedtest](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[start_date] [datetime2](7) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[submit_date] [datetime2](7) NULL,
	[test_access_code] [nvarchar](13) NULL,
	[total_score] [int] NULL,
	[test_id] [int] NOT NULL,
	[test_section_id] [int] NULL,
	[test_session_language_id] [int] NULL,
	[test_order_number] [nvarchar](12) NULL,
	[en_converted_score] [nvarchar](50) NULL,
	[fr_converted_score] [nvarchar](50) NULL,
	[is_invalid] [bit] NOT NULL,
	[uit_invite_id] [int] NULL,
	[orderless_financial_data_id] [int] NULL,
	[accommodation_request_id] [int] NULL,
	[test_session_id] [int] NULL,
	[previous_status_id] [int] NULL,
	[status_id] [int] NOT NULL,
	[ta_user_id] [int] NULL,
	[user_id] [int] NOT NULL,
	[score_valid_until] [date] NULL,
	[user_accommodation_file_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_status_id_user_id_tac_test_id_combination] UNIQUE NONCLUSTERED 
(
	[status_id] ASC,
	[user_id] ASC,
	[test_access_code] ASC,
	[test_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[assessment_process_assigned_test_specs_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[assessment_process_assigned_test_specs_vw] AS
                SELECT
                    ats.id,
                    ats.first_name,
                    ats.last_name,
                    ats.email,
                    apv.id as 'assessment_process_id',
                    apv.reference_number as 'assessment_process_reference_number',
                    apv.closing_date as 'assessment_process_closing_date',
                    apv.allow_booking_external_tc as 'assessment_process_allow_booking_external_tc',
                    apv.contact_email_for_candidates,
                    u.id as 'hr_user_id',
                    u.email as 'hr_email',
                    u.first_name as 'hr_first_name',
                    u.last_name as 'hr_last_name',
                    apv.dept_id as 'assessment_process_dept_id ',
                    apv.dept_eabrv as 'assessment_process_dept_eabrv',
                    apv.dept_fabrv as 'assessment_process_dept_fabrv',
                    apv.dept_edesc as 'assessment_process_dept_edesc',
                    apv.dept_fdesc as 'assessment_process_dept_fdesc',
                    IIF(bc.id is not NULL, bc.id, NULL) as 'billing_contact_id',
                    IIF(bc.id is not NULL, bc.first_name, NULL) as 'billing_contact_first_name',
                    IIF(bc.id is not NULL, bc.last_name, NULL) as 'billing_contact_last_name ',
                    IIF(bc.id is not NULL, bc.email, NULL) as 'billing_contact_email',
                    tst.id as 'test_skill_type_id',
                    tst.codename as 'test_skill_type_codename',
                    type_e.text as 'test_skill_type_name_en',
                    type_f.text as 'test_skill_type_name_fr',
                    ats.test_skill_sub_type_id as 'test_skill_sub_type_id',
                    CASE
                        WHEN tst.id = 2 THEN sled.codename
                        WHEN tst.id = 3 THEN occd.codename
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_codename',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_e.text
                        WHEN tst.id = 3 THEN occ_sub_type_e.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_en',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_f.text
                        WHEN tst.id = 3 THEN occ_sub_type_f.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_fr',
                    rft.id as 'reason_for_testing_id',
                    rft.minimum_process_length as 'reason_for_testing_minimum_process_length',
                    rft.codename as 'reason_for_testing_codename',
                    reason_for_testing_e.text as 'reason_for_testing_name_en',
                    reason_for_testing_f.text as 'reason_for_testing_name_fr',
                    ats.level_required,
                    apv.request_sent,
                    crcs.id as 'consumed_reservation_codes_status_id',
                    crcs.codename 'consumed_reservation_codes_status_codename',
                    crc.test_session_id,
                    at.id as 'assigned_test_id',
                    atsts.id as 'assigned_test_status_id',
                    atsts.codename as 'assigned_test_status_codename',
                    uaf.id as 'user_accommodation_file_id',
                    uaf.is_alternate_test_request,
                    td.id as 'test_id',
                    td.parent_code as 'parent_code',
                    td.test_code as 'test_code',
                    td.version as 'version',
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    c_u.id as 'candidate_user_id',
                    c_u.username as 'candidate_username',
                    c_u.email as 'candidate_email',
                    c_u.first_name as 'candidate_first_name',
                    c_u.last_name as 'candidate_last_name'
                FROM master..custom_models_assessmentprocessassignedtestspecs ats
                LEFT JOIN master..custom_models_billingcontact bc on bc.id = ats.billing_contact_id
                JOIN master..assessment_process_vw apv on apv.id = ats.assessment_process_id
                JOIN master..user_management_models_user u on u.id = apv.user_id
                JOIN master..cms_models_testskilltype tst on tst.id = ats.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM master..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM master..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN master..cms_models_testskillsledesc sled on sled.id = ats.test_skill_sub_type_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM master..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM master..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                LEFT JOIN master..cms_models_testskilloccupationaldesc occd on occd.id = ats.test_skill_sub_type_id
                OUTER APPLY (SELECT TOP 1 occdte.text FROM master..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = occd.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM master..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = occd.id AND occdtf.language_id = 2) occ_sub_type_f
                JOIN master..custom_models_reasonfortesting rft on rft.id = ats.reason_for_testing_id
                OUTER APPLY (SELECT TOP 1 text FROM master..custom_models_reasonfortestingtext rftt WHERE rftt.reason_for_testing_id = rft.id AND language_id = 1) reason_for_testing_e
                OUTER APPLY (SELECT TOP 1 text FROM master..custom_models_reasonfortestingtext rftt WHERE rftt.reason_for_testing_id = rft.id AND language_id = 2) reason_for_testing_f
                LEFT JOIN master..custom_models_consumedreservationcodes crc on crc.assessment_process_assigned_test_specs_id = ats.id
                LEFT JOIN master..custom_models_consumedreservationcodestatus crcs on crcs.id = crc.status_id
                LEFT JOIN master..custom_models_assignedtest at on at.id = crc.assigned_test_id
                LEFT JOIN master..custom_models_assignedteststatus atsts on atsts.id = at.status_id
                LEFT JOIN master..custom_models_useraccommodationfile uaf on uaf.id = crc.user_accommodation_file_id
                LEFT JOIN master..custom_models_useraccommodationfiletesttoadminister uaftta on uaftta.user_accommodation_file_id = uaf.id
                LEFT JOIN master..user_management_models_user c_u on c_u.id = uaf.user_id
                LEFT JOIN master..cms_models_testdefinition td on td.id = uaftta.test_id
                
GO
/****** Object:  Table [dbo].[cms_models_testskill]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testskill](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_definition_id] [int] NOT NULL,
	[test_skill_type_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_uitinvites]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_uitinvites](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[invite_date] [date] NOT NULL,
	[validity_end_date] [date] NOT NULL,
	[ta_test_permissions_id] [int] NULL,
	[reason_for_deletion_id] [int] NULL,
	[reason_for_modification_id] [int] NULL,
	[orderless_request] [bit] NOT NULL,
	[ta_user_id] [int] NOT NULL,
	[test_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestpermissions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestpermissions](
	[id] [int] NOT NULL,
	[date_assigned] [datetime2](7) NOT NULL,
	[expiry_date] [date] NOT NULL,
	[test_order_number] [nvarchar](12) NOT NULL,
	[staffing_process_number] [nvarchar](50) NOT NULL,
	[department_ministry_code] [nvarchar](10) NOT NULL,
	[is_org] [nvarchar](16) NOT NULL,
	[is_ref] [nvarchar](20) NOT NULL,
	[billing_contact] [nvarchar](180) NOT NULL,
	[billing_contact_info] [nvarchar](255) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_id] [int] NULL,
	[reason_for_modif_or_del] [nvarchar](300) NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_custompermissions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_custompermissions](
	[permission_id] [int] IDENTITY(1,1) NOT NULL,
	[en_name] [nvarchar](75) NOT NULL,
	[en_description] [nvarchar](max) NOT NULL,
	[fr_description] [nvarchar](max) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[date_assigned] [datetime2](7) NOT NULL,
	[expiry_date] [datetime2](7) NULL,
	[content_type_id] [int] NOT NULL,
	[fr_name] [nvarchar](75) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testskillsle]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testskillsle](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_skill_id] [int] NOT NULL,
	[test_skill_sle_desc_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testskilloccupational]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testskilloccupational](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_skill_id] [int] NOT NULL,
	[test_skill_occupational_desc_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalconsumedreservationcodes]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalconsumedreservationcodes](
	[id] [int] NOT NULL,
	[reservation_code] [nvarchar](14) NOT NULL,
	[modify_date] [date] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assessment_process_assigned_test_specs_id] [int] NULL,
	[candidate_id] [int] NULL,
	[history_user_id] [int] NULL,
	[status_id] [int] NULL,
	[test_session_id] [int] NULL,
	[assigned_test_id] [int] NULL,
	[user_accommodation_file_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfiletesttoadminister]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfiletesttoadminister](
	[id] [int] NOT NULL,
	[specified_test_description] [nvarchar](175) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_id] [int] NULL,
	[user_accommodation_file_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfiletesttime]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfiletesttime](
	[id] [int] NOT NULL,
	[test_section_time] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_section_id] [int] NULL,
	[user_accommodation_file_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfileothermeasures]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfileothermeasures](
	[id] [int] NOT NULL,
	[other_measures] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodation_file_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfilebreakbank]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfilebreakbank](
	[id] [int] NOT NULL,
	[break_time] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodation_file_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicalcustomuserpermissions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicalcustomuserpermissions](
	[user_permission_id] [int] NOT NULL,
	[goc_email] [nvarchar](254) NOT NULL,
	[pri] [nvarchar](10) NOT NULL,
	[supervisor] [nvarchar](180) NOT NULL,
	[supervisor_email] [nvarchar](254) NOT NULL,
	[rationale] [nvarchar](300) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[permission_id] [int] NULL,
	[reason_for_modif_or_del] [nvarchar](300) NULL,
	[military_nbr] [nvarchar](9) NOT NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testsection]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testsection](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order] [int] NOT NULL,
	[section_type] [int] NOT NULL,
	[default_time] [int] NULL,
	[en_title] [nvarchar](150) NOT NULL,
	[fr_title] [nvarchar](150) NOT NULL,
	[next_section_button_type] [int] NOT NULL,
	[scoring_type] [int] NOT NULL,
	[uses_notepad] [bit] NULL,
	[block_cheating] [bit] NULL,
	[default_tab] [int] NULL,
	[test_definition_id] [int] NOT NULL,
	[minimum_score] [int] NULL,
	[uses_calculator] [bit] NOT NULL,
	[item_exposure] [bit] NOT NULL,
	[needs_approval] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_orderlessfinancialdata]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_orderlessfinancialdata](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[reference_number] [nvarchar](25) NOT NULL,
	[department_ministry_id] [int] NOT NULL,
	[fis_organisation_code] [nvarchar](16) NOT NULL,
	[fis_reference_code] [nvarchar](20) NOT NULL,
	[billing_contact_name] [nvarchar](180) NOT NULL,
	[billing_contact_info] [nvarchar](255) NOT NULL,
	[level_required] [nvarchar](50) NULL,
	[uit_invite_id] [int] NULL,
	[reason_for_testing_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfile]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfile](
	[id] [int] NOT NULL,
	[comments] [nvarchar](max) NOT NULL,
	[is_uit] [bit] NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[created_date] [date] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[last_modified_by_user_id] [int] NULL,
	[status_id] [int] NULL,
	[test_center_id] [int] NULL,
	[user_id] [int] NULL,
	[assigned_to_user_id] [int] NULL,
	[reason_for_action] [nvarchar](500) NULL,
	[rationale] [nvarchar](max) NOT NULL,
	[accommodation_request_id] [int] NULL,
	[complexity_id] [int] NULL,
	[is_alternate_test_request] [bit] NOT NULL,
	[candidate_phone_number] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[user_accommodation_file_report_data_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[user_accommodation_file_report_data_vw] AS
                SELECT
                    huaf.id,
                    huaf.history_id,
                    huaf.history_date,
                    huaf.is_uit,
                    huaf.created_date,
                    CASE
                        WHEN uafs.codename = 'pending_approval' THEN CONVERT(VARCHAR(10), latest_completed_file_data.history_date, 23)
                        ELSE NULL
                    END as 'request_completed_by_aae_date',
                    uafs.id as 'status_id',
                    uafs.codename as 'status_codename',
                    uafst_en.text as 'status_name_en',
                    uafst_fr.text as 'status_name_fr',
                    ummu.id as 'user_id',
                    ummu.username as 'user_username',
                    ummu.email as 'user_email',
                    ummu.first_name as 'user_first_name',
                    ummu.last_name as 'user_last_name',
                    at_u.id as 'assigned_to_user_id',
                    at_u.email as 'assigned_to_user_email',
                    at_u_email_data.goc_email as 'assigned_to_user_goc_email',
                    at_u.first_name as 'assigned_to_user_first_name',
                    at_u.last_name 'assigned_to_user_last_name',
                    IIF(at_u.phone_number is NULL OR at_u.phone_number = '', NULL, at_u.phone_number) as 'assigned_to_phone_number',
                    hr_u.id as 'hr_user_id',
                    hr_u.email as 'hr_email',
                    hr_u.first_name as 'hr_first_name',
                    hr_u.last_name as 'hr_last_name',
                    IIF(hr_u.phone_number is NULL OR hr_u.phone_number = '', NULL, hr_u.phone_number) as 'hr_phone_number',
                    ta_u.id as 'ta_user_id',
                    ta_u.email as 'ta_email',
                    ta_u.first_name as 'ta_first_name',
                    ta_u.last_name as 'ta_last_name',
                    IIF(ta_u.phone_number is NULL OR ta_u.phone_number = '', NULL, ta_u.phone_number) as 'ta_phone_number',
                    IIF(huafom.history_type = '+', huafom.other_measures, NULL) as 'other_measures',
                    -- TEST SKILL TYPE ID
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 THEN ts.test_skill_type_id
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_id
                    END as 'test_skill_type_id',
                    -- TEST SKILL TYPE CODENAME
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 THEN tst.codename
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_codename
                    END as 'test_skill_type_codename',
                    -- TEST SKILL TYPE NAME (EN)
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 THEN type_e.text
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_name_en
                    END as 'test_skill_type_name_en',
                    -- TEST SKILL TYPE NAME (FR)
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 THEN type_f.text
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_name_fr
                    END as 'test_skill_type_name_fr',
                    -- TEST SKILL SUB-TYPE ID
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sled.id
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occd.id
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_id
                    END as 'test_skill_sub_type_id',
                    -- TEST SKILL SUB-TYPE CODENAME
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sled.codename
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occd.codename
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_codename
                    END as 'test_skill_sub_type_codename',
                    -- TEST SKILL SUB-TYPE NAME (EN)
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sle_sub_type_e.text
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occ_sub_type_e.text
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_name_en
                    END as 'test_skill_sub_type_name_en',
                    -- TEST SKILL SUB-TYPE NAME (FR)
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sle_sub_type_f.text
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occ_sub_type_f.text
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_name_fr
                    END as 'test_skill_sub_type_name_fr',
                    -- REQUESTING DEPARTMENT DATA
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 THEN
                        CASE
                            WHEN ofd.id is not NULL THEN ofd_dept_vw.DEPT_ID
                            WHEN tp_dept_vw.DEPT_ID is not NULL THEN tp_dept_vw.DEPT_ID
                            ELSE 104
                        END
                        -- Supervised Test
                        ELSE apatsv.assessment_process_dept_id
                    END as 'requesting_dept_id',
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 THEN
                        CASE
                            WHEN ofd.id is not NULL THEN ofd_dept_vw.EABRV
                            WHEN tp_dept_vw.DEPT_ID is not NULL THEN tp_dept_vw.EABRV
                            ELSE 'PSC'
                        END
                        -- Supervised Test
                        ELSE apatsv.assessment_process_dept_eabrv
                    END as 'requesting_dept_abrv_en',
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 THEN
                        CASE
                            WHEN ofd.id is not NULL THEN ofd_dept_vw.FABRV
                            WHEN tp_dept_vw.DEPT_ID is not NULL THEN tp_dept_vw.FABRV
                            ELSE 'CFP'
                        END
                        -- Supervised Test
                        ELSE apatsv.assessment_process_dept_fabrv
                    END as 'requesting_dept_abrv_fr',
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 THEN
                        CASE
                            WHEN ofd.id is not NULL THEN ofd_dept_vw.EDESC
                            WHEN tp_dept_vw.DEPT_ID is not NULL THEN tp_dept_vw.EDESC
                            ELSE 'Public Service Commission'
                        END
                        -- Supervised Test
                        ELSE apatsv.assessment_process_dept_edesc
                    END as 'requesting_dept_desc_en',
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 THEN
                        CASE
                            WHEN ofd.id is not NULL THEN ofd_dept_vw.EDESC
                            WHEN tp_dept_vw.DEPT_ID is not NULL THEN tp_dept_vw.EDESC
                            ELSE 'Commission de la fonction publique'
                        END
                        -- Supervised Test
                        ELSE apatsv.assessment_process_dept_fdesc
                    END as 'requesting_dept_desc_fr',
                    tc.name as 'test_center_name',
                    td.id as 'test_id',
                    td.parent_code,
                    td.test_code,
                    td.version,
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    huaftta.specified_test_description,
                    default_times.sum as 'total_default_time',
                    -- if adjusted_times.sum is not defined ==> go get the most updated data from the real table (not the historical one)
                    IIF(adjusted_times.sum is not NULL, adjusted_times.sum, default_times.sum) as 'total_adjusted_time',
                    -- if last entry of break bank is "+" ==> break bank defined
                    -- if last entry of break bank is "-" ==> no break bank defined
                    IIF(huafbb.history_type = '+', huafbb.break_time, NULL) as 'break_time'
                FROM master..custom_models_historicaluseraccommodationfile huaf
                -- PREVIOUS COMPLETED USER ACCOMMODATION FILE
                OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_historicaluseraccommodationfile phuaf_2 WHERE phuaf_2.id = huaf.id AND phuaf_2.history_date < huaf.history_date ORDER BY phuaf_2.history_date DESC) previous_huaf
                -- TEST CENTER
                LEFT JOIN master..custom_models_testcenter tc on tc.id = huaf.test_center_id
                -- CANDIDATE USER DATA
                JOIN master..user_management_models_user ummu ON ummu.id = huaf.user_id
                -- FILE STATUS DATA
                JOIN master..custom_models_useraccommodationfilestatus uafs ON uafs.id = huaf.status_id
                JOIN master..custom_models_useraccommodationfilestatustext uafst_en on uafst_en.user_accommodation_file_status_id = uafs.id AND uafst_en.language_id = 1
                JOIN master..custom_models_useraccommodationfilestatustext uafst_fr on uafst_fr.user_accommodation_file_status_id = uafs.id AND uafst_fr.language_id = 2
                -- ASSIGNED AAE USER DATA
                LEFT JOIN master..user_management_models_user at_u on at_u.id = huaf.assigned_to_user_id
                -- GOC EMAIL OF ASSIGNED AAE (FROM HISTORICAL CUSTOM USER PERMISSIONS)
                OUTER APPLY (SELECT TOP 1 hcup.goc_email FROM master..user_management_models_historicalcustomuserpermissions hcup WHERE hcup.user_id = at_u.id AND hcup.history_type = '+' AND hcup.permission_id = (SELECT cp.permission_id FROM master..user_management_models_custompermissions cp WHERE cp.codename = 'is_aae') ORDER BY hcup.history_date DESC) at_u_email_data
                -- REQUEST COMPLETED DATE (FROM HISTORICAL TABLE)
                OUTER APPLY (SELECT TOP 1 huaf.history_date FROM master..custom_models_historicaluseraccommodationfile huaf WHERE huaf.id = huaf.id AND huaf.status_id = (SELECT uafs_2.id FROM master..custom_models_useraccommodationfilestatus uafs_2 WHERE uafs_2.codename = 'completed') ORDER BY huaf.history_date DESC) latest_completed_file_data
                -- OTHER MEASURES DATA
                OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_historicaluseraccommodationfileothermeasures huafom_2 WHERE huafom_2.user_accommodation_file_id = huaf.id AND huafom_2.history_date < huaf.history_date ORDER BY huafom_2.history_date DESC) huafom
                -- TEST SKILL TYPE / SUB-TYPE DATA (UIT TESTS)
                OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_assignedtest at_1 WHERE at_1.user_accommodation_file_id = huaf.id ORDER BY at_1.id DESC) at
                LEFT JOIN master..cms_models_testskill ts on ts.test_definition_id = at.test_id
                LEFT JOIN master..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM master..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM master..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN master..cms_models_testskillsle tssle on tssle.test_skill_id = ts.id
                LEFT JOIN master..cms_models_testskillsledesc sled on sled.id = tssle.test_skill_sle_desc_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM master..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM master..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                LEFT JOIN master..cms_models_testskilloccupational tsocc on tsocc.test_skill_id = ts.id
                LEFT JOIN master..cms_models_testskilloccupationaldesc occd on occd.id = tsocc.test_skill_occupational_desc_id
                OUTER APPLY (SELECT TOP 1 occdte.text FROM master..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = occd.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM master..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = occd.id AND occdtf.language_id = 2) occ_sub_type_f
                -- DEPARTMENT DATA (UIT TESTS)
                -- ORDERED TESTS
                LEFT JOIN master..custom_models_uitinvites ui on ui.id = at.uit_invite_id
                OUTER APPLY (SELECT TOP 1 * FROM master..cms_models_historicaltestpermissions htp_2 WHERE htp_2.test_order_number = at.test_order_number AND htp_2.history_date < ui.invite_date ORDER BY htp_2.history_date DESC) htp
                LEFT JOIN master..CAT_REF_DEPARTMENTS_VW tp_dept_vw on CONVERT(varchar, tp_dept_vw.DEPT_ID) = CONVERT(varchar, htp.department_ministry_code)
                -- ORDERLESS TESTS
                LEFT JOIN master..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
                LEFT JOIN master..CAT_REF_DEPARTMENTS_VW ofd_dept_vw on CONVERT(varchar, ofd_dept_vw.DEPT_ID) = CONVERT(varchar, ofd.department_ministry_id)
                -- DEPARTMENT DATA (UIT TESTS) - END
                -- TEST SKILL TYPE / SUB-TYPE DATA (SUPERVISED TESTS)
                OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_historicalconsumedreservationcodes hcrc_1 WHERE hcrc_1.user_accommodation_file_id = huaf.id AND hcrc_1.history_date < huaf.history_date ORDER BY hcrc_1.history_date DESC) hcrc
                LEFT JOIN master..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = hcrc.assessment_process_assigned_test_specs_id
                --  TEST TO ADMINISTER
                OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_historicaluseraccommodationfiletesttoadminister huaftta_2 WHERE huaftta_2.user_accommodation_file_id = huaf.id AND huaftta_2.history_date < huaf.history_date ORDER BY huaftta_2.history_date DESC) huaftta
                LEFT JOIN master..cms_models_testdefinition td on td.id = huaftta.test_id
                -- BREAK TIME DATA
                OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_historicaluseraccommodationfilebreakbank huafbb_2 WHERE huafbb_2.user_accommodation_file_id = huaf.id AND huafbb_2.history_date < huaf.history_date ORDER BY huafbb_2.history_date DESC) huafbb
                -- TOTAL DEFAULT TEST TIME
                OUTER APPLY (SELECT SUM(tsec.default_time) as 'sum' FROM master..cms_models_testsection tsec WHERE tsec.test_definition_id = td.id AND tsec.default_time is not NULL) default_times
                -- TOTAL ADJUSTED TIME
                OUTER APPLY (SELECT SUM(huaftt.test_section_time) AS 'sum' FROM master..custom_models_historicaluseraccommodationfiletesttime huaftt WHERE huaftt.user_accommodation_file_id = huaf.id AND huaftt.history_date < huaf.history_date AND DATEDIFF(second, huaftt.history_date, previous_huaf.history_date) < 1 AND huaftt.history_type = '+') adjusted_times
                LEFT JOIN master..user_management_models_user hr_u on hr_u.id = apatsv.hr_user_id
                LEFT JOIN master..user_management_models_user ta_u on ta_u.id = at.ta_user_id
                -- ONLY COMPLETED USER ACCOMMODATION FILES
                WHERE huaf.status_id = (SELECT uafs_2.id FROM master..custom_models_useraccommodationfilestatus uafs_2 WHERE uafs_2.codename = 'pending_approval')
    
GO
/****** Object:  Table [dbo].[custom_models_testcenterroomdata]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenterroomdata](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](150) NOT NULL,
	[email] [nvarchar](254) NULL,
	[max_occupancy] [int] NOT NULL,
	[active] [bit] NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcenterrooms]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenterrooms](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[room_data_id] [int] NOT NULL,
	[test_center_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[test_center_rooms_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[test_center_rooms_vw] AS
                SELECT
                    r.id,
                    r.test_center_id,
                    rd.name,
                    rd.email,
                    rd.max_occupancy,
                    rd.active
                FROM master..custom_models_testcenterrooms r
                JOIN master..custom_models_testcenterroomdata rd on rd.id = r.room_data_id
        
GO
/****** Object:  Table [dbo].[CAT_REF_COUNTRY_VW]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_COUNTRY_VW](
	[CNTRY_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](64) NULL,
	[FABRV] [nvarchar](64) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetimeoffset](7) NOT NULL,
	[XDT] [datetimeoffset](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[CNTRY_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[test_center_test_sessions_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[test_center_test_sessions_vw] AS
                SELECT
                    CAST(ts.id as varchar) as 'id',
                    ts.test_center_id,
                    tc.name as 'test_center_name',
                    tc.postal_code as 'test_center_postal_code',
                    ts.test_session_order,
                    ts.assessment_process_assigned_test_specs_id,
                    tc.security_email as 'test_center_security_email',
                    tsd.test_center_room_id,
                    rv.name as 'room_name',
                    IIF(ts.is_standard = 1, crc.count, NULL) as 'spaces_booked',
                    IIF(ts.is_standard = 1, (tsd.spaces_available-crc.count), NULL) as 'spaces_unbooked',
                    tsd.spaces_available as 'session_spaces_available',
                    rv.max_occupancy as 'room_max_occupancy',
                    tsd.id as 'test_center_test_session_data_id',
                    tsd.open_to_ogd,
                    tc.country_id,
                    c.EABRV as 'country_eabrv',
                    c.EDESC as 'country_edesc',
                    c.FABRV as 'country_fabrv',
                    c.FDESC as 'country_fdesc',
                    tc.department_id,
                    dep.EABRV as 'dept_eabrv',
                    dep.EDESC as 'dept_edesc',
                    dep.FABRV as 'dept_fabrv',
                    dep.FDESC as 'dept_fdesc',
                    DATEADD(hour, -tc.booking_delay, tsd.start_time) as 'latest_booking_modification_time',
                    tc.booking_delay as 'booking_delay',
                    tsd.date,
                    tsd.start_time,
                    tsd.end_time,
                    tsd.spaces_available,
                    ta_u.id as 'test_assessor_user_id',
                    ta_u.email as 'test_assessor_user_email',
                    ta_u.first_name as 'test_assessor_user_first_name',
                    ta_u.last_name as 'test_assessor_user_last_name',
                    tsd.candidate_phone_number,
                    tsd.test_skill_type_id,
                    tst.codename as 'test_skill_type_codename',
                    stte.text as 'test_skill_type_en_name',
                    sttf.text as 'test_skill_type_fr_name',
                    tsd.test_skill_sub_type_id,
                    apatsv.test_skill_sub_type_codename,
                    CASE
                        -- SLE Sub Type
                        WHEN tsd.test_skill_type_id = (SELECT id FROM master..cms_models_testskilltype WHERE codename = 'sle') THEN (SELECT text FROM master..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = tsd.test_skill_sub_type_id AND language_id = 1)
                        ELSE
                            -- OCCUPATIONAL Sub Type
                            CASE
                                WHEN tsd.test_skill_type_id = (SELECT id FROM master..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM master..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = tsd.test_skill_sub_type_id AND language_id = 1)
                            ELSE
                                NULL
                            END
                    END AS 'test_skill_sub_type_en_name',
                    CASE
                        -- SLE Sub Type
                        WHEN tsd.test_skill_type_id = (SELECT id FROM master..cms_models_testskilltype WHERE codename = 'SLE') THEN (SELECT text FROM master..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = tsd.test_skill_sub_type_id AND language_id = 2)
                        ELSE
                            -- OCCUPATIONAL Sub Type
                            CASE
                                WHEN tsd.test_skill_type_id = (SELECT id FROM master..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM master..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = tsd.test_skill_sub_type_id AND language_id = 2)
                            ELSE
                                NULL
                            END
                    END AS 'test_skill_sub_type_fr_name',
                    CASE
                        WHEN tst.codename = 'sle' AND apatsv.test_skill_sub_type_codename in ('oe', 're', 'we') THEN 1
                        WHEN tst.codename = 'sle' AND apatsv.test_skill_sub_type_codename in ('of', 'rf', 'wf') THEN 2
                        ELSE NULL
                    END AS 'language_id',
                    ts.is_standard,
                    ts.user_accommodation_file_id,
                    -- is_alternate_test_request
                    0 as 'is_alternate_test_request',
                    -- user_id
                    NULL as 'user_id',
                    -- user_username
                    NULL as 'user_username',
                    -- user_email
                    NULL as 'user_email',
                    -- user_first_name
                    NULL as 'user_first_name',
                    -- user_last_name 
                    NULL as 'user_last_name',
                    apatsv.assessment_process_closing_date as 'closing_date',
                    td.id as 'test_id',
                    td.parent_code as 'parent_code',
                    td.test_code as 'test_code',
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    td.version as 'version',
                    uaftta.specified_test_description,
					vtms.meeting_link 'meeting_link'
                FROM master..custom_models_testcentertestsessions ts
                LEFT JOIN master..custom_models_testcenter tc on tc.id = ts.test_center_id
                JOIN master..custom_models_testcentertestsessiondata tsd on tsd.id = ts.test_session_data_id
                LEFT JOIN master..user_management_models_user ta_u on ta_u.id = tsd.test_assessor_user_id
                LEFT JOIN master..CAT_REF_DEPARTMENTS_VW dep on dep.DEPT_ID = tc.department_id
                LEFT JOIN master..CAT_REF_COUNTRY_VW c on c.CNTRY_ID = tc.country_id
                LEFT JOIN master..test_center_rooms_vw rv on rv.id = tsd.test_center_room_id
                JOIN master..cms_models_testskilltype tst on tst.id = tsd.test_skill_type_id
                JOIN master..cms_models_testskilltypetext stte on stte.test_skill_type_id = tsd.test_skill_type_id AND stte.language_id = 1
                JOIN master..cms_models_testskilltypetext sttf on sttf.test_skill_type_id = tsd.test_skill_type_id AND sttf.language_id = 2
                LEFT JOIN master..custom_models_useraccommodationfiletesttoadminister uaftta on uaftta.user_accommodation_file_id = ts.user_accommodation_file_id
                LEFT JOIN master..cms_models_testdefinition td on td.id = uaftta.test_id
                LEFT JOIN master..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = ts.assessment_process_assigned_test_specs_id
				LEFT JOIN master..custom_models_virtualteamsmeetingsession vtms ON ts.id = vtms.test_session_id
                OUTER APPLY (SELECT COUNT(*) as count FROM master..custom_models_consumedreservationcodes WHERE test_session_id=ts.id) crc
                WHERE ts.is_standard = 1

                UNION

                -- Accommodation Requests
                SELECT
                    -- Show testsession ID if exists, otherwise show consumed reservation code ID with "-CRC" as suffix (this is fixing potential duplicated IDs which might cause candidates to have unrelated sessions/accommodation requests associated to their manage reservations table)
                    IIF(crc.test_session_id is not NULL, CAST(ts.id as varchar), CAST(crc.id as varchar) + '-CRC') as 'id',
                    tc.id,
                    tc.name as 'test_center_name',
                    tc.postal_code as 'test_center_postal_code',
                    ts.test_session_order,
                    ts.assessment_process_assigned_test_specs_id,
                    tc.security_email as 'test_center_security_email',
                    IIF(crc.test_session_id is not NULL, tsd.test_center_room_id, NULL) as 'test_center_room_id',
                    IIF(crc.test_session_id is not NULL, rv.name , NULL) as 'room_name',
                    -- spaced_booked
                    NULL,
                    -- spaces_unbooked
                    NULL,
                    -- session_spaces_available
                    NULL,
                    -- room_max_occupancy
                    NULL,
                    IIF(crc.test_session_id is not NULL, tsd.id, NULL) as 'test_center_test_session_data_id',
                    -- open_to_ogd
                    NULL,
                    tc.country_id,
                    c.EABRV as 'country_eabrv',
                    c.EDESC as 'country_edesc',
                    c.FABRV as 'country_fabrv',
                    c.FDESC as 'country_fdesc',
                    tc.department_id,
                    dep.EABRV as 'dept_eabrv',
                    dep.EDESC as 'dept_edesc',
                    dep.FABRV as 'dept_fabrv',
                    dep.FDESC as 'dept_fdesc',
                    --DATEADD(hour, -tc.booking_delay, tsd.start_time) as 'latest_booking_modification_time',
                    NULL,
                    tc.booking_delay as 'booking_delay',
                    IIF(crc.test_session_id is not NULL, tsd.date, NULL) as 'date',
                    IIF(crc.test_session_id is not NULL, tsd.start_time, NULL) as 'start_time',
                    IIF(crc.test_session_id is not NULL, tsd.end_time, NULL) as 'end_time',
                    --tsd.spaces_available,
                    NULL,
                    ta_u.id as 'test_assessor_user_id',
                    ta_u.email as 'test_assessor_user_email',
                    ta_u.first_name as 'test_assessor_user_first_name',
                    ta_u.last_name as 'test_assessor_user_last_name',
                    tsd.candidate_phone_number,
                    apats.test_skill_type_id,
                    tst.codename as 'test_skill_type_codename',
                    stte.text as 'test_skill_type_en_name',
                    sttf.text as 'test_skill_type_fr_name',
                    apats.test_skill_sub_type_id,
                    apatsv.test_skill_sub_type_codename,
                    CASE
                        -- SLE Sub Type
                        WHEN apats.test_skill_type_id = (SELECT id FROM master..cms_models_testskilltype WHERE codename = 'sle') THEN (SELECT text FROM master..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = apats.test_skill_sub_type_id AND language_id = 1)
                        ELSE
                            -- OCCUPATIONAL Sub Type
                            CASE
                                WHEN apats.test_skill_type_id = (SELECT id FROM master..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM master..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = apats.test_skill_sub_type_id AND language_id = 1)
                            ELSE
                                NULL
                            END
                    END AS 'test_skill_sub_type_en_name',
                    CASE
                        -- SLE Sub Type
                        WHEN apats.test_skill_type_id = (SELECT id FROM master..cms_models_testskilltype WHERE codename = 'SLE') THEN (SELECT text FROM master..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = apats.test_skill_sub_type_id AND language_id = 2)
                        ELSE
                            -- OCCUPATIONAL Sub Type
                            CASE
                                WHEN apats.test_skill_type_id = (SELECT id FROM master..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM master..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = apats.test_skill_sub_type_id AND language_id = 2)
                            ELSE
                                NULL
                            END
                    END AS 'test_skill_sub_type_fr_name',
                    CASE
                        WHEN tst.codename = 'sle' AND apatsv.test_skill_sub_type_codename in ('oe', 're', 'we') THEN 1
                        WHEN tst.codename = 'sle' AND apatsv.test_skill_sub_type_codename in ('of', 'rf', 'wf') THEN 2
                        ELSE NULL
                    END AS 'language_id',
                    0 as 'is_standard',
                    uaf.id as 'user_accommodation_file_id',
                    uaf.is_alternate_test_request,
                    u.id as 'user_id',
                    u.username as 'user_username',
                    u.email as 'user_email',
                    u.first_name as 'user_first_name',
                    u.last_name as 'user_last_name ',
                    apatsv.assessment_process_closing_date as 'closing_date',
                    td.id as 'test_id',
                    td.parent_code as 'parent_code',
                    td.test_code as 'test_code',
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    td.version as 'version',
                    uaftta.specified_test_description,
					vtms.meeting_link 'meeting_link'
                FROM master..custom_models_consumedreservationcodes crc
                JOIN master..custom_models_useraccommodationfile uaf on crc.user_accommodation_file_id = uaf.id
                JOIN master..user_management_models_user u on u.id = uaf.user_id
                LEFT JOIN master..custom_models_testcenter tc on tc.id = uaf.test_center_id
                LEFT JOIN master..CAT_REF_DEPARTMENTS_VW dep on dep.DEPT_ID = tc.department_id
                LEFT JOIN master..CAT_REF_COUNTRY_VW c on c.CNTRY_ID = tc.country_id
                JOIN master..custom_models_assessmentprocessassignedtestspecs apats on apats.id = crc.assessment_process_assigned_test_specs_id
                JOIN master..cms_models_testskilltype tst on tst.id = apats.test_skill_type_id
                JOIN master..cms_models_testskilltypetext stte on stte.test_skill_type_id = apats.test_skill_type_id AND stte.language_id = 1
                JOIN master..cms_models_testskilltypetext sttf on sttf.test_skill_type_id = apats.test_skill_type_id AND sttf.language_id = 2
                LEFT JOIN master..custom_models_testcentertestsessions ts on ts.assessment_process_assigned_test_specs_id = crc.assessment_process_assigned_test_specs_id
                LEFT JOIN master..custom_models_testcentertestsessiondata tsd on tsd.id = ts.test_session_data_id
                LEFT JOIN master..user_management_models_user ta_u on ta_u.id = tsd.test_assessor_user_id
                LEFT JOIN master..test_center_rooms_vw rv on rv.id = tsd.test_center_room_id
                LEFT JOIN master..custom_models_useraccommodationfiletesttoadminister uaftta on uaftta.user_accommodation_file_id = uaf.id
                LEFT JOIN master..cms_models_testdefinition td on td.id = uaftta.test_id
                LEFT JOIN master..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = crc.assessment_process_assigned_test_specs_id
				LEFT JOIN master..custom_models_virtualteamsmeetingsession vtms ON ts.id = vtms.test_session_id
                
GO
/****** Object:  View [dbo].[aae_reports_number_of_requests_received_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[aae_reports_number_of_requests_received_vw] AS
                SELECT
                    -- generating ID by converting the created_date in timestamp
                    DATEDIFF(SECOND, '1970-01-01', uaf.created_date) as 'id',
                    uaf.created_date,
                    COUNT(uaf.id) as 'number_of_requests'
                FROM master..custom_models_useraccommodationfile uaf
                GROUP BY uaf.created_date
    
GO
/****** Object:  Table [dbo].[user_management_models_customuserpermissions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_customuserpermissions](
	[user_permission_id] [int] IDENTITY(1,1) NOT NULL,
	[permission_id] [int] NOT NULL,
	[goc_email] [nvarchar](254) NOT NULL,
	[pri] [nvarchar](10) NOT NULL,
	[rationale] [nvarchar](300) NOT NULL,
	[supervisor] [nvarchar](180) NOT NULL,
	[supervisor_email] [nvarchar](254) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[reason_for_modif_or_del] [nvarchar](300) NULL,
	[military_nbr] [nvarchar](9) NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[user_permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [one_user_id_per_permission_id_instance] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_ettaactions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_ettaactions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[action_reason] [nvarchar](300) NULL,
	[action_type_id] [nvarchar](25) NOT NULL,
	[assigned_test_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[user_look_up_tests_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[user_look_up_tests_vw] AS
                SELECT
					at.id,
					ats1.id as 'status_id',
					ats1.codename as 'status_codename',
					ats2.id as 'previous_status_id',
					ats2.codename as 'previous_status_codename',
					at.start_date,
					CONVERT(VARCHAR(10), at.start_date, 23) as 'simplified_start_date',
					at.modify_date,
					at.submit_date,
					CONVERT(VARCHAR(10), at.submit_date, 23) as 'simplified_submit_date',
					at.test_access_code,
					at.total_score,
					td.id as 'test_id',
					td.version as 'test_version',
					td.parent_code,
					td.test_code,
					td.en_name as 'test_name_en',
					td.fr_name as 'test_name_fr',
					td.retest_period,
					at.ta_user_id,
					ta.username as 'ta_username',
					ta.email as 'ta_email',
					IIF(historical_permission_data.goc_email IS NOT NULL, historical_permission_data.goc_email, permission_data.goc_email) as 'ta_goc_email',
					ta.first_name as 'ta_first_name',
					ta.last_name as 'ta_last_name',
					at.test_section_id,
					at.test_session_language_id,
					u.id as 'candidate_user_id',
					u.username as 'candidate_username',
					u.email as 'candidate_email',
					u.first_name as 'candidate_first_name',
					u.last_name as 'candidate_last_name',
					at.test_order_number,
					at.en_converted_score,
					at.fr_converted_score,
					at.is_invalid,
					IIF(at.is_invalid = 1, etta_invalidate_action.reason, NULL) as 'invalidate_test_reason',
					at.uit_invite_id,
					at.orderless_financial_data_id,
					at.accommodation_request_id,
					at.test_session_id,
					CASE
						WHEN at.orderless_financial_data_id IS NOT NULL THEN ofd.reference_number
						WHEN at.test_order_number IS NOT NULL THEN historical_test_permission_data.staffing_process_number
					ELSE
						NULL
					END as 'assessment_process_reference_nbr',
					CASE
						-- UIT TEST
						WHEN at.uit_invite_id is not NULL THEN ui.validity_end_date
						-- SUPERVISED TEST
						ELSE apatsv.assessment_process_closing_date
					END as 'validity_end_date_or_process_closing_date'
				FROM master..custom_models_assignedtest at
				JOIN master..user_management_models_user u on u.id = at.user_id
				JOIN master..user_management_models_user ta on ta.id = at.ta_user_id
				JOIN master..cms_models_testdefinition td on td.id = at.test_id
				LEFT JOIN master..custom_models_assignedteststatus ats1 on ats1.id = at.status_id
				LEFT JOIN master..custom_models_assignedteststatus ats2 on ats2.id = at.previous_status_id
				OUTER APPLY (SELECT TOP 1 EA.action_reason AS 'reason' FROM master..custom_models_ettaactions ea WHERE ea.assigned_test_id = at.id AND ea.action_type_id = 'UN_ASSIGN_CANDIDATE' ORDER BY ea.modify_date DESC) etta_invalidate_action
				LEFT JOIN master..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
				OUTER APPLY (SELECT TOP 1 htp.staffing_process_number FROM master..cms_models_historicaltestpermissions htp WHERE htp.user_id = ta.id AND htp.test_id = td.id AND htp.test_order_number = at.test_order_number AND htp.history_type = '+' ORDER BY htp.history_date DESC) historical_test_permission_data
				OUTER APPLY (SELECT TOP 1 hcup.goc_email FROM master..user_management_models_historicalcustomuserpermissions hcup WHERE hcup.user_id = ta.id AND hcup.permission_id = (SELECT cp.permission_id FROM master..user_management_models_custompermissions cp WHERE cp.codename = 'is_test_administrator') AND hcup.history_date < at.start_date AND hcup.history_type = '+' ORDER BY hcup.history_date DESC) historical_permission_data
				OUTER APPLY (SELECT TOP 1 cup.goc_email FROM master..user_management_models_customuserpermissions cup WHERE cup.user_id = ta.id AND cup.permission_id = (SELECT cp.permission_id FROM master..user_management_models_custompermissions cp WHERE cp.codename = 'is_test_administrator')) permission_data
				LEFT JOIN master..custom_models_uitinvites ui on ui.id = at.uit_invite_id
				LEFT JOIN master..custom_models_consumedreservationcodes crc on crc.assigned_test_id = at.id
				LEFT JOIN master..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = crc.assessment_process_assigned_test_specs_id
            
GO
/****** Object:  View [dbo].[user_accommodation_file_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[user_accommodation_file_vw] AS
                SELECT
                    uaf.id,
                    uaf.comments,
                    uaf.is_uit,
                    uaf.modify_date,
                    uaf.created_date,
                    CASE
                        -- Supervised Test (getting reference number from assessment process data)
                        WHEN uaf.is_uit = 0 THEN apatsv.assessment_process_closing_date
                        -- UIT Test
                        ELSE ui.validity_end_date
                    END as 'process_end_date',
                    uaf.last_modified_by_user_id,
                    uaf.is_alternate_test_request,
                    uafs.id as 'status_id',
                    uafs.codename as 'status_codename',
                    uafst_en.text as 'status_name_en',
                    uafst_fr.text as 'status_name_fr',
                    tc.id as 'test_center_id',
	                tc.name as 'test_center_name',
                    ummu.id as 'user_id',
                    ummu.username as 'user_username',
                    ummu.email as 'user_email',
                    ummu.first_name as 'user_first_name',
                    ummu.last_name as 'user_last_name',
                    at_u.id as 'assigned_to_user_id',
                    at_u.email as 'assigned_to_user_email',
                    at_u.first_name as 'assigned_to_user_first_name',
                    at_u.last_name 'assigned_to_user_last_name',
                    -- TEST SKILL TYPE ID
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN ts.test_skill_type_id
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_id
                    END as 'test_skill_type_id',
                    -- TEST SKILL TYPE CODENAME
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN tst.codename
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_codename
                    END as 'test_skill_type_codename',
                    -- TEST SKILL TYPE NAME (EN)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN type_e.text
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_name_en
                    END as 'test_skill_type_name_en',
                    -- TEST SKILL TYPE NAME (FR)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN type_f.text
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_name_fr
                    END as 'test_skill_type_name_fr',
                    -- TEST SKILL SUB-TYPE ID
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sled.id
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occd.id
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_id
                    END as 'test_skill_sub_type_id',
                    -- TEST SKILL SUB-TYPE CODENAME
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sled.codename
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occd.codename
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_codename
                    END as 'test_skill_sub_type_codename',
                    -- TEST SKILL SUB-TYPE NAME (EN)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sle_sub_type_e.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occ_sub_type_e.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_name_en
                    END as 'test_skill_sub_type_name_en',
                    -- TEST SKILL SUB-TYPE NAME (FR)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sle_sub_type_f.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occ_sub_type_f.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_name_fr
                    END as 'test_skill_sub_type_name_fr'
                FROM master..custom_models_useraccommodationfile uaf
                JOIN master..user_management_models_user ummu ON ummu.id = uaf.user_id
                JOIN master..custom_models_useraccommodationfilestatus uafs ON uafs.id = uaf.status_id
                JOIN master..custom_models_useraccommodationfilestatustext uafst_en on uafst_en.user_accommodation_file_status_id = uafs.id AND uafst_en.language_id = 1
                JOIN master..custom_models_useraccommodationfilestatustext uafst_fr on uafst_fr.user_accommodation_file_status_id = uafs.id AND uafst_fr.language_id = 2
                LEFT JOIN master..custom_models_testcenter tc on tc.id = uaf.test_center_id
                LEFT JOIN master..user_management_models_user at_u on at_u.id = uaf.assigned_to_user_id
                -- TEST SKILL TYPE / SUB-TYPE DATA (UIT TESTS)
                OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_assignedtest at_1 WHERE at_1.user_accommodation_file_id = uaf.id ORDER BY at_1.id DESC) at
                LEFT JOIN master..cms_models_testskill ts on ts.test_definition_id = at.test_id
                LEFT JOIN master..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM master..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM master..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN master..cms_models_testskillsle tssle on tssle.test_skill_id = ts.id
                LEFT JOIN master..cms_models_testskillsledesc sled on sled.id = tssle.test_skill_sle_desc_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM master..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM master..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                LEFT JOIN master..cms_models_testskilloccupational tsocc on tsocc.test_skill_id = ts.id
                LEFT JOIN master..cms_models_testskilloccupationaldesc occd on occd.id = tsocc.test_skill_occupational_desc_id
                OUTER APPLY (SELECT TOP 1 occdte.text FROM master..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = occd.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM master..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = occd.id AND occdtf.language_id = 2) occ_sub_type_f
                -- TEST SKILL TYPE / SUB-TYPE DATA (SUPERVISED TESTS)
                OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_historicalconsumedreservationcodes hcrc_1 WHERE hcrc_1.user_accommodation_file_id = uaf.id ORDER BY hcrc_1.history_date DESC) hcrc
                LEFT JOIN master..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = hcrc.assessment_process_assigned_test_specs_id
                LEFT JOIN master..custom_models_uitinvites ui on ui.id = at.uit_invite_id
    
GO
/****** Object:  Table [dbo].[custom_models_testcenterolatimeslotprioritization]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenterolatimeslotprioritization](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[percentage] [int] NOT NULL,
	[reason_for_testing_priority_id] [int] NOT NULL,
	[test_center_ola_time_slot_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_DAY_OF_WEEK_VW]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_DAY_OF_WEEK_VW](
	[id] [int] NOT NULL,
	[day_of_week_id] [int] NOT NULL,
	[text] [nvarchar](50) NOT NULL,
	[language_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_language]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_language](
	[language_id] [int] IDENTITY(1,1) NOT NULL,
	[ISO_Code_1] [nvarchar](2) NOT NULL,
	[ISO_Code_2] [nvarchar](5) NOT NULL,
	[date_created] [datetime2](7) NOT NULL,
	[date_from] [datetime2](7) NOT NULL,
	[date_to] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[language_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[ISO_Code_1] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_languagetext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_languagetext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](50) NOT NULL,
	[language_id] [int] NOT NULL,
	[language_ref_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[test_center_ola_time_slots_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[test_center_ola_time_slots_vw] AS
                SELECT
                    ts.id,
                    ts.start_time,
                    ts.end_time,
                    ts.utc_offset,
                    ts.availability,
					ts.slots_to_prioritize,
                    l.language_id as 'assessed_language_id',
                    assessed_language_text_en.text as 'assessed_language_text_en',
                    assessed_language_text_fr.text as 'assessed_language_text_fr',
                    ts.day_of_week_id,
                    day_of_week_text_en.text as 'day_of_week_text_en',
                    day_of_week_text_fr.text as 'day_of_week_text_fr',
                    ts.test_center_id,
                    number_of_selected_assessors.count as 'number_of_selected_assessors',
					-- available slots (high + medium + low)
					(number_of_selected_assessors.count * (SELECT * FROM master..get_ola_prioritization_value_by_codename_fn('high')) / 100) + (number_of_selected_assessors.count * (SELECT * FROM master..get_ola_prioritization_value_by_codename_fn('medium')) / 100) + (number_of_selected_assessors.count * (SELECT * FROM master..get_ola_prioritization_value_by_codename_fn('low')) / 100) as 'number_of_available_slots',
					-- NO LONGER USED AT THE MOMENT, SINCE PRIORITIZATION FROM BO OLA CONFIGS IS NOW USED INSTEAD
					IIF(prioritization_high.percentage is not NULL, prioritization_high.percentage, 0) as 'prioritization_high',
					IIF(prioritization_medium.percentage is not NULL, prioritization_medium.percentage, 0) as 'prioritization_medium',
					IIF(prioritization_low.percentage is not NULL, prioritization_low.percentage, 0) as 'prioritization_low'
                FROM master..custom_models_testcenterolatimeslot ts
                JOIN master..custom_models_language l on l.language_id = ts.assessed_language_id
                OUTER APPLY (SELECT lt1.text FROM master..custom_models_languagetext lt1 WHERE lt1.language_ref_id = l.language_id AND lt1.language_id = 1) assessed_language_text_en
                OUTER APPLY (SELECT lt2.text FROM master..custom_models_languagetext lt2 WHERE lt2.language_ref_id = l.language_id AND lt2.language_id = 2) assessed_language_text_fr
                OUTER APPLY (SELECT dowen.text FROM master..CAT_REF_DAY_OF_WEEK_VW dowen WHERE dowen.day_of_week_id = ts.day_of_week_id AND dowen.language_id = 1) day_of_week_text_en
                OUTER APPLY (SELECT dowfr.text FROM master..CAT_REF_DAY_OF_WEEK_VW dowfr WHERE dowfr.day_of_week_id = ts.day_of_week_id AND dowfr.language_id = 2) day_of_week_text_fr
				OUTER APPLY (SELECT COUNT(*) as 'count' FROM master..custom_models_testcenterolatimeslotassessoravailability tcotsaa WHERE tcotsaa.test_center_ola_time_slot_id = ts.id) number_of_selected_assessors
                OUTER APPLY (SELECT tctsp1.percentage FROM master..custom_models_testcenterolatimeslotprioritization tctsp1 WHERE tctsp1.test_center_ola_time_slot_id = ts.id AND tctsp1.reason_for_testing_priority_id = (SELECT rftp1.id FROM master..custom_models_reasonfortestingpriority rftp1 WHERE rftp1.codename = 'high')) prioritization_high
				OUTER APPLY (SELECT tctsp2.percentage FROM master..custom_models_testcenterolatimeslotprioritization tctsp2 WHERE tctsp2.test_center_ola_time_slot_id = ts.id AND tctsp2.reason_for_testing_priority_id = (SELECT rftp2.id FROM master..custom_models_reasonfortestingpriority rftp2 WHERE rftp2.codename = 'medium')) prioritization_medium
				OUTER APPLY (SELECT tctsp3.percentage FROM master..custom_models_testcenterolatimeslotprioritization tctsp3 WHERE tctsp3.test_center_ola_time_slot_id = ts.id AND tctsp3.reason_for_testing_priority_id = (SELECT rftp3.id FROM master..custom_models_reasonfortestingpriority rftp3 WHERE rftp3.codename = 'low')) prioritization_low
    
GO
/****** Object:  Table [dbo].[user_management_models_userprofilechangerequest]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_userprofilechangerequest](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[current_first_name] [nvarchar](30) NOT NULL,
	[current_last_name] [nvarchar](150) NOT NULL,
	[current_birth_date] [date] NOT NULL,
	[new_first_name] [nvarchar](30) NULL,
	[new_last_name] [nvarchar](150) NULL,
	[new_birth_date] [date] NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[comments] [nvarchar](max) NULL,
	[reason_for_deny] [nvarchar](255) NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[user_profile_change_request_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[user_profile_change_request_vw] AS
                SELECT
                    cr.id,
                    cr.user_id,
                    cr.current_first_name,
                    cr.current_last_name,
                    cr.current_birth_date,
                    cr.new_first_name,
                    cr.new_last_name,
                    cr.new_birth_date,
                    cr.modify_date,
                    cr.comments,
                    cr.reason_for_deny,
                    u.username,
                    u.email,
                    u.pri,
                    u.military_nbr
                FROM master..user_management_models_userprofilechangerequest cr
                JOIN master..user_management_models_user u ON u.id = cr.user_id
        
GO
/****** Object:  View [dbo].[completed_supervised_user_accommodation_file_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[completed_supervised_user_accommodation_file_vw] AS
                SELECT
                    uaf.id,
                    uaf.comments,
                    uaf.is_uit,
                    uaf.modify_date,
                    uaf.created_date,
                    uaf.last_modified_by_user_id,
                    uaf.is_alternate_test_request,
                    uafs.id as 'status_id',
                    uafs.codename as 'status_codename',
                    uafst_en.text as 'status_name_en',
                    uafst_fr.text as 'status_name_fr',
                    uaf.test_center_id,
                    ummu.id as 'user_id',
                    ummu.username as 'user_username',
                    ummu.email as 'user_email',
                    ummu.first_name as 'user_first_name',
                    ummu.last_name as 'user_last_name',
                    at_u.id as 'assigned_to_user_id',
                    at_u.email as 'assigned_to_user_email',
                    at_u.first_name as 'assigned_to_user_first_name',
                    at_u.last_name 'assigned_to_user_last_name',
                    crc.id as 'consumed_reservation_code_id',
                    apatsv.id as 'assessment_process_assigned_test_specs_id',
                    apatsv.assessment_process_closing_date as 'closing_date',
                    apatsv.assessment_process_dept_id as 'requesting_dept_id',
                    apatsv.assessment_process_dept_eabrv as 'requesting_dept_abrv_en',
                    apatsv.assessment_process_dept_fabrv as 'requesting_dept_abrv_fr',
                    apatsv.assessment_process_dept_edesc as 'requesting_dept_desc_en',
                    apatsv.assessment_process_dept_fdesc as 'requesting_dept_desc_fr',
                    apatsv.test_skill_type_id,
                    apatsv.test_skill_type_codename,
                    apatsv.test_skill_type_name_en,
                    apatsv.test_skill_type_name_fr,
                    apatsv.test_skill_sub_type_id,
                    apatsv.test_skill_sub_type_codename,
                    apatsv.test_skill_sub_type_name_en,
                    apatsv.test_skill_sub_type_name_fr,
                    td.id as 'test_id',
                    td.parent_code as 'parent_code',
	                td.test_code as 'test_code',
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    td.version as 'version',
                    uaftta.specified_test_description
                FROM master..custom_models_useraccommodationfile uaf
                JOIN master..user_management_models_user ummu ON ummu.id = uaf.user_id
                JOIN master..custom_models_useraccommodationfilestatus uafs ON uafs.id = uaf.status_id
                JOIN master..custom_models_useraccommodationfilestatustext uafst_en on uafst_en.user_accommodation_file_status_id = uafs.id AND uafst_en.language_id = 1
                JOIN master..custom_models_useraccommodationfilestatustext uafst_fr on uafst_fr.user_accommodation_file_status_id = uafs.id AND uafst_fr.language_id = 2
                JOIN master..custom_models_consumedreservationcodes crc on crc.user_accommodation_file_id = uaf.id
                JOIN master..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = crc.assessment_process_assigned_test_specs_id
                JOIN master..custom_models_useraccommodationfiletesttoadminister uaftta on uaftta.user_accommodation_file_id = crc.user_accommodation_file_id
                LEFT JOIN master..cms_models_testdefinition td on td.id = uaftta.test_id
                LEFT JOIN master..user_management_models_user at_u on at_u.id = uaf.assigned_to_user_id
                LEFT JOIN master..custom_models_testcentertestsessions tcts on tcts.user_accommodation_file_id = uaf.id
                -- GETTING ONLY UITS THAT ARE COMPLETED WHERE THERE IS NO ASSOCIATED TEST SESSION YET
                WHERE uaf.is_uit = 0 AND uafs.codename = 'completed' AND tcts.user_accommodation_file_id is NULL
    
GO
/****** Object:  Table [dbo].[custom_models_testcentercitytext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcentercitytext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](150) NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[language_id] [int] NOT NULL,
	[test_center_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[test_center_ola_test_assessor_unavailability_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[test_center_ola_test_assessor_unavailability_vw] AS
                SELECT
                    tcoau.id as 'id',
                    tcoau.test_center_ola_test_assessor_id as 'test_center_ola_test_assessor_id',
                    tcota.user_id as 'assessor_id',
                    tcota.supervisor as 'is_supervisor',
                    u.first_name as 'first_name',
                    u.last_name as 'last_name',
                    u.email as 'email',
                    tcota.test_center_id as 'test_center_id',
                    tc.name as 'test_center_name',
                    tccte.text as 'test_center_city_en',
                    tcctf.text as 'test_center_city_fr',
                    tcoau.start_date as 'start_date',
                    tcoau.end_date as 'end_date',
                    tcoau.reason as 'reason'

                    FROM master..custom_models_testcenterolaassessorunavailability tcoau
                    LEFT JOIN master..custom_models_testcenterolatestassessor tcota on tcota.id = tcoau.test_center_ola_test_assessor_id
                    LEFT JOIN master..user_management_models_user u on u.id = tcota.user_id
                    LEFT JOIN master..custom_models_testcenter tc on tc.id = tcota.test_center_id
                    LEFT JOIN master..custom_models_testcentercitytext tccte on tcota.test_center_id = tccte.test_center_id AND tccte.language_id = 1
                    LEFT JOIN master..custom_models_testcentercitytext tcctf on tcota.test_center_id = tcctf.test_center_id AND tcctf.language_id = 2
                
GO
/****** Object:  View [dbo].[user_look_up_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[user_look_up_vw] AS
				SELECT
                    u.id as 'user_id',
                    u.first_name,
                    u.last_name,
                    u.birth_date as 'date_of_birth',
                    u.email,
                    u.username
                FROM master..user_management_models_user u
        
GO
/****** Object:  View [dbo].[assigned_tests_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[assigned_tests_vw] AS
                SELECT
					at.id,
					ats1.id as 'status_id',
					ats1.codename as 'status_codename',
					ats2.id as 'previous_status_id',
					ats2.codename as 'previous_status_codename',
					at.start_date,
					at.modify_date,
					at.submit_date,
					at.test_access_code,
					at.total_score,
					at.test_id,
					at.ta_user_id,
					ta.username as 'ta_username',
					at.test_section_id,
					at.test_session_language_id,
					at.user_id,
					u.username,
					at.test_order_number,
					at.en_converted_score,
					at.fr_converted_score,
					at.is_invalid,
					at.uit_invite_id,
					at.orderless_financial_data_id,
					at.accommodation_request_id,
					at.test_session_id
				FROM master..custom_models_assignedtest at
				JOIN master..user_management_models_user u on u.id = at.user_id
				JOIN master..user_management_models_user ta on ta.id = at.ta_user_id
				LEFT JOIN master..custom_models_assignedteststatus ats1 on ats1.id = at.status_id
				LEFT JOIN master..custom_models_assignedteststatus ats2 on ats2.id = at.previous_status_id
            
GO
/****** Object:  View [dbo].[assessment_process_results_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[assessment_process_results_vw] AS
                SELECT
                    id,
                    user_id,
                    reference_number,
                    closing_date,
                    (SELECT COUNT(DISTINCT email) FROM master..custom_models_assessmentprocessassignedtestspecs WHERE assessment_process_id = ap.id) AS "number_of_candidates",
                    (SELECT COUNT(*)
                        FROM master..custom_models_assignedtest at
						RIGHT JOIN master..custom_models_historicalconsumedreservationcodes hcrc on hcrc.assigned_test_id = at.id
                        JOIN master..custom_models_assessmentprocessassignedtestspecs ats on ats.id = hcrc.assessment_process_assigned_test_specs_id
                        JOIN master..custom_models_assessmentprocess ap2 on ap2.id = ats.assessment_process_id
                        WHERE at.status_id in (SELECT id FROM custom_models_assignedteststatus WHERE codename in ('quit', 'submitted')) AND ap2.id = ap.id ) AS "number_of_tests_taken"
                FROM master..custom_models_assessmentprocess ap
                WHERE request_sent = 'TRUE'
            
GO
/****** Object:  View [dbo].[aae_reports_number_of_closed_requests_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[aae_reports_number_of_closed_requests_vw] AS
                SELECT
                    uaf.id,
                    uaf.created_date,
                    uafs.id as 'status_id',
                    uafs.codename as 'status_codename',
                    first_completed_by_aae_date.history_date as 'first_completed_by_aae_date',
                    first_approved_by_candidate_date.history_date as 'first_approved_by_candidate_date',
                    tta.test_id,
                    -- TEST SKILL TYPE ID
                    ts.test_skill_type_id,
                    tst.codename as 'test_skill_type_codename',
                    type_e.text as 'test_skill_type_name_en',
                    type_f.text as 'test_skill_type_name_fr',
                    -- TEST SKILL SUB-TYPE ID
                    CASE
                        WHEN ts.test_skill_type_id = 2 THEN sled.id
                        WHEN ts.test_skill_type_id = 3 THEN occd.id
                        WHEN ts.test_skill_type_id = 1 THEN NULL
                    END as 'test_skill_sub_type_id',
                    -- TEST SKILL SUB-TYPE CODENAME
                    CASE
                        WHEN ts.test_skill_type_id = 2 THEN sled.codename
                        WHEN ts.test_skill_type_id = 3 THEN occd.codename
                        WHEN ts.test_skill_type_id = 1 THEN NULL
                    END as 'test_skill_sub_type_codename',
                    -- TEST SKILL SUB-TYPE NAME (EN)
                    CASE
                        WHEN ts.test_skill_type_id = 2 THEN sle_sub_type_e.text
                        WHEN ts.test_skill_type_id = 3 THEN occ_sub_type_e.text
                        WHEN ts.test_skill_type_id = 1 THEN NULL
                    END as 'test_skill_sub_type_name_en',
                    -- TEST SKILL SUB-TYPE NAME (FR)
                    CASE
                        WHEN ts.test_skill_type_id = 2 THEN sle_sub_type_f.text
                        WHEN ts.test_skill_type_id = 3 THEN occ_sub_type_f.text
                        WHEN ts.test_skill_type_id = 1 THEN NULL
                    END as 'test_skill_sub_type_name_fr'
                FROM master..custom_models_useraccommodationfile uaf
                JOIN master..custom_models_useraccommodationfilestatus uafs on uafs.id = uaf.status_id
                JOIN master..custom_models_useraccommodationfiletesttoadminister tta on tta.user_accommodation_file_id = uaf.id
                -- GETTING FIRST COMPLETED BY AAE DATE
                OUTER APPLY (SELECT TOP 1 huaf.history_date 
                            FROM master..custom_models_historicaluseraccommodationfile huaf 
                            WHERE huaf.id = uaf.id AND huaf.status_id = (SELECT uafs_2.id 
                                                                        FROM master..custom_models_useraccommodationfilestatus uafs_2 
                                                                        WHERE uafs_2.codename = 'pending_approval'
                                                                        ) 
                            ORDER BY huaf.history_date ASC
                            ) first_completed_by_aae_date
                -- GETTING FIRST APPROVED DATE
                OUTER APPLY (SELECT TOP 1 huaf.history_date 
                            FROM master..custom_models_historicaluseraccommodationfile huaf 
                            WHERE huaf.id = uaf.id AND huaf.status_id = (SELECT uafs_2.id 
                                                                        FROM master..custom_models_useraccommodationfilestatus uafs_2 
                                                                        WHERE uafs_2.codename = 'completed'
                                                                        ) 
                            ORDER BY huaf.history_date ASC
                            ) first_approved_by_candidate_date
                LEFT JOIN master..cms_models_testskill ts on ts.test_definition_id = tta.test_id
                LEFT JOIN master..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM master..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM master..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN master..cms_models_testskillsle tssle on tssle.test_skill_id = ts.id
                LEFT JOIN master..cms_models_testskillsledesc sled on sled.id = tssle.test_skill_sle_desc_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM master..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM master..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                LEFT JOIN master..cms_models_testskilloccupational tsocc on tsocc.test_skill_id = ts.id
                LEFT JOIN master..cms_models_testskilloccupationaldesc occd on occd.id = tsocc.test_skill_occupational_desc_id
                OUTER APPLY (SELECT TOP 1 occdte.text FROM master..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = occd.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM master..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = occd.id AND occdtf.language_id = 2) occ_sub_type_f
                -- ONLY GETTING COMPLETED REQUESTS
                WHERE uafs.codename = 'completed'
    
GO
/****** Object:  Table [dbo].[custom_models_historicaluitinvites]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluitinvites](
	[id] [int] NOT NULL,
	[invite_date] [date] NOT NULL,
	[validity_end_date] [date] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[ta_test_permissions_id] [int] NULL,
	[reason_for_deletion_id] [int] NULL,
	[reason_for_modification_id] [int] NULL,
	[orderless_request] [bit] NOT NULL,
	[ta_user_id] [int] NULL,
	[test_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_unsupervisedtestaccesscode]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_unsupervisedtestaccesscode](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_access_code] [nvarchar](10) NOT NULL,
	[test_order_number] [nvarchar](12) NULL,
	[candidate_email] [nvarchar](254) NULL,
	[created_date] [date] NOT NULL,
	[validity_end_date] [date] NULL,
	[test_id] [int] NOT NULL,
	[uit_invite_id] [int] NOT NULL,
	[orderless_financial_data_id] [int] NULL,
	[accommodation_request_id] [int] NULL,
	[ta_user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[test_access_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalunsupervisedtestaccesscode]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalunsupervisedtestaccesscode](
	[id] [int] NOT NULL,
	[test_access_code] [nvarchar](10) NOT NULL,
	[test_order_number] [nvarchar](12) NULL,
	[candidate_email] [nvarchar](254) NULL,
	[created_date] [date] NOT NULL,
	[validity_end_date] [date] NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_id] [int] NULL,
	[uit_invite_id] [int] NULL,
	[orderless_financial_data_id] [int] NULL,
	[accommodation_request_id] [int] NULL,
	[ta_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[uit_completed_processes_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[uit_completed_processes_vw] AS
                SELECT
                    hui.id,
                    hui.invite_date,
                    hui.validity_end_date,
                    hui.orderless_request,
                    u.id as 'ta_user_id',
                    u.email as 'ta_email',
                    u.first_name as 'ta_first_name',
                    u.last_name as 'ta_last_name',
                    htp.id as 'ta_test_permissions_id',
                    IIF (hui.ta_test_permissions_id is not NULL, htp.staffing_process_number, orderless_financial_data.reference_number) as 'assessment_process_or_reference_nbr',
                    -- ==================== DEPARTMENT DATA ====================
                    IIF (hui.ta_test_permissions_id is not NULL,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN test_permission_dep.DEPT_ID is not NULL THEN test_permission_dep.DEPT_ID
                            -- PSC DEPT_ID
                            ELSE 104
                        END,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN orderless_dep.DEPT_ID is not NULL THEN orderless_dep.DEPT_ID
                            -- PSC DEPT_ID
                            ELSE 104
                        END
                        ) as 'dept_id',
                    IIF (hui.ta_test_permissions_id is not NULL,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN test_permission_dep.DEPT_ID is not NULL THEN test_permission_dep.EABRV
                            -- PSC DEPT_ID
                            ELSE 'PSC'
                        END,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN orderless_dep.DEPT_ID is not NULL THEN orderless_dep.EABRV
                            -- PSC DEPT_ID
                            ELSE 'PSC'
                        END
                        ) as 'dept_eabrv',
                    IIF (hui.ta_test_permissions_id is not NULL,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN test_permission_dep.DEPT_ID is not NULL THEN test_permission_dep.FABRV
                            -- PSC DEPT_ID
                            ELSE 'CFP'
                        END,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN orderless_dep.DEPT_ID is not NULL THEN orderless_dep.FABRV
                            -- PSC DEPT_ID
                            ELSE 'CFP'
                        END
                        ) as 'dept_fabrv',
                    IIF (hui.ta_test_permissions_id is not NULL,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN test_permission_dep.DEPT_ID is not NULL THEN test_permission_dep.EDESC
                            -- PSC DEPT_ID
                            ELSE 'Public Service Commission'
                        END,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN orderless_dep.DEPT_ID is not NULL THEN orderless_dep.EDESC
                            -- PSC DEPT_ID
                            ELSE 'Public Service Commission'
                        END
                        ) as 'dept_edesc',
                    IIF (hui.ta_test_permissions_id is not NULL,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN test_permission_dep.DEPT_ID is not NULL THEN test_permission_dep.FDESC
                            -- PSC DEPT_ID
                            ELSE 'Commission de la fonction publique'
                        END,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN orderless_dep.DEPT_ID is not NULL THEN orderless_dep.FDESC
                            -- PSC DEPT_ID
                            ELSE 'Commission de la fonction publique'
                        END
                        ) as 'dept_fdesc',
                    -- ==================== DEPARTMENT DATA (END) ====================
                    td.id as 'test_id',
                    td.test_code,
                    td.version,
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    total_number_of_utac.count as 'total_number_of_utac',
                    number_of_remaining_utac.count as 'number_of_remaining_utac',
                    number_of_active_tests.count as 'number_of_active_tests',
                    number_of_tests_taken.count as 'number_of_tests_taken'
                FROM master..custom_models_historicaluitinvites hui
                JOIN master..user_management_models_user u on u.id = hui.ta_user_id
                LEFT JOIN master..cms_models_historicaltestpermissions htp on htp.id = hui.ta_test_permissions_id AND htp.history_type = '+'
                LEFT JOIN master..cms_models_testdefinition td on td.id = hui.test_id
                -- GETTING ORDERLESS FINANCIAL DATA (ONE TO MANY, SO ONLY GETTING THE FIRST RESULT)
                OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_orderlessfinancialdata ofd WHERE ofd.uit_invite_id = hui.id) orderless_financial_data
                LEFT JOIN master..CAT_REF_DEPARTMENTS_VW orderless_dep on orderless_dep.DEPT_ID = orderless_financial_data.department_ministry_id
                LEFT JOIN master..CAT_REF_DEPARTMENTS_VW test_permission_dep on CAST(orderless_dep.DEPT_ID AS VARCHAR) = htp.department_ministry_code
                -- GETTING TOTAL NUMBER OF UNSUPERVISED TEST ACCESS CODE FOR RESPECTIVE UIT_INVITE_ID
                OUTER APPLY (SELECT COUNT(*) as 'count' 
                            FROM master..custom_models_historicalunsupervisedtestaccesscode hutac 
                            WHERE hutac.uit_invite_id = hui.id AND hutac.history_type = '+') total_number_of_utac
                -- GETTING NUMBER OF REMAINING UNSUPERVISED TEST ACCESS CODE FOR RESPECTIVE UIT_INVITE_ID
                OUTER APPLY (SELECT COUNT(*) as 'count' 
                            FROM master..custom_models_unsupervisedtestaccesscode utac 
                            WHERE utac.uit_invite_id = hui.id) number_of_remaining_utac
                -- GETTING NUMBER OF ACTIVE TESTS (CHECKED_IN, PRE_TEST, ACTIVE OR TRANSITION) FOR RESPECTIVE UIT_INVITE_ID
                OUTER APPLY (SELECT COUNT(*) as 'count' 
                            FROM master..custom_models_assignedtest at 
                            WHERE at.uit_invite_id = hui.id 
                            AND at.status_id in (SELECT id 
                                                FROM master..custom_models_assignedteststatus ats 
                                                WHERE ats.codename in ('checked_in', 'pre_test', 'active', 'transition')
                                                )
                            ) number_of_active_tests
                -- GETTING NUMBER OF TESTS TAKEN
                OUTER APPLY (SELECT COUNT(*) as 'count' 
                            FROM master..custom_models_assignedtest at 
                            WHERE at.uit_invite_id = hui.id 
                            AND at.status_id in (SELECT id 
                                                FROM master..custom_models_assignedteststatus ats 
                                                WHERE ats.codename in ('submitted', 'quit')
                                                )
                            ) number_of_tests_taken
                -- HISTORY_TYPE +
                -- AND
                -- VALIDITY_END_DATE IS IN THE PAST
                -- OR
                -- NO MORE REMAINING UNSUPERVISED TEST ACCESS CODE AND NO MORE ACTIVE TESTS
                -- AND
                -- REMOVING NULL TA_TEST_PERMISSION_ID FOR NON-ORDERLESS TEST (OLDER DATA)
                WHERE hui.history_type = '+' 
                AND (hui.validity_end_date < CONVERT(VARCHAR(10), GETDATE(), 23)
                    OR
                    number_of_remaining_utac.count <= 0 AND number_of_active_tests.count <= 0
                    )
                AND (hui.orderless_request = 0 AND hui.ta_test_permissions_id is not NULL OR hui.orderless_request = 1)
                
GO
/****** Object:  Table [dbo].[cms_models_scoringraw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_scoringraw](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[result_valid_indefinitely] [bit] NOT NULL,
	[scoring_method_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_scoringmethods]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_scoringmethods](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[method_type_id] [int] NOT NULL,
	[test_id] [int] NOT NULL,
	[validity_period] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_scoringmethodtypes]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_scoringmethodtypes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[method_type_codename] [nvarchar](50) NOT NULL,
	[en_name] [nvarchar](max) NOT NULL,
	[fr_name] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_scoring_method_type_codename] UNIQUE NONCLUSTERED 
(
	[method_type_codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_scoringthreshold]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_scoringthreshold](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[minimum_score] [int] NOT NULL,
	[maximum_score] [int] NOT NULL,
	[en_conversion_value] [nvarchar](max) NOT NULL,
	[scoring_method_id] [int] NOT NULL,
	[fr_conversion_value] [nvarchar](max) NOT NULL,
	[result_valid_indefinitely] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_scoringpassfail]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_scoringpassfail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[minimum_score] [int] NOT NULL,
	[scoring_method_id] [int] NOT NULL,
	[result_valid_indefinitely] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[scored_tests_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[scored_tests_vw] AS
                SELECT
                    at.id,
                    at.start_date,
                    CONVERT(VARCHAR(10), at.start_date, 23) as 'simplified_start_date',
                    at.modify_date,
                    at.submit_date,
                    at.test_access_code,
                    at.total_score,
                    td.id as 'test_id',
                    td.parent_code,
                    td.test_code,
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    td.retest_period,
                    td.show_score,
                    td.show_result,
                    ts.test_skill_type_id,
                    type_e.text as 'test_skill_type_name_en',
                    type_f.text as 'test_skill_type_name_fr',
                    test_skill_sub_type_data.id as 'test_skill_sub_type_id',
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return NULL
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'sle')
                                                    -- Getting sle description text (EN)
                                                    THEN (SELECT text 
                                                            FROM master..cms_models_testskillsledesctext 
                                                            WHERE language_id = 1 AND test_skill_sle_desc_id = (SELECT id 
                                                                                                                FROM master..cms_models_testskillsledesc 
                                                                                                                WHERE id = tssle.test_skill_sle_desc_id))
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'occupational')
                                                    -- Getting occupational description text (EN)
                                                    THEN (SELECT text 
                                                            FROM master..cms_models_testskilloccupationaldesctext
                                                            WHERE language_id = 1 AND test_skill_occupational_desc_id = (SELECT id 
                                                                                                                        FROM master..cms_models_testskilloccupationaldesc 
                                                                                                                        WHERE id = tsocc.test_skill_occupational_desc_id))
                    END as 'test_skill_sub_type_name_en',
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return None
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'sle')
                                                    -- Getting sle description text (FR)
                                                    THEN (SELECT text 
                                                            FROM master..cms_models_testskillsledesctext 
                                                            WHERE language_id = 2 AND test_skill_sle_desc_id = (SELECT id 
                                                                                                                FROM master..cms_models_testskillsledesc 
                                                                                                                WHERE id = tssle.test_skill_sle_desc_id))
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    -- Getting occupational description text (FR)
                                                    THEN (SELECT text 
                                                            FROM master..cms_models_testskilloccupationaldesctext
                                                            WHERE language_id = 2 AND test_skill_occupational_desc_id = (SELECT id 
                                                                                                                        FROM master..cms_models_testskilloccupationaldesc 
                                                                                                                        WHERE id = tsocc.test_skill_occupational_desc_id))
                    END as 'test_skill_sub_type_name_fr',
                    at.test_session_language_id,
                    at.test_order_number,
                    at.en_converted_score,
                    at.fr_converted_score,
                    at.is_invalid,
                    at.uit_invite_id,
                    at.orderless_financial_data_id,
                    at.accommodation_request_id,
                    at.test_session_id,
                    ats.id as 'status_id',
                    ats.codename as 'status_codename',
                    at.ta_user_id,
                    u.id as 'candidate_user_id',
                    u.email as 'candidate_email',
                    u.first_name as 'candidate_first_name',
                    u.last_name as 'candidate_last_name',
                    u.birth_date as 'candidate_dob',
                    sm.validity_period,
                    CASE
                        -- Scoring Method NONE ==> return 0
                        WHEN sm.method_type_id = (SELECT id FROM master..cms_models_scoringmethodtypes WHERE method_type_codename = 'NONE') THEN 0
                        -- Scoring method RAW ==> retuning respective boolean value from table
                        WHEN sm.method_type_id = (SELECT id FROM master..cms_models_scoringmethodtypes WHERE method_type_codename = 'RAW') THEN (SELECT result_valid_indefinitely FROM master..cms_models_scoringraw WHERE scoring_method_id = sm.id)
                        -- Scoring method PASS/FAIL ==> retuning respective boolean value from table
                        WHEN sm.method_type_id = (SELECT id FROM master..cms_models_scoringmethodtypes WHERE method_type_codename = 'PASS_FAIL') THEN (SELECT result_valid_indefinitely FROM master..cms_models_scoringpassfail WHERE scoring_method_id = sm.id)
                        -- Scoring method THRESHOLD ==> retuning respective boolean value from table
                        WHEN sm.method_type_id = (SELECT id FROM master..cms_models_scoringmethodtypes WHERE method_type_codename = 'THRESHOLD') THEN (SELECT result_valid_indefinitely FROM master..cms_models_scoringthreshold WHERE scoring_method_id = sm.id AND en_conversion_value = at.en_converted_score)
                    END as 'result_valid_indefinitely',
                    at.score_valid_until,
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return NULL
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'sle') 
                                                    -- return respective score_valid_until (if defined)
                                                    THEN IIF(calculated_sle_tests_score_validity.id = at.id, calculated_sle_tests_score_validity.score_valid_until, NULL)
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    -- return respective score_valid_until (if defined)
                                                    THEN IIF(calculated_occ_tests_score_validity.id = at.id, calculated_occ_tests_score_validity.score_valid_until, NULL)
                    END as 'calculated_score_valid_until',
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return NULL
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'sle') 
                                                    -- Return 1 if calculated_sle_tests_score_validity is defined, otherwise return 0
                                                    THEN IIF(calculated_sle_tests_score_validity.id = at.id, 1, 0)
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    -- Return 1 if calculated_occ_tests_score_validity is defined, otherwise return 0
                                                    THEN IIF(calculated_occ_tests_score_validity.id = at.id, 1, 0)
                    END as 'is_most_recent_valid_test',
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return NULL
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'sle') 
                                                    -- Return respective retest_period
                                                    THEN calculated_sle_tests_retest_data.retest_period
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    -- Return respective retest_period
                                                    THEN calculated_occ_tests_retest_data.retest_period
                    END as 'most_updated_retest_period',
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return NULL
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'sle') 
                                                    -- Return submit_date + respective retest_period (in days) (YYYY-MM-DD)
                                                    THEN CONVERT(VARCHAR(10), DATEADD(DAY, calculated_sle_tests_retest_data.retest_period, calculated_sle_tests_retest_data.submit_date), 23)
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    -- Return submit_date + respective retest_period (in days) (YYYY-MM-DD)
                                                    THEN CONVERT(VARCHAR(10), DATEADD(DAY, calculated_occ_tests_retest_data.retest_period, calculated_occ_tests_retest_data.submit_date), 23)
                    END as 'calculated_retest_period_date',
                    CASE
                        -- test has been quit
                        WHEN at.status_id = (SELECT id FROM master..custom_models_assignedteststatus WHERE codename = 'quit') THEN 0
                        -- if supervised test ==> test must have been submitted at least two days ago to be displayed
                        WHEN at.uit_invite_id is NULL AND at.submit_date <= DATEADD(DAY, -2, GETUTCDATE()) THEN 0
                        -- if unsupervised test ==> current date (UTC) must be greater than two days after the validity end date of respective uit invite
                        WHEN at.uit_invite_id is not NULL AND CONVERT(VARCHAR(10), GETUTCDATE(), 23) > (SELECT DATEADD(DAY, 2, validity_end_date) FROM master..custom_models_uitinvites WHERE id = at.uit_invite_id) THEN 0
                    ELSE
                        1
                    END as 'pending_score'
                FROM master..custom_models_assignedtest at
                JOIN master..custom_models_assignedteststatus ats on ats.id = at.status_id
                JOIN master..user_management_models_user u on u.id = at.user_id
                JOIN master..cms_models_testdefinition td on td.id = at.test_id
                JOIN master..cms_models_scoringmethods sm on sm.test_id = at.test_id
                JOIN master..cms_models_testskill ts on ts.test_definition_id = at.test_id
                JOIN master..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM master..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM master..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN master..cms_models_testskillsle tssle on tssle.test_skill_id = ts.id
                LEFT JOIN master..cms_models_testskilloccupational tsocc on tsocc.test_skill_id = ts.id
                OUTER APPLY (SELECT (CASE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    THEN NULL
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'sle') 
                                                    THEN tssle.test_skill_sle_desc_id
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM master..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    THEN tsocc.test_skill_occupational_desc_id
                    END) as 'id') test_skill_sub_type_data
                -- calculated_sle_tests_score_validity
                OUTER APPLY (SELECT TOP 1 at2.id, at2.score_valid_until
                            FROM custom_models_assignedtest at2
                            WHERE at2.is_invalid = 0
                            AND at2.user_id = u.id AND at2.test_id in (SELECT test_definition_id 
                                                                        FROM cms_models_testskill ts2 
                                                                        WHERE ts2.id in (SELECT tssle2.test_skill_id 
                                                                                        FROM cms_models_testskillsle tssle2 
                                                                                        WHERE tssle2.test_skill_id in (SELECT ts3.id 
                                                                                                                        FROM cms_models_testskill ts3
                                                                                                                        WHERE ts3.test_skill_type_id = ts.test_skill_type_id) 
                                                                                            AND tssle2.test_skill_sle_desc_id = test_skill_sub_type_data.id)) ORDER BY at2.submit_date DESC) calculated_sle_tests_score_validity
                -- calculated_occ_tests_score_validity
                OUTER APPLY (SELECT TOP 1 at2.id, at2.score_valid_until
                            FROM custom_models_assignedtest at2
                            WHERE at2.is_invalid = 0
                            AND at2.user_id = u.id AND at2.test_id in (SELECT test_definition_id 
                                                                        FROM cms_models_testskill ts2 
                                                                        WHERE ts2.id in (SELECT tsocc2.test_skill_id 
                                                                                        FROM cms_models_testskilloccupational tsocc2 
                                                                                        WHERE tsocc2.test_skill_id in (SELECT ts3.id 
                                                                                                                        FROM cms_models_testskill ts3
                                                                                                                        WHERE ts3.test_skill_type_id = ts.test_skill_type_id) 
                                                                                            AND tsocc2.test_skill_occupational_desc_id = test_skill_sub_type_data.id)) ORDER BY at2.submit_date DESC) calculated_occ_tests_score_validity
                -- calculated_sle_tests_retest_data
                OUTER APPLY (SELECT TOP 1 td2.retest_period, at2.submit_date
                            FROM custom_models_assignedtest at2
                            JOIN master..cms_models_testdefinition td2 on td2.id = at2.test_id
                            WHERE at2.user_id = u.id AND at2.test_id in (SELECT test_definition_id 
                                                                        FROM cms_models_testskill ts2 
                                                                        WHERE ts2.id in (SELECT tssle2.test_skill_id 
                                                                                        FROM cms_models_testskillsle tssle2 
                                                                                        WHERE tssle2.test_skill_id in (SELECT ts3.id 
                                                                                                                        FROM cms_models_testskill ts3
                                                                                                                        WHERE ts3.test_skill_type_id = ts.test_skill_type_id) 
                                                                                            AND tssle2.test_skill_sle_desc_id = test_skill_sub_type_data.id)) ORDER BY at2.submit_date DESC) calculated_sle_tests_retest_data
                -- calculated_occ_tests_retest_data
                OUTER APPLY (SELECT TOP 1 td2.retest_period, at2.submit_date
                            FROM custom_models_assignedtest at2
                            JOIN master..cms_models_testdefinition td2 on td2.id = at2.test_id
                            WHERE at2.user_id = u.id AND at2.test_id in (SELECT test_definition_id 
                                                                        FROM cms_models_testskill ts2 
                                                                        WHERE ts2.id in (SELECT tsocc2.test_skill_id 
                                                                                        FROM cms_models_testskilloccupational tsocc2 
                                                                                        WHERE tsocc2.test_skill_id in (SELECT ts3.id 
                                                                                                                        FROM cms_models_testskill ts3
                                                                                                                        WHERE ts3.test_skill_type_id = ts.test_skill_type_id) 
                                                                                            AND tsocc2.test_skill_occupational_desc_id = test_skill_sub_type_data.id)) ORDER BY at2.submit_date DESC) calculated_occ_tests_retest_data
                -- status must be submitted or quit
                WHERE at.status_id in (SELECT id 
                                    FROM master..custom_models_assignedteststatus 
                                    WHERE codename in ('submitted', 'quit'))
    
GO
/****** Object:  View [dbo].[ta_history_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[ta_history_vw] AS
					SELECT t1.id as [assigned_test_id],
                    t1.is_invalid as [is_invalid],
					t3.first_name as [ta_first_name],
					t3.last_name as [ta_last_name],
                    t3.email as [ta_email], 
                    cup.pri as [pri],
                    cup.military_nbr [military_number],
                    t1.ta_user_id as [ta_user_id],
					t3.username as [ta_username],
                    cup.goc_email [goc_email],
                    -- Need to add requesting org from uitinvite and/or HR request (supervised), Extract based on interface language.
                    CONCAT(IIF(t1.uit_invite_id is null, CONCAT(apvw.dept_eabrv, ' - ',apvw.dept_edesc), CONCAT(depvw.eabrv,' - ', depvw.EDESC)),'/',IIF(t1.uit_invite_id is null, CONCAT(apvw.dept_fabrv, ' - ',apvw.dept_fdesc), CONCAT(depvw.fabrv,' - ', depvw.FDESC)) ) [requesting_organization],
                    --IIF(t1.uit_invite_id is null, CONCAT(apvw.dept_fabrv, ' - ',apvw.dept_fdesc), CONCAT(depvw.fabrv,' - ', depvw.FDESC))
                    td.test_code as [test],
                    CAST(t1.submit_date as date) [test_submit_date],
                    -- cdd.id [CAT User ID],
                    CONCAT(cdd.last_name, ', ', cdd.first_name) [candidate_name],
                    t1.user_id as [candidate_user_id],
					cdd.username as [candidate_username]

                FROM master..custom_models_assignedtest t1
                    JOIN master..user_management_models_user cdd on t1.user_id = cdd.id
                    CROSS APPLY (SELECT TOP 1 * FROM master..user_management_models_historicalcustomuserpermissions hcup WHERE hcup.permission_id = 1 AND hcup.user_id = t1.ta_user_id AND history_type = '+' AND hcup.history_date < t1.start_date) cup
                    JOIN master..cms_models_testdefinition td on td.id = t1.test_id
                    JOIN master..user_management_models_user t3 on t3.id = t1.ta_user_id
                    LEFT JOIN master..custom_models_uitinvites uit on uit.id = t1.uit_invite_id
                    LEFT JOIN master..custom_models_orderlessfinancialdata ofd on t1.orderless_financial_data_id = ofd.id
                    LEFT JOIN master..CAT_REF_DEPARTMENTS_VW depvw on depvw.DEPT_ID = ofd.department_ministry_id
                    LEFT JOIN master..custom_models_consumedreservationcodes crc on crc.assigned_test_id = t1.id
                    LEFT JOIN master..custom_models_assessmentprocessassignedtestspecs apts on apts.id = crc.assessment_process_assigned_test_specs_id
                    LEFT JOIN master..assessment_process_vw apvw on apvw.id = apts.assessment_process_id
        
GO
/****** Object:  View [dbo].[scorer_ola_tests_to_assess_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[scorer_ola_tests_to_assess_vw] AS
                SELECT
                    TOP 100 PERCENT
                    DATEDIFF(SECOND, '1970-01-01', tsd.start_time) + IIF(tsd.test_skill_sub_type_id = (SELECT id FROM master..cms_models_testskillsledesc tssle WHERE tssle.codename = 'oe'),  '1', '2') as 'id',
                    tsd.date,
                    DATEPART(dw, tsd.date) as 'day_of_week_id',
                    tsd.start_time,
                    CONVERT(VARCHAR(8), tsd.start_time, 108) as 'simplified_start_time',
                    tsd.end_time,
                    CONVERT(VARCHAR(8), tsd.end_time, 108) as 'simplified_end_time',
                    IIF(tsd.test_skill_sub_type_id = (SELECT id FROM master..cms_models_testskillsledesc tssle WHERE tssle.codename = 'oe'),  1, 2) as 'language_id',
                    ts.is_standard,
                    count(*) as 'nbr_of_sessions'
                FROM master..custom_models_testcentertestsessions ts
                JOIN master..custom_models_testcentertestsessiondata tsd on tsd.id = ts.test_session_data_id
                JOIN master..custom_models_consumedreservationcodes crc ON crc.test_session_id = ts.id
                JOIN master..custom_models_consumedreservationcodestatus crcs ON crcs.id = crc.status_id
                -- ONLY GETTING OLA TEST SESSIONS THAT ARE BOOKED (NOT PAIRED)
                WHERE tsd.test_skill_type_id = (SELECT id FROM master..cms_models_testskilltype tst WHERE tst.codename = 'sle')
                AND tsd.test_skill_sub_type_id IN (SELECT id FROM master..cms_models_testskillsledesc tssle WHERE tssle.codename IN ('oe', 'of'))
                AND crcs.codename = 'booked'
                GROUP BY tsd.date, tsd.start_time, tsd.end_time, tsd.test_skill_sub_type_id, ts.is_standard
                ORDER BY tsd.date, tsd.start_time
    
GO
/****** Object:  Table [dbo].[custom_models_historicaltestaccesscode]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestaccesscode](
	[id] [int] NOT NULL,
	[test_access_code] [nvarchar](10) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_id] [int] NULL,
	[created_date] [date] NOT NULL,
	[test_session_id] [int] NULL,
	[ta_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_taactions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_taactions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[action_type_id] [nvarchar](15) NOT NULL,
	[assigned_test_id] [int] NOT NULL,
	[test_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_locktestactions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_locktestactions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[lock_start_date] [datetime2](7) NOT NULL,
	[lock_end_date] [datetime2](7) NULL,
	[ta_action_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_assignedtestsectionaccesstimes]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assignedtestsectionaccesstimes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[time_type] [int] NOT NULL,
	[time] [datetime2](7) NOT NULL,
	[assigned_test_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaltaextendedprofile]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaltaextendedprofile](
	[id] [int] NOT NULL,
	[department_id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluser]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluser](
	[id] [int] NOT NULL,
	[password] [nvarchar](128) NOT NULL,
	[is_superuser] [bit] NOT NULL,
	[username] [nvarchar](150) NOT NULL,
	[email] [nvarchar](254) NOT NULL,
	[is_staff] [bit] NOT NULL,
	[is_active] [bit] NOT NULL,
	[date_joined] [datetime2](7) NOT NULL,
	[secondary_email] [nvarchar](254) NULL,
	[first_name] [nvarchar](30) NOT NULL,
	[last_name] [nvarchar](150) NOT NULL,
	[birth_date] [date] NOT NULL,
	[pri] [nvarchar](10) NOT NULL,
	[last_password_change] [date] NULL,
	[psrs_applicant_id] [nvarchar](8) NULL,
	[last_login] [datetime2](7) NULL,
	[last_login_attempt] [datetime2](7) NULL,
	[login_attempts] [int] NOT NULL,
	[locked] [bit] NOT NULL,
	[locked_until] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[is_profile_complete] [bit] NOT NULL,
	[last_profile_update] [datetime2](7) NULL,
	[military_nbr] [nvarchar](9) NOT NULL,
	[phone_number] [nvarchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_accommodationrequest]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_accommodationrequest](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[break_bank_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_breakbank]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_breakbank](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[break_time] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_breakbankactions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_breakbankactions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[action_type] [nvarchar](15) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[new_remaining_time] [int] NULL,
	[break_bank_id] [int] NOT NULL,
	[test_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_assignedtestsection]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assignedtestsection](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_section_time] [int] NULL,
	[score] [int] NULL,
	[assigned_test_id] [int] NOT NULL,
	[test_section_id] [int] NOT NULL,
	[timed_out] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[adapted_tests_report_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[adapted_tests_report_vw] AS
				SELECT
                    t1.id as 'assigned_test_id',
                    cdd.last_name as 'candidate_last_name',
                    cdd.first_name as 'candidate_first_name',
                    cdd.email as 'candidate_email',
                    CASE
                        WHEN ordered_test_org_data.DEPT_ID is not NULL THEN ordered_test_org_data.DEPT_ID
                        WHEN orderless_test_org_data.DEPT_ID is not NULL THEN orderless_test_org_data.DEPT_ID
                        WHEN supervised_test_org_data.[assessment_process_dept_id ] is not NULL THEN supervised_test_org_data.assessment_process_dept_id
                    END as 'requesting_organization_dept_id',
                    CASE
                        WHEN ordered_test_org_data.DEPT_ID is not NULL THEN ordered_test_org_data.EDESC
                        WHEN orderless_test_org_data.DEPT_ID is not NULL THEN orderless_test_org_data.EDESC
                        WHEN supervised_test_org_data.[assessment_process_dept_id ] is not NULL THEN supervised_test_org_data.assessment_process_dept_edesc
                    END as 'requesting_organization_dept_edesc',
                    CASE
                        WHEN ordered_test_org_data.DEPT_ID is not NULL THEN ordered_test_org_data.FDESC
                        WHEN orderless_test_org_data.DEPT_ID is not NULL THEN orderless_test_org_data.FDESC
                        WHEN supervised_test_org_data.[assessment_process_dept_id ] is not NULL THEN supervised_test_org_data.assessment_process_dept_fdesc
                    END as 'requesting_organization_dept_fdesc',
                    CASE
                        WHEN ordered_test_org_data.DEPT_ID is not NULL THEN ordered_test_org_data.EABRV
                        WHEN orderless_test_org_data.DEPT_ID is not NULL THEN orderless_test_org_data.EABRV
                        WHEN supervised_test_org_data.[assessment_process_dept_id ] is not NULL THEN supervised_test_org_data.assessment_process_dept_eabrv
                    END as 'requesting_organization_dept_eabrv',
                    CASE
                        WHEN ordered_test_org_data.DEPT_ID is not NULL THEN ordered_test_org_data.FABRV
                        WHEN orderless_test_org_data.DEPT_ID is not NULL THEN orderless_test_org_data.FABRV
                        WHEN supervised_test_org_data.[assessment_process_dept_id ] is not NULL THEN supervised_test_org_data.assessment_process_dept_fabrv
                    END as 'requesting_organization_dept_fabrv',
                    t1.ta_user_id as 'test_administrator_user_id',
					ta_historical_user_data.username as 'test_administrator_username',
                    ta_historical_user_data.last_name as 'test_administrator_last_name', --getting historical user data based on test access code creation date
                    ta_historical_user_data.first_name as 'test_administrator_first_name', --getting historical user data based on test access code creation date
                    ta_historical_permission_data.goc_email as 'test_administrator_goc_email',
                    ta_org_data.DEPT_ID as 'test_administrator_dept_id',
                    ta_org_data.EDESC as 'test_administrator_dept_edesc',
                    ta_org_data.FDESC as 'test_administrator_dept_fdesc',
                    ta_org_data.EABRV as 'test_administrator_dept_eabrv',
                    ta_org_data.FABRV as 'test_administrator_dept_fabrv',
                    t1.test_order_number as 'test_order_number',
	                ofd.reference_number as 'reference_number',
                    ts1.id as 'test_status_id',
                    ts1.codename as 'test_status_codename',
                    t1.is_invalid as 'is_invalid',
                    -- Test start date and time (time when the timed section was actually started ... might get complicated with multi-section tests)
                    calculated_start_time.start_time as 'test_start_time',
                    -- Test Submit date and time (time when the timed section was actually submitted ... might show strange results for users that x-out)
                    calculated_end_time.end_time as 'test_end_time',
                    td.test_code as 'test_code',
                    td.en_name as 'test_description_en',
                    td.fr_name as 'test_description_fr',
                    -- Total time alotted 
                    total_time_allotted_calculations.total_time_seconds as 'total_time_allotted_in_minutes', 
                    -- Time Used / Spent in test
                    ROUND((total_time_used_calculations.total_time_seconds
                        -
                        IIF(locked_data.total_time_in_seconds is NULL, 0, locked_data.total_time_in_seconds)  
                        -
                        IIF((bb.break_time - break_bank_data.new_remaining_time) is null, 0, (bb.break_time - break_bank_data.new_remaining_time))
                    )/CAST(60 as FLOAT), 2) as 'total_time_used_in_minutes',
                    -- Break Bank alotted
                    IIF(ar.break_bank_id is null, 0 , bb.break_time/60) as 'break_bank_allotted_in_minutes',
                    -- Break bank used
                    IIF((bb.break_time - break_bank_data.new_remaining_time) is null, 0, ROUND((bb.break_time - break_bank_data.new_remaining_time)/CAST(60 as FLOAT), 2)) as 'break_bank_used_in_minutes'
                FROM master..custom_models_assignedtest t1
                JOIN master..custom_models_assignedteststatus ts1 ON ts1.id = t1.status_id
                JOIN master..user_management_models_user cdd ON t1.user_id = cdd.id
                OUTER APPLY (SELECT TOP 1 * 
                            FROM master..user_management_models_historicaluser hu 
                            WHERE hu.id = t1.ta_user_id 
                            AND hu.history_date < IIF(t1.uit_invite_id is not NULL, 
                                                    (SELECT huitac.history_date
                                                    FROM master..custom_models_historicalunsupervisedtestaccesscode huitac 
                                                    WHERE huitac.uit_invite_id = t1.uit_invite_id 
                                                    AND huitac.test_access_code = t1.test_access_code 
                                                    AND huitac.history_type = '+'
                                                    ),
                                                    (SELECT htac.history_date 
                                                        FROM master..custom_models_historicaltestaccesscode htac 
                                                        WHERE htac.test_id = t1.test_id 
                                                        AND htac.test_access_code = t1.test_access_code 
                                                        AND htac.history_type = '+'
                                                        )
                            ) ORDER BY hu.history_date DESC
                            ) ta_historical_user_data
                OUTER APPLY (SELECT TOP 1 hup.goc_email 
                            FROM master..user_management_models_historicalcustomuserpermissions hup 
                            WHERE hup.permission_id = (SELECT cp.permission_id 
                                                        FROM master..user_management_models_custompermissions cp 
                                                        WHERE cp.codename = 'is_test_administrator')
                                                        AND hup.history_type = '+' 
                                                        AND hup.history_date < IIF(t1.uit_invite_id is not NULL, 
                                                                                (SELECT huitac.history_date
                                                                                FROM master..custom_models_historicalunsupervisedtestaccesscode huitac 
                                                                                WHERE huitac.uit_invite_id = t1.uit_invite_id 
                                                                                AND huitac.test_access_code = t1.test_access_code 
                                                                                AND huitac.history_type = '+'
                                                                                ),
                                                                                (SELECT htac.history_date 
                                                                                    FROM master..custom_models_historicaltestaccesscode htac 
                                                                                    WHERE htac.test_id = t1.test_id 
                                                                                    AND htac.test_access_code = t1.test_access_code 
                                                                                    AND htac.history_type = '+'
                                                                                    )
                                                                                )
                            AND hup.user_id = t1.ta_user_id
                            ORDER BY hup.history_date DESC
                            ) ta_historical_permission_data
                OUTER APPLY (SELECT dv.DEPT_ID, dv.EDESC, dv.FDESC, dv.EABRV, dv.FABRV 
                            FROM master..CAT_REF_DEPARTMENTS_VW dv 
                            WHERE dv.DEPT_ID = (SELECT TOP 1 htep.department_id 
                                                FROM master..user_management_models_historicaltaextendedprofile htep 
                                                WHERE htep.history_date < IIF(t1.uit_invite_id is not NULL, 
                                                                            (SELECT huitac.history_date
                                                                            FROM master..custom_models_historicalunsupervisedtestaccesscode huitac 
                                                                            WHERE huitac.uit_invite_id = t1.uit_invite_id 
                                                                            AND huitac.test_access_code = t1.test_access_code 
                                                                            AND huitac.history_type = '+'
                                                                            ),
                                                                            (SELECT htac.history_date 
                                                                            FROM master..custom_models_historicaltestaccesscode htac 
                                                                            WHERE htac.test_id = t1.test_id 
                                                                            AND htac.test_access_code = t1.test_access_code 
                                                                            AND htac.history_type = '+'
                                                                            )
                                                                            )
                                                AND htep.user_id = t1.ta_user_id
                                                ORDER BY htep.history_date DESC
                                                )
                            ) ta_org_data
                JOIN master..cms_models_testdefinition td ON t1.test_id = td.id
                JOIN master..custom_models_accommodationrequest ar ON t1.accommodation_request_id = ar.id
                LEFT JOIN master..custom_models_orderlessfinancialdata ofd ON t1.orderless_financial_data_id = ofd.id
                OUTER APPLY (SELECT TOP 1 atsat.time as 'start_time' 
                                FROM master..custom_models_assignedtestsectionaccesstimes atsat 
                                WHERE atsat.time_type = 1 
                                AND atsat.assigned_test_section_id = (SELECT TOP 1 ats.id 
                                                                    FROM master..custom_models_assignedtestsection ats 
                                                                    WHERE ats.assigned_test_id = t1.id AND ats.test_section_time is not NULL
                                                                    ) ORDER BY atsat.time
                                ) calculated_start_time
                OUTER APPLY (SELECT TOP 1 atsat.time as 'end_time' 
                                FROM master..custom_models_assignedtestsectionaccesstimes atsat 
                                WHERE atsat.time_type = 2 
                                AND atsat.assigned_test_section_id = (SELECT TOP 1 ats.id 
                                                                    FROM master..custom_models_assignedtestsection ats 
                                                                    WHERE ats.assigned_test_id = t1.id AND ats.test_section_time is not NULL 
                                                                    ORDER BY ats.id DESC
                                ) ORDER BY atsat.time DESC
                                ) calculated_end_time
                OUTER APPLY (SELECT SUM(test_section_time) as total_time_seconds 
                                FROM (SELECT temp_ats2.assigned_test_id, temp_ats2.test_section_time as test_section_time
                                    FROM (SELECT temp_ats1.id, temp_ats1.assigned_test_id , temp_ats1.test_section_time
                                            FROM master..custom_models_assignedtestsection temp_ats1 
                                            WHERE temp_ats1.assigned_test_id = t1.id AND temp_ats1.test_section_time is not NULL
                                    ) temp_ats2
                                ) x 
                                GROUP BY x.assigned_test_id) total_time_allotted_calculations
                OUTER APPLY (SELECT SUM(diff_time) as total_time_seconds 
                                FROM (SELECT temp_ats2.assigned_test_id, DATEDIFF(SECOND, atsat1.time, atsat2.time) as diff_time 
                                    FROM (SELECT temp_ats1.id, temp_ats1.assigned_test_id 
                                            FROM master..custom_models_assignedtestsection temp_ats1 
                                            WHERE temp_ats1.assigned_test_id = t1.id AND temp_ats1.test_section_time is not NULL) temp_ats2
                                    CROSS APPLY (SELECT TOP 1 * FROM custom_models_assignedtestsectionaccesstimes a WHERE temp_ats2.id = a.assigned_test_section_id AND a.time_type = 1 ORDER BY a.time ASC) atsat1 
                                    CROSS APPLY (SELECT TOP 1 * FROM custom_models_assignedtestsectionaccesstimes a WHERE temp_ats2.id = a.assigned_test_section_id AND a.time_type = 2 ORDER BY a.time ASC) atsat2
                            ) x 
                            GROUP BY x.assigned_test_id) total_time_used_calculations
                OUTER APPLY (SELECT SUM(DATEDIFF(second, lta.lock_start_date, lta.lock_end_date)) as 'total_time_in_seconds'
                            FROM master..custom_models_taactions ta
                            LEFT JOIN master..custom_models_locktestactions lta ON ta.id = lta.ta_action_id 
                            OUTER APPLY (SELECT id as 'timed_test_section_id' FROM master..cms_models_testsection ts WHERE ts.id in (SELECT ats.test_section_id FROM master..custom_models_assignedtestsection ats WHERE ats.assigned_test_id = t1.id AND ats.test_section_time is not NULL)) timed_test_section_ids
                            WHERE ta.assigned_test_id = t1.id AND ta.action_type_id in ('LOCK', 'UNLOCK') AND test_section_id in (timed_test_section_ids.timed_test_section_id)) locked_data
                LEFT JOIN master..custom_models_breakbank bb ON ar.break_bank_id = bb.id
                OUTER APPLY (SELECT TOP 1 bba.new_remaining_time as 'new_remaining_time'
                            FROM master..custom_models_breakbank sbb
                            LEFT JOIN master..custom_models_breakbankactions bba ON bba.break_bank_id = sbb.id
                            WHERE sbb.id = bb.id
                            ORDER BY bba.id DESC) break_bank_data
                -- ordered requesting org (old supervised + unsupervised)
                OUTER APPLY (SELECT dv.DEPT_ID, dv.EDESC, dv.FDESC, dv.EABRV, dv.FABRV 
                            FROM master..CAT_REF_DEPARTMENTS_VW dv 
                            WHERE dv.DEPT_ID = (SELECT TOP 1 IIF(TRY_CAST(htp.department_ministry_code as NUMERIC) is not NULL,
                                                                IIF((SELECT dv1.DEPT_ID FROM master..CAT_REF_DEPARTMENTS_VW dv1 WHERE dv1.DEPT_ID = CAST(htp.department_ministry_code as NUMERIC)) is not NULL,
                                                                    (SELECT dv1.DEPT_ID FROM master..CAT_REF_DEPARTMENTS_VW dv1 WHERE dv1.DEPT_ID = CAST(htp.department_ministry_code as NUMERIC)),
                                                                    104 -- defaulting to PSC
                                                                ),
                                                                104) -- defaulting to PSC
                                                FROM cms_models_historicaltestpermissions htp WHERE htp.test_order_number = t1.test_order_number AND htp.history_type = '+')) ordered_test_org_data
                -- orderless org data
                OUTER APPLY (SELECT dv.DEPT_ID, dv.EDESC, dv.FDESC, dv.EABRV, dv.FABRV FROM master..CAT_REF_DEPARTMENTS_VW dv WHERE dv.DEPT_ID = ofd.department_ministry_id) orderless_test_org_data
                -- supervised requesting org
                OUTER APPLY (SELECT apatpv.[assessment_process_dept_id ], apatpv.assessment_process_dept_edesc, apatpv.assessment_process_dept_fdesc, apatpv.assessment_process_dept_eabrv, apatpv.assessment_process_dept_fabrv FROM master..assessment_process_assigned_test_specs_vw apatpv WHERE id = (SELECT TOP 1 crc.assessment_process_assigned_test_specs_id FROM master..custom_models_historicalconsumedreservationcodes crc WHERE crc.assigned_test_id = t1.id ORDER BY crc.history_date ASC)) supervised_test_org_data
                WHERE t1.accommodation_request_id is not null
        
GO
/****** Object:  Table [dbo].[custom_models_testaccesscode]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testaccesscode](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_access_code] [nvarchar](10) NOT NULL,
	[test_id] [int] NOT NULL,
	[created_date] [date] NOT NULL,
	[test_session_id] [int] NULL,
	[ta_user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[test_access_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[bo_test_access_codes_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[bo_test_access_codes_vw] AS
				SELECT
                    tac.test_access_code,
                    NULL as 'test_order_number',
                    tac.test_session_id,
                    NULL as 'reference_number',
                    tac.created_date,
                    ta.id as 'ta_user_id',
                    ta.username as 'ta_username',
                    ta.email as 'ta_email',
                    ta.first_name as 'ta_first_name',
                    ta.last_name as 'ta_last_name',
                    up.goc_email as 'ta_goc_email',
                    td.id as 'test_id',
                    td.en_name as 'en_test_name',
                    td.fr_name as 'fr_test_name'
                FROM master..custom_models_testaccesscode tac
                OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicaluser u1 WHERE u1.id = tac.ta_user_id ORDER BY u1.history_date DESC) ta
                JOIN master..cms_models_testdefinition td on td.id = tac.test_id
                OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicalcustomuserpermissions up1 WHERE up1.user_id = tac.ta_user_id AND up1.history_date < tac.created_date ORDER BY up1.history_date DESC) up

                UNION

                SELECT 
                    utac.test_access_code,
                    utac.test_order_number,
                    NULL as 'test_session_id',
                    ofd.reference_number,
                    utac.created_date,
                    ta.id as 'ta_user_id',
                    ta.username as 'ta_username',
                    ta.email as 'ta_email',
                    ta.first_name as 'ta_first_name',
                    ta.last_name as 'ta_last_name',
                    up.goc_email as 'ta_goc_email',
                    td.id as 'test_id',
                    td.en_name as 'en_test_name',
                    td.fr_name as 'fr_test_name'
                FROM master..custom_models_unsupervisedtestaccesscode utac
                OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicaluser u1 WHERE u1.id = utac.ta_user_id ORDER BY u1.history_date DESC) ta
                JOIN master..cms_models_testdefinition td on td.id = utac.test_id
                LEFT JOIN master..custom_models_orderlessfinancialdata ofd on ofd.id = utac.orderless_financial_data_id
                OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicalcustomuserpermissions up1 WHERE up1.user_id = utac.ta_user_id AND up1.history_date < utac.created_date ORDER BY up1.history_date DESC) up
        
GO
/****** Object:  View [dbo].[assessment_process_results_candidates_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[assessment_process_results_candidates_vw] AS
                SELECT
                    ats.id,
                    ats.first_name as 'assessment_process_candidate_first_name',
                    ats.last_name as 'assessment_process_candidate_last_name',
                    ats.email as 'assessment_process_candidate_email',
                    apv.id as 'assessment_process_id',
                    apv.reference_number as 'assessment_process_reference_number',
                    apv.closing_date as 'assessment_process_closing_date',
                    apv.allow_booking_external_tc as 'assessment_process_allow_booking_external_tc',
                    apv.dept_id as 'assessment_process_dept_id ',
                    apv.dept_eabrv as 'assessment_process_dept_eabrv',
                    apv.dept_fabrv as 'assessment_process_dept_fabrv',
                    apv.dept_edesc as 'assessment_process_dept_edesc',
                    apv.dept_fdesc as 'assessment_process_dept_fdesc',
                    bc.id as 'billing_contact_id',
                    bc.first_name as 'billing_contact_first_name',
                    bc.last_name as 'billing_contact_last_name ',
                    bc.email as 'billing_contact_email',
                    tst.id as 'test_skill_type_id',
                    tst.codename as 'test_skill_type_codename',
                    type_e.text as 'test_skill_type_name_en',
                    type_f.text as 'test_skill_type_name_fr',
                    ats.test_skill_sub_type_id as 'test_skill_sub_type_id',
                    sled.codename as 'test_skill_sub_type_codename',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_e.text
                        WHEN tst.id = 3 THEN occ_sub_type_e.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_en',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_f.text
                        WHEN tst.id = 3 THEN occ_sub_type_f.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_fr',
                    rft.id as 'reason_for_testing_id',
					reason_for_testing_e.text as 'reason_for_testing_name_en',
					reason_for_testing_f.text as 'reason_for_testing_name_fr',
                    ats.level_required,
                    apv.request_sent,
                    crc.reservation_code,
                    crc.status_id as 'consumed_reservation_code_status_id',
                    crc.test_session_id,
                    crc.candidate_id,
                    crc.assigned_test_id,
                    at.status_id as 'assigned_test_status_id',
                    atst.codename as 'assigned_test_status_codename',
                    at.en_converted_score,
                    at.fr_converted_score,
                    at.submit_date,
                    at.total_score,
                    uaf.id as 'user_accommodation_file_id',
                    uaf.is_alternate_test_request,
                    mu.username as 'user_username',
                    mu.email as 'user_email',
                    mu.pri as 'user_pri',
                    mu.military_nbr as 'user_military_nbr',
                    mu.first_name as 'user_first_name',
                    mu.last_name as 'user_last_name',
                    td.test_code as 'test_definition_test_code',
                    td.en_name as 'test_definition_en_name',
                    td.fr_name as 'test_definition_fr_name'
                FROM master..custom_models_assessmentprocessassignedtestspecs ats
                LEFT JOIN master..custom_models_billingcontact bc on bc.id = ats.billing_contact_id
                JOIN master..assessment_process_vw apv on apv.id = ats.assessment_process_id
                JOIN master..cms_models_testskilltype tst on tst.id = ats.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM master..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM master..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN master..cms_models_testskillsledesc sled on sled.id = ats.test_skill_sub_type_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM master..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM master..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                OUTER APPLY (SELECT TOP 1 occdte.text FROM master..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = sled.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM master..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = sled.id AND occdtf.language_id = 2) occ_sub_type_f
                JOIN master..custom_models_reasonfortesting rft on rft.id = ats.reason_for_testing_id
				OUTER APPLY (SELECT TOP 1 text FROM master..custom_models_reasonfortestingtext rftt WHERE rftt.reason_for_testing_id = rft.id AND language_id = 1) reason_for_testing_e
				OUTER APPLY (SELECT TOP 1 text FROM master..custom_models_reasonfortestingtext rftt WHERE rftt.reason_for_testing_id = rft.id AND language_id = 2) reason_for_testing_f
                LEFT JOIN master..custom_models_consumedreservationcodes crc on crc.assessment_process_assigned_test_specs_id = ats.id
                LEFT JOIN master..custom_models_assignedtest at on at.id = crc.assigned_test_id
                LEFT JOIN master..custom_models_assignedteststatus atst on atst.id = at.status_id
                LEFT JOIN master..user_management_models_user mu on mu.id = crc.candidate_id
                LEFT JOIN master..cms_models_testdefinition td on td.id = at.test_id
                LEFT JOIN master..custom_models_useraccommodationfile uaf on uaf.id = crc.user_accommodation_file_id

        
GO
/****** Object:  Table [dbo].[custom_models_uitinviterelatedcandidates]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_uitinviterelatedcandidates](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [nvarchar](30) NOT NULL,
	[last_name] [nvarchar](150) NOT NULL,
	[email] [nvarchar](150) NOT NULL,
	[uit_invite_id] [int] NOT NULL,
	[reason_for_deletion_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[uit_related_candidates_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[uit_related_candidates_vw] AS
                SELECT
                    uirc.id,
                    u.id as 'user_id',
                    uirc.first_name,
                    uirc.last_name,
                    uirc.email,
                    uirc.reason_for_deletion_id,
                    ui.id as 'uit_invite_id',
                    ui.invite_date,
                    ui.validity_end_date,
                    ui.orderless_request,
                    ui.ta_user_id,
                    ui.test_id,
                    at.id as 'assigned_test_id',
                    at.test_access_code,
                    at.start_date,
                    at.modify_date,
                    at.submit_date,
                    ats.id as 'status_id',
                    ats.codename as 'status_codename',
                    uaf.id as 'user_accommodation_file_id',
                    uaf.is_alternate_test_request
                FROM master..custom_models_uitinviterelatedcandidates uirc
                JOIN master..custom_models_uitinvites ui on ui.id = uirc.uit_invite_id
                JOIN master..custom_models_historicalunsupervisedtestaccesscode hutac on hutac.candidate_email = uirc.email AND hutac.uit_invite_id = uirc.uit_invite_id AND hutac.history_type = '+'
                LEFT JOIN master..custom_models_assignedtest at on at.uit_invite_id = uirc.uit_invite_id AND at.test_access_code = hutac.test_access_code
                LEFT JOIN master..custom_models_assignedteststatus ats on ats.id = at.status_id
                LEFT JOIN master..user_management_models_user u on u.id = at.user_id
                LEFT JOIN master..custom_models_useraccommodationfile uaf on uaf.id = at.user_accommodation_file_id
                
GO
/****** Object:  Table [dbo].[user_management_models_useraccommodationsprofile]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_useraccommodationsprofile](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[has_built_in_accessibility] [bit] NOT NULL,
	[needs_extra_time] [bit] NOT NULL,
	[needs_breaks] [bit] NOT NULL,
	[needs_adaptive_tech] [bit] NOT NULL,
	[needs_ergonomic] [bit] NOT NULL,
	[needs_access_assistance] [bit] NOT NULL,
	[needs_environment] [bit] NOT NULL,
	[has_other_needs_written] [bit] NOT NULL,
	[previously_accommodated] [bit] NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[created_date] [date] NOT NULL,
	[user_id] [int] NOT NULL,
	[has_other_needs_oral] [bit] NOT NULL,
	[needs_repeated_oral_questions] [bit] NOT NULL,
	[needs_scheduling_adaptation] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ta_assigned_candidates_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[ta_assigned_candidates_vw] AS
                SELECT
                    at.id,
                    at.status_id,
                    atst.codename as 'status_codename',
                    at.previous_status_id,
                    atst2.codename as 'previous_status_codename',
                    at.start_date,
                    at.modify_date,
                    at.submit_date,
                    at.test_access_code,
                    at.total_score,
                    at.test_id,
                    td.parent_code,
                    td.test_code,
                    td.en_name as  'test_name_en',
                    td.fr_name as  'test_name_fr',
                    at.test_section_id,
                    at.test_session_language_id,
                    at.test_order_number,
                    at.en_converted_score,
                    at.fr_converted_score,
                    at.is_invalid,
                    at.uit_invite_id,
                    at.orderless_financial_data_id,
                    at.accommodation_request_id,
                    at.test_session_id,
                    uaf.id as 'user_accommodation_file_id',
                    uaf.is_alternate_test_request,
                    u.id as 'candidate_user_id',
                    u.email as 'candidate_email',
                    u.first_name as 'candidate_first_name',
                    u.last_name as 'candidate_last_name',
                    u.birth_date as 'candidate_dob',
                    IIF(uap.id is not NULL, 1, 0) as 'candidate_has_accommodations_profile',
                    tau.id as 'ta_user_id',
                    tau.username as 'ta_username',
                    tau.email as 'ta_email',
                    tau.first_name as 'ta_first_name',
                    tau.last_name as 'ta_last_name',
                    ta_approve_action_test_section_id,
                    CASE
                        WHEN test_started_id is NULL then 0
                        ELSE 1
                    END as 'test_started',
                    CASE
                        WHEN timed_section_has_been_accessed is NULL then 0
                        ELSE 1
                    END as 'timed_section_accessed',
                    CASE
                        WHEN test_started_id is NULL THEN non_started_test_next_test_section_id_that_needs_approval
                        ELSE started_test_next_test_section_id_that_needs_approval
                    END as 'next_test_section_id_that_needs_approval',
                    CASE
                        -- THERE IS NO TEST SECTION TO BE APPROVED (TEST STARTED OR NOT)
                        WHEN (test_started_id is NULL AND non_started_test_next_test_section_id_that_needs_approval IS NULL) OR (test_started_id is not NULL AND started_test_next_test_section_id_that_needs_approval is NULL) THEN 0
                        -- TEST NOT STARTED + LATEST APPROVED TEST SECTION ID (BY TA) IS THE SAME AS THE NEXT TEST SECTION ID THAT NEEDS APPROVAL
                        WHEN test_started_id is NULL AND ta_approve_action_test_section_id = non_started_test_next_test_section_id_that_needs_approval AND ta_approve_action_test_section_id is not NULL THEN 0
                        -- TEST STARTED + LATEST APPROVED TEST SECTION ID (BY TA) IS THE SAME AS THE NEXT TEST SECTION ID THAT NEEDS APPROVAL
                        WHEN test_started_id is not NULL AND ta_approve_action_test_section_id = started_test_next_test_section_id_that_needs_approval THEN 0
                        -- TEST STARTED (NO MORE SECTION TO BE APPROVED)
                        WHEN test_started_id is not NULL AND started_test_next_test_section_id_that_needs_approval is NULL THEN 0
                        -- NEEDS APPROVAL
                        ELSE 1
                    END as 'needs_approval'
                FROM master..custom_models_assignedtest at
                JOIN master..cms_models_testdefinition td on td.id = at.test_id
                JOIN master..user_management_models_user u on u.id= at.user_id
                JOIN master..user_management_models_user tau on tau.id = at.ta_user_id
                LEFT JOIN master..custom_models_assignedteststatus atst on atst.id = at.status_id
                LEFT JOIN master..custom_models_assignedteststatus atst2 on atst2.id = at.previous_status_id
                LEFT JOIN master..user_management_models_useraccommodationsprofile uap on uap.user_id = at.user_id
                LEFT JOIN master..custom_models_useraccommodationfile uaf on uaf.id = at.user_accommodation_file_id
                -- CHECKING IF TEST HAS BEEN STARTED BASED ON ASSIGNED TEST SECTION ACCESS TIMES ENTRIES
                OUTER APPLY (SELECT TOP 1 id as 'test_started_id' FROM master..custom_models_assignedtestsectionaccesstimes atsat1 WHERE atsat1.assigned_test_section_id in (SELECT id FROM master..custom_models_assignedtestsection ats1 WHERE ats1.assigned_test_id = at.id) AND time_type = 1 ORDER BY atsat1.id DESC) test_started_id
                -- CHECKING IF TIMED SECTION HAS BEEN ACCESSED
                OUTER APPLY (SELECT TOP 1 id as 'timed_section_has_been_accessed' FROM master..custom_models_assignedtestsectionaccesstimes atsat1 WHERE atsat1.assigned_test_section_id in (SELECT id FROM master..custom_models_assignedtestsection ats1 WHERE ats1.assigned_test_id = at.id AND ats1.test_section_time is not NULL) AND time_type = 1 ORDER BY atsat1.id DESC) timed_section_has_been_accessed
                -- GETTING THE NEXT TEST SECTION ID THAT NEEDS APPROVAL IF THE TEST HAS NOT BEEN STARTED YET (BASICALLY GETTING THE FIRST TEST SECTION THAT NEEDS APPROVAL)
                OUTER APPLY (SELECT TOP 1 id as 'non_started_test_next_test_section_id_that_needs_approval' FROM master..cms_models_testsection ts1 WHERE ts1.id in (SELECT test_section_id FROM master..custom_models_assignedtestsection ats3 WHERE ats3.assigned_test_id = at.id AND ts1.needs_approval = 1) ORDER BY ts1.[order]) non_started_test_next_test_section_id_that_needs_approval
                -- GETTING LATEST ACCESSED ASSIGNED TEST SECTION ID (NEEDED TO ONLY GET CURRENT/FUTURE ASSIGNED TEST SECTIONS)
                OUTER APPLY (SELECT TOP 1 assigned_test_section_id as 'latest_assigned_test_section_id' FROM master..custom_models_assignedtestsectionaccesstimes atsat2 WHERE atsat2.assigned_test_section_id in (SELECT id FROM master..custom_models_assignedtestsection ats2 WHERE ats2.assigned_test_id = at.id) AND atsat2.time_type = 1 ORDER BY atsat2.time DESC) latest_assigned_test_section_id
                -- GETTING THE NEXT TEST SECTION ID THAT NEEDS APPROVAL IF THE TEST HAS BEEN STARTED (USING THE 'latest_assigned_test_section_id' TO MAKE SURE WE ARE NOT GETTING A PAST TEST SESSION)
                OUTER APPLY (SELECT TOP 1 id as 'started_test_next_test_section_id_that_needs_approval' FROM master..cms_models_testsection ts2 WHERE ts2.id in (SELECT test_section_id FROM master..custom_models_assignedtestsection ats4 WHERE ats4.assigned_test_id = at.id AND ats4.id >= latest_assigned_test_section_id AND ts2.needs_approval = 1) ORDER BY ts2.[ORDER]) started_test_next_test_section_id_that_needs_approval
                -- GETTING TEST SECTION ID FROM THE LATEST APPROVE TA ACTION (IF IT EXISTS)
                OUTER APPLY (SELECT TOP 1 test_section_id as 'ta_approve_action_test_section_id' FROM master..custom_models_taactions ta WHERE ta.assigned_test_id = at.id AND ta.action_type_id = 'APPROVE' ORDER BY ta.modify_date DESC) ta_approve_action_test_section_id 
                WHERE at.status_id in (SELECT id FROM master..custom_models_assignedteststatus WHERE codename in ('checked_in', 'pre_test', 'active', 'transition', 'locked', 'paused'))
            
GO
/****** Object:  Table [dbo].[custom_models_consumedreservationcodestatustext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_consumedreservationcodestatustext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](30) NOT NULL,
	[consumed_reservation_code_status_id] [int] NOT NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[booked_reservations_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[booked_reservations_vw] AS
                SELECT
                    rc.id as "id",
                    rc.reservation_code as 'reservation_code',
                    rc.modify_date,
                    rc.candidate_id,
                    IIF(uap.id is not NULL, 1, 0) as 'candidate_has_accommodations_profile',
                    rc.user_accommodation_file_id,
                    apv.closing_date as 'assessment_process_closing_date',
                    crcs.id as 'consumed_reservation_code_status_id',
                    crcs.codename as 'consumed_reservation_codename',
                    status_e.text as 'consumed_reservation_code_en_status',
                    status_f.text as 'consumed_reservation_code_fr_status',
					tst.id as 'test_skill_type_id',
					ats.test_skill_type_codename,
                    type_e.text as 'test_skill_type_en_name',
                    type_f.text as 'test_skill_type_fr_name',
                    ats.test_skill_sub_type_id as 'test_skill_sub_type_id',
					ats.test_skill_sub_type_codename,
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_e.text
                        WHEN tst.id = 3 THEN occ_sub_type_e.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_en_name',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_f.text
                        WHEN tst.id = 3 THEN occ_sub_type_f.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_fr_name',
                    apv.request_sent,
                    rc.test_session_id,
                    tctsd.date,
                    tctsd.start_time,
                    tctsd.end_time,
                    tc.name as 'test_center_name',
                    tc.postal_code as 'test_center_postal_code',
                    tc.country_id,
                    c.EABRV as 'country_eabrv',
                    c.EDESC as 'country_edesc',
                    c.FABRV as 'country_fabrv',
                    c.FDESC as 'country_fdesc'
                FROM master..custom_models_consumedreservationcodes rc
                JOIN master..assessment_process_assigned_test_specs_vw ats ON rc.assessment_process_assigned_test_specs_id=ats.id
                JOIN master..assessment_process_vw apv ON apv.id = ats.assessment_process_id
                JOIN master..cms_models_testskilltype tst ON tst.id = ats.test_skill_type_id
                JOIN master..custom_models_consumedreservationcodestatus crcs ON crcs.id = rc.status_id
                OUTER APPLY (SELECT TOP 1 crcst.text FROM master..custom_models_consumedreservationcodestatustext crcst WHERE crcst.consumed_reservation_code_status_id = crcs.id AND crcst.language_id = 1) status_e
                OUTER APPLY (SELECT TOP 1 crcst.text FROM master..custom_models_consumedreservationcodestatustext crcst WHERE crcst.consumed_reservation_code_status_id = crcs.id AND crcst.language_id = 2) status_f
                OUTER APPLY (SELECT TOP 1 tstte.text FROM master..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM master..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN master..cms_models_testskillsledesc sled ON sled.id = ats.test_skill_sub_type_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM master..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM master..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                LEFT JOIN master..cms_models_testskilloccupationaldesc occd on occd.id = ats.test_skill_sub_type_id
                OUTER APPLY (SELECT TOP 1 occdte.text FROM master..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = sled.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM master..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = sled.id AND occdtf.language_id = 2) occ_sub_type_f
                LEFT JOIN master..custom_models_testcentertestsessions tcts ON tcts.id=rc.test_session_id
                LEFT JOIN master..custom_models_testcentertestsessiondata tctsd ON tctsd.id=tcts.test_session_data_id
                LEFT JOIN master..custom_models_testcenter tc ON tc.id=tcts.test_center_id
                LEFT JOIN master..CAT_REF_COUNTRY_VW c on c.CNTRY_ID = tc.country_id
                LEFT JOIN master..user_management_models_useraccommodationsprofile uap on uap.user_id = rc.candidate_id
    
GO
/****** Object:  View [dbo].[test_session_count_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[test_session_count_vw] AS
                SELECT
                    tcts.id,
                    (SELECT COUNT(*) FROM master..custom_models_consumedreservationcodes WHERE test_session_id=tcts.id) as count,
                    tctsd.spaces_available as max
                    FROM master..custom_models_testcentertestsessions tcts
                    JOIN master..custom_models_testcentertestsessiondata tctsd on tctsd.id = tcts.test_session_data_id
            
GO
/****** Object:  View [dbo].[test_session_attendees_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[test_session_attendees_vw] AS
                SELECT
                    crc.id,
                    crc.reservation_code,
                    crc.status_id,
                    crc.test_session_id,
                    crc.assigned_test_id,
                    crc.assessment_process_assigned_test_specs_id,
                    atsv.first_name,
                    atsv.last_name,
                    atsv.email,
                    atsv.assessment_process_id,
                    u.id as 'cat_user_id',
                    u.first_name as 'cat_user_first_name',
                    u.last_name as 'cat_user_last_name',
                    u.email 'cat_user_email'
                FROM master..custom_models_consumedreservationcodes crc
                JOIN master..custom_models_consumedreservationcodestatus crcs on crcs.id = crc.status_id
                JOIN master..assessment_process_assigned_test_specs_vw atsv on atsv.id = crc.assessment_process_assigned_test_specs_id
                JOIN master..user_management_models_user u on u.id = crc.candidate_id
                WHERE crcs.codename in ('booked', 'requested_acco')
            
GO
/****** Object:  View [dbo].[bundle_association_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[bundle_association_vw] AS
                SELECT 
                    NULL as 'bundle_bundles_association_id',
                    bia.id as 'bundle_items_association_id', 
                    bia.bundle_source_id,
                    bia.system_id_link as 'system_id_link',
                    bia.version as 'version',
                    NULL as 'bundle_link_id',
                    bia.display_order
                FROM master..cms_models_bundleitemsassociation bia
                UNION
                SELECT 
                    bba.id as 'bundle_bundles_association_id',
                    NULL as 'bundle_items_association_id',
                    bba.bundle_source_id,
                    NULL as 'system_id_link',
                    NULL as 'version',
                    bba.bundle_link_id as 'bundle_link_id',
                    bba.display_order 
                FROM master..cms_models_bundlebundlesassociation bba
        
GO
/****** Object:  Table [dbo].[custom_models_assessmentprocessdefaulttestspecs]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assessmentprocessdefaulttestspecs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_skill_sub_type_id] [int] NULL,
	[level_required] [nvarchar](30) NULL,
	[assessment_process_id] [int] NOT NULL,
	[test_skill_type_id] [int] NOT NULL,
	[reason_for_testing_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_assessment_process_test_skill_and_test_skill_sub_type_combination] UNIQUE NONCLUSTERED 
(
	[assessment_process_id] ASC,
	[test_skill_type_id] ASC,
	[test_skill_sub_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[assessment_process_default_test_specs_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[assessment_process_default_test_specs_vw] AS
                SELECT
                    ts.id,
                    ts.assessment_process_id,
                    tst.id as 'test_skill_type_id',
                    tst.codename as 'test_skill_type_codename',
                    type_e.text as 'test_skill_type_name_en',
                    type_f.text as 'test_skill_type_name_fr',
                    ts.test_skill_sub_type_id as 'test_skill_sub_type_id',
                    sled.codename as 'test_skill_sub_type_codename',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_e.text
                        WHEN tst.id = 3 THEN occ_sub_type_e.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_en',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_f.text
                        WHEN tst.id = 3 THEN occ_sub_type_f.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_fr',
                    rft.id as 'reason_for_testing_id',
                    reason_for_testing_e.text as 'reason_for_testing_name_en',
                    reason_for_testing_f.text as 'reason_for_testing_name_fr',
                    IIF(ts.level_required != '', ts.level_required, NULL) as 'level_required'
                FROM master..custom_models_assessmentprocessdefaulttestspecs ts
                JOIN master..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM master..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM master..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN master..cms_models_testskillsledesc sled on sled.id = ts.test_skill_sub_type_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM master..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM master..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                LEFT JOIN master..cms_models_testskilloccupationaldesc occd on occd.id = ts.test_skill_sub_type_id
                OUTER APPLY (SELECT TOP 1 occdte.text FROM master..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = sled.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM master..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = sled.id AND occdtf.language_id = 2) occ_sub_type_f
                LEFT JOIN master..custom_models_reasonfortesting rft on rft.id = ts.reason_for_testing_id
                OUTER APPLY (SELECT TOP 1 text FROM master..custom_models_reasonfortestingtext rftt WHERE rftt.reason_for_testing_id = rft.id AND language_id = 1) reason_for_testing_e
                OUTER APPLY (SELECT TOP 1 text FROM master..custom_models_reasonfortestingtext rftt WHERE rftt.reason_for_testing_id = rft.id AND language_id = 2) reason_for_testing_f
        
GO
/****** Object:  View [dbo].[assessment_process_results_candidates_report_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[assessment_process_results_candidates_report_vw] AS
               SELECT
                    ats.id,
                    ats.first_name as 'assessment_process_candidate_first_name',
                    ats.last_name as 'assessment_process_candidate_last_name',
                    ats.email as 'assessment_process_candidate_email',
                    apv.id as 'assessment_process_id',
                    apv.reference_number as 'assessment_process_reference_number',
                    apv.closing_date as 'assessment_process_closing_date',
                    apv.allow_booking_external_tc as 'assessment_process_allow_booking_external_tc',
                    apv.dept_id as 'assessment_process_dept_id ',
                    apv.dept_eabrv as 'assessment_process_dept_eabrv',
                    apv.dept_fabrv as 'assessment_process_dept_fabrv',
                    apv.dept_edesc as 'assessment_process_dept_edesc',
                    apv.dept_fdesc as 'assessment_process_dept_fdesc',
                    bc.id as 'billing_contact_id',
                    bc.first_name as 'billing_contact_first_name',
                    bc.last_name as 'billing_contact_last_name ',
                    bc.email as 'billing_contact_email',
                    tst.id as 'test_skill_type_id',
                    tst.codename as 'test_skill_type_codename',
                    type_e.text as 'test_skill_type_name_en',
                    type_f.text as 'test_skill_type_name_fr',
                    ats.test_skill_sub_type_id as 'test_skill_sub_type_id',
                    sled.codename as 'test_skill_sub_type_codename',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_e.text
                        WHEN tst.id = 3 THEN occ_sub_type_e.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_en',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_f.text
                        WHEN tst.id = 3 THEN occ_sub_type_f.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_fr',
                    rft.id as 'reason_for_testing_id',
                    reason_for_testing_e.text as 'reason_for_testing_name_en',
                    reason_for_testing_f.text as 'reason_for_testing_name_fr',
                    ats.level_required,
                    apv.request_sent,
                    hcrc.reservation_code,
                    hcrc.status_id as 'consumed_reservation_code_status_id',
                    hcrc.test_session_id,
                    hcrc.candidate_id,
                    hcrc.assigned_test_id,
                    at.status_id as 'assigned_test_status_id',
                    atst.codename as 'assigned_test_status_codename',
                    at.en_converted_score,
                    at.fr_converted_score,
                    at.submit_date,
                    at.total_score,
                    mu.username as 'user_username',
                    mu.email as 'user_email',
                    mu.pri as 'user_pri',
                    mu.military_nbr as 'user_military_nbr',
                    mu.first_name as 'user_first_name',
                    mu.last_name as 'user_last_name',
                    td.test_code as 'test_definition_test_code',
                    td.en_name as 'test_definition_en_name',
                    td.fr_name as 'test_definition_fr_name'
                FROM master..custom_models_assessmentprocessassignedtestspecs ats
                RIGHT JOIN master..custom_models_historicalconsumedreservationcodes hcrc on hcrc.assessment_process_assigned_test_specs_id = ats.id
                LEFT JOIN master..custom_models_billingcontact bc on bc.id = ats.billing_contact_id
                JOIN master..assessment_process_vw apv on apv.id = ats.assessment_process_id
                JOIN master..cms_models_testskilltype tst on tst.id = ats.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM master..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM master..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN master..cms_models_testskillsledesc sled on sled.id = ats.test_skill_sub_type_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM master..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM master..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                OUTER APPLY (SELECT TOP 1 occdte.text FROM master..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = sled.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM master..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = sled.id AND occdtf.language_id = 2) occ_sub_type_f
                JOIN master..custom_models_reasonfortesting rft on rft.id = ats.reason_for_testing_id
                OUTER APPLY (SELECT TOP 1 text FROM master..custom_models_reasonfortestingtext rftt WHERE rftt.reason_for_testing_id = rft.id AND language_id = 1) reason_for_testing_e
                OUTER APPLY (SELECT TOP 1 text FROM master..custom_models_reasonfortestingtext rftt WHERE rftt.reason_for_testing_id = rft.id AND language_id = 2) reason_for_testing_f
                LEFT JOIN master..custom_models_assignedtest at on at.id = hcrc.assigned_test_id
                LEFT JOIN master..custom_models_assignedteststatus atst on atst.id = at.status_id
                LEFT JOIN master..user_management_models_user mu on mu.id = hcrc.candidate_id
                LEFT JOIN master..cms_models_testdefinition td on td.id = at.test_id
                WHERE atst.codename IN ('submitted', 'quit')
        
GO
/****** Object:  Table [dbo].[user_management_models_taextendedprofile]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_taextendedprofile](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[department_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ta_extended_profile_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[ta_extended_profile_vw] AS
                SELECT
                    tep.id,
                    u.id as 'user_id',
                    u.first_name,
                    u.last_name,
                    u.email,
                    dep.DEPT_ID as 'dept_id',
                    dep.EABRV as 'dept_eabrv',
                    dep.FABRV as 'dept_fabrv',
                    dep.EDESC as 'dept_edesc',
                    dep.FDESC as 'dept_fdesc'
                FROM master..user_management_models_taextendedprofile tep
                JOIN master..user_management_models_user u on u.id = tep.user_id
                LEFT JOIN master..CAT_REF_DEPARTMENTS_VW dep on dep.DEPT_ID = tep.department_id
                
GO
/****** Object:  Table [dbo].[cms_models_itemdevelopmentstatuses]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemdevelopmentstatuses](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[en_name] [nvarchar](25) NOT NULL,
	[fr_name] [nvarchar](25) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_item_development_status_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemresponseformats]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemresponseformats](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[en_name] [nvarchar](25) NOT NULL,
	[fr_name] [nvarchar](25) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_item_response_format_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_items]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_items](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[version] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[active_status] [bit] NOT NULL,
	[development_status_id] [int] NOT NULL,
	[response_format_id] [int] NULL,
	[item_bank_id] [int] NOT NULL,
	[shuffle_options] [bit] NOT NULL,
	[item_type_id] [int] NOT NULL,
	[last_modified_by_user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [system_id_combined_with_version_must_be_unique_in_item_bank] UNIQUE NONCLUSTERED 
(
	[system_id] ASC,
	[version] ASC,
	[item_bank_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemhistoricalids]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemhistoricalids](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[historical_id] [nvarchar](50) NOT NULL,
	[item_bank_id] [int] NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_item_historical_id_and_item_bank_combination] UNIQUE NONCLUSTERED 
(
	[historical_id] ASC,
	[item_bank_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemtype]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemtype](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[item_latest_versions_data_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[item_latest_versions_data_vw] AS
				SELECT
					i.id as 'item_id',
					i.system_id,
					i.version,
					v.number_of_versions as 'number_of_versions',
					i.item_bank_id,
					i.modify_date,
					i.last_modified_by_user_id,
					i.active_status,
					i.shuffle_options,
					i.item_type_id,
                    it.codename as 'item_type_codename',
					ds.id as 'development_status_id',
					ds.codename as 'development_status_codename',
					ds.en_name as 'development_status_name_en',
					ds.fr_name as 'development_status_name_fr',
                    IIF (i.response_format_id is not NULL, rf.id, NULL) as 'response_format_id',
					IIF (i.response_format_id is not NULL, rf.codename, NULL) as 'response_format_codename',
					IIF (i.response_format_id is not NULL, rf.en_name, NULL) as 'response_format_name_en',
					IIF (i.response_format_id is not NULL, rf.fr_name, NULL) as 'response_format_name_fr',
					IIF (hi.id is not NULL, hi.historical_id, NULL) as 'historical_id'
				FROM master..cms_models_items i
                JOIN master..cms_models_itemtype it on it.id = i.item_type_id
				JOIN master..cms_models_itemdevelopmentstatuses ds on ds.id = i.development_status_id
				LEFT JOIN master..cms_models_itemresponseformats rf on rf.id = i.response_format_id
				LEFT JOIN master..cms_models_itemhistoricalids hi on hi.system_id = i.system_id
				OUTER APPLY (SELECT MAX(version) as 'number_of_versions' FROM master..cms_models_items it WHERE it.system_id = i.system_id) v
				WHERE i.id in (SELECT MAX(it.id) FROM master..cms_models_items it GROUP BY it.system_id)
        
GO
/****** Object:  View [dbo].[all_items_data_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[all_items_data_vw] AS
				SELECT
					i.id as 'item_id',
					i.system_id,
					i.version,
					v.number_of_versions as 'number_of_versions',
					i.item_bank_id,
					i.modify_date,
					i.last_modified_by_user_id,
					i.active_status,
                    i.shuffle_options,
                    i.item_type_id,
                    it.codename as 'item_type_codename',
					ds.id as 'development_status_id',
					ds.codename as 'development_status_codename',
					ds.en_name as 'development_status_name_en',
					ds.fr_name as 'development_status_name_fr',
					IIF (rf.id is not NULL, rf.id, NULL) as 'response_format_id',
					IIF (rf.codename is not NULL, rf.codename, NULL) as 'response_format_codename',
					IIF (rf.en_name is not NULL, rf.en_name, NULL) as 'response_format_name_en',
					IIF (rf.fr_name is not NULL, rf.fr_name, NULL) as 'response_format_name_fr',
					IIF (hi.id is not NULL, hi.historical_id, NULL) as 'historical_id'
				FROM master..cms_models_items i
                JOIN master..cms_models_itemtype it on it.id = i.item_type_id
				JOIN master..cms_models_itemdevelopmentstatuses ds on ds.id = i.development_status_id
				LEFT JOIN master..cms_models_itemresponseformats rf on rf.id = i.response_format_id
				LEFT JOIN master..cms_models_itemhistoricalids hi on hi.system_id = i.system_id
				OUTER APPLY (SELECT MAX(version) as 'number_of_versions' FROM master..cms_models_items it WHERE it.system_id = i.system_id) v
        
GO
/****** Object:  Table [dbo].[cms_models_itemdrafts]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemdrafts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[historical_id] [nvarchar](50) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[development_status_id] [int] NOT NULL,
	[item_id] [int] NOT NULL,
	[item_bank_id] [int] NOT NULL,
	[response_format_id] [int] NULL,
	[active_status] [bit] NOT NULL,
	[shuffle_options] [bit] NOT NULL,
	[item_type_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[item_drafts_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[item_drafts_vw] AS
				SELECT
					id.item_id as 'item_id',
					id.system_id,
					id.item_bank_id,
					id.modify_date,
					id.user_id,
					u.username,
					id.active_status,
					id.shuffle_options,
					id.item_type_id,
					it.codename as 'item_type_codename',
					ds.id as 'development_status_id',
					ds.codename as 'development_status_codename',
					ds.en_name as 'development_status_name_en',
					ds.fr_name as 'development_status_name_fr',
					IIF (rf.id is not NULL, rf.id, NULL) as 'response_format_id',
					IIF (rf.codename is not NULL, rf.codename, NULL) as 'response_format_codename',
					IIF (rf.en_name is not NULL, rf.en_name, NULL) as 'response_format_name_en',
					IIF (rf.fr_name is not NULL, rf.fr_name, NULL) as 'response_format_name_fr',
					id.historical_id as 'historical_id'
				FROM master..cms_models_itemdrafts id
				JOIN master..user_management_models_user u on u.id = id.user_id
				JOIN master..cms_models_itemtype it on it.id = id.item_type_id
				JOIN master..cms_models_itemdevelopmentstatuses ds on ds.id = id.development_status_id
				LEFT JOIN master..cms_models_itemresponseformats rf on rf.id = id.response_format_id
        
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfilecomplexity]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfilecomplexity](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[active] [bit] NOT NULL,
	[order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_user_accommodation_file_complexity_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcentertestadministrators]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcentertestadministrators](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_center_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[test_centers_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[test_centers_vw] AS
                SELECT
                    tc.id,
                    tc.name,
                    tc.postal_code,
                    tc.security_email,
                    tc.booking_delay,
                    tc.accommodations_friendly,
                    tc.modify_date,
                    tc.ola_authorized,
                    tcta.number_of_tas,
                    tcr.number_of_rooms,
                    c.CNTRY_ID as 'country_id',
                    c.EABRV as 'country_eabrv',
                    c.FABRV as 'country_fabrv',
                    c.EDESC as 'country_edesc',
                    c.FDESC as 'country_fdesc',
                    d.DEPT_ID as 'dept_id',
                    d.EABRV as 'dept_eabrv',
                    d.FABRV as 'dept_fabrv',
                    d.EDESC as 'dept_edesc',
                    d.FDESC as 'dept_fdesc',
                    pending_requests.count as 'pending_accommodation_requests'
                FROM master..custom_models_testcenter tc
                OUTER APPLY (SELECT COUNT(id) as number_of_tas FROM master..custom_models_testcentertestadministrators WHERE test_center_id = tc.id) tcta
                OUTER APPLY (SELECT COUNT(id) as number_of_rooms FROM master..custom_models_testcenterrooms WHERE test_center_id = tc.id) tcr
                LEFT JOIN CAT_REF_DEPARTMENTS_VW d on d.DEPT_ID = tc.department_id
                LEFT JOIN CAT_REF_COUNTRY_VW c on c.CNTRY_ID = tc.country_id
                OUTER APPLY (SELECT COUNT(*) as 'count'
							 FROM master..custom_models_useraccommodationfile uaf
							 LEFT JOIN master..custom_models_testcentertestsessions tcts ON tcts.user_accommodation_file_id = uaf.id
							 WHERE uaf.test_center_id = tc.id 
							 AND uaf.status_id = (SELECT id 
												  FROM master..custom_models_useraccommodationfilestatus uafs 
												  WHERE uafs.codename = 'completed')
							 AND tcts.id is NULL
							 ) pending_requests
        
GO
/****** Object:  View [dbo].[selected_user_accommodation_file_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[selected_user_accommodation_file_vw] AS
                SELECT
                    uaf.id,
                    uaf.comments,
                    uaf.is_uit,
                    uaf.modify_date,
                    uaf.created_date,
                    uaf.rationale,
                    uaf.is_alternate_test_request,
                    ummu2.id as 'last_modified_by_user_id',
                    ummu2.email AS 'last_modified_by_user_email',
                    ummu2.first_name AS 'last_modified_by_user_first_name',
                    ummu2.last_name AS 'last_modified_by_user_last_name',
                    ummu3.id as 'assigned_to_user_id',
                    ummu3.email AS 'assigned_to_user_email',
                    ummu3.first_name AS 'assigned_to_user_first_name',
                    ummu3.last_name AS 'assigned_to_user_last_name',
                    uafs.id AS 'status_id',
                    uafs.codename AS 'status_codename',
                    uaf.complexity_id AS 'complexity_id',
                    uafc.codename AS 'complexity_codename',
                    uaf.test_center_id,
                    ummu.id AS 'user_id',
                    ummu.username AS 'user_username',
                    ummu.email AS 'user_email',
                    ummu.secondary_email AS 'user_secondary_email',
                    ummu.first_name AS 'user_first_name',
                    ummu.last_name AS 'user_last_name',
                    ummu.birth_date AS 'user_dob',
                    ummu.psrs_applicant_id AS 'user_psrs_applicant_id',
                    ummu.pri AS 'user_pri',
                    ummu.military_nbr AS 'user_military_nbr',
                    ummu.phone_number AS 'user_phone_number',
                    uaf.candidate_phone_number AS 'ola_phone_number',
                    CASE
                        -- Supervised Test (getting reference number from assessment process data)
                        WHEN uaf.is_uit = 0 THEN apatsv.assessment_process_closing_date
                        -- UIT Test
                        ELSE ui.validity_end_date
                    END as 'process_end_date',
                    CASE
                        -- Supervised Test (getting reference number from assessment process data)
                        WHEN uaf.is_uit = 0 THEN apatsv.assessment_process_reference_number
                        -- UIT Test
                        ELSE
                            CASE
                                -- Orderless Test (getting reference number from orderless financial data)
                                WHEN ofd.id is not NULL THEN ofd.reference_number
                                -- Ordered Test (getting reference number from historical test permission)
                                WHEN htp.id is not NULL THEN htp.staffing_process_number
                                -- Undefined reference number
                                ELSE NULL
                            END
                    END as 'reference_number',
                    tcv.name as 'test_center_name',
                    -- DEPT ID
                    CASE
                        -- Supervised Test (getting department data from user accommodation file request)
                        WHEN uaf.is_uit = 0 THEN tcv.dept_id
                        -- UIT Test
                        ELSE
                            CASE
                                -- Orderless Test (getting department data from orderless financial data)
                                WHEN ofd.id is not NULL THEN dept_ref_from_ofd.DEPT_ID
                                -- Ordered Test (getting department data from historical test permission)
                                WHEN htp.id is not NULL THEN
                                    CASE
                                        -- MATCHING DEPT_ID FOUND
                                        WHEN (SELECT COUNT(*) FROM master..CAT_REF_DEPARTMENTS_VW WHERE CONVERT(varchar, DEPT_ID) = CONVERT(varchar, htp.department_ministry_code)) > 0 THEN dep_ref_from_htp.DEPT_ID
                                        -- PSC DEPT_ID
                                        ELSE 104
                                    END
                                -- Undefined department
                                ELSE NULL
                            END
                    END as 'dept_id',
                    -- DEPT ABRV (EN)
                    CASE
                        -- Supervised Test (getting department data from user accommodation file request)
                        WHEN uaf.is_uit = 0 THEN tcv.dept_eabrv
                        -- UIT Test
                        ELSE
                            CASE
                                -- Orderless Test (getting department data from orderless financial data)
                                WHEN ofd.id is not NULL THEN dept_ref_from_ofd.EABRV
                                -- Ordered Test (getting department data from historical test permission)
                                WHEN htp.id is not NULL THEN
                                    CASE
                                        -- MATCHING DEPT_ID FOUND
                                        WHEN (SELECT COUNT(*) FROM master..CAT_REF_DEPARTMENTS_VW WHERE CONVERT(varchar, DEPT_ID) = CONVERT(varchar, htp.department_ministry_code)) > 0 THEN dep_ref_from_htp.EABRV
                                        -- PSC DEPT_EABRV
                                        ELSE 'PSC'
                                    END
                                -- Undefined department
                                ELSE NULL
                            END
                    END as 'dept_eabrv',
                    -- DEPT ABRV (FR)
                    CASE
                        -- Supervised Test (getting department data from user accommodation file request)
                        WHEN uaf.is_uit = 0 THEN tcv.dept_fabrv
                        -- UIT Test
                        ELSE
                            CASE
                                -- Orderless Test (getting department data from orderless financial data)
                                WHEN ofd.id is not NULL THEN dept_ref_from_ofd.FABRV
                                -- Ordered Test (getting department data from historical test permission)
                                WHEN htp.id is not NULL THEN
                                    CASE
                                        -- MATCHING DEPT_ID FOUND
                                        WHEN (SELECT COUNT(*) FROM master..CAT_REF_DEPARTMENTS_VW WHERE CONVERT(varchar, DEPT_ID) = CONVERT(varchar, htp.department_ministry_code)) > 0 THEN dep_ref_from_htp.FABRV
                                        -- PSC DEPT_FABRV
                                        ELSE 'CFP'
                                    END
                                -- Undefined department
                                ELSE NULL
                            END
                    END as 'dept_fabrv',
                    -- DEPT DESCRIPTION (EN)
                    CASE
                        -- Supervised Test (getting department data from user accommodation file request)
                        WHEN uaf.is_uit = 0 THEN tcv.dept_edesc
                        -- UIT Test
                        ELSE
                            CASE
                                -- Orderless Test (getting department data from orderless financial data)
                                WHEN ofd.id is not NULL THEN dept_ref_from_ofd.EDESC
                                -- Ordered Test (getting department data from historical test permission)
                                WHEN htp.id is not NULL THEN
                                    CASE
                                        -- MATCHING DEPT_ID FOUND
                                        WHEN (SELECT COUNT(*) FROM master..CAT_REF_DEPARTMENTS_VW WHERE CONVERT(varchar, DEPT_ID) = CONVERT(varchar, htp.department_ministry_code)) > 0 THEN dep_ref_from_htp.EDESC
                                        -- PSC DEPT_EDESC
                                        ELSE 'Public Service Commission'
                                    END
                                -- Undefined department
                                ELSE NULL
                            END
                    END as 'dept_edesc',
                    -- DEPT DESCRIPTION (FR)
                    CASE
                        -- Supervised Test (getting department data from user accommodation file request)
                        WHEN uaf.is_uit = 0 THEN tcv.dept_fdesc
                        -- UIT Test
                        ELSE
                            CASE
                                -- Orderless Test (getting department data from orderless financial data)
                                WHEN ofd.id is not NULL THEN dept_ref_from_ofd.FDESC
                                -- Ordered Test (getting department data from historical test permission)
                                WHEN htp.id is not NULL THEN
                                    CASE
                                        -- MATCHING DEPT_ID FOUND
                                        WHEN (SELECT COUNT(*) FROM master..CAT_REF_DEPARTMENTS_VW WHERE CONVERT(varchar, DEPT_ID) = CONVERT(varchar, htp.department_ministry_code)) > 0 THEN dep_ref_from_htp.FDESC
                                        -- PSC DEPT_FDESC
                                        ELSE 'Commission de la fonction publique'
                                    END
                                -- Undefined department
                                ELSE NULL
                            END
                    END as 'dept_fdesc',
                    -- PRIMARY CONTACT USER ID
                    CASE
                        -- Supervised Test (HR Contact)
                        WHEN hru.id is not NULL THEN  hru.id
                        -- UIT Test (TA Contact)
                        ELSE tau.id
                    END as 'primary_contact_user_id',
                    -- PRIMARY CONTACT USER EMAIL
                    CASE
                        -- Supervised Test (HR Contact)
                        WHEN hru.id is not NULL THEN  hru.email
                        -- UIT Test (TA Contact)
                        ELSE tau.email
                    END as 'primary_contact_user_email',
                    -- PRIMARY CONTACT USER FIRST NAME
                    CASE
                        -- Supervised Test (HR Contact)
                        WHEN hru.id is not NULL THEN  hru.first_name
                        -- UIT Test (TA Contact)
                        ELSE tau.first_name
                    END as 'primary_contact_user_first_name',
                    -- PRIMARY CONTACT USER LAST NAME
                    CASE
                        -- Supervised Test (HR Contact)
                        WHEN hru.id is not NULL THEN  hru.last_name
                        -- UIT Test (TA Contact)
                        ELSE tau.last_name
                    END as 'primary_contact_user_last_name',
                    -- TEST SKILL TYPE ID
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN ts.test_skill_type_id
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_id
                    END as 'test_skill_type_id',
                    -- TEST SKILL TYPE CODENAME
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN tst.codename
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_codename
                    END as 'test_skill_type_codename',
                    -- TEST SKILL TYPE NAME (EN)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN type_e.text
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_name_en
                    END as 'test_skill_type_name_en',
                    -- TEST SKILL TYPE NAME (FR)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN type_f.text
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_name_fr
                    END as 'test_skill_type_name_fr',
                    -- TEST SKILL SUB-TYPE ID
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sled.id
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occd.id
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_id
                    END as 'test_skill_sub_type_id',
                    -- TEST SKILL SUB-TYPE CODENAME
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sled.codename
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occd.codename
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_codename
                    END as 'test_skill_sub_type_codename',
                    -- TEST SKILL SUB-TYPE NAME (EN)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sle_sub_type_e.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occ_sub_type_e.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_name_en
                    END as 'test_skill_sub_type_name_en',
                    -- TEST SKILL SUB-TYPE NAME (FR)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sle_sub_type_f.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occ_sub_type_f.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_name_fr
                    END as 'test_skill_sub_type_name_fr'
                FROM master..custom_models_useraccommodationfile uaf
                JOIN master..user_management_models_user ummu ON ummu.id = uaf.user_id
                JOIN master..custom_models_useraccommodationfilestatus uafs ON uafs.id = uaf.status_id
                LEFT JOIN master..custom_models_useraccommodationfilecomplexity uafc ON uafc.id = uaf.complexity_id
                LEFT JOIN master..user_management_models_user ummu2 ON ummu2.id = uaf.last_modified_by_user_id
                LEFT JOIN master..user_management_models_user ummu3 ON ummu3.id = uaf.assigned_to_user_id
                -- using assigned test to get data (if UIT)
                OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_assignedtest at_1 WHERE at_1.user_accommodation_file_id = uaf.id ORDER BY at_1.id DESC) at
                LEFT JOIN master..user_management_models_user tau on tau.id = at.ta_user_id
                LEFT JOIN master..custom_models_uitinvites ui on ui.id = at.uit_invite_id
                LEFT JOIN master..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
                LEFT JOIN master..CAT_REF_DEPARTMENTS_VW dept_ref_from_ofd on dept_ref_from_ofd.DEPT_ID = ofd.department_ministry_id
                OUTER APPLY (SELECT TOP 1 htp_1.id, htp_1.test_order_number, htp_1.department_ministry_code, htp_1.staffing_process_number FROM master..cms_models_historicaltestpermissions htp_1 WHERE htp_1.test_order_number = at.test_order_number ORDER BY htp_1.history_date DESC) htp
                LEFT JOIN master..CAT_REF_DEPARTMENTS_VW dep_ref_from_htp on CONVERT(varchar, dep_ref_from_htp.DEPT_ID) = CONVERT(varchar, htp.department_ministry_code)
                LEFT JOIN master..cms_models_testskill ts on ts.test_definition_id = at.test_id
                LEFT JOIN master..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM master..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM master..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN master..cms_models_testskillsle tssle on tssle.test_skill_id = ts.id
                LEFT JOIN master..cms_models_testskillsledesc sled on sled.id = tssle.test_skill_sle_desc_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM master..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM master..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                LEFT JOIN master..cms_models_testskilloccupational tsocc on tsocc.test_skill_id = ts.id
                LEFT JOIN master..cms_models_testskilloccupationaldesc occd on occd.id = tsocc.test_skill_occupational_desc_id
                OUTER APPLY (SELECT TOP 1 occdte.text FROM master..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = occd.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM master..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = occd.id AND occdtf.language_id = 2) occ_sub_type_f
                -- ----------------------------------------------------
                -- using consumed reservation codes to get data (is Supervised)
                OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_historicalconsumedreservationcodes hcrc_1 WHERE hcrc_1.user_accommodation_file_id = uaf.id ORDER BY hcrc_1.history_date DESC) hcrc
                LEFT JOIN master..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = hcrc.assessment_process_assigned_test_specs_id
                LEFT JOIN master..custom_models_assessmentprocess ap on ap.id = apatsv.assessment_process_id
                LEFT JOIN master..user_management_models_user hru on hru.id = ap.user_id
                -- ------------------------------------------------------------------------
                LEFT JOIN master..test_centers_vw tcv on tcv.id = uaf.test_center_id
    
GO
/****** Object:  Table [dbo].[cms_models_itemcontent]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemcontent](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[content_order] [int] NOT NULL,
	[item_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemcontenttext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemcontenttext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[item_content_id] [int] NOT NULL,
	[language_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[item_content_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[item_content_vw] AS
				SELECT
					ic.id as 'item_content_id',
					ic.content_type,
					ic.content_order,
					ic.modify_date as 'item_content_modify_date',
					ic.item_id,
					ict.id as 'item_content_text_id',
					ict.text as 'text',
					ict.modify_date as 'item_content_text_modify_date',
					ict.language_id
				FROM master..cms_models_itemcontent ic
				LEFT JOIN master..cms_models_itemcontenttext ict on ict.item_content_id = ic.id
        
GO
/****** Object:  Table [dbo].[user_management_models_historicalpermissionrequest]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicalpermissionrequest](
	[permission_request_id] [int] NOT NULL,
	[goc_email] [nvarchar](254) NOT NULL,
	[pri] [nvarchar](10) NOT NULL,
	[supervisor] [nvarchar](180) NOT NULL,
	[supervisor_email] [nvarchar](254) NOT NULL,
	[rationale] [nvarchar](300) NOT NULL,
	[request_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[permission_requested_id] [int] NULL,
	[military_nbr] [nvarchar](9) NOT NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[financial_report_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[financial_report_vw] AS
				SELECT TOP 100 percent
                    IIF (u.id is null, v.last_name, u.last_name) as "candidate_last_name",
                    IIF (u.id is null, v.first_name, u.first_name) as "candidate_first_name",
                    IIF (u.id is null, v.pri, u.pri) as "candidate_pri",
                    IIF (u.id is null, v.military_nbr, u.military_nbr) as "candidate_military_nbr",
                    ta.goc_email as "ta_email",
                    de.edesc as "ta_org_en",
                    de.fdesc as "ta_org_fr",
                    IIF (dp.edesc is null and a.orderless_financial_data_id is not null, odp.edesc, IIF(dp.edesc is null, 'Public Service Commission', dp.edesc)) as "requesting_dep_en",
                    IIF (dp.fdesc is null and a.orderless_financial_data_id is not null, odp.fdesc, IIF(dp.fdesc is null, 'Public Service Commission', dp.fdesc)) as "requesting_dep_fr",
                    a.test_order_number as "order_no",
                    p.staffing_process_number as "assessment_process",
                    o.reference_number as "reference_number",
                    CASE
                        WHEN a.orderless_financial_data_id is not null THEN o.department_ministry_id
                        WHEN ISNUMERIC(p.department_ministry_code) = 1 THEN (p.department_ministry_code) ELSE (104)
                    END as "org_code",
                    IIF (a.orderless_financial_data_id is not null, o.fis_organisation_code , p.is_org) as "fis_org_code",
                    IIF (a.orderless_financial_data_id is not null, o.fis_reference_code, p.is_ref) as "fis_ref_code",
                    IIF (a.orderless_financial_data_id is not null, o.billing_contact_name, p.billing_contact) as "billing_contact_name",
                    dbo.status2str('fr', a.status_id) AS "test_status_fr",
                    dbo.status2str('en', a.status_id) AS "test_status_en",
                    a.is_invalid as "is_invalid",
                    cast(a.submit_date as date) as "submit_date",
                    d.test_code as "test_code",
                    d.fr_name as "test_description_fr",
                    d.en_name as "test_description_en",
                    a.orderless_financial_data_id,
                    a.status_id as "test_status_id",
                    atst.codename as "test_status_codename",
                    a.test_id as "test_id",
                    a.id as "assigned_test_id"
                    FROM
                    master..custom_models_assignedtest a
                    JOIN master..custom_models_assignedteststatus atst on atst.id = a.status_id
                    OUTER APPLY (SELECT TOP 1 id, last_name, first_name, pri, military_nbr FROM master..user_management_models_historicaluser u1 WHERE u1.id = a.user_id AND u1.history_date < a.submit_date ORDER BY u1.history_date DESC) u
                    OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicalpermissionrequest pr WHERE pr.user_id = a.ta_user_id and pr.history_type = '+' and pr.permission_requested_id = 1 and pr.history_date < a.submit_date order by history_date desc) ta
                    JOIN master..user_management_models_user v on v.id = a.user_id
                    LEFT JOIN master..user_management_models_taextendedprofile e ON e.user_id = a.ta_user_id
                    LEFT JOIN master..CAT_REF_DEPARTMENTS_VW de ON de.DEPT_ID = e.department_id
                    OUTER APPLY (SELECT TOP 1 staffing_process_number, department_ministry_code, is_org, is_ref, billing_contact FROM master..cms_models_historicaltestpermissions h WHERE h.test_order_number = a.test_order_number AND h.user_id = a.ta_user_id AND h.history_date < a.submit_date AND h.history_type = '+' ORDER BY h.history_date DESC) p
                    LEFT JOIN master..CAT_REF_DEPARTMENTS_VW dp ON cast(dp.DEPT_ID as nvarchar) = p.department_ministry_code
                    JOIN master..cms_models_testdefinition d ON d.id = a.test_id
                    LEFT JOIN master..custom_models_orderlessfinancialdata o ON o.id = a.orderless_financial_data_id
                    LEFT JOIN master..CAT_REF_DEPARTMENTS_VW odp ON cast(odp.DEPT_ID as nvarchar) = o.department_ministry_id

                    WHERE
                    a.status_id IN  (SELECT id FROM master..custom_models_assignedteststatus WHERE codename in ('submitted', 'quit'))

                    ORDER BY
                    a.test_order_number, a.test_id, a.status_id, a.id DESC
        
GO
/****** Object:  Table [dbo].[cms_models_itemoption]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemoption](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[option_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[score] [nvarchar](25) NULL,
	[option_order] [int] NOT NULL,
	[item_id] [int] NOT NULL,
	[exclude_from_shuffle] [bit] NOT NULL,
	[historical_option_id] [nvarchar](50) NULL,
	[rationale] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemoptiontext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemoptiontext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[item_option_id] [int] NOT NULL,
	[language_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[item_option_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[item_option_vw] AS
				SELECT
                    io.id as 'item_option_id',
                    io.option_type,
                    io.option_order,
					io.exclude_from_shuffle,
                    io.historical_option_id,
                    io.rationale,
                    io.modify_date as 'item_option_modify_date',
                    io.item_id,
                    io.score,
                    iot.id as 'item_option_text_id',
                    iot.text as 'text',
                    iot.modify_date as 'item_option_text_modify_date',
                    iot.language_id
                FROM master..cms_models_itemoption io
                LEFT JOIN master..cms_models_itemoptiontext iot on iot.item_option_id = io.id
        
GO
/****** Object:  Table [dbo].[cms_models_itemcontentdrafts]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemcontentdrafts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[content_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[content_order] [int] NOT NULL,
	[item_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemcontenttextdrafts]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemcontenttextdrafts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[item_content_id] [int] NOT NULL,
	[language_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[item_content_drafts_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[item_content_drafts_vw] AS
				SELECT 
					c.id as 'item_content_id',
					c.system_id,
					c.content_type,
					c.modify_date as 'item_content_modify_date',
					c.content_order,
					c.item_id,
					c.user_id,
					u.username,
					ct.id as 'item_content_text_id',
					ct.text,
					ct.modify_date as 'item_content_text_modify_date',
					ct.language_id
				FROM master..cms_models_itemcontentdrafts c
				JOIN master..user_management_models_user u on u.id = c.user_id
				JOIN master..cms_models_itemcontenttextdrafts ct on ct.item_content_id = c.id
        
GO
/****** Object:  Table [dbo].[cms_models_itemoptiondrafts]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemoptiondrafts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[option_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[score] [nvarchar](25) NULL,
	[option_order] [int] NOT NULL,
	[item_id] [int] NOT NULL,
	[exclude_from_shuffle] [bit] NOT NULL,
	[historical_option_id] [nvarchar](50) NULL,
	[rationale] [nvarchar](max) NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemoptiontextdrafts]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemoptiontextdrafts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[item_option_id] [int] NOT NULL,
	[language_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[item_option_drafts_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[item_option_drafts_vw] AS
				SELECT 
					o.id as 'item_option_id',
					o.system_id,
					o.option_type,
					o.modify_date as 'item_option_modify_date',
					o.option_order,
					o.exclude_from_shuffle,
					o.historical_option_id,
					o.rationale,
					o.score,
					o.item_id,
					o.user_id,
					u.username,
					ot.id as 'item_option_text_id',
					ot.text,
					ot.modify_date as 'item_option_text_modify_date',
					ot.language_id
				FROM master..cms_models_itemoptiondrafts o
				JOIN master..user_management_models_user u on u.id = o.user_id
				JOIN master..cms_models_itemoptiontextdrafts ot on ot.item_option_id = o.id
        
GO
/****** Object:  View [dbo].[user_accommodation_file_tests_to_administer_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[user_accommodation_file_tests_to_administer_vw] AS
                SELECT
                    td.id,
                    td.parent_code,
                    td.test_code,
                    td.version,
                    td.en_name as 'name_en',
	                td.fr_name as 'name_fr',
                    td.active,
                    td.version_notes,
                    td.is_uit,
                    tst.id as 'test_skill_type_id',
                    tst.codename as 'test_skill_type_codename',
                    type_e.text as 'test_skill_type_name_en',
                    type_f.text as 'test_skill_type_name_fr',
                    CASE
                        WHEN tst.id = 2 THEN sled.id
                        WHEN tst.id = 3 THEN occd.id
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_id',
                    CASE
                        WHEN tst.id = 2 THEN sled.codename
                        WHEN tst.id = 3 THEN occd.codename
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_codename',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_e.text
                        WHEN tst.id = 3 THEN occ_sub_type_e.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_en',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_f.text
                        WHEN tst.id = 3 THEN occ_sub_type_f.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_fr'
                FROM master..cms_models_testdefinition td
                JOIN master..cms_models_testskill ts on ts.test_definition_id = td.id
                LEFT JOIN master..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM master..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM master..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN master..cms_models_testskillsle tssle on tssle.test_skill_id = ts.id
                LEFT JOIN master..cms_models_testskillsledesc sled on sled.id = tssle.test_skill_sle_desc_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM master..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM master..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                LEFT JOIN master..cms_models_testskilloccupational tsocc on tsocc.test_skill_id = ts.id
                LEFT JOIN master..cms_models_testskilloccupationaldesc occd on occd.id = tsocc.test_skill_occupational_desc_id
                OUTER APPLY (SELECT TOP 1 occdte.text FROM master..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = occd.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM master..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = occd.id AND occdtf.language_id = 2) occ_sub_type_f
                -- Active, non-public and non-archived tests
                WHERE td.active = 1 AND td.is_public = 0 AND td.archived = 0
    
GO
/****** Object:  View [dbo].[test_order_and_reference_numbers_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[test_order_and_reference_numbers_vw] AS
				SELECT
					test_order_number,
					reference_number,
					ta_user_id,
					IIF(u.id is not null, u.username, v.username) AS 'ta_username',
					IIF(u.id is not null, u.email, v.email) AS 'ta_email',
					staffing_process_number
				FROM (

					SELECT DISTINCT 
						htp.test_order_number as 'test_order_number', 
						NULL as 'reference_number',
						htp.user_id as 'ta_user_id', 
						htp.staffing_process_number as 'staffing_process_number'
					FROM master..cms_models_historicaltestpermissions htp

					UNION

					SELECT DISTINCT
						NULL as 'test_order_number',
						ofd.reference_number as 'reference_number',
						CASE
							WHEN hutac.ta_user_id is not NULL THEN hutac.ta_user_id
							WHEN at.orderless_financial_data_id is not NULL THEN at.ta_user_id
							WHEN ui.ta_user_id is not NULL THEN ui.ta_user_id
						ELSE
							NULL
						END as 'ta_user_id',
						NULL as 'staffing_process_number'
					FROM master..custom_models_orderlessfinancialdata ofd
					OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_historicalunsupervisedtestaccesscode hutac1 WHERE hutac1.orderless_financial_data_id = ofd.id AND hutac1.history_type = '+' AND hutac1.orderless_financial_data_id is not NULL ORDER BY hutac1.history_date DESC) hutac
					LEFT JOIN master..custom_models_assignedtest at on at.orderless_financial_data_id = ofd.id
					LEFT JOIN master..custom_models_uitinvites ui on ui.id = ofd.uit_invite_id
				) r
				OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicaluser u1 WHERE u1.id = r.ta_user_id ORDER BY u1.history_date DESC) u
				JOIN master..user_management_models_user v on v.id = r.ta_user_id
        
GO
/****** Object:  Table [dbo].[cms_models_testpermissions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testpermissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date_assigned] [datetime2](7) NOT NULL,
	[expiry_date] [date] NOT NULL,
	[test_order_number] [nvarchar](12) NOT NULL,
	[staffing_process_number] [nvarchar](50) NOT NULL,
	[department_ministry_code] [nvarchar](10) NOT NULL,
	[is_org] [nvarchar](16) NOT NULL,
	[is_ref] [nvarchar](20) NOT NULL,
	[billing_contact] [nvarchar](180) NOT NULL,
	[billing_contact_info] [nvarchar](255) NOT NULL,
	[test_id] [int] NOT NULL,
	[reason_for_modif_or_del] [nvarchar](300) NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[active_tests_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[active_tests_vw] AS
				SELECT
                    at.id as 'assigned_test_id',
                    u.id as 'candidate_user_id',
                    u.username as 'candidate_username',
                    u.email as 'candidate_email',
                    u.first_name as 'candidate_first_name',
                    u.last_name as 'candidate_last_name',
                    ta.id as 'ta_user_id',
                    ta.username as 'ta_username',
                    ta.email as 'ta_email',
                    ta.first_name as 'ta_first_name',
                    ta.last_name as 'ta_last_name',
                    at.status_id as 'test_status_id',
                    atst.codename as 'test_status_codename',
                    CASE
                        WHEN tp.test_order_number is not NULL THEN tp.test_order_number
                        WHEN ofd.reference_number is not NULL THEN ofd.reference_number
                        WHEN apatsv.assessment_process_reference_number is not NULL THEN apatsv.assessment_process_reference_number
                        ELSE NULL
                    END as 'test_order_reference_number',
                    td.id as 'test_id',
                    td.test_code
                FROM master..custom_models_assignedtest at
                JOIN master..custom_models_assignedteststatus atst on atst.id = at.status_id
                JOIN master..user_management_models_user u on u.id = at.user_id
                JOIN master..user_management_models_user ta on ta.id = at.ta_user_id
                LEFT JOIN master..cms_models_testpermissions tp on tp.test_order_number = at.test_order_number AND tp.user_id = at.ta_user_id
                LEFT JOIN master..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
                LEFT JOIN master..custom_models_consumedreservationcodes crc on crc.assigned_test_id = at.id
                LEFT JOIN master..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = crc.assessment_process_assigned_test_specs_id
                LEFT JOIN master..cms_models_testdefinition td on td.id = at.test_id
                WHERE at.status_id in (SELECT id FROM master..custom_models_assignedteststatus WHERE codename in ('assigned', 'checked_in', 'active', 'transition', 'locked', 'paused', 'pre_test'))
        
GO
/****** Object:  Table [dbo].[user_management_models_custompermission2fa]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_custompermission2fa](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[is_2fa_active] [bit] NOT NULL,
	[custom_permission_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[custom_permission_2fa_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[custom_permission_2fa_vw] AS
                SELECT
                    cp2fa.id as 'custom_permission2fa_id',
                    cp2fa.is_2fa_active as 'is_2fa_active',
                    cp.permission_id as 'permission_id',
                    cp.en_name as 'en_name',
                    cp.fr_name as 'fr_name',
                    cp.en_description as 'en_description',
                    cp.fr_description as 'fr_description'
                FROM master..[user_management_models_custompermission2fa] cp2fa
                JOIN master..[user_management_models_custompermissions] cp on cp.permission_id=cp2fa.custom_permission_id
GO
/****** Object:  View [dbo].[virtual_test_session_details_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[virtual_test_session_details_vw] AS
                SELECT
                    vtms.id,
                    vtms.candidate_id,
                    vtms.start_time,
                    vtms.date,
                    vtms.end_time,
                    vtms.meeting_link,
                    stte.text as 'test_skill_type_en_name',
                    sttf.text as 'test_skill_type_fr_name',
	                CASE
                        -- SLE Sub Type
                        WHEN tsd.test_skill_type_id = (SELECT id FROM master..cms_models_testskilltype WHERE codename = 'sle') THEN (SELECT text FROM master..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = tsd.test_skill_sub_type_id AND language_id = 1)
                        ELSE
                            -- OCCUPATIONAL Sub Type
                            CASE
                                WHEN tsd.test_skill_type_id = (SELECT id FROM master..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM master..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = tsd.test_skill_sub_type_id AND language_id = 1)
                            ELSE
                                NULL
                            END
                    END AS 'test_skill_sub_type_en_name',
                    CASE
                        -- SLE Sub Type
                        WHEN tsd.test_skill_type_id = (SELECT id FROM master..cms_models_testskilltype WHERE codename = 'SLE') THEN (SELECT text FROM master..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = tsd.test_skill_sub_type_id AND language_id = 2)
                        ELSE
                            -- OCCUPATIONAL Sub Type
                            CASE
                                WHEN tsd.test_skill_type_id = (SELECT id FROM master..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM master..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = tsd.test_skill_sub_type_id AND language_id = 2)
                            ELSE
                                NULL
                            END
                    END AS 'test_skill_sub_type_fr_name',
					candidate.email as 'candidate_email',
					ap.allow_last_minute_cancellation_tc,
					tcola.booking_delay as 'test_center_booking_delay'
                FROM master..custom_models_virtualteamsmeetingsession vtms
                JOIN master..custom_models_testcentertestsessions ts on ts.id=vtms.test_session_id
                JOIN master..custom_models_testcentertestsessiondata tsd on tsd.id=ts.test_session_data_id
                JOIN master..cms_models_testskilltypetext stte on stte.test_skill_type_id = tsd.test_skill_type_id AND stte.language_id = 1
                JOIN master..cms_models_testskilltypetext sttf on sttf.test_skill_type_id = tsd.test_skill_type_id AND sttf.language_id = 2
				JOIN master..user_management_models_user candidate ON vtms.candidate_id=candidate.id
				JOIN master..custom_models_assessmentprocessassignedtestspecs apats ON ts.assessment_process_assigned_test_specs_id=apats.id
				JOIN master..custom_models_assessmentprocess ap ON ap.id=apats.assessment_process_id
				LEFT JOIN master..custom_models_testcenterolaconfigs tcola ON tcola.test_center_id=ts.test_center_id
    
GO
/****** Object:  View [dbo].[test_data_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[test_data_vw] AS
				SELECT 
					at.id as 'assigned_test_id',
					at.status_id as 'test_status_id',
					atst.codename as 'test_status_codename',
					IIF (hta.id is not NULL, hta.id, ta.id) as 'ta_user_id',
					IIF (hta.id is not NULL, hta.username, ta.username) as 'ta_username',
					IIF (hta.id is not NULL, hta.email, ta.email) as 'ta_email',
					IIF (hu.id is not NULL, hu.id, u.id) as 'candidate_user_id',
					IIF (hu.id is not NULL, hu.username, u.username) as 'candidate_username',
					IIF (hu.id is not NULL, hu.email, u.email) as 'candidate_email',
					IIF (hu.id is not NULL, hu.first_name, u.first_name) as 'candidate_first_name',
					IIF (hu.id is not NULL, hu.last_name, u.last_name) as 'candidate_last_name',
					td.id as 'test_id',
					td.en_name as 'en_test_name',
					td.fr_name as 'fr_test_name',
					td.version as 'test_version',
					IIF (htp.id is not NULL, htp.test_order_number, htp2.test_order_number) as 'test_order_number',
					IIF (htp.id is not NULL, htp.staffing_process_number, htp2.staffing_process_number) as 'staffing_process_number',
					htp3.ta_user_ids as 'allowed_ta_user_ids',
					ofd.reference_number as 'reference_number'
				FROM master..custom_models_assignedtest at
				JOIN master..custom_models_assignedteststatus atst on atst.id = at.status_id
				OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicaluser u1 WHERE u1.id = at.ta_user_id AND u1.history_date < at.submit_date ORDER BY u1.history_date DESC) hta
				JOIN master..user_management_models_user ta on ta.id = at.ta_user_id
				OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicaluser u2 WHERE u2.id = at.user_id AND u2.history_date < at.submit_date ORDER BY u2.history_date DESC) hu
				JOIN master..user_management_models_user u on u.id = at.user_id
				JOIN master..cms_models_testdefinition td on td.id = at.test_id
				OUTER APPLY (SELECT TOP 1 * FROM master..cms_models_historicaltestpermissions htp1 WHERE htp1.test_order_number = at.test_order_number AND htp1.history_date < at.submit_date ORDER BY htp1.history_date DESC) htp
				OUTER APPLY (SELECT TOP 1 * FROM master..cms_models_historicaltestpermissions htp1 WHERE htp1.test_order_number = at.test_order_number ORDER BY htp1.history_date DESC) htp2
				OUTER APPLY (SELECT string_agg(ta_user_id_list.user_id, ',') within group (ORDER BY ta_user_id_list.user_id) as 'ta_user_ids' FROM (SELECT DISTINCT user_id FROM master..cms_models_historicaltestpermissions htp WHERE htp.history_type = '+' AND htp.test_order_number = at.test_order_number) ta_user_id_list) htp3
				LEFT JOIN master..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
        
GO
/****** Object:  View [dbo].[billing_contact_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[billing_contact_vw] AS
                SELECT
                bc.id,
                bc.first_name,
                bc.last_name,
                bc.email,
                bc.fis_organisation_code,
                bc.fis_reference_code,
                bc.user_id,
                bc.department_id as "dept_id",
                odv.EDESC as "dept_edesc",
                odv.FDESC as "dept_fdesc",
                odv.EABRV as "dept_eabrv",
                odv.FABRV as "dept_fabrv"
                FROM master..custom_models_billingcontact bc
                LEFT JOIN master..CAT_REF_DEPARTMENTS_VW odv ON bc.department_id = odv.DEPT_ID
            
GO
/****** Object:  View [dbo].[test_center_ola_vacation_block_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[test_center_ola_vacation_block_vw] AS
                SELECT 
                    vb.id,
                    vb.date_from,
                    vb.date_to,
                    vb.test_center_id,
                    vacation_block_availability_en.availability as 'availability_en',
                    vacation_block_availability_fr.availability as 'availability_fr'
                FROM master..custom_models_testcenterolavacationblock vb
                OUTER APPLY (SELECT vba1.availability as 'availability' FROM master..custom_models_testcenterolavacationblockavailability vba1 WHERE vba1.test_center_ola_vacation_block_id = vb.id AND vba1.language_id = 1) vacation_block_availability_en
                OUTER APPLY (SELECT vba2.availability as 'availability' FROM master..custom_models_testcenterolavacationblockavailability vba2 WHERE vba2.test_center_ola_vacation_block_id = vb.id AND vba2.language_id = 2) vacation_block_availability_fr
    
GO
/****** Object:  Table [dbo].[custom_models_scorerolaassignedtestsession]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_scorerolaassignedtestsession](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[candidate_id] [int] NOT NULL,
	[test_assessor_id] [int] NOT NULL,
	[test_session_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[scorer_ola_detailed_tests_to_assess_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[scorer_ola_detailed_tests_to_assess_vw] AS
                SELECT
                    TOP 100 PERCENT
                    ts.id,
                    tsd.date,
                    DATEPART(dw, tsd.date) as 'day_of_week_id',
                    tsd.start_time,
                    CONVERT(VARCHAR(8), tsd.start_time, 108) as 'simplified_start_time',
                    tsd.end_time,
                    CONVERT(VARCHAR(8), tsd.end_time, 108) as 'simplified_end_time',
                    IIF(tsd.test_skill_sub_type_id = (SELECT id FROM master..cms_models_testskillsledesc tssle WHERE tssle.codename = 'oe'),  1, 2) as 'language_id',
                    ts.is_standard,
                    ts.user_accommodation_file_id,
					u.id as 'candidate_user_id',
                    u.email as 'candidate_user_email',
                    u.first_name as 'candidate_user_first_name',
                    u.last_name 'candidate_user_last_name',
                    u.pri as 'candidate_pri',
                    tsd.candidate_phone_number,
                    crc.id as 'consumed_reservation_code_id',
                    crcs.id as 'consumed_reservation_code_status_id',
                    crcs.codename as 'consumed_reservation_code_status_codename',
                    apatsv.id as 'assessment_process_assigned_test_specs_id',
                    apatsv.assessment_process_reference_number,
                    booked_date.history_date as 'booked_date',
                    apatsv.assessment_process_closing_date,
                    apatsv.assessment_process_dept_id,
                    apatsv.assessment_process_dept_eabrv,
                    apatsv.assessment_process_dept_edesc,
                    apatsv.assessment_process_dept_fabrv,
                    apatsv.assessment_process_dept_fdesc,
                    apatsv.hr_user_id,
                    apatsv.hr_email,
                    apatsv.hr_first_name,
                    apatsv.hr_last_name,
                    apatsv.reason_for_testing_id,
                    apatsv.reason_for_testing_minimum_process_length,
                    apatsv.reason_for_testing_name_en,
                    apatsv.reason_for_testing_name_fr,
                    apatsv.level_required,
                    assessor_u.id as 'test_assessor_user_id',
                    assessor_u.email as 'test_assessor_user_email',
                    assessor_u.first_name as 'test_assessor_user_first_name',
                    assessor_u.last_name 'test_assessor_user_last_name',
					vtms.meeting_link 'meeting_link'
                FROM master..custom_models_testcentertestsessions ts
                JOIN master..custom_models_testcentertestsessiondata tsd ON tsd.id = ts.test_session_data_id
                LEFT JOIN master..custom_models_consumedreservationcodes crc ON crc.test_session_id = ts.id
                LEFT JOIN master..custom_models_consumedreservationcodestatus crcs ON crcs.id = crc.status_id
                LEFT JOIN master..assessment_process_assigned_test_specs_vw apatsv ON apatsv.id = ts.assessment_process_assigned_test_specs_id
				OUTER APPLY (SELECT crc_2.candidate_id FROM master..custom_models_consumedreservationcodes crc_2 WHERE crc_2.assessment_process_assigned_test_specs_id = ts.assessment_process_assigned_test_specs_id) cdd_id_from_consumed_reservation_code
				LEFT JOIN master..user_management_models_user u ON u.id = cdd_id_from_consumed_reservation_code.candidate_id
                LEFT JOIN master..custom_models_scorerolaassignedtestsession soats ON soats.test_session_id = ts.id
                LEFT JOIN master..user_management_models_user assessor_u ON assessor_u.id = soats.test_assessor_id
				LEFT JOIN master..custom_models_virtualteamsmeetingsession vtms ON ts.id = vtms.test_session_id
                OUTER APPLY (SELECT TOP 1 hcrc.history_date FROM master..custom_models_historicalconsumedreservationcodes hcrc WHERE hcrc.id = crc.id AND hcrc.status_id = (SELECT crcs.id FROM master..custom_models_consumedreservationcodestatus crcs WHERE crcs.codename = 'booked') ORDER BY hcrc.history_date DESC) booked_date
                -- ONLY GETTING OLA TEST SESSIONS THAT ARE BOOKED/PAIRED OR MULTI-SESSIONS SESSIONS (MULTI-SESSIONS MEANS THAT THIS IS COMING FROM A SCHEDULED ACCOMMODATION)
                WHERE tsd.test_skill_type_id = (SELECT id FROM master..cms_models_testskilltype tst WHERE tst.codename = 'sle')
                AND tsd.test_skill_sub_type_id IN (SELECT id FROM master..cms_models_testskillsledesc tssle WHERE tssle.codename IN ('oe', 'of'))
                AND (crcs.codename IN ('booked', 'paired_ola') OR (SELECT COUNT(*) FROM master..custom_models_testcentertestsessions ts_2 WHERE ts_2.assessment_process_assigned_test_specs_id = ts.assessment_process_assigned_test_specs_id) > 1)
                -- ORDERING BY HIGHEST PRIORITY (REASON FOR TESTING MINIMUM PROCESS LENGTH FOLLOWED BY ASSESSMENT PROCESS CLOSING DATE FOLLOWED BY BOOKED DATE)
                ORDER BY apatsv.reason_for_testing_minimum_process_length, apatsv.assessment_process_closing_date, booked_date.history_date
    
GO
/****** Object:  Table [dbo].[cms_models_itembankattributestext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembankattributestext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[item_bank_attribute_id] [int] NOT NULL,
	[language_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itembankattributevaluestext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembankattributevaluestext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](max) NULL,
	[item_bank_attribute_values_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itembankattributevalues]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembankattributevalues](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attribute_value_order] [int] NOT NULL,
	[item_bank_attribute_id] [int] NOT NULL,
	[attribute_value_type] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[item_bank_attribute_values_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[item_bank_attribute_values_vw] AS
                SELECT 
                    av.id as 'item_bank_attribute_value_id', 
                    av.attribute_value_order,
                    av.item_bank_attribute_id,
                    at_e.text as 'attribute_text_en',
	                at_f.text as 'attribute_text_fr',
                    av.attribute_value_type,
                    avt.id as 'item_bank_attribute_values_text_id',
                    avt.text
                FROM master..cms_models_itembankattributevalues av 
                JOIN master..cms_models_itembankattributevaluestext avt ON avt.item_bank_attribute_values_id = av.id
                JOIN master..cms_models_itembankattributestext at_e on at_e.item_bank_attribute_id = av.item_bank_attribute_id AND at_e.language_id = 1
                JOIN master..cms_models_itembankattributestext at_f on at_f.item_bank_attribute_id = av.item_bank_attribute_id AND at_f.language_id = 2
        
GO
/****** Object:  Table [dbo].[custom_models_reservationcodes]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_reservationcodes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[created_date] [date] NOT NULL,
	[reservation_code] [nvarchar](14) NOT NULL,
	[assessment_process_assigned_test_specs_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_assessment_process_assigned_test_specs_id] UNIQUE NONCLUSTERED 
(
	[assessment_process_assigned_test_specs_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[candidate_reservations_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[candidate_reservations_vw] AS
                SELECT
					rc.id as 'id',
					rc.reservation_code as 'reservation_code',
					apv.reference_number as 'assessment_process_reference_number',
					apv.closing_date as 'assessment_process_closing_date',
					apv.allow_booking_external_tc as 'allow_booking_external_tc',
					apv.dept_id as 'dept_id',
					apv.dept_eabrv as 'dept_eabrv',
					apv.dept_fabrv as 'dept_fabrv',
					apv.dept_edesc as 'dept_edesc',
					apv.dept_fdesc as 'dept_fdesc',
					apv.contact_email_for_candidates,
					tst.id as 'test_skill_type_id',
					type_e.text as 'test_skill_type_en_name',
					type_f.text as 'test_skill_type_fr_name',
					ats.test_skill_sub_type_id as 'test_skill_sub_type_id',
					CASE
						WHEN tst.id = 2 THEN sle_sub_type_e.text
						WHEN tst.id = 3 THEN occ_sub_type_e.text
						ELSE
							NULL
					END AS 'test_skill_sub_type_en_name',
					CASE
						WHEN tst.id = 2 THEN sle_sub_type_f.text
						WHEN tst.id = 3 THEN occ_sub_type_f.text
						ELSE
							NULL
					END AS 'test_skill_sub_type_fr_name',
					apv.request_sent
				FROM (SELECT id, reservation_code, assessment_process_assigned_test_specs_id FROM master..custom_models_reservationcodes
						UNION SELECT id, reservation_code, assessment_process_assigned_test_specs_id FROM master..custom_models_ConsumedReservationCodes) rc
				JOIN master..custom_models_assessmentprocessassignedtestspecs ats ON rc.assessment_process_assigned_test_specs_id=ats.id
				JOIN master..assessment_process_vw apv ON apv.id = ats.assessment_process_id
				JOIN master..cms_models_testskilltype tst ON tst.id = ats.test_skill_type_id
				OUTER APPLY (SELECT TOP 1 tstte.text FROM master..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
				OUTER APPLY (SELECT TOP 1 tsttf.text FROM master..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
				LEFT JOIN master..cms_models_testskillsledesc sled ON sled.id = ats.test_skill_sub_type_id
				OUTER APPLY (SELECT TOP 1 sledte.text FROM master..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
				OUTER APPLY (SELECT TOP 1 sledtf.text FROM master..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
				LEFT JOIN master..cms_models_testskilloccupationaldesc occd on occd.id = ats.test_skill_sub_type_id
				OUTER APPLY (SELECT TOP 1 occdte.text FROM master..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = sled.id AND occdte.language_id = 1) occ_sub_type_e
				OUTER APPLY (SELECT TOP 1 occdtf.text FROM master..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = sled.id AND occdtf.language_id = 2) occ_sub_type_f
        
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfiletesttime]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfiletesttime](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_section_time] [int] NOT NULL,
	[test_section_id] [int] NULL,
	[user_accommodation_file_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfileothermeasures]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfileothermeasures](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[other_measures] [nvarchar](max) NOT NULL,
	[user_accommodation_file_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfilebreakbank]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfilebreakbank](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[break_time] [int] NOT NULL,
	[user_accommodation_file_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[user_accommodation_file_data_for_details_popup_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[user_accommodation_file_data_for_details_popup_vw] AS
                SELECT
                    uaf.id,
                    uaf.is_uit,
                    uaf.created_date,
                    CASE
                        WHEN uafs.codename = 'completed' THEN CONVERT(VARCHAR(10), latest_completed_file_data.history_date, 23)
                        ELSE NULL
                    END as 'request_completed_date',
                    CASE
                        WHEN uafs.codename = 'administered' THEN CONVERT(VARCHAR(10), latest_administered_file_data.history_date, 23)
                        ELSE NULL
                    END as 'request_administered_date',
                    uaf.is_alternate_test_request,
                    uafs.id as 'status_id',
                    uafs.codename as 'status_codename',
                    uafst_en.text as 'status_name_en',
                    uafst_fr.text as 'status_name_fr',
                    ummu.id as 'user_id',
                    ummu.username as 'user_username',
                    ummu.email as 'user_email',
                    ummu.first_name as 'user_first_name',
                    ummu.last_name as 'user_last_name',
                    uaf.candidate_phone_number,
                    at_u.id as 'assigned_to_user_id',
                    at_u.email as 'assigned_to_user_email',
                    at_u_email_data.goc_email as 'assigned_to_user_goc_email',
                    at_u.first_name as 'assigned_to_user_first_name',
                    at_u.last_name 'assigned_to_user_last_name',
                    IIF(at_u.phone_number is NULL OR at_u.phone_number = '', NULL, at_u.phone_number) as 'assigned_to_phone_number',
                    hr_u.id as 'hr_user_id',
                    hr_u.email as 'hr_email',
                    hr_u.first_name as 'hr_first_name',
                    hr_u.last_name as 'hr_last_name',
                    IIF(hr_u.phone_number is NULL OR hr_u.phone_number = '', NULL, hr_u.phone_number) as 'hr_phone_number',
                    ta_u.id as 'ta_user_id',
                    ta_u.email as 'ta_email',
                    ta_u.first_name as 'ta_first_name',
                    ta_u.last_name as 'ta_last_name',
                    IIF(ta_u.phone_number is NULL OR ta_u.phone_number = '', NULL, ta_u.phone_number) as 'ta_phone_number',
                    uafom.other_measures,
                    -- TEST SKILL TYPE ID
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN ts.test_skill_type_id
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_id
                    END as 'test_skill_type_id',
                    -- TEST SKILL TYPE CODENAME
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN tst.codename
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_codename
                    END as 'test_skill_type_codename',
                    -- TEST SKILL TYPE NAME (EN)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN type_e.text
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_name_en
                    END as 'test_skill_type_name_en',
                    -- TEST SKILL TYPE NAME (FR)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN type_f.text
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_name_fr
                    END as 'test_skill_type_name_fr',
                    -- TEST SKILL SUB-TYPE ID
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sled.id
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occd.id
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_id
                    END as 'test_skill_sub_type_id',
                    -- TEST SKILL SUB-TYPE CODENAME
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sled.codename
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occd.codename
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_codename
                    END as 'test_skill_sub_type_codename',
                    -- TEST SKILL SUB-TYPE NAME (EN)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sle_sub_type_e.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occ_sub_type_e.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_name_en
                    END as 'test_skill_sub_type_name_en',
                    -- TEST SKILL SUB-TYPE NAME (FR)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sle_sub_type_f.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occ_sub_type_f.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_name_fr
                    END as 'test_skill_sub_type_name_fr',
                    -- REQUESTING DEPARTMENT DATA
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN
                        CASE
                            WHEN ofd.id is not NULL THEN ofd_dept_vw.DEPT_ID
                            WHEN tp_dept_vw.DEPT_ID is not NULL THEN tp_dept_vw.DEPT_ID
                            ELSE 104
                        END
                        -- Supervised Test
                        ELSE apatsv.assessment_process_dept_id
                    END as 'requesting_dept_id',
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN
                        CASE
                            WHEN ofd.id is not NULL THEN ofd_dept_vw.EABRV
                            WHEN tp_dept_vw.DEPT_ID is not NULL THEN tp_dept_vw.EABRV
                            ELSE 'PSC'
                        END
                        -- Supervised Test
                        ELSE apatsv.assessment_process_dept_eabrv
                    END as 'requesting_dept_abrv_en',
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN
                        CASE
                            WHEN ofd.id is not NULL THEN ofd_dept_vw.FABRV
                            WHEN tp_dept_vw.DEPT_ID is not NULL THEN tp_dept_vw.FABRV
                            ELSE 'CFP'
                        END
                        -- Supervised Test
                        ELSE apatsv.assessment_process_dept_fabrv
                    END as 'requesting_dept_abrv_fr',
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN
                        CASE
                            WHEN ofd.id is not NULL THEN ofd_dept_vw.EDESC
                            WHEN tp_dept_vw.DEPT_ID is not NULL THEN tp_dept_vw.EDESC
                            ELSE 'Public Service Commission'
                        END
                        -- Supervised Test
                        ELSE apatsv.assessment_process_dept_edesc
                    END as 'requesting_dept_desc_en',
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN
                        CASE
                            WHEN ofd.id is not NULL THEN ofd_dept_vw.EDESC
                            WHEN tp_dept_vw.DEPT_ID is not NULL THEN tp_dept_vw.EDESC
                            ELSE 'Commission de la fonction publique'
                        END
                        -- Supervised Test
                        ELSE apatsv.assessment_process_dept_fdesc
                    END as 'requesting_dept_desc_fr',
                    tc.name as 'test_center_name',
                    td.id as 'test_id',
                    td.parent_code,
                    td.test_code,
                    td.version,
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    uaftta.specified_test_description,
                    default_times.sum as 'total_default_time',
                    adjusted_times.sum as 'total_adjusted_time',
                    uafbb.break_time
                FROM master..custom_models_useraccommodationfile uaf
                -- CANDIDATE USER DATA
                JOIN master..user_management_models_user ummu ON ummu.id = uaf.user_id
                -- FILE STATUS DATA
                JOIN master..custom_models_useraccommodationfilestatus uafs ON uafs.id = uaf.status_id
                JOIN master..custom_models_useraccommodationfilestatustext uafst_en on uafst_en.user_accommodation_file_status_id = uafs.id AND uafst_en.language_id = 1
                JOIN master..custom_models_useraccommodationfilestatustext uafst_fr on uafst_fr.user_accommodation_file_status_id = uafs.id AND uafst_fr.language_id = 2
                -- TEST CENTER
                LEFT JOIN master..custom_models_testcenter tc on tc.id = uaf.test_center_id
                -- ASSIGNED AAE USER DATA
                LEFT JOIN master..user_management_models_user at_u on at_u.id = uaf.assigned_to_user_id
                -- GOC EMAIL OF ASSIGNED AAE (FROM HISTORICAL CUSTOM USER PERMISSIONS)
                OUTER APPLY (SELECT TOP 1 hcup.goc_email FROM master..user_management_models_historicalcustomuserpermissions hcup WHERE hcup.user_id = at_u.id AND hcup.history_type = '+' AND hcup.permission_id = (SELECT cp.permission_id FROM master..user_management_models_custompermissions cp WHERE cp.codename = 'is_aae') ORDER BY hcup.history_date DESC) at_u_email_data
                -- REQUEST COMPLETED DATE (FROM HISTORICAL TABLE)
                OUTER APPLY (SELECT TOP 1 huaf.history_date FROM master..custom_models_historicaluseraccommodationfile huaf WHERE huaf.id = uaf.id AND huaf.status_id = (SELECT uafs_2.id FROM master..custom_models_useraccommodationfilestatus uafs_2 WHERE uafs_2.codename = 'completed') ORDER BY huaf.history_date DESC) latest_completed_file_data
                -- REQUEST ADMINISTERED DATE (FROM HISTORICAL TABLE)
                OUTER APPLY (SELECT TOP 1 huaf.history_date FROM master..custom_models_historicaluseraccommodationfile huaf WHERE huaf.id = uaf.id AND huaf.status_id = (SELECT uafs_2.id FROM master..custom_models_useraccommodationfilestatus uafs_2 WHERE uafs_2.codename = 'administered') ORDER BY huaf.history_date DESC) latest_administered_file_data
                -- OTHER MEASURES DATA
                LEFT JOIN master..custom_models_useraccommodationfileothermeasures uafom on uafom.user_accommodation_file_id = uaf.id
                -- TEST SKILL TYPE / SUB-TYPE DATA (UIT TESTS)
                OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_assignedtest at_1 WHERE at_1.user_accommodation_file_id = uaf.id ORDER BY at_1.id DESC) at
                LEFT JOIN master..cms_models_testskill ts on ts.test_definition_id = at.test_id
                LEFT JOIN master..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM master..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM master..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN master..cms_models_testskillsle tssle on tssle.test_skill_id = ts.id
                LEFT JOIN master..cms_models_testskillsledesc sled on sled.id = tssle.test_skill_sle_desc_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM master..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM master..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                LEFT JOIN master..cms_models_testskilloccupational tsocc on tsocc.test_skill_id = ts.id
                LEFT JOIN master..cms_models_testskilloccupationaldesc occd on occd.id = tsocc.test_skill_occupational_desc_id
                OUTER APPLY (SELECT TOP 1 occdte.text FROM master..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = occd.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM master..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = occd.id AND occdtf.language_id = 2) occ_sub_type_f
                -- DEPARTMENT DATA (UIT TESTS)
                -- ORDERED TESTS
                LEFT JOIN master..custom_models_uitinvites ui on ui.id = at.uit_invite_id
                OUTER APPLY (SELECT TOP 1 * FROM master..cms_models_historicaltestpermissions htp_2 WHERE htp_2.test_order_number = at.test_order_number AND htp_2.history_date < ui.invite_date ORDER BY htp_2.history_date DESC) htp
                LEFT JOIN master..CAT_REF_DEPARTMENTS_VW tp_dept_vw on CONVERT(varchar, tp_dept_vw.DEPT_ID) = CONVERT(varchar, htp.department_ministry_code)
                -- ORDERLESS TESTS
                LEFT JOIN master..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
                LEFT JOIN master..CAT_REF_DEPARTMENTS_VW ofd_dept_vw on CONVERT(varchar, ofd_dept_vw.DEPT_ID) = CONVERT(varchar, ofd.department_ministry_id)
                -- DEPARTMENT DATA (UIT TESTS) - END
                -- TEST SKILL TYPE / SUB-TYPE DATA (SUPERVISED TESTS)
                OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_historicalconsumedreservationcodes hcrc_1 WHERE hcrc_1.user_accommodation_file_id = uaf.id ORDER BY hcrc_1.history_date DESC) hcrc
                LEFT JOIN master..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = hcrc.assessment_process_assigned_test_specs_id
                --  TEST TO ADMINISTER
                LEFT JOIN master..custom_models_useraccommodationfiletesttoadminister uaftta on uaftta.user_accommodation_file_id = uaf.id
                LEFT JOIN master..cms_models_testdefinition td on td.id = uaftta.test_id
                -- BREAK TIME DATA
                LEFT JOIN master..custom_models_useraccommodationfilebreakbank uafbb on uafbb.user_accommodation_file_id = uaf.id
                -- TOTAL DEFAULT TEST TIME
                OUTER APPLY (SELECT SUM(tsec.default_time) as 'sum' FROM master..cms_models_testsection tsec WHERE tsec.test_definition_id = td.id AND tsec.default_time is not NULL) default_times
                -- TOTAL ADJUSTED TIME
                OUTER APPLY (SELECT SUM(uaftt.test_section_time) AS 'sum' FROM master..custom_models_useraccommodationfiletesttime uaftt WHERE uaftt.user_accommodation_file_id = uaf.id) adjusted_times
                LEFT JOIN master..user_management_models_user hr_u on hr_u.id = apatsv.hr_user_id
                LEFT JOIN master..user_management_models_user ta_u on ta_u.id = at.ta_user_id
    
GO
/****** Object:  View [dbo].[test_center_test_administrators_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[test_center_test_administrators_vw] AS
                SELECT 
                    ta.id,
                    ta.test_center_id,
                    u.id as 'user_id',
                    u.email,
                    u.first_name,
                    u.last_name
                FROM master..custom_models_testcentertestadministrators ta
                JOIN master..user_management_models_user u on ta.user_id = u.id
        
GO
/****** Object:  View [dbo].[test_result_vw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[test_result_vw] AS
				SELECT
                    assigned_test_id,
                    candidate_user_id,
                    candidate_username,
                    candidate_email,
                    uit_candidate_email,
                    user_firstname AS 'candidate_first_name',
                    user_lastname AS 'candidate_last_name',
                    candidate_pri,
					candidate_dob,
                    candidate_military_nbr,
                    ta_user_id,
                    ta_username,
                    ta_email,
                    reference_number,
                    test_order_number,
                    process_number,
                    history_id,
                    td_test_code,
                    td_fr_name,
                    td_en_name,
                    submit_date,
                    test_score,
                    CASE
                        WHEN test_is_invalid = 1 THEN 'Invalide'
                        ELSE
                            CASE
                                WHEN test_score IS NULL THEN '-'
                                WHEN test_fr_converted_score IS NULL THEN 'Conversion invalide du résultat'
                            ELSE
                                test_fr_converted_score
                            END
                    END AS 'level_fr',
                    CASE
                        WHEN test_is_invalid = 1 THEN 'Invalid'
                        ELSE
                            CASE
                                WHEN test_score IS NULL THEN '-'
                                WHEN test_en_converted_score IS NULL THEN 'Invalid Score Conversion'
                            ELSE
                                test_en_converted_score
                            END
                    END AS 'level_en',
                    dbo.status2str('fr', test_status_id) AS test_status_fr,
                    dbo.status2str('en', test_status_id) AS test_status_en,
                    test_status_id,
                    test_status_codename,
                    test_id,
                    allowed_ta_user_ids

                FROM
                (
                    SELECT
                        at.id AS 'assigned_test_id',
                        at.test_id,
                        at.ta_user_id AS 'ta_user_id',
                        ta.username AS 'ta_username',
                        ta.email AS 'ta_email',
                        ofd.reference_number AS 'reference_number',
                        at.test_order_number AS 'test_order_number',
                        at.submit_date AS 'submit_date',
                        at.total_score AS 'test_score',
                        at.en_converted_score AS 'test_en_converted_score',
                        at.fr_converted_score AS 'test_fr_converted_score',
                        at.status_id AS 'test_status_id',
                        atst.codename AS 'test_status_codename',
                        at.is_invalid AS 'test_is_invalid',

                        IIF(u.id is not null, u.id, v.id) AS 'candidate_user_id',
                        IIF(u.id is not null, u.username, v.username) AS 'candidate_username',
                        t.candidate_email AS 'uit_candidate_email',
                        IIF(u.id is not null, u.email, v.email) AS 'candidate_email',
                        IIF(u.id is not null, u.first_name, v.first_name) AS 'user_firstname',
                        IIF(u.id is not null, u.last_name, v.last_name) AS 'user_lastname',
                        IIF(u.id is not null, u.pri, v.pri) AS 'candidate_pri',
						IIF(u.id is not null, u.birth_date, v.birth_date) AS 'candidate_dob',
                        IIF(u.id is not null, u.military_nbr, v.military_nbr) AS 'candidate_military_nbr',

                        p.staffing_process_number AS 'process_number',
                        p.history_id AS 'history_id',

                        td.test_code AS 'td_test_code',
                        td.fr_name AS 'td_fr_name',
                        td.en_name AS 'td_en_name',

                        htp3.ta_user_ids as 'allowed_ta_user_ids'

                    FROM master..custom_models_assignedtest at
                    JOIN master..custom_models_assignedteststatus atst on atst.id = at.status_id
                    OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_historicaluser u1 WHERE u1.id = at.user_id AND u1.history_date < at.submit_date ORDER BY u1.history_date DESC) u
                    OUTER APPLY (SELECT TOP 1 * FROM master..user_management_models_user u2 WHERE u2.id = at.ta_user_id) ta
                    JOIN master..user_management_models_user v on v.id = at.user_id
                    OUTER APPLY (SELECT TOP 1 * FROM master..cms_models_historicaltestpermissions p1 WHERE p1.test_order_number = at.test_order_number AND p1.user_id = at.ta_user_id AND p1.history_date < at.submit_date ORDER BY p1.history_date DESC) p
                    JOIN master..cms_models_testdefinition td ON td.id = at.test_id
                    LEFT JOIN master..custom_models_orderlessfinancialdata ofd on at.orderless_financial_data_id = ofd.id
                    OUTER APPLY (SELECT TOP 1 * FROM master..custom_models_historicalunsupervisedtestaccesscode t1 WHERE t1.test_access_code = at.test_access_code AND t1.uit_invite_id = at.uit_invite_id ORDER BY t1.history_date DESC) t
                    OUTER APPLY (SELECT string_agg(ta_username_list.user_id, ',') within group (ORDER BY ta_username_list.user_id) as 'ta_user_ids' FROM (SELECT DISTINCT user_id FROM master..cms_models_historicaltestpermissions htp WHERE htp.history_type = '+' AND htp.test_order_number = at.test_order_number) ta_username_list) htp3
                ) r
        
GO
/****** Object:  Table [dbo].[auth_group]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_group](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](150) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [auth_group_name_a6ea08ec_uniq] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_group_permissions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_group_permissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[group_id] [int] NOT NULL,
	[permission_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_permission]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_permission](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[content_type_id] [int] NOT NULL,
	[codename] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[authtoken_token]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[authtoken_token](
	[key] [nvarchar](40) NOT NULL,
	[created] [datetime2](7) NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_ABORIGINAL_VW]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_ABORIGINAL_VW](
	[ABRG_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[ABRG_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_CLASSIFICATION_VW]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_CLASSIFICATION_VW](
	[CLASSIF_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[CLASS_GRP_CD] [nvarchar](2) NOT NULL,
	[CLASS_SBGRP_CD] [nvarchar](3) NULL,
	[CLASS_LVL_CD] [nvarchar](2) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[PSC_CLASS_IND] [int] NOT NULL,
	[DFLT_BUD_CD] [nvarchar](5) NULL,
	[EQLZN_AMT] [int] NULL,
	[EQLZN_AMT_EFDT] [datetime2](7) NULL,
	[EQLZN_AMT_XDT] [datetime2](7) NULL,
	[OFCR_LVL_IND] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CLASSIF_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW](
	[AREA_RES_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetimeoffset](7) NOT NULL,
	[XDT] [datetimeoffset](7) NULL,
	[PRESENTATION_ORDER] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AREA_RES_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_DISABILITY_VW]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_DISABILITY_VW](
	[DSBL_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[FULL_EDESC] [nvarchar](200) NULL,
	[FULL_FDESC] [nvarchar](200) NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[DSBL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_EMPLOYER_STS_VW]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_EMPLOYER_STS_VW](
	[EMPSTS_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[EMPSTS_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_GENDER_VW]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_GENDER_VW](
	[GND_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[GND_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_LANGUAGE_VW]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_LANGUAGE_VW](
	[language_id] [int] NOT NULL,
	[iso_code_1] [nvarchar](50) NOT NULL,
	[iso_code_2] [nvarchar](50) NOT NULL,
	[edesc] [nvarchar](50) NOT NULL,
	[fdesc] [nvarchar](50) NOT NULL,
	[date_from] [datetimeoffset](7) NOT NULL,
	[date_to] [datetimeoffset](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[language_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_PROVINCE_VW]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_PROVINCE_VW](
	[PROV_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[PROV_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAT_REF_VISIBLE_MINORITY_VW]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAT_REF_VISIBLE_MINORITY_VW](
	[VISMIN_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[VISMIN_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_addressbookcontact]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_addressbookcontact](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](30) NULL,
	[parent_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_addressbookcontact_test_section]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_addressbookcontact_test_section](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[addressbookcontact_id] [int] NOT NULL,
	[testsection_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_addressbookcontactdetails]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_addressbookcontactdetails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](150) NOT NULL,
	[contact_id] [int] NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_answer]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_answer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[scoring_value] [int] NOT NULL,
	[question_id] [int] NOT NULL,
	[order] [int] NOT NULL,
	[ppc_answer_id] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_answerdetails]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_answerdetails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[answer_id] [int] NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_bundleitemsbasicrule]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_bundleitemsbasicrule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[number_of_items] [int] NOT NULL,
	[bundle_rule_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_competencytype]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_competencytype](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fr_name] [nvarchar](100) NOT NULL,
	[en_name] [nvarchar](100) NOT NULL,
	[max_score] [int] NOT NULL,
	[test_definition_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_contenttype]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_contenttype](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_contenttypetext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_contenttypetext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](30) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[content_type_id] [int] NOT NULL,
	[language_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_emailquestion]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_emailquestion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email_id] [int] NOT NULL,
	[from_field_id] [int] NOT NULL,
	[question_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_emailquestion_cc_field]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_emailquestion_cc_field](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[emailquestion_id] [int] NOT NULL,
	[addressbookcontact_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_emailquestion_competency_types]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_emailquestion_competency_types](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[emailquestion_id] [int] NOT NULL,
	[competencytype_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_emailquestion_to_field]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_emailquestion_to_field](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[emailquestion_id] [int] NOT NULL,
	[addressbookcontact_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_emailquestiondetails]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_emailquestiondetails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[subject_field] [nvarchar](max) NOT NULL,
	[date_field] [nvarchar](max) NOT NULL,
	[body] [nvarchar](max) NOT NULL,
	[email_question_id] [int] NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_fileresourcedetails]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_fileresourcedetails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[path] [nvarchar](200) NOT NULL,
	[test_definition_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaladdressbookcontact]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaladdressbookcontact](
	[id] [int] NOT NULL,
	[name] [nvarchar](30) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[parent_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaladdressbookcontactdetails]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaladdressbookcontactdetails](
	[id] [int] NOT NULL,
	[title] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[contact_id] [int] NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalanswer]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalanswer](
	[id] [int] NOT NULL,
	[scoring_value] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
	[order] [int] NOT NULL,
	[ppc_answer_id] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalanswerdetails]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalanswerdetails](
	[id] [int] NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[answer_id] [int] NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundlebundlesassociation]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundlebundlesassociation](
	[id] [int] NOT NULL,
	[display_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[bundle_link_id] [int] NULL,
	[bundle_source_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundlebundlesrule]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundlebundlesrule](
	[id] [int] NOT NULL,
	[number_of_bundles] [int] NULL,
	[shuffle_bundles] [bit] NOT NULL,
	[keep_items_together] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[bundle_rule_id] [int] NULL,
	[history_user_id] [int] NULL,
	[display_order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundlebundlesruleassociations]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundlebundlesruleassociations](
	[id] [int] NOT NULL,
	[display_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[bundle_id] [int] NULL,
	[history_user_id] [int] NULL,
	[bundle_bundles_rule_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundleitemsassociation]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundleitemsassociation](
	[id] [int] NOT NULL,
	[system_id_link] [nvarchar](15) NOT NULL,
	[version] [int] NULL,
	[display_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[bundle_source_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundleitemsbasicrule]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundleitemsbasicrule](
	[id] [int] NOT NULL,
	[number_of_items] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[bundle_rule_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundlerule]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundlerule](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[bundle_id] [int] NULL,
	[history_user_id] [int] NULL,
	[rule_type_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundleruletype]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundleruletype](
	[id] [int] NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalbundles]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalbundles](
	[id] [int] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[version_text] [nvarchar](50) NULL,
	[shuffle_items] [bit] NOT NULL,
	[shuffle_bundles] [bit] NOT NULL,
	[active] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_id] [int] NULL,
	[shuffle_between_bundles] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalcompetencytype]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalcompetencytype](
	[id] [int] NOT NULL,
	[fr_name] [nvarchar](100) NOT NULL,
	[en_name] [nvarchar](100) NOT NULL,
	[max_score] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_definition_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalcontenttype]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalcontenttype](
	[id] [int] NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalcontenttypetext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalcontenttypetext](
	[id] [int] NOT NULL,
	[text] [nvarchar](30) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[content_type_id] [int] NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalemailquestion]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalemailquestion](
	[id] [int] NOT NULL,
	[email_id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[from_field_id] [int] NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalemailquestiondetails]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalemailquestiondetails](
	[id] [int] NOT NULL,
	[subject_field] [nvarchar](max) NOT NULL,
	[date_field] [nvarchar](max) NOT NULL,
	[body] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[email_question_id] [int] NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalfileresourcedetails]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalfileresourcedetails](
	[id] [int] NOT NULL,
	[path] [nvarchar](200) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_definition_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemattributevalue]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemattributevalue](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_id] [int] NULL,
	[item_bank_attribute_value_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemattributevaluedrafts]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemattributevaluedrafts](
	[id] [int] NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_id] [int] NULL,
	[item_bank_attribute_value_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembank]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembank](
	[id] [int] NOT NULL,
	[custom_item_bank_id] [nvarchar](25) NOT NULL,
	[en_name] [nvarchar](150) NOT NULL,
	[fr_name] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[archived] [bit] NOT NULL,
	[department_id] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[comments] [nvarchar](200) NOT NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembankaccesstypes]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembankaccesstypes](
	[id] [int] NOT NULL,
	[en_description] [nvarchar](150) NOT NULL,
	[fr_description] [nvarchar](150) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[priority] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembankattributes]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembankattributes](
	[id] [int] NOT NULL,
	[attribute_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembankattributestext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembankattributestext](
	[id] [int] NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_attribute_id] [int] NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembankattributevalues]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembankattributevalues](
	[id] [int] NOT NULL,
	[attribute_value_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_attribute_id] [int] NULL,
	[attribute_value_type] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembankattributevaluestext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembankattributevaluestext](
	[id] [int] NOT NULL,
	[text] [nvarchar](max) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_attribute_values_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembankpermissions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembankpermissions](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_id] [int] NULL,
	[item_bank_access_type_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitembankrule]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitembankrule](
	[id] [int] NOT NULL,
	[order] [int] NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_id] [int] NULL,
	[item_bank_bundle_id] [int] NULL,
	[test_section_component_id] [int] NULL,
	[content_type_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemcomments]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemcomments](
	[id] [int] NOT NULL,
	[comment] [nvarchar](255) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[version] [int] NOT NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemcontent]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemcontent](
	[id] [int] NOT NULL,
	[content_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[content_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemcontentdrafts]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemcontentdrafts](
	[id] [int] NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[content_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[content_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemcontenttext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemcontenttext](
	[id] [int] NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_content_id] [int] NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemcontenttextdrafts]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemcontenttextdrafts](
	[id] [int] NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_content_id] [int] NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemdevelopmentstatuses]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemdevelopmentstatuses](
	[id] [int] NOT NULL,
	[en_name] [nvarchar](25) NOT NULL,
	[fr_name] [nvarchar](25) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemdrafts]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemdrafts](
	[id] [int] NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[historical_id] [nvarchar](50) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[development_status_id] [int] NULL,
	[history_user_id] [int] NULL,
	[item_id] [int] NULL,
	[item_bank_id] [int] NULL,
	[response_format_id] [int] NULL,
	[active_status] [bit] NOT NULL,
	[shuffle_options] [bit] NOT NULL,
	[item_type_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemhistoricalids]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemhistoricalids](
	[id] [int] NOT NULL,
	[historical_id] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_bank_id] [int] NULL,
	[system_id] [nvarchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemoption]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemoption](
	[id] [int] NOT NULL,
	[option_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[score] [nvarchar](25) NULL,
	[option_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_id] [int] NULL,
	[exclude_from_shuffle] [bit] NOT NULL,
	[historical_option_id] [nvarchar](50) NULL,
	[rationale] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemoptiondrafts]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemoptiondrafts](
	[id] [int] NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[option_type] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[score] [nvarchar](25) NULL,
	[option_order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_id] [int] NULL,
	[exclude_from_shuffle] [bit] NOT NULL,
	[historical_option_id] [nvarchar](50) NULL,
	[rationale] [nvarchar](max) NOT NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemoptiontext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemoptiontext](
	[id] [int] NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_option_id] [int] NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemoptiontextdrafts]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemoptiontextdrafts](
	[id] [int] NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_option_id] [int] NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemresponseformats]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemresponseformats](
	[id] [int] NOT NULL,
	[en_name] [nvarchar](25) NOT NULL,
	[fr_name] [nvarchar](25) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitems]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitems](
	[id] [int] NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[version] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[active_status] [bit] NOT NULL,
	[development_status_id] [int] NULL,
	[history_user_id] [int] NULL,
	[response_format_id] [int] NULL,
	[item_bank_id] [int] NULL,
	[shuffle_options] [bit] NOT NULL,
	[item_type_id] [int] NULL,
	[last_modified_by_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemtype]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemtype](
	[id] [int] NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalitemtypetext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalitemtypetext](
	[id] [int] NOT NULL,
	[text] [nvarchar](30) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[item_type_id] [int] NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalmultiplechoicequestion]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalmultiplechoicequestion](
	[id] [int] NOT NULL,
	[question_difficulty_type] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalnewquestion]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalnewquestion](
	[id] [int] NOT NULL,
	[question_type] [int] NOT NULL,
	[pilot] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[question_block_type_id] [int] NULL,
	[test_section_component_id] [int] NULL,
	[order] [int] NOT NULL,
	[dependent_order] [int] NOT NULL,
	[shuffle_answer_choices] [bit] NOT NULL,
	[ppc_question_id] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalnextsectionbuttontypepopup]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalnextsectionbuttontypepopup](
	[id] [int] NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[title] [nvarchar](max) NOT NULL,
	[button_text] [nvarchar](50) NOT NULL,
	[confirm_proceed] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[test_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalnextsectionbuttontypeproceed]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalnextsectionbuttontypeproceed](
	[id] [int] NOT NULL,
	[button_text] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[test_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalorderlesstestpermissions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalorderlesstestpermissions](
	[id] [int] NOT NULL,
	[parent_code] [nvarchar](25) NOT NULL,
	[test_code] [nvarchar](25) NOT NULL,
	[date_assigned] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[ta_extended_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesection]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesection](
	[id] [int] NOT NULL,
	[order] [int] NOT NULL,
	[page_section_type] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[section_component_page_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesectiontypeimagezoom]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesectiontypeimagezoom](
	[id] [int] NOT NULL,
	[large_image] [nvarchar](250) NOT NULL,
	[small_image] [nvarchar](250) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[page_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesectiontypeitembankinstruction]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesectiontypeitembankinstruction](
	[id] [int] NOT NULL,
	[content_type_id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[page_section_id] [int] NULL,
	[system_id] [nvarchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesectiontypemarkdown]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesectiontypemarkdown](
	[id] [int] NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[page_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesectiontypesampleemail]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesectiontypesampleemail](
	[id] [int] NOT NULL,
	[email_id] [int] NOT NULL,
	[subject_field] [nvarchar](max) NOT NULL,
	[from_field] [nvarchar](max) NOT NULL,
	[to_field] [nvarchar](max) NOT NULL,
	[date_field] [nvarchar](max) NOT NULL,
	[body] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[page_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesectiontypesampleemailresponse]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesectiontypesampleemailresponse](
	[id] [int] NOT NULL,
	[to_field] [nvarchar](max) NOT NULL,
	[cc_field] [nvarchar](max) NULL,
	[response] [nvarchar](max) NULL,
	[reason] [nvarchar](max) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[page_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesectiontypesampletaskresponse]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesectiontypesampletaskresponse](
	[id] [int] NOT NULL,
	[response] [nvarchar](max) NULL,
	[reason] [nvarchar](max) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[page_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesectiontypesurveylink]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesectiontypesurveylink](
	[id] [int] NOT NULL,
	[link_value] [nvarchar](300) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[page_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesectiontypesurveylinktext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesectiontypesurveylinktext](
	[id] [int] NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[page_section_type_survey_link_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalpagesectiontypetreedescription]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalpagesectiontypetreedescription](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[address_book_contact_id] [int] NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[page_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalquestionblocktype]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalquestionblocktype](
	[id] [int] NOT NULL,
	[name] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_definition_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalquestionlistrule]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalquestionlistrule](
	[id] [int] NOT NULL,
	[number_of_questions] [int] NOT NULL,
	[order] [int] NULL,
	[shuffle] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_section_component_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalquestionsection]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalquestionsection](
	[id] [int] NOT NULL,
	[order] [int] NOT NULL,
	[question_section_type] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalquestionsectiontypemarkdown]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalquestionsectiontypemarkdown](
	[id] [int] NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[question_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalquestionsituation]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalquestionsituation](
	[id] [int] NOT NULL,
	[situation] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[question_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalreducercontrol]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalreducercontrol](
	[reducer] [nvarchar](20) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalscoringmethods]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalscoringmethods](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[method_type_id] [int] NULL,
	[test_id] [int] NULL,
	[validity_period] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalscoringmethodtypes]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalscoringmethodtypes](
	[id] [int] NOT NULL,
	[method_type_codename] [nvarchar](50) NOT NULL,
	[en_name] [nvarchar](max) NOT NULL,
	[fr_name] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalscoringpassfail]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalscoringpassfail](
	[id] [int] NOT NULL,
	[minimum_score] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[scoring_method_id] [int] NULL,
	[result_valid_indefinitely] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalscoringraw]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalscoringraw](
	[id] [int] NOT NULL,
	[result_valid_indefinitely] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[scoring_method_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalscoringthreshold]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalscoringthreshold](
	[id] [int] NOT NULL,
	[minimum_score] [int] NOT NULL,
	[maximum_score] [int] NOT NULL,
	[en_conversion_value] [nvarchar](max) NOT NULL,
	[fr_conversion_value] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[scoring_method_id] [int] NULL,
	[result_valid_indefinitely] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalsectioncomponentpage]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalsectioncomponentpage](
	[id] [int] NOT NULL,
	[order] [int] NOT NULL,
	[en_title] [nvarchar](150) NOT NULL,
	[fr_title] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_section_component_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalsituationexamplerating]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalsituationexamplerating](
	[id] [int] NOT NULL,
	[score] [float] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[competency_type_id] [int] NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalsituationexampleratingdetails]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalsituationexampleratingdetails](
	[id] [int] NOT NULL,
	[example] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[example_rating_id] [int] NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestdefinition]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestdefinition](
	[id] [int] NOT NULL,
	[parent_code] [nvarchar](25) NOT NULL,
	[test_code] [nvarchar](25) NOT NULL,
	[version] [int] NOT NULL,
	[en_name] [nvarchar](150) NOT NULL,
	[fr_name] [nvarchar](150) NOT NULL,
	[is_public] [bit] NOT NULL,
	[active] [bit] NOT NULL,
	[retest_period] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[archived] [bit] NOT NULL,
	[version_notes] [nvarchar](500) NOT NULL,
	[count_up] [bit] NOT NULL,
	[is_uit] [bit] NOT NULL,
	[show_result] [bit] NOT NULL,
	[show_score] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestsection]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestsection](
	[id] [int] NOT NULL,
	[order] [int] NOT NULL,
	[section_type] [int] NOT NULL,
	[default_time] [int] NULL,
	[en_title] [nvarchar](150) NOT NULL,
	[fr_title] [nvarchar](150) NOT NULL,
	[next_section_button_type] [int] NOT NULL,
	[scoring_type] [int] NOT NULL,
	[minimum_score] [int] NULL,
	[uses_notepad] [bit] NULL,
	[block_cheating] [bit] NULL,
	[default_tab] [int] NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_definition_id] [int] NULL,
	[uses_calculator] [bit] NOT NULL,
	[item_exposure] [bit] NOT NULL,
	[needs_approval] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestsectioncomponent]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestsectioncomponent](
	[id] [int] NOT NULL,
	[order] [int] NOT NULL,
	[component_type] [int] NOT NULL,
	[en_title] [nvarchar](150) NOT NULL,
	[fr_title] [nvarchar](150) NOT NULL,
	[shuffle_all_questions] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [nvarchar](2) NULL,
	[shuffle_question_blocks] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestsectionreducer]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestsectionreducer](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[reducer_id] [nvarchar](20) NULL,
	[test_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestskill]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestskill](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_definition_id] [int] NULL,
	[test_skill_type_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestskilloccupational]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestskilloccupational](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_skill_id] [int] NULL,
	[test_skill_occupational_desc_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestskilloccupationaldesc]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestskilloccupationaldesc](
	[id] [int] NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestskilloccupationaldesctext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestskilloccupationaldesctext](
	[id] [int] NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[test_skill_occupational_desc_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestskillsle]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestskillsle](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_skill_id] [int] NULL,
	[test_skill_sle_desc_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestskillsledesc]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestskillsledesc](
	[id] [int] NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestskillsledesctext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestskillsledesctext](
	[id] [int] NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[test_skill_sle_desc_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestskilltype]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestskilltype](
	[id] [int] NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicaltestskilltypetext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicaltestskilltypetext](
	[id] [int] NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[test_skill_type_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_historicalviewedquestions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_historicalviewedquestions](
	[id] [int] NOT NULL,
	[ppc_question_id] [nvarchar](25) NOT NULL,
	[count] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemattributevalue]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemattributevalue](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[item_id] [int] NOT NULL,
	[item_bank_attribute_value_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemattributevaluedrafts]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemattributevaluedrafts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[item_id] [int] NOT NULL,
	[item_bank_attribute_value_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itembank]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembank](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[custom_item_bank_id] [nvarchar](25) NOT NULL,
	[en_name] [nvarchar](150) NOT NULL,
	[fr_name] [nvarchar](150) NOT NULL,
	[archived] [bit] NOT NULL,
	[department_id] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[comments] [nvarchar](200) NOT NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_custom_item_bank_id] UNIQUE NONCLUSTERED 
(
	[custom_item_bank_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itembankaccesstypes]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembankaccesstypes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[en_description] [nvarchar](150) NOT NULL,
	[fr_description] [nvarchar](150) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[priority] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_item_bank_access_type_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itembankattributes]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembankattributes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attribute_order] [int] NOT NULL,
	[item_bank_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itembankpermissions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembankpermissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[item_bank_id] [int] NOT NULL,
	[item_bank_access_type_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itembankrule]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itembankrule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order] [int] NULL,
	[item_bank_id] [int] NOT NULL,
	[item_bank_bundle_id] [int] NOT NULL,
	[test_section_component_id] [int] NOT NULL,
	[content_type_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemcomments]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemcomments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[comment] [nvarchar](255) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
	[version] [int] NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_itemtypetext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_itemtypetext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](30) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[item_type_id] [int] NOT NULL,
	[language_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_multiplechoicequestion]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_multiplechoicequestion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[question_difficulty_type] [int] NOT NULL,
	[question_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_newquestion]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_newquestion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[question_type] [int] NOT NULL,
	[pilot] [bit] NOT NULL,
	[question_block_type_id] [int] NULL,
	[test_section_component_id] [int] NOT NULL,
	[order] [int] NOT NULL,
	[dependent_order] [int] NOT NULL,
	[shuffle_answer_choices] [bit] NOT NULL,
	[ppc_question_id] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_newquestion_dependencies]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_newquestion_dependencies](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[from_newquestion_id] [int] NOT NULL,
	[to_newquestion_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_nextsectionbuttontypepopup]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_nextsectionbuttontypepopup](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[title] [nvarchar](max) NOT NULL,
	[button_text] [nvarchar](50) NOT NULL,
	[confirm_proceed] [bit] NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[test_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_nextsectionbuttontypeproceed]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_nextsectionbuttontypeproceed](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[button_text] [nvarchar](max) NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[test_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_orderlesstestpermissions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_orderlesstestpermissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[parent_code] [nvarchar](25) NOT NULL,
	[test_code] [nvarchar](25) NOT NULL,
	[date_assigned] [datetime2](7) NOT NULL,
	[ta_extended_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesection]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesection](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order] [int] NOT NULL,
	[page_section_type] [int] NOT NULL,
	[section_component_page_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesectiontypeimagezoom]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesectiontypeimagezoom](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[large_image] [nvarchar](250) NOT NULL,
	[small_image] [nvarchar](250) NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[page_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesectiontypeitembankinstruction]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesectiontypeitembankinstruction](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content_type_id] [int] NOT NULL,
	[page_section_id] [int] NOT NULL,
	[system_id] [nvarchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [no duplicate page section entries] UNIQUE NONCLUSTERED 
(
	[system_id] ASC,
	[page_section_id] ASC,
	[content_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesectiontypemarkdown]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesectiontypemarkdown](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[page_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesectiontypesampleemail]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesectiontypesampleemail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email_id] [int] NOT NULL,
	[subject_field] [nvarchar](max) NOT NULL,
	[from_field] [nvarchar](max) NOT NULL,
	[to_field] [nvarchar](max) NOT NULL,
	[date_field] [nvarchar](max) NOT NULL,
	[body] [nvarchar](max) NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[page_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesectiontypesampleemailresponse]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesectiontypesampleemailresponse](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[to_field] [nvarchar](max) NOT NULL,
	[cc_field] [nvarchar](max) NULL,
	[response] [nvarchar](max) NULL,
	[reason] [nvarchar](max) NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[page_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesectiontypesampletaskresponse]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesectiontypesampletaskresponse](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[response] [nvarchar](max) NULL,
	[reason] [nvarchar](max) NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[page_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesectiontypesurveylink]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesectiontypesurveylink](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[link_value] [nvarchar](300) NOT NULL,
	[page_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesectiontypesurveylinktext]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesectiontypesurveylinktext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[language_id] [int] NOT NULL,
	[page_section_type_survey_link_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_pagesectiontypetreedescription]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_pagesectiontypetreedescription](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[address_book_contact_id] [int] NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[page_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_questionblocktype]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_questionblocktype](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](150) NOT NULL,
	[test_definition_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_questionlistrule]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_questionlistrule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[number_of_questions] [int] NOT NULL,
	[order] [int] NULL,
	[shuffle] [bit] NOT NULL,
	[test_section_component_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_questionlistrule_question_block_type]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_questionlistrule_question_block_type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[questionlistrule_id] [int] NOT NULL,
	[questionblocktype_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_questionsection]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_questionsection](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order] [int] NOT NULL,
	[question_section_type] [int] NOT NULL,
	[question_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_questionsectiontypemarkdown]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_questionsectiontypemarkdown](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[question_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_questionsituation]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_questionsituation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[situation] [nvarchar](max) NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
	[question_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_reducercontrol]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_reducercontrol](
	[reducer] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[reducer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_sectioncomponentpage]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_sectioncomponentpage](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order] [int] NOT NULL,
	[en_title] [nvarchar](150) NOT NULL,
	[fr_title] [nvarchar](150) NOT NULL,
	[test_section_component_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_situationexamplerating]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_situationexamplerating](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[score] [float] NOT NULL,
	[question_id] [int] NOT NULL,
	[competency_type_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_situationexampleratingdetails]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_situationexampleratingdetails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[example] [nvarchar](max) NOT NULL,
	[example_rating_id] [int] NOT NULL,
	[language_id] [nvarchar](2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testsectioncomponent]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testsectioncomponent](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order] [int] NOT NULL,
	[component_type] [int] NOT NULL,
	[en_title] [nvarchar](150) NOT NULL,
	[fr_title] [nvarchar](150) NOT NULL,
	[shuffle_all_questions] [bit] NOT NULL,
	[language_id] [nvarchar](2) NULL,
	[shuffle_question_blocks] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testsectioncomponent_test_section]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testsectioncomponent_test_section](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[testsectioncomponent_id] [int] NOT NULL,
	[testsection_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_testsectionreducer]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_testsectionreducer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[reducer_id] [nvarchar](20) NOT NULL,
	[test_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cms_models_viewedquestions]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cms_models_viewedquestions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ppc_question_id] [nvarchar](25) NOT NULL,
	[count] [int] NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_additionaltime]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_additionaltime](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_section_time] [int] NOT NULL,
	[accommodation_request_id] [int] NOT NULL,
	[test_section_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_assignedanswerchoices]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assignedanswerchoices](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[answer_choices] [nvarchar](max) NOT NULL,
	[assigned_test_id] [int] NOT NULL,
	[question_id] [int] NULL,
	[item_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_assignedquestionslist]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assignedquestionslist](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[questions_list] [nvarchar](max) NOT NULL,
	[assigned_test_id] [int] NOT NULL,
	[test_section_id] [int] NOT NULL,
	[from_item_bank] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_assignedtestanswerscore]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_assignedtestanswerscore](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rationale] [nvarchar](max) NOT NULL,
	[score] [float] NOT NULL,
	[question_id] [int] NOT NULL,
	[scorer_assigned_test_id] [int] NOT NULL,
	[competency_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_candidateanswers]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_candidateanswers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[assigned_test_id] [int] NOT NULL,
	[question_id] [int] NULL,
	[test_section_component_id] [int] NOT NULL,
	[mark_for_review] [bit] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[selected_language_id] [int] NULL,
	[item_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_candidateemailresponseanswers]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_candidateemailresponseanswers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[answer_id] [int] NOT NULL,
	[email_type] [nvarchar](25) NOT NULL,
	[email_body] [nvarchar](max) NULL,
	[reasons_for_action] [nvarchar](max) NULL,
	[candidate_answers_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_candidateemailresponseanswers_email_cc]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_candidateemailresponseanswers_email_cc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[candidateemailresponseanswers_id] [int] NOT NULL,
	[addressbookcontact_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_candidateemailresponseanswers_email_to]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_candidateemailresponseanswers_email_to](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[candidateemailresponseanswers_id] [int] NOT NULL,
	[addressbookcontact_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_candidatemultiplechoiceanswers]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_candidatemultiplechoiceanswers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[answer_id] [int] NULL,
	[candidate_answers_id] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[item_answer_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_candidatetaskresponseanswers]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_candidatetaskresponseanswers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[answer_id] [int] NOT NULL,
	[task] [nvarchar](max) NULL,
	[reasons_for_action] [nvarchar](max) NULL,
	[candidate_answers_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_criticality]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_criticality](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description_en] [nvarchar](150) NOT NULL,
	[description_fr] [nvarchar](150) NOT NULL,
	[active] [bit] NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[priority] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_criticality_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_databasecheckmodel]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_databasecheckmodel](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_ettaactiontypes]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_ettaactiontypes](
	[action_type] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[action_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_gcnotifyemailfailedbulkdelivery]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_gcnotifyemailfailedbulkdelivery](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sent_date] [datetimeoffset](7) NOT NULL,
	[email_to] [nvarchar](254) NOT NULL,
	[email_contact_if_fail] [nvarchar](254) NULL,
	[first_name_to] [nvarchar](30) NOT NULL,
	[last_name_to] [nvarchar](150) NOT NULL,
	[status_date] [datetimeoffset](7) NOT NULL,
	[status] [nvarchar](30) NOT NULL,
	[assessment_process_id] [int] NULL,
	[gc_notify_template_id] [int] NOT NULL,
	[uit_invite_id] [int] NULL,
	[email_body] [nvarchar](max) NULL,
	[email_subject] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_gcnotifyemailpendingdelivery]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_gcnotifyemailpendingdelivery](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[gc_notify_email_id] [nvarchar](36) NOT NULL,
	[sent_date] [datetimeoffset](7) NOT NULL,
	[email_to] [nvarchar](254) NOT NULL,
	[email_contact_if_fail] [nvarchar](254) NULL,
	[first_name_to] [nvarchar](30) NOT NULL,
	[last_name_to] [nvarchar](150) NOT NULL,
	[last_checked_date] [datetimeoffset](7) NOT NULL,
	[assessment_process_id] [int] NULL,
	[gc_notify_template_id] [int] NOT NULL,
	[uit_invite_id] [int] NULL,
	[email_body] [nvarchar](max) NULL,
	[email_subject] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_gcnotifyemailverifiedattempteddelivery]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_gcnotifyemailverifiedattempteddelivery](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sent_date] [datetimeoffset](7) NOT NULL,
	[email_to] [nvarchar](254) NOT NULL,
	[email_contact_if_fail] [nvarchar](254) NULL,
	[first_name_to] [nvarchar](30) NOT NULL,
	[last_name_to] [nvarchar](150) NOT NULL,
	[status_date] [datetimeoffset](7) NOT NULL,
	[status] [nvarchar](30) NOT NULL,
	[is_delivered] [bit] NOT NULL,
	[assessment_process_id] [int] NULL,
	[gc_notify_template_id] [int] NOT NULL,
	[uit_invite_id] [int] NULL,
	[email_body] [nvarchar](max) NULL,
	[email_subject] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_gcnotifytemplate]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_gcnotifytemplate](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[folder] [nvarchar](100) NULL,
	[codename] [nvarchar](100) NOT NULL,
	[template_id] [nvarchar](36) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalaccommodationrequest]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalaccommodationrequest](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[break_bank_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaladditionaltime]    Script Date: 2025-03-05 11:23:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaladditionaltime](
	[id] [int] NOT NULL,
	[test_section_time] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[accommodation_request_id] [int] NULL,
	[history_user_id] [int] NULL,
	[test_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassessmentprocess]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassessmentprocess](
	[id] [int] NOT NULL,
	[reference_number] [nvarchar](30) NOT NULL,
	[department_id] [int] NOT NULL,
	[closing_date] [date] NULL,
	[allow_booking_external_tc] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[default_billing_contact_id] [int] NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
	[request_sent] [bit] NOT NULL,
	[contact_email_for_candidates] [nvarchar](254) NOT NULL,
	[duration] [int] NOT NULL,
	[allow_last_minute_cancellation_tc] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassessmentprocessassignedtestspecs]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassessmentprocessassignedtestspecs](
	[id] [int] NOT NULL,
	[test_skill_sub_type_id] [int] NULL,
	[level_required] [nvarchar](30) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assessment_process_id] [int] NULL,
	[billing_contact_id] [int] NULL,
	[history_user_id] [int] NULL,
	[test_skill_type_id] [int] NULL,
	[email] [nvarchar](254) NOT NULL,
	[first_name] [nvarchar](30) NOT NULL,
	[last_name] [nvarchar](150) NOT NULL,
	[reason_for_testing_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassessmentprocessdefaulttestspecs]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassessmentprocessdefaulttestspecs](
	[id] [int] NOT NULL,
	[test_skill_sub_type_id] [int] NULL,
	[level_required] [nvarchar](30) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assessment_process_id] [int] NULL,
	[history_user_id] [int] NULL,
	[test_skill_type_id] [int] NULL,
	[reason_for_testing_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassignedanswerchoices]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassignedanswerchoices](
	[id] [int] NOT NULL,
	[answer_choices] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
	[item_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassignedquestionslist]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassignedquestionslist](
	[id] [int] NOT NULL,
	[questions_list] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
	[test_section_id] [int] NULL,
	[from_item_bank] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassignedtest]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassignedtest](
	[id] [int] NOT NULL,
	[start_date] [datetime2](7) NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[submit_date] [datetime2](7) NULL,
	[test_access_code] [nvarchar](13) NULL,
	[test_order_number] [nvarchar](12) NULL,
	[total_score] [int] NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_id] [int] NULL,
	[test_section_id] [int] NULL,
	[test_session_language_id] [int] NULL,
	[en_converted_score] [nvarchar](50) NULL,
	[fr_converted_score] [nvarchar](50) NULL,
	[is_invalid] [bit] NOT NULL,
	[uit_invite_id] [int] NULL,
	[orderless_financial_data_id] [int] NULL,
	[accommodation_request_id] [int] NULL,
	[test_session_id] [int] NULL,
	[previous_status_id] [int] NULL,
	[status_id] [int] NULL,
	[ta_user_id] [int] NULL,
	[user_id] [int] NULL,
	[score_valid_until] [date] NULL,
	[user_accommodation_file_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassignedtestanswerscore]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassignedtestanswerscore](
	[id] [int] NOT NULL,
	[rationale] [nvarchar](max) NOT NULL,
	[score] [float] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[competency_id] [int] NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
	[scorer_assigned_test_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassignedtestsection]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassignedtestsection](
	[id] [int] NOT NULL,
	[test_section_time] [int] NULL,
	[score] [int] NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
	[test_section_id] [int] NULL,
	[timed_out] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassignedtestsectionaccesstimes]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassignedtestsectionaccesstimes](
	[id] [int] NOT NULL,
	[time_type] [int] NOT NULL,
	[time] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assigned_test_section_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalassignedteststatus]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalassignedteststatus](
	[id] [int] NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalbillingcontact]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalbillingcontact](
	[id] [int] NOT NULL,
	[first_name] [nvarchar](30) NOT NULL,
	[last_name] [nvarchar](150) NOT NULL,
	[email] [nvarchar](254) NOT NULL,
	[department_id] [int] NOT NULL,
	[fis_organisation_code] [nvarchar](16) NOT NULL,
	[fis_reference_code] [nvarchar](20) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalbreakbank]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalbreakbank](
	[id] [int] NOT NULL,
	[break_time] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalbreakbankactions]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalbreakbankactions](
	[id] [int] NOT NULL,
	[action_type] [nvarchar](15) NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[new_remaining_time] [int] NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[break_bank_id] [int] NULL,
	[history_user_id] [int] NULL,
	[test_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalcandidateanswers]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalcandidateanswers](
	[id] [int] NOT NULL,
	[mark_for_review] [bit] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
	[question_id] [int] NULL,
	[selected_language_id] [int] NULL,
	[test_section_component_id] [int] NULL,
	[item_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalcandidateemailresponseanswers]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalcandidateemailresponseanswers](
	[id] [int] NOT NULL,
	[answer_id] [int] NOT NULL,
	[email_type] [nvarchar](25) NOT NULL,
	[email_body] [nvarchar](max) NULL,
	[reasons_for_action] [nvarchar](max) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[candidate_answers_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalcandidatemultiplechoiceanswers]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalcandidatemultiplechoiceanswers](
	[id] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[answer_id] [int] NULL,
	[candidate_answers_id] [int] NULL,
	[history_user_id] [int] NULL,
	[item_answer_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalcandidatetaskresponseanswers]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalcandidatetaskresponseanswers](
	[id] [int] NOT NULL,
	[answer_id] [int] NOT NULL,
	[task] [nvarchar](max) NULL,
	[reasons_for_action] [nvarchar](max) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[candidate_answers_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalcriticality]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalcriticality](
	[id] [int] NOT NULL,
	[description_en] [nvarchar](150) NOT NULL,
	[description_fr] [nvarchar](150) NOT NULL,
	[active] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[codename] [nvarchar](50) NOT NULL,
	[priority] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalettaactions]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalettaactions](
	[id] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[action_reason] [nvarchar](300) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[action_type_id] [nvarchar](25) NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalettaactiontypes]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalettaactiontypes](
	[action_type] [nvarchar](25) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicallanguage]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicallanguage](
	[language_id] [int] NOT NULL,
	[ISO_Code_1] [nvarchar](2) NOT NULL,
	[ISO_Code_2] [nvarchar](5) NOT NULL,
	[date_created] [datetime2](7) NOT NULL,
	[date_from] [datetime2](7) NOT NULL,
	[date_to] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicallanguagetext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicallanguagetext](
	[id] [int] NOT NULL,
	[text] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[language_ref_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicallocktestactions]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicallocktestactions](
	[id] [int] NOT NULL,
	[lock_start_date] [datetime2](7) NOT NULL,
	[lock_end_date] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[ta_action_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalnotepad]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalnotepad](
	[notepad] [nvarchar](max) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalolaglobalconfigs]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalolaglobalconfigs](
	[id] [int] NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[value] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalolaprioritization]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalolaprioritization](
	[id] [int] NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[value] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalorderlessfinancialdata]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalorderlessfinancialdata](
	[id] [int] NOT NULL,
	[reference_number] [nvarchar](25) NOT NULL,
	[department_ministry_id] [int] NOT NULL,
	[fis_organisation_code] [nvarchar](16) NOT NULL,
	[fis_reference_code] [nvarchar](20) NOT NULL,
	[billing_contact_name] [nvarchar](180) NOT NULL,
	[billing_contact_info] [nvarchar](255) NOT NULL,
	[level_required] [nvarchar](50) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[uit_invite_id] [int] NULL,
	[reason_for_testing_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalpausetestactions]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalpausetestactions](
	[id] [int] NOT NULL,
	[pause_start_date] [datetime2](7) NOT NULL,
	[pause_test_time] [int] NOT NULL,
	[pause_end_date] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[ta_action_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalreasonfortesting]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalreasonfortesting](
	[id] [int] NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[minimum_process_length] [int] NULL,
	[active] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[reason_for_testing_type_id] [int] NULL,
	[waiting_period] [int] NULL,
	[reason_for_testing_priority_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalreasonfortestingpriority]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalreasonfortestingpriority](
	[id] [int] NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalreasonfortestingprioritytext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalreasonfortestingprioritytext](
	[id] [int] NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[reason_for_testing_priority_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalreasonfortestingtext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalreasonfortestingtext](
	[id] [int] NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[reason_for_testing_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalreasonfortestingtype]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalreasonfortestingtype](
	[id] [int] NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalreasonsfortesting]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalreasonsfortesting](
	[id] [int] NOT NULL,
	[description_en] [nvarchar](255) NOT NULL,
	[description_fr] [nvarchar](255) NOT NULL,
	[active] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalreservationcodes]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalreservationcodes](
	[id] [int] NOT NULL,
	[created_date] [date] NOT NULL,
	[reservation_code] [nvarchar](14) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assessment_process_assigned_test_specs_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalscorerolaassignedtestsession]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalscorerolaassignedtestsession](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[candidate_id] [int] NULL,
	[history_user_id] [int] NULL,
	[test_assessor_id] [int] NULL,
	[test_session_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalscorerolatestsessionskipaction]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalscorerolatestsessionskipaction](
	[id] [int] NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[candidate_id] [int] NULL,
	[history_user_id] [int] NULL,
	[scorer_ola_test_session_skip_option_id] [int] NULL,
	[test_assessor_id] [int] NULL,
	[test_session_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalscorerolatestsessionskipoption]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalscorerolatestsessionskipoption](
	[id] [int] NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[active] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalscorerolatestsessionskipoptiontext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalscorerolatestsessionskipoptiontext](
	[id] [int] NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[scorer_ola_test_session_skip_option_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalsiteadminsetting]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalsiteadminsetting](
	[id] [int] NOT NULL,
	[en_description] [nvarchar](75) NOT NULL,
	[fr_description] [nvarchar](75) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[is_active] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicalsystemalert]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicalsystemalert](
	[id] [int] NOT NULL,
	[title] [nvarchar](255) NOT NULL,
	[criticality_id] [int] NULL,
	[active_date] [datetime2](7) NOT NULL,
	[end_date] [datetime2](7) NOT NULL,
	[message_text_en] [nvarchar](max) NOT NULL,
	[message_text_fr] [nvarchar](max) NOT NULL,
	[archived] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltaactions]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltaactions](
	[id] [int] NOT NULL,
	[modify_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[action_type_id] [nvarchar](15) NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
	[test_section_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltaactiontypes]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltaactiontypes](
	[action_type] [nvarchar](15) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestcenter]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestcenter](
	[id] [int] NOT NULL,
	[name] [nvarchar](150) NOT NULL,
	[postal_code] [nvarchar](15) NOT NULL,
	[security_email] [nvarchar](254) NULL,
	[department_id] [int] NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[booking_delay] [int] NOT NULL,
	[country_id] [int] NOT NULL,
	[accommodations_friendly] [bit] NOT NULL,
	[ola_authorized] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestcenteraddresstext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestcenteraddresstext](
	[id] [int] NOT NULL,
	[text] [nvarchar](255) NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[test_center_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestcenterassociatedmanagers]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestcenterassociatedmanagers](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_center_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestcentercitytext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestcentercitytext](
	[id] [int] NOT NULL,
	[text] [nvarchar](150) NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[test_center_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestcenterolaassessorunavailability]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestcenterolaassessorunavailability](
	[id] [int] NOT NULL,
	[start_date] [datetimeoffset](7) NOT NULL,
	[end_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_center_ola_test_assessor_id] [int] NULL,
	[reason] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestcenterotherdetailstext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestcenterotherdetailstext](
	[id] [int] NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[test_center_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestcenterprovincetext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestcenterprovincetext](
	[id] [int] NOT NULL,
	[text] [nvarchar](150) NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[test_center_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestcenterroomdata]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestcenterroomdata](
	[id] [int] NOT NULL,
	[name] [nvarchar](150) NOT NULL,
	[email] [nvarchar](254) NULL,
	[max_occupancy] [int] NOT NULL,
	[active] [bit] NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestcenterroomdataotherdetailstext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestcenterroomdataotherdetailstext](
	[id] [int] NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[test_center_room_data_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestcenterrooms]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestcenterrooms](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[room_data_id] [int] NULL,
	[test_center_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestcentertestadministrators]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestcentertestadministrators](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_center_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestcentertestsessiondata]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestcentertestsessiondata](
	[id] [int] NOT NULL,
	[open_to_ogd] [bit] NULL,
	[date] [date] NOT NULL,
	[start_time] [datetimeoffset](7) NOT NULL,
	[end_time] [datetimeoffset](7) NOT NULL,
	[spaces_available] [int] NULL,
	[test_skill_sub_type_id] [int] NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_center_room_id] [int] NULL,
	[test_skill_type_id] [int] NULL,
	[test_assessor_user_id] [int] NULL,
	[candidate_phone_number] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestcentertestsessionmeetinguuid]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestcentertestsessionmeetinguuid](
	[id] [int] NOT NULL,
	[uuid] [nvarchar](100) NOT NULL,
	[test_session_id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestcentertestsessionofficers]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestcentertestsessionofficers](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_access_code_id] [int] NULL,
	[test_administrator_id] [int] NULL,
	[test_session_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestcentertestsessions]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestcentertestsessions](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[test_center_id] [int] NULL,
	[test_session_data_id] [int] NULL,
	[is_standard] [bit] NOT NULL,
	[user_accommodation_file_id] [int] NULL,
	[assessment_process_assigned_test_specs_id] [int] NULL,
	[test_session_order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaltestscorerassignment]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaltestscorerassignment](
	[id] [int] NOT NULL,
	[status] [int] NOT NULL,
	[score_date] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[assigned_test_id] [int] NULL,
	[history_user_id] [int] NULL,
	[scorer_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluitinviterelatedcandidates]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluitinviterelatedcandidates](
	[id] [int] NOT NULL,
	[first_name] [nvarchar](30) NOT NULL,
	[last_name] [nvarchar](150) NOT NULL,
	[email] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[uit_invite_id] [int] NULL,
	[reason_for_deletion_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluitreasonsfordeletion]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluitreasonsfordeletion](
	[id] [int] NOT NULL,
	[en_name] [nvarchar](50) NOT NULL,
	[fr_name] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluitreasonsformodification]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluitreasonsformodification](
	[id] [int] NOT NULL,
	[en_name] [nvarchar](50) NOT NULL,
	[fr_name] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfilecandidatelimitations]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfilecandidatelimitations](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodation_file_id] [int] NULL,
	[user_accommodation_file_candidate_limitations_option_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfilecandidatelimitationsoption]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfilecandidatelimitationsoption](
	[id] [int] NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[active] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfilecandidatelimitationsoptiontext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfilecandidatelimitationsoptiontext](
	[id] [int] NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[user_accommodation_file_candidate_limitations_option_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfilecomments]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfilecomments](
	[id] [int] NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[comment] [nvarchar](255) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
	[user_accommodation_file_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfilecomplexity]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfilecomplexity](
	[id] [int] NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[active] [bit] NOT NULL,
	[order] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfilecomplexitytext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfilecomplexitytext](
	[id] [int] NOT NULL,
	[text] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[user_accommodation_file_complexity_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfilerecommendations]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfilerecommendations](
	[id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodation_file_id] [int] NULL,
	[user_accommodation_file_recommendations_option_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfilerecommendationsoption]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfilerecommendationsoption](
	[id] [int] NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[active] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfilerecommendationsoptiontext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfilerecommendationsoptiontext](
	[id] [int] NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[user_accommodation_file_recommendations_option_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfilestatus]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfilestatus](
	[id] [int] NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[active] [bit] NOT NULL,
	[order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_historicaluseraccommodationfilestatustext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_historicaluseraccommodationfilestatustext](
	[id] [int] NOT NULL,
	[text] [nvarchar](50) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[language_id] [int] NULL,
	[user_accommodation_file_status_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_keyring]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_keyring](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](20) NOT NULL,
	[key] [nvarchar](120) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_notepad]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_notepad](
	[assigned_test_id] [int] NOT NULL,
	[notepad] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[assigned_test_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_olaglobalconfigs]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_olaglobalconfigs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[value] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_pausetestactions]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_pausetestactions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[pause_start_date] [datetime2](7) NOT NULL,
	[pause_test_time] [int] NOT NULL,
	[pause_end_date] [datetime2](7) NULL,
	[ta_action_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_reasonfortestingprioritytext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_reasonfortestingprioritytext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[language_id] [int] NOT NULL,
	[reason_for_testing_priority_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_reason_for_testing_priority_id_and_language_id_combination] UNIQUE NONCLUSTERED 
(
	[reason_for_testing_priority_id] ASC,
	[language_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_reasonfortestingtype]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_reasonfortestingtype](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_reason_for_testing_type_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_reasonsfortesting]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_reasonsfortesting](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description_en] [nvarchar](255) NOT NULL,
	[description_fr] [nvarchar](255) NOT NULL,
	[active] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_scorerolatestsessionskipaction]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_scorerolatestsessionskipaction](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[candidate_id] [int] NOT NULL,
	[scorer_ola_test_session_skip_option_id] [int] NOT NULL,
	[test_assessor_id] [int] NOT NULL,
	[test_session_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_scorerolatestsessionskipoption]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_scorerolatestsessionskipoption](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[active] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_scorer_ola_test_session_skip_option_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_scorerolatestsessionskipoptiontext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_scorerolatestsessionskipoptiontext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[language_id] [int] NOT NULL,
	[scorer_ola_test_session_skip_option_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_siteadminsetting]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_siteadminsetting](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[en_description] [nvarchar](75) NOT NULL,
	[fr_description] [nvarchar](75) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[is_active] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_systemalert]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_systemalert](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](255) NOT NULL,
	[criticality_id] [int] NOT NULL,
	[active_date] [datetime2](7) NOT NULL,
	[end_date] [datetime2](7) NOT NULL,
	[message_text_en] [nvarchar](max) NOT NULL,
	[message_text_fr] [nvarchar](max) NOT NULL,
	[archived] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_taactiontypes]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_taactiontypes](
	[action_type] [nvarchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[action_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcenteraddresstext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenteraddresstext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](255) NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[language_id] [int] NOT NULL,
	[test_center_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcenterassociatedmanagers]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenterassociatedmanagers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_center_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcenterotherdetailstext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenterotherdetailstext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[language_id] [int] NOT NULL,
	[test_center_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcenterprovincetext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenterprovincetext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](150) NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[language_id] [int] NOT NULL,
	[test_center_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcenterroomdataotherdetailstext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcenterroomdataotherdetailstext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](max) NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[language_id] [int] NOT NULL,
	[test_center_room_data_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcentertestsessionmeetinguuid]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcentertestsessionmeetinguuid](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[uuid] [nvarchar](100) NOT NULL,
	[test_session_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testcentertestsessionofficers]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testcentertestsessionofficers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[test_access_code_id] [int] NOT NULL,
	[test_administrator_id] [int] NOT NULL,
	[test_session_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [test_session__with_test_administrator_and_test_access_code_must_be_a_unique_combination] UNIQUE NONCLUSTERED 
(
	[test_session_id] ASC,
	[test_administrator_id] ASC,
	[test_access_code_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_testscorerassignment]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_testscorerassignment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[status] [int] NOT NULL,
	[score_date] [datetime2](7) NULL,
	[assigned_test_id] [int] NOT NULL,
	[scorer_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_uitreasonsfordeletion]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_uitreasonsfordeletion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[en_name] [nvarchar](50) NOT NULL,
	[fr_name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_uitreasonsformodification]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_uitreasonsformodification](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[en_name] [nvarchar](50) NOT NULL,
	[fr_name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfilecandidatelimitations]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfilecandidatelimitations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_accommodation_file_id] [int] NOT NULL,
	[user_accommodation_file_candidate_limitations_option_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_user_accommodation_file_and_candidate_limitations_option_combination] UNIQUE NONCLUSTERED 
(
	[user_accommodation_file_id] ASC,
	[user_accommodation_file_candidate_limitations_option_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[active] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_user_accommodation_file_candidate_limitations_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[language_id] [int] NOT NULL,
	[user_accommodation_file_candidate_limitations_option_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfilecomments]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfilecomments](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[comment] [nvarchar](255) NOT NULL,
	[user_id] [int] NOT NULL,
	[user_accommodation_file_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfilecomplexitytext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfilecomplexitytext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](50) NOT NULL,
	[language_id] [int] NOT NULL,
	[user_accommodation_file_complexity_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfilerecommendations]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfilerecommendations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_accommodation_file_id] [int] NOT NULL,
	[user_accommodation_file_recommendations_option_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_user_accommodation_file_and_recommendations_option_combination] UNIQUE NONCLUSTERED 
(
	[user_accommodation_file_id] ASC,
	[user_accommodation_file_recommendations_option_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfilerecommendationsoption]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfilerecommendationsoption](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codename] [nvarchar](50) NOT NULL,
	[active] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [must_be_a_unique_user_accommodation_file_recommendations_codename] UNIQUE NONCLUSTERED 
(
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [nvarchar](150) NOT NULL,
	[language_id] [int] NOT NULL,
	[user_accommodation_file_recommendations_option_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_admin_log]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_admin_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[action_time] [datetime2](7) NOT NULL,
	[object_id] [nvarchar](max) NULL,
	[object_repr] [nvarchar](200) NOT NULL,
	[action_flag] [smallint] NOT NULL,
	[change_message] [nvarchar](max) NOT NULL,
	[content_type_id] [int] NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_celery_beat_clockedschedule]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_celery_beat_clockedschedule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[clocked_time] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_celery_beat_crontabschedule]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_celery_beat_crontabschedule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[minute] [nvarchar](240) NOT NULL,
	[hour] [nvarchar](96) NOT NULL,
	[day_of_week] [nvarchar](64) NOT NULL,
	[day_of_month] [nvarchar](124) NOT NULL,
	[month_of_year] [nvarchar](64) NOT NULL,
	[timezone] [nvarchar](63) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_celery_beat_intervalschedule]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_celery_beat_intervalschedule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[every] [int] NOT NULL,
	[period] [nvarchar](24) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_celery_beat_periodictask]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_celery_beat_periodictask](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](200) NOT NULL,
	[task] [nvarchar](200) NOT NULL,
	[args] [nvarchar](max) NOT NULL,
	[kwargs] [nvarchar](max) NOT NULL,
	[queue] [nvarchar](200) NULL,
	[exchange] [nvarchar](200) NULL,
	[routing_key] [nvarchar](200) NULL,
	[expires] [datetime2](7) NULL,
	[enabled] [bit] NOT NULL,
	[last_run_at] [datetime2](7) NULL,
	[total_run_count] [int] NOT NULL,
	[date_changed] [datetime2](7) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[crontab_id] [int] NULL,
	[interval_id] [int] NULL,
	[solar_id] [int] NULL,
	[one_off] [bit] NOT NULL,
	[start_time] [datetime2](7) NULL,
	[priority] [int] NULL,
	[headers] [nvarchar](max) NOT NULL,
	[clocked_id] [int] NULL,
	[expire_seconds] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_celery_beat_periodictasks]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_celery_beat_periodictasks](
	[ident] [smallint] NOT NULL,
	[last_update] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ident] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_celery_beat_solarschedule]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_celery_beat_solarschedule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[event] [nvarchar](24) NOT NULL,
	[latitude] [numeric](9, 6) NOT NULL,
	[longitude] [numeric](9, 6) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_content_type]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_content_type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[app_label] [nvarchar](100) NOT NULL,
	[model] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_migrations]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_migrations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[app] [nvarchar](255) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[applied] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_rest_passwordreset_resetpasswordtoken]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_rest_passwordreset_resetpasswordtoken](
	[created_at] [datetime2](7) NOT NULL,
	[key] [nvarchar](64) NOT NULL,
	[ip_address] [nvarchar](39) NULL,
	[user_agent] [nvarchar](256) NOT NULL,
	[user_id] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [django_rest_passwordreset_resetpasswordtoken_key_f1b65873_uniq] UNIQUE NONCLUSTERED 
(
	[key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_session]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_session](
	[session_key] [nvarchar](40) NOT NULL,
	[session_data] [nvarchar](max) NOT NULL,
	[expire_date] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[session_key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_cateducation]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_cateducation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[education_id] [int] NOT NULL,
	[active_flg] [nvarchar](1) NOT NULL,
	[edesc] [nvarchar](200) NOT NULL,
	[fdesc] [nvarchar](200) NOT NULL,
	[efdt] [datetime2](7) NOT NULL,
	[xdt] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcateducation]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcateducation](
	[education_id] [int] NOT NULL,
	[active_flg] [nvarchar](1) NOT NULL,
	[edesc] [nvarchar](200) NOT NULL,
	[fdesc] [nvarchar](200) NOT NULL,
	[efdt] [datetime2](7) NOT NULL,
	[xdt] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefaboriginalvw]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefaboriginalvw](
	[ABRG_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefclassificationvw]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefclassificationvw](
	[CLASSIF_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[CLASS_GRP_CD] [nvarchar](2) NOT NULL,
	[CLASS_SBGRP_CD] [nvarchar](3) NULL,
	[CLASS_LVL_CD] [nvarchar](2) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[PSC_CLASS_IND] [int] NOT NULL,
	[DFLT_BUD_CD] [nvarchar](5) NULL,
	[EQLZN_AMT] [int] NULL,
	[EQLZN_AMT_EFDT] [datetime2](7) NULL,
	[EQLZN_AMT_XDT] [datetime2](7) NULL,
	[OFCR_LVL_IND] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefcountryvw]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefcountryvw](
	[CNTRY_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](64) NULL,
	[FABRV] [nvarchar](64) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetimeoffset](7) NOT NULL,
	[XDT] [datetimeoffset](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefdayofweekvw]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefdayofweekvw](
	[id] [int] NOT NULL,
	[day_of_week_id] [int] NOT NULL,
	[text] [nvarchar](50) NOT NULL,
	[language_id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefdepartmentsvw]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefdepartmentsvw](
	[DEPT_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](20) NULL,
	[FABRV] [nvarchar](20) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefdisabilityvw]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefdisabilityvw](
	[DSBL_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[FULL_EDESC] [nvarchar](200) NULL,
	[FULL_FDESC] [nvarchar](200) NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefemployerstsvw]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefemployerstsvw](
	[EMPSTS_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefgendervw]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefgendervw](
	[GND_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatreflanguagevw]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatreflanguagevw](
	[language_id] [int] NOT NULL,
	[iso_code_1] [nvarchar](50) NOT NULL,
	[iso_code_2] [nvarchar](50) NOT NULL,
	[edesc] [nvarchar](50) NOT NULL,
	[fdesc] [nvarchar](50) NOT NULL,
	[date_from] [datetimeoffset](7) NOT NULL,
	[date_to] [datetimeoffset](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefprovincevw]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefprovincevw](
	[PROV_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EABRV] [nvarchar](10) NULL,
	[FABRV] [nvarchar](10) NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefresidencevw]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefresidencevw](
	[AREA_RES_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetimeoffset](7) NOT NULL,
	[XDT] [datetimeoffset](7) NULL,
	[PRESENTATION_ORDER] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ref_table_views_historicalcatrefvisibleminorityvw]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ref_table_views_historicalcatrefvisibleminorityvw](
	[VISMIN_ID] [int] NOT NULL,
	[ACTIVE_FLG] [nvarchar](1) NOT NULL,
	[EDESC] [nvarchar](200) NOT NULL,
	[FDESC] [nvarchar](200) NOT NULL,
	[EFDT] [datetime2](7) NOT NULL,
	[XDT] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_accommodations]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_accommodations](
	[user_id] [int] NOT NULL,
	[font_family] [nvarchar](75) NULL,
	[font_size] [nvarchar](10) NULL,
	[spacing] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_eeinfooptions]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_eeinfooptions](
	[opt_id] [int] NOT NULL,
	[opt_value] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[opt_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicalaccommodations]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicalaccommodations](
	[font_family] [nvarchar](75) NULL,
	[font_size] [nvarchar](10) NULL,
	[spacing] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicalcustompermission2fa]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicalcustompermission2fa](
	[id] [int] NOT NULL,
	[is_2fa_active] [bit] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[custom_permission_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicalcustompermissions]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicalcustompermissions](
	[permission_id] [int] NOT NULL,
	[en_name] [nvarchar](75) NOT NULL,
	[fr_name] [nvarchar](75) NOT NULL,
	[en_description] [nvarchar](max) NOT NULL,
	[fr_description] [nvarchar](max) NOT NULL,
	[codename] [nvarchar](25) NOT NULL,
	[date_assigned] [datetime2](7) NOT NULL,
	[expiry_date] [datetime2](7) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[content_type_id] [int] NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaleeinfooptions]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaleeinfooptions](
	[opt_id] [int] NOT NULL,
	[opt_value] [nvarchar](25) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluser2fatracking]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluser2fatracking](
	[id] [int] NOT NULL,
	[two_factor_auth_code] [nvarchar](6) NOT NULL,
	[expiry_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluseraccommodationsprofile]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluseraccommodationsprofile](
	[id] [int] NOT NULL,
	[has_built_in_accessibility] [bit] NOT NULL,
	[needs_extra_time] [bit] NOT NULL,
	[needs_breaks] [bit] NOT NULL,
	[needs_adaptive_tech] [bit] NOT NULL,
	[needs_ergonomic] [bit] NOT NULL,
	[needs_access_assistance] [bit] NOT NULL,
	[needs_environment] [bit] NOT NULL,
	[has_other_needs_written] [bit] NOT NULL,
	[previously_accommodated] [bit] NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[created_date] [date] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
	[has_other_needs_oral] [bit] NOT NULL,
	[needs_repeated_oral_questions] [bit] NOT NULL,
	[needs_scheduling_adaptation] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluseraccommodationsprofileaccessassistance]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileaccessassistance](
	[id] [int] NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodations_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluseraccommodationsprofileadaptivetech]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileadaptivetech](
	[id] [int] NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodations_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluseraccommodationsprofilebreaks]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluseraccommodationsprofilebreaks](
	[id] [int] NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodations_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluseraccommodationsprofilebuiltinaccessibility]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluseraccommodationsprofilebuiltinaccessibility](
	[id] [int] NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodations_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluseraccommodationsprofileenvironment]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileenvironment](
	[id] [int] NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodations_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluseraccommodationsprofileergonomic]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileergonomic](
	[id] [int] NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodations_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluseraccommodationsprofileextratime]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileextratime](
	[id] [int] NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodations_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluseraccommodationsprofileotherneedsoral]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileotherneedsoral](
	[id] [int] NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodations_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluseraccommodationsprofileotherneedswritten]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileotherneedswritten](
	[id] [int] NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodations_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluseraccommodationsprofilepreviouslyaccommodated]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluseraccommodationsprofilepreviouslyaccommodated](
	[id] [int] NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodations_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluseraccommodationsprofilerepeatedoralquestions]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluseraccommodationsprofilerepeatedoralquestions](
	[id] [int] NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodations_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluseraccommodationsprofileschedulingadaptation]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileschedulingadaptation](
	[id] [int] NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_accommodations_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluserextendedprofile]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluserextendedprofile](
	[id] [int] NOT NULL,
	[current_employer_id] [int] NOT NULL,
	[organization_id] [int] NOT NULL,
	[group_id] [int] NOT NULL,
	[level_id] [int] NOT NULL,
	[residence_id_deprecated] [int] NULL,
	[education_id] [int] NOT NULL,
	[gender_id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
	[aboriginal_id] [int] NULL,
	[disability_id] [int] NULL,
	[identify_as_woman_id] [int] NULL,
	[visible_minority_id] [int] NULL,
	[subgroup_id] [int] NOT NULL,
	[residence_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluserextendedprofileaboriginal]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluserextendedprofileaboriginal](
	[id] [int] NOT NULL,
	[aboriginal_id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_extended_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluserextendedprofiledisability]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluserextendedprofiledisability](
	[id] [int] NOT NULL,
	[other_specification] [nvarchar](200) NULL,
	[disability_id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_extended_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluserextendedprofilevisibleminority]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluserextendedprofilevisibleminority](
	[id] [int] NOT NULL,
	[other_specification] [nvarchar](200) NULL,
	[visible_minority_id] [int] NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_extended_profile_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluserpasswordresettracking]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluserpasswordresettracking](
	[id] [int] NOT NULL,
	[request_attempts] [int] NOT NULL,
	[last_request_date] [datetime2](7) NOT NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetime2](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_historicaluserprofilechangerequest]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_historicaluserprofilechangerequest](
	[id] [int] NOT NULL,
	[current_first_name] [nvarchar](30) NOT NULL,
	[current_last_name] [nvarchar](150) NOT NULL,
	[current_birth_date] [date] NOT NULL,
	[new_first_name] [nvarchar](30) NULL,
	[new_last_name] [nvarchar](150) NULL,
	[new_birth_date] [date] NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[comments] [nvarchar](max) NULL,
	[reason_for_deny] [nvarchar](255) NULL,
	[history_id] [int] IDENTITY(1,1) NOT NULL,
	[history_date] [datetimeoffset](7) NOT NULL,
	[history_change_reason] [nvarchar](100) NULL,
	[history_type] [nvarchar](1) NOT NULL,
	[history_user_id] [int] NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_permissionrequest]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_permissionrequest](
	[permission_request_id] [int] IDENTITY(1,1) NOT NULL,
	[goc_email] [nvarchar](254) NOT NULL,
	[pri] [nvarchar](10) NOT NULL,
	[supervisor] [nvarchar](180) NOT NULL,
	[supervisor_email] [nvarchar](254) NOT NULL,
	[rationale] [nvarchar](300) NOT NULL,
	[request_date] [datetime2](7) NOT NULL,
	[permission_requested_id] [int] NOT NULL,
	[military_nbr] [nvarchar](9) NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[permission_request_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [one_user_id_per_permission_requested_instance] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[permission_requested_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_user_groups]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_user_groups](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[group_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_user_user_permissions]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_user_user_permissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[permission_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_user2fatracking]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_user2fatracking](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[two_factor_auth_code] [nvarchar](6) NOT NULL,
	[expiry_date] [datetimeoffset](7) NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [user_management_models_user2fatracking_two_factor_auth_code_9d4d6cc5_uniq] UNIQUE NONCLUSTERED 
(
	[two_factor_auth_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_useraccommodationsprofileaccessassistance]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_useraccommodationsprofileaccessassistance](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[user_accommodations_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_accommodations_profile_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_useraccommodationsprofileadaptivetech]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_useraccommodationsprofileadaptivetech](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[user_accommodations_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_accommodations_profile_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_useraccommodationsprofilebreaks]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_useraccommodationsprofilebreaks](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[user_accommodations_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_accommodations_profile_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_useraccommodationsprofilebuiltinaccessibility]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_useraccommodationsprofilebuiltinaccessibility](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[user_accommodations_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_accommodations_profile_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_useraccommodationsprofileenvironment]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_useraccommodationsprofileenvironment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[user_accommodations_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_accommodations_profile_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_useraccommodationsprofileergonomic]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_useraccommodationsprofileergonomic](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[user_accommodations_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_accommodations_profile_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_useraccommodationsprofileextratime]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_useraccommodationsprofileextratime](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[user_accommodations_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_accommodations_profile_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_useraccommodationsprofileotherneedsoral]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_useraccommodationsprofileotherneedsoral](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[user_accommodations_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_accommodations_profile_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_useraccommodationsprofileotherneedswritten]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_useraccommodationsprofileotherneedswritten](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[user_accommodations_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_accommodations_profile_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_useraccommodationsprofilepreviouslyaccommodated]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_useraccommodationsprofilepreviouslyaccommodated](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[user_accommodations_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_accommodations_profile_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_useraccommodationsprofilerepeatedoralquestions]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_useraccommodationsprofilerepeatedoralquestions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[user_accommodations_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_accommodations_profile_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_useraccommodationsprofileschedulingadaptation]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_useraccommodationsprofileschedulingadaptation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[modify_date] [datetimeoffset](7) NOT NULL,
	[user_accommodations_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_accommodations_profile_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_userextendedprofile]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_userextendedprofile](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[current_employer_id] [int] NOT NULL,
	[organization_id] [int] NOT NULL,
	[group_id] [int] NOT NULL,
	[level_id] [int] NOT NULL,
	[residence_id_deprecated] [int] NULL,
	[education_id] [int] NOT NULL,
	[gender_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[aboriginal_id] [int] NOT NULL,
	[disability_id] [int] NOT NULL,
	[identify_as_woman_id] [int] NOT NULL,
	[visible_minority_id] [int] NOT NULL,
	[subgroup_id] [int] NOT NULL,
	[residence_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_userextendedprofileaboriginal]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_userextendedprofileaboriginal](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[aboriginal_id] [int] NOT NULL,
	[user_extended_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_userextendedprofiledisability]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_userextendedprofiledisability](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[other_specification] [nvarchar](200) NULL,
	[disability_id] [int] NOT NULL,
	[user_extended_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_userextendedprofilevisibleminority]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_userextendedprofilevisibleminority](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[other_specification] [nvarchar](200) NULL,
	[visible_minority_id] [int] NOT NULL,
	[user_extended_profile_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_management_models_userpasswordresettracking]    Script Date: 2025-03-05 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_management_models_userpasswordresettracking](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[request_attempts] [int] NOT NULL,
	[last_request_date] [datetime2](7) NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[auth_group_permissions]  WITH CHECK ADD  CONSTRAINT [auth_group_permissions_group_id_b120cbf9_fk_auth_group_id] FOREIGN KEY([group_id])
REFERENCES [dbo].[auth_group] ([id])
GO
ALTER TABLE [dbo].[auth_group_permissions] CHECK CONSTRAINT [auth_group_permissions_group_id_b120cbf9_fk_auth_group_id]
GO
ALTER TABLE [dbo].[auth_group_permissions]  WITH CHECK ADD  CONSTRAINT [auth_group_permissions_permission_id_84c5c92e_fk_auth_permission_id] FOREIGN KEY([permission_id])
REFERENCES [dbo].[auth_permission] ([id])
GO
ALTER TABLE [dbo].[auth_group_permissions] CHECK CONSTRAINT [auth_group_permissions_permission_id_84c5c92e_fk_auth_permission_id]
GO
ALTER TABLE [dbo].[auth_permission]  WITH CHECK ADD  CONSTRAINT [auth_permission_content_type_id_2f476e4b_fk_django_content_type_id] FOREIGN KEY([content_type_id])
REFERENCES [dbo].[django_content_type] ([id])
GO
ALTER TABLE [dbo].[auth_permission] CHECK CONSTRAINT [auth_permission_content_type_id_2f476e4b_fk_django_content_type_id]
GO
ALTER TABLE [dbo].[authtoken_token]  WITH CHECK ADD  CONSTRAINT [authtoken_token_user_id_35299eff_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[authtoken_token] CHECK CONSTRAINT [authtoken_token_user_id_35299eff_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_addressbookcontact]  WITH CHECK ADD  CONSTRAINT [cms_models_addressbookcontact_parent_id_79b1b083_fk_cms_models_addressbookcontact_id] FOREIGN KEY([parent_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[cms_models_addressbookcontact] CHECK CONSTRAINT [cms_models_addressbookcontact_parent_id_79b1b083_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[cms_models_addressbookcontact_test_section]  WITH CHECK ADD  CONSTRAINT [cms_models_addressbookcontact_test_section_addressbookcontact_id_fbad927a_fk_cms_models_addressbookcontact_id] FOREIGN KEY([addressbookcontact_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[cms_models_addressbookcontact_test_section] CHECK CONSTRAINT [cms_models_addressbookcontact_test_section_addressbookcontact_id_fbad927a_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[cms_models_addressbookcontact_test_section]  WITH CHECK ADD  CONSTRAINT [cms_models_addressbookcontact_test_section_testsection_id_160077cf_fk_cms_models_testsection_id] FOREIGN KEY([testsection_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[cms_models_addressbookcontact_test_section] CHECK CONSTRAINT [cms_models_addressbookcontact_test_section_testsection_id_160077cf_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[cms_models_addressbookcontactdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_addressbookcontactdetails_contact_id_f1faffff_fk_cms_models_addressbookcontact_id] FOREIGN KEY([contact_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[cms_models_addressbookcontactdetails] CHECK CONSTRAINT [cms_models_addressbookcontactdetails_contact_id_f1faffff_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[cms_models_addressbookcontactdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_addressbookcontactdetails_language_id_83f6f951_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_addressbookcontactdetails] CHECK CONSTRAINT [cms_models_addressbookcontactdetails_language_id_83f6f951_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_answer]  WITH CHECK ADD  CONSTRAINT [cms_models_answer_question_id_806baab0_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_answer] CHECK CONSTRAINT [cms_models_answer_question_id_806baab0_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_answerdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_answerdetails_answer_id_a8677b2a_fk_cms_models_answer_id] FOREIGN KEY([answer_id])
REFERENCES [dbo].[cms_models_answer] ([id])
GO
ALTER TABLE [dbo].[cms_models_answerdetails] CHECK CONSTRAINT [cms_models_answerdetails_answer_id_a8677b2a_fk_cms_models_answer_id]
GO
ALTER TABLE [dbo].[cms_models_answerdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_answerdetails_language_id_6036a3f6_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_answerdetails] CHECK CONSTRAINT [cms_models_answerdetails_language_id_6036a3f6_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesassociation]  WITH CHECK ADD  CONSTRAINT [cms_models_bundlebundlesassociation_bundle_link_id_6c6b165e_fk_cms_models_bundles_id] FOREIGN KEY([bundle_link_id])
REFERENCES [dbo].[cms_models_bundles] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesassociation] CHECK CONSTRAINT [cms_models_bundlebundlesassociation_bundle_link_id_6c6b165e_fk_cms_models_bundles_id]
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesassociation]  WITH CHECK ADD  CONSTRAINT [cms_models_bundlebundlesassociation_bundle_source_id_a18a1360_fk_cms_models_bundles_id] FOREIGN KEY([bundle_source_id])
REFERENCES [dbo].[cms_models_bundles] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesassociation] CHECK CONSTRAINT [cms_models_bundlebundlesassociation_bundle_source_id_a18a1360_fk_cms_models_bundles_id]
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesrule]  WITH CHECK ADD  CONSTRAINT [cms_models_bundlebundlesrule_bundle_rule_id_064bd8a7_fk_cms_models_bundlerule_id] FOREIGN KEY([bundle_rule_id])
REFERENCES [dbo].[cms_models_bundlerule] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesrule] CHECK CONSTRAINT [cms_models_bundlebundlesrule_bundle_rule_id_064bd8a7_fk_cms_models_bundlerule_id]
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesruleassociations]  WITH CHECK ADD  CONSTRAINT [cms_models_bundlebundlesruleassociations_bundle_bundles_rule_id_916d9ccf_fk_cms_models_bundlebundlesrule_id] FOREIGN KEY([bundle_bundles_rule_id])
REFERENCES [dbo].[cms_models_bundlebundlesrule] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesruleassociations] CHECK CONSTRAINT [cms_models_bundlebundlesruleassociations_bundle_bundles_rule_id_916d9ccf_fk_cms_models_bundlebundlesrule_id]
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesruleassociations]  WITH CHECK ADD  CONSTRAINT [cms_models_bundlebundlesruleassociations_bundle_id_9580884a_fk_cms_models_bundles_id] FOREIGN KEY([bundle_id])
REFERENCES [dbo].[cms_models_bundles] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundlebundlesruleassociations] CHECK CONSTRAINT [cms_models_bundlebundlesruleassociations_bundle_id_9580884a_fk_cms_models_bundles_id]
GO
ALTER TABLE [dbo].[cms_models_bundleitemsassociation]  WITH CHECK ADD  CONSTRAINT [cms_models_bundleitemsassociation_bundle_source_id_9aa01f76_fk_cms_models_bundles_id] FOREIGN KEY([bundle_source_id])
REFERENCES [dbo].[cms_models_bundles] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundleitemsassociation] CHECK CONSTRAINT [cms_models_bundleitemsassociation_bundle_source_id_9aa01f76_fk_cms_models_bundles_id]
GO
ALTER TABLE [dbo].[cms_models_bundleitemsbasicrule]  WITH CHECK ADD  CONSTRAINT [cms_models_bundleitemsbasicrule_bundle_rule_id_4d81112d_fk_cms_models_bundlerule_id] FOREIGN KEY([bundle_rule_id])
REFERENCES [dbo].[cms_models_bundlerule] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundleitemsbasicrule] CHECK CONSTRAINT [cms_models_bundleitemsbasicrule_bundle_rule_id_4d81112d_fk_cms_models_bundlerule_id]
GO
ALTER TABLE [dbo].[cms_models_bundlerule]  WITH CHECK ADD  CONSTRAINT [cms_models_bundlerule_bundle_id_83cd1eb1_fk_cms_models_bundles_id] FOREIGN KEY([bundle_id])
REFERENCES [dbo].[cms_models_bundles] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundlerule] CHECK CONSTRAINT [cms_models_bundlerule_bundle_id_83cd1eb1_fk_cms_models_bundles_id]
GO
ALTER TABLE [dbo].[cms_models_bundlerule]  WITH CHECK ADD  CONSTRAINT [cms_models_bundlerule_rule_type_id_c3dcf271_fk_cms_models_bundleruletype_id] FOREIGN KEY([rule_type_id])
REFERENCES [dbo].[cms_models_bundleruletype] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundlerule] CHECK CONSTRAINT [cms_models_bundlerule_rule_type_id_c3dcf271_fk_cms_models_bundleruletype_id]
GO
ALTER TABLE [dbo].[cms_models_bundles]  WITH CHECK ADD  CONSTRAINT [cms_models_bundles_item_bank_id_0b261390_fk_cms_models_itembank_id] FOREIGN KEY([item_bank_id])
REFERENCES [dbo].[cms_models_itembank] ([id])
GO
ALTER TABLE [dbo].[cms_models_bundles] CHECK CONSTRAINT [cms_models_bundles_item_bank_id_0b261390_fk_cms_models_itembank_id]
GO
ALTER TABLE [dbo].[cms_models_competencytype]  WITH CHECK ADD  CONSTRAINT [cms_models_competencytype_test_definition_id_c6db2ef3_fk_cms_models_testdefinition_id] FOREIGN KEY([test_definition_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[cms_models_competencytype] CHECK CONSTRAINT [cms_models_competencytype_test_definition_id_c6db2ef3_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[cms_models_contenttypetext]  WITH CHECK ADD  CONSTRAINT [cms_models_contenttypetext_content_type_id_d78e6c05_fk_cms_models_contenttype_id] FOREIGN KEY([content_type_id])
REFERENCES [dbo].[cms_models_contenttype] ([id])
GO
ALTER TABLE [dbo].[cms_models_contenttypetext] CHECK CONSTRAINT [cms_models_contenttypetext_content_type_id_d78e6c05_fk_cms_models_contenttype_id]
GO
ALTER TABLE [dbo].[cms_models_contenttypetext]  WITH CHECK ADD  CONSTRAINT [cms_models_contenttypetext_language_id_244a9cc5_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_contenttypetext] CHECK CONSTRAINT [cms_models_contenttypetext_language_id_244a9cc5_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_from_field_id_672cf5e1_fk_cms_models_addressbookcontact_id] FOREIGN KEY([from_field_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion] CHECK CONSTRAINT [cms_models_emailquestion_from_field_id_672cf5e1_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_question_id_8722face_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion] CHECK CONSTRAINT [cms_models_emailquestion_question_id_8722face_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion_cc_field]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_cc_field_addressbookcontact_id_a5b39c8a_fk_cms_models_addressbookcontact_id] FOREIGN KEY([addressbookcontact_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion_cc_field] CHECK CONSTRAINT [cms_models_emailquestion_cc_field_addressbookcontact_id_a5b39c8a_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion_cc_field]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_cc_field_emailquestion_id_247929dd_fk_cms_models_emailquestion_id] FOREIGN KEY([emailquestion_id])
REFERENCES [dbo].[cms_models_emailquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion_cc_field] CHECK CONSTRAINT [cms_models_emailquestion_cc_field_emailquestion_id_247929dd_fk_cms_models_emailquestion_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion_competency_types]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_competency_types_competencytype_id_740ad965_fk_cms_models_competencytype_id] FOREIGN KEY([competencytype_id])
REFERENCES [dbo].[cms_models_competencytype] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion_competency_types] CHECK CONSTRAINT [cms_models_emailquestion_competency_types_competencytype_id_740ad965_fk_cms_models_competencytype_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion_competency_types]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_competency_types_emailquestion_id_9dd0bbbe_fk_cms_models_emailquestion_id] FOREIGN KEY([emailquestion_id])
REFERENCES [dbo].[cms_models_emailquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion_competency_types] CHECK CONSTRAINT [cms_models_emailquestion_competency_types_emailquestion_id_9dd0bbbe_fk_cms_models_emailquestion_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion_to_field]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_to_field_addressbookcontact_id_4dbb8281_fk_cms_models_addressbookcontact_id] FOREIGN KEY([addressbookcontact_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion_to_field] CHECK CONSTRAINT [cms_models_emailquestion_to_field_addressbookcontact_id_4dbb8281_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestion_to_field]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestion_to_field_emailquestion_id_e2c8020e_fk_cms_models_emailquestion_id] FOREIGN KEY([emailquestion_id])
REFERENCES [dbo].[cms_models_emailquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestion_to_field] CHECK CONSTRAINT [cms_models_emailquestion_to_field_emailquestion_id_e2c8020e_fk_cms_models_emailquestion_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestiondetails]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestiondetails_email_question_id_c2edf1c2_fk_cms_models_emailquestion_id] FOREIGN KEY([email_question_id])
REFERENCES [dbo].[cms_models_emailquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_emailquestiondetails] CHECK CONSTRAINT [cms_models_emailquestiondetails_email_question_id_c2edf1c2_fk_cms_models_emailquestion_id]
GO
ALTER TABLE [dbo].[cms_models_emailquestiondetails]  WITH CHECK ADD  CONSTRAINT [cms_models_emailquestiondetails_language_id_0224dc80_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_emailquestiondetails] CHECK CONSTRAINT [cms_models_emailquestiondetails_language_id_0224dc80_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_fileresourcedetails]  WITH CHECK ADD  CONSTRAINT [cms_models_fileresourcedetails_test_definition_id_b42e598c_fk_cms_models_testdefinition_id] FOREIGN KEY([test_definition_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[cms_models_fileresourcedetails] CHECK CONSTRAINT [cms_models_fileresourcedetails_test_definition_id_b42e598c_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[cms_models_historicaladdressbookcontact]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaladdressbookcontact_history_user_id_169d2cd5_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaladdressbookcontact] CHECK CONSTRAINT [cms_models_historicaladdressbookcontact_history_user_id_169d2cd5_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaladdressbookcontactdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaladdressbookcontactdetails_history_user_id_d61a839a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaladdressbookcontactdetails] CHECK CONSTRAINT [cms_models_historicaladdressbookcontactdetails_history_user_id_d61a839a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalanswer]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalanswer_history_user_id_a46c511b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalanswer] CHECK CONSTRAINT [cms_models_historicalanswer_history_user_id_a46c511b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalanswerdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalanswerdetails_history_user_id_0bdcd587_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalanswerdetails] CHECK CONSTRAINT [cms_models_historicalanswerdetails_history_user_id_0bdcd587_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundlebundlesassociation]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundlebundlesassociation_history_user_id_687fa5f8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundlebundlesassociation] CHECK CONSTRAINT [cms_models_historicalbundlebundlesassociation_history_user_id_687fa5f8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundlebundlesrule]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundlebundlesrule_history_user_id_6a6b65c1_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundlebundlesrule] CHECK CONSTRAINT [cms_models_historicalbundlebundlesrule_history_user_id_6a6b65c1_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundlebundlesruleassociations]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundlebundlesruleassociations_history_user_id_591e87f8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundlebundlesruleassociations] CHECK CONSTRAINT [cms_models_historicalbundlebundlesruleassociations_history_user_id_591e87f8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundleitemsassociation]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundleitemsassociation_history_user_id_ae061ebd_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundleitemsassociation] CHECK CONSTRAINT [cms_models_historicalbundleitemsassociation_history_user_id_ae061ebd_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundleitemsbasicrule]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundleitemsbasicrule_history_user_id_3fa59b22_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundleitemsbasicrule] CHECK CONSTRAINT [cms_models_historicalbundleitemsbasicrule_history_user_id_3fa59b22_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundlerule]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundlerule_history_user_id_52ef23e5_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundlerule] CHECK CONSTRAINT [cms_models_historicalbundlerule_history_user_id_52ef23e5_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundleruletype]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundleruletype_history_user_id_3d33bed6_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundleruletype] CHECK CONSTRAINT [cms_models_historicalbundleruletype_history_user_id_3d33bed6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalbundles]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalbundles_history_user_id_c6ce3cf0_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalbundles] CHECK CONSTRAINT [cms_models_historicalbundles_history_user_id_c6ce3cf0_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalcompetencytype]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalcompetencytype_history_user_id_03723bb8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalcompetencytype] CHECK CONSTRAINT [cms_models_historicalcompetencytype_history_user_id_03723bb8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalcontenttype]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalcontenttype_history_user_id_952e43c4_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalcontenttype] CHECK CONSTRAINT [cms_models_historicalcontenttype_history_user_id_952e43c4_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalcontenttypetext]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalcontenttypetext_history_user_id_e0355ef1_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalcontenttypetext] CHECK CONSTRAINT [cms_models_historicalcontenttypetext_history_user_id_e0355ef1_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalemailquestion]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalemailquestion_history_user_id_8fe38777_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalemailquestion] CHECK CONSTRAINT [cms_models_historicalemailquestion_history_user_id_8fe38777_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalemailquestiondetails]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalemailquestiondetails_history_user_id_be7314d8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalemailquestiondetails] CHECK CONSTRAINT [cms_models_historicalemailquestiondetails_history_user_id_be7314d8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalfileresourcedetails]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalfileresourcedetails_history_user_id_4fcbed7a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalfileresourcedetails] CHECK CONSTRAINT [cms_models_historicalfileresourcedetails_history_user_id_4fcbed7a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemattributevalue]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemattributevalue_history_user_id_d97a2eb7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemattributevalue] CHECK CONSTRAINT [cms_models_historicalitemattributevalue_history_user_id_d97a2eb7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemattributevaluedrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemattributevaluedrafts_history_user_id_e3aa635b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemattributevaluedrafts] CHECK CONSTRAINT [cms_models_historicalitemattributevaluedrafts_history_user_id_e3aa635b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembank]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembank_history_user_id_04c046c4_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembank] CHECK CONSTRAINT [cms_models_historicalitembank_history_user_id_04c046c4_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembankaccesstypes]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembankaccesstypes_history_user_id_d238ee07_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembankaccesstypes] CHECK CONSTRAINT [cms_models_historicalitembankaccesstypes_history_user_id_d238ee07_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributes]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembankattributes_history_user_id_d5ec1401_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributes] CHECK CONSTRAINT [cms_models_historicalitembankattributes_history_user_id_d5ec1401_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributestext]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembankattributestext_history_user_id_a07cf2c6_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributestext] CHECK CONSTRAINT [cms_models_historicalitembankattributestext_history_user_id_a07cf2c6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributevalues]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembankattributevalues_history_user_id_672987d7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributevalues] CHECK CONSTRAINT [cms_models_historicalitembankattributevalues_history_user_id_672987d7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributevaluestext]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembankattributevaluestext_history_user_id_5c546f91_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembankattributevaluestext] CHECK CONSTRAINT [cms_models_historicalitembankattributevaluestext_history_user_id_5c546f91_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembankpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembankpermissions_history_user_id_d485f3e7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembankpermissions] CHECK CONSTRAINT [cms_models_historicalitembankpermissions_history_user_id_d485f3e7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitembankrule]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitembankrule_history_user_id_e08f44cb_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitembankrule] CHECK CONSTRAINT [cms_models_historicalitembankrule_history_user_id_e08f44cb_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemcomments]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemcomments_history_user_id_ac0c70f0_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemcomments] CHECK CONSTRAINT [cms_models_historicalitemcomments_history_user_id_ac0c70f0_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontent]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemcontent_history_user_id_c55c24c2_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontent] CHECK CONSTRAINT [cms_models_historicalitemcontent_history_user_id_c55c24c2_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontentdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemcontentdrafts_history_user_id_1ce23c77_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontentdrafts] CHECK CONSTRAINT [cms_models_historicalitemcontentdrafts_history_user_id_1ce23c77_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontenttext]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemcontenttext_history_user_id_4ec1df1e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontenttext] CHECK CONSTRAINT [cms_models_historicalitemcontenttext_history_user_id_4ec1df1e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontenttextdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemcontenttextdrafts_history_user_id_ff112d0f_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemcontenttextdrafts] CHECK CONSTRAINT [cms_models_historicalitemcontenttextdrafts_history_user_id_ff112d0f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemdevelopmentstatuses]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemdevelopmentstatuses_history_user_id_edf80216_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemdevelopmentstatuses] CHECK CONSTRAINT [cms_models_historicalitemdevelopmentstatuses_history_user_id_edf80216_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemdrafts_history_user_id_431d565d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemdrafts] CHECK CONSTRAINT [cms_models_historicalitemdrafts_history_user_id_431d565d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemhistoricalids]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemhistoricalids_history_user_id_6036519a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemhistoricalids] CHECK CONSTRAINT [cms_models_historicalitemhistoricalids_history_user_id_6036519a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemoption]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemoption_history_user_id_38210b29_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemoption] CHECK CONSTRAINT [cms_models_historicalitemoption_history_user_id_38210b29_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemoptiondrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemoptiondrafts_history_user_id_71b44451_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemoptiondrafts] CHECK CONSTRAINT [cms_models_historicalitemoptiondrafts_history_user_id_71b44451_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemoptiontext]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemoptiontext_history_user_id_f95f76bd_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemoptiontext] CHECK CONSTRAINT [cms_models_historicalitemoptiontext_history_user_id_f95f76bd_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemoptiontextdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemoptiontextdrafts_history_user_id_7e77ea68_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemoptiontextdrafts] CHECK CONSTRAINT [cms_models_historicalitemoptiontextdrafts_history_user_id_7e77ea68_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemresponseformats]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemresponseformats_history_user_id_fdc1c6eb_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemresponseformats] CHECK CONSTRAINT [cms_models_historicalitemresponseformats_history_user_id_fdc1c6eb_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitems]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitems_history_user_id_26c39f49_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitems] CHECK CONSTRAINT [cms_models_historicalitems_history_user_id_26c39f49_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemtype]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemtype_history_user_id_7a3bc0e3_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemtype] CHECK CONSTRAINT [cms_models_historicalitemtype_history_user_id_7a3bc0e3_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalitemtypetext]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalitemtypetext_history_user_id_fb4c6a44_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalitemtypetext] CHECK CONSTRAINT [cms_models_historicalitemtypetext_history_user_id_fb4c6a44_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalmultiplechoicequestion]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalmultiplechoicequestion_history_user_id_2127596e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalmultiplechoicequestion] CHECK CONSTRAINT [cms_models_historicalmultiplechoicequestion_history_user_id_2127596e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalnewquestion]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalnewquestion_history_user_id_646fa9db_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalnewquestion] CHECK CONSTRAINT [cms_models_historicalnewquestion_history_user_id_646fa9db_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalnextsectionbuttontypepopup]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalnextsectionbuttontypepopup_history_user_id_d79535e3_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalnextsectionbuttontypepopup] CHECK CONSTRAINT [cms_models_historicalnextsectionbuttontypepopup_history_user_id_d79535e3_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalnextsectionbuttontypeproceed]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalnextsectionbuttontypeproceed_history_user_id_702f68da_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalnextsectionbuttontypeproceed] CHECK CONSTRAINT [cms_models_historicalnextsectionbuttontypeproceed_history_user_id_702f68da_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalorderlesstestpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalorderlesstestpermissions_history_user_id_745dc4e7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalorderlesstestpermissions] CHECK CONSTRAINT [cms_models_historicalorderlesstestpermissions_history_user_id_745dc4e7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesection]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesection_history_user_id_637d9cfc_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesection] CHECK CONSTRAINT [cms_models_historicalpagesection_history_user_id_637d9cfc_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypeimagezoom]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesectiontypeimagezoom_history_user_id_3bad7e1b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypeimagezoom] CHECK CONSTRAINT [cms_models_historicalpagesectiontypeimagezoom_history_user_id_3bad7e1b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypeitembankinstruction]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesectiontypeitembankinstruction_history_user_id_612947a8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypeitembankinstruction] CHECK CONSTRAINT [cms_models_historicalpagesectiontypeitembankinstruction_history_user_id_612947a8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypemarkdown]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesectiontypemarkdown_history_user_id_ba0c29c9_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypemarkdown] CHECK CONSTRAINT [cms_models_historicalpagesectiontypemarkdown_history_user_id_ba0c29c9_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesampleemail]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesectiontypesampleemail_history_user_id_e5f75b8e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesampleemail] CHECK CONSTRAINT [cms_models_historicalpagesectiontypesampleemail_history_user_id_e5f75b8e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesampleemailresponse]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesectiontypesampleemailresponse_history_user_id_f14ecc3f_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesampleemailresponse] CHECK CONSTRAINT [cms_models_historicalpagesectiontypesampleemailresponse_history_user_id_f14ecc3f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesampletaskresponse]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesectiontypesampletaskresponse_history_user_id_be1e02c8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesampletaskresponse] CHECK CONSTRAINT [cms_models_historicalpagesectiontypesampletaskresponse_history_user_id_be1e02c8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesurveylink]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesectiontypesurveylink_history_user_id_6a80eccb_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesurveylink] CHECK CONSTRAINT [cms_models_historicalpagesectiontypesurveylink_history_user_id_6a80eccb_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesurveylinktext]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesectiontypesurveylinktext_history_user_id_f1fbf1a0_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypesurveylinktext] CHECK CONSTRAINT [cms_models_historicalpagesectiontypesurveylinktext_history_user_id_f1fbf1a0_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypetreedescription]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalpagesectiontypetreedescription_history_user_id_727dfa3e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalpagesectiontypetreedescription] CHECK CONSTRAINT [cms_models_historicalpagesectiontypetreedescription_history_user_id_727dfa3e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalquestionblocktype]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalquestionblocktype_history_user_id_a3a4cf61_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalquestionblocktype] CHECK CONSTRAINT [cms_models_historicalquestionblocktype_history_user_id_a3a4cf61_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalquestionlistrule]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalquestionlistrule_history_user_id_2e9fcc9e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalquestionlistrule] CHECK CONSTRAINT [cms_models_historicalquestionlistrule_history_user_id_2e9fcc9e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalquestionsection]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalquestionsection_history_user_id_1c550235_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalquestionsection] CHECK CONSTRAINT [cms_models_historicalquestionsection_history_user_id_1c550235_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalquestionsectiontypemarkdown]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalquestionsectiontypemarkdown_history_user_id_f1873bfb_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalquestionsectiontypemarkdown] CHECK CONSTRAINT [cms_models_historicalquestionsectiontypemarkdown_history_user_id_f1873bfb_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalquestionsituation]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalquestionsituation_history_user_id_bdaf9cc7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalquestionsituation] CHECK CONSTRAINT [cms_models_historicalquestionsituation_history_user_id_bdaf9cc7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalreducercontrol]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalreducercontrol_history_user_id_92c6c3b4_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalreducercontrol] CHECK CONSTRAINT [cms_models_historicalreducercontrol_history_user_id_92c6c3b4_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalscoringmethods]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalscoringmethods_history_user_id_9dd32302_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalscoringmethods] CHECK CONSTRAINT [cms_models_historicalscoringmethods_history_user_id_9dd32302_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalscoringmethodtypes]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalscoringmethodtypes_history_user_id_2b11daf1_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalscoringmethodtypes] CHECK CONSTRAINT [cms_models_historicalscoringmethodtypes_history_user_id_2b11daf1_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalscoringpassfail]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalscoringpassfail_history_user_id_8b7e957d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalscoringpassfail] CHECK CONSTRAINT [cms_models_historicalscoringpassfail_history_user_id_8b7e957d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalscoringraw]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalscoringraw_history_user_id_08618cdb_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalscoringraw] CHECK CONSTRAINT [cms_models_historicalscoringraw_history_user_id_08618cdb_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalscoringthreshold]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalscoringthreshold_history_user_id_c7fb14a6_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalscoringthreshold] CHECK CONSTRAINT [cms_models_historicalscoringthreshold_history_user_id_c7fb14a6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalsectioncomponentpage]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalsectioncomponentpage_history_user_id_68763e21_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalsectioncomponentpage] CHECK CONSTRAINT [cms_models_historicalsectioncomponentpage_history_user_id_68763e21_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalsituationexamplerating]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalsituationexamplerating_history_user_id_6e3cf95d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalsituationexamplerating] CHECK CONSTRAINT [cms_models_historicalsituationexamplerating_history_user_id_6e3cf95d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalsituationexampleratingdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalsituationexampleratingdetails_history_user_id_9c4de5ce_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalsituationexampleratingdetails] CHECK CONSTRAINT [cms_models_historicalsituationexampleratingdetails_history_user_id_9c4de5ce_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestdefinition]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestdefinition_history_user_id_0707a2c7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestdefinition] CHECK CONSTRAINT [cms_models_historicaltestdefinition_history_user_id_0707a2c7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestpermissions_history_user_id_dd4a0896_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestpermissions] CHECK CONSTRAINT [cms_models_historicaltestpermissions_history_user_id_dd4a0896_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestsection]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestsection_history_user_id_820e6892_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestsection] CHECK CONSTRAINT [cms_models_historicaltestsection_history_user_id_820e6892_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestsectioncomponent]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestsectioncomponent_history_user_id_973cd7d1_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestsectioncomponent] CHECK CONSTRAINT [cms_models_historicaltestsectioncomponent_history_user_id_973cd7d1_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestsectionreducer]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestsectionreducer_history_user_id_badc22a0_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestsectionreducer] CHECK CONSTRAINT [cms_models_historicaltestsectionreducer_history_user_id_badc22a0_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestskill]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestskill_history_user_id_4d4ce620_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestskill] CHECK CONSTRAINT [cms_models_historicaltestskill_history_user_id_4d4ce620_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestskilloccupational]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestskilloccupational_history_user_id_98edc47b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestskilloccupational] CHECK CONSTRAINT [cms_models_historicaltestskilloccupational_history_user_id_98edc47b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestskilloccupationaldesc]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestskilloccupationaldesc_history_user_id_e0a44bc8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestskilloccupationaldesc] CHECK CONSTRAINT [cms_models_historicaltestskilloccupationaldesc_history_user_id_e0a44bc8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestskilloccupationaldesctext]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestskilloccupationaldesctext_history_user_id_8ddb97d9_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestskilloccupationaldesctext] CHECK CONSTRAINT [cms_models_historicaltestskilloccupationaldesctext_history_user_id_8ddb97d9_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestskillsle]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestskillsle_history_user_id_65f47130_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestskillsle] CHECK CONSTRAINT [cms_models_historicaltestskillsle_history_user_id_65f47130_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestskillsledesc]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestskillsledesc_history_user_id_95d8bb37_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestskillsledesc] CHECK CONSTRAINT [cms_models_historicaltestskillsledesc_history_user_id_95d8bb37_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestskillsledesctext]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestskillsledesctext_history_user_id_9016484f_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestskillsledesctext] CHECK CONSTRAINT [cms_models_historicaltestskillsledesctext_history_user_id_9016484f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestskilltype]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestskilltype_history_user_id_4892185d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestskilltype] CHECK CONSTRAINT [cms_models_historicaltestskilltype_history_user_id_4892185d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicaltestskilltypetext]  WITH CHECK ADD  CONSTRAINT [cms_models_historicaltestskilltypetext_history_user_id_5b165acd_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicaltestskilltypetext] CHECK CONSTRAINT [cms_models_historicaltestskilltypetext_history_user_id_5b165acd_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_historicalviewedquestions]  WITH CHECK ADD  CONSTRAINT [cms_models_historicalviewedquestions_history_user_id_0390a32d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_historicalviewedquestions] CHECK CONSTRAINT [cms_models_historicalviewedquestions_history_user_id_0390a32d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_itemattributevalue]  WITH CHECK ADD  CONSTRAINT [cms_models_itemattributevalue_item_bank_attribute_value_id_d8372e5c_fk_cms_models_itembankattributevalues_id] FOREIGN KEY([item_bank_attribute_value_id])
REFERENCES [dbo].[cms_models_itembankattributevalues] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemattributevalue] CHECK CONSTRAINT [cms_models_itemattributevalue_item_bank_attribute_value_id_d8372e5c_fk_cms_models_itembankattributevalues_id]
GO
ALTER TABLE [dbo].[cms_models_itemattributevalue]  WITH CHECK ADD  CONSTRAINT [cms_models_itemattributevalue_item_id_8d85287b_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemattributevalue] CHECK CONSTRAINT [cms_models_itemattributevalue_item_id_8d85287b_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[cms_models_itemattributevaluedrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemattributevaluedrafts_item_bank_attribute_value_id_0b89d116_fk_cms_models_itembankattributevalues_id] FOREIGN KEY([item_bank_attribute_value_id])
REFERENCES [dbo].[cms_models_itembankattributevalues] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemattributevaluedrafts] CHECK CONSTRAINT [cms_models_itemattributevaluedrafts_item_bank_attribute_value_id_0b89d116_fk_cms_models_itembankattributevalues_id]
GO
ALTER TABLE [dbo].[cms_models_itemattributevaluedrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemattributevaluedrafts_item_id_73f7efa8_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemattributevaluedrafts] CHECK CONSTRAINT [cms_models_itemattributevaluedrafts_item_id_73f7efa8_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[cms_models_itemattributevaluedrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemattributevaluedrafts_user_id_7776a9b3_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemattributevaluedrafts] CHECK CONSTRAINT [cms_models_itemattributevaluedrafts_user_id_7776a9b3_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_itembank]  WITH CHECK ADD  CONSTRAINT [cms_models_itembank_language_id_d0cd7692_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_itembank] CHECK CONSTRAINT [cms_models_itembank_language_id_d0cd7692_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_itembankattributes]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankattributes_item_bank_id_3ce78b18_fk_cms_models_itembank_id] FOREIGN KEY([item_bank_id])
REFERENCES [dbo].[cms_models_itembank] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankattributes] CHECK CONSTRAINT [cms_models_itembankattributes_item_bank_id_3ce78b18_fk_cms_models_itembank_id]
GO
ALTER TABLE [dbo].[cms_models_itembankattributestext]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankattributestext_item_bank_attribute_id_cf1c91d8_fk_cms_models_itembankattributes_id] FOREIGN KEY([item_bank_attribute_id])
REFERENCES [dbo].[cms_models_itembankattributes] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankattributestext] CHECK CONSTRAINT [cms_models_itembankattributestext_item_bank_attribute_id_cf1c91d8_fk_cms_models_itembankattributes_id]
GO
ALTER TABLE [dbo].[cms_models_itembankattributestext]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankattributestext_language_id_253b28db_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_itembankattributestext] CHECK CONSTRAINT [cms_models_itembankattributestext_language_id_253b28db_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_itembankattributevalues]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankattributevalues_item_bank_attribute_id_f483be53_fk_cms_models_itembankattributes_id] FOREIGN KEY([item_bank_attribute_id])
REFERENCES [dbo].[cms_models_itembankattributes] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankattributevalues] CHECK CONSTRAINT [cms_models_itembankattributevalues_item_bank_attribute_id_f483be53_fk_cms_models_itembankattributes_id]
GO
ALTER TABLE [dbo].[cms_models_itembankattributevaluestext]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankattributevaluestext_item_bank_attribute_values_id_a7e74aa0_fk_cms_models_itembankattributevalues_id] FOREIGN KEY([item_bank_attribute_values_id])
REFERENCES [dbo].[cms_models_itembankattributevalues] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankattributevaluestext] CHECK CONSTRAINT [cms_models_itembankattributevaluestext_item_bank_attribute_values_id_a7e74aa0_fk_cms_models_itembankattributevalues_id]
GO
ALTER TABLE [dbo].[cms_models_itembankpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankpermissions_item_bank_access_type_id_16005ee4_fk_cms_models_itembankaccesstypes_id] FOREIGN KEY([item_bank_access_type_id])
REFERENCES [dbo].[cms_models_itembankaccesstypes] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankpermissions] CHECK CONSTRAINT [cms_models_itembankpermissions_item_bank_access_type_id_16005ee4_fk_cms_models_itembankaccesstypes_id]
GO
ALTER TABLE [dbo].[cms_models_itembankpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankpermissions_item_bank_id_64659e7d_fk_cms_models_itembank_id] FOREIGN KEY([item_bank_id])
REFERENCES [dbo].[cms_models_itembank] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankpermissions] CHECK CONSTRAINT [cms_models_itembankpermissions_item_bank_id_64659e7d_fk_cms_models_itembank_id]
GO
ALTER TABLE [dbo].[cms_models_itembankpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankpermissions_user_id_c93fb428_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankpermissions] CHECK CONSTRAINT [cms_models_itembankpermissions_user_id_c93fb428_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_itembankrule]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankrule_content_type_id_8f246e70_fk_cms_models_contenttype_id] FOREIGN KEY([content_type_id])
REFERENCES [dbo].[cms_models_contenttype] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankrule] CHECK CONSTRAINT [cms_models_itembankrule_content_type_id_8f246e70_fk_cms_models_contenttype_id]
GO
ALTER TABLE [dbo].[cms_models_itembankrule]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankrule_item_bank_bundle_id_aae4b25f_fk_cms_models_bundles_id] FOREIGN KEY([item_bank_bundle_id])
REFERENCES [dbo].[cms_models_bundles] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankrule] CHECK CONSTRAINT [cms_models_itembankrule_item_bank_bundle_id_aae4b25f_fk_cms_models_bundles_id]
GO
ALTER TABLE [dbo].[cms_models_itembankrule]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankrule_item_bank_id_29e59d03_fk_cms_models_itembank_id] FOREIGN KEY([item_bank_id])
REFERENCES [dbo].[cms_models_itembank] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankrule] CHECK CONSTRAINT [cms_models_itembankrule_item_bank_id_29e59d03_fk_cms_models_itembank_id]
GO
ALTER TABLE [dbo].[cms_models_itembankrule]  WITH CHECK ADD  CONSTRAINT [cms_models_itembankrule_test_section_component_id_47b6d059_fk_cms_models_testsectioncomponent_id] FOREIGN KEY([test_section_component_id])
REFERENCES [dbo].[cms_models_testsectioncomponent] ([id])
GO
ALTER TABLE [dbo].[cms_models_itembankrule] CHECK CONSTRAINT [cms_models_itembankrule_test_section_component_id_47b6d059_fk_cms_models_testsectioncomponent_id]
GO
ALTER TABLE [dbo].[cms_models_itemcomments]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcomments_user_id_3e2d5a19_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemcomments] CHECK CONSTRAINT [cms_models_itemcomments_user_id_3e2d5a19_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_itemcontent]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcontent_item_id_43e12653_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemcontent] CHECK CONSTRAINT [cms_models_itemcontent_item_id_43e12653_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[cms_models_itemcontentdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcontentdrafts_item_id_3284e8a9_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemcontentdrafts] CHECK CONSTRAINT [cms_models_itemcontentdrafts_item_id_3284e8a9_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[cms_models_itemcontentdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcontentdrafts_user_id_f841be69_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemcontentdrafts] CHECK CONSTRAINT [cms_models_itemcontentdrafts_user_id_f841be69_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_itemcontenttext]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcontenttext_item_content_id_b2168cac_fk_cms_models_itemcontent_id] FOREIGN KEY([item_content_id])
REFERENCES [dbo].[cms_models_itemcontent] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemcontenttext] CHECK CONSTRAINT [cms_models_itemcontenttext_item_content_id_b2168cac_fk_cms_models_itemcontent_id]
GO
ALTER TABLE [dbo].[cms_models_itemcontenttext]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcontenttext_language_id_16fe5c73_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_itemcontenttext] CHECK CONSTRAINT [cms_models_itemcontenttext_language_id_16fe5c73_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_itemcontenttextdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcontenttextdrafts_item_content_id_b1757bf8_fk_cms_models_itemcontentdrafts_id] FOREIGN KEY([item_content_id])
REFERENCES [dbo].[cms_models_itemcontentdrafts] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemcontenttextdrafts] CHECK CONSTRAINT [cms_models_itemcontenttextdrafts_item_content_id_b1757bf8_fk_cms_models_itemcontentdrafts_id]
GO
ALTER TABLE [dbo].[cms_models_itemcontenttextdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemcontenttextdrafts_language_id_a51222e0_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_itemcontenttextdrafts] CHECK CONSTRAINT [cms_models_itemcontenttextdrafts_language_id_a51222e0_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_itemdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemdrafts_development_status_id_bb6cfb29_fk_cms_models_itemdevelopmentstatuses_id] FOREIGN KEY([development_status_id])
REFERENCES [dbo].[cms_models_itemdevelopmentstatuses] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemdrafts] CHECK CONSTRAINT [cms_models_itemdrafts_development_status_id_bb6cfb29_fk_cms_models_itemdevelopmentstatuses_id]
GO
ALTER TABLE [dbo].[cms_models_itemdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemdrafts_item_bank_id_81328a9e_fk_cms_models_itembank_id] FOREIGN KEY([item_bank_id])
REFERENCES [dbo].[cms_models_itembank] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemdrafts] CHECK CONSTRAINT [cms_models_itemdrafts_item_bank_id_81328a9e_fk_cms_models_itembank_id]
GO
ALTER TABLE [dbo].[cms_models_itemdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemdrafts_item_id_0413531b_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemdrafts] CHECK CONSTRAINT [cms_models_itemdrafts_item_id_0413531b_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[cms_models_itemdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemdrafts_item_type_id_0b9389e4_fk_cms_models_itemtype_id] FOREIGN KEY([item_type_id])
REFERENCES [dbo].[cms_models_itemtype] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemdrafts] CHECK CONSTRAINT [cms_models_itemdrafts_item_type_id_0b9389e4_fk_cms_models_itemtype_id]
GO
ALTER TABLE [dbo].[cms_models_itemdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemdrafts_response_format_id_bac4b698_fk_cms_models_itemresponseformats_id] FOREIGN KEY([response_format_id])
REFERENCES [dbo].[cms_models_itemresponseformats] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemdrafts] CHECK CONSTRAINT [cms_models_itemdrafts_response_format_id_bac4b698_fk_cms_models_itemresponseformats_id]
GO
ALTER TABLE [dbo].[cms_models_itemdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemdrafts_user_id_d63ad8b7_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemdrafts] CHECK CONSTRAINT [cms_models_itemdrafts_user_id_d63ad8b7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_itemhistoricalids]  WITH CHECK ADD  CONSTRAINT [cms_models_itemhistoricalids_item_bank_id_3a1acc99_fk_cms_models_itembank_id] FOREIGN KEY([item_bank_id])
REFERENCES [dbo].[cms_models_itembank] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemhistoricalids] CHECK CONSTRAINT [cms_models_itemhistoricalids_item_bank_id_3a1acc99_fk_cms_models_itembank_id]
GO
ALTER TABLE [dbo].[cms_models_itemoption]  WITH CHECK ADD  CONSTRAINT [cms_models_itemoption_item_id_13766378_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemoption] CHECK CONSTRAINT [cms_models_itemoption_item_id_13766378_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[cms_models_itemoptiondrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemoptiondrafts_item_id_5c99e7fc_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemoptiondrafts] CHECK CONSTRAINT [cms_models_itemoptiondrafts_item_id_5c99e7fc_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[cms_models_itemoptiondrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemoptiondrafts_user_id_80736c5b_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemoptiondrafts] CHECK CONSTRAINT [cms_models_itemoptiondrafts_user_id_80736c5b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_itemoptiontext]  WITH CHECK ADD  CONSTRAINT [cms_models_itemoptiontext_item_option_id_973f732c_fk_cms_models_itemoption_id] FOREIGN KEY([item_option_id])
REFERENCES [dbo].[cms_models_itemoption] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemoptiontext] CHECK CONSTRAINT [cms_models_itemoptiontext_item_option_id_973f732c_fk_cms_models_itemoption_id]
GO
ALTER TABLE [dbo].[cms_models_itemoptiontext]  WITH CHECK ADD  CONSTRAINT [cms_models_itemoptiontext_language_id_6665c0f2_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_itemoptiontext] CHECK CONSTRAINT [cms_models_itemoptiontext_language_id_6665c0f2_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_itemoptiontextdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemoptiontextdrafts_item_option_id_82ec1a3f_fk_cms_models_itemoptiondrafts_id] FOREIGN KEY([item_option_id])
REFERENCES [dbo].[cms_models_itemoptiondrafts] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemoptiontextdrafts] CHECK CONSTRAINT [cms_models_itemoptiontextdrafts_item_option_id_82ec1a3f_fk_cms_models_itemoptiondrafts_id]
GO
ALTER TABLE [dbo].[cms_models_itemoptiontextdrafts]  WITH CHECK ADD  CONSTRAINT [cms_models_itemoptiontextdrafts_language_id_853b7c7a_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_itemoptiontextdrafts] CHECK CONSTRAINT [cms_models_itemoptiontextdrafts_language_id_853b7c7a_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_items]  WITH CHECK ADD  CONSTRAINT [cms_models_items_development_status_id_5fa3efeb_fk_cms_models_itemdevelopmentstatuses_id] FOREIGN KEY([development_status_id])
REFERENCES [dbo].[cms_models_itemdevelopmentstatuses] ([id])
GO
ALTER TABLE [dbo].[cms_models_items] CHECK CONSTRAINT [cms_models_items_development_status_id_5fa3efeb_fk_cms_models_itemdevelopmentstatuses_id]
GO
ALTER TABLE [dbo].[cms_models_items]  WITH CHECK ADD  CONSTRAINT [cms_models_items_item_bank_id_4d7618ce_fk_cms_models_itembank_id] FOREIGN KEY([item_bank_id])
REFERENCES [dbo].[cms_models_itembank] ([id])
GO
ALTER TABLE [dbo].[cms_models_items] CHECK CONSTRAINT [cms_models_items_item_bank_id_4d7618ce_fk_cms_models_itembank_id]
GO
ALTER TABLE [dbo].[cms_models_items]  WITH CHECK ADD  CONSTRAINT [cms_models_items_item_type_id_65c778ef_fk_cms_models_itemtype_id] FOREIGN KEY([item_type_id])
REFERENCES [dbo].[cms_models_itemtype] ([id])
GO
ALTER TABLE [dbo].[cms_models_items] CHECK CONSTRAINT [cms_models_items_item_type_id_65c778ef_fk_cms_models_itemtype_id]
GO
ALTER TABLE [dbo].[cms_models_items]  WITH CHECK ADD  CONSTRAINT [cms_models_items_last_modified_by_user_id_298b5b00_fk_user_management_models_user_id] FOREIGN KEY([last_modified_by_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_items] CHECK CONSTRAINT [cms_models_items_last_modified_by_user_id_298b5b00_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_items]  WITH CHECK ADD  CONSTRAINT [cms_models_items_response_format_id_05d3fd3a_fk_cms_models_itemresponseformats_id] FOREIGN KEY([response_format_id])
REFERENCES [dbo].[cms_models_itemresponseformats] ([id])
GO
ALTER TABLE [dbo].[cms_models_items] CHECK CONSTRAINT [cms_models_items_response_format_id_05d3fd3a_fk_cms_models_itemresponseformats_id]
GO
ALTER TABLE [dbo].[cms_models_itemtypetext]  WITH CHECK ADD  CONSTRAINT [cms_models_itemtypetext_item_type_id_1e3d067f_fk_cms_models_itemtype_id] FOREIGN KEY([item_type_id])
REFERENCES [dbo].[cms_models_itemtype] ([id])
GO
ALTER TABLE [dbo].[cms_models_itemtypetext] CHECK CONSTRAINT [cms_models_itemtypetext_item_type_id_1e3d067f_fk_cms_models_itemtype_id]
GO
ALTER TABLE [dbo].[cms_models_itemtypetext]  WITH CHECK ADD  CONSTRAINT [cms_models_itemtypetext_language_id_4ddd669d_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_itemtypetext] CHECK CONSTRAINT [cms_models_itemtypetext_language_id_4ddd669d_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_multiplechoicequestion]  WITH CHECK ADD  CONSTRAINT [cms_models_multiplechoicequestion_question_id_0a0ba0f5_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_multiplechoicequestion] CHECK CONSTRAINT [cms_models_multiplechoicequestion_question_id_0a0ba0f5_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_newquestion]  WITH CHECK ADD  CONSTRAINT [cms_models_newquestion_question_block_type_id_427156c0_fk_cms_models_questionblocktype_id] FOREIGN KEY([question_block_type_id])
REFERENCES [dbo].[cms_models_questionblocktype] ([id])
GO
ALTER TABLE [dbo].[cms_models_newquestion] CHECK CONSTRAINT [cms_models_newquestion_question_block_type_id_427156c0_fk_cms_models_questionblocktype_id]
GO
ALTER TABLE [dbo].[cms_models_newquestion]  WITH CHECK ADD  CONSTRAINT [cms_models_newquestion_test_section_component_id_91088323_fk_cms_models_testsectioncomponent_id] FOREIGN KEY([test_section_component_id])
REFERENCES [dbo].[cms_models_testsectioncomponent] ([id])
GO
ALTER TABLE [dbo].[cms_models_newquestion] CHECK CONSTRAINT [cms_models_newquestion_test_section_component_id_91088323_fk_cms_models_testsectioncomponent_id]
GO
ALTER TABLE [dbo].[cms_models_newquestion_dependencies]  WITH CHECK ADD  CONSTRAINT [cms_models_newquestion_dependencies_from_newquestion_id_d41f8c55_fk_cms_models_newquestion_id] FOREIGN KEY([from_newquestion_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_newquestion_dependencies] CHECK CONSTRAINT [cms_models_newquestion_dependencies_from_newquestion_id_d41f8c55_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_newquestion_dependencies]  WITH CHECK ADD  CONSTRAINT [cms_models_newquestion_dependencies_to_newquestion_id_f4ac3126_fk_cms_models_newquestion_id] FOREIGN KEY([to_newquestion_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_newquestion_dependencies] CHECK CONSTRAINT [cms_models_newquestion_dependencies_to_newquestion_id_f4ac3126_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypepopup]  WITH CHECK ADD  CONSTRAINT [cms_models_nextsectionbuttontypepopup_language_id_d78f96a9_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypepopup] CHECK CONSTRAINT [cms_models_nextsectionbuttontypepopup_language_id_d78f96a9_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypepopup]  WITH CHECK ADD  CONSTRAINT [cms_models_nextsectionbuttontypepopup_test_section_id_88d07a1e_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypepopup] CHECK CONSTRAINT [cms_models_nextsectionbuttontypepopup_test_section_id_88d07a1e_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypeproceed]  WITH CHECK ADD  CONSTRAINT [cms_models_nextsectionbuttontypeproceed_language_id_4884bd07_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypeproceed] CHECK CONSTRAINT [cms_models_nextsectionbuttontypeproceed_language_id_4884bd07_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypeproceed]  WITH CHECK ADD  CONSTRAINT [cms_models_nextsectionbuttontypeproceed_test_section_id_87c9536a_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[cms_models_nextsectionbuttontypeproceed] CHECK CONSTRAINT [cms_models_nextsectionbuttontypeproceed_test_section_id_87c9536a_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[cms_models_orderlesstestpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_orderlesstestpermissions_ta_extended_profile_id_24e99afe_fk_user_management_models_taextendedprofile_id] FOREIGN KEY([ta_extended_profile_id])
REFERENCES [dbo].[user_management_models_taextendedprofile] ([id])
GO
ALTER TABLE [dbo].[cms_models_orderlesstestpermissions] CHECK CONSTRAINT [cms_models_orderlesstestpermissions_ta_extended_profile_id_24e99afe_fk_user_management_models_taextendedprofile_id]
GO
ALTER TABLE [dbo].[cms_models_pagesection]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesection_section_component_page_id_5f2d5b02_fk_cms_models_sectioncomponentpage_id] FOREIGN KEY([section_component_page_id])
REFERENCES [dbo].[cms_models_sectioncomponentpage] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesection] CHECK CONSTRAINT [cms_models_pagesection_section_component_page_id_5f2d5b02_fk_cms_models_sectioncomponentpage_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypeimagezoom]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypeimagezoom_language_id_ea2bd6c0_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypeimagezoom] CHECK CONSTRAINT [cms_models_pagesectiontypeimagezoom_language_id_ea2bd6c0_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypeimagezoom]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypeimagezoom_page_section_id_2dc05d0b_fk_cms_models_pagesection_id] FOREIGN KEY([page_section_id])
REFERENCES [dbo].[cms_models_pagesection] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypeimagezoom] CHECK CONSTRAINT [cms_models_pagesectiontypeimagezoom_page_section_id_2dc05d0b_fk_cms_models_pagesection_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypeitembankinstruction]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypeitembankinstruction_page_section_id_3f49d03e_fk_cms_models_pagesection_id] FOREIGN KEY([page_section_id])
REFERENCES [dbo].[cms_models_pagesection] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypeitembankinstruction] CHECK CONSTRAINT [cms_models_pagesectiontypeitembankinstruction_page_section_id_3f49d03e_fk_cms_models_pagesection_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypemarkdown]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypemarkdown_language_id_6a33fb46_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypemarkdown] CHECK CONSTRAINT [cms_models_pagesectiontypemarkdown_language_id_6a33fb46_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypemarkdown]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypemarkdown_page_section_id_7056e4ea_fk_cms_models_pagesection_id] FOREIGN KEY([page_section_id])
REFERENCES [dbo].[cms_models_pagesection] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypemarkdown] CHECK CONSTRAINT [cms_models_pagesectiontypemarkdown_page_section_id_7056e4ea_fk_cms_models_pagesection_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemail]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypesampleemail_language_id_e2c97cd3_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemail] CHECK CONSTRAINT [cms_models_pagesectiontypesampleemail_language_id_e2c97cd3_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemail]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypesampleemail_page_section_id_2408a69f_fk_cms_models_pagesection_id] FOREIGN KEY([page_section_id])
REFERENCES [dbo].[cms_models_pagesection] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemail] CHECK CONSTRAINT [cms_models_pagesectiontypesampleemail_page_section_id_2408a69f_fk_cms_models_pagesection_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemailresponse]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypesampleemailresponse_language_id_ea345371_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemailresponse] CHECK CONSTRAINT [cms_models_pagesectiontypesampleemailresponse_language_id_ea345371_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemailresponse]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypesampleemailresponse_page_section_id_ee8b98df_fk_cms_models_pagesection_id] FOREIGN KEY([page_section_id])
REFERENCES [dbo].[cms_models_pagesection] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampleemailresponse] CHECK CONSTRAINT [cms_models_pagesectiontypesampleemailresponse_page_section_id_ee8b98df_fk_cms_models_pagesection_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampletaskresponse]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypesampletaskresponse_language_id_7d71a824_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampletaskresponse] CHECK CONSTRAINT [cms_models_pagesectiontypesampletaskresponse_language_id_7d71a824_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampletaskresponse]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypesampletaskresponse_page_section_id_c784cc05_fk_cms_models_pagesection_id] FOREIGN KEY([page_section_id])
REFERENCES [dbo].[cms_models_pagesection] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesampletaskresponse] CHECK CONSTRAINT [cms_models_pagesectiontypesampletaskresponse_page_section_id_c784cc05_fk_cms_models_pagesection_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesurveylink]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypesurveylink_page_section_id_fb5a7b70_fk_cms_models_pagesection_id] FOREIGN KEY([page_section_id])
REFERENCES [dbo].[cms_models_pagesection] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesurveylink] CHECK CONSTRAINT [cms_models_pagesectiontypesurveylink_page_section_id_fb5a7b70_fk_cms_models_pagesection_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesurveylinktext]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypesurveylinktext_language_id_e75242c0_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesurveylinktext] CHECK CONSTRAINT [cms_models_pagesectiontypesurveylinktext_language_id_e75242c0_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesurveylinktext]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypesurveylinktext_page_section_type_survey_link_id_a6e0ad83_fk_cms_models_pagesectiontypesurveylink_id] FOREIGN KEY([page_section_type_survey_link_id])
REFERENCES [dbo].[cms_models_pagesectiontypesurveylink] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypesurveylinktext] CHECK CONSTRAINT [cms_models_pagesectiontypesurveylinktext_page_section_type_survey_link_id_a6e0ad83_fk_cms_models_pagesectiontypesurveylink_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypetreedescription]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypetreedescription_address_book_contact_id_c2d58539_fk_cms_models_addressbookcontact_id] FOREIGN KEY([address_book_contact_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypetreedescription] CHECK CONSTRAINT [cms_models_pagesectiontypetreedescription_address_book_contact_id_c2d58539_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypetreedescription]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypetreedescription_language_id_71256d48_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypetreedescription] CHECK CONSTRAINT [cms_models_pagesectiontypetreedescription_language_id_71256d48_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypetreedescription]  WITH CHECK ADD  CONSTRAINT [cms_models_pagesectiontypetreedescription_page_section_id_a3dd572f_fk_cms_models_pagesection_id] FOREIGN KEY([page_section_id])
REFERENCES [dbo].[cms_models_pagesection] ([id])
GO
ALTER TABLE [dbo].[cms_models_pagesectiontypetreedescription] CHECK CONSTRAINT [cms_models_pagesectiontypetreedescription_page_section_id_a3dd572f_fk_cms_models_pagesection_id]
GO
ALTER TABLE [dbo].[cms_models_questionblocktype]  WITH CHECK ADD  CONSTRAINT [cms_models_questionblocktype_test_definition_id_6cbb6eae_fk_cms_models_testdefinition_id] FOREIGN KEY([test_definition_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[cms_models_questionblocktype] CHECK CONSTRAINT [cms_models_questionblocktype_test_definition_id_6cbb6eae_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[cms_models_questionlistrule]  WITH CHECK ADD  CONSTRAINT [cms_models_questionlistrule_test_section_component_id_5464ef8c_fk_cms_models_testsectioncomponent_id] FOREIGN KEY([test_section_component_id])
REFERENCES [dbo].[cms_models_testsectioncomponent] ([id])
GO
ALTER TABLE [dbo].[cms_models_questionlistrule] CHECK CONSTRAINT [cms_models_questionlistrule_test_section_component_id_5464ef8c_fk_cms_models_testsectioncomponent_id]
GO
ALTER TABLE [dbo].[cms_models_questionlistrule_question_block_type]  WITH CHECK ADD  CONSTRAINT [cms_models_questionlistrule_question_block_type_questionblocktype_id_fcad318b_fk_cms_models_questionblocktype_id] FOREIGN KEY([questionblocktype_id])
REFERENCES [dbo].[cms_models_questionblocktype] ([id])
GO
ALTER TABLE [dbo].[cms_models_questionlistrule_question_block_type] CHECK CONSTRAINT [cms_models_questionlistrule_question_block_type_questionblocktype_id_fcad318b_fk_cms_models_questionblocktype_id]
GO
ALTER TABLE [dbo].[cms_models_questionlistrule_question_block_type]  WITH CHECK ADD  CONSTRAINT [cms_models_questionlistrule_question_block_type_questionlistrule_id_c6d3d4c2_fk_cms_models_questionlistrule_id] FOREIGN KEY([questionlistrule_id])
REFERENCES [dbo].[cms_models_questionlistrule] ([id])
GO
ALTER TABLE [dbo].[cms_models_questionlistrule_question_block_type] CHECK CONSTRAINT [cms_models_questionlistrule_question_block_type_questionlistrule_id_c6d3d4c2_fk_cms_models_questionlistrule_id]
GO
ALTER TABLE [dbo].[cms_models_questionsection]  WITH CHECK ADD  CONSTRAINT [cms_models_questionsection_question_id_3008549b_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_questionsection] CHECK CONSTRAINT [cms_models_questionsection_question_id_3008549b_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_questionsectiontypemarkdown]  WITH CHECK ADD  CONSTRAINT [cms_models_questionsectiontypemarkdown_language_id_235f31cd_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_questionsectiontypemarkdown] CHECK CONSTRAINT [cms_models_questionsectiontypemarkdown_language_id_235f31cd_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_questionsectiontypemarkdown]  WITH CHECK ADD  CONSTRAINT [cms_models_questionsectiontypemarkdown_question_section_id_f8a74e6d_fk_cms_models_questionsection_id] FOREIGN KEY([question_section_id])
REFERENCES [dbo].[cms_models_questionsection] ([id])
GO
ALTER TABLE [dbo].[cms_models_questionsectiontypemarkdown] CHECK CONSTRAINT [cms_models_questionsectiontypemarkdown_question_section_id_f8a74e6d_fk_cms_models_questionsection_id]
GO
ALTER TABLE [dbo].[cms_models_questionsituation]  WITH CHECK ADD  CONSTRAINT [cms_models_questionsituation_language_id_c900854a_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_questionsituation] CHECK CONSTRAINT [cms_models_questionsituation_language_id_c900854a_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_questionsituation]  WITH CHECK ADD  CONSTRAINT [cms_models_questionsituation_question_id_38282eb0_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_questionsituation] CHECK CONSTRAINT [cms_models_questionsituation_question_id_38282eb0_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_scoringmethods]  WITH CHECK ADD  CONSTRAINT [cms_models_scoringmethods_method_type_id_43694ccc_fk_cms_models_scoringmethodtypes_id] FOREIGN KEY([method_type_id])
REFERENCES [dbo].[cms_models_scoringmethodtypes] ([id])
GO
ALTER TABLE [dbo].[cms_models_scoringmethods] CHECK CONSTRAINT [cms_models_scoringmethods_method_type_id_43694ccc_fk_cms_models_scoringmethodtypes_id]
GO
ALTER TABLE [dbo].[cms_models_scoringmethods]  WITH CHECK ADD  CONSTRAINT [cms_models_scoringmethods_test_id_3601f271_fk_cms_models_testdefinition_id] FOREIGN KEY([test_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[cms_models_scoringmethods] CHECK CONSTRAINT [cms_models_scoringmethods_test_id_3601f271_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[cms_models_scoringpassfail]  WITH CHECK ADD  CONSTRAINT [cms_models_scoringpassfail_scoring_method_id_991811eb_fk_cms_models_scoringmethods_id] FOREIGN KEY([scoring_method_id])
REFERENCES [dbo].[cms_models_scoringmethods] ([id])
GO
ALTER TABLE [dbo].[cms_models_scoringpassfail] CHECK CONSTRAINT [cms_models_scoringpassfail_scoring_method_id_991811eb_fk_cms_models_scoringmethods_id]
GO
ALTER TABLE [dbo].[cms_models_scoringraw]  WITH CHECK ADD  CONSTRAINT [cms_models_scoringraw_scoring_method_id_0c8d1d57_fk_cms_models_scoringmethods_id] FOREIGN KEY([scoring_method_id])
REFERENCES [dbo].[cms_models_scoringmethods] ([id])
GO
ALTER TABLE [dbo].[cms_models_scoringraw] CHECK CONSTRAINT [cms_models_scoringraw_scoring_method_id_0c8d1d57_fk_cms_models_scoringmethods_id]
GO
ALTER TABLE [dbo].[cms_models_scoringthreshold]  WITH CHECK ADD  CONSTRAINT [cms_models_scoringthreshold_scoring_method_id_03081867_fk_cms_models_scoringmethods_id] FOREIGN KEY([scoring_method_id])
REFERENCES [dbo].[cms_models_scoringmethods] ([id])
GO
ALTER TABLE [dbo].[cms_models_scoringthreshold] CHECK CONSTRAINT [cms_models_scoringthreshold_scoring_method_id_03081867_fk_cms_models_scoringmethods_id]
GO
ALTER TABLE [dbo].[cms_models_sectioncomponentpage]  WITH CHECK ADD  CONSTRAINT [cms_models_sectioncomponentpage_test_section_component_id_9728c6d0_fk_cms_models_testsectioncomponent_id] FOREIGN KEY([test_section_component_id])
REFERENCES [dbo].[cms_models_testsectioncomponent] ([id])
GO
ALTER TABLE [dbo].[cms_models_sectioncomponentpage] CHECK CONSTRAINT [cms_models_sectioncomponentpage_test_section_component_id_9728c6d0_fk_cms_models_testsectioncomponent_id]
GO
ALTER TABLE [dbo].[cms_models_situationexamplerating]  WITH CHECK ADD  CONSTRAINT [cms_models_situationexamplerating_competency_type_id_ac381784_fk_cms_models_competencytype_id] FOREIGN KEY([competency_type_id])
REFERENCES [dbo].[cms_models_competencytype] ([id])
GO
ALTER TABLE [dbo].[cms_models_situationexamplerating] CHECK CONSTRAINT [cms_models_situationexamplerating_competency_type_id_ac381784_fk_cms_models_competencytype_id]
GO
ALTER TABLE [dbo].[cms_models_situationexamplerating]  WITH CHECK ADD  CONSTRAINT [cms_models_situationexamplerating_question_id_49e6c856_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[cms_models_situationexamplerating] CHECK CONSTRAINT [cms_models_situationexamplerating_question_id_49e6c856_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[cms_models_situationexampleratingdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_situationexampleratingdetails_example_rating_id_104f8c7b_fk_cms_models_situationexamplerating_id] FOREIGN KEY([example_rating_id])
REFERENCES [dbo].[cms_models_situationexamplerating] ([id])
GO
ALTER TABLE [dbo].[cms_models_situationexampleratingdetails] CHECK CONSTRAINT [cms_models_situationexampleratingdetails_example_rating_id_104f8c7b_fk_cms_models_situationexamplerating_id]
GO
ALTER TABLE [dbo].[cms_models_situationexampleratingdetails]  WITH CHECK ADD  CONSTRAINT [cms_models_situationexampleratingdetails_language_id_dda792a3_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_situationexampleratingdetails] CHECK CONSTRAINT [cms_models_situationexampleratingdetails_language_id_dda792a3_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_testpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_testpermissions_test_id_8048e01b_fk_cms_models_testdefinition_id] FOREIGN KEY([test_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[cms_models_testpermissions] CHECK CONSTRAINT [cms_models_testpermissions_test_id_8048e01b_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[cms_models_testpermissions]  WITH CHECK ADD  CONSTRAINT [cms_models_testpermissions_user_id_d8dceccc_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_testpermissions] CHECK CONSTRAINT [cms_models_testpermissions_user_id_d8dceccc_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_testsection]  WITH CHECK ADD  CONSTRAINT [cms_models_testsection_test_definition_id_8528a699_fk_cms_models_testdefinition_id] FOREIGN KEY([test_definition_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[cms_models_testsection] CHECK CONSTRAINT [cms_models_testsection_test_definition_id_8528a699_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[cms_models_testsectioncomponent]  WITH CHECK ADD  CONSTRAINT [cms_models_testsectioncomponent_language_id_629fef2b_fk_custom_models_language_ISO_Code_1] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([ISO_Code_1])
GO
ALTER TABLE [dbo].[cms_models_testsectioncomponent] CHECK CONSTRAINT [cms_models_testsectioncomponent_language_id_629fef2b_fk_custom_models_language_ISO_Code_1]
GO
ALTER TABLE [dbo].[cms_models_testsectioncomponent_test_section]  WITH CHECK ADD  CONSTRAINT [cms_models_testsectioncomponent_test_section_testsection_id_94e1c7f9_fk_cms_models_testsection_id] FOREIGN KEY([testsection_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[cms_models_testsectioncomponent_test_section] CHECK CONSTRAINT [cms_models_testsectioncomponent_test_section_testsection_id_94e1c7f9_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[cms_models_testsectioncomponent_test_section]  WITH CHECK ADD  CONSTRAINT [cms_models_testsectioncomponent_test_section_testsectioncomponent_id_4be93fd7_fk_cms_models_testsectioncomponent_id] FOREIGN KEY([testsectioncomponent_id])
REFERENCES [dbo].[cms_models_testsectioncomponent] ([id])
GO
ALTER TABLE [dbo].[cms_models_testsectioncomponent_test_section] CHECK CONSTRAINT [cms_models_testsectioncomponent_test_section_testsectioncomponent_id_4be93fd7_fk_cms_models_testsectioncomponent_id]
GO
ALTER TABLE [dbo].[cms_models_testsectionreducer]  WITH CHECK ADD  CONSTRAINT [cms_models_testsectionreducer_reducer_id_f963998d_fk_cms_models_reducercontrol_reducer] FOREIGN KEY([reducer_id])
REFERENCES [dbo].[cms_models_reducercontrol] ([reducer])
GO
ALTER TABLE [dbo].[cms_models_testsectionreducer] CHECK CONSTRAINT [cms_models_testsectionreducer_reducer_id_f963998d_fk_cms_models_reducercontrol_reducer]
GO
ALTER TABLE [dbo].[cms_models_testsectionreducer]  WITH CHECK ADD  CONSTRAINT [cms_models_testsectionreducer_test_section_id_22fe65f5_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[cms_models_testsectionreducer] CHECK CONSTRAINT [cms_models_testsectionreducer_test_section_id_22fe65f5_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[cms_models_testskill]  WITH CHECK ADD  CONSTRAINT [cms_models_testskill_test_definition_id_8a099a46_fk_cms_models_testdefinition_id] FOREIGN KEY([test_definition_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[cms_models_testskill] CHECK CONSTRAINT [cms_models_testskill_test_definition_id_8a099a46_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[cms_models_testskill]  WITH CHECK ADD  CONSTRAINT [cms_models_testskill_test_skill_type_id_16b260d6_fk_cms_models_testskilltype_id] FOREIGN KEY([test_skill_type_id])
REFERENCES [dbo].[cms_models_testskilltype] ([id])
GO
ALTER TABLE [dbo].[cms_models_testskill] CHECK CONSTRAINT [cms_models_testskill_test_skill_type_id_16b260d6_fk_cms_models_testskilltype_id]
GO
ALTER TABLE [dbo].[cms_models_testskilloccupational]  WITH CHECK ADD  CONSTRAINT [cms_models_testskilloccupational_test_skill_id_b732043b_fk_cms_models_testskill_id] FOREIGN KEY([test_skill_id])
REFERENCES [dbo].[cms_models_testskill] ([id])
GO
ALTER TABLE [dbo].[cms_models_testskilloccupational] CHECK CONSTRAINT [cms_models_testskilloccupational_test_skill_id_b732043b_fk_cms_models_testskill_id]
GO
ALTER TABLE [dbo].[cms_models_testskilloccupational]  WITH CHECK ADD  CONSTRAINT [cms_models_testskilloccupational_test_skill_occupational_desc_id_513b1ae2_fk_cms_models_testskilloccupationaldesc_id] FOREIGN KEY([test_skill_occupational_desc_id])
REFERENCES [dbo].[cms_models_testskilloccupationaldesc] ([id])
GO
ALTER TABLE [dbo].[cms_models_testskilloccupational] CHECK CONSTRAINT [cms_models_testskilloccupational_test_skill_occupational_desc_id_513b1ae2_fk_cms_models_testskilloccupationaldesc_id]
GO
ALTER TABLE [dbo].[cms_models_testskilloccupationaldesctext]  WITH CHECK ADD  CONSTRAINT [cms_models_testskilloccupationaldesctext_language_id_3211fdfc_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_testskilloccupationaldesctext] CHECK CONSTRAINT [cms_models_testskilloccupationaldesctext_language_id_3211fdfc_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_testskilloccupationaldesctext]  WITH CHECK ADD  CONSTRAINT [cms_models_testskilloccupationaldesctext_test_skill_occupational_desc_id_f18c59ed_fk_cms_models_testskilloccupationaldesc_id] FOREIGN KEY([test_skill_occupational_desc_id])
REFERENCES [dbo].[cms_models_testskilloccupationaldesc] ([id])
GO
ALTER TABLE [dbo].[cms_models_testskilloccupationaldesctext] CHECK CONSTRAINT [cms_models_testskilloccupationaldesctext_test_skill_occupational_desc_id_f18c59ed_fk_cms_models_testskilloccupationaldesc_id]
GO
ALTER TABLE [dbo].[cms_models_testskillsle]  WITH CHECK ADD  CONSTRAINT [cms_models_testskillsle_test_skill_id_c2b4afa6_fk_cms_models_testskill_id] FOREIGN KEY([test_skill_id])
REFERENCES [dbo].[cms_models_testskill] ([id])
GO
ALTER TABLE [dbo].[cms_models_testskillsle] CHECK CONSTRAINT [cms_models_testskillsle_test_skill_id_c2b4afa6_fk_cms_models_testskill_id]
GO
ALTER TABLE [dbo].[cms_models_testskillsle]  WITH CHECK ADD  CONSTRAINT [cms_models_testskillsle_test_skill_sle_desc_id_0cab4608_fk_cms_models_testskillsledesc_id] FOREIGN KEY([test_skill_sle_desc_id])
REFERENCES [dbo].[cms_models_testskillsledesc] ([id])
GO
ALTER TABLE [dbo].[cms_models_testskillsle] CHECK CONSTRAINT [cms_models_testskillsle_test_skill_sle_desc_id_0cab4608_fk_cms_models_testskillsledesc_id]
GO
ALTER TABLE [dbo].[cms_models_testskillsledesctext]  WITH CHECK ADD  CONSTRAINT [cms_models_testskillsledesctext_language_id_03542c46_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_testskillsledesctext] CHECK CONSTRAINT [cms_models_testskillsledesctext_language_id_03542c46_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_testskillsledesctext]  WITH CHECK ADD  CONSTRAINT [cms_models_testskillsledesctext_test_skill_sle_desc_id_5102d069_fk_cms_models_testskillsledesc_id] FOREIGN KEY([test_skill_sle_desc_id])
REFERENCES [dbo].[cms_models_testskillsledesc] ([id])
GO
ALTER TABLE [dbo].[cms_models_testskillsledesctext] CHECK CONSTRAINT [cms_models_testskillsledesctext_test_skill_sle_desc_id_5102d069_fk_cms_models_testskillsledesc_id]
GO
ALTER TABLE [dbo].[cms_models_testskilltypetext]  WITH CHECK ADD  CONSTRAINT [cms_models_testskilltypetext_language_id_562595a0_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[cms_models_testskilltypetext] CHECK CONSTRAINT [cms_models_testskilltypetext_language_id_562595a0_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[cms_models_testskilltypetext]  WITH CHECK ADD  CONSTRAINT [cms_models_testskilltypetext_test_skill_type_id_f499424f_fk_cms_models_testskilltype_id] FOREIGN KEY([test_skill_type_id])
REFERENCES [dbo].[cms_models_testskilltype] ([id])
GO
ALTER TABLE [dbo].[cms_models_testskilltypetext] CHECK CONSTRAINT [cms_models_testskilltypetext_test_skill_type_id_f499424f_fk_cms_models_testskilltype_id]
GO
ALTER TABLE [dbo].[cms_models_viewedquestions]  WITH CHECK ADD  CONSTRAINT [cms_models_viewedquestions_user_id_363d8337_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[cms_models_viewedquestions] CHECK CONSTRAINT [cms_models_viewedquestions_user_id_363d8337_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_accommodationrequest]  WITH CHECK ADD  CONSTRAINT [custom_models_accommodationrequest_break_bank_id_4981951a_fk_custom_models_breakbank_id] FOREIGN KEY([break_bank_id])
REFERENCES [dbo].[custom_models_breakbank] ([id])
GO
ALTER TABLE [dbo].[custom_models_accommodationrequest] CHECK CONSTRAINT [custom_models_accommodationrequest_break_bank_id_4981951a_fk_custom_models_breakbank_id]
GO
ALTER TABLE [dbo].[custom_models_additionaltime]  WITH CHECK ADD  CONSTRAINT [custom_models_additionaltime_accommodation_request_id_1c869f3a_fk_custom_models_accommodationrequest_id] FOREIGN KEY([accommodation_request_id])
REFERENCES [dbo].[custom_models_accommodationrequest] ([id])
GO
ALTER TABLE [dbo].[custom_models_additionaltime] CHECK CONSTRAINT [custom_models_additionaltime_accommodation_request_id_1c869f3a_fk_custom_models_accommodationrequest_id]
GO
ALTER TABLE [dbo].[custom_models_additionaltime]  WITH CHECK ADD  CONSTRAINT [custom_models_additionaltime_test_section_id_c5195f14_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[custom_models_additionaltime] CHECK CONSTRAINT [custom_models_additionaltime_test_section_id_c5195f14_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[custom_models_assessmentprocess]  WITH CHECK ADD  CONSTRAINT [custom_models_assessmentprocess_default_billing_contact_id_e3910a1f_fk_custom_models_billingcontact_id] FOREIGN KEY([default_billing_contact_id])
REFERENCES [dbo].[custom_models_billingcontact] ([id])
GO
ALTER TABLE [dbo].[custom_models_assessmentprocess] CHECK CONSTRAINT [custom_models_assessmentprocess_default_billing_contact_id_e3910a1f_fk_custom_models_billingcontact_id]
GO
ALTER TABLE [dbo].[custom_models_assessmentprocess]  WITH CHECK ADD  CONSTRAINT [custom_models_assessmentprocess_user_id_ca4b922f_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_assessmentprocess] CHECK CONSTRAINT [custom_models_assessmentprocess_user_id_ca4b922f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_assessmentprocessassignedtestspecs]  WITH CHECK ADD  CONSTRAINT [custom_models_assessmentprocessassignedtestspecs_assessment_process_id_20ee4bd6_fk_custom_models_assessmentprocess_id] FOREIGN KEY([assessment_process_id])
REFERENCES [dbo].[custom_models_assessmentprocess] ([id])
GO
ALTER TABLE [dbo].[custom_models_assessmentprocessassignedtestspecs] CHECK CONSTRAINT [custom_models_assessmentprocessassignedtestspecs_assessment_process_id_20ee4bd6_fk_custom_models_assessmentprocess_id]
GO
ALTER TABLE [dbo].[custom_models_assessmentprocessassignedtestspecs]  WITH CHECK ADD  CONSTRAINT [custom_models_assessmentprocessassignedtestspecs_billing_contact_id_60259a3a_fk_custom_models_billingcontact_id] FOREIGN KEY([billing_contact_id])
REFERENCES [dbo].[custom_models_billingcontact] ([id])
GO
ALTER TABLE [dbo].[custom_models_assessmentprocessassignedtestspecs] CHECK CONSTRAINT [custom_models_assessmentprocessassignedtestspecs_billing_contact_id_60259a3a_fk_custom_models_billingcontact_id]
GO
ALTER TABLE [dbo].[custom_models_assessmentprocessassignedtestspecs]  WITH CHECK ADD  CONSTRAINT [custom_models_assessmentprocessassignedtestspecs_reason_for_testing_id_b76a8945_fk_custom_models_reasonfortesting_id] FOREIGN KEY([reason_for_testing_id])
REFERENCES [dbo].[custom_models_reasonfortesting] ([id])
GO
ALTER TABLE [dbo].[custom_models_assessmentprocessassignedtestspecs] CHECK CONSTRAINT [custom_models_assessmentprocessassignedtestspecs_reason_for_testing_id_b76a8945_fk_custom_models_reasonfortesting_id]
GO
ALTER TABLE [dbo].[custom_models_assessmentprocessassignedtestspecs]  WITH CHECK ADD  CONSTRAINT [custom_models_assessmentprocessassignedtestspecs_test_skill_type_id_09d38c1d_fk_cms_models_testskilltype_id] FOREIGN KEY([test_skill_type_id])
REFERENCES [dbo].[cms_models_testskilltype] ([id])
GO
ALTER TABLE [dbo].[custom_models_assessmentprocessassignedtestspecs] CHECK CONSTRAINT [custom_models_assessmentprocessassignedtestspecs_test_skill_type_id_09d38c1d_fk_cms_models_testskilltype_id]
GO
ALTER TABLE [dbo].[custom_models_assessmentprocessdefaulttestspecs]  WITH CHECK ADD  CONSTRAINT [custom_models_assessmentprocessdefaulttestspecs_assessment_process_id_c6f8c308_fk_custom_models_assessmentprocess_id] FOREIGN KEY([assessment_process_id])
REFERENCES [dbo].[custom_models_assessmentprocess] ([id])
GO
ALTER TABLE [dbo].[custom_models_assessmentprocessdefaulttestspecs] CHECK CONSTRAINT [custom_models_assessmentprocessdefaulttestspecs_assessment_process_id_c6f8c308_fk_custom_models_assessmentprocess_id]
GO
ALTER TABLE [dbo].[custom_models_assessmentprocessdefaulttestspecs]  WITH CHECK ADD  CONSTRAINT [custom_models_assessmentprocessdefaulttestspecs_reason_for_testing_id_bddf4e79_fk_custom_models_reasonfortesting_id] FOREIGN KEY([reason_for_testing_id])
REFERENCES [dbo].[custom_models_reasonfortesting] ([id])
GO
ALTER TABLE [dbo].[custom_models_assessmentprocessdefaulttestspecs] CHECK CONSTRAINT [custom_models_assessmentprocessdefaulttestspecs_reason_for_testing_id_bddf4e79_fk_custom_models_reasonfortesting_id]
GO
ALTER TABLE [dbo].[custom_models_assessmentprocessdefaulttestspecs]  WITH CHECK ADD  CONSTRAINT [custom_models_assessmentprocessdefaulttestspecs_test_skill_type_id_1546393c_fk_cms_models_testskilltype_id] FOREIGN KEY([test_skill_type_id])
REFERENCES [dbo].[cms_models_testskilltype] ([id])
GO
ALTER TABLE [dbo].[custom_models_assessmentprocessdefaulttestspecs] CHECK CONSTRAINT [custom_models_assessmentprocessdefaulttestspecs_test_skill_type_id_1546393c_fk_cms_models_testskilltype_id]
GO
ALTER TABLE [dbo].[custom_models_assignedanswerchoices]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedanswerchoices_assigned_test_id_f747586b_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedanswerchoices] CHECK CONSTRAINT [custom_models_assignedanswerchoices_assigned_test_id_f747586b_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_assignedanswerchoices]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedanswerchoices_item_id_df0a3d61_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedanswerchoices] CHECK CONSTRAINT [custom_models_assignedanswerchoices_item_id_df0a3d61_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[custom_models_assignedanswerchoices]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedanswerchoices_question_id_3605d91d_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedanswerchoices] CHECK CONSTRAINT [custom_models_assignedanswerchoices_question_id_3605d91d_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[custom_models_assignedquestionslist]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedquestionslist_assigned_test_id_3853595b_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedquestionslist] CHECK CONSTRAINT [custom_models_assignedquestionslist_assigned_test_id_3853595b_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_assignedquestionslist]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedquestionslist_test_section_id_98703e7c_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedquestionslist] CHECK CONSTRAINT [custom_models_assignedquestionslist_test_section_id_98703e7c_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_accommodation_request_id_bed6e914_fk_custom_models_accommodationrequest_id] FOREIGN KEY([accommodation_request_id])
REFERENCES [dbo].[custom_models_accommodationrequest] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_accommodation_request_id_bed6e914_fk_custom_models_accommodationrequest_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_orderless_financial_data_id_7f81cd1b_fk_custom_models_orderlessfinancialdata_id] FOREIGN KEY([orderless_financial_data_id])
REFERENCES [dbo].[custom_models_orderlessfinancialdata] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_orderless_financial_data_id_7f81cd1b_fk_custom_models_orderlessfinancialdata_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_previous_status_id_b176a09b_fk_custom_models_assignedteststatus_id] FOREIGN KEY([previous_status_id])
REFERENCES [dbo].[custom_models_assignedteststatus] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_previous_status_id_b176a09b_fk_custom_models_assignedteststatus_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_status_id_90b641db_fk_custom_models_assignedteststatus_id] FOREIGN KEY([status_id])
REFERENCES [dbo].[custom_models_assignedteststatus] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_status_id_90b641db_fk_custom_models_assignedteststatus_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_ta_user_id_b92176b3_fk_user_management_models_user_id] FOREIGN KEY([ta_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_ta_user_id_b92176b3_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_test_id_3f85b2bd_fk_cms_models_testdefinition_id] FOREIGN KEY([test_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_test_id_3f85b2bd_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_test_section_id_4b099ca4_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_test_section_id_4b099ca4_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_test_session_id_b32aef61_fk_custom_models_testcentertestsessions_id] FOREIGN KEY([test_session_id])
REFERENCES [dbo].[custom_models_testcentertestsessions] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_test_session_id_b32aef61_fk_custom_models_testcentertestsessions_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_test_session_language_id_51c9e503_fk_custom_models_language_language_id] FOREIGN KEY([test_session_language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_test_session_language_id_51c9e503_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_user_accommodation_file_id_6e03a4be_fk_custom_models_useraccommodationfile_id] FOREIGN KEY([user_accommodation_file_id])
REFERENCES [dbo].[custom_models_useraccommodationfile] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_user_accommodation_file_id_6e03a4be_fk_custom_models_useraccommodationfile_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtest_user_id_bb2be315_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtest] CHECK CONSTRAINT [custom_models_assignedtest_user_id_bb2be315_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtestanswerscore]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtestanswerscore_competency_id_d76589c4_fk_cms_models_competencytype_id] FOREIGN KEY([competency_id])
REFERENCES [dbo].[cms_models_competencytype] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtestanswerscore] CHECK CONSTRAINT [custom_models_assignedtestanswerscore_competency_id_d76589c4_fk_cms_models_competencytype_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtestanswerscore]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtestanswerscore_question_id_b91975dc_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtestanswerscore] CHECK CONSTRAINT [custom_models_assignedtestanswerscore_question_id_b91975dc_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtestanswerscore]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtestanswerscore_scorer_assigned_test_id_10b59bb0_fk_custom_models_testscorerassignment_id] FOREIGN KEY([scorer_assigned_test_id])
REFERENCES [dbo].[custom_models_testscorerassignment] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtestanswerscore] CHECK CONSTRAINT [custom_models_assignedtestanswerscore_scorer_assigned_test_id_10b59bb0_fk_custom_models_testscorerassignment_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtestsection]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtestsection_assigned_test_id_987da5cb_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtestsection] CHECK CONSTRAINT [custom_models_assignedtestsection_assigned_test_id_987da5cb_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtestsection]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtestsection_test_section_id_6ad5b8e1_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtestsection] CHECK CONSTRAINT [custom_models_assignedtestsection_test_section_id_6ad5b8e1_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[custom_models_assignedtestsectionaccesstimes]  WITH CHECK ADD  CONSTRAINT [custom_models_assignedtestsectionaccesstimes_assigned_test_section_id_791d317d_fk_custom_models_assignedtestsection_id] FOREIGN KEY([assigned_test_section_id])
REFERENCES [dbo].[custom_models_assignedtestsection] ([id])
GO
ALTER TABLE [dbo].[custom_models_assignedtestsectionaccesstimes] CHECK CONSTRAINT [custom_models_assignedtestsectionaccesstimes_assigned_test_section_id_791d317d_fk_custom_models_assignedtestsection_id]
GO
ALTER TABLE [dbo].[custom_models_billingcontact]  WITH CHECK ADD  CONSTRAINT [custom_models_billingcontact_user_id_1ade3b1e_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_billingcontact] CHECK CONSTRAINT [custom_models_billingcontact_user_id_1ade3b1e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_breakbankactions]  WITH CHECK ADD  CONSTRAINT [custom_models_breakbankactions_break_bank_id_c476530a_fk_custom_models_breakbank_id] FOREIGN KEY([break_bank_id])
REFERENCES [dbo].[custom_models_breakbank] ([id])
GO
ALTER TABLE [dbo].[custom_models_breakbankactions] CHECK CONSTRAINT [custom_models_breakbankactions_break_bank_id_c476530a_fk_custom_models_breakbank_id]
GO
ALTER TABLE [dbo].[custom_models_breakbankactions]  WITH CHECK ADD  CONSTRAINT [custom_models_breakbankactions_test_section_id_84fc0f7e_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[custom_models_breakbankactions] CHECK CONSTRAINT [custom_models_breakbankactions_test_section_id_84fc0f7e_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[custom_models_candidateanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateanswers_assigned_test_id_b9450261_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateanswers] CHECK CONSTRAINT [custom_models_candidateanswers_assigned_test_id_b9450261_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_candidateanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateanswers_item_id_e674b8d3_fk_cms_models_items_id] FOREIGN KEY([item_id])
REFERENCES [dbo].[cms_models_items] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateanswers] CHECK CONSTRAINT [custom_models_candidateanswers_item_id_e674b8d3_fk_cms_models_items_id]
GO
ALTER TABLE [dbo].[custom_models_candidateanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateanswers_question_id_b058e438_fk_cms_models_newquestion_id] FOREIGN KEY([question_id])
REFERENCES [dbo].[cms_models_newquestion] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateanswers] CHECK CONSTRAINT [custom_models_candidateanswers_question_id_b058e438_fk_cms_models_newquestion_id]
GO
ALTER TABLE [dbo].[custom_models_candidateanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateanswers_selected_language_id_fabe2905_fk_custom_models_language_language_id] FOREIGN KEY([selected_language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_candidateanswers] CHECK CONSTRAINT [custom_models_candidateanswers_selected_language_id_fabe2905_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_candidateanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateanswers_test_section_component_id_d53f08dc_fk_cms_models_testsectioncomponent_id] FOREIGN KEY([test_section_component_id])
REFERENCES [dbo].[cms_models_testsectioncomponent] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateanswers] CHECK CONSTRAINT [custom_models_candidateanswers_test_section_component_id_d53f08dc_fk_cms_models_testsectioncomponent_id]
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateemailresponseanswers_candidate_answers_id_778d7cf8_fk_custom_models_candidateanswers_id] FOREIGN KEY([candidate_answers_id])
REFERENCES [dbo].[custom_models_candidateanswers] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers] CHECK CONSTRAINT [custom_models_candidateemailresponseanswers_candidate_answers_id_778d7cf8_fk_custom_models_candidateanswers_id]
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_cc]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateemailresponseanswer_candidateemailresponseanswers_id_133a2c98_fk_custom_models_candidateemailre] FOREIGN KEY([candidateemailresponseanswers_id])
REFERENCES [dbo].[custom_models_candidateemailresponseanswers] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_cc] CHECK CONSTRAINT [custom_models_candidateemailresponseanswer_candidateemailresponseanswers_id_133a2c98_fk_custom_models_candidateemailre]
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_cc]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateemailresponseanswers_email_cc_addressbookcontact_id_ec3b48ac_fk_cms_models_addressbookcontact_id] FOREIGN KEY([addressbookcontact_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_cc] CHECK CONSTRAINT [custom_models_candidateemailresponseanswers_email_cc_addressbookcontact_id_ec3b48ac_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_to]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateemailresponseanswer_candidateemailresponseanswers_id_6cf729a1_fk_custom_models_candidateemailre] FOREIGN KEY([candidateemailresponseanswers_id])
REFERENCES [dbo].[custom_models_candidateemailresponseanswers] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_to] CHECK CONSTRAINT [custom_models_candidateemailresponseanswer_candidateemailresponseanswers_id_6cf729a1_fk_custom_models_candidateemailre]
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_to]  WITH CHECK ADD  CONSTRAINT [custom_models_candidateemailresponseanswers_email_to_addressbookcontact_id_dd690d67_fk_cms_models_addressbookcontact_id] FOREIGN KEY([addressbookcontact_id])
REFERENCES [dbo].[cms_models_addressbookcontact] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidateemailresponseanswers_email_to] CHECK CONSTRAINT [custom_models_candidateemailresponseanswers_email_to_addressbookcontact_id_dd690d67_fk_cms_models_addressbookcontact_id]
GO
ALTER TABLE [dbo].[custom_models_candidatemultiplechoiceanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidatemultiplechoiceanswers_answer_id_48929347_fk_cms_models_answer_id] FOREIGN KEY([answer_id])
REFERENCES [dbo].[cms_models_answer] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidatemultiplechoiceanswers] CHECK CONSTRAINT [custom_models_candidatemultiplechoiceanswers_answer_id_48929347_fk_cms_models_answer_id]
GO
ALTER TABLE [dbo].[custom_models_candidatemultiplechoiceanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidatemultiplechoiceanswers_candidate_answers_id_04d6bdbc_fk_custom_models_candidateanswers_id] FOREIGN KEY([candidate_answers_id])
REFERENCES [dbo].[custom_models_candidateanswers] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidatemultiplechoiceanswers] CHECK CONSTRAINT [custom_models_candidatemultiplechoiceanswers_candidate_answers_id_04d6bdbc_fk_custom_models_candidateanswers_id]
GO
ALTER TABLE [dbo].[custom_models_candidatetaskresponseanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_candidatetaskresponseanswers_candidate_answers_id_c31f131d_fk_custom_models_candidateanswers_id] FOREIGN KEY([candidate_answers_id])
REFERENCES [dbo].[custom_models_candidateanswers] ([id])
GO
ALTER TABLE [dbo].[custom_models_candidatetaskresponseanswers] CHECK CONSTRAINT [custom_models_candidatetaskresponseanswers_candidate_answers_id_c31f131d_fk_custom_models_candidateanswers_id]
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodes]  WITH CHECK ADD  CONSTRAINT [custom_models_consumedreservationcodes_assessment_process_assigned_test_specs_id_2ef736e3_fk_custom_models_assessmentproces] FOREIGN KEY([assessment_process_assigned_test_specs_id])
REFERENCES [dbo].[custom_models_assessmentprocessassignedtestspecs] ([id])
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodes] CHECK CONSTRAINT [custom_models_consumedreservationcodes_assessment_process_assigned_test_specs_id_2ef736e3_fk_custom_models_assessmentproces]
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodes]  WITH CHECK ADD  CONSTRAINT [custom_models_consumedreservationcodes_assigned_test_id_62d93a9e_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodes] CHECK CONSTRAINT [custom_models_consumedreservationcodes_assigned_test_id_62d93a9e_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodes]  WITH CHECK ADD  CONSTRAINT [custom_models_consumedreservationcodes_candidate_id_b43d9574_fk_user_management_models_user_id] FOREIGN KEY([candidate_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodes] CHECK CONSTRAINT [custom_models_consumedreservationcodes_candidate_id_b43d9574_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodes]  WITH CHECK ADD  CONSTRAINT [custom_models_consumedreservationcodes_status_id_0d58d758_fk_custom_models_consumedreservationcodestatus_id] FOREIGN KEY([status_id])
REFERENCES [dbo].[custom_models_consumedreservationcodestatus] ([id])
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodes] CHECK CONSTRAINT [custom_models_consumedreservationcodes_status_id_0d58d758_fk_custom_models_consumedreservationcodestatus_id]
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodes]  WITH CHECK ADD  CONSTRAINT [custom_models_consumedreservationcodes_test_session_id_1cf25080_fk_custom_models_testcentertestsessions_id] FOREIGN KEY([test_session_id])
REFERENCES [dbo].[custom_models_testcentertestsessions] ([id])
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodes] CHECK CONSTRAINT [custom_models_consumedreservationcodes_test_session_id_1cf25080_fk_custom_models_testcentertestsessions_id]
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodes]  WITH CHECK ADD  CONSTRAINT [custom_models_consumedreservationcodes_user_accommodation_file_id_ca701db4_fk_custom_models_useraccommodationfile_id] FOREIGN KEY([user_accommodation_file_id])
REFERENCES [dbo].[custom_models_useraccommodationfile] ([id])
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodes] CHECK CONSTRAINT [custom_models_consumedreservationcodes_user_accommodation_file_id_ca701db4_fk_custom_models_useraccommodationfile_id]
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodestatustext]  WITH CHECK ADD  CONSTRAINT [custom_models_consumedreservationcodestatu_consumed_reservation_code_status_id_f646aafa_fk_custom_models_consumedreservat] FOREIGN KEY([consumed_reservation_code_status_id])
REFERENCES [dbo].[custom_models_consumedreservationcodestatus] ([id])
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodestatustext] CHECK CONSTRAINT [custom_models_consumedreservationcodestatu_consumed_reservation_code_status_id_f646aafa_fk_custom_models_consumedreservat]
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodestatustext]  WITH CHECK ADD  CONSTRAINT [custom_models_consumedreservationcodestatustext_language_id_5d88bde3_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_consumedreservationcodestatustext] CHECK CONSTRAINT [custom_models_consumedreservationcodestatustext_language_id_5d88bde3_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_ettaactions]  WITH CHECK ADD  CONSTRAINT [custom_models_ettaactions_action_type_id_cb7d22be_fk_custom_models_ettaactiontypes_action_type] FOREIGN KEY([action_type_id])
REFERENCES [dbo].[custom_models_ettaactiontypes] ([action_type])
GO
ALTER TABLE [dbo].[custom_models_ettaactions] CHECK CONSTRAINT [custom_models_ettaactions_action_type_id_cb7d22be_fk_custom_models_ettaactiontypes_action_type]
GO
ALTER TABLE [dbo].[custom_models_ettaactions]  WITH CHECK ADD  CONSTRAINT [custom_models_ettaactions_assigned_test_id_72b6dd96_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_ettaactions] CHECK CONSTRAINT [custom_models_ettaactions_assigned_test_id_72b6dd96_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailfailedbulkdelivery]  WITH CHECK ADD  CONSTRAINT [custom_models_gcnotifyemailfailedbulkdelivery_assessment_process_id_db3a8394_fk_custom_models_assessmentprocess_id] FOREIGN KEY([assessment_process_id])
REFERENCES [dbo].[custom_models_assessmentprocess] ([id])
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailfailedbulkdelivery] CHECK CONSTRAINT [custom_models_gcnotifyemailfailedbulkdelivery_assessment_process_id_db3a8394_fk_custom_models_assessmentprocess_id]
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailfailedbulkdelivery]  WITH CHECK ADD  CONSTRAINT [custom_models_gcnotifyemailfailedbulkdelivery_gc_notify_template_id_3094af67_fk_custom_models_gcnotifytemplate_id] FOREIGN KEY([gc_notify_template_id])
REFERENCES [dbo].[custom_models_gcnotifytemplate] ([id])
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailfailedbulkdelivery] CHECK CONSTRAINT [custom_models_gcnotifyemailfailedbulkdelivery_gc_notify_template_id_3094af67_fk_custom_models_gcnotifytemplate_id]
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailfailedbulkdelivery]  WITH CHECK ADD  CONSTRAINT [custom_models_gcnotifyemailfailedbulkdelivery_uit_invite_id_1738e528_fk_custom_models_uitinvites_id] FOREIGN KEY([uit_invite_id])
REFERENCES [dbo].[custom_models_uitinvites] ([id])
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailfailedbulkdelivery] CHECK CONSTRAINT [custom_models_gcnotifyemailfailedbulkdelivery_uit_invite_id_1738e528_fk_custom_models_uitinvites_id]
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailpendingdelivery]  WITH CHECK ADD  CONSTRAINT [custom_models_gcnotifyemailpendingdelivery_assessment_process_id_cebc14b5_fk_custom_models_assessmentprocess_id] FOREIGN KEY([assessment_process_id])
REFERENCES [dbo].[custom_models_assessmentprocess] ([id])
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailpendingdelivery] CHECK CONSTRAINT [custom_models_gcnotifyemailpendingdelivery_assessment_process_id_cebc14b5_fk_custom_models_assessmentprocess_id]
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailpendingdelivery]  WITH CHECK ADD  CONSTRAINT [custom_models_gcnotifyemailpendingdelivery_gc_notify_template_id_169a7b49_fk_custom_models_gcnotifytemplate_id] FOREIGN KEY([gc_notify_template_id])
REFERENCES [dbo].[custom_models_gcnotifytemplate] ([id])
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailpendingdelivery] CHECK CONSTRAINT [custom_models_gcnotifyemailpendingdelivery_gc_notify_template_id_169a7b49_fk_custom_models_gcnotifytemplate_id]
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailpendingdelivery]  WITH CHECK ADD  CONSTRAINT [custom_models_gcnotifyemailpendingdelivery_uit_invite_id_8d6045b4_fk_custom_models_uitinvites_id] FOREIGN KEY([uit_invite_id])
REFERENCES [dbo].[custom_models_uitinvites] ([id])
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailpendingdelivery] CHECK CONSTRAINT [custom_models_gcnotifyemailpendingdelivery_uit_invite_id_8d6045b4_fk_custom_models_uitinvites_id]
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailverifiedattempteddelivery]  WITH CHECK ADD  CONSTRAINT [custom_models_gcnotifyemailverifiedattempteddelivery_assessment_process_id_cbddd202_fk_custom_models_assessmentprocess_id] FOREIGN KEY([assessment_process_id])
REFERENCES [dbo].[custom_models_assessmentprocess] ([id])
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailverifiedattempteddelivery] CHECK CONSTRAINT [custom_models_gcnotifyemailverifiedattempteddelivery_assessment_process_id_cbddd202_fk_custom_models_assessmentprocess_id]
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailverifiedattempteddelivery]  WITH CHECK ADD  CONSTRAINT [custom_models_gcnotifyemailverifiedattempteddelivery_gc_notify_template_id_e012afe1_fk_custom_models_gcnotifytemplate_id] FOREIGN KEY([gc_notify_template_id])
REFERENCES [dbo].[custom_models_gcnotifytemplate] ([id])
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailverifiedattempteddelivery] CHECK CONSTRAINT [custom_models_gcnotifyemailverifiedattempteddelivery_gc_notify_template_id_e012afe1_fk_custom_models_gcnotifytemplate_id]
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailverifiedattempteddelivery]  WITH CHECK ADD  CONSTRAINT [custom_models_gcnotifyemailverifiedattempteddelivery_uit_invite_id_69ff453c_fk_custom_models_uitinvites_id] FOREIGN KEY([uit_invite_id])
REFERENCES [dbo].[custom_models_uitinvites] ([id])
GO
ALTER TABLE [dbo].[custom_models_gcnotifyemailverifiedattempteddelivery] CHECK CONSTRAINT [custom_models_gcnotifyemailverifiedattempteddelivery_uit_invite_id_69ff453c_fk_custom_models_uitinvites_id]
GO
ALTER TABLE [dbo].[custom_models_historicalaccommodationrequest]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalaccommodationrequest_history_user_id_48058e1a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalaccommodationrequest] CHECK CONSTRAINT [custom_models_historicalaccommodationrequest_history_user_id_48058e1a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaladditionaltime]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaladditionaltime_history_user_id_79ecc5ad_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaladditionaltime] CHECK CONSTRAINT [custom_models_historicaladditionaltime_history_user_id_79ecc5ad_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassessmentprocess]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassessmentprocess_history_user_id_d36da1cc_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassessmentprocess] CHECK CONSTRAINT [custom_models_historicalassessmentprocess_history_user_id_d36da1cc_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassessmentprocessassignedtestspecs]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassessmentprocessassignedtestspecs_history_user_id_7ab465d2_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassessmentprocessassignedtestspecs] CHECK CONSTRAINT [custom_models_historicalassessmentprocessassignedtestspecs_history_user_id_7ab465d2_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassessmentprocessdefaulttestspecs]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassessmentprocessdefaulttestspecs_history_user_id_54b3e0a8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassessmentprocessdefaulttestspecs] CHECK CONSTRAINT [custom_models_historicalassessmentprocessdefaulttestspecs_history_user_id_54b3e0a8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassignedanswerchoices]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassignedanswerchoices_history_user_id_c362dfcc_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassignedanswerchoices] CHECK CONSTRAINT [custom_models_historicalassignedanswerchoices_history_user_id_c362dfcc_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassignedquestionslist]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassignedquestionslist_history_user_id_2432475f_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassignedquestionslist] CHECK CONSTRAINT [custom_models_historicalassignedquestionslist_history_user_id_2432475f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtest]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassignedtest_history_user_id_35caf1b3_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtest] CHECK CONSTRAINT [custom_models_historicalassignedtest_history_user_id_35caf1b3_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtestanswerscore]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassignedtestanswerscore_history_user_id_6d006e79_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtestanswerscore] CHECK CONSTRAINT [custom_models_historicalassignedtestanswerscore_history_user_id_6d006e79_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtestsection]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassignedtestsection_history_user_id_2bc9c72e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtestsection] CHECK CONSTRAINT [custom_models_historicalassignedtestsection_history_user_id_2bc9c72e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtestsectionaccesstimes]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassignedtestsectionaccesstimes_history_user_id_f8454526_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassignedtestsectionaccesstimes] CHECK CONSTRAINT [custom_models_historicalassignedtestsectionaccesstimes_history_user_id_f8454526_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalassignedteststatus]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalassignedteststatus_history_user_id_1fe2656d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalassignedteststatus] CHECK CONSTRAINT [custom_models_historicalassignedteststatus_history_user_id_1fe2656d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalbillingcontact]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalbillingcontact_history_user_id_3ccdcc9a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalbillingcontact] CHECK CONSTRAINT [custom_models_historicalbillingcontact_history_user_id_3ccdcc9a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalbreakbank]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalbreakbank_history_user_id_d599effa_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalbreakbank] CHECK CONSTRAINT [custom_models_historicalbreakbank_history_user_id_d599effa_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalbreakbankactions]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalbreakbankactions_history_user_id_96fa1293_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalbreakbankactions] CHECK CONSTRAINT [custom_models_historicalbreakbankactions_history_user_id_96fa1293_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalcandidateanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalcandidateanswers_history_user_id_01cd4f89_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalcandidateanswers] CHECK CONSTRAINT [custom_models_historicalcandidateanswers_history_user_id_01cd4f89_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalcandidateemailresponseanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalcandidateemailresponseanswers_history_user_id_2186ad1f_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalcandidateemailresponseanswers] CHECK CONSTRAINT [custom_models_historicalcandidateemailresponseanswers_history_user_id_2186ad1f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalcandidatemultiplechoiceanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalcandidatemultiplechoiceanswers_history_user_id_c8affaca_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalcandidatemultiplechoiceanswers] CHECK CONSTRAINT [custom_models_historicalcandidatemultiplechoiceanswers_history_user_id_c8affaca_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalcandidatetaskresponseanswers]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalcandidatetaskresponseanswers_history_user_id_bc92de0a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalcandidatetaskresponseanswers] CHECK CONSTRAINT [custom_models_historicalcandidatetaskresponseanswers_history_user_id_bc92de0a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalconsumedreservationcodes]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalconsumedreservationcodes_history_user_id_fd8e8de8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalconsumedreservationcodes] CHECK CONSTRAINT [custom_models_historicalconsumedreservationcodes_history_user_id_fd8e8de8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalcriticality]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalcriticality_history_user_id_a66b50d4_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalcriticality] CHECK CONSTRAINT [custom_models_historicalcriticality_history_user_id_a66b50d4_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalettaactions]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalettaactions_history_user_id_6c9d09f7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalettaactions] CHECK CONSTRAINT [custom_models_historicalettaactions_history_user_id_6c9d09f7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalettaactiontypes]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalettaactiontypes_history_user_id_7a3aa15e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalettaactiontypes] CHECK CONSTRAINT [custom_models_historicalettaactiontypes_history_user_id_7a3aa15e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicallanguage]  WITH CHECK ADD  CONSTRAINT [custom_models_historicallanguage_history_user_id_15b8a210_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicallanguage] CHECK CONSTRAINT [custom_models_historicallanguage_history_user_id_15b8a210_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicallanguagetext]  WITH CHECK ADD  CONSTRAINT [custom_models_historicallanguagetext_history_user_id_dc7e850a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicallanguagetext] CHECK CONSTRAINT [custom_models_historicallanguagetext_history_user_id_dc7e850a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicallocktestactions]  WITH CHECK ADD  CONSTRAINT [custom_models_historicallocktestactions_history_user_id_07a0396d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicallocktestactions] CHECK CONSTRAINT [custom_models_historicallocktestactions_history_user_id_07a0396d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalnotepad]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalnotepad_history_user_id_746fd39e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalnotepad] CHECK CONSTRAINT [custom_models_historicalnotepad_history_user_id_746fd39e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalolaglobalconfigs]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalolaglobalconfigs_history_user_id_340aa7c9_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalolaglobalconfigs] CHECK CONSTRAINT [custom_models_historicalolaglobalconfigs_history_user_id_340aa7c9_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalolaprioritization]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalolaprioritization_history_user_id_7c840ad6_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalolaprioritization] CHECK CONSTRAINT [custom_models_historicalolaprioritization_history_user_id_7c840ad6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalorderlessfinancialdata]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalorderlessfinancialdata_history_user_id_ceae4655_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalorderlessfinancialdata] CHECK CONSTRAINT [custom_models_historicalorderlessfinancialdata_history_user_id_ceae4655_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalpausetestactions]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalpausetestactions_history_user_id_200ee504_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalpausetestactions] CHECK CONSTRAINT [custom_models_historicalpausetestactions_history_user_id_200ee504_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalreasonfortesting]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalreasonfortesting_history_user_id_922bc154_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalreasonfortesting] CHECK CONSTRAINT [custom_models_historicalreasonfortesting_history_user_id_922bc154_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalreasonfortestingpriority]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalreasonfortestingpriority_history_user_id_27baa80e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalreasonfortestingpriority] CHECK CONSTRAINT [custom_models_historicalreasonfortestingpriority_history_user_id_27baa80e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalreasonfortestingprioritytext]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalreasonfortestingprioritytext_history_user_id_204b2030_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalreasonfortestingprioritytext] CHECK CONSTRAINT [custom_models_historicalreasonfortestingprioritytext_history_user_id_204b2030_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalreasonfortestingtext]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalreasonfortestingtext_history_user_id_4cacfe03_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalreasonfortestingtext] CHECK CONSTRAINT [custom_models_historicalreasonfortestingtext_history_user_id_4cacfe03_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalreasonfortestingtype]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalreasonfortestingtype_history_user_id_a5899455_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalreasonfortestingtype] CHECK CONSTRAINT [custom_models_historicalreasonfortestingtype_history_user_id_a5899455_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalreasonsfortesting]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalreasonsfortesting_history_user_id_44a16c2b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalreasonsfortesting] CHECK CONSTRAINT [custom_models_historicalreasonsfortesting_history_user_id_44a16c2b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalreservationcodes]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalreservationcodes_history_user_id_c5b3cfc7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalreservationcodes] CHECK CONSTRAINT [custom_models_historicalreservationcodes_history_user_id_c5b3cfc7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalscorerolaassignedtestsession]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalscorerolaassignedtestsession_history_user_id_77103a24_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalscorerolaassignedtestsession] CHECK CONSTRAINT [custom_models_historicalscorerolaassignedtestsession_history_user_id_77103a24_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalscorerolatestsessionskipaction]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalscorerolatestsessionskipaction_history_user_id_a5f95dc6_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalscorerolatestsessionskipaction] CHECK CONSTRAINT [custom_models_historicalscorerolatestsessionskipaction_history_user_id_a5f95dc6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalscorerolatestsessionskipoption]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalscorerolatestsessionskipoption_history_user_id_d77902d5_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalscorerolatestsessionskipoption] CHECK CONSTRAINT [custom_models_historicalscorerolatestsessionskipoption_history_user_id_d77902d5_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalscorerolatestsessionskipoptiontext]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalscorerolatestsessionskipoptiontext_history_user_id_00f8d682_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalscorerolatestsessionskipoptiontext] CHECK CONSTRAINT [custom_models_historicalscorerolatestsessionskipoptiontext_history_user_id_00f8d682_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalsiteadminsetting]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalsiteadminsetting_history_user_id_1ce6fef7_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalsiteadminsetting] CHECK CONSTRAINT [custom_models_historicalsiteadminsetting_history_user_id_1ce6fef7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalsystemalert]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalsystemalert_history_user_id_0a714e2a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalsystemalert] CHECK CONSTRAINT [custom_models_historicalsystemalert_history_user_id_0a714e2a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltaactions]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltaactions_history_user_id_08fb14d6_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltaactions] CHECK CONSTRAINT [custom_models_historicaltaactions_history_user_id_08fb14d6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltaactiontypes]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltaactiontypes_history_user_id_fd011f0b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltaactiontypes] CHECK CONSTRAINT [custom_models_historicaltaactiontypes_history_user_id_fd011f0b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestaccesscode_history_user_id_afcad320_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestaccesscode] CHECK CONSTRAINT [custom_models_historicaltestaccesscode_history_user_id_afcad320_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenter]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestcenter_history_user_id_69fe8b2e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenter] CHECK CONSTRAINT [custom_models_historicaltestcenter_history_user_id_69fe8b2e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenteraddresstext]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestcenteraddresstext_history_user_id_960a6fa2_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenteraddresstext] CHECK CONSTRAINT [custom_models_historicaltestcenteraddresstext_history_user_id_960a6fa2_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenterassociatedmanagers]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestcenterassociatedmanagers_history_user_id_d5effc5b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenterassociatedmanagers] CHECK CONSTRAINT [custom_models_historicaltestcenterassociatedmanagers_history_user_id_d5effc5b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestcentercitytext]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestcentercitytext_history_user_id_ad990f3c_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestcentercitytext] CHECK CONSTRAINT [custom_models_historicaltestcentercitytext_history_user_id_ad990f3c_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenterolaassessorunavailability]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestcenterolaassessorunavailability_history_user_id_ce266aee_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenterolaassessorunavailability] CHECK CONSTRAINT [custom_models_historicaltestcenterolaassessorunavailability_history_user_id_ce266aee_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenterotherdetailstext]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestcenterotherdetailstext_history_user_id_956a9007_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenterotherdetailstext] CHECK CONSTRAINT [custom_models_historicaltestcenterotherdetailstext_history_user_id_956a9007_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenterprovincetext]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestcenterprovincetext_history_user_id_06e4f168_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenterprovincetext] CHECK CONSTRAINT [custom_models_historicaltestcenterprovincetext_history_user_id_06e4f168_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenterroomdata]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestcenterroomdata_history_user_id_e0b914e6_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenterroomdata] CHECK CONSTRAINT [custom_models_historicaltestcenterroomdata_history_user_id_e0b914e6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenterroomdataotherdetailstext]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestcenterroomdataotherdetailstext_history_user_id_07a16c4d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenterroomdataotherdetailstext] CHECK CONSTRAINT [custom_models_historicaltestcenterroomdataotherdetailstext_history_user_id_07a16c4d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenterrooms]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestcenterrooms_history_user_id_74bde7b9_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestcenterrooms] CHECK CONSTRAINT [custom_models_historicaltestcenterrooms_history_user_id_74bde7b9_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestcentertestadministrators]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestcentertestadministrators_history_user_id_45836901_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestcentertestadministrators] CHECK CONSTRAINT [custom_models_historicaltestcentertestadministrators_history_user_id_45836901_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestcentertestsessiondata]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestcentertestsessiondata_history_user_id_0a53e555_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestcentertestsessiondata] CHECK CONSTRAINT [custom_models_historicaltestcentertestsessiondata_history_user_id_0a53e555_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestcentertestsessionmeetinguuid]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestcentertestsessionmeetinguuid_history_user_id_790c6a8b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestcentertestsessionmeetinguuid] CHECK CONSTRAINT [custom_models_historicaltestcentertestsessionmeetinguuid_history_user_id_790c6a8b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestcentertestsessionofficers]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestcentertestsessionofficers_history_user_id_89e57f7b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestcentertestsessionofficers] CHECK CONSTRAINT [custom_models_historicaltestcentertestsessionofficers_history_user_id_89e57f7b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestcentertestsessions]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestcentertestsessions_history_user_id_8a9857f5_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestcentertestsessions] CHECK CONSTRAINT [custom_models_historicaltestcentertestsessions_history_user_id_8a9857f5_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaltestscorerassignment]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaltestscorerassignment_history_user_id_fdae91ec_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaltestscorerassignment] CHECK CONSTRAINT [custom_models_historicaltestscorerassignment_history_user_id_fdae91ec_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluitinviterelatedcandidates]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluitinviterelatedcandidates_history_user_id_8663059b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluitinviterelatedcandidates] CHECK CONSTRAINT [custom_models_historicaluitinviterelatedcandidates_history_user_id_8663059b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluitinvites]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluitinvites_history_user_id_2f97b781_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluitinvites] CHECK CONSTRAINT [custom_models_historicaluitinvites_history_user_id_2f97b781_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluitreasonsfordeletion]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluitreasonsfordeletion_history_user_id_bd145b99_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluitreasonsfordeletion] CHECK CONSTRAINT [custom_models_historicaluitreasonsfordeletion_history_user_id_bd145b99_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluitreasonsformodification]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluitreasonsformodification_history_user_id_84a56fd9_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluitreasonsformodification] CHECK CONSTRAINT [custom_models_historicaluitreasonsformodification_history_user_id_84a56fd9_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicalunsupervisedtestaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_historicalunsupervisedtestaccesscode_history_user_id_a604e192_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicalunsupervisedtestaccesscode] CHECK CONSTRAINT [custom_models_historicalunsupervisedtestaccesscode_history_user_id_a604e192_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfile]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationfile_history_user_id_caf00c3a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfile] CHECK CONSTRAINT [custom_models_historicaluseraccommodationfile_history_user_id_caf00c3a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilebreakbank]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationfilebreakbank_history_user_id_c7031081_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilebreakbank] CHECK CONSTRAINT [custom_models_historicaluseraccommodationfilebreakbank_history_user_id_c7031081_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilecandidatelimitations]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationfilecandidatelimitations_history_user_id_9865d896_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilecandidatelimitations] CHECK CONSTRAINT [custom_models_historicaluseraccommodationfilecandidatelimitations_history_user_id_9865d896_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilecandidatelimitationsoption]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationf_history_user_id_8a0e3e65_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilecandidatelimitationsoption] CHECK CONSTRAINT [custom_models_historicaluseraccommodationf_history_user_id_8a0e3e65_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilecandidatelimitationsoptiontext]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationf_history_user_id_36b84da4_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilecandidatelimitationsoptiontext] CHECK CONSTRAINT [custom_models_historicaluseraccommodationf_history_user_id_36b84da4_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilecomments]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationfilecomments_history_user_id_43b708f8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilecomments] CHECK CONSTRAINT [custom_models_historicaluseraccommodationfilecomments_history_user_id_43b708f8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilecomplexity]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationfilecomplexity_history_user_id_eba168c3_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilecomplexity] CHECK CONSTRAINT [custom_models_historicaluseraccommodationfilecomplexity_history_user_id_eba168c3_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilecomplexitytext]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationfilecomplexitytext_history_user_id_9405d3f8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilecomplexitytext] CHECK CONSTRAINT [custom_models_historicaluseraccommodationfilecomplexitytext_history_user_id_9405d3f8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfileothermeasures]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationfileothermeasures_history_user_id_05f63945_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfileothermeasures] CHECK CONSTRAINT [custom_models_historicaluseraccommodationfileothermeasures_history_user_id_05f63945_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilerecommendations]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationfilerecommendations_history_user_id_a22ccad6_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilerecommendations] CHECK CONSTRAINT [custom_models_historicaluseraccommodationfilerecommendations_history_user_id_a22ccad6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilerecommendationsoption]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationfilerecommendationsoption_history_user_id_47eb13d1_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilerecommendationsoption] CHECK CONSTRAINT [custom_models_historicaluseraccommodationfilerecommendationsoption_history_user_id_47eb13d1_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilerecommendationsoptiontext]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationf_history_user_id_3c5b02dd_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilerecommendationsoptiontext] CHECK CONSTRAINT [custom_models_historicaluseraccommodationf_history_user_id_3c5b02dd_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilestatus]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationfilestatus_history_user_id_8f8f9dc5_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilestatus] CHECK CONSTRAINT [custom_models_historicaluseraccommodationfilestatus_history_user_id_8f8f9dc5_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilestatustext]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationfilestatustext_history_user_id_d7bbfde9_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfilestatustext] CHECK CONSTRAINT [custom_models_historicaluseraccommodationfilestatustext_history_user_id_d7bbfde9_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfiletesttime]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationfiletesttime_history_user_id_27c06365_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfiletesttime] CHECK CONSTRAINT [custom_models_historicaluseraccommodationfiletesttime_history_user_id_27c06365_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfiletesttoadminister]  WITH CHECK ADD  CONSTRAINT [custom_models_historicaluseraccommodationfiletesttoadminister_history_user_id_ac544cb8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_historicaluseraccommodationfiletesttoadminister] CHECK CONSTRAINT [custom_models_historicaluseraccommodationfiletesttoadminister_history_user_id_ac544cb8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_languagetext]  WITH CHECK ADD  CONSTRAINT [custom_models_languagetext_language_id_59918655_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_languagetext] CHECK CONSTRAINT [custom_models_languagetext_language_id_59918655_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_languagetext]  WITH CHECK ADD  CONSTRAINT [custom_models_languagetext_language_ref_id_23ecc389_fk_custom_models_language_language_id] FOREIGN KEY([language_ref_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_languagetext] CHECK CONSTRAINT [custom_models_languagetext_language_ref_id_23ecc389_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_locktestactions]  WITH CHECK ADD  CONSTRAINT [custom_models_locktestactions_ta_action_id_5bdbaa00_fk_custom_models_taactions_id] FOREIGN KEY([ta_action_id])
REFERENCES [dbo].[custom_models_taactions] ([id])
GO
ALTER TABLE [dbo].[custom_models_locktestactions] CHECK CONSTRAINT [custom_models_locktestactions_ta_action_id_5bdbaa00_fk_custom_models_taactions_id]
GO
ALTER TABLE [dbo].[custom_models_notepad]  WITH CHECK ADD  CONSTRAINT [custom_models_notepad_assigned_test_id_97426b23_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_notepad] CHECK CONSTRAINT [custom_models_notepad_assigned_test_id_97426b23_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_orderlessfinancialdata]  WITH CHECK ADD  CONSTRAINT [custom_models_orderlessfinancialdata_reason_for_testing_id_f5867705_fk_custom_models_reasonfortesting_id] FOREIGN KEY([reason_for_testing_id])
REFERENCES [dbo].[custom_models_reasonfortesting] ([id])
GO
ALTER TABLE [dbo].[custom_models_orderlessfinancialdata] CHECK CONSTRAINT [custom_models_orderlessfinancialdata_reason_for_testing_id_f5867705_fk_custom_models_reasonfortesting_id]
GO
ALTER TABLE [dbo].[custom_models_orderlessfinancialdata]  WITH CHECK ADD  CONSTRAINT [custom_models_orderlessfinancialdata_uit_invite_id_5e98d5d1_fk_custom_models_uitinvites_id] FOREIGN KEY([uit_invite_id])
REFERENCES [dbo].[custom_models_uitinvites] ([id])
GO
ALTER TABLE [dbo].[custom_models_orderlessfinancialdata] CHECK CONSTRAINT [custom_models_orderlessfinancialdata_uit_invite_id_5e98d5d1_fk_custom_models_uitinvites_id]
GO
ALTER TABLE [dbo].[custom_models_pausetestactions]  WITH CHECK ADD  CONSTRAINT [custom_models_pausetestactions_ta_action_id_35e9bc1f_fk_custom_models_taactions_id] FOREIGN KEY([ta_action_id])
REFERENCES [dbo].[custom_models_taactions] ([id])
GO
ALTER TABLE [dbo].[custom_models_pausetestactions] CHECK CONSTRAINT [custom_models_pausetestactions_ta_action_id_35e9bc1f_fk_custom_models_taactions_id]
GO
ALTER TABLE [dbo].[custom_models_reasonfortesting]  WITH CHECK ADD  CONSTRAINT [custom_models_reasonfortesting_reason_for_testing_priority_id_279dd33c_fk_custom_models_reasonfortestingpriority_id] FOREIGN KEY([reason_for_testing_priority_id])
REFERENCES [dbo].[custom_models_reasonfortestingpriority] ([id])
GO
ALTER TABLE [dbo].[custom_models_reasonfortesting] CHECK CONSTRAINT [custom_models_reasonfortesting_reason_for_testing_priority_id_279dd33c_fk_custom_models_reasonfortestingpriority_id]
GO
ALTER TABLE [dbo].[custom_models_reasonfortesting]  WITH CHECK ADD  CONSTRAINT [custom_models_reasonfortesting_reason_for_testing_type_id_b9eef50a_fk_custom_models_reasonfortestingtype_id] FOREIGN KEY([reason_for_testing_type_id])
REFERENCES [dbo].[custom_models_reasonfortestingtype] ([id])
GO
ALTER TABLE [dbo].[custom_models_reasonfortesting] CHECK CONSTRAINT [custom_models_reasonfortesting_reason_for_testing_type_id_b9eef50a_fk_custom_models_reasonfortestingtype_id]
GO
ALTER TABLE [dbo].[custom_models_reasonfortestingprioritytext]  WITH CHECK ADD  CONSTRAINT [custom_models_reasonfortestingprioritytext_language_id_c0b18dbd_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_reasonfortestingprioritytext] CHECK CONSTRAINT [custom_models_reasonfortestingprioritytext_language_id_c0b18dbd_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_reasonfortestingprioritytext]  WITH CHECK ADD  CONSTRAINT [custom_models_reasonfortestingprioritytext_reason_for_testing_priority_id_dd597ac6_fk_custom_models_reasonfortestingpriority_id] FOREIGN KEY([reason_for_testing_priority_id])
REFERENCES [dbo].[custom_models_reasonfortestingpriority] ([id])
GO
ALTER TABLE [dbo].[custom_models_reasonfortestingprioritytext] CHECK CONSTRAINT [custom_models_reasonfortestingprioritytext_reason_for_testing_priority_id_dd597ac6_fk_custom_models_reasonfortestingpriority_id]
GO
ALTER TABLE [dbo].[custom_models_reasonfortestingtext]  WITH CHECK ADD  CONSTRAINT [custom_models_reasonfortestingtext_language_id_5a0f9546_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_reasonfortestingtext] CHECK CONSTRAINT [custom_models_reasonfortestingtext_language_id_5a0f9546_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_reasonfortestingtext]  WITH CHECK ADD  CONSTRAINT [custom_models_reasonfortestingtext_reason_for_testing_id_ad042635_fk_custom_models_reasonfortesting_id] FOREIGN KEY([reason_for_testing_id])
REFERENCES [dbo].[custom_models_reasonfortesting] ([id])
GO
ALTER TABLE [dbo].[custom_models_reasonfortestingtext] CHECK CONSTRAINT [custom_models_reasonfortestingtext_reason_for_testing_id_ad042635_fk_custom_models_reasonfortesting_id]
GO
ALTER TABLE [dbo].[custom_models_reservationcodes]  WITH CHECK ADD  CONSTRAINT [custom_models_reservationcodes_assessment_process_assigned_test_specs_id_35db7141_fk_custom_models_assessmentproces] FOREIGN KEY([assessment_process_assigned_test_specs_id])
REFERENCES [dbo].[custom_models_assessmentprocessassignedtestspecs] ([id])
GO
ALTER TABLE [dbo].[custom_models_reservationcodes] CHECK CONSTRAINT [custom_models_reservationcodes_assessment_process_assigned_test_specs_id_35db7141_fk_custom_models_assessmentproces]
GO
ALTER TABLE [dbo].[custom_models_scorerolaassignedtestsession]  WITH CHECK ADD  CONSTRAINT [custom_models_scorerolaassignedtestsession_candidate_id_e975a2a2_fk_user_management_models_user_id] FOREIGN KEY([candidate_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_scorerolaassignedtestsession] CHECK CONSTRAINT [custom_models_scorerolaassignedtestsession_candidate_id_e975a2a2_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_scorerolaassignedtestsession]  WITH CHECK ADD  CONSTRAINT [custom_models_scorerolaassignedtestsession_test_assessor_id_1811f990_fk_user_management_models_user_id] FOREIGN KEY([test_assessor_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_scorerolaassignedtestsession] CHECK CONSTRAINT [custom_models_scorerolaassignedtestsession_test_assessor_id_1811f990_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_scorerolaassignedtestsession]  WITH CHECK ADD  CONSTRAINT [custom_models_scorerolaassignedtestsession_test_session_id_8339f7ca_fk_custom_models_testcentertestsessions_id] FOREIGN KEY([test_session_id])
REFERENCES [dbo].[custom_models_testcentertestsessions] ([id])
GO
ALTER TABLE [dbo].[custom_models_scorerolaassignedtestsession] CHECK CONSTRAINT [custom_models_scorerolaassignedtestsession_test_session_id_8339f7ca_fk_custom_models_testcentertestsessions_id]
GO
ALTER TABLE [dbo].[custom_models_scorerolatestsessionskipaction]  WITH CHECK ADD  CONSTRAINT [custom_models_scorerolatestsessionskipacti_scorer_ola_test_session_skip_option_id_eb13d5cf_fk_custom_models_scorerolatestses] FOREIGN KEY([scorer_ola_test_session_skip_option_id])
REFERENCES [dbo].[custom_models_scorerolatestsessionskipoption] ([id])
GO
ALTER TABLE [dbo].[custom_models_scorerolatestsessionskipaction] CHECK CONSTRAINT [custom_models_scorerolatestsessionskipacti_scorer_ola_test_session_skip_option_id_eb13d5cf_fk_custom_models_scorerolatestses]
GO
ALTER TABLE [dbo].[custom_models_scorerolatestsessionskipaction]  WITH CHECK ADD  CONSTRAINT [custom_models_scorerolatestsessionskipaction_candidate_id_45f7a103_fk_user_management_models_user_id] FOREIGN KEY([candidate_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_scorerolatestsessionskipaction] CHECK CONSTRAINT [custom_models_scorerolatestsessionskipaction_candidate_id_45f7a103_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_scorerolatestsessionskipaction]  WITH CHECK ADD  CONSTRAINT [custom_models_scorerolatestsessionskipaction_test_assessor_id_3b421e7f_fk_user_management_models_user_id] FOREIGN KEY([test_assessor_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_scorerolatestsessionskipaction] CHECK CONSTRAINT [custom_models_scorerolatestsessionskipaction_test_assessor_id_3b421e7f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_scorerolatestsessionskipaction]  WITH CHECK ADD  CONSTRAINT [custom_models_scorerolatestsessionskipaction_test_session_id_2a260487_fk_custom_models_testcentertestsessions_id] FOREIGN KEY([test_session_id])
REFERENCES [dbo].[custom_models_testcentertestsessions] ([id])
GO
ALTER TABLE [dbo].[custom_models_scorerolatestsessionskipaction] CHECK CONSTRAINT [custom_models_scorerolatestsessionskipaction_test_session_id_2a260487_fk_custom_models_testcentertestsessions_id]
GO
ALTER TABLE [dbo].[custom_models_scorerolatestsessionskipoptiontext]  WITH CHECK ADD  CONSTRAINT [custom_models_scorerolatestsessionskipopti_scorer_ola_test_session_skip_option_id_34496c0b_fk_custom_models_scorerolatestses] FOREIGN KEY([scorer_ola_test_session_skip_option_id])
REFERENCES [dbo].[custom_models_scorerolatestsessionskipoption] ([id])
GO
ALTER TABLE [dbo].[custom_models_scorerolatestsessionskipoptiontext] CHECK CONSTRAINT [custom_models_scorerolatestsessionskipopti_scorer_ola_test_session_skip_option_id_34496c0b_fk_custom_models_scorerolatestses]
GO
ALTER TABLE [dbo].[custom_models_scorerolatestsessionskipoptiontext]  WITH CHECK ADD  CONSTRAINT [custom_models_scorerolatestsessionskipoptiontext_language_id_839f651b_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_scorerolatestsessionskipoptiontext] CHECK CONSTRAINT [custom_models_scorerolatestsessionskipoptiontext_language_id_839f651b_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_systemalert]  WITH CHECK ADD  CONSTRAINT [custom_models_systemalert_criticality_id_b6852b8d_fk_custom_models_criticality_id] FOREIGN KEY([criticality_id])
REFERENCES [dbo].[custom_models_criticality] ([id])
GO
ALTER TABLE [dbo].[custom_models_systemalert] CHECK CONSTRAINT [custom_models_systemalert_criticality_id_b6852b8d_fk_custom_models_criticality_id]
GO
ALTER TABLE [dbo].[custom_models_taactions]  WITH CHECK ADD  CONSTRAINT [custom_models_taactions_action_type_id_805b1ae2_fk_custom_models_taactiontypes_action_type] FOREIGN KEY([action_type_id])
REFERENCES [dbo].[custom_models_taactiontypes] ([action_type])
GO
ALTER TABLE [dbo].[custom_models_taactions] CHECK CONSTRAINT [custom_models_taactions_action_type_id_805b1ae2_fk_custom_models_taactiontypes_action_type]
GO
ALTER TABLE [dbo].[custom_models_taactions]  WITH CHECK ADD  CONSTRAINT [custom_models_taactions_assigned_test_id_c10f40b0_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_taactions] CHECK CONSTRAINT [custom_models_taactions_assigned_test_id_c10f40b0_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_taactions]  WITH CHECK ADD  CONSTRAINT [custom_models_taactions_test_section_id_10d7e562_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[custom_models_taactions] CHECK CONSTRAINT [custom_models_taactions_test_section_id_10d7e562_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[custom_models_testaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_testaccesscode_ta_user_id_58b74f96_fk_user_management_models_user_id] FOREIGN KEY([ta_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_testaccesscode] CHECK CONSTRAINT [custom_models_testaccesscode_ta_user_id_58b74f96_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_testaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_testaccesscode_test_id_72b65694_fk_cms_models_testdefinition_id] FOREIGN KEY([test_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[custom_models_testaccesscode] CHECK CONSTRAINT [custom_models_testaccesscode_test_id_72b65694_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[custom_models_testaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_testaccesscode_test_session_id_21cd9de2_fk_custom_models_testcentertestsessions_id] FOREIGN KEY([test_session_id])
REFERENCES [dbo].[custom_models_testcentertestsessions] ([id])
GO
ALTER TABLE [dbo].[custom_models_testaccesscode] CHECK CONSTRAINT [custom_models_testaccesscode_test_session_id_21cd9de2_fk_custom_models_testcentertestsessions_id]
GO
ALTER TABLE [dbo].[custom_models_testcenteraddresstext]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenteraddresstext_language_id_4e944c35_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_testcenteraddresstext] CHECK CONSTRAINT [custom_models_testcenteraddresstext_language_id_4e944c35_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_testcenteraddresstext]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenteraddresstext_test_center_id_6ea760d4_fk_custom_models_testcenter_id] FOREIGN KEY([test_center_id])
REFERENCES [dbo].[custom_models_testcenter] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenteraddresstext] CHECK CONSTRAINT [custom_models_testcenteraddresstext_test_center_id_6ea760d4_fk_custom_models_testcenter_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterassociatedmanagers]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterassociatedmanagers_test_center_id_6c9f600e_fk_custom_models_testcenter_id] FOREIGN KEY([test_center_id])
REFERENCES [dbo].[custom_models_testcenter] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterassociatedmanagers] CHECK CONSTRAINT [custom_models_testcenterassociatedmanagers_test_center_id_6c9f600e_fk_custom_models_testcenter_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterassociatedmanagers]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterassociatedmanagers_user_id_7f50fd82_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterassociatedmanagers] CHECK CONSTRAINT [custom_models_testcenterassociatedmanagers_user_id_7f50fd82_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_testcentercitytext]  WITH CHECK ADD  CONSTRAINT [custom_models_testcentercitytext_language_id_dc2aa3c3_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_testcentercitytext] CHECK CONSTRAINT [custom_models_testcentercitytext_language_id_dc2aa3c3_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_testcentercitytext]  WITH CHECK ADD  CONSTRAINT [custom_models_testcentercitytext_test_center_id_a953f366_fk_custom_models_testcenter_id] FOREIGN KEY([test_center_id])
REFERENCES [dbo].[custom_models_testcenter] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcentercitytext] CHECK CONSTRAINT [custom_models_testcentercitytext_test_center_id_a953f366_fk_custom_models_testcenter_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterolaassessorunavailability]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterolaassessorunavail_test_center_ola_test_assessor_id_09fc42a3_fk_custom_models_testcenterolates] FOREIGN KEY([test_center_ola_test_assessor_id])
REFERENCES [dbo].[custom_models_testcenterolatestassessor] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterolaassessorunavailability] CHECK CONSTRAINT [custom_models_testcenterolaassessorunavail_test_center_ola_test_assessor_id_09fc42a3_fk_custom_models_testcenterolates]
GO
ALTER TABLE [dbo].[custom_models_testcenterolaconfigs]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterolaconfigs_test_center_id_e120fe73_fk_custom_models_testcenter_id] FOREIGN KEY([test_center_id])
REFERENCES [dbo].[custom_models_testcenter] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterolaconfigs] CHECK CONSTRAINT [custom_models_testcenterolaconfigs_test_center_id_e120fe73_fk_custom_models_testcenter_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterolatestassessor]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterolatestassessor_test_center_id_d71746b1_fk_custom_models_testcenter_id] FOREIGN KEY([test_center_id])
REFERENCES [dbo].[custom_models_testcenter] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterolatestassessor] CHECK CONSTRAINT [custom_models_testcenterolatestassessor_test_center_id_d71746b1_fk_custom_models_testcenter_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterolatestassessor]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterolatestassessor_user_id_1b44db99_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterolatestassessor] CHECK CONSTRAINT [custom_models_testcenterolatestassessor_user_id_1b44db99_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterolatestassessorapprovedlanguage]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterolatestassessorapp_test_center_ola_test_assessor_id_0d038112_fk_custom_models_testcenterolates] FOREIGN KEY([test_center_ola_test_assessor_id])
REFERENCES [dbo].[custom_models_testcenterolatestassessor] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterolatestassessorapprovedlanguage] CHECK CONSTRAINT [custom_models_testcenterolatestassessorapp_test_center_ola_test_assessor_id_0d038112_fk_custom_models_testcenterolates]
GO
ALTER TABLE [dbo].[custom_models_testcenterolatestassessorapprovedlanguage]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterolatestassessorapprovedlanguage_language_id_128c0f71_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_testcenterolatestassessorapprovedlanguage] CHECK CONSTRAINT [custom_models_testcenterolatestassessorapprovedlanguage_language_id_128c0f71_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterolatimeslot]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterolatimeslot_assessed_language_id_ec9c4e0c_fk_custom_models_language_language_id] FOREIGN KEY([assessed_language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_testcenterolatimeslot] CHECK CONSTRAINT [custom_models_testcenterolatimeslot_assessed_language_id_ec9c4e0c_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterolatimeslot]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterolatimeslot_test_center_id_2b80695d_fk_custom_models_testcenter_id] FOREIGN KEY([test_center_id])
REFERENCES [dbo].[custom_models_testcenter] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterolatimeslot] CHECK CONSTRAINT [custom_models_testcenterolatimeslot_test_center_id_2b80695d_fk_custom_models_testcenter_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterolatimeslotassessoravailability]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterolatimeslotassesso_test_center_ola_test_assessor_id_762915c2_fk_custom_models_testcenterolates] FOREIGN KEY([test_center_ola_test_assessor_id])
REFERENCES [dbo].[custom_models_testcenterolatestassessor] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterolatimeslotassessoravailability] CHECK CONSTRAINT [custom_models_testcenterolatimeslotassesso_test_center_ola_test_assessor_id_762915c2_fk_custom_models_testcenterolates]
GO
ALTER TABLE [dbo].[custom_models_testcenterolatimeslotassessoravailability]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterolatimeslotassesso_test_center_ola_time_slot_id_e5429f83_fk_custom_models_testcenterolatim] FOREIGN KEY([test_center_ola_time_slot_id])
REFERENCES [dbo].[custom_models_testcenterolatimeslot] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterolatimeslotassessoravailability] CHECK CONSTRAINT [custom_models_testcenterolatimeslotassesso_test_center_ola_time_slot_id_e5429f83_fk_custom_models_testcenterolatim]
GO
ALTER TABLE [dbo].[custom_models_testcenterolatimeslotprioritization]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterolatimeslotpriorit_reason_for_testing_priority_id_26565ab1_fk_custom_models_reasonfortesting] FOREIGN KEY([reason_for_testing_priority_id])
REFERENCES [dbo].[custom_models_reasonfortestingpriority] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterolatimeslotprioritization] CHECK CONSTRAINT [custom_models_testcenterolatimeslotpriorit_reason_for_testing_priority_id_26565ab1_fk_custom_models_reasonfortesting]
GO
ALTER TABLE [dbo].[custom_models_testcenterolatimeslotprioritization]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterolatimeslotpriorit_test_center_ola_time_slot_id_cd5a0686_fk_custom_models_testcenterolatim] FOREIGN KEY([test_center_ola_time_slot_id])
REFERENCES [dbo].[custom_models_testcenterolatimeslot] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterolatimeslotprioritization] CHECK CONSTRAINT [custom_models_testcenterolatimeslotpriorit_test_center_ola_time_slot_id_cd5a0686_fk_custom_models_testcenterolatim]
GO
ALTER TABLE [dbo].[custom_models_testcenterolavacationblock]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterolavacationblock_test_center_id_32df41c4_fk_custom_models_testcenter_id] FOREIGN KEY([test_center_id])
REFERENCES [dbo].[custom_models_testcenter] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterolavacationblock] CHECK CONSTRAINT [custom_models_testcenterolavacationblock_test_center_id_32df41c4_fk_custom_models_testcenter_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterolavacationblockavailability]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterolavacationblockav_test_center_ola_vacation_block_id_e911a769_fk_custom_models_testcenterolavac] FOREIGN KEY([test_center_ola_vacation_block_id])
REFERENCES [dbo].[custom_models_testcenterolavacationblock] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterolavacationblockavailability] CHECK CONSTRAINT [custom_models_testcenterolavacationblockav_test_center_ola_vacation_block_id_e911a769_fk_custom_models_testcenterolavac]
GO
ALTER TABLE [dbo].[custom_models_testcenterolavacationblockavailability]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterolavacationblockavailability_language_id_ea8bcdcf_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_testcenterolavacationblockavailability] CHECK CONSTRAINT [custom_models_testcenterolavacationblockavailability_language_id_ea8bcdcf_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterotherdetailstext]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterotherdetailstext_language_id_4dbd55f6_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_testcenterotherdetailstext] CHECK CONSTRAINT [custom_models_testcenterotherdetailstext_language_id_4dbd55f6_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterotherdetailstext]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterotherdetailstext_test_center_id_499bd0c0_fk_custom_models_testcenter_id] FOREIGN KEY([test_center_id])
REFERENCES [dbo].[custom_models_testcenter] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterotherdetailstext] CHECK CONSTRAINT [custom_models_testcenterotherdetailstext_test_center_id_499bd0c0_fk_custom_models_testcenter_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterprovincetext]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterprovincetext_language_id_49e4fd9f_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_testcenterprovincetext] CHECK CONSTRAINT [custom_models_testcenterprovincetext_language_id_49e4fd9f_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterprovincetext]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterprovincetext_test_center_id_6b06086e_fk_custom_models_testcenter_id] FOREIGN KEY([test_center_id])
REFERENCES [dbo].[custom_models_testcenter] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterprovincetext] CHECK CONSTRAINT [custom_models_testcenterprovincetext_test_center_id_6b06086e_fk_custom_models_testcenter_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterroomdataotherdetailstext]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterroomdataotherdetailstext_language_id_23186bce_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_testcenterroomdataotherdetailstext] CHECK CONSTRAINT [custom_models_testcenterroomdataotherdetailstext_language_id_23186bce_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterroomdataotherdetailstext]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterroomdataotherdetailstext_test_center_room_data_id_c70ec92a_fk_custom_models_testcenterroomdata_id] FOREIGN KEY([test_center_room_data_id])
REFERENCES [dbo].[custom_models_testcenterroomdata] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterroomdataotherdetailstext] CHECK CONSTRAINT [custom_models_testcenterroomdataotherdetailstext_test_center_room_data_id_c70ec92a_fk_custom_models_testcenterroomdata_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterrooms]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterrooms_room_data_id_87ad0dc5_fk_custom_models_testcenterroomdata_id] FOREIGN KEY([room_data_id])
REFERENCES [dbo].[custom_models_testcenterroomdata] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterrooms] CHECK CONSTRAINT [custom_models_testcenterrooms_room_data_id_87ad0dc5_fk_custom_models_testcenterroomdata_id]
GO
ALTER TABLE [dbo].[custom_models_testcenterrooms]  WITH CHECK ADD  CONSTRAINT [custom_models_testcenterrooms_test_center_id_c5e66fe3_fk_custom_models_testcenter_id] FOREIGN KEY([test_center_id])
REFERENCES [dbo].[custom_models_testcenter] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcenterrooms] CHECK CONSTRAINT [custom_models_testcenterrooms_test_center_id_c5e66fe3_fk_custom_models_testcenter_id]
GO
ALTER TABLE [dbo].[custom_models_testcentertestadministrators]  WITH CHECK ADD  CONSTRAINT [custom_models_testcentertestadministrators_test_center_id_b95ecbc2_fk_custom_models_testcenter_id] FOREIGN KEY([test_center_id])
REFERENCES [dbo].[custom_models_testcenter] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcentertestadministrators] CHECK CONSTRAINT [custom_models_testcentertestadministrators_test_center_id_b95ecbc2_fk_custom_models_testcenter_id]
GO
ALTER TABLE [dbo].[custom_models_testcentertestadministrators]  WITH CHECK ADD  CONSTRAINT [custom_models_testcentertestadministrators_user_id_8633f416_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcentertestadministrators] CHECK CONSTRAINT [custom_models_testcentertestadministrators_user_id_8633f416_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessiondata]  WITH CHECK ADD  CONSTRAINT [custom_models_testcentertestsessiondata_test_assessor_user_id_39841767_fk_user_management_models_user_id] FOREIGN KEY([test_assessor_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessiondata] CHECK CONSTRAINT [custom_models_testcentertestsessiondata_test_assessor_user_id_39841767_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessiondata]  WITH CHECK ADD  CONSTRAINT [custom_models_testcentertestsessiondata_test_center_room_id_30277bf1_fk_custom_models_testcenterrooms_id] FOREIGN KEY([test_center_room_id])
REFERENCES [dbo].[custom_models_testcenterrooms] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessiondata] CHECK CONSTRAINT [custom_models_testcentertestsessiondata_test_center_room_id_30277bf1_fk_custom_models_testcenterrooms_id]
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessiondata]  WITH CHECK ADD  CONSTRAINT [custom_models_testcentertestsessiondata_test_skill_type_id_c6898ddd_fk_cms_models_testskilltype_id] FOREIGN KEY([test_skill_type_id])
REFERENCES [dbo].[cms_models_testskilltype] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessiondata] CHECK CONSTRAINT [custom_models_testcentertestsessiondata_test_skill_type_id_c6898ddd_fk_cms_models_testskilltype_id]
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessionofficers]  WITH CHECK ADD  CONSTRAINT [custom_models_testcentertestsessionofficers_test_access_code_id_80df4ba4_fk_custom_models_testaccesscode_id] FOREIGN KEY([test_access_code_id])
REFERENCES [dbo].[custom_models_testaccesscode] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessionofficers] CHECK CONSTRAINT [custom_models_testcentertestsessionofficers_test_access_code_id_80df4ba4_fk_custom_models_testaccesscode_id]
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessionofficers]  WITH CHECK ADD  CONSTRAINT [custom_models_testcentertestsessionofficers_test_administrator_id_45af2d1b_fk_user_management_models_user_id] FOREIGN KEY([test_administrator_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessionofficers] CHECK CONSTRAINT [custom_models_testcentertestsessionofficers_test_administrator_id_45af2d1b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessionofficers]  WITH CHECK ADD  CONSTRAINT [custom_models_testcentertestsessionofficers_test_session_id_7aa2d863_fk_custom_models_testcentertestsessions_id] FOREIGN KEY([test_session_id])
REFERENCES [dbo].[custom_models_testcentertestsessions] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessionofficers] CHECK CONSTRAINT [custom_models_testcentertestsessionofficers_test_session_id_7aa2d863_fk_custom_models_testcentertestsessions_id]
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessions]  WITH CHECK ADD  CONSTRAINT [custom_models_testcentertestsessions_assessment_process_assigned_test_specs_id_36fc1a0c_fk_custom_models_assessmentproces] FOREIGN KEY([assessment_process_assigned_test_specs_id])
REFERENCES [dbo].[custom_models_assessmentprocessassignedtestspecs] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessions] CHECK CONSTRAINT [custom_models_testcentertestsessions_assessment_process_assigned_test_specs_id_36fc1a0c_fk_custom_models_assessmentproces]
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessions]  WITH CHECK ADD  CONSTRAINT [custom_models_testcentertestsessions_test_center_id_ef790d87_fk_custom_models_testcenter_id] FOREIGN KEY([test_center_id])
REFERENCES [dbo].[custom_models_testcenter] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessions] CHECK CONSTRAINT [custom_models_testcentertestsessions_test_center_id_ef790d87_fk_custom_models_testcenter_id]
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessions]  WITH CHECK ADD  CONSTRAINT [custom_models_testcentertestsessions_test_session_data_id_e493ebef_fk_custom_models_testcentertestsessiondata_id] FOREIGN KEY([test_session_data_id])
REFERENCES [dbo].[custom_models_testcentertestsessiondata] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessions] CHECK CONSTRAINT [custom_models_testcentertestsessions_test_session_data_id_e493ebef_fk_custom_models_testcentertestsessiondata_id]
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessions]  WITH CHECK ADD  CONSTRAINT [custom_models_testcentertestsessions_user_accommodation_file_id_381f32c3_fk_custom_models_useraccommodationfile_id] FOREIGN KEY([user_accommodation_file_id])
REFERENCES [dbo].[custom_models_useraccommodationfile] ([id])
GO
ALTER TABLE [dbo].[custom_models_testcentertestsessions] CHECK CONSTRAINT [custom_models_testcentertestsessions_user_accommodation_file_id_381f32c3_fk_custom_models_useraccommodationfile_id]
GO
ALTER TABLE [dbo].[custom_models_testscorerassignment]  WITH CHECK ADD  CONSTRAINT [custom_models_testscorerassignment_assigned_test_id_47becc54_fk_custom_models_assignedtest_id] FOREIGN KEY([assigned_test_id])
REFERENCES [dbo].[custom_models_assignedtest] ([id])
GO
ALTER TABLE [dbo].[custom_models_testscorerassignment] CHECK CONSTRAINT [custom_models_testscorerassignment_assigned_test_id_47becc54_fk_custom_models_assignedtest_id]
GO
ALTER TABLE [dbo].[custom_models_testscorerassignment]  WITH CHECK ADD  CONSTRAINT [custom_models_testscorerassignment_scorer_user_id_4b2bd448_fk_user_management_models_user_id] FOREIGN KEY([scorer_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_testscorerassignment] CHECK CONSTRAINT [custom_models_testscorerassignment_scorer_user_id_4b2bd448_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_uitinviterelatedcandidates]  WITH CHECK ADD  CONSTRAINT [custom_models_uitinviterelatedcandidates_reason_for_deletion_id_8f7d89ce_fk_custom_models_uitreasonsfordeletion_id] FOREIGN KEY([reason_for_deletion_id])
REFERENCES [dbo].[custom_models_uitreasonsfordeletion] ([id])
GO
ALTER TABLE [dbo].[custom_models_uitinviterelatedcandidates] CHECK CONSTRAINT [custom_models_uitinviterelatedcandidates_reason_for_deletion_id_8f7d89ce_fk_custom_models_uitreasonsfordeletion_id]
GO
ALTER TABLE [dbo].[custom_models_uitinviterelatedcandidates]  WITH CHECK ADD  CONSTRAINT [custom_models_uitinviterelatedcandidates_uit_invite_id_74d00ec5_fk_custom_models_uitinvites_id] FOREIGN KEY([uit_invite_id])
REFERENCES [dbo].[custom_models_uitinvites] ([id])
GO
ALTER TABLE [dbo].[custom_models_uitinviterelatedcandidates] CHECK CONSTRAINT [custom_models_uitinviterelatedcandidates_uit_invite_id_74d00ec5_fk_custom_models_uitinvites_id]
GO
ALTER TABLE [dbo].[custom_models_uitinvites]  WITH CHECK ADD  CONSTRAINT [custom_models_uitinvites_reason_for_deletion_id_fb429165_fk_custom_models_uitreasonsfordeletion_id] FOREIGN KEY([reason_for_deletion_id])
REFERENCES [dbo].[custom_models_uitreasonsfordeletion] ([id])
GO
ALTER TABLE [dbo].[custom_models_uitinvites] CHECK CONSTRAINT [custom_models_uitinvites_reason_for_deletion_id_fb429165_fk_custom_models_uitreasonsfordeletion_id]
GO
ALTER TABLE [dbo].[custom_models_uitinvites]  WITH CHECK ADD  CONSTRAINT [custom_models_uitinvites_reason_for_modification_id_51e74101_fk_custom_models_uitreasonsformodification_id] FOREIGN KEY([reason_for_modification_id])
REFERENCES [dbo].[custom_models_uitreasonsformodification] ([id])
GO
ALTER TABLE [dbo].[custom_models_uitinvites] CHECK CONSTRAINT [custom_models_uitinvites_reason_for_modification_id_51e74101_fk_custom_models_uitreasonsformodification_id]
GO
ALTER TABLE [dbo].[custom_models_uitinvites]  WITH CHECK ADD  CONSTRAINT [custom_models_uitinvites_ta_test_permissions_id_723fe8ab_fk_cms_models_testpermissions_id] FOREIGN KEY([ta_test_permissions_id])
REFERENCES [dbo].[cms_models_testpermissions] ([id])
GO
ALTER TABLE [dbo].[custom_models_uitinvites] CHECK CONSTRAINT [custom_models_uitinvites_ta_test_permissions_id_723fe8ab_fk_cms_models_testpermissions_id]
GO
ALTER TABLE [dbo].[custom_models_uitinvites]  WITH CHECK ADD  CONSTRAINT [custom_models_uitinvites_ta_user_id_b6d9c9fa_fk_user_management_models_user_id] FOREIGN KEY([ta_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_uitinvites] CHECK CONSTRAINT [custom_models_uitinvites_ta_user_id_b6d9c9fa_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_uitinvites]  WITH CHECK ADD  CONSTRAINT [custom_models_uitinvites_test_id_fed22d54_fk_cms_models_testdefinition_id] FOREIGN KEY([test_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[custom_models_uitinvites] CHECK CONSTRAINT [custom_models_uitinvites_test_id_fed22d54_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_unsupervisedtestaccesscode_accommodation_request_id_9c0dd526_fk_custom_models_accommodationrequest_id] FOREIGN KEY([accommodation_request_id])
REFERENCES [dbo].[custom_models_accommodationrequest] ([id])
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode] CHECK CONSTRAINT [custom_models_unsupervisedtestaccesscode_accommodation_request_id_9c0dd526_fk_custom_models_accommodationrequest_id]
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_unsupervisedtestaccesscode_orderless_financial_data_id_25b01bf8_fk_custom_models_orderlessfinancialdata_id] FOREIGN KEY([orderless_financial_data_id])
REFERENCES [dbo].[custom_models_orderlessfinancialdata] ([id])
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode] CHECK CONSTRAINT [custom_models_unsupervisedtestaccesscode_orderless_financial_data_id_25b01bf8_fk_custom_models_orderlessfinancialdata_id]
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_unsupervisedtestaccesscode_ta_user_id_4ab38abd_fk_user_management_models_user_id] FOREIGN KEY([ta_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode] CHECK CONSTRAINT [custom_models_unsupervisedtestaccesscode_ta_user_id_4ab38abd_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_unsupervisedtestaccesscode_test_id_bb7066b0_fk_cms_models_testdefinition_id] FOREIGN KEY([test_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode] CHECK CONSTRAINT [custom_models_unsupervisedtestaccesscode_test_id_bb7066b0_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode]  WITH CHECK ADD  CONSTRAINT [custom_models_unsupervisedtestaccesscode_uit_invite_id_e8fb2831_fk_custom_models_uitinvites_id] FOREIGN KEY([uit_invite_id])
REFERENCES [dbo].[custom_models_uitinvites] ([id])
GO
ALTER TABLE [dbo].[custom_models_unsupervisedtestaccesscode] CHECK CONSTRAINT [custom_models_unsupervisedtestaccesscode_uit_invite_id_e8fb2831_fk_custom_models_uitinvites_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfile]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfile_accommodation_request_id_b7b930b1_fk_custom_models_accommodationrequest_id] FOREIGN KEY([accommodation_request_id])
REFERENCES [dbo].[custom_models_accommodationrequest] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfile] CHECK CONSTRAINT [custom_models_useraccommodationfile_accommodation_request_id_b7b930b1_fk_custom_models_accommodationrequest_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfile]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfile_assigned_to_user_id_a59845ce_fk_user_management_models_user_id] FOREIGN KEY([assigned_to_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfile] CHECK CONSTRAINT [custom_models_useraccommodationfile_assigned_to_user_id_a59845ce_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfile]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfile_complexity_id_fe265835_fk_custom_models_useraccommodationfilecomplexity_id] FOREIGN KEY([complexity_id])
REFERENCES [dbo].[custom_models_useraccommodationfilecomplexity] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfile] CHECK CONSTRAINT [custom_models_useraccommodationfile_complexity_id_fe265835_fk_custom_models_useraccommodationfilecomplexity_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfile]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfile_last_modified_by_user_id_f9c2633b_fk_user_management_models_user_id] FOREIGN KEY([last_modified_by_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfile] CHECK CONSTRAINT [custom_models_useraccommodationfile_last_modified_by_user_id_f9c2633b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfile]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfile_status_id_e99dea6b_fk_custom_models_useraccommodationfilestatus_id] FOREIGN KEY([status_id])
REFERENCES [dbo].[custom_models_useraccommodationfilestatus] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfile] CHECK CONSTRAINT [custom_models_useraccommodationfile_status_id_e99dea6b_fk_custom_models_useraccommodationfilestatus_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfile]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfile_test_center_id_ce927340_fk_custom_models_testcenter_id] FOREIGN KEY([test_center_id])
REFERENCES [dbo].[custom_models_testcenter] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfile] CHECK CONSTRAINT [custom_models_useraccommodationfile_test_center_id_ce927340_fk_custom_models_testcenter_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfile]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfile_user_id_f563e388_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfile] CHECK CONSTRAINT [custom_models_useraccommodationfile_user_id_f563e388_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilebreakbank]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfilebreakbank_user_accommodation_file_id_4f4c558d_fk_custom_models_useraccommodationfile_id] FOREIGN KEY([user_accommodation_file_id])
REFERENCES [dbo].[custom_models_useraccommodationfile] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilebreakbank] CHECK CONSTRAINT [custom_models_useraccommodationfilebreakbank_user_accommodation_file_id_4f4c558d_fk_custom_models_useraccommodationfile_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecandidatelimitations]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfilecandida_user_accommodation_file_candidate_limitati_3e758f85_fk_custom_models_useraccommodatio] FOREIGN KEY([user_accommodation_file_candidate_limitations_option_id])
REFERENCES [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecandidatelimitations] CHECK CONSTRAINT [custom_models_useraccommodationfilecandida_user_accommodation_file_candidate_limitati_3e758f85_fk_custom_models_useraccommodatio]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecandidatelimitations]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfilecandida_user_accommodation_file_id_0698cd13_fk_custom_models_useraccommodatio] FOREIGN KEY([user_accommodation_file_id])
REFERENCES [dbo].[custom_models_useraccommodationfile] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecandidatelimitations] CHECK CONSTRAINT [custom_models_useraccommodationfilecandida_user_accommodation_file_id_0698cd13_fk_custom_models_useraccommodatio]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfilecandida_user_accommodation_file_candidate_limitati_6eb77dca_fk_custom_models_useraccommodatio] FOREIGN KEY([user_accommodation_file_candidate_limitations_option_id])
REFERENCES [dbo].[custom_models_useraccommodationfilecandidatelimitationsoption] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext] CHECK CONSTRAINT [custom_models_useraccommodationfilecandida_user_accommodation_file_candidate_limitati_6eb77dca_fk_custom_models_useraccommodatio]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfilecandidatelimitationsoptiontext_language_id_90cde177_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecandidatelimitationsoptiontext] CHECK CONSTRAINT [custom_models_useraccommodationfilecandidatelimitationsoptiontext_language_id_90cde177_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecomments]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfilecomments_user_accommodation_file_id_a8459122_fk_custom_models_useraccommodationfile_id] FOREIGN KEY([user_accommodation_file_id])
REFERENCES [dbo].[custom_models_useraccommodationfile] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecomments] CHECK CONSTRAINT [custom_models_useraccommodationfilecomments_user_accommodation_file_id_a8459122_fk_custom_models_useraccommodationfile_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecomments]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfilecomments_user_id_6c0f3c54_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecomments] CHECK CONSTRAINT [custom_models_useraccommodationfilecomments_user_id_6c0f3c54_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecomplexitytext]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfilecomplex_user_accommodation_file_complexity_id_97de3071_fk_custom_models_useraccommodatio] FOREIGN KEY([user_accommodation_file_complexity_id])
REFERENCES [dbo].[custom_models_useraccommodationfilecomplexity] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecomplexitytext] CHECK CONSTRAINT [custom_models_useraccommodationfilecomplex_user_accommodation_file_complexity_id_97de3071_fk_custom_models_useraccommodatio]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecomplexitytext]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfilecomplexitytext_language_id_f238d03a_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilecomplexitytext] CHECK CONSTRAINT [custom_models_useraccommodationfilecomplexitytext_language_id_f238d03a_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfileothermeasures]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfileothermeasures_user_accommodation_file_id_31f7595b_fk_custom_models_useraccommodationfile_id] FOREIGN KEY([user_accommodation_file_id])
REFERENCES [dbo].[custom_models_useraccommodationfile] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfileothermeasures] CHECK CONSTRAINT [custom_models_useraccommodationfileothermeasures_user_accommodation_file_id_31f7595b_fk_custom_models_useraccommodationfile_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilerecommendations]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfilerecomme_user_accommodation_file_recommendations_op_c7c87db5_fk_custom_models_useraccommodatio] FOREIGN KEY([user_accommodation_file_recommendations_option_id])
REFERENCES [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilerecommendations] CHECK CONSTRAINT [custom_models_useraccommodationfilerecomme_user_accommodation_file_recommendations_op_c7c87db5_fk_custom_models_useraccommodatio]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilerecommendations]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfilerecommendations_user_accommodation_file_id_415c3370_fk_custom_models_useraccommodationfile_id] FOREIGN KEY([user_accommodation_file_id])
REFERENCES [dbo].[custom_models_useraccommodationfile] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilerecommendations] CHECK CONSTRAINT [custom_models_useraccommodationfilerecommendations_user_accommodation_file_id_415c3370_fk_custom_models_useraccommodationfile_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfilerecomme_user_accommodation_file_recommendations_op_e3a02c60_fk_custom_models_useraccommodatio] FOREIGN KEY([user_accommodation_file_recommendations_option_id])
REFERENCES [dbo].[custom_models_useraccommodationfilerecommendationsoption] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] CHECK CONSTRAINT [custom_models_useraccommodationfilerecomme_user_accommodation_file_recommendations_op_e3a02c60_fk_custom_models_useraccommodatio]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfilerecommendationsoptiontext_language_id_f314f70e_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilerecommendationsoptiontext] CHECK CONSTRAINT [custom_models_useraccommodationfilerecommendationsoptiontext_language_id_f314f70e_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilestatustext]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfilestatust_user_accommodation_file_status_id_ce8326b2_fk_custom_models_useraccommodatio] FOREIGN KEY([user_accommodation_file_status_id])
REFERENCES [dbo].[custom_models_useraccommodationfilestatus] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilestatustext] CHECK CONSTRAINT [custom_models_useraccommodationfilestatust_user_accommodation_file_status_id_ce8326b2_fk_custom_models_useraccommodatio]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilestatustext]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfilestatustext_language_id_0cc3cd71_fk_custom_models_language_language_id] FOREIGN KEY([language_id])
REFERENCES [dbo].[custom_models_language] ([language_id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfilestatustext] CHECK CONSTRAINT [custom_models_useraccommodationfilestatustext_language_id_0cc3cd71_fk_custom_models_language_language_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfiletesttime]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfiletesttime_test_section_id_1b1c4998_fk_cms_models_testsection_id] FOREIGN KEY([test_section_id])
REFERENCES [dbo].[cms_models_testsection] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfiletesttime] CHECK CONSTRAINT [custom_models_useraccommodationfiletesttime_test_section_id_1b1c4998_fk_cms_models_testsection_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfiletesttime]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfiletesttime_user_accommodation_file_id_b111fe9f_fk_custom_models_useraccommodationfile_id] FOREIGN KEY([user_accommodation_file_id])
REFERENCES [dbo].[custom_models_useraccommodationfile] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfiletesttime] CHECK CONSTRAINT [custom_models_useraccommodationfiletesttime_user_accommodation_file_id_b111fe9f_fk_custom_models_useraccommodationfile_id]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfiletesttoadminister]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfiletesttoa_user_accommodation_file_id_757f8100_fk_custom_models_useraccommodatio] FOREIGN KEY([user_accommodation_file_id])
REFERENCES [dbo].[custom_models_useraccommodationfile] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfiletesttoadminister] CHECK CONSTRAINT [custom_models_useraccommodationfiletesttoa_user_accommodation_file_id_757f8100_fk_custom_models_useraccommodatio]
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfiletesttoadminister]  WITH CHECK ADD  CONSTRAINT [custom_models_useraccommodationfiletesttoadminister_test_id_28ff21a1_fk_cms_models_testdefinition_id] FOREIGN KEY([test_id])
REFERENCES [dbo].[cms_models_testdefinition] ([id])
GO
ALTER TABLE [dbo].[custom_models_useraccommodationfiletesttoadminister] CHECK CONSTRAINT [custom_models_useraccommodationfiletesttoadminister_test_id_28ff21a1_fk_cms_models_testdefinition_id]
GO
ALTER TABLE [dbo].[custom_models_virtualteamsmeetingsession]  WITH CHECK ADD  CONSTRAINT [custom_models_virtualteamsmeetingsession_candidate_id_2afaa609_fk_user_management_models_user_id] FOREIGN KEY([candidate_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_virtualteamsmeetingsession] CHECK CONSTRAINT [custom_models_virtualteamsmeetingsession_candidate_id_2afaa609_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_virtualteamsmeetingsession]  WITH CHECK ADD  CONSTRAINT [custom_models_virtualteamsmeetingsession_test_administrator_id_61ac6b91_fk_user_management_models_user_id] FOREIGN KEY([test_administrator_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[custom_models_virtualteamsmeetingsession] CHECK CONSTRAINT [custom_models_virtualteamsmeetingsession_test_administrator_id_61ac6b91_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[custom_models_virtualteamsmeetingsession]  WITH CHECK ADD  CONSTRAINT [custom_models_virtualteamsmeetingsession_test_session_id_bd248f17_fk_custom_models_testcentertestsessions_id] FOREIGN KEY([test_session_id])
REFERENCES [dbo].[custom_models_testcentertestsessions] ([id])
GO
ALTER TABLE [dbo].[custom_models_virtualteamsmeetingsession] CHECK CONSTRAINT [custom_models_virtualteamsmeetingsession_test_session_id_bd248f17_fk_custom_models_testcentertestsessions_id]
GO
ALTER TABLE [dbo].[custom_models_virtualteamsmeetingsession]  WITH CHECK ADD  CONSTRAINT [custom_models_virtualteamsmeetingsession_virtual_test_room_id_b76eb184_fk_custom_models_virtualtestroom_id] FOREIGN KEY([virtual_test_room_id])
REFERENCES [dbo].[custom_models_virtualtestroom] ([id])
GO
ALTER TABLE [dbo].[custom_models_virtualteamsmeetingsession] CHECK CONSTRAINT [custom_models_virtualteamsmeetingsession_virtual_test_room_id_b76eb184_fk_custom_models_virtualtestroom_id]
GO
ALTER TABLE [dbo].[django_admin_log]  WITH CHECK ADD  CONSTRAINT [django_admin_log_content_type_id_c4bce8eb_fk_django_content_type_id] FOREIGN KEY([content_type_id])
REFERENCES [dbo].[django_content_type] ([id])
GO
ALTER TABLE [dbo].[django_admin_log] CHECK CONSTRAINT [django_admin_log_content_type_id_c4bce8eb_fk_django_content_type_id]
GO
ALTER TABLE [dbo].[django_admin_log]  WITH CHECK ADD  CONSTRAINT [django_admin_log_user_id_c564eba6_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[django_admin_log] CHECK CONSTRAINT [django_admin_log_user_id_c564eba6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask]  WITH CHECK ADD  CONSTRAINT [django_celery_beat_periodictask_clocked_id_47a69f82_fk_django_celery_beat_clockedschedule_id] FOREIGN KEY([clocked_id])
REFERENCES [dbo].[django_celery_beat_clockedschedule] ([id])
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask] CHECK CONSTRAINT [django_celery_beat_periodictask_clocked_id_47a69f82_fk_django_celery_beat_clockedschedule_id]
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask]  WITH CHECK ADD  CONSTRAINT [django_celery_beat_periodictask_crontab_id_d3cba168_fk_django_celery_beat_crontabschedule_id] FOREIGN KEY([crontab_id])
REFERENCES [dbo].[django_celery_beat_crontabschedule] ([id])
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask] CHECK CONSTRAINT [django_celery_beat_periodictask_crontab_id_d3cba168_fk_django_celery_beat_crontabschedule_id]
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask]  WITH CHECK ADD  CONSTRAINT [django_celery_beat_periodictask_interval_id_a8ca27da_fk_django_celery_beat_intervalschedule_id] FOREIGN KEY([interval_id])
REFERENCES [dbo].[django_celery_beat_intervalschedule] ([id])
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask] CHECK CONSTRAINT [django_celery_beat_periodictask_interval_id_a8ca27da_fk_django_celery_beat_intervalschedule_id]
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask]  WITH CHECK ADD  CONSTRAINT [django_celery_beat_periodictask_solar_id_a87ce72c_fk_django_celery_beat_solarschedule_id] FOREIGN KEY([solar_id])
REFERENCES [dbo].[django_celery_beat_solarschedule] ([id])
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask] CHECK CONSTRAINT [django_celery_beat_periodictask_solar_id_a87ce72c_fk_django_celery_beat_solarschedule_id]
GO
ALTER TABLE [dbo].[django_rest_passwordreset_resetpasswordtoken]  WITH CHECK ADD  CONSTRAINT [django_rest_passwordreset_resetpasswordtoken_user_id_e8015b11_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[django_rest_passwordreset_resetpasswordtoken] CHECK CONSTRAINT [django_rest_passwordreset_resetpasswordtoken_user_id_e8015b11_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcateducation]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcateducation_history_user_id_c293ef98_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcateducation] CHECK CONSTRAINT [ref_table_views_historicalcateducation_history_user_id_c293ef98_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefaboriginalvw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcataboriginalvw_history_user_id_8324f28f_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefaboriginalvw] CHECK CONSTRAINT [ref_table_views_historicalcataboriginalvw_history_user_id_8324f28f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefclassificationvw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatrefclassificationvw_history_user_id_2483771d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefclassificationvw] CHECK CONSTRAINT [ref_table_views_historicalcatrefclassificationvw_history_user_id_2483771d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefcountryvw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatrefcountryvw_history_user_id_3ee9baaa_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefcountryvw] CHECK CONSTRAINT [ref_table_views_historicalcatrefcountryvw_history_user_id_3ee9baaa_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefdayofweekvw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatrefdayofweekvw_history_user_id_86fc7d9f_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefdayofweekvw] CHECK CONSTRAINT [ref_table_views_historicalcatrefdayofweekvw_history_user_id_86fc7d9f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefdepartmentsvw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatrefdepartmentsvw_history_user_id_c317800b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefdepartmentsvw] CHECK CONSTRAINT [ref_table_views_historicalcatrefdepartmentsvw_history_user_id_c317800b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefdisabilityvw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatdisabilityvw_history_user_id_2d889408_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefdisabilityvw] CHECK CONSTRAINT [ref_table_views_historicalcatdisabilityvw_history_user_id_2d889408_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefemployerstsvw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatrefemployerstsvw_history_user_id_0a65cee4_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefemployerstsvw] CHECK CONSTRAINT [ref_table_views_historicalcatrefemployerstsvw_history_user_id_0a65cee4_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefgendervw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicaloltfgendervw_history_user_id_74ac4795_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefgendervw] CHECK CONSTRAINT [ref_table_views_historicaloltfgendervw_history_user_id_74ac4795_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatreflanguagevw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatreflanguagevw_history_user_id_5405333a_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatreflanguagevw] CHECK CONSTRAINT [ref_table_views_historicalcatreflanguagevw_history_user_id_5405333a_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefprovincevw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatrefprovincevw_history_user_id_aee32566_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefprovincevw] CHECK CONSTRAINT [ref_table_views_historicalcatrefprovincevw_history_user_id_aee32566_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefresidencevw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatrefresidencevw_history_user_id_de109b1f_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefresidencevw] CHECK CONSTRAINT [ref_table_views_historicalcatrefresidencevw_history_user_id_de109b1f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefvisibleminorityvw]  WITH CHECK ADD  CONSTRAINT [ref_table_views_historicalcatvisibleminorityvw_history_user_id_9012a568_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[ref_table_views_historicalcatrefvisibleminorityvw] CHECK CONSTRAINT [ref_table_views_historicalcatvisibleminorityvw_history_user_id_9012a568_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_accommodations]  WITH CHECK ADD  CONSTRAINT [user_management_models_accommodations_user_id_5af49f7b_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_accommodations] CHECK CONSTRAINT [user_management_models_accommodations_user_id_5af49f7b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_custompermission2fa]  WITH CHECK ADD  CONSTRAINT [user_management_models_custompermission2fa_custom_permission_id_9c60927f_fk_user_management_models_customp] FOREIGN KEY([custom_permission_id])
REFERENCES [dbo].[user_management_models_custompermissions] ([permission_id])
GO
ALTER TABLE [dbo].[user_management_models_custompermission2fa] CHECK CONSTRAINT [user_management_models_custompermission2fa_custom_permission_id_9c60927f_fk_user_management_models_customp]
GO
ALTER TABLE [dbo].[user_management_models_custompermissions]  WITH CHECK ADD  CONSTRAINT [user_management_models_custompermissions_content_type_id_e81880f7_fk_django_content_type_id] FOREIGN KEY([content_type_id])
REFERENCES [dbo].[django_content_type] ([id])
GO
ALTER TABLE [dbo].[user_management_models_custompermissions] CHECK CONSTRAINT [user_management_models_custompermissions_content_type_id_e81880f7_fk_django_content_type_id]
GO
ALTER TABLE [dbo].[user_management_models_customuserpermissions]  WITH CHECK ADD  CONSTRAINT [user_management_models_customuserpermissions_permission_id_e289ff04_fk_user_management_models_custompermissions_permission_id] FOREIGN KEY([permission_id])
REFERENCES [dbo].[user_management_models_custompermissions] ([permission_id])
GO
ALTER TABLE [dbo].[user_management_models_customuserpermissions] CHECK CONSTRAINT [user_management_models_customuserpermissions_permission_id_e289ff04_fk_user_management_models_custompermissions_permission_id]
GO
ALTER TABLE [dbo].[user_management_models_customuserpermissions]  WITH CHECK ADD  CONSTRAINT [user_management_models_customuserpermissions_user_id_9e1b2b30_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_customuserpermissions] CHECK CONSTRAINT [user_management_models_customuserpermissions_user_id_9e1b2b30_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicalaccommodations]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicalaccommodations_history_user_id_c359668c_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicalaccommodations] CHECK CONSTRAINT [user_management_models_historicalaccommodations_history_user_id_c359668c_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicalcustompermission2fa]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicalcustompermission2fa_history_user_id_0de1935b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicalcustompermission2fa] CHECK CONSTRAINT [user_management_models_historicalcustompermission2fa_history_user_id_0de1935b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicalcustompermissions]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicalcustompermissions_history_user_id_86d03e32_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicalcustompermissions] CHECK CONSTRAINT [user_management_models_historicalcustompermissions_history_user_id_86d03e32_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicalcustomuserpermissions]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicalcustomuserpermissions_history_user_id_8b8967d6_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicalcustomuserpermissions] CHECK CONSTRAINT [user_management_models_historicalcustomuserpermissions_history_user_id_8b8967d6_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaleeinfooptions]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaleeinfooptions_history_user_id_6c433a59_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaleeinfooptions] CHECK CONSTRAINT [user_management_models_historicaleeinfooptions_history_user_id_6c433a59_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicalpermissionrequest]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicalpermissionrequest_history_user_id_78f13c90_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicalpermissionrequest] CHECK CONSTRAINT [user_management_models_historicalpermissionrequest_history_user_id_78f13c90_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaltaextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaltaextendedprofile_history_user_id_e7baf289_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaltaextendedprofile] CHECK CONSTRAINT [user_management_models_historicaltaextendedprofile_history_user_id_e7baf289_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluser]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluser_history_user_id_470afeb5_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluser] CHECK CONSTRAINT [user_management_models_historicaluser_history_user_id_470afeb5_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluser2fatracking]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluser2fatracking_history_user_id_5a46b6a2_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluser2fatracking] CHECK CONSTRAINT [user_management_models_historicaluser2fatracking_history_user_id_5a46b6a2_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluseraccommodationsprofile_history_user_id_606a121e_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofile] CHECK CONSTRAINT [user_management_models_historicaluseraccommodationsprofile_history_user_id_606a121e_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileaccessassistance]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluseraccom_history_user_id_9a14d5c8_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileaccessassistance] CHECK CONSTRAINT [user_management_models_historicaluseraccom_history_user_id_9a14d5c8_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileadaptivetech]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluseraccom_history_user_id_33a1339d_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileadaptivetech] CHECK CONSTRAINT [user_management_models_historicaluseraccom_history_user_id_33a1339d_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofilebreaks]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluseraccommodationsprofilebreaks_history_user_id_087e6945_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofilebreaks] CHECK CONSTRAINT [user_management_models_historicaluseraccommodationsprofilebreaks_history_user_id_087e6945_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofilebuiltinaccessibility]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluseraccom_history_user_id_757c2581_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofilebuiltinaccessibility] CHECK CONSTRAINT [user_management_models_historicaluseraccom_history_user_id_757c2581_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileenvironment]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluseraccommodationsprofileenvironment_history_user_id_19dae965_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileenvironment] CHECK CONSTRAINT [user_management_models_historicaluseraccommodationsprofileenvironment_history_user_id_19dae965_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileergonomic]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluseraccommodationsprofileergonomic_history_user_id_67da663f_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileergonomic] CHECK CONSTRAINT [user_management_models_historicaluseraccommodationsprofileergonomic_history_user_id_67da663f_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileextratime]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluseraccommodationsprofileextratime_history_user_id_2b4c4fe9_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileextratime] CHECK CONSTRAINT [user_management_models_historicaluseraccommodationsprofileextratime_history_user_id_2b4c4fe9_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileotherneedsoral]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluseraccom_history_user_id_90361bff_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileotherneedsoral] CHECK CONSTRAINT [user_management_models_historicaluseraccom_history_user_id_90361bff_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileotherneedswritten]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluseraccommodationsprofileotherneeds_history_user_id_c5420c79_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileotherneedswritten] CHECK CONSTRAINT [user_management_models_historicaluseraccommodationsprofileotherneeds_history_user_id_c5420c79_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofilepreviouslyaccommodated]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluseraccom_history_user_id_29634320_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofilepreviouslyaccommodated] CHECK CONSTRAINT [user_management_models_historicaluseraccom_history_user_id_29634320_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofilerepeatedoralquestions]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluseraccom_history_user_id_1936f0d9_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofilerepeatedoralquestions] CHECK CONSTRAINT [user_management_models_historicaluseraccom_history_user_id_1936f0d9_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileschedulingadaptation]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluseraccom_history_user_id_6ff7d882_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluseraccommodationsprofileschedulingadaptation] CHECK CONSTRAINT [user_management_models_historicaluseraccom_history_user_id_6ff7d882_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluserextendedprofile_history_user_id_3793f527_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofile] CHECK CONSTRAINT [user_management_models_historicaluserextendedprofile_history_user_id_3793f527_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofileaboriginal]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluserextendedprofileaboriginal_history_user_id_e07411e0_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofileaboriginal] CHECK CONSTRAINT [user_management_models_historicaluserextendedprofileaboriginal_history_user_id_e07411e0_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofiledisability]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluserextendedprofiledisability_history_user_id_77c65c1b_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofiledisability] CHECK CONSTRAINT [user_management_models_historicaluserextendedprofiledisability_history_user_id_77c65c1b_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofilevisibleminority]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluserextendedprofilevisibleminority_history_user_id_36d81667_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluserextendedprofilevisibleminority] CHECK CONSTRAINT [user_management_models_historicaluserextendedprofilevisibleminority_history_user_id_36d81667_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluserpasswordresettracking]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluserpasswordresettracking_history_user_id_ed693536_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluserpasswordresettracking] CHECK CONSTRAINT [user_management_models_historicaluserpasswordresettracking_history_user_id_ed693536_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_historicaluserprofilechangerequest]  WITH CHECK ADD  CONSTRAINT [user_management_models_historicaluserprofilechangerequest_history_user_id_22f23c73_fk_user_management_models_user_id] FOREIGN KEY([history_user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_historicaluserprofilechangerequest] CHECK CONSTRAINT [user_management_models_historicaluserprofilechangerequest_history_user_id_22f23c73_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_permissionrequest]  WITH CHECK ADD  CONSTRAINT [user_management_models_permissionrequest_permission_requested_id_d064f657_fk_user_management_models_customp] FOREIGN KEY([permission_requested_id])
REFERENCES [dbo].[user_management_models_custompermissions] ([permission_id])
GO
ALTER TABLE [dbo].[user_management_models_permissionrequest] CHECK CONSTRAINT [user_management_models_permissionrequest_permission_requested_id_d064f657_fk_user_management_models_customp]
GO
ALTER TABLE [dbo].[user_management_models_permissionrequest]  WITH CHECK ADD  CONSTRAINT [user_management_models_permissionrequest_user_id_c19c5bb7_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_permissionrequest] CHECK CONSTRAINT [user_management_models_permissionrequest_user_id_c19c5bb7_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_taextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_taextendedprofile_user_id_4783af64_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_taextendedprofile] CHECK CONSTRAINT [user_management_models_taextendedprofile_user_id_4783af64_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_user_groups]  WITH CHECK ADD  CONSTRAINT [user_management_models_user_groups_group_id_315780fa_fk_auth_group_id] FOREIGN KEY([group_id])
REFERENCES [dbo].[auth_group] ([id])
GO
ALTER TABLE [dbo].[user_management_models_user_groups] CHECK CONSTRAINT [user_management_models_user_groups_group_id_315780fa_fk_auth_group_id]
GO
ALTER TABLE [dbo].[user_management_models_user_groups]  WITH CHECK ADD  CONSTRAINT [user_management_models_user_groups_user_id_87f76697_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_user_groups] CHECK CONSTRAINT [user_management_models_user_groups_user_id_87f76697_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_user_user_permissions]  WITH CHECK ADD  CONSTRAINT [user_management_models_user_user_permissions_permission_id_c6b7b475_fk_auth_permission_id] FOREIGN KEY([permission_id])
REFERENCES [dbo].[auth_permission] ([id])
GO
ALTER TABLE [dbo].[user_management_models_user_user_permissions] CHECK CONSTRAINT [user_management_models_user_user_permissions_permission_id_c6b7b475_fk_auth_permission_id]
GO
ALTER TABLE [dbo].[user_management_models_user_user_permissions]  WITH CHECK ADD  CONSTRAINT [user_management_models_user_user_permissions_user_id_663216bc_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_user_user_permissions] CHECK CONSTRAINT [user_management_models_user_user_permissions_user_id_663216bc_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_user2fatracking]  WITH CHECK ADD  CONSTRAINT [user_management_models_user2fatracking_user_id_5831497c_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_user2fatracking] CHECK CONSTRAINT [user_management_models_user2fatracking_user_id_5831497c_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_useraccommodationsprofile_user_id_1fd25caf_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofile] CHECK CONSTRAINT [user_management_models_useraccommodationsprofile_user_id_1fd25caf_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileaccessassistance]  WITH CHECK ADD  CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_823c3112_fk_user_management_models_useracc] FOREIGN KEY([user_accommodations_profile_id])
REFERENCES [dbo].[user_management_models_useraccommodationsprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileaccessassistance] CHECK CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_823c3112_fk_user_management_models_useracc]
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileadaptivetech]  WITH CHECK ADD  CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_b6d6cc03_fk_user_management_models_useracc] FOREIGN KEY([user_accommodations_profile_id])
REFERENCES [dbo].[user_management_models_useraccommodationsprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileadaptivetech] CHECK CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_b6d6cc03_fk_user_management_models_useracc]
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofilebreaks]  WITH CHECK ADD  CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_ab03c2a5_fk_user_management_models_useracc] FOREIGN KEY([user_accommodations_profile_id])
REFERENCES [dbo].[user_management_models_useraccommodationsprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofilebreaks] CHECK CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_ab03c2a5_fk_user_management_models_useracc]
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofilebuiltinaccessibility]  WITH CHECK ADD  CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_79e25995_fk_user_management_models_useracc] FOREIGN KEY([user_accommodations_profile_id])
REFERENCES [dbo].[user_management_models_useraccommodationsprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofilebuiltinaccessibility] CHECK CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_79e25995_fk_user_management_models_useracc]
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileenvironment]  WITH CHECK ADD  CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_65d28ac8_fk_user_management_models_useracc] FOREIGN KEY([user_accommodations_profile_id])
REFERENCES [dbo].[user_management_models_useraccommodationsprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileenvironment] CHECK CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_65d28ac8_fk_user_management_models_useracc]
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileergonomic]  WITH CHECK ADD  CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_27c7bb7c_fk_user_management_models_useracc] FOREIGN KEY([user_accommodations_profile_id])
REFERENCES [dbo].[user_management_models_useraccommodationsprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileergonomic] CHECK CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_27c7bb7c_fk_user_management_models_useracc]
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileextratime]  WITH CHECK ADD  CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_26025fad_fk_user_management_models_useracc] FOREIGN KEY([user_accommodations_profile_id])
REFERENCES [dbo].[user_management_models_useraccommodationsprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileextratime] CHECK CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_26025fad_fk_user_management_models_useracc]
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileotherneedsoral]  WITH CHECK ADD  CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_9d5d8d45_fk_user_management_models_useracc] FOREIGN KEY([user_accommodations_profile_id])
REFERENCES [dbo].[user_management_models_useraccommodationsprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileotherneedsoral] CHECK CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_9d5d8d45_fk_user_management_models_useracc]
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileotherneedswritten]  WITH CHECK ADD  CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_e714ff5c_fk_user_management_models_useracc] FOREIGN KEY([user_accommodations_profile_id])
REFERENCES [dbo].[user_management_models_useraccommodationsprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileotherneedswritten] CHECK CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_e714ff5c_fk_user_management_models_useracc]
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofilepreviouslyaccommodated]  WITH CHECK ADD  CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_8c70e5b7_fk_user_management_models_useracc] FOREIGN KEY([user_accommodations_profile_id])
REFERENCES [dbo].[user_management_models_useraccommodationsprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofilepreviouslyaccommodated] CHECK CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_8c70e5b7_fk_user_management_models_useracc]
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofilerepeatedoralquestions]  WITH CHECK ADD  CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_ece62575_fk_user_management_models_useracc] FOREIGN KEY([user_accommodations_profile_id])
REFERENCES [dbo].[user_management_models_useraccommodationsprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofilerepeatedoralquestions] CHECK CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_ece62575_fk_user_management_models_useracc]
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileschedulingadaptation]  WITH CHECK ADD  CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_4dce0738_fk_user_management_models_useracc] FOREIGN KEY([user_accommodations_profile_id])
REFERENCES [dbo].[user_management_models_useraccommodationsprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_useraccommodationsprofileschedulingadaptation] CHECK CONSTRAINT [user_management_models_useraccommodationsp_user_accommodations_profile_id_4dce0738_fk_user_management_models_useracc]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_aboriginal_id_2706213d_fk_user_management_models_eeinfooptions_opt_id] FOREIGN KEY([aboriginal_id])
REFERENCES [dbo].[user_management_models_eeinfooptions] ([opt_id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile] CHECK CONSTRAINT [user_management_models_userextendedprofile_aboriginal_id_2706213d_fk_user_management_models_eeinfooptions_opt_id]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_disability_id_34b76084_fk_user_management_models_eeinfooptions_opt_id] FOREIGN KEY([disability_id])
REFERENCES [dbo].[user_management_models_eeinfooptions] ([opt_id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile] CHECK CONSTRAINT [user_management_models_userextendedprofile_disability_id_34b76084_fk_user_management_models_eeinfooptions_opt_id]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_identify_as_woman_id_4a2e3adf_fk_user_management_models_eeinfooptions_opt_id] FOREIGN KEY([identify_as_woman_id])
REFERENCES [dbo].[user_management_models_eeinfooptions] ([opt_id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile] CHECK CONSTRAINT [user_management_models_userextendedprofile_identify_as_woman_id_4a2e3adf_fk_user_management_models_eeinfooptions_opt_id]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_user_id_01a1740c_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile] CHECK CONSTRAINT [user_management_models_userextendedprofile_user_id_01a1740c_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_visible_minority_id_67436112_fk_user_management_models_eeinfooptions_opt_id] FOREIGN KEY([visible_minority_id])
REFERENCES [dbo].[user_management_models_eeinfooptions] ([opt_id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofile] CHECK CONSTRAINT [user_management_models_userextendedprofile_visible_minority_id_67436112_fk_user_management_models_eeinfooptions_opt_id]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofileaboriginal]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_user_extended_profile_id_b182177f_fk_user_management_models_userext] FOREIGN KEY([user_extended_profile_id])
REFERENCES [dbo].[user_management_models_userextendedprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofileaboriginal] CHECK CONSTRAINT [user_management_models_userextendedprofile_user_extended_profile_id_b182177f_fk_user_management_models_userext]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofiledisability]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_user_extended_profile_id_602f0aa4_fk_user_management_models_userext] FOREIGN KEY([user_extended_profile_id])
REFERENCES [dbo].[user_management_models_userextendedprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofiledisability] CHECK CONSTRAINT [user_management_models_userextendedprofile_user_extended_profile_id_602f0aa4_fk_user_management_models_userext]
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofilevisibleminority]  WITH CHECK ADD  CONSTRAINT [user_management_models_userextendedprofile_user_extended_profile_id_631fa36c_fk_user_management_models_userext] FOREIGN KEY([user_extended_profile_id])
REFERENCES [dbo].[user_management_models_userextendedprofile] ([id])
GO
ALTER TABLE [dbo].[user_management_models_userextendedprofilevisibleminority] CHECK CONSTRAINT [user_management_models_userextendedprofile_user_extended_profile_id_631fa36c_fk_user_management_models_userext]
GO
ALTER TABLE [dbo].[user_management_models_userpasswordresettracking]  WITH CHECK ADD  CONSTRAINT [user_management_models_userpasswordresettracking_user_id_5fa82965_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_userpasswordresettracking] CHECK CONSTRAINT [user_management_models_userpasswordresettracking_user_id_5fa82965_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[user_management_models_userprofilechangerequest]  WITH CHECK ADD  CONSTRAINT [user_management_models_userprofilechangerequest_user_id_782279f4_fk_user_management_models_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user_management_models_user] ([id])
GO
ALTER TABLE [dbo].[user_management_models_userprofilechangerequest] CHECK CONSTRAINT [user_management_models_userprofilechangerequest_user_id_782279f4_fk_user_management_models_user_id]
GO
ALTER TABLE [dbo].[cms_models_bundles]  WITH CHECK ADD  CONSTRAINT [shuffle_bundles_and_shuffle_between_bundles_are_mutually_exclusive] CHECK  (([shuffle_between_bundles]=(0) AND [shuffle_bundles]=(1) OR [shuffle_between_bundles]=(1) AND [shuffle_bundles]=(0) OR [shuffle_between_bundles]=(0) AND [shuffle_bundles]=(0)))
GO
ALTER TABLE [dbo].[cms_models_bundles] CHECK CONSTRAINT [shuffle_bundles_and_shuffle_between_bundles_are_mutually_exclusive]
GO
ALTER TABLE [dbo].[django_admin_log]  WITH CHECK ADD  CONSTRAINT [django_admin_log_action_flag_a8637d59_check] CHECK  (([action_flag]>=(0)))
GO
ALTER TABLE [dbo].[django_admin_log] CHECK CONSTRAINT [django_admin_log_action_flag_a8637d59_check]
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask]  WITH CHECK ADD CHECK  (([expire_seconds]>=(0)))
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask]  WITH CHECK ADD CHECK  (([priority]>=(0)))
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask]  WITH CHECK ADD  CONSTRAINT [django_celery_beat_periodictask_total_run_count_cf45f5ae_check] CHECK  (([total_run_count]>=(0)))
GO
ALTER TABLE [dbo].[django_celery_beat_periodictask] CHECK CONSTRAINT [django_celery_beat_periodictask_total_run_count_cf45f5ae_check]
GO
ALTER TABLE [dbo].[user_management_models_customuserpermissions]  WITH CHECK ADD  CONSTRAINT [pri_and_military_nbr_at_least_1_is_not_empty_in_user_permissions] CHECK  (([military_nbr]='' AND [pri]>'' OR [military_nbr]>'' AND [pri]='' OR [military_nbr]>'' AND [pri]>''))
GO
ALTER TABLE [dbo].[user_management_models_customuserpermissions] CHECK CONSTRAINT [pri_and_military_nbr_at_least_1_is_not_empty_in_user_permissions]
GO
ALTER TABLE [dbo].[user_management_models_permissionrequest]  WITH CHECK ADD  CONSTRAINT [pri_and_military_nbr_at_least_1_is_not_empty_in_permission_request] CHECK  (([military_nbr]='' AND [pri]>'' OR [military_nbr]>'' AND [pri]='' OR [military_nbr]>'' AND [pri]>''))
GO
ALTER TABLE [dbo].[user_management_models_permissionrequest] CHECK CONSTRAINT [pri_and_military_nbr_at_least_1_is_not_empty_in_permission_request]
GO
