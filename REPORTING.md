# How to generate Sonatype, Snyk and Accessibility reports

## Sonatype:

See [SONATYPE-REPORT](SONATYPE-REPORT.md)

---

## Snyk:

See [SNYK-REPORT](SNYK-REPORT.md)

---

## Accessibility Tool (pa11y):

This script will generate all the accessibility reports, meaning one for each URL in the application

**Note that the frontend container must be running to be able to execute the following commands properly:**

In gitbash:

```shell
./run-accessibility-check.sh
```

In powershell:

```shell
.\run-accessibility-check.sh
```

These reports will be created and saved in _reports_ folder under _./project-thundercat/_.
