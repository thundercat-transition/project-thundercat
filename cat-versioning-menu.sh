#!/bin/bash
source .env

function exit_message () {
	echo "-----------------------------------------------------------"
	echo "JOB FINISHED SUCCESSFULLY"
	echo "-----------------------------------------------------------"
	echo
	echo "Press Enter to Close This Window"

	read
	exit
}

function modify_package_json () {
	echo "-----------------------------------------------------------"
	echo "Modify frontend/package.json with version: $VERSION"
	echo "-----------------------------------------------------------"
	# Verify if jq is installed
	echo "Verifying if jq is installed..."
	if ! [ -x "$(command -v jq)" ]; then
		echo 'Error: jq is not installed.' >&2
		sleep 2s
		echo 'Install jq...'
		#Install jq
		curl -L -o /usr/bin/jq.exe https://github.com/stedolan/jq/releases/latest/download/jq-win64.exe
		jq --version
		sleep 2s
	fi
	echo "OK"
	sleep 1s

	echo "Modifying frontend/package.json..."
	echo $(variable=$VERSION; jq --arg variable "$variable" '.version = $variable' $(dirname "$0")/frontend/package.json) > frontend/package.json
	echo "OK"
	sleep 5s

	# Next Step
	exit_message
}

function create_and_push_mssql_image () {
	# Create MSSQL Image
	echo "-----------------------------------------------------------"
	echo "Creating New MSSQL Image with version: $VERSION"
	echo "-----------------------------------------------------------"
	cd $(dirname "$0")

	echo "Docker Login"
	# Login with username/password from our .env file
	echo $DOCKER_HUB_PASSWORD | docker login -u $DOCKER_HUB_USERNAME --password-stdin
	sleep 1s

	echo "Build CAT-MSSQL"
	docker build -f Dockerfile_Create_Image_mssql -t $DOCKER_HUB_PROJECT/$DOCKER_HUB_MSSQL_IMAGE .
	sleep 2s

	echo "Push CAT-MSSQL on Docker with our Version as the Tag"
	echo $DOCKER_HUB_PROJECT
	echo $DOCKER_HUB_MSSQL_IMAGE
	# Create Tag
	docker image tag $DOCKER_HUB_PROJECT/$DOCKER_HUB_MSSQL_IMAGE $DOCKER_HUB_PROJECT/$DOCKER_HUB_MSSQL_IMAGE:$VERSION
	# Push the Tagged Image
	docker image push $DOCKER_HUB_PROJECT/$DOCKER_HUB_MSSQL_IMAGE:$VERSION
	sleep 5s

	# Next Step
	modify_package_json
}

# list of date tags that won't be deleted
function get_list_of_dates () {
	local start_date=$1
	local end_date=$2
	local dates=("$end_date")

	while [[ "$start_date" != "$end_date" ]]; do
		dates+=("$start_date")
		start_date=$(date --date "$start_date + 1 day" +"%Y%m%d")
	done

	#return list
	echo ${dates[@]}
}

# cleans a variable from invisible characters
function clean() {
    local a=${1//[^[:alnum:]]/}
    echo "${a,,}"
}

# deletes tags that are older than 10 days old
function delete_old_tags() {
	echo "-----------------------------------------------------------"
	echo "Get DockerHub Token by Loggin in"
	echo "-----------------------------------------------------------"
	USER=$DOCKER_HUB_USERNAME
	PASS=$DOCKER_HUB_PASSWORD
	IMAGE=$DOCKER_HUB_MSSQL_IMAGE

	TOKEN=$(\
	curl \
	--silent \
	--header "Content-Type: application/json" \
	--request POST \
	--data '{"username": "'${USER}'", "password": "'${PASS}'"}' \
	${DOCKER_HUB_REPO}/users/login/ \
	| jq -r .token\
	)

	echo
	echo OK - Token Length: ${#TOKEN}
	echo


	echo "-----------------------------------------------------------"
	echo "Get All TAGs for MSSQL Image and Delete 10 days older"
	echo "-----------------------------------------------------------"

	# set variables
	TODAY_DATE=$(date +%Y%m%d)
	START_DATE=$(date +%Y%m%d -d "10 day ago")
	# -------

	# print variables
	echo "Today's date: $TODAY_DATE"
	echo
	echo "Date 10 days ago: $START_DATE"
	echo

	# generate list of dates from 10 days ago to today
	LIST_OF_DATES=$(get_list_of_dates "$START_DATE" "$TODAY_DATE")

	# output tags for cat-mssql
	IMAGE_TAGS=$(curl -s -H "Authorization: JWT ${TOKEN}" https://hub.docker.com/v2/repositories/projectthundercat/cat-mssql/tags/?page_size=100 | jq -r '.results|.[]|.name')
	for TAG in ${IMAGE_TAGS}
		do
		echo "  - ${TAG}"
		# Look if we have a Release_
		if [[ "${TAG}" == *"Release_"* ]]; then
		continue
		fi
		# Look if we have latest
		if [[ "${TAG}" == *"latest"* ]]; then
		continue
		fi
		# Verify if TAG in last 10 days
		to_delete=true
		for date in $LIST_OF_DATES;
			do
			if [[ "${TAG}" == *"${date}"* ]]; then
				to_delete=false
			fi
		done

		if ($to_delete); then
		echo "     - Deleting..."

		# There were some invisible characters in TAG, therefore, we had to clean them up
		CLEAN_TAG=$(clean $TAG)

		# Generating the URL
		URL="${DOCKER_HUB_REPO}/repositories/${DOCKER_HUB_PROJECT}/${DOCKER_HUB_MSSQL_IMAGE}/tags/${CLEAN_TAG}"

		response= $(curl -X DELETE -H "Authorization: JWT ${TOKEN}" -H "Accept: application/json" "${URL}")
		echo $response

		fi
	done

	# Next Step
	create_and_push_mssql_image
}

# User Specifies the Version
function specify_version () {
	echo "-----------------------------------------------------------"
	echo "CAT RELEASE: Specify your custom version"
	echo "-----------------------------------------------------------"

	# Make the user start with Release_
	read -p "Release_" specified_version
	VERSION="Release_"$specified_version

	# Next Step
	delete_old_tags
}

# Show menu
function show_menu () {
	echo 'CAT Versioning Menu: '

	PS3='Choose the option: '
	menuItems=("Release Version" "Auto-generate Version" "Quit")
	select menuItem in "${menuItems[@]}"; do
		case $menuItem in
			"Release Version")
				# Function that asks for a version
				specify_version
				;;
			"Auto-generate Version")
				# Function that creates directly the new MSSQL Image + TAG
				create_and_push_mssql_image
			break
				;;
		"Quit")
			echo "User requested exit"
			exit_message
			;;
			*) echo "invalid option $REPLY";;
		esac
	done
}

VERSION=$(date '+%Y%m%d%H%M%S');

# Exits the execution if there is an error
set -e

# Executes different functions when giving an argument to running this script
case $1 in
	auto)
		create_and_push_mssql_image
		;;
	release)
		specify_version
		;;
  	*)
    	show_menu
    	;;
esac



