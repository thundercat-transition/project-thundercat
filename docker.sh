#!/bin/bash
case "$1" in
    "restart")
        sh docker-trigger.sh down c up
    ;;
    "rebuild")
        sh docker-trigger.sh down c build
    ;;
    "purge")
        sh docker-trigger.sh down s i c build
    ;;
    "nuke")
        sh docker-trigger.sh down s i v c build
    ;;
    "orbital-strike")
        sh docker-trigger.sh down s i v vd c build
    ;;
    "help")
        help_text
    ;;
    "h")
        help_text
    ;;
    *)
        echo "unknown command $1"
        help_text
    ;;
esac

# ===============
# Functions
# ===============

# Prints the help dialog
function help_text()
{
    echo "
        restart: docker compose down; clear; docker-compose up
        rebuild: docker compose down; clear; docker-compose up --build
        purge: docker compose down; docker system prune -f; docker image prune -a -f; clear; docker-compose up --build
        nuke: docker compose down; docker system prune -f; docker image prune -a -f; docker volume prune -f; clear; docker-compose up --build
        orbital-strike: nuke that first deletes C:/MSSQL/DockerFiles directory
        help: show help text
        h: show help text
    "
}
