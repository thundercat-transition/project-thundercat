import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from common.pages.login import Login
from common.web_driver_util import init_web_driver


class CAT618(unittest.TestCase):
    """https://cfp-psc.atlassian.net/browse/CAT-618"""

    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()

    def test_login_and_create_account_tabs_exist(self):
        Login(self.driver)

        # verify tabs exist
        self.assertEqual(
            len(self.driver.find_elements(By.CSS_SELECTOR, ".nav-tabs .nav-item")), 2
        )

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
