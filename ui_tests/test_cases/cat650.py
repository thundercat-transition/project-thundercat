import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from common.pages.login import Login
from common.web_driver_util import init_web_driver


class CAT650(unittest.TestCase):
    """https://cfp-psc.atlassian.net/browse/CAT-650"""

    # Sets up the driver and to maximazes the window.
    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()

    # This clicks the button and makes sure the setting tools is viewable.
    def test_gear_button(self):
        login = Login(self.driver)
        # This assert checks that the button for the settings is there.
        self.assertTrue(
            self.driver.find_element(
                By.XPATH,
                "//*[@id='root']/div/div[1]/div/div/div[1]/div/div/div/div/nav/div[2]/div/div[2]/button",
            )
        )
        login.settings_button()
        # This assert checks to see if Setting tool shows up.
        self.assertTrue(self.driver.find_element(By.XPATH, "//*[@id='modal-heading']"))

    # Quits the program.
    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
