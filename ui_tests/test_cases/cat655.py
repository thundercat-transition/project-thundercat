import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from selenium.webdriver.common.by import By
from common.pages.login import Login
from common.web_driver_util import init_web_driver, settings


class CAT655(unittest.TestCase):
    """https://cfp-psc.atlassian.net/browse/CAT-655"""

    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()
        self.user = settings["candidate"]

    def test_sample_test_instructions(self):
        login = Login(self.driver)

        homepage = login.login_valid_user(self.user["email"], self.user["password"])
        sample_test = homepage.sample_tests_button()
        sample_emib = sample_test.select_test("Sample e-MIB")
        sample_emib.proceed_to_test()

        sample_emib.nav_instructions(0)

        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(
                (By.ID, "left-tabs-navigation-tabpane-event_key_1")
            )
        )

        self.assertTrue(
            self.driver.find_element(
                By.ID, "left-tabs-navigation-tabpane-event_key_1"
            ).is_displayed(),
            "Instructions not loaded",
        )

        sample_emib.nav_instructions(1)

        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(
                (By.ID, "left-tabs-navigation-tabpane-event_key_2")
            )
        )

        self.assertTrue(
            self.driver.find_element(
                By.ID, "left-tabs-navigation-tabpane-event_key_2"
            ).is_displayed(),
            "Test Tips not loaded",
        )

        sample_emib.nav_instructions(2)

        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(
                (By.ID, "left-tabs-navigation-tabpane-event_key_3")
            )
        )

        self.assertTrue(
            self.driver.find_element(
                By.ID, "left-tabs-navigation-tabpane-event_key_3"
            ).is_displayed(),
            "Evaluation not loaded",
        )

        sample_emib.nav_instructions(0)
        sample_emib.goto("Next page")

        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(
                (By.ID, "left-tabs-navigation-tabpane-event_key_2")
            )
        )

        self.assertTrue(
            self.driver.find_element(
                By.ID, "left-tabs-navigation-tabpane-event_key_2"
            ).is_displayed(),
            "Test Tips not loaded",
        )

        sample_emib.goto("Previous page")

        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(
                (By.ID, "left-tabs-navigation-tabpane-event_key_1")
            )
        )

        self.assertTrue(
            self.driver.find_element(
                By.ID, "left-tabs-navigation-tabpane-event_key_1"
            ).is_displayed(),
            "Instructions not loaded",
        )

        sample_emib.start_test_popup()

        self.assertTrue(
            self.driver.find_element(By.ID, "modal-heading").is_displayed(),
            "Start test popup not loaded",
        )

        sample_emib.cancel_test_popup()

    def validate_text(self, text):
        content = self.driver.page_source
        return content.find(text)

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()

