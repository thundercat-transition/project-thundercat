import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from common.pages.login import Login
from common.web_driver_util import init_web_driver, settings


class CAT646(unittest.TestCase):
    """https://cfp-psc.atlassian.net/browse/CAT-646"""

    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()
        self.user = settings["candidate"]

    def test_notepad_and_responses_are_cleared(self):
        login = Login(self.driver)

        # login and navigate to Sample e-MIB
        homepage = login.login_valid_user(self.user["email"], self.user["password"])
        sample_tests = homepage.sample_tests_button()
        sample_emib = sample_tests.select_test("Sample e-MIB")

        # perform specified actions on user one
        sample_emib.proceed_to_test()
        sample_emib.start_test()

        sample_emib.goto("In-Box")
        sample_emib.write_to_notepad("I am the first person here.")

        # add email responses and task lists to the FIRST email
        sample_emib.add_email_response()
        sample_emib.type_email_response("Hello, world!")
        sample_emib.type_email_reasoning("It seemed like the right thing to do.")
        sample_emib.save_response()

        sample_emib.task_list("add")
        sample_emib.task_list("open", number=0)
        sample_emib.task_list("write", "This is a response.", number=0)
        sample_emib.task_list("write", "This is my reasoning.", number=1)
        sample_emib.task_list("save")

        sample_emib.add_email_response()
        sample_emib.type_email_response("foobar")
        sample_emib.cancel_response()

        sample_tests = sample_emib.submit_test()
        sample_tests.logout()

        # login with a second user and navigate to Sample e-MIB
        login.select_language()
        homepage = login.login_valid_user(self.user["email"], self.user["password"])
        sample_tests = homepage.sample_tests_button()
        sample_emib = sample_tests.select_test("Sample e-MIB")

        sample_emib.proceed_to_test()
        sample_emib.start_test()

        # verify notepad is empty
        self.assertEqual(sample_emib.get_notepad_text(), "")

        # verify each email is unread
        NUM_EMAILS = 3
        unread_emails = self.driver.find_elements(
            By.CSS_SELECTOR, "#read-email-preview"
        )
        self.assertEqual(len(unread_emails), NUM_EMAILS)

        # verify there are not email responses or task lists on the FIRST email
        container = self.driver.find_element(
            By.CSS_SELECTOR, "#inbox-tabs-tabpane-first>div>div:last-child"
        )
        self.assertEqual(len(container.find_elements(By.XPATH, "child::div")), 0)

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
