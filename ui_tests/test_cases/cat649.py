import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from common.pages.login import Login
from common.web_driver_util import init_web_driver, settings


class CAT649(unittest.TestCase):
    """https://cfp-psc.atlassian.net/browse/CAT-649"""

    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()
        self.user = settings["candidate"]

    def test_logout(self):
        login = Login(self.driver)

        homepage = login.login_valid_user(self.user["email"], self.user["password"])
        homepage.logout()

        page_text = self.driver.page_source
        if "Candidate Assessment Tool" in page_text:
            self.fail("No login page found")

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
