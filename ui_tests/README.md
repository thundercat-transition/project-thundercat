# CAT Automated Testing Branch

## Requirements to Run Tests

- Download [Python 3.6.7 Windows x86-64 executable installer](https://www.python.org/downloads/release/python-367/).
  - "Install now"
  - Bypass character count limit
- Launch the Python terminal from Start Menu.
  - Type "pip install -m selenium" and allow the files to download
- Download webdrivers for the browsers you intend to run tests in and place them in `/ui_tests/drivers/`.
- You must run test cases and test suites with the `/ui_tests/` folder as your root directory, not `/project-thundercat/`. This is an issue that we will look to fix in the future. See the example command prompt below for an example.

```cmd
// this will work
C:\..\project-thundercat\ui_tests> python test_suites/all_cat_tests.py

// this will *not* work
C:\..\project-thundercat> python ui_tests/test_suites/all_cat_tests.py
```

## Accounts to Use in Tests

| Type                 | Email                 |
| -------------------- | --------------------- |
| Candidate accounts   | tester[#]@canada.ca   |
| God account          | assigner@canada.ca    |
| PPC Administrator    | ppcadmin@canada.ca    |
| Test Administrator   | testadmin@canada.ca   |
| System Administrator | systemadmin@canada.ca |
| Scorer account       | scorer@canada.ca      |

See [Changing Test Settings](#changing-test-settings) for a guide on accessing these values in a test script. **Do not hardcode emails and passwords in test scripts.** Always access them through `test_settings.json`.

## Writing Tests

- Always use `unittest` asserts in place of the `assert` keyword, so the test runner will know the result of a test. For example, use `self.assertEquals(42, 21 * 2)` instead of `assert 42 == (21 * 2)`. For more examples, see the [unittest documentation](https://docs.python.org/3.6/library/unittest.html).

## Changing Test Settings

- To modify test settings (browser, environment, account, etc..) make changes to the JSON object in `test_settings.json`.
- This way, you only need to change one file to modify the entire test suite.
- **Do not push changes to this file.**

```Python
from common.pages.login import Login
from common.web_driver_util import init_web_driver, settings # import settings

# accessing account credentials in a test script
class TestExample:
    def setup():
        self.driver = init_web_driver()
        self.driver.maximize_window()
        self.user = settings["candidate"] # get accounts from test_setting.json
        self.admin = settings["admin"]

    def test_dummy():
        login = Login(self.driver)
        login.login_valid_user(self.user["email"], self.user["password"])
```
