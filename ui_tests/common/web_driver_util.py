from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from random import randint, choices
from string import ascii_lowercase, digits
import json

with open("test_settings.json") as f:
    settings = json.load(f)


def init_web_driver():
    """Initializes and returns a Selenium webdriver for the requested browser
    
    Args:
        browser (str): Chrome, Firefox, IE, or random
        headless (bool): Optional, run Chrome or Firefox in headless mode
    
    """
    path_to_driver = (
        "C:\\Katalon_Studio_Windows_64-7.2.1\\configuration\\resources\\drivers"
    )

    browser = settings["browser"].lower()

    # get random browser
    if browser.startswith("r"):
        randInt = randint(0, 1)  # change to (0, 2) to allow IE
        if randInt == 0:
            browser = "c"
        elif randInt == 1:
            browser = "f"
        elif randInt == 2:
            browser = "ie"

    if browser.startswith("c"):
        # prevent "Failed to laod extension" error in Chrome
        chrome_options = ChromeOptions()
        chrome_options.add_experimental_option("useAutomationExtension", False)

        if settings["headless"]:
            chrome_options.add_argument("--headless")
            chrome_options.add_argument("--window-size=1920x1080")

        driver = webdriver.Chrome(
            executable_path=path_to_driver + "\\chromedriver_win32\\chromedriver.exe",
            options=chrome_options,
        )
    elif browser.startswith("f"):
        firefox_options = FirefoxOptions()

        if settings["headless"]:
            firefox_options.add_argument("--headless")
            firefox_options.add_argument("--window-size=1920x1080")

        # service_log_path prevents the creation of geckdriver.log files in root
        driver = webdriver.Firefox(
            executable_path=path_to_driver + "\\firefox_win64\\geckodriver.exe",
            service_log_path=None,
            options=firefox_options,
        )
    else:
        # doesn't fix ie. should fix ie but doesn't f i x ie :)
        capabilities = DesiredCapabilities.INTERNETEXPLORER.copy()
        capabilities["BROWSER_NAME"] = "IE"
        capabilities["IE_ENSURE_CLEAN_SESSION"] = True
        capabilities["IGNORE_PROTECTED_MODE_SETTINGS"] = True
        capabilities["INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS"] = True
        capabilities["IGNORE_ZOOM_SETTING"] = True

        driver = webdriver.Ie(
            executable_path=path_to_driver + "\\iedriver_win64\\IEDriverServer.exe",
            desired_capabilities=capabilities,
        )

    # search the DOM for x seconds before giving up
    driver.implicitly_wait(10)
    return driver


def get_base_url():
    """Returns a String containing the base URL for the selected testing environment"""
    env = settings["environment"].lower()
    if env.startswith("l"):
        return "http://localhost:81"
    elif env.startswith("t"):
        return "test.url"
    else:
        return "http://167.43.6.68/"


def generate_random_account_info():
    """
    Use in scripts that create accounts. Can be passed directly to create_account().
    Last name for created accounts is always TESTACCOUNT for easy identification.
    If account is created successfully pass dict to save_created_account().
    """
    FNAME_LENGTH = randint(4, 12)
    EMAIL_LENGTH = randint(4, 16)
    PRI_LENGTH = 8
    PASSWORD = "Random1!"
    return {
        "fname": "".join(choices(ascii_lowercase, k=FNAME_LENGTH)),
        "lname": "TESTACCOUNT",
        "dob": (
            str(randint(1, 28))
            + "/"
            + str(randint(1, 12))
            + "/"
            + str(randint(1900, 2020))
        ),
        "email": (
            "".join(choices(ascii_lowercase + digits, k=EMAIL_LENGTH)) + "@canada.ca"
        ),
        "pri": "".join(choices(digits, k=PRI_LENGTH)),
        "pswd": PASSWORD,
        "confirmPswd": PASSWORD,
        "privNotice": True,
    }


def save_created_account(acc_info):
    output = (
        acc_info["fname"]
        + ", "
        + acc_info["lname"]
        + ", "
        + acc_info["dob"]
        + ", "
        + acc_info["email"]
        + ", "
        + acc_info["pri"]
        + ", "
        + acc_info["pswd"]
        + "\n"
    )

    with open("created-accounts.txt", "a") as text_file:
        text_file.write(output)
