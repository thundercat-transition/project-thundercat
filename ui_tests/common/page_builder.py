from selenium import webdriver


class PageBuilder:
    def __init__(self, driver):
        self.driver = driver

    def __return_contact_us(self):
        from common.pages.contact_us import ContactUs

        return ContactUs(self.driver)

    def __return_hompage(self):
        from common.pages.homepage import Homepage

        return Homepage(self.driver)

    def __return_incident_report(self):
        from common.pages.incident_report import IncidentReport

        return IncidentReport(self.driver)

    def __return_login(self):
        from common.pages.login import Login

        # return Login(self.driver)

    def __return_my_tests(self):
        from common.pages.my_tests import MyTests

        return MyTests(self.driver)

    def __return_ppc_admin_page(self):
        from common.pages.ppc_admin_page import PpcAdminPage

        return PpcAdminPage(self.driver)

    def __return_profile(self):
        from common.pages.profile import Profile

        return Profile(self.driver)

    def __return_sample_tests(self):
        from common.pages.sample_tests import SampleTests

        return SampleTests(self.driver)

    def __return_test_admin_page(self):
        from common.pages.test_admin_page import TestAdminPage

        return TestAdminPage(self.driver)

    def __return_logout(self):
        return

    def __return_system_admin_page(self):
        return

    def __return_scorer_page(self):
        return

    def __return_check_in(self):
        from common.pages.homepage import Homepage

        return Homepage(self.driver)

    def __return_sample_emib(self):
        from common.pages.cat_tests.sample_emib import SampleEmib

        return SampleEmib(self.driver)

    def __return_pizza_test(self):
        from common.pages.cat_tests.pizza_test import PizzaTest

        return PizzaTest(self.driver)

    def return_page_object(self, page_name):
        if page_name == "System Administrator":
            return self.__return_system_admin_page()
        elif page_name == "PPC Administrator":
            return self.__return_ppc_admin_page()
        elif page_name == "Test Administrator":
            return self.__return_test_admin_page()
        elif page_name == "Check-in":
            return self.__return_hompage()
        elif page_name == "My Profile":
            return self.__return_profile()
        elif page_name == "Incident Reports":
            return self.__return_incident_report()
        elif page_name == "My Tests":
            return self.__return_my_tests()
        elif page_name == "Contact Us":
            return self.__return_contact_us()
        elif page_name == "Sample Tests":
            return self.__return_sample_tests()
        elif page_name == "Logout":
            return self.__return_logout()
        elif page_name == "Sample e-MIB":
            return self.__return_sample_emib()
        elif page_name == "Pizza Test":
            return self.__return_pizza_test()

