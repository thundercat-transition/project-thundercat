from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from common.pages.page import Page


class Homepage(Page):
    def open_check_in(self):
        self.driver.find_element(
            By.CSS_SELECTOR, "#unit-test-check-in-button > button"
        ).click()

    def enter_access_code(self, code):
        code_input = self.driver.find_element(By.ID, "test-access-field")
        code_input.clear()
        code_input.send_keys(code)

    def cancel_check_in(self):
        self.driver.find_element(By.ID, "unit-test-left-btn").click()

    def submit_check_in(self):
        self.driver.find_element(By.ID, "unit-test-right-btn").click()
