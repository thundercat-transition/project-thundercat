from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from common.pages.page import Page


class SystemAdministration(Page):
    def open_nav_item(self, nav_item):
        index_table = {
            "dashboard": 0,
            "active tests": 1,
            "test access codes": 2,
            "permissions": 3,
            "ta test accesses": 4,
            "incident reports": 5,
            "scoring": 6,
            "re-scoring": 7,
            "reports": 8,
        }

        index = str(index_table[nav_item.lower()])

        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.ID, "navigation-items-section"))
        )

        self.driver.find_element(
            By.CSS_SELECTOR, "#navigation-items-section > div:nth-child(" + index + ")"
        ).click()

