from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from common.web_driver_util import get_base_url, settings
from common.pages.system_administration import SystemAdministration
from common.pages.homepage import Homepage
from common.pages.page import Page
from random import randint
from datetime import date


class Login(Page):
    def __init__(self, driver, auto_select_language=True):
        Page.__init__(self, driver)
        self.driver = driver
        driver.get(get_base_url() + "login/")

        # necessary for CAT617
        if auto_select_language:
            self.select_language()

    def select_language(self):
        """Selects language on the Welcome page based on the value in test_settings.json"""
        language = settings["language"].lower()
        if language.startswith("e"):
            self.__select_english()
        elif language.startswith("f"):
            self.__select_french()
        else:  # random
            if randint(0, 1) == 0:
                self.__select_english()
            else:
                self.__select_french()

    def __select_english(self):
        """Select English on the CAT welcome page"""
        self.driver.find_element(
            By.XPATH, "//button[contains(text(), 'English')]"
        ).click()

    def __select_french(self):
        """Select French on the CAT welcome page"""
        self.driver.find_element(
            By.XPATH, "//button[contains(text(), 'Français')]"
        ).click()

    def login_valid_user(self, email, password, user_is_admin=False):
        self.__fill_in_login_form(email, password)

        # submit form normally
        self.driver.find_element(By.CSS_SELECTOR, "input[type='submit']").click()

        return self.__return_from_login(user_is_admin)

    def login_valid_user_thru_header(self, email, password, user_is_admin=False):
        self.__fill_in_login_form(email, password)

        language = Page.get_language(self)
        if language == "en":
            button_text = "Login"
        else:
            button_text = "Connexion"

        # submit form using header button
        self.driver.find_element(
            By.XPATH, "//button[contains(text(), '" + button_text + "')]"
        ).click()

        return self.__return_from_login(user_is_admin)

    def __return_from_login(self, user_is_admin):
        if user_is_admin:
            # wait x seconds for the page to switch to /system-administration/
            WebDriverWait(self.driver, 10).until(
                EC.url_contains("system-administration")
            )
            return SystemAdministration(self.driver)
        else:
            return Homepage(self.driver)

    def __fill_in_login_form(self, email, password):
        # enter login information
        email_input = self.driver.find_element(By.ID, "username")
        email_input.clear()
        email_input.send_keys(email)

        password_input = self.driver.find_element(By.ID, "password")
        password_input.clear()
        password_input.send_keys(password)

    def open_create_account_tab(self):
        self.driver.find_element(By.ID, "login-tabs-tab-account").click()

    def create_account(self, acc_info):
        """
        Accepts a dictionary containing keys:
            fname, lname, dob (DD/MM/YYYY), email, pri, pswd, confirmPswd, privNotice (boolean).
        """
        # enter account info
        self.driver.find_element(By.ID, "first-name-field").send_keys(acc_info["fname"])
        self.driver.find_element(By.ID, "last-name-field").send_keys(acc_info["lname"])
        self.__fill_in_date_of_birth(acc_info["dob"])
        self.driver.find_element(By.ID, "email-address-field").send_keys(
            acc_info["email"]
        )
        self.driver.find_element(By.ID, "pri-or-military-nbr-field").send_keys(
            acc_info["pri"]
        )
        self.driver.find_element(By.ID, "password-field").send_keys(acc_info["pswd"])
        self.driver.find_element(By.ID, "password-confirmation-field").send_keys(
            acc_info["confirmPswd"]
        )

        if acc_info["privNotice"]:
            self.driver.find_element(By.XPATH, "//input[@type='checkbox']").send_keys(
                Keys.SPACE
            )

    def __fill_in_date_of_birth(self, dob):
        """Hacky method for dealing with React selects. See test_admin_page.py for more joy."""
        # split date string into parts
        day, month, year = dob.split("/")

        # get element ids for each date part
        day_input_id = "react-select-2-option-" + str(int(day) - 1)
        month_input_id = "react-select-3-option-" + str(int(month) - 1)
        current_year = date.today().year
        year_input_id = "react-select-4-option-" + str(current_year - int(year))

        # dropdown arrows
        dob_dropdowns = self.driver.find_elements(
            By.CLASS_NAME, "css-1hb7zxy-IndicatorsContainer"
        )

        # open each dropdown and select the date
        dob_dropdowns[0].click()
        self.driver.find_element(By.ID, day_input_id).click()
        dob_dropdowns[1].click()
        self.driver.find_element(By.ID, month_input_id).click()
        dob_dropdowns[2].click()
        self.driver.find_element(By.ID, year_input_id).click()

    def submit_create_account_tab(self):
        self.driver.find_element(By.CSS_SELECTOR, "button[type='submit']").click()
        return Homepage(self.driver)
