from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from common.pages.page import Page


class TestAdminPage(Page):
    def __init__(self, driver):
        self.driver = driver

    def generate_test_code(self):
        self.driver.find_element(
            By.CSS_SELECTOR, "#generate-test-access-code-button > button"
        ).click()

    def enter_staffing_num(self, text):
        """Enters text into staffing process number field"""
        self.driver.find_element(By.ID, "staffing-process-nbr-field").send_keys(text)

    def set_test_session_lang_english(self):
        """Sets the test session language to value passed into method"""
        # this whole way of doing this is kinda hacky, if you can find a better way of doing this then change it
        self.driver.find_element_by_class_name(
            "css-yk16xz-control"
        ).click()  # clicks the dropdown arrow
        wait = WebDriverWait(self.driver, timeout=10)
        wait.until(
            EC.element_to_be_clickable((By.CLASS_NAME, "css-1n7v3ny-option"))
        )  # waits until the dropdown options are clickable
        self.driver.find_element(
            By.ID, "react-select-2-option-0"
        ).click()  # clicks English option

    def set_test_session_lang_french(self):
        """Sets the test session language to value passed into method"""
        # this whole way of doing this is kinda hacky, if you can find a better way of doing this then change it
        self.driver.find_element_by_class_name(
            "css-yk16xz-control"
        ).click()  # clicks the dropdown arrow
        wait = WebDriverWait(self.driver, timeout=10)
        wait.until(
            EC.element_to_be_clickable((By.CLASS_NAME, " css-yt9ioa-option"))
        )  # waits until the dropdown options are clickable
        self.driver.find_element(
            By.ID, "react-select-2-option-1"
        ).click()  # clicks French option

    def set_test_to_administer(self):
        """Placeholder function, dropdown doesn't work yet"""
