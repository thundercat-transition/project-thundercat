from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from common.pages.page import Page

class PizzaTest(Page):
    def __init__(self, driver):
        Page.__init__(self, driver)
        self.driver = driver

    def write_to_notepad(self, text):
        self.driver.find_element(By.ID, "text-area-notepad").send_keys(text)

    def clear_notepad(self):
        # untested on IE!
        browser = self.driver.capabilities["browserName"]
        if browser == "chrome":
            # this method works on Chrome only
            self.driver.find_element(By.ID, "text-area-notepad").send_keys(
                Keys.CONTROL + "a"
            )
            self.driver.find_element(By.ID, "text-area-notepad").send_keys(Keys.DELETE)
        else:
            # this method works on Firefox only
            self.driver.find_element(By.ID, "text-area-notepad").clear()

    def get_notepad_text(self):
        """Returns a String containing the current contents of the notepad"""
        return self.driver.find_element(By.ID, "text-area-notepad").text

    def toggle_notepad(self):
        """Open/close the notepad"""
        self.driver.find_element(By.ID, "notepad-button").click()