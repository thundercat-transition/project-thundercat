from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from common.pages.page import Page


class SampleEmib(Page):
    def __init__(self, driver):
        Page.__init__(self, driver)
        self.driver = driver

    def proceed_to_test(self):
        self.driver.find_element(By.CLASS_NAME, "btn-wide").click()

    def nav_instructions(self, index):
        instructions = self.driver.find_elements_by_css_selector(
            "#navigation-items-section a"
        )
        instructions[index].click()

    def start_test(self):
        self.driver.find_element(By.ID, "unit-test-start-btn").click()
        self.driver.find_element(By.ID, "unit-test-right-btn").click()

    def start_test_popup(self):
        self.driver.find_element(By.ID, "unit-test-start-btn").click()

    def cancel_test_popup(self):
        self.driver.find_element(By.ID, "unit-test-left-btn").click()

    def submit_test(self):
        self.driver.find_element(By.ID, "unit-test-submit-btn").click()
        self.driver.find_element(By.ID, "unit-test-right-btn").click()
        return Page.return_page_object(self, "Sample Tests")

    def write_to_notepad(self, text):
        self.driver.find_element(By.ID, "text-area-notepad").send_keys(text)

    def clear_notepad(self):
        # untested on IE!
        browser = self.driver.capabilities["browserName"]
        if browser == "chrome":
            # this method works on Chrome only
            self.driver.find_element(By.ID, "text-area-notepad").send_keys(
                Keys.CONTROL + "a"
            )
            self.driver.find_element(By.ID, "text-area-notepad").send_keys(Keys.DELETE)
        else:
            # this method works on Firefox only
            self.driver.find_element(By.ID, "text-area-notepad").clear()

    def get_notepad_text(self):
        """Returns a String containing the current contents of the notepad"""
        return self.driver.find_element(By.ID, "text-area-notepad").text

    def toggle_notepad(self):
        """Open/close the notepad"""
        self.driver.find_element(By.ID, "notepad-button").click()

    def toggle_timer(self):
        """Show/hide the timer"""
        self.driver.find_element(By.ID, "unit-test-toggle-timer").click()

    def goto(self, location):
        """Navigate between the Instructions, Background Information, and In-Box pages"""
        if location == "Next page":
            self.__next_page()
        elif location == "Previous page":
            self.__previous_page()
        elif location == "Instructions":
            self.driver.find_element(By.ID, "emib-tabs-tab-instructions").click()
        elif location == "Background Information":
            self.driver.find_element(By.ID, "emib-tabs-tab-background").click()
        elif location == "In-Box":
            self.driver.find_element(By.ID, "emib-tabs-tab-inbox").click()

    def __next_page(self):
        if Page.get_language(self) == "en":
            button_text = "Next"
        else:
            button_text = "Suivant"

        self.driver.find_element(
            By.XPATH, "//span[contains(text(), '" + button_text + "')]"
        ).click()

    def __previous_page(self):
        if Page.get_language(self) == "en":
            button_text = "Previous"
        else:
            button_text = "Précédent"

        self.driver.find_element(
            By.XPATH, "//span[contains(text(), '" + button_text + "')]"
        ).click()

    def in_box_open_email(self, email):
        """Takes an int. Assumes In-Box is already open"""
        emails = self.driver.find_elements(By.ID, "unread-email-preview")
        emails[email].click()

    def add_email_response(self):
        """Clicks the Add email response button"""
        # for this function we need to interact with the element ia javascript, for some reason we can't interact with it via selenium commands
        self.driver.execute_script(
            "document.getElementById('unit-test-email-reply-button').click();"
        )  # clicks the add email response button

    def type_email_response(self, text):
        """Enters text into the email response field"""
        self.driver.find_element(By.ID, "your-response-text-area").send_keys(
            text
        )  # types text into the email response field

    def type_email_reasoning(self, text):
        """Enters text into the email response reasoning field"""
        self.driver.find_element(By.ID, "reasons-for-action-text-area").send_keys(
            text
        )  # types text into the email reasoning field

    def cancel_response(self):
        self.driver.find_element(
            By.CSS_SELECTOR, ".ReactModal__Content--after-open .btn-danger"
        ).click()  # clicks the cancel response button

    def save_response(self):
        self.driver.find_element(
            By.ID, "unit-test-email-response-button"
        ).click()  # clicks the save response button

    def task_list(self, action, text=None, number=None):
        """JavaScript is used in certain task list functions to stop 'element not interactable' exceptions"""
        if action == "add":
            self.__add_task_list()
        elif action == "open":
            self.__open_task_list(number)
        elif action == "write":
            self.__write_to_task_list(text, number)
        elif action == "save":
            self.__save_open_task_list()
        elif action == "cancel":
            self.__cancel_open_task_list()

    def __add_task_list(self):
        """Adds a task list to the correct email tab"""
        add_task_buttons = self.driver.find_elements(
            By.ID, "unit-test-email-task-button"
        )
        current_email = self.driver.find_element(
            By.CSS_SELECTOR, "#unit-test-selected-email-preview"
        ).text

        if "1" in current_email:
            add_task_list = add_task_buttons[0]
        elif "2" in current_email:
            add_task_list = add_task_buttons[1]
        elif "3" in current_email:
            add_task_list = add_task_buttons[2]

        # press the "Add task list" button
        self.driver.execute_script("arguments[0].click();", add_task_list)

        WebDriverWait(self.driver, 10).until(
            EC.visibility_of(
                self.driver.find_element(By.ID, "unit-test-email-response-button")
            )
        )

        self.driver.execute_script(
            "document.getElementById('unit-test-email-response-button').click();"
        )

    def __open_task_list(self, index):
        """Index starts at zero. I.e., index 0 represents Task List 1."""
        language = Page.get_language(self)
        if language == "en":
            button_text = "Task List Response"
        elif language == "fr":
            button_text = "Réponse par liste de tâches"

        all_tasks = self.driver.find_elements(
            By.XPATH,
            "//label[contains(text(), '" + button_text + "')]/ancestor::button",
        )

        # open task list and press "Edit Response"
        all_tasks[index].click()
        self.driver.find_element(By.ID, "unit-test-view-task-edit-button").click()

    def __write_to_task_list(self, text, field):
        """Assumes task list is open. Writes to Response or Actions field"""
        if field == 0:
            text_area_id = "your-tasks-text-area"
        else:
            text_area_id = "reasons-for-action-text-area"
        self.driver.find_element(By.ID, text_area_id).send_keys(text)

    def __save_open_task_list(self):
        self.driver.find_element(By.ID, "unit-test-email-response-button").click()

    def __cancel_open_task_list(self):
        self.driver.find_element(
            By.CSS_SELECTOR, ".ReactModalPortal .btn-danger"
        ).click()

