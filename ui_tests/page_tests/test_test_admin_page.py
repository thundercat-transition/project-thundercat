import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from common.pages.login import Login
from common.web_driver_util import init_web_driver, settings
from selenium.webdriver.common.by import By


class TestTAdminPage(unittest.TestCase):
    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()
        self.admin = settings["admin"]

    def test_all_test_admin_page_functions(self):
        # login and navigate to the test
        login = Login(self.driver)
        system_admin = login.login_valid_user(
            self.admin["email"], self.admin["password"], user_is_admin=True
        )
        admin_page = system_admin.open_menu_item("Test Administrator")
        admin_page.generate_test_code()
        admin_page.enter_staffing_num("1234")
        admin_page.set_test_session_lang_english()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
