import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from common.pages.login import Login
from common.web_driver_util import init_web_driver, settings
from common.pages.page import Page


class TestWebheader(unittest.TestCase):
    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()
        self.user = settings["candidate"]

    def test_header_func(self):
        login = Login(self.driver)
        homepage = login.login_valid_user(self.user["email"], self.user["password"])
        homepage.open_menu_item("Incident Reports")

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
