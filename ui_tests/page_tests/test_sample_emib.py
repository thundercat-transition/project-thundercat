import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from common.pages.login import Login
from common.web_driver_util import init_web_driver, settings
from selenium.webdriver.common.by import By


class TestSampleEmib(unittest.TestCase):
    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()
        self.user = settings["candidate"]

    def test_all_emib_functions(self):
        # login and navigate to the test
        login = Login(self.driver)
        homepage = login.login_valid_user(self.user["email"], self.user["password"])
        sample_tests = homepage.sample_tests_button()
        sample_emib = sample_tests.select_test("Sample e-MIB")

        # emib functions
        sample_emib.proceed_to_test()
        sample_emib.write_to_notepad("Where's the money, Lebowski?")
        sample_emib.goto("Next page")
        sample_emib.goto("Previous page")

        # verify we are on the correct tab
        test_instructions_tab = self.driver.find_element(
            By.ID, "left-tabs-navigation-tab-event_key_1"
        )
        test_instructions_classes = test_instructions_tab.get_attribute("class")
        self.assertIn("active", test_instructions_classes)

        # enter the test
        sample_emib.start_test()
        sample_emib.write_to_notepad("Not bad, kid.")

        # close then reopen the notepad
        sample_emib.toggle_notepad()
        sample_emib.toggle_notepad()

        # verify notepad is correct then clear it
        self.assertEqual(
            "Where's the money, Lebowski?Not bad, kid.",
            self.driver.find_element(By.ID, "text-area-notepad").text,
        )
        sample_emib.clear_notepad()
        self.assertEqual("", self.driver.find_element(By.ID, "text-area-notepad").text)

        # switch to in-box tab and open email three
        sample_emib.goto("In-Box")
        sample_emib.in_box_open_email(2)
        sample_emib.add_email_response()
        sample_emib.type_email_response("You're pretty good.")
        sample_emib.type_email_reasoning("This is now shorter.")
        sample_emib.save_response()

        # hide then reshow the timer
        sample_emib.toggle_timer()
        sample_emib.toggle_timer()

        # add three task lists, open the second one
        sample_emib.task_list("add")
        sample_emib.task_list("add")
        sample_emib.task_list("add")
        sample_emib.task_list("open", number=1)

        # write to the opened task list
        sample_emib.task_list("write", "hello, world!", 0)
        sample_emib.task_list("write", "goodbye, cruel world!", 1)
        sample_emib.task_list("save")

        # submit the test
        sample_emib.submit_test()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
