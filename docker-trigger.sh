#!/bin/bash
for var in "$@"
do
    case "$var" in
        "up")
            docker compose up
        ;;
        "down")
            docker compose down
        ;;
        "build")
            docker compose up --build
        ;;
        "c")
            clear
        ;;
        "s")
            docker system prune -f
        ;;
        "i")
            docker image prune -a -f
        ;;
        "v")
            docker volume prune -f
        ;;
        "vd")
            rm -rf C:/MSSQL/DockerFiles
        ;;
        "help")
            help_text
        ;;
        "h")
            help_text
        ;;
        *)
            echo "unknown command $var"
            help_text
        ;;
    esac
done


# ===============
# Functions
# ===============

# Prints the help dialog
function help_text()
{
    echo "
        up: docker compose up
        down: docker compose down
        build: docker compose up --build
        c: clear the console
        s: docker system prune -f
        i: docker image prune -a -f
        v: docker volume prune -f
        vd: rm -rf C:/MSSQL/DockerFiles
        help: show help text
        h: show help text
    "
}