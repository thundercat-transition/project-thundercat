# Docker - Scripts

## Table of Content

1. [CAT-MSSQL Image](#cat-mssql-image)
    1. [First Steps](#first-steps)
        1. [Create a new environment file](#create-a-new-environment-file)
        1. [Running the script for the first time](#running-the-script-for-the-first-time)
    1. [Auto-Versioning](#auto-versioning)
        1. [Information](#information)
        1. [Before](#before)
        1. [Run](#run)
        1. [After](#after)
    1. [Release Versioning](#release-versioning)
        1. [Information](#information-1)
        1. [Before](#before-1)
        1. [Run](#run-1)
        1. [After](#after-1)

<br>

## CAT-MSSQL Image

### First Steps

#### Create a new environment file

1. At the base of our project (C:\_dev\Ideaprojects\project-thundercat), create a file named: `.env`
1. Add these lines in the file:
    ```cmd
    export DOCKER_HUB_USERNAME=oec-cat.dev@cfp-psc.gc.ca
    export DOCKER_HUB_PASSWORD=***PASSWORD***
    export DOCKER_HUB_REPO=https://hub.docker.com/v2
    export DOCKER_HUB_PROJECT=projectthundercat
    export DOCKER_HUB_MSSQL_IMAGE=cat-mssql
    export DOCKER_HUB_CAT_IMAGE=cat-build
    ```
    **Note**: This file won't be added to our Git repository, so don't worry for the password.

<br>

#### Running the script for the first time

1.	Open Git Bash as an **Administrator** and cd to the ProjectThundercat folder:
    ```cmd
    cd C:\_dev\Ideaprojects\project-thundercat
    ```
1. Run the auto-version script:
    ```cmd
    sh ./generate-cat-mssql-image.sh
    ```
1. It should work until the end and download all the libraries to your computer.
    - Next time, you will be able to run the script just by double-clicking the script.

<br>

---

<br>

### Auto-Versioning

#### Information

- We are using [generate-cat-mssql-image.sh](../../generate-cat-mssql-image.sh)
- This script is using [cat-versioning-menu.sh](../../cat-versioning-menu.sh) by sending it the value: **auto**
    - "cat-versioning-menu.sh" is the user friendly way to execute scripts
- We use this script to automatically generate a **new version** of our application with a datetime.
    - It will help us not walking on each other when running pipeline tests on Gitlab.
    - It will help us having a new version for ***React-Cache-Buster*** so we have an automatic cache refresh when deploying.

<br>

#### Before

- Make sure you have created the [sql scripts](./cat-mssql-readme.md/#prepare-sql-scripts-to-create-mssql-image).
- If its your first time running this script, make sure you have followed the [first steps](#first-steps)

<br>

#### Run

- Run [generate-cat-mssql-image.sh](../../generate-cat-mssql-image.sh)
    - By **double-clicking** the file
    - Or by running it in Windows PowerShell
        ```cmd
        cd C:\_dev\Ideaprojects\project-thundercat

        sh ./generate-cat-mssql-image.sh
        ```

<br>

#### After

- Make sure your [package.json](../../frontend/package.json) has now a **new version**.
    - Save the file so it formats correctly.
- Verify that the new version's tag is now available on [DockerHub](https://hub.docker.com/r/projectthundercat/cat-mssql).
- Push the new package.json to your remote branch.

<br>

---

<br>

### Release Versioning

#### Information

- We are using [generate-cat-mssql-image-for-release.sh](../../generate-cat-mssql-image-for-release.sh)
- This script is using [cat-versioning-menu.sh](../../cat-versioning-menu.sh) by sending it the value: **release**
    - "cat-versioning-menu.sh" is the user friendly way to execute scripts
- We use this script to generate a **release version** of our application with a format: **Release_VERSION**
    - It will help us not walking on each other when running pipeline tests on Gitlab.
    - It will help us having a new version for ***React-Cache-Buster*** so we have an automatic cache refresh when deploying.
- Compared to the **Auto-Versioning** script, this script:
    - **Deletes** all remote **TAGs** that are **10 days older**. It helps to keep a clean DockerHub environment.
    - Keeps all the **Release_** TAGs.

<br>

#### Before

- Make sure this is for a **Release**.
- Make sure you have created the [sql scripts](./cat-mssql-readme.md/#prepare-sql-scripts-to-create-mssql-image). In this case, make sure all sql scripts have been merged together.

<br>

#### Run

- Run [generate-cat-mssql-image-for-release.sh](../../generate-cat-mssql-image-for-release.sh)
    - By **double-clicking** the file
    - Or by running it in Windows PowerShell
        ```cmd
        cd C:\_dev\Ideaprojects\project-thundercat

        sh ./generate-cat-mssql-image-for-release.sh
        ```
- Specify the **Release Version**
    - Example: ***Release_1.3***
- Press "Enter"

<br>

#### After

- Make sure your [package.json](../../frontend/package.json) has now a **new version**.
    - Save the file so it formats correctly.
- Verify that the new version's tag is now available on [DockerHub](https://hub.docker.com/r/projectthundercat/cat-mssql).
- Push the new package.json to your remote branch.