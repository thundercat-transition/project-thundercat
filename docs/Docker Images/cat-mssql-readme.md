# Image: CAT-MSSQL

## Table of Content

1. [Description](#description)
1. [Prepare SQL Scripts to Create MSSQL Image](#prepare-sql-scripts-to-create-mssql-image)
1. [Use Script to: Generate a New TAG and Push to Docker Hub](#use-script-to-generate-a-new-tag-and-push-to-docker-hub)
1. [Manually - Pull, Generate and Push to Docker Hub](#manually---pull-generate-and-push-to-docker-hub)
    1. [Pull Image](#pull-image)
    1. [Build Image](#build-image)
    1. [Push Image - Latest](#push-image---latest)
    1. [Push Image - With a TAG](#push-image---with-a-tag)
1. [Testing your new MSSQL image locally](#testing-your-new-mssql-image-locally)

<br>

## Description

<br>

**Cat-mssql** is the image used as the database for all the Gitlab's pipelines tests. If you want to add packages to this image or modify the way it works, you will have to modify [Dockerfile_Create_Image_mssql](../../Dockerfile_Create_Image_mssql).

<br>

## Prepare SQL Scripts to Create MSSQL Image

<br>

- Generate the [schema_structure.sql](https://gitlab.com/thundercat-transition/project-thundercat/-/tree/develop/mssql_image_creation/schema_structure.sql) file based on the latest version of the database structure (including all models/tables and most updated migrations):

  - Open Microsoft SQL Server Management Studio and connect to your local database.
  - Right click on your local database and select 'Tasks - Generate Scripts...': <br><br>
    ![generate_schema_structure_1.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_1.png) <br><br>
  - Then follow those screenshots below to generate the new 'schema_structure.sql' file: <br><br>
    ![generate_schema_structure_2.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_2.png) <br><br>
    ![generate_schema_structure_3.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_3.png) <br><br>
    ![generate_schema_structure_4.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_4.png) <br><br>
    ![generate_schema_structure_5.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_5.png) <br><br>
    ![generate_schema_structure_6.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_6.png) <br><br>
  - Most updated version of 'schema_structure.sql' should now be generated are updates should be visible as part of your changes in VS Code. <br><br>

- Update the [static_table_inserts.sql](https://gitlab.com/thundercat-transition/project-thundercat/-/tree/develop/mssql_image_creation/static_table_inserts.sql) file if needed:

  - If you've added or updated model(s)/table(s) with static data (such as user permissions, reasons for deletion, view data, etc.), you'll need to update the 'static_table_inserts.sql' script to add the needed inserts in the preloaded database of the MSSQL image.
  - To do so, right click on your local database and select 'Tasks - Generate Scripts...': <br><br>
    ![generate_schema_structure_1.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_1.png) <br><br>
  - Then follow those screenshots below: <br><br>
    ![generate_schema_structure_2.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_2.png) <br><br>
    ![generate_schema_structure_7.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_7.png) <br><br>
    ![generate_schema_structure_8.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_8.png) <br><br>
    ![generate_schema_structure_9.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_9.png) <br><br>
    ![generate_schema_structure_10.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_10.png) <br><br>
    ![generate_schema_structure_11.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_11.png) <br><br>
    ![generate_schema_structure_12.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_12.png) <br><br>
  - Then copy the SETS and INSERTS from the generated script (you don't need to copy the USE(s) and the GO(s)): <br><br>
    ![generate_schema_structure_13.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_13.png) <br><br>
  - Finally open the 'static_table_inserts.sql' file, add your copied SETS and INSERTS and save: <br><br>
    ![generate_schema_structure_14.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_14.png) <br><br>
  - Most updated version of 'static_table_inserts.sql' should now be visible as part of your changes in VS Code. <br><br>

<br>

## Use Script to: Generate a New TAG and Push to Docker Hub

<br>

See our [Scripts guide](./scripts.md)

<br>

## Manually - Pull, Generate and Push to Docker Hub

<br>

Before executing any of these commands, please make sure you did a ***Login***. Follow [this guide](./readme.md/#login-with-powershell).

<br>

### Pull Image

- Do a Pull to get the image tag on Docker Hub:

  ```cmd
  docker pull projectthundercat/cat-mssql:<TAG>
  ```
- To get the latest:
  ```cmd
  docker pull projectthundercat/cat-mssql:latest
  ```

<br>

### Build Image

- Create/Generate a New Image from our Dockerfile:

  ```cmd
  docker build -f .\Dockerfile_Create_Image_mssql -t projectthundercat/cat-mssql .
  ```

<br>

### Push Image - Latest

- When your new Image has been created, do a Push:

  ```cmd
  docker push projectthundercat/cat-mssql
  ```

<br>

### Push Image - With a TAG

- When your new Image has been created, do a Push:

  ```cmd
  # Create Tag
  docker image tag projectthundercat/cat-mssql projectthundercat/cat-mssql:<TAG>

  # Push the Tagged Image
  docker image push projectthundercat/cat-mssql:<TAG>
  ```

<br>

## Testing your new MSSQL image locally

<br>

  - First, you'll need to do some temporary changes in [entrypoint.sh](../../backend/entrypoint.sh) (backend):

    <br>

    ![generate_schema_structure_15.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_15.png)

    <br>
  - And in [docker-compose.yml](../../docker-compose.yml) files (make sure you add the **TAG** if needed):
    ![generate_schema_structure_16.png](../../mssql_image_creation/documentation_screenshots/generate_schema_structure_16.png) <br><br>
  - Then simply do a docker down and up build:
    ```cmd
    docker compose down
    docker compose up --build
    ```
  - Once everything is up and running, you should be able to see your preloaded database with all the static data from your MSSQL image.
