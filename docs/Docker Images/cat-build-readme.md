# Image: CAT-BUILD

## Table of Content

1. [Description](#description)
1. [Manually - Pull, Generate and Push to Docker Hub](#manually---pull-generate-and-push-to-docker-hub)
    1. [Pull Image](#pull-image)
    1. [Build Image](#build-image)
    1. [Push Image - Latest](#push-image---latest)
    1. [Push Image - With a TAG](#push-image---with-a-tag)

<br>

## Description

<br>

**Cat-build** is the image used to store all the packages and will run all the containers during Gitlab's pipelines. If you want to add packages to this image, you will have to modify [Dockerfile_Create_Image](../../Dockerfile_Create_Image).


<br>

## Manually - Pull, Generate and Push to Docker Hub

<br>

Before executing any of these commands, please make sure you did a ***Login***. Follow [this guide](./readme.md/#login-with-powershell).

<br>

### Pull Image

- Do a Pull to get the image tag on Docker Hub:

  ```cmd
  docker pull projectthundercat/cat-build:<TAG>
  ```
- To get the latest:
  ```cmd
  docker pull projectthundercat/cat-build:latest
  ```

<br>

### Build Image

- Create/Generate a New Image from our Dockerfile:

  ```cmd
  docker build -f .\Dockerfile_Create_Image -t projectthundercat/cat-build .
  ```

<br>

### Push Image - Latest

- When your new Image has been created, do a Push:

  ```cmd
  docker push projectthundercat/cat-build
  ```

<br>

### Push Image - With a TAG

- When your new Image has been created, do a Push:

  ```cmd
  # Create Tag
  docker image tag projectthundercat/cat-build projectthundercat/cat-build:<TAG>

  # Push the Tagged Image
  docker image push projectthundercat/cat-build:<TAG>
  ```