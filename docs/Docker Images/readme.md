# Docker Images

## Table of Content

1. [Documentation](#documentation)
    1. [Why would you want to create a new image?](#why-would-you-want-to-create-a-new-image)
    2. [What is a Dockerfile?](#what-is-a-dockerfile)
    3. [Hosting Docker Images](#hosting-docker-images)
        1. [Hosting Images on Docker Hub](#hosting-images-on-docker-hub)
        2. [Login - Docker Hub](#login---docker-hub)
        3. [Login with PowerShell](#login-with-powershell)
2. References
    1. [Scripts](./scripts.md)
    2. [Cat-Build](./cat-build-readme.md)
    3. [Cat-Mssql](./cat-mssql-readme.md)

<br>

---

<br>

## Documentation

<br>

### Why would you want to create a new image?

A docker image is a package used by each container created by your pipelines on Gitlab. It containes multiple dependencies that won't have to be downloaded each time we create a new container when starting a pipeline.

<br>

### What is a Dockerfile?

- See our main Dockerfiles: [Dockerfile_Create_Image](https://gitlab.com/thundercat-transition/project-thundercat/-/blob/develop/Dockerfile_Create_Image) & [Dockerfile_Create_Image_mssql](https://gitlab.com/thundercat-transition/project-thundercat/-/blob/develop/Dockerfile_Create_Image_mssql)
- You will find out that the Dockerfiles are used to create a [Unix Alpine](https://alpinelinux.org/) or an MSSQL environment
- Add the dependencies and/or the database structure and data that will have to be used primarily in each container used in the pipelines or the ones that have to be installed directly on Unix using "apk add".

<br>

### Hosting Docker Images

<br>

#### Hosting Images on Docker Hub

We are currently hosting all our docker images on [DockerHub](https://hub.docker.com/u/projectthundercat). It is ***public*** and ***free***. Therefore, the information is available for all to see. **Please don't put any password on the docker images.**

<br>

#### Login - Docker Hub

To connect to our [DockerHub Project](https://hub.docker.com/u/projectthundercat), you will need:

1. Access to S:\CMB-DGGM\ITSD-DSTI\CAT\_KeyPass
    - Create a ticket on Hermes to HelpDesk to get access to that folder
    - Make sure you are connected to the VPN when trying to access it
2. Access to our Keepass: **ServersAndDatabases.kdbx**
    - If you don't have the password, ask to a team member.
3. Get the ***username***/***password*** from the Docker folder --> Docker Community Account
4. You will be able to use the same credentials for your local ***Docker Desktop***

<br>

#### Login with PowerShell

1. Open WindowsPowerShell
2. cd to our project

    ```cmd
    cd C:\_dev\IdeaProjects\project-thundercat\
    ```

3. To login to Docker Hub, use the command:

    ```cmd
    docker login
    ```
4. Use the credentials found [in previous step](#login---docker-hub)
