# GC Notify

## Links:

- [GC Notify Home](https://notification.canada.ca/)
- [GC Notify About](https://notification.canada.ca/why-gc-notify)
- [GC Notify API Documentation](https://documentation.notification.canada.ca/en/)

## Getting Access:

- Request access from a team member who already has access
  - Jonathan Cherry (Michael)
  - Ludovic Couture-Niles
  - Thomas Richer
  - (update as people are added)
- Login to the Home page
- Select CFP_OEC-PSC_CAT

## Dashboard

Shows basic overall details like number of messages sent

## Templates

Where email templates are maintained. Most are organized by folders. You can easily create new folders and templates here or modify existing ones.

If you add a new template, you will need to the template_id (seen at the bottom of the template) it to custom_models_gcnotifytemplate. See [0093_gcnotifytemplates_insert.py](../backend/backend/custom_models/migrations/0093_gcnotifytemplates_insert.py) for an example. You will also need to update the following:

- [gc_notify_template_codenames.py](../backend/backend/static/gc_notify_template_codenames.py) add a new codename
- [gc_notify_view.py](../backend/backend/views/gc_notify_view.py) see the "send_2fa_email" function as an example. The "send_gc_notify_email" function does the actual sending.

We will also need to discuss if a local template should be created incase we ever need to revert to the in-house email server for some reason. Ideally we will never do this.

## API Integration

Manage API Keys. If you create a new API key, you will be able to see it ONCE and then it will be permanently hidden. Do not forget to copy and store it.

NOTE: All active keys are stored in S:\CMB-DGGM\ITSD-DSTI\CAT_KeyPass. Ask a team member for the passkey for the Servers and Databases File.

API Keys are under prod. The following keys currently exist:

- _TEAM only emails (DO NOT USE)_ - this is a testing key that will allow emails to be sent, but only to team members in GC Notify. Not really worth using. Counts towards our limits.
- _LIVE any email (use on servers)_ - this allows emails to be sent to any email address. Ideal for servers or for local testing IF NEEDED. Counts towards our limits.
- _TEST no emails (use locally)_ - this mimics the entire process of sending emails (they can even be "verified" as delivered). The body will be bumped to the console. Does not count towards our limits.

To add an API key:

- Login as super user.
- Navigate to the Superuser Dashboard.
- Select "Site Admin" on the side navigation.
- Copy the desired API key from KeepPass.
- Paste into the entry field for "Send emails with GC Notify".
- Activate the setting with the slider.
- The key will be validated for the given format and saved into the DB for use.

To change an API Key:

- Login as super user.
- Navigate to the Superuser Dashboard.
- Select "Site Admin" on the side navigation.
- Deactive the "Send emails with GC Notify" setting.
- Copy/paste the new key.
- Activate the "Send emails with GC Notify" setting.

NOTE: If the format of API keys ever changes, you will need to update the validateKey function in [regexValidator](../frontend/src/helpers/regexValidator.js).

## Team members

Here you can manage and add team members

## Settings

Where you can change service name, email address, request limit increases, etc. The Contact Us link is also present on this page.

# Adding a New Template

## Create the Template in GCNotify

- Login to https://notification.canada.ca/
- Open the Templates tab
- Open an existing folder or create a New Folder (depending on needs)
- Create a template
- Select email or text (for now we only support emails)
- Add a template name (for internal use)
- Add a subject line
- Fill in the body
- use (( ))'s to identify variables that will be filled in from the API
- When completed, save the template and copy the Template ID from the bottom of the page (there is a button to copy it to clipboard)

## Register the Template ID with the application

- Add a nane to [GCNotifyTemplateCodename](../backend/backend/static/gc_notify_template_codenames.py). Make this as close to the template name in GC Notify
- Add a migration with the new teamplate name and template id. See [custom_models migration 95](../backend\backend\custom_models\migrations\0095_gcnotifytemplates_insert_continued.py) for an example. You will need the Template id copied from the previous section, the folder on GC notify (for ease of reference and support), and the GCNotifyTemplateCodename name you created in the previous step.
- Update static_table_inserts as per [cat-mssql-readme](<./Docker Images/cat-mssql-readme.md>)

## Add the API call to the template

- Add a suitably named function to [gc_notify_view](../backend/backend/views/gc_notify_view.py). Call send_gc_notify_email with the following:
  - recipient email address
  - the codename you created in [GCNotifyTemplateCodename](../backend/backend/static/gc_notify_template_codenames.py)
  - the "personalization" which we will explain shortly
  - the sender email (in case a reply is needed) or None
  - the first and last name of the recipient (mostly for debugging purposes)
  - UIT Invite ID or None (if this is a UIT invite and will need an auto email upon failed delivery)
  - assessment_process_id or None (if this is a supervised test invite and will need an auto email upon failed delivery)
- Create the personalization, a map of attribute names (the variables used in the actual template between the (( ))'s ) and their values. This must be passed to gc notify.
- This function will create a [GCNotifyEmailPendingDelivery](../backend\backend\custom_models\gc_notify_email_pending_delivery.py) object and send the email
- if no response is required for a failed delivery, add the code name to the list of codenames to ignore in [check_gc_notify_delivery_status](../backend/backend/celery/task_definition/gc_notify_tasks.py) line 122. Otherwise see the next section

## Emails requiring notification of Failed Delivery

### Bulk emails requiring a response

For individual emails that are not part of a group, see the next section

- This is a little more complex. You will need to add a column to the following objects:
  - [GCNotifyEmailPendingDelivery](../backend\backend\custom_models\gc_notify_email_pending_delivery.py) - tracks emails that haven't been delivered
  - [GCNotifyEmailFailedBulkDelivery](../backend\backend\custom_models\gc_notify_email_failed_bulk_delivery.py) - tracks
  - [GCNotifyEmailVerifiedAttemptedDelivery](../backend\backend\custom_models\gc_notify_email_verified_attempted_delivery.py)
- the added column will be used to link back to the group of emails so that all failed deliveries can be sent at once
- update all uses of the models appropriately

### For bulk or individual failed delivery notifications

- Create a new template, register it to the application, and add the api call as above for the new email detailing who failed to reiceve a notification. See "Supervised > Failed_Invitation_Delivery" or "UIT > FAILED_UIT" Templates on GC Notify for examples
- update [check_gc_notify_delivery_status](../backend/backend/celery/task_definition/gc_notify_tasks.py)
  - for a bulk email, add to the list of failed bulk emails around line 129. This will ensure a GCNotifyEmailFailedBulkDelivery is created for this email. Also add an elif clause after "elif gc_notify_email_pending_delivery.uit_invite_id is not None:" (line 225) to handle sending the bulk email. This will ensure all emails for the group have been attempted before sending the "failed to deliver to.." email
  - for individual emails, add an elif clause for the codename after the bulk emails (line 129) but before the else at line 152. Send the email from here as there is only one in the group

## Update the Documentation

Update [gc-notify-templates](./gc-notify-templates.md) as needed
