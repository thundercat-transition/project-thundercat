# Data Model

## Data model as of February 2021

### Diagram Description

This diagram was generated from the django models. Models can be found in the **models** folders on the backend (Django)  
![CAT data model diagram](./diagrams/CAT-db-model-2021.png "CAT data model diagram")

### Link to Diagram

The database model diagram was created on dbdiagram.io and can be found here:  
https://dbdiagram.io/d/602d4c0980d742080a3aec1c

### Additional Notes

The Custom Permissions model in user_management_models can cause some problems. It is one of two models where data is loaded into the database on migrations. This model represents the "Roles" a user can have on the system.  
The roles that are pre-loaded into the database are: Test Administrator, Business Operations, Test Scorer, Test Developer and R&D. The entries are created using an auto-increment id and this can cause problems if you reverse/revert the migrations that create these roles. The database will no longer match the static values in the static/permission.py file and many functions will not work correctly. This could be fixed by making the migrations apply using the static values.

The other model that preloads data into the database is the Language model and it only loads the language entries for French and English. There have been no observed side effects from reversing these migrations.
