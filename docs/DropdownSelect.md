# Dropdown Select

This is a custom component designed to replace `react-select` for the sake of ensuring accessibility.

Not all features in `react-select` are present in this component. Missing features will need to be implemented as needed.

The end result of this component will be a select dropdown with some amount of options.

# Props

- `defaultValue`

  This is the current value of the dropdown. It can be a string, number, or object taking the form of `{ value: X }`.

  Do not pass `null` as a value or react will complain about having an uncontrolled component.

- `onChange`

  A function passed to this prop will be called whenever the select tag's `onchange` fires. An event will be passed to the function, which is different than react-select. Note to get the value that changed, you should use `event.target.value`.

- `options`

  Pass an array of objects in the form: `{ label: "label for option", value: 1 }`

  This will populate the select options

- `ariaLabelledBy`

  This prop will set the `aria-labelledby` attribute of the select tag.

- `ariaRequired`

  This prop will set the `aria-required` attribute of the select tag.

# Controlling Focus and Refs

This prop is exported as a forwarded ref. To force focus onto the component, simply create and pass a ref, then request focus as follows:

```js
let dropdownRef = React.CreateRef();

...

<DropdownSelect ref={dropDownRef}>

...

dropdownRef.target.focus();

```

# How to Replace Select

Replace the `Select` component with `DropdownSelect`

The `value` prop on the `Select` component will need to be renamed to `defaultValue`

React-select has different input for `onchange`. You will need to change the function you passed to `onchange` to allow it to work with the new input from `DropdownSelect`.

The input to this function is an onchange event, rather than an object following the form `{ label: X, value: Y }`. Change it as follows

```
selectOnchange = input => {
    // do something with input.value;

}

...

dropdownOnchange = event => {
    // do something with event.target.value;

}

```
