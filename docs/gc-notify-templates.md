# GC Notify Templates

This file exists to document the existing templates and the variables that GC Notify expects.

This is organized by Folder and Template

## 2fa

### Two_Factor_Authentication

Sent when a user with elevated privileges logs into to CAT

- `first_name` - candidate's first name
- `last_name` - candidate's last name
- `auth_code` - the actual 2fa code that must be submitted in order to login (must be entered within 15 minutes)

## Accomodations

### Canceled_Accomodations_Request

Email informing CDD/TCM/HR? that a request was canceled

- `first_name` - first name of the person to whom the email is being sent
- `last_name` - last name of the person to whom the email is being sent
- `comments` - comments left by the candidate when requesting accomodation
- `rationale` - TODO
- `assigned_to_user_email` - AAE user email
- `assigned_to_user_first_name` - AAE first name
- `assigned_to_user_last_name` - AAE last name
- `user_email` - candidate email
- `user_secondary_email` - candidate secondary email
- `user_first_name` - candidate first name
- `user_last_name` - candidate last name
- `reference_number` - UIT or Scheduled test idenitifier
- `test_center_name` - Name of the test center
- `dept_eabrv` - English abreviaton of the testing center's dept
- `dept_fabrv` - French abreviaton of the testing center's dept
- `dept_edesc` - English name of the testing center's dept
- `dept_fdesc` - English abreviaton of the testing center's dept
- `primary_contact_user_email` - HR/TA (who requested the candidate take the test) email
- `primary_contact_user_first_name` - HR/TA (who requested the candidate take the test) first name
- `primary_contact_user_last_name` - HR/TA (who requested the candidate take the test) last name
- `test_skill_type_name_en` - Test skill English name
- `test_skill_type_name_fr` - Test skill French name
- `test_skill_sub_type_name_en` - Test subskill English name
- `test_skill_sub_type_name_fr` - Test subskill French name
- `created_date_year` - Year that the process was created
- `created_date_month_en` - Month (English) that the process was created
- `created_date_month_fr` - Month (French) that the process was created
- `created_date_day` - Day that the process was created
- `process_end_date_year` - Year that the process ends
- `process_end_date_month_en` - Month (English) that the process ends
- `process_end_date_month_fr` - Month (French) that the process ends
- `process_end_date_day` - Day that the process ends

### Canceled_Accomodations_Request_Candidate

Email informing CDD/TCM/HR? that a request was canceled by the candidate

- `first_name` - first name of the person to whom the email is being sent
- `last_name` - last name of the person to whom the email is being sent
- `comments` - comments left by the candidate when requesting accomodation
- `rationale` - TODO
- `assigned_to_user_email` - AAE user email
- `assigned_to_user_first_name` - AAE first name
- `assigned_to_user_last_name` - AAE last name
- `user_email` - candidate email
- `user_secondary_email` - candidate secondary email
- `user_first_name` - candidate first name
- `user_last_name` - candidate last name
- `reference_number` - UIT or Scheduled test idenitifier
- `test_center_name` - Name of the test center
- `dept_eabrv` - English abreviaton of the testing center's dept
- `dept_fabrv` - French abreviaton of the testing center's dept
- `dept_edesc` - English name of the testing center's dept
- `dept_fdesc` - English abreviaton of the testing center's dept
- `primary_contact_user_email` - HR/TA (who requested the candidate take the test) email
- `primary_contact_user_first_name` - HR/TA (who requested the candidate take the test) first name
- `primary_contact_user_last_name` - HR/TA (who requested the candidate take the test) last name
- `test_skill_type_name_en` - Test skill English name
- `test_skill_type_name_fr` - Test skill French name
- `test_skill_sub_type_name_en` - Test subskill English name
- `test_skill_sub_type_name_fr` - Test subskill French name
- `created_date_year` - Year that the process was created
- `created_date_month_en` - Month (English) that the process was created
- `created_date_month_fr` - Month (French) that the process was created
- `created_date_day` - Day that the process was created
- `process_end_date_year` - Year that the process ends
- `process_end_date_month_en` - Month (English) that the process ends
- `process_end_date_month_fr` - Month (French) that the process ends
- `process_end_date_day` - Day that the process ends

### Completed_Accomodations_Request

Email to CDD/TCM/HR? that an acccomodations Request was completed

- `first_name` - first name of the person to whom the email is being sent
- `last_name` - last name of the person to whom the email is being sent
- `comments` - comments left by the candidate when requesting accomodation
- `rationale` - TODO
- `assigned_to_user_email` - AAE user email
- `assigned_to_user_first_name` - AAE first name
- `assigned_to_user_last_name` - AAE last name
- `user_email` - candidate email
- `user_secondary_email` - candidate secondary email
- `user_first_name` - candidate first name
- `user_last_name` - candidate last name
- `reference_number` - UIT or Scheduled test idenitifier
- `test_center_name` - Name of the test center
- `dept_eabrv` - English abreviaton of the testing center's dept
- `dept_fabrv` - French abreviaton of the testing center's dept
- `dept_edesc` - English name of the testing center's dept
- `dept_fdesc` - English abreviaton of the testing center's dept
- `primary_contact_user_email` - HR/TA (who requested the candidate take the test) email
- `primary_contact_user_first_name` - HR/TA (who requested the candidate take the test) first name
- `primary_contact_user_last_name` - HR/TA (who requested the candidate take the test) last name
- `test_skill_type_name_en` - Test skill English name
- `test_skill_type_name_fr` - Test skill French name
- `test_skill_sub_type_name_en` - Test subskill English name
- `test_skill_sub_type_name_fr` - Test subskill French name
- `created_date_year` - Year that the process was created
- `created_date_month_en` - Month (English) that the process was created
- `created_date_month_fr` - Month (French) that the process was created
- `created_date_day` - Day that the process was created
- `process_end_date_year` - Year that the process ends
- `process_end_date_month_en` - Month (English) that the process ends
- `process_end_date_month_fr` - Month (French) that the process ends
- `process_end_date_day` - Day that the process ends

### Expired_Accommodation_Request

Email to CDD/TCM/HR? that an acccomodations Request has expired

- `first_name` - first name of the person to whom the email is being sent
- `last_name` - last name of the person to whom the email is being sent
- `comments` - comments left by the candidate when requesting accomodation
- `rationale` - TODO
- `assigned_to_user_email` - AAE user email
- `assigned_to_user_first_name` - AAE first name
- `assigned_to_user_last_name` - AAE last name
- `user_email` - candidate email
- `user_secondary_email` - candidate secondary email
- `user_first_name` - candidate first name
- `user_last_name` - candidate last name
- `reference_number` - UIT or Scheduled test idenitifier
- `test_center_name` - Name of the test center
- `dept_eabrv` - English abreviaton of the testing center's dept
- `dept_fabrv` - French abreviaton of the testing center's dept
- `dept_edesc` - English name of the testing center's dept
- `dept_fdesc` - English abreviaton of the testing center's dept
- `primary_contact_user_email` - HR/TA (who requested the candidate take the test) email
- `primary_contact_user_first_name` - HR/TA (who requested the candidate take the test) first name
- `primary_contact_user_last_name` - HR/TA (who requested the candidate take the test) last name
- `test_skill_type_name_en` - Test skill English name
- `test_skill_type_name_fr` - Test skill French name
- `test_skill_sub_type_name_en` - Test subskill English name
- `test_skill_sub_type_name_fr` - Test subskill French name
- `created_date_year` - Year that the process was created
- `created_date_month_en` - Month (English) that the process was created
- `created_date_month_fr` - Month (French) that the process was created
- `created_date_day` - Day that the process was created
- `process_end_date_year` - Year that the process ends
- `process_end_date_month_en` - Month (English) that the process ends
- `process_end_date_month_fr` - Month (French) that the process ends
- `process_end_date_day` - Day that the process ends

### New_Accomodations_Request_HR

Email to HR to inform them that a candidate has requested an accomodation

- `first_name` - first name of the person to whom the email is being sent
- `last_name` - last name of the person to whom the email is being sent
- `comments` - comments left by the candidate when requesting accomodation
- `rationale` - TODO
- `assigned_to_user_email` - AAE user email
- `assigned_to_user_first_name` - AAE first name
- `assigned_to_user_last_name` - AAE last name
- `user_email` - candidate email
- `user_secondary_email` - candidate secondary email
- `user_first_name` - candidate first name
- `user_last_name` - candidate last name
- `reference_number` - UIT or Scheduled test idenitifier
- `test_center_name` - Name of the test center
- `dept_eabrv` - English abreviaton of the testing center's dept
- `dept_fabrv` - French abreviaton of the testing center's dept
- `dept_edesc` - English name of the testing center's dept
- `dept_fdesc` - English abreviaton of the testing center's dept
- `primary_contact_user_email` - HR/TA (who requested the candidate take the test) email
- `primary_contact_user_first_name` - HR/TA (who requested the candidate take the test) first name
- `primary_contact_user_last_name` - HR/TA (who requested the candidate take the test) last name
- `test_skill_type_name_en` - Test skill English name
- `test_skill_type_name_fr` - Test skill French name
- `test_skill_sub_type_name_en` - Test subskill English name
- `test_skill_sub_type_name_fr` - Test subskill French name
- `created_date_year` - Year that the process was created
- `created_date_month_en` - Month (English) that the process was created
- `created_date_month_fr` - Month (French) that the process was created
- `created_date_day` - Day that the process was created
- `process_end_date_year` - Year that the process ends
- `process_end_date_month_en` - Month (English) that the process ends
- `process_end_date_month_fr` - Month (French) that the process ends
- `process_end_date_day` - Day that the process ends

### Re_Opened_Accomodations_Request

Email to CDD/TCM/HR that an accomodations request was re-opened

- `first_name` - first name of the person to whom the email is being sent
- `last_name` - last name of the person to whom the email is being sent
- `comments` - comments left by the candidate when requesting accomodation
- `rationale` - TODO
- `assigned_to_user_email` - AAE user email
- `assigned_to_user_first_name` - AAE first name
- `assigned_to_user_last_name` - AAE last name
- `user_email` - candidate email
- `user_secondary_email` - candidate secondary email
- `user_first_name` - candidate first name
- `user_last_name` - candidate last name
- `reference_number` - UIT or Scheduled test idenitifier
- `test_center_name` - Name of the test center
- `dept_eabrv` - English abreviaton of the testing center's dept
- `dept_fabrv` - French abreviaton of the testing center's dept
- `dept_edesc` - English name of the testing center's dept
- `dept_fdesc` - English abreviaton of the testing center's dept
- `primary_contact_user_email` - HR/TA (who requested the candidate take the test) email
- `primary_contact_user_first_name` - HR/TA (who requested the candidate take the test) first name
- `primary_contact_user_last_name` - HR/TA (who requested the candidate take the test) last name
- `test_skill_type_name_en` - Test skill English name
- `test_skill_type_name_fr` - Test skill French name
- `test_skill_sub_type_name_en` - Test subskill English name
- `test_skill_sub_type_name_fr` - Test subskill French name
- `created_date_year` - Year that the process was created
- `created_date_month_en` - Month (English) that the process was created
- `created_date_month_fr` - Month (French) that the process was created
- `created_date_day` - Day that the process was created
- `process_end_date_year` - Year that the process ends
- `process_end_date_month_en` - Month (English) that the process ends
- `process_end_date_month_fr` - Month (French) that the process ends
- `process_end_date_day` - Day that the process ends

## Misc

### Failed_Send_Test_Session_Candidates_To_Security

Email to notify the test center manager that the security email did not work when sending the list of Candidates 2 days in the future

- `security_email` - the email address for security

### Password Reset

Email with link to reset the user's password

- `email` - the email address related to the candidate's account
- `url` - the unique url the candidate can click on to reset their password

### Share Result; Draft

Drafted version of email used to share results

- `cdd_firstname` - the candidate/test taker's first name
- `cdd_lastname` - the candidate/test taker's last name
- `test_code` - the test code/identifier
- `test_name_FR` - the test name in French
- `test_name_EN` - the test name in English
- `recipient_name` - the person recieving the email
- `PRI` - candidate's PRI
- `test_submit_date` - the date of the test
- `test_validity_date` - the end validity date
- `converted_score_FR` - the converted value in Frnech
- `total_score` - the actual score
- `converted_score_EN` - the converted value in English

### Test_Session_Candidates_To_Security

Automated email to security listing all candidates who will be part of a test session and the date/time of that session.

- `test_date` - date of the test
- `test_time` - formatted time of the test (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an instance of test_time
- `in_two_days` - two days from today
- `formatted_candidates_list_array` - formatted list of the candidates who will be attending the session

## OLA

### Candidate_Withdrawl_Notification

Notification to candidate that they have withdrawn from a process

- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
- `assessment_process_dept_fdesc` - full French name of the dept requesting the candidate take the test
- `assessment_process_dept_fabrv` - French acryonym of the above dept
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill
- `assessment_process_dept_edesc` - full English name of the dept requesting the candidate take the test
- `assessment_process_dept_eabrv` - English acryonym of the above dept
- `contact_email_for_candidates` - contact in case the candidates need to reach out to someone

### OLA_Code_Booking

Notifcation to candidate that they have booked an OLA

- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
- `dept_fdesc` - full French name of the dept requesting the candidate take the test
- `dept_fabrv` - French acryonym of the above dept
- `reservation_code` - the reservation code (for ease of reference)
- `closing_date` - the closing date of the process
- `contact_email_for_candidates` - contact in case the candidates need to reach out to someone
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill
- `dept_edesc` - full English name of the dept requesting the candidate take the test
- `dept_eabrv` - English acryonym of the above dept

### OLA_Code_Book_Later

Notification to candidate that they have canceled their reservation to be booked later (for OLA)

- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
- `dept_fdesc` - full French name of the dept requesting the candidate take the test
- `dept_fabrv` - French acryonym of the above dept
- `reservation_code` - the reservation code (for ease of reference)
- `closing_date` - the closing date of the process
- `contact_email_for_candidates` - contact in case the candidates need to reach out to someone
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill
- `dept_edesc` - full English name of the dept requesting the candidate take the test
- `dept_eabrv` - English acryonym of the above dept

### OLA_INVITE_CANDIDATE_TO_NON_STANDARD_TEST_SESSION_IN_PERSON

Notification to candidate that they have been assigned to an in-person accommodated test session (for OLA)

- `date` - the date of the test session
- `start_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an
- `end_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an
- `test_name_en` - the test name in English
- `test_name_fr` - the test name in French
- `room_name` - the room name where the test will be taken
- `test_center_address_en` - the address of the test center in english
- `test_center_address_fr` - the address of the test center in french
- `test_center_city_en` - the city of the test center in english
- `test_center_city_fr` - the city of the test center in french
- `test_center_postal_code` - the postal code of the test center
- `test_center_province_en` - the province of the test center in english
- `test_center_province_fr` - the province of the test center in french
- `country_eabrv` - the English acryonym of the above country
- `country_fabrv` - the French acryonym of the above country
- `country_edesc` - the English description of the above country
- `country_fdesc` - the French description of the above country
- `test_center_other_details_text_en` - the English text of the above test center other information
- `test_center_other_details_text_fr` - the French text of the above test center other information

### OLA_INVITE_CANDIDATE_TO_NON_STANDARD_TEST_SESSION_VIRTUAL

Notification to candidate that they have been assigned to a virtual accommodated test session (for OLA)

- `date` - the date of the test session
- `start_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an
- `end_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an
- `test_name_en` - the test name in English
- `test_name_fr` - the test name in French
- `room_name` - the room name where the test will be taken
- `test_center_address_en` - the address of the test center in english
- `test_center_address_fr` - the address of the test center in french
- `test_center_city_en` - the city of the test center in english
- `test_center_city_fr` - the city of the test center in french
- `test_center_postal_code` - the postal code of the test center
- `test_center_province_en` - the province of the test center in english
- `test_center_province_fr` - the province of the test center in french
- `country_eabrv` - the English acryonym of the above country
- `country_fabrv` - the French acryonym of the above country
- `country_edesc` - the English description of the above country
- `country_fdesc` - the French description of the above country
- `test_center_other_details_text_en` - the English text of the above test center other information
- `test_center_other_details_text_fr` - the French text of the above test center other information

### OLA_UPDATE_NON_STANDARD_TEST_SESSION_INVITE_IN_PERSON

Notification to candidate that their assigned in-person accommodated test session has been modified (for OLA)

- `date` - the date of the test session
- `start_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an
- `end_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an
- `test_name_en` - the test name in English
- `test_name_fr` - the test name in French
- `room_name` - the room name where the test will be taken
- `test_center_address_en` - the address of the test center in english
- `test_center_address_fr` - the address of the test center in french
- `test_center_city_en` - the city of the test center in english
- `test_center_city_fr` - the city of the test center in french
- `test_center_postal_code` - the postal code of the test center
- `test_center_province_en` - the province of the test center in english
- `test_center_province_fr` - the province of the test center in french
- `country_eabrv` - the English acryonym of the above country
- `country_fabrv` - the French acryonym of the above country
- `country_edesc` - the English description of the above country
- `country_fdesc` - the French description of the above country
- `test_center_other_details_text_en` - the English text of the above test center other information
- `test_center_other_details_text_fr` - the French text of the above test center other information

### OLA_UPDATE_NON_STANDARD_TEST_SESSION_INVITE_VIRTUAL

Notification to candidate that their assigned virtual accommodated test session has been modified (for OLA)

- `date` - the date of the test session
- `start_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an
- `end_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an
- `test_name_en` - the test name in English
- `test_name_fr` - the test name in French
- `room_name` - the room name where the test will be taken
- `test_center_address_en` - the address of the test center in english
- `test_center_address_fr` - the address of the test center in french
- `test_center_city_en` - the city of the test center in english
- `test_center_city_fr` - the city of the test center in french
- `test_center_postal_code` - the postal code of the test center
- `test_center_province_en` - the province of the test center in english
- `test_center_province_fr` - the province of the test center in french
- `country_eabrv` - the English acryonym of the above country
- `country_fabrv` - the French acryonym of the above country
- `country_edesc` - the English description of the above country
- `country_fdesc` - the French description of the above country
- `test_center_other_details_text_en` - the English text of the above test center other information
- `test_center_other_details_text_fr` - the French text of the above test center other information

### TWO_DAY_REMINDER

Its in the name

- `date` - date of the test session
- `start_time` - start time of the test session

### SEVEN_DAY_REMINDER

Its in the name

- `date` - date of the test session
- `start_time` - start time of the test session
- `last_cancelation_date` - the last date/time (with timezone) that the candidate can cancel. This takes into account the global closing window (if it applies to this test), the booking delay for the test center that the code has been assigned to (if it has been assigned), or the longest booing delay for any test center.

## Supervised

### Delete_Non_Standard_Test_Session

Notification to candidates registered for a non-standard test session that that session is now canceled

- `date` - the date of the test session
- `start_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an
- `end_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an
- `test_name_en` - the test name in English
- `test_name_fr` - the test name in French
- `test_version` - the test version
- `room_name` - the room name where the test will be taken

### Delete_Specific_Test_Session

Notification to candidates registered for a test session that that session is now canceled

- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
- `test_session_date` - the date the canceled test session was scheduled for
- `test_session_start_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an instance of test_time
- `reservation_code` - the reservation code (for ease of reference)
- `closing_date` - the closing date of the process
- `contact_email_for_candidates` - the contact email for the process itself (not the test session)
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill

### Failed_Invitation_Delivery

Email send to the candidate contact if any emails failed to be delivered to the invited candidates

- `process_name` - the name of the process they were invited to
- `email list` - list of emails that were unsuccessfully delivered

### Failed_Send_Assigned_Test_to_Administer_Delivery

Email send to the HR Coordinator if email failed to be delivered to the reservation code invitation action

- `process_name` - the name of the process they were invited to
- `candidate_email` - candidate email that was unsuccessfully delivered

### HR_Triggered_Withdraw

Notification that the HR rep has canceled the test/withrawn the candidate

- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
- `assessment_process_dept_fdesc` - full French name of the dept requesting the candidate take the test
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill
- `assessment_process_dept_eabrv` - English acryonym of the above dept
- `contact_email_for_candidates` - contact in case the candidates need to reach out to someone

### Invite_Candidate_To_Non_Standard_Test_Session

Email send to the candidate to inform him of an invitation of a non-standard test session

- `date` - the date of the test session
- `start_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an
- `end_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an
- `test_name_en` - the test name in English
- `test_name_fr` - the test name in French
- `test_version` - the test version
- `room_name` - the room name where the test will be taken

### Resend_Already_Assigned_Test_to_Administer

Email send to the candidate with details of the reservation code and test to take when the HR Coordinator updates the email of an already assigned candidate/test to administer

- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill
- `assessment_process_dept_fdesc` - full French name of the dept requesting the candidate take the test
- `assessment_process_dept_fabrv` - French acryonym of the above dept
- `assessment_process_dept_edesc` - full English name of the dept requesting the candidate take the test
- `assessment_process_dept_eabrv` - English acryonym of the above dept
- `reservation_code` - the code the candidate must enter into the system in order to book a session
- `assessment_process_closing_date` - end date of the process; tests must be completed before then
- `contact_email_for_candidates` - contact in case the candidates need to reach out to someone
- `site_protocol` - URL protocol that helps you build the needed CAT URL (ex: http://)
- `site_domain` - URL domain that helps you build the needed CAT URL (ex: devwww5.psc-cfp.gc.ca/)

### Reservation_Code_Booking

Email to the candidate with details of the session that they just booked

- `dept_fdesc` - full French name of the dept requesting the candidate take the test
- `dept_fabrv` - French acryonym of the above dept
- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
- `date` - the date of the test session
- `formatted_start_time` - the start time of the test session (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an instance of test_time
- `formatted_end_time` - the end time of the test session (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an instance of test_time
- `formatted_duration` - the length of time of the test session, in minutes
- `tc_dept_fdesc` - the French name of the test center department
- `tc_dept_fabrv` - the French acryonym of the test center department
- `test_center_address_en` - the address of the test center in english
- `test_center_address_fr` - the address of the test center in french
- `room_name` - the room name where the test will be taken
- `test_center_city_en` - the city of the test center in english
- `test_center_city_fr` - the city of the test center in french
- `test_center_province_en` - the province of the test center in english
- `test_center_province_fr` - the province of the test center in french
- `test_center_postal_code` - the postal code of the test center
- `formatted_url` - the CAT url form the env that sent the email
- `booking_delay` - the number of hours before the test session when the candidate can still update their reservation
- `closing_date` - the closing date for the process
- `contact_email_for_candidates` - the contact for the candidates, should they need to email someone about the process
- `dept_edesc` - full English name of the dept requesting the candidate take the test
- `dept_eabrv` - English acryonym of the above dept
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill
- `tc_dept_edesc` - the English name of the test center department
- `tc_dept_eabrv` - the English acryonym of the test center department

### Reservation_Code_Book_Later

Email to the candidate with details of the session that they just canceled, which they can book a different session later

- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
- `dept_fdesc` - full French name of the dept requesting the candidate take the test
- `dept_fabrv` - French acryonym of the above dept
- `reservation_code` - the code for the test, for the candidate's reference (it is already reserved in their account)
- `closing_date` - the closing date for the process
- `contact_email_for_candidates` - the contact for the candidates, should they need to email someone about the process
- `formatted_url` - the CAT url form the env that sent the email
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill
- `tc_dept_edesc` - the English name of the test center department
- `tc_dept_eabrv` - the English acryonym of the test center department

### Reservation_Code_Withdraw_No_Booking; Reservation_Code_Withdraw_From_Booked_Session

Email sent to candidate contact to notify them that the candidate has withdrawn from a process

The only difference between the templates is the origin of the email; the first is when there is no session booked the other is when they have already booked a session

- `first_name` - first name of the candidate contact
- `last_name` - last name of the candidate contact
- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
- `assessment_process_reference_number` - the identifier of the assessment process
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill

### Reservation_Invite

Invitatiton email to a supervised test. Candidate can use the reservation code to schedule a test session before the closing date.

- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
- `assessment_process_dept_fdesc` - full French name of the dept requesting the candidate take the test
- `assessment_process_dept_fabrv` - French acryonym of the above dept
- `reservation_code` - the code the candidate must enter into the system in order to book a session
- `closing_date` - end date of the process; tests must be completed before then
- `contact_email_for_candidates` - contact in case the candidates need to reach out to someone
- `formatted_url` - the CAT url for the given environment (usually prod)
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill
- `assessment_process_dept_edesc` - full English name of the dept requesting the candidate take the test
- `assessment_process_dept_eabrv` - English acryonym of the above dept

### Revoke_Assigned_Test_To_Administer

Email sent to notify the candidate that his reservation code and test to take has been revoked

- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill
- `assessment_process_dept_fdesc` - full French name of the dept requesting the candidate take the test
- `assessment_process_dept_fabrv` - French acryonym of the above dept
- `assessment_process_dept_edesc` - full English name of the dept requesting the candidate take the test
- `assessment_process_dept_eabrv` - English acryonym of the above dept
- `reservation_code` - the code the candidate must enter into the system in order to book a session
- `assessment_process_closing_date` - end date of the process; tests must be completed before then
- `contact_email_for_candidates` - contact in case the candidates need to reach out to someone
- `site_protocol` - URL protocol that helps you build the needed CAT URL (ex: http://)
- `site_domain` - URL domain that helps you build the needed CAT URL (ex: devwww5.psc-cfp.gc.ca/)

### Send_New_Assigned_Test_to_Administer

Email send to the candidate with details of the reservation code and test to take when the HR Coordinator assigns a new test to administer

- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill
- `assessment_process_dept_fdesc` - full French name of the dept requesting the candidate take the test
- `assessment_process_dept_fabrv` - French acryonym of the above dept
- `assessment_process_dept_edesc` - full English name of the dept requesting the candidate take the test
- `assessment_process_dept_eabrv` - English acryonym of the above dept
- `reservation_code` - the code the candidate must enter into the system in order to book a session
- `assessment_process_closing_date` - end date of the process; tests must be completed before then
- `contact_email_for_candidates` - contact in case the candidates need to reach out to someone
- `site_protocol` - URL protocol that helps you build the needed CAT URL (ex: http://)
- `site_domain` - URL domain that helps you build the needed CAT URL (ex: devwww5.psc-cfp.gc.ca/)

### Test_Bumped_by_TC

Test Center canceled reservation, returned code to Candidate.

- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill
- `test_session_date` - the date the canceled test session was scheduled for
- `test_session_start_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an instance of test_time
- `reservation_code` - the code the candidate must enter into the system in order to book a session
- `assessment_process_closing_date` - end date of the process; tests must be completed before then
- `contact_email_for_candidates` - contact in case the candidates need to reach out to someone

### Test_Session_Cancellation_Based_on_Assessment_Process_Data_Updates

Email send to the candidate with details of the reservation code, test to take, possibly updated process closing date and/or possibly updated contact email for candidates

- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill
- `assessment_process_dept_fdesc` - full French name of the dept requesting the candidate take the test
- `assessment_process_dept_fabrv` - French acryonym of the above dept
- `assessment_process_dept_edesc` - full English name of the dept requesting the candidate take the test
- `assessment_process_dept_eabrv` - English acryonym of the above dept
- `reservation_code` - the code the candidate must enter into the system in order to book a session
- `assessment_process_closing_date` - end date of the process; tests must be completed before then
- `contact_email_for_candidates` - contact in case the candidates need to reach out to someone
- `site_protocol` - URL protocol that helps you build the needed CAT URL (ex: http://)
- `site_domain` - URL domain that helps you build the needed CAT URL (ex: devwww5.psc-cfp.gc.ca/)

### Test_Session_Cancellation_Inform_Related_HR_Coordinator

Email send to respective HR Coordinator when one or more of their assigned candidates get their test session cancelled

- `reference_number` - the identifier of the assessment process
- `closing_date` - end date of the process; tests must be completed before then
- `dept_fdesc` - full French name of the dept requesting the candidate take the test
- `dept_fabrv` - French acryonym of the above dept
- `dept_edesc` - full English name of the dept requesting the candidate take the test
- `dept_eabrv` - French acryonym of the above dept
- `invitee_list` - bulleted list of invitees (what the HR rep sees, not the candidate profile) of impacted invitees. Formatted as " \* First_Name Last_Name - Email"

### Update_Active_Assessment_Process_Data

Email send to the candidate with details of the test session that is being cancelled due to assessment process duration update

- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill
- `assessment_process_dept_fdesc` - full French name of the dept requesting the candidate take the test
- `assessment_process_dept_fabrv` - French acryonym of the above dept
- `assessment_process_dept_edesc` - full English name of the dept requesting the candidate take the test
- `assessment_process_dept_eabrv` - English acryonym of the above dept
- `reservation_code` - the code the candidate must enter into the system in order to book a session
- `assessment_process_closing_date` - end date of the process; tests must be completed before then
- `contact_email_for_candidates` - contact in case the candidates need to reach out to someone
- `site_protocol` - URL protocol that helps you build the needed CAT URL (ex: http://)
- `site_domain` - URL domain that helps you build the needed CAT URL (ex: devwww5.psc-cfp.gc.ca/)

### Update_Non_Standard_Test_Session_Invite

Email send to the candidate with details of the test session that is being updated

- `date` - the date of the test session
- `start_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an
- `end_time` - the time the canceled test session was scheduled for (time is adjusted to "Canada/Eastern") i.e. ensure to add ET after an
- `test_name_en` - the test name in English
- `test_name_fr` - the test name in French
- `test_version` - the test version
- `room_name` - the room name where the test will be taken

## UIT

### Deactivate_Uit_Test_Cancellation

Email sent to notify candidates that a UIT has been canceled

- `first_name` - candidate first name
- `last_name` - candidate last name
- `test_fr_name` - French name of the test
- `org_code_fr` - the department requesting they take the test; French acronym
- `test_access_code` - the code the candidate must enter into CAT in order to access the test
- `billing_contact_info` - contact in case the candidates need to reach out to someone
- `test_en_name` - English name of the test
- `org_code_en` - the department requesting they take the test; English acronym

### FAILED_UIT

Email send to the candidate contact if any emails failed to be delivered to the UIT candidates

- `process_name` - the name of the process they were invited to
- `email list` - list of emails that were unsuccessfully delivered

### Modify_Uit_Validity_End_Date

Email to notify candidates of a change to the UIT's end date

- `first_name` - candidate first name
- `last_name` - candidate last name
- `test_fr_name` - French name of the test
- `org_code_fr` - the department requesting they take the test; French acronym
- `deadline_day` - end date; day
- `deadline_month_fr` - end date; French month name
- `deadline_year` - end date, year
- `test_access_code` - the code the candidate must enter into CAT in order to access the test
- `billing_contact_info` - contact in case the candidates need to reach out to someone
- `test_en_name` - English name of the test
- `org_code_en` - the department requesting they take the test; English acronym
- `deadline_month_en` - end date; English month name

### UIT_Invite_Basic/ all UIT_Invite templates

Email sent inviting a candidate to take a UIT. All templates use the following:

- `test_fr_name` - French name of the test
- `test_en_name` - English name of the test
- `first_name` - candidate first name
- `last_name` - candidate last name
- `org_code_fr` - the department requesting they take the test; French acronym
- `deadline_day` - end date; day
- `deadline_month_fr` - end date; French month name
- `deadline_year` - end date, year
- `default_test_time` - the length of time permitted for the test
- `test_access_code` - the code the candidate must enter into CAT in order to access the test
- `cat_url` - the CAT url for the given environment (usually prod)
- `account_setup_help_link_fr` - details on how to set up a CAT account, in French
- `billing_contact_info` - contact in case the candidates need to reach out to someone
- `org_code_en` - the department requesting they take the test; English acronym
- `deadline_month_en` - end date; English month name
- `account_setup_help_link_en` - details on how to set up a CAT account, in English

### Pending_Approval_Accommodations_Request

Email sent to candidate when AAE trigger the complete request action (From AAE Dashboard)

- `user_first_name` - candidate first name
- `user_last_name` - candidate last name
- `test_skill_type_name_en` - the English name of the test skill
- `test_skill_type_name_fr` - the French name of the test skill
- `test_skill_sub_type_name_en` - the English name of the test sub-skill
- `test_skill_sub_type_name_fr` - the French name of the test sub-skill
