# Katalon Setup

## Install Katalon
**WE CURRENTLY DON'T HAVE IT ON THE SOFTWARE CENTER**
1.  Go to: https://katalon.com/download
2.  Select the version 8.x and click Download
3.  Execute the .exe and it should install automatically here: C:\Users\\**yourusername**\\.katalon\packages\Katalon_Studio_Windows_64-8.x.x\Katalon_Studio_Windows_64-8.x.x

## Install Chrome Drivers

1. In Katalon Studio, click on Tools/Update WebDrivers/Chrome
2. Katalon should update the drivers so you can run all the tests on the version of Chrome that you have.

##  Make Katalon connect to the database using Windows User

1. Go to: https://learn.microsoft.com/en-us/sql/connect/jdbc/download-microsoft-jdbc-driver-for-sql-server?view=sql-server-ver16
2. Download Microsoft JDBC Driver for SQL Server for x64
3. Extract to your preferred location
4. Get the sqljdbc_auth.dll file from \sqljdbc_12.6\enu\auth\x64 in your extracted folder
5. Rename and Copy sqljdbc_auth.dll
6. Paste the file in: **KatalonStudioDirectory**\jre\bin
