import "react-app-polyfill/ie9";
import React from "react";
import { createRoot } from "react-dom/client";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
import CacheBuster from "react-cache-buster";
import packageInfo from "../package.json";
import "react-tooltip/dist/react-tooltip.css";
import CustomRouter from "./CustomRouter";
import store, { history } from "./store-index";

const domNode = document.getElementById("root");
const root = createRoot(domNode);

root.render(
  <CacheBuster
    currentVersion={packageInfo.version}
    isEnabled={true} // If false, the library is disabled.
    isVerboseMode={false} // If true, the library writes verbose logs to console.
    metaFileDirectory={"./"}
  >
    {/* Provider -> Provides Redux States to Sub-Components */}
    <Provider store={store}>
      {/* CustomRouter -> Provides History + Locations to Sub-Routes */}
      <CustomRouter history={history}>
        {/* App -> Parent of All Application's Components */}
        <App />
      </CustomRouter>
    </Provider>
  </CacheBuster>
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
