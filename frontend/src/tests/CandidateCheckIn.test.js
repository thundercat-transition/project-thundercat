import React from "react";
import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import UnconnectedCandidateCheckIn from "../CandidateCheckIn";

// Create a mock reducer
const mockReducer = (state = {}, action) => state;

describe("CandidateCheckIn Component", () => {
  it("when profileCompleted is true", () => {
    // Configure the store with the mock reducer
    const store = configureStore({
      reducer: {
        mock: mockReducer,
        accommodations: (state = { fontSize: "16px" }, action) => state,
        localize: (state = { currentLanguage: "en" }, action) => state,
        user: (state = { profileCompleted: true }, action) => state,
        siteAdminSettings: (state = { qualityOfLife: false }, action) => state
      }
    });

    render(
      <Provider store={store}>
        <UnconnectedCandidateCheckIn />
      </Provider>
    );
    expect(screen.getByTestId("unit-test-profile-completed")).toBeInTheDocument();
  });

  it("when profileCompleted is false", () => {
    // Configure the store with the mock reducer
    const store = configureStore({
      reducer: {
        mock: mockReducer,
        accommodations: (state = { fontSize: "16px" }, action) => state,
        localize: (state = { currentLanguage: "en" }, action) => state,
        user: (state = { profileCompleted: false }, action) => state,
        siteAdminSettings: (state = { qualityOfLife: false }, action) => state
      }
    });

    render(
      <Provider store={store}>
        <UnconnectedCandidateCheckIn />
      </Provider>
    );
    expect(screen.queryByTestId("unit-test-profile-completed")).not.toBeInTheDocument();
  });
});
