import React, { useRef, useEffect } from "react";
import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import TestNavBar from "../TestNavBar";
import { configureStore } from "@reduxjs/toolkit";

// Create a mock reducer
const mockReducer = (state = {}, action) => state;

// Configure the store with the mock reducer
const store = configureStore({
  reducer: {
    mock: mockReducer,
    localize: (state = { currentLanguage: "en" }, action) => state,
    testStatus: (
      state = { isTestActive: true, isTestPreTestOrTransition: true, isTestInvalidated: false },
      action
    ) => state,
    login: (state = { authenticated: true }, action) => state,
    accommodations: (state = { fontSize: "16px" }, action) => state
  }
});

// Mock sub-components
jest.mock("../components/commons/CustomButton", () => () => (
  <div data-testid="custom-button">CustomButton</div>
));
jest.mock("../components/commons/ReturnHome", () => () => (
  <div data-testid="return-home">ReturnHome</div>
));
jest.mock("../components/commons/QuitTest", () => () => (
  <div data-testid="quit-test">QuitTest</div>
));
jest.mock("../components/commons/ToolsButton", () => () => (
  <div data-testid="tools-button">ToolsButton</div>
));
jest.mock("../components/commons/Settings", () => () => <div data-testid="settings">Settings</div>);

// Unit Tests
describe("TestNavBar Component", () => {
  let mockSetNavBarHeight;

  beforeEach(() => {
    mockSetNavBarHeight = jest.fn();
  });

  it("renders without crashing", () => {
    render(
      <Provider store={store}>
        <TestNavBar setNavBarHeight={mockSetNavBarHeight} />
      </Provider>
    );
    expect(screen.getByTestId("test-nav-bar-component")).toBeInTheDocument();
  });

  it("renders CustomButton", () => {
    render(
      <Provider store={store}>
        <TestNavBar
          isTestActive={true}
          isTestInvalidated={false}
          setNavBarHeight={mockSetNavBarHeight}
        />
      </Provider>
    );
    const customButtons = screen.getAllByTestId("custom-button");
    expect(customButtons.length).toBe(2);
  });

  it("renders ReturnHome button when showReturnHomeButton is true", () => {
    render(
      <Provider store={store}>
        <TestNavBar
          showReturnHomeButton={true}
          handleReturnHome={jest.fn()}
          setNavBarHeight={mockSetNavBarHeight}
        />
      </Provider>
    );
    expect(screen.getByTestId("return-home")).toBeInTheDocument();
  });

  it("renders QuitTest button when showQuitTestButton is true", () => {
    render(
      <Provider store={store}>
        <TestNavBar
          showQuitTestButton={true}
          handleQuitTest={jest.fn()}
          setNavBarHeight={mockSetNavBarHeight}
        />
      </Provider>
    );
    expect(screen.getByTestId("quit-test")).toBeInTheDocument();
  });

  // test to see if the height of TestNavBar is detected correctly
  it("calls setNavBarHeight with correct value", () => {
    // Set a Height for TestNavBarHeight test
    const testNavBarHeight = 100;

    // TestNavBar needs a ref for the height; we are faking it here
    const TestNavBarWithRef = () => {
      // Fake a ref to TestNavBarHeight
      const testNavBarHeightRef = useRef({ clientHeight: testNavBarHeight });
      useEffect(() => {
        // if the ref has data, execute mockSetNarBarHeight
        if (testNavBarHeightRef.current) {
          mockSetNavBarHeight(testNavBarHeightRef.current.clientHeight);
        }
      }, []);
      return (
        <TestNavBar
          setNavBarHeight={mockSetNavBarHeight}
          testNavBarHeightRef={testNavBarHeightRef}
        />
      );
    };

    // Render the TestNavBarWithRef faked component
    render(
      <Provider store={store}>
        <TestNavBarWithRef />
      </Provider>
    );
    expect(mockSetNavBarHeight).toHaveBeenCalledWith(testNavBarHeight);
  });
});
