**Notice:**
Some tests in this project have not been converted from Enzyme to Testing Library yet. These old tests can be found in the `old_tests` folder located in the main `frontend` directory.
