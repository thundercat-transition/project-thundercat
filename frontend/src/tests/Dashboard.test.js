import React from "react";
import { render, screen } from "@testing-library/react";
import { UnconnectedDashboard } from "../Dashboard";
import LOCALIZE from "../text_resources";
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";

// Mock imports - Found in _mocks_ folder
jest.mock("react-markdown");
jest.mock("rehype-raw");

// Mock sub-components
jest.mock("../components/candidate/AssignedTestTable", () => () => <div>AssignedTestTable</div>);
jest.mock("../components/candidate/ReservationManagement", () => () => (
  <div>ReservationManagement</div>
));
jest.mock("../MyTests", () => () => <div>MyTests</div>);

// Create a mock reducer
const mockReducer = (state = {}, action) => state;

// Configure the store with the mock reducer
const store = configureStore({
  reducer: {
    mock: mockReducer,
    localize: (state = { currentLanguage: "en" }, action) => state,
    testBuilder: (state = { validation_errors: [] }, action) => state,
    accommodations: (state = { fontSize: "16px" }, action) => state
  }
});

describe("Dashboard Component", () => {
  test("renders title correctly with user's full name", () => {
    render(
      <Provider store={store}>
        <UnconnectedDashboard firstName="John" lastName="Doe" username="johndoe" />
      </Provider>
    );
    const title = screen.getByRole("heading", { level: 1 });
    expect(title).toHaveTextContent(LOCALIZE.formatString(LOCALIZE.dashboard.title, "John", "Doe"));
  });

  test("renders AssignedTestTable component", () => {
    render(
      <Provider store={store}>
        <UnconnectedDashboard firstName="John" lastName="Doe" username="johndoe" />
      </Provider>
    );
    const assignedTestTable = screen.getByText("AssignedTestTable");
    expect(assignedTestTable).toBeInTheDocument();
  });

  test("renders ReservationManagement component", () => {
    render(
      <Provider store={store}>
        <UnconnectedDashboard firstName="John" lastName="Doe" username="johndoe" />
      </Provider>
    );
    const reservationManagement = screen.getByText("ReservationManagement");
    expect(reservationManagement).toBeInTheDocument();
  });

  test("renders MyTests component", () => {
    render(
      <Provider store={store}>
        <UnconnectedDashboard firstName="John" lastName="Doe" username="johndoe" />
      </Provider>
    );
    const myTests = screen.getByText("MyTests");
    expect(myTests).toBeInTheDocument();
  });
});
