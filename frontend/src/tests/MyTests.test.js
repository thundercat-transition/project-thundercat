import React from "react";
import { Provider } from "react-redux";
import { render, screen } from "@testing-library/react";
import { UnconnectedMyTests as MyTests } from "../MyTests";
import LOCALIZE from "../text_resources";
import { configureStore } from "@reduxjs/toolkit";
import { MemoryRouter } from "react-router-dom";

// Create a mock reducer
const mockReducer = (state = {}, action) => state;

// Configure the store with the mock reducer
const store = configureStore({
  reducer: {
    mock: mockReducer,
    localize: (state = { currentLanguage: "en" }, action) => state,
    testBuilder: (state = { validation_errors: [] }, action) => state,
    accommodations: (state = { fontSize: "16px" }, action) => state
  }
});

describe("renders component content", () => {
  // Mocked Redux Actions
  const mockGetValidScoredTests = jest.fn().mockResolvedValue([]);
  const mockGetArchivedScoredTests = jest.fn().mockResolvedValue([]);

  render(
    <Provider store={store}>
      {/* TODO: find another way; this is the only fix I found and it looks sketchy */}
      <MemoryRouter future={{ v7_relativeSplatPath: true, v7_startTransition: true }}>
        <MyTests
          getValidScoredTests={mockGetValidScoredTests}
          getArchivedScoredTests={mockGetArchivedScoredTests}
          firstName="Testing"
          lastName="MyTests"
        />
      </MemoryRouter>
    </Provider>
  );

  it("should render alert messages", () => {
    expect(screen.getByText(LOCALIZE.myTests.alertCatTestsOnly1)).toBeInTheDocument();
    expect(screen.getByText(LOCALIZE.myTests.alertCatTestsOnly2)).toBeInTheDocument();
  });
});
