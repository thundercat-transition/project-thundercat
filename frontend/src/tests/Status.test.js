import React from "react";
import { render, screen, act } from "@testing-library/react";
import { Provider } from "react-redux";
import Status from "../Status";
import { configureStore } from "@reduxjs/toolkit";
import LOCALIZE from "../text_resources";
import { LANGUAGES } from "../modules/LocalizeRedux";

// Mock fetch function
global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve({})
  })
);

// Create a mock reducer
const mockReducer = (state = {}, action) => state;

// Configure the store with the mock reducer
const store = configureStore({
  reducer: {
    mock: mockReducer,
    localize: (state = { currentLanguage: "en" }, action) => state
  }
});

describe("Status Component", () => {
  test("renders without crashing", async () => {
    // Since we are using Fetch (mocked); we need to wait for the component to render
    await act(async () => {
      render(
        <Provider store={store}>
          <Status />
        </Provider>
      );
    });
    expect(screen.getByTestId("status-component")).toBeInTheDocument();
  });

  test("displays text in English when currentLanguage is 'en'", async () => {
    // Set our language to English
    LOCALIZE.setLanguage(LANGUAGES.english);

    const storeWithEnglish = configureStore({
      reducer: {
        mock: mockReducer,
        localize: (state = { currentLanguage: "en" }, action) => state
      }
    });

    // Since we are using Fetch (mocked); we need to wait for the component to render
    await act(async () => {
      render(
        <Provider store={storeWithEnglish}>
          <Status />
        </Provider>
      );
    });
    expect(screen.getByText(LOCALIZE.statusPage.welcomeMsg)).toBeInTheDocument();
  });

  test("displays text in French when currentLanguage is 'fr'", async () => {
    // Set our language to French
    LOCALIZE.setLanguage(LANGUAGES.french);

    const storeWithFrench = configureStore({
      reducer: {
        mock: mockReducer,
        localize: (state = { currentLanguage: "fr" }, action) => state
      }
    });

    // Since we are using Fetch (mocked); we need to wait for the component to render
    await act(async () => {
      render(
        <Provider store={storeWithFrench}>
          <Status />
        </Provider>
      );
    });
    expect(screen.getByText(LOCALIZE.statusPage.welcomeMsg)).toBeInTheDocument();
  });
});
