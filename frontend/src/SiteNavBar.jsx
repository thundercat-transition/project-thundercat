import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "./css/lib/aurora.min.css";
import "./css/cat-theme.css";
import LoginButton from "./components/commons/LoginButton";
import ToolsButton from "./components/commons/ToolsButton";
import Settings from "./components/commons/Settings";
import Translation from "./components/commons/Translation";
import LOCALIZE from "./text_resources";
import psc_logo_en from "./images/psc_logo_en.png";
import psc_logo_fr from "./images/psc_logo_fr.png";
import canada_logo from "./images/canada_logo.png";
import { Navbar, Nav, Row, Col } from "react-bootstrap";
import SelectLanguage from "./SelectLanguage";
import { LANGUAGES } from "./modules/LocalizeRedux";
import { PATH } from "./components/commons/Constants";
import "./css/site-nav-bar.css";
import SiteNavMenu from "./SiteNavMenu";
import { setNavBarHeight } from "./modules/TestSectionRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import CustomButton from "./components/commons/CustomButton";
import THEME from "./components/commons/CustomButtonTheme";
import withRouter from "./components/withRouter";

const styles = {
  banner: {
    width: "100%"
  },
  navLinks: {
    color: "#00565e"
  },
  menuStyle: {
    float: "left"
  },
  homeStyle: {
    float: "right"
  },
  secondaryNavBar: {
    height: "auto",
    padding: "0.5rem 0"
  },
  topImg: {
    justifyContent: "space-between",
    backgroundColor: "#fff",
    flexFlow: "row nowrap",
    position: "relative",
    display: "flex",
    alignItems: "center",
    paddingTop: 0.5,
    paddingBottom: 0.5,
    paddingLeft: 1,
    paddingRight: 1,
    marginLeft: 0,
    width: "100%"
  },
  navBrandRow: {
    paddingRight: 15,
    paddingLeft: 15
  },
  row: {
    padding: "10px 0 10px 0"
  }
};

const SiteNavBar = props => {
  // Props from Redux
  // currentLanguage: PropTypes.string,
  // isTestActive: PropTypes.bool.isRequired,
  // authenticateAction: PropTypes.func,
  // logoutAction: PropTypes.func

  const siteNavBarHeightRef = React.createRef();

  // States
  const [state, setState] = useState({
    setFocusOnQuitTestButton: false,
    hideToolsButton: false
  });

  // Equivalent of componentDidMount
  useEffect(() => {
    // updating nav bar height redux state
    if (siteNavBarHeightRef.current) {
      props.setNavBarHeight(siteNavBarHeightRef.current.clientHeight);
    }
    // if pathname does not include sample test or test base path
    if (
      !props.location.pathname.includes(PATH.sampleTests) &&
      !props.location.pathname.includes(PATH.testBase)
    ) {
      // need to hide the tools button
      setState({ hideToolsButton: true });
    }
  }, []);

  // Equivalent of componentDidMount
  // -> siteNavBarHeightRef
  useEffect(() => {
    // updating nav bar height redux state
    if (siteNavBarHeightRef.current) {
      props.setNavBarHeight(siteNavBarHeightRef.current.clientHeight);
    }
  }, [siteNavBarHeightRef]);

  // Navigation bar font size conversion (adding 4px to the selected font size)
  const navBarFontSize = `${(parseInt(props.fontSize.substring(0, 2)) + 4).toString()}px`;
  const navLinksStyle = { ...styles.navLinks, fontSize: navBarFontSize };

  // Determine if user has already selected a language.
  const isLanguageSelected = props.currentLanguage !== "";

  return (
    <div ref={siteNavBarHeightRef} className="notranslate">
      <a href="#main-content" className="visually-hidden" id="skip-to-main-content-link-id">
        {LOCALIZE.mainTabs.skipToMain}
      </a>
      {!isLanguageSelected && <SelectLanguage />}
      {isLanguageSelected &&
        !props.isTestActive &&
        !props.errorStatus &&
        !props.isTestInvalidated && (
          <div>
            <div>
              <div aria-label="Main">
                <Row style={styles.topImg}>
                  <Col>
                    <img
                      alt={LOCALIZE.mainTabs.psc}
                      src={props.currentLanguage === LANGUAGES.french ? psc_logo_fr : psc_logo_en}
                      width="370px"
                      height="27.75px"
                      className="align-top"
                    />
                  </Col>
                  <Col className="text-right">
                    <img
                      alt={LOCALIZE.mainTabs.canada}
                      src={canada_logo}
                      width="120px"
                      height="28.77px"
                      className="align-top"
                    />
                  </Col>
                </Row>
                <div className="notranslate">
                  <Navbar
                    className="nav-two w-100"
                    style={styles.secondaryNavBar}
                    bg="light"
                    variant="light"
                    expand={
                      parseInt(props.accommodations.fontSize.split("px")[0]) < 20 ? "md" : "lg"
                    }
                  >
                    <Navbar.Brand>
                      <Row style={styles.navBrandRow}>
                        {!props.authenticated && (
                          <Nav.Link
                            href={PATH.login}
                            style={navLinksStyle}
                            id="navigation-bar-main-link-id"
                            className="notranslate"
                          >
                            {LOCALIZE.mainTabs.homeTabTitleUnauthenticated}
                          </Nav.Link>
                        )}
                        {props.authenticated && (
                          <div>
                            <div style={styles.menuStyle} className="notranslate">
                              <SiteNavMenu />
                            </div>
                            <div style={styles.homeStyle} className="notranslate">
                              <Nav.Link
                                href={
                                  props.isSuperUser
                                    ? PATH.superuserDashboard
                                    : props.isEtta || props.isRdOperations
                                      ? PATH.systemAdministration
                                      : props.isPpc
                                        ? PATH.ppcAdministration
                                        : props.isTcm
                                          ? PATH.testCenterManager
                                          : props.isScorer
                                            ? PATH.scorerBase
                                            : props.isTa
                                              ? PATH.testAdministration
                                              : props.isTb
                                                ? PATH.testBuilder
                                                : props.isTd
                                                  ? PATH.testBuilder
                                                  : props.isHrCoordinator
                                                    ? PATH.inPersonTesting
                                                    : props.isAae
                                                      ? PATH.aae
                                                      : props.isConsultationServices
                                                        ? PATH.consultationServices
                                                        : PATH.dashboard
                                }
                                style={navLinksStyle}
                              >
                                {LOCALIZE.mainTabs.homeTabTitleAuthenticated}
                              </Nav.Link>
                            </div>
                          </div>
                        )}
                        <Nav.Link
                          href={PATH.sampleTests}
                          style={navLinksStyle}
                          className="notranslate"
                        >
                          {LOCALIZE.mainTabs.sampleTests}
                        </Nav.Link>
                      </Row>
                    </Navbar.Brand>
                    <Navbar.Toggle
                      label={LOCALIZE.ariaLabel.navExpandButton}
                      aria-controls="basic-navbar-nav"
                    >
                      <CustomButton
                        label={
                          <div>
                            <FontAwesomeIcon icon={faBars} style={{ height: "1.4em" }} />
                          </div>
                        }
                        buttonTheme={
                          props.isTestActive || props.isTestInvalidated
                            ? THEME.PRIMARY_IN_TEST
                            : THEME.SECONDARY
                        }
                        customStyle={{
                          marginRight: "16px",
                          minWidth: 0,
                          fontSize: props.accommodations.fontSize * 1.5
                        }}
                      />
                    </Navbar.Toggle>
                    <Navbar.Collapse id="basic-navbar-nav">
                      <Row className="justify-content-end w-100" style={styles.row}>
                        <LoginButton />
                        <ToolsButton hideButton={state.hideToolsButton} />
                        <Settings />
                        <Translation />
                      </Row>
                    </Navbar.Collapse>
                  </Navbar>
                </div>
              </div>
            </div>
          </div>
        )}
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    isTestActive: state.testStatus.isTestActive,
    authenticated: state.login.authenticated,
    isSuperUser: state.user.isSuperUser,
    isEtta: state.userPermissions.isEtta,
    isRdOperations: state.userPermissions.isRdOperations,
    isPpc: state.userPermissions.isPpc,
    isTcm: state.userPermissions.isTcm,
    isTa: state.userPermissions.isTa,
    isScorer: state.userPermissions.isScorer,
    isTb: state.userPermissions.isTb,
    isTd: state.userPermissions.isTd,
    isHrCoordinator: state.userPermissions.isHrCoordinator,
    isAae: state.userPermissions.isAae,
    isConsultationServices: state.userPermissions.isConsultationServices,
    errorStatus: state.errorStatusRedux.errorStatus,
    fontSize: state.accommodations.fontSize,
    isTestInvalidated: state.testStatus.isTestInvalidated,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setNavBarHeight
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SiteNavBar));
