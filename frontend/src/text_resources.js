/* eslint-disable no-irregular-whitespace */
import LocalizedStrings from "react-localization";

const LOCALIZE_OBJ = {
  en: {
    // Main Tabs
    mainTabs: {
      homeTabTitleUnauthenticated: "Home",
      homeTabTitleAuthenticated: "Home",
      dashboardTabTitle: "Dashboard",
      sampleTest: "Sample e-MIB",
      sampleTests: "Sample Tests",
      statusTabTitle: "Status",
      psc: "Public Service Commission of Canada",
      canada: "Government of Canada",
      skipToMain: "Skip to main content",
      menu: "MENU"
    },

    // HTML Page Titles
    titles: {
      CAT: "OÉC/CAT - CFP/PSC",
      sampleTests: "CAT - Sample Tests",
      eMIBOverview: "e-MIB Overview",
      eMIB: "e-MIB Assessment",
      uitTest: "CAT - Test: {0}",
      status: "CAT - System Status",
      home: "CAT - Home",
      homeWithError: "Error CAT - Home",
      profile: "CAT - Profile",
      systemAdministration: "CAT - Business Operations",
      rdOperations: "CAT - R&D Operations",
      systemAdminAndRdOperations: "CAT - Operations",
      testAdministration: "CAT - Test Administrator",
      testBuilder: "CAT - Test Builder",
      itemBankPermissions: "CAT - Item Bank Accesses",
      itemBankEditor: "CAT - Item Bank Editor",
      itemEditor: "CAT - Item Editor",
      ppcAdministration: "CAT - R&D Dashboard",
      testCenterManager: "CAT - Test Centre Coordinator",
      resetPassword: "CAT - Reset Password",
      inPersonTesting: "CAT - HR Coordinator",
      aae: "CAT - Assessment Accommodations",
      consultationServices: "CAT - Consultation Services",
      scorer: "CAT - Assessor"
    },

    // Site Nav Bar
    siteNavBar: {
      settings: "Display"
    },

    accommodations: {
      notificationPopup: {
        title: "Accessibility Display Settings",
        description1: "The current display settings have been saved.",
        description2: "When you log in, these settings will be automatically applied."
      },
      defaultFonts: {
        fontSize: "16px (default)",
        fontFamily: "Nunito Sans (Default)"
      }
    },

    // authentication
    authentication: {
      login: {
        title: "Login",
        content: {
          title: "Login",
          inputs: {
            emailTitle: "Email Address or Username:",
            passwordTitle: "Password:",
            showPassword: "Show Password"
          }
        },
        button: "Login",
        forgotPassword: "Forgot Password?",
        forgotPasswordPopup: {
          title: "Forgot Password?",
          description: "Enter the email address of the account that requires a password reset.",
          emailAddressLabel: "Email Address:",
          sendResetLinkButton: "Send password reset link",
          invalidEmailAddressErrorMessage: "Must be a valid email address",
          usernameDoesNotExistErrorMessage: "Email provided does not match an active account",
          emailSentSuccessfully: "An email has been sent to this address",
          emailSentSuccessfullMessage: `If you didn't receive the email, verify the email address and click the "Resend Email" button below.`,
          resendEmail: "Resend Email"
        },
        invalidCredentials: "Email address and/or password is invalid",
        passwordFieldSelected: "Password field selected."
      },
      createAccount: {
        title: "Create an account",
        content: {
          title: "Create an account",
          description: "Please fill out the following information to create an account.",
          inputs: {
            valid: "Valid",
            firstNameTitle: "First Name:",
            firstNameError: "Must be a valid First Name",
            lastNameTitle: "Last Name:",
            lastNameError: "Must be a valid Last Name",
            usernameTitle: "Username (optional):",
            usernameAlreadyUsedError: "This username has already been used",
            usernameAlreadyUsedInEmailError:
              "This username has already been used as another account's email address.",
            dobDayTitle: "Date of Birth:",
            dobError: "Must be a valid Date",
            psrsAppIdTitle: "PSRS Applicant ID (if applicable):",
            psrsAppIdError: "Must be a valid PSRS Applicant ID",
            emailTitle: "Email Address:",
            emailError: "Must be a valid Email Address",
            emailAlreadyUsedInUsernameError:
              "This email address has already been used as another account's username.",
            priTitle: "PRI (if applicable):",
            militaryNbrTitle: "Service Number (if applicable):",
            priError: "Must be a valid PRI",
            militaryNbrError: "Must be a valid Service Number",
            passwordTitle: "Password:",
            showPassword: "Show Password",
            passwordErrors: {
              description: "Your password must contain the following:",
              upperCase: "At least one uppercase letter",
              lowerCase: "At least one lowercase letter",
              digit: "At least one number",
              specialCharacter: "At least one special character (#?!@$%^&*-)",
              length: "Minimum of 8 characters and maximum of 15"
            },
            passwordConfirmationTitle: "Confirm Password:",
            showPasswordConfirmation: "Show Password Confirmation",
            passwordConfirmationError: "Your password confirmation must match your New Password"
          }
        }
      },
      twoFactorAuth: {
        title: "Two Factor Authentication",
        content: {
          title: "Check your email for a security code",
          description: "Enter the code within 15 minutes.",
          label: "Security Code",
          newCode: "A new code has been sent to your email",
          resend: "Send a new code",
          invalidCode:
            "The security code provided does not match our records. Double-check your email or request a new code.",
          expiredCode:
            "This code has expired. Please resend the code and try again within 15 minutes."
        },
        privacyNotice:
          "I have read the {0} and I accept the manner in which the Public Service Commission collects, uses and discloses personal information.",
        privacyNoticeLink: "Privacy Notice",
        privacyNoticeError: "You must accept the privacy notice by clicking on the checkbox",
        button: "Create account",
        accountAlreadyAssociatedToThisEmailError:
          "An account is already associated to this email address",
        passwordTooCommonError: "This password is too common",
        passwordTooSimilarToUsernameError: "The password is too similar to the username",
        passwordTooSimilarToFirstNameError: "The password is too similar to the first name",
        passwordTooSimilarToLastNameError: "The password is too similar to the last name",
        passwordTooSimilarToEmailError: "The password is too similar to the email"
      },
      resetPassword: {
        title: "Reset Password",
        content: {
          title: "Reset Password",
          newPassword: "New Password:",
          showNewPassword: "Show New Password",
          confirmNewPassword: "Confirm New Password:",
          showConfirmNewPassword: "Show New Password Confirmation",
          button: "Reset Password"
        },
        popUpBox: {
          title: "Password Reset Successful",
          description: `Click "OK" to log in.`
        }
      }
    },

    // Menu Items
    menu: {
      scorer: "Test Scorer",
      etta: "Business Operations",
      rdOperations: "R&D Operations",
      operations: "Operations",
      ppc: "R&D Dashboard",
      testCenterManager: "Test Center Coordinator",
      ta: "Test Administrator",
      testBuilder: "Test Developer",
      inPersonTesting: "HR Coordinator",
      checkIn: "Check-in",
      profile: "My Profile",
      incidentReport: "Incident Reports",
      MyTests: "My Tests",
      ContactUs: "Contact Us",
      logout: "Logout",
      superuser: "Superuser Dashboard",
      aae: "Assessment Accommodations",
      consultationServices: "Consultation Services"
    },

    // Token Expired Popup Box
    tokenExpired: {
      title: "Session Expired",
      description:
        "Your session has expired due to inactivity. Please log in again to access your account."
    },

    // Home Page
    homePage: {
      welcomeMsg: "Welcome to the Candidate Assessment Tool",
      description: `This application is used to assess candidates for positions in the federal public service. Please login to access your test. If you do not have an account, select the "Create an account" link to register.`,
      description2fa:
        "This website uses two-factor authentication for users with additional permissions. This step helps keep the Candidate assessment tool secure by verifying your account each time you log in."
    },

    // Sample Tests Page
    sampleTestDashboard: {
      title: "Sample Tests",
      paragraph1p1: "The purpose of the ",
      paragraph1bold: "sample tests",
      paragraph1p2:
        " is to allow you to familiarize yourself with the format and question types found in the actual test, to enable you to try the different configurations of the accessibility display settings, and to provide guidance on how to navigate on the Candidate Assessment Tool. They are not intended to assess your performance level and you will not be assigned a score. You can retake these sample tests as many times as you like.",
      paragraph2: "Click on “Start” to access the sample test selected.",
      paragraph3:
        "Please ensure that you complete the actual test that was assigned to you by the deadline indicated in your invitation email.",
      table: {
        columnOne: "Sample Test",
        columnTwo: "Action",
        viewButton: "Start",
        viewButtonAccessibilityLabel: "Start the {0} test."
      }
    },

    // Retest Period Warning
    retestPeriodWarningPopup: {
      dashboardTitle: "Start Test During Retest Waiting Period of a Previous Test?",
      bookingTitle: "Reserve Test Session During Retest Waiting Period?",
      systemMessageDescription:
        "You must respect the waiting period if you have previously taken this test, regardless of its format (unsupervised, online or paper). If you retake this test before the end of the waiting period, your result will not be valid and a new waiting period will be set, starting from the date of your last test.",
      description: "Are you sure you want to proceed?",
      checkboxLabel: "Yes I wish to proceed."
    },

    accommodationsRequestDetailsPopup: {
      viewAccommodationsRequestDetails: "View Accommodations Request Details",
      viewAccommodationsRequestDetailsAccessibilityLabel:
        "View accommodations request details for the {0}",
      viewAccommodationsRequestDetailsAccessibilityLabelOf:
        "View accommodations request details of {0}",
      viewAlternateTestRequestDetails: "View alternate test request details",
      viewAlternateTestRequestDetailsAccessibilityLabel:
        "View alternate test request details for the {0}",
      viewAlternateTestRequestDetailsAccessibilityLabelOf:
        "View alternate test request details of {0}",
      title: "Accommodations Request Details",
      titleAlternateTestRequest: "Alternate test request details",
      description: "Below are the details of your accommodations request for the {0} test.",
      descriptionAlternateTestRequest1: "User: {0} {1} ({2})",
      descriptionAlternateTestRequest2: "Test: {0}",
      descriptionAlternateTestRequest3: "Below are the details of your alternate test request.",
      requestDateLabel: "Request Date:",
      requestNumberLabel: "Request Number:",
      requestStatusLabel: "Request Status:",
      phoneNumberLabel: "Phone Number:",
      aaeExpertLabel: "Assessment Specialist:",
      aaeExpertEmailLabel: "Assessment Specialist Contact Information:",
      hrContactInfoLabel: "HR Contact Info:",
      generalInfo: {
        generalInfoLabel: "General Information:",
        generalInfoContent: {
          introductionHeader: "General Information",
          introductionParagraph1:
            "The assessment accommodations recommended in this report have been issued exclusively for the assessed individual for a single completion of the test. This report should be kept confidential and used solely for evaluation purposes. In addition, the report must not be used for any other test, including a re-test (taking the same test again).",
          introductionParagraph2:
            "Should the assessed individual be invited to take the same test again and require accommodations, they must submit a new request for assessment accommodations in the Candidate’s Assessment Tool (CAT).",
          introductionParagraph3:
            "Please note that only assessment accommodation specialists from the Personnel Psychology Centre (PPC) can modify this report. Any modifications made by an unauthorized source will render it invalid. In addition, assessment accommodations which are not included in this report are not recommended.",
          importantNotesHeader: "Important Notes on Roles and Responsibilities",
          importantNotesParagraph1:
            "When a Public Service Commission (PSC) standardized test is used, the accommodations recommended by the PPC are mandatory and must be implemented as described, whether administered by the PSC or the Organization.",
          importantNotesParagraph2:
            "When an assessment accommodation is requested for a test developed by the Organization (e.g., interview, simulation, virtual test), the PPC will make accommodation recommendations. The Organization is responsible for the implementation of these recommendations.  ",
          candidateDeclarationHeader: "Candidate Declaration",
          candidateDeclarationParagraph:
            "I have read and understood the assessment accommodation recommendations in this report and acknowledge that they will be applied as described during my test."
        }
      },
      testToAdministerLabel: "Test to Administer:",
      timingAndBreaksTitle: "Time and Breaks:",
      totalTestTimeLabel: "Total Test Time:",
      additionalTimeDetails: "includes {0} extra minutes",
      totalBreakBankLabel: "Total Break Time Bank:",
      otherMeasuresLabel: "Other Measures:",
      cancelRequestButton: "Cancel Request",
      cancelRequestConfirmationPopup: {
        title: "Are you sure you want to cancel your request?",
        systemMessageDescription:
          "Your accommodations request will be cancelled. This action cannot be revoked!",
        systemMessageDescriptionAlternateTestRequest:
          "Your alternate test request will be cancelled. This action cannot be revoked!",
        description: "Are you sure you want to continue?"
      },
      approvalSuccessPopup: {
        title: "Accepted Successfully",
        systemMessageDescription: "You accommodation request has been approved successfully."
      },
      printDownload: "Print or Download",
      printDownloadDescription: "{0}",
      printDocumentTitle: "Accommodation Request Details - {0}",
      printComponent: {
        draftWatermark: "-PREVIEW ONLY-",
        title: "PERSONNEL PSYCHOLOGY CENTRE (PPC)",
        reportSection: {
          title: "Assessment Accommodations Report",
          candidateNameLabel: "Assessed Individual's Name:",
          candidateOlaPhoneNumberLabel: "Assessed Individual's Phone Number: ",
          referenceNumberLabel: "Reference Number:",
          testSkillLabel: "Test Type:",
          testNameLabel: "Assessment Name:",
          testTimeLabel: "Total Test Time:",
          breakBankLabel: "Break Bank Time:",
          testCenterLabel: "Test Centre:",
          requestingOrganizationLabel: "Department or Agency:",
          primaryContactLabel: "Primary Contact:",
          primaryContactPhoneNbrLabel: "Telephone Number:",
          primaryContactEmailLabel: "Email:",
          aaeExpertContactLabel: "Written By:",
          requestCompletedDate: "Date:"
        },
        canadaLogoAriaLabel: "Public Service Commission of Canada logo",
        accommodationsSection: {
          title: "Assessment Accommodation Recommendations",
          generalInfoSection: {
            paragraph1: "Note for the Assessed Individual",
            paragraph2:
              "Please note that the assessment specialist who has written this report remains available to discuss your accommodations if needed. Their contact information is on the last page of this report.",
            paragraph3:
              "Additional information regarding access to the platform and the platform itself will be sent once the test is scheduled."
          },
          otherMeasures: {
            title: "Other Measures"
          }
        },
        contactInfoSection: {
          title: "Contact Information",
          allInquiries: {
            description: "For all inquiries related to this report, please contact:",
            role: "Assessment Specialist",
            unit: "Assessment Accommodation Unit",
            ppcDescription: "Personnel Psychology Centre (PPC)",
            phoneNumber: "Telephone Number: {0}",
            email: "Email: {0}"
          },
          generalInquiries: {
            description: "For general inquiries, please contact:",
            unit: "Assessment Accommodation Unit",
            phoneNumber: "Telephone Number: (819) 420-8690",
            email: "Email: cfp.ae-aa.psc@cfp-psc.gc.ca"
          }
        }
      }
    },

    // Terms of Use
    termsOfUse: {
      general: {
        title: "General",
        para1:
          "This is an agreement between you and the Public Service Commission of Canada (PSC) for your participation in the SLE Oral Language Assessment.",
        para2:
          "To proceed with scheduling, you must agree to these Terms of Use without limitation or qualification. You must confirm your agreement by placing a checkmark in the box at the bottom of this agreement and clicking the “Next” button.",
        para3:
          "If the PSC has reason to doubt the validity of your test result or your compliance with these terms of use, you may need to be reassessed at a later date."
      },
      definitions: {
        title: "Definitions",
        para1Bold: "Test content:",
        para1:
          " the test questions, answers provided by candidates in response to test questions, and all related information including notes, descriptions or recordings."
      },
      acceptableUses: {
        title: "Acceptable Uses",
        para1: "Access and use of the test is provided solely to:",
        bullet1:
          "Allow you to demonstrate your ability with respect to the qualification being assessed, while complying with standardized testing conditions.",
        bullet2:
          "Allow you to respond to the test questions in good faith and to the best of your ability, while complying with the instructions that you will receive prior to taking the test."
      },
      prohibitedUses: {
        title: "Prohibited Uses",
        para1: "Apart from the Accepted Uses outlined above, all other uses are prohibited.",
        para2: "Prohibited uses include, but are not limited to, the following examples:",
        bullet1: "Recording the test using any means, including devices or software.",
        bullet2:
          "Reproduce or collect, transmit or broadcast any content or information from the test.",
        bullet3: "Retain test content, in any form, following completion of the test.",
        bullet4:
          "Complete the test with assistance from other people or other resources (Notes prepared before the test, books, dictionaries, glossaries in print or electronic format, websites, translation tools in print or electronic format, tools or software using artificial intelligence, social media, live captions, etc.).",
        bullet5:
          "Share and/or discuss in any medium any content or information from the test with others, which includes any notes taken during the test.",
        bullet6:
          "Misuse information to selectively advantage or disadvantage you or any other candidate or potential candidate in a federal public service appointment process.",
        bullet7: "Use the test content for commercial gain in any form.",
        bullet8:
          "Use the test to further any activity that may be unlawful, misleading or malicious.",
        bullet9: "Facilitate or encourage any violation of these Terms of Use."
      },
      security: {
        title: "Security",
        para1:
          "All test content is copyrighted material of the PSC and is designated “Protected B”.",
        para2:
          "The PSC has implemented security measures to ensure the protection and confidentiality of the information provided by you and to ensure that your electronic test session is secure. However, it is your obligation to respect the following:",
        bullet1:
          "Access to the test content is provided for your sole personal use (see the section “Acceptable Uses”).",
        bullet2: "Do not record the test, in whole or in part, in any form.",
        bullet3:
          "At all times during the test, you must be alone in a quiet place where you will not be disturbed and where your exchanges with the person administering the test cannot be overheard or recorded.",
        bullet4:
          "Take the test in an environment you feel comfortable being supervised over videoconference by the person administering the test. Ensure that the background visible on screen is as neutral as possible.",
        bullet5:
          "Immediately notify the person administering your test, or the Personnel Psychology Centre ({0}) of any breach or possible breach of security.",
        bullet5Link: "cfp.cpp-ppc.psc@cfp-psc.gc.ca"
      },
      intellectualPropertyRights: {
        title: "Intellectual Property Rights",
        para1: `The permission for non-commercial reproduction in the "Important Notices" found on publicly accessible pages of the PSC Web site does not apply to the test. It is replaced by the following:`,
        bullet1: "All content in the test is copyrighted material of the PSC.",
        bullet2: `You may access and use the test solely for the purposes set out in the "Acceptable Uses" section above.`,
        bullet3:
          "You may not copy, reproduce, translate, distribute or disseminate the content of the test, in whole or in part, in any form or by any means."
      },
      ensuringTheIntegrityOfTheTestingProcess: {
        title: "Ensuring the Integrity of the Testing Process",
        bullet1:
          "The PSC reserves the right, at its discretion, to take any actions it deems necessary to ensure that it renders test results that are reliable, valid and fair.",
        bullet2:
          "Such actions may be taken if there are irregularities observed with your test, or the PSC has reason to doubt whether these Terms of Use were respected and that standardized testing conditions were in place.",
        bullet3:
          "Possible actions include the early termination of your test session, the suspension or invalidation of your test result, and the imposition of conditions intended to increase the security of future assessments (e.g., requirement of in-person testing).",
        bullet4:
          "The PSC may also take actions in the context of an audit of its testing program. In this context, your test result may be invalidated or suspended pending a retest under controlled conditions with enhanced security (e.g., in-person testing).",
        para1:
          "The department who ordered your test may be notified of any such actions taken by the PSC with regard to your test. The department will be responsible for sharing the notification with you.",
        para2:
          "If you have questions or concerns about actions taken regarding your test, they may be raised with the PSC’s Personnel Psychology Centre at the following email address, {0}.",
        para2Link: "cfp.cpp-ppc.psc@cfp-psc.gc.ca"
      },
      consequencesOfABreachOfTermsOfUse: {
        title: "Consequences of a Breach of Terms of Use",
        bullet1:
          "The PSC reserves the right, at its sole discretion, to pursue any legal remedy/take action against you, including but not limited to the suspension of your test results, termination of your employment application, or claim for copyright infringement and breach of confidence, upon any breach by you of these Terms of Use.",
        bullet2:
          "Parties involved in the improper retention, disclosure or use of PSC test content, including the content of this test, may be the subject of an investigation under the Public Service Employment Act (PSEA), where a finding of fraud may be referred to the Royal Canadian Mounted Police (RCMP).",
        bullet3:
          "All test content is designated “Protected B”.  Any unauthorized reproduction, recording and/or disclosure of test content is in contravention of the {0} and may be the subject of an investigation under that policy.",
        bullet3LinkLabel: "Government Security Policy",
        bullet3Link: "https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=16578",
        bullet4:
          "Suspected cheating will in all cases be referred to appropriate authorities, such as the PSC’s Investigations branch or officials with the department who ordered your assessment. Consequently, it may be the subject of an investigation under the PSEA, where a finding of fraud may be referred to the RCMP."
      },
      checkboxLabel: "I have read and accept these Terms of Use."
    },

    privacyNoticeStatementOLA: {
      para1:
        "Personal information is used to provide assessment services to clients of the Personnel Psychology Centre. It is collected for staffing related purposes, in accordance with sections 11, 30 and 36 of the {0}. For organizations not subject to this act, personal information is collected under the authority of that organization's enabling statute and section 35 of the {1}. Second language evaluation results will be disclosed to authorized organizational officials. Results of all other tests will be disclosed only to the requesting organization. Test results may be disclosed to the Investigations Directorate of the Public Service Commission if an investigation is conducted pursuant to section 66 or 69 of the {2}. Your information may also be used for statistical and analytical research purposes. In some cases, information may be disclosed without your consent, pursuant to section 8(2) of the {3}. Provision of your personal information is voluntary, but if you choose not to provide your information, you may not be able to receive Personnel Psychology Centre services.",
      para1Link1Label: "Public Service Employment Act",
      para1Link1: "https://www.laws-lois.justice.gc.ca/eng/acts/p-33.01/index.html",
      para1Link2Label: "Privacy Act",
      para1Link2: "https://laws-lois.justice.gc.ca/eng/acts/p-21/FullText.html",
      para2:
        "The information is collected and used as described in the Personnel Psychology Centre's (PSC PCU 025) PIB, found in the Public Service Commission's {0}.",
      para2LinkLabel: "Info Source",
      para2Link:
        "https://www.canada.ca/en/public-service-commission/corporate/about-us/access-information-privacy-office/info-source-sources-federal-government-employee-information.html",
      para3:
        "You have the right to access and correct your personal information, and to request corrections where you believe there is an error or omission. You also have the right to file a complaint to the {0} regarding the handling of your personal information.",
      para3LinkLabel: "Privacy Commissioner of Canada",
      para3Link: "https://www.priv.gc.ca/en/",
      recording: {
        title: "Recording",
        para1:
          "Your test will be recorded by the PSC for administrative purposes. The content of the recording is protected under the Privacy Act and as also required by this Act, the recording itself will be kept for 2 years. No other recording of your assessment is permitted."
      },
      checkboxLabel: "I have read this privacy notice statement."
    },

    // Dashboard Page
    dashboard: {
      title: "Welcome, {0} {1}.",
      description:
        "To start your test, select the Check-in button below, and enter your test access code.",
      lastLogin: "[Last Logged In: {0}]",
      lastLoginNever: "never",
      sideNavItems: {
        takeATest: "Take a Test",
        reserveSeat: {
          title: "Reservations",
          text: "Use the reservation code provided in your invitation email to look for available supervised test sessions.",
          makeReservation: {
            title: "Make a Reservation",
            reservationCode: "Reservation Code:",
            reservationCodeHelp: "Type your Reservation Code that starts with the letters RES.",
            invalidReservationCode:
              "This code is invalid or expired. If you are trying to take a test, select 'take a test” from the menu on the left then click on the “Check-in” button.",
            reservationCodeAlreadyUsed: "The reservation code has already been used",
            searchBar: "Reservation Code",
            button: "Claim Code",
            tablePrefixParagraph: "{0} available test session(s) found for reservation code {1}",
            table: {
              columnOne: "Test",
              columnTwo: "Test Session Date",
              columnThree: "Test Centre",
              columnThreeOla: "Test Session Time",
              columnFour: "Actions",
              infoButton: "Session Information",
              infoButtonAccessibilityLabel: "Enter your reservation code",
              noDataMessage: "There are currently no available test sessions"
            },
            reservedTablePrefixParagraph: "My Reservation Codes:",
            reservedTable: {
              columnOne: "Test",
              columnTwo: "Reservation Code",
              columnThree: "Actions",
              findSessions: "Find test sessions",
              findSessionsAccessibilityLabel: "Todo Find Sessions help text",
              searchResultsButton: "Search for Existing Results",
              searchResultsAccessibilityLabel: "Todo Search for Existing Results help text",
              requestAccommodationsAccessibilityLabel: "Request an accommodation for {0} test",
              requestAccommodationsButton: "Request an accommodation",
              accessibilityRequestButton: "Open Accessibility Request",
              accessibilityRequestAccessibilityLabel: "Todo Open Accessibility Request help text",
              withdrawButton: "Withdraw from Process",
              withdrawAccessibilityLabel: "Todo Withdraw from Process help text",
              noDataMessage:
                "No Reservation Codes found for your account; please enter a new reservation code above"
            },
            sessionPopup: {
              title: "Test Session Information",
              paragraph: 'Click "Reserve" to confirm your attendence for this testing session.',
              bookItButton: "Reserve"
            },
            withdrawnSuccessPopup: {
              title: "Successfully Withdrawn",
              paragraph:
                "You have successfully withdrawn from this process, no further action is necessary at this time."
            },
            bookingSuccessPopup: {
              title: "Test Session Successfully Booked",
              paragraph:
                "You have successfully made your reservation. Your booked test sessions can be seen under the “Manage Reservation” tab."
            },
            confirmWithdrawPopup: {
              title: "Confirm Your Withdrawal",
              paragraph: `Are you sure you want to withdraw from this assessment process?
              If you proceed, you will not be able to take this test and the organization who invited you to take this test will be notified of your decision to withdraw from this assessment process.`
            },
            noSessionsAvailablePopup: {
              title: "No Sessions Available",
              paragraph:
                "There are no sessions available for this reservation code before the closing date."
            },
            notEnoughSeatsPopup: {
              title: "Test Session not available",
              paragraph:
                "This test session is currently fully booked; please select another test session."
            },
            notEnoughTimePopup: {
              title: "Test Session not available",
              paragraph: "This test session cannot be booked after {0}."
            }
          },
          reservationDetailsPopup: {
            description: "Here are the details of your test session:",
            testSessionDateLabel: "Test Session Date:",
            testSessionStartTimeLabel: "Start Time:",
            testSessionEndTimeLabel: "End Time:",
            testSkillLabel: "Test Type:",
            additionalDetailsLabel: "Additional Details:",
            testCenterAdditionalInfoLabel: "Test Centre Information:",
            roomAdditionalInfo: "Room information:",
            departmentLabel: "Organization:",
            locationLabel: "Location:",
            phoneNumber: "Phone Number:",
            termsOfUseCheckboxLabel: "I confirm that I have read and accept the {0}",
            termsOfUseCheckboxLink: "Terms of Use",
            privacyNoticeStatementCheckboxLabel: "I confirm that I have read the {0}",
            privacyNoticeStatementCheckboxLink: "Privacy Notice Statement",
            termsOfUsePopup: {
              title: "Terms of Use"
            },
            privacyNoticeStatementPopup: {
              title: "Privacy Notice Statement"
            },
            editPhoneNumberPopop: {
              title: "Edit Phone Number",
              phoneNumber: "Phone Number"
            }
          },
          manageReservation: {
            title: "Manage Reservations",
            table: {
              columnOne: "Test",
              columnTwo: "Test Session Date",
              columnThree: "Session Locale",
              columnFour: "Actions",
              infoButton: "View test session information",
              editButton: "Edit reservation",
              deleteButton: "Withdraw from the assessment process",
              noDataMessage: "No current reservations"
            },
            reservationPopup: {
              titleView: "Session Information",
              titleChange: "Edit Reservation",
              titleWithdraw: "Withdraw from Process",
              joinMeetingButton: "Join Meeting",
              changeItButton: "Change",
              paragraphLater:
                'Click "Book later" to select a different test session at a later time.',
              bookLaterButton: "Book later",
              paragraphWithdraw: `Click “Withdraw” to cancel your reservation and withdraw from the assessment process.

                If you proceed, you will not be able to take this test and the organization who invited you to take this test will be notified of your decision to withdraw from this assessment process.`,
              withdrawButton: "Withdraw"
            },
            bookingActionSuccessfulPopup: {
              title: "Reservation successfully cancelled",
              paragraphLater:
                "You have successfully cancelled your reservation. You can make a new reservation by reusing the reservation code in the “Make a Reservation” tab.",
              paragraphChange: "You have successfully changed your booking"
            },
            withdrawSuccessfulPopup: {
              title: "Successfully Withdrawn",
              paragraph:
                "You have successfully withdrawn from this process, no further action is necessary at this time."
            },
            actionViolatesPopup: {
              title: "The reservation cannot be modified",
              paragraph: "The reservation for this test session cannot be changed after {0}."
            }
          }
        },
        incidentReport: "Incident Reports",
        testResults: "My Test Results"
      },
      table: {
        columnOne: "Test",
        columnTwo: "Actions",
        requestAccommodations: "Request an accommodation",
        requestAccommodationsAccessibilityLabel: "Request an accommodation for the {0}",
        joinMeetingButton: "Join virtual teams meeting",
        joinMeetingButtonLabel: "Join virtual teams meeting for {0}",
        startTest: "Start",
        resumeTest: "Resume",
        startTestAccessibilityLabel: "Start the {0} test.",
        resumeTestAccessibilityLabel: "Resume the {0} test.",
        noTests: "You have no assigned tests"
      },
      requestAccommodationsPopup: {
        title: "Request an accommodation",
        alternativeTitle: "Request an accommodation",
        successTitle: "Request Sent",
        needAccommodationsProfileSystemMessageDescription:
          "An accommodations profile is mandatory in order to send that request. Go to the {0} page to complete this information before proceeding.",
        incompletedAccommodationsProfileLink: "My Profile",
        description:
          "By submitting this request, you are requesting that assessment accommodations be added to your test experience for the {0}. Please complete the section below. An assessment specialist will contact you.",
        successSystemMessageDescription:
          "This request has been sent successfully. Please do not schedule a testing session before receiving your accommodation report.",
        requestTypeLabel: "Request Type:",
        requestTypeOptions: {
          accommodations: "Accommodations",
          alternateTest: "Alternate Test (for previous test administrator)",
          both: "Both"
        },
        testCenterLabel: "Location:",
        phoneNumberLabel: "Phone Number:",
        commentLabel: "Comments (optional):",
        testAdministeredDetailsLabel: "Reason for request:",
        checkboxDescription:
          "I confirm that my {0} is up to date and correctly represents my situation.",
        checkboxDescriptionLink: "Accommodations Profile",
        rightButton: "Submit request"
      },
      ongoingAccommodationRequestProcessPopup: {
        title: "Start Test",
        systemMessageDescription:
          "Your accommodation request has not been completely processed or applied yet. Are you sure you want to start your test anyway?",
        checkboxDescription: "Yes, I'm sure I want to continue without any accommodations."
      }
    },

    // Checkin action
    candidateCheckIn: {
      button: "Check-in",
      loading: "Searching for active tests...",
      page: {
        title: "Enter the Test Access Code",
        incompletedProfileTitle: "Update Your Profile Information",
        description:
          'Please type the Test Access Code provided by the test administrator in the box below. Then select "Check-in" to access your test.',
        textLabel: "Test Access Code:",
        takeTestTooltip: "Type your Test Access Code",
        textLabelError:
          "Must be a valid Test Access Code. If your code starts with “RES-”, you have been invited to schedule an in-person test. Please select “Reservation” on the left menu.",
        testAccessCodeAlreadyUsedError: "You have already used this test access code",
        currentTestAlreadyInActiveStateError: "You have already checked in for this test",
        incompletedProfileSystemMessage:
          "You must complete or review your profile before you can check-in a test. Additional information is required for statistical purposes.",
        incompletedProfileDescription:
          "Go to the {0} page to complete this information before proceeding.",
        incompletedProfileLink: "My Profile",
        incompletedProfileRightButton: "Update Profile"
      }
    },

    // Lock Screen
    candidateLockScreen: {
      tabTitle: "Test Locked",
      description: {
        part1: "Your test has been locked by the test administrator.",
        part2: "Your test timer is on hold.",
        part3: "Your responses have been saved.",
        part4: "Please contact your test administrator for further instructions."
      }
    },

    // Pause Screen
    candidatePauseScreen: {
      tabTitle: "Test Paused",
      description: {
        part1:
          "You have paused your test. It will resume when you select Resume Test or once the timer reaches 00:00:00.",
        part2: "Your test timer is on pause.",
        part3: "Your responses have been saved."
      }
    },

    // Invalidate Test Screen
    candidateInvalidateTestScreen: {
      tabTitle: "Test Unassigned/Invalidated",
      description: {
        part1: "Your test has either been unassigned or invalidated.",
        part2: "Please contact the test administrator for further instructions.",
        returnToDashboard: "Return to Home Page"
      }
    },

    // My Test
    myTests: {
      title: "Welcome, {0} {1}.",
      sideNavTitle: "My Test Results",
      alertCatTestsOnly1:
        "The results of all tests that you have taken using the Candidate Assessment Tool will be available on this page 48 hours after the test expiration date (the ‘deadline’ in your invitation email). These results are valid when certain conditions, such as respecting the retest period, are met.",
      alertCatTestsOnly2:
        "Please note: The valid result is always the last result obtained when the test conditions have been met, regardless of the format (online or paper) and the method of administration (in person or remotely).",
      topTabs: {
        validTests: "Current valid test results",
        archivedTests: "Archived test results"
      },
      table: {
        nameOfTest: "Test",
        testDate: "Test Date",
        score: "Raw Score",
        result: "Result",
        expiration: "Expiration",
        reTest: "Retest Date",
        actions: "Action",
        viewResultsButton: "View",
        viewResultsButtonAriaLabel:
          "View results of the {0} test, completed on {1}, where a retest is possible as of {2} and the score is {3}.",
        viewResultsPopup: {
          archivedWatermark: "ARCHIVED",
          title: "Test Result",
          testLabel: "Test:",
          testDescriptionLabel: "Test Description:",
          // languageLabel: "Test Language:",
          testDateLabel: "Test Date:",
          testStatus: "Test Status:",
          scoreLabel: "Raw Score:",
          resultLabel: "Result:",
          retestDateLabel: "Date after which a retest is possible:",
          retestPeriodLabel: "Mininum number of days before a retest:",
          retestPeriodUnits: "days",
          scoreValidity: "Result Validity Period (months):",
          scoreValidityUnits: "month(s)",
          expiration: "Expiration:"
        },
        noDataMessage: "No tests have been scored yet."
      }
    },

    // Test Builder Page
    testBuilder: {
      backToTestDefinitionSelection: "Back to the list of Test Definitions",
      backToTestDefinitionSelectionPopup: {
        title: "Return to Test Definition Selection?",
        description: "Are you sure you want to continue?",
        systemMessageDescription: "You will lose everything!"
      },
      containerLabel: "Test Definition",
      itemBanksContainerLabel: "Item Banks",
      goBackButton: "Back to Test Builder",
      inTestView: "Preview",
      errors: {
        nondescriptError:
          "Non descript error. Report this to IT. Maybe a user is logged into the adminconsole.",
        cheatingPopup: {
          title: "Caught Cheating Attempt",
          description:
            "You are not allowed to switch or open new tabs. Any attempt to minimize the screen or change focus will be considered a cheating attempt.",
          warning:
            "After {0} more detections your test will be terminated and your results invalidated. If this was an error, alert your TA."
        },
        backendAndDbDownPopup: {
          title: "Connection Issue!",
          description:
            "The connection to your test has been interrupted. To continue the test, make sure you are connected to the Internet, then try to reconnect to your CAT account.",
          rightButton: "Return to Home page"
        }
      },
      testDefinitionSelectPlaceholder: "Select a Test Section",
      sideNavItems: {
        testDefinition: "Test Information",
        testSections: "Test Sections",
        testSectionComponents: "Test Sub-Sections",
        sectionComponentPages: "Section Titles",
        componentPageSections: "Section Content",
        questionBlockTypes: "Item Block Names",
        questions: "Items",
        scoring: "Scoring Method",
        answers: "Answers",
        addressBook: "Address Book Contacts",
        testManagement: "Test Activation"
      },
      // some values are underscored because python uses underscores
      testDefinition: {
        title: "Test Definition and Version",
        description: "These are the top level properties of a test.",
        validationErrors: {
          invalidTestDefinitionDataError:
            "Error: All Test Information fields are mandatory. Please fill out the missing fields and try again."
        },
        uploadFailedErrorMessage: "Upload Error: Review the highlighted sections",
        uploadFailedComplementaryErrorMessages: "Upload Error. Please review {0}",
        uploadJsonFailedComplementaryErrorMessages: "JSON Upload Error: Review JSON content",
        uploadTestDefinition: {
          title1: "Upload Test Version to server",
          title2: "Test Version Uploaded Successfully",
          description1:
            "Uploading this Test Definition will create a new CAT version. Are you sure you want to proceed?",
          description2:
            "The Test Version has been uploaded successfully. However, please note that this new version is currently deactivated. To activate this new test version, update the Test Status under the Test Activation section."
        },
        uploadFailedPopup: {
          title: "Upload Failed",
          systemMessage:
            "All Test Sections with a Scoring Method set to AUTO SCORE must contain at least one Test Sub-Section with a Sub-Section Type set at ITEMS_(TEST_BUILDER)."
        },
        undefinedVersion: "0",
        selectTestDefinition: {
          sideNavigationItems: {
            allTests: "All Test Definitions",
            AllActiveTestVersions: "Active Test Definitions",
            archivedTestCodes: "Archived Test Definitions",
            uploadJson: "Upload JSON"
          },
          newTestDefinitionButton: "New Test Definition",
          allTests: {
            title: "All Test Definitions",
            description: "Create, modify and upload Test Definitions.",
            table: {
              column1: "Parent Code",
              column2: "Test Code",
              column3: "Test Name",
              column4: "Actions",
              noData: "There is no test definition data",
              selectSpecificTestVersionTooltip: "Select Test Definition Version",
              selectSpecificTestVersionLabel:
                "Select a specific version of this test definition where the parent code is: {0}, the test code is: {1} and the test name is: {2}",
              selectLatestTestVersionTooltip: "View Latest Test Definition Version",
              selectLatestTestVersionLabel:
                "View latest test version of this test definition where the parent code is: {0}, the test code is: {1} and the test name is: {2}",
              archiveTestCodeTooltip: "Archive Test Definition",
              archiveTestCodeLabel:
                "Archive test code of the this test definition where the parent code is: {0}, the test code is: {1} and the test name is: {2}"
            },
            testDefinitionPopup: {
              title: "Select Test Definition Version",
              parentCode: "Parent Code:",
              testCode: "Test Code:",
              versionNotes: "Version Notes:",
              uitStatus: "UIT:",
              testStatus: "Test Status:",
              sampleTestStatus: "Sample Test:",
              name: "Test Name:",
              rightButton: "Select Test Definition",
              dropdown: {
                title: "CAT Version:",
                titleTooltip: "To be completed"
              }
            },
            archiveTestCodePopup: {
              title: "Archive Test Definition",
              systemMessageDescription: "All test accesses related to {0} will be revoked.",
              description: "Are you sure you want to continue?",
              rightButton: "Archive Test Definition"
            }
          },
          allActiveTestVersions: {
            title: "Active Test Definitions",
            description: "View and deactivate active Test Versions",
            table: {
              column1: "Parent Code",
              column2: "Test Code",
              column3: "Test Name",
              column4: "CAT Version",
              column5: "Actions",
              noData: "There is no test definition data",
              viewButtonTooltip: "View Test Definition",
              viewButtonAriaLabel:
                "View details of the following test definition where the parent code is: {0}, the test code is: {1}, the test name is: {2} and the version is: {3}",
              deactivateButtonTooltip: "Deactivate Test Definition",
              deactivateButtonAriaLabel:
                "Deactivate test definition version where the parent code is: {0}, the test code is: {1}, the test name is: {2} and the version is: {3}"
            },
            popup: {
              title: "Deactivate Test Definition",
              description: "All test accesses related to {0} will be revoked.",
              rightButton: "Deactivate Test Definition"
            }
          },
          archivedTestCodes: {
            title: "Archived Test Definitions",
            description: "Restore deactivated Test Definitions.",
            table: {
              column1: "Parent Code",
              column2: "Test Code",
              column3: "Test Name",
              column4: "Actions",
              noData: "There is no test definition data",
              restoreButton: "Restore Test Definition",
              restoreButtonAriaLabel:
                "Restore test definition test code where the parent code is: {0}, the test code is: {1} and the test name is: {2}"
            },
            popup: {
              title: "Restore Test Definition",
              description: "Test Definition {0} will be restored",
              rightButton: "Restore Test Definition"
            }
          },
          uploadJson: {
            title: "Upload JSON",
            description: "Upload JSON files to the server.",
            textAreaLabel: "Please select the JSON file:",
            uploadJsonButton: "Upload JSON",
            invalidJsonError: "Upload Error. Please review the JSON file.",
            uploadJsonSuccessful: "JSON File Uploaded Successfully: {0} {1} v{2}",
            fileSelectedAccessibility: "The selected file is {0}",
            browseButton: "Browse...",
            wrongFileTypeError: "Must be a valid JSON file."
          },
          itemBanks: {
            title: "Item Banks",
            table: {
              column1: "Item Bank ID",
              column2: "Item Bank Name",
              column3: "Actions",
              viewButtonTooltip: "View",
              viewButtonAriaLabel: "View item bank {0}",
              downloadReportButtonTooltip: "Download Report",
              noData: "There are no results found based on your search"
            }
          },
          itemBankEditor: {
            backToItemBankSelectionButton: "Back to List of Item Banks",
            backToItemBankSelectionPopup: {
              title: "Return to Item Bank Selection?",
              systemMessageDescription: "All unsaved data will be lost!",
              description: "Are you sure you want to proceed?"
            },
            sideNavigationItems: {
              itemBankDefinition: "Item Bank Definition",
              itemAttributes: "Custom Variables",
              items: "Items ({0})",
              bundles: "Grouping ({0})"
            },
            itemBankDefinition: {
              custom_item_bank_id: {
                title: "Item Bank ID:",
                errorMessage: "Must be a valid Item Bank ID"
              },
              custom_item_bank_id_already_exists: {
                title: "Item Bank ID:",
                errorMessage: "This Item Bank ID already exists"
              },
              en_name: {
                title: "Item Bank Name (English):",
                errorMessage: "Must be a valid name"
              },
              fr_name: {
                title: "Item Bank Name (French):",
                errorMessage: "Must be a valid name"
              },
              updateExistingItemBankPopup: {
                title: "Update Existing Item Bank Definition",
                systemMessageDescription:
                  "You are about to update an existing item bank definition!",
                description: "Are you sure you want to proceed?"
              }
            },
            itemAttributes: {
              createNewButton: "Variable",
              collapseAll: "Collapse All Variables",
              uploadDataButton: "Upload Data",
              uploadDataErrorMessage:
                "Error found in item attributes. Please make sure that all parameters are defined.",
              itemAttributeCollapsingItem: {
                title: "Custom Variable {0}: ",
                values: "Values: ",
                en_name: {
                  title: "Variable Name in English:",
                  errorMessage: "Must be a valid name"
                },
                fr_name: {
                  title: "Variable Name in French:",
                  errorMessage: "Must be a valid name"
                },
                attributeTitle: "Variable Name:",
                attributeValuesTitle: "Values:",
                createNewAttributeValueButton: "Add Value",
                attributeTranslationsButtonLabel: "Modify Translations",
                attributeValueCollapsingItem: {
                  title: "Attribute Value {0}: {1}",
                  value_identifier: {
                    title: "Value Identifier",
                    errorMessage: "Must be a valid value identifier"
                  }
                },
                attributeNameTranslationsPopup: {
                  title: "Translations - Variable Name"
                }
              },
              dataUploadedSuccessfullyPopup: {
                title: "Item Bank Attributes Data Uploaded Successfully",
                systemMessage:
                  "The item bank attributes data has been uploaded successfully in the database."
              }
            },
            items: {
              itemTitle: "{0} {1} {2} - System ID: {3} ",
              createNewItemButton: "New Item",
              createItemPopup: {
                title: "Create Item",
                description: "Please provide the required information to create a new item.",
                responseFormatLabel: "Response Format:",
                itemTypeLabel: "Item Format:",
                historicalIdLabel: "R&D Item ID:",
                historicalIdAlreadyBeenUsedError:
                  "This R&D Item ID is already used in this item bank.",
                rightButton: "Create Item"
              },
              table: {
                itemId: "System Item ID",
                itemType: "Item Status",
                version: "Version",
                actions: "Actions",
                viewAction: "Preview",
                viewActionAccessibilityLabel: "View Item {0}",
                editAction: "Edit",
                editActionAccessibilityLabel: "Edit Item ID {0}",
                noData: "There are no items in this bank.",
                rndItemId: "R&D Item ID",
                itemTypeLabel: "Item Format",
                itemTypeQuestion: "Question",
                itemTypeInstruction: "Instruction"
              },
              viewSelectedItemPopup: {
                title: "Item Preview: {0}",
                questionLabel: "Question"
              },
              backToItemBankEditorButton: "Back to Item Bank",
              sideNavigationItems: {
                general: "General",
                properties: "Properties",
                statistics: "Statistics",
                socialRelationships: "Social Relationships",
                history: "History",
                versionComparison: "Version Comparision",
                uploadData: "Upload Data"
              },
              topTabs: {
                generalTabs: {
                  generaL: "General"
                },
                contentTabs: {
                  content: "Content",
                  screenReader: "Screen Reader"
                },
                otherTabs: {
                  categories: "Custom Variables",
                  comments: "Comments",
                  translations: "Translations"
                }
              },
              general: {
                systemIdLabel: "System Item ID:",
                historicalIdLabel: "R&D Item ID:",
                editHistoricalIdTooltip: "Edit",
                addEditHistoricalIdPopup: {
                  title: "R&D Item ID",
                  description: "Please provide the required information.",
                  historicalIdLabel: "R&D Item ID:"
                },
                developmentStatusLabel: "Item Status:",
                versionLabel: "Version:",
                deleteDraftTooltip: "Delete Item Draft",
                deleteDraftConfirmationPopup: {
                  title: "Delete Item Draft?",
                  systemMessageDescription: "This draft will be deleted!",
                  description: "Are you sure you want to continue?"
                },
                draftVersionLabel: "<Draft>",
                responseFormatLabel: "Response Format:",
                scoringFormatLabel: "Scoring Method:",
                shuffleOptionsLabel: "Randomize Answer Choices:",
                activeStatusLabel: "Active:",
                historicalIdAlreadyExistsError:
                  "This R&D Item ID is already used in this item bank."
              },
              content: {
                languageDropdownLabel: "Language:",
                addStemButton: "Stem",
                stemSections: "Stem(s):",
                addInstructionButton: "Instruction",
                instructionSections: "Instruction(s):",
                addOptionButton: "Answer Choice",
                options: "Answer Choices:",
                score: "Scoring Value:",
                text: "Text:",
                excludeFromShuffle: "Exclude From Randomization:",
                detailsButton: "Details",
                detailsPopup: {
                  title: "Additional Answer Choice Information",
                  textLabel: "Text:",
                  historicalOptionIdLabel: "R&D Answer Choice ID:",
                  rationaleLabel: "Rationale:",
                  excludeFromShuffleLabel: "Exclude From Randomization:",
                  scoreLabel: "Scoring Value: "
                },
                addAlternativeContentPopup: {
                  title: "Add Alternative Item Content",
                  description: "Please provide the required information.",
                  alternativeContentTypeLabel: "Alternative Content Type:",
                  alternativeContentTypeOptions: {
                    screenReader: "Screen Reader"
                  },
                  rightButton: "Save"
                },
                deleteAlternativeContentPopup: {
                  title: "Delete Alternative Item Content Tab?",
                  systemMessageDescription: "The alternative item content tab will be deleted.",
                  description: "Are you sure you want to delete this tab?"
                }
              },
              comments: {
                addCommentButton: "Comment",
                addCommentPopup: {
                  title: "Add Comment",
                  commentLabel: "Comment:",
                  addButton: "Save"
                },
                deleteCommentPopup: {
                  title: "Delete Comment",
                  systemMessageDescription1: "Are you sure you want to delete this comment?",
                  systemMessageDescription2: "- {0}"
                }
              },
              previewItemPopup: {
                title: "Item Preview: {0}"
              },
              uploadConfirmationPopup: {
                title: "Upload Item",
                titleSuccessfully: "Uploaded Successfully!",
                systemMessageDescription:
                  "Uploading this item will update the content from the following fields from the previous version:",
                systemMessageDescriptionSuccessfully:
                  "Item version {0} has been uploaded successfully.",
                systemMessageDescriptionSuccessfullyOverwrite:
                  "Item version {0} has been uploaded successfully.",
                itemModificationsTable: {
                  itemElements: {
                    stemSections: "Stem(s)",
                    options: "Answer Choices",
                    categories: "Custom Variables",
                    historicalId: "R&D Item ID",
                    itemStatus: "Item Status",
                    responseFormat: "Response Format",
                    scoringFormat: "Scoring Method",
                    shuffleOptions: "Randomize Answer Choices",
                    instructions: "Instruction(s)"
                  },
                  column1: "Fields",
                  column2: "Update",
                  column3: "Warnings",
                  warnings: {
                    stemsNotDefined: "Stem(s) not defined.",
                    containsBlankStems: "Contains blank stems(s).",
                    optionsNotDefined: "Answer choice(s) not defined.",
                    containsBlankOptions: "Contains blank answer choice(s).",
                    instructionsNotDefinded: "Instruction(s) not defined",
                    containsBlankInstructions: "Contains blank instruction(s)."
                  }
                },
                whatDoYouWantToDo: "What do you want to do?",
                createNewVersionRadio: "Create a new version number for this item",
                overwriteCurrentVersionRadio: "Keep the current version number for the item",
                reviewedWarningsCheckboxLabel: "I have reviewed and accept the warnings."
              },
              uploadData: {
                title: "Upload Data: {0} ({1})",
                uploadLabel: "Upload Item:",
                uploadButton: "Upload",
                uploadPopup: {
                  title: "Upload Item",
                  description: "Please insert your comment if needed.",
                  comment: "Comment (optional):",
                  uploadButton: "Upload",
                  uploadSuccessful: "Item version {0} has been uploaded successfully."
                },
                uploadFailedErrorMessage: "Upload Error: Review the highlighted sections"
              },
              backToItemBankEditorPopup: {
                title: "Return to Item Bank Editor?",
                systemMessageDescription: "All unsaved data will be lost!",
                description: "Are you sure you want to proceed?"
              },
              changeItemConfirmationPopup: {
                goPreviousTitle: "Navigate to previous item?",
                goNextTitle: "Navigate to next item?",
                changeItemTitle: "Navigate to another item?",
                systemMessageDescription: "All unsaved data will be lost!",
                description: "Are you sure you want to proceed?"
              }
            },
            bundles: {
              bundleTitle: "{0}",
              createNewBundleButton: "New Grouping",
              createBundlePopup: {
                title: "Create Grouping",
                description: "Please provide the required information to create a new grouping.",
                bundleNameLabel: "Grouping Name:",
                bundleNameAlreadyBeenUsedError:
                  "This Grouping Name is already used in this item bank.",
                bundleVersionLabel: "Grouping Version:",
                rightButton: "Create Grouping"
              },
              backToItemBankEditorButton: "Back to Item Bank",
              table: {
                bundleName: "Grouping Name",
                bundleVersion: "Grouping Version",
                itemCount: "Number of Items",
                bundleCount: "Number of Groupings",
                status: "Status",
                actions: "Actions",
                editAction: "Edit",
                editActionAccessibilityLabel: "Edit Grouping {0}",
                duplicateAction: "Duplicate",
                duplicateActionAccessibilityLabel: "Duplicate Grouping {0}",
                noData: "There are no groupings in this bank"
              },
              topTabs: {
                generalTabs: {
                  general: "General"
                },
                addTabs: {
                  addItems: "Add Items",
                  addBundles: "Add Groupings"
                },
                contentTabs: {
                  itemContent: "Item Content ({0})",
                  bundleContent: "Grouping Content ({0})"
                }
              },
              previewBundlePopup: {
                title: "Grouping Preview",
                systemMessageErrorBundleStructuralConflictError:
                  "The Grouping being uploaded contains a Grouping and a Sub-Grouping. Please remove the conflicting Sub-Grouping and try again.",
                systemMessageErrorDuplicateSystemIdsFoundInBundleAssociationsError:
                  "The Grouping is being previewed contains duplicate items. Please remove duplicate items and try again.",
                close: "Close",
                contentTypeDropdown: {
                  standard: "Standard",
                  screenReader: "Screen Reader"
                }
              },
              changeBundleConfirmationPopup: {
                goPreviousTitle: "Navigate to previous grouping?",
                goNextTitle: "Navigate to next grouping?",
                changeBundleTitle: "Change Grouping?",
                systemMessageDescription: "All unsaved data will be lost!",
                description: "Are you sure you want to proceed?"
              },
              uploadSuccessfulPopup: {
                title: "Uploaded Successfully!",
                titleWithError: "Grouping cannot be uploaded",
                systemMessageDescription: "Changes have been uploaded successfully.",
                systemMessageErrorBundleNameAlreadyUsed:
                  "This Grouping Name is already used in this item bank.",
                systemMessageErrorBundleStructuralConflictError:
                  "The Grouping being uploaded contains a Grouping and a Sub-Grouping. Please remove the conflicting Sub-Grouping and try again.",
                systemMessageErrorDuplicateSystemIdsFoundInBundleAssociationsError:
                  "The Grouping being uploaded contains duplicate items. Please remove duplicate items and try again."
              },
              general: {
                bundleNameLabel: "Grouping Name:",
                bundleVersionLabel: "Grouping Version:",
                shuffleItemsLabel: "Randomize Items:",
                shuffleBundlesLabel: "Randomize Groupings:",
                shuffleBetweenBundlesLabel: "Randomize Between Groupings:",
                activeLabel: "Active:",
                editBundleNameTooltip: "Edit",
                editBundleNamePopup: {
                  title: "Edit Grouping Name",
                  description: "Please provide the required information.",
                  bundleNameLabel: "Grouping Name:",
                  bundleNameMustBeValid: "Grouping Name must be provided."
                }
              },
              addItems: {
                table: {
                  systemId: "System Item ID",
                  rndItemId: "R&D Item ID",
                  responseFormat: "Response Format",
                  itemStatus: "Item Status",
                  version: "Version",
                  noData: "There are no results found based on your search"
                },
                searchDropdowns: {
                  developmentStatusLabel: "Item Status:",
                  activeStateLabel: "Active:"
                },
                addItemsToBundleButton: "Add to Grouping"
              },
              itemContent: {
                basicRuleLabel: "Number of items to select:",
                basicRuleErrorLabel: "Must be a valid number"
              },
              addBundles: {
                table: {
                  bundleName: "Grouping Name",
                  bundleVersion: "Version",
                  itemCount: "Item Count",
                  bundleCount: "Grouping Count",
                  status: "Status",
                  noData: "There are no results found based on your search"
                },
                addBundlesToBundleButton: "Add to Grouping"
              },
              bundleContent: {
                createNewBundleRuleButton: "New Grouping Rule",
                bundleRuleCollapsingItemContainer: {
                  title: "Rule {0}",
                  numberOfBundlesToPull: "Number of groupings to be selected:",
                  numberOfBundlesToPullErrorMessage: "Must be a valid number",
                  shuffleBundles: "Randomize Groupings:",
                  keepItemsTogether: "Keep items together:"
                },
                bundleAssociationsSystemMessage:
                  "All groupings that are not part of a rule will be removed upon uploading."
              }
            },
            deleteConfirmationPopup: {
              title: "Delete Confirmation",
              description:
                "Deleting this item will delete all the content displayed and all child objects that are related to this object. Are you sure you want to delete this object?"
            }
          }
        },
        test_code: {
          title: "Full Test Code",
          titleTooltip: "Unique Identifier for a test definition",
          errorMessage: "Must be a valid Full Test Code"
        },
        parent_code: {
          title: "Parent Code",
          titleTooltip: "The test code digits that group all version of this test.",
          errorMessage: "Must be a valid Test Number"
        },
        version: {
          title: "CAT Version",
          titleTooltip: "The latest version of the test definition",
          errorMessage: "Not a valid Version Number"
        },
        test_skill_type_id: {
          title: "Test Type",
          titleTooltip: "Test type of the test definition (example: SLE)",
          errorMessage: "Please select a Test Type",
          selectPlaceholder: "Please Select"
        },
        test_skill_sub_type_id: {
          title: "Test",
          titleTooltip: "(example: English - Written Expression)",
          errorMessage: "Please select a Test",
          selectPlaceholder: "Please Select"
        },
        retest_period: {
          title: "Retest Period",
          titleTooltip: "Retest period in number of days",
          errorMessage: "Not a valid number."
        },
        version_notes: {
          title: "Test Version Notes",
          titleTooltip: "Some wording here..."
        },
        fr_name: {
          title: "Test Name (French)",
          titleTooltip: "Name display to candidates when they have an active test",
          errorMessage: "Not a valid Name"
        },
        en_name: {
          title: "Test Name (English)",
          titleTooltip: "Name display to candidates when they have an active test",
          errorMessage: "Not a valid Name"
        },
        show_score: {
          title: "Show Raw Score",
          titleTooltip: `Raw score will be shown under the candidate's "My Test Results" page`
        },
        show_result: {
          title: "Show Test Result",
          titleTooltip: `Test Result will be shown under the candidate's "My Test Results" page`
        },
        is_uit: {
          title: "Unsupervised Test",
          titleTooltip: "Is this a unsupervised internet test?"
        },
        is_public: {
          title: "Sample Test",
          titleTooltip: "Is this a sample test?"
        },
        count_up: {
          title: "Timer Counts Up",
          titleTooltip: "Timed section(s) will count up instead of counting down - TO BE COMPLETED"
        },
        active_status: {
          title: "Test Status",
          titleTooltip: "Is this test active and available to use?"
        },
        test_language: {
          title: "Test Language",
          titleTooltip: "Is this test presented in a single language?",
          errorMessage: "not a valid choice",
          selectPlaceholder: "--"
        },
        constants: {
          testSectionScoringTypeObject: {
            not_scorable: "NOT SCORABLE",
            auto_score: "AUTO SCORE",
            competency: "COMPENTENCY SCORE"
          },
          nextSectionButtonTypeObject: {
            proceed: "PROCEED TO NEXT SECTION",
            popup: "POP-UP WINDOW"
          }
        }
      },
      testSection: {
        title: "Test Sections",
        validationErrors: {
          mustContainAtLeastThreeTestSectionError:
            "Error: Must contain at least three test sections",
          mustContainQuitTestSectionError: "Error: Must contain a 'Quit' test section",
          mustContainFinishTestSectionError: "Error: Must contain a 'Finish' test section"
        },
        description:
          "Create a test by adding and defining Test Sections. Typical Test Sections may include the following pages: Welcome, Test Security, Terms and Conditions, Navigation, Specific Instructions, Test Questions, Test Submitted and Quit Test.",
        collapsableItemName: "Test Section {0}: {1}",
        addButton: "New Test Section",
        delete: {
          title: "Delete Test Section Component",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        fr_title: {
          title: "Test Section Name (French)",
          titleTooltip: "Hidden name used for easy identification by internal users",
          errorMessage: "Not a valid Name"
        },
        en_title: {
          title: "Test Section Name (English)",
          titleTooltip: "Hidden name used for easy identification by internal users",
          errorMessage: "Not a valid Name"
        },
        uses_notepad: {
          title: "Notepad",
          titleTooltip: "Does this test section use the notepad component?",
          errorMessage: "not a valid choice"
        },
        uses_calculator: {
          title: "Calculator",
          titleTooltip: "Does this test section use the calculator component?"
        },
        block_cheating: {
          title: "Monitor Window Switching",
          titleTooltip: "Should this test section report the user for switching tabs etc.?",
          errorMessage: "not a valid choice"
        },
        item_exposure: {
          title: "Item Exposure",
          titleTooltip: "Should this test section apply the item exposure logic?"
        },
        scoring_type: {
          title: "Scoring",
          titleTooltip: "Set the correct scoring method for the components in this test section.",
          errorMessage: "not a valid choice"
        },
        minimum_score: {
          title: "Minimum Score",
          titleTooltip:
            "Set minimum score for a test section. Currently this value is only for reference in reports.",
          errorMessage: "not a valid choice"
        },
        default_time: {
          title: "Default Time Limit",
          titleTooltip:
            "How long does a candidate have on this section? If the section is not timed, then leave this empty. This field can be overridden by a TA",
          errorMessage: "Only numbers are allowed"
        },
        default_tab: {
          title: "Default Tab",
          titleTooltip: "Which top tab should be displayed when a candidate begins this section",
          errorMessage: "not a valid choice",
          selectPlaceholder: "0"
        },
        order: {
          title: "Order of Presentation",
          titleTooltip: "The order in the test that this section appears.",
          errorMessage: "not a valid choice",
          selectPlaceholder: "0"
        },
        next_section_button: {
          title: "Next Section Button",
          titleTooltip: "Define a button to take the user to the next section.",
          errorMessage: "not a valid choice",
          selectPlaceholder: "0"
        },
        reducers: {
          title: "Reducers",
          titleTooltip:
            "Which aspects of the test should be cleared when a user navigates to this section?",
          errorMessage: "not a valid choice",
          selectPlaceholder: "--"
        },
        section_type: {
          title: "Section Type",
          titleTooltip:
            "The type of test section. Such as: Single page, Top Tab navigation, Finish, Quit...",
          errorMessage: "Not a valid Type"
        },
        deletePopup: {
          title: "Confirm Deletion",
          content: "Are you sure you want to delete this section?"
        }
      },
      nextSectionButton: {
        header: "Next Section Button",
        needs_approval: {
          title: "Needs TA Approval",
          titleTooltip: "Candidate will need TA approval to proceed to next section"
        },
        button_type: {
          title: "Button Type",
          errorMessage:
            "Selected values for Section Type and Button Type are not compatible. Please change.",
          titleTooltip: "This is selected based on the test section component."
        },
        confirm_proceed: {
          title: "Confirmation Checkbox",
          titleTooltip:
            "A checkbox the user has to click before being allowed to press the action button."
        },
        button_text: {
          title: "Button Label",
          titleTooltip: "The text that the button displays to the user"
        },
        title: {
          title: "Pop-up Window Title",
          titleTooltip:
            "The text that appears on the header of the popup when the next section button is clicked."
        },
        content: {
          title: "Pop-up Window Content",
          titleTooltip:
            "The text that appears in the body of the popup when the next section button is clicked."
        }
      },
      testSectionComponents: {
        title: "Test Sub-Sections",
        validationErrors: {
          mustFixTheQuestionsRulesErrors:
            "Error: Fix the Test Assembly Rules (Item Blocks) below and try again.",
          undefinedPpcQuestionIdInDependentQuestion:
            "Please make sure that all dependent items have a defined 'R&D Item ID'",
          notEnoughDefinedPpcQuestionIdBasedOnNbOfQuestions:
            "Error: Insufficient number of items associated to item blocks. Please return to the Items section and assign item blocks to a sufficient number of items that meets the Number of Items as specified in the Test Assembly Rules (Item Blocks) below.",
          mustContainQuestionListComponentType:
            'Error: Please make sure that any Test Section with a "Scoring Method" set at "AUTO SCORE" contains at least one "Sub-Section Type" set at "ITEMS_(TEST_BUILDER)".',
          mustContainItemBankRules:
            "All test section component of component type ITEM_BANK must have defined and valid rules",
          mustContainSubSections:
            "All test sections must contain at least one associated sub-section"
        },
        description:
          'Create and define Test Sub-Sections for all Test Sections of the test. Typical Test Sub-Sections may include "Test Instructions" and "Test Questions" under the associated "Test Questions" section. All Test Sections must include a Test Sub-Section. If no Test Sub-Section is needed, please create a Test Sub-Section and enter the same name as its parent section.',
        collapsableItemName: "Test Sub-Section {0}: {1}",
        addButton: "Add Test Sub-Section",
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        parentTestSection: {
          title: "Test Section",
          titleTooltip: "The test section that the displayed objects appear in."
        },
        fr_title: {
          title: "French Name",
          titleTooltip: "Hidden name used for easy identification by internal users",
          errorMessage: "Not a valid Name"
        },
        en_title: {
          title: "English Name",
          titleTooltip: "Hidden name used for easy identification by internal users",
          errorMessage: "Not a valid Name"
        },
        order: {
          title: "Sub-Section Order",
          titleTooltip: "The order in which this component appears in a Top Tab navigation.",
          errorMessage: "Not a valid Name"
        },
        component_type: {
          title: "Sub-Section Type",
          titleTooltip: "The type of component. Such as: Inbox, Single Page, Side Navigation...",
          errorMessage:
            "Make sure that this is a valid parent test section data and component type combination"
        },
        content_type: {
          title: "Content Type (format)",
          titleTooltip: "The type of content. Standard or alternative content"
        },
        shuffle_all_questions: {
          title: "Randomize Items (Test Builder)",
          titleTooltip:
            "Shuffle the questions in this question list? Question dependencies remain in the order they were given."
        },
        shuffle_question_blocks: {
          title: "Randomize Blocks (Test Builder)",
          titleTooltip: "Shuffle the question blocks in this question list? (TO BE COMPLETED)"
        },
        shuffle_item_bank_rules: {
          title: "Shuffle Item Bank Rules",
          titleTooltip: "Shuffle the item bank rules?"
        },
        language: {
          title: "Sub-Section Language",
          titleTooltip:
            "Which language will the content in this section be provided in? This is mandatory for screen readers to use the correct accent.",
          errorMessage: "not a valid choice",
          selectPlaceholder: "--"
        }
      },
      questionListRules: {
        title: "Test Assembly Rules (Item Blocks)",
        collapsableItemName: "Rule {0}",
        addButton: {
          title: "Add Block",
          titleTooltip: "You must create Question Block Types in order to add rules."
        },
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        number_of_questions: {
          title: "Number of Items",
          titleTooltip: "The number of questions to select of the following question block type.",
          errorMessage: "Must be a valid Number of Items"
        },
        question_block_type: {
          title: "Block:",
          titleTooltip: "The question block type from which to select questions.",
          errorMessage: "Must select a Test Assembly Rule (Item Blocks)"
        },
        order: {
          title: "Block Order:",
          titleTooltip:
            "The order that this rule will appear in the question list. If there is no order, leave this field empty."
        },
        shuffle: {
          title: "Randomize Items:",
          titleTooltip:
            "Shuffle the questions in this rule? Question dependencies remain in the order they were given."
        }
      },
      itemBankRules: {
        title: "Test Assembly Rules (Item Bank)",
        collapsableItemName: "Grouping {0}",
        order: {
          title: "Order",
          titleTooltip: "The order that this rule will appear."
        },
        item_bank: {
          title: "Item Bank:",
          titleTooltip: "Item bank from which you want to pull the respective grouping from."
        },
        item_bank_bundle: {
          title: "Grouping:",
          titleTooltip: "Respective Grouping from which the items list will be generated."
        }
      },
      sectionComponentPages: {
        title: "Test Sub-Sections",
        description:
          'Create and define Test Sub-Sections for all Test Sections of the test. Typical Test Sub-Sections may include "Test Instructions" and "Test Questions" under the associated "Test Questions" section. All Test Sections must include a Test Sub-Section. If no Test Sub-Section is needed, please create a Test Sub-Section and enter the same name as its parent section.  ',
        validationErrors: {
          mustContainPageContentTitlesError:
            'Error: All Test Sub-Sections except the ones with Sub-Section Type set to "ITEMS_(TEST_BUILDER)" must be associated with a Section Title.'
        },
        addButton: "Add Test Sub-Section",
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        collapsableItemName: "Test Sub-Section {0}",
        parentTestSectionComponent: {
          title: "Test Sub-Section",
          titleTooltip: "The parent test section the displayed objects are displayed in."
        },
        fr_title: {
          title: "French Name",
          titleTooltip:
            "Name of the side navigation tab when the site language is French. This field is hidden when the Test Section Component is SINGLE_PAGE.",
          errorMessage: "Not a valid Name"
        },
        en_title: {
          title: "English Name",
          titleTooltip:
            "Name of the side navigation tab when the site language is French. This field is hidden when the Test Section Component is SINGLE_PAGE.",
          errorMessage: "Not a valid Name"
        },
        order: {
          title: "Sub-Section Order",
          titleTooltip: "The order in which this page appears in a side navigation panel.",
          errorMessage: "Not a valid Name"
        }
      },
      componentPageSections: {
        title: "Section Content",
        description: "Add content to Test Sections and Sub-Sections.",
        collapsableItemName: "Section Component Page Order: {0}, Type: {1}",
        parentSectionComponentPage: {
          title: "Section Titles",
          titleTooltip: "The parent side navigation tab that the objects displayed will belong to."
        },
        pageSectionLaguage: {
          title: "Language",
          titleTooltip:
            "If a test is bilingual, you must create page sections for each language. This will show all the page sections created for the selected language."
        },
        addButton: "Add Content",
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        order: {
          title: "Section Content Order:",
          titleTooltip: "The order in which this page appears in a side navigation panel.",
          errorMessage: "Not a valid Name"
        },
        page_section_type: {
          title: "Content Type:",
          titleTooltip:
            "The type of section to display (i.e. Markdwon, sample email, sample teask response).",
          errorMessage: "Not a valid type."
        },
        item_bank: {
          title: "Item Bank:",
          titleTooltip: "Select the appropriate instructions from an item bank",
          errorMessage: "Not a valid item bank."
        },
        item: {
          title: "Instruction:",
          titleTooltip: "Select the Instructions to use",
          errorMessage: "Not a valid instruction item."
        },
        instruction_preview: {
          title: "Selected Instruction's Content: ",
          titleTooltip: "This is a preview of the content of the selected instruction",
          errorMessage: "Not a valid instruction item."
        },
        content_type: {
          title: "Content Format:",
          titleTooltip: "Select the type of content",
          errorMessage: "Not a valid content type."
        },
        survey_link_text_to_display: {
          title: "Text to Display:",
          titleTooltip: "Text that will be displayed as a hyperlink",
          translationButtonTooltip: "Modify Translations",
          translationPopup: {
            title: "Survey Link - Text to Display - Translation",
            en_name: {
              title: "Text to Display (EN):"
            },
            fr_name: {
              title: "Text to Display (FR):"
            }
          }
        },
        survey_link_value: {
          title: "URL:",
          titleTooltip: "Enter the URL here"
        }
      },
      itemBankInstructionPageSectionForm: {
        instructionDropdownTitle: "{0} - Historical ID: {1} - Version: {2}",
        itemContentDropdownOptions: {
          screenReader: "Screen Reader",
          regular: "Regular"
        }
      },
      markdownPageSection: {
        content: {
          title: "Content",
          titleTooltip: "The markdown content to display."
        }
      },
      sampleEmailPageSection: {
        email_id: {
          title: "Email ID",
          titleTooltip: "The email ID to display"
        },
        from_field: {
          title: "From",
          titleTooltip: "Who the email is from."
        },
        to_field: {
          title: "To",
          titleTooltip: "Who the email is to"
        },
        date_field: {
          title: "Date",
          titleTooltip: "The date displayed on the email."
        },
        subject_field: {
          title: "Subject",
          titleTooltip: "The subject displayed on the email."
        },
        body: {
          title: "Body",
          titleTooltip: "The body of the email to display."
        }
      },
      sampleEmailResonsePageSection: {
        cc_field: {
          title: "CC",
          titleTooltip: "Who is CC'd on the email"
        },
        to_field: {
          title: "To",
          titleTooltip: "Who the email is to"
        },
        response: {
          title: "Response",
          titleTooltip: "The email response."
        },
        reason: {
          title: "Reason",
          titleTooltip: "The reason for responding with this email."
        }
      },
      sampleTaskResonsePageSection: {
        response: {
          title: "Response",
          titleTooltip: "The task response."
        },
        reason: {
          title: "Reason",
          titleTooltip: "The reason for responding with this task."
        }
      },
      zoomImagePageSection: {
        small_image: {
          title: "Full Image Name",
          titleTooltip: "name of the image file including extension."
        },
        note: "Note",
        noteDescription:
          "In order for this image to be displayed properly you must provide the image to the IT team to upload to the application server."
      },
      treeDescriptionPageSection: {
        address_book_contact: {
          title: "Root Contact",
          titleTooltip: "The root contact that the tree description will start from."
        }
      },
      addressBook: {
        topTitle: "Address Book",
        description:
          "These are email like contacts used in Inbox style Test Section Components and Tree Description Component Page Sections.",
        collapsableItemName: "{0}",
        addButton: "Add Address Book Contact",
        name: {
          title: "Name",
          titleTooltip: "The name of the contact."
        },
        title: {
          title: "Title",
          titleTooltip: "The title of the contact."
        },
        parent: {
          title: "Parent Contact",
          titleTooltip: "The contact that this contact reports to."
        },
        test_section: {
          title: "Test Sections",
          titleTooltip:
            "Which test sections can a user send emails to this contact or if there is a tree description in a test section then this contact must be in that test section."
        }
      },
      questions: {
        topTitle: "Items",
        description:
          'Create items using the Test Builder. To create items, a "ITEMS_(TEST_BUILDER)" Sub-Section Type must be defined for a given Test Sub-Section.',
        validationErrors: {
          uploadFailedMissingQuestionListQuestions:
            'Error: Please make sure that all Test Sub-Sections with a Sub-Section Type set to "ITEMS_(TEST_BUILDER)" contain at least one item.'
        },
        collapsableItemNames: {
          block: "[Block]",
          id: "[ID]",
          text: "[Text]"
        },
        emailCollapsableItemName: "Email ID: {0}, From: {1}",
        addButton: "New Item",
        searchResults: "{0} Item(s) found",
        pilot: {
          title: "Experimental Item",
          titleTooltip:
            "Is the question a pilot question? It's answer will not be counted towards a final score.",
          description: ""
        },
        questionBlockSelection: {
          title: "Item Block Name",
          titleTooltip: "The Question Block Type to narrow the number of results."
        },
        order: {
          title: "Item Order:",
          titleTooltip:
            "Order that questions will be displayed in the test (if there are no dependencies and/or shuffle) - TO BE COMPLETED"
        },
        ppc_question_id: {
          title: "R&D Item ID",
          titleTooltip: "PPC Question ID Tooltip Content - TO BE COMPLETED"
        },
        question_type: {
          title: "Response Format",
          titleTooltip:
            "i.e. multiple choice, email. This is pre-determined by the Test Section Component Type (Question List or Inbox)"
        },
        question_block_type: {
          title: "Item Block Name",
          titleTooltip:
            "The question block type identifier. Used to determine what questions are returned for a question lists rule set.",
          selectPlaceholder: "Please Select"
        },
        dependencies: {
          title: "Dependents",
          titleTooltip: "Questions that this question must be presented with (in order)."
        },
        dependent_order: {
          title: "Dependent Order",
          titleTooltip:
            "The order this question is presented in when dependents exist. This is only used for ordering questions in a dependency sitaution."
        },
        shuffle_answer_choices: {
          title: "Randomize Answer Choices",
          titleTooltip: "If enabled, the answer choices will be shuffled - TO BE COMPLETED"
        },
        previewPopup: {
          title: "Question Preview"
        }
      },
      scoringMethod: {
        title: "Scoring Method",
        validationErrors: {
          mustApplyScoringMethodError: "Error: Must apply a scoring method type"
        },
        description: "Select the scoring method for this test.",
        scoringMethodUndefinedError: "Please select a valid scoring method.",
        scoringMethodType: {
          title: "Scoring Method:",
          titleTooltip: "Scoring mechanism to use for score conversion"
        },
        resultValidIndefinitely: {
          title: "Result Validity Period: Indeterminate",
          titleTooltip: `A test result with an "indeterminate" Result Validity Period will remain valid forever or until a candidate retakes the test.`
        },
        validityPeriod: {
          title: "Result Validity Period (months):",
          titleTooltip: "The period (in months) during which the test result is valid",
          errorMessage: "Must be a valid number"
        },
        scoringPassFailMinimumScore: {
          title: "Pass mark:",
          titleTooltip: "Minimum total score required to get a 'pass' mark",
          errorMessage: "Must be a valid number"
        },
        scoringThreshold: {
          addButton: "Score Band",
          collapsingItem: {
            title: "Score Band: {0} to {1} = {2}",
            minimumScore: {
              title: "Minimum Score:",
              placeholder: "{minimum_score}",
              titleTooltip: "Minimum required score to get the conversion value",
              errorMessage: "Must be a valid Minimum Score"
            },
            maximumScore: {
              title: "Maximum Score:",
              placeholder: "{maximum_score}",
              titleTooltip: "Maximum required score to get the conversion value",
              errorMessage: "Must be a valid Maximum Score"
            },
            enConversionValue: {
              title: "Value (English):",
              placeholder: "{conversion_value}",
              titleTooltip:
                "Conversion value to display when respecting the minimum and maximum scores",
              errorMessage: "Must be a valid Value"
            },
            frConversionValue: {
              title: "Value (French):",
              placeholder: "{conversion_value}",
              titleTooltip:
                "Conversion value to display when respecting the minimum and maximum scores",
              errorMessage: "Must be a valid Value"
            }
          },
          deletePopup: {
            title: "Delete Confirmation",
            content:
              "Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
          },
          invalidCollectionError: "Must be valid band ranges"
        }
      },
      questionSituations: {
        situation: {
          title: "Situation",
          titleTooltip:
            "The situation text to be displayed at the top of the question when a test is viewed in scorer mode."
        }
      },
      questionBlockTypes: {
        topTitle: "Item Block Names",
        description:
          'Define block names to be used with items. Blocks must only be used for items created within the "Items" section of the Test Builder. For items pulled from the Item Bank, use the "Grouping" functionality within the Item Bank instead.',
        collapsableItemName: "Question Block Type: {0}",
        addButton: "Add Item Block Name",
        addName: "***ADD NAME***",
        name: {
          title: "Name",
          titleTooltip: "The unique identifier for a question block type.",
          errorMessage: "Must be a valid name."
        }
      },
      competencyTypes: {
        topTitle: "Competency Types",
        description:
          "These are types associated with questions. These types are assigned to questions for manual marking.",
        collapsableItemName: "Competency Type: {0}",
        addButton: "Add Competency Type",
        en_name: {
          title: "English Name",
          titleTooltip: "The unique identifier for a question block type."
        },
        fr_name: {
          title: "French Name",
          titleTooltip: "The unique identifier for a question block type."
        },
        max_score: {
          title: "Max Score",
          titleTooltip: "The max score a candidate can get for this competency."
        }
      },
      questionSections: {
        title: "Stem(s):",
        collapsableItemName: "Stem {0}",
        description: `Create and define Test Sub-Sections for all Test Sections of the test. Typical Test Sub-Sections may include "Test Instructions" and "Test Questions" under the associated "Test Questions" section. All Test Sections must include a Test Sub-Section. If no Test Sub-Section is needed, please create a Test Sub-Section and enter the same name as its parent section.`,
        addButton: "Stem",
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        order: {
          title: "Stem Order",
          titleTooltip: "The order in which this component appears in a Top Tab navigation.",
          errorMessage: "Not a valid Name"
        },
        question_section_type: {
          title: "Content Type:",
          titleTooltip: "The type of section. Such as: Markdown, Image, Video...",
          errorMessage: "Not a valid Type"
        }
      },
      answers: {
        title: "Answer Choices:",
        collapsableItemNames: {
          id: "[ID]",
          value: "[Value]",
          text: "[Text]"
        },
        addButton: "Answer Choice",
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        content: {
          title: "content",
          titleTooltip: "The text to display for this answer.",
          errorMessage: "Not a valid input"
        },
        order: {
          title: "Answer Choice Order",
          titleTooltip: "The order in which this answer appears."
        },
        ppc_answer_id: {
          title: "R&D Answer Choice ID",
          titleTooltip: "PPC Answer ID Tooltip Content - TO BE COMPLETED"
        },
        scoring_value: {
          title: "Scoring Value",
          titleTooltip:
            "The score to be added to the total test score when this answer is selected.",
          errorMessage: "Not a valid value"
        }
      },
      questionSectionMarkdown: {
        content: {
          title: "Content",
          titleTooltip: "Markdown to appear on the question."
        },
        delete: {
          title: "Confirm Deletion",
          content:
            "Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
        }
      },
      questionSectionEnd: {
        content: {
          title: "Specific Question",
          titleTooltip:
            "The literal question after context to be answered. This allows screen readers to navigate directly to this section."
        },
        delete: {
          title: "Confirm Deletion",
          content:
            "Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
        }
      },
      multipleChoiceQuestions: {
        header: "Question Sections"
      },
      emailQuestions: {
        scoringTitle: "Scoring",
        exampleRatingsTitle: "Example Ratings",
        competency_types: {
          title: "Competencies",
          titleTooltip: "The competencies being assessed in this question."
        },
        deleteDescription:
          "Deleting this item will delete the current object and any children this object may have. Are you sure you want to delete this object?",
        email_id: {
          title: "Email Id",
          titleTooltip:
            "Email Id is displayed at the top of the email and is the order in which emails are displayed."
        },
        to_field: {
          title: "To Field",
          titleTooltip: "The contact the email is to."
        },
        from_field: {
          title: "From Field",
          titleTooltip: "The Contact the email is from."
        },
        cc_field: {
          title: "CC Field",
          titleTooltip: "Contacts that are CC'd on this email."
        },
        date_field: {
          title: "Date",
          titleTooltip: "The date text to display on the email"
        },
        subject_field: {
          title: "Subject",
          titleTooltip: "The subject field to display on the email"
        },
        body: {
          title: "Body",
          titleTooltip: "The content of the email to display."
        }
      },
      situationRating: {
        score: {
          title: "Rating Score",
          titleTooltip:
            "This is the value that the example answered would be scored. Keep this to one decimal place."
        },
        example: {
          title: "Example Rationale",
          titleTooltip: "The Rationale behind giving the candidates answer this score."
        }
      },
      testManagement: {
        description: "Manage the status of this Test Version.",
        deactivateTestVersion: {
          title: "Test Version Status",
          titleTooltip:
            "De-activate this test version: Remove this test version from the sample tests pages or make it unavailable to be assigned by business operations (this does not invalidate currently active test accesses)."
        },
        activateTestVersion: {
          title: "Test Version Status",
          titleTooltip:
            "Make this test version available for use: Visible on the sample tests page or available to be assigned by business operations (as appropriate)."
        },
        uploadTestVersion: {
          title: "Upload Test Version to server",
          titleTooltip:
            "Upload the changes made to this test version to the server as a new version."
        },
        uploadTestVersionButton: "Upload",
        downloadTestVersion: {
          title: "Download Test Version JSON",
          titleTooltip: "Extract a JSON file of this test version; this excludes any changes made."
        },
        downloadTestVersionButton: "Download"
      }
    },
    // HR Coordinator Testing Page
    inPersonTesting: {
      title: "Welcome, {0} {1}.",
      containerLabel: "HR Coordinator",
      sideNavItems: {
        assessmentProcesses: "Assessment Processes",
        manageBillingContacts: "Manage Billing"
      },
      assessmentProcesses: {
        title: "Assessment Processes",
        createProcess: {
          title: "Create Process",
          referenceNumberLabel: "Assessment Process / Reference Number",
          departmentLabel: "Organization:",
          durationLabel: "Process Duration (calendar days):",
          allowBookingExternalTcLabel: "Allow reservations at other organizations' test centres:",
          defaultBillingContactLabel: "Billing Contact:",
          contactEmailForCandidates: "Contact Email for Candidates:",
          invalidContactEmailForCandidates: "Must be a valid Email Address",
          testsToAdministerLabel: "Test(s) to Administer:",
          testsToAdministerTable: {
            columnOne: "Test",
            columnTwo: "Default Reason for Testing",
            columnThree: "Default Required Level",
            columnFour: "Actions",
            viewAvailableTestSessions: "View Available Test Sessions",
            viewAvailableTestSessionsAccessibility: "View available test sessions for {0}",
            deleteTestAccessibility: "Delete {0}",
            addTestToAdministerButton: "Test to Administer:",
            addTestToAdministerPopup: {
              title: "Add a test to administer",
              testSkillTypeLabel: "Test Type:",
              testSkillSubTypeLabel: "Test:",
              defaultReasonForTestingLabel: "Default Reason for Testing",
              defaultLevelRequiredLabel: "Default Required Level",
              addTestButton: "Add Test"
            },
            viewAvailableTestSessionsPopup: {
              title: "Available Test Sessions",
              description: "Here are the available test sessions for {0}:",
              table: {
                columnOne: "Test Session Date",
                columnTwo: "Test Centre",
                columnThree: "Organization",
                columnFour: "City",
                columnFive: "Spaces Available",
                noData: "No test sessions available at the moment"
              }
            },
            deleteTestToAdministerPopup: {
              title: "Delete Test",
              systemMessageDescription: "The test {0}, will be removed from the process.",
              description: "Are you sure you want to proceed?"
            }
          },
          createProcessButton: "Create Process",
          confirmationPopup: {
            title: "Process Created Successfully",
            systemMessageDescription: "Your process has been created successfully."
          }
        },
        assignCandidates: {
          title: "Candidates",
          referenceNumberLabel: "Assessment Process / Reference Number:",
          sentDateLabel: "Initial Sent Date:",
          departmentLabel: "Requesting Organization:",
          closingDateLabel: "Expiry Date:",
          durationLabel: "Process Duration (calendar days):",
          durationError: "Process duration must be greater than {0}",
          allowBookingExternalTcLabel: "Allow reservations at other organizations' test centres:",
          updatesSuccessPopup: {
            title: "Update successfull",
            systemMessageDescription: "Assessment Process Data Updated Successfully"
          },
          contactEmailForCandidates: "Contact Email for Candidates:",
          testsToAdministerLabel: "List of tests for this process:",
          testsToAdministerTable: {
            columnOne: "Test",
            columnTwo: "Default Reason for Testing",
            columnThree: "Level Required",
            columnFour: "Actions",
            viewAvailableTestSessions: "View Available Test Sessions",
            viewAvailableTestSessionsAccessibility: "View available test sessions for {0}",
            viewAvailableTestSessionsPopup: {
              title: "Available Test Sessions",
              description: "Here are the available test sessions for {0}:",
              table: {
                columnOne: "Test Session Date",
                columnTwo: "Test Centre",
                columnThree: "Organization",
                columnFour: "City",
                columnFive: "Spaces Available",
                noData: "No test sessions available at the moment"
              }
            }
          },
          assignedCandidatesLabel: "List of Candidates:",
          assignedCandidatesTable: {
            addCandidateButton: "Add Candidate",
            inviteCandidateButton: "Add Candidate",
            columnOne: "First Name",
            columnTwo: "Last Name",
            columnThree: "Email Address",
            columnFour: "Tests Details",
            columnFourTooltip: "<Test> | <Reason for Testing> | <Level Required> | (<Status>)",
            columnFourFormattedRelatedStatus: "| ({0})",
            columnFive: "Actions",
            editTooltip: "Edit",
            editAccessibility: "Edit {0} {1}",
            deleteAccessibility: "Delete {0} {1}",
            editAssignedCandidatePopup: {
              title: "Edit Assigned Candidate"
            },
            deleteAssignedCandidatePopup: {
              title: "Delete Assigned Candidate",
              systemMessageDescription:
                "{0} {1} will be removed from the list of assigned candidates.",
              description: "Are you sure you want to proceed?"
            }
          },
          addCandidatePopup: {
            title: "Add Candidate",
            description: "Please provide the required information",
            firstNameLabel: "First Name:",
            invalidFirstName: "Must be a valid First Name",
            lastNameLabel: "Last Name:",
            invalidLastName: "Must be a valid Last Name",
            emailLabel: "Email:",
            invalidEmail: "Must be a valid Email",
            duplicateEmailError: "This email has already been used in this assessment process",
            billingContactLabel: "Billing Contact:",
            testsToAdministerTable: {
              columnOne: "Test",
              columnTwo: "Reason for Testing",
              columnThree: "Level Required",
              columnFour: "Assign Test",
              columnFive: "Action",
              columnSix: "Status"
            },
            addCandidateButton: "Add Candidate"
          },
          sendRequestConfirmationPopup: {
            title: "Send Invitations?",
            titleSuccess: "Invitations Sent Successfully",
            systemMessageDescription: "You will not be able to modify invitations once sent.",
            systemMessageDescriptionSuccess: "Invitations have been sent successfully.",
            description: "Are you sure you want to proceed?",
            sendRequestButton: "Send"
          },
          editProcessNamePopup: {
            title: "Edit {0}"
          },
          allowLastMinuteCancellationsToggle: "Allow Last Minute Cancellations (Oral only):",
          olaCheckbox1:
            "I confirm that the candidate(s) have valid results for the Test of Reading Comprehension and the Test of Written Expression that meet the required level.",
          olaCheckbox2:
            "I attest that the staffing process is imperative and that the appointment is imminent",
          sendRequestButton: "Send Invitations",
          deleteProcessPopup: {
            title: "Delete Assessment Process",
            message: "Assessment Process {0} will be deleted.",
            description: "Are you sure you want to proceed?"
          }
        },
        activeProcesses: {
          title: "Active Processes",
          sendUpdatesButton: "Send Updates",
          updatesSuccessPopup: {
            title: "Update successfull",
            systemMessageDescription:
              "The assessment process data has been modified successfully. Affected candidates will be notified by email."
          },
          inviteCandidatePopup: {
            title: "Invite Candidate",
            inviteCandidateButton: "Send Invitations"
          },
          editCandidatePopup: {
            sendUpdatesButton: "Send Updates",
            unassignTestTooltip: "Unassign Test",
            unassignTestAccessibility: "Unassign {0} Test",
            unassignTestPopup: {
              title: "Unassign Test",
              systemMessageDescription:
                "This action cannot be undone. This test cannot be resent to this candidate under this process.",
              description: "Are you sure you want to proceed?",
              unassignButton: "Unassign",
              unassignLoading: "Unassigning..."
            }
          }
        },
        processResults: {
          title: "Results",
          table: {
            noData: "There is no result available.",
            topTabs: {
              columnOne: "Assessment Process / Reference Number",
              columnTwo: "Expiry Date",
              columnThree: "Candidates",
              columnFour: "Test Taken",
              columnFive: "Actions"
            },
            viewAssessmentProcessButton: {
              ariaLabel: "View assessment process: {0}",
              tooltipContent: "View assessment process"
            },
            downloadReportAssessmentProcessButton: {
              ariaLabel: "Download the test result report for the assessment process: {0}",
              tooltipContent: "Download the test result report for the assessment process."
            },
            viewProcessResultPopup: {
              noData: "There is no data available.",
              columnFive: "Test Results"
            }
          }
        }
      },
      manageBillingContacts: {
        title: "Manage Billing Information",
        table: {
          topTabs: {
            columnOne: "First Name",
            columnTwo: "Last Name",
            columnThree: "Email Address",
            columnFour: "Organization",
            columnFive: "IS - Organization Code",
            columnSix: "IS - Reference Code",
            columnSeven: "Actions"
          },
          noData: "There are no results found based on your search.",
          addBillingContactButton: "Add",
          modifyBillingContactButton: {
            ariaLabel: "Edit the contact ({0} {1})",
            tooltipContent: "Edit"
          },
          deleteBillingContactButton: {
            ariaLabel: "Delete the contact ({0} {1})",
            tooltipContent: "Delete"
          }
        },
        billingContactPopup: {
          firstName: {
            title: "First Name:",
            error: "Must be a valid First Name"
          },
          lastName: {
            title: "Last Name:",
            error: "Must be a valid Last Name"
          },
          email: {
            title: "Email:",
            error: "Must be a valid Email Address"
          },
          departmentId: {
            title: "Organization:",
            error: "Department Error"
          },
          fisOrgCode: {
            title: "IS - Organization Code",
            error: "IS - Organization Code:"
          },
          fisRefCode: {
            title: "IS - Reference Code",
            error: "IS - Reference Code:"
          }
        },
        addBillingContactPopup: {
          title: "Add Billing Contact",
          addButton: "Add",
          description:
            "Add the required billing information. If uncertain, check with your management or finance team."
        },
        modifyBillingContactPopup: {
          title: "Edit Billing Contact",
          description:
            "Changes made to this billing contact will be applicable only to its next uses."
        },
        deleteBillingContactPopup: {
          title: "Delete Billing Contact",
          message: "{0} {1} will be deleted from your list of billing contacts.",
          description: "Are you sure you want to proceed?"
        },
        errorBillingContactPopup: {
          title: {
            addBillingContact: "Error - Add Billing Contact",
            modifyBillingContact: "Error - Modify Billing Contact",
            deleteBillingContact: "Error - Delete Billing Contact"
          },
          description: {
            addBillingContact:
              "There has been an error when adding the billing contact. Please try again.",
            modifyBillingContact:
              "There has been an error when modifying the billing contact. Please try again.",
            deleteBillingContact:
              "There has been an error when deleting the billing contact. Please try again."
          }
        }
      }
    },
    // Test Administration Page
    testAdministration: {
      title: "Welcome, {0} {1}.",
      containerLabel: "Test Administrator",
      sideNavItems: {
        supervisedTesting: "Supervised Testing",
        activeCandidates: "Active Candidates",
        uat: "Unsupervised Internet Testing",
        reports: "Reports"
      },
      supervisedTesting: {
        description:
          "As candidates check-in, you will see their names appear in the list of Active Candidates below. After all candidates have checked in, delete the Test Access Code.",
        testSessionLabel: "Test Session:",
        testSessionAttendees: {
          viewAttendeesTooltip: "View list of candidates",
          popup: {
            title: "List of candidates",
            description1: "Test Session Date: {0}",
            description2: "Room: {0}",
            description3: "Test: {0}",
            table: {
              columnOne: "First Name",
              columnTwo: "Last Name",
              columnThree: "Email",
              columnFour: "Account Email",
              noData: "There are currently no candidates."
            },
            printButton: "Print",
            printDocumentTitle: "Candidate list for test session {0}"
          }
        },
        testSessionTestAccessCodes: "Access codes for this session's tests\u00A0:",
        testSessionAssignedCandidates: "Session Candidates:",
        testAccessCodesTable: {
          testAccessCode: "Test Access Code",
          test: "Test",
          action: "Action",
          actionButton: "Delete",
          actionButtonAriaLabel: "Delete {0}",
          generateNewCode: "Generate Test Access Code"
        },
        generateTestAccessCodePopup: {
          title: "Generate a Test Access Code",
          description: "Provide the following information to generate a Test Access Code:",
          testSession: "Test Session:",
          testToAdminister: "Test:",
          cannotGenerateTestAccessCodeError:
            "You do not have the right test permissions to generate a test access code for that particular test session",
          testSessionOfficers: "Test Session Officer(s):",
          billingInformation: {
            title: "Billing Information:",
            staffingProcessNumber: "Assessment Process Number:",
            departmentMinistry: "Organization Code:",
            contact: "Billing Contact Name:",
            isOrg: "FIS Organization Code:",
            isRef: "FIS Reference Code:"
          },
          generateButton: "Generate"
        },
        disableTestAccessCodeConfirmationPopup: {
          title: "Delete Test Access Code",
          description:
            "If you delete the Test Access Code {0}, candidates will not be able to use it to check in."
        },
        approveAllButton: "Approve All",
        lockAllButton: "Lock All",
        unlockAllButton: "Unlock All",
        assignedCandidatesTable: {
          candidate: "Candidate Name and Contact Details",
          dob: "Date of Birth",
          testCode: "Test Form",
          status: "Status",
          timer: "Time Limit",
          timeRemaining: "Time Left",
          actions: "Action",
          actionTooltips: {
            updateTestTimer: "Edit Time Limit",
            addEditBreakBank: "Add/Edit Break Bank",
            approve: "Approve Candidate",
            unAssign: "Remove Access",
            lock: "Lock Test",
            unlock: "Unlock Test",
            report: "Report Candidate"
          },
          updateTestTimerAriaLabel:
            "Edit the test timer of user {0} {1}. Their date of birth is {2}. Their test form is {3}. Their current test status is {4}. Their test timer is currently set to {5} hours and {6} minutes.",
          addEditBreakBankAriaLabel:
            "Add / Edit the break bank of user {0} {1}. Their date of birth is {2}. Their test form is {3}. Their current test status is {4}. Their test limt is currently set to {5} hours and {6} minutes.",
          approveAriaLabel:
            "Approve user {0} {1}. Their date of birth is {2}. Their test form is {3}. Their current test status is {4}. Their test timer is currently set to {5} hours and {6} minutes.",
          unAssignAriaLabel:
            "Remove access from user {0} {1}. Their date of birth is {2}. Their test form is {3}. Their current test status is {4}. Their test timer is currently set to {5} hours and {6} minutes.",
          lockAriaLabel:
            "Lock {0} {1}'s test. Their date of birth is {2}. Their test form is {3}. Their current test status is {4}. Their test timer is currently set to {5} hours and {6} minutes.",
          unlockAriaLabel:
            "Unlock {0} {1}'s test. Their date of birth is {2}. Their test form is {3}. Their current test status is {4}. Their test timer is currently set to {5} hours and {6} minutes.",
          reportAriaLabel: "Report {0} {1} user.",
          noActiveCandidates: "There are no active candidates"
        },
        editTimePopup: {
          title: "Edit Time Limit",
          description1: "This should be used for accommodation purposes only.",
          description2: "Adjust the time limit for {0} {1}.",
          description3:
            "The timers cannot be set for less than the original amount of time allowed for the respective test section.",
          description4: "Total Test Time: {0} Hours {1} Minutes",
          hours: "HOURS",
          minutes: "MINUTES",
          incrementHoursButton: "Increase the number of hours. Current value is {0}.",
          decrementHoursButton: "Decrease the number of hours. Current value is {0}.",
          incrementMinutesButton: "Increase the number of minutes. Current value is {0}.",
          decrementMinutesButton: "Decrease the number of minutes. Current value is {0}.",
          timerCurrentValue: "The timer value is {0} hours and {1} minutes.",
          setTimerButton: "Set Timer"
        },
        addEditBreakBankPopup: {
          title: "Set Break Bank",
          description: "Allocate break bank to {0} {1} for this test.",
          setTimeTitle: "Break Bank",
          setBreakBankButton: "Enter"
        },
        lockPopup: {
          title: "Lock a Test",
          description1: "You are about to lock {0} {1}'s test.",
          lockButton: "Lock the Test"
        },
        unlockPopup: {
          title: "Resume Test",
          description1: "You are about to unlock {0} {1}'s test.",
          description2: "The test timer will resume once you unlock the test.",
          description3: "Be sure to complete an incident report regarding this candidate.",
          unlockButton: "Unlock Test"
        },
        lockAllPopup: {
          title: "Lock All Candidate Tests",
          description1: "You are about to lock this test for all of your active candidates.",
          description2:
            "If, after the incident that has caused you to lock the tests, it is determined that the test can resume, you will need to unlock the tests for the candidates that wish to continue testing.",
          description3:
            "Be sure to complete an incident report and a withdrawal from examination form for any candidates not able to continue after the incident.",
          lockTestsButton: "Lock Tests"
        },
        unlockAllPopup: {
          title: "Unlock All Candidate Tests",
          description1: "You are about to unlock this test for all of your active candidates.",
          description2:
            "Be sure to lock any tests for candidates who do not wish to continue with the test or who are unable to continue.",
          description3:
            "Be sure to file a 'withdrawal from examination' form for those candidates, as well as an incident report.",
          unlockTestsButton: "Unlock All Tests"
        },
        approveAllPopup: {
          title: "Approve All Candidates",
          description:
            'You are about to approve all your active candidates. Please check that the candidates listed in the "Candidates for this session" section correspond to those who were originally scheduled to take the test.',
          approveAllButton: "Approve All"
        },
        unAssignPopup: {
          title: "Remove Access",
          description: "Are you sure you want to remove {0} {1}'s access to the test?",
          confirm: "Remove Access"
        }
      },
      uit: {
        title: "Unsupervised Internet Testing (UIT)",
        description: "Manage your UIT invitations and processes.",
        tabs: {
          invitations: {
            title: "Invitations",
            testOrderNumber: "Test Order Number:",
            testOrderNumberCurrentValueAccessibility: "The selected test order number is:",
            testOrderNumberErrorMessage: "Please select the test order number",
            orderlessTestOption: "No Test Order",
            reuseData: "Reuse Data:",
            referenceNumber: "Internal Reference Number:",
            referenceNumberError: "Please Provide an Internal Reference Number",
            departmentMinistry: "Organization:",
            fisOrganisationCode: "IS - Organization Code:",
            fisOrganisationCodeError: "Please Provide the FIS Organisation Code",
            fisReferenceCode: "IS - Reference Code:",
            fisReferenceCodeError: "Please Provide the FIS Reference Code",
            billingContactName: "Billing Contact Name:",
            billingContactNameError: "Please Provide the Billing Contact Name",
            billingInformation: "Contact Email for Candidates:",
            billingInformationError: "Must be a valid email address",
            test: "Test:",
            providedInCsvOption: "Provided in the list",
            reasonForTesting: "Reason for Testing:",
            invalidReasonForTesting: "Please select the reason for testing",
            levelRequired: "Level Required:",
            levelRequiredRawScore: "Raw Score Required:",
            invalidLevelRequired: "Please select the level required",
            invalidLevelRequiredRawScore: "Provided number exceeds the maximum possible score",
            testCurrentValueAccessibility: "The selected tests are: {0}",
            testsErrorMessage: "Please select at least one test",
            validityEndDate: "Expiry Date:",
            numberOfCandidatesBasedOnEndDateExceededError:
              "Maximum number of candidates ending the same day exceeded",
            emailTemplateCurrentValueAccessibility: "The selected email template is:",
            emailTemplate: "Email Template:",
            candidates: "List of Candidates:",
            candidatesCurrentValueAccessibility: "The selected candidates are:",
            browseButton: "Browse...",
            fileSelectedAccessibility: "The selected file is {0}",
            downloadTemplateButton: "Download Template",
            csvTemplateFileName: "List of Candidates.xlsx",
            wrongFileTypeError: "Please select a CSV file type",
            invalidFileError:
              "Error found in file. Please fix the information in the following cells: {0}",
            noDataInCsvFileError: "Please make sure that your CSV file contains data",
            noCsvFileSelectedError: "Please select a valid CSV file",
            maximumInvitedCandideReachedError:
              "Cannot exceed a total of 500 invitations at once (including multi-tests requests)",
            duplicateEmailsFoundInCsvError:
              "Duplicate emails found. Please fix the information related to cells {0} and {1}.",
            sendInvitesButton: "Send Invitations",
            resetAllFields: "Reset All Fields",
            invitesConfirmationPopup: {
              title: "Review and Send Invitation to Candidates",
              titleInvitationsSentSuccessfull: "Invitations Sent Successfully",
              titleInvitationsSentFail: "Invitations Not Sent",
              inviteData: {
                processNumber: "Assessment Process Number:",
                referenceNumber: "Internal Reference Number:",
                requestingDepartment: "Requesting Organization:",
                fisOrganisationCode: "FIS Organisation Code:",
                fisReferenceCode: "FIS Reference Code:",
                HrCoordinator: "Billing Contact Name:",
                billingContactInfo: "Contact Email for Candidates:",
                test: "Test:",
                validityEndDate: "Expiry Date:",
                reasonForTesting: "Reason for Testing:",
                levelRequired: "Level Required:",
                levelRequiredRawScore: "Raw Score Required:",
                table: {
                  title: "List of Candidates ({0}):",
                  emailAddress: "Email Address",
                  firstName: "First Name",
                  lastName: "Last Name"
                },
                invitationSentSuccessfullMessage:
                  "The invitations have been sent to the candidates.",
                invitationSentFailMessage:
                  "Something happened and the invitations haven't been sent! Please contact the HelpDesk."
              },
              sendButton: "Send"
            },
            resetAllFieldsPopup: {
              title: "Reset All Fields?",
              description: "Are you sure you want to reset all fields?",
              resetButton: "Reset"
            }
          },
          activeProcessess: {
            title: "Active Processes",
            table: {
              columnOne: "Assessment Process / Reference Number",
              columnTwo: "Test",
              columnThree: "Number of Tests Taken",
              columnFour: "Requesting Organization",
              columnFive: "Invitation Date",
              columnSix: "Expiry Date",
              columnSeven: "Actions",
              noActiveProcessess: "There are no active processes",
              viewSelectedProcessTooltip: "View Active Process Details",
              updateSelectedProcessExpiryDate: "Update Process Expiry Date",
              deleteSelectedProcessTooltip: "Delete Active Process",
              viewButtonAccessibility: "View assessment process number {0}",
              updateSelectedProcessExpiryDateAccessibility: "Update expiry date of process {0}",
              deleteButtonAccessibility: "Delete assessment process number {0}"
            },
            selectedProcessPopup: {
              title: "Active Process Details",
              financialData: {
                processNumber: "Assessment Process / Reference Number:",
                requestingDepartment: "Requesting Organization:",
                HrCoordinator: "Billing Contact Name:",
                validityEndDate: "Expiry Date:",
                test: "Test:"
              },
              description: "The following candidates ({0}) have been invited to take the UIT:",
              columnOne: "Email Address",
              columnTwo: "Status",
              columnThree: "Actions",
              testInProgress: "In Progress",
              testTaken: "Taken",
              testNotTaken: "Not Taken",
              testUnassigned: "Unassigned",
              testInvalidated: "Invalidated",
              testCodeDeactivated: "Deactivated",
              deleteCandidateTooltip: "Unassign/Deactivate Test Access",
              deleteCandidateAccessibility: "Unassign/deactivate the test access of {0}"
            },
            updateProcessExpiryDatePopup: {
              title: "Modify Expiry Date",
              titleSuccess: "Process Successfully Updated: {0}",
              validityEndDate: "Expiry Date:",
              reasonForModification: "Reason for modification:",
              rightButton: "Modify",
              successMessage: "The expiry date has been updated and the emails have been sent."
            },
            deleteProcessPopup: {
              title: "Delete Assessment Process Number: {0}",
              titleSuccess: "Process Successfully Deleted: {0}",
              systemMessageDescription:
                "You are about to delete the following process. Note that there are still {0} candidate(s) who have not taken their test yet. Are you sure you want to continue?",
              description: "Process Details:",
              deleteButton: "Delete Process",
              successMessage:
                "The assessment process number has been deleted and cancellation emails have been sent."
            },
            deleteSingleUitTestPopup: {
              title: "Confirmation to Unassign/Deactivate Test Access",
              description: "Are you sure you want to unassign/deactivate {0}'s access to the test?",
              deleteButton: "Unassign/Deactivate",
              selectReason: "Please select a reason:"
            },
            uitTestAlreadyStartedPopup: {
              title: "Action Denied"
            }
          },
          completedProcessess: {
            title: "Completed Processess",
            table: {
              process: "Assessment Process / Reference Number",
              test: "Test",
              totalCandidates: "Number of Tests Taken",
              testsTaken: "Requesting Organization",
              endDate: "Expiry Date",
              actions: "Action",
              actionTooltip: "View Completed Process Details"
            },
            noCompletedProcessess: "There are no completed processes",
            popup: {
              title: "UIT: {0} - {1}",
              description: "The following candidates ({0}) have been invited to take the UIT:"
            }
          }
        }
      },
      reports: {
        description: "Generate a results report for a Test Order you administered."
      }
    },

    // PPC Administration Page
    ppcAdministration: {
      title: "Welcome, {0} {1}.",
      containerLabel: "R&D Dashboard",
      sideNavItems: {
        reports: "Reports"
      },
      reports: {
        title: "Reports",
        description: "Generate a report based on a Test Order."
      }
    },

    // Test Center Manager Page
    testCenterManager: {
      title: "Welcome, {0} {1}.",
      containerTestCentersLabel: "Test Centres",
      containerOlaLabel: "OLA",
      table: {
        columnOne: "Test Centre",
        columnTwo: "Address",
        columnThree: "Number of Rooms",
        columnFour: "Number of Test Administrators",
        columnFive: "Pending Requests",
        columnSix: "Action",
        viewButtonAccessibility: "View {0} test center",
        viewButtonTooltip: "View",
        noTestCenter: "There are no test centres to display."
      },
      testCenterManagement: {
        topTabTitle: "{0}",
        sideNavigationItems: {
          testCenterManagement: {
            title: "Test Centre Management",
            topTabs: {
              centerInfo: {
                title: "Test centre information",
                departmentLabel: "Organization:",
                addressLabel: "Address:",
                cityLabel: "City:",
                postalCodeLabel: "Postal Code:",
                provinceLabel: "Province:",
                countryLabel: "Country:",
                securityEmailLabel: "Commissionnaire's Email:",
                securityEmailErrorMessage: "Must be a valid email address",
                bookingDelayLabel: "Reservation deadline (hours before test session)",
                otherDetailsLabel: "Other information:",
                accommodationsFriendly: "Test can be administered with accommodations",
                translationsPopup: {
                  address: {
                    title: "Translation - Address",
                    translationsButtonAriaLabel: "Edit Translation",
                    translationsButtonTooltip: "Edit Translation",
                    en_name: {
                      title: "Address in English:"
                    },
                    fr_name: {
                      title: "Address in French:"
                    }
                  },
                  city: {
                    title: "Translation - City",
                    translationsButtonAriaLabel: "Edit Translation",
                    translationsButtonTooltip: "Edit Translation",
                    en_name: {
                      title: "City in English:"
                    },
                    fr_name: {
                      title: "City in French:"
                    }
                  },
                  province: {
                    title: "Translation - Province",
                    translationsButtonAriaLabel: "Edit Translation",
                    translationsButtonTooltip: "Edit Translation",
                    en_name: {
                      title: "Province in English:"
                    },
                    fr_name: {
                      title: "Province in French:"
                    }
                  },
                  otherDetails: {
                    title: "Translation - Other Information",
                    translationsButtonAriaLabel: "Edit Translation",
                    translationsButtonTooltip: "Edit Translation",
                    en_name: {
                      title: "Other Details in English:"
                    },
                    fr_name: {
                      title: "Other Details in French:"
                    }
                  }
                }
              },
              testAdministrators: {
                title: "Test Administrators",
                addTestAdministratorButton: "Add a Test Administrator",
                addTestAdministratorPopup: {
                  title: "Add Test Administrators",
                  description:
                    "Add the test administrators you wish to associate with this test centre.",
                  testAdministratorsLabel: "Test Administrator(s):",
                  testAdministratorsCurrentValueAccessibility:
                    "The selected test administrators are:",
                  addTestAdministratorsButton: "Save"
                },
                table: {
                  columnOne: "First Name",
                  columnTwo: "Last Name",
                  columnThree: "Email",
                  columnFour: "Action",
                  deleteButtonTooltip: "Delete",
                  deleteButtonAccessibility:
                    "Delete {0} {1} from the list of associated test administrators.",
                  noData: "There are no test administrators at this test centre."
                },
                deleteTestAdministratorPopup: {
                  title: "Delete test administrator association",
                  systemMessageDescription:
                    "This action will remove {0} {1} from the list of associated test administrators.",
                  description: "Are you sure you want to proceed?"
                }
              },
              rooms: {
                title: "Rooms",
                addRoomButton: "Add Room",
                addRoomPopup: {
                  title: "Add Room",
                  description: "Provide the details of the room you wish to add.",
                  nameLabel: "Room Name:",
                  emailLabel: "Room Email:",
                  emailErrorMessage: "Must be a valid email",
                  maximumOccupancyLabel: "Maximum Occupancy:",
                  otherDetailsLabel: "Additional information:",
                  activeLabel: "Active:",
                  addRoomButton: "Add Room"
                },
                table: {
                  columnOne: "Room Name",
                  columnTwo: "Room Capacity",
                  columnThree: "Status",
                  columnFour: "Actions",
                  editButtonTooltip: "Edit",
                  editButtonAccessibility: "Edit Room {0}",
                  deleteButtonTooltip: "Delete",
                  deleteButtonAccessibility: "Delete Room {0}",
                  noData: "There are no rooms associated to this test centre."
                },
                editRoomPopup: {
                  title: "Edit Room",
                  titleSuccess: "Room Updated Successfully",
                  description: "Provide the details of the room you wish to edit.",
                  nameLabel: "Room Name:",
                  emailLabel: "Email:",
                  emailErrorMessage: "Must be a valid email",
                  maximumOccupancyLabel: "Maximum Occupancy:",
                  otherDetailsLabel: "Additional information:",
                  activeLabel: "Active:",
                  systemMessageDescription: "Room {0} has been updated successfully."
                },
                deleteRoomPopup: {
                  title: "Delete Room",
                  titleWithError: "Room in Use",
                  systemMessageDescription: "This action will remove room {0}.",
                  systemMessageDescriptionWithError:
                    "The room {0} cannot be deleted. It is currently in use for current or future test sessions.",
                  description: "Are you sure you want to proceed?"
                },
                translationsPopup: {
                  otherDetails: {
                    title: "Translation - Additional Information",
                    translationsButtonAriaLabel: "Edit Translation",
                    translationsButtonTooltip: "Edit Translation",
                    en_name: {
                      title: "Additional information in English"
                    },
                    fr_name: {
                      title: "Additional information in French"
                    }
                  }
                }
              }
            }
          },
          testSessions: {
            title: "Test Sessions",
            topTabs: {
              testSessionsData: {
                standardTitle: "Standard Test Sessions",
                nonStandardTitle: "Non-Standard Test Sessions",
                addTestSessionButton: "Add Test Session",
                addTestSessionPopup: {
                  title: "Add Test Session",
                  description: "Provide the details of the test session you wish to add.",
                  tabName: "Session {0}",
                  roomLabel: "Room:",
                  roomOptionalLabel: "Room (optional):",
                  roomCapacity: "Capacity:",
                  openToOgdLabel: "Open to other departments:",
                  dateLabel: "Date:",
                  startTimeLabel: "Start Time:",
                  endTimeLabel: "End Time:",
                  invalidStartEndTimeCombination: "The start time must be before the end time",
                  invalidStartDateAndTimeBasedOnBookingDelay:
                    "Start time is not respecting the booking delay",
                  spacesAvailableLabel: "Spaces Available:",
                  invalidSpacesAvailableError:
                    "The number of spaces cannot exceed the maximum occupancy of the selected room.",
                  testSkillType: "Test Type:",
                  testSkillSubType: "Test:",
                  test: "Test (as defined by the Assessment Accomodations Unit):",
                  testAssessor: "Assessor:",
                  addTestSessionButton: "Save",
                  deleteTestSessionTabPopup: {
                    title: "Delete Test Session",
                    systemMessageDescription: "The test session will be deleted.",
                    description: "Are you sure you want to delete this test session?"
                  },
                  invalidTestSessionDateAndTimeCombinationPopup: {
                    title: "Invalid Date and Time",
                    systemMessageDescription:
                      "Please make sure that all test sessions are in chronological order.",
                    description: "Close this popup and apply the required changes."
                  }
                },
                table: {
                  columnOne: "Test Session Date",
                  columnTwo: "Room",
                  columnThree: "Test",
                  columnFour: "Spaces Available",
                  columnFive: "Actions",
                  viewButtonTooltip: "View assessment process",
                  viewButtonAccessibility: "View list of candidates for room {0} on {1}",
                  editButtonTooltip: "Edit",
                  editButtonAccessibility: "Edit the {0} test session in room {1}.",
                  deleteButtonTooltip: "Delete",
                  deleteButtonAccessibility: "Delete the {0} test session in room {1}",
                  noData: "There are no Test Sessions"
                },
                viewAttendeesPopup: {
                  title: "List of candidates",
                  description1: "Test Session Date: {0}",
                  description2: "Room: {0}",
                  description3: "Test: {0}",
                  table: {
                    columnOne: "First Name",
                    columnTwo: "Last Name",
                    columnThree: "Email",
                    columnFour: "Account Email",
                    noData: "There are currently no candidates."
                  },
                  printButton: "Print",
                  printDocumentTitle: "Candidate list for test session {0}"
                },
                editTestSessionPopup: {
                  title: "Edit Test Session",
                  titleSuccess: "Test Session Updated Successfully",
                  description: "Provide the details of the test session you wish to modify.",
                  roomLabel: "Room:",
                  roomCapacity: "Capacity:",
                  openToOgdLabel: "Open to other departments:",
                  dateLabel: "Date:",
                  startTimeLabel: "Start Time:",
                  endTimeLabel: "End Time:",
                  invalidStartEndTimeCombination: "Start time must be before end time",
                  invalidStartDateAndTimeBasedOnBookingDelay:
                    "Start time is not respecting the reservation deadline",
                  spacesAvailableLabel: "Spaces Available:",
                  invalidSpacesAvailableError:
                    "This number cannot exceed the maximum occupancy of the selected room",
                  testSkillType: "Test Type:",
                  testSkillSubType: "Test:",
                  test: "Test (as defined by the Assessment Accomodations Unit):",
                  systemMessageDescription:
                    "The test session scheduled in room {0} on {1} has been updated successfully.",
                  systemMessageDescriptionNonStandard:
                    "The test session has been updated successfully."
                },
                deleteTestSessionPopup: {
                  title: "Delete Test Session",
                  systemMessageDescription:
                    "This action will remove the test session scheduled in room {0} on {1}.",
                  systemMessageDescriptionNonStandard:
                    "This action will remove all session(s) scheduled for that test.",
                  description: "Are you sure you want to proceed?"
                },
                timeOverlappingErrorPopup: {
                  title: "Schedule Conflict",
                  systemMessageDescription:
                    "The time you entered is conflicting with other test session(s).",
                  description: "Please close this popup and revise the desired time."
                }
              }
            }
          },
          accommodationRequests: {
            title: "Requests for accommodations",
            topTabs: {
              pendingRequests: {
                title: "Pending Requests",
                table: {
                  columnOne: "Test",
                  columnTwo: "Candidate",
                  columnThree: "Requesting Organization",
                  columnFour: "Request Date",
                  columnFive: "Closing Date",
                  columnSix: "Actions",
                  noData: "No pending requests",
                  viewButtonAccessibility: "View  request for {0} {1}",
                  viewButtonTooltip: "View accommodation request"
                }
              }
            }
          },
          ola: {
            title: "OLA",
            topTabs: {
              olaConfigs: {
                title: "OLA Settings",
                bookingDelayLabel: "Booking Delay (Hours):",
                advancedBookingDelayLabel: "Advanced Booking (Days):",
                savedConfirmationPopup: {
                  title: "OLA settings saved successfully !",
                  systemMessageDescription:
                    "The test center OLA settings have been saved successfully!"
                },
                addVacationBlockButton: "Vacation Block",
                addEditVacationBlockPopup: {
                  addTitle: "Add Vacation Block",
                  editTitle: "Edit Vacation Block",
                  addDescription: "Provide the details of the vacation block you wish to add.",
                  editDescription: "Edit the vacation block details.",
                  dateFromLabel: "Date From:",
                  dateToLabel: "Date To:",
                  invalidDateCombinationError: "Invalid dates",
                  enAvailabilityLabel: "EN Availability %",
                  frAvailabilityLabel: "FR Availability %",
                  vacationBlockAlreadyExistsError:
                    "This vacation block already exists in this test center",
                  rightButton: "Vacation Block"
                },
                vacationBlocksTable: {
                  dateRange: "Date Range",
                  enAvailability: "EN Availability %",
                  frAvailability: "FR Availability %",
                  actions: "Actions",
                  editButtonTooltip: "Edit",
                  editButtonTooltipAccessibility: "Edit {0} to {1} vacation block",
                  deleteButtonTooltipAccessibility: "Delete {0} to {1} vacation block",
                  noData: "No vacation blocks"
                },
                deleteVacationBlockConfirmationPopup: {
                  title: "Delete Vacation Block",
                  systemMessageDescription:
                    "Are you sure you want to delete the {0} {1} {2} vacation block?",
                  separator: "-"
                }
              },
              testAssessors: {
                title: "Assessors",
                addTestAssessorButton: "Assessor",
                addEditTestAssessorPopup: {
                  addTitle: "Add Assessor",
                  editTitle: "Test Assessor",
                  addDescription: "Provide the details of the test assessor you wish to add.",
                  editDescription: "Provide the details of the assessor you wish to edit.",
                  testAssessorLabel: "Assessor:",
                  languageCertificationLabel: "Language Certification:",
                  supervisorLabel: "Supervisor:",
                  rightButton: "Assessor"
                },
                deleteTestAssessorConfirmationPopup: {
                  title: "Delete Assessor",
                  systemMessageDescription: "Are you sure you want to delete the Assessor {0}?"
                },
                testAssessorsTable: {
                  assessor: "Assessors",
                  languageCertification: "Language Certification",
                  supervisor: "Supervisor",
                  actions: "Actions",
                  editButtonTooltip: "Edit",
                  editButtonTooltipAccessibility: "Edit the test assessor {0}",
                  deleteButtonTooltipAccessibility: "Delete the test assessor {0}",
                  noData: "No Assessors"
                }
              },
              timeSlots: {
                title: "Time Slots",
                addTimeSlotButton: "Time Slot",
                addEditTimeSlotPopup: {
                  addTitle: "Set Default Testing Periods",
                  editTitle: "Edit Time Slot",
                  addDescription: "Provide details for the settings.",
                  editDescription: "Edit the time slot details.",
                  dayOfWeekLabel: "Day:",
                  startTimeLabel: "Start Time:",
                  endTimeLabel: "End Time:",
                  startEndTimeCombinationError: "The start time must be before the end time",
                  overlappingTimeError:
                    "The time slot you are trying to add/edit is overlapping with another time slot in this test center",
                  languageToAssessLabel: "Language to Assess:",
                  assessorAvailabilityLabel: "% of availability:",
                  assessorAvailabilityTable: {
                    title: "Available Assessors ({0}/{1})",
                    column1: "Assessor",
                    column2: "Included in Availability",
                    noData: "No assessors added"
                  },
                  slotsToPrioritizeLabel: "Number of periods to prioritize:",
                  slotsToPrioritizeValueLabel: "({0}% buffer)",
                  slotsToPrioritizeError: "Must not exceed the number of available assessors",
                  prioritizationLabel: "Prioritization levels:",
                  prioritizationOptionLabel: "{0} [%]:",
                  prioritizationValueLabel: "({0} periods)",
                  prioritizationError: "Combined percentages must not exceed 100%",
                  rightButton: "Time Slot"
                },
                deleteTimeSlotConfirmationPopup: {
                  title: "Delete Time Slot",
                  systemMessageDescription:
                    "Are you sure you want to delete the {0}, from {1} to {2} time slot?"
                },
                timeSlotsTable: {
                  dayOfWeek: "Day",
                  timeOfDay: "Time",
                  assessedLanguage: "Assessed Language",
                  nbrOfSelectedAssessors: "Default number of assessors available",
                  actions: "Actions",
                  editButtonTooltip: "Edit",
                  editButtonTooltipAccessibility: "Edit {0}, from {1} to {2}",
                  deleteButtonTooltipAccessibility: "Delete {0}, from {1} to {2}",
                  noData: "There are currently no time slots at the moment"
                }
              }
            }
          }
        },
        backToTestCenterManagement: {
          buttonTitle: "Back to Test Centre Administration",
          popup: {
            title: "Back to Test Centre Management?",
            warningDescription: "All unsaved data will be lost.",
            description: "Do you want to continue?"
          }
        }
      },
      ola: {
        lookAhead: {
          navTitle: "Look Ahead"
        },
        unavailability: {
          navTitle: "Unavailability",
          title: "Unavailability",
          description: "Manage test centre unavailability."
        },
        reports: {
          navTitle: "Reports"
        }
      }
    },

    // Report Generator Component
    reports: {
      reportTypeLabel: "Report Type:",
      reportTypes: {
        individualScoreSheet: "Individual Test Result",
        resultsReport: "Group Test Results",
        financialReport: "Financial Report",
        financialReportFromDateLabel: "From",
        financialReportToDateLabel: "To",
        testContentReport: "Test Content Report",
        testTakerReport: "Candidate Test Information Report",
        candidateActionsReport: "Candidate Actions Report",
        taHistoryReport: "Test Administrator History Report",
        adaptedTestsReport: "Adapted Tests Report",
        adaptedTestsReportFromDateLabel: "From",
        adaptedTestsReportToDateLabel: "To",
        assessmentProcessResultsReport: "Assessment Process Results Report"
      },
      testOrderNumberLabel: "Test Order / Reference Number:",
      testLabel: "Test:",
      testsLabel: "Test(s):",
      testLabelAccessibility: "The selected tests are:",
      candidateLabel: "Candidate:",
      dateFromLabel: "From:",
      dateToLabel: "To:",
      invalidDate: "Must be a valid Date",
      dateError: "The first date must be before or the same as the second date",
      parentCodeLabel: "Test Number:",
      testCodeLabel: "Test Form:",
      testVersionLabel: "v:",
      generateButton: "Generate Report",
      noDataPopup: {
        title: "No Results ",
        description: "No results found with parameters provided."
      },
      testStatus: {
        assigned: "CHECKED-IN",
        ready: "READY",
        preTest: "PRE-TEST",
        active: "TESTING",
        transition: "IN TRANSITION",
        locked: "LOCKED",
        paused: "PAUSED",
        quit: "QUIT",
        submitted: "SUBMITTED",
        unassigned: "UNASSIGNED"
      },
      timedOut: "Timed Out",
      orderlessRequest: "Orderless",
      individualScoreSheet: {
        personID: "Person ID",
        emailAddress: "Email Address",
        name: "Name",
        pri: "PRI",
        dateOfBirth: "Date of Birth",
        militaryNbr: "Service Number (Military)",
        orderNumber: "Order No.",
        assessmentProcessOrReferenceNb: "Assessment Process / Reference Number",
        test: "Test",
        testDescriptionFR: "Test Description (FR)",
        testDescriptionEN: "Test Description (EN)",
        testStartDate: "Test Start Date",
        score: "Score",
        level: "Level",
        testStatus: "Test Status"
      },
      resultsReport: {
        lastname: "Last Name",
        firstname: "First Name",
        emailAddress: "Email Address",
        uitInvitationEmail: "UIT Invitation Email",
        pri: "PRI",
        dateOfBirth: "Date of Birth",
        militaryNbr: "Service Number (Military)",
        orderNumber: "Order No.",
        assessmentProcessOrReferenceNb: "Assessment Process / Reference Number",
        test: "Test",
        testDescriptionFR: "Test Description (FR)",
        testDescriptionEN: "Test Description (EN)",
        testDate: "Test Date",
        score: "Score",
        level: "Level",
        testStatus: "Test Status"
      },
      financialReport: {
        candidateLastName: "Last Name",
        candidateFirstName: "First Name",
        candidatePri: "PRI",
        candidateMilitaryNbr: "Service Number (Military)",
        taWorkEmail: "Test Administrator's Work Email",
        taOrg: "Test Administrator's Organization",
        requestingDepartment: "Requesting Departement",
        orderNumber: "Order No.",
        assessmentProcess: "Assessment Process / Reference Number",
        orgCode: "Organization Code",
        fisOrgCode: "FIS Organization Code",
        fisRefCode: "FIS Reference Code",
        billingContactName: "Contact Email for Candidates",
        testStatus: "Test Status",
        isInvalid: "Is Invalid",
        testDate: "Test Date",
        testVersion: "Test Version",
        testDescriptionFR: "Test Description (FR)",
        testDescriptionEN: "Test Description (EN)"
      },
      testContentReport: {
        questionSectionNumber: "Question Section No.",
        questionSectionTitleFR: "Question Section Title (FR)",
        questionSectionTitleEN: "Question Section Title (EN)",
        rdItemID: "R&D Item ID",
        questionNumber: "Question No.",
        questionTextFR: "Item Text (FR)",
        questionTextFrHTML: "Item Text HTML (FR)",
        questionTextEN: "Item Text (EN)",
        questionTextEnHTML: "Item Text HTML (EN)",
        answerChoiceID: "Choice {0} - ID",
        answerChoiceTextFR: "Choice {0} - Text (FR)",
        answerChoiceTextFrHTML: "Choice {0} - Text HTML (FR)",
        answerChoiceTextEN: "Choice {0} - Text (EN)",
        answerChoiceTextEnHTML: "Choice {0} - Text HTML (EN)",
        scoringValue: "Choice {0} - Value",
        sampleTest: "Sample Test"
      },
      testTakerReport: {
        selectedLanguage: "en",
        descSelectedLanguage: "edesc",
        abrvSelectedLanguage: "eabrv",
        userCode: "User code",
        username: "Username",
        firstname: "First Name",
        lastname: "Last Name",
        pri: "PRI",
        militaryNbr: "Service number",
        dateOfBirth: "Date of Birth",
        currentEmployer: "Current Employer",
        org: "Organization",
        group: "Occupational Group",
        level: "Occupational Level",
        residence: "Residence",
        education: "Education",
        gender: "Gender",
        identifyAsWoman: "Woman",
        aboriginal: "Indigenous",
        aboriginalInuit: "Indigenous - Inuit",
        aboriginalMetis: "Indigenous - Métis",
        aboriginalNAIndianFirstNation: "Indigenous - NA Indian/First Nation",
        visibleMinority: "Visible Minority",
        visibleMinorityBlack: "VM - Black",
        visibleMinorityChinese: "VM - Chinese",
        visibleMinorityFilipino: "VM - Filipino",
        visibleMinorityJapanese: "VM - Japanese",
        visibleMinorityKorean: "VM - Korean",
        visibleMinorityNonWhiteLatinx: "VM - Non-white Latinx",
        visibleMinorityNonWhiteMiddleEastern: "VM - Non-white Middle Eastern",
        visibleMinorityMixedOrigin: "VM - Mixed Origin",
        visibleMinoritySouthAsianEastIndian: "VM - South Asian/East Indian",
        visibleMinoritySoutheastAsian: "VM - Southeast Asian",
        visibleMinorityOther: "VM - Other",
        visibleMinorityOtherDetails: "VM - Other (details)",
        disability: "Disability",
        disabilityVisual: "Disability - Visual",
        disabilityDexterity: "Disability - Dexterity",
        disabilityHearing: "Disability - Hearing",
        disabilityMobility: "Disability - Mobility",
        disabilitySpeech: "Disability - Speech",
        disabilityOther: "Disability - Other",
        disabilityOtherDetails: "Disability - Other (details)",
        requestingDepartment: "Requesting Organization",
        requestingDepartmentLanguage: "eabrv",
        administrationMode: "Administration Mode",
        assisted: "Assisted",
        automated: "Automated",
        reasonForTesting: "Reason for Testing",
        levelRequired: "Required Level",
        testNumber: "Test Number",
        testSectionLanguage: "Test Section Language",
        testForm: "Test Form",
        catVersion: "CAT Version",
        testDate: "Test Date",
        testStartDate: "Test Start Date",
        submitTestDate: "Submit Test Date",
        questionSectionNumber: "Question Section No.",
        rdItemID: "R&D Item ID",
        pilotItem: "Pilot Item",
        itemOrderMaster: "Item Order - Master",
        itemOrderPresentation: "Item Order - Presentation",
        itemOrderViewed: "Item Order - View",
        questionTextFr: "Item Text (FR)",
        questionTextEn: "Item Text (EN)",
        questionTextHtmlFr: "Item Text HTML (FR)",
        questionTextHtmlEn: "Item Text HTML (EN)",
        choiceXPpcAnswerId: "Choice {0} - ID",
        choiceXPoints: "Choice {0} - Value",
        choiceXPresentation: "Choice {0} - Presentation",
        choiceXTextFr: "Choice {0} - Text (FR)",
        choiceXTextEn: "Choice {0} - Text (EN)",
        choiceXTextHtmlFr: "Choice {0} - Text HTML (FR)",
        choiceXTextHtmlEn: "Choice {0} - Text HTML (EN)",
        languageOfAnswer: "Language of Answer",
        timeSpent: "Total Time Spent on Item (s)",
        responseMasterOrder: "Response",
        responseChoiceId: "Response - CAT Choice ID",
        scoringValue: "Item Score",
        sectionScore: "Section Score",
        totalScore: "Total Score",
        convertedScore: "Level",
        testStatus: "Test Status",
        testSubmissionMode: "Section Submission Mode",
        scoreValidity: "Result Validity"
      },
      candidateActionsReport: {
        multiAssignedTestsSelectionOptions: {
          label: "Test Session:",
          startDate: "Test Date:",
          submitDate: "Submit Date:",
          modifyDate: "Modified Date:"
        },
        origin: "Entry Type",
        historyDate: "Entry Date",
        historyType: "Initial or Modified Entry",
        status: "Test Status",
        previousStatus: "Previous Status",
        startDate: "Test Date",
        submitDate: "Submit Test Date",
        testAccessCode: "Test Access Code",
        totalScore: "Total Score",
        testAdministrator: "Test Administrator",
        testSessionLanguage: "Test Session Language",
        enConvertedScore: "Level (EN)",
        frConvertedScore: "Level (FR)",
        uitInviteId: "UIT Invite ID",
        isInvalid: "Score Validity",
        timeType: "Section Entry or Exit",
        assignedTestSectionId: "Test Section Name",
        candidateAnswerId: "Candidate Item Session ID",
        markForReview: "Marked for Review",
        questionId: "CAT Item ID",
        selectedLanguage: "Interface Language",
        answerId: "Response - CAT Choice ID",
        candidateAnswerIdRef: "Candidate Item Session ID (Ref.)"
      },
      adaptedTestsReport: {
        candidateLastName: "Last Name",
        candidateFirstName: "First Name",
        requestingOrganization: "Requesting Organization",
        taWorkEmail: "TA’s Work Email",
        taLastName: "TA’s Last Name",
        taFirstName: "TA’s First Name",
        taOrganization: "TA's Organization",
        orderNumber: "Order No.",
        referenceNumber: "Assessment Process / Reference Number",
        testStatus: "Test Status",
        isInvalid: "Is Invalid",
        testStartDateAndTime: "Test Start Date and Time",
        testSubmitDateAndTime: "Test Submit Date and Time",
        testVersion: "Test Version",
        testDescriptionFr: "Test Description (FR)",
        testDescriptionEn: "Test Description (EN)",
        totalTimeAllotted: "Total Time Allotted",
        totalTimeUsed: "Total Time Used",
        breakBankAllotted: "Break Bank Allotted",
        breakBankUsed: "Break Time Used"
      },
      itemBankReport: {
        name: "Item Bank Content Report - {0} [{1}]",
        selectedLanguage: "en",
        systemId: "System ID",
        version: "Version",
        historicalId: "R&D Item ID",
        itemStatus: "Item Status",
        scoringKey: "Scoring Key",
        scoringKeyId: "Scoring Key ID",
        stemId: "Stem {0} - ID",
        stemTextEn: "Stem {0} - Text (EN)",
        stemTextHtmlEn: "Stem {0} - Text HTML (EN)",
        stemTextFr: "Stem {0} - Text (FR)",
        stemTextHtmlFr: "Stem {0} - Text HTML (FR)",
        stemScreenReaderId: "Stem {0} - Screen Reader ID",
        stemScreenReaderTextEn: "Stem {0} - Screen Reader Text (EN)",
        stemScreenReaderTextHtmlEn: "Stem {0} - Screen Reader Text HTML (EN)",
        stemScreenReaderTextFr: "Stem {0} - Screen Reader Text (FR)",
        stemScreenReaderTextHtmlFr: "Stem {0} - Screen Reader Text HTML (FR)",
        optionHistoricalId: "Choice {0} - ID",
        optionTextEn: "Choice {0} - Text (EN)",
        optionTextHtmlEn: "Choice {0} - Text HTML (EN)",
        optionTextFr: "Choice {0} - Text (FR)",
        optionTextHtmlFr: "Choice {0} - Text HTML (FR)",
        optionScore: "Choice {0} - Value",
        optionScreenReaderHistoricalId: "Choice {0} - Screen Reader ID",
        optionScreenReaderTextEn: "Choice {0} - Screen Reader Text (EN)",
        optionScreenReaderTextHtmlEn: "Choice {0} - Screen Reader Text HTML (EN)",
        optionScreenReaderTextFr: "Choice {0} - Screen Reader Text (FR)",
        optionScreenReaderTextHtmlFr: "Choice {0} - Screen Reader Text HTML (FR)",
        optionScreenReaderScore: "Choice {0} - Screen Reader Value",
        itemType: "Item Type"
      },
      taHistoryReport: {
        assignedTest: "Assigned Test ID",
        is_invalid: "Is Invalid",
        ta: "Test Administrator (TA) Name",
        pri: "PRI",
        priTooltip: "Personal Record Identifier",
        military_number: "Military Service Number",
        militaryNbrTooltip: "Military Service Number",
        ta_username: "TA Account Email",
        ta_email: "TA Account Email",
        goc_email: "Work Email",
        requesting_organization: "Requesting Organization",
        test: "Test",
        test_submit_date: "Test Submit Date",
        candidate_name: "Candidate Name",
        candidate_username: "Candidate Account Email"
      },
      assessmentProcessResultsReport: {
        firstName: "First Name",
        lastName: "Last Name",
        email: "Email Address",
        username: "Username",
        invitationEmail: "Invitation Email",
        pri: "PRI",
        militaryNbr: "Service Number (Military)",
        assessmentProcess: "Assessment Process / Reference Number",
        testDefinition: "Test",
        testDefinitionEnName: "Test Description (EN)",
        testDefinitionFrName: "Test Description (FR)",
        testDate: "Test Date",
        score: "Score",
        level: "Level",
        testStatus: "Test Status",
        noDataPopup: {
          title: "No Result ",
          description: "None of the candidates have taken a test yet."
        }
      }
    },

    // DatePicker Component
    datePicker: {
      dayField: "Day",
      dayFieldSelected: "Day field selected",
      monthField: "Month",
      monthFieldSelected: "Month field selected",
      yearField: "Year",
      yearFieldSelected: "Year field selected",
      hourField: "Hour",
      hourFieldSelected: "Hour field selected",
      minuteField: "Minute",
      minuteFieldSelected: "Minute field selected",
      currentValue: "The current value is:",
      none: "Select",
      datePickedError: "Must be a valid Date",
      futureDatePickedError: "Must be a future date",
      futureDatePickedIncludingTodayError: "Cannot be earlier than today's date",
      comboStartEndDatesPickedError: "Start date must be before end date",
      comboFromToDatesPickedError: "From date must be before To date",
      deselectOption: "(unchanged)"
    },

    // PrivacyNoticeStatement Component
    privacyNoticeStatement: {
      title: "Privacy Notice Statement",
      descriptionTitle: "Privacy Notice Statement",
      paragraph1:
        "Personal information is used to provide assessment services to clients of the Personnel Psychology Centre. It is collected for staffing related purposes, in accordance with sections 11, 30 and 36 of the {0}. For organizations not subject to this act, personal information is collected under the authority of that organization’s enabling statute and section 35 of the {1}. Second language evaluation results will be disclosed to authorized organizational officials. Results of all other tests will be disclosed only to the requesting organization. Test results may be disclosed to the Investigations Directorate of the Public Service Commission if an investigation is conducted pursuant to section 66 or 69 of the {2}. Your information may also be used for statistical and analytical research purposes. In some cases, information may be disclosed without your consent, pursuant to section 8(2) of the {3}. Provision of your personal information is voluntary, but if you choose not to provide your information, you may not be able to receive Personnel Psychology Centre services.",
      paragraph2:
        "The information is collected and used as described in the Personnel Psychology Centre’s (PSC PCU 025) PIB, found in the Public Service Commission’s {0}.",
      paragraph3:
        "You have the right to access and correct your personal information, and to request corrections where you believe there is an error or omission. You also have the right to file a complaint to the {0} regarding the handling of your personal information.",
      publicServiceEmploymentActLinkTitle: "Public Service Employment Act",
      publicServiceEmploymentActLink: "https://laws-lois.justice.gc.ca/eng/acts/p-33.01/",
      privacyActLinkTitle: "PA",
      privacyActLink: "https://laws-lois.justice.gc.ca/eng/acts/p-21/",
      infoSourceLinkTitle: "Info Source",
      infoSourceLink:
        "https://www.canada.ca/en/public-service-commission/corporate/about-us/access-information-privacy-office/info-source-sources-federal-government-employee-information.html#122",
      privacyCommissionerOfCanadaLinkTitle: "Privacy Commissioner of Canada",
      privacyCommissionerOfCanadaLink: "https://www.priv.gc.ca/en/"
    },

    // DropdownSelect Component
    dropdownSelect: {
      pleaseSelect: "Select",
      noOptions: "No Options"
    },

    // SearchBarWithDisplayOptions component
    SearchBarWithDisplayOptions: {
      searchBarTitle: "Search:",
      resultsFound: "{0} result(s)",
      clearSearch: "Clear Search",
      noResultsFound: "There are no results found based on your search.",
      displayOptionLabel: "Display:",
      displayOptionAccessibility: "Number of results per page",
      displayOptionCurrentValueAccessibility: "The selected value is:"
    },

    // Profile Page
    profile: {
      completeProfilePrompt: "To update your profile, complete required fields and click Save.",
      title: "Welcome, {0} {1}.",
      sideNavItems: {
        personalInfo: "Personal Information",
        accommodations: "Assessment Accommodations Profile",
        password: "Password",
        preferences: "Preferences",
        permissions: "Rights and Permissions",
        profileMerge: "Merge Accounts"
      },
      personalInfo: {
        title: "My Personal Information",
        nameSection: {
          firstName: "First Name:",
          lastName: "Last Name:"
        },
        usernameSection: {
          username: "Username:",
          usernameError: "Must be a valid username",
          usernameAlreadyAssociatedError: "This username has already been used"
        },
        emailAddressesSection: {
          primary: "Primary Email Address:",
          secondary: "Secondary Email Address (optional):",
          emailError: "Must be a valid Email Address",
          emailAlreadyAssociatedError: "An account is already associated to this email address"
        },
        dateOfBirth: {
          title: "Date of Birth:",
          titleTooltip:
            "Adding your date of birth will facilitate the consolidation of all your results in your test result record."
        },
        phoneNumber: {
          title: "Cell (optional):",
          titleTooltip:
            "Adding your phone number will enable the system to send you notifications when the feature becomes available.",
          phoneNumberError: "Must be a valid phone number"
        },
        pri: {
          title: "PRI (optional):",
          titleTooltip:
            "Adding your PRI will facilitate the consolidation of all your results in your test result record.",
          priError: "Must be a valid PRI"
        },
        militaryNbr: {
          title: "Service Number (optional):",
          titleTooltip:
            "Adding your service number will facilitate the consolidation of all your results in your test result record.",
          militaryNbrError: "Must be a valid Service Number"
        },
        priMilitaryNbrError: "A PRI or a Military Service Number is required.",
        psrsAppId: {
          title: "PSRS Applicant ID (optional):",
          psrsAppIdError: "Must be a valid PSRS Applicant ID"
        },
        optionalField: "(optional)",
        saveConfirmationPopup: {
          title: "Personal Information Updated",
          description: "Your personal information has been saved successfully."
        },
        profileChangeRequest: {
          tooltip: "Modify Information",
          existingRequestTooltip: "View modification request",
          requestPopup: {
            title: "Modification Request",
            successTitle: "Request submitted successfully",
            deleteTitle: "Discard?",
            description:
              "Any changes to names and dates of birth will be reviewed before it can be saved to your account due to the need to reflect the required changes in the test results database.",
            successDescription: "Your request has been sent successfully",
            current: "Current",
            new: "New",
            firstNameLabel: "First Name:",
            firstNameError: "Must be a valid First Name",
            lastNameLabel: "Last Name:",
            lastNameError: "Must be a valid Last Name",
            dobLabel: "Date of Birth:",
            comments: "Comments (optional):",
            deleteConfirmationSystemMessage:
              "This modification request will be cancelled and your profile will remain unchanged",
            deleteConfirmationDescription: "Are you sure you want to proceed?"
          }
        }
      },
      eeInformation: {
        title: "Employment Equity",
        description1:
          "The Public Service Commission (PSC) needs your help to ensure its assessments are working as intended. The personal information you share below will be accessible exclusively to the PSC and solely used for research, analysis, and test development purposes. Participation is voluntary: you can select the option “Prefer not to say” for any questions below. The information you provide will not be shared with human resource personnel or those responsible for hiring decisions.",
        description2:
          "The PSC is committed to protecting the privacy rights of individuals. Here is our {0}.",
        privacyNoticeLink: "Privacy Notice",
        identifyAsWoman: "Do you identify as a woman?",
        invalidIdentifyAsWomanErrorMessage: "Please select a response option",
        aboriginal: "Are you an Indigenous person?",
        invalidAboriginalErrorMessage: "Please select a response option",
        subAboriginal: "Please select all that apply:",
        invalidSubAboriginalErrorMessage: "Please select at least one option",
        visibleMinority: "Are you a member of a visible minority group?",
        invalidVisibleMinorityErrorMessage: "Please select a response option",
        subVisibleMinority: "Please select all that apply:",
        subVisibleMinorityOtherTextAreaLabel: "If other, please specify:",
        invalidSubVisibleMinorityErrorMessage: "Please select at least one option",
        disability: "Are you a person with a disability?",
        invalidDisabilityErrorMessage: "Please select a response option",
        subDisability: "Please select all that apply:",
        subDisabilityOtherTextAreaLabel: "If other, please specify:",
        invalidSubDisabilityErrorMessage: "Please select at least one option"
      },
      additionalInfo: {
        title: "Additional Information (Optional)",
        current_employer: {
          title: "Current Employer:"
        },
        organization: {
          title: "Organization:"
        },
        group: {
          title: "Group:"
        },
        subclassification: {
          title: "Subgroup:"
        },
        level: {
          title: "Level:"
        },
        residence: {
          title: "Residence:"
        },
        education: {
          title: "Education:"
        },
        gender: {
          title: "Gender:"
        }
      },
      accommodations: {
        title: "Accessible Assessment Profile",
        description:
          "Please complete your profile by answering the questions below. Your responses will help improve accessibility for assessments and remove or mitigate any barriers you may encounter. Answer the questions as best as you can. An assessment accommodation specialist may reach out to you for more information or clarification.",
        builtInAccessibility: {
          title:
            "1. Display options (online tests only) - available on the Candidate Assessment Tool Testing Platform (CAT)",
          hasBuiltInAccessibilityQuestion:
            "When taking an online test, I require display options that are different than those available in the built-in accessibility features in CAT (such as font type and size, text spacing, zoom and/or colour).",
          pleaseDescribe:
            "Yes, please describe what barriers are not being met by the platform’s built-in accessibility features:",
          builtInAccessibilityError: "You must provide a description"
        },
        extraTime: {
          title: "2. Extra Time",
          needsExtraTimeQuestion: "I typically require extra time when completing a test.",
          pleaseDescribe:
            "If yes, please identify which barriers extra time addresses and how much time you usually need during a 1-hour testing session:",
          extraTimeError: "You must provide a description"
        },
        breaks: {
          title: "3. Breaks",
          needsBreaksQuestion:
            "I require breaks to fully demonstrate the capacities being assessed.",
          pleaseDescribe:
            "If yes, please indicate which barriers breaks address and how much break time do you usually need during a 1-hour testing session:",
          breaksError: "You must provide a description"
        },
        adaptiveTech: {
          title: "4. Technology",
          needsAdaptiveTechQuestion:
            "I require the use of adaptive technology and/or other technological supports when completing a test (e.g., speech-to-text).",
          pleaseDescribe: "Yes (please describe):",
          adaptiveTechError: "You must provide a description"
        },
        ergonomic: {
          title: "5. Ergonomics (in-person test only)",
          needsErgonomicQuestion:
            "I require an adapted seat or ergonomic devices (e.g. mouse, keyboard, chair) when completing a test.",
          pleaseDescribe: "Yes (please describe):",
          ergonomicError: "You must provide a description"
        },
        accessAssistance: {
          title: "6. Access to the testing site or room (in-person test only)",
          needsAccessAssistanceQuestion: "I require assistance to access the testing site or room.",
          pleaseDescribe: "Yes (please describe):",
          accessAssistanceError: "You must provide a description"
        },
        environment: {
          title: "7. Environment (in-person test only)",
          needsEnvironmentQuestion:
            "I have specific needs   related to the testing environment (e.g., lighting).",
          pleaseDescribe: "Yes (please describe):",
          environmentError: "You must provide a description"
        },
        schedulingAdaptation: {
          title: "8. Scheduling",
          needsSchedulingAdaptationQuestion:
            "I require a specific time-of-day to complete a test (e.g., morning).",
          pleaseDescribe: "Yes (please describe):",
          schedulingAdaptationError: "You must provide a description"
        },
        otherNeedsWritten: {
          title: "9. Written tests (online or in person)",
          hasOtherNeedsWrittenQuestion: "I have experienced other barriers during written tests.",
          pleaseDescribe: "Yes, please describe these other barriers.",
          otherNeedsWrittenError: "You must provide a description"
        },
        otherNeedsOral: {
          title: "10. Oral tests",
          hasOtherNeedsOralQuestion: "I have experienced barriers when completing oral tests.",
          pleaseDescribe: "Yes, please describe which barrier(s) you have encountered.",
          otherNeedsOralError: "You must provide a description"
        },
        repeatedOralQuestions: {
          title: "11. Repetition of questions during an oral test",
          needsRepeatedOralQuestionsQuestion:
            "I usually require oral questions to be repeated during a test.",
          pleaseDescribe:
            "Yes, please describe which barriers repeating the questions helps to address",
          repeatedOralQuestionsError: "You must provide a description"
        },
        previouslyAccommodated: {
          title: "12. Previously Accommodated",
          previouslyAccommodatedQuestion:
            "I have received assessment accommodations in other testing contexts such as school, post-secondary studies, or work.",
          pleaseDescribe:
            "Yes, please describe the assessment accommodations previously obtained in standardized assessment settings.",
          previouslyAccommodatedError: "You must provide a description"
        },
        privacyNotice:
          "I have read the {0} and I accept the manner in which the Public Service Commission collects, uses and discloses personal information.",
        privacyNoticeLink: "Privacy Notice",
        understandAccommodationsMesures:
          "I understand that the accommodation measures above are subject to review by the assessment accommodations team.",
        successPopup: {
          title: "Accommodations Profile Updated",
          description: "Your accommodations profile has been saved successfully."
        },
        errorPopup: {
          title: "Error - Saving User Accommodations Profile",
          description:
            "There has been an error when saving your user accommodations profile. Please try again."
        },
        placeholder: "Click or tap here to enter text."
      },
      password: {
        newPassword: {
          title: "My Password",
          updatedDate: "Your password was last updated on: {0}",
          updatedDateNever: "never",
          currentPassword: "Current Password:",
          showCurrentPassword: "Show Current Password",
          newPassword: "New Password:",
          showNewPassword: "Show New Password",
          confirmPassword: "Confirm Password:",
          showConfirmPassword: "Show Password Confirmation",
          invalidPasswordError: "Invalid password",
          popup: {
            title: "Password Updated",
            description: "Your password has been updated successfully."
          }
        },
        passwordRecovery: {
          title: "Password Recovery",
          secretQuestion: "Secret Question:",
          secretAnswer: "Secret Answer:",
          secretQuestionUpdatedConfirmation: "Your secret question has been updated successfully"
        }
      },
      preferences: {
        title: "My Preferences",
        description: "Modify my preferences.",
        notifications: {
          title: "Notifications",
          checkBoxOne: "Always send notifications to my primary email address",
          checkBoxTwo: "Always send notifications to my secondary email address"
        },
        display: {
          title: "Display",
          checkBoxOne: "Allow others to see my profile picture",
          checkBoxTwo: "Hide Tooltip icons"
        },
        accessibility: {
          title: "Accessibility:",
          description: "Adjust the font, font size, and text spacing to be displayed:"
        }
      },
      permissions: {
        title: "My Rights and Permissions",
        description: "You do not need additional rights and permissions to take tests.",
        systemPermissionInformation: {
          title: "Information about your user role",
          addPermission: "Obtain Rights and Permissions",
          pending: "Pending",
          superUser: {
            title: "Super User",
            permission: "User has full system access and permissions"
          }
        },
        addPermissionPopup: {
          title: "Rights and Permissions Request",
          description:
            "Please fill out the following information and select the requested user role(s).",
          gocEmail: "Government of Canada Email Address:",
          gocEmailTooltip: "You must enter your Government of Canada email address to register.",
          gocEmailError: "Must be a valid Email Address",
          pri: "PRI:",
          militaryNbr: "Service Number:",
          priError: "Must be a valid PRI",
          militaryNbrError: "Must be a valid Service Number",
          priMilitaryNbrError: "A PRI or a Military Service Number is required.",
          supervisor: "Supervisor Name:",
          supervisorError: "The name contains one or more invalid characters",
          supervisorEmail: "Supervisor Email Address:",
          supervisorEmailError: "Must be a valid Email Address",
          rationale: "Reason for the request:",
          rationaleError: "Must provide a reason for this request",
          permissions: "Obtain permissions for the following user roles:",
          permissionsError: "Must select at least one user role"
        },
        noPermissions: "You have no permissions."
      },
      testPermissions: {
        title: "Test Access Permissions",
        description: "As a Test Administrator, you have access to administer the following tests:",
        table: {
          title: "Test Accesses",
          column1: "Test Form",
          column1OrderItem: "Order by test version",
          column2: "Test Order Number",
          column2OrderItem: "Order by test order number",
          column3: "Assessment Process Number",
          column3OrderItem: "Order by assessment process number",
          column4: "Expiry Date",
          column4OrderItem: "Order by expiry date"
        }
      },
      saveButton: "Save"
    },

    // ETTA Page
    systemAdministrator: {
      title: "Welcome, {0} {1}.",
      containerLabelEtta: "Business Operations",
      containerLabelRD: "R&D Operations",
      containerLabelBoth: "Operations",
      sideNavItems: {
        dashboard: "Dashboard",
        activeTests: "Active Tests",
        testAccessCodes: "Test Access Codes",
        permissions: "Rights and Permissions",
        itemBanks: "Item Bank Administration",
        testCenters: "Test Center Administration",
        testAccesses: "Test Access Management",
        incidentReports: "Incident Reports",
        scoring: "Scoring",
        reScoring: "Re-Scoring",
        alternateTestRequests: "Alternate Test Requests",
        reports: "Reports",
        userLookUp: "User Look-Up",
        userProfileChangeRequests: "User Profile Modification Requests",
        systemAlert: "System Alerts",
        olaConfigs: "OLA Settings"
      },
      permissions: {
        description: "Manage and assign rights and permissions to users.",
        tabs: {
          permissionRequests: {
            title: "Rights and Permissions Requests",
            table: {
              columnOne: "User Role",
              columnTwo: "User Name and Contact Details",
              columnThree: "Request Date",
              columnFour: "Action",
              noPermission: "There are no rights or permissions requests at the moment."
            },
            viewButton: "View",
            viewButtonAccessibility: "View {0} {1}",
            popup: {
              title: "Rights and Permissions Request",
              description:
                "Please approve or deny the request for rights and permissions made by {0}.",
              permissionRequested: "Obtain rights for the following user role:",
              deniedReason: "Reason for denial of request:",
              denyButton: "Deny",
              grantButton: "Approve",
              grantPermissionErrors: {
                notEmptyDeniedReasonError:
                  "The field must be empty if you want to approve this request",
                missingDeniedReasonError: "You must enter a reason to deny the permission",
                usernameDoesNotExistError: "This username no longer exists"
              }
            }
          },
          activePermissions: {
            title: "Active Rights and Permissions",
            table: {
              permission: "User Role",
              user: "User Name and Contact Details",
              action: "Action",
              actionButtonLabel: "Modify",
              actionButtonAriaLabel: "Modify {0} {1}"
            },
            viewEditDetailsPopup: {
              title: "Modify or Delete User Roles",
              description: "Modify or delete {0}'s rights and permissions.",
              deleteButton: "Delete",
              saveButton: "Modify",
              permission: "User Role:",
              reasonForModification: "Reason for modification or deletion:",
              reasonForModificationError:
                "You must provide a reason for modifying or deleting this request"
            },
            deletePermissionConfirmationPopup: {
              title: "Confirm deletion of rights and permissions",
              systemMessageDescription:
                "This action will revoke {0} permission from {1}'s account. Are you sure you want to proceed?"
            },
            updatePermissionDataConfirmationPopup: {
              title: "Confirmation of updates",
              description: "{0}'s rights and permissions have been updated successfully."
            }
          }
        }
      },
      itemBanks: {
        title: "Item Bank Administration",
        description: "Create item banks and grant bank accesses to test developers.",
        tabs: {
          activeItemBanks: {
            title: "Active Item Banks",
            newItemBankButton: "New Item Bank",
            table: {
              column1: "Item Bank",
              column2: "Last Modified",
              column3: "Organization",
              column4: "Actions",
              noData: "There are no results found based on your search"
            },
            newItemBankPopup: {
              title: "Create Item Bank",
              description: "Complete the following fields to create a new item bank.",
              itemBankDepartment: "Owner organization:",
              itemBankPrefix: "Item bank prefix:",
              itemBankLanguage: "Item language:",
              itemBankComments: "Comments (optional):",
              rightButton: "Create",
              successMessage: "The new item bank has been created.",
              itemBankAlreadyExistsError: "This prefix is already used by another item bank."
            },
            selectedItemBank: {
              backToRdOperationsButton: "Back to Item Bank Administration",
              backToRdOperationsPopup: {
                title: "Return to Item Bank Administration?",
                warningDescription: "All unsaved data will be lost.",
                description: "Do you want to continue?"
              },
              tabs: {
                selectedItemBank: {
                  title: "Item Bank {0}",
                  sideNavigationItems: {
                    ItemBankAccesses: {
                      ItemTitle: "Item Bank Accesses",
                      title: "Item Bank: {0}",
                      description: "Manage test developer accesses to this item bank.",
                      itemBankPrefix: "Item bank prefix:",
                      itemBankAlreadyExistsError:
                        "This prefix is already used by another item bank.",
                      itemBankNameEn: "English name of item bank:",
                      itemBankNameFr: "French name of item bank:",
                      itemBankDepartment: "Owner organization:",
                      itemBankLanguage: "Item language:",
                      addTestDeveloperButton: "Add test developer",
                      addTestDeveloperPopup: {
                        title: "Add Test Developer to Item Bank {0}",
                        description: "Select a test developer to add to the item bank.",
                        testDeveloperLabel: "Test developer:",
                        addButton: "Test developer"
                      },
                      deleteTestDeveloperPopup: {
                        title: "This action will delete the item bank accesses of {0}.",
                        description: "This action will delete the item bank accesses of {0}.",
                        deleteButton: "Delete"
                      },
                      table: {
                        column1: "Test Developer",
                        column2: "Access Type",
                        column3: "Actions",
                        deleteButtonTooltip: "Delete",
                        deleteButtonAriaLabel: "Delete access to this item bank for {0}",
                        noData: "There are no test developers associated with this item bank."
                      }
                    }
                  }
                }
              }
            }
          },
          archivedItemBanks: {
            title: "Archived Item Banks"
          }
        }
      },
      testCenterAdministration: {
        title: "Test Center Administration",
        description: "Create test centres and grant accesses to test centre coordinators.",
        tabs: {
          testCenters: {
            title: "Test Centres",
            newTestCenterButton: "New Test Centre",
            table: {
              column1: "Name",
              column2: "Last Modified",
              column3: "Organization",
              column4: "Action",
              viewButtonTooltip: "View",
              viewTestCenterAriaLabel: "View {0} test center",
              noData: "There are no results found based on your search."
            },
            newTestCenterPopup: {
              title: "Create a New Test Centre",
              description: "Complete the following fields to create a new test centre.",
              testCenterName: "Test Centre Name:",
              testCenterDepartment: "Organization",
              rightButton: "Create",
              successMessage: "The new test centre has been created."
            },
            selectedTestCenter: {
              backToRdOperationsButton: "Back to Test Centre Administration",
              backToRdOperationsPopup: {
                title: "Return to Test Centre Administration?",
                warningDescription: "All unsaved data will be lost.",
                description: "Do you want to continue?"
              },
              tabs: {
                selectedTestCenter: {
                  title: "Test Centre {0}",
                  sideNavigationItems: {
                    testCenterAccesses: {
                      itemTitle: "Test Center Accesses",
                      title: "Test Centre: {0}",
                      description:
                        "Manage access for test centre coordinators to this testing centre.",
                      testCenterName: "Test Centre Name:",
                      testCenterDepartment: "Organization",
                      testCenterOlaAuthorized: "OLA Authorized",
                      addTestCenterManagerButton: "Add a Test Centre Coordinator",
                      addTestCenterManagerPopup: {
                        title: "Add a Test Centre Coordinator to {0}",
                        description: "Select a test centre coordinator to add to the test centre.",
                        testCenterManagerLabel: "Test Centre Coordinator",
                        addButton: "Save"
                      },
                      deleteTestCenterManagerPopup: {
                        title: "This action will delete the test centre accesses of {0}.",
                        description: "This action will delete the test centre accesses of {0}.",
                        deleteButton: "Delete"
                      },
                      table: {
                        column1: "Test Centre Coordinator",
                        column2: "Action",
                        deleteButtonTooltip: "Delete",
                        deleteButtonAriaLabel: "Delete access for {0}",
                        noData:
                          "There are no test centre coordinators associated with this test centre."
                      }
                    }
                  }
                }
              }
            }
          },
          archivedTestCenters: {
            title: "Archived Test Centres"
          }
        }
      },
      testAccesses: {
        title: "Test Access Management",
        description: "Assign and manage test accesses for test administrators.",
        tabs: {
          assignTestAccesses: {
            version: {
              title: "{0} v{1}"
            },
            testDescription: "{0} v{1}",
            title: "Assign Test Accesses",
            testOrderNumberLabel: "Test Order Number:",
            staffingProcessNumber: "Assessment Process Number:",
            departmentMinistry: "Organization Code:",
            isOrg: "IS - Organization Code:",
            isRef: "IS - Reference Code:",
            billingContact: "Billing Contact Name:",
            billingContactInfo: "Contact Email for Candidates:",
            users: "Test Administrator(s):",
            usersCurrentValueAccessibility: "The selected test administrators are:",
            expiryDate: "Expiry Date:",
            testAccesses: "Test Accesses:",
            testAccess: "Test Access:",
            testAccessesError: "Must select at least one test",
            testAccessAlreadyExistsError:
              "{0} has already been granted access to the {1} for Test Order Number {2}.",
            emptyFieldError:
              "This field is empty or you have exceeded the maximum amount of characters allowed for this field",
            mustBeAnEmailError: "Must be a valid email address",
            unableToAccessOrderingServiceError:
              "Unable to connect to the ordering service. Please enter financial data manually.",
            searchButton: "Search",
            manuelEntryButton: "Manual Entry",
            noTestOrderNumberFound: "This order number cannot be found, an exact match is required",
            refreshButton: "Clear All Fields",
            refreshConfirmationPopup: {
              title: "Clear all fields?",
              description: "This action will clear all fields.",
              confirm: "Clear"
            },
            saveButton: "Save",
            saveConfirmationPopup: {
              title: "Confirm Test Access Assignment",
              usersSection: "Names and contact details for users being assigned access:",
              testsSection: "Tests for which rights are assigned:",
              orderInfoSection: {
                title: "Test Order Information:",
                testOrderNumber: "Test Order Number:",
                staffingProcessNumber: "Staffing Process Number:",
                departmentMinistry: "Organization Code:"
              },
              expiriDateSection: "Test Access Expiration Date:",
              confirmation: "Please review and confirm the following Test Access Assignment(s).",
              testAccessesGrantedConfirmationPopup: {
                title: "Confirmation of Test Access Rights",
                description: "Test accesses have been assigned successfully."
              }
            }
          },
          activeTestAccesses: {
            title: "Active Test Accesses",
            table: {
              testAdministrator: "Test Administrator",
              test: "Test",
              orderNumber: "Test Order Number",
              expiryDate: "Expiry Date",
              action: "Action",
              actionButtonLabel: "Modify",
              actionButtonAriaLabel:
                "Modify test access details of user {0} {1}, test {2}, order number {3}, expiry date {4}."
            },
            viewTestPermissionPopup: {
              title: "Modify or Delete Test Access",
              description: `Modify or delete {0}'s test access.`,
              username: "Government of Canada Email:",
              deleteButtonAccessibility: "Delete",
              saveButtonAccessibility: "Modify"
            },
            deleteConfirmationPopup: {
              title: "Confirm Deletion of Test Access",
              systemMessageDescription:
                "This action will remove {0} test access (Test Order Number: {1}) from {2}'s account. Are you sure you want to proceed?"
            },
            saveConfirmationPopup: {
              title: "Confirmation of updates",
              description: "{0}'s test access has been updated successfully."
            }
          },
          orderlessTestAccesses: {
            title: "Orderless Test Accesses",
            addNewOrderlessTaButton: "Add TA",
            addOrderlessTaPopup: {
              title: "Add Test Administrator(s)",
              description:
                "Select the test administrator(s) you would like to add to the list of TAs with orderless test accesses.",
              testAdministrators: "Test Administrator(s):",
              invalidTestAdministrators: '"{0}" already has orderless test access',
              departmentMinistry: "Organization:",
              successfulMessage: "Test administrator(s) have been added successfully."
            },
            confirmDeleteOrderlessTaPopup: {
              title: "Delete Confirmation",
              systemMessageDescription: "All orderless test accesses for {0} will be revoked.",
              description: "Are you sure you want to proceed?"
            },
            table: {
              column_1: "Test Administrator",
              column_2: "Organization",
              column_3: "Actions",
              viewTooltip: "View Details",
              deleteTooltip: "Delete All Test Accesses"
            },
            selectedOrderlessTestAdministrator: {
              backToOrderlessTestAdministratorSelectionButton: "Back to Orderless Test Access",
              tabTitle: "Orderless Test Accesses ({0})",
              sideNavItem1Title: "TA Test Accesses",
              testAccesses: {
                title: "Add/Remove Test Accesses",
                departmentUpdateTitle: "Edit TA's organization",
                departmentLabel: "Organization:",
                description: "Assign or revoke {0} {1} test accesses.",
                table: {
                  column_1: "Test Code",
                  column_2: "Test Name",
                  column_3: "Assigned",
                  noResultsFound: "No test accesses found"
                }
              },
              saveConfirmationPopup: {
                title: "Edit TA's Organization",
                systemMessageDescription: "The TA's organization has been updated successfully."
              }
            }
          }
        }
      },
      testAccessCodes: {
        description: "Displays all active test access codes",
        table: {
          columnOne: "Test Access Code",
          columnTwo: "Test",
          columnThree: "Test Order / Reference Number",
          columnFour: "Test Administrator",
          columnFive: "Date Created",
          columnSix: "Action",
          noTestAccessCode: "There are no test access codes",
          deleteButtonAccessibility: "Delete {0}"
        }
      },
      activeTests: {
        description: "Displays all active candidates",
        table: {
          columnOne: "Candidate Name",
          columnTwo: "Test Administrator",
          columnThree: "Test Form",
          columnFour: "Test Order / Reference Number:",
          columnFive: "Status",
          columnSix: "Time Limit",
          columnSeven: "Action",
          noActiveCandidates: "There are no active candidates.",
          actionButton: "Invalidate",
          invalidateButtonAccessibility: "Invalidate {0}"
        },
        invalidatePopup: {
          title: "Invalidate Test Access",
          description: "Are you sure you want to invalidate {0} {1}'s access to the test?",
          actionButton: "Invalidate Access",
          reasonForInvalidatingTheTest: "Reason for invalidating test access:"
        },
        validatePopup: {
          title: "Restore test access",
          description: "Are you sure you want to restore {0} {1}'s access to the test?",
          actionButton: "Restore Acess",
          reasonForInvalidatingTheTest: "Reason for restoring the test access:"
        }
      },
      userLookUp: {
        description: "Displays list of all users",
        table: {
          columnOne: "Email",
          columnTwo: "First Name",
          columnThree: "Last Name",
          columnFour: "Date of Birth",
          columnFive: "Action",
          viewButton: "View",
          noUsers: "There are no active users."
        },
        viewButtonAccessibility: "View {0} {1}"
      },
      userLookUpDetails: {
        sideNavigationItems: {
          userPersonalInfo: "Personal Information",
          myTests: "Tests",
          rightsAndPermissions: "Rights and Permissions",
          testPermissions: "Test Permissions"
        },
        backToUserLookUp: "Back to User Look-Up results",
        containerLabel: "User Details ({0} {1})",
        testPermissions: {
          title: "Test Access Permissions",
          description: "As a Test Administrator, {0} {1} has access to the following tests:",
          noTestPermissions: "The user does not have any test access permissions."
        },
        userRights: {
          title: "Rights and Permissions",
          description: "Displays {0} {1}'s rights",
          noRights: "The user does not have any rights."
        },
        Tests: {
          title: "View {0} {1}'s Tests",
          description: "Displays all the tests taken by {0} {1}",
          table: {
            columnOne: "Test",
            columnTwo: "Test Date",
            columnThree: "Retest possible as of",
            columnFour: "Score",
            columnFive: "Status",
            columnSix: "Invalidated",
            columnSeven: "Action",
            noTests: "The user does not have any tests. ",
            viewTestDetailsTooltip: "View Test Details",
            invalidateTestTooltip: "Invalidate Test",
            validateTestTooltip: "Cancel the invalidation",
            invalidateButton: "Invalidate",
            viewButtonAccessibility: "View {0}",
            invalidateButtonAccessibility: "Invalidate {0}",
            validateButtonAccessibility: "Cancel the Invalidation of the {0}",
            downloadCandidateActionReport: "Generate Candidate Actions Report",
            candidateActionReportAria: "Generate Candidate Actions Report for {0}"
          },
          viewSelectedTestPopup: {
            title: "View Test Details",
            description: "Displays all the details of the test",
            testName: "Test Name:",
            testCode: "Test Code:",
            testAccessCode: "Test Access Code:",
            testOrderNumber: "Test Order Number:",
            assessmentProcessOrReferenceNb: "Assessment Process / Reference Number:",
            validityEndDateOrProcessClosingDate: "Validity End Date / Process Closing Date:",
            testAdministrator: "Test Administrator:",
            testAdministratorEmail: "Email:",
            testAdministratorGocEmail: "Government of Canada Email Address:",
            totalScore: "Total Score:",
            version: "CAT Version:",
            invalidTestReason: "Reason for Invalid Test:"
          }
        },
        userPersonalInfo: {
          title: "Personal Information",
          firstName: "First Name:",
          lastName: "Last Name:",
          username: "Username:",
          primaryEmailAddress: "Primary Email Address:",
          secondaryEmailAddress: "Secondary Email Address:",
          dateOfBirth: "Date of Birth:",
          psrsApplicantId: "PSRS Applicant ID:",
          pri: "PRI:",
          militaryNbr: "Service Number (Military)",
          lastLogin: "Last Login:",
          lastPasswordChange: "Last Password Change:",
          accountCreationDate: "Account Creation Date:",
          lastUpdatedDate: "Last Update:",
          additionalInfo: "Additional Information",
          currentEmployer: "Current Employer:",
          organization: "Organization:",
          group: "Group:",
          subGroup: "Subgroup:",
          level: "Level:",
          residence: "Residence:",
          education: "Education:",
          gender: "Gender:",
          noPersonalInformation: "There is no personal information on the user.",
          noAdditionalInfomration: "There is no additional information on the user."
        }
      },
      userProfileChangeRequests: {
        title: "User Profile Modification Requests",
        table: {
          columnOne: "Account Email",
          columnTwo: "First Name",
          columnThree: "Last Name",
          columnFour: "DOB",
          columnFive: "Action",
          reviewButton: "Review",
          reviewButtonAccessibility: "Review profile change request of {0} {1}",
          noData: "There is no user profile modification request to display"
        },
        popup: {
          title: "Review Profile Modification Request",
          description: "Changes made to could take up to 2 business days to be applied.",
          usernameLabel: "Username:",
          priLabel: "PRI:",
          current: "Current",
          new: "New",
          firstNameLabel: "First Name:",
          lastNameLabel: "Last Name:",
          dateOfBirthLabel: "Date of Birth:",
          commentsLabel: "Comments:",
          reasonForDenyLabel: "Reason for denial of request:",
          reasonForDenyError: "Must provide a reason for denial of request",
          leftButton: "Deny",
          rightButton: "Approve"
        },
        successPopup: {
          title: "Action Completed",
          systemMessageDescription: "Your action has been completed successfully."
        }
      },
      alternateTestRequests: {
        title: "Alternate Test Requests",
        description: "Manage alternate tests",
        pendingRequests: {
          title: "Pending Requests",
          table: {
            columnOne: "Request ID",
            columnTwo: "Candidate ID",
            columnThree: "Candidate",
            columnFour: "Request Date",
            columnFive: "Process End Date",
            columnSix: "Test Center",
            columnSeven: "Test",
            columnHeight: "Actions",
            noData: "No pending alternate test requests",
            viewTooltip: "View",
            viewTooltipAccessibility: "View request of {0} {1}"
          },
          viewSelectedPendingAlternateTestRequestPopup: {
            title: "Alternate Test Request - {0} {1}",
            titleSuccess: "Alternate test request sent successfully",
            description1: "User: {0} {1} ({2})",
            description1continued: "Test: {0}",
            description2:
              "The information below has been provided for your analysis and recommendation on which alternate test to apply for this candidate.",
            successSystemMessageDescription: "This request has been sent successfully!",
            locationLabel: "Location:",
            testAdministeredDetailsLabel: "Reason for request:",
            recommendationsLabel: "Recommendations:",
            testCannotBePerformedInCatLabel: "Test cannot be performed with CAT:",
            testToAdministerLabel: "Test to administer:",
            specifiedTestDescriptionLabel: "Test description:",
            specifiedTestDescriptionError: "You must provide a test description",
            rationaleLabel: "Rationale:",
            cancelRequestButton: "Cancel Request"
          },
          cancelRequestConfirmationPopup: {
            title: "Are you sure you want to cancel your request?",
            systemMessageDescription:
              "Your alternate test request will be cancelled. This action cannot be revoked!",
            description: "Are you sure you want to continue?"
          }
        },
        completedRequests: {
          title: "Completed Requests",
          table: {
            columnOne: "Request ID",
            columnTwo: "Candidate ID",
            columnThree: "Candidate",
            columnFour: "Request Date",
            columnFive: "Process End Date",
            columnSix: "Test Center",
            columnSeven: "Test",
            columnHeight: "Actions",
            noData: "No completed alternate test requests",
            viewTooltip: "View",
            viewTooltipAccessibility: "View request of {0} {1}"
          },
          viewSelectedCompletedAlternateTestRequestPopup: {
            title: "Alternate Test Request - {0} {1}",
            description1: "User: {0} {1} ({2})",
            description1continued: "Test: {0}",
            description2:
              "The information below has been provided for your analysis and recommendation on which alternate test to apply for this candidate.",
            successSystemMessageDescription: "This request has been sent successfully!",
            locationLabel: "Location:",
            commentsLabel: "Comments:",
            recommendationsLabel: "Recommendations:",
            testCannotBePerformedInCatLabel: "Test cannot be performed with CAT:",
            testToAdministerLabel: "Test to administer:",
            specifiedTestDescriptionLabel: "Test description:",
            noTestToAdministerDefinedLabel:
              "No test to administer has been defined for this request.",
            rationaleLabel: "Rationale:"
          }
        }
      },
      reports: {
        title: "Reports",
        description: "Generate a results report for a Test Order you administered."
      },
      systemAlerts: {
        title: "System Alerts",
        description: "Manage System Alerts",
        tabs: {
          activeSystemAlerts: {
            title: "Active System Alerts",
            addNewSystemAlertButton: "Add System Alert",
            addSystemAlertPopup: {
              title: "Add System Alert",
              fields: {
                title: "Title:",
                criticality: "Criticality:",
                startDate: "Start Date:",
                endDate: "End Date:",
                messageText: "Message Text:",
                messageTextTabs: {
                  english: "English",
                  french: "French"
                }
              },
              buttons: {
                rightButton: "Add"
              },
              successfulMessage: "System Alert has been added successfully."
            },
            archiveSystemAlertPopup: {
              title: "Archive System Alert",
              systemMessageTitle: "Archive this system alert?",
              systemMessageMessage:
                "Archiving this system alert will remove it from the login page, and move it to the archived System Alerts.",
              archiveSuccessfullyMessage: "System Alert has been archived successfully.",
              archiveRightButton: "Archive"
            },
            table: {
              column_1: "Alert",
              column_2: "Criticality",
              column_3: "Active Date",
              column_4: "End Date",
              column_5: "Actions",
              viewTooltip: "View",
              modifyTooltip: "Modify",
              archiveToolTip: "Archive"
            },
            viewPopupTitle: "View System Alert",
            modifyPopupTitle: "Modify System Alert",
            modifyRightButton: "Modify",
            modifySuccessfullyMessage: "System Alert has been modified successfully."
          },
          archivedSystemAlerts: {
            title: "Archived System Alerts",
            table: {
              column_1: "Alert",
              column_2: "Criticality",
              column_3: "Active Date",
              column_4: "End Date",
              column_5: "Actions",
              viewTooltip: "View",
              cloneTooltip: "Clone"
            },
            viewPopupTitle: "View System Alert",
            clonePopupTitle: "Clone System Alert",
            cloneSuccessfullyMessage: "System Alert has been cloned successfully."
          }
        }
      },
      olaConfigs: {
        title: "OLA Settings",
        description: "Manage OLA settings.",
        tabs: {
          prioritization: {
            title: "Priority distribution",
            prioritizationLabelFormatter: "{0} (%):",
            prioritizationError: "Combined percentages must not exceed 100%"
          },
          serviceStandards: {
            title: "Service Standards",
            table: {
              column1: "Reason for Testing",
              column2: "Service Standard",
              column3: "Waiting Period",
              column4: "Priority",
              column5: "Active",
              column6: "Actions",
              editButtonTooltip: "Edit",
              editButtonTooltipAccessibility: "Edit {0}"
            },
            serviceStandardPopup: {
              editTitle: "Edit OLA Service Standard",
              description: "Manage OLA service standard settings.",
              nameTranslationTitle: "Translation",
              nameLabel: "Reason for testing:",
              translationsButtonAriaLabel: "Edit Translation",
              translationsButtonTooltip: "Edit Translation",
              en_name: {
                title: "Reason for testing in English:"
              },
              fr_name: {
                title: "Reason for testing in French:"
              },
              nameError: "Click on the translation button to complete all fields",
              priorityLabel: "Priority Level:",
              minimumProcessLengthLabel: "Service Standard (days):",
              waitingPeriodLabel: "Waiting Period (days):",
              waitingPeriodError: "Must not exceed the Service Standard value",
              activeStateLabel: "Active:"
            }
          },
          olaConfig: {
            title: "Last Minute Settings",
            cancellationWindow: "Late cancellation window (hours):",
            openBookingPeriod:
              "Open booking to all reasons for testing - new reservations only (days):"
          }
        }
      }
    },

    // Scorer Pages
    scorer: {
      situationTitle: "Situation",
      assignedTest: {
        displayOptionLabel: "Display:",
        displayOptionAccessibility: "Number of results per page",
        displayOptionCurrentValueAccessibility: "The selected value is:",
        table: {
          assignedTestId: "Assigned Test ID",
          completionDate: "Completion Date",
          testName: "Test Name",
          testLanguage: "Test Language",
          action: "Action",
          actionButtonLabel: "Score Test",
          en: "English",
          fr: "French",
          actionButtonAriaLabel:
            "Score test of: assigned test id: {0}, completion date: {1}, test name: {2}, test language: {3}."
        },
        noResultsFound: "There are no tests waiting to be scored."
      },
      sidebar: {
        title: "Scoring Panel",
        competency: "Competency",
        rational: "Rating rationale",
        placeholder: "Input rationale here..."
      },
      submitButton: "Submit Final Scores",
      unsupportedScoringType:
        "The scoring type: {0} is not supported. The test definition must be incorrect.",
      selectQuestion: "Select a question to continue",
      competency: {
        bar: {
          title: "Behavioural Achored Ratings",
          description:
            "Below is a complete view of the BAR for this question and competency. Once selected, you can use the arrows to navigate, expand, and collapse information.",
          barButton: "View BAR"
        }
      },
      ola: {
        tabName: "OLA",
        pageTitle: "Oral Language Assessment",
        myAssignedTests: {
          tabName: "My Tests ",
          table: {
            column1: "Test Session Time",
            column2: "Test Language",
            column3: "Candidate",
            column4: "Actions",
            viewDetailsButtonTooltip: "View",
            viewDetailsButtonTooltipAccessibility:
              "View details of the test session for candidate {0}",
            joinVideoCallTooltip: "Join meeting",
            joinVideoCallTooltipAccessibility: "Join the test session for candidate {0}",
            noData: "There are currently no test sessions at the moment"
          },
          assignedCandidatePopup: {
            title: "Information",
            candidateLabel: "Candidate:",
            olaPhoneNumberLabel: "Candidate's phone number:",
            candidatePriLabel: "PRI:",
            referenceNumberLabel: "Reference Number:",
            requestingDepartmentLabel: "Requesting Organization:",
            hrContactLabel: "HR Contact:",
            testToAdministerLabel: "Test to Administer:",
            olaFieldInput: "OLA {0}",
            languageOfTestLabel: "Language of Test:",
            reasonForTestingLabel: "Reason for Testing:",
            levelRequiredLabel: "Required Level:"
          },
          unassignCandidateConfirmationPopup: {
            title: "Unassign Candidate",
            systemMessageDescription:
              "Arrangements will need to be made to have another assessor assess this candidate.",
            rightButton: "Unassign"
          }
        },
        testsToAssess: {
          tabName: "Tests to Assess",
          showUnassignedOnly: "Show only unassigned test sessions",
          table: {
            testSessionTime: "Test Session Time",
            sessionLanguage: "Session Language",
            reasonForTesting: "Reason For Testing",
            candidate: "Candidate",
            testAssessor: "Assessor",
            actions: "Actions",
            viewDetailsButtonTooltip: "View",
            viewDetailsButtonTooltipAccessibility:
              "View details of the test session for candidate {0}",
            assignButtonTooltip: "Assign",
            assignButtonTooltipAccessibility: "Assign {0} test session",
            unassignButtonTooltip: "Unassign",
            unassignButtonTooltipAccessibility: "Unassign {0} test session",
            assignTestAssessorTooltip: "Assign Assessor",
            assignTestAssessorTooltipAccessibility: "Assign {0} to Test Assessor",
            cancelTestSessionTooltip: "Cancel Test Session",
            cancelTestSessionTooltipAccessibility: "Cancel Test Session of {0}",
            noData: "There are currently no test sessions at the moment"
          },
          assignOrAssignedCandidatePopup: {
            assignTitle: "Information",
            assignedTitle: "Information",
            description: "Review the candidate's details and accept or skip to the next candidate.",
            candidateLabel: "Candidate:",
            candidatePriLabel: "PRI:",
            referenceNumberLabel: "Reference Number:",
            requestingDepartmentLabel: "Requesting Organization:",
            hrContactLabel: "HR Contact:",
            testToAdministerLabel: "Test to Administer:",
            olaFieldInput: "OLA {0}",
            languageOfTestLabel: "Language of Test:",
            reasonForTestingLabel: "Reason for Testing:",
            levelRequiredLabel: "Required Level:",
            leftButton: "Skip",
            rightButton: "Accept"
          },
          unassignCandidateConfirmationPopup: {
            title: "Unassign Candidate",
            systemMessageDescription:
              "Arrangements will need to be made to have another assessor assess this candidate.",
            rightButton: "Unassign"
          },
          assignTestAssessorPopup: {
            title: "Assign Assessor",
            systemMessageDescription:
              "Please make sure that the selected assessor is aware of this new assigned test session.",
            testAssessorLabel: "Assessor:",
            rightButton: "Assign"
          },
          cancelTestSessionConfirmationPopup: {
            title: "Cancel Test Session",
            systemMessageDescription: "You are about to cancel the test session of {0}.",
            description: "Are you sure you want to continue?",
            rightButton: "Cancel Session",
            rightButtonLoading: "Cancelling..."
          },
          alreadyPairedPopup: {
            title: "Already Assigned",
            systemMessageDescription: "This candidate is already assigned to another assessor.",
            description: "Close this popup to view the information of the next candidate."
          },
          conflictingSchedulePopup: {
            title: "Conflicting Schedule",
            systemMessageDescription:
              "There is a schedule conflict with the test sessions you are trying to assign.",
            description: "Close this popup to refresh the page."
          }
        },
        notLinkedToTestCenterMsg: "Sorry, you are not linked to a test center",
        assessorsUnavailability: {
          tabName: "My unavailability",
          addUnavailabilityButton: "Add unavailability",
          table: {
            column1: "Test Centre",
            column2: "Assessor",
            column3: "Start Date",
            column4: "End Date",
            column5: "Actions",
            modifyButtonTooltip: "Modify",
            modifyButtonTooltipAccessibility:
              "Modify the unavailability period for {0} in the {1} Test Center, with a Start Date of {2} and an End Date of {3}",
            deleteButtonTooltip: "Delete",
            deleteButtonTooltipAccessibility:
              "Delete the unavailability period for {0} in the {1} Test Center, with a Start Date of {2} and an End Date of {3}",
            noData: "No unavailability period at the moment"
          },
          addUnavailabilityPopup: {
            title: "Add an unavailability period",
            description: "Provide details for the unavailability period",
            assessor: "Assessor:",
            testCenters: "Test Centre(s):",
            startDate: "Start Date:",
            endDate: "End Date:",
            reasonForUnavailability: "Reason for unavailability:",
            rightButton: "Add"
          },
          modifyUnavailabilityPopup: {
            title: "Modify unavailability period",
            description: "Provide details for the unavailability period",
            rightButton: "Modify"
          },
          deleteUnavailabilityConfirmationPopup: {
            title: "Delete the unavailability period",
            systemMessageDescription: "Do you want to proceed? This action cannot be undone.",
            rightButton: "Delete"
          },
          errorPopup: {
            addErrorTitle: "Error - Adding Assessor Unavailability",
            addErrorDescription: "There was an error. Please try again.",
            modifyErrorTitle: "Error - Modifying Assessor Unavailability",
            modifyErrorDescription: "There was an error. Please try again.",
            deleteErrorTitle: "Error - Deleting Assessor Unavailability",
            deleteErrorDescription: "There was an error. Please try again."
          },
          errors: {
            endDateError: "The end date cannot be before today's date"
          }
        },
        myAvailability: {
          tabName: "My Availability",
          table: {
            column1: "Testing Period",
            column2: "Language to Assess",
            column3: "Test Centre",
            column4: "Available",
            noData: "No availability period at the moment"
          }
        }
      }
    },

    // Status Page
    statusPage: {
      title: "CAT Status",
      logo: "Thunder CAT Logo",
      welcomeMsg:
        "Internal status page to quickly determine the status / health of the Candidate Assessment Tool.",
      versionMsg: "Candidate Assessment Tool Version: ",
      gitHubRepoBtn: "GitHub Repository",
      serviceStatusTable: {
        title: "Service Status",
        frontendDesc: "Front end application built and serving successfully",
        backendDesc: "Back end application completing API requests successfully",
        databaseDesc: "Database completing API requests sccessfully"
      },
      systemStatusTable: {
        title: "System Status",
        javaScript: "JavaScript",
        browsers: "Chrome, Firefox, Edge",
        screenResolution: "Screen resolution minimum of 1024 x 768"
      },
      additionalRequirements:
        "Additionally, the following requirements must be met to use this application in a test centre.",
      secureSockets: "Secure Socket Layer (SSL) encryption enabled",
      fullScreen: "Full-screen mode enabled",
      copyPaste: "Copy + paste functionality enabled"
    },

    // AAE
    aae: {
      title: "Assessment Accommodations",
      sideNav: {
        accommodationRequests: {
          title: "Requests for accommodations",
          description: "Manage all assessment accommodation requests",
          topTabs: {
            openRequests: {
              title: "Open Requests",
              table: {
                columnOne: "Request ID",
                columnTwo: "Candidate ID",
                columnThree: "Candidate",
                columnFour: "Test Type",
                columnFive: "Request Date",
                columnSix: "Process End Date",
                columnSeven: "Status",
                columnEight: "Assigned To",
                columnNine: "Test Skill",
                columnTen: "Actions",
                noData: "No Open Requests",
                viewButtonAccessibility: "View  request for {0} {1}",
                viewButtonTooltip: "View accommodation request"
              }
            },
            completedRequets: {
              title: "Completed Requests",
              table: {
                columnOne: "Request ID",
                columnTwo: "Candidate ID",
                columnThree: "Candidate",
                columnFour: "Test Type",
                columnFive: "Request Date",
                columnSix: "Status",
                columnSeven: "Assigned To",
                columnEight: "Test Skill",
                columnNine: "Actions",
                noData: "No completed requests",
                viewButtonAccessibility: "View  request for {0} {1}",
                viewButtonTooltip: "View accommodation request"
              }
            }
          },
          selectedRequest: {
            backToAccommodationRequestSelectionButton: "Back to Requests for Accommodations",
            backToAccommodationRequestSelectionPopup: {
              title: "Back to Requests for Accommodations",
              systemMessageDescription: "All unsaved data will be lost!",
              description: "Are you sure you want to proceed?"
            },
            topTabs: {
              main: {
                title: "{0} {1}",
                sideNavigation: {
                  candidateData: {
                    title: "Candidate Information",
                    userIdLabel: "Candidate ID:",
                    userFirstNameLabel: "First Name:",
                    userLastNameLabel: "Last Name:",
                    userEmailLabel: "Email address:",
                    userSecondaryEmailLabel: "Secondary email address:",
                    userDobLabel: "Date of birth:",
                    userPhoneNumberLabel: "Candidate's Cellphone Number",
                    olaPhoneNumberLabel: "Candidate's phone number:",
                    userPsrsApplicantIdLabel: "PSRS Applicant ID:",
                    userPriLabel: "PRI:",
                    userMilitaryNbrLabel: "Service Number:",
                    referenceNumberLabel: "Assessment Process / Reference Number:",
                    departmentLabel: "Organization:",
                    primaryContactLabel: "Primary Contact:",
                    candidateCommentsLabel: "Comments:"
                  },
                  userAccommodationsProfile: {
                    title: "User Accommodations Profile"
                  },
                  testToAdminister: {
                    sideNavTitle: "Test to administer",
                    title: "Test to Administer {0}",
                    testCenterLabel: "Test Center:",
                    testSkillTypeLabel: "Test Type:",
                    testSkillSubTypeLabel: "Test:",
                    testCannotBePerformedInCatSwitchDescription: "Test cannot be performed in CAT:",
                    specifiedTestDescriptionLabel: "Test Description:",
                    specifiedTestDescriptionError: "You must provide a test description",
                    table: {
                      columnOne: "Test Code",
                      columnTwo: "CAT Version",
                      columnThree: "Test Name",
                      noData: "No tests available at the moment"
                    }
                  },
                  breakBankAndTimeAdjustment: {
                    sideNavTitle: "Break / Time Adjustment",
                    title: "Break / Time Adjustment {0}",
                    testToAdministerLabel: "Test to Administer:",
                    defaultTestTimeLabel: "Default Test Time:",
                    noTestSelectedSystemMessageDescription:
                      "Please select a test in order to set the desired break bank and/or test time.",
                    allottedBreakBankLabel: "Allotted Break Bank:",
                    testSectionTimers: "Test Section Timers:",
                    sectionPrefixLabel: "Section:",
                    outsideOfCATSection: "Customize",
                    testSectionTimersError:
                      "You cannot set a test section timer below its default time."
                  },
                  otherMeasures: {
                    sideNavTitle: "Other Measures",
                    title: "Other Measures",
                    inputLabel: "Additional Measures to apply:"
                  },
                  fileAdministration: {
                    sideNavTitle: "File Administration",
                    title: "Request Administration {0}",
                    requestId: "Request Number:",
                    assignedToLabel: "Assigned to:",
                    fileStatusLabel: "Request Status:",
                    fileSubmitDateLabel: "Request Submit Date:",
                    fileComplexityLabel: "Request Complexity:",
                    lastModifiedDateLabel: "Last Modified Date:",
                    lastModifiedByLabel: "Last Modified By:",
                    processEndDateLabel: "Process End Date:"
                  },
                  reports: {
                    sideNavTitle: "Reports",
                    title: "Reports",
                    previewCurrentFileDataLabel: "Preview Current File Data:",
                    viewDraftReportButton: "View Draft Report",
                    previousReportsLabel: "Previous Reports:",
                    previousReportsTable: {
                      columnOne: "Report Date",
                      columnTwo: "Test Name",
                      columnThree: "Test Type",
                      columnFour: "Action",
                      viewButton: "View",
                      viewButtonAccessibility: "View {0} - {1} report",
                      noData: "No previous reports yet"
                    }
                  }
                }
              },
              other: {
                comments: {
                  title: "Notes",
                  addCommentButton: "Note",
                  addCommentPopup: {
                    title: "Add Comment",
                    commentLabel: "Comment:",
                    addButton: "Save"
                  },
                  deleteCommentPopup: {
                    title: "Delete Note",
                    systemMessageDescription1: "Are you sure you want to delete this note?",
                    systemMessageDescription2: "- {0}"
                  }
                },
                rationale: {
                  title: "Rationale",
                  rationaleLabel: "Please provide a rationale:"
                },
                stats: {
                  title: "Stats",
                  candidateLimitations: {
                    title: "Candidate Limitations"
                  },
                  recommendations: {
                    title: "Recommendations"
                  }
                }
              }
            },
            footer: {
              rejectButton: "Reject",
              rejectPopup: {
                title: "Reject Request?",
                systemMessageDescription: "This action will reject this request.",
                description: "Are you sure you want to continue?"
              },
              completeRequestButton: "Complete Request",
              completePopup: {
                title: "Complete Request?",
                systemMessageDescription: "This action will complete this request.",
                description: "Are you sure you want to continue?"
              },
              reOpenRequestButton: "Re-Open Request",
              reOpenPopup: {
                title: "Re-Open Request?",
                systemMessageDescription: "This action will re-open this request.",
                description: "Are you sure you want to continue?"
              },
              actionDeniedPopup: {
                title: "Action Denied!",
                systemMessageDescription:
                  "You are not allowed to perform this action, since the candidate decided to cancel this request."
              },
              savedButCancelledByCandidatePopup: {
                title: "Data Saved, But,...",
                systemMessageDescription:
                  "Your data has been properly saved, however the status is now set based on the candidate's actions."
              }
            }
          }
        },
        accommodationReports: {
          title: "Reports",
          description: "Generate all assessment accommodation reports.",
          reportTypes: {
            numberOfRequestsReceived: {
              title: "Number of Requests Received",
              year: "Year",
              quarter: "Quarter",
              month: "Month",
              requestsReceived: "Requests Received",
              totalNumberOfRequests: "Total Number of Requests:"
            },
            numberOfClosedRequests: {
              title: "Number of Completed Requests",
              requestId: "Request ID",
              receivedDate: "Received Date",
              sleSkill: "Test Type Description",
              firstCompletedByAaeDate: "First Completed by AAU Date",
              firstApprovedByCandidateDate: "First Agreed by Candidate Date ",
              status: "Current Status",
              statusValue: "Completed",
              totalNumberOfRequests: "Total Number of Requests:"
            },
            numberOfRequestsByLimitationAndTestType: {
              title: "Number of Requests by Limitation / by SLE Test Type"
            },
            numberOfRequestsByRecommendation: {
              title: "Number of Requests by Recommendation"
            },
            serviceStandards: {
              title: "Service Standards"
            },
            timeInEachStatus: {
              title: "Time in Each Status"
            }
          }
        }
      }
    },

    // Consultation Services
    consultationServices: {
      title: "Consultation Services",
      sideNav: {
        consultationServicesRequests: {
          title: "Requests for consultation",
          description: "Description here...",
          topTabs: {
            openRequests: {
              title: "Pending Request",
              table: {
                columnOne: "Candidate",
                columnTwo: "Test Type",
                columnThree: "Request Date",
                columnFour: "Actions",
                noData: "No pending requests",
                viewButtonAccessibility: "View  request for {0} {1}",
                viewButtonTooltip: "View accommodation request"
              }
            },
            completedRequets: {
              title: "Completed Requests",
              table: {
                columnOne: "Candidate",
                columnTwo: "Test Type",
                columnThree: "Request Date",
                columnFour: "Actions",
                noData: "No completed requests",
                viewButtonAccessibility: "View  request for {0} {1}",
                viewButtonTooltip: "View consultation request"
              }
            }
          }
        }
      }
    },

    // Settings Dialog
    settings: {
      systemSettings: "Accessibility Display Settings",
      zoom: {
        title: "Zoom (+/-)",
        description:
          "Use your browser settings to change the zoom level. Alternatively, you can hold down CTRL and the + / - keys on your keyboard to zoom in or out."
      },
      fontSize: {
        title: "Font Size",
        label: "Font Size:",
        description: "Select the size of your text:"
      },
      fontStyle: {
        title: "Font Style",
        label: "Font Style:",
        description: "Select a new font for your text:"
      },
      lineSpacing: {
        title: "Text Spacing",
        label: "Text Spacing:",
        description: "Turn on accessibility text spacing."
      },
      color: {
        title: "Text and background colour",
        description: "Use your browser settings to select the text and background colour."
      }
    },

    // Multiple Choice  test
    mcTest: {
      questionList: {
        questionIdShort: "Q{0}",
        questionIdLong: "Question {0}",
        reviewButton: "Mark for Review",
        markedReviewButton: "Unmark for Review",
        seenQuestion: "This question has been viewed",
        unseenQuestion: "This question has not been viewed",
        answeredQuestion: "This question has been answered",
        unansweredQuestion: "This question has not been answered",
        reviewQuestion: "This question has been marked for review."
      },
      finishPage: {
        homeButton: "Return to Home Page"
      }
    },
    // eMIB Test
    emibTest: {
      // Home Page
      homePage: {
        testTitle: "Sample e-MIB",
        welcomeMsg: "Welcome to the eMIB Sample Test"
      },

      // HowTo Page
      howToPage: {
        tipsOnTest: {
          title: "Tips on taking the e-MIB",
          part1: {
            description:
              "The e-MIB presents you with situations that will give you the opportunity to demonstrate the Key Leadership Competencies. Here are some tips that will help you provide assessors with the information they need to evaluate your performance on the Key Leadership Competencies:",
            bullet1:
              "Answer all the questions asked in the emails you received. Also, note that there may be situations that span several emails and for which no specific questions are asked, but that you can respond to. This will ensure that you take advantage of all the opportunities provided to demonstrate the competencies.",
            bullet2:
              "Don’t hesitate to provide your recommendations where appropriate, even if they are only initial thoughts.  If needed, you can then note other information you would need to reach a final decision.",
            bullet3:
              "For situations in which you feel you need to speak with someone in person before you can make a decision, you should indicate what information you need and how this would affect your decision one way or the other.",
            bullet4:
              "Use only the information provided in the emails and the background information. Do not make any inferences based on the culture of your own organization. Avoid making assumptions that are not reasonably supported by either the background information or the emails.",
            bullet5:
              "The e-MIB is set within a specific industry in order to provide enough context for the situations presented. However, effective responses rely on the competency targeted and not on specific knowledge of the industry."
          },
          part2: {
            title: "Other important information",
            bullet1:
              "You will be assessed on the content of your emails, the information provided in your task list and the reasons listed for your actions. Information left on your notepad will not be evaluated.",
            bullet2:
              "You will not be assessed on how you write. No points will be deducted for spelling, grammar, punctuation or incomplete sentences. However, your writing will need to be clear enough to ensure that the assessors understand which situation you are responding to and your main points.",
            bullet3: "You can answer the emails in any order you want.",
            bullet4: "You are responsible for managing your own time."
          }
        },
        testInstructions: {
          title: "Test instructions",
          hiddenTabNameComplementary: 'Under "Instructions"',
          onlyOneTabAvailableForNowMsg:
            "Note that there is only one main tab available for now. As soon as you start the test, the other main tabs will become available.",
          para1:
            "When you start the test, first read the background information which describes your position and the fictitious organization you work for. We recommend you take approximately 10 minutes to read it. Then proceed to the in-box where you can read the emails you received and take action to respond to them as though you were a manager within the fictitious organization.",
          para2: "While you are in the email in-box, you will have access to the following:",
          bullet1: "The test instructions;",
          bullet2:
            "The background information describing your job as the manager and the fictitious organization where you work;",
          bullet3: "A notepad to serve as scrap paper.",
          step1Section: {
            title: "Step 1 - Responding to emails",
            description:
              "You can respond to the emails you received in two ways: by writing an email or by adding tasks to your task list. A description of both ways of responding is presented below, followed by examples.",
            part1: {
              title: "Example of an email you have received:",
              para1:
                "Two options are provided below to demonstrate different ways of responding to the email. You can choose one of the two options presented or a combination of the two. The responses are only provided to illustrate how to use each of the two ways of responding. They do not necessarily demonstrate the Key Leadership Competencies assessed in this situation."
            },
            part2: {
              title: "Responding with an email response",
              para1:
                "You can write an email in response to one you received in your in-box. The written response should reflect how you would respond as a manager.",
              para2:
                "You can use the following options: reply, reply all or forward. If you choose to forward an email, you will have access to a directory with all of your contacts. You can write as many emails as you like to respond to an email or to manage situations you identified across several of the emails you received."
            },
            part3: {
              title: "Example of responding with an email response:"
            },
            part4: {
              title: "Adding a task to the task list",
              para1:
                "In addition to, or instead of, responding by email, you can add tasks to the task list. A task is an action that you intend to take to address a situation presented in the emails. For example, tasks could include planning a meeting or asking a colleague for information. You should provide enough information in your task description to ensure it is clear which situation you are addressing. You should also specify what actions you plan to take, and with whom. You can return to the email to add, delete or edit tasks at any time."
            },
            part5: {
              title: "Example of adding a task to the task list:"
            },
            part6: {
              title: "How to choose a way of responding",
              para1:
                "There are no right or wrong ways to respond. When responding to an email, you can:",
              bullet1: "send an email or emails; or",
              bullet2: "add a task or tasks to your task list; or",
              bullet3: "do both.",
              para2:
                "You will be assessed on the content of your responses, and not on your method of responding (i.e. whether you responded by email and/or by adding a task to your task list). As such, answers need to be detailed and clear enough for assessors to evaluate how you are addressing the situation. For example, if you plan to schedule a meeting, you will need to specify what will be discussed at that meeting.",
              para3Part1:
                "When responding to an email you received, if you decide to write an email ",
              para3Part2: "and",
              para3Part3:
                " to add a task to your task list, you do not need to repeat the same information in both places. For example, if you mention in an email that you will schedule a meeting with a co-worker, you do not need to add that meeting to your task list."
            }
          },
          step2Section: {
            title: "Step 2 - Adding reasons for your actions (optional)",
            description:
              "After writing an email or adding a task, if you feel the need to explain why you took a specific action, you can write it in the reasons for your actions section, located below your email response or tasks. Providing reasons for your actions is optional. Note that you may choose to explain some actions and not others if they do not require any additional explanations. Similarly, you may decide to add the reasons for your actions when responding to some emails and not to others. This also applies to tasks in the task list.",
            part1: {
              title: "Example of an email response with reasons for your actions:"
            },
            part2: {
              title: "Example of a task list with reasons for your actions:"
            }
          },
          exampleEmail: {
            to: "T.C. Bernier (Manager, Quality Assurance Team)",
            from: "Geneviève Bédard (Director, Research and Innovation Unit)",
            subject: "Preparing Mary for her assignment",
            date: "Friday, November 4",
            body: "Hello T.C.,\n\nI was pleased to hear that one of your quality assurance analysts, Mary Woodside, has accepted a six-month assignment with my team, starting on January 2. I understand she has experience in teaching and using modern teaching tools from her previous work as a college professor. My team needs help developing innovative teaching techniques that promote employee productivity and general well-being. Therefore, I think Mary’s experience will be a good asset for the team.\n\nAre there any areas in which you might want Mary to gain more experience and which could be of value when she returns to your team? I want to maximize the benefits of the assignment for both our teams.\n\nLooking forward to getting your input,\n\nGeneviève"
          },
          exampleEmailResponse: {
            emailBody:
              "Hi Geneviève,\n\nI agree that we should plan Mary’s assignment so both our teams can benefit from it. I suggest that we train Mary in the synthesis of data from multiple sources. Doing so could help her broaden her skill set and would be beneficial to my team when she comes back. Likewise, your team members could benefit from her past experience in teaching. I’ll consult her directly as I would like to have her input on this. I’ll get back to you later this week once I have more information to provide you on this matter.\n\nThat being said, what are your expectations? Are there any current challenges or specific team dynamics that should be taken into account? I’d like to consider all factors, such as the current needs of your team, challenges and team dynamics before I meet with Mary to discuss her assignment.\n\nThanks,\n\nT.C.",
            reasonsForAction:
              "I am planning to meet with Mary to discuss her expectations regarding the assignment and to set clear objectives. I want to ensure she feels engaged and knows what is expected of her, to help her prepare accordingly. I will also check what her objectives were for the year to ensure that what I propose is consistent with her professional development plan."
          },
          exampleTaskResponse: {
            task: "- Reply to Geneviève’s email:\n     > Suggest training Mary in the synthesis of information from multiple sources so that she can broaden her skill set.\n     > Ask what her expectations are and what the challenges are on her team so I can consider all factors when determining how her team could benefit from Mary’s experience in providing training.\n     > Inform her that I will get more information from Mary and will respond with suggestions by the end of the week.\n- Schedule a meeting with Mary to discuss her assignment objectives and ensure she feels engaged and knows what is expected of her.\n- Refer to Mary’s past and current objectives to ensure that what I propose is in line with her professional development plan.",
            reasonsForAction:
              "Training Mary in the synthesis of information from multiple sources would be beneficial to my team which needs to consolidate information gathered from many sources. Asking Geneviève what her expectations and challenges are will help me better prepare Mary and ensure that the assignment is beneficial to both our teams."
          }
        },
        evaluation: {
          title: "Evaluation",
          bullet1:
            "Both the actions you take and the explanations you give will be considered when assessing your performance on each of the Key Leadership Competencies (described below). You will be assessed on the extent to which they demonstrate the Key Leadership Competencies.",
          bullet2:
            "Your actions will be evaluated on effectiveness. Effectiveness is measured by whether your actions would have a positive or a negative impact on resolving the situations presented and by how widespread that impact would be.",
          bullet3:
            "Your responses will also be evaluated on how well they meet the organizational objectives presented in the background information.",
          keyLeadershipCompetenciesSection: {
            title: "Key Leadership Competencies",
            para1Title: "Create Vision and Strategy: ",
            para1:
              "Managers help to define the future and chart a path forward. To do so, they take into account context. They leverage their knowledge and seek and integrate information from diverse sources to implement concrete activities. They consider different perspectives and consult as needed. Managers balance organizational priorities and improve outcomes.",
            para2Title: "Mobilize People: ",
            para2:
              "Managers inspire and motivate the people they lead. They manage the performance of their employees and provide constructive and respectful feedback to encourage and enable performance excellence. They lead by example, setting goals for themselves that are more demanding than those that they set for others.",
            para3Title: "Uphold Integrity and Respect: ",
            para3:
              "Managers exemplify ethical practices, professionalism and personal integrity, acting in the interest of Canada and Canadians. They create respectful, inclusive and trusting work environments where sound advice is valued. They encourage the expression of diverse opinions and perspectives, while fostering collegiality.",
            para4Title: "Collaborate with Partners and Stakeholders: ",
            para4:
              "Managers are deliberate and resourceful about seeking a wide spectrum of perspectives. In building partnerships, they manage expectations and aim to reach consensus. They demonstrate openness and flexibility to improve outcomes and bring a whole-of-organization perspective to their interactions. Managers acknowledge the role of partners in achieving outcomes.",
            para5Title: "Promote Innovation and Guide Change: ",
            para5:
              "Managers create an environment that supports bold thinking, experimentation and intelligent risk taking. When implementing change, managers maintain momentum, address resistance and anticipate consequences. They use setbacks as a valuable source of insight and learning.",
            para6Title: "Achieve Results: ",
            para6:
              "Managers ensure that they meet team objectives by managing resources. They anticipate, plan, monitor progress and adjust as needed. They demonstrate awareness of the context when making decisions. Managers take personal responsibility for their actions and the outcomes of their decisions."
          }
        }
      },

      // Background Page
      background: {
        hiddenTabNameComplementary: 'under "Background Information"',
        orgCharts: {
          link: "Image Description",
          ariaLabel: "Image description of the organization chart",
          treeViewInstructions:
            "Below is a tree view of the organization chart. Once selected, you can use the arrow keys to navigation, expand, and collapse information."
        }
      },

      // Inbox Page
      inboxPage: {
        tabName: 'under "In-Box"',
        emailId: " email ",
        subject: "Subject",
        to: "To",
        from: "From",
        date: "Date",
        addReply: "Add email response",
        addTask: "Add task list",
        yourActions: `You responded with {0} emails and {1} tasks`,
        editActionDialog: {
          addEmail: "Add email response",
          editEmail: "Edit email response",
          addTask: "Add task list",
          editTask: "Edit task",
          save: "Save response"
        },
        characterLimitReached: "(Limit reached)",
        emailCommons: {
          to: "To:",
          toFieldSelected: "To field selected.",
          cc: "CC:",
          ccFieldSelected: "Cc field selected.",
          currentSelectedPeople: "Current selected people are: {0}",
          currentSelectedPeopleAreNone: "None",
          reply: "Reply",
          replyAll: "Reply all",
          forward: "Forward",
          editButton: "Edit response",
          deleteButton: "Delete response",
          originalEmail: "Original email",
          toAndCcFieldsPlaceholder: "Select from address book",
          yourResponse: "Your response"
        },
        addEmailResponse: {
          selectResponseType: "Please select how you would like to respond to the original email:",
          response: "Your email response: {0} character limit",
          reasonsForAction: "Your reasons for actions (optional): {0} character limit",
          emailResponseTooltip: "Write a response to the email you received.",
          reasonsForActionTooltip:
            "Here, you can explain why you took a specific action in response to a situation if you feel you need to provide additional information",
          invalidToFieldError: "This field cannot be empty"
        },
        emailResponse: {
          title: "Email Response #",
          description: "For this response, you've chosen to:",
          response: "Your email response:",
          reasonsForAction: "Your reasons for action (optional):"
        },
        addEmailTask: {
          header: "Email #{0}: {1}",
          task: "Your task list response: {0} character limit",
          reasonsForAction: "Your reasons for actions (optional): {0} character limit"
        },
        taskContent: {
          title: "Task List Response #",
          task: "Your task list response:",
          taskTooltipPart1: "An action you intend to take to address a situation in the emails.",
          taskTooltipPart2: "Example: Planning a meeting, asking a colleague for information.",
          reasonsForAction: "Your reasons for action:",
          reasonsForActionTooltip:
            "Here, you can explain why you took a specific action in response to a situation if you feel you need to provide additional information"
        },
        deleteResponseConfirmation: {
          title: "Are you sure you want to cancel this response?",
          systemMessageDescription:
            "Your response will not be saved if you proceed. If you wish to save your answer, you may return to the response. All of your responses can be edited or deleted before submission.",
          description:
            'If you do not wish to save the response, click the "Delete response" button.'
        }
      },

      // Confirmation Page
      confirmationPage: {
        submissionConfirmedTitle: "Congratulations! Your test has been submitted.",
        feedbackSurvey:
          "We would appreciate your feedback on the assessment. Please fill out this optional {0} before logging out and leaving.",
        optionalSurvey: "15 minute survey",
        surveyLink: "https://surveys-sondages.psc-cfp.gc.ca/s/se.ashx?s=25113745259E9113&c=en-US",
        logout:
          "For security reasons, please ensure you log out of your account in the top right corner of this page. You may quietly gather your belongings and leave the test session. If you have any questions or concerns about your test, please contact {0}.",
        thankYou: "Thank you for completing your assessment. Good luck!"
      },

      // Quit Confirmation Page
      quitConfirmationPage: {
        title: "You have quit the test",
        instructionsTestNotScored1: "Your test ",
        instructionsTestNotScored2: "will not be scored.",
        instructionsRaiseHand:
          "Please raise your hand. The test administrator will come see you with further instructions.",
        instructionsEmail:
          "If you have any questions or concerns about your test, please contact {0}."
      },

      // Timeout Page
      timeoutConfirmation: {
        timeoutConfirmedTitle: "Your test time is over...",
        timeoutSaved:
          "Your responses have been saved and submitted for scoring. Please note that the information in the notepad is not saved.",
        timeoutIssue:
          'If there was an issue, please advise your test administrator. Click "Continue" to exit this test session and to receive further instructions.'
      },

      // Test tabs
      tabs: {
        instructionsTabTitle: "Instructions",
        backgroundTabTitle: "Background Information",
        inboxTabTitle: "In-Box",
        disabled: "You cannot access the inbox until you start the test."
      },

      // Test Footer
      testFooter: {
        timer: {
          timer: "Timer",
          timeLeft: "Time left in test session:",
          timerHidden: "Timer hidden.",
          timerHide: "Hide timer",
          timeRemaining: "Time remaining: {0} Hours, {1} Minutes, {2} Seconds"
        },
        pauseButton: "Pause Test",
        unpauseButton: "Resume Test",
        unpausePopup: {
          title: "Resume Test",
          description1: "Are you sure you want to resume your test?",
          description2:
            "The remaining time in your break bank will be re-calculated when you select Resume Test.",
          unpauseButton: "Resume Test"
        },
        submitTestPopupBox: {
          title: "Submit test",
          warning: {
            title: "Warning! The notepad will not be saved.",
            message1:
              "This will end your test session. Before submitting your answers, ensure that you have answered all the questions as you will not be able to go back to make changes. Anything written in the notepad will not be submitted with the test for scoring.",
            message2: "Do you want to proceed?"
          },
          description:
            "If you are ready to send your test in for scoring, click the “Submit Test” button. You will be exited out of this test session and provided further instructions."
        },
        quitTestPopupBox: {
          title: "Quit Test",
          description1:
            "Are you sure you want to quit this test? All answers will be deleted. You will not be able to recover your answers, and will lose access to the test.",
          description2: "To quit, you must acknowledge the following:",
          checkboxOne: "I choose to quit this test",
          checkboxTwo: "I understand that my test will not be scored",
          checkboxThree:
            "I am aware that there may be a delay before being retested, should I wish to take this test again"
        }
      }
    },

    superUserDashboard: {
      title: "Superuser Dashboard",
      sideNav: {
        celeryTab: "Celery Tasks",
        adminTab: "Site Admin"
      },
      settingsTab: {
        settingHeader: "Setting",
        actionHeader: "Action",
        emptyTableMsg: "No settings",
        invalidKey: "Invalid key format"
      },
      virtualMeetingRoomsTab: {
        title: "Teams Virtual Meeting Rooms Settings",
        table: {
          noData: "No Virtual Rooms created",
          column1: "Name",
          column2: "User ID",
          column3: "Delete"
        },
        addNewRoomButton: "New Room",
        deleteRoomButton: "Delete Room {0}",
        deleteRoomButtonTooltip: "Delete Room",
        addPopup: {
          title: "Add New Virtual Teams Room",
          desc: "Too add a room, please fill out the fields below",
          name: "Name",
          userId: "User ID",
          nameAlreadyExists: "This name already exists",
          nameInvalid: "Name should be the same format as an email",
          userIdInvalid: "User ID should follow the format 8 chars-4 chars-4 chars-12 chars"
        },
        deletePopup: {
          title: "Delete Virtual Teams Room",
          desc: "Are you sure you want to delete {0}? This action cannot be undone.",
          desc2: "Are you sure you want to continue?"
        }
      },
      celeryTasks: {
        title: "Celery",
        description:
          "Execute scheduled tasks in an ad-hoc manner to “force” the execution to resolve PROD level problems",
        popupText: "Check the docker logs to see the task execution",
        uitTab: {
          tabTitle: "UIT Tasks",
          resetCompletedProfileFlag: "Reset Completed Profile Flag",
          handleExpiredUitProcesses: "Handle Expired Uit Processes"
        },
        permissionTab: {
          tabTitle: "Permission Tasks",
          removeExpiredTestPermissions: "Remove Expired Test Permissions",
          deprovisionAfterInactivity: "Deprovision After Inactivity",
          deprovisionAdminAfterInactivityThirtyDays:
            "Deprovision Admin After Inactivity Thirty Days",
          removeExpiredTestScorerPermissions: "Remove Expired Test Scorer Permissions (540 days+)"
        },
        testTab: {
          tabTitle: "Test Tasks",
          updateNonSubmittedActiveAssignedTests: "Update Non Submitted Active Assigned Tests",
          invalidateLockedAssignedTests: "Invalidate Locked Assigned Tests",
          unassignReadyAndPreTestAssignedTests: "Unassign Ready And Pre Test Assigned Tests",
          unpauseAssignedTests: "Unpause Assigned Tests",
          expireOldAccommodations: "Expire Old Accomodations Requests"
        },
        table: {
          featureHeader: "Feature",
          actionHeader: "Action",
          runButton: "Run",
          emptyTableMsg: "No permissions"
        },
        celeryConfirmationPopup: {
          title: "Celery Task Executed",
          description: "Check the docker logs to see the task execution",
          closeButton: "Close"
        }
      },
      siteAdmin: {
        title: "Site Admin",
        paragraph: "Control features and functions for the site overall.",
        features: {
          title: "Features"
        },
        tfa: {
          title: "2 Factor Authenication"
        },
        virtualMeeting: {
          title: "Virtual Meeting Settings"
        },
        virtualMeetingRooms: {
          title: "Virtual Meeting Rooms Settings"
        }
      }
    },

    // Screen Reader
    ariaLabel: {
      mainMenu: "Main Menu",
      tabMenu: "eMIB Tab Menu",
      instructionsMenu: "Instructions Menu",
      languageToggleBtn: "language-toggle-button",
      authenticationMenu: "Authentication Menu",
      emailHeader: "email header",
      responseDetails: "response details",
      reasonsForActionDetails: "reasons for action details",
      taskDetails: "task details",
      emailOptions: "email options",
      taskOptions: "task options",
      taskTooltip: "task tooltip",
      emailResponseTooltip: "email response tooltip",
      reasonsForActionTooltip: "reasons for action tooltip",
      passwordConfirmationRequirements: "It must match your password",
      dobDayField: "Day field selected",
      dobMonthField: "Month field selected",
      dobYearField: "Year field selected",
      emailsList: "Emails list",
      questionList: "Question list",
      topNavigationSection: "Top navigation",
      sideNavigationSection: "Side navigation menu",
      quitTest: "Quit Test",
      selectedPermission: "Selected permission:",
      description: "Description:",
      pendingStatus: "Status pending:",
      scorerPanel: "Scorer Panel",
      navExpandButton: "Toggle navigation",
      images: {
        banner: "Banner",
        canadaLogo: "Canada",
        pscLogo: "Public Service Commission of Canada"
      }
    },

    // Commons
    commons: {
      psc: "Public Service Commission",
      loading: "Loading",
      nextButton: "Next",
      backButton: "Back",
      enterEmibSample: "Proceed to sample e-MIB test",
      enterEmibReal: "Continue to test instructions",
      resumeEmibReal: "Resume e-MIB",
      startTest: "Start test",
      resumeTest: "Resume test",
      confirmStartTest: {
        aboutToStart: "You are about to start the test.",
        breakBankTitle: "Break Bank:",
        breakBankWarning: "You have a {0} break bank for this test.",
        testSectionTimeTitle: "Total Time:",
        newtimerWarning: "You will have a total of {0} to complete the test.",
        additionalTimeWarning: "This includes {0} of additional time.",
        wishToProceed: "Do you wish to proceed?",
        timeUnlimited: "unlimited time",
        confirmProceed: "Yes, I wish to proceed."
      },
      submitTestButton: "Submit test",
      quitTest: "Quit Test",
      returnHome: "Return to Home Page",
      returnToTest: "Return to test",
      returnToResponse: "Return to response",
      passStatus: "Pass",
      failStatus: "Fail",
      error: "Error",
      success: "Success",
      info: "Information",
      warning: "Warning!",
      enabled: "Enabled",
      disabled: "Disabled",
      backToTop: "Back to top",
      notepad: {
        title: "Notepad",
        hideNotepad: "Hide notepad",
        placeholder: "Put your notes here..."
      },
      calculator: {
        title: "Calculator",
        errorMessage: "Error",
        calculatorResultAccessibility: "Calculator diplays",
        backspaceButton: "Backspace",
        hideCalculator: "Hide calculator"
      },
      cancel: "Cancel",
      accept: "Accept",
      cancelChanges: "Cancel changes",
      cancelResponse: "Cancel response",
      addButton: "Add",
      editButton: "Edit",
      deleteButton: "Delete",
      saveButton: "Save",
      saveDraftButton: "Save",
      applyButton: "Apply",
      duplicateButton: "Duplicate",
      previewButton: "Preview",
      close: "Close",
      login: "Login",
      ok: "OK",
      deleteConfirmation: "Confirm Deletion",
      continue: "Continue",
      sendRequest: "Send request",
      submit: "Submit",
      na: "N/A",
      valid: "Valid",
      invalid: "Invalidated",
      confirm: "Confirm",
      english: "English",
      french: "French",
      bilingual: "Bilingual",
      // do not forget to update the text_resources.py file (assigned_test_status) if updates are made for those statuses
      status: {
        checkedIn: "Checked-in",
        preTest: "Pre-Test",
        active: "Testing",
        transition: "In Transition",
        locked: "Locked",
        paused: "Paused",
        neverStarted: "Not Started Yet",
        inactivity: "Inactivity",
        timedOut: "Timed Out",
        quit: "Quit",
        submitted: "Submitted",
        unassigned: "Unassigned",
        invalid: "Invalid",
        pending: "Pending",
        cannotBeShared: "Cannot be shared"
      },
      reservationCodeStatus: {
        // codename ==> reserved
        consumed: "Claimed",
        booked: "Reserved",
        revoked: "Revoked",
        withdrawn: "Withdrawn",
        sendUpdates: "Send Updates",
        sent: "Sent"
      },
      seconds: "second(s)",
      minutes: "minute(s)",
      hours: "hour(s)",
      pleaseSelect: "Please select",
      none: "None",
      convertedScore: {
        invalidScoreConversion: "Invalid Score Conversion",
        pass: "Pass",
        fail: "Fail",
        none: "No Score",
        notScored: "Not Scored",
        invalid: "Invalid"
      },
      yes: "Yes",
      no: "No",
      preferNotToAnswer: "Prefer not to say",
      active: "Active",
      inactive: "Inactive",
      moveUp: "Move Up",
      moveDown: "Move Down",
      default: "Default",
      tools: "Tools",
      genericTable: {
        maximumAllowedResultsExceededMessage:
          "Too many results ( >{0} ). Use the search tool to reduce the number of results."
      },
      pagination: {
        nextPageButton: "Next page",
        previousPageButton: "Previous page",
        skipToPagesLink: "Skip to list of pages",
        breakAriaLabel: "Move multiple pages"
      },
      saved: "Saved",
      fieldCannotBeEmptyErrorMessage: "This field cannot be empty",
      currentLanguage: "en",
      true: "TRUE",
      false: "FALSE",
      next: "Next",
      previous: "Previous",
      upload: "Upload",
      all: "All",
      generate: "Regenerate",
      unchanged: "(unchanged)",
      discard: "Discard",
      never: "Never",
      indefinite: "Inderterminate",
      sent: "Sent",
      testType: {
        uit: "UIT",
        supervised: "Supervised"
      },
      approve: "Approve",
      protected: "PROTECTED",
      agree: "I agree",
      daysOfWeek: {
        monday: "Monday",
        tuesday: "Tuesday",
        wednesday: "Wednesday",
        thursday: "Thursday",
        friday: "Friday",
        saturday: "Saturday",
        sunday: "Sunday"
      }
    }
  },

  fr: {
    // Main Tabs
    mainTabs: {
      homeTabTitleUnauthenticated: "Accueil",
      homeTabTitleAuthenticated: "Accueil",
      dashboardTabTitle: "Tableau de bord",
      sampleTest: "Échantillon de la BRG-e",
      sampleTests: "Exemples de tests",
      statusTabTitle: "Statut",
      psc: "Commission de la fonction publique du Canada",
      canada: "Gouvernement du canada",
      skipToMain: "Passer au contenu principal",
      menu: "MENU"
    },

    // HTML Page Titles
    titles: {
      CAT: "OÉC/CAT - CFP/PSC",
      sampleTests: "OÉC - Exemples de tests",
      eMIBOverview: "Aperçu de la BRG-e",
      eMIB: "Évaluation BRG-e ",
      uitTest: "OÉC - Test\u00A0: {0}",
      status: "OÉC - État du system",
      home: "OÉC - Accueil",
      homeWithError: "Erreur OÉC - Accueil",
      profile: "OÉC - Profil",
      systemAdministration: "OÉC - Activités d’affaires",
      rdOperations: "OÉC - Activités R&D",
      systemAdminAndRdOperations: "OÉC - Activités ",
      testAdministration: "OÉC - Administrateur de tests",
      testBuilder: "OÉC - Constructeur de tests",
      itemBankPermissions: "OÉC - Accès à la banque d'items",
      itemBankEditor: "OÉC - Éditeur de banque d'items",
      itemEditor: "OÉC - Éditeur d'items",
      ppcAdministration: "OÉC - Tableau de bord de R&D",
      testCenterManager: "OÉC - Coordonnateur de centre de tests",
      resetPassword: "OÉC - Réinitialiser le mot de passe",
      inPersonTesting: "OÉC - Coordonnateur RH",
      aae: "OÉC - Demande de mesures d'adaptation en matière d'évaluation",
      consultationServices: "OÉC - Services de consultation",
      scorer: "OEC - Évaluateur"
    },

    // Site Nav Bar
    siteNavBar: {
      settings: "Affichage"
    },

    accommodations: {
      notificationPopup: {
        title: "Paramètres d'affichage pour l'accessibilité",
        description1: "Les paramètres d'affichage utilisés actuellement ont été sauvegardés.",
        description2:
          "Lors de votre prochaine connexion, ces paramètres seront automatiquement appliqués."
      },
      defaultFonts: {
        fontSize: "16px (défaut)",
        fontFamily: "Nunito Sans (défaut)"
      }
    },

    // authentication
    authentication: {
      login: {
        title: "Ouvrir une session",
        content: {
          title: "Ouvrir une session",
          inputs: {
            emailTitle: "Adresse courriel ou nom d'utilisateur\u00A0:",
            passwordTitle: "Mot de passe\u00A0:",
            showPassword: "Afficher le mot de passe"
          }
        },
        button: "Ouvrir une session",
        forgotPassword: "Avez-vous oublié votre mot de passe ?",
        forgotPasswordPopup: {
          title: "Avez-vous oublié votre mot de passe ?",
          description:
            "Veuillez inscrire l’adresse courriel du compte qui nécessite une réinitialisation du mot de passe.",
          emailAddressLabel: "Adresse courriel\u00A0:",
          sendResetLinkButton: "Envoyer un lien pour réinitialiser le mot de passe",
          invalidEmailAddressErrorMessage: "Doit être une adresse courriel valide",
          usernameDoesNotExistErrorMessage:
            "L'adresse courriel fournie ne correspond pas à un compte actif",
          emailSentSuccessfully: "Un courriel a été envoyé à cette adresse",
          emailSentSuccessfullMessage:
            "Si vous n'avez pas reçu le courriel, vérifiez l’adresse courriel et cliquez sur le bouton «\u00A0Renvoyer le courriel\u00A0» ci-dessous.",
          resendEmail: "Renvoyer le courriel"
        },
        invalidCredentials: "Adresse courriel et/ou mot de passe invalide",
        passwordFieldSelected: "Champ Mot de passe sélectionné"
      },
      createAccount: {
        title: "Créer un compte",
        content: {
          title: "Créer un compte",
          description: "Veuillez remplir le formulaire suivant pour créer un compte.",
          inputs: {
            valid: "Valide",
            firstNameTitle: "Prénom\u00A0:",
            firstNameError: "Le prénom contient un ou plusieurs caractères invalides",
            lastNameTitle: "Nom de famille\u00A0:",
            lastNameError: "Le nom de famille contient un ou plusieurs caractères invalides",
            usernameTitle: "Nom d'utilisateur (facultatif)\u00A0:",
            usernameAlreadyUsedError: "Ce nom d'utilisateur a déjà été utilisé",
            usernameAlreadyUsedInEmailError:
              "Ce nom d'utilisateur est déjà associé à l'adresse courriel d'un autre compte.",
            dobDayTitle: "Date de naissance\u00A0:",
            dobError: "Doit être une date valide",
            psrsAppIdTitle: "Numéro d'identification du SRFP (s'il y a lieu)\u00A0:",
            psrsAppIdError: "Doit être un numéro d'identification du SRFP valide",
            emailTitle: "Adresse courriel\u00A0:",
            emailError: "Doit être une adresse courriel valide",
            emailAlreadyUsedInUsernameError:
              "Cette adresse courriel est déjà associée à un autre compte.",
            priTitle: "CIDP (s'il y a lieu)\u00A0:",
            militaryNbrTitle: "Numéro de service (s'il y a lieu)\u00A0:",
            priError: "Doit être un CIDP valide",
            militaryNbrError: "Doit être un numéro de matricule valide",
            passwordTitle: "Mot de passe\u00A0:",
            showPassword: "Afficher le mot de passe",
            passwordErrors: {
              description: "Votre mot de passe doit contenir les éléments suivants\u00A0:",
              upperCase: "Au moins une lettre majuscule",
              lowerCase: "Au moins une lettre minuscule",
              digit: "Au moins un chiffre",
              specialCharacter: "Au moins un caractère spécial (#?!@$%^&*-)",
              length: "Un minimum de 8 caractères et un maximum de 15 caractères"
            },
            passwordConfirmationTitle: "Confirmer le mot de passe\u00A0:",
            showPasswordConfirmation: "Afficher la confirmation du mot de passe",
            passwordConfirmationError:
              "La confirmation de votre mot de passe doit correspondre à votre nouveau mot de passe"
          }
        }
      },
      twoFactorAuth: {
        title: "Authentification à deux facteurs",
        content: {
          title: "Vérifier votre courriel pour un code de sécurité",
          description: "Entrez le code dans un délai de 15 minutes.",
          label: "Code de sécurité",
          newCode: "Un nouveau code a été envoyé à votre adresse courriel",
          resend: "Envoyer un nouveau code",
          invalidCode:
            "Le code de sécurité fourni ne correspond pas à nos registres. Revérifiez votre courriel ou demandez un nouveau code.",
          expiredCode:
            "FR This code has expired. Please resend the code and try again within 15 minutes."
        },
        privacyNotice:
          "J'ai lu {0} et j'accepte la manière dont la Commission de la fonction publique recueille, utilise et communique les renseignements personnels.",
        privacyNoticeLink: "l'énoncé de confidentialité",
        privacyNoticeError: "Vous devez accepter l’avis de confidentialité en cliquant sur la case",
        button: "Créer un compte",
        accountAlreadyAssociatedToThisEmailError:
          "Un compte est déjà associé à cette adresse de courriel",
        passwordTooCommonError: "Ce mot de passe est trop commun",
        passwordTooSimilarToUsernameError:
          "Le mot de passe est trop semblable au nom d’utilisateur",
        passwordTooSimilarToFirstNameError: "Le mot de passe est trop semblable au prénom",
        passwordTooSimilarToLastNameError: "Le mot de passe est trop semblable au nom de famille",
        passwordTooSimilarToEmailError: "Le mot de passe est trop semblable au courriel"
      },
      resetPassword: {
        title: "Réinitialiser le mot de passe",
        content: {
          title: "Réinitialiser le mot de passe",
          newPassword: "Nouveau mot de passe\u00A0:",
          showNewPassword: "Afficher le nouveau mot de passe",
          confirmNewPassword: "Confirmer le nouveau mot de passe\u00A0:",
          showConfirmNewPassword: "Afficher la confirmation du nouveau mot de passe",
          button: "Réinitialiser le mot de passe"
        },
        popUpBox: {
          title: "Réinitialisation du mot de passe réussie",
          description: "Cliquez sur «\u00A0OK\u00A0» pour ouvrir une session."
        }
      }
    },

    // Menu Items
    menu: {
      scorer: "Correcteur de test",
      etta: "Activités d’affaires",
      rdOperations: "Activités R&D",
      operations: "Activités",
      ppc: "Tableau de bord de R&D",
      testCenterManager: "Coordonnateur de centre de tests",
      ta: "Administrateur de tests",
      testBuilder: "Développeur de test",
      inPersonTesting: "Coordonateur RH",
      checkIn: "M'enregistrer",
      profile: "Mon profil",
      incidentReport: "Rapports d'incidents",
      MyTests: "Mes tests",
      ContactUs: "Nous joindre",
      logout: "Fermer la session",
      aae: "Mesures d’adaptation en matière d’évaluation",
      consultationServices: "Services de consultation"
    },

    // Token Expired Popup Box
    tokenExpired: {
      title: "Session expirée",
      description:
        "Votre session a pris fin en raison de votre inactivité. Veuillez entrer votre adresse courriel et votre mot de passe pour ouvrir une nouvelle session."
    },

    // Home Page
    homePage: {
      welcomeMsg: "Bienvenue dans l'Outil d'évaluation des candidats",
      description:
        "Cette application est utilisée pour évaluer les candidats pour des postes au sein de la fonction publique fédérale. Veuillez ouvrir une session pour accéder à un test. Si vous n'avez pas de compte, veuillez cliquer sur le lien «\u00A0Créer un compte\u00A0» pour vous inscrire.",
      description2fa:
        "Ce site web utilise une authentification à deux facteurs pour les utilisateurs disposant de permissions supplémentaires. Cette étape permet d'assurer la sécurité de l'Outil d'évaluation des candidats en vérifiant votre compte chaque fois que vous ouvrez une session."
    },

    // Sample Tests Page
    sampleTestDashboard: {
      title: "Exemples de tests",
      paragraph1p1: "Les ",
      paragraph1bold: "exemples de test",
      paragraph1p2:
        " ont pour but de présenter le format des questions du test réel, de vous permettre de tester et modifier les paramètres d’affichage selon vos préférences, ainsi que de pratiquer la navigation sur la plateforme de l’Outil d’évaluation du candidat. Ils ne sont pas destinés à évaluer votre niveau de performance et aucune note de ne vous sera attribuée. Vous pouvez repasser ces exemples de tests autant de fois que vous le souhaitez.",
      paragraph2: "Cliquez sur « Commencer » pour accéder à l’exemple de test sélectionné.",
      paragraph3:
        "Veuillez-vous assurer de compléter le test réel qui vous a été assigné avant la date limite, comme indiqué dans votre courriel d’invitation.",
      table: {
        columnOne: "Exemple de test",
        columnTwo: "Action",
        viewButton: "Commencer",
        viewButtonAccessibilityLabel: "Commencer le test {0}."
      }
    },

    // Retest Period Warning
    retestPeriodWarningPopup: {
      dashboardTitle: "Commencer le test durant la période d'attente d'un test précédent\u00A0?",
      bookingTitle:
        "Réserver une séance de test durant la période d'attente d'un test précédent\u00A0?",
      systemMessageDescription:
        "Vous devez respecter la période d’attente si vous avez fait ce test précédemment, peu importe son format (non supervisé ou en personne, en ligne ou papier). Si vous repassez ce test avant la fin de cette période d'attente, votre résultat ne sera pas valide et une nouvelle période d'attente sera fixée, à compter de la date de votre test le plus récent.",
      description: "Êtes-vous sûr de vouloir poursuivre\u00A0?",
      checkboxLabel: "Oui, je souhaite poursuivre."
    },

    accommodationsRequestDetailsPopup: {
      viewAccommodationsRequestDetails: "Voir les détails de la demande de mesures d'adaptation",
      viewAccommodationsRequestDetailsAccessibilityLabel:
        "Voir les détails de la demande de mesures d'adaptation pour le test {0}",
      viewAccommodationsRequestDetailsAccessibilityLabelOf:
        "Voir les détails de la demande de mesures d'adaptation de {0}",
      viewAlternateTestRequestDetails: "Afficher les détails de la demande de test alternatif",
      viewAlternateTestRequestDetailsAccessibilityLabel:
        "Afficher les détails de la demande de test alternatif pour le {0}",
      viewAlternateTestRequestDetailsAccessibilityLabelOf:
        "Afficher les détails de la demande de test alternatif de {0}",
      title: "Détails de la demande de mesures d'adaptation",
      titleAlternateTestRequest: "Détails de la demande de test alternatif",
      description: "Voici les détails de votre demande de mesures d'adaptation pour le test {0}.",
      descriptionAlternateTestRequest1: "Utilisateur: {0} {1} ({2})",
      descriptionAlternateTestRequest2: "Test : {0}",
      descriptionAlternateTestRequest3:
        "Vous trouverez ci-dessous les détails de votre demande de test alternatif.",
      requestDateLabel: "Date de la demande\u00A0:",
      requestNumberLabel: "Numéro de la demande :",
      requestStatusLabel: "Statut de la demande\u00A0:",
      phoneNumberLabel: "Numéro de téléphone :",
      aaeExpertLabel: "Spécialiste en évaluation:",
      aaeExpertEmailLabel: "Coordonnées du spécialiste en évaluation:",
      hrContactInfoLabel: "Coordonnées des RH :",
      generalInfo: {
        generalInfoLabel: "Informations générales\u00A0:",
        generalInfoContent: {
          introductionHeader: "Renseignements généraux",
          introductionParagraph1:
            "Les mesures d'adaptation en matière d’évaluation recommandées dans ce rapport ont été émises exclusivement pour la personne évaluée et pour une seule passation du test. Ce rapport doit rester strictement confidentiel et n’être utilisé qu’à des fins d’évaluation. De plus, le rapport ne doit pas être utilisé pour un autre test, y compris une reprise (repasser le même test).",
          introductionParagraph2:
            "Si la personne évaluée est invitée à passer le même test et nécessite des mesures d’adaptation, elle doit soumettre une nouvelle demande de mesures d’adaptation en matière d’évaluation dans l’Outil d’évaluation des candidats (OEC).",
          introductionParagraph3:
            "Veuillez noter que seuls les spécialistes des mesures d’adaptation en matière d’évaluation du Centre de psychologie du personnel (CPP) peuvent modifier ce rapport. Toute modification apportée par une source non autorisée rendra le rapport invalide. De plus, les mesures d’adaptation en matière d’évaluation qui ne sont pas incluses dans ce rapport ne sont pas recommandées.",
          importantNotesHeader: "Notes importantes sur les rôles et responsabilités",
          importantNotesParagraph1:
            "Lorsqu’un test standardisé de la Commission de la fonction publique (CFP) est utilisé, les mesures d’adaptation recommandées par le CPP sont obligatoires et doivent être appliquées telles que décrites, qu’elles soient administrées par la CFP ou par l’organisation. ",
          importantNotesParagraph2:
            "Lorsqu’une mesure d’adaptation est demandée dans le cadre d’un test élaboré par l’organisation (p. ex. entrevue, simulation, test virtuel), le CPP formulera des recommandations de mesures d’adaptation. L’organisation est responsable de mettre en œuvre ces recommandations.",
          candidateDeclarationHeader: "Déclaration de la personne évaluée ",
          candidateDeclarationParagraph:
            "J'ai lu et compris les recommandations de mesures d'adaptation en matière d’évaluation contenues dans ce rapport et j'accepte qu'elles soient appliquées lors de mon test."
        }
      },
      testToAdministerLabel: "Test à administrer :",
      timingAndBreaksTitle: "Temps et pauses :",
      totalTestTimeLabel: "Durée totale du test\u00A0:",
      additionalTimeDetails: "comprend {0} minutes additionnelles",
      totalBreakBankLabel: "Total de la banque de temps de pause :",
      otherMeasuresLabel: "Autres mesures :",
      cancelRequestButton: "Annuler la demande",
      cancelRequestConfirmationPopup: {
        title: "Êtes-vous sûr de vouloir annuler votre demande?",
        systemMessageDescription:
          "Votre demande de mesures d'adaptation sera annulée. Cette action est irréversible!",
        systemMessageDescriptionAlternateTestRequest:
          "Votre demande de test alternatif sera annulée. Cette action est irréversible!",
        description: "Êtes-vous sûr de vouloir continuer?"
      },
      approvalSuccessPopup: {
        title: "Acceptée avec succès",
        systemMessageDescription:
          "Votre demande de mesures d'adaptation a été acceptée avec succès."
      },
      printDownload: "Imprimer ou Télécharger",
      printDownloadDescription: "{0}",
      printDocumentTitle: "Détails de la demande de mesures d'adaptation",
      printComponent: {
        draftWatermark: "-APERÇU-",
        title: "CENTRE DE PSYCHOLOGIE DU PERSONNEL (CPP)",
        reportSection: {
          title: "Rapport de mesures d'adaptation en matière d'évaluation",
          candidateNameLabel: "Nom de la personne évaluée\u00A0:",
          candidateOlaPhoneNumberLabel: "Numéro de téléphone de la personne évaluée\u00A0:",
          referenceNumberLabel: "Numéro de référence\u00A0:",
          testSkillLabel: "Type de test\u00A0:",
          testNameLabel: "Nom de l'évaluation\u00A0:",
          testTimeLabel: "Durée totale du test\u00A0:",
          breakBankLabel: "Banque de temps de pause\u00A0:",
          testCenterLabel: "Centre de test\u00A0:",
          requestingOrganizationLabel: "Ministère ou organisme\u00A0:",
          primaryContactLabel: "Personne-ressource principale\u00A0:",
          primaryContactPhoneNbrLabel: "Numéro de téléphone\u00A0:",
          primaryContactEmailLabel: "Courriel\u00A0:",
          aaeExpertContactLabel: "Élaboré par\u00A0:",
          requestCompletedDate: "Date\u00A0:"
        },
        canadaLogoAriaLabel: "Logo de la Commission de la fonction publique du Canada",
        accommodationsSection: {
          title: "Mesures d'adaptation",
          generalInfoSection: {
            paragraph1: "Note à la personne évaluée",
            paragraph2:
              "Veuillez noter que le ou la spécialiste en évaluation ayant rédigé ce rapport demeure disponible pour la personne évaluée afin de discuter des mesures adaptations recommandées, si nécessaire. Ses coordonnées se trouvent à la dernière page de ce rapport.",
            paragraph3:
              "Lorsque votre test sera mis à l'horaire, vous recevrez des informations additionnelles concernant la plateforme et comment y accéder."
          },
          otherMeasures: {
            title: "Autres mesures"
          }
        },
        contactInfoSection: {
          title: "Coordonnées pour communiquer avec nous",
          allInquiries: {
            description: "Pour toute demande concernant ce rapport, communiquez avec\u00A0:",
            role: "Spécialiste en évaluation",
            unit: "Unité d'adaptation en matière d'évaluation",
            ppcDescription: "Centre de psychologie du personnel (CPP)",
            phoneNumber: "Numéro de téléphone\u00A0: {0}",
            email: "Courriel\u00A0: {0}"
          },
          generalInquiries: {
            description: "Pour toute demande générale, communiquez avec\u00A0:",
            unit: "Unité d'adaptation en matière d'évaluation",
            phoneNumber: "Numéro de téléphone\u00A0: (819) 420-8690",
            email: "Courriel\u00A0: cfp.ae-aa.psc@cfp-psc.gc.ca"
          }
        }
      }
    },

    // Terms of Use
    termsOfUse: {
      general: {
        title: "Renseignements généraux",
        para1:
          "Ceci constitue une entente entre vous et la Commission de la fonction publique du Canada (CFP) concernant votre participation à l’évaluation linguistique à l’oral.",
        para2:
          "Pour procéder à la mise à l’horaire de votre évaluation, vous devez accepter les présentes conditions d'utilisation sans restriction ni réserve. Vous devez confirmer votre acceptation en cochant la case située au bas de cette entente et en cliquant sur le bouton « Suivant ».",
        para3:
          "Si la CFP a des raisons de douter de la validité de votre résultat au test ou de votre respect des présentes conditions, vous pourriez devoir repasser le test à une date ultérieure."
      },
      definitions: {
        title: "Définitions",
        para1Bold: "Contenu du test\u00A0:",
        para1:
          " L'ensemble des questions posées lors du test, les réponses fournies par les candidats à ces questions, et toute information connexe y compris toutes notes, descriptions ou enregistrements."
      },
      acceptableUses: {
        title: "Utilisation autorisée",
        para1:
          "L'accès au contenu du test et son utilisation sont strictement réservés aux fins suivantes\u00A0:",
        bullet1:
          "Vous permettre de démontrer vos compétences par rapport à la qualification évaluée tout en respectant les conditions de test standardisé.",
        bullet2:
          "Vous permettre de répondre aux questions du test en toute bonne foi et au mieux de vos capacités, tout en respectant les instructions que vous recevrez avant de passer le test."
      },
      prohibitedUses: {
        title: "Utilisation interdite",
        para1:
          "En dehors des utilisations autorisées spécifiées précédemment, toute autre utilisation du test est strictement interdite.",
        para2:
          "Les utilisations interdites comprennent, sans s'y limiter, les exemples suivants\u00A0:",
        bullet1:
          "Enregistrer le test de n’importe quelle manière, y compris via des appareils ou des logiciels.",
        bullet2: "Reproduire, collecter le contenu ou des renseignements liés au test.",
        bullet3:
          "Conserver tout élément du contenu du test, sous n’importe quelle forme, une fois le test terminé.",
        bullet4:
          "Effectuer le test avec l'aide d'une autre personne ou de ressources (notes préparées à l’avance, livres, dictionnaires, lexiques en format imprimé ou électronique, sites Web, outils de traduction en format imprimé ou électronique, outils ou logiciels d’intelligence artificielle, médias sociaux, générateur de sous-titres etc.).",
        bullet5:
          "Partager ou discuter avec d'autres personnes, de n’importe quelle façon, du contenu ou des renseignements liés au test, y compris des notes prises pendant le test.",
        bullet6:
          "Délibérément utiliser des informations à mauvais escient pour procurer un avantage ou un désavantage, à vous-même, à n’importe quel candidat ou à un candidat potentiel, afin d’influencer un processus de nomination à la fonction publique fédérale.",
        bullet7: "Exploiter commercialement le test ou son contenu de n’importe quelle façon.",
        bullet8: "Utiliser le test à des fins illicites, trompeuses ou malveillantes.",
        bullet9: "Faciliter ou encourager toute violation des présentes conditions d'utilisation."
      },
      security: {
        title: "Sécurité",
        para1:
          "L'ensemble du contenu du test est protégé par les droits d'auteur de la CFP et est désigné « Protégé B».",
        para2:
          "La CFP a mis en place des mesures de sécurité en vue d'assurer la protection et la confidentialité des renseignements que vous fournissez et de veiller à ce que votre séance de test en ligne soit sécurisée. Toutefois, vous avez l’obligation de respecter les règles suivantes\u00A0:",
        bullet1:
          "L'accès au contenu du test est strictement réservé à votre usage personnel (voir la section « Utilisation autorisée »).",
        bullet2:
          "Ne pas enregistrer le test, dans son intégralité ou en partie, sous quelque forme que ce soit.",
        bullet3:
          "Pendant toute la durée du test, vous devez être seul dans un endroit calme où vous ne serez pas dérangé et où vos échanges avec la personne qui administre le test ne pourront pas être entendus ou enregistrés.",
        bullet4:
          "Vous devez passer le test dans un endroit où vous vous sentirez à l'aise d'être supervisé(e) via vidéoconférence par la personne qui administre le test. Veillez à ce que l'arrière-plan visible à l'image soit aussi neutre que possible.",
        bullet5:
          "Vous devez immédiatement avertir la personne qui administre votre test ou le Centre de psychologie du personnel ({0}) de toute atteinte réelle ou potentielle à la sécurité du test.",
        bullet5Link: "cfp.cpp-ppc.psc@cfp-psc.gc.ca"
      },
      intellectualPropertyRights: {
        title: "Droits de propriété intellectuelle",
        para1:
          "L'autorisation de reproduction à des fins non commerciales figurant dans les « Avis importants » sur les pages du site Web de la CFP accessibles au public ne s'applique pas au test. Elle est remplacée par les dispositions suivantes\u00A0:",
        bullet1: "Tout le contenu du test est protégé par les droits d'auteur de la CFP.",
        bullet2:
          "Vous pouvez uniquement consulter et utiliser le test aux fins spécifiées dans la section « Utilisation autorisée ».",
        bullet3:
          "Toute copie, reproduction, traduction, distribution ou diffusion du contenu du test, en tout ou en partie, sous toute forme ou par quelque moyen que ce soit, est strictement interdite."
      },
      ensuringTheIntegrityOfTheTestingProcess: {
        title: "Garantir l'intégrité du processus de test",
        bullet1:
          "La CFP se réserve le droit, à sa discrétion, de prendre toutes les mesures qu'elle juge nécessaires pour s'assurer que les résultats des tests soient fiables, valides et équitables.",
        bullet2:
          "Ces mesures pourraient être prises si des irrégularités sont constatées lors de votre test, ou si la CFP a des raisons de douter du respect des présentes conditions d'utilisation et de la mise en place de conditions de test standardisé.",
        bullet3:
          "Les mesures possibles incluent l’interruption anticipée de votre test, la suspension ou l'invalidation de votre résultat et l'imposition de conditions visant à accroître la sécurité des évaluations futures (par exemple, l'obligation d’effectuer le test en personne).",
        bullet4:
          "La CFP pourrait également procéder à des audits de son programme d’évaluation. Dans ce contexte, votre résultat de test pourrait être invalidé ou suspendu, et vous pourriez être soumis à un nouveau test dans des conditions de sécurité renforcées (par exemple, un test en personne).",
        para1:
          "Le ministère qui a demandé votre test pourrait être informé de toute mesure prise par la CFP à l’égard de votre test. Le ministère sera responsable de vous transmettre les informations pertinentes.",
        para2:
          "Si vous avez des questions ou des préoccupations concernant les mesures prises à l'égard de votre test, veuillez contacter le Centre de psychologie du personnel de la CFP à l'adresse électronique suivante\u00A0: {0}.",
        para2Link: "cfp.cpp-ppc.psc@cfp-psc.gc.ca"
      },
      consequencesOfABreachOfTermsOfUse: {
        title: "Conséquences d’une infraction aux conditions d'utilisation",
        bullet1:
          "La CFP se réserve le droit, à sa seule discrétion, d'intenter toute action en justice à votre encontre, y compris, mais sans s’y limiter, la suspension de vos résultats, le rejet de votre candidature, ou une plainte pour violation des droits d'auteur et non-respect de l'entente de confidentialité, si vous contrevenez aux présentes conditions d'utilisation.",
        bullet2:
          "Toute personne impliquée dans la conservation, la divulgation ou l'utilisation non autorisée du contenu des examens de la CFP, y compris le contenu de ce test, pourrait faire l'objet d'une enquête en vertu de la Loi sur l'emploi dans la fonction publique (LEFP). Les personnes reconnues coupables de fraude pourraient voir leur dossier renvoyé à la Gendarmerie royale du Canada (GRC).",
        bullet3:
          "Le contenu intégral du test est classifié « Protégé B ». Toute reproduction, enregistrement et/ou divulgation non autorisé de ce contenu contrevient à la {0} et pourra faire l’objet d’une enquête dans le cadre de cette politique.",
        bullet3LinkLabel: "Politique sur la sécurité du gouvernement",
        bullet3Link: "https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=16578",
        bullet4:
          "Toute tentative de tricherie présumée sera systématiquement transmise aux autorités compétentes, telles que la Direction générale des enquêtes de la CFP ou les responsables du ministère ayant demandé votre évaluation. Par conséquent, elle pourra faire l’objet d’une enquête en vertu de la LEFP. Les personnes reconnues coupables de fraude pourraient voir leur dossier renvoyé à la GRC."
      },
      checkboxLabel: "J’ai lu et j’accepte ces conditions d’utilisation."
    },

    privacyNoticeStatementOLA: {
      para1:
        "Les renseignements personnels servent à fournir des services d’évaluation aux clients du Centre de psychologie du personnel. Ils sont recueillis à des fins de dotation, conformément aux articles 11, 30 et 36 de la {0}. Dans le cas des organisations qui ne sont pas assujetties à cette loi, les renseignements personnels sont recueillis en vertu de la loi habilitante de l’organisation concernée ainsi que de l’article 35 de la {1}. Les résultats aux tests d’évaluation de langue seconde seront divulgués aux responsables de l’organisation autorisés. Les résultats obtenus à tous les autres examens seront communiqués exclusivement à l’organisation qui en a fait la demande. Les résultats obtenus aux examens pourraient être communiqués à la Direction des enquêtes de la Commission de la fonction publique si une enquête a lieu en vertu des articles 66 ou 69 de la {2}. Vos renseignements pourraient également être utilisés à des fins de recherche statistique et analytique. En vertu du paragraphe 8(2) de la {3}, des renseignements pourraient, dans certains cas, être divulgués sans votre autorisation. La communication de vos renseignements personnels est volontaire, mais si vous choisissez de ne pas fournir ces renseignements, il se peut que vous ne puissiez pas recevoir les services du Centre de psychologie du personnel.",
      para1Link1Label: "Loi sur l'emploi dans la fonction publique",
      para1Link1: "https://laws-lois.justice.gc.ca/fra/lois/p-33.01/",
      para1Link2Label: "Loi sur la protection des renseignements personnels",
      para1Link2: "https://laws-lois.justice.gc.ca/fra/lois/p-21/index.html",
      para2:
        "Les renseignements sont recueillis et utilisés de la façon décrite dans le fichier de renseignements personnels du Centre de psychologie du personnel (CFP PCU 025), qui se trouve dans {0} de la Commission de la fonction publique.",
      para2LinkLabel: "l'Info Source",
      para2Link:
        "https://www.canada.ca/fr/commission-fonction-publique/organisation/propos-nous/bureau-acces-information-protection-renseignements-personnels/info-source.html#122",
      para3:
        "Vous avez le droit d’accéder à vos renseignements personnels et de les corriger, ainsi que de demander qu’ils soient modifiés si vous croyez qu’ils sont erronés ou incomplets. Vous avez également le droit de porter plainte auprès du {0} au sujet du traitement de vos renseignements personnels.",
      para3LinkLabel: "Commissaire à la protection de la vie privée du Canada",
      para3Link: "https://www.priv.gc.ca/fr/",
      recording: {
        title: "Enregistrement du test",
        para1:
          "Votre test sera enregistré par la CFP à des fins administratives. Le contenu de l’enregistrement est protégé en vertu de la Loi sur la protection des renseignements personnels et l’enregistrement sera conservé pendant 2 ans. Aucun autre enregistrement de votre test n’est autorisé."
      },
      checkboxLabel: "J’ai lu cet énoncé de confidentialité."
    },

    // Dashboard Page
    dashboard: {
      title: "Bienvenue {0} {1}.",
      description:
        "Pour commencer votre test, cliquez sur le bouton M'enregistrer ci-dessous, et entrez votre code d'accès au test.",
      lastLogin: "[Date de la dernière ouverture de session\u00A0: {0}]",
      lastLoginNever: "jamais",
      sideNavItems: {
        takeATest: "Passer un test",
        reserveSeat: {
          title: "Réservations",
          text: "Utilisez le code de réservation fourni dans votre courriel d'invitation pour rechercher des séances de test supervisé disponibles.",
          makeReservation: {
            title: "Effectuer une réservation",
            reservationCode: "Code de réservation\u00A0:",
            reservationCodeHelp: "Inscrivez votre Code de réservation débutant par les lettres RES",
            invalidReservationCode:
              "Ce code n'est pas valide ou a expiré. Si vous tentez de passer un test, sélectionnez «passer un test» dans le menu de gauche puis cliquez sur le bouton «m'enregistrer».",
            reservationCodeAlreadyUsed: "Le code de réservation a déjà été utilisé",
            searchBar: "FR Reservation Code",
            button: "Réclamer votre code",
            tablePrefixParagraph:
              "{0} séance(s) de test trouvée(s) pour le code de réservation {1}",
            table: {
              columnOne: "Test",
              columnTwo: "Date de la séance de test",
              columnThree: "Centre de tests",
              columnThreeOla: "Heure de la séance",
              columnFour: "Actions",
              infoButton: "Informations sur la séance",
              infoButtonAccessibilityLabel: "Entrez votre code de réservation",
              noDataMessage: "Il n'y a actuellement aucune séance de test disponible"
            },
            reservedTablePrefixParagraph: "Mes codes de réservations:",
            reservedTable: {
              columnOne: "Test",
              columnTwo: "Code de réservation",
              columnThree: "Actions",
              findSessions: "Trouver des séances de test",
              findSessionsAccessibilityLabel: "FR Todo Find Sessions help text",
              searchResultsButton: "FR Search for Existing Results",
              searchResultsAccessibilityLabel: "FR Todo Search for Existing Results help text",
              requestAccommodationsAccessibilityLabel:
                "Faire une demande de mesures d’adaptation pour le test de {0}",
              requestAccommodationsButton: "Faire une demande de mesures d’adaptation",
              accessibilityRequestButton: "FR Open Accessibility Request",
              accessibilityRequestAccessibilityLabel:
                "FR Todo Open Accessibility Request help text",
              withdrawButton: "Me retirer du processus",
              withdrawAccessibilityLabel: "Todo Withdraw from Process help text",
              noDataMessage:
                "Aucun code de réservation n'a été trouvé pour votre compte ; veuillez entrer un nouveau code de réservation ci-dessus"
            },
            sessionPopup: {
              title: "Informations sur la séance de test",
              paragraph:
                "Cliquez sur « Réserver » pour confirmer votre présence à cette séance de test.",
              bookItButton: "Réserver"
            },
            withdrawnSuccessPopup: {
              title: "Retiré avec succès",
              paragraph:
                "Vous vous êtes retiré avec succès de ce processus, aucune autre action n'est nécessaire pour l'instant."
            },
            bookingSuccessPopup: {
              title: "Séance de test réservée avec succès",
              paragraph:
                "Votre réservation a été effectuée avec succès. Vos séances de test réservées peuvent être consultées sous l'onglet \"Gérer la réservation"
            },
            confirmWithdrawPopup: {
              title: "Confirmez votre retrait",
              paragraph: `Êtes-vous sûr de vouloir vous retirer de ce processus d'évaluation ?
              Si vous continuez, vous ne pourrez pas passer ce test et l'organisation qui vous a invité à passer le test sera informée de votre décision de vous retirer de ce processus d'évaluation.`
            },
            noSessionsAvailablePopup: {
              title: "FR No Sessions Available",
              paragraph:
                "FR There are no sessions available for this reservation code before the closing date."
            },
            notEnoughSeatsPopup: {
              title: "La séance de test n’est pas disponible",
              paragraph:
                "Cette séance de test est actuellement complète; veuillez choisir une autre séance de test."
            },
            notEnoughTimePopup: {
              title: "La séance de test n’est pas disponible",
              paragraph:
                "Il n'est pas possible d'effectuer une réservation pour cette séance de test après {0}."
            }
          },
          reservationDetailsPopup: {
            description: "Voici les informations de votre séance de test\u00A0:",
            testSessionDateLabel: "Date de la séance de test\u00A0:",
            testSessionStartTimeLabel: "Heure de début\u00A0:",
            testSessionEndTimeLabel: "Heure de fin\u00A0:",
            testSkillLabel: "Type de test\u00A0:",
            additionalDetailsLabel: "Information supplémentaire\u00A0:",
            testCenterAdditionalInfoLabel: "Information du centre de test\u00A0:",
            roomAdditionalInfo: "Information de la salle\u00A0:",
            departmentLabel: "Organisation\u00A0:",
            locationLabel: "Lieu\u00A0:",
            phoneNumber: "Numéro de téléphone :",
            termsOfUseCheckboxLabel: "Je confirme que j’ai lu et que j’accepte les {0}",
            termsOfUseCheckboxLink: "conditions d'utilisation",
            privacyNoticeStatementCheckboxLabel: "Je confirme que j’ai lu cet {0}",
            privacyNoticeStatementCheckboxLink: "énoncé de confidentialité",
            termsOfUsePopup: {
              title: "Conditions d'utilisation"
            },
            privacyNoticeStatementPopup: {
              title: "Énoncé de confidentialité"
            },
            editPhoneNumberPopop: {
              title: "Modifier le numéro de téléphone ",
              phoneNumber: "Numéro de téléphone :"
            }
          },
          manageReservation: {
            title: "Gérer vos réservations",
            table: {
              columnOne: "Test",
              columnTwo: "Date de la séance de test",
              columnThree: "Centre de tests",
              columnFour: "Actions",
              infoButton: "Voir les informations sur la séance de test",
              editButton: "Modifier la réservation",
              deleteButton: "Me retirer du processus d'évaluation",
              noDataMessage: "Pas de réservation en cours"
            },
            reservationPopup: {
              titleView: "Informations sur la séance",
              titleChange: "Modifier la réservation",
              titleWithdraw: "Me retirer du processus",
              joinMeetingButton: "FR Join Meeting",
              changeItButton: "FR Change",
              paragraphLater:
                "Cliquez sur «Réserver plus tard» pour sélectionner une autre séance de test à une date ultérieure.",
              bookLaterButton: "Réserver plus tard",
              paragraphWithdraw: `Cliquez sur «Me retirer» pour annuler votre réservation et

                Si vous continuez, vous ne pourrez pas passer ce test et l'organisation qui vous a invité à passer le test sera informée de votre décision de vous retirer de ce processus d'évaluation`,
              withdrawButton: "Me retirer"
            },
            bookingActionSuccessfulPopup: {
              title: "Annulation de réservation réussie",
              paragraphLater:
                "Vous avez annulé votre réservation avec succès. Vous pouvez effectuer une nouvelle réservation en réutilisant votre code de réservation dans l’onglet «Effectuer une réservation».",
              paragraphChange: "FR You have successfully changed your booking"
            },
            withdrawSuccessfulPopup: {
              title: "Retiré avec succès",
              paragraph:
                "Vous vous êtes retiré avec succès de ce processus, aucune autre action n'est nécessaire pour l'instant."
            },
            actionViolatesPopup: {
              title: "La réservation ne peut pas être modifiée",
              paragraph:
                "La réservation pour cette séance de test ne peut pas être modifiée après le {0}."
            }
          }
        },
        incidentReport: "FR Incident Reports",
        testResults: "Mes résultats de tests"
      },
      table: {
        columnOne: "Test",
        columnTwo: "Actions",
        requestAccommodations: "Faire une demande de mesures d'adaptation",
        requestAccommodationsAccessibilityLabel:
          "Voir les détails de la demande de mesures d'adaptation pour le test {0}",
        joinMeetingButton: "FR Join virtual teams meeting",
        joinMeetingButtonLabel: "FR Join virtual teams meeting for {0}",
        startTest: "Commencer",
        resumeTest: "Reprendre",
        startTestAccessibilityLabel: "Commencer le test {0}.",
        resumeTestAccessibilityLabel: "Reprendre le test {0}.",
        noTests: "Aucun test ne vous est attribué"
      },
      requestAccommodationsPopup: {
        title: "Faire une demande de mesures d’adaptation ",
        alternativeTitle: "Faire une demande de mesures d’adaptation",
        successTitle: "Demande envoyée",
        needAccommodationsProfileSystemMessageDescription:
          "Un profil des mesures d'adaptation est obligatoire pour envoyer cette demande. Rendez-vous sur la page {0} pour compléter ces informations avant de poursuivre.",
        incompletedAccommodationsProfileLink: "Mon profil",
        description:
          "En soumettant cette demande, vous demandez que des mesures d'adaptation en matière d'évaluation soient ajoutées à votre profil pour le {test_name}. Veuillez compléter la section ci-dessous. Un ou une spécialiste en évaluation vous contactera.",
        successSystemMessageDescription:
          "Cette demande a été envoyée avec succès. Veuillez attendre de recevoir votre rapport de mesures d’adaptation avant de mettre une session de test à l’horaire.",
        requestTypeLabel: "Type de demande :",
        requestTypeOptions: {
          accommodations: "Mesures d'adaptation",
          alternateTest: "Test alternatif (administrateur de test antérieur)",
          both: "Les deux"
        },
        testCenterLabel: "Lieu\u00A0:",
        phoneNumberLabel: "Numéro de téléphone :",
        commentLabel: "Commentaires (facultatif)\u00A0:",
        testAdministeredDetailsLabel: "Motif de la demande\u00A0:",
        checkboxDescription:
          "Je confirme que mon {0} est à jour et qu'il représente bien ma situation.",
        checkboxDescriptionLink: "profil de mesures d'adaptation",
        rightButton: "Soumettre la demande"
      },
      ongoingAccommodationRequestProcessPopup: {
        title: "FR Start Test",
        systemMessageDescription:
          "FR Your accommodation request has not been completely processed or applied yet. Are you sure you want to start your test anyway?",
        checkboxDescription: "FR Yes, I'm sure I want to continue without any accommodations."
      }
    },

    // My Test
    myTests: {
      title: "Bienvenue {0} {1}.",
      sideNavTitle: "Mes résultats de tests",
      alertCatTestsOnly1:
        "Les résultats de tous les tests que vous avez passés en utilisant l'Outil d'évaluation des candidats seront affichés sur cette page 48 heures après la date d’expiration du test (la « date limite » figurant dans votre courriel d’invitation au test). Ces résultats sont valides lorsque les conditions ont été respectées, notamment le respect du délai d’attente avant de reprendre un test.",
      alertCatTestsOnly2:
        "À noter\u00A0: Le résultat valide est toujours le dernier résultat obtenu lorsque les conditions du test ont été respectées, peu importe le format (en ligne ou papier) et la méthode d’administration (en personne ou à distance).",
      topTabs: {
        validTests: "Résultats de tests actuellement valides",
        archivedTests: "Résultats de tests archivés"
      },
      table: {
        nameOfTest: "Test",
        testDate: "Date du test",
        score: "Score brut",
        result: "Résultat",
        expiration: "Expiration",
        reTest: "Date de reprise",
        actions: "Action",
        viewResultsButton: "Afficher",
        viewResultsButtonAriaLabel:
          "Afficher les résultats du test {0}, passé le {1}, reprise possible à compter du {2}, et votre résultat est {3}.",
        viewResultsPopup: {
          archivedWatermark: "ARCHIVÉ",
          title: "Résultat du test",
          testLabel: "Test\u00A0:",
          testDescriptionLabel: "Description du test\u00A0:",
          // languageLabel: "Langue du test\u00A0:",
          testDateLabel: "Date du test\u00A0:",
          testStatus: "Statut du test\u00A0:",
          scoreLabel: "Score brut\u00A0:",
          resultLabel: "Résultat\u00A0:",
          retestDateLabel: "Date à partir de laquelle une reprise est possible\u00A0:",
          retestPeriodLabel: "Nombre de jours minimal avant une reprise\u00A0:",
          retestPeriodUnits: "jours",
          scoreValidity: "Période de validité du résultat (mois)\u00A0:",
          scoreValidityUnits: "mois",
          expiration: "Expiration\u00A0:"
        },
        noDataMessage: "Aucun test n'a encore été corrigé."
      }
    },

    // Checkin action
    candidateCheckIn: {
      button: "M'enregistrer",
      loading: "Recherche de tests actifs...",
      page: {
        title: "Entrer le code d'accès au test",
        incompletedProfileTitle: "Mise à jour des renseignements de votre profil",
        description: `Veuillez inscrire le code d'accès au test, reçu de l'administrateur de tests, dans le champ ci-dessous. Ensuite, cliquez sur «\u00A0M'enregistrer\u00A0».`,
        textLabel: "Code d'accès au test\u00A0:",
        takeTestTooltip: "Inscrivez votre Code d'accès au test",
        textLabelError:
          "Doit être un code d'accès au test valide. Si votre code débute par «RES- », vous avez été invité à planifier un test en personne. Veuillez sélectionner «Réservation» dans le menu de gauche.",
        testAccessCodeAlreadyUsedError: "Vous avez déjà utilisé ce code d'accès au test",
        currentTestAlreadyInActiveStateError: "Vous êtes déjà enregistré pour ce test",
        incompletedProfileSystemMessage:
          "Vous devez remplir ou réviser votre profil avant de vous enregistrer pour un test. Des renseignements supplémentaires sont requis à des fins statistiques.",
        incompletedProfileDescription:
          "Avant de poursuivre, allez à la page {0} pour inscrire ces renseignements. ",
        incompletedProfileLink: "Mon profil",
        incompletedProfileRightButton: "Mettre à jour le profil"
      }
    },

    // Lock Screen
    candidateLockScreen: {
      tabTitle: "Test verrouillé",
      description: {
        part1: "Votre test a été verrouillé par l'administrateur du test.",
        part2: "Le chronomètre de votre test a été mis en pause.",
        part3: "Vos réponses ont été sauvegardées.",
        part4: "Veuillez contacter l'administrateur du test pour obtenir davantage d'instructions."
      }
    },

    // Pause Screen
    candidatePauseScreen: {
      tabTitle: "Test en pause",
      description: {
        part1:
          "Vous avez mis votre test en pause. Il reprendra lorsque vous sélectionnerez Reprendre le test ou lorsque la minuterie atteindra 00:00:00.",
        part2: "La minuterie de votre test est sur pause.",
        part3: "Vos réponses ont été enregistrées."
      }
    },

    // Invalidate Test Screen
    candidateInvalidateTestScreen: {
      tabTitle: "Test désassigné/invalidé",
      description: {
        part1: "Votre test a été désassigné ou invalidé.",
        part2: "Veuillez contacter l'administrateur de test pour obtenir davantage d'instructions.",
        returnToDashboard: "Retour à la page d'accueil"
      }
    },

    // Test Builder Page
    testBuilder: {
      backToTestDefinitionSelection: "Retour à la liste des définitions de tests",
      backToTestDefinitionSelectionPopup: {
        title: "FR Return to Test Definition Selection?",
        description: "FR Are you sure you want to continue?",
        systemMessageDescription: "FR You will lose everything!"
      },
      containerLabel: "Définition de test",
      itemBanksContainerLabel: "Banques d'items",
      goBackButton: "FR Back to Test Builder",
      inTestView: "Aperçu",
      errors: {
        nondescriptError:
          "FR Non descript error. Report this to IT. Maybe a user is logged into the adminconsole.",
        cheatingPopup: {
          title: "FR Caught Cheating Attempt",
          description:
            "FR You are not allowed to switch or open new tabs. Any attempt to minimize the screen or change focus will be considered a cheating attempt.",
          warning:
            "FR After {0} more detections your test will be terminated and your results invalidated. If this was an error, alert your TA."
        },
        backendAndDbDownPopup: {
          title: "Problème de connexion !",
          description:
            "La connexion à votre test a été interrompue. Pour continuer le test, assurez-vous d’être connecté à Internet, puis essayez de vous reconnecter à votre compte de l'OÉC.",
          rightButton: "Retour à la page d'accueil"
        }
      },
      testDefinitionSelectPlaceholder: "Sélectionner un section de test",
      sideNavItems: {
        testDefinition: "Information du test",
        testSections: "Sections de test",
        testSectionComponents: "Sous-sections de test",
        sectionComponentPages: "Titres des sections",
        componentPageSections: "Contenu des sections",
        questionBlockTypes: "Noms des blocs d'items",
        questions: "Items",
        scoring: "Méthode de notation",
        answers: "FR Answers",
        addressBook: "Contacts du carnet d'adresses",
        testManagement: "Activation du test"
      },
      // some values are underscored because python uses underscores
      testDefinition: {
        title: "Définition du test ",
        description: "FR These are the top level properties of a test.",
        validationErrors: {
          invalidTestDefinitionDataError:
            "Erreur\u00A0: Tous les champs de l'Information sur le test sont obligatoires. Veuillez remplir les champs manquants et réessayer."
        },
        uploadFailedErrorMessage: "FR Upload Error: Review the highlighted sections",
        uploadFailedComplementaryErrorMessages: "Erreur de téléversement. Veuillez réviser {0}.",
        uploadJsonFailedComplementaryErrorMessages: "FR JSON Upload Error: Review JSON content",
        uploadTestDefinition: {
          title1: "Téléverser la version de test au serveur",
          title2: "Cette version de test a été téléversée avec succès",
          description1:
            "Une fois téléversée, une nouvelle version CAT de cette définition de test sera créée. Êtes-vous sûr de vouloir continuer ?",
          description2:
            "La version test a été téléversée avec succès. Cependant, veuillez noter que cette nouvelle version est actuellement désactivée. Pour activer cette nouvelle version de test, mettez à jour le statut du test dans la section Activation du test."
        },
        uploadFailedPopup: {
          title: "Échec de téléversment",
          systemMessage:
            "Toutes les sections de test dont la méthode de notation est réglée à NOTATION AUTOMATIQUE doivent contenir au moins une sous-section de test dont le type de sous-section est réglé à ITEMS_(BANQUE_D'ITEMS)."
        },
        undefinedVersion: "<FR undefined>",
        selectTestDefinition: {
          sideNavigationItems: {
            allTests: "Toutes les définitions de tests",
            AllActiveTestVersions: "Définitions de tests actives",
            archivedTestCodes: "Définitions de tests archivées",
            uploadJson: "Téléverser le JSON"
          },
          newTestDefinitionButton: "Nouvelle définition de test",
          allTests: {
            title: "Toutes les définitions de tests",
            description: "Création, modification et téléversement de définitions de tests.",
            table: {
              column1: "Numéro du test",
              column2: "FR Test Code",
              column3: "Nom du test",
              column4: "Actions",
              noData: "FR There is no test definition data",
              selectSpecificTestVersionTooltip: "Sélectionner une version de la définition de test",
              selectSpecificTestVersionLabel:
                "FR Select a specific version of this test definition where the parent code is: {0}, the test code is: {1} and the test name is: {2}",
              selectLatestTestVersionTooltip:
                "Afficher la version la plus récente de la définition de test",
              selectLatestTestVersionLabel:
                "FR View latest test version of this test definition where the parent code is: {0}, the test code is: {1} and the test name is: {2}",
              archiveTestCodeTooltip: "Archiver la définition de test",
              archiveTestCodeLabel:
                "FR Archive test code of the this test definition where the parent code is: {0}, the test code is: {1} and the test name is: {2}"
            },
            testDefinitionPopup: {
              title: "Sélectionner une version de la définition de test",
              parentCode: "FR Parent Code:",
              testCode: "FR Test Code:",
              versionNotes: "Notes sur la version\u00A0:",
              uitStatus: "TELNS\u00A0:",
              testStatus: "Statut du test\u00A0:",
              sampleTestStatus: "Exemple de test\u00A0:",
              name: "Nom du test\u00A0:",
              rightButton: "Sélectionner la définition du test",
              dropdown: {
                title: "Version OÉC\u00A0:",
                titleTooltip: "FR To be completed"
              }
            },
            archiveTestCodePopup: {
              title: "Archiver la définition de test",
              systemMessageDescription: "Tous les accès reliés au {0} seront révoquées.",
              description: "Êtes-vous sûr de vouloir continuer ?",
              rightButton: "Archiver la définition de test"
            }
          },
          allActiveTestVersions: {
            title: "Définitions de tests actives",
            description: "Affichage et désactivation de versions de tests actives",
            table: {
              column1: "FR Parent Code",
              column2: "FR Test Code",
              column3: "Nom du test",
              column4: "Version OÉC",
              column5: "Actions",
              noData: "FR There is no test definition data",
              viewButtonTooltip: "Afficher la définition de test",
              viewButtonAriaLabel:
                "FR View details of the following test definition where the parent code is: {0}, the test code is: {1}, the test name is: {2} and the version is: {3}",
              deactivateButtonTooltip: "Désactiver la définition de test",
              deactivateButtonAriaLabel:
                "FR Deactivate test definition version where the parent code is: {0}, the test code is: {1}, the test name is: {2} and the version is: {3}"
            },
            popup: {
              title: "Désactiver la définition de test",
              description: "Tous les accès reliés au {0} seront révoquées.",
              rightButton: "Désactiver la définition de test"
            }
          },
          archivedTestCodes: {
            title: "Définitions de tests archivées",
            description: "Restauration des définitions de tests désactivées.",
            table: {
              column1: "FR Parent Code",
              column2: "FR Test Code",
              column3: "Nom du test",
              column4: "Actions",
              noData: "FR There is no test definition data",
              restoreButton: "Restaurer la définition de test",
              restoreButtonAriaLabel:
                "FR Restore test definition test code where the parent code is: {0}, the test code is: {1} and the test name is: {2}"
            },
            popup: {
              title: "Restaurer la définition de test",
              description: "La définition du test {0} sera restaurée",
              rightButton: "Restaurer la définition de test"
            }
          },
          uploadJson: {
            title: "Téléverser le JSON",
            description: "Téléversement de fichiers JSON au serveur",
            textAreaLabel: "Veuillez sélectionner le fichier JSON\u00A0:",
            uploadJsonButton: "Téléverser le JSON",
            invalidJsonError: "Erreur de téléversement. Veuillez réviser le fichier JSON",
            uploadJsonSuccessful: "Le fichier JSON « {0} {1} v{2} » a été téléversée avec succès.",
            fileSelectedAccessibility: "FR The selected file is {0}",
            browseButton: "Parcourir...",
            wrongFileTypeError: "Doit être un fichier JSON valide"
          },
          itemBanks: {
            title: "Banques d'items",
            table: {
              column1: "ID banque d'items",
              column2: "Nom de la banque d'items",
              column3: "Actions",
              viewButtonTooltip: "Afficher",
              viewButtonAriaLabel: "Afficher la banque d'items {0}",
              downloadReportButtonTooltip: "Télécharger le rapport",
              noData: "Votre recherche n'a généré aucun résultat"
            }
          },
          itemBankEditor: {
            backToItemBankSelectionButton: "Retour à la liste des banques d'items",
            backToItemBankSelectionPopup: {
              title: "FR Return to Item Bank Selection?",
              systemMessageDescription: "Toutes les données non enregistrées seront perdues !",
              description: "Êtes-vous sûr de vouloir continuer?"
            },
            sideNavigationItems: {
              itemBankDefinition: "Définition de la banque d'items",
              itemAttributes: "Variables personnalisées",
              items: "Items ({0})",
              bundles: "Groupement ({0})"
            },
            itemBankDefinition: {
              custom_item_bank_id: {
                title: "ID de la banque d'items\u00A0:",
                errorMessage: "FR Must be a valid Item Bank ID"
              },
              custom_item_bank_id_already_exists: {
                title: "ID de la banque d'items\u00A0:",
                errorMessage: "FR This Item Bank ID already exists"
              },
              en_name: {
                title: "Nom de la banque d'items (anglais)\u00A0:",
                errorMessage: "FR Must be a valid name"
              },
              fr_name: {
                title: "Nom de la banque d'items (français)\u00A0:",
                errorMessage: "FR Must be a valid name"
              },
              updateExistingItemBankPopup: {
                title: "FR Update Existing Item Bank Definition",
                systemMessageDescription:
                  "FR You are about to update an existing item bank definition!",
                description: "FR Are you sure you want to proceed?"
              }
            },
            itemAttributes: {
              createNewButton: "Variable",
              collapseAll: "Réduire toutes les variables",
              uploadDataButton: "FR Upload Data",
              uploadDataErrorMessage:
                "FR Error found in item attributes. Please make sure that all parameters are defined.",
              itemAttributeCollapsingItem: {
                title: "Variable personnalisée {0}\u00A0: ",
                values: "Valeurs\u00A0: ",
                en_name: {
                  title: "Nom de variable en anglais\u00A0:",
                  errorMessage: "FR Must be a valid name"
                },
                fr_name: {
                  title: "Nom de variable en français\u00A0:",
                  errorMessage: "FR Must be a valid name"
                },
                attributeTitle: "Nom de variable\u00A0:",
                attributeValuesTitle: "Valeurs\u00A0:",
                createNewAttributeValueButton: "FR Add Value",
                attributeTranslationsButtonLabel: "Modifier les traductions",
                attributeValueCollapsingItem: {
                  title: "FR Attribute Value {0}: {1}",
                  value_identifier: {
                    title: "FR Value Identifier",
                    errorMessage: "FR Must be a valid value identifier"
                  }
                },
                attributeNameTranslationsPopup: {
                  title: "Traductions - Nom de variable"
                }
              },
              dataUploadedSuccessfullyPopup: {
                title: "FR Item Bank Attributes Data Uploaded Successfully",
                systemMessage:
                  "FR The item bank attributes data has been uploaded successfully in the database."
              }
            },
            items: {
              itemTitle: "{0} {1} {2} - ID item système\u00A0: {3} ",
              createNewItemButton: "Nouvel item",
              createItemPopup: {
                title: "Créer un item",
                description:
                  "Veuillez fournir les informations requises pour créer un nouvel item.",
                responseFormatLabel: "Format de réponse\u00A0:",
                itemTypeLabel: "Format de l'item\u00A0:",
                historicalIdLabel: "ID item R&D\u00A0:",
                historicalIdAlreadyBeenUsedError:
                  "Cet ID item R&D est déjà utilisé dans cette banque d'item.",
                rightButton: "Créer un item"
              },
              table: {
                itemId: "ID item système",
                itemType: "Statut de l'item",
                version: "Version",
                actions: "Actions",
                viewAction: "Aperçu",
                viewActionAccessibilityLabel: "Afficher l'item {0}",
                editAction: "Modifier",
                editActionAccessibilityLabel: "Modifier l'identifiant de l'item {0}",
                noData: "Il n'y a pas d'items dans cette banque.",
                rndItemId: "ID item R&D",
                itemTypeLabel: "Format de l'item",
                itemTypeQuestion: "FR Question",
                itemTypeInstruction: "Directive"
              },
              viewSelectedItemPopup: {
                title: "Aperçu de l'item\u00A0: {0}",
                questionLabel: "FR Question"
              },
              backToItemBankEditorButton: "Retour à la banque d'items",
              sideNavigationItems: {
                general: "Général",
                properties: "Propriétés",
                statistics: "Statistiques",
                socialRelationships: "Rapports sociaux",
                history: "Historique",
                versionComparison: "Comparaison de versions",
                uploadData: "FR Upload Data"
              },
              topTabs: {
                generalTabs: {
                  generaL: "Général"
                },
                contentTabs: {
                  content: "Contenu",
                  screenReader: "Lecteur d'écran"
                },
                otherTabs: {
                  categories: "Variables personnalisées",
                  comments: "Commentaires",
                  translations: "Traductions"
                }
              },
              general: {
                systemIdLabel: "ID item système\u00A0:",
                historicalIdLabel: "ID item R&D\u00A0:",
                editHistoricalIdTooltip: "Modifier",
                addEditHistoricalIdPopup: {
                  title: "ID item R&D",
                  description: "Veuillez fournir les informations requises.",
                  historicalIdLabel: "ID item R&D\u00A0:"
                },
                developmentStatusLabel: "Statut de l'item\u00A0:",
                versionLabel: "Version\u00A0:",
                deleteDraftTooltip: "Supprimer l'ébauche de l'item",
                deleteDraftConfirmationPopup: {
                  title: "Supprimer l'ébauche de l'item ?",
                  systemMessageDescription: "Cette ébauche sera supprimée !",
                  description: "Êtes-vous sûr de vouloir continuer ?"
                },
                draftVersionLabel: "<Ébauche>",
                responseFormatLabel: "Format de réponse\u00A0:",
                scoringFormatLabel: "Méthode de notation\u00A0:",
                shuffleOptionsLabel: "Randomiser choix de réponses\u00A0:",
                activeStatusLabel: "Actif\u00A0:",
                historicalIdAlreadyExistsError:
                  "Cet ID item R&D est déjà utilisé dans cette banque d'item."
              },
              content: {
                languageDropdownLabel: "Langue\u00A0:",
                addStemButton: "Énoncé",
                stemSections: "Énoncé(s)\u00A0:",
                addInstructionButton: "Directive",
                instructionSections: "Directive(s):",
                addOptionButton: "Choix de réponse",
                options: "Choix de réponses\u00A0:",
                score: "Valeur de la notation\u00A0:",
                text: "Texte\u00A0:",
                excludeFromShuffle: "Exclure de la randomisation\u00A0:",
                detailsButton: "Détails",
                detailsPopup: {
                  title: "Information supplémentaire du choix de réponse",
                  textLabel: "Texte\u00A0:",
                  historicalOptionIdLabel: "ID choix de réponse R&D\u00A0:",
                  rationaleLabel: "Justification\u00A0:",
                  excludeFromShuffleLabel: "Exclure de la randomisation\u00A0:",
                  scoreLabel: "Valeur de la notation\u00A0:"
                },
                addAlternativeContentPopup: {
                  title: "Ajouter du contenu d'item alternatif ",
                  description: "Veuillez fournir les informations requises.",
                  alternativeContentTypeLabel: "Type de contenu alternatif\u00A0:",
                  alternativeContentTypeOptions: {
                    screenReader: "Lecteur d'écran"
                  },
                  rightButton: "Sauvegarder"
                },
                deleteAlternativeContentPopup: {
                  title: "Supprimer l'onglet de contenu d'item alternatif ?",
                  systemMessageDescription: "L'onglet de contenu d'item alternatif sera supprimé.",
                  description: "Êtes-vous sûr de vouloir supprimer cet onglet ?"
                }
              },
              comments: {
                addCommentButton: "Commentaire",
                addCommentPopup: {
                  title: "Ajouter un commentaire",
                  commentLabel: "Commentaire\u00A0:",
                  addButton: "Sauvegarder"
                },
                deleteCommentPopup: {
                  title: "Supprimer le commentaire",
                  systemMessageDescription1: "Êtes-vous sûr de vouloir supprimer ce commentaire ?",
                  systemMessageDescription2: "FR - {0}"
                }
              },
              previewItemPopup: {
                title: "Aperçu de l'item\u00A0: {0}"
              },
              uploadConfirmationPopup: {
                title: "Téléverser l'item",
                titleSuccessfully: "Téléversé avec succès !",
                systemMessageDescription:
                  "Le contenu des champs suivants sera mis à jour à la suite du téléversement de l'item\u00A0:",
                systemMessageDescriptionSuccessfully:
                  "La version {0} de l'item a été téléversée avec succès.",
                systemMessageDescriptionSuccessfullyOverwrite:
                  "La version {0} de l'item a été téléversée avec succès.",
                itemModificationsTable: {
                  itemElements: {
                    stemSections: "Énoncé(s)",
                    options: "Choix de réponses",
                    categories: "Variables personnalisées",
                    historicalId: "ID item R&D",
                    itemStatus: "Statut de l'item",
                    responseFormat: "Format de réponse",
                    scoringFormat: "Méthode de notation",
                    shuffleOptions: "Randomiser choix de réponses",
                    instructions: "FR Instruction(s)"
                  },
                  column1: "Champs",
                  column2: "Mise à jour",
                  column3: "Avertissements",
                  warnings: {
                    stemsNotDefined: "Énoncé(s) non défini(s).",
                    containsBlankStems: "Contient un ou des énoncés manquants.",
                    optionsNotDefined: "Choix de réponse(s) non définie(s).",
                    containsBlankOptions: "Contient un ou des choix de réponses manquants.",
                    instructionsNotDefinded: "FR Instruction(s) not defined",
                    containsBlankInstructions: "FR Contains blank instruction(s)."
                  }
                },
                whatDoYouWantToDo: "Que voulez-vous faire ?",
                createNewVersionRadio: "Créer un nouveau numéro de version pour cet item",
                overwriteCurrentVersionRadio: "Conserver le numéro de version actuel de l'item",
                reviewedWarningsCheckboxLabel: "J'ai lu et accepté les avertissements."
              },
              uploadData: {
                title: "FR Upload Data: {0} ({1})",
                uploadLabel: "FR Upload Item:",
                uploadButton: "FR Upload",
                uploadPopup: {
                  title: "FR Upload Item",
                  description: "FR Please insert your comment if needed.",
                  comment: "FR Comment (optional):",
                  uploadButton: "FR Upload",
                  uploadSuccessful: "FR Item version {0} has been uploaded successfully."
                },
                uploadFailedErrorMessage: "FR Upload Error: Review the highlighted sections"
              },
              backToItemBankEditorPopup: {
                title: "Revenir à l'éditeur de banque d'items ?",
                systemMessageDescription: "Toutes les données non enregistrées seront perdues !",
                description: "Êtes-vous sûr de vouloir continuer ?"
              },
              changeItemConfirmationPopup: {
                goPreviousTitle: "Naviguer à l'item précédent ?",
                goNextTitle: "Naviguer au prochain item ?",
                changeItemTitle: "Naviguer vers un autre item ?",
                systemMessageDescription: "Toutes les données non enregistrées seront perdues !",
                description: "Êtes-vous sûr de vouloir continuer?"
              }
            },
            bundles: {
              bundleTitle: "{0}",
              createNewBundleButton: "Nouveau groupement",
              createBundlePopup: {
                title: "Créer un groupement",
                description:
                  "Veuillez fournir les informations requises pour créer un nouveau groupement.",
                bundleNameLabel: "Nom du groupement\u00A0:",
                bundleNameAlreadyBeenUsedError:
                  "Ce Nom du groupement est déjà utilisé dans cette banque d'item.",
                bundleVersionLabel: "Version du groupement\u00A0:",
                rightButton: "Créer un groupement"
              },
              backToItemBankEditorButton: "Retour à la banque d'items",
              table: {
                bundleName: "Nom du groupement",
                bundleVersion: "Version du groupement",
                itemCount: "Nombre d'items",
                bundleCount: "Nombre de groupements",
                status: "Statut",
                actions: "Actions",
                editAction: "Modifier",
                editActionAccessibilityLabel: "Modifier le groupement {0}",
                duplicateAction: "Dupliquer",
                duplicateActionAccessibilityLabel: "Dupliquer le groupement {0}",
                noData: "Cette banque ne contient aucun groupement"
              },
              topTabs: {
                generalTabs: {
                  general: "Général"
                },
                addTabs: {
                  addItems: "Ajouter des items",
                  addBundles: "Ajouter des groupements"
                },
                contentTabs: {
                  itemContent: "Contenu de l'item ({0})",
                  bundleContent: "Contenu du groupement ({0})"
                }
              },
              previewBundlePopup: {
                title: "Aperçu du groupement",
                systemMessageErrorBundleStructuralConflictError:
                  "Le groupement téléversé contient à la fois un groupement ainsi qu'un sous-groupement. Veuillez supprimer le sous-groupement conflictuel et essayer à nouveau.",
                systemMessageErrorDuplicateSystemIdsFoundInBundleAssociationsError:
                  "Le groupement en cours de prévisualisation contient des items en double. Veuillez supprimer les doublons et essayer à nouveau.",
                close: "Fermer",
                contentTypeDropdown: {
                  standard: "FR Standard",
                  screenReader: "FR Screen Reader"
                }
              },
              changeBundleConfirmationPopup: {
                goPreviousTitle: "Naviguer au groupement précédent ?",
                goNextTitle: "Naviguer au prochain groupement ?",
                changeBundleTitle: "Changer de groupement ?",
                systemMessageDescription: "Toutes les données non enregistrées seront perdues !",
                description: "Êtes-vous sûr de vouloir continuer?"
              },
              uploadSuccessfulPopup: {
                title: "Téléversé avec succès !",
                titleWithError: "Le groupement ne peut être téléversé",
                systemMessageDescription: "Les modifications ont été téléversées avec succès.",
                systemMessageErrorBundleNameAlreadyUsed:
                  "Ce Nom du groupement est déjà utilisé dans cette banque d'item.",
                systemMessageErrorBundleStructuralConflictError:
                  "Le groupement téléversé contient à la fois un groupement ainsi qu'un sous-groupement. Veuillez supprimer le sous-groupement conflictuel et essayer à nouveau.",
                systemMessageErrorDuplicateSystemIdsFoundInBundleAssociationsError:
                  "Le groupement téléversé contient des items en double. Veuillez supprimer les doublons et essayer à nouveau."
              },
              general: {
                bundleNameLabel: "Nom du groupement\u00A0:",
                bundleVersionLabel: "Version du groupement\u00A0:",
                shuffleItemsLabel: "Randomiser les items\u00A0:",
                shuffleBundlesLabel: "Randomiser les groupements\u00A0:",
                shuffleBetweenBundlesLabel: "FR Randomize Between Groupings:",
                activeLabel: "Actif\u00A0:",
                editBundleNameTooltip: "Modifier",
                editBundleNamePopup: {
                  title: "Modifier le nom du groupement",
                  description: "Veuillez fournir les informations requises.",
                  bundleNameLabel: "Nom du groupement\u00A0:",
                  bundleNameMustBeValid: "Le Nom du groupement doit être être fourni."
                }
              },
              addItems: {
                table: {
                  systemId: "ID item système",
                  rndItemId: "ID item R&D",
                  responseFormat: "Format de réponse",
                  itemStatus: "Statut de l'item",
                  version: "Version",
                  noData: "Votre recherche n'a généré aucun résultat"
                },
                searchDropdowns: {
                  developmentStatusLabel: "Statut de l'item\u00A0:",
                  activeStateLabel: "Actif\u00A0:"
                },
                addItemsToBundleButton: "Ajouter au groupement"
              },
              itemContent: {
                basicRuleLabel: "Nombre d'items à sélectionner\u00A0:",
                basicRuleErrorLabel: "Doit être un nombre valide"
              },
              addBundles: {
                table: {
                  bundleName: "Nom du groupement",
                  bundleVersion: "Version",
                  itemCount: "Nombre d'items",
                  bundleCount: "Nombre de groupement",
                  status: "Statut",
                  noData: "Votre recherche n'a généré aucun résultat"
                },
                addBundlesToBundleButton: "Ajouter au groupement"
              },
              bundleContent: {
                createNewBundleRuleButton: "Nouvelle règle de groupement",
                bundleRuleCollapsingItemContainer: {
                  title: "Règle {0}",
                  numberOfBundlesToPull: "Nombre de groupements à sélectionner\u00A0:",
                  numberOfBundlesToPullErrorMessage: "Doit être un nombre valide",
                  shuffleBundles: "Randomiser les groupements\u00A0:",
                  keepItemsTogether: "Conserver les items ensemble\u00A0:"
                },
                bundleAssociationsSystemMessage:
                  "Tous les groupements qui ne font pas partie d'une règle seront supprimés lors du téléversement."
              }
            },
            deleteConfirmationPopup: {
              title: "FR Delete Confirmation",
              description:
                "FR Deleting this item will delete all the content displayed and all child objects that are related to this object. Are you sure you want to delete this object?"
            }
          }
        },
        test_code: {
          title: "Code complet du test",
          titleTooltip: "FR Unique Identifier for a test definition",
          errorMessage: "Dois être un Code de test complet valide"
        },
        parent_code: {
          title: "FR Parent Code",
          titleTooltip: "FR The test code digits that group all version of this test.",
          errorMessage: "Dois être un Numéro de test valide"
        },
        version: {
          title: "Version OÉC",
          titleTooltip: "FR The latest version of the test definition",
          errorMessage: "FR Not a valid Version Number"
        },
        test_skill_type_id: {
          title: "Type de test",
          titleTooltip: "Type de test de la définition de test (exemple : ÉLS)",
          errorMessage: "Veuillez choisir un Type de test",
          selectPlaceholder: "Veuillez choisir"
        },
        test_skill_sub_type_id: {
          title: "Test\u00A0:",
          titleTooltip: "(exemple : Anglais - Expression écrite)",
          errorMessage: "Veuillez choisir un test",
          selectPlaceholder: "Veuillez choisir"
        },
        retest_period: {
          title: "Période d'attente",
          titleTooltip: "FR Retest period in number of days",
          errorMessage: "FR Not a valid number."
        },
        version_notes: {
          title: "Notes sur la version de test",
          titleTooltip: "FR Some wording here..."
        },
        fr_name: {
          title: "Nom du test (français)",
          titleTooltip: "FR Name display to candidates when they have an active test",
          errorMessage: "FR Not a valid Name"
        },
        en_name: {
          title: "Nom du test (anglais)",
          titleTooltip: "FR Name display to candidates when they have an active test",
          errorMessage: "FR Not a valid Name"
        },
        show_score: {
          title: "Afficher le score brut",
          titleTooltip: `Le score brut sera affiché dans la page "Mes résultats de tests" du candidat`
        },
        show_result: {
          title: "Afficher le résultat du test",
          titleTooltip: `Le résultat de test sera affiché dans la page "Mes résultats de tests" du candidat`
        },
        is_uit: {
          title: "Test non-supervisé",
          titleTooltip: "FR Is this a unsupervised internet test?"
        },
        is_public: {
          title: "Exemple de test",
          titleTooltip: "FR Is this a sample test?"
        },
        count_up: {
          title: "Chronomètre croissant",
          titleTooltip:
            "FR Timed section(s) will count up instead of counting down - TO BE COMPLETED"
        },
        active_status: {
          title: "Statut du test",
          titleTooltip: "FR Is this test active and available to use?"
        },
        test_language: {
          title: "FR Test Language",
          titleTooltip: "FR Is this test presented in a single language?",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "--"
        },
        constants: {
          testSectionScoringTypeObject: {
            not_scorable: "NON NOTÉ",
            auto_score: "NOTATION AUTOMATIQUE",
            competency: "NOTATION PAR COMPÉTENCE"
          },
          nextSectionButtonTypeObject: {
            proceed: "CONTINUER À LA SECTION SUIVANTE",
            popup: "FENÊTRE DE DIALOGUE"
          }
        }
      },
      testSection: {
        title: "Sections de test",
        validationErrors: {
          mustContainAtLeastThreeTestSectionError:
            "FR Error: Must contain at least three test sections",
          mustContainQuitTestSectionError: "FR Error: Must contain a 'Quit' test section",
          mustContainFinishTestSectionError: "FR Error: Must contain a 'Finish' test section"
        },
        description:
          "Créez un test en ajoutant et en définissant des sections de test. Les sections typiques d'un test peuvent comprendre les pages suivantes\u00A0: Bienvenue, Sécurité du test, Conditions d'utilisation, Navigation, Directives spécifiques, Questions du test, Test soumis et Quitter le test.",
        collapsableItemName: "Section de test {0}: {1}",
        addButton: "Nouvelle section de test",
        delete: {
          title: "FR Delete Test Section Component",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        fr_title: {
          title: "Nom de la section du test (français)",
          titleTooltip: "FR Hidden name used for easy identification by internal users",
          errorMessage: "FR Not a valid Name"
        },
        en_title: {
          title: "Nom de la section du test (anglais)",
          titleTooltip: "FR Hidden name used for easy identification by internal users",
          errorMessage: "FR Not a valid Name"
        },
        uses_notepad: {
          title: "Bloc-notes",
          titleTooltip: "FR Does this test section use the notepad component?",
          errorMessage: "FR not a valid choice"
        },
        uses_calculator: {
          title: "Calculatrice",
          titleTooltip: "FR Does this test section use the calculator component?"
        },
        block_cheating: {
          title: "Détection du changement de fenêtre",
          titleTooltip: "FR Should this test section report the user for switching tabs etc.?",
          errorMessage: "FR not a valid choice"
        },
        item_exposure: {
          title: "Exposition aux items",
          titleTooltip: "FR Should this test section apply the Item Exposure logic?"
        },
        scoring_type: {
          title: "Notation",
          titleTooltip:
            "FR Set the correct scoring method for the components in this test section.",
          errorMessage: "FR not a valid choice"
        },
        minimum_score: {
          title: "Note minimale",
          titleTooltip:
            "FR Set minimum score for a test section. Currently this value is only for reference in reports.",
          errorMessage: "FR not a valid choice"
        },
        default_time: {
          title: "Temps limite par défaut",
          titleTooltip:
            "FR How long does a candidate have on this section? If the section is not timed, then leave this empty. This field can be overridden by a TA",
          errorMessage: "FR Only numbers are allowed"
        },
        default_tab: {
          title: "Onglet par défaut",
          titleTooltip: "FR Which top tab should be displayed when a candidate begins this section",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "0"
        },
        order: {
          title: "Ordre de présentation",
          titleTooltip: "FR The order in the test that this section appears.",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "0"
        },
        next_section_button: {
          title: "FR Next Section Button",
          titleTooltip: "FR Define a button to take the user to the next section.",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "0"
        },
        reducers: {
          title: "FR Reducers",
          titleTooltip:
            "FR Which aspects of the test should be cleared when a user navigates to this section?",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "--"
        },
        section_type: {
          title: "Type de section",
          titleTooltip:
            "FR The type of test section. Such as: Single page, Top Tab navigation, Finish, Quit...",
          errorMessage: "FR Not a valid Type"
        },
        deletePopup: {
          title: "Confirmer la suppression",
          content: "Voulez-vous vraiment supprimer cette section?"
        }
      },
      nextSectionButton: {
        header: "Bouton de la section suivante",
        needs_approval: {
          title: "Besoin d'approbation de l'AT",
          titleTooltip:
            "Un candidat aura besoin de l'approbation d'un AT avant de poursuivre à la prochaine section"
        },
        button_type: {
          title: "Type de bouton",
          errorMessage:
            "Les valeurs sélectionnées pour le Type de section et le Type de bouton ne sont pas compatibles. Veuillez les modifier.",
          titleTooltip: "FR This is selected based on the test section component."
        },
        confirm_proceed: {
          title: "Boîte de confirmation",
          titleTooltip:
            "FR A checkbox the user has to click before being allowed to press the action button."
        },
        button_text: {
          title: "Libellé du bouton",
          titleTooltip: "FR The text that the button displays to the user"
        },
        title: {
          title: "Titre de la fenêtre de dialogue",
          titleTooltip:
            "FR The text that appears on the header of the popup when the next section button is clicked."
        },
        content: {
          title: "Contenu de la fenêtre de dialogue",
          titleTooltip:
            "FR The text that appears in the body of the popup when the next section button is clicked."
        }
      },
      testSectionComponents: {
        title: "Sous-sections du test",
        validationErrors: {
          mustFixTheQuestionsRulesErrors:
            "Erreur: Veuillez corriger les Règles d'assemblage de tests (Blocs d'items) ci-bas et réessayer.",
          undefinedPpcQuestionIdInDependentQuestion:
            'Assurez-vous que tous les items dépendants ont un "ID item R&D" défini.',
          notEnoughDefinedPpcQuestionIdBasedOnNbOfQuestions:
            "Erreur\u00A0: Nombre insuffisant d'items associés aux blocs d'items. Veuillez retourner à la section Items et assigner des blocs d'items à un nombre suffisant d'items qui correspond au nombre d'items spécifié dans les Règles d'assemblage de tests (Blocs d'items) ci-dessous.",
          mustContainQuestionListComponentType:
            'Erreur\u00A0: Assurez-vous que toute section de test dont la "méthode de notation" est réglée sur "NOTATION AUTOMATIQUE" contient au moins un type de sous-section réglé sur "ITEMS_(CONSTRUCTEUR_DE_TEST)".',
          mustContainItemBankRules:
            "FR All test section component of component type ITEM_BANK must have defined and valid rules",
          mustContainSubSections:
            "Toutes les sections du test doivent contenir au moins une sous-section associée"
        },
        description:
          'Création de définitions des sous-sections du test. Les sous-sections typiques peuvent inclure les "Directives du test" et les "Questions du test" dans la section "Questions du test" associée. Toutes les sections du test doivent comporter une sous-section. Si aucune sous-section n\'est nécessaire, veuillez en créer une et lui donner le même nom que sa section mère.',
        collapsableItemName: "Sous-section de test {0}: {1}",
        addButton: "Ajouter une sous-section de test",
        delete: {
          title: "Supprimer",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        parentTestSection: {
          title: "Section de test",
          titleTooltip: "FR The test section that the displayed objects appear in."
        },
        fr_title: {
          title: "Nom français",
          titleTooltip: "FR Hidden name used for easy identification by internal users",
          errorMessage: "FR Not a valid Name"
        },
        en_title: {
          title: "Nom anglais",
          titleTooltip: "FR Hidden name used for easy identification by internal users",
          errorMessage: "FR Not a valid Name"
        },
        order: {
          title: "Ordre de la sous-section",
          titleTooltip: "FR The order in which this component appears in a Top Tab navigation.",
          errorMessage: "FR Not a valid Name"
        },
        component_type: {
          title: "Type de la sous-section",
          titleTooltip: "FR The type of component. Such as: Inbox, Single Page, Side Navigation...",
          errorMessage:
            "FR Make sure that this is a valid parent test section data and component type combination"
        },
        content_type: {
          title: "FR Content Type (format)",
          titleTooltip: "FR The type of content. Standard or alternative content"
        },
        shuffle_all_questions: {
          title: "Randomiser les items (Constructeur de tests)",
          titleTooltip:
            "FR Shuffle the questions in this question list? Question dependencies remain in the order they were given."
        },
        shuffle_question_blocks: {
          title: "Randomiser les blocs (Constructeur de tests)",
          titleTooltip: "FR Shuffle the question blocks in this question list? (TO BE COMPLETED)"
        },
        shuffle_item_bank_rules: {
          title: "FR Shuffle Item Bank Rules",
          titleTooltip: "FR Shuffle the item bank rules?"
        },
        language: {
          title: "Langue de la sous-section",
          titleTooltip:
            "FR Which language will the content in this section be provided in? This is mandatory for screen readers to use the correct accent.",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "--"
        }
      },
      questionListRules: {
        title: "Règles d'assemblage de tests (Blocs d'items)",
        collapsableItemName: "Règle {0}",
        addButton: {
          title: "Ajouter un bloc",
          titleTooltip: "FR You must create Question Block Types in order to add rules."
        },
        delete: {
          title: "Supprimer",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        number_of_questions: {
          title: "Nombre d'items\u00A0:",
          titleTooltip:
            "FR The number of questions to select of the following question block type.",
          errorMessage: "Dois être un Nombre d'items valide"
        },
        question_block_type: {
          title: "Bloc\u00A0:",
          titleTooltip: "FR The question block type from which to select questions.",
          errorMessage: "Dois sélectionner une Règle d'assemblage de tests (Blocs d'items)"
        },
        order: {
          title: "Ordre du bloc\u00A0:",
          titleTooltip:
            "FR The order that this rule will appear in the question list. If there is no order, leave this field empty."
        },
        shuffle: {
          title: "Randomiser les items\u00A0:",
          titleTooltip:
            "FR Shuffle the questions in this rule? Question dependencies remain in the order they were given."
        }
      },
      itemBankRules: {
        title: "Règles d'assemblage de tests (Banque d'items)",
        collapsableItemName: "Groupement {0}",
        order: {
          title: "Ordre de la sous-section",
          titleTooltip: "FR The order that this rule will appear."
        },
        item_bank: {
          title: "Banque d'items",
          titleTooltip: "FR Item bank from which you want to pull the respective grouping from."
        },
        item_bank_bundle: {
          title: "Groupement\u00A0:",
          titleTooltip: "FR Respective Grouping from which the items list will be generated."
        }
      },
      sectionComponentPages: {
        title: "Sous-sections du test",
        description:
          'Création de définitions des sous-sections du test. Les sous-sections typiques peuvent inclure les "Directives du test" et les "Questions du test" dans la section "Questions du test" associée. Toutes les sections du test doivent comporter une sous-section. Si aucune sous-section n\'est nécessaire, veuillez en créer une et lui donner le même nom que sa section mère.',
        validationErrors: {
          mustContainPageContentTitlesError:
            'Erreur\u00A0: Toutes les Sous-sections de test, à l\'exception de celles dont le Type de sous-section est "ITEMS_(TEST_BUILDER)", doivent être associées à un Titre de section.'
        },
        addButton: "Ajouter une sous-section de test",
        delete: {
          title: "Supprimer",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        collapsableItemName: "Sous-section de test {0}",
        parentTestSectionComponent: {
          title: "Sous-section de test",
          titleTooltip: "FR The parent test section the displayed objects are displayed in."
        },
        fr_title: {
          title: "Nom français",
          titleTooltip:
            "FR Name of the side navigation tab when the site language is French. This field is hidden when the Test Section Component is SINGLE_PAGE.",
          errorMessage: "FR Not a valid Name"
        },
        en_title: {
          title: "Nom anglais",
          titleTooltip:
            "FR Name of the side navigation tab when the site language is French. This field is hidden when the Test Section Component is SINGLE_PAGE.",
          errorMessage: "FR Not a valid Name"
        },
        order: {
          title: "Ordre de la sous-section",
          titleTooltip: "FR The order in which this page appears in a side navigation panel.",
          errorMessage: "FR Not a valid Name"
        }
      },
      componentPageSections: {
        title: "Contenu des sections",
        description: "Ajouter du contenu aux sections et sous-sections de test.",
        collapsableItemName: "FR Section Component Page Order: {0}, Type: {1}",
        parentSectionComponentPage: {
          title: "Titres des sections",
          titleTooltip:
            "FR The parent side navigation tab that the objects displayed will belong to."
        },
        pageSectionLaguage: {
          title: "FR Language",
          titleTooltip:
            "FR If a test is bilingual, you must create page sections for each language. This will show all the page sections created for the selected language."
        },
        addButton: "Ajouter du contenu",
        delete: {
          title: "Supprimer",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        order: {
          title: "Ordre du contenu de la section\u00A0:",
          titleTooltip: "FR The order in which this page appears in a side navigation panel.",
          errorMessage: "FR Not a valid Name"
        },
        page_section_type: {
          title: "Type de contenu\u00A0:",
          titleTooltip:
            "FR The type of section to display (i.e. Markdwon, sample email, sample teask response).",
          errorMessage: "FR Not a valid type."
        },
        item_bank: {
          title: "Banque d'items\u00A0:",
          titleTooltip: "Sélectionnez les directives appropriées provenant d'une banque d’items",
          errorMessage: "FR Not a valid item bank."
        },
        item: {
          title: "Directive\u00A0:",
          titleTooltip: "Sélectionnez les directives à utiliser",
          errorMessage: "FR Not a valid instruction item."
        },
        instruction_preview: {
          title: "FR Selected Instruction's Content: ",
          titleTooltip: "FR This is a preview of the content of the selected instruction",
          errorMessage: "FR Not a valid instruction item."
        },
        content_type: {
          title: "Format de contenu\u00A0:",
          titleTooltip: "Sélectionnez le type de contenu",
          errorMessage: "FR Not a valid content type."
        },
        survey_link_text_to_display: {
          title: "Texte à afficher\u00A0:",
          titleTooltip: "Texte qui sera affiché en hyperlien",
          translationButtonTooltip: "Modifier les traductions",
          translationPopup: {
            title: "Lien du sondage - Texte à afficher - Traduction",
            en_name: {
              title: "Texte à afficher (ANG)\u00A0:"
            },
            fr_name: {
              title: "Texte à afficher (FRA)\u00A0:"
            }
          }
        },
        survey_link_value: {
          title: "URL\u00A0:",
          titleTooltip: "Saisir l'URL ici"
        }
      },
      itemBankInstructionPageSectionForm: {
        instructionDropdownTitle: "FR {0} - Historical ID: {1} - Version: {2}",
        itemContentDropdownOptions: {
          screenReader: "Lecteur d'écran",
          regular: "Régulier"
        }
      },
      markdownPageSection: {
        content: {
          title: "Contenu",
          titleTooltip: "FR The markdown content to display."
        }
      },
      sampleEmailPageSection: {
        email_id: {
          title: "FR Email ID",
          titleTooltip: "FR The email ID to display"
        },
        from_field: {
          title: "FR From",
          titleTooltip: "FR Who the email is from."
        },
        to_field: {
          title: "FR To",
          titleTooltip: "FR Who the email is to"
        },
        date_field: {
          title: "FR Date",
          titleTooltip: "FR The date displayed on the email."
        },
        subject_field: {
          title: "FR Subject",
          titleTooltip: "FR The subject displayed on the email."
        },
        body: {
          title: "FR Body",
          titleTooltip: "FR The body of the email to display."
        }
      },
      sampleEmailResonsePageSection: {
        cc_field: {
          title: "FR CC",
          titleTooltip: "FR Who is CC'd on the email"
        },
        to_field: {
          title: "FR To",
          titleTooltip: "FR Who the email is to"
        },
        response: {
          title: "FR Response",
          titleTooltip: "FR The email response."
        },
        reason: {
          title: "FR Reason",
          titleTooltip: "FR The reason for responding with this email."
        }
      },
      sampleTaskResonsePageSection: {
        response: {
          title: "FR Response",
          titleTooltip: "FR The task response."
        },
        reason: {
          title: "FR Reason",
          titleTooltip: "FR The reason for responding with this task."
        }
      },
      zoomImagePageSection: {
        small_image: {
          title: "FR Full Image Name",
          titleTooltip: "FR name of the image file including extension."
        },
        note: "FR Note",
        noteDescription:
          "FR In order for this image to be displayed properly you must provide the image to the IT team to upload to the application server."
      },
      treeDescriptionPageSection: {
        address_book_contact: {
          title: "FR Root Contact",
          titleTooltip: "FR The root contact that the tree description will start from."
        }
      },
      addressBook: {
        topTitle: "FR Address Book",
        description:
          "FR These are email like contacts used in Inbox style Test Section Components and Tree Description Component Page Sections.",
        collapsableItemName: "{0}",
        addButton: "FR Add Address Book Contact",
        name: {
          title: "FR Name",
          titleTooltip: "FR The name of the contact."
        },
        title: {
          title: "FR Title",
          titleTooltip: "FR The title of the contact."
        },
        parent: {
          title: "FR Parent Contact",
          titleTooltip: "FR The contact that this contact reports to."
        },
        test_section: {
          title: "FR Test Sections",
          titleTooltip:
            "FR Which test sections can a user send emails to this contact or if there is a tree description in a test section then this contact must be in that test section."
        }
      },
      questions: {
        topTitle: "Items",
        description:
          'Créer des items à l\'aide du Constructeur de tests. Pour créer des items, un type de sous-section "ITEMS_(CONSTRUCTEUR_DE_TEST)" doit être défini pour une sous-section de test donnée.',
        validationErrors: {
          uploadFailedMissingQuestionListQuestions:
            'Erreur\u00A0: Assurez-vous que toutes les Sous-sections de test dont le Type de sous-section est "ITEMS_(CONSTRUCTEUR_DE_TEST)" contiennent au moins un item.'
        },
        collapsableItemNames: {
          block: "[Bloc]",
          id: "[ID]",
          text: "[Texte]"
        },
        emailCollapsableItemName: "FR Email ID: {0}, From: {1}",
        addButton: "Nouvel item",
        searchResults: "{0} Item(s) trouvé(s)",
        pilot: {
          title: "Item expérimental",
          titleTooltip:
            "FR Is the question a pilot question? It's answer will not be counted towards a final score.",
          description: ""
        },
        questionBlockSelection: {
          title: "Nom du bloc d'items",
          titleTooltip: "FR The Question Block Type to narrow the number of results."
        },
        order: {
          title: "Ordre de l'item\u00A0:",
          titleTooltip:
            "FR Order that questions will be displayed in the test (if there are no dependencies and/or shuffle) - TO BE COMPLETED"
        },
        ppc_question_id: {
          title: "ID item R&D",
          titleTooltip: "FR PPC Question ID Tooltip Content - TO BE COMPLETED"
        },
        question_type: {
          title: "Format de réponse",
          titleTooltip:
            "FR i.e. multiple choice, email. This is pre-determined by the Test Section Component Type (Question List or Inbox)"
        },
        question_block_type: {
          title: "Nom du bloc d'items",
          titleTooltip:
            "FR The question block type identifier. Used to determine what questions are returned for a question lists rule set.",
          selectPlaceholder: "FR Please Select"
        },
        dependencies: {
          title: "Dépendants",
          titleTooltip: "FR Questions that this question must be presented with (in order)."
        },
        dependent_order: {
          title: "FR Dependent Order",
          titleTooltip:
            "FR The order this question is presented in when dependents exist. This is only used for ordering questions in a dependency sitaution."
        },
        shuffle_answer_choices: {
          title: "Randomiser les choix de réponses",
          titleTooltip: "FR If enabled, the answer choices will be shuffled - TO BE COMPLETED"
        },
        previewPopup: {
          title: "FR Question Preview"
        }
      },
      scoringMethod: {
        title: "Méthode de notation",
        validationErrors: {
          mustApplyScoringMethodError: "FR Error: Must apply a scoring method type"
        },
        description: "Sélectionnez la méthode de notation pour ce test.",
        scoringMethodUndefinedError: "Veuillez choisir une méthode de notation valide.",
        scoringMethodType: {
          title: "Méthode de notation\u00A0:",
          titleTooltip: "FR Scoring mechanism to use for score conversion"
        },
        resultValidIndefinitely: {
          title: "Période de validité du résultat: indéterminée",
          titleTooltip: `Un résultat de test avec une Période de validité "indéterminée" restera valide pour toujours ou jusqu'à ce que le candidat repasse le test.`
        },
        validityPeriod: {
          title: "Période de validité du résultat (mois)\u00A0:",
          titleTooltip: "La période (en mois) pendant laquelle le résultat du test est valide",
          errorMessage: "Doit être un nombre valide"
        },
        scoringPassFailMinimumScore: {
          title: "Note de passage\u00A0:",
          placeholder: "{FR minimum_score}",
          titleTooltip: "FR Minimum total score required to get a 'pass' mark",
          errorMessage: "Dois être un nombre valide"
        },
        scoringThreshold: {
          addButton: "Bande de scores",
          collapsingItem: {
            title: "Bande de scores\u00A0: {0} to {1} = {2}",
            minimumScore: {
              title: "Score minimal\u00A0:",
              placeholder: "{FR maximum_score}",
              titleTooltip: "FR Minimum required score to get the conversion value",
              errorMessage: "Dois être un score minimal valide"
            },
            maximumScore: {
              title: "Score maximal\u00A0:",
              placeholder: "{FR conversion_value}",
              titleTooltip: "FR Maximum required score to get the conversion value",
              errorMessage: "Dois être un score maximal valide"
            },
            enConversionValue: {
              title: "Valeur (anglais)\u00A0:",
              placeholder: "FR {conversion_value}",
              titleTooltip:
                "FR Conversion value to display when respecting the minimum and maximum scores",
              errorMessage: "Dois être une valeur valide"
            },
            frConversionValue: {
              title: "Valeur (français)\u00A0:",
              placeholder: "FR {conversion_value}",
              titleTooltip:
                "FR Conversion value to display when respecting the minimum and maximum scores",
              errorMessage: "Dois être une valeur valide"
            }
          },
          deletePopup: {
            title: "Confirmer la suppression",
            content:
              "FR Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
          },
          invalidCollectionError: "Dois s'agir d'étendues de bandes valides"
        }
      },
      questionSituations: {
        situation: {
          title: "FR Situation",
          titleTooltip:
            "FR The situation text to be displayed at the top of the question when a test is viewed in scorer mode."
        }
      },
      questionBlockTypes: {
        topTitle: "Noms des blocs d'items",
        description:
          'Définition des noms des blocs à utiliser avec les items. Les blocs ne doivent être utilisés que pour les items créés dans la section "Items" du Constructeur de tests. Pour les items tirés de la banque d\'items, utiliser plutôt la fonctionnalité "Groupement" de la banque d\'items.',
        collapsableItemName: "FR Question Block Type: {0}",
        addButton: "Ajouter un nom de bloc d'items",
        addName: "***AJOUTER UN NOM***",
        name: {
          title: "Nom",
          titleTooltip: "FR The unique identifier for a question block type.",
          errorMessage: "Dois être un nom valide."
        }
      },
      competencyTypes: {
        topTitle: "FR Competency Types",
        description:
          "FR These are types associated with questions. These types are assigned to questions for manual marking.",
        collapsableItemName: "FR Competency Type: {0}",
        addButton: "FR Add Competency Type",
        en_name: {
          title: "FR English Name",
          titleTooltip: "FR The unique identifier for a question block type."
        },
        fr_name: {
          title: "FR French Name",
          titleTooltip: "FR The unique identifier for a question block type."
        },
        max_score: {
          title: "FR Max Score",
          titleTooltip: "FR The max score a candidate can get for this competency."
        }
      },
      questionSections: {
        title: "Énoncé(s)\u00A0:",
        collapsableItemName: "Énoncé {0}",
        description: `Création et définition des sous-sections du test. Les sous-sections typiques peuvent inclure les "Directives du test" et les "Questions du test" dans la section "Questions du test" associée. Toutes les sections du test doivent comporter une sous-section. Si aucune sous-section n'est nécessaire, veuillez en créer une et lui donner le même nom que sa section mère.`,
        addButton: "Énoncé",
        delete: {
          title: "Supprimer",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        order: {
          title: "Ordre de l'énoncé",
          titleTooltip: "FR The order in which this component appears in a Top Tab navigation.",
          errorMessage: "FR Not a valid Name"
        },
        question_section_type: {
          title: "Type de contenu\u00A0:",
          titleTooltip: "FR The type of section. Such as: Markdown, Image, Video...",
          errorMessage: "FR Not a valid Type"
        }
      },
      answers: {
        title: "Choix de réponses\u00A0:",
        collapsableItemNames: {
          id: "[ID]",
          value: "[Valeur]",
          text: "[Texte]"
        },
        addButton: "Choix de réponse",
        delete: {
          title: "Supprimer",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        content: {
          title: "Contenu",
          titleTooltip: "FR The text to display for this answer.",
          errorMessage: "FR Not a valid input"
        },
        order: {
          title: "Ordre du choix de réponse",
          titleTooltip: "FR The order in which this answer appears."
        },
        ppc_answer_id: {
          title: "ID choix de réponse R&D",
          titleTooltip: " FR PPC Answer ID Tooltip Content - TO BE COMPLETED"
        },
        scoring_value: {
          title: "Valeur de la notation",
          titleTooltip:
            "FR The score to be added to the total test score when this answer is selected.",
          errorMessage: "FR Not a valid value"
        }
      },
      questionSectionMarkdown: {
        content: {
          title: "Contenu",
          titleTooltip: "FR Markdown to appear on the question."
        },
        delete: {
          title: "Confirmer la suppression",
          content:
            "FR Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
        }
      },
      questionSectionEnd: {
        content: {
          title: "FR Specific Question",
          titleTooltip:
            "FR The literal question after context to be answered. This allows screen readers to navigate directly to this section."
        },
        delete: {
          title: "Confirmer la suppression",
          content:
            "FR Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
        }
      },
      multipleChoiceQuestions: {
        header: "FR Question Sections"
      },
      emailQuestions: {
        scoringTitle: "FR Scoring",
        exampleRatingsTitle: "FR Example Ratings",
        competency_types: {
          title: "FR Competencies",
          titleTooltip: "FR The competencies being assessed in this question."
        },
        deleteDescription:
          "FR Deleting this item will delete the current object and any children this object may have. Are you sure you want to delete this object?",
        email_id: {
          title: "FR Email Id",
          titleTooltip:
            "FR Email Id is displayed at the top of the email and is the order in which emails are displayed."
        },
        to_field: {
          title: "FR To Field",
          titleTooltip: "FR The contact the email is to."
        },
        from_field: {
          title: "FR From Field",
          titleTooltip: "FR The Contact the email is from."
        },
        cc_field: {
          title: "FR CC Field",
          titleTooltip: "FR Contacts that are CC'd on this email."
        },
        date_field: {
          title: "FR Date",
          titleTooltip: "FR The date text to display on the email"
        },
        subject_field: {
          title: "FR Subject",
          titleTooltip: "FR The subject field to display on the email"
        },
        body: {
          title: "FR Body",
          titleTooltip: "FR The content of the email to display."
        }
      },
      situationRating: {
        score: {
          title: "FR Rating Score",
          titleTooltip:
            "FR This is the value that the example answered would be scored. Keep this to one decimal place."
        },
        example: {
          title: "FR Example Rationale",
          titleTooltip: "FR The Rationale behind giving the candidates answer this score."
        }
      },
      testManagement: {
        description: "Gérer le statut de cette version de test.",
        deactivateTestVersion: {
          // TODO: Remove because on and off are the same text
          title: "Statut de la version de test",
          titleTooltip:
            "FR De-activate this test version: Remove this test version from the sample tests pages or make it unavailable to be assigned by business operations (this does not invalidate currently active test accesses)."
        },
        activateTestVersion: {
          title: "Statut de la version de test",
          titleTooltip:
            "FR Make this test version available for use: Visible on the sample tests page or available to be assigned by business operations (as appropriate)."
        },
        uploadTestVersion: {
          title: "Téléverser la version de test au serveur",
          titleTooltip:
            "FR Upload the changes made to this test version to the server as a new version."
        },
        uploadTestVersionButton: "Téléverser",
        downloadTestVersion: {
          title: "Télécharger le JSON de la version de test",
          titleTooltip:
            "FR Extract a JSON file of this test version; this excludes any changes made."
        },
        downloadTestVersionButton: "Télécharger"
      }
    },
    // HR Coordinator Testing Page
    inPersonTesting: {
      title: "Bienvenue {0} {1}.",
      containerLabel: "Coordonateur RH",
      sideNavItems: {
        assessmentProcesses: "Processus d'évaluation",
        manageBillingContacts: "Gestion de la facturation"
      },
      assessmentProcesses: {
        title: "Processus d'évaluation",
        createProcess: {
          title: "Créer un processus",
          referenceNumberLabel: "Processus d'évaluation / Numéro de référence",
          departmentLabel: "Organisation :",
          durationLabel: "Durée du processus (jours civils) :",
          allowBookingExternalTcLabel:
            "Autoriser les réservations dans les centres de test des autres organisations :",
          defaultBillingContactLabel: "Personne-ressource pour la facturation :",
          contactEmailForCandidates: "Courriel de contact pour les candidats :",
          invalidContactEmailForCandidates: "Doit être une adresse courriel valide",
          testsToAdministerLabel: "Test(s) à administrer :",
          testsToAdministerTable: {
            columnOne: "Test",
            columnTwo: "Motif du test par défaut",
            columnThree: "Niveau requis par défaut",
            columnFour: "Actions",
            viewAvailableTestSessions: "Voir les séances de test disponibles",
            viewAvailableTestSessionsAccessibility: "Voir les séances de test disponibles pour {0}",
            deleteTestAccessibility: "Supprimer {0}",
            addTestToAdministerButton: "Test à administrer :",
            addTestToAdministerPopup: {
              title: "Ajouter un test à administrer",
              testSkillTypeLabel: "Type de test :",
              testSkillSubTypeLabel: "Test\u00A0:",
              defaultReasonForTestingLabel: "Motif du test par défaut",
              defaultLevelRequiredLabel: "Niveau requis par défaut",
              addTestButton: "Ajouter un test"
            },
            viewAvailableTestSessionsPopup: {
              title: "Séances de test disponibles",
              description: "Voici les séances de test disponibles pour {0}",
              table: {
                columnOne: "Date de la séance de test",
                columnTwo: "Centre de tests",
                columnThree: "Organisation",
                columnFour: "Ville",
                columnFive: "Places disponibles",
                noData: "Aucune séance de test n'est disponible pour le moment"
              }
            },
            deleteTestToAdministerPopup: {
              title: "Supprimer le test",
              systemMessageDescription: "Le test {0} sera supprimé du processus.",
              description: "Êtes-vous sûr de vouloir continuer?"
            }
          },
          createProcessButton: "Créer le processus",
          confirmationPopup: {
            title: "Le processus a été créé avec succès",
            systemMessageDescription: "Votre processus a été créé avec succès"
          }
        },
        assignCandidates: {
          title: "Candidats",
          referenceNumberLabel: "Processus d'évaluation / Numéro de référence :",
          sentDateLabel: "Date d'envoi initiale\u00A0:",
          departmentLabel: "Organisation requérante :",
          closingDateLabel: "Date d'échéance :",
          durationLabel: "Durée du processus (jours civils) :",
          durationError: "La durée du prcoessus doit être supérieure à {0}",
          allowBookingExternalTcLabel:
            "Autoriser les réservations dans les centres de tests des autres organisations :",
          updatesSuccessPopup: {
            title: "Les données ont été mises à jour avec succès.",
            systemMessageDescription: "Mise à jour des données du processus d'évaluation réussie "
          },
          contactEmailForCandidates: "Courriel de contact pour les candidats :",
          testsToAdministerLabel: "Liste des tests pour ce processus :",
          testsToAdministerTable: {
            columnOne: "Test",
            columnTwo: "Raison du test par défaut",
            columnThree: "Niveau requis par défaut",
            columnFour: "Actions",
            viewAvailableTestSessions: "Voir les séances de test disponibles",
            viewAvailableTestSessionsAccessibility: "Voir les séances de test disponibles pour {0}",
            viewAvailableTestSessionsPopup: {
              title: "Séances de test disponibles",
              description: "Voici les séances de test disponibles pour {0}",
              table: {
                columnOne: "Date de la séance de test",
                columnTwo: "Centre de tests",
                columnThree: "Organisation",
                columnFour: "Ville",
                columnFive: "Places disponibles",
                noData: "Aucune séance de test n'est disponible pour le moment"
              }
            }
          },
          assignedCandidatesLabel: "Liste de candidats :",
          assignedCandidatesTable: {
            addCandidateButton: "Ajouter un candidat",
            inviteCandidateButton: "Ajouter des candidats",
            columnOne: "Prénom",
            columnTwo: "Nom de famille :",
            columnThree: "Adresse courriel",
            columnFour: "Détails des tests",
            columnFourTooltip: "<Test> | <Raison du test> | <Niveau requis> | (<Statut>)",
            columnFourFormattedRelatedStatus: "| ({0})",
            columnFive: "Actions",
            editTooltip: "Modifier",
            editAccessibility: "Modifier {0} {1}",
            deleteAccessibility: "Supprimer {0} {1}",
            editAssignedCandidatePopup: {
              title: "Modifier un candidat assigné"
            },
            deleteAssignedCandidatePopup: {
              title: "Supprimer le candidat assigné",
              systemMessageDescription: "{0} {1} sera supprimé de la liste des candidats assignés",
              description: "Êtes-vous sûr de vouloir continuer?"
            }
          },
          addCandidatePopup: {
            title: "Ajouter un candidat",
            description: "Veuillez fournir les informations demandées",
            firstNameLabel: "Prénom :",
            invalidFirstName: "Le prénom contient un ou plusieurs caractères invalides",
            lastNameLabel: "Nom de famille :",
            invalidLastName: "Le nom de famille contient un ou plusieurs caractères invalides",
            emailLabel: "Adresse courriel :",
            invalidEmail: "Doit être une adresse courriel valide",
            duplicateEmailError:
              "Ce courriel a déjà été utilisé dans le cadre de ce processus d'évaluation.",
            billingContactLabel: "Personne-ressource pour la facturation",
            testsToAdministerTable: {
              columnOne: "Test",
              columnTwo: "Raison du test",
              columnThree: "Niveau requis",
              columnFour: "Assigner le test",
              columnFive: "Actions",
              columnSix: "Statut"
            },
            addCandidateButton: "Ajouter le candidat"
          },
          sendRequestConfirmationPopup: {
            title: "Envoyer les invitations?",
            titleSuccess: "Les invitations ont été envoyées avec succès",
            systemMessageDescription:
              "Vous ne pourrez pas modifier les invitations une fois qu'elles auront été envoyées.",
            systemMessageDescriptionSuccess: "Les invitations ont été envoyées avec succès.",
            description: "Êtes-vous sûr de vouloir continuer?",
            sendRequestButton: "Envoyer"
          },
          editProcessNamePopup: {
            title: "FR Edit {0}"
          },
          allowLastMinuteCancellationsToggle: "FR Allow Last Minute Cancellations (Oral only):",
          olaCheckbox1:
            "Je confirme que le(s) candidat(s) ont des résultats valides pour le Test de compréhension de l'écrit et le Test d'expression écrite qui correspondent au niveau requis.",
          olaCheckbox2:
            "J'atteste que le processus de dotation est impératif et que la nomination est imminente.",
          sendRequestButton: "Envoyer les invitations",
          deleteProcessPopup: {
            title: "FR Delete Assessment Process",
            message: "FR Assessment Process {0} will be deleted.",
            description: "FR Are you sure you want to proceed?"
          }
        },
        activeProcesses: {
          title: "Processus actifs",
          sendUpdatesButton: "Envoyer les mises à jour",
          updatesSuccessPopup: {
            title: "Les données ont été mises à jour avec succès.",
            systemMessageDescription:
              "Les données du processus d'évaluation ont été modifiée avec succès. Les candidats concernés seront informés par courrier électronique."
          },
          inviteCandidatePopup: {
            title: "Inviter des candidats",
            inviteCandidateButton: "Envoyer les invitations"
          },
          editCandidatePopup: {
            sendUpdatesButton: "FR Send Updates",
            unassignTestTooltip: "Désassigner le test",
            unassignTestAccessibility: "Désassigner le test {0}",
            unassignTestPopup: {
              title: "Désassigner le test",
              systemMessageDescription:
                "Cette action ne peut être annulée. Ce test ne peut pas être renvoyé à ce candidat pour ce processus.",
              description: "Êtes-vous certain de vouloir continuer?",
              unassignButton: "Désassigner",
              unassignLoading: "FR Unassigning..."
            }
          }
        },
        processResults: {
          title: "Résultats",
          table: {
            noData: "Aucun résultat n'est disponible.",
            topTabs: {
              columnOne: "Processus d'évaluation / Numéro de référence",
              columnTwo: "Date d'échéance",
              columnThree: "Candidats",
              columnFour: "Tests complétés",
              columnFive: "Actions"
            },
            viewAssessmentProcessButton: {
              ariaLabel: "Voir le processus d'évaluation : {0}",
              tooltipContent: "Voir le processus d'évaluation"
            },
            downloadReportAssessmentProcessButton: {
              ariaLabel:
                "Télécharger le rapport de résultats de test du processus d'évaluation : {0}",
              tooltipContent:
                "Télécharger le rapport de résultats de test du processus d'évaluation"
            },
            viewProcessResultPopup: {
              noData: "Il n'y a aucune donnée disponible",
              columnFive: "Résultats de test"
            }
          }
        }
      },
      manageBillingContacts: {
        title: "Gérer les informations de facturation",
        table: {
          topTabs: {
            columnOne: "Prénom",
            columnTwo: "Nom de famille",
            columnThree: "Adresse courriel",
            columnFour: "Organisation",
            columnFive: "RI - Code d'organisation",
            columnSix: "RI - Code de référence",
            columnSeven: "Actions"
          },
          noData: "Votre recherche n'a généré aucun résultat.",
          addBillingContactButton: "Ajouter",
          modifyBillingContactButton: {
            ariaLabel: "Modifier la personne-ressource ({0} {1})",
            tooltipContent: "Modifier"
          },
          deleteBillingContactButton: {
            ariaLabel: "Supprimer la personne-ressource ({0} {1})",
            tooltipContent: "Supprimer"
          }
        },
        billingContactPopup: {
          firstName: {
            title: "Prénom :",
            error: "Le prénom contient un ou plusieurs caractères invalides"
          },
          lastName: {
            title: "Nom de famille :",
            error: "Le nom de famille contient un ou plusieurs caractères invalides"
          },
          email: {
            title: "Adresse courriel :",
            error: "Doit être une adresse courriel valide"
          },
          departmentId: {
            title: "Organisation :",
            error: "FR Department Error"
          },
          fisOrgCode: {
            title: "RI - Code d'organisation :",
            error: "RI - Code d'organisation :"
          },
          fisRefCode: {
            title: "RI - Code de référence :",
            error: "RI - Code de référence :"
          }
        },
        addBillingContactPopup: {
          title: "Ajouter une personne-ressource pour la facturation",
          addButton: "Ajouter",
          description:
            "Ajouter les informations de facturation requises. En cas de doute, vérifiez auprès de votre gestion ou de votre équipe des finances."
        },
        modifyBillingContactPopup: {
          title: "Modifier la personne-ressource pour la facturation",
          description:
            "Les modifications apportées à cette personne-ressource ne seront applicables qu'à ses prochaines utilisations."
        },
        deleteBillingContactPopup: {
          title: "Supprimer la personne-ressource pour la facturation",
          message:
            " {0} {1} sera supprimé de votre liste de personnes-ressources pour la facturation.",
          description: "Êtes-vous sûr de vouloir continuer?"
        },
        errorBillingContactPopup: {
          title: {
            addBillingContact: "Erreur - Ajouter une personne-ressource pour la facturation",
            modifyBillingContact: "Erreur - Modifier la personne-ressource pour la facturation",
            deleteBillingContact: "Erreur - Supprimer la personne-ressource pour la facturation"
          },
          description: {
            addBillingContact:
              "Une erreur s’est produite lors de l'ajout de la personne-ressource pour la facturation. Veuillez réessayer.",
            modifyBillingContact:
              "Une erreur s’est produite lors de la modification de la personne-ressource pour la facturation. Veuillez réessayer.",
            deleteBillingContact:
              "Une erreur s’est produite lors de la suppression de la personne-ressource pour la facturation. Veuillez réessayer."
          }
        }
      }
    },
    // Test Administration Page
    testAdministration: {
      title: "Bienvenue {0} {1}.",
      containerLabel: "Administrateur de tests",
      sideNavItems: {
        supervisedTesting: "Tests supervisés",
        activeCandidates: "Candidats actifs",
        uat: "Tests en ligne non supervisés",
        reports: "Rapports"
      },
      supervisedTesting: {
        description:
          "Au fur et à mesure que les candidats s'enregistrent, leurs noms seront affichés dans la liste des candidats actifs ci-dessous. Une fois que tous les candidats se sont enregistrés, supprimez le code d'accès au test.",
        testSessionLabel: "Séance de tests :",
        testSessionAttendees: {
          viewAttendeesTooltip: "Afficher la liste de candidats",
          popup: {
            title: "Liste de candidats",
            description1: "Date de la séance de test : {0}",
            description2: "Salle : {0}",
            description3: "Test : {0}",
            table: {
              columnOne: "Prénom",
              columnTwo: "Nom de famille",
              columnThree: "Adresse courriel ",
              columnFour: "Courriel du compte",
              noData: "Il n'y a aucun candidat pour l'instant."
            },
            printButton: "Imprimer",
            printDocumentTitle: "Liste des candidats pour la séance de test {0}"
          }
        },
        testSessionTestAccessCodes: "Codes d'accès aux tests de cette séance\u00A0:",
        testSessionAssignedCandidates: "Candidats pour cette séance\u00A0:",
        testAccessCodesTable: {
          testAccessCode: "Code d'accès au test",
          test: "Test",
          action: "Action",
          actionButton: "Supprimer",
          actionButtonAriaLabel: "Supprimer {0}",
          generateNewCode: "Générer le code d'accès au test"
        },
        generateTestAccessCodePopup: {
          title: "Générer un code d'accès au test",
          description:
            "Veuillez fournir les renseignements suivants pour générer un code d'accès au test\u00A0:",
          testSession: "Séance de tests :",
          testToAdminister: "Test\u00A0:",
          cannotGenerateTestAccessCodeError:
            "FR You do not have the right test permissions to generate a test access code for that particular test session",
          testSessionOfficers: "Agent de séance de test :",
          billingInformation: {
            title: "Renseignements sur la facturation\u00A0:",
            staffingProcessNumber: "Numéro du processus d'évaluation\u00A0:",
            departmentMinistry: "Code de l'organisation\u00A0:",
            contact: "Nom du responsable de la facturation\u00A0:",
            isOrg: "Code d'organisation SIF\u00A0:",
            isRef: "Code de référence SIF\u00A0:"
          },
          generateButton: "Générer"
        },
        disableTestAccessCodeConfirmationPopup: {
          title: "Supprimer le code d'accès au test",
          description:
            "Si vous supprimez le code d'accès au test {0}, les candidats ne pourront pas l'utiliser pour s'enregistrer."
        },
        approveAllButton: "Tout approuver",
        lockAllButton: "Tout verrouiller",
        unlockAllButton: "Tout déverrouiller ",
        assignedCandidatesTable: {
          candidate: "Nom et coordonnées du candidat",
          dob: "Date de naissance",
          testCode: "Version de test",
          status: "Statut",
          timer: "Durée du test",
          timeRemaining: "Temps restant",
          actions: "Action",
          actionTooltips: {
            updateTestTimer: "Modifier la durée du test",
            addEditBreakBank: "Ajouter/Modifier la banque de pauses",
            approve: "Admettre un candidat",
            unAssign: "Enlever l'accès",
            lock: "Verrouiller un test",
            unlock: "Déverrouiller un test",
            report: "FR Report Candidate"
          },
          updateTestTimerAriaLabel:
            "Modifier la durée du test de l'utilisateur {0} {1}. Sa date de naissance est {2}. La version actuelle de son test est {3}. Le statut actuel de son test est {4}. La durée du test est actuellement réglée à {5} heures et {6} minutes.",
          addEditBreakBankAriaLabel:
            "Ajouter / Modifier la banque de pauses de l'utilisateur {0} {1}. Sa date de naissance est {2}. Sa version de test est {3}. Son statut actuel de test est {4}. Sa limite de temps est actuellement réglée sur {5} heures et {6} minutes.",
          approveAriaLabel:
            "Admettre le candidat {0} {1}. Sa date de naissance est {2}. La version actuelle de son test est {3}. Le statut actuel de son test est {4}. La durée du test est actuellement réglée à {4} heures et {6} minutes.",
          unAssignAriaLabel:
            "Enlever l'accès de l’utilisateur {0} {1}. Sa date de naissance est {2}. La version actuelle de son test est {3}. Le statut actuel de son test est {4}. La durée du test est actuellement réglée à {5} heures et {6} minutes.",
          lockAriaLabel:
            "Verrouiller le test de {0} {1}. Sa date de naissance est {2}. La version actuelle de son test est {3}. Le statut actuel de son test est {4}. La durée du test est actuellement réglée à {5} heures et {6} minutes.",
          unlockAriaLabel:
            "Déverrouiller le test de {0} {1}. Sa date de naissance est {2}. La version actuelle de son test est {3}. Le statut actuel de son test est {4}. La durée du test est actuellement réglée à {5} heures et {6} minutes.",
          reportAriaLabel: "FR Report {0} {1} user.",
          noActiveCandidates: "Il n'y a aucun candidat actif"
        },
        editTimePopup: {
          title: "Modifier la durée du test",
          description1: "À utiliser en tant que mesure d'adaptation seulement.",
          description2: "Modifier la durée du test pour {0} {1}.",
          description3:
            "Le chronomètre ne peut être réglé pour une durée moindre que la durée initialement prévue pour compléter une section spécifique.",
          description4: "Durée totale du test\u00A0: {0} Heures {1} Minutes",
          hours: "HEURES",
          minutes: "MINUTES",
          incrementHoursButton: "Augmenter le nombre d'heures. La valeur actuelle est {0}.",
          decrementHoursButton: "Réduire le nombre d'heures. La valeur actuelle est {0}.",
          incrementMinutesButton: "Augmenter le nombre de minutes. La valeur actuelle est {0}.",
          decrementMinutesButton: "Réduire le nombre de minutes. La valeur actuelle est {0}.",
          timerCurrentValue: "La durée du test est de {0} heures {1} minutes.",
          setTimerButton: "Régler le chronomètre"
        },
        addEditBreakBankPopup: {
          title: "Régler la banque de pauses",
          description: "Attribuer la banque de pauses à {0} {1} pour ce test.",
          setTimeTitle: "Banque de pauses",
          setBreakBankButton: "Inscrire"
        },
        lockPopup: {
          title: "Verrouiller un test",
          description1: "Vous êtes sur le point de verrouiller le test de {0} {1}.",
          lockButton: "Verrouiller le test"
        },
        unlockPopup: {
          title: "Reprendre un test",
          description1: "Vous êtes sur le point de déverrouiller le test de {0} {1}.",
          description2: "Le chronomètre du test sera réactivé une fois le test déverrouillé.",
          description3: "Assurez-vous de remplir un rapport d'incident pour ce candidat.",
          unlockButton: "Déverrouiller le test"
        },
        lockAllPopup: {
          title: "Verrouiller les tests de tous les candidats",
          description1:
            "Vous êtes sur le point de verrouiller ce test pour tous les candidats actifs.",
          description2:
            "Si, suite à l'incident qui a nécessité de verrouiller ce test, il est possible de reprendre le test, vous devrez déverrouiller les tests des candidats qui souhaitent poursuivre.",
          description3:
            "Assurez-vous de compléter un rapport d'incident et de remplir un formulaire de retrait du candidat pour tous les candidats qui ne poursuivront pas le test.",
          lockTestsButton: "Verrouiller les tests"
        },
        unlockAllPopup: {
          title: "Déverrouiller les tests de tous les candidats",
          description1:
            "Vous êtes sur le point de déverrouiller ce test pour tous les candidats actifs.",
          description2:
            "Assurez-vous de verrouiller les tests des candidats ne souhaitant pas ou ne pouvant pas poursuivre.",
          description3:
            "De plus, veuillez remplir un formulaire de retrait du candidat ainsi qu'un rapport d'incident pour les candidats qui ne poursuivent pas le test.",
          unlockTestsButton: "Déverrouiller les tests"
        },
        approveAllPopup: {
          title: "Approuver tous les candidats",
          description:
            'Vous êtes sur le point d\'approuver tous vos candidats actifs. Veuillez vérifier que les candidats listés dans la section "Candidats pour cette séance" correspondent à ceux qui étaient initialement prévus pour passer le test.',
          approveAllButton: "Tout approuver"
        },
        unAssignPopup: {
          title: "Enlever l'accès",
          description: "Voulez-vous vraiment enlever l'accès au test de {0} {1} ?",
          confirm: "Enlever l'accès"
        }
      },
      uit: {
        title: "Tests en ligne non supervisés (TELNS)",
        description: "Gérer vos invitations et processus de TELNS.",
        tabs: {
          invitations: {
            title: "Invitations",
            testOrderNumber: "Numéro de commande du test\u00A0:",
            testOrderNumberCurrentValueAccessibility:
              "Le numéro de commande du test sélectionné est\u00A0:",
            testOrderNumberErrorMessage: "Veuillez sélectionner le numéro de commande du test",
            orderlessTestOption: "Pas de commande de test",
            reuseData: "Réutiliser les données\u00A0:",
            referenceNumber: "Numéro de référence interne\u00A0:",
            referenceNumberError: "Veuillez indiquer un numéro de référence interne",
            departmentMinistry: "Organisation\u00A0:",
            fisOrganisationCode: "RI - Code d'organisation :",
            fisOrganisationCodeError: "Veuillez indiquer un code de SIF de l'organisation",
            fisReferenceCode: "RI - Code de référence :",
            fisReferenceCodeError: "Veuillez indiquer un code de référence de SIF",
            billingContactName: "Personne-ressource pour la facturation\u00A0:",
            billingContactNameError: "Veuillez indiquer une personne-ressource pour la facturation",
            billingInformation: "Courriel de contact pour les candidats\u00A0:",
            billingInformationError: "Doit être une adresse courriel valide",
            test: "Test\u00A0:",
            providedInCsvOption: "Fourni dans la liste",
            reasonForTesting: "Motif du test\u00A0:",
            invalidReasonForTesting: "Veuillez choisir le motif du test",
            levelRequired: "Niveau demandé\u00A0:",
            levelRequiredRawScore: "Score brut requis\u00A0:",
            invalidLevelRequired: "Veuillez choisir le niveau demandé",
            invalidLevelRequiredRawScore: "Le nombre fourni dépasse le score maximal possible",
            testCurrentValueAccessibility: "Les tests sélectionnées sont\u00A0: {0}",
            testsErrorMessage: "Veuillez sélectionner au moins un test",
            validityEndDate: "Date d'échéance\u00A0:",
            numberOfCandidatesBasedOnEndDateExceededError:
              "Le nombre maximum de candidats terminant le même jour est dépassé. Veuillez choisir une autre date d'échéance.",
            emailTemplate: "Gabarit du courriel\u00A0:",
            emailTemplateCurrentValueAccessibility: "Le gabarit du courriel sélectionné est\u00A0:",
            candidates: "Liste des candidats\u00A0:",
            candidatesCurrentValueAccessibility: "Les candidats sélectionnés sont\u00A0:",
            browseButton: "Parcourir...",
            fileSelectedAccessibility: "Le fichier sélectionné est {0}",
            downloadTemplateButton: "Télécharger le gabarit",
            csvTemplateFileName: "Liste des candidats.xlsx",
            wrongFileTypeError: "Veuillez sélectionner un fichier de type CSV",
            invalidFileError:
              "Erreur trouvée dans le fichier. Veuillez corriger les renseignements dans les cellules suivantes\u00A0: {0}",
            noDataInCsvFileError:
              "Veuillez vous assurer que votre fichier CSV contient des données",
            noCsvFileSelectedError: "Veuillez sélectionner un fichier CSV valide",
            maximumInvitedCandideReachedError:
              "Ne peut pas dépasser un total de 500 invitations à la fois (y compris les demandes pour des tests multiples)",
            duplicateEmailsFoundInCsvError:
              "Des courriels en double ont été trouvés. Veuillez corriger les renseignements relatifs aux cellules {0} et {1}.",
            sendInvitesButton: "Envoyer les invitations",
            resetAllFields: "Réinitialiser tous les champs",
            invitesConfirmationPopup: {
              title: "Réviser et envoyer l'invitation aux candidats",
              titleInvitationsSentSuccessfull: "Invitations envoyées avec succès",
              titleInvitationsSentFail: "Invitations non envoyées",
              inviteData: {
                processNumber: "Numéro du processus d'évaluation\u00A0:",
                referenceNumber: "Numéro de référence interne\u00A0:",
                requestingDepartment: "Organisation requérante\u00A0:",
                fisOrganisationCode: "Code de SIF de l'organisation\u00A0:",
                fisReferenceCode: "Code de référence de SIF\u00A0:",
                HrCoordinator: "Personne-ressource pour la facturation\u00A0:",
                billingContactInfo: "Courriel de contact pour les candidats\u00A0:",
                test: "Test\u00A0:",
                validityEndDate: "Date d'échéance\u00A0:",
                reasonForTesting: "Motif du test\u00A0:",
                levelRequired: "Niveau demandé\u00A0:",
                levelRequiredRawScore: "Score brut requis\u00A0:",
                table: {
                  title: "Liste des candidats\u00A0({0})\u00A0:",
                  emailAddress: "Adresse courriel",
                  firstName: "Prénom",
                  lastName: "Nom de famille"
                },
                invitationSentSuccessfullMessage: "Les invitations ont été envoyées aux candidats.",
                invitationSentFailMessage:
                  "Quelque chose s'est produit et les invitations n'ont pas été envoyées ! Veuillez contacter le service de dépannage."
              },
              sendButton: "Envoyer"
            },
            resetAllFieldsPopup: {
              title: "Réinitialiser tous les champs ?",
              description: "Êtes-vous sûr de vouloir réinitialiser tous les champs ?",
              resetButton: "Réinitialiser"
            }
          },
          activeProcessess: {
            title: "Processus actifs",
            table: {
              columnOne: "Processus d'évaluation/numéro de référence",
              columnTwo: "Test",
              columnThree: "Nombre de tests passés",
              columnFour: "Organisation requérante",
              columnFive: "Date de l'invitation",
              columnSix: "Date d'échéance",
              columnSeven: "Actions",
              noActiveProcessess: "Il n'y a aucun processus en cours",
              viewSelectedProcessTooltip: "Afficher les renseignements liés au processus actif",
              updateSelectedProcessExpiryDate: "Mise à jour de la date d'échéance du processus",
              deleteSelectedProcessTooltip: "Supprimer le processus actif",
              viewButtonAccessibility: "Afficher le numéro du processus d'évaluation {0}",
              updateSelectedProcessExpiryDateAccessibility:
                "Mise à jour de la date d'échéance du processus {0}",
              deleteButtonAccessibility: "Supprimer le numéro du processus d'évaluation {0}"
            },
            selectedProcessPopup: {
              title: "Renseignements liés au processus actif",
              financialData: {
                processNumber: "Processus d'évaluation/numéro de référence\u00A0:",
                requestingDepartment: "Organisation requérante\u00A0:",
                HrCoordinator: "Nom du responsable de la facturation\u00A0:",
                validityEndDate: "Date d'échéance\u00A0:",
                test: "Test\u00A0:"
              },
              description: "Les candidats suivants ({0}) ont été invités à faire le TELNS\u00A0:",
              columnOne: "Adresse courriel",
              columnTwo: "Statut",
              columnThree: "Actions",
              testInProgress: "En cours",
              testTaken: "Fait",
              testNotTaken: "Non fait",
              testUnassigned: "Désassigné",
              testInvalidated: "Invalidé",
              testCodeDeactivated: "Désactivé",
              deleteCandidateTooltip: "Désassigner/désactiver l'accès au test",
              deleteCandidateAccessibility: "Désassignation/désactivation de l'accès au test de {0}"
            },
            updateProcessExpiryDatePopup: {
              title: "Modifier la date d'échéance",
              titleSuccess: "Mise à jour du processus réussie\u00A0: {0}",
              validityEndDate: "Date d'échéance\u00A0:",
              reasonForModification: "Raison de la modification\u00A0:",
              rightButton: "Modifier",
              successMessage: "La date d'échéance a été modifiée et les courriels ont été envoyés."
            },
            deleteProcessPopup: {
              title: "Supprimer le numéro du processus d'évaluation\u00A0: {0}",
              titleSuccess: "Supression du processus réussie\u00A0: {0}",
              systemMessageDescription:
                "Vous êtes sur le point de supprimer le processus suivant. Notez qu'il y a encore {0} candidat(s) qui n'ont pas encore fait leur test. Êtes-vous sûr de vouloir continuer ?",
              description: "Renseignements sur le processus\u00A0:",
              deleteButton: "Supprimer le processus",
              successMessage:
                "Le numéro du processus d'évaluation a été supprimé et les courriels d'annulation ont été envoyés."
            },
            deleteSingleUitTestPopup: {
              title: "Confirmer la désassignation/désactivation de l'accès au test",
              description:
                "Êtes-vous sûr de vouloir désassigner/désactiver l'accès de {0} au test ?",
              deleteButton: "Désassigner/désactiver",
              selectReason: "Veuillez choisir une raison\u00A0:"
            },
            uitTestAlreadyStartedPopup: {
              title: "Action refusée"
            }
          },
          completedProcessess: {
            title: "Processus complétés",
            table: {
              process: "Processus d'évaluation/numéro de référence",
              test: "Test",
              totalCandidates: "Nombre de tests passés",
              testsTaken: "Organisation requérante",
              endDate: "Date d'échéance",
              actions: "Action",
              actionTooltip: "Afficher les renseignements liés au processus complété"
            },
            noCompletedProcessess: "Il n'y a aucun processus complété",
            popup: {
              title: "TELNS\u00A0: {0} - {1}",
              description: "Les candidats suivants ({0}) ont été invités à passer le TELNS\u00A0:"
            }
          }
        }
      },
      reports: {
        description:
          "Générer un rapport de résultats pour une commande de test que vous avez administré."
      }
    },

    // PPC  Administration Page
    ppcAdministration: {
      title: "Bienvenue {0} {1}.",
      containerLabel: "Tableau de bord de R&D",
      sideNavItems: {
        reports: "Rapports"
      },
      reports: {
        title: "Rapports",
        description: "Générer un rapport en lien avec une commande de test."
      }
    },

    // Test Center Manager Page
    testCenterManager: {
      title: "Bienvenue {0} {1}.",
      containerTestCentersLabel: "Centres de tests",
      containerOlaLabel: "FR OLA",
      table: {
        columnOne: "Centre de tests",
        columnTwo: "Adresse",
        columnThree: "Nombre de salles",
        columnFour: "Nombre d'administrateurs de test",
        columnFive: "Demandes en attente",
        columnSix: "Action",
        viewButtonAccessibility: "Afficher le centre de test {0}",
        viewButtonTooltip: "Afficher",
        noTestCenter: "Il n'y a pas de centre de tests à afficher"
      },
      testCenterManagement: {
        topTabTitle: "{0}",
        sideNavigationItems: {
          testCenterManagement: {
            title: "Gestion des centres de tests",
            topTabs: {
              centerInfo: {
                title: "Informations sur le centre de tests",
                departmentLabel: "Organisation :",
                addressLabel: "Adresse :",
                cityLabel: "Ville :",
                postalCodeLabel: "Code postal :",
                provinceLabel: "Province :",
                countryLabel: "Pays :",
                securityEmailLabel: "Courriel du commissionnaire :",
                securityEmailErrorMessage: "Doit être une adresse courriel valide",
                bookingDelayLabel: "Clôture des réservations (heures avant la session de test)",
                otherDetailsLabel: "Autres informations :",
                accommodationsFriendly: "Test peut être administré avec mesures d'adaptation",
                translationsPopup: {
                  address: {
                    title: "Traduction - Adresse",
                    translationsButtonAriaLabel: "Modifier la traduction",
                    translationsButtonTooltip: "Modifier la traduction",
                    en_name: {
                      title: "Adresse en anglais\u00A0:"
                    },
                    fr_name: {
                      title: "Adresse en français\u00A0:"
                    }
                  },
                  city: {
                    title: "Traduction - Ville",
                    translationsButtonAriaLabel: "Modifier la traduction",
                    translationsButtonTooltip: "Modifier la traduction",
                    en_name: {
                      title: "Ville en anglais\u00A0:"
                    },
                    fr_name: {
                      title: "Ville en français\u00A0:"
                    }
                  },
                  province: {
                    title: "Traduction - Province",
                    translationsButtonAriaLabel: "Modifier la traduction",
                    translationsButtonTooltip: "Modifier la traduction",
                    en_name: {
                      title: "Province en anglais\u00A0:"
                    },
                    fr_name: {
                      title: "Province en français\u00A0:"
                    }
                  },
                  otherDetails: {
                    title: "Traduction - Autre détails",
                    translationsButtonAriaLabel: "Modifier la traduction",
                    translationsButtonTooltip: "Modifier la traduction",
                    en_name: {
                      title: "Autres Informations en anglais\u00A0:"
                    },
                    fr_name: {
                      title: "Autres informations en français\u00A0:"
                    }
                  }
                }
              },
              testAdministrators: {
                title: "Administrateurs de tests",
                addTestAdministratorButton: "Ajouter un administrateur de tests",
                addTestAdministratorPopup: {
                  title: "Ajouter des administrateurs de tests",
                  description:
                    "Ajoutez les administrateurs de tests que vous souhaitez associer à ce centre de tests.",
                  testAdministratorsLabel: "Administrateur(s) de tests :",
                  testAdministratorsCurrentValueAccessibility:
                    "Les administrateurs de tests sélectionnés sont :",
                  addTestAdministratorsButton: "Sauvegarder"
                },
                table: {
                  columnOne: "Prénom",
                  columnTwo: "Nom de famille",
                  columnThree: "Courriel",
                  columnFour: "Action",
                  deleteButtonTooltip: "Supprimer",
                  deleteButtonAccessibility:
                    "Supprimer {0} {1} de la liste des  administrateurs de tests associés",
                  noData: "Il n'y a pas d'administrateurs de tests associés à ce centre de tests."
                },
                deleteTestAdministratorPopup: {
                  title: "Suprimer l'association de l'administrateur de tests ",
                  systemMessageDescription:
                    "Cette action supprimera {0} {1} de la liste des administrateurs de tests associés.",
                  description: "Souhaitez-vous poursuivre?"
                }
              },
              rooms: {
                title: "Salles",
                addRoomButton: "Ajouter une salle",
                addRoomPopup: {
                  title: "Ajouter une salle",
                  description: "Indiquez les détails de la salle que vous souhaitez ajouter.",
                  nameLabel: "Nom de la salle :",
                  emailLabel: "Courriel de la salle :",
                  emailErrorMessage: "Doit être une adresse courriel valide",
                  maximumOccupancyLabel: "Capacité maximale :",
                  otherDetailsLabel: "Informations supplémentaires :",
                  activeLabel: "Active :",
                  addRoomButton: "Ajouter la salle"
                },
                table: {
                  columnOne: "Nom de la salle",
                  columnTwo: "Capacité de la salle",
                  columnThree: "Statut",
                  columnFour: "Actions",
                  editButtonTooltip: "Modifier",
                  editButtonAccessibility: "Modifier la salle {0}",
                  deleteButtonTooltip: "Supprimer",
                  deleteButtonAccessibility: "Supprimer la salle {0}",
                  noData: "Il n'y a aucune salle associée à ce centre de tests."
                },
                editRoomPopup: {
                  title: "Modifier la salle",
                  titleSuccess: "Salle mise à jour avec succès",
                  description: "Indiquez les détails de la pièce que vous souhaitez modifier.",
                  nameLabel: "Nom de la salle :",
                  emailLabel: "Courriel de la salle :",
                  emailErrorMessage: "FR Must be a valid email",
                  maximumOccupancyLabel: "Capacité maximale :",
                  otherDetailsLabel: "Informations supplémentaires :",
                  activeLabel: "Active\u00A0:",
                  systemMessageDescription: "La salle {0} a été mise à jour avec succès."
                },
                deleteRoomPopup: {
                  title: "Supprimer la salle",
                  titleWithError: "Salle occupée",
                  systemMessageDescription: "Cette action supprimera la salle {0}.",
                  systemMessageDescriptionWithError:
                    "La salle {0} ne peut pas être supprimée. Elle est actuellement utilisée pour des séances de test en cours ou à venir.",
                  description: "Souhaitez-vous poursuivre?"
                },
                translationsPopup: {
                  otherDetails: {
                    title: "Traduction - Informations supplémentaires",
                    translationsButtonAriaLabel: "Modifier la traduction",
                    translationsButtonTooltip: "Modifier la traduction",
                    en_name: {
                      title: "Informations supplémentaires en anglais\u00A0:"
                    },
                    fr_name: {
                      title: "Informations supplémentaires en français"
                    }
                  }
                }
              }
            }
          },
          testSessions: {
            title: "Séances de tests",
            topTabs: {
              testSessionsData: {
                standardTitle: "Séances de test standard",
                nonStandardTitle: "Séances de test non-standard",
                addTestSessionButton: "Ajouter une séance de test",
                addTestSessionPopup: {
                  title: "Ajouter une séance de test",
                  description:
                    "Indiquez les détails de la séance de test que vous souhaitez ajouter.",
                  tabName: "Séance {0}",
                  roomLabel: "Salle\u00A0:",
                  roomOptionalLabel: "Salle (facultatif)\u00A0:",
                  roomCapacity: "FR Capacity:",
                  openToOgdLabel: "Ouvert à d'autres ministères :",
                  dateLabel: "Date\u00A0:",
                  startTimeLabel: "Heure de début :",
                  endTimeLabel: "Heure de fin :",
                  invalidStartEndTimeCombination:
                    "L'heure du début doit être plus tôt que l'heure de la fin",
                  invalidStartDateAndTimeBasedOnBookingDelay:
                    "FR Start time is not respecting the booking delay",
                  spacesAvailableLabel: "Places disponibles",
                  invalidSpacesAvailableError:
                    "Le nombre de places ne peut pas excéder la capacité maximale de la salle sélectionnée.",
                  testSkillType: "Type de test :",
                  testSkillSubType: "Test :",
                  test: "Test (tel que défini par l'Unité d’adaptation en matière d’évaluation):",
                  testAssessor: "Évaluateur : ",
                  addTestSessionButton: "Sauvegarder",
                  deleteTestSessionTabPopup: {
                    title: "Supprimer la séance de test",
                    systemMessageDescription: "La séance de test sera supprimée.",
                    description: "Voulez-vous vraiment supprimer cette séance de test?"
                  },
                  invalidTestSessionDateAndTimeCombinationPopup: {
                    title: "Date et heure invalides",
                    systemMessageDescription:
                      "Veillez à ce que toutes les séances de test soient en ordre chronologique.",
                    description:
                      "Veuillez fermer cette fenêtre et apporter les modifications nécessaires."
                  }
                },
                table: {
                  columnOne: "Date de la séance de test",
                  columnTwo: "Salle",
                  columnThree: "Test",
                  columnFour: "Places disponibles",
                  columnFive: "Actions",
                  viewButtonTooltip: "Voir le processus d'évaluation",
                  viewButtonAccessibility: "Voir la liste de candidats pour la salle {0} le {1}",
                  editButtonTooltip: "Modifier",
                  editButtonAccessibility: "Modifier la session de test du {0} de la salle {1}.",
                  deleteButtonTooltip: "Supprimer",
                  deleteButtonAccessibility: "Supprimer la séance de test du {0} de la salle {1}",
                  noData: "Il n'y a aucune séance de test"
                },
                viewAttendeesPopup: {
                  title: "Liste de candidats",
                  description1: "Date de la séance de test : {0}",
                  description2: "Salle : {0}",
                  description3: "Test : {0}",
                  table: {
                    columnOne: "Prénom",
                    columnTwo: "Nom de famille",
                    columnThree: "Adresse courriel",
                    columnFour: "Courriel du compte",
                    noData: "Il n'y a aucun candidat pour l'instant."
                  },
                  printButton: "Imprimer",
                  printDocumentTitle: "Liste des candidats pour la séance de test {0}"
                },
                editTestSessionPopup: {
                  title: "Modifier la séance de test",
                  titleSuccess: "Séance de test mise à jour avec succès",
                  description:
                    "Indiquez les détails de la séance de test que vous souhaitez modifier.",
                  roomLabel: "Salle\u00A0:",
                  roomCapacity: "FR Capacity:",
                  openToOgdLabel: "Ouvert à d'autres ministères :",
                  dateLabel: "Date\u00A0:",
                  startTimeLabel: "Heure de début :",
                  endTimeLabel: "Heure de fin :",
                  invalidStartEndTimeCombination: "FR Start time must be before end time",
                  invalidStartDateAndTimeBasedOnBookingDelay:
                    "L'heure de début ne respecte pas l'heure de clôture des réservations",
                  spacesAvailableLabel: "Places disponibles",
                  invalidSpacesAvailableError:
                    "FR This number cannot exceed the maximum occupancy of the selected room",
                  testSkillType: "Type de test :",
                  testSkillSubType: "Test :",
                  test: "Test (tel que défini par l'Unité d’adaptation en matière d’évaluation):",
                  systemMessageDescription:
                    "La séance de test planifiée dans la salle {0} le {1} a été mise à jour avec succès.",
                  systemMessageDescriptionNonStandard:
                    "La séance de test a été mise à jour avec succès. "
                },
                deleteTestSessionPopup: {
                  title: "Supprimer la séance de test",
                  systemMessageDescription:
                    "Cette action supprimera la séance de test planifiée dans la salle {0} le {1}.",
                  systemMessageDescriptionNonStandard:
                    "Cette action supprimera toutes les séances prévues pour ce test.",
                  description: "Souhaitez-vous poursuivre?"
                },
                timeOverlappingErrorPopup: {
                  title: "Conflit d'horaire",
                  systemMessageDescription:
                    "L'heure que vous avez indiquée est en conflit avec d'autres séances de test.",
                  description: "Veuillez fermer cette fenêtre et modifier l'heure désirée"
                }
              }
            }
          },
          accommodationRequests: {
            title: "Demandes de mesures d'adaption",
            topTabs: {
              pendingRequests: {
                title: "Demandes en attente",
                table: {
                  columnOne: "Test",
                  columnTwo: "Candidat ou candidate",
                  columnThree: "Organisation requérante",
                  columnFour: "Date de la demande",
                  columnFive: "Date de clôture",
                  columnSix: "Actions",
                  noData: "Aucune demande en attente",
                  viewButtonAccessibility: "Afficher la demande de {0} {1}",
                  viewButtonTooltip: "Afficher la demande de mesures d'adaptation"
                }
              }
            }
          },
          ola: {
            title: "ÉLO",
            topTabs: {
              olaConfigs: {
                title: "Paramètres ÉLO",
                bookingDelayLabel: "Délai de réservation (heures) :",
                advancedBookingDelayLabel: "Réservation anticipée (jours) :",
                savedConfirmationPopup: {
                  title: "Paramètres ÉLO sauvegardés avec succès !",
                  systemMessageDescription: "Les paramètres ÉLO ont été sauvegardées avec succès !"
                },
                addVacationBlockButton: "Bloc de vacances",
                addEditVacationBlockPopup: {
                  addTitle: "Ajouter un bloc de vacances",
                  editTitle: "Modifier le bloc de vacances",
                  addDescription:
                    "Indiquez les détails du bloc de vacances que vous souhaitez ajouter.",
                  editDescription: "Modifier les informations du bloc de vacances.",
                  dateFromLabel: "Date de début:",
                  dateToLabel: "Date de fin:",
                  invalidDateCombinationError: "Dates invalides",
                  enAvailabilityLabel: "% de disponibilité ANG",
                  frAvailabilityLabel: "% de disponibilité FR",
                  vacationBlockAlreadyExistsError:
                    "Ce bloc vacances existe déjà dans ce centre de test",
                  rightButton: "Bloc de vacances"
                },
                vacationBlocksTable: {
                  dateRange: "Plage de dates",
                  enAvailability: "% de disponibilité ANG",
                  frAvailability: "% de disponibilité FR",
                  actions: "Actions",
                  editButtonTooltip: "Modifier",
                  editButtonTooltipAccessibility: "Modifier le bloc de vacances du {0} au {1}",
                  deleteButtonTooltipAccessibility: "Supprimer le bloc de vacances du {0} au {1}",
                  noData: "Aucun bloc de vacances"
                },
                deleteVacationBlockConfirmationPopup: {
                  title: "Supprimer bloc de vacances",
                  systemMessageDescription:
                    "Êtes-vous sûr de vouloir supprimer le bloc de vacances du {0} {1} {2} ?",
                  separator: "au"
                }
              },
              testAssessors: {
                title: "Évaluateurs",
                addTestAssessorButton: "Évaluateur",
                addEditTestAssessorPopup: {
                  addTitle: "Ajouter un évaluateur",
                  editTitle: "Modifier l'évaluateur",
                  addDescription:
                    "Indiquer les informations de l'évaluateur que vous souhaitez ajouter.",
                  editDescription:
                    "Indiquer les détails de l'évaluateur que vous souhaitez rajouter",
                  testAssessorLabel: "Évaluateur :",
                  languageCertificationLabel: "Langue de certification :",
                  supervisorLabel: "Superviseur :",
                  rightButton: "Évaluateur"
                },
                deleteTestAssessorConfirmationPopup: {
                  title: "Supprimer l'évaluateur",
                  systemMessageDescription: "Êtes-vous sûr de vouloir supprimer l'évaluateur {0} ?"
                },
                testAssessorsTable: {
                  assessor: "Évaluateurs",
                  languageCertification: "Langue de certification",
                  supervisor: "Superviseur",
                  actions: "Actions",
                  editButtonTooltip: "Modifier",
                  editButtonTooltipAccessibility: "Modifier l'évaluateur de test {0}",
                  deleteButtonTooltipAccessibility: "Supprimer l'évaluateur de test {0}",
                  noData: "Aucun évaluateur"
                }
              },
              timeSlots: {
                title: "Plages horaires",
                addTimeSlotButton: "Plage horaire",
                addEditTimeSlotPopup: {
                  addTitle: "Ajoutez une période de test récurrente",
                  editTitle: "Modifier la plage horaire",
                  addDescription: "Réglez les paramètres de la période de test.",
                  editDescription: "Modifier les informations de la plage horaire.",
                  dayOfWeekLabel: "Jour :",
                  startTimeLabel: "Heure de début :",
                  endTimeLabel: "Heure de fin :",
                  startEndTimeCombinationError: "L'heure de début doit être avant l'heure de fin",
                  overlappingTimeError:
                    "La plage horaire que vous essayez d'ajouter/modifier chevauche une autre plage horaire dans ce centre de test.",
                  languageToAssessLabel: "Langue à évaluer :",
                  assessorAvailabilityLabel: "% de disponibilité :",
                  assessorAvailabilityTable: {
                    title: "Évaluateurs disponibles ({0}/{1})",
                    column1: "Évaluateur",
                    column2: "Inclus dans la disponibilité",
                    noData: "Aucun évaluateur ajouté"
                  },
                  slotsToPrioritizeLabel: "Nombre total de périodes à prioriser :",
                  slotsToPrioritizeValueLabel: "(marge de {0}%)",
                  slotsToPrioritizeError:
                    "Cette valeur ne peut pas être supérieure au nombre d'évaluateurs disponibles",
                  prioritizationLabel: "Distribution des priorités :",
                  prioritizationOptionLabel: "{0} [%]\u00A0:",
                  prioritizationValueLabel: "({0} périodes)",
                  prioritizationError: "La somme des pourcentages ne peut pas dépasser 100 %",
                  rightButton: "Plage horaire"
                },
                deleteTimeSlotConfirmationPopup: {
                  title: "Supprimer la plage horaire",
                  systemMessageDescription:
                    "Êtes-vous sûr de vouloir supprimer la plage horaire du {0}, de {1} à {2} ?"
                },
                timeSlotsTable: {
                  dayOfWeek: "Jour",
                  timeOfDay: "Heure",
                  assessedLanguage: "Langue évaluée",
                  nbrOfSelectedAssessors: "Nombre d'évaluateurs disponibles par défaut",
                  actions: "Actions",
                  editButtonTooltip: "Modifier",
                  editButtonTooltipAccessibility: "Modifier le {0}, du {1} au {2}",
                  deleteButtonTooltipAccessibility: "Supprimer le {0}, du {1} au {2}",
                  noData: "Il n'y a aucune plage horaire pour l'instant"
                }
              }
            }
          }
        },
        backToTestCenterManagement: {
          buttonTitle: "Retour à l'administration des centres de tests",
          popup: {
            title: "Retour à la gestion des centres de tests?",
            warningDescription: "Toutes les données non enregistrées seront perdues.",
            description: "Souhaitez-vous poursuivre?"
          }
        }
      },
      ola: {
        lookAhead: {
          navTitle: "FR Look Ahead"
        },
        unavailability: {
          navTitle: "FR Unavailability",
          title: "FR Unavailability",
          description: "FR Manage test centre unavailability."
        },
        reports: {
          navTitle: "FR Reports"
        }
      }
    },

    // Report Generator Component
    reports: {
      reportTypeLabel: "Type de rapport\u00A0:",
      reportTypes: {
        individualScoreSheet: "Résultat de test individuel",
        resultsReport: "Sommaire des résultats de test",
        financialReport: "Rapport financier",
        financialReportFromDateLabel: "Du",
        financialReportToDateLabel: "Au",
        testContentReport: "Rapport sur le contenu du test",
        testTakerReport: "Rapport d'information sur le test du candidat",
        candidateActionsReport: "Rapport sur les actions du candidat",
        taHistoryReport: "Rapport sur l'historique de l'administrateur de test",
        adaptedTestsReport: "Rapport sur les tests adaptés",
        adaptedTestsReportFromDateLabel: "De",
        adaptedTestsReportToDateLabel: "Au",
        assessmentProcessResultsReport: "Rapport sur les résultats du processus d'évaluation"
      },
      testOrderNumberLabel: "Commande de test / Numéro de référence\u00A0:",
      testLabel: "Test\u00A0:",
      testsLabel: "Test(s)\u00A0:",
      testLabelAccessibility: "Les tests sélectionnés sont\u00A0:",
      candidateLabel: "Candidat\u00A0:",
      dateFromLabel: "Du\u00A0:",
      dateToLabel: "Au\u00A0:",
      invalidDate: "Doit être une date valide",
      dateError: "La première date doit être antérieure ou égale à la deuxième",
      parentCodeLabel: "Numéro de test\u00A0:",
      testCodeLabel: "Version de test\u00A0:",
      testVersionLabel: "v\u00A0:",
      generateButton: "Générer le rapport",
      noDataPopup: {
        title: "Aucun résultat",
        description: "Les paramètres fournis ne génèrent aucun résultat."
      },
      testStatus: {
        assigned: "ENREGISTRÉ",
        ready: "PRÊT",
        preTest: "PRÉ-TEST",
        active: "EN TEST",
        transition: "EN TRANSITION",
        locked: "VERROUILLÉ",
        paused: "EN PAUSE",
        quit: "QUITTÉ",
        submitted: "SOUMIS",
        unassigned: "DÉSASSIGNÉ"
      },
      timedOut: "Temps écoulé",
      orderlessRequest: "Non commandé",
      individualScoreSheet: {
        personID: "No. de personne",
        emailAddress: "Adresse courriel",
        name: "Nom",
        pri: "CIDP",
        dateOfBirth: "Date de naissance",
        militaryNbr: "Numéro de service (militaire)",
        orderNumber: "No. de commande",
        assessmentProcessOrReferenceNb: "Processus d'évaluation / Numéro de référence",
        test: "Test",
        testDescriptionFR: "Description du test (FR)",
        testDescriptionEN: "Description du test (ANG)",
        testStartDate: "Date de début du test",
        score: "Résultat",
        level: "Niveau",
        testStatus: "Statut du test"
      },
      resultsReport: {
        lastname: "Nom de famille",
        firstname: "Prénom",
        emailAddress: "Adresse courriel",
        uitInvitationEmail: "Courriel d'invitation au TELNS",
        pri: "CIDP",
        dateOfBirth: "Date de naissance",
        militaryNbr: "Numéro de service (militaire)",
        orderNumber: "No. de commande",
        assessmentProcessOrReferenceNb: "Processus d'évaluation / Numéro de référence",
        test: "Test",
        testDescriptionFR: "Description du test (FR)",
        testDescriptionEN: "Description du test (ANG)",
        testDate: "Date du test",
        score: "Résultat",
        level: "Niveau",
        testStatus: "Statut du test"
      },
      financialReport: {
        candidateLastName: "Nom de famille",
        candidateFirstName: "Prénom",
        candidatePri: "CIDP",
        candidateMilitaryNbr: "Numéro de service (militaire)",
        taWorkEmail: "Adresse courriel professionnelle de l'administrateur de test",
        taOrg: "Organisation de l'administrateur de test",
        requestingDepartment: "Organisation requérante",
        orderNumber: "No. de commande",
        assessmentProcess: "Processus d'évaluation / Numéro de référence",
        orgCode: "Code de l'organisation",
        fisOrgCode: "Code d'organisation SIF",
        fisRefCode: "Code de référence SIF",
        billingContactName: "Courriel de contact pour les candidats",
        testStatus: "Statut du test",
        isInvalid: "Est invalide",
        testDate: "Date du test",
        testVersion: "Version de test",
        testDescriptionFR: "Description du test (FR)",
        testDescriptionEN: "Description du test (ANG)"
      },
      testContentReport: {
        questionSectionNumber: "No. de section de la question",
        questionSectionTitleFR: "Titre de section de la question (FR)",
        questionSectionTitleEN: "Titre de section de la question (ANG)",
        rdItemID: "ID item R&D",
        questionNumber: "No. de la question",
        questionTextFR: "Texte de l'item (FR)",
        questionTextFrHTML: "Texte de l'item HTML (FR)",
        questionTextEN: "Texte de l'item (ANG)",
        questionTextEnHTML: "Texte de l'item HTML (ANG)",
        answerChoiceID: "Choix {0} - ID",
        answerChoiceTextFR: "Choix {0} - texte (FR)",
        answerChoiceTextFrHTML: "Choix {0} - texte HMTL (FR)",
        answerChoiceTextEN: "Choix {0} - texte (ANG)",
        answerChoiceTextEnHTML: "Choix {0} - texte HTML (ANG)",
        scoringValue: "Choix {0} - valeur",
        sampleTest: "Exemple de test"
      },
      testTakerReport: {
        selectedLanguage: "fr",
        descSelectedLanguage: "fdesc",
        abrvSelectedLanguage: "fabrv",
        userCode: "Code d'utilisateur",
        username: "Nom d'utilisateur",
        firstname: "Prénom",
        lastname: "Nom de famille",
        pri: "CIDP",
        militaryNbr: "numéro de matricule",
        dateOfBirth: "Date de naissance",
        currentEmployer: "Employeur actuel",
        org: "Organisation",
        group: "Groupe occupationnel",
        level: "Niveau occupationnel",
        residence: "Résidence",
        education: "Études",
        gender: "Genre",
        identifyAsWoman: "Femme",
        aboriginal: "Autochtone",
        aboriginalInuit: "Indigenous - Inuit",
        aboriginalMetis: "Indigenous - Métis",
        aboriginalNAIndianFirstNation: "Indigenous - Indien d'AN/Première nation",
        visibleMinority: "Minorité visible",
        visibleMinorityBlack: "MV - Noir",
        visibleMinorityChinese: "MV - Chinois",
        visibleMinorityFilipino: "MV - Philippin",
        visibleMinorityJapanese: "MV - Japonais",
        visibleMinorityKorean: "MV - Coréen",
        visibleMinorityNonWhiteLatinx: "MV - Latinx non blanc",
        visibleMinorityNonWhiteMiddleEastern: "MV - Moyen Orient non blanc",
        visibleMinorityMixedOrigin: "MV - Origine mixte",
        visibleMinoritySouthAsianEastIndian: "MV - Asiatique du Sud-est",
        visibleMinoritySoutheastAsian: "MV - Asiatique du Sud/Indien de l'Est",
        visibleMinorityOther: "MV - Autre",
        visibleMinorityOtherDetails: "MV - Autre (précisions)",
        disability: "Handicap",
        disabilityVisual: "Handicap - Visuel",
        disabilityDexterity: "Handicap - Dextérité",
        disabilityHearing: "Handicap - Audition",
        disabilityMobility: "Handicap - Mobilité",
        disabilitySpeech: "Handicap - Élocution",
        disabilityOther: "Handicap - Autre",
        disabilityOtherDetails: "Handicap - Autre (précisions)",
        requestingDepartment: "Ministère demandeur",
        requestingDepartmentLanguage: "fabrv",
        administrationMode: "Mode d'administration",
        assisted: "Assisté",
        automated: "Automatisé",
        reasonForTesting: "Motif du test",
        levelRequired: "Niveau exigé",
        testNumber: "Numéro de test",
        testSectionLanguage: "Langue de la section de test",
        testForm: "Version de test",
        catVersion: "Version OÉC",
        testDate: "Date du test",
        testStartDate: "Date de début du test",
        submitTestDate: "Date de la remise du test",
        questionSectionNumber: "No. de section de la question",
        rdItemID: "ID item R&D",
        pilotItem: "Item pilote",
        itemOrderMaster: "Ordre d'item - maître",
        itemOrderPresentation: "Ordre d'item - présentation",
        itemOrderViewed: "Ordre d'item - visualisation",
        questionTextFr: "Texte de l'item (FR)",
        questionTextEn: "Texte de l'item (ANG)",
        questionTextHtmlFr: "Texte de l'item HTML (FR)",
        questionTextHtmlEn: "Texte de l'item HTML (ANG)",
        choiceXPpcAnswerId: "Choice {0} - ID",
        choiceXPoints: "Choice {0} - valeur",
        choiceXPresentation: "Choix {0} - présentation",
        choiceXTextFr: "Choix {0} - texte (FR)",
        choiceXTextEn: "Choix {0} - texte (ANG)",
        choiceXTextHtmlFr: "Choix {0} - texte HMTL (FR)",
        choiceXTextHtmlEn: "Choix {0} - texte HTML (ANG)",
        languageOfAnswer: "Langue de la réponse",
        timeSpent: "Temps total passé sur l'item (s)",
        responseMasterOrder: "Réponse",
        responseChoiceId: "Réponse - ID choix OÉC",
        scoringValue: "Score de l'item",
        sectionScore: "Score de la section",
        totalScore: "Score total",
        convertedScore: "Niveau",
        testStatus: "Statut du test",
        testSubmissionMode: "Mode de soumission de la section",
        scoreValidity: "Validité du résultat"
      },
      candidateActionsReport: {
        multiAssignedTestsSelectionOptions: {
          label: "Séance de test\u00A0:",
          startDate: "Date du test\u00A0:",
          submitDate: "Date de remise du test\u00A0:",
          modifyDate: "Date de modification\u00A0:"
        },
        origin: "Type d'entrée",
        historyDate: "Date de l'entrée",
        historyType: "Entrée initale ou modifiée",
        status: "Statut du test",
        previousStatus: "Statut précédent",
        startDate: "Date du test",
        submitDate: "Date de la remise du test",
        testAccessCode: "Code d'accès au test",
        totalScore: "Score total",
        testAdministrator: "Administrateur de test",
        testSessionLanguage: "Langue de la séance de test",
        enConvertedScore: "Niveau (ANG)",
        frConvertedScore: "Niveau (FR)",
        uitInviteId: "ID invitation TELNS",
        isInvalid: "Validité du résultat",
        timeType: "Entrée ou sortie de section",
        assignedTestSectionId: "Nom de la section de test",
        candidateAnswerId: "ID séance d'item du candidat",
        markForReview: "Coché pour révision",
        questionId: "ID item OÉC",
        selectedLanguage: "Langue de l'interface",
        answerId: "Réponse - ID choix OÉC",
        candidateAnswerIdRef: "ID séance d'item du candidat (réf.)"
      },
      adaptedTestsReport: {
        candidateLastName: "Nom de famille",
        candidateFirstName: "Prénom",
        requestingOrganization: "Organisation requérante",
        taWorkEmail: "Adresse courriel professionnelle de l'AT",
        taLastName: "Nom de famille de l'AT",
        taFirstName: "Prénom de l’AT",
        taOrganization: "Organisation de l’AT",
        orderNumber: "No. de commande",
        referenceNumber: "Processus d'évaluation / Numéro de référence",
        testStatus: "Statut du test",
        isInvalid: "Est invalide",
        testStartDateAndTime: "Date et heure de début du test",
        testSubmitDateAndTime: "Date et heure de soumission du test",
        testVersion: "Version de test",
        testDescriptionFr: "Description du test (FR)",
        testDescriptionEn: "Description du test (ANG)",
        totalTimeAllotted: "Durée totale allouée",
        totalTimeUsed: "Temps total utilisé",
        breakBankAllotted: "Banque de pauses allouée",
        breakBankUsed: "Temps de pause utilisé"
      },
      itemBankReport: {
        name: "Rapport de contenu de la banque d'items - {0} [{1}]",
        selectedLanguage: "fr",
        systemId: "ID du système",
        version: "Version",
        historicalId: "ID item R&D",
        itemStatus: "Statut de l'item",
        scoringKey: "Clé de correction",
        scoringKeyId: "ID de la clé de réponse",
        stemId: "Énoncé {0} - ID",
        stemTextEn: "Énoncé {0} - Texte (ANG)",
        stemTextHtmlEn: "Énoncé {0} - Texte HTML (ANG)",
        stemTextFr: "Énoncé {0} - Texte (FR)",
        stemTextHtmlFr: "Énoncé {0} - Texte HTML (FR)",
        stemScreenReaderId: "Énoncé {0} - ID lecteur d'écran",
        stemScreenReaderTextEn: "Énoncé {0} - Texte lecteur d'écran (ANG)",
        stemScreenReaderTextHtmlEn: "Énoncé {0} - Texte lecteur d'écran HTML (ANG)",
        stemScreenReaderTextFr: "Énoncé {0} - Texte lecteur d'écran (FR)",
        stemScreenReaderTextHtmlFr: "Énoncé {0} - Texte lecteur d'écran HTML (FR)",
        optionHistoricalId: "Choix {0} - ID",
        optionTextEn: "Choix {0} - Texte (ANG)",
        optionTextHtmlEn: "Choix {0} - Texte HTML (ANG)",
        optionTextFr: "Choix {0} - Texte (FR)",
        optionTextHtmlFr: "Choix {0} - Texte HTML (FR)",
        optionScore: "Choix {0} - Valeur",
        optionScreenReaderHistoricalId: "Choix {0} - ID lecteur d'écran",
        optionScreenReaderTextEn: "Choix {0} - Texte lecteur d'écran (ANG)",
        optionScreenReaderTextHtmlEn: "Choix {0} - Texte lecteur d'écran HTML (ANG)",
        optionScreenReaderTextFr: "Choix {0} - Texte lecteur d'écran (FR)",
        optionScreenReaderTextHtmlFr: "Choix {0} - Texte lecteur d'écran HTML (FR)",
        optionScreenReaderScore: "Choix {0} - Valeur lecteur d'écran",
        itemType: "Type d'item"
      },
      taHistoryReport: {
        assignedTest: "ID du test assigné",
        is_invalid: "FR Is Invalid",
        ta: "Nom de l'administrateur de test (AT)",
        pri: "CIDP",
        priTooltip: "Code d’identification de dossier personnel",
        military_number: "Numéro de service militaire",
        militaryNbrTooltip: "Numéro de service militaire",
        ta_username: "Courriel du compte de l'AT",
        ta_email: "FR TA Account Email",
        goc_email: "Courriel professionnel",
        requesting_organization: "Organisation requérante",
        test: "Test",
        test_submit_date: "Date de soumission du test",
        candidate_name: "Nom du candidat",
        candidate_username: "Courriel du compte du candidat"
      },
      assessmentProcessResultsReport: {
        firstName: "Prénom",
        lastName: "Nom de famille",
        email: "Adresse courriel",
        username: "FR Username",
        invitationEmail: "Courriel d'invitation",
        pri: "CIDP",
        militaryNbr: "Numéro de service (militaire)",
        assessmentProcess: "Processus d'évaluation / Numéro de référence",
        testDefinition: "Test",
        testDefinitionEnName: "Description du test (ANG)",
        testDefinitionFrName: "Description du test (FR)",
        testDate: "Date du test",
        score: "Résultat",
        level: "Niveau",
        testStatus: "Statut du test",
        noDataPopup: {
          title: "Auncun résultat",
          description: "Aucun des candidats n'a encore passé de tests."
        }
      }
    },

    // DatePicker Component
    datePicker: {
      dayField: "Jour",
      dayFieldSelected: "Champ Jour sélectionné",
      monthField: "Mois",
      monthFieldSelected: "Champ Mois sélectionné",
      yearField: "Année",
      yearFieldSelected: "Champ Année sélectionné",
      hourField: "Heure",
      hourFieldSelected: "Champ Heure sélectionné",
      minuteField: "Minute",
      minuteFieldSelected: "Champ Minute sélectionné",
      currentValue: "La valeur actuelle est\u00A0:",
      none: "Sélectionnez",
      datePickedError: "Doit être une date valide\u00A0:",
      futureDatePickedError: "Doit être une date ultérieure",
      futureDatePickedIncludingTodayError: "Ne peut être une date antérieure à aujourd'hui",
      comboStartEndDatesPickedError: "La date de début doit être antérieure à la date de fin",
      comboFromToDatesPickedError: "La date de début doit être antérieure à la date de fin",
      deselectOption: "(inchangé)"
    },

    // PrivacyNoticeStatement Component
    privacyNoticeStatement: {
      title: "Énoncé de confidentialité",
      descriptionTitle: "Énoncé de confidentialité",
      paragraph1:
        "Les renseignements personnels servent à fournir des services d’évaluation aux clients du Centre de psychologie du personnel. Ils sont recueillis à des fins de dotation, conformément aux articles 11, 30 et 36 de la {0}. Dans le cas des organisations qui ne sont pas assujetties à cette loi, les renseignements personnels sont recueillis en vertu de la loi habilitante de l’organisation concernée ainsi que de l’article 35 de la {1}. Les résultats aux tests d’évaluation de langue seconde seront divulgués aux responsables de l’organisation autorisés. Les résultats obtenus à tous les autres examens seront communiqués exclusivement à l’organisation qui en a fait la demande. Les résultats obtenus aux examens pourraient être communiqués à la Direction des enquêtes de la Commission de la fonction publique si une enquête a lieu en vertu des articles 66 ou 69 de la {2}. Vos renseignements pourraient également être utilisés à des fins de recherche statistique et analytique. En vertu du paragraphe 8(2) de la {3}, des renseignements pourraient, dans certains cas, être divulgués sans votre autorisation. La communication de vos renseignements personnels est volontaire, mais si vous choisissez de ne pas fournir ces renseignements, il se peut que vous ne puissiez pas recevoir les services du Centre de psychologie du personnel.",
      paragraph2:
        "Les renseignements sont recueillis et utilisés de la façon décrite dans le fichier de renseignements personnels du Centre de psychologie du personnel (CFP PCU 025), qui se trouve dans {0} de la Commission de la fonction publique.",
      paragraph3:
        "Vous avez le droit d’accéder à vos renseignements personnels et de les corriger, ainsi que de demander qu’ils soient modifiés si vous croyez qu’ils sont erronés ou incomplets. Vous avez également le droit de porter plainte auprès du {0} au sujet du traitement de vos renseignements personnels.",
      publicServiceEmploymentActLinkTitle: "Loi sur l’emploi dans la fonction publique",
      publicServiceEmploymentActLink: "https://laws-lois.justice.gc.ca/fra/lois/p-33.01/",
      privacyActLinkTitle: "Loi sur la protection des renseignements personnels",
      privacyActLink: "https://laws-lois.justice.gc.ca/fra/lois/p-21/",
      infoSourceLinkTitle: "l’Info Source",
      infoSourceLink:
        "https://www.canada.ca/fr/commission-fonction-publique/organisation/propos-nous/bureau-acces-information-protection-renseignements-personnels/info-source.html#122",
      privacyCommissionerOfCanadaLinkTitle:
        "Commissaire à la protection de la vie privée du Canada",
      privacyCommissionerOfCanadaLink: "https://www.priv.gc.ca/fr/"
    },

    // DropdownSelect Component
    dropdownSelect: {
      pleaseSelect: "Sélectionnez",
      noOptions: "Aucune option"
    },

    // SearchBarWithDisplayOptions component
    SearchBarWithDisplayOptions: {
      searchBarTitle: "Recherche\u00A0:",
      resultsFound: "{0} résultat(s)",
      clearSearch: "Effacer la recherche",
      noResultsFound: "Votre recherche n'a généré aucun résultat.",
      displayOptionLabel: "Affichage\u00A0:",
      displayOptionAccessibility: "Nombre de résultats par page",
      displayOptionCurrentValueAccessibility: "La valeur sélectionnée est\u00A0:"
    },

    // Profile Page
    profile: {
      completeProfilePrompt:
        "Pour mettre à jour votre profil, veuillez remplir les champs requis puis cliquez sur Sauvegarder.",
      title: "Bienvenue {0} {1}.",
      sideNavItems: {
        personalInfo: "Renseignements personnels",
        accommodations: "Profil des mesures d’adaptation en matière d’évaluation",
        password: "Mot de passe",
        preferences: "Préférences",
        permissions: "Droits et autorisations",
        profileMerge: "Fusion des comptes"
      },
      personalInfo: {
        title: "Mes renseignements personnels",
        nameSection: {
          firstName: "Prénom\u00A0:",
          lastName: "Nom de famille\u00A0:"
        },
        usernameSection: {
          username: "Nom d'utilisateur\u00A0:",
          usernameError: "Doit être un nom d'utilisateur valide",
          usernameAlreadyAssociatedError: "Ce nom d'utilisateur a déjà été utilisé"
        },
        emailAddressesSection: {
          primary: "Adresse courriel primaire\u00A0:",
          secondary: "Adresse courriel secondaire (facultatif)\u00A0:",
          emailError: "Doit être une adresse courriel valide",
          emailAlreadyAssociatedError: "Un compte est déjà associé à cette adresse courriel"
        },
        dateOfBirth: {
          title: "Date de naissance\u00A0:",
          titleTooltip:
            "Le fait d'ajouter votre date de naissance facilitera le regroupement de tous vos résultats dans votre dossier de résultats de tests."
        },
        phoneNumber: {
          title: "Cell. (facultatif)\u00A0:",
          titleTooltip:
            "L'ajout de votre numéro de téléphone permettra au système de vous envoyer des notifications  lorsque la fonctionnalité sera disponible.",
          phoneNumberError: "Doit être un numéro de téléphone valide"
        },
        pri: {
          title: "CIDP (facultatif)\u00A0:",
          titleTooltip:
            "L'ajout de votre CIDP facilitera la consolidation de tous vos résultats dans votre dossier de résultats de tests.",
          priError: "Doit être un CIDP valide"
        },
        militaryNbr: {
          title: "Numéro de matricule (facultatif)\u00A0:",
          titleTooltip:
            "L'ajout de votre numéro de service facilitera la consolidation de tous vos résultats dans votre dossier de résultats de tests.",
          militaryNbrError: "Doit être un numéro de matricule valide"
        },
        priMilitaryNbrError: "Un CIDP ou un numéro de service militaire est requis",
        psrsAppId: {
          title: "Numéro d'identification du SRFP (facultatif)\u00A0:",
          psrsAppIdError: "Doit être un numéro d'identification du SRFP valide"
        },
        optionalField: "(facultatif)",
        saveConfirmationPopup: {
          title: "Vos renseignements personnels ont été mis à jour",
          description: "Vos renseignements personnels ont été sauvegardés avec succès."
        },
        profileChangeRequest: {
          tooltip: "Modifier les informations",
          existingRequestTooltip: "Voir la demande de modification ",
          requestPopup: {
            title: "Demande de modification",
            successTitle: "Demande soumise avec succès",
            deleteTitle: "Supprimer?",
            description:
              "Toutes les modifications de noms ou de dates de naissance seront revues avant d’être sauvegardées sur votre compte afin de refléter les modifications requises dans la base de données des résultats de tests.",
            successDescription: "Votre demande a été soumise avec succès",
            current: "Actuel",
            new: "Nouveau",
            firstNameLabel: "Prénom\u00A0:",
            firstNameError: "Doit être un prénom valide\u00A0:",
            lastNameLabel: "Nom de famille\u00A0:",
            lastNameError: "Doit être un nom de famille valide\u00A0:",
            dobLabel: "Date de naissance\u00A0:",
            comments: "Commentaires (facultatif)\u00A0:",
            deleteConfirmationSystemMessage:
              "Cette demande de modification sera annulée et votre profil restera inchangé.",
            deleteConfirmationDescription: "Êtes-vous sûr de vouloir continuer?"
          }
        }
      },
      eeInformation: {
        title: "Équité en matière d'emploi",
        description1:
          "La Commission de la fonction publique du Canada (CFP) a besoin de votre aide afin de s’assurer que ses évaluations fonctionnent comme prévu. Les renseignements personnels que vous partagez ci-dessous seront exclusivement accessibles à la CFP et utilisés uniquement à des fins de recherche, d'analyse et d'élaboration de tests. La participation est volontaire\u00A0: vous pouvez choisir l’option «\u00A0Préfère ne pas répondre\u00A0» pour n’importe laquelle des questions ci-dessous. L’information fournie ne sera pas communiquée au personnel des ressources humaines ou aux responsables de l’embauche.",
        description2:
          "La CFP s’engage à protéger le droit des personnes à la vie privée. Vous pouvez consulter notre {0}.",
        privacyNoticeLink: "énoncé de confidentialité",
        identifyAsWoman: "Est-ce que vous vous désignez comme étant une femme ?",
        invalidIdentifyAsWomanErrorMessage: "Veuillez sélectionner un choix de réponse",
        aboriginal: "Êtes-vous Autochtone ?",
        invalidAboriginalErrorMessage: "Veuillez sélectionner un choix de réponse",
        subAboriginal:
          "Veuillez sélectionner tous les énoncés qui s’appliquent à votre situation\u00A0:",
        invalidSubAboriginalErrorMessage: "Veuillez sélectionner au moins un choix de réponse",
        visibleMinority: "Êtes-vous membre d’un groupe de minorité visible ?",
        invalidVisibleMinorityErrorMessage: "Veuillez sélectionner un choix de réponse",
        subVisibleMinority:
          "Veuillez sélectionner tous les énoncés qui s’appliquent à votre situation\u00A0:",
        subVisibleMinorityOtherTextAreaLabel: "Si autre, veuillez préciser\u00A0:",
        invalidSubVisibleMinorityErrorMessage: "Veuillez sélectionner au moins un choix de réponse",
        disability: "Êtes-vous une personne en situation de handicap ?",
        invalidDisabilityErrorMessage: "Veuillez sélectionner un choix de réponse",
        subDisability:
          "Veuillez sélectionner tous les énoncés qui s’appliquent à votre situation\u00A0:",
        subDisabilityOtherTextAreaLabel: "Si autre, veuillez préciser\u00A0:",
        invalidSubDisabilityErrorMessage: "Veuillez sélectionner au moins un choix de réponse"
      },
      additionalInfo: {
        title: "Renseignements supplémentaires (facultatif)",
        current_employer: {
          title: "Employeur actuel\u00A0:"
        },
        organization: {
          title: "Organisation\u00A0:"
        },
        group: {
          title: "Groupe\u00A0:"
        },
        subclassification: {
          title: "Sous-groupe\u00A0:"
        },
        level: {
          title: "Niveau\u00A0:"
        },
        residence: {
          title: "Résidence\u00A0:"
        },
        education: {
          title: "Études\u00A0:"
        },
        gender: {
          title: "Genre\u00A0:"
        }
      },
      accommodations: {
        title: "Profil d’évaluations accessibles",
        description:
          "Veuillez compléter votre profil en répondant aux questions ci-dessous. Vos réponses permettront d’améliorer l’accessibilité des évaluations et d’éliminer ou d’atténuer les obstacles que vous pourriez rencontrer. Répondez aux questions du mieux que vous pouvez. Un spécialiste des mesures d’adaptation en matière d’évaluation pourrait vous contacter pour obtenir plus de renseignements ou des clarifications.",
        builtInAccessibility: {
          title:
            "1. Options d’affichage (tests en ligne seulement) – disponibles sur la plateforme de test Outil d’évaluation des candidats (OEC)",
          hasBuiltInAccessibilityQuestion:
            "Lorsque je passe un test en ligne, j’ai besoin d’options d’affichage différentes de celles disponibles dans les fonctionnalités d’accessibilité intégrées dans l’OEC (telles que le type et la taille de la police, l’espacement du texte, le grossissement et/ou la couleur des caractères).",
          pleaseDescribe:
            "Oui, veuillez décrire les obstacles qui ne sont pas pris en charge par les fonctionnalités d’accessibilité intégrées :",
          builtInAccessibilityError: "Vous devez fournir une description"
        },
        extraTime: {
          title: "2. Temps supplémentaire",
          needsExtraTimeQuestion:
            "J'ai généralement besoin de temps supplémentaire pour terminer un test.",
          pleaseDescribe:
            "Si oui, veuillez indiquer quels obstacles le temps supplémentaire permet de résoudre et de combien de temps il vous faut généralement pour une séance de test d'une heure\u00A0:",
          extraTimeError: "Vous devez fournir une description"
        },
        breaks: {
          title: "3. Pauses",
          needsBreaksQuestion:
            "J'ai besoin de pauses pour démontrer pleinement les capacités évaluées.",
          pleaseDescribe:
            "Si oui, veuillez indiquer quels obstacles les pauses permettent de résoudre et combien de temps de pause il vous faut généralement pour une séance de test d'une heure\u00A0:",
          breaksError: "Vous devez fournir une description"
        },
        adaptiveTech: {
          title: "4. Technologie",
          needsAdaptiveTechQuestion:
            "J’ai besoin d’utiliser des technologies d’adaptation et/ou d’autres mesures de soutien technologique pour terminer un test (p. ex. lecteur d’écran).",
          pleaseDescribe: "Oui (veuillez décrire):",
          adaptiveTechError: "Vous devez fournir une description"
        },
        ergonomic: {
          title: "5. Ergonomie (test en personne seulement)",
          needsErgonomicQuestion:
            "J’ai besoin d’un siège adapté ou des dispositifs ergonomiques pour terminer un test (p. ex. souris, clavier, chaise).",
          pleaseDescribe: "Oui (veuillez décrire):",
          ergonomicError: "Vous devez fournir une description"
        },
        accessAssistance: {
          title: "6. Accès au lieu ou à la salle d’examen (test en personne seulement)",
          needsAccessAssistanceQuestion:
            "J'ai besoin d'aide pour accéder au lieu ou à la salle d'examen.",
          pleaseDescribe: "Oui (veuillez décrire):",
          accessAssistanceError: "Vous devez fournir une description"
        },
        environment: {
          title: "7. Environnement (test en personne seulement)",
          needsEnvironmentQuestion:
            "J'ai des besoins spécifiques liés à l’environnement de test (p. ex. éclairage).",
          pleaseDescribe: "Oui (veuillez décrire):",
          environmentError: "Vous devez fournir une description"
        },
        schedulingAdaptation: {
          title: "8. Mise à l’horaire",
          needsSchedulingAdaptationQuestion:
            "J’ai besoin de passer le test  à un moment précis de la journée (p. ex. avant-midi).",
          pleaseDescribe: "Oui (veuillez décrire):",
          schedulingAdaptationError: "Vous devez fournir une description"
        },
        otherNeedsWritten: {
          title: "9. Tests écrits (en ligne ou en personne)",
          hasOtherNeedsWrittenQuestion:
            "J’ai rencontré d’autres obstacles lorsque je passe des tests écrits.",
          pleaseDescribe: "Oui, veuillez décrire ces autres obstacles.",
          otherNeedsWrittenError: "Vous devez fournir une description"
        },
        otherNeedsOral: {
          title: "10. Tests oraux",
          hasOtherNeedsOralQuestion:
            "J’ai rencontré d’autres obstacles lorsque je passe des tests oraux.",
          pleaseDescribe: "Oui, veuillez décrire ces autres obstacles.",
          otherNeedsOralError: "Vous devez fournir une description"
        },
        repeatedOralQuestions: {
          title: "11. Répétition des questions lors d’un test oral",
          needsRepeatedOralQuestionsQuestion:
            "J’ai généralement besoin que les questions posées oralement soient répétées lors d’un test.",
          pleaseDescribe:
            "Oui, veuillez décrire quels obstacles la répétition des questions permet de résoudre.",
          repeatedOralQuestionsError: "Vous devez fournir une description"
        },
        previouslyAccommodated: {
          title: "12. Mesures d'adaptation précédentes",
          previouslyAccommodatedQuestion:
            "J’ai obtenu des mesures d’adaptation lors des tests dans d’autres contextes comme l’école, les études post-secondaires ou le travail.",
          pleaseDescribe:
            "Oui, veuillez décrire les mesures d’adaptations obtenues au préalable dans des contextes d’évaluations standardisées.",
          previouslyAccommodatedError: "Vous devez fournir une description"
        },
        privacyNotice:
          "J'ai lu l' {0} et j'accepte la manière dont la Commission de la fonction publique recueille, utilise et divulgue les informations personnelles.",
        privacyNoticeLink: "Avis de confidentialité",
        understandAccommodationsMesures:
          "Je comprends que les mesures d'adaptation ci-dessus sont susceptibles de révision par l'équipe des mesures d’adaptation en matière d’évaluation.",
        successPopup: {
          title: "Profil des mesures d'adaptation mis à jour",
          description: "Votre profil des mesures d'adaptation a été sauvegardé avec succès."
        },
        errorPopup: {
          title: "Erreur - Enregistrement du profil des mesures d'adaptation de l'utilisateur",
          description:
            "Une erreur s'est produite lors de l'enregistrement de votre profil des mesures d'adaptation. Veuillez réessayer."
        },
        placeholder: "Cliquez ou appuyez ici pour entrer du texte."
      },
      password: {
        newPassword: {
          title: "Mon mot de passe",
          updatedDate: "La dernière mise à jour de votre mot de passe était le\u00A0: {0}",
          updatedDateNever: "jamais",
          currentPassword: "Mot de passe actuel\u00A0:",
          showCurrentPassword: "Afficher le mot de passe actuel",
          newPassword: "Nouveau mot de passe\u00A0:",
          showNewPassword: "Afficher le nouveau mot de passe",
          confirmPassword: "Confirmer le mot de passe\u00A0:",
          showConfirmPassword: "Afficher la confirmation du mot de passe",
          invalidPasswordError: "Mot de passe invalide",
          popup: {
            title: "Le mot de passe a été mis à jour",
            description: "Votre mot de passe a été mis à jour avec succès."
          }
        },
        passwordRecovery: {
          title: "FR Password Recovery",
          secretQuestion: "FR Secret Question:",
          secretAnswer: "FR Secret Answer:",
          secretQuestionUpdatedConfirmation: "FR Your secret question has been updated successfully"
        }
      },
      preferences: {
        title: "Mes préférences",
        description: "Modifier mes préférences.",
        notifications: {
          title: "Notifications",
          checkBoxOne: "Toujours envoyer un courriel à mon adresse courriel principale",
          checkBoxTwo: "Toujours envoyer un courriel à ma deuxième adresse courriel"
        },
        display: {
          title: "Affichage",
          checkBoxOne: "Permettre à d'autres de voir ma photo de profil",
          checkBoxTwo: "Masquer les icônes d'infobulles"
        },
        accessibility: {
          title: "Accessibilité\u00A0:",
          description:
            "Ajuster la police de caractères, la taille de la police et l'espacement de texte qui sera affiché\u00A0:"
        }
      },
      permissions: {
        title: "Mes droits et autorisations",
        description: "Vous n'avez pas besoin d'autres droits et autorisations pour faire un test.",
        systemPermissionInformation: {
          title: "Information concernant votre rôle d'utilisateur",
          addPermission: "Obtenir des droits et autorisations",
          pending: "En attente",
          superUser: {
            title: "Super Utilisateur",
            permission: "L'utilisateur détient tous les rôles et accès dans le système"
          }
        },
        addPermissionPopup: {
          title: "Demande de droits et d'autorisations",
          description:
            "Veuillez inscrire les renseignements demandés et cocher le ou les rôles d'utilisateur(s) requis.",
          gocEmail: "Adresse courriel du gouvernement du Canada\u00A0:",
          gocEmailTooltip:
            "Vous devez entrer votre adresse courriel du gouvernement du Canada pour vous inscrire.",
          gocEmailError: "Doit être une adresse courriel valide",
          pri: "CIDP\u00A0:",
          militaryNbr: "Numéro de matricule\u00A0:",
          priError: "Doit être un CIDP ou un numéro de matricule valide",
          militaryNbrError: "Doit êtreun numéro de matricule valide",
          priMilitaryNbrError: "Un CIDP ou un numéro de service militaire est requis",
          supervisor: "Nom du superviseur\u00A0:",
          supervisorError: "Le nom entré contient un ou plusieurs caractères invalides",
          supervisorEmail: "Adresse courriel du superviseur\u00A0:",
          supervisorEmailError: "Doit être une adresse courriel valide",
          rationale: "Raison de la demande\u00A0:",
          rationaleError: "La raison de la demande doit être entrée",
          permissions: "Obtenir les droits pour ces rôles d'utilisateurs\u00A0:",
          permissionsError: "Au moins un rôle d'utilisateur doit être sélectionné"
        },
        noPermissions: "Vous n’avez aucune autorisation."
      },
      testPermissions: {
        title: "Autorisations d'accès aux tests",
        description:
          "En tant qu'administrateur de tests, vous avez les autorisations d'accès requises pour faire passer les tests suivants\u00A0:",
        table: {
          title: "Accès aux tests",
          column1: "Version de test",
          column1OrderItem: "Classer par version du test",
          column2: "Numéro de commande du test",
          column2OrderItem: "Classer par numéro de commande de test",
          column3: "Numéro du processus d'évaluation",
          column3OrderItem: "Classer par numéro de processus d'évaluation",
          column4: "Date d'échéance",
          column4OrderItem: "Classer par date d'échéance"
        }
      },
      saveButton: "Sauvegarder"
    },

    // ETTA Page
    systemAdministrator: {
      title: "Bienvenue {0} {1}.",
      containerLabelEtta: "Activités d’affaires",
      containerLabelRD: "Activités R&D",
      containerLabelBoth: "Activités",
      sideNavItems: {
        dashboard: "Tableau de bord",
        activeTests: "Tests actifs",
        testAccessCodes: "Codes d'accès aux tests",
        permissions: "Droits et autorisations",
        itemBanks: "Gestion des banques d'items",
        testCenters: "Administration des centres de tests",
        testAccesses: "Gestion des accès aux tests",
        incidentReports: "Rapports d'incidents",
        scoring: "Résultats",
        reScoring: "Révision des résultats",
        alternateTestRequests: "Demandes de test alternatif",
        reports: "Rapports",
        userLookUp: "Liste des utilisateurs",
        userProfileChangeRequests: "Demandes de modifications du profil de l'utilisateur",
        systemAlert: "Alertes de système",
        olaConfigs: "Paramètres ELO"
      },
      permissions: {
        description: "Attribuer et gérer les droits et autorisations des utilisateurs.",
        tabs: {
          permissionRequests: {
            title: "Demandes de droits et d'autorisations",
            table: {
              columnOne: "Rôle d'utilisateur",
              columnTwo: "Nom et coordonnées de l'utilisateur",
              columnThree: "Date de la demande",
              columnFour: "Action",
              noPermission: "Il n'y a aucune demande de droits et d'autorisations actuellement."
            },
            viewButton: "Afficher",
            viewButtonAccessibility: "Afficher {0} {1}",
            popup: {
              title: "Demande de droits et d'autorisations",
              description:
                "Veuillez accepter ou refuser la demande de droits et d'autorisations formulée par {0}.",
              permissionRequested: "Obtenir les droits pour ce rôle d'utilisateur\u00A0:",
              deniedReason: "Raison du refus\u00A0:",
              denyButton: "Refuser",
              grantButton: "Accepter",
              grantPermissionErrors: {
                notEmptyDeniedReasonError:
                  "Ce champ doit être libre pour pouvoir accepter la demande",
                missingDeniedReasonError: "La raison du refus doit être entrée",
                usernameDoesNotExistError: "Ce nom d'utilisateur n'existe plus"
              }
            }
          },
          activePermissions: {
            title: "Droits et autorisations actifs",
            table: {
              permission: "Rôle d'utilisateur",
              user: "Nom et coordonnées de l'utilisateur",
              action: "Action",
              actionButtonLabel: "Modifier",
              actionButtonAriaLabel: "Modifier {0} {1}"
            },
            viewEditDetailsPopup: {
              title: "Modifier ou supprimer le rôle d'un utilisateur",
              description: "Modifier ou supprimer les droits et autorisations de {0}.",
              deleteButton: "Supprimer",
              saveButton: "Modifier",
              permission: "Rôle d'utilisateur\u00A0:",
              reasonForModification: "Raison de la modification ou de la supression\u00A0:",
              reasonForModificationError:
                "Vous devez fournir une raison pour modifier ou supprimer cette demande"
            },
            deletePermissionConfirmationPopup: {
              title: "Confirmer la supression des droits et autorisations",
              systemMessageDescription:
                "Vous êtes sur le point de révoquer les droits et autorisations {0} de {1}. Souhaitez-vous poursuivre ?"
            },
            updatePermissionDataConfirmationPopup: {
              title: "Confirmation de la modification",
              description: "Les droits et autorisations de {0} ont été mis à jour avec succès."
            }
          }
        }
      },
      itemBanks: {
        title: "Gestion des banques d'items",
        description:
          "Créer des banques d'items et attribuer des accès à ces banques aux développeurs de test.",
        tabs: {
          activeItemBanks: {
            title: "Banques d'items actives",
            newItemBankButton: "Nouvelle banque d'items",
            table: {
              column1: "Banque d'items",
              column2: "Dernière modification",
              column3: "Organisation",
              column4: "Actions",
              noData: "Votre recherche n'a généré aucun résultat"
            },
            newItemBankPopup: {
              title: "Créer une banque d'items",
              description:
                "Remplissez les champs ci-dessous pour créer une nouvelle banque d'items.",
              itemBankDepartment: "Organisation propriétaire\u00A0:",
              itemBankPrefix: "Préfixe de la banque d'items\u00A0:",
              itemBankLanguage: "Langue des items\u00A0:",
              itemBankComments: "Commentaires (facultatif)\u00A0:",
              rightButton: "Créer",
              successMessage: "La nouvelle banque d'items a été créée.",
              itemBankAlreadyExistsError:
                "Ce préfixe est déjà utilisé par une autre banque d'items."
            },
            selectedItemBank: {
              backToRdOperationsButton: "Retour à l'administration des banques d'items",
              backToRdOperationsPopup: {
                title: "Retourner à l'administration des banques d'items ?",
                warningDescription: "Les données qui non pas été sauvegardées seront perdues.",
                description: "Voulez-vous continuer ?"
              },
              tabs: {
                selectedItemBank: {
                  title: "Banque d'items {0}",
                  sideNavigationItems: {
                    ItemBankAccesses: {
                      ItemTitle: "Accès à la banque d'items",
                      title: "Banque d'items\u00A0: {0}",
                      description:
                        "Gérer les accès des développeurs de test à cette banque d'items.",
                      itemBankPrefix: "Préfixe de la banque d'items\u00A0:",
                      itemBankAlreadyExistsError:
                        "Ce préfixe est déjà utilisé par une autre banque d'items.",
                      itemBankNameEn: "Nom anglais de la banque d'items\u00A0:",
                      itemBankNameFr: "Nom français de la banque d'items\u00A0:",
                      itemBankDepartment: "Organisation propriétaire\u00A0:",
                      itemBankLanguage: "Langue des items\u00A0:",
                      addTestDeveloperButton: "Ajouter un développeur de test",
                      addTestDeveloperPopup: {
                        title: "Ajouter un développeur de test à la banque d'items {0}",
                        description:
                          "Sélectionnez un développeur de test à ajouter à la banque d'items.",
                        testDeveloperLabel: "Développeur de test\u00A0:",
                        addButton: "Développeur de test"
                      },
                      deleteTestDeveloperPopup: {
                        title: "Supprimer un développeur de test de la banque d'items {0}.",
                        description:
                          "Cette action supprimera l'accès de {0} à cette banque d'items.",
                        deleteButton: "Supprimer"
                      },
                      table: {
                        column1: "Développeur de test",
                        column2: "Type d'accès",
                        column3: "Actions",
                        deleteButtonTooltip: "Supprimer",
                        deleteButtonAriaLabel: "Supprimer l'accès de {0} à cette banque d'items",
                        noData: "Aucun développeur de test n'est associé à cette banque d'items."
                      }
                    }
                  }
                }
              }
            }
          },
          archivedItemBanks: {
            title: "Banques d'items archivées"
          }
        }
      },
      testCenterAdministration: {
        title: "Administration des centres de tests",
        description:
          "Créer les centres de tests et gérer les accès des coordonnateur de centre de tests",
        tabs: {
          testCenters: {
            title: "Centres de tests",
            newTestCenterButton: "Nouveau centre de tests",
            table: {
              column1: "Nom",
              column2: "Dernière mise à jour",
              column3: "Organisation",
              column4: "Action",
              viewButtonTooltip: "Afficher",
              viewTestCenterAriaLabel: "Afficher le centre de test {0}",
              noData: "Votre recherche n'a généré aucun résultat."
            },
            newTestCenterPopup: {
              title: "Créer un nouveau centre de tests",
              description: "Remplissez les champs suivants pour créer un nouveau centre de tests.",
              testCenterName: "Nom du centre de tests :",
              testCenterDepartment: "Organisation",
              rightButton: "Créer",
              successMessage: "Le nouveau centre de tests a été créé."
            },
            selectedTestCenter: {
              backToRdOperationsButton: "Retour à l'administration des centres de tests",
              backToRdOperationsPopup: {
                title: "Retour à l'administration des centres de test?",
                warningDescription: "Toutes les données non enregistrées seront perdues.",
                description: "Souhaitez-vous poursuivre?"
              },
              tabs: {
                selectedTestCenter: {
                  title: "Centre de tests {0}",
                  sideNavigationItems: {
                    testCenterAccesses: {
                      itemTitle: "Accès au centre de tests",
                      title: "Centre de tests : {0}",
                      description:
                        "Gérez l’accès des coordonnateurs de centre de tests pour ce centre de test.",
                      testCenterName: "Nom du centre de tests :",
                      testCenterDepartment: "Organisation",
                      testCenterOlaAuthorized: "FR OLA Authorized",
                      addTestCenterManagerButton: "Ajouter un Coordonnateur de centre de tests",
                      addTestCenterManagerPopup: {
                        title: "Ajouter un Coordonnateur de centre de tests à {0}",
                        description:
                          "Sélectionnez un coordonnateur de centre de tests à ajouter au centre de tests.",
                        testCenterManagerLabel: "Coordonnateur de centre de tests",
                        addButton: "Sauvegarder"
                      },
                      deleteTestCenterManagerPopup: {
                        title: "Cette action supprimera les accès au centre de tests de {0}.",
                        description: "Cette action supprimera les accès au centre de tests de {0}.",
                        deleteButton: "Supprimer"
                      },
                      table: {
                        column1: "Coordonnateur de centre de tests",
                        column2: "Action",
                        deleteButtonTooltip: "Supprimer",
                        deleteButtonAriaLabel: "Supprimer l'accès de {0}",
                        noData:
                          "Aucun coordonnateur de centre de tests n'est associé à ce centre de tests."
                      }
                    }
                  }
                }
              }
            }
          },
          archivedTestCenters: {
            title: "Centres de tests archivés"
          }
        }
      },
      testAccesses: {
        title: "Gestion des accès aux tests",
        description:
          "Attribuer et gérer les droits d'accès aux tests pour les administrateurs de tests.",
        tabs: {
          assignTestAccesses: {
            version: {
              title: "{0} v{1}"
            },
            testDescription: "{0} v{1}",
            title: "Attribuer des droits d'accès aux tests",
            testOrderNumberLabel: "Numéro de commande du test\u00A0:",
            staffingProcessNumber: "Numéro du processus d'évaluation\u00A0:",
            departmentMinistry: "Code de l'organisation\u00A0:",
            isOrg: "RI - Code d'organisation :",
            isRef: "RI - Code de référence :",
            billingContact: "Nom du responsable de la facturation\u00A0:",
            billingContactInfo: "Courriel de contact pour les candidats\u00A0:",
            users: "Administrateur(s) de tests\u00A0:",
            usersCurrentValueAccessibility: "Les administrateurs de tests sélectionnés sont\u00A0:",
            expiryDate: "Date d'échéance\u00A0:",
            testAccesses: "Accès aux tests\u00A0:",
            testAccess: "Accès au test\u00A0:",
            testAccessesError: "Au moins un test doit être sélectionné",
            testAccessAlreadyExistsError:
              "{0} a déja accès au {1} dont numéro de commande de test est {2}.",
            emptyFieldError:
              "Ce champ est vide ou le nombre de caractères dépasse la limite permise",
            mustBeAnEmailError: "Doit être une adresse courriel valide",
            unableToAccessOrderingServiceError:
              "Impossible d'accéder au service de commande de tests. Veuillez entrer les données financières manuellement.",
            searchButton: "Recherche",
            manuelEntryButton: "Entrée manuelle",
            noTestOrderNumberFound:
              "Numéro de commande introuvable; le numéro entré doit correspondre à un numéro de commande existant",
            refreshButton: "Réinitialiser les champs",
            refreshConfirmationPopup: {
              title: "Voulez-vous effacer tous les champs ?",
              description: "Cette action effacera tous les champs.",
              confirm: "Effacer"
            },
            saveButton: "Sauvegarder",
            saveConfirmationPopup: {
              title: "Confirmer l'attribution des droits d'accès aux tests",
              usersSection:
                "Noms et coordonnées des utilisateurs recevant des droits d'accès\u00A0:",
              testsSection: "Tests pour lesquels les droits sont attribués\u00A0:",
              orderInfoSection: {
                title: "Renseignements sur la commande des tests\u00A0:",
                testOrderNumber: "Numéro de commande du test\u00A0:",
                staffingProcessNumber: "Numéro du processus d'évaluation\u00A0:",
                departmentMinistry: "Code de l'organisation\u00A0:"
              },
              expiriDateSection: "Date d'échéance de l'accès aux tests\u00A0:",
              confirmation: "Veuillez réviser et confirmer les accès aux tests.",
              testAccessesGrantedConfirmationPopup: {
                title: "Confirmation des droits d'accès",
                description: "Les droits d'accès aux tests ont été attribués avec succès."
              }
            }
          },
          activeTestAccesses: {
            title: "Droits d'accès actifs",
            table: {
              testAdministrator: "Administrateur de tests",
              test: "Test",
              orderNumber: "Numéro de commande du test",
              expiryDate: "Date d'échéance",
              action: "Action",
              actionButtonLabel: "Modifier",
              actionButtonAriaLabel:
                "Modifier les données sur l'accès aux tests de l'utilisateur {0} {1}, test {2}, numéro de commande {3}, date d'échéance {4}."
            },
            viewTestPermissionPopup: {
              title: "Modifier ou supprimer les accès au test",
              description: `Modifier ou supprimer les accès au test de {0}.`,
              username: "Adresse courriel du gouvernement du Canada\u00A0:",
              deleteButtonAccessibility: "Supprimer",
              saveButtonAccessibility: "Modifier"
            },
            deleteConfirmationPopup: {
              title: "Confirmer la supression des accès au test",
              systemMessageDescription:
                "Vous êtes sur le point de révoquer les accès au(x) test(s) {0} (Numéro de commande du test\u00A0: {1}) du compte de {2}. Souhaitez-vous poursuivre ?"
            },
            saveConfirmationPopup: {
              title: "Confirmation de la modification",
              description: "Les accès au test de {0} ont été mis à jour avec succès."
            }
          },
          orderlessTestAccesses: {
            title: "Accès aux tests non commandés",
            addNewOrderlessTaButton: "Ajouter un AT",
            addOrderlessTaPopup: {
              title: "Ajouter un (des) administrateur(s) de test",
              description:
                "Sélectionnez le (les) administrateur(s) que vous désirez ajouter à la liste des AT avec des accès aux tests non commandés.",
              testAdministrators: "Administrateur(s) de test\u00A0:",
              invalidTestAdministrators: '"{0}" a déjà accès aux tests non commandés',
              departmentMinistry: "Organisation\u00A0:",
              successfulMessage: "Le (les) administrateur(s) de test ont été ajoutés avec succès."
            },
            confirmDeleteOrderlessTaPopup: {
              title: "Confirmer la suppression des accès aux tests",
              systemMessageDescription:
                "Tous les accès au test non commandé pour {0} seront révoqués.",
              description: "Êtes-vous sûr de vouloir continuer ?"
            },
            table: {
              column_1: "Administrateur de test",
              column_2: "Organisation",
              column_3: "Actions",
              viewTooltip: "Afficher les détails",
              deleteTooltip: "Supprimer tous les accès aux tests"
            },
            selectedOrderlessTestAdministrator: {
              backToOrderlessTestAdministratorSelectionButton:
                "Retour aux accès aux tests non commandés",
              tabTitle: "Accès aux tests non commandés ({0})",
              sideNavItem1Title: "Accès aux tests de l'AT",
              testAccesses: {
                title: "Ajouter/retirer les accès aux tests",
                departmentUpdateTitle: "Modifier l'organisation de l'AT",
                departmentLabel: "Organisation\u00A0:",
                description: "Attribuer ou révoquer les accès au test de {0} {1}.",
                table: {
                  column_1: "Code du test",
                  column_2: "Nom du test",
                  column_3: "Assigné",
                  noResultsFound: "Aucun accès aux tests trouvés"
                }
              },
              saveConfirmationPopup: {
                title: "Modifier l'organisation de l'AT",
                systemMessageDescription: "L'organisation de l'AT a été mis à jour avec succès."
              }
            }
          }
        }
      },
      testAccessCodes: {
        description: "Affichage de tous les codes d'accès des tests actifs",
        table: {
          columnOne: "Code d'accès au test",
          columnTwo: "Test",
          columnThree: "Commande de test / Numéro de référence",
          columnFour: "Administrateur de tests",
          columnFive: "Date de création",
          columnSix: "Action",
          noTestAccessCode: "Il n'y a aucun code d'accès au test",
          deleteButtonAccessibility: "Supprimer {0}"
        }
      },
      activeTests: {
        description: "Affichage de tous les candidats actifs",
        table: {
          columnOne: "Nom du candidat",
          columnTwo: "Administrateur de tests",
          columnThree: "Version de test",
          columnFour: "Commande de test / Numéro de référence\u00A0:",
          columnFive: "Statut",
          columnSix: "Durée du test",
          columnSeven: "Action",
          noActiveCandidates: "Il n'y a aucun candidat actif.",
          actionButton: "Invalider",
          invalidateButtonAccessibility: "Invalider {0}"
        },
        invalidatePopup: {
          title: "Invalider l'accès au test",
          description: "Voulez-vous vraiment invalider l'accès au test de {0} {1} ?",
          actionButton: "Invalider l'accès",
          reasonForInvalidatingTheTest: "Raison de l'invalidation de l'accès au test\u00A0:"
        },
        validatePopup: {
          title: "Rétablir l'accès au test",
          description: "Voulez-vous vraiment rétablir l'accès au test de {0} {1}\u00A0?",
          actionButton: "Rétablir l'accès",
          reasonForInvalidatingTheTest: "Raison du rétablissement de l'accès au test\u00A0:"
        }
      },
      userLookUp: {
        description: "Affichage de la liste de tous les utilisateurs",
        table: {
          columnOne: "Courriel",
          columnTwo: "Prénom",
          columnThree: "Nom de famille",
          columnFour: "Date de naissance",
          columnFive: "Action",
          viewButton: "Afficher",
          noUsers: "Il n'y a aucun candidat actif."
        },
        viewButtonAccessibility: "Afficher {0} {1}"
      },
      userLookUpDetails: {
        sideNavigationItems: {
          userPersonalInfo: "Renseignements personnels",
          myTests: "Tests",
          rightsAndPermissions: "Droits et autorisations",
          testPermissions: "Test Permissions"
        },
        backToUserLookUp: "Retour aux résultats de la recherche",
        containerLabel: "Renseignements sur l'utilisateur ({0} {1})",
        testPermissions: {
          title: "Autorisations d'accès aux tests",
          description:
            "En tant qu'administrateur de tests, {0} {1} a accès aux tests suivants\u00A0:",
          noTestPermissions: "L'utilisateur n'a aucune autorisation d'accès aux tests."
        },
        userRights: {
          title: "Droits et autorisations",
          description: "Affichage des droits de {0} {1}",
          noRights: "L'utilisateur n'a aucune autorisation."
        },
        Tests: {
          title: "Tests de {0} {1}",
          description: "Affichage de tous les tests effectués par {0} {1}",
          table: {
            columnOne: "Test",
            columnTwo: "Date du test",
            columnThree: "Date à partir de laquelle une reprise est possible",
            columnFour: "Résultat",
            columnFive: "Statut",
            columnSix: "Invalidé",
            columnSeven: "Action",
            noTests: "Il n'y a aucun test pour cet utilisateur.",
            viewTestDetailsTooltip: "Afficher les renseignements sur le test",
            invalidateTestTooltip: "Invalider le test",
            validateTestTooltip: "Annuler l'invalidation",
            invalidateButton: "Invalider",
            viewButtonAccessibility: "Afficher {0}",
            invalidateButtonAccessibility: "Invalider {0}",
            validateButtonAccessibility: "Annuler l'invalidation du {0}",
            downloadCandidateActionReport: "Générer le rapport sur les actions du candidat",
            candidateActionReportAria: "Générer les Rapport sur les actions du candidat pour le {0}"
          },
          viewSelectedTestPopup: {
            title: "Afficher les renseignements sur le test",
            description: "Affichage de tous les renseignements sur le test",
            testName: "Nom du test\u00A0:",
            testCode: "Code du test\u00A0:",
            testAccessCode: "Code d'accès au test\u00A0:",
            testOrderNumber: "Numéro de commande du test\u00A0:",
            assessmentProcessOrReferenceNb: "Processus d'évaluation / Numéro de référence\u00A0:",
            validityEndDateOrProcessClosingDate:
              "Date de fin de la validité / Date de clôture du processus\u00A0:",
            testAdministratorEmail: "Courriel\u00A0:",
            testAdministratorGocEmail: "Adresse courriel du gouvernement du Canada\u00A0:",
            testAdministrator: "Administrateur de tests\u00A0:",
            totalScore: "Score total\u00A0:",
            version: "Version OÉC\u00A0:",
            invalidTestReason: "Raison de l'invalidation du test\u00A0:"
          }
        },
        userPersonalInfo: {
          title: "Renseignements personnels",
          firstName: "Prénom\u00A0:",
          lastName: "Nom de famille\u00A0:",
          username: "Nom d'utilisateur\u00A0:",
          primaryEmailAddress: "Adresse courriel primaire\u00A0:",
          secondaryEmailAddress: "Adresse courriel secondaire\u00A0:",
          dateOfBirth: "Date de naissance\u00A0:",
          psrsApplicantId: "Numéro d'identification du SRFP\u00A0:",
          pri: "CIDP\u00A0:",
          militaryNbr: "Numéro de service (militaire)\u00A0:",
          lastLogin: "Date de la dernière ouverture de session\u00A0:",
          lastPasswordChange: "Date de la dernière modification du mot de passe\u00A0:",
          accountCreationDate: "Date de création du compte\u00A0:",
          lastUpdatedDate: "Dernière mise à jour\u00A0:",
          additionalInfo: "Renseignements supplémentaires",
          currentEmployer: "Employeur actuel\u00A0:",
          organization: "Organisation\u00A0:",
          group: "Groupe\u00A0:",
          subGroup: "Sous-groupe\u00A0:",
          level: "Niveau\u00A0:",
          residence: "Résidence\u00A0:",
          education: "Études\u00A0:",
          gender: "Genre\u00A0:",
          noPersonalInformation: "Il n'y a aucun renseignement personnel relatif à l'utilisateur.",
          noAdditionalInfomration: "Il n'y a aucun renseignement supplémentaire sur l'utilisateur."
        }
      },
      userProfileChangeRequests: {
        title: "Demandes de modifications du profil de l'utilisateur",
        table: {
          columnOne: "Courriel du compte",
          columnTwo: "Prénom",
          columnThree: "Nom de famille",
          columnFour: "DDN",
          columnFive: "Action",
          reviewButton: "Examiner",
          reviewButtonAccessibility: "FR Review profile change request of {0} {1}",
          noData: "Il n'y a pas de demande de modifications de profil d'utilisateur à afficher"
        },
        popup: {
          title: "Examiner la demande de modification du profil",
          description:
            "Les modifications apportées aux noms et aux dates de naissance pourraient prendre jusqu'à deux jours ouvrables avant d'être appliquées.",
          usernameLabel: "FR Username:",
          priLabel: "FR PRI:",
          current: "Actuel",
          new: "Nouveau",
          firstNameLabel: "Prénom\u00A0:",
          lastNameLabel: "Nom de famille\u00A0:",
          dateOfBirthLabel: "Date de naissance\u00A0:",
          commentsLabel: "Commentaires\u00A0:",
          reasonForDenyLabel: "Raison du refus\u00A0:",
          reasonForDenyError: "La raison du refus de la demande doit être entrée",
          leftButton: "Refuser",
          rightButton: "Accepter"
        },
        successPopup: {
          title: "Action terminée",
          systemMessageDescription: "Votre action a été effectuée avec succès."
        }
      },
      alternateTestRequests: {
        title: "Demandes de test alternatif",
        description: "Gérer les tests alternatifs",
        pendingRequests: {
          title: "Demandes en attente",
          table: {
            columnOne: "ID de la demande",
            columnTwo: "ID du candidat ou de la candidate",
            columnThree: "Candidat ou Candidate",
            columnFour: "Date de la demande",
            columnFive: "Date de clôture du processus",
            columnSix: "Centre de test",
            columnSeven: "Test",
            columnHeight: "Actions",
            noData: "Aucune demande de test alternatif en attente",
            viewTooltip: "Afficher",
            viewTooltipAccessibility: "Afficher la demande {0} {1}"
          },
          viewSelectedPendingAlternateTestRequestPopup: {
            title: "Demande de test alternatif - {0} {1}",
            titleSuccess: "Demande de test alternatif envoyée avec succès",
            description1: "Utilisateur: {0} {1} ({2})",
            description1continued: "Test: {0}",
            description2:
              "Les informations ci-dessous ont été fournies pour vous permettre d'analyser et de recommander quel test alternatif devrait être utiliser pour ce candidat ou cette candidate.",
            successSystemMessageDescription: "Cette demande a été envoyée avec succès !",
            locationLabel: "Lieu :",
            testAdministeredDetailsLabel: "Motif de la demande\u00A0:",
            recommendationsLabel: "Recommandations :",
            testCannotBePerformedInCatLabel: "Le test ne peut pas être effectué avec l'OÉC :",
            testToAdministerLabel: "Test à administrer :",
            specifiedTestDescriptionLabel: "Description du test :",
            specifiedTestDescriptionError: "Vous devez fournir une description du test",
            rationaleLabel: "Raisonnement :",
            cancelRequestButton: "Annuler la demande"
          },
          cancelRequestConfirmationPopup: {
            title: "Êtes-vous sûr de vouloir annuler votre demande ?",
            systemMessageDescription:
              "Votre demande de test alternatif sera annulée. Cette action est irréversible!",
            description: "Êtes-vous sûr de vouloir continuer?"
          }
        },
        completedRequests: {
          title: "Demandes complétées",
          table: {
            columnOne: "ID de la demande",
            columnTwo: "ID du candidat ou de la candidate",
            columnThree: "Candidat ou Candidate",
            columnFour: "Date de la demande",
            columnFive: "Date de clôture du processus",
            columnSix: "Centre de test",
            columnSeven: "Test",
            columnHeight: "Actions",
            noData: "Aucune demande de tests alternatifs complétée",
            viewTooltip: "Afficher",
            viewTooltipAccessibility: "Afficher la demande {0} {1}"
          },
          viewSelectedCompletedAlternateTestRequestPopup: {
            title: "Demandes de test alternatif - {0} {1}",
            description1: "Utilisateur: {0} {1} ({2})",
            description1continued: "Test: {0}",
            description2:
              "Les informations ci-dessous ont été fournies pour vous permettre d'analyser et de recommander quel test alternatif devrait être utiliser pour ce candidat ou cette candidate.",
            successSystemMessageDescription: "FR This request has been sent successfully!",
            locationLabel: "Lieu :",
            commentsLabel: "Commentaires :",
            recommendationsLabel: "Recommandations :",
            testCannotBePerformedInCatLabel: "Le test ne peut pas être effectué avec l'OÉC :",
            testToAdministerLabel: "Test à administrer :",
            specifiedTestDescriptionLabel: "Description du test :",
            noTestToAdministerDefinedLabel:
              "Aucun test à administrer n'a été défini pour cette demande.",
            rationaleLabel: "Raisonnement :"
          }
        }
      },
      reports: {
        title: "Rapports",
        description:
          "Générer un rapport de résultats pour une commande de test que vous avez administré."
      },
      systemAlerts: {
        title: "Alertes de système",
        description: "Gérer les alertes de système",
        tabs: {
          activeSystemAlerts: {
            title: "Alertes de système actives",
            addNewSystemAlertButton: "Ajouter une alerte de système",
            addSystemAlertPopup: {
              title: "Ajouter une alerte de système",
              fields: {
                title: "Titre\u00A0:",
                criticality: "Criticité\u00A0:",
                startDate: "Date de début\u00A0:",
                endDate: "Date de fin\u00A0:",
                messageText: "Texte du message\u00A0:",
                messageTextTabs: {
                  english: "Anglais",
                  french: "Français"
                }
              },
              buttons: {
                rightButton: "Ajouter"
              },
              successfulMessage: "L'alerte de système a été ajoutée avec succès."
            },
            archiveSystemAlertPopup: {
              title: "Archiver les alertes de système",
              systemMessageTitle: "Archiver cette alerte de système ?",
              systemMessageMessage:
                "L'archivage de cette alerte de système la supprimera de la page de connexion et la déplacera vers les alertes de système archivées.",
              archiveSuccessfullyMessage: "L'alerte de système a été archivée avec succès.",
              archiveRightButton: "Archiver"
            },
            table: {
              column_1: "Alerte",
              column_2: "Criticité",
              column_3: "Date d'entrée en vigueur",
              column_4: "Date de fin",
              column_5: "Actions",
              viewTooltip: "Afficher",
              modifyTooltip: "Modifier",
              archiveToolTip: "Archiver"
            },
            viewPopupTitle: "Afficher l'alerte de système",
            modifyPopupTitle: "Modifier l'alerte de système",
            modifyRightButton: "Modifier",
            modifySuccessfullyMessage: "L'alerte de système a été modifiée avec succès."
          },
          archivedSystemAlerts: {
            title: "Alertes de système archivées",
            table: {
              column_1: "Alerte",
              column_2: "Criticité",
              column_3: "Date d'entrée en vigueur",
              column_4: "Date de fin",
              column_5: "Actions",
              viewTooltip: "Afficher",
              cloneTooltip: "Cloner"
            },
            viewPopupTitle: "Afficher l'alerte de système",
            clonePopupTitle: "Cloner l'alerte de système",
            cloneSuccessfullyMessage: "L'alerte de système a été clonée avec succès."
          }
        }
      },
      olaConfigs: {
        title: "Paramètres ELO",
        description: "Configurez les paramètres de l'ELO.",
        tabs: {
          prioritization: {
            title: "Distributions des priorités",
            prioritizationLabelFormatter: "{0} (%)\u00A0:",
            prioritizationError: "La somme des pourcentages ne peut pas dépasser 100 %"
          },
          serviceStandards: {
            title: "Normes de service",
            table: {
              column1: "Motif du test",
              column2: "Norme de service",
              column3: "Période d'attente",
              column4: "Priorité",
              column5: "Actif",
              column6: "Actions",
              editButtonTooltip: "Modifier",
              editButtonTooltipAccessibility: "Modifier {0}"
            },
            serviceStandardPopup: {
              editTitle: "Modifier la norme de service pour l'ELO",
              description: "Définissez les paramètres de la norme de service pour l'ELO.",
              nameTranslationTitle: "Traduction",
              nameLabel: "Motif du test :",
              translationsButtonAriaLabel: "Modifier la traduction",
              translationsButtonTooltip: "Modifier la traduction",
              en_name: {
                title: "Motif du test en anglais :"
              },
              fr_name: {
                title: "Motif du test en français :"
              },
              nameError: "Cliquez sur le bouton Traduction et complétez tous les champs",
              priorityLabel: "Niveau de priorité : ",
              minimumProcessLengthLabel: "Norme de service (jours) :",
              waitingPeriodLabel: "Période d'attente (jours) : ",
              waitingPeriodError:
                "Cette valeur ne peut pas être supérieure à celle de la norme de service",
              activeStateLabel: "Actif : "
            }
          },
          olaConfig: {
            title: "Paramètres - Dernière minute",
            cancellationWindow: "Période d'annulation tardive (heures) :",
            openBookingPeriod:
              "Ouverture des réservations pour tous les motifs de test - nouvelles réservations seulement (jours) :"
          }
        }
      }
    },

    // Scorer Pages
    scorer: {
      situationTitle: "Situation",
      assignedTest: {
        displayOptionLabel: "Affichage\u00A0:",
        displayOptionAccessibility: "Nombre de résultats par page",
        displayOptionCurrentValueAccessibility: "La valeur sélectionnée est\u00A0:",
        table: {
          assignedTestId: "FR Assigned Test ID",
          completionDate: "FR Completion Date",
          testName: "FR Test Name",
          testLanguage: "FR Test Language",
          action: "Action",
          actionButtonLabel: "FR Score Test",
          en: "Anglais",
          fr: "Français",
          actionButtonAriaLabel:
            "FR Score test of: assigned test id: {0}, completion date: {1}, test name: {2}, test language: {3}."
        },
        noResultsFound: "FR There are no tests waiting to be scored."
      },
      sidebar: {
        title: "FR Scoring Panel",
        competency: "FR Competency",
        rational: "FR Rating rationale",
        placeholder: "FR Input rationale here..."
      },
      submitButton: "FR Submit Final Scores",
      unsupportedScoringType:
        "FR The scoring type: {0} is not supported. The test definition must be incorrect.",
      selectQuestion: "FR Select a question to continue",
      competency: {
        bar: {
          title: "FR Behavioural Achored Ratings",
          description:
            "FR Below is a complete view of the BAR for this question and competency. Once selected, you can use the arrows to navigate, expand, and collapse information.",
          barButton: "FR View BAR"
        }
      },
      ola: {
        tabName: "ÉLO",
        pageTitle: "Évaluation linguistique à l'oral",
        myAssignedTests: {
          tabName: "Mes tests ",
          table: {
            column1: "Heure de la séance de test",
            column2: "Langue du test",
            column3: "Candidat",
            column4: "Actions",
            viewDetailsButtonTooltip: "Afficher",
            viewDetailsButtonTooltipAccessibility:
              "Afficher les détails de la séance de test pour le candidat ou la candidate {0}",
            joinVideoCallTooltip: "FR Join meeting",
            joinVideoCallTooltipAccessibility: "FR Join the test session for candidate {0}",
            noData: "Il n'y a aucune séance de test pour l'instant"
          },
          assignedCandidatePopup: {
            title: "Information",
            candidateLabel: "Candidat :",
            olaPhoneNumberLabel: "Numéro de téléphone du candidat :",
            candidatePriLabel: "CIDP :",
            referenceNumberLabel: "Numéro de référence :",
            requestingDepartmentLabel: "Organisme requérante:",
            hrContactLabel: "Contact RH :",
            testToAdministerLabel: "Test à administrer :",
            olaFieldInput: "FR OLA {0}",
            languageOfTestLabel: "Langue du test :",
            reasonForTestingLabel: "Motif du test :",
            levelRequiredLabel: "Niveau requis :"
          },
          unassignCandidateConfirmationPopup: {
            title: "Désassigner candidat",
            systemMessageDescription:
              "Des dispositions devront être prises pour qu’un autre évaluateur évalue ce candidat.",
            rightButton: "Désassigner"
          }
        },
        testsToAssess: {
          tabName: "Tests à évaluer ",
          showUnassignedOnly: "Afficher uniquement les séances de test non assignées",
          table: {
            testSessionTime: "Heure de la séance de test",
            sessionLanguage: "Langue de la séance",
            reasonForTesting: "Motif du test",
            candidate: "Candidat",
            testAssessor: "Évaluateur",
            actions: "Actions",
            viewDetailsButtonTooltip: "Afficher",
            viewDetailsButtonTooltipAccessibility:
              "Afficher les détails de la séance de test pour le candidat ou la candidate {0}",
            assignButtonTooltip: "Assigner",
            assignButtonTooltipAccessibility: "Assigner la séance de test {0}",
            unassignButtonTooltip: "Désassigner",
            unassignButtonTooltipAccessibility: "Désassigner la séance de test du {0}",
            assignTestAssessorTooltip: "Assigner un évaluateur",
            assignTestAssessorTooltipAccessibility: "Assigner {0} à un évaluateur de test",
            cancelTestSessionTooltip: "Annuler la séance de testn",
            cancelTestSessionTooltipAccessibility: "Annuler la séance de test de {0}",
            noData: "Il n'y a aucune séance de test pour l'instant"
          },
          assignOrAssignedCandidatePopup: {
            assignTitle: "Information",
            assignedTitle: "Information",
            description:
              "Réviser les informations du candidat et accepter ou passer au candidat suivant.",
            candidateLabel: "Candidat :",
            candidatePriLabel: "CIDP :",
            referenceNumberLabel: "Numéro de référence :",
            requestingDepartmentLabel: "Organisation requérante :",
            hrContactLabel: "Contact RH :",
            testToAdministerLabel: "Test à administrer :",
            olaFieldInput: "FR OLA {0}",
            languageOfTestLabel: "Langue du test :",
            reasonForTestingLabel: "Motif du test :",
            levelRequiredLabel: "Niveau requis :",
            leftButton: "Suivant",
            rightButton: "Accepter"
          },
          unassignCandidateConfirmationPopup: {
            title: "Désassigner candidat",
            systemMessageDescription:
              "Des dispositions devront être prises pour qu’un autre évaluateur évalue ce candidat.",
            rightButton: "Désassigner"
          },
          assignTestAssessorPopup: {
            title: "Assigner l'évaluateur ",
            systemMessageDescription:
              "Veuillez vous assurer que l'évaluateur sélectionné est informé de la nouvelle session de test qui lui a été attribuée.",
            testAssessorLabel: "Évaluateur\u00A0:",
            rightButton: "Assigner"
          },
          cancelTestSessionConfirmationPopup: {
            title: "Annuler la séance de test",
            systemMessageDescription: "Vous etes sur le point d'annuler la session de test de {0}.",
            description: "Êtes-vous sûr de vouloir continuer?",
            rightButton: "Annuler la session",
            rightButtonLoading: "FR Cancelling..."
          },
          alreadyPairedPopup: {
            title: "Déjà assigné",
            systemMessageDescription: "Ce candidat est déjà assigné à un autre évaluateur.",
            description: "Fermez cette fenêtre pour afficher l’information du candidat suivant."
          },
          conflictingSchedulePopup: {
            title: "Conflit d'horaire",
            systemMessageDescription:
              "Il y a un conflit d'horaire avec les séances de test que vous essayez d'assigner",
            description: "Fermez cette fenêtre pour rafraîchir la page."
          }
        },
        notLinkedToTestCenterMsg: "FR Sorry, you are not linked to a test center",
        assessorsUnavailability: {
          tabName: "Mes non-disponibilités",
          addUnavailabilityButton: "Ajouter une non-disponibilité",
          table: {
            column1: "Centre de test",
            column2: "Évaluateur",
            column3: "Date de début",
            column4: "Date de fin",
            column5: "Actions",
            modifyButtonTooltip: "Modifier",
            modifyButtonTooltipAccessibility:
              "Modifier la période de non-disponibilité de {0} pour le centre de test {1}, dont la Date de début est {2} et la Date de fin est {3}",
            deleteButtonTooltip: "Supprimer",
            deleteButtonTooltipAccessibility:
              "Supprimer la période de non-disponibilité de {0} pour le centre de test {1}, dont la Date de début est {2} et la Date de fin est {3}",
            noData: "Aucune période de non-disponibilité pour le moment"
          },
          addUnavailabilityPopup: {
            title: "Ajouter une non-disponibilité",
            description: "Indiquer les détails de la période de non-disponibilité à ajouter.",
            assessor: "Évaluateur :",
            testCenters: "Centre(s) de test :",
            startDate: "Date de début :",
            endDate: "Date de fin :",
            reasonForUnavailability: "Raison de la non-disponibilité :",
            rightButton: "Ajouter"
          },
          modifyUnavailabilityPopup: {
            title: "Modifier la période de non-disponibilité",
            description: "Indiquer les détails de la période de non-disponibilité à ajouter.",
            rightButton: "Modifier"
          },
          deleteUnavailabilityConfirmationPopup: {
            title: "Supprimer la période de non-disponibilité",
            systemMessageDescription: "Voulez-vous procéder? Cette action ne peut être annulée.",
            rightButton: "Supprimer"
          },
          errorPopup: {
            addErrorTitle: "Erreur - Ajout de la non-disponibilité de l'évaluateur",
            addErrorDescription: "Une erreur s'est produite. Veuillez réessayer.",
            modifyErrorTitle: "Erreur - Modification de la non-disponibilité de l'évaluateur",
            modifyErrorDescription: "Une erreur s'est produite. Veuillez réessayer.",
            deleteErrorTitle: "Erreur - Suppression de la non-disponibilité de l'évaluateur",
            deleteErrorDescription: "Une erreur s'est produite. Veuillez réessayer."
          },
          errors: {
            endDateError:
              "La date de fin doit correspondre ou être postérieure à la date d'aujourd'hui."
          }
        },
        myAvailability: {
          tabName: "Mes disponibilités",
          table: {
            column1: "Période de test",
            column2: "Langue à évaluer",
            column3: "Centre de test",
            column4: "Disponible",
            noData: "Aucune période de disponibilité pour le moment"
          }
        }
      }
    },

    // Status Page
    statusPage: {
      title: "Statut de OÉC",
      logo: "Logo Thunder CAT",
      welcomeMsg:
        "Page de statut interne afin de déterminer rapidement l'état / la santé de l'outil d'évaluation des candidats.",
      versionMsg: "Version de l'Outil d'évaluation des candidats\u00A0: ",
      gitHubRepoBtn: "Répertoire GitHub",
      serviceStatusTable: {
        title: "Statut des services",
        frontendDesc: "La Face avant de l'application est construite et utilisée avec succès",
        backendDesc: "La Face arrière de l'application réussit les demandes API avec succès",
        databaseDesc: "La Base de données réussit les demandes API avec succès"
      },
      systemStatusTable: {
        title: "Statut du système",
        javaScript: "JavaScript",
        browsers: "Chrome, Firefox, Edge",
        screenResolution: "Résolution d'écran minimum de 1024 x 768"
      },
      additionalRequirements:
        "De plus, les exigences suivantes doivent être respectées pour l’utilisation de cette application dans un centre de test.",
      secureSockets: "Chiffrement Secure Socket Layer (SSL) activé",
      fullScreen: "Mode plein écran activé",
      copyPaste: "Fonction copier-coller activée"
    },

    // AAE
    aae: {
      title: "Mesures d’adaptation en matière d’évaluation",
      sideNav: {
        accommodationRequests: {
          title: "Demandes de mesures d'adaption",
          description: "Gérer toutes les demandes de mesures d'adaptation en matière d'évaluation",
          topTabs: {
            openRequests: {
              title: "Demandes en cours",
              table: {
                columnOne: "ID de la demande",
                columnTwo: "ID du candidat ou de la candidate\u00A0:",
                columnThree: "Candidat ou candidate",
                columnFour: "Type de test",
                columnFive: "Date de la demande",
                columnSix: "Date de clôture du processus",
                columnSeven: "Statut",
                columnEight: "Assignée à",
                columnNine: "Type de test",
                columnTen: "Actions",
                noData: "Aucune demande en cours",
                viewButtonAccessibility: "Afficher la demande de {0} {1}",
                viewButtonTooltip: "Afficher la demande de mesures d'adaptation"
              }
            },
            completedRequets: {
              title: "Demandes complétées",
              table: {
                columnOne: "ID de la demande",
                columnTwo: "ID du candidat ou de la candidate\u00A0:",
                columnThree: "Candidat ou candidate",
                columnFour: "Type de test",
                columnFive: "Date de la demande",
                columnSix: "Statut",
                columnSeven: "Assignée à",
                columnEight: "Type de test",
                columnNine: "Actions",
                noData: "Aucune demande complétée",
                viewButtonAccessibility: "Afficher la demande de {0} {1}",
                viewButtonTooltip: "Afficher la demande de mesures d'adaptation"
              }
            }
          },
          selectedRequest: {
            backToAccommodationRequestSelectionButton:
              "Retour aux Demandes pour mesures d'adaptation",
            backToAccommodationRequestSelectionPopup: {
              title: "Retour aux Demandes pour mesures d'adaptation",
              systemMessageDescription: "Toutes les données non sauvegardées seront perdues!",
              description: "Êtes-vous sûr de vouloir poursuivre?"
            },
            topTabs: {
              main: {
                title: "{0} {1}",
                sideNavigation: {
                  candidateData: {
                    title: "Informations du candidat ou de la candidate",
                    userIdLabel: "ID du candidat ou de la candidate\u00A0:",
                    userFirstNameLabel: "Prénom :",
                    userLastNameLabel: "Nom de famille :",
                    userEmailLabel: "Adresse courriel\u00A0:",
                    userSecondaryEmailLabel: "Adresse courriel secondaire\u00A0:",
                    userDobLabel: "Date de naissance\u00A0:",
                    userPhoneNumberLabel:
                      "Numéro de téléphone cellulaire du candidat ou de la candidate: ",
                    olaPhoneNumberLabel: "Numéro de téléphone du candidat :",
                    userPsrsApplicantIdLabel: "Numéro d'identification du SRFP\u00A0:",
                    userPriLabel: "CIDP\u00A0:",
                    userMilitaryNbrLabel: "Numéro de matricule\u00A0:",
                    referenceNumberLabel: "Processus d'évaluation / Numéro de référence\u00A0:",
                    departmentLabel: "Organisation\u00A0:",
                    primaryContactLabel: "Personne-ressource principale\u00A0:",
                    candidateCommentsLabel: "Commentaires\u00A0:"
                  },
                  userAccommodationsProfile: {
                    title: "Profil des mesures d'adaptation de l'utilisateur"
                  },
                  testToAdminister: {
                    sideNavTitle: "Test à administrer",
                    title: "FR Test to Administer {0}",
                    testCenterLabel: "Centre de test\u00A0:",
                    testSkillTypeLabel: "Type de test\u00A0:",
                    testSkillSubTypeLabel: "Test\u00A0:",
                    testCannotBePerformedInCatSwitchDescription:
                      "Le test ne peut pas être effectué avec l'OÉC\u00A0:",
                    specifiedTestDescriptionLabel: "Description du test\u00A0:",
                    specifiedTestDescriptionError: "Vous devez fournir une description du test",
                    table: {
                      columnOne: "Code du test",
                      columnTwo: "Version OÉC",
                      columnThree: "Nom du test",
                      noData: "Aucun test n'est disponible pour le moment"
                    }
                  },
                  breakBankAndTimeAdjustment: {
                    sideNavTitle: "Réglage du temps / pause",
                    title: "Réglage du temps / pause {0}",
                    testToAdministerLabel: "Test à administrer\u00A0:",
                    defaultTestTimeLabel: "Durée du test par défaut\u00A0:",
                    noTestSelectedSystemMessageDescription:
                      "Veuillez sélectionner un test afin de définir la banque de pauses allouées et/ou le temps de test souhaité.",
                    allottedBreakBankLabel: "Banque de pauses allouée\u00A0:",
                    testSectionTimers: "Minuteries des sections du test\u00A0:",
                    sectionPrefixLabel: "Section:",
                    outsideOfCATSection: "Personnaliser",
                    testSectionTimersError:
                      "Vous ne pouvez pas régler la minuterie d'une section d'un test en dessous de son temps par défaut."
                  },
                  otherMeasures: {
                    sideNavTitle: "Autres mesures",
                    title: "Autres mesures",
                    inputLabel: "Mesures supplémentaires à appliquer\u00A0:"
                  },
                  fileAdministration: {
                    sideNavTitle: "FR File Administration",
                    title: "Administration de la requête {0}",
                    requestId: "Numéro de la demande :",
                    assignedToLabel: "Assignée à\u00A0:",
                    fileStatusLabel: "Statut de la demande\u00A0:",
                    fileSubmitDateLabel: "Date de soumission de la demande\u00A0:",
                    fileComplexityLabel: "Complexité de la demande :",
                    lastModifiedDateLabel: "Date de la dernière modification\u00A0:",
                    lastModifiedByLabel: "Modifiée en dernier lieu par\u00A0:",
                    processEndDateLabel: "Date de fin du processus\u00A0:"
                  },
                  reports: {
                    sideNavTitle: "Rapports",
                    title: "Rapports",
                    previewCurrentFileDataLabel: "Aperçu des données du dossier actuel :",
                    viewDraftReportButton: "Afficher l'ébauche du rapport",
                    previousReportsLabel: "Rapports précédents :",
                    previousReportsTable: {
                      columnOne: "Date du rapport",
                      columnTwo: "Nom du test",
                      columnThree: "Type de test",
                      columnFour: "Action",
                      viewButton: "Afficher",
                      viewButtonAccessibility: "Afficher {0} - {1} le rapport",
                      noData: "Aucun rapport précédent jusqu'à présent"
                    }
                  }
                }
              },
              other: {
                comments: {
                  title: "Notes",
                  addCommentButton: "Note",
                  addCommentPopup: {
                    title: "Ajouter un commentaire",
                    commentLabel: "Commentaire\u00A0:",
                    addButton: "Sauvegarder"
                  },
                  deleteCommentPopup: {
                    title: "Supprimer la note",
                    systemMessageDescription1: "Êtes-vous sûr de vouloir supprimer cette note?",
                    systemMessageDescription2: "- {0}"
                  }
                },
                rationale: {
                  title: "Raisonnement",
                  rationaleLabel: "Veuillez fournir un raisonnement\u00A0:"
                },
                stats: {
                  title: "Stats",
                  candidateLimitations: {
                    title: "Limitations fonctionnelles du candidat"
                  },
                  recommendations: {
                    title: "Recommandations"
                  }
                }
              }
            },
            footer: {
              rejectButton: "Rejeter",
              rejectPopup: {
                title: "Rejeter la demande?",
                systemMessageDescription: "Cette action rejettera cette demande.",
                description: "Êtes-vous sûr de vouloir continuer?"
              },
              completeRequestButton: "Compléter la demande",
              completePopup: {
                title: "Compléter la demande?",
                systemMessageDescription: "Cette action complétera cette demande.",
                description: "Êtes-vous sûr de vouloir continuer?"
              },
              reOpenRequestButton: "Réouvrir de la demande",
              reOpenPopup: {
                title: "Réouvrir de la demande?",
                systemMessageDescription: "Cette action rouvrira cette demande.",
                description: "Êtes-vous sûr de vouloir continuer ?"
              },
              actionDeniedPopup: {
                title: "Action refusée !",
                systemMessageDescription:
                  "Vous n'êtes pas autorisé à effectuer cette action car le candidat a décidé d'annuler cette demande."
              },
              savedButCancelledByCandidatePopup: {
                title: "Données sauvegardées, mais...",
                systemMessageDescription:
                  "Vos données ont été correctement sauvegardées, mais le statut est maintenant défini en fonction des actions du candidat."
              }
            }
          }
        },
        accommodationReports: {
          title: "Rapports",
          description: "Générer tous les rapports opérationnels.",
          reportTypes: {
            numberOfRequestsReceived: {
              title: "Nombre de demandes reçues",
              year: "Année",
              quarter: "Trimestre",
              month: "Mois",
              requestsReceived: "Demandes reçues",
              totalNumberOfRequests: "Nombre total de demandes :"
            },
            numberOfClosedRequests: {
              title: "Nombre de demandes complétées",
              requestId: "ID de la demande",
              receivedDate: "Date de réception",
              sleSkill: "Description du type de test",
              firstCompletedByAaeDate: "Première date de complétion par UMAE",
              firstApprovedByCandidateDate: "Première date d'accord par le candidat",
              status: "Statut actuel",
              statusValue: "Complétée",
              totalNumberOfRequests: "Nombre total de demandes : "
            },
            numberOfRequestsByLimitationAndTestType: {
              title: "Nombre de demandes par limitations / par type de test ÉLS"
            },
            numberOfRequestsByRecommendation: {
              title: "Nombre de demandes par recommendations"
            },
            serviceStandards: {
              title: "Normes de service"
            },
            timeInEachStatus: {
              title: "Durée passée dans chaque statut"
            }
          }
        }
      }
    },

    // Consultation Services
    consultationServices: {
      title: "Services de consultation",
      sideNav: {
        consultationServicesRequests: {
          title: "Demandes de consultation",
          description: "FR Description here...",
          topTabs: {
            openRequests: {
              title: "Demandes en attente",
              table: {
                columnOne: "Candidat ou candidate",
                columnTwo: "Type de test",
                columnThree: "Date de la demande",
                columnFour: "Actions",
                noData: "Aucune demande en attente",
                viewButtonAccessibility: "Afficher la demande de {0} {1}",
                viewButtonTooltip: "Afficher la demande de mesures d'adaptation"
              }
            },
            completedRequets: {
              title: "Demandes complétées",
              table: {
                columnOne: "Candidat ou candidate",
                columnTwo: "Type de test",
                columnThree: "Date de la demande",
                columnFour: "Actions",
                noData: "Aucune demande complétée",
                viewButtonAccessibility: "Afficher la demande de {0} {1}",
                viewButtonTooltip: "Afficher la demande de consultation"
              }
            }
          }
        }
      }
    },

    // Settings Dialog
    settings: {
      systemSettings: "Paramètres d'affichage pour l'accessibilité",
      zoom: {
        title: "Zoom avant et zoom arrière (+ / -)",
        description:
          "Utilisez les paramètres de votre navigateur pour modifier le niveau de zoom. Vous pouvez également appuyer simultanément sur les touches CTRL et + / - de votre clavier pour effectuer un zoom avant ou un zoom arrière."
      },
      fontSize: {
        title: "Taille de la police",
        label: "Taille de la police\u00A0:",
        description: "Veuillez sélectionner la taille du texte\u00A0:"
      },
      fontStyle: {
        title: "Police",
        label: "Police\u00A0:",
        description: "Sélectionner une nouvelle police pour votre texte\u00A0:"
      },
      lineSpacing: {
        title: "Espacement de texte",
        label: "Espacement de texte\u00A0:",
        description: "Activer l'accessibilité pour l'espacement de texte."
      },
      color: {
        title: "Couleur du texte et de l’arrière plan",
        description:
          "Utilisez les paramètres de votre navigateur pour modifier la couleur du texte et de l’arrière-plan."
      }
    },

    // Multiple Choice  test
    mcTest: {
      questionList: {
        questionIdShort: "Q{0}",
        questionIdLong: "Question {0}",
        reviewButton: "Cocher pour révision",
        markedReviewButton: "Décocher la révision",
        seenQuestion: "Cette question a été vue",
        unseenQuestion: "Cette question n'a pas été vue",
        answeredQuestion: "Une réponse a été entrée pour cette question",
        unansweredQuestion: "Aucune réponse n'a été entrée pour cette question",
        reviewQuestion: "Cette question a été cochée pour révision."
      },
      finishPage: {
        homeButton: "Retour à la page d'accueil"
      }
    },
    // eMIB Test
    emibTest: {
      // Home Page
      homePage: {
        testTitle: "Échantillon de la BRG-e",
        welcomeMsg: "Bienvenue dans le test pratique de BRG-e"
      },

      // HowTo Page
      howToPage: {
        tipsOnTest: {
          title: "Conseils pour répondre à la BRG-e",
          part1: {
            description:
              "La BRG-e vous présente des situations qui vous donneront l’occasion de démontrer les compétences clés en matière de leadership. Voici quelques conseils qui vous aideront à fournir aux évaluateurs l’information dont ils ont besoin pour évaluer votre rendement par rapport à ces compétences clés en leadership\u00A0:",
            bullet1:
              "Répondez à toutes les questions posées dans les courriels que vous avez reçus. Notez également que des situations pour lesquelles on ne pose pas de questions précises peuvent être abordées dans plusieurs courriels. Vous profiterez ainsi de toutes les occasions qui vous sont offertes de démontrer ces compétences.",
            bullet2:
              "N’hésitez pas à présenter vos recommandations si nécessaire, et ce, même s’il s’agit seulement de réflexions préliminaires. S’il le faut, vous pouvez ensuite noter les autres renseignements dont vous auriez besoin pour en arriver à une décision finale.",
            bullet3:
              "Si dans certaines situations vous croyez avoir besoin de parler en personne avec quelqu’un avant de prendre une décision, indiquez les renseignements dont vous avez besoin et comment cela pourrait influencer votre décision.",
            bullet4:
              "Utilisez uniquement l’information fournie dans les courriels et l’information contextuelle. Ne tirez aucune conclusion fondée sur la culture de votre propre organisation. Évitez de faire des suppositions qui ne sont pas raisonnablement corroborées par l’information contextuelle ou les courriels.",
            bullet5:
              "Les situations présentées dans la BRG-e s’inscrivent dans un domaine particulier afin de vous donner suffisamment de contexte pour y répondre.  Pour être efficaces, vos réponses doivent démontrer la compétence ciblée, et non la connaissance du domaine en question."
          },
          part2: {
            title: "Autres renseignements importants",
            bullet1:
              "Le contenu de vos courriels, l'information fournie dans votre liste de tâches et les justifications des mesures prises seront évalués. Le contenu du bloc-notes ne sera pas évalué.",
            bullet2:
              "Votre rédaction ne sera pas évaluée. Aucun point ne sera enlevé pour les fautes d’orthographe, de grammaire, de ponctuation ou pour les phrases incomplètes. Votre rédaction devra toutefois être suffisamment claire pour que les évaluateurs comprennent la situation que vous traitez et vos principaux arguments.",
            bullet3: "Vous pouvez répondre aux courriels dans l’ordre que vous désirez.",
            bullet4: "Vous êtes responsable de la gestion de votre temps."
          }
        },
        testInstructions: {
          title: "Instructions du test",
          hiddenTabNameComplementary: "Sous «\u00A0Instructions\u00A0»",
          onlyOneTabAvailableForNowMsg:
            "Veuillez noter qu’il n’y a qu’un seul onglet principal disponible pour l’instant. Dès que vous commencerez le test, les autres principaux onglets deviendront disponibles.",
          para1:
            "Lorsque vous commencez le test, lisez d’abord l’information contextuelle qui décrit votre poste et l’organisation fictive où vous travaillez. Nous vous recommandons de prendre environ 10 minutes pour la lire. Passez ensuite à la boîte de réception pour lire les courriels que vous avez reçus et prenez des mesures pour y répondre, comme si vous étiez gestionnaire dans cette organisation fictive.",
          para2:
            "Lorsque vous serez dans la boîte de réception, vous aurez accès aux éléments suivants\u00A0:",
          bullet1: "les instructions du test;",
          bullet2:
            "l’information contextuelle décrivant votre rôle en tant que gestionnaire et l’organisation fictive où vous travaillez;",
          bullet3: "un bloc-notes pouvant servir de papier brouillon.",
          step1Section: {
            title: "Étape 1 — Répondre aux courriels",
            description:
              "Vous pouvez répondre aux courriels que vous avez reçus de deux façons\u00A0: en écrivant un courriel ou en ajoutant des tâches à votre liste de tâches. Ces deux façons de répondre sont décrites ci-dessous, suivies d’exemples.",
            part1: {
              title: "Exemple d’un courriel que vous avez reçu\u00A0:",
              para1:
                "Vous trouverez ci-dessous deux façons de répondre au courriel. Vous pouvez choisir l’une ou l’autre des deux options présentées, ou combiner les deux. Les réponses fournies ne sont présentées que pour illustrer comment utiliser chacune des deux façons de répondre. Elles ne démontrent pas nécessairement les compétences clés en leadership qui sont évaluées dans cette situation."
            },
            part2: {
              title: "Ajouter une réponse par courriel",
              para1:
                "Vous pouvez écrire un courriel pour répondre à celui que vous avez reçu dans votre boîte de réception. Votre réponse écrite devrait refléter la façon dont vous répondriez en tant que gestionnaire.",
              para2:
                "Vous pouvez utiliser les fonctions suivantes\u00A0: répondre, répondre à tous ou transférer. Si vous choisissez de transférer un courriel, vous aurez accès à un répertoire qui contient tous vos contacts. Vous pouvez écrire autant de courriels que vous le souhaitez pour répondre à un courriel ou pour gérer des situations que vous remarquez dans plusieurs des courriels reçus."
            },
            part3: {
              title: "Exemple d’une réponse par courriel\u00A0:"
            },
            part4: {
              title: "Ajouter une tâche à la liste de tâches",
              para1:
                "En plus de répondre par courriel, ou au lieu d’en écrire un, vous pouvez ajouter des tâches à la liste de tâches. Une tâche représente une mesure que vous comptez prendre pour gérer une situation présentée dans les courriels. Voici des exemples de tâches\u00A0: planifier une rencontre ou communiquer avec un collègue afin d’obtenir de l’information. Assurez-vous de fournir suffisamment d’information dans votre description de la tâche pour que nous sachions à quelle situation vous répondez. Vous devez également préciser quelles mesures vous comptez prendre et qui devra participer à cette tâche. Vous pouvez en tout temps retourner à un courriel pour ajouter, supprimer ou modifier des tâches."
            },
            part5: {
              title: "Exemple d’ajout d’une tâche à la liste de tâches\u00A0:"
            },
            part6: {
              title: "Comment choisir une façon de répondre",
              para1:
                "Il n’y a pas de bonne ou de mauvaise façon de répondre. Lorsque vous répondez à un courriel, vous pouvez\u00A0:",
              bullet1: "envoyer un ou des courriels;",
              bullet2: "ajouter une ou des tâches à votre liste de tâches;",
              bullet3: "faire les deux.",
              para2:
                "C’est le contenu de vos réponses qui sera évalué, et non la façon de répondre (c’est-à-dire si vous avez répondu par courriel ou en ajoutant une tâche à votre liste de tâches). Par conséquent, vos réponses doivent être suffisamment détaillées et claires pour que les évaluateurs puissent évaluer comment vous gérez la situation. Par exemple, si vous prévoyez organiser une réunion, vous devez préciser de quoi il y sera question.",
              para3Part1: "Si vous décidez d’écrire un courriel ",
              para3Part2: "et",
              para3Part3:
                " d’ajouter une tâche à votre liste de tâches pour répondre à un courriel que vous avez reçu, vous n’avez pas à répéter la même information aux deux endroits. Par exemple, si vous mentionnez dans un courriel que vous organiserez une réunion avec un collègue, vous n’avez pas à ajouter cette réunion à votre liste de tâches."
            }
          },
          step2Section: {
            title: "Étape 2 — Justifier les mesures prises (facultatif)",
            description:
              "Après avoir écrit un courriel ou ajouté une tâche, vous pouvez expliquer votre raisonnement dans la section réservée à cet effet, si vous le souhaitez. Cette section se situe au bas des réponses par courriel et des tâches. La justification des mesures prises est facultative. Notez que vous pouvez choisir d’expliquer certaines mesures que vous avez prises tandis que d’autres ne nécessitent pas d’explications supplémentaires. De même, vous pouvez décider de justifier les mesures prises lorsque vous répondez à certains courriels, et non à d'autres. Cela s'applique également aux tâches de la liste de tâches.",
            part1: {
              title:
                "Exemple d’une réponse par courriel accompagnée de justifications des mesures prises\u00A0:"
            },
            part2: {
              title:
                "Exemple d’une liste de tâches accompagnée de justifications des mesures prises\u00A0:"
            }
          },
          exampleEmail: {
            to: "T.C. Bernier (gestionnaire, Équipe de l’assurance de la qualité)",
            from: "Geneviève Bédard (directrice, Unité de recherche et innovation)",
            subject: "Préparer Mary à son affectation",
            date: "vendredi 4 novembre",
            body: "Bonjour T.C.,\n\nJ’ai été ravie d’apprendre qu’une de tes analystes de l’assurance de la qualité, Mary Woodside, avait accepté une affectation de six mois avec mon équipe, à compter du 2 janvier. Je crois comprendre qu’elle a de l’expérience en enseignement et en utilisation d’outils pédagogiques modernes dans le cadre de son travail antérieur de professeure au niveau collégial. Mon équipe a besoin d’aide pour mettre au point des techniques d’enseignement novatrices qui favorisent la productivité et le bien-être général des employés. Je pense donc que l’expérience de Mary sera un bon atout pour l’équipe.\n\nY a-t-il des domaines dans lesquels tu aimerais que Mary acquière plus d’expérience, laquelle serait utile lors de son retour dans ton équipe ? Je tiens à maximiser les avantages de l’affectation pour nos deux équipes.\n\nAu plaisir de recevoir tes commentaires.\n\nGeneviève"
          },
          exampleEmailResponse: {
            emailBody:
              "Bonjour Geneviève,\n\nJe suis d’accord que nous devrions planifier l’affectation de Mary afin que nos deux équipes en tirent parti. Je suggère de former Mary à la synthèse de données provenant de sources multiples. Cela l’aiderait à élargir ses compétences et serait utile à mon équipe à son retour. De même, les membres de ton équipe pourraient profiter de son expérience en enseignement. Je la consulterai directement, car j’aimerais connaître son avis à ce sujet. Je te recontacterai au cours de la semaine, quand j’aurai plus d’information à te fournir.\n\nCela dit, quelles sont tes attentes ? Y a-t-il présentement certains défis ou des aspects particuliers de la dynamique de ton équipe dont il faudrait tenir compte ? Avant de rencontrer Mary pour discuter de son affectation, j’aimerais tenir compte de tous les facteurs, tels que les besoins actuels, les défis et la dynamique de ton équipe.\n\nMerci.\n\nT.C.",
            reasonsForAction:
              "Je compte rencontrer Mary pour discuter de ses attentes concernant l’affectation et pour établir des objectifs clairs. Je veux qu’elle se sente motivée et sache ce qu’on attend d’elle, afin de l’aider à se préparer en conséquence. J’examinerai également ses objectifs pour l’année afin de m’assurer que ce que je propose cadre bien avec son plan de perfectionnement professionnel."
          },
          exampleTaskResponse: {
            task: "- Répondre au courriel de Geneviève\u00A0:\n     > lui proposer de former Mary à la synthèse de l’information provenant de sources multiples afin qu’elle puisse élargir ses compétences;\n     >	lui demander quelles sont ses attentes et quels sont les défis de son équipe afin que je puisse tenir compte de tous les facteurs pour déterminer comment son équipe pourrait profiter de l’expérience de Mary dans le domaine de la formation;\n     > l’informer que je travaille à recueillir plus d’information auprès de Mary, et que je lui ferai part de mes suggestions d’ici la fin de la semaine.\n- Organiser une réunion avec Mary pour discuter des objectifs de son affectation et veiller à ce qu’elle se sente motivée et à ce qu’elle sache ce qui est attendu d’elle.\n- Consulter les objectifs passés et actuels de Mary pour vérifier que ce que je propose est conforme à son plan de perfectionnement professionnel.",
            reasonsForAction:
              "Former Mary à la synthèse de l’information provenant de sources multiples serait avantageux pour mon équipe, qui a besoin de consolider l’information recueillie auprès de nombreuses sources. Demander à Geneviève quels sont ses attentes et ses défis m’aidera à mieux préparer Mary et à m’assurer que l’affectation sera avantageuse pour nos deux équipes."
          }
        },
        evaluation: {
          title: "Évaluation",
          bullet1:
            "Les mesures que vous prenez et les explications que vous donnez seront prises en compte dans l’évaluation de votre rendement pour chacune des compétences clés en leadership (décrites ci-dessous). On évaluera à quel point ces mesures et explications démontrent les compétences clés en leadership.",
          bullet2:
            "L’efficacité des mesures prises sera évaluée. Le niveau d’efficacité est déterminé par l’effet positif ou négatif que ces mesures auraient sur la résolution des situations présentées, et par l’étendue de cet effet.",
          bullet3:
            "Vos réponses seront également évaluées en fonction de leur contribution à l’atteinte des objectifs organisationnels présentés dans l’information contextuelle.",
          keyLeadershipCompetenciesSection: {
            title: "Compétences clés en leadership",
            para1Title: "Créer une vision et une stratégie\u00A0: ",
            para1:
              "Les gestionnaires contribuent à définir l’avenir et à tracer la voie à suivre. Pour ce faire, ils tiennent compte du contexte. Ils mettent à contribution leurs connaissances. Ils obtiennent et intègrent de l’information provenant de diverses sources pour la mise en œuvre d’activités concrètes. Ils considèrent divers points de vue et consultent d’autres personnes, au besoin. Les gestionnaires assurent l’équilibre entre les priorités organisationnelles et contribuent à améliorer les résultats.",
            para2Title: "Mobiliser les personnes\u00A0: ",
            para2:
              "Les gestionnaires inspirent et motivent les personnes qu’ils dirigent. Ils gèrent le rendement de leurs employés et leur offrent de la rétroaction constructive et respectueuse pour encourager et rendre possible l’excellence en matière de rendement. Ils dirigent en donnant l’exemple et se fixent des objectifs personnels qui sont plus exigeants que ceux qu’ils établissent pour les autres.",
            para3Title: "Préserver l’intégrité et le respect\u00A0: ",
            para3:
              "Les gestionnaires donnent l’exemple sur le plan des pratiques éthiques, du professionnalisme et de l’intégrité personnelle, en agissant dans l’intérêt du Canada, des Canadiens et des Canadiennes. Ils créent des environnements de travail inclusifs, empreints de respect et de confiance, où les conseils judicieux sont valorisés. Ils encouragent les autres à faire part de leurs points de vue, tout en encourageant la collégialité.",
            para4Title: "Collaborer avec les partenaires et les intervenants\u00A0: ",
            para4:
              "Les gestionnaires cherchent à obtenir, de façon délibérée et ingénieuse, un grand éventail de perspectives. Lorsqu’ils établissent des partenariats, ils gèrent les attentes et visent à atteindre un consensus. Ils font preuve d’ouverture et de souplesse afin d’améliorer les résultats et apportent une perspective globale de l’organisation à leurs interactions. Les gestionnaires reconnaissent le rôle des partenaires dans l’obtention des résultats.",
            para5Title: "Promouvoir l’innovation et orienter le changement\u00A0: ",
            para5:
              "Les gestionnaires créent un environnement propice aux idées audacieuses, à l’expérimentation et à la prise de risques en toute connaissance de cause. Lors de la mise en œuvre d’un changement, ils maintiennent l’élan, surmontent la résistance et anticipent les conséquences. Ils perçoivent les revers comme une bonne occasion de comprendre et d’apprendre.",
            para6Title: "Obtenir des résultats\u00A0: ",
            para6:
              "Les gestionnaires s’assurent de répondre aux objectifs de l’équipe en gérant les ressources. Ils prévoient, planifient et surveillent les progrès, et font des ajustements au besoin. Ils démontrent leur connaissance du contexte lors de la prise de décisions. Les gestionnaires assument la responsabilité personnelle à l’égard de leurs actions et des résultats de leurs décisions."
          }
        }
      },

      // Background Page
      background: {
        hiddenTabNameComplementary: "Sous «\u00A0Information contextuelle\u00A0»",
        orgCharts: {
          link: "Description de l'image",
          ariaLabel: "Description de l'image de l'Organigramme Équipe",
          treeViewInstructions:
            "Ci-dessous, vous trouverez la vue arborescente de l’organigramme. Une fois sélectionné, vous pouvez utiliser les touches fléchées pour la navigation, l’expansion et l’effondrement de l’information."
        }
      },

      // Inbox Page
      inboxPage: {
        tabName: "Sous «\u00A0Boîte de réception\u00A0»",
        emailId: " courriel ",
        subject: "Objet",
        to: "À",
        from: "De",
        date: "Date",
        addReply: "Ajouter une réponse par courriel",
        addTask: "Ajouter une réponse par liste de tâches",
        yourActions: `Vous avez répondu avec {0} courriel(s) et {1} liste(s) de tâches`,
        editActionDialog: {
          addEmail: "Ajouter une réponse par courriel",
          editEmail: "Modifier la réponse par courriel",
          addTask: "Ajouter à la liste de tâches",
          editTask: "Modifier la tâche",
          save: "Sauvegarder la réponse"
        },
        characterLimitReached: "(Limite atteinte)",
        emailCommons: {
          to: "À\u00A0:",
          toFieldSelected: "Champ À choisi",
          cc: "Cc\u00A0:",
          ccFieldSelected: "Champ CC choisi",
          currentSelectedPeople: "Les personnes choisies actuellement sont\u00A0: {0}",
          currentSelectedPeopleAreNone: "Aucunes",
          reply: "répondre",
          replyAll: "répondre à tous",
          forward: "transmettre",
          editButton: "Modifier la réponse",
          deleteButton: "Supprimer la réponse",
          originalEmail: "Courriel d’origine",
          toAndCcFieldsPlaceholder: "Sélectionnez à partir du carnet d’adresses",
          yourResponse: "Votre réponse"
        },
        addEmailResponse: {
          selectResponseType:
            "Veuillez choisir la manière dont vous souhaitez répondre au courriel d’origine\u00A0:",
          response: "Votre réponse par courriel\u00A0: limite de {0} caractères",
          reasonsForAction:
            "Justification des mesures prises (facultatif)\u00A0: limite de {0} caractères",
          emailResponseTooltip: "Rédiger une réponse au courriel que vous avez reçu.",
          reasonsForActionTooltip:
            "Dans cette section, vous pouvez expliquer pourquoi vous avez pris une certaine mesure en réponse à une situation, si vous souhaitez fournir des renseignements supplémentaires.",
          invalidToFieldError: "Ce champ ne peut être vide"
        },
        emailResponse: {
          title: "Réponse par courriel no. ",
          description: "Pour cette réponse, vous avez choisi de\u00A0:",
          response: "Votre réponse par courriel\u00A0:",
          reasonsForAction: "Votre justification des mesures prises (facultatif)\u00A0:"
        },
        addEmailTask: {
          header: "Courriel no {0}: {1}",
          task: "Votre réponse par liste de tâches\u00A0: limite de {0} caractères",
          reasonsForAction:
            "Justification des mesures prises (facultatif)\u00A0: limite de {0} caractères"
        },
        taskContent: {
          title: "Réponse par liste de tâches no. ",
          task: "Votre réponse par liste de tâches\u00A0:",
          taskTooltipPart1:
            "Une action que vous comptez prendre pour résoudre une situation dans les courriels.",
          taskTooltipPart2:
            "Exemple\u00A0: planifier une réunion, demander de l'information à un collègue.",
          reasonsForAction: "Votre justification des mesures prises (facultatif)\u00A0:",
          reasonsForActionTooltip:
            "Dans cette section, vous pouvez expliquer pourquoi vous avez pris une certaine mesure en réponse à une situation, si vous souhaitez fournir des renseignements supplémentaires."
        },
        deleteResponseConfirmation: {
          title: "Êtes-vous certain de vouloir annuler cette réponse ?",
          systemMessageDescription:
            "Votre réponse ne sera pas sauvegardée si vous continuez. Si vous souhaitez enregistrer votre réponse, vous pouvez y retourner. Toutes vos réponses peuvent être modifiées ou supprimées avant de soumettre le test.",
          description:
            "Si vous ne voulez pas sauvegarder la réponse, cliquez sur le bouton «\u00A0Supprimer la réponse\u00A0»."
        }
      },

      // Confirmation Page
      confirmationPage: {
        submissionConfirmedTitle: "Félicitations ! Votre test a été soumis.",
        feedbackSurvey:
          "Nous aimerions recevoir vos commentaires sur l’évaluation. Veuillez remplir ce {0} facultatif avant de vous déconnecter et de quitter.",
        optionalSurvey: "sondage de 15 minutes",
        surveyLink: "https://surveys-sondages.psc-cfp.gc.ca/s/se.ashx?s=25113745259E9113&c=fr-CA",
        logout:
          "Pour des raisons de sécurité, assurez-vous de fermer votre session dans le coin supérieur droit de cette page. Vous pouvez discrètement recueillir vos effets personnels et quitter la séance de test. Si vous avez des questions ou des préoccupations au sujet de votre test, veuillez communiquer avec {0}.",
        thankYou: "Nous vous remercions d’avoir terminé votre évaluation. Bonne chance !"
      },

      // Quit Confirmation Page
      quitConfirmationPage: {
        title: "Vous avez quitté le test",
        instructionsTestNotScored1: "Votre test ",
        instructionsTestNotScored2: "ne sera pas corrigé.",
        instructionsRaiseHand:
          "Veuillez lever la main. L’administrateur de tests viendra vous donner d’autres directives.",
        instructionsEmail:
          "Si vous avez des questions ou des préoccupations au sujet de votre test, veuillez communiquer avec {0}."
      },

      // Timeout Page
      timeoutConfirmation: {
        timeoutConfirmedTitle: "Votre temps de test est écoulé...",
        timeoutSaved:
          "Vos réponses ont été sauvegardées et soumises aux fins de notation. Veuillez prendre note que l’information dans le bloc-notes n’est pas sauvegardée.",
        timeoutIssue:
          "S’il y a eu un problème, veuillez en informer votre administrateur de tests. Cliquez sur «\u00A0Continuer\u00A0» pour quitter cette séance de test et pour recevoir d’autres instructions."
      },

      // Test tabs
      tabs: {
        instructionsTabTitle: "Instructions",
        backgroundTabTitle: "Information contextuelle",
        inboxTabTitle: "Boîte de réception",
        disabled: "Vous ne pouvez accéder à la boîte de courriel avant d'avoir commencé le test."
      },

      // Test Footer
      testFooter: {
        timer: {
          timer: "Chronomètre",
          timeLeft: "Temps restant dans la séance de test\u00A0:",
          timerHidden: "Minuterie cachée.",
          timerHide: "Masquer le chronomètre",
          timeRemaining: "Temps restant\u00A0: {0} heures, {1} minutes, {2} secondes"
        },
        pauseButton: "Mettre le test en pause",
        unpauseButton: "Reprendre le test",
        unpausePopup: {
          title: "Reprendre le test",
          description1: "Êtes-vous sûr de vouloir reprendre votre test ?",
          description2:
            "Le temps restant dans votre banque de pauses sera recalculé lorsque vous sélectionnerez Reprendre le test.",
          unpauseButton: "Reprendre le test"
        },
        submitTestPopupBox: {
          title: "Soumettre votre test",
          warning: {
            title: "Avertissement\u00A0: your notebook will not be saved.",
            message1:
              "Cette étape mettra fin à votre séance de test. Assurez-vous d'avoir répondu à toutes les questions avant de soumettre votre test. Une fois le test soumis, vous ne pourrez plus apporter de changements. Le contenu du bloc-notes ne sera ni transmis ni corrigé.",
            message2: "Souhaitez-vous poursuivre ?"
          },
          description:
            "Si vous êtes prêt(e) à soumettre votre test pour la notation, cliquez sur le bouton «\u00A0Soumettre le test\u00A0». La séance de test sera fermée et vous recevrez d’autres instructions."
        },
        quitTestPopupBox: {
          title: "Quitter le test",
          description1:
            "Êtes-vous certain de vouloir quitter ce test ? Toutes vos réponses seront perdues, vous ne pourrez les récupérer et vous perdrez l'accès au test.",
          description2: "Pour quitter, vous devez consentir aux énoncés suivants\u00A0:",
          checkboxOne: "Je choisis de quitter ce test;",
          checkboxTwo: "Je comprends que mon test ne sera pas noté;",
          checkboxThree:
            "Je comprends qu'il pourrait y avoir une période d'attente avant de pouvoir refaire ce test;"
        }
      }
    },

    superUserDashboard: {
      title: "FR Superuser Dashboard",
      sideNav: {
        celeryTab: "FR Celery Tasks",
        adminTab: "FR Site Admin"
      },
      settingsTab: {
        settingHeader: "FR Setting",
        actionHeader: "FR Action",
        emptyTableMsg: "FR No settings",
        invalidKey: "FR Invalid key format"
      },
      virtualMeetingRoomsTab: {
        title: "FR Teams Virtual Meeting Rooms Settings",
        table: {
          noData: "FR No Virtual Rooms created",
          column1: "FR Name",
          column2: "FR User ID",
          column3: "Delete"
        },
        addNewRoomButton: "FR New Room",
        deleteRoomButton: "FR Delete Room {0}",
        deleteRoomButtonTooltip: "FR Delete Room",
        addPopup: {
          title: "FR Add New Virtual Teams Room",
          desc: "FR Too add a room, please fill out the fields below",
          name: "FR Name",
          userId: "FR User ID",
          nameAlreadyExists: "FR This name already exists",
          nameInvalid: "FR Name should be the same format as an email",
          userIdInvalid: "FR User ID should follow the format 8 chars-4 chars-4 chars-12 char"
        },
        deletePopup: {
          title: "FR Delete Virtual Teams Room",
          desc: "FR Are you sure you want to delete {0}? This action cannot be undone.",
          desc2: "FR Are you sure you want to continue?"
        }
      },
      celeryTasks: {
        title: "FR Celery",
        description:
          "FR Execute scheduled tasks in an ad-hoc manner to “force” the execution to resolve PROD level problems",
        popupText: "FR Check the docker logs to see the task execution",
        uitTab: {
          tabTitle: "FR UIT Tasks",
          taskHeader: "FR Task",
          actionHeader: "FR Action",
          runButton: "FR Run",
          emptyTableMsg: "FR No celery tasks",
          resetCompletedProfileFlag: "FR Reset Completed Profile Flag",
          handleExpiredUitProcesses: "FR Handle Expired Uit Processes"
        },
        permissionTab: {
          tabTitle: "FR Permission Tasks",
          taskHeader: "FR Task",
          actionHeader: "FR Action",
          runButton: "FR Run",
          emptyTableMsg: "FR No celery tasks",
          removeExpiredTestPermissions: "FR Remove Expired Test Permissions",
          deprovisionAfterInactivity: "FR Deprovision After Inactivity",
          deprovisionAdminAfterInactivityThirtyDays:
            "FR Deprovision Admin After Inactivity Thirty Days",
          removeExpiredTestScorerPermissions:
            "FR Remove Expired Test Scorer Permissions (540 days+)"
        },
        testTab: {
          tabTitle: "FR Test Tasks",
          taskHeader: "FR Task",
          actionHeader: "FR Action",
          runButton: "FR Run",
          emptyTableMsg: "FR No celery tasks",
          updateNonSubmittedActiveAssignedTests: "FR Update Non Submitted Active Assigned Tests",
          invalidateLockedAssignedTests: "FR Invalidate Locked Assigned Tests",
          unassignReadyAndPreTestAssignedTests: "FR Unassign Ready And Pre Test Assigned Tests",
          unpauseAssignedTests: "FR Unpause Assigned Tests",
          expireOldAccommodations: "FR Expire Old Accomodations Requests"
        },
        table: {
          featureHeader: "FR Feature",
          actionHeader: "FR Action",
          runButton: "FR Run",
          emptyTableMsg: "FR No permissions"
        },
        celeryConfirmationPopup: {
          title: "FR Celery Task Executed",
          description: "FR Check the docker logs to see the task execution",
          closeButton: "FR Close"
        }
      },
      siteAdmin: {
        title: "FR Site Admin",
        paragraph: "FR Control features and functions for the site overall.",
        features: {
          title: "FR Features"
        },
        tfa: {
          title: "FR 2 Factor Authenication"
        },
        virtualMeeting: {
          title: "FR Virtual Meeting Settings"
        }
      }
    },

    // Screen Reader
    ariaLabel: {
      mainMenu: "Menu Principal",
      tabMenu: "Menu des onglets de la BRG-e",
      instructionsMenu: "Menu des instructions",
      languageToggleBtn: "bouton-de-langue-a-bascule",
      authenticationMenu: "Menu d'authentification",
      emailHeader: "en-tête du courriel",
      responseDetails: "détails de la réponse",
      reasonsForActionDetails: "motifs de l'action",
      taskDetails: "détails sur la ou les tâches",
      emailOptions: "options de messagerie",
      taskOptions: "options de tâche",
      taskTooltip: "infobulle de tâche",
      emailResponseTooltip: "Infobulle pour les réponses par courriel",
      reasonsForActionTooltip: "infobulle des motifs de l'action",
      passwordConfirmationRequirements: "Il doit correspondre à votre mot de passe",
      dobDayField: "Champ Journée sélectionné",
      dobMonthField: "Champ Mois sélectionné",
      dobYearField: "Champ Année sélectionné",
      emailsList: "Liste des courriels",
      questionList: "List des questions",
      topNavigationSection: "Barre de navigation du haut",
      sideNavigationSection: "Menu de navigation latérale",
      quitTest: "Quitter le test",
      selectedPermission: "Permission sélectionnée\u00A0:",
      description: "Description\u00A0:",
      pendingStatus: "Statut en attente\u00A0:",
      scorerPanel: "FR Scorer Panel",
      navExpandButton: "Navigation à bascule",
      images: {
        banner: "FR Banner",
        canadaLogo: "FR Canada",
        pscLogo: "Commission de la fonction publique du Canada"
      }
    },

    // Commons
    commons: {
      psc: "Commission de la fonction publique",
      loading: "Chargement",
      nextButton: "Suivant",
      backButton: "Retour",
      enterEmibSample: "Passer à l’échantillon du test de la BRG-e",
      enterEmibReal: "Passer aux instructions du test",
      resumeEmibReal: "Rentrez la BRG-e",
      startTest: "Commencer le test",
      resumeTest: "Reprendre le test",
      confirmStartTest: {
        aboutToStart: "Vous êtes sur le point de commencer le test.",
        breakBankTitle: "Banque de pauses\u00A0:",
        breakBankWarning: "Vous avez une banque de pauses de {0} pour ce test.",
        testSectionTimeTitle: "Durée totale\u00A0:",
        newtimerWarning: "Vous aurez un total de {0} pour compléter le test.",
        additionalTimeWarning: "Ceci inclut {0} de temps supplémentaire.",
        wishToProceed: "Souhaitez-vous poursuivre ?",
        timeUnlimited: "temps illimité",
        confirmProceed: "Oui, je souhaite poursuivre."
      },
      submitTestButton: "Soumettre votre test",
      quitTest: "Quitter le test",
      returnHome: "Retour à la page d'accueil",
      returnToTest: "Reprendre le test",
      returnToResponse: "Retourner à la réponse",
      passStatus: "Réussi",
      failStatus: "Échoue",
      error: "Erreur",
      success: "Succès",
      info: "Information",
      warning: "Avertissement !",
      enabled: "Activé",
      disabled: "Désactivé",
      backToTop: "Haut de la page",
      notepad: {
        title: "Bloc-notes",
        hideNotepad: "Masquer le bloc-notes",
        placeholder: "Mettez vos notes ici..."
      },
      calculator: {
        title: "Calculatrice",
        errorMessage: "Erreur",
        calculatorResultAccessibility: "La calculatrice affiche",
        backspaceButton: "Espace arrière",
        hideCalculator: "Masquer la calculatrice"
      },
      cancel: "Annuler",
      accept: "Accepter",
      cancelChanges: "Annuler les modifications",
      cancelResponse: "Annuler la réponse",
      addButton: "Ajouter",
      editButton: "Modifier",
      deleteButton: "Supprimer",
      saveButton: "Sauvegarder",
      saveDraftButton: "Sauvegarder",
      applyButton: "Appliquer",
      duplicateButton: "Dupliquer",
      previewButton: "Aperçu",
      close: "Fermer",
      login: "Ouvrir une session",
      ok: "OK",
      deleteConfirmation: "Confirmer la suppression",
      continue: "Continuer",
      sendRequest: "Envoyer la requête",
      submit: "Soumettre",
      na: "S/O",
      valid: "Valide",
      invalid: "Invalidé",
      confirm: "Confirmer",
      english: "Anglais",
      french: "Français",
      bilingual: "Bilingue",
      // do not forget to update the text_resources.py file (assigned_test_status) if updates are made for those statuses
      status: {
        checkedIn: "Enregistré",
        preTest: "Pré-test",
        active: "En test",
        transition: "En transition",
        locked: "Verrouillé",
        paused: "En pause",
        neverStarted: "Non commencé",
        inactivity: "Inactivité",
        timedOut: "Temps écoulé",
        quit: "Quitté",
        submitted: "Soumis",
        unassigned: "Non attribué",
        invalid: "Invalide",
        pending: "En attente",
        cannotBeShared: "Ne peut être partagé"
      },
      reservationCodeStatus: {
        // codename ==> reserved
        consumed: "Réclamé",
        booked: "Réservé",
        revoked: "Révoqué",
        withdrawn: "S'est retiré",
        sendUpdates: "Envoyer les mises à jour",
        sent: "Envoyé"
      },
      seconds: "seconde(s)",
      minutes: "minute(s)",
      hours: "heure(s)",
      pleaseSelect: "Veuillez sélectionner une option",
      none: "Aucun",
      convertedScore: {
        invalidScoreConversion: "Conversion invalide du résultat",
        pass: "Réussite",
        fail: "Échec",
        none: "Aucun résultat",
        notScored: "Aucune correction",
        invalid: "Invalide"
      },
      yes: "Oui",
      no: "Non",
      preferNotToAnswer: "Préfère ne pas répondre",
      active: "Actif",
      inactive: "Inactif",
      moveUp: "FR Move Up",
      moveDown: "FR Move Down",
      default: "Par défaut",
      tools: "Outils",
      genericTable: {
        maximumAllowedResultsExceededMessage:
          "Trop de résultats ( >{0} ). Utilisez l'outils de recherche pour réduire le nombre de résultats."
      },
      pagination: {
        nextPageButton: "Page suivante",
        previousPageButton: "Page précédente",
        skipToPagesLink: "Aller directement à la liste des pages",
        breakAriaLabel: "FR Move multiple pages"
      },
      saved: "Sauvegarder",
      fieldCannotBeEmptyErrorMessage: "Ce champ ne peut être vide",
      currentLanguage: "fr",
      true: "VRAI",
      false: "FAUX",
      next: "Suivant",
      previous: "Précédent",
      upload: "Téléverser",
      all: "Tous",
      generate: "Regénérer",
      unchanged: "(inchangé)",
      discard: "Supprimer",
      never: "Jamais",
      indefinite: "Indéterminée",
      sent: "Envoyé",
      testType: {
        uit: "FR UIT",
        supervised: "FR Supervised"
      },
      approve: "FR Approve",
      protected: "PROTÉGÉ",
      agree: "Je suis en accord",
      daysOfWeek: {
        monday: "Lundi",
        tuesday: "Mardi",
        wednesday: "Mercredi",
        thursday: "Jeudi",
        friday: "Vendredi",
        saturday: "Samedi",
        sunday: "Dimanche"
      }
    }
  }
};

const LOCALIZE = new LocalizedStrings(LOCALIZE_OBJ);

export default LOCALIZE;
export { LOCALIZE_OBJ };
