import React, { Component } from "react";
import LOCALIZE from "./text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import getValidScoredTests, { getArchivedScoredTests } from "./modules/MyTestsRedux";
import CustomButton from "./components/commons/CustomButton";
import THEME from "./components/commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars } from "@fortawesome/free-solid-svg-icons";
import getConvertedScore from "./helpers/scoreConversion";
import AlertMessage, { ALERT_TYPE } from "./components/commons/AlertMessage";
import TEST_STATUS from "./components/ta/Constants";
import { Tabs, Tab } from "react-bootstrap";
import ValidTests from "./components/myTests/ValidTests";
import ArchivedTests from "./components/myTests/ArchivedTests";
import StyledTooltip, { TYPE, EFFECT } from "./components/authentication/StyledTooltip";
import { convertDateTimeToUsersTimezone } from "./modules/helpers";

const styles = {
  firstRowStyle: {
    textAlign: "left"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  alertMessageContainer: {
    padding: "12px 0"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  allUnset: {
    all: "unset"
  }
};

class MyTests extends Component {
  state = {
    validTestsCurrentlyLoading: true,
    validTestsRowsDefinition: {},
    selectedValidTestDetails: {},
    triggerValidTestDetailsPopup: false,
    archivedTestsCurrentlyLoading: true,
    archivedTestsRowsDefinition: {},
    selectedArchivedTestDetails: {},
    triggerArchivedTestDetailsPopup: false
  };

  componentDidMount = () => {
    this.populateValidTestsRowsDefinition();
    this.populateArchivedTestsRowsDefinition();
  };

  getTABS = () => {
    return [
      {
        key: `valid-tests`,
        tabName: LOCALIZE.myTests.topTabs.validTests,
        body: (
          <ValidTests
            currentlyLoading={this.state.validTestsCurrentlyLoading}
            rowsDefinition={this.state.validTestsRowsDefinition}
            selectedTestDetails={this.state.selectedValidTestDetails}
            triggerPopup={this.state.triggerValidTestDetailsPopup}
          />
        )
      },
      {
        key: `archived-tests`,
        tabName: LOCALIZE.myTests.topTabs.archivedTests,
        body: (
          <ArchivedTests
            currentlyLoading={this.state.archivedTestsCurrentlyLoading}
            rowsDefinition={this.state.archivedTestsRowsDefinition}
            selectedTestDetails={this.state.selectedArchivedTestDetails}
            triggerPopup={this.state.triggerArchivedTestDetailsPopup}
          />
        )
      }
    ];
  };

  // populating valid tests table rows
  populateValidTestsRowsDefinition = () => {
    // initializing needed object and array for validTestsRowsDefinition props (needed for GenericTable component)
    let validTestsRowsDefinition = {};
    const data = [];
    this.props
      .getValidScoredTests()
      .then(response => {
        for (let i = 0; i < response.length; i++) {
          const adjustedSubmitDate = convertDateTimeToUsersTimezone(
            response[i].utc_submit_date
          ).adjustedDate;
          const adjustedRetestDate = convertDateTimeToUsersTimezone(
            response[i].utc_retest_period_date
          ).adjustedDate;
          const tempdata = response[i];
          tempdata.adjusted_submit_date = adjustedSubmitDate;
          tempdata.adjusted_retest_date = adjustedRetestDate;
          // pushing needed data in data array (needed for GenericTable component)
          data.push({
            column_1: `${response[i][`test_name_${this.props.currentLanguage}`]} (${
              response[i].test_code
            })`,
            column_2: adjustedSubmitDate,
            column_3: response[i].is_invalid
              ? LOCALIZE.commons.status.invalid
              : response[i].pending_score
                ? LOCALIZE.commons.status.pending
                : response[i].total_score !== null
                  ? response[i].show_score
                    ? response[i].total_score
                    : LOCALIZE.commons.status.cannotBeShared
                  : LOCALIZE.commons.na,
            column_4: response[i].is_invalid
              ? LOCALIZE.commons.status.invalid
              : response[i].pending_score
                ? LOCALIZE.commons.status.pending
                : response[i].show_result || response[i].status_codename === TEST_STATUS.QUIT
                  ? getConvertedScore(
                      response[i][`${this.props.currentLanguage}_converted_score`],
                      response[i].status_codename
                    )
                  : LOCALIZE.commons.status.cannotBeShared,
            column_5:
              response[i].pending_score && !response[i].is_invalid
                ? LOCALIZE.commons.status.pending
                : response[i].is_most_recent_valid_test &&
                    response[i].result_valid_indefinitely &&
                    response[i].status_codename !== TEST_STATUS.QUIT
                  ? LOCALIZE.commons.never
                  : response[i].calculated_score_valid_until !== null
                    ? response[i].calculated_score_valid_until
                    : LOCALIZE.commons.na,
            column_6:
              response[i].pending_score && !response[i].is_invalid
                ? LOCALIZE.commons.status.pending
                : adjustedRetestDate,
            column_7: (
              <StyledTooltip
                id={`valid-results-button-row-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`valid-results-button-row-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faBinoculars} />
                        </>
                      }
                      action={() => {
                        this.handleViewValidTestResults(tempdata);
                      }}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.myTests.table.viewResultsButtonAriaLabel,
                        `${response[i][`test_name_${this.props.currentLanguage}`]} (${
                          response[i].test_code
                        })`,
                        adjustedSubmitDate,
                        adjustedRetestDate,
                        getConvertedScore(
                          response[i][`${this.props.currentLanguage}_converted_score`],
                          response[i].status_codename
                        )
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>{LOCALIZE.myTests.table.viewResultsButton}</p>
                  </div>
                }
              />
            )
          });
        }
        // updating validTestsRowsDefinition object with provided data and needed style
        validTestsRowsDefinition = {
          column_1_style: styles.firstRowStyle,
          column_2_style: styles.basicRowStyle,
          column_3_style: styles.basicRowStyle,
          column_4_style: styles.basicRowStyle,
          data: data
        };

        // saving results in state
        this.setState({
          validTestsRowsDefinition: validTestsRowsDefinition
        });
      })
      .then(() => {
        this.setState({ validTestsCurrentlyLoading: false });
      });
  };

  // populating archived tests table rows
  populateArchivedTestsRowsDefinition = () => {
    // initializing needed object and array for archivedTestsRowsDefinition props (needed for GenericTable component)
    let archivedTestsRowsDefinition = {};
    const data = [];
    this.props
      .getArchivedScoredTests()
      .then(response => {
        for (let i = 0; i < response.length; i++) {
          const adjustedSubmitDate = convertDateTimeToUsersTimezone(
            response[i].utc_submit_date
          ).adjustedDate;
          const adjustedRetestDate = convertDateTimeToUsersTimezone(
            response[i].utc_retest_period_date
          ).adjustedDate;
          const tempdata = response[i];
          tempdata.adjusted_submit_date = adjustedSubmitDate;
          tempdata.adjusted_retest_date = adjustedRetestDate;
          // pushing needed data in data array (needed for GenericTable component)
          data.push({
            column_1: `${response[i][`test_name_${this.props.currentLanguage}`]} (${
              response[i].test_code
            })`,
            column_2: adjustedSubmitDate,
            column_3: (
              <StyledTooltip
                id={`archived-results-button-row-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`archived-results-button-row-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faBinoculars} />
                        </>
                      }
                      action={() => {
                        this.handleViewArchivedTestResults(tempdata);
                      }}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.myTests.table.viewResultsButtonAriaLabel,
                        `${response[i][`test_name_${this.props.currentLanguage}`]} (${
                          response[i].test_code
                        })`,
                        adjustedSubmitDate,
                        adjustedRetestDate,
                        getConvertedScore(
                          response[i][`${this.props.currentLanguage}_converted_score`],
                          response[i].status_codename
                        )
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>{LOCALIZE.myTests.table.viewResultsButton}</p>
                  </div>
                }
              />
            )
          });
        }
        // updating archivedTestsRowsDefinition object with provided data and needed style
        archivedTestsRowsDefinition = {
          column_1_style: styles.firstRowStyle,
          column_2_style: styles.basicRowStyle,
          column_3_style: styles.basicRowStyle,
          column_4_style: styles.basicRowStyle,
          data: data
        };

        // saving results in state
        this.setState({
          archivedTestsRowsDefinition: archivedTestsRowsDefinition
        });
      })
      .then(() => {
        this.setState({ archivedTestsCurrentlyLoading: false });
      });
  };

  handleViewValidTestResults = data => {
    this.setState({ selectedValidTestDetails: data }, () => {
      this.setState({ triggerValidTestDetailsPopup: !this.state.triggerValidTestDetailsPopup });
    });
  };

  handleViewArchivedTestResults = data => {
    this.setState({ selectedArchivedTestDetails: data }, () => {
      this.setState({
        triggerArchivedTestDetailsPopup: !this.state.triggerArchivedTestDetailsPopup
      });
    });
  };

  render() {
    const TABS = this.getTABS();

    return (
      <div>
        <h2>{LOCALIZE.myTests.sideNavTitle}</h2>
        <div style={styles.alertMessageContainer}>
          <AlertMessage
            alertType={ALERT_TYPE.light}
            message={
              <>
                <p>{LOCALIZE.myTests.alertCatTestsOnly1}</p>
                <p>{LOCALIZE.myTests.alertCatTestsOnly2}</p>
              </>
            }
            usesMarkdownOrHtmlTags={false}
          />
        </div>
        <Tabs
          defaultActiveKey="valid-tests"
          id="my-tests-tabs"
          style={styles.tabNavigation}
          // onSelect={this.handleTabChange}
        >
          {TABS.map((tab, index) => {
            return (
              <Tab key={index} eventKey={tab.key} title={tab.tabName} style={styles.tabContainer}>
                {tab.body}
              </Tab>
            );
          })}
        </Tabs>
      </div>
    );
  }
}
export { MyTests as UnconnectedMyTests };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getValidScoredTests,
      getArchivedScoredTests
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(MyTests);
