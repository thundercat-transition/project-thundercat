import React, { Component } from "react";
import LOCALIZE from "./text_resources";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import updateCheckInRoom from "./modules/UpdateCheckInRoomRedux";
import CustomButton from "./components/commons/CustomButton";
import THEME from "./components/commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { getLineSpacingCSS } from "./modules/AccommodationsRedux";
import { Row, Col, Button } from "react-bootstrap";
import { PATH } from "./components/commons/Constants";
import SystemMessage, { MESSAGE_TYPE } from "./components/commons/SystemMessage";
import StyledTooltip, { TYPE, EFFECT } from "./components/authentication/StyledTooltip";
import { setMyProfileSideNavigationSelectedItem } from "./modules/UserProfileRedux";
import { history } from "./store-index";

const columnSizes = {
  labelTooltipColumn: {
    xs: 6,
    sm: 6,
    md: 4,
    lg: 4,
    xl: 4
  },
  inputColumn: {
    xs: 6,
    sm: 6,
    md: 4,
    lg: 4,
    xl: 4
  },
  buttonColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 4
  }
};

const styles = {
  checkInBtnContainer: {
    textAlign: "center"
  },
  checkInBtn: {
    minWidth: 250
  },
  textInput: {
    width: "100%",
    borderRadius: 4,
    minHeight: 32
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  row: {
    padding: "15px 40px 15px 40px"
  },
  uncompletedProfileDescription: {
    marginTop: 18
  },
  profileLink: {
    textDecoration: "underline",
    color: "#0278A4",
    cursor: "pointer",
    backgroundColor: "transparent",
    border: "none",
    padding: 0
  },
  testAccessCodeLabel: {
    fontWeight: "bold",
    textAlign: "right"
  },
  testAccessCodeAndDisplayContainer: {
    margin: "18px 0 12px 0",
    width: "100%"
  },
  tooltipButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e",
    transform: "scale(1.2)"
  },
  tooltipIcon: {
    color: "#00565e"
  },
  useCodeButtonContainer: {
    fontWeight: "bold",
    textAlign: "left"
  },
  label: {
    paddingRight: 6
  },
  inputContainer: {
    marginTop: 2
  }
};

class CandidateCheckIn extends Component {
  static propTypes = {
    // Props from Redux
    updateCheckInRoom: PropTypes.func
  };

  state = {
    testAccessCode: "",
    isInvalidTestAccessCode: false,
    testAccessCodeAlreadyCheckedIn: false,
    currentTestAlreadyInActiveState: false
  };

  handleCheckin = () => {
    const { testAccessCode } = this.state;

    this.props.updateCheckInRoom(testAccessCode).then(response => {
      if (response.ok) {
        this.setState({
          ...this.state,
          testAccessCode: "",
          isInvalidTestAccessCode: false,
          testAccessCodeAlreadyCheckedIn: false,
          currentTestAlreadyInActiveState: false
        });
        this.props.handleCheckIn(testAccessCode);
      } else if (
        response.error ===
        "This room has already been checked in using the provided test access code."
      ) {
        this.setState(
          {
            ...this.state,
            testAccessCodeAlreadyCheckedIn: true,
            isInvalidTestAccessCode: false,
            currentTestAlreadyInActiveState: false
          },
          () => {
            document.getElementById("test-access-code-field").focus();
          }
        );
      } else if (
        response.error === "The test you are trying to check-in is already in an active state."
      ) {
        this.setState(
          {
            ...this.state,
            testAccessCodeAlreadyCheckedIn: false,
            isInvalidTestAccessCode: false,
            currentTestAlreadyInActiveState: true
          },
          () => {
            document.getElementById("test-access-code-field").focus();
          }
        );
      } else {
        this.setState(
          {
            ...this.state,
            isInvalidTestAccessCode: true,
            testAccessCodeAlreadyCheckedIn: false,
            currentTestAlreadyInActiveState: false
          },
          () => {
            document.getElementById("test-access-code-field").focus();
          }
        );
      }
    });
  };

  handleUserProfileRedirect = () => {
    // setting My Profile side navigation item redux states
    this.props.setMyProfileSideNavigationSelectedItem("profile-personal-info");
    // redirecting user to profile page
    setTimeout(() => {
      history.push(PATH.profile);
    }, 100);
  };

  getTestAccessCode = event => {
    this.setState({ testAccessCode: event.target.value.toUpperCase() });
  };

  handleKeyDown = event => {
    // if key pressed is "Enter"
    if (event.key === "Enter") {
      // handling check-in action
      this.handleCheckin();
    }
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();

    // if quality of life is set, override profile complete
    let { profileCompleted } = this.props;
    const { qualityOfLife } = this.props;
    if (qualityOfLife) {
      profileCompleted = true;
    }
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }
    return (
      <div>
        {profileCompleted ? (
          <div data-testid="unit-test-profile-completed" className="d-flex justify-content-center">
            <Row
              className={
                !this.state.isInvalidTestAccessCode &&
                !this.state.testAccessCodeAlreadyCheckedIn &&
                !this.state.currentTestAlreadyInActiveState
                  ? "align-items-center align-items-start justify-content-center"
                  : "align-items-start justify-content-center"
              }
              style={styles.testAccessCodeAndDisplayContainer}
            >
              <Col
                xl={columnSizes.labelTooltipColumn.xl}
                lg={columnSizes.labelTooltipColumn.lg}
                md={columnSizes.labelTooltipColumn.md}
                sm={columnSizes.labelTooltipColumn.sm}
                xs={columnSizes.labelTooltipColumn.xs}
                style={styles.testAccessCodeLabel}
              >
                <Row className="align-items-center justify-content-center">
                  <label
                    htmlFor={`test-access-code-field`}
                    id={`test-access-code-field-title`}
                    style={styles.label}
                  >
                    {LOCALIZE.candidateCheckIn.page.textLabel}
                  </label>
                  <StyledTooltip
                    id="test-access-code-tooltip"
                    place="left"
                    variant={TYPE.light}
                    effect={EFFECT.solid}
                    openOnClick={false}
                    tooltipElement={
                      <Button
                        data-tip=""
                        data-for="test-access-code-tooltip"
                        tabIndex="-1"
                        aria-label={LOCALIZE.ariaLabel.emailResponseTooltip}
                        style={styles.tooltipButton}
                        variant="link"
                      >
                        <FontAwesomeIcon
                          icon={faQuestionCircle}
                          style={styles.tooltipIcon}
                        ></FontAwesomeIcon>
                      </Button>
                    }
                    tooltipContent={<div>{LOCALIZE.candidateCheckIn.page.takeTestTooltip}</div>}
                  />
                </Row>
              </Col>
              <Col
                xl={columnSizes.inputColumn.xl}
                lg={columnSizes.inputColumn.lg}
                md={columnSizes.inputColumn.md}
                sm={columnSizes.inputColumn.sm}
                xs={columnSizes.inputColumn.xs}
                style={styles.inputContainer}
              >
                <Row className="align-items-center justify-content-center">
                  <div className="input-group">
                    <input
                      id={`test-access-code-field`}
                      className={
                        this.state.isInvalidTestAccessCode ||
                        this.state.testAccessCodeAlreadyCheckedIn ||
                        this.state.currentTestAlreadyInActiveState
                          ? "invalid-field"
                          : "valid-field"
                      }
                      aria-labelledby={`test-access-code-title`}
                      style={{ ...styles.textInput, ...accommodationsStyle }}
                      type="text"
                      value={this.state.testAccessCode}
                      onChange={this.getTestAccessCode}
                      onKeyDown={this.handleKeyDown}
                    ></input>
                  </div>
                </Row>
                <Row className="align-items-center justify-content-center">
                  {this.state.isInvalidTestAccessCode && (
                    <label
                      id="invalid-test-access-error"
                      htmlFor="test-access-code-field"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.candidateCheckIn.page.textLabelError}
                    </label>
                  )}
                  {this.state.testAccessCodeAlreadyCheckedIn && (
                    <label
                      id="test-access-code-already-checked-in-error"
                      htmlFor="test-access-code-field"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.candidateCheckIn.page.testAccessCodeAlreadyUsedError}
                    </label>
                  )}
                  {this.state.currentTestAlreadyInActiveState && (
                    <label
                      id="test-already-in-active-state-error"
                      htmlFor="test-access-code-field"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.candidateCheckIn.page.currentTestAlreadyInActiveStateError}
                    </label>
                  )}
                </Row>
              </Col>
              <Col
                xl={columnSizes.buttonColumn.xl}
                lg={columnSizes.buttonColumn.lg}
                md={columnSizes.buttonColumn.md}
                sm={columnSizes.buttonColumn.sm}
                xs={columnSizes.buttonColumn.xs}
                style={styles.useCodeButtonContainer}
              >
                <div
                  style={styles.checkInBtnContainer}
                  data-testid="unit-test-check-in-button"
                  className="notranslate"
                >
                  <CustomButton
                    label={
                      <>
                        <FontAwesomeIcon icon={faCheck} />
                        <span
                          id="candidate-check-in"
                          style={styles.buttonLabel}
                          className="notranslate"
                        >
                          {LOCALIZE.candidateCheckIn.button}
                        </span>
                      </>
                    }
                    customStyle={styles.checkInBtn}
                    action={this.handleCheckin}
                    buttonTheme={THEME.PRIMARY}
                  />
                </div>
              </Col>
            </Row>
          </div>
        ) : (
          <Row
            className="align-items-start justify-content-center"
            style={styles.testAccessCodeAndDisplayContainer}
          >
            <SystemMessage
              messageType={MESSAGE_TYPE.info}
              title={LOCALIZE.commons.info}
              message={
                <p className="notranslate">
                  {LOCALIZE.candidateCheckIn.page.incompletedProfileSystemMessage}
                </p>
              }
            />
            <p style={styles.uncompletedProfileDescription} className="notranslate">
              {LOCALIZE.formatString(
                LOCALIZE.candidateCheckIn.page.incompletedProfileDescription,
                <button
                  aria-label={LOCALIZE.candidateCheckIn.page.incompletedProfileLink}
                  tabIndex="0"
                  onClick={this.handleUserProfileRedirect}
                  style={styles.profileLink}
                >
                  {LOCALIZE.candidateCheckIn.page.incompletedProfileLink}
                </button>
              )}
            </p>
          </Row>
        )}
      </div>
    );
  }
}

export { CandidateCheckIn as UnconnectedCandidateCheckIn };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    profileCompleted: state.user.profileCompleted,
    qualityOfLife: state.siteAdminSettings.qualityOfLife
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateCheckInRoom,
      setMyProfileSideNavigationSelectedItem
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CandidateCheckIn);
