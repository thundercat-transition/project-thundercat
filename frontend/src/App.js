import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Route, Navigate, Routes } from "react-router-dom";
import {
  authenticateAction,
  logoutAction,
  isTokenStillValid,
  refreshAuthToken
} from "./modules/LoginRedux";
import { updateAssignedTestId, getAssignedTests } from "./modules/AssignedTestsRedux";
import "./css/lib/aurora.min.css";
import "./css/cat-theme.css";
import { Helmet } from "react-helmet";
import Status from "./Status";
import Home from "./Home";
import TestAdministration from "./components/ta/TestAdministration";
import SelectLanguage from "./SelectLanguage";
import SiteNavBar from "./SiteNavBar";
import { PATH, URL_PREFIX } from "./components/commons/Constants";

import SampleTestsRoutes from "./components/samples/SampleTestRoutes";
import {
  getUserPermissions,
  updateCurrentHomePageState,
  updatePermissionsState,
  resetPermissionsState
} from "./modules/PermissionsRedux";
import { resetUserState } from "./modules/UserRedux";
import PrivateRoute from "./components/commons/PrivateRoute";
import Dashboard from "./Dashboard";
import Profile from "./components/profile/Profile";
import { PERMISSION } from "./components/profile/Constants";
// import IncidentReport from "./IncidentReport";
// import ContactUs from "./ContactUs";
import SystemAdministration from "./components/etta/SystemAdministration";
import PpcAdministration from "./PpcAdministration";
import InPersonTesting from "./components/inPersonTesting/InPersonTesting";
import { resetTestStatusState } from "./modules/TestStatusRedux";
import { resetNotepadState } from "./modules/NotepadRedux";
import { resetTestStatusState as resetSampleTestStatusState } from "./modules/SampleTestStatusRedux";

import { resetAccommodations, getLineSpacingCSS } from "./modules/AccommodationsRedux";

import ExpiredTokenPopup from "./ExpiredTokenPopup";
import TestLanding from "./components/testFactory/TestLanding";
import SessionStorage, { ACTION, ITEM } from "./SessionStorage";
import TEST_STATUS from "./components/ta/Constants";
import TestBuilder from "./components/testBuilder/TestBuilder";
import TestBuilderTestPage from "./components/testFactory/TestBuilderTestPage";
import SelectedItemBank from "./components/etta/rd_item_banks/selected_item_bank/SelectedItemBank";
import ItemBankEditor from "./components/testBuilder/itemBank/ItemBankEditor";
import Accommodations from "./Accommodations";
import ResetPasswordPage from "./ResetPasswordPage";
import InvalidateTestScreen from "./components/commons/InvalidateTestScreen";
import UserLookUpDetails from "./components/etta/UserLookUpDetails";
import { resetUserLookUpState } from "./modules/UserLookUpRedux";
import SelectedOrderlessTestAdministratorDetails from "./components/etta/test_accesses/SelectedOrderlessTestAdministratorDetails";
import ItemEditor from "./components/testBuilder/itemBank/items/ItemEditor";
import BundleEditor from "./components/testBuilder/itemBank/bundles/BundleEditor";
import SuperuserDashboard from "./components/superuser/SuperuserDashboard";
import TestCenterManager from "./components/testCenterManager/TestCenterManager";
import SelectedTestCenter from "./components/etta/test_center_administration/selected_test_center/SelectedTestCenter";
import TestCenterData from "./components/testCenterManager/TestCenterData";
import AaeDashboard from "./components/aae/AaeDashboard";
import ConsultationServicesDashboard from "./components/consultationServices/ConsultationServicesDashboard";
import SelectedUserAccommodationFile from "./components/aae/SelectedUserAccommodationFile";
import ScorerDashboard from "./components/scorer/ScorerDashboard";
import { history } from "./store-index";
import getAndSetUserPermissions from "./helpers/loginUtils";
import { resetUserProfileStates } from "./modules/UserProfileRedux";
import { resetErrorStatusState } from "./modules/ErrorStatusRedux";
import { resetAllItemBanksStates } from "./modules/ItemBankRedux";
import { resetActiveItemBanksState } from "./modules/RDItemBankRedux";
import { resetTestAdministrationStates } from "./modules/TestAdministrationRedux";
import { resetTestCenterStates } from "./modules/TestCenterRedux";
import { resetAaeStates } from "./modules/AaeRedux";
import { resetConsultationServicesStates } from "./modules/ConsultationServicesRedux";
import { resetScorerStates } from "./modules/ScorerRedux";
import { setQualityOfLife, getEnableQualityofLife } from "./modules/SiteAdminSettingsRedux";

// global variables
const TOKEN_RELATED_POLLING_TIMER = 60000; // 60 seconds
let ALIVE_COUNTDOWN;

const App = props => {
  // States
  const [showTokenExpiredDialog, setShowTokenExpiredDialog] = useState(false);

  const handleExpiredTokenAction = () => {
    // display token expired popup
    setShowTokenExpiredDialog(true);

    // reset all states in case of token expiration
    props.resetTestStatusState();
    props.resetSampleTestStatusState();
    props.resetAccommodations();
    props.resetNotepadState();
    props.resetUserState();
    props.resetUserProfileStates();
    props.resetErrorStatusState();
    props.resetUserLookUpState();
    props.resetAllItemBanksStates();
    props.resetPermissionsState();
    props.resetActiveItemBanksState();
    props.resetTestAdministrationStates();
    props.resetTestCenterStates();
    props.resetAaeStates();
    props.resetConsultationServicesStates();
    props.resetScorerStates();
    props.logoutAction();
    // redirecting user to login page
    history.push(PATH.login);
  };

  const closePopup = () => {
    setShowTokenExpiredDialog(false);
    // reloading page to make sure that all states are reseted
    window.location.reload();
  };

  // validating user auth token
  const validateToken = async () => {
    const token = SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN);
    // token exists
    if (token !== null && props.authenticated) {
      props.isTokenStillValid().then(tokenIsValid => {
        // token is no longer valid
        if (!tokenIsValid) {
          // kick the user out (inactivity)
          handleExpiredTokenAction();
        }
      });
      // token does not exist
    } else {
      // user is still authenticated
      if (props.authenticated) {
        // kick the user out (inactivity)
        handleExpiredTokenAction();
      }
    }
  };

  const refreshToken = async () => {
    props.isTokenStillValid().then(tokenIsValid => {
      // token is still valid
      if (tokenIsValid) {
        // refreshing token
        props
          .refreshAuthToken()
          .then(response => {
            // removing the old token from the local storage
            SessionStorage(ACTION.REMOVE, ITEM.AUTH_TOKEN);
            SessionStorage(ACTION.REMOVE, ITEM.REFRESH_TOKEN);

            // updating the local storage with the new refreshed token
            SessionStorage(ACTION.SET, ITEM.AUTH_TOKEN, response.access);
            SessionStorage(ACTION.SET, ITEM.REFRESH_TOKEN, response.refresh);
          })
          .then(() => {
            // resetting ALIVE_COUNTDOWN
            clearInterval(ALIVE_COUNTDOWN);
            const aliveInterval = setInterval(validateToken, TOKEN_RELATED_POLLING_TIMER);
            ALIVE_COUNTDOWN = aliveInterval;
          });
        // token is no longer valid
      } else {
        // kick the user out (inactivity)
        handleExpiredTokenAction();
      }
    });
  };

  // equivalent of componentDidMount
  useEffect(() => {
    // getting the authentication token from the local storage
    const token = SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN);

    // if there is no token, then there is no point in trying to verify it
    if (token === undefined) {
      // update authenticated state to false
      props.authenticateAction(false);
    }

    // if authenticated
    if (props.authenticated) {
      // getting and setting user permissions
      getAndSetUserPermissions(
        props.getUserPermissions,
        props.updatePermissionsState,
        props.updateCurrentHomePageState,
        props.isSuperUser
      );
    }
    props.getEnableQualityofLife().then(response => props.setQualityOfLife(response.is_active));
  }, []);

  // if authenticated gets updated (equivalent of componentDidUpdate)
  useEffect(() => {
    // user is authenticated
    if (props.authenticated) {
      // getting and handling assigned tests
      props.getAssignedTests(props.currentLanguage).then(assigned_tests => {
        // check for each assigned test
        for (let i = 0; i < assigned_tests.length; i++) {
          // if there is an active test (active, locked or paused)
          if (
            assigned_tests[i].status_codename === TEST_STATUS.ACTIVE ||
            assigned_tests[i].status_codename === TEST_STATUS.LOCKED ||
            assigned_tests[i].status_codename === TEST_STATUS.PAUSED
          ) {
            // setting/updating needed redux states
            props.updateAssignedTestId(
              assigned_tests[i].id,
              assigned_tests[i].test_id,
              assigned_tests[i].accommodation_request_id,
              assigned_tests[i].user_accommodation_file_id
            );
            // redirect to test page
            history.push(PATH.testBase);
          }
        }
      });

      // validating the token
      validateToken();

      // create polling function for the validateToken logic
      // ===================================================
      if (ALIVE_COUNTDOWN) {
        clearInterval(ALIVE_COUNTDOWN);
      }
      // called every minute
      const aliveInterval = setInterval(validateToken, TOKEN_RELATED_POLLING_TIMER);
      ALIVE_COUNTDOWN = aliveInterval;
      // user is not authenticated
    } else {
      // clearing the ALIVE_COUNTDOWN interval, so the validate token function is no longer called (polling function related)
      clearInterval(ALIVE_COUNTDOWN);
    }
    // ===================================================
  }, [props.authenticated]);

  // if triggerAppRerender gets updated (equivalent of componentDidUpdate)
  useEffect(() => {
    // trigger re-render when triggerAppRerender() is being called
  }, [props.triggerAppRerender]);

  // equivalent of componentWillUnmount
  useEffect(() => {
    return () => {
      clearInterval(ALIVE_COUNTDOWN);
    };
  }, []);

  let accommodationStyles = {
    fontFamily: props.accommodations.fontFamily,
    fontSize: props.accommodations.fontSize
  };
  if (props.accommodations.spacing) {
    accommodationStyles = {
      ...accommodationStyles,
      ...getLineSpacingCSS()
    };
  }

  // Determine if user has already selected a language. Based on local storage, not props
  const isLanguageSelected = SessionStorage(ACTION.GET, ITEM.CAT_LANGUAGE);

  return (
    <div
      className="notranslate"
      id="AppRoot"
      style={
        window.location.href.includes(URL_PREFIX.itemBank)
          ? { ...accommodationStyles, ...{ height: "100vh", overflow: "hidden" } }
          : accommodationStyles
      }
      // if authenticated, calling the refreshToken
      // if not authenticated, clearing the ALIVE_COUNTDOWN interval, so the validate token function is no longer called (polling function related)
      onBlur={props.authenticated ? refreshToken : clearInterval(ALIVE_COUNTDOWN)}
      translate="no"
    >
      {!isLanguageSelected && <SelectLanguage />}
      {isLanguageSelected && (
        <div>
          <Helmet>
            <html lang={props.currentLanguage} translate="no" />
            <meta name="google" content="notranslate" />
          </Helmet>
          <div>
            <Accommodations accommodations={props.accommodations} />
            <div>
              {!props.isTestActive && !props.isTestPreTestOrTransition && <SiteNavBar />}
              <Routes>
                <Route
                  path={PATH.login}
                  element={
                    <PrivateRoute
                      auth={!props.authenticated}
                      path={PATH.login}
                      component={Home}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />
                <Route path={PATH.status} element={<Status />} />
                <Route path={`${PATH.sampleTests}/*`} element={<SampleTestsRoutes />} />
                <Route path={PATH.resetPassword} element={<ResetPasswordPage />} />
                <Route
                  auth={props.authenticated}
                  path={PATH.testBuilderTest}
                  element={<TestBuilderTestPage />}
                />
                <Route
                  auth={props.authenticated}
                  path={`${PATH.testBase}/*`}
                  element={<TestLanding />}
                />

                <Route
                  path={PATH.testAdministration}
                  element={
                    <PrivateRoute
                      auth={props.isTa && props.authenticated}
                      path={PATH.testAdministration}
                      component={TestAdministration}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.systemAdministration}
                  element={
                    <PrivateRoute
                      auth={(props.isEtta || props.isRdOperations) && props.authenticated}
                      path={PATH.systemAdministration}
                      component={SystemAdministration}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.ppcAdministration}
                  element={
                    <PrivateRoute
                      auth={props.isPpc && props.authenticated}
                      path={PATH.ppcAdministration}
                      component={PpcAdministration}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.testCenterManager}
                  element={
                    <PrivateRoute
                      auth={props.isTcm && props.authenticated}
                      path={PATH.testCenterManager}
                      component={TestCenterManager}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.testCenterManagement}
                  element={
                    <PrivateRoute
                      auth={props.isTcm && props.authenticated}
                      path={PATH.testCenterManagement}
                      component={TestCenterData}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.testCenterAccesses}
                  element={
                    <PrivateRoute
                      auth={props.isEtta && props.authenticated}
                      path={PATH.testCenterAccesses}
                      component={SelectedTestCenter}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.scorerBase}
                  element={
                    <PrivateRoute
                      auth={props.isScorer && props.authenticated}
                      path={PATH.scorerBase}
                      component={ScorerDashboard}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.inPersonTesting}
                  element={
                    <PrivateRoute
                      auth={props.isHrCoordinator && props.authenticated}
                      path={PATH.inPersonTesting}
                      component={InPersonTesting}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.dashboard}
                  element={
                    <PrivateRoute
                      auth={props.authenticated}
                      path={PATH.dashboard}
                      component={Dashboard}
                      redirectTo={PATH.login}
                    />
                  }
                />

                <Route
                  path={PATH.profile}
                  element={
                    <PrivateRoute
                      auth={props.authenticated}
                      path={PATH.profile}
                      component={Profile}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                {/* <PrivateRoute
                      auth={props.authenticated}
                      path={PATH.incidentReport}
                      component={IncidentReport}
                      redirectTo={PATH.login}
                    /> */}
                <Route
                  path={PATH.testBuilder}
                  element={
                    <PrivateRoute
                      auth={(props.isTb || props.isTd) && props.authenticated}
                      path={PATH.testBuilder}
                      component={TestBuilder}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.itemBankAccesses}
                  element={
                    <PrivateRoute
                      auth={props.isRdOperations && props.authenticated}
                      path={PATH.itemBankAccesses}
                      component={SelectedItemBank}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.testCenterAccesses}
                  element={
                    <PrivateRoute
                      auth={props.isEtta && props.authenticated}
                      path={PATH.testCenterAccesses}
                      component={SelectedTestCenter}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.itemBankEditor}
                  element={
                    <PrivateRoute
                      auth={props.isTd && props.authenticated}
                      path={PATH.itemBankEditor}
                      component={ItemBankEditor}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.itemEditor}
                  element={
                    <PrivateRoute
                      auth={props.isTd && props.authenticated}
                      path={PATH.itemEditor}
                      component={ItemEditor}
                      redirectTo={PATH.itemBankEditor}
                    />
                  }
                />

                <Route
                  path={PATH.bundleEditor}
                  element={
                    <PrivateRoute
                      auth={props.isTd && props.authenticated}
                      path={PATH.bundleEditor}
                      component={BundleEditor}
                      redirectTo={PATH.itemBankEditor}
                    />
                  }
                />

                {/* <PrivateRoute
                      auth={props.authenticated}
                      path={PATH.contactUs}
                      component={ContactUs}
                      redirectTo={PATH.login}
                    /> */}

                <Route
                  path={PATH.invalidateTest}
                  element={
                    <PrivateRoute
                      auth={props.isTestInvalidated && props.authenticated}
                      path={PATH.invalidateTest}
                      component={InvalidateTestScreen}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.userLookUpDetails}
                  element={
                    <PrivateRoute
                      auth={props.isEtta && props.authenticated}
                      path={PATH.userLookUpDetails}
                      component={UserLookUpDetails}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.orderlessTestAdministratorDetails}
                  element={
                    <PrivateRoute
                      auth={props.isEtta && props.authenticated}
                      path={PATH.orderlessTestAdministratorDetails}
                      component={SelectedOrderlessTestAdministratorDetails}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.superuserDashboard}
                  element={
                    <PrivateRoute
                      auth={props.isSuperUser && props.authenticated}
                      path={PATH.superuserDashboard}
                      component={SuperuserDashboard}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.aae}
                  element={
                    <PrivateRoute
                      auth={(props.isAae || props.isSuperUser) && props.authenticated}
                      path={PATH.aae}
                      component={AaeDashboard}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />

                <Route
                  path={PATH.aaeSelectedUserAccommodationFile}
                  element={
                    <PrivateRoute
                      auth={(props.isAae || props.isSuperUser) && props.authenticated}
                      path={PATH.aaeSelectedUserAccommodationFile}
                      component={SelectedUserAccommodationFile}
                      redirectTo={PATH.aae}
                    />
                  }
                />

                <Route
                  path={PATH.consultationServices}
                  element={
                    <PrivateRoute
                      auth={props.isConsultationServices && props.authenticated}
                      path={PATH.consultationServices}
                      component={ConsultationServicesDashboard}
                      redirectTo={props.currentHomePage}
                    />
                  }
                />
                <Route path="*" element={<Navigate to={PATH.login} replace />} />
              </Routes>
            </div>
          </div>
        </div>
      )}
      <ExpiredTokenPopup
        showTokenExpiredDialog={showTokenExpiredDialog}
        closePopupFunction={closePopup}
      />
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    authenticated: state.login.authenticated,
    testNameId: state.testStatus.currentTestId,
    isSuperUser: state.user.isSuperUser,
    isEtta: state.userPermissions.isEtta,
    isRdOperations: state.userPermissions.isRdOperations,
    isPpc: state.userPermissions.isPpc,
    isTcm: state.userPermissions.isTcm,
    isTa: state.userPermissions.isTa,
    isScorer: state.userPermissions.isScorer,
    isTb: state.userPermissions.isTb,
    isTd: state.userPermissions.isTd,
    isHrCoordinator: state.userPermissions.isHrCoordinator,
    isAae: state.userPermissions.isAae,
    isConsultationServices: state.userPermissions.isConsultationServices,
    currentHomePage: state.userPermissions.currentHomePage,
    username: state.user.username,
    activeTestPath: state.testStatus.activeTestPath,
    accommodations: state.accommodations,
    isTestInvalidated: state.testStatus.isTestInvalidated,
    triggerAppRerender: state.app.triggerAppRerender,
    isTestActive: state.testStatus.isTestActive,
    isTestPreTestOrTransition: state.testStatus.isTestPreTestOrTransition
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      authenticateAction,
      logoutAction,
      refreshAuthToken,
      getUserPermissions,
      updatePermissionsState,
      resetTestStatusState,
      resetSampleTestStatusState,
      resetNotepadState,
      resetPermissionsState,
      isTokenStillValid,
      getAssignedTests,
      updateCurrentHomePageState,
      resetAccommodations,
      resetUserState,
      updateAssignedTestId,
      resetUserLookUpState,
      resetUserProfileStates,
      resetErrorStatusState,
      resetAllItemBanksStates,
      resetActiveItemBanksState,
      resetTestAdministrationStates,
      resetTestCenterStates,
      resetAaeStates,
      resetConsultationServicesStates,
      resetScorerStates,
      setQualityOfLife,
      getEnableQualityofLife
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(App);
