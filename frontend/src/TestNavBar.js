import React, { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "./css/lib/aurora.min.css";
import "./css/cat-theme.css";
import Settings from "./components/commons/Settings";
import Translation from "./components/commons/Translation";
import LOCALIZE from "./text_resources";
import psc_logo_light_en from "./images/psc_logo_light_en.png";
import psc_logo_light_fr from "./images/psc_logo_light_fr.png";
import { Navbar, Nav, Row } from "react-bootstrap";
import QuitTest from "./components/commons/QuitTest";
import SelectLanguage from "./SelectLanguage";
import { LANGUAGES } from "./modules/LocalizeRedux";
import { PATH } from "./components/commons/Constants";
import { setNavBarHeight } from "./modules/TestSectionRedux";
import ToolsButton from "./components/commons/ToolsButton";
import CustomButton from "./components/commons/CustomButton";
import THEME from "./components/commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import ReturnHome from "./components/commons/ReturnHome";

const styles = {
  navBar: {
    position: "unset"
  },
  row: {
    padding: "10px 0 10px 0"
  },
  col: {
    padding: "0 10px 0 10px"
  },
  iconButton: {
    marginRight: 6
  }
};

function TestNavBar(props) {
  const { showQuitTestButton = false, showReturnHomeButton = false } = props;

  const testNavBarHeightRef = useRef(null);

  const [focusOnQuitTestButton, setFocusOnQuitTestButton] = useState(false);

  // Equivalent of componentDidMount
  useEffect(() => {
    // updating nav bar height redux state
    if (testNavBarHeightRef.current) {
      props.setNavBarHeight(testNavBarHeightRef.current.clientHeight);
    }
  }, []);

  // Equivalent of componentDidUpdate
  useEffect(() => {
    // updating nav bar height redux state
    if (testNavBarHeightRef.current) {
      props.setNavBarHeight(testNavBarHeightRef.current.clientHeight);
    }
  }, [testNavBarHeightRef]);

  const getNavBar = () => {
    return (
      <Navbar
        ref={testNavBarHeightRef}
        style={styles.navBar}
        bg="dark"
        fixed="top"
        variant="dark"
        expand={"xl"}
      >
        <Navbar.Brand>
          <img
            alt=""
            src={props.currentLanguage === LANGUAGES.french ? psc_logo_light_fr : psc_logo_light_en}
            width="370px"
            height="27.75px"
            className="d-inline-block align-top"
          />
        </Navbar.Brand>
        <Navbar.Toggle
          label={LOCALIZE.ariaLabel.navExpandButton}
          aria-controls="basic-navbar-nav"
          aria-label={LOCALIZE.ariaLabel.navExpandButton}
          style={{
            fontSize: props.accommodations.fontSize
          }}
        >
          <CustomButton
            label={
              <div>
                <FontAwesomeIcon icon={faBars} />
              </div>
            }
            buttonTheme={
              props.isTestActive || props.isTestInvalidated
                ? THEME.PRIMARY_IN_TEST
                : THEME.SECONDARY
            }
            customStyle={{
              marginRight: "16px",
              minWidth: 0
            }}
          />
        </Navbar.Toggle>
        <Navbar.Collapse id="basic-navbar-nav">
          <Row className="justify-content-end w-100" style={styles.row}>
            <Nav className="mr-auto" />
            {showReturnHomeButton && (
              <ReturnHome
                id="unit-test-return-home-navbar-button"
                handleReturnHome={props.handleReturnHome}
              />
            )}
            {showQuitTestButton && (
              <QuitTest
                id="unit-test-quit-test-navbar-button"
                setFocusOnQuitTestButton={focusOnQuitTestButton}
                handleQuitTest={props.handleQuitTest}
              />
            )}
            <ToolsButton calledInRealTest={true} />
            <Settings
              isTestActive={props.isTestActive}
              isTestInvalidated={props.isTestInvalidated}
              id="unit-test-settings-navbar-button"
            />
            <Translation
              isTestActive={props.isTestActive}
              isTestInvalidated={props.isTestInvalidated}
              id="unit-test-translation-navbar-button"
            />
          </Row>
        </Navbar.Collapse>
      </Navbar>
    );
  };

  // Determine if user has already selected a language.
  const isLanguageSelected = props.currentLanguage !== "";
  return (
    <div data-testid="test-nav-bar-component">
      <a href="#main-content" className="visually-hidden" id="skip-to-main-content-link-id">
        {LOCALIZE.mainTabs.skipToMain}
      </a>
      {!isLanguageSelected && <SelectLanguage />}
      {isLanguageSelected && window.location.pathname.indexOf(PATH.sampleTests) === -1 && (
        <div>
          <div>
            {/* Frontend Testing -> testFnGetNavBar */}
            {!props.testFnGetNavBar ? getNavBar() : props.testFnGetNavBar()}
          </div>
        </div>
      )}
    </div>
  );
}

export { TestNavBar as UnconnectedTestNavBar };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    isTestActive: state.testStatus.isTestActive,
    isTestPreTestOrTransition: state.testStatus.isTestPreTestOrTransition,
    authenticated: state.login.authenticated,
    isTestInvalidated: state.testStatus.isTestInvalidated,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setNavBarHeight
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestNavBar);
