import React, { Component } from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import LOCALIZE from "./text_resources";
import ContentContainer from "./components/commons/ContentContainer";
import AuthenticationTabs from "./components/authentication/AuthenticationTabs";
import {
  updatePageHasErrorState,
  updateShow2FA,
  authenticateAction,
  getUserInformation
} from "./modules/LoginRedux";
import { getActiveSystemAlertsToBeDisplayedOnHomePage } from "./modules/SystemAlertsRedux";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import AlertMessage from "./components/commons/AlertMessage";
import getCriticalityDataForSystemAlert from "./helpers/criticality";
import { PATH } from "./components/commons/Constants";
import { PERMISSION } from "./components/profile/Constants";
import {
  getUserPermissions,
  updatePermissionsState,
  updateCurrentHomePageState
} from "./modules/PermissionsRedux";
import { setUserInformation } from "./modules/UserRedux";
import usernameFormatter from "./helpers/usernameFormatter";
import getAndSetUserPermissions, { getAndSetUserInfo, setReduxStates } from "./helpers/loginUtils";
import { history } from "./store-index";
import { setTestDefinitionSelectionSideNavState } from "./modules/TestBuilderRedux";
import { setBOUserSideNavState } from "./modules/UserLookUpRedux";

export const styles = {
  appPadding: {
    padding: "15px"
  },
  paragraph: {
    paddingBottom: "15px"
  },
  systemAlertsContainer: {
    maxWidth: "75%",
    margin: "0 auto"
  }
};

class Home extends Component {
  static propTypes = {
    // Props from Redux
    authenticated: PropTypes.bool,
    updatePageHasErrorState: PropTypes.func
  };

  state = {
    activeSystemAlerts: []
  };

  // updating pageHasError state to 'false' on render, so the error isn't displayed on refresh
  componentDidMount = () => {
    this.props.updatePageHasErrorState(false);
    this.handleSystemAlerts();
  };

  handleSystemAlerts = () => {
    // getting all active system alerts that need to be displayed at the moment
    this.props.getActiveSystemAlertsToBeDisplayedOnHomePage().then(response => {
      this.setState({ activeSystemAlerts: response });
    });
  };

  // TODO
  handleGetUserPermissionsLogic = () => {
    // getting user permissions
    this.props
      .getUserPermissions()
      .then(response => {
        // initializing permission flags
        const { isSuperUser } = this.props;
        let isEtta = false;
        let isPpc = false;
        let isTa = false;
        let isScorer = false;
        let isTb = false;
        let isAae = false;
        let isRdOperations = false;
        let isTd = false;
        let isTcm = false;
        let isHrCoordinator = false;
        let isConsultationServices = false;
        // specified user is a super user
        if (isSuperUser) {
          // setting all permission flags to true
          isEtta = true;
          isPpc = true;
          isTa = true;
          isScorer = true;
          isTb = true;
          isAae = true;
          isRdOperations = true;
          isTd = true;
          isTcm = true;
          isHrCoordinator = true;
          isConsultationServices = true;
          // // specified user is not a super user
        } else {
          // looping in user permissions to update the needed permission flags
          for (let i = 0; i < response.length; i++) {
            if (response[i].codename === PERMISSION.systemAdministrator) {
              isEtta = true;
            } else if (response[i].codename === PERMISSION.ppcAdministrator) {
              isPpc = true;
            } else if (response[i].codename === PERMISSION.scorer) {
              isScorer = true;
            } else if (response[i].codename === PERMISSION.testAdministrator) {
              isTa = true;
            } else if (response[i].codename === PERMISSION.testBuilder) {
              isTb = true;
            } else if (response[i].codename === PERMISSION.aae) {
              isAae = true;
            } else if (response[i].codename === PERMISSION.rdOperations) {
              isRdOperations = true;
            } else if (response[i].codename === PERMISSION.testDeveloper) {
              isTd = true;
            } else if (response[i].codename === PERMISSION.testCenterManager) {
              isTcm = true;
            } else if (response[i].codename === PERMISSION.hrCoordinator) {
              isHrCoordinator = true;
            } else if (response[i].codename === PERMISSION.consultationServices) {
              isConsultationServices = true;
            }
          }
        }
        // update permissions states based on the user permission flags
        this.props.updatePermissionsState({
          isEtta: isEtta,
          isPpc: isPpc,
          isTa: isTa,
          isScorer: isScorer,
          isTb: isTb,
          isAae: isAae,
          isRdOperations: isRdOperations,
          isTd: isTd,
          isTcm: isTcm,
          isHrCoordinator: isHrCoordinator,
          isConsultationServices: isConsultationServices
        });
        // update the current home page state based on the permissions
        // is super user or ETTA
        if (isEtta || isRdOperations || isSuperUser) {
          this.props.updateCurrentHomePageState(PATH.systemAdministration);
          // is PPC
        } else if (isPpc) {
          this.props.updateCurrentHomePageState(PATH.ppcAdministration);
          // is scorer
        } else if (isScorer) {
          this.props.updateCurrentHomePageState(PATH.scorerBase);
          // is TA
        } else if (isTa) {
          this.props.updateCurrentHomePageState(PATH.testAdministration);
          // is Test Builder
        } else if (isTb) {
          this.props.updateCurrentHomePageState(PATH.testBuilder);
          // is Test Developer
        } else if (isTd) {
          this.props.updateCurrentHomePageState(PATH.testBuilder);
          // is Test Center Manager
        } else if (isTcm) {
          this.props.updateCurrentHomePageState(PATH.testCenterManager);
          // is HR Coordinator
        } else if (isHrCoordinator) {
          this.props.updateCurrentHomePageState(PATH.inPersonTesting);
          // is Consultation Services
        } else if (isConsultationServices) {
          this.props.updateCurrentHomePageState(PATH.consultationServices);
          // is candidate
        } else {
          this.props.updateCurrentHomePageState(PATH.dashboard);
        }
      })
      .then(() => {
        // This line is necessary to avoid visual redirect to the "login" page before entering the dashboard
        this.props.updateShow2FA(false);
        // will also trigger the redirect to the dynamic home page
        this.props.authenticateAction(true);
      });
  };

  authorizeAfter2FA = () => {
    // getting and setting user info
    getAndSetUserInfo(this.props.getUserInformation, this.props.setUserInformation).then(() => {
      // updating/setting the right redux states
      setReduxStates(
        this.props.updatePageHasErrorState,
        this.props.setTestDefinitionSelectionSideNavState,
        this.props.setBOUserSideNavState
      ).then(() => {
        // getting and setting user permissions
        getAndSetUserPermissions(
          this.props.getUserPermissions,
          this.props.updatePermissionsState,
          this.props.updateCurrentHomePageState,
          this.props.isSuperUser
        )
          .then(() => {
            // updating authenticated redux state to true
            this.props.authenticateAction(true);
          })
          .then(() => {
            // redirecting user to dynamic home page
            history.push(this.props.currentHomePage);
          });
      });
    });
  };

  render() {
    return (
      <div className="app" style={styles.appPadding}>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          {!this.props.pageHasError && (
            <title className="notranslate">{LOCALIZE.titles.home}</title>
          )}
          {this.props.pageHasError && (
            <title className="notranslate">{LOCALIZE.titles.homeWithError}</title>
          )}
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main" tabIndex={0} className="notranslate">
            <h1>{LOCALIZE.homePage.welcomeMsg}</h1>
            <p style={styles.paragraph}>
              {this.props.show2FA
                ? LOCALIZE.homePage.description2fa
                : LOCALIZE.homePage.description}
            </p>
            <div style={styles.systemAlertsContainer}>
              {this.state.activeSystemAlerts.map(systemAlertData => {
                const alertType = getCriticalityDataForSystemAlert(
                  systemAlertData.criticality_data.codename
                );
                return (
                  <AlertMessage
                    alertType={alertType}
                    message={systemAlertData[`message_text_${this.props.currentLanguage}`]}
                  />
                );
              })}
            </div>
            <div>
              <AuthenticationTabs authorizeAfter2FA={this.authorizeAfter2FA} />
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { Home as UnconnectedHome };
const mapStateToProps = (state, ownProps) => {
  return {
    authenticated: state.login.authenticated,
    pageHasError: state.login.pageHasError,
    currentLanguage: state.localize.language,
    show2FA: state.login.show2FA,
    isSuperUser: state.user.isSuperUser,
    currentHomePage: state.userPermissions.currentHomePage
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updatePageHasErrorState,
      getActiveSystemAlertsToBeDisplayedOnHomePage,
      getUserPermissions,
      updatePermissionsState,
      updateCurrentHomePageState,
      updateShow2FA,
      authenticateAction,
      getUserInformation,
      setUserInformation,
      setTestDefinitionSelectionSideNavState,
      setBOUserSideNavState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Home);
