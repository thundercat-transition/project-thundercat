// format the test skill to return "SkillType" or "SkillType - SubskillType"

import LOCALIZE from "../text_resources";

// only compatible with the "_en/_fr" format at the end of the name
function getFormattedTestSkill(data, language) {
  let formattedTestSkill = `${data[`test_skill_type_name_${language}`]}`;

  // if there is a subskill
  if (data.test_skill_sub_type_id !== null) {
    formattedTestSkill = formattedTestSkill.concat(
      ` - ${data[`test_skill_sub_type_name_${language}`]}`
    );
  }

  // setting formattedTestSkill to N/A if still null at this point
  if (formattedTestSkill === null || formattedTestSkill === "null") {
    formattedTestSkill = LOCALIZE.commons.na;
  }

  return formattedTestSkill;
}

export default getFormattedTestSkill;
