// populate custom date year options (from current year to current year + 5)
function populateCustomFutureYearsDateOptions(numberOfYears) {
  const yearOptionsArray = [];
  const currentYear = new Date().getFullYear();
  // loop from current year to current year + <provided_number> years
  for (let i = currentYear; i <= currentYear + numberOfYears; i++) {
    // push each value in yearOptionsArray
    yearOptionsArray.push({ value: i, label: `${i}` });
  }
  // returning array
  return yearOptionsArray;
}

// populate custom date year options (from current year -1 to current year + numberOfYears)
export function populateCustomLastAndFutureYearsDateOptions(numberOfYears) {
  const yearOptionsArray = [];
  const currentYear = new Date().getFullYear();
  // loop from current year to current year + <provided_number> years
  for (let i = currentYear - 1; i <= currentYear + numberOfYears; i++) {
    // push each value in yearOptionsArray
    yearOptionsArray.push({ value: i, label: `${i}` });
  }
  // returning array
  return yearOptionsArray;
}

// populate custom date year options (from 2020 to current year)
export function populateCustomYearsFutureDateOptions() {
  const yearOptionsArray = [];
  const currentYear = new Date().getFullYear();
  // loop from current year to current year
  for (let i = 2020; i <= currentYear; i++) {
    // push each value in yearOptionsArray
    yearOptionsArray.push({ value: i, label: `${i}` });
  }
  // returning array
  return yearOptionsArray;
}

// populate hours field from 0 to 23
export function populateTimeHourOptions() {
  const hourOptionsArray = [];
  // loop from 0 to 23
  for (let i = 0; i <= 23; i++) {
    // push each value in dayOptionsArray
    if (i < 10) {
      hourOptionsArray.push({ value: i, label: `0${i}` });
    } else {
      hourOptionsArray.push({ value: i, label: `${i}` });
    }
  }
  // returning array
  return hourOptionsArray;
}

// populate minutes field from 0 to 59
export function populateTimeMinOptions() {
  const minuteOptionsArray = [];
  // loop from 0 to 59
  for (let i = 0; i <= 59; i++) {
    // push each value in monthOptionsArray
    if (i < 10) {
      minuteOptionsArray.push({ value: i, label: `0${i}` });
    } else {
      minuteOptionsArray.push({ value: i, label: `${i}` });
    }
  }
  // returning array
  return minuteOptionsArray;
}

// populate minutes field with 15 minute slots (0, 15, 30, 45)
export function populateTimeMinWith15MinSlotsOptions() {
  const minuteOptionsArray = [];
  // populating options
  // 00
  minuteOptionsArray.push({ value: 0, label: `00` });
  // 15
  minuteOptionsArray.push({ value: 15, label: `15` });
  // 30
  minuteOptionsArray.push({ value: 30, label: `30` });
  // 45
  minuteOptionsArray.push({ value: 45, label: `45` });
  // returning array
  return minuteOptionsArray;
}

export default populateCustomFutureYearsDateOptions;
