import SessionStorage, { ACTION } from "../SessionStorage";

export function clearTestSessionStorageButKeepLanguage() {
  SessionStorage(ACTION.CLEAR_KEEP_LANGUAGE);
}

export default function clearTestSessionStorage() {
  SessionStorage(ACTION.CLEAR);
}
