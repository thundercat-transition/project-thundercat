/* eslint-disable array-callback-return */
// getting property name as string (ref: https://stackoverflow.com/questions/13612006/get-object-property-name-as-a-string)
function getPropertyName(obj, expression) {
  const res = {};
  Object.keys(obj).map(k => {
    res[k] = k;
  });
  return expression(res);
}

export default getPropertyName;
