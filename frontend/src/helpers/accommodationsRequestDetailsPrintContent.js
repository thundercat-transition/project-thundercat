import { LANGUAGES } from "../modules/LocalizeRedux";
import LOCALIZE from "../text_resources";
import { Col, Row } from "react-bootstrap";
import psc_logo_en from "../images/psc_logo_en.png";
import psc_logo_fr from "../images/psc_logo_fr.png";
import mini_banner from "../images/mini_banner.png";
import canada_logo from "../images/canada_logo.png";
import { getFormattedTotalTestTime } from "../components/aae/AccommodationsRequestDetailsPopup";
import InputMask from "react-input-mask";
import { MASKS, MASK_FORMAT_CHARS } from "../components/commons/InputMask/helpers";
import { TestSkillSleDescCodename } from "../components/testFactory/Constants";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

/* eslint-disable react/react-in-jsx-scope */
const styles = {
  headerContainer: {
    display: "table",
    // 0.4in ==> marging set in @page in cat-theme.css
    width: "calc(100vw - 0.4in)"
  },
  headerTableRow: {
    display: "table-row"
  },
  headerImageContainer: {
    display: "table-cell"
  },
  headerLabelContainer: {
    display: "table-cell"
  },
  headerLabel: {
    textAlign: "right"
  },
  mainTitleStyle: {
    fontWeight: "bold",
    fontSize: 24,
    padding: "24px 0 12px 0"
  },
  titleStyle: {
    fontWeight: "bold",
    fontSize: 22,
    padding: "24px 0 12px 0"
  },
  reportSectionContainer: {
    margin: 12
  },
  boldText: {
    fontWeight: "bold"
  },
  centerText: {
    textAlign: "center"
  },
  hr: {
    borderTop: "2px dashed black"
  },
  reportFormContainer: {
    margin: 48
  },
  rowContainer: {
    padding: "3px 0",
    display: "table",
    width: "100%"
  },
  leftColContainer: {
    display: "table-cell",
    width: "35%",
    verticalAlign: "middle"
  },
  rightColContainer: {
    display: "table-cell",
    width: "65%",
    verticalAlign: "middle"
  },
  footerContainer: {
    display: "table",
    // 0.4in ==> marging set in @page in cat-theme.css
    width: "calc(100vw - 0.4in)"
  },
  footerFalseColumn: {
    display: "table-cell",
    verticalAlign: "middle",
    width: "33%"
  },
  footerLabelContainer: {
    display: "table-cell",
    verticalAlign: "middle",
    width: "33%",
    textAlign: "center"
  },
  footerImageContainer: {
    display: "table-cell",
    verticalAlign: "middle",
    width: "33%",
    textAlign: "right"
  },
  canadaLogo: {
    height: 30,
    width: 120
  },
  otherMeasures: {
    whiteSpace: "pre-line"
  },
  signatureBlock: {
    margin: "12px 72px"
  },
  noMargingPadding: {
    padding: 0,
    margin: 0
  }
};

// function that returns the content to be printed (HTML code)
function getAccommodationsRequestDetailsContentToPrint(
  accommodationsRequestData,
  currentLanguage,
  isDraft
) {
  // getting formatted test skill
  const formattedTestSkill =
    accommodationsRequestData.test_skill_sub_type_id !== null
      ? `${accommodationsRequestData[`test_skill_type_name_${currentLanguage}`]} - ${
          accommodationsRequestData[`test_skill_sub_type_name_${currentLanguage}`]
        }`
      : `${accommodationsRequestData[`test_skill_type_name_${currentLanguage}`]}`;

  return (
    <div className="main-div">
      <table>
        {/* Blank Header */}
        <thead>
          <div className="header-space">
            {/* non-breaking space */}
            &nbsp;
          </div>
        </thead>
        {/* Blank Header (END) */}
        <tbody>
          <tr>
            <td>
              <div>
                {/* Banner */}
                <img
                  alt={LOCALIZE.ariaLabel.images.banner}
                  src={mini_banner}
                  width="100%"
                  height="100%"
                  className="align-top"
                  aria-label={
                    LOCALIZE.accommodationsRequestDetailsPopup.printComponent.canadaLogoAriaLabel
                  }
                />
                {/* Banner (END) */}
                {/* Report Section */}
                <div style={styles.reportSectionContainer}>
                  <p style={styles.mainTitleStyle}>
                    {LOCALIZE.accommodationsRequestDetailsPopup.printComponent.title}
                  </p>
                  <p style={styles.titleStyle}>
                    {LOCALIZE.accommodationsRequestDetailsPopup.printComponent.reportSection.title}
                  </p>
                  <hr style={styles.hr} />
                  {/* Report Form Section */}
                  <div style={styles.reportFormContainer}>
                    <Row className="align-items-center" style={styles.rowContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.leftColContainer}
                      >
                        <label
                          id="accommodations-request-details-print-component-candidate-name-label"
                          style={styles.boldText}
                        >
                          {
                            LOCALIZE.accommodationsRequestDetailsPopup.printComponent.reportSection
                              .candidateNameLabel
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.rightColContainer}
                      >
                        <p>{`${accommodationsRequestData.user_last_name}, ${accommodationsRequestData.user_first_name}`}</p>
                      </Col>
                    </Row>
                    {/* OLA Test */}
                    {(accommodationsRequestData.test_skill_sub_type_codename ===
                      TestSkillSleDescCodename.ORAL_EN ||
                      accommodationsRequestData.test_skill_sub_type_codename ===
                        TestSkillSleDescCodename.ORAL_FR) && (
                      <Row className="align-items-center" style={styles.rowContainer}>
                        <Col
                          xl={columnSizes.firstColumn.xl}
                          lg={columnSizes.firstColumn.lg}
                          md={columnSizes.firstColumn.md}
                          sm={columnSizes.firstColumn.sm}
                          xs={columnSizes.firstColumn.xs}
                          style={styles.leftColContainer}
                        >
                          <label
                            id="accommodations-request-details-print-component-candidate-ola-phone-number-label"
                            style={styles.boldText}
                          >
                            {
                              LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                                .reportSection.candidateOlaPhoneNumberLabel
                            }
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.secondColumn.xl}
                          lg={columnSizes.secondColumn.lg}
                          md={columnSizes.secondColumn.md}
                          sm={columnSizes.secondColumn.sm}
                          xs={columnSizes.secondColumn.xs}
                          style={styles.rightColContainer}
                        >
                          {accommodationsRequestData.candidate_phone_number !== null ? (
                            <InputMask
                              className={"mask-input-without-style"}
                              type="text"
                              value={accommodationsRequestData.candidate_phone_number}
                              mask={MASKS.phoneNumber}
                              formatChars={MASK_FORMAT_CHARS.phoneNumber}
                            />
                          ) : (
                            "(___) ___ - ____"
                          )}
                        </Col>
                      </Row>
                    )}
                    <Row className="align-items-center" style={styles.rowContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.leftColContainer}
                      >
                        <label
                          id="accommodations-request-details-print-component-request-number-label"
                          style={styles.boldText}
                        >
                          {
                            LOCALIZE.accommodationsRequestDetailsPopup.printComponent.reportSection
                              .referenceNumberLabel
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.rightColContainer}
                      >
                        <p>{accommodationsRequestData.id}</p>
                      </Col>
                    </Row>
                    <Row className="align-items-center" style={styles.rowContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.leftColContainer}
                      >
                        <label
                          id="accommodations-request-details-print-component-test-skill-label"
                          style={styles.boldText}
                        >
                          {
                            LOCALIZE.accommodationsRequestDetailsPopup.printComponent.reportSection
                              .testSkillLabel
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.rightColContainer}
                      >
                        <p>{formattedTestSkill}</p>
                      </Col>
                    </Row>
                    <Row className="align-items-center" style={styles.rowContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.leftColContainer}
                      >
                        <label
                          id="accommodations-request-details-print-component-test-name-label"
                          style={styles.boldText}
                        >
                          {
                            LOCALIZE.accommodationsRequestDetailsPopup.printComponent.reportSection
                              .testNameLabel
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.rightColContainer}
                      >
                        <p>{accommodationsRequestData[`test_name_${currentLanguage}`]}</p>
                      </Col>
                    </Row>
                    {/* Not an OLA Test */}
                    {accommodationsRequestData.test_skill_sub_type_codename !==
                      TestSkillSleDescCodename.ORAL_EN &&
                      accommodationsRequestData.test_skill_sub_type_codename !==
                        TestSkillSleDescCodename.ORAL_FR && (
                        <Row className="align-items-center" style={styles.rowContainer}>
                          <Col
                            xl={columnSizes.firstColumn.xl}
                            lg={columnSizes.firstColumn.lg}
                            md={columnSizes.firstColumn.md}
                            sm={columnSizes.firstColumn.sm}
                            xs={columnSizes.firstColumn.xs}
                            style={styles.leftColContainer}
                          >
                            <label
                              id="accommodations-request-details-print-component-test-time-label"
                              style={styles.boldText}
                            >
                              {
                                LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                                  .reportSection.testTimeLabel
                              }
                            </label>
                          </Col>
                          <Col
                            xl={columnSizes.secondColumn.xl}
                            lg={columnSizes.secondColumn.lg}
                            md={columnSizes.secondColumn.md}
                            sm={columnSizes.secondColumn.sm}
                            xs={columnSizes.secondColumn.xs}
                            style={styles.rightColContainer}
                          >
                            <p>
                              {getFormattedTotalTestTime(
                                accommodationsRequestData.total_default_time,
                                accommodationsRequestData.total_adjusted_time
                              )}
                            </p>
                          </Col>
                        </Row>
                      )}
                    {accommodationsRequestData.break_time !== null && (
                      <Row className="align-items-center" style={styles.rowContainer}>
                        <Col
                          xl={columnSizes.firstColumn.xl}
                          lg={columnSizes.firstColumn.lg}
                          md={columnSizes.firstColumn.md}
                          sm={columnSizes.firstColumn.sm}
                          xs={columnSizes.firstColumn.xs}
                          style={styles.leftColContainer}
                        >
                          <label
                            id="accommodations-request-details-print-component-test-time-label"
                            style={styles.boldText}
                          >
                            {
                              LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                                .reportSection.breakBankLabel
                            }
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.secondColumn.xl}
                          lg={columnSizes.secondColumn.lg}
                          md={columnSizes.secondColumn.md}
                          sm={columnSizes.secondColumn.sm}
                          xs={columnSizes.secondColumn.xs}
                          style={styles.rightColContainer}
                        >
                          <p>
                            {`${accommodationsRequestData.break_time} ${LOCALIZE.commons.minutes}`}
                          </p>
                        </Col>
                      </Row>
                    )}
                    <Row className="align-items-center" style={styles.rowContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.leftColContainer}
                      >
                        <label
                          id="accommodations-request-details-print-component-test-center-label"
                          style={styles.boldText}
                        >
                          {
                            LOCALIZE.accommodationsRequestDetailsPopup.printComponent.reportSection
                              .testCenterLabel
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.rightColContainer}
                      >
                        <p>
                          {accommodationsRequestData.test_center_name !== null
                            ? accommodationsRequestData.test_center_name
                            : LOCALIZE.commons.na}
                        </p>
                      </Col>
                    </Row>
                    <Row className="align-items-center" style={styles.rowContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.leftColContainer}
                      >
                        <label
                          id="accommodations-request-details-print-component-requesting-organization-label"
                          style={styles.boldText}
                        >
                          {
                            LOCALIZE.accommodationsRequestDetailsPopup.printComponent.reportSection
                              .requestingOrganizationLabel
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.rightColContainer}
                      >
                        <p>
                          {`${
                            accommodationsRequestData[`requesting_dept_desc_${currentLanguage}`]
                          } (${
                            accommodationsRequestData[`requesting_dept_abrv_${currentLanguage}`]
                          })`}
                        </p>
                      </Col>
                    </Row>
                    <Row className="align-items-center" style={styles.rowContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.leftColContainer}
                      >
                        <label
                          id="accommodations-request-details-print-component-test-center-label"
                          style={styles.boldText}
                        >
                          {
                            LOCALIZE.accommodationsRequestDetailsPopup.printComponent.reportSection
                              .primaryContactLabel
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.rightColContainer}
                      >
                        <p>
                          {accommodationsRequestData.hr_user_id !== null
                            ? `${accommodationsRequestData.hr_last_name}, ${accommodationsRequestData.hr_first_name}`
                            : `${accommodationsRequestData.ta_last_name}, ${accommodationsRequestData.ta_first_name}`}
                        </p>
                      </Col>
                    </Row>
                    <Row className="align-items-center" style={styles.rowContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.leftColContainer}
                      >
                        <label
                          id="accommodations-request-details-print-component-primary-contact-phone-number-label"
                          style={styles.boldText}
                        >
                          {
                            LOCALIZE.accommodationsRequestDetailsPopup.printComponent.reportSection
                              .primaryContactPhoneNbrLabel
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.rightColContainer}
                      >
                        {accommodationsRequestData.hr_user_id !== null ? (
                          accommodationsRequestData.hr_phone_number !== null ? (
                            <InputMask
                              className={"mask-input-without-style"}
                              type="text"
                              value={accommodationsRequestData.hr_phone_number}
                              mask={MASKS.phoneNumber}
                              formatChars={MASK_FORMAT_CHARS.phoneNumber}
                            />
                          ) : (
                            "(___) ___ - ____"
                          )
                        ) : accommodationsRequestData.ta_phone_number !== null ? (
                          <InputMask
                            className={"mask-input-without-style"}
                            type="text"
                            value={accommodationsRequestData.ta_phone_number}
                            mask={MASKS.phoneNumber}
                            formatChars={MASK_FORMAT_CHARS.phoneNumber}
                          />
                        ) : (
                          "(___) ___ - ____"
                        )}
                      </Col>
                    </Row>
                    <Row className="align-items-center" style={styles.rowContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.leftColContainer}
                      >
                        <label
                          id="accommodations-request-details-print-component-primary-contact-email-label"
                          style={styles.boldText}
                        >
                          {
                            LOCALIZE.accommodationsRequestDetailsPopup.printComponent.reportSection
                              .primaryContactEmailLabel
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.rightColContainer}
                      >
                        <p>
                          {accommodationsRequestData.hr_user_id !== null
                            ? accommodationsRequestData.hr_email
                            : accommodationsRequestData.ta_email}
                        </p>
                      </Col>
                    </Row>
                    <Row className="align-items-center" style={styles.rowContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.leftColContainer}
                      >
                        <label
                          id="accommodations-request-details-print-component-aae-expert-contact-label"
                          style={styles.boldText}
                        >
                          {
                            LOCALIZE.accommodationsRequestDetailsPopup.printComponent.reportSection
                              .aaeExpertContactLabel
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.rightColContainer}
                      >
                        <p>
                          {`${accommodationsRequestData.assigned_to_user_last_name}, ${accommodationsRequestData.assigned_to_user_first_name}`}
                        </p>
                      </Col>
                    </Row>
                    <Row className="align-items-center" style={styles.rowContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.leftColContainer}
                      >
                        <label
                          id="accommodations-request-details-print-component-request-completed-date-label"
                          style={styles.boldText}
                        >
                          {
                            LOCALIZE.accommodationsRequestDetailsPopup.printComponent.reportSection
                              .requestCompletedDate
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.rightColContainer}
                      >
                        <p>
                          {typeof accommodationsRequestData.request_completed_by_aae_date !==
                            "undefined" &&
                          accommodationsRequestData.request_completed_by_aae_date !== null
                            ? accommodationsRequestData.request_completed_by_aae_date
                            : typeof accommodationsRequestData.request_completed_date !==
                                "undefined" &&
                              accommodationsRequestData.request_completed_date !== null
                            ? accommodationsRequestData.request_completed_date
                            : LOCALIZE.commons.na}
                        </p>
                      </Col>
                    </Row>
                  </div>
                  {/* Report Form Section (END) */}
                </div>
                {/* Report Section (END) */}
                {/* General Info Section */}
                <div className="pagebreak">
                  <h3>
                    {
                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo.generalInfoContent
                        .introductionHeader
                    }
                  </h3>
                  <p>
                    {
                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo.generalInfoContent
                        .introductionParagraph1
                    }
                  </p>
                  <p>
                    {
                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo.generalInfoContent
                        .introductionParagraph2
                    }
                  </p>
                  <p>
                    {
                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo.generalInfoContent
                        .introductionParagraph3
                    }
                  </p>
                  <p>
                    {
                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo.generalInfoContent
                        .introductionParagraph4
                    }
                  </p>
                  <h3>
                    {
                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo.generalInfoContent
                        .importantNotesHeader
                    }
                  </h3>
                  <p>
                    {
                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo.generalInfoContent
                        .importantNotesParagraph1
                    }
                  </p>
                  <p>
                    {
                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo.generalInfoContent
                        .importantNotesParagraph2
                    }
                  </p>
                  <p>
                    {
                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo.generalInfoContent
                        .importantNotesParagraph3
                    }
                  </p>
                  <h3>
                    {
                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo.generalInfoContent
                        .candidateDeclarationHeader
                    }
                  </h3>
                  <p>
                    {
                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo.generalInfoContent
                        .candidateDeclarationParagraph
                    }
                  </p>
                </div>
                {/* General Info Section (END) */}
                {/* Accommodations Section */}
                <div>
                  <p style={styles.titleStyle}>
                    {
                      LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                        .accommodationsSection.title
                    }
                  </p>
                  {/* Accommodations - General Info Section */}
                  <div>
                    <p style={styles.boldText}>
                      {
                        LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                          .accommodationsSection.generalInfoSection.paragraph1
                      }
                    </p>
                    <p>
                      {
                        LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                          .accommodationsSection.generalInfoSection.paragraph2
                      }
                    </p>
                    <p>
                      {
                        LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                          .accommodationsSection.generalInfoSection.paragraph3
                      }
                    </p>
                  </div>
                  {/* Accommodations - General Info Section (END) */}
                  {/* Other Measures Section */}
                  {accommodationsRequestData.other_measures !== null &&
                    accommodationsRequestData.other_measures !== "" && (
                      <div>
                        <p style={styles.boldText}>
                          {
                            LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                              .accommodationsSection.otherMeasures.title
                          }
                        </p>
                        <p style={styles.otherMeasures}>
                          {accommodationsRequestData.other_measures}
                        </p>
                      </div>
                    )}
                  {/* Other Measures Section (END) */}
                </div>
                {/* Accommodations Section (END) */}
                {/* Contact Information */}
                <div>
                  <p style={styles.titleStyle}>
                    {
                      LOCALIZE.accommodationsRequestDetailsPopup.printComponent.contactInfoSection
                        .title
                    }
                  </p>
                  {/* All Inquiries */}
                  <div>
                    <p style={styles.boldText}>
                      {
                        LOCALIZE.accommodationsRequestDetailsPopup.printComponent.contactInfoSection
                          .allInquiries.description
                      }
                    </p>
                    <div style={styles.signatureBlock}>
                      <p style={styles.noMargingPadding}>
                        {accommodationsRequestData.assigned_to_user_id !== null
                          ? `${accommodationsRequestData.assigned_to_user_last_name.toUpperCase()} ${accommodationsRequestData.assigned_to_user_first_name.toUpperCase()}`
                          : ""}
                      </p>
                      <p style={styles.noMargingPadding}>
                        {
                          LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                            .contactInfoSection.allInquiries.role
                        }
                      </p>
                      <p style={styles.noMargingPadding}>
                        {
                          LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                            .contactInfoSection.allInquiries.unit
                        }
                      </p>
                      <p style={styles.noMargingPadding}>
                        {
                          LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                            .contactInfoSection.allInquiries.ppcDescription
                        }
                      </p>
                      <p style={styles.noMargingPadding}>
                        {LOCALIZE.formatString(
                          LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                            .contactInfoSection.allInquiries.phoneNumber,
                          accommodationsRequestData.assigned_to_phone_number !== null ? (
                            <InputMask
                              className={"mask-input-without-style"}
                              type="text"
                              value={accommodationsRequestData.assigned_to_phone_number}
                              mask={MASKS.phoneNumber}
                              formatChars={MASK_FORMAT_CHARS.phoneNumber}
                            />
                          ) : (
                            "(___) ___ - ____"
                          )
                        )}
                      </p>
                      <p style={styles.noMargingPadding}>
                        {LOCALIZE.formatString(
                          LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                            .contactInfoSection.allInquiries.email,
                          accommodationsRequestData.assigned_to_user_id !== null
                            ? accommodationsRequestData.assigned_to_user_email
                            : ""
                        )}
                      </p>
                    </div>
                  </div>
                  {/* All Inquiries (END) */}
                  {/* General Inquiries */}
                  <div>
                    <p style={styles.boldText}>
                      {
                        LOCALIZE.accommodationsRequestDetailsPopup.printComponent.contactInfoSection
                          .allInquiries.description
                      }
                    </p>
                    <div style={styles.signatureBlock}>
                      <p style={styles.noMargingPadding}>
                        {
                          LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                            .contactInfoSection.generalInquiries.unit
                        }
                      </p>
                      <p style={styles.noMargingPadding}>
                        {
                          LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                            .contactInfoSection.generalInquiries.phoneNumber
                        }
                      </p>
                      <p style={styles.noMargingPadding}>
                        {
                          LOCALIZE.accommodationsRequestDetailsPopup.printComponent
                            .contactInfoSection.generalInquiries.email
                        }
                      </p>
                    </div>
                  </div>
                  {/* General Inquiries (END) */}
                </div>
                {/* Contact Information (END) */}
              </div>
            </td>
          </tr>
        </tbody>
        {/* Blank Footer */}
        <tfoot>
          <tr>
            <td>
              <div className="footer-space">
                {/* non-breaking space */}
                &nbsp;
              </div>
            </td>
          </tr>
        </tfoot>
        {/* Blank Footer (END) */}
      </table>
      {/* Official Header */}
      <div className="header" style={styles.headerContainer}>
        <div style={styles.headerTableRow}>
          <div style={styles.headerImageContainer}>
            <img
              alt={LOCALIZE.ariaLabel.images.pscLogo}
              src={currentLanguage === LANGUAGES.french ? psc_logo_fr : psc_logo_en}
              width="370px"
              height="27.75px"
              className="align-top"
            />
          </div>
          <div style={styles.headerLabelContainer}>
            <p style={{ ...styles.headerLabel, ...styles.boldText }}>
              {LOCALIZE.commons.protected}
            </p>
          </div>
        </div>
      </div>
      {/* Official Header (END) */}
      {/* Watermark */}
      {isDraft && (
        <div id="draft-watermark">
          <span id="draft-watermark-text">
            {LOCALIZE.accommodationsRequestDetailsPopup.printComponent.draftWatermark}
          </span>
        </div>
      )}
      {/* Watermark (END) */}
      {/* Official Footer */}
      <div className="footer" style={styles.footerContainer}>
        <div style={styles.footerFalseColumn}></div>
        <div style={styles.footerLabelContainer}>
          <p
            style={{ ...styles.boldText, ...styles.centerText }}
          >{`${accommodationsRequestData.user_last_name}, ${accommodationsRequestData.user_first_name}`}</p>
        </div>
        <div style={styles.footerImageContainer}>
          <img
            alt={LOCALIZE.ariaLabel.images.canadaLogo}
            src={canada_logo}
            width="10%"
            height="10%"
            style={styles.canadaLogo}
            className="align-center"
          />
        </div>
      </div>
      {/* Official Footer (END) */}
    </div>
  );
}

export default getAccommodationsRequestDetailsContentToPrint;
