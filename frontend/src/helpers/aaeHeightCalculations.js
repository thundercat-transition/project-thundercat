// getting height based on provided dynamic values
function getSelectedUserAccommodationFileHeightCalculations(
  fontSize,
  spacingEnabled,
  testNavBarHeight,
  topTabsHeight,
  testFooterHeight
) {
  // calculating height based on provided dynamic values
  // "24" is for the padding top in test section component factory (TestSectionComponentFactory.jsx)
  let heightAdjustments = testNavBarHeight + topTabsHeight + testFooterHeight + 24;
  // if spacing enabled
  if (spacingEnabled) {
    // adding necessary adjustments
    heightAdjustments += parseInt(fontSize.split("px")[0]) / 2 + 2;
  }
  // if contains top tabs
  if (topTabsHeight !== 0) {
    // "20" is for the top tabs padding top
    heightAdjustments += 20;
  }
  // returning style object
  return { height: `calc(100vh - ${heightAdjustments}px)` };
}

export default getSelectedUserAccommodationFileHeightCalculations;
