// getting display options for pages with pagination

function getDisplayOptions() {
  const displayOptionsArray = [];

  // TESTING VALUES (Do not commit testing values) =====
  // displayOptionsArray.push({ value: 2, label: "2" });
  // displayOptionsArray.push({ value: 3, label: "3" });
  // ===================================================

  displayOptionsArray.push({ value: 25, label: "25" });
  displayOptionsArray.push({ value: 50, label: "50" });
  displayOptionsArray.push({ value: 100, label: "100" });

  return displayOptionsArray;
}

export default getDisplayOptions;
