// formatting date (format: AAAA-MM-DD)
// 2021-5-7 ==> 2021-05-07
function getFormattedDate(date) {
  const year = date.split("-")[0];
  let month = parseInt(date.split("-")[1]);
  let day = parseInt(date.split("-")[2]);

  // month value is less than 10
  if (month < 10) {
    // adding "0" in front of the number
    month = `0${month}`;
  }

  // day value is less than 10
  if (day < 10) {
    // adding "0" in front of the number
    day = `0${day}`;
  }

  return `${year}-${month}-${day}`;
}

export default getFormattedDate;
