import { ALERT_TYPE } from "../components/commons/AlertMessage";

// codenames from the DB
const CRITICALITY = {
  danger: "danger",
  warning: "warning",
  info: "information"
};

// transforming switch based on provided font size
function getCriticalityDataForSystemAlert(criticalityCodename) {
  switch (criticalityCodename) {
    // DANGER
    case CRITICALITY.danger:
      return ALERT_TYPE.danger;
    // WARNING
    case CRITICALITY.warning:
      return ALERT_TYPE.warning;

    // INFO
    default:
      return ALERT_TYPE.info;
  }
}

export default getCriticalityDataForSystemAlert;
