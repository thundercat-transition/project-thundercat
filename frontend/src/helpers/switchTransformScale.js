// transforming switch based on provided font size
function getSwitchTransformScale(fontSize) {
  // converting current font size in Int
  const fontSizeInt = parseInt(fontSize.substring(0, 2));
  // converting switch dimensions based on font size
  switch (true) {
    case fontSizeInt <= 12:
      return { height: 15, width: 30 };
    case fontSizeInt > 12 && fontSizeInt <= 15:
      return { height: 20, width: 40 };
    case fontSizeInt > 15 && fontSizeInt <= 22:
      return { height: 25, width: 50 };
    case fontSizeInt > 22 && fontSizeInt <= 30:
      return { height: 30, width: 60 };
    case fontSizeInt > 30 && fontSizeInt <= 40:
      return { height: 35, width: 70 };
    case fontSizeInt > 40:
      return { height: 40, width: 80 };
    default:
      return { height: 25, width: 50 };
  }
}

export default getSwitchTransformScale;
