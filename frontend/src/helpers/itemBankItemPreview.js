/* eslint-disable prefer-destructuring */
import * as _ from "lodash";
import {
  ITEM_CONTENT_TYPE,
  ITEM_OPTION_TYPE
} from "../components/testBuilder/itemBank/items/Constants";
import LOCALIZE from "../text_resources";
import shuffleArrayOfObjects from "./shuffleArray";

// getting item content data for preview
function getItemContentToPreview(itemData, languageData, contentType = ITEM_CONTENT_TYPE.markdown) {
  // creating a copy of item_content
  const copyOfItemContent = _.cloneDeep(itemData.item_content);

  for (let i = 0; i < languageData.length; i++) {
    // item content in language of current iteration is defined
    if (typeof itemData.item_content[languageData[i].ISO_Code_1] !== "undefined") {
      // updating copyOfItemContent with object in language of current iteration

      // only getting markdown content
      copyOfItemContent[[languageData[i].ISO_Code_1]] = itemData.item_content[
        languageData[i].ISO_Code_1
      ].filter(obj => {
        return obj.content_type === contentType;
      });
    }
  }

  return copyOfItemContent;
}

// getting item content data for preview
export function getItemOptionsToPreview(
  itemData,
  languageData,
  optionType = ITEM_OPTION_TYPE.multipleChoice,
  calledForItemPreview = false
) {
  // creating a copy of item_options
  const copyOfItemOptions = _.cloneDeep(itemData.item_options);

  // looping in languageData
  for (let i = 0; i < languageData.length; i++) {
    // item options in language of current iteration is defined
    if (typeof itemData.item_options[languageData[i].ISO_Code_1] !== "undefined") {
      // updating copyOfItemOptions with object in language of current iteration
      // only getting multiple choice content
      copyOfItemOptions[[languageData[i].ISO_Code_1]] = itemData.item_options[
        languageData[i].ISO_Code_1
      ].filter(obj => {
        return obj.option_type === optionType;
      });

      // calledForItemPreview is set to true
      if (calledForItemPreview) {
        // shuffle answer choices is enabled
        if (itemData.shuffle_options) {
          // this is the mirror logic of the shuffle item options logic in the backend (shuffle_item_options function in ...\project-thundercat\backend\cms\serializers\item_bank_serializer.py)
          // initializing needed variables
          const optionsArray = copyOfItemOptions[[languageData[i].ISO_Code_1]];

          const finalOptionsArray = [];
          let arrayToBeShuffled = [];
          // looping in optionsArray
          for (let i = 0; i < optionsArray.length; i++) {
            // option of current iteration is excluded from shuffle
            if (optionsArray[i].exclude_from_shuffle) {
              // populating finalOptionsArray with object value
              finalOptionsArray.push(optionsArray[i]);
            } else {
              // populating finalOptionsArray with null value
              finalOptionsArray.push(null);
              // populating arrayToBeShuffled with object value
              arrayToBeShuffled.push(optionsArray[i]);
            }
          }

          // shuffling arrayToBeShuffled array
          arrayToBeShuffled = shuffleArrayOfObjects(arrayToBeShuffled);

          // combining finalOptionsArray and arrayToBeShuffled together
          for (let i = 0; i < finalOptionsArray.length; i++) {
            // if value of current iteration is null
            if (finalOptionsArray[i] === null) {
              // overriding respective value in finalOptionsArray
              finalOptionsArray[i] = arrayToBeShuffled[0];
              // removing first element of arrayToBeShuffled
              arrayToBeShuffled.shift();
            }
          }
          // updating copyOfItemOptions with object in language of current iteration
          copyOfItemOptions[[languageData[i].ISO_Code_1]] = finalOptionsArray;
        }
      }
    }
  }

  return copyOfItemOptions;
}

export default getItemContentToPreview;
