import LOCALIZE from "../text_resources";

/* eslint-disable react/react-in-jsx-scope */
const styles = {
  table: {
    width: "100%",
    border: "1px solid #00565e"
  },
  tableHeader: {
    backgroundColor: "#00565e",
    color: "white",
    fontWeight: "bold"
  },
  tableHeaderTh: {
    padding: 12
  },
  tableRwo: {
    border: "1px solid #00565e"
  },
  tableRowTh: {
    padding: 12,
    fontWeight: "unset"
  },
  boldText: {
    fontWeight: "bold"
  }
};

// function that returns the content to be printed (HTML code)
function getContentToPrint(columnsDefinition, testSessionData, rowsDefinition) {
  return (
    <div>
      <div>
        <p>
          {
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
              .testSessionsData.viewAttendeesPopup.description1
          }
        </p>
        <ul>
          <li>
            {LOCALIZE.formatString(
              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                .topTabs.testSessionsData.viewAttendeesPopup.description2,
              <span style={styles.boldText}>{testSessionData.formatted_date}</span>
            )}
          </li>
          <li>
            {LOCALIZE.formatString(
              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                .topTabs.testSessionsData.viewAttendeesPopup.description3,
              <span style={styles.boldText}>{testSessionData.room_name}</span>
            )}
          </li>
          <li>
            {LOCALIZE.formatString(
              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                .topTabs.testSessionsData.viewAttendeesPopup.description4,
              <span style={styles.boldText}>{testSessionData.formatted_test_skill}</span>
            )}
          </li>
        </ul>
      </div>
      <div>
        <table style={styles.table}>
          <tr style={styles.tableHeader}>
            {columnsDefinition.map(columnObj => {
              return <th style={styles.tableHeaderTh}>{columnObj.label}</th>;
            })}
          </tr>
          {rowsDefinition.data.map(rowDefinition => {
            return (
              <tr style={styles.tableRwo}>
                <th style={styles.tableRowTh}>{rowDefinition.column_1}</th>
                <th style={styles.tableRowTh}>{rowDefinition.column_2}</th>
                <th style={styles.tableRowTh}>{rowDefinition.column_3}</th>
                <th style={styles.tableRowTh}>{rowDefinition.column_4}</th>
              </tr>
            );
          })}
        </table>
      </div>
    </div>
  );
}

export default getContentToPrint;
