import moment from "moment";

// getting retest date based on provided test date and number of days (retest period)
// date needs to be provided in format 'yyyy-mm-dd'
function getRetestDate(testDate, numberOfDays) {
  return moment(testDate).add(parseInt(numberOfDays), "days").format("YYYY-MM-DD");
}

export default getRetestDate;
