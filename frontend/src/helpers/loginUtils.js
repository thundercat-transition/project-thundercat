import { PATH } from "../components/commons/Constants";
import { PERMISSION } from "../components/profile/Constants";
import usernameFormatter from "./usernameFormatter";

// getting/setting user permissions (including the update of all needed redux states)
export default async function getAndSetUserPermissions(
  getUserPermissions,
  updatePermissionsState,
  updateCurrentHomePageState,
  isSuperUser
) {
  // initialize permission flags
  await getUserPermissions().then(response => {
    // initializing permission flags
    let isEtta = false;
    let isPpc = false;
    let isTa = false;
    let isScorer = false;
    let isTb = false;
    let isAae = false;
    let isRdOperations = false;
    let isTd = false;
    let isTcm = false;
    let isHrCoordinator = false;
    let isConsultationServices = false;
    // specified user is a super user
    if (isSuperUser) {
      // setting all permission flags to true
      isEtta = true;
      isPpc = true;
      isTa = true;
      isScorer = true;
      isTb = true;
      isAae = true;
      isRdOperations = true;
      isTd = true;
      isTcm = true;
      isHrCoordinator = true;
      isConsultationServices = true;
      // // specified user is not a super user
    } else {
      // looping in user permissions to update the needed permission flags
      for (let i = 0; i < response.length; i++) {
        if (response[i].codename === PERMISSION.systemAdministrator) {
          isEtta = true;
        } else if (response[i].codename === PERMISSION.ppcAdministrator) {
          isPpc = true;
        } else if (response[i].codename === PERMISSION.scorer) {
          isScorer = true;
        } else if (response[i].codename === PERMISSION.testAdministrator) {
          isTa = true;
        } else if (response[i].codename === PERMISSION.testBuilder) {
          isTb = true;
        } else if (response[i].codename === PERMISSION.aae) {
          isAae = true;
        } else if (response[i].codename === PERMISSION.rdOperations) {
          isRdOperations = true;
        } else if (response[i].codename === PERMISSION.testDeveloper) {
          isTd = true;
        } else if (response[i].codename === PERMISSION.testCenterManager) {
          isTcm = true;
        } else if (response[i].codename === PERMISSION.hrCoordinator) {
          isHrCoordinator = true;
        } else if (response[i].codename === PERMISSION.consultationServices) {
          isConsultationServices = true;
        }
      }
    }
    // update permissions states based on the user permission flags
    updatePermissionsState({
      isEtta: isEtta,
      isPpc: isPpc,
      isTa: isTa,
      isScorer: isScorer,
      isTb: isTb,
      isAae: isAae,
      isRdOperations: isRdOperations,
      isTd: isTd,
      isTcm: isTcm,
      isHrCoordinator: isHrCoordinator,
      isConsultationServices: isConsultationServices
    });
    // update the current home page state based on the permissions
    if (isSuperUser) {
      updateCurrentHomePageState(PATH.superuserDashboard);
      // is ETTA
    } else if (isEtta || isRdOperations) {
      updateCurrentHomePageState(PATH.systemAdministration);
      // is PPC
    } else if (isPpc) {
      updateCurrentHomePageState(PATH.ppcAdministration);
      // is Test Center Manager
    } else if (isTcm) {
      updateCurrentHomePageState(PATH.testCenterManager);
      // is scorer
    } else if (isScorer) {
      updateCurrentHomePageState(PATH.scorerBase);
      // is TA
    } else if (isTa) {
      updateCurrentHomePageState(PATH.testAdministration);
      // is Test Builder
    } else if (isTb) {
      updateCurrentHomePageState(PATH.testBuilder);
      // is Test Developer
    } else if (isTd) {
      updateCurrentHomePageState(PATH.testBuilder);
      // is HR Coordinator
    } else if (isHrCoordinator) {
      updateCurrentHomePageState(PATH.inPersonTesting);
      // is AAE
    } else if (isAae) {
      updateCurrentHomePageState(PATH.aae);
      // is Consultation Services
    } else if (isConsultationServices) {
      updateCurrentHomePageState(PATH.consultationServices);
      // is candidate
    } else {
      updateCurrentHomePageState(PATH.dashboard);
    }
  });
}

export async function getAndSetUserInfo(getUserInformation, setUserInformation) {
  await getUserInformation().then(response => {
    setUserInformation(
      response.id,
      response.first_name,
      response.last_name,
      usernameFormatter(response.username),
      response.is_staff,
      response.last_password_change,
      response.phone_number,
      response.psrs_applicant_id,
      response.email,
      response.secondary_email,
      response.birth_date,
      response.pri,
      response.military_nbr,
      response.is_profile_complete
    );
  });
}

export async function setReduxStates(
  updatePageHasErrorState,
  setTestDefinitionSelectionSideNavState,
  setBOUserSideNavState
) {
  updatePageHasErrorState(false);
  // resetting side nav selected tab props
  setTestDefinitionSelectionSideNavState(0);
  setBOUserSideNavState(0);
}
