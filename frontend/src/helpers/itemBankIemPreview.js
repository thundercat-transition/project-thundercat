import * as _ from "lodash";
import {
  ITEM_CONTENT_TYPE,
  ITEM_OPTION_TYPE
} from "../components/testBuilder/itemBank/items/Constants";
import LOCALIZE from "../text_resources";

// getting item content data for preview
function getItemContentToPreview(itemData, languageData, contentTabs, contentTabProps) {
  // creating a copy of item_content
  const copyOfItemContent = _.cloneDeep(itemData.item_content);

  // getting current selected tab
  const currentSelectedTab = contentTabs.filter(obj => {
    return obj.key === contentTabProps;
  })[0];

  // current selected tab is undefined or tab key is content-1 (regular content tab)
  if (typeof currentSelectedTab === "undefined" || currentSelectedTab.key === "content-1") {
    // looping in languageData
    for (let i = 0; i < languageData.length; i++) {
      // item content in language of current iteration is defined
      if (typeof itemData.item_content[languageData[i].ISO_Code_1] !== "undefined") {
        // updating copyOfItemContent with object in language of current iteration
        // only getting markdown content
        copyOfItemContent[[languageData[i].ISO_Code_1]] = itemData.item_content[
          languageData[i].ISO_Code_1
        ].filter(obj => {
          return obj.content_type === ITEM_CONTENT_TYPE.markdown;
        });
      }
    }
    // alternative content
  } else {
    // getting alternative tab name
    const alternativeTabName = currentSelectedTab.tabName.props.children[0].props.children;

    // looping in languageData
    for (let i = 0; i < languageData.length; i++) {
      // item content in language of current iteration is defined
      if (typeof itemData.item_content[languageData[i].ISO_Code_1] !== "undefined") {
        switch (alternativeTabName) {
          // SCREEN READER
          case LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.topTabs
            .contentTabs.screenReader:
            // only getting screen reader content
            copyOfItemContent[[languageData[i].ISO_Code_1]] = itemData.item_content[
              languageData[i].ISO_Code_1
            ].filter(obj => {
              return obj.content_type === ITEM_CONTENT_TYPE.screenReader;
            });
            break;
          default:
            copyOfItemContent[[languageData[i].ISO_Code_1]] = [];
            break;
        }
      }
    }
  }

  return copyOfItemContent;
}

// getting item content data for preview
export function getItemOptionsToPreview(itemData, languageData, contentTabs, contentTabProps) {
  // creating a copy of item_options
  const copyOfItemOptions = _.cloneDeep(itemData.item_options);

  // getting current selected tab
  const currentSelectedTab = contentTabs.filter(obj => {
    return obj.key === contentTabProps;
  })[0];

  // current selected tab is undefined or tab key is content-1 (regular content tab)
  if (typeof currentSelectedTab === "undefined" || currentSelectedTab.key === "content-1") {
    // looping in languageData
    for (let i = 0; i < languageData.length; i++) {
      // item options in language of current iteration is defined
      if (typeof itemData.item_options[languageData[i].ISO_Code_1] !== "undefined") {
        // updating copyOfItemOptions with object in language of current iteration
        // only getting multiple choice content
        copyOfItemOptions[[languageData[i].ISO_Code_1]] = itemData.item_options[
          languageData[i].ISO_Code_1
        ].filter(obj => {
          return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
        });
      }
    }
    // alternative content
  } else {
    // getting alternative tab name
    const alternativeTabName = currentSelectedTab.tabName.props.children[0].props.children;

    // looping in languageData
    for (let i = 0; i < languageData.length; i++) {
      // item options in language of current iteration is defined
      if (typeof itemData.item_options[languageData[i].ISO_Code_1] !== "undefined") {
        switch (alternativeTabName) {
          // SCREEN READER
          case LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items.topTabs
            .contentTabs.screenReader:
            // only getting multiple choice screen reader content
            copyOfItemOptions[[languageData[i].ISO_Code_1]] = itemData.item_options[
              languageData[i].ISO_Code_1
            ].filter(obj => {
              return obj.option_type === ITEM_OPTION_TYPE.multipleChoiceScreenReader;
            });
            break;
          default:
            copyOfItemOptions[[languageData[i].ISO_Code_1]] = [];
            break;
        }
      }
    }
  }

  return copyOfItemOptions;
}

export default getItemContentToPreview;
