import LOCALIZE from "../text_resources";

// source: https://stackoverflow.com/questions/4687723/how-to-convert-minutes-to-hours-minutes-and-add-various-time-values-together-usi
// converting minutes to hours and minutes
function minutesToHours(number) {
  const hours = Math.floor(number / 60);
  const minutes = Math.trunc(number % 60);
  return { hours: hours, minutes: minutes };
}

export function hoursMinutesToMinutes(hours, minutes) {
  let result = 0;
  result = minutes + hours * 60;
  return result;
}

export function getFormattedTime(time) {
  // initializing formattedTime
  let formattedTime = time;
  // adding zero in front of the hours number if less than 10 (to have something like 01:30 not 1:30)
  if (time < 10) {
    formattedTime = `0${time}`;
  }
  // returning formattedTime
  return `${formattedTime}`;
}

// getting formatted time (in hours and minutes)
export function getTimeInHoursMinutes(minutes) {
  // getting time in hours and minutes
  const convertedTime = minutesToHours(minutes);
  // retuning time in format (hours : minutes): 00 : 00
  return {
    hours: Number(convertedTime.hours),
    formattedHours: getFormattedTime(convertedTime.hours),
    minutes: Number(convertedTime.minutes),
    formattedMinutes: getFormattedTime(convertedTime.minutes)
  };
}

// getting formatted test time (time displayed in start test confirmation popup when starting a test)
export function getFormattedTestTime(providedMinutes) {
  // making sure that the provided time is a int
  const min = parseInt(providedMinutes);
  // amount of time is less or equal to 60 minutes
  if (min <= 60) {
    return `${min} ${LOCALIZE.commons.minutes}`;
  }
  // amount of time is greater than 60 minutes
  const hours = min / 60;
  const rhours = Math.floor(hours);
  const minutes = (hours - rhours) * 60;
  const rminutes = Math.round(minutes);
  return `${rhours} ${LOCALIZE.commons.hours} ${rminutes} ${LOCALIZE.commons.minutes}`;
}

export function secondsToMinutesSeconds(seconds) {
  const timeInMinutes = Math.floor(seconds / 60);
  const timeInSeconds = seconds - timeInMinutes * 60;

  return `${timeInMinutes} ${LOCALIZE.commons.minutes} ${timeInSeconds} ${LOCALIZE.commons.seconds}`;
}

export default minutesToHours;
