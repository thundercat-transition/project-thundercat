import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "./text_resources";
import { connect } from "react-redux";
import { isTokenStillValid } from "./modules/LoginRedux";
import { bindActionCreators } from "redux";
import AssignedTestTable from "./components/candidate/AssignedTestTable";
import ContentContainer from "./components/commons/ContentContainer";
import { Helmet } from "react-helmet";
import LastLogin from "./components/authentication/LastLogin";
import { PATH } from "./components/commons/Constants";
import ReservationManagement from "./components/candidate/ReservationManagement";
import MyTests from "./MyTests";
import SideNavigation from "./components/eMIB/SideNavigation";
import { styles as SystemAdministrationStyles } from "./components/etta/SystemAdministration";
import { setCandidateUserSideNavState } from "./modules/CandidateRedux";
import { history } from "./store-index";

export const styles = {
  appPadding: {
    padding: "15px"
  }
};

class Dashboard extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // Props from Redux
    isTokenStillValid: PropTypes.func
  };

  state = {
    isCandidateDashboard: false
  };

  componentDidMount = () => {
    // to display last login if it is a candidate dashboard
    if (this.props.currentHomePage === PATH.dashboard) {
      this.setState({
        isCandidateDashboard: true
      });
    }
    // if there is an active test
    if (this.props.isTestActive) {
      // redirecting to test
      history.push(PATH.testBase);
    }
  };

  getCandidateSections = () => {
    return [
      {
        menuString: LOCALIZE.dashboard.sideNavItems.takeATest,
        body: <AssignedTestTable />
      },
      {
        menuString: LOCALIZE.dashboard.sideNavItems.reserveSeat.title,
        body: <ReservationManagement />
      },
      {
        menuString: LOCALIZE.dashboard.sideNavItems.testResults,
        body: <MyTests />
      }
    ];
  };

  render() {
    const specs = this.getCandidateSections();
    // This renders two versions of the dashboard
    // If the user data has loaded, then it shows the candidate dash
    // otherwise it shows the header common to every dashboard
    // and a loading circle
    return (
      <div className="app" style={styles.appPadding}>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title className="notranslate">{LOCALIZE.titles.home}</title>
        </Helmet>
        {this.state.isCandidateDashboard && <LastLogin lastLoginDate={this.props.lastLogin} />}
        <ContentContainer>
          <div id="main-content" role="main">
            <div
              id="user-welcome-message-div"
              aria-labelledby="user-welcome-message"
              className="notranslate"
            >
              <h1 id="user-welcome-message" className="green-divider">
                {LOCALIZE.formatString(
                  LOCALIZE.dashboard.title,
                  this.props.firstName,
                  this.props.lastName
                )}
              </h1>
              <div>
                <section aria-label={LOCALIZE.ariaLabel.sideNavigationSection}>
                  <div style={SystemAdministrationStyles.sectionContainer}>
                    <SideNavigation
                      specs={specs}
                      startIndex={this.props.selectedSideNavItem}
                      loadOnClick={true}
                      displayNextPreviousButton={false}
                      isMain={true}
                      tabContainerStyle={SystemAdministrationStyles.tabContainer}
                      tabContentStyle={SystemAdministrationStyles.tabContent}
                      navStyle={SystemAdministrationStyles.nav}
                      bodyContentCustomStyle={SystemAdministrationStyles.sideNavBodyContent}
                      updateSelectedSideNavItem={this.props.setCandidateUserSideNavState}
                    />
                  </div>
                </section>
              </div>
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { Dashboard as UnconnectedDashboard };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    lastLogin: state.user.lastLogin,
    currentHomePage: state.userPermissions.currentHomePage,
    isTestActive: state.testStatus.isTestActive,
    selectedSideNavItem: state.candidate.selectedSideNavItem
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid,
      setCandidateUserSideNavState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
