/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col } from "react-bootstrap";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import { getTimeInHoursMinutes, hoursMinutesToMinutes } from "../../helpers/timeConversion";
import SetTimer from "../commons/SetTimer";
import {
  setAaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState,
  setAaeSelectedUserAccommodationFileData
} from "../../modules/AaeRedux";
import * as _ from "lodash";
import { userAccommodationFileIsInEndState } from "./SelectedUserAccommodationFile";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 7
  }
};

const styles = {
  noMargin: {
    margin: 0
  },
  formContainer: {
    padding: 12
  },
  rowStyle: {
    padding: "6px 0px"
  },
  inputTitles: {
    fontWeight: "bold"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  systemMessageContainer: {
    padding: 24
  },
  breakBankAndTimeAdjustmentMainContainer: {
    padding: "24px 0"
  },
  noTopPadding: {
    paddingTop: 0
  },
  timerLabel: {
    fontWeight: "bold"
  },
  additionalPadding: {
    paddingLeft: 24
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: "24px 0 0 0",
    marginTop: 6
  }
};

class SelectedUserAccommodationFileMainTabBreakBankAndTimeAdjustment extends Component {
  componentDidMount = () => {
    this.handleReduxStateUpdates();
  };

  componentDidUpdate = prevProps => {
    // if timeUpdated gets updated
    if (prevProps.timeUpdated !== this.props.timeUpdated) {
      // updating redux states
      this.handleReduxStateUpdates();
    }
    // if validTimer gets updated
    if (prevProps.validTimer !== this.props.validTimer) {
      // updating redux break bank and time adjustment valid state
      this.props.setAaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState(
        this.props.validTimer
      );
    }
  };

  handleReduxStateUpdates = () => {
    // creating a copy of respective redux states
    const copyOfAaeSelectedUserAccommodationFileData = _.cloneDeep(
      this.props.aaeSelectedUserAccommodationFileData
    );

    // ========== BREAK BANK ==========
    // getting minutes from hours/minutes
    // using index 0 of currentTimer since this is for the break bank data
    const formattedMinutes = hoursMinutesToMinutes(
      parseInt(this.props.currentTimer[0].hours),
      parseInt(this.props.currentTimer[0].minutes)
    );
    // updating needed attributes
    copyOfAaeSelectedUserAccommodationFileData.break_bank = formattedMinutes;
    // ========== BREAK BANK (END) ==========

    // ========== TEST SECTIONS TIME ==========
    // looping in test_sections_time
    for (let i = 1; i < this.props.currentTimer.length; i++) {
      // getting minutes from hours/minutes
      // using index 1+ of currentTimer since this is for the test sections data
      const formattedMinutes = hoursMinutesToMinutes(
        parseInt(this.props.currentTimer[i].hours),
        parseInt(this.props.currentTimer[i].minutes)
      );
      // updating needed attributes
      copyOfAaeSelectedUserAccommodationFileData.test_sections_time[i - 1].test_section_time =
        formattedMinutes;
    }
    // ========== TEST SECTIONS TIME (END) ==========

    // updating redux states
    this.props.setAaeSelectedUserAccommodationFileData(copyOfAaeSelectedUserAccommodationFileData);
  };

  getDefaultTotalTestTime = () => {
    // initializing defaultTotalTestTime
    let defaultTotalTestTime = null;
    // test_sections_time is not empty
    if (this.props.aaeSelectedUserAccommodationFileData.default_test_sections_time.length > 0) {
      // setting defaultTotalTestTime to 0
      defaultTotalTestTime = 0;
      // looping in test_sections_time
      for (
        let i = 0;
        i < this.props.aaeSelectedUserAccommodationFileData.default_test_sections_time.length;
        i++
      ) {
        // adding time of current iteration to defaultTotalTestTime
        defaultTotalTestTime +=
          this.props.aaeSelectedUserAccommodationFileData.default_test_sections_time[i]
            .default_time;
      }
      // formatting time
      defaultTotalTestTime = `${getTimeInHoursMinutes(defaultTotalTestTime).formattedHours} : ${
        getTimeInHoursMinutes(defaultTotalTestTime).formattedMinutes
      }`;
    }
    return defaultTotalTestTime;
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div>
        <h2 style={styles.noMargin}>
          {LOCALIZE.formatString(
            LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
              .breakBankAndTimeAdjustment.title,
            typeof this.props.aaeSelectedUserAccommodationFileData.test_to_administer.test !==
              "undefined" &&
              this.props.aaeSelectedUserAccommodationFileData.test_to_administer.test !== null
              ? `[${this.props.aaeSelectedUserAccommodationFileData.test_to_administer.test_code} - v${this.props.aaeSelectedUserAccommodationFileData.test_to_administer.test_version}]`
              : typeof this.props.aaeSelectedUserAccommodationFileData.test_to_administer
                  .specified_test_description !== "undefined" &&
                this.props.aaeSelectedUserAccommodationFileData.test_to_administer
                  .specified_test_description !== ""
              ? `[${this.props.aaeSelectedUserAccommodationFileData.test_to_administer.specified_test_description}]`
              : ""
          )}
        </h2>
        {/* making sure that a test has been selected */}
        {(typeof this.props.aaeSelectedUserAccommodationFileData.test_to_administer.test !==
          "undefined" &&
          this.props.aaeSelectedUserAccommodationFileData.test_to_administer.test !== null) ||
        (typeof this.props.aaeSelectedUserAccommodationFileData.test_to_administer
          .specified_test_description !== "undefined" &&
          this.props.aaeSelectedUserAccommodationFileData.test_to_administer
            .specified_test_description !== null &&
          this.props.aaeSelectedUserAccommodationFileData.test_to_administer
            .specified_test_description !== "") ? (
          <div style={styles.formContainer}>
            <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
              >
                <label
                  htmlFor="user-accommodation-file-test-to-administer"
                  style={styles.inputTitles}
                >
                  {
                    LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                      .sideNavigation.breakBankAndTimeAdjustment.testToAdministerLabel
                  }
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
              >
                <input
                  id="user-accommodation-file-test-to-administer"
                  tabIndex={0}
                  className={"valid-field input-disabled-look"}
                  type="text"
                  value={
                    this.props.aaeSelectedUserAccommodationFileData.test_to_administer.test_code !==
                    null
                      ? `${this.props.aaeSelectedUserAccommodationFileData.test_to_administer.test_code} - v${this.props.aaeSelectedUserAccommodationFileData.test_to_administer.test_version}`
                      : this.props.aaeSelectedUserAccommodationFileData.test_to_administer
                          .specified_test_description
                  }
                  style={{
                    ...styles.inputs,
                    ...accommodationsStyle
                  }}
                  aria-disabled={true}
                />
              </Col>
            </Row>
            <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
              >
                <label
                  htmlFor="user-accommodation-file-default-test-time"
                  style={styles.inputTitles}
                >
                  {
                    LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                      .sideNavigation.breakBankAndTimeAdjustment.defaultTestTimeLabel
                  }
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
              >
                <input
                  id="user-accommodation-file-default-test-time"
                  tabIndex={0}
                  className={"valid-field input-disabled-look"}
                  type="text"
                  value={
                    this.props.aaeSelectedUserAccommodationFileData.default_test_sections_time
                      .length > 0
                      ? this.getDefaultTotalTestTime()
                      : LOCALIZE.commons.na
                  }
                  style={{
                    ...styles.inputs,
                    ...accommodationsStyle
                  }}
                  aria-disabled={true}
                />
              </Col>
            </Row>
            <div style={styles.breakBankAndTimeAdjustmentMainContainer}>
              {/* BREAK BANK */}
              <Row className="align-items-center justify-content-center">
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <h3 style={styles.timerLabel}>
                    {
                      LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                        .sideNavigation.breakBankAndTimeAdjustment.allottedBreakBankLabel
                    }
                  </h3>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <SetTimer
                    index={0}
                    currentHours={
                      getTimeInHoursMinutes(
                        this.props.aaeSelectedUserAccommodationFileData.break_bank
                      ).formattedHours
                    }
                    currentMinutes={
                      getTimeInHoursMinutes(
                        this.props.aaeSelectedUserAccommodationFileData.break_bank
                      ).formattedMinutes
                    }
                    readOnlyMode={userAccommodationFileIsInEndState(
                      this.props.aaeSelectedUserAccommodationFileData.status_codename
                    )}
                  />
                </Col>
              </Row>
            </div>
            <div
              style={{ ...styles.breakBankAndTimeAdjustmentMainContainer, ...styles.noTopPadding }}
            >
              {/* TEST SECTIONS TIME */}
              <h3>
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.breakBankAndTimeAdjustment.testSectionTimers
                }
              </h3>
              {this.props.aaeSelectedUserAccommodationFileData.test_sections_time.map(
                (testSectionData, index) => {
                  return (
                    <Row
                      className="align-items-center justify-content-center"
                      style={styles.rowStyle}
                    >
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <p style={{ ...styles.timerLabel, ...styles.additionalPadding }}>
                          {`${
                            LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                              .sideNavigation.breakBankAndTimeAdjustment.sectionPrefixLabel
                          } ${
                            testSectionData[`title_${this.props.currentLanguage}`] !== null &&
                            typeof testSectionData[`title_${this.props.currentLanguage}`] !==
                              "undefined"
                              ? testSectionData[`title_${this.props.currentLanguage}`]
                              : LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs
                                  .main.sideNavigation.breakBankAndTimeAdjustment
                                  .outsideOfCATSection
                          }`}
                        </p>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      >
                        <SetTimer
                          index={index + 1}
                          currentHours={
                            getTimeInHoursMinutes(testSectionData.test_section_time).formattedHours
                          }
                          currentMinutes={
                            getTimeInHoursMinutes(testSectionData.test_section_time)
                              .formattedMinutes
                          }
                          readOnlyMode={userAccommodationFileIsInEndState(
                            this.props.aaeSelectedUserAccommodationFileData.status_codename
                          )}
                        />
                      </Col>
                    </Row>
                  );
                }
              )}
              {!this.props.validTimer && (
                <div>
                  <p style={styles.errorMessage}>
                    {
                      LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                        .sideNavigation.breakBankAndTimeAdjustment.testSectionTimersError
                    }
                  </p>
                </div>
              )}
            </div>
          </div>
        ) : (
          <div style={styles.systemMessageContainer}>
            <SystemMessage
              messageType={MESSAGE_TYPE.info}
              title={LOCALIZE.commons.info}
              message={
                <p className="notranslate">
                  {
                    LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                      .sideNavigation.breakBankAndTimeAdjustment
                      .noTestSelectedSystemMessageDescription
                  }
                </p>
              }
            />
          </div>
        )}
      </div>
    );
  }
}

export { SelectedUserAccommodationFileMainTabBreakBankAndTimeAdjustment as unconnectedSelectedUserAccommodationFileMainTabBreakBankAndTimeAdjustment };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    aaeInitialSelectedUserAccommodationFileData:
      state.aae.aaeInitialSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileData: state.aae.aaeSelectedUserAccommodationFileData,
    currentTimer: state.timer.currentTimer,
    validTimer: state.timer.validTimer,
    timeUpdated: state.timer.timeUpdated
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setAaeSelectedUserAccommodationFileData,
      setAaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectedUserAccommodationFileMainTabBreakBankAndTimeAdjustment);
