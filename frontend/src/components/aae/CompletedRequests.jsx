import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { history } from "../../store-index";
import LOCALIZE from "../../text_resources";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import Pagination from "../commons/Pagination";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import SearchBarWithDisplayOptions from "../commons/SearchBarWithDisplayOptions";
import {
  updateCurrentAaeCompletedRequestsPageState,
  updateAaeCompletedRequestsPageSizeState,
  updateSearchAaeCompletedRequestsStates,
  getAllUserAccommodationFiles,
  getFoundUserAccommodationFiles,
  getSelectedUserAccommodationFileData,
  setAaeInitialSelectedUserAccommodationFileData,
  setAaeSelectedUserAccommodationFileData,
  setAaeSelectedUserAccommodationFileTestToAdministerValidState
} from "../../modules/AaeRedux";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars } from "@fortawesome/free-solid-svg-icons";
import { PATH } from "../commons/Constants";
import { updateTimerState } from "../../modules/SetTimerRedux";
import { getTimeInHoursMinutes } from "../../helpers/timeConversion";

const styles = {
  allUnset: {
    all: "unset"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  }
};

class CompletedRequests extends Component {
  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    resultsFound: 0,
    completedRequests: [],
    clearSearchTriggered: false
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentAaeCompletedRequestsPageState(1);
    // populating associated test administrators
    this.populateCompletedRequestsBasedOnActiveSearch();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateCompletedRequestsBasedOnActiveSearch();
    }
    // if aaeCompletedRequestsPaginationPage gets updated
    if (
      prevProps.aaeCompletedRequestsPaginationPage !== this.props.aaeCompletedRequestsPaginationPage
    ) {
      this.populateCompletedRequestsBasedOnActiveSearch();
    }
    // if aaeCompletedRequestsPaginationPageSize get updated
    if (
      prevProps.aaeCompletedRequestsPaginationPageSize !==
      this.props.aaeCompletedRequestsPaginationPageSize
    ) {
      this.populateCompletedRequestsBasedOnActiveSearch();
    }
    // if search aaeCompletedRequestsKeyword gets updated
    if (prevProps.aaeCompletedRequestsKeyword !== this.props.aaeCompletedRequestsKeyword) {
      // if aaeCompletedRequestsKeyword redux state is empty
      if (this.props.aaeCompletedRequestsKeyword !== "") {
        this.populateFoundCompletedRequests();
      } else if (
        this.props.aaeCompletedRequestsKeyword === "" &&
        this.props.aaeCompletedRequestsActiveSearch
      ) {
        this.populateFoundCompletedRequests();
      }
    }
    // if aaeCompletedRequestsActiveSearch gets updated
    if (
      prevProps.aaeCompletedRequestsActiveSearch !== this.props.aaeCompletedRequestsActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.aaeCompletedRequestsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateCompletedRequests();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundCompletedRequests();
      }
    }
  };

  populateCompletedRequestsBasedOnActiveSearch = () => {
    // current search
    if (this.props.aaeCompletedRequestsActiveSearch) {
      this.populateFoundCompletedRequests();
      // no search
    } else {
      this.populateCompletedRequests();
    }
  };

  // populating completed accommodation requests
  populateCompletedRequests = () => {
    this.setState({ currentlyLoading: true }, () => {
      const completedRequestsArray = [];
      this.props
        .getAllUserAccommodationFiles(
          this.props.aaeCompletedRequestsPaginationPage,
          this.props.aaeCompletedRequestsPaginationPageSize,
          false
        )
        .then(response => {
          this.populateAccommodationRequestsCompletedRequestsObject(
            completedRequestsArray,
            response
          );
        })
        .then(() => {
          if (this.state.clearSearchTriggered) {
            // go back to the first page to avoid display bugs
            this.props.updateCurrentAaeCompletedRequestsPageState(1);
            this.setState({ clearSearchTriggered: false });
          }
        })
        .then(() => {
          this.setState({ currentlyLoading: false });
        });
    });
  };

  // populating found completed accommodation requests
  populateFoundCompletedRequests = () => {
    // making sure that the table is not currently loading
    if (!this.state.currentlyLoading) {
      this.setState(
        { currentlyLoading: true },
        () => {
          const completedRequestsArray = [];
          setTimeout(() => {
            this.props
              .getFoundUserAccommodationFiles(
                this.props.aaeCompletedRequestsKeyword,
                this.props.currentLanguage,
                this.props.aaeCompletedRequestsPaginationPage,
                this.props.aaeCompletedRequestsPaginationPageSize,
                false
              )
              .then(response => {
                // there are no results found
                if (response[0] === "no results found") {
                  this.setState({
                    testSesions: [],
                    rowsDefinition: {},
                    numberOfPages: 1,
                    nextPageNumber: 0,
                    previousPageNumber: 0,
                    resultsFound: 0
                  });
                  // there is at least one result found
                } else {
                  this.populateAccommodationRequestsCompletedRequestsObject(
                    completedRequestsArray,
                    response
                  );
                  this.setState({ resultsFound: response.count });
                }
              })
              .then(() => {
                this.setState(
                  {
                    currentlyLoading: false
                  },
                  () => {
                    // make sure that this element exsits to avoid any error
                    if (
                      document.getElementById(
                        "accommodation-requests-completed-requests-results-found"
                      )
                    ) {
                      document
                        .getElementById("accommodation-requests-completed-requests-results-found")
                        .focus();
                    }
                  }
                );
              });
          });
        },
        250
      );
    }
  };

  populateAccommodationRequestsCompletedRequestsObject = (completedRequestsArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];

        // Add the item to completed requests array
        completedRequestsArray.push(currentResult);

        // getting formatted test skill / sub-skill
        let formattedTestSkill = `${
          currentResult[`test_skill_type_name_${this.props.currentLanguage}`]
        }`;
        if (currentResult.test_skill_sub_type_id !== null) {
          formattedTestSkill = formattedTestSkill.concat(
            ` - ${currentResult[`test_skill_sub_type_name_${this.props.currentLanguage}`]}`
          );
        }

        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult.id,
          column_2: currentResult.user_id,
          column_3: `${currentResult.user_last_name}, ${currentResult.user_first_name}`,
          column_4: `${
            currentResult.is_uit
              ? LOCALIZE.commons.testType.uit
              : LOCALIZE.commons.testType.supervised
          }`,
          column_5: `${currentResult.created_date}`,
          column_6: currentResult[`status_name_${this.props.currentLanguage}`],
          column_7:
            currentResult.assigned_to_user_id !== null
              ? `${currentResult.assigned_to_user_last_name}, ${currentResult.assigned_to_user_first_name}`
              : LOCALIZE.commons.na,
          column_8: formattedTestSkill,
          column_9: (
            <>
              <StyledTooltip
                id={`user-accommodation-file-completed-requests-view-row-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`user-accommodation-file-completed-requests-view-row-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faBinoculars} />
                        </>
                      }
                      action={() => this.viewSelectedUserAccommodationFile(currentResult)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.aae.sideNav.accommodationRequests.topTabs.completedRequets.table
                          .viewButtonAccessibility,
                        currentResult.user_first_name,
                        currentResult.user_last_name
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.aae.sideNav.accommodationRequests.topTabs.completedRequets.table
                          .viewButtonTooltip
                      }
                    </p>
                  </div>
                }
              />
            </>
          )
        });
      }
    }
    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.CENTERED_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      column_6_style: COMMON_STYLE.CENTERED_TEXT,
      column_7_style: COMMON_STYLE.CENTERED_TEXT,
      column_8_style: COMMON_STYLE.CENTERED_TEXT,
      column_9_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };
    // saving results in state
    this.setState({
      completedRequests: completedRequestsArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(response.count / this.props.aaeCompletedRequestsPaginationPageSize),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  viewSelectedUserAccommodationFile = userAccommodationFileData => {
    // getting selected user accommodation file data
    this.props
      .getSelectedUserAccommodationFileData(userAccommodationFileData.id)
      .then(response => {
        // initializing timerState with only the break bank value
        const timerState = [];

        // adding break bank value to timerState array
        timerState.push({
          hours: getTimeInHoursMinutes(response.break_bank).formattedHours,
          minutes: getTimeInHoursMinutes(response.break_bank).formattedMinutes
        });

        // adding test sections time values to timerState array
        for (let i = 0; i < response.test_sections_time.length; i++) {
          timerState.push({
            hours: getTimeInHoursMinutes(response.test_sections_time[i].test_section_time)
              .formattedHours,
            minutes: getTimeInHoursMinutes(response.test_sections_time[i].test_section_time)
              .formattedMinutes
          });
        }

        // setting redux states
        this.props.setAaeInitialSelectedUserAccommodationFileData(response);
        this.props.setAaeSelectedUserAccommodationFileData(response);
        this.props.setAaeSelectedUserAccommodationFileTestToAdministerValidState(true);
        this.props.updateTimerState(timerState);
      })
      .then(() => {
        // redirecting user to selected user accommodation file page
        history.push(PATH.aaeSelectedUserAccommodationFile);
      });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.completedRequets.table.columnOne,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.completedRequets.table.columnTwo,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.aae.sideNav.accommodationRequests.topTabs.completedRequets.table.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.completedRequets.table.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.completedRequets.table.columnFive,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.completedRequets.table.columnSix,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.aae.sideNav.accommodationRequests.topTabs.completedRequets.table.columnSeven,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.aae.sideNav.accommodationRequests.topTabs.completedRequets.table.columnEight,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.completedRequets.table.columnNine,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"accommodation-requests-completed-requests"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchAaeCompletedRequestsStates}
            updatePageState={this.props.updateCurrentAaeCompletedRequestsPageState}
            paginationPageSize={Number(this.props.aaeCompletedRequestsPaginationPageSize)}
            updatePaginationPageSize={this.props.updateAaeCompletedRequestsPageSizeState}
            data={this.state.pendingRequests}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.aaeCompletedRequestsKeyword}
          />
          <div>
            <GenericTable
              classnamePrefix="accommodation-requests-completed-requests"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.aae.sideNav.accommodationRequests.topTabs.completedRequets.table.noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <Pagination
            paginationContainerId={
              "accommodation-requests-completed-requests-pagination-pages-link"
            }
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.aaeCompletedRequestsPaginationPage}
            updatePaginationPageState={this.props.updateCurrentAaeCompletedRequestsPageState}
            firstTableRowId={"accommodation-requests-completed-requests-table-row-0"}
          />
        </div>
      </div>
    );
  }
}

export { CompletedRequests as unconnectedCompletedRequests };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    aaeCompletedRequestsPaginationPage: state.aae.aaeCompletedRequestsPaginationPage,
    aaeCompletedRequestsPaginationPageSize: state.aae.aaeCompletedRequestsPaginationPageSize,
    aaeCompletedRequestsKeyword: state.aae.aaeCompletedRequestsKeyword,
    aaeCompletedRequestsActiveSearch: state.aae.aaeCompletedRequestsActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateCurrentAaeCompletedRequestsPageState,
      updateAaeCompletedRequestsPageSizeState,
      updateSearchAaeCompletedRequestsStates,
      getAllUserAccommodationFiles,
      getFoundUserAccommodationFiles,
      getSelectedUserAccommodationFileData,
      setAaeInitialSelectedUserAccommodationFileData,
      setAaeSelectedUserAccommodationFileData,
      setAaeSelectedUserAccommodationFileTestToAdministerValidState,
      updateTimerState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CompletedRequests);
