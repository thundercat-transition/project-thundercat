/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Col, Row } from "react-bootstrap";
import getSelectedUserAccommodationFileHeightCalculations from "../../helpers/aaeHeightCalculations";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle, faSpinner, faTimes, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../commons/PopupBox";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import {
  createUserAccommodationFileComment,
  deleteUserAccommodationFileComment,
  setAaeInitialSelectedUserAccommodationFileData,
  setAaeSelectedUserAccommodationFileData
} from "../../modules/AaeRedux";

const columnSizes = {
  commentSectionFirstColumn: {
    xs: 11,
    sm: 11,
    md: 11,
    lg: 11,
    xl: 11
  },
  commentSectionSecondColumn: {
    xs: 1,
    sm: 1,
    md: 1,
    lg: 1,
    xl: 1
  },
  popupFirstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  },
  popupSecondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  }
};

const styles = {
  mainContainer: {
    padding: "24px 12px",
    overflowY: "auto",
    backgroundColor: "#FAFAFA",
    borderLeft: "1px solid #CDCDCD",
    borderRight: "1px solid #CDCDCD"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  addCommentBtnContainer: {
    marginLeft: 12
  },
  deleteCommentButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  allUnset: {
    all: "unset"
  },
  italic: {
    fontStyle: "italic"
  },
  commentsDeleteButtonTableRow: {
    padding: 0,
    textAlign: "center"
  },
  commentsContainer: {
    margin: "24px 0"
  },
  itemContainer: {
    width: "100%",
    margin: "12px 0"
  },
  labelContainer: {
    verticalAlign: "middle"
  },
  inputContainer: {
    verticalAlign: "middle"
  },
  label: {
    paddingRight: 12,
    margin: 0
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  textAreaInput: {
    minHeight: 80,
    resize: "none",
    width: "100%"
  },
  bold: {
    fontWeight: "bold"
  }
};

class SelectedUserAccommodationFileCommentsTab extends Component {
  state = {
    triggerRerender: false,
    showAddCommentPopup: false,
    commentContent: "",
    addCommentLoading: false,
    showDeleteCommentPopup: false,
    commentDataToBeDeleted: {},
    deleteCommentLoading: false
  };

  openAddCommentPopup = () => {
    this.setState({ showAddCommentPopup: true });
  };

  closeAddCommentPopup = () => {
    this.setState({ showAddCommentPopup: false, commentContent: "" });
  };

  updateCommentContent = event => {
    const commentContent = event.target.value;
    // allow maximum of 255 characters
    if (commentContent.length <= 255) {
      this.setState({
        commentContent: commentContent
      });
    }
  };

  handleAddComment = () => {
    this.setState({ addCommentLoading: true }, () => {
      const body = {
        user_accommodation_file_id: this.props.aaeSelectedUserAccommodationFileData.id,
        comment: this.state.commentContent
      };
      this.props.createUserAccommodationFileComment(body).then(response => {
        if (response.ok) {
          // updating the redux states
          // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
          const new_obj = {
            ...this.props.aaeSelectedUserAccommodationFileData,
            file_comments: response.file_comments
          };

          // setting new redux state
          this.props.setAaeSelectedUserAccommodationFileData(new_obj);
          this.props.setAaeInitialSelectedUserAccommodationFileData(new_obj);

          this.setState(
            {
              showAddCommentPopup: false,
              commentContent: ""
            },
            () => {
              setTimeout(() => {
                this.setState({ addCommentLoading: false });
              }, 100);
            }
          );
          // should never happen
        } else {
          throw new Error(
            "An error occurred during the create user accommodation file comment process"
          );
        }
      });
    });
  };

  openDeleteCommentPopup = data => {
    this.setState({ showDeleteCommentPopup: true, commentDataToBeDeleted: data });
  };

  closeDeleteCommentPopup = () => {
    this.setState({ showDeleteCommentPopup: false, commentDataToBeDeleted: {} });
  };

  handleDeleteComment = () => {
    this.setState({ deleteCommentLoading: true });
    const body = {
      user_accommodation_file_id: this.props.aaeSelectedUserAccommodationFileData.id,
      comment_id: this.state.commentDataToBeDeleted.id
    };
    this.props.deleteUserAccommodationFileComment(body).then(response => {
      if (response.ok) {
        // updating the redux states
        // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
        const new_obj = {
          ...this.props.aaeSelectedUserAccommodationFileData,
          file_comments: response.file_comments
        };

        // setting new redux state
        this.props.setAaeSelectedUserAccommodationFileData(new_obj);
        this.props.setAaeInitialSelectedUserAccommodationFileData(new_obj);

        this.setState(
          {
            showDeleteCommentPopup: false,
            commentDataToBeDeleted: {}
          },
          () => {
            setTimeout(() => {
              this.setState({ deleteCommentLoading: false });
            }, 100);
          }
        );

        // should never happen
      } else {
        throw new Error(
          "An error occurred during the delete user accommodation file comment process"
        );
      }
    });
  };

  render() {
    // calculating the height of the top tabs and above
    // =========================================================================================
    let heightOfBackToUserAccommodationFileSelectionButton = 0;
    let heightOfTopTabs = 0;
    let heightOfFooter = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-user-accommodation-file-selection-btn") != null) {
      heightOfBackToUserAccommodationFileSelectionButton = document.getElementById(
        "back-to-user-accommodation-file-selection-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight += heightOfBackToUserAccommodationFileSelectionButton + heightOfTopTabs + 14; // height of "back to user accommodation file selection button" + height of margin under button

    if (document.getElementById("selected-user-accommodation-file-footer-main-div") !== null) {
      heightOfFooter = document.getElementById(
        "selected-user-accommodation-file-footer-main-div"
      ).offsetHeight;
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    const customHeight = getSelectedUserAccommodationFileHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight,
      heightOfFooter
    );
    // =========================================================================================

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={{ ...styles.mainContainer, ...customHeight }}>
        <div style={styles.addCommentBtnContainer}>
          <CustomButton
            label={
              <>
                <FontAwesomeIcon icon={faPlusCircle} />
                <span id="add-comment-btn" style={styles.buttonLabel} className="notranslate">
                  {
                    LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.other
                      .comments.addCommentButton
                  }
                </span>
              </>
            }
            action={this.openAddCommentPopup}
            buttonTheme={THEME.PRIMARY}
          />
        </div>
        <div style={styles.commentsContainer}>
          {this.props.aaeSelectedUserAccommodationFileData.file_comments.map(data => {
            return (
              <div style={styles.allUnset}>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.itemContainer}
                >
                  <Col
                    xl={columnSizes.commentSectionFirstColumn.xl}
                    lg={columnSizes.commentSectionFirstColumn.lg}
                    md={columnSizes.commentSectionFirstColumn.md}
                    sm={columnSizes.commentSectionFirstColumn.sm}
                    xs={columnSizes.commentSectionFirstColumn.xs}
                    style={styles.commentsTableRow}
                  >
                    <p>
                      <span style={styles.bold}>{`${
                        data.modify_date.split(".")[0].split("T")[0]
                      } - [${data.user_last_name}, ${data.user_first_name}]:`}</span>
                      <br />
                      <span style={styles.italic}>{data.comment}</span>
                    </p>
                  </Col>
                  <Col
                    xl={columnSizes.commentSectionSecondColumn.xl}
                    lg={columnSizes.commentSectionSecondColumn.lg}
                    md={columnSizes.commentSectionSecondColumn.md}
                    sm={columnSizes.commentSectionSecondColumn.sm}
                    xs={columnSizes.commentSectionSecondColumn.xs}
                    style={styles.commentsDeleteButtonTableRow}
                  >
                    <StyledTooltip
                      id={`delete-user-accommodation-file-comment-tooltip-${data.id}`}
                      place="top"
                      variant={TYPE.light}
                      effect={EFFECT.solid}
                      openOnClick={false}
                      disabled={data.username !== this.props.username}
                      tooltipElement={
                        <CustomButton
                          dataTip=""
                          dataFor={`delete-user-accommodation-file-comment-tooltip-${data.id}`}
                          label={
                            <>
                              <FontAwesomeIcon icon={faTrashAlt} style={styles.buttonIcon} />
                            </>
                          }
                          customStyle={styles.deleteCommentButton}
                          action={() => this.openDeleteCommentPopup(data)}
                          buttonTheme={THEME.PRIMARY}
                          disabled={
                            this.props.isSuperUser ? false : data.user !== this.props.userId
                          }
                        />
                      }
                      tooltipContent={
                        <div>
                          <p>{LOCALIZE.commons.deleteButton}</p>
                        </div>
                      }
                    />
                  </Col>
                </Row>
              </div>
            );
          })}
        </div>
        <PopupBox
          show={this.state.showAddCommentPopup}
          handleClose={() => {}}
          size="lg"
          title={
            LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.other.comments
              .addCommentPopup.title
          }
          description={
            <div>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.popupFirstColumn.xl}
                  lg={columnSizes.popupFirstColumn.lg}
                  md={columnSizes.popupFirstColumn.md}
                  sm={columnSizes.popupFirstColumn.sm}
                  xs={columnSizes.popupFirstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label id="user-accommodation-file-comment-label">
                      {
                        LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.other
                          .comments.addCommentPopup.commentLabel
                      }
                    </label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.popupSecondColumn.xl}
                  lg={columnSizes.popupSecondColumn.lg}
                  md={columnSizes.popupSecondColumn.md}
                  sm={columnSizes.popupSecondColumn.sm}
                  xs={columnSizes.popupSecondColumn.xs}
                  style={styles.inputContainer}
                >
                  <textarea
                    id="user-accommodation-file-comment"
                    className={"valid-field"}
                    aria-labelledby="user-accommodation-file-comment-label"
                    aria-required={true}
                    style={{ ...styles.input, ...styles.textAreaInput, ...accommodationsStyle }}
                    value={this.state.commentContent}
                    onChange={this.updateCommentContent}
                    maxLength="255"
                  ></textarea>
                </Col>
              </Row>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeAddCommentPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            !this.state.addCommentLoading ? (
              LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.other.comments
                .addCommentPopup.addButton
            ) : (
              // eslint-disable-next-line jsx-a11y/label-has-associated-control
              <label className="fa fa-spinner fa-spin">
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            )
          }
          rightButtonAction={this.handleAddComment}
          rightButtonIcon={!this.state.addCommentLoading ? faPlusCircle : ""}
          rightButtonState={
            this.state.addCommentLoading
              ? BUTTON_STATE.disabled
              : this.state.commentContent !== ""
                ? BUTTON_STATE.enabled
                : BUTTON_STATE.disabled
          }
        />
        {Object.keys(this.state.commentDataToBeDeleted).length > 0 && (
          <PopupBox
            show={this.state.showDeleteCommentPopup}
            handleClose={() => {}}
            size="lg"
            title={
              LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.other.comments
                .deleteCommentPopup.title
            }
            description={
              <div>
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.warning}
                  message={
                    <>
                      <p>
                        {
                          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.other
                            .comments.deleteCommentPopup.systemMessageDescription1
                        }
                      </p>
                      <p style={{ ...styles.leftMargin, ...styles.italic }}>
                        {LOCALIZE.formatString(
                          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.other
                            .comments.deleteCommentPopup.systemMessageDescription2,
                          this.state.commentDataToBeDeleted.comment
                        )}
                      </p>
                    </>
                  }
                />
              </div>
            }
            leftButtonType={BUTTON_TYPE.secondary}
            leftButtonTitle={LOCALIZE.commons.cancel}
            leftButtonIcon={faTimes}
            leftButtonAction={this.closeDeleteCommentPopup}
            rightButtonType={BUTTON_TYPE.danger}
            rightButtonTitle={
              !this.state.deleteCommentLoading ? (
                LOCALIZE.commons.deleteButton
              ) : (
                // eslint-disable-next-line jsx-a11y/label-has-associated-control
                <label className="fa fa-spinner fa-spin">
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              )
            }
            rightButtonAction={this.handleDeleteComment}
            rightButtonIcon={!this.state.deleteCommentLoading ? faTrashAlt : ""}
            rightButtonState={
              this.state.deleteCommentLoading ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
            }
          />
        )}
      </div>
    );
  }
}

export { SelectedUserAccommodationFileCommentsTab as unconnectedSelectedUserAccommodationFileCommentsTab };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    isSuperUser: state.user.isSuperUser,
    userId: state.user.user_id,
    testNavBarHeight: state.testSection.testNavBarHeight,
    aaeInitialSelectedUserAccommodationFileData:
      state.aae.aaeInitialSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileData: state.aae.aaeSelectedUserAccommodationFileData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createUserAccommodationFileComment,
      deleteUserAccommodationFileComment,
      setAaeSelectedUserAccommodationFileData,
      setAaeInitialSelectedUserAccommodationFileData
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectedUserAccommodationFileCommentsTab);
