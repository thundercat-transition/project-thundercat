/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Col } from "react-bootstrap";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { LANGUAGES } from "../../modules/LocalizeRedux";
import DatePicker from "../commons/DatePicker";
import InputMask from "react-input-mask";
import { MASKS, MASK_FORMAT_CHARS } from "../commons/InputMask/helpers";
import { TestSkillSleDescCodename } from "../testFactory/Constants";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 8
  }
};

const styles = {
  noMargin: {
    margin: 0
  },
  formContainer: {
    padding: 12
  },
  rowStyle: {
    padding: "6px 0px"
  },
  inputTitles: {
    fontWeight: "bold"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  textAreaInput: {
    width: "100%",
    height: 115,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block"
  }
};

class SelectedUserAccommodationFileMainTabCandidateData extends Component {
  state = {};

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div>
        <h2 style={styles.noMargin}>
          {
            LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
              .candidateData.title
          }
        </h2>
        <div style={styles.formContainer}>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label htmlFor="user-accommodation-file-user-id" style={styles.inputTitles}>
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.candidateData.userIdLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-user-id"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={this.props.aaeSelectedUserAccommodationFileData.user_id}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label htmlFor="user-accommodation-file-user-first-name" style={styles.inputTitles}>
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.candidateData.userFirstNameLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-user-first-name"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={`${this.props.aaeSelectedUserAccommodationFileData.user_first_name}`}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label htmlFor="user-accommodation-file-user-last-name" style={styles.inputTitles}>
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.candidateData.userLastNameLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-user-last-name"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={`${this.props.aaeSelectedUserAccommodationFileData.user_last_name}`}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label htmlFor="user-accommodation-file-user-email" style={styles.inputTitles}>
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.candidateData.userEmailLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-user-email"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={this.props.aaeSelectedUserAccommodationFileData.user_email}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                htmlFor="user-accommodation-file-user-secondary-email"
                style={styles.inputTitles}
              >
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.candidateData.userSecondaryEmailLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-user-secondary-email"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={this.props.aaeSelectedUserAccommodationFileData.user_secondary_email}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label id="user-accommodation-file-user-dob" style={styles.inputTitles}>
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.candidateData.userDobLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <DatePicker
                ariaLabelledBy={"user-accommodation-file-user-dob"}
                dateLabelId={"user-accommodation-file-user-dob-id"}
                initialDateDayValue={{
                  label: this.props.aaeSelectedUserAccommodationFileData.user_dob.split("-")[2],
                  value: this.props.aaeSelectedUserAccommodationFileData.user_dob.split("-")[2]
                }}
                initialDateMonthValue={{
                  label: this.props.aaeSelectedUserAccommodationFileData.user_dob.split("-")[1],
                  value: this.props.aaeSelectedUserAccommodationFileData.user_dob.split("-")[1]
                }}
                initialDateYearValue={{
                  label: this.props.aaeSelectedUserAccommodationFileData.user_dob.split("-")[0],
                  value: this.props.aaeSelectedUserAccommodationFileData.user_dob.split("-")[0]
                }}
                disabledDropdowns={true}
                displayValidIcon={false}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label htmlFor="user-accommodation-file-user-phone-number" style={styles.inputTitles}>
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.candidateData.userPhoneNumberLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <InputMask
                id="user-accommodation-file-user-phone-number"
                className={"valid-field input-disabled-look"}
                style={{ ...styles.inputs, ...accommodationsStyle }}
                type="text"
                value={this.props.aaeSelectedUserAccommodationFileData.user_phone_number}
                mask={MASKS.phoneNumber}
                formatChars={MASK_FORMAT_CHARS.phoneNumber}
              />
            </Col>
          </Row>
          {/* OLA TEST */}
          {(this.props.aaeSelectedUserAccommodationFileData.test_skill_sub_type_codename ===
            TestSkillSleDescCodename.ORAL_EN ||
            this.props.aaeSelectedUserAccommodationFileData.test_skill_sub_type_codename ===
              TestSkillSleDescCodename.ORAL_FR) && (
            <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
              >
                <label
                  htmlFor="user-accommodation-file-ola-phone-number"
                  style={styles.inputTitles}
                >
                  {
                    LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                      .sideNavigation.candidateData.olaPhoneNumberLabel
                  }
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
              >
                <InputMask
                  id="user-accommodation-file-ola-phone-number"
                  className={"valid-field input-disabled-look"}
                  style={{ ...styles.inputs, ...accommodationsStyle }}
                  type="text"
                  value={this.props.aaeSelectedUserAccommodationFileData.ola_phone_number}
                  mask={MASKS.phoneNumber}
                  formatChars={MASK_FORMAT_CHARS.phoneNumber}
                />
              </Col>
            </Row>
          )}
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                htmlFor="user-accommodation-file-user-psrs-applicant-id"
                style={styles.inputTitles}
              >
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.candidateData.userPsrsApplicantIdLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-user-psrs-applicant-id"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={this.props.aaeSelectedUserAccommodationFileData.user_psrs_applicant_id}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label htmlFor="user-accommodation-file-user-pri" style={styles.inputTitles}>
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.candidateData.userPriLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-user-pri"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={this.props.aaeSelectedUserAccommodationFileData.user_pri}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label htmlFor="user-accommodation-file-user-military-nbr" style={styles.inputTitles}>
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.candidateData.userMilitaryNbrLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-user-military-nbr"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={this.props.aaeSelectedUserAccommodationFileData.user_military_nbr}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label htmlFor="user-accommodation-file-reference-number" style={styles.inputTitles}>
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.candidateData.referenceNumberLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-reference-number"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={this.props.aaeSelectedUserAccommodationFileData.reference_number}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label htmlFor="user-accommodation-file-department" style={styles.inputTitles}>
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.candidateData.departmentLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-department"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={
                  this.props.currentLanguage === LANGUAGES.english
                    ? `${this.props.aaeSelectedUserAccommodationFileData.dept_edesc} (${this.props.aaeSelectedUserAccommodationFileData.dept_eabrv})`
                    : `${this.props.aaeSelectedUserAccommodationFileData.dept_fdesc} (${this.props.aaeSelectedUserAccommodationFileData.dept_fabrv})`
                }
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label htmlFor="user-accommodation-file-primary-contact" style={styles.inputTitles}>
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.candidateData.primaryContactLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-primary-contact"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={
                  this.props.aaeSelectedUserAccommodationFileData.primary_contact_user_id !== null
                    ? `${this.props.aaeSelectedUserAccommodationFileData.primary_contact_user_last_name}, ${this.props.aaeSelectedUserAccommodationFileData.primary_contact_user_first_name} (${this.props.aaeSelectedUserAccommodationFileData.primary_contact_user_email})`
                    : ""
                }
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                htmlFor="user-accommodation-file-candidate-comments"
                style={styles.inputTitles}
              >
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.candidateData.candidateCommentsLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <textarea
                id="user-accommodation-file-candidate-comments"
                className={"valid-field input-disabled-look"}
                style={{ ...styles.textAreaInput, ...accommodationsStyle }}
                value={this.props.aaeSelectedUserAccommodationFileData.comments}
                maxLength="5000"
                aria-disabled={true}
              ></textarea>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export { SelectedUserAccommodationFileMainTabCandidateData as unconnectedSelectedUserAccommodationFileMainTabCandidateData };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    aaeInitialSelectedUserAccommodationFileData:
      state.aae.aaeInitialSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileData: state.aae.aaeSelectedUserAccommodationFileData
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectedUserAccommodationFileMainTabCandidateData);
