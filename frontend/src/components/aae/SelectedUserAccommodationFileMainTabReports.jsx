/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { getAccommodationFileAvailableReports } from "../../modules/AaeRedux";
import * as _ from "lodash";
import { Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars } from "@fortawesome/free-solid-svg-icons";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import AccommodationsRequestDetailsPopup, {
  ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE
} from "./AccommodationsRequestDetailsPopup";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 7
  }
};

const styles = {
  noMargin: {
    margin: 0
  },
  formContainer: {
    padding: 12
  },
  rowStyle: {
    padding: "6px 0px"
  },
  inputTitles: {
    fontWeight: "bold",
    margin: 0
  },
  tableContainer: {
    padding: "24px 0"
  },
  iconLabel: {
    marginRight: 6
  },
  boldText: {
    fontWeight: "bold"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  allUnset: {
    all: "unset"
  }
};

class SelectedUserAccommodationFileMainTabReports extends Component {
  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    previousReportsData: [],
    showAccommodationsRequestDetailsPopup: false,
    selectedAccommodationRequestData: {},
    getHistoricalData: false,
    isDraft: false
  };

  componentDidMount = () => {
    // populating table
    this.populatePreviousReportsTable();
  };

  componentDidUpdate = prevProps => {
    // if status gets updated
    if (
      prevProps.aaeSelectedUserAccommodationFileData.status_id !==
      this.props.aaeSelectedUserAccommodationFileData.status_id
    ) {
      // populating table
      this.populatePreviousReportsTable();
    }
  };

  populatePreviousReportsTable = () => {
    this.setState({ currentlyLoading: true }, () => {
      this.props
        .getAccommodationFileAvailableReports(this.props.aaeSelectedUserAccommodationFileData.id)
        .then(response => {
          // initializing needed object and array for rowsDefinition state (needed for GenericTable component)
          let rowsDefinition = {};
          const data = [];
          const previousReportsData = response;
          for (let i = 0; i < response.length; i++) {
            // getting formatted test skill
            const formattedTestSkill =
              response[i].test_skill_sub_type_id !== null
                ? `${response[i][`test_skill_type_name_${this.props.currentLanguage}`]} - ${
                    response[i][`test_skill_sub_type_name_${this.props.currentLanguage}`]
                  }`
                : `${response[i][`test_skill_type_name_${this.props.currentLanguage}`]}`;
            // pushing needed data in data array (needed for GenericTable component)
            data.push({
              column_1: response[i].history_date.split("T")[0],
              column_2:
                response[i].test_code !== null
                  ? response[i][`test_name_${this.props.currentLanguage}`]
                  : response[i].specified_test_description,
              column_3: formattedTestSkill,
              column_4: (
                <StyledTooltip
                  id={`previous-reports-view-row-${i}`}
                  place="top"
                  variant={TYPE.light}
                  effect={EFFECT.solid}
                  openOnClick={false}
                  tooltipElement={
                    <div style={styles.allUnset}>
                      <CustomButton
                        dataTip=""
                        dataFor={`previous-reports-view-row-${i}`}
                        label={
                          <>
                            <FontAwesomeIcon icon={faBinoculars} />
                          </>
                        }
                        action={() => {
                          this.handleViewPreviousReportData(response[i]);
                        }}
                        customStyle={styles.actionButton}
                        buttonTheme={THEME.PRIMARY}
                        ariaLabel={LOCALIZE.formatString(
                          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                            .sideNavigation.reports.previousReportsTable.viewButtonAccessibility,
                          response[i].history_date.split("T")[0],
                          response[i][`test_name_${this.props.currentLanguage}`]
                        )}
                      />
                    </div>
                  }
                  tooltipContent={
                    <div>
                      <p>
                        {
                          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                            .sideNavigation.reports.previousReportsTable.viewButton
                        }
                      </p>
                    </div>
                  }
                />
              )
            });
          }

          // updating rowsDefinition object with provided data and needed style
          rowsDefinition = {
            column_1_style: COMMON_STYLE.CENTERED_TEXT,
            column_2_style: COMMON_STYLE.CENTERED_TEXT,
            column_3_style: COMMON_STYLE.CENTERED_TEXT,
            column_4_style: COMMON_STYLE.CENTERED_TEXT,
            data: data
          };

          // setting needed states
          this.setState({
            rowsDefinition: rowsDefinition,
            previousReportsData: previousReportsData,
            currentlyLoading: false
          });
        });
    });
  };

  viewDraftReport = () => {
    // updating needed states
    this.setState({
      showAccommodationsRequestDetailsPopup: true,
      selectedAccommodationRequestData: this.props.aaeSelectedUserAccommodationFileData,
      getHistoricalData: false,
      isDraft: true
    });
  };

  closeAccommodationsRequestDetailsPopup = () => {
    this.setState({
      showAccommodationsRequestDetailsPopup: false,
      selectedAccommodationRequestData: {},
      getHistoricalData: false,
      isDraft: false
    });
  };

  handleViewPreviousReportData = reportData => {
    // updating needed states
    this.setState({
      showAccommodationsRequestDetailsPopup: true,
      selectedAccommodationRequestData: reportData,
      getHistoricalData: true,
      isDraft: false
    });
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
            .reports.previousReportsTable.columnOne,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
            .reports.previousReportsTable.columnTwo,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
            .reports.previousReportsTable.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
            .reports.previousReportsTable.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    return (
      <div>
        <h2 style={styles.noMargin}>
          {
            LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
              .reports.title
          }
        </h2>
        <div style={styles.formContainer}>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                htmlFor="user-accommodation-file-file-administration-request-id-input"
                style={styles.inputTitles}
              >
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.reports.previewCurrentFileDataLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <CustomButton
                label={
                  <>
                    <FontAwesomeIcon icon={faBinoculars} style={styles.iconLabel} />
                    <span className="notranslate">
                      {
                        LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                          .sideNavigation.reports.viewDraftReportButton
                      }
                    </span>
                  </>
                }
                action={this.viewDraftReport}
                buttonTheme={THEME.SECONDARY}
                customStyle={styles.buttonMinWidth}
                ariaLabel={
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.reports.viewDraftReportButton
                }
                disabled={false}
              />
            </Col>
          </Row>
          <div style={styles.tableContainer}>
            <p style={styles.boldText}>
              {
                LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                  .sideNavigation.reports.previousReportsLabel
              }
            </p>
            <GenericTable
              classnamePrefix="user-accommodation-file-reports-previous-reports"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                  .sideNavigation.reports.previousReportsTable.noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
        </div>
        {Object.keys(this.state.selectedAccommodationRequestData).length > 0 &&
          this.state.showAccommodationsRequestDetailsPopup && (
            <AccommodationsRequestDetailsPopup
              source={ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.aae}
              userAccommodationFileId={this.state.selectedAccommodationRequestData.id}
              selectedUserAccommodationFileData={this.state.selectedAccommodationRequestData}
              getHistoricalData={this.state.getHistoricalData}
              showPopup={this.state.showAccommodationsRequestDetailsPopup}
              closeAccommodationsRequestDetailsPopup={this.closeAccommodationsRequestDetailsPopup}
              isDraft={this.state.isDraft}
            />
          )}
      </div>
    );
  }
}

export { SelectedUserAccommodationFileMainTabReports as unconnectedSelectedUserAccommodationFileMainTabReports };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    aaeInitialSelectedUserAccommodationFileData:
      state.aae.aaeInitialSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileData: state.aae.aaeSelectedUserAccommodationFileData,
    triggerAaeRerender: state.aae.triggerAaeRerender
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAccommodationFileAvailableReports
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectedUserAccommodationFileMainTabReports);
