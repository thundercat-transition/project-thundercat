import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import OpenRequests from "./OpenRequests";
import CompletedRequests from "./CompletedRequests";
import { setAaeRequestsSelectedTab } from "../../modules/AaeRedux";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  },
  viewEditDetailsBtn: {
    minWidth: 100
  },
  buttonIcon: {
    marginRight: 6
  },
  viewPermissionRequestButton: {
    marginLeft: 6
  }
};

class AccommodationRequests extends Component {
  render() {
    const TABS = [
      {
        key: "accommodation-requests-open-requests",
        tabName: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.openRequests.title,
        body: <OpenRequests />
      },
      {
        key: "accommodation-requests-completed-requests",
        tabName: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.completedRequets.title,
        body: <CompletedRequests />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.aae.sideNav.accommodationRequests.title}</h2>
          <p>{LOCALIZE.aae.sideNav.accommodationRequests.description}</p>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey={
                  this.props.aaeRequestsSelectedTab !== "" &&
                  this.props.aaeRequestsSelectedTab !== null
                    ? this.props.aaeRequestsSelectedTab
                    : "accommodation-requests-open-requests"
                }
                id="accommodation-requests-open-requests-tabs"
                style={styles.tabNavigation}
                onSelect={event => this.props.setAaeRequestsSelectedTab(event)}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    aaeRequestsSelectedTab: state.aae.aaeRequestsSelectedTab
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setAaeRequestsSelectedTab
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AccommodationRequests);
