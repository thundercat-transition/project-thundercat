import LOCALIZE from "../../text_resources";

function GenerateServiceStandardsReport(providedReportDataArray, currentLanguage) {
  // initializing reportData
  const reportData = [];

  // pushing columns definition
  reportData.push([]);

  // looping in provided providedReportDataArray and formatting data
  for (let i = 0; i < providedReportDataArray.length; i++) {
    reportData.push([]);
  }

  return reportData;
}

export default GenerateServiceStandardsReport;
