/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Row, Col } from "react-bootstrap";
import getSelectedUserAccommodationFileHeightCalculations from "../../helpers/aaeHeightCalculations";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { setAaeSelectedUserAccommodationFileData } from "../../modules/AaeRedux";
import * as _ from "lodash";
import LOCALIZE from "../../text_resources";
import getCheckboxTransformScale from "../../helpers/checkboxTransformScale";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 1,
    xl: 1
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 11,
    xl: 11
  }
};

const styles = {
  mainContainer: {
    padding: 12,
    overflowY: "auto",
    backgroundColor: "#FAFAFA",
    borderLeft: "1px solid #CDCDCD",
    borderRight: "1px solid #CDCDCD"
  },
  bold: {
    fontWeight: "bold"
  },
  rowStyle: {
    margin: "0 auto",
    width: "95%"
  },
  noMargin: {
    margin: 0
  },
  heightAdjustment: {
    paddingTop: 9
  },
  noPadding: {
    padding: 0
  },
  label: {
    paddingLeft: 12,
    marginBottom: 0
  },
  checkbox: {
    margin: "0 6px",
    verticalAlign: "middle",
    bottom: 1,
    position: "relative"
  }
};

class SelectedUserAccommodationFileStatsTab extends Component {
  state = {
    triggerRerender: false
  };

  toggleCandidateLimitationsCheckbox = (
    user_accommodation_file_candidate_limitations_option_id,
    event
  ) => {
    // creating a copy of respective redux states
    const copyOfAaeSelectedUserAccommodationFileData = _.cloneDeep(
      this.props.aaeSelectedUserAccommodationFileData
    );

    // checkbox checked
    if (event.target.checked) {
      // user_accommodation_file_candidate_limitations_option_id not already in candidate_limitations_stats_selected_options
      if (
        !copyOfAaeSelectedUserAccommodationFileData.candidate_limitations_stats_selected_options.includes(
          user_accommodation_file_candidate_limitations_option_id
        )
      ) {
        // adding user_accommodation_file_candidate_limitations_option_id to candidate_limitations_stats_selected_options array
        copyOfAaeSelectedUserAccommodationFileData.candidate_limitations_stats_selected_options.push(
          user_accommodation_file_candidate_limitations_option_id
        );
      }
      // checkbox unchecked
    } else {
      // user_accommodation_file_candidate_limitations_option_id is already in candidate_limitations_stats_selected_options
      if (
        copyOfAaeSelectedUserAccommodationFileData.candidate_limitations_stats_selected_options.includes(
          user_accommodation_file_candidate_limitations_option_id
        )
      ) {
        // removing user_accommodation_file_candidate_limitations_option_id from candidate_limitations_stats_selected_options array
        const indexOfItemToRemove =
          copyOfAaeSelectedUserAccommodationFileData.candidate_limitations_stats_selected_options.indexOf(
            user_accommodation_file_candidate_limitations_option_id
          );
        copyOfAaeSelectedUserAccommodationFileData.candidate_limitations_stats_selected_options.splice(
          indexOfItemToRemove,
          1
        );
      }
    }

    // reordering by candidate_limitations_stats_selected_options
    copyOfAaeSelectedUserAccommodationFileData.candidate_limitations_stats_selected_options.sort(
      (a, b) => (a > b ? 1 : -1)
    );

    // updating redux states
    this.props.setAaeSelectedUserAccommodationFileData(copyOfAaeSelectedUserAccommodationFileData);
  };

  toggleRecommendationsCheckbox = (user_accommodation_file_recommendations_option_id, event) => {
    // creating a copy of respective redux states
    const copyOfAaeSelectedUserAccommodationFileData = _.cloneDeep(
      this.props.aaeSelectedUserAccommodationFileData
    );

    // checkbox checked
    if (event.target.checked) {
      // user_accommodation_file_recommendations_option_id not already in recommendations_stats_selected_options
      if (
        !copyOfAaeSelectedUserAccommodationFileData.recommendations_stats_selected_options.includes(
          user_accommodation_file_recommendations_option_id
        )
      ) {
        // adding user_accommodation_file_recommendations_option_id to recommendations_stats_selected_options array
        copyOfAaeSelectedUserAccommodationFileData.recommendations_stats_selected_options.push(
          user_accommodation_file_recommendations_option_id
        );
      }
      // checkbox unchecked
    } else {
      // user_accommodation_file_recommendations_option_id is already in recommendations_stats_selected_options
      if (
        copyOfAaeSelectedUserAccommodationFileData.recommendations_stats_selected_options.includes(
          user_accommodation_file_recommendations_option_id
        )
      ) {
        // removing user_accommodation_file_recommendations_option_id from recommendations_stats_selected_options array
        const indexOfItemToRemove =
          copyOfAaeSelectedUserAccommodationFileData.recommendations_stats_selected_options.indexOf(
            user_accommodation_file_recommendations_option_id
          );
        copyOfAaeSelectedUserAccommodationFileData.recommendations_stats_selected_options.splice(
          indexOfItemToRemove,
          1
        );
      }
    }

    // reordering by recommendations_stats_selected_options
    copyOfAaeSelectedUserAccommodationFileData.recommendations_stats_selected_options.sort(
      (a, b) => (a > b ? 1 : -1)
    );

    // updating redux states
    this.props.setAaeSelectedUserAccommodationFileData(copyOfAaeSelectedUserAccommodationFileData);
  };

  render() {
    // calculating the height of the top tabs and above
    // =========================================================================================
    let heightOfBackToUserAccommodationFileSelectionButton = 0;
    let heightOfTopTabs = 0;
    let heightOfFooter = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-user-accommodation-file-selection-btn") != null) {
      heightOfBackToUserAccommodationFileSelectionButton = document.getElementById(
        "back-to-user-accommodation-file-selection-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight += heightOfBackToUserAccommodationFileSelectionButton + heightOfTopTabs + 14; // height of "back to user accommodation file selection button" + height of margin under button

    if (document.getElementById("selected-user-accommodation-file-footer-main-div") !== null) {
      heightOfFooter = document.getElementById(
        "selected-user-accommodation-file-footer-main-div"
      ).offsetHeight;
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    const customHeight = getSelectedUserAccommodationFileHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight,
      heightOfFooter
    );
    // =========================================================================================

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    return (
      <div style={{ ...styles.mainContainer, ...customHeight }}>
        <h2 style={{ ...styles.noMargin, ...styles.heightAdjustment }}>
          {
            LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.other.stats
              .candidateLimitations.title
          }
        </h2>
        <div>
          {this.props.aaeSelectedUserAccommodationFileData.candidate_limitations_stats_options
            // sorting by label/text
            .sort((a, b) =>
              a[this.props.currentLanguage][0].text.localeCompare(
                b[this.props.currentLanguage][0].text
              )
            )
            .map((candidate_limitation_option, i) => {
              return (
                <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                  >
                    <div>
                      <input
                        id={`user-accommodation-file-candidate-limitations-option-${i}`}
                        type="checkbox"
                        style={{
                          ...styles.checkbox,
                          ...{ transform: checkboxTransformScale }
                        }}
                        checked={this.props.aaeSelectedUserAccommodationFileData.candidate_limitations_stats_selected_options.includes(
                          candidate_limitation_option[this.props.currentLanguage][0]
                            .user_accommodation_file_candidate_limitations_option_id
                        )}
                        onChange={event =>
                          this.toggleCandidateLimitationsCheckbox(
                            candidate_limitation_option[this.props.currentLanguage][0]
                              .user_accommodation_file_candidate_limitations_option_id,
                            event
                          )
                        }
                      />
                    </div>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.noPadding}
                  >
                    <label
                      htmlFor={`user-accommodation-file-candidate-limitations-option-${i}`}
                      style={styles.label}
                    >
                      {candidate_limitation_option[this.props.currentLanguage][0].text}
                    </label>
                  </Col>
                </Row>
              );
            })}
        </div>
        <h2 style={styles.noMargin}>
          {
            LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.other.stats
              .recommendations.title
          }
        </h2>
        <div>
          {this.props.aaeSelectedUserAccommodationFileData.recommendations_stats_options
            // sorting by label/text
            .sort((a, b) =>
              a[this.props.currentLanguage][0].text.localeCompare(
                b[this.props.currentLanguage][0].text
              )
            )
            .map((recommendations_option, i) => {
              return (
                <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                  >
                    <div>
                      <input
                        id={`user-accommodation-file-recommendations-option-${i}`}
                        type="checkbox"
                        style={{
                          ...styles.checkbox,
                          ...{ transform: checkboxTransformScale }
                        }}
                        checked={this.props.aaeSelectedUserAccommodationFileData.recommendations_stats_selected_options.includes(
                          recommendations_option[this.props.currentLanguage][0]
                            .user_accommodation_file_recommendations_option_id
                        )}
                        onChange={event =>
                          this.toggleRecommendationsCheckbox(
                            recommendations_option[this.props.currentLanguage][0]
                              .user_accommodation_file_recommendations_option_id,
                            event
                          )
                        }
                      />
                    </div>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.noPadding}
                  >
                    <label
                      htmlFor={`user-accommodation-file-recommendations-option-${i}`}
                      style={styles.label}
                    >
                      {recommendations_option[this.props.currentLanguage][0].text}
                    </label>
                  </Col>
                </Row>
              );
            })}
        </div>
      </div>
    );
  }
}

export { SelectedUserAccommodationFileStatsTab as unconnectedSelectedUserAccommodationFileStatsTab };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testNavBarHeight: state.testSection.testNavBarHeight,
    aaeInitialSelectedUserAccommodationFileData:
      state.aae.aaeInitialSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileData: state.aae.aaeSelectedUserAccommodationFileData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setAaeSelectedUserAccommodationFileData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SelectedUserAccommodationFileStatsTab);
