/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Container, Col } from "react-bootstrap";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import Switch from "react-switch";
import getSwitchTransformScale from "../../helpers/switchTransformScale";

const columnSizes = {
  firstColumn: {
    xs: 9,
    sm: 9,
    md: 11,
    lg: 11,
    xl: 11
  },
  secondColumn: {
    xs: 3,
    sm: 3,
    md: 1,
    lg: 1,
    xl: 1
  },
  firstFullColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  },
  secondFullColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  }
};

const styles = {
  mainContainer: {
    width: "100%"
  },
  questionsContainer: {
    padding: "12px 25px",
    width: "100%"
  },
  updateButtonContainer: {
    marginTop: 24,
    textAlign: "center"
  },
  updateButton: {
    marginRight: 50,
    minWidth: 100
  },
  saveButtonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  textareaInput: {
    minHeight: 85,
    overflowY: "auto",
    resize: "none",
    display: "table-cell",
    width: "100%"
  },
  switch: {
    float: "inline-end",
    cursor: "not-allowed"
  },
  questionRow: {
    marginTop: "24px"
  },
  checkboxContainer: {
    paddingLeft: "24px"
  },
  checkbox: {
    verticalAlign: "middle"
  },
  privacyNoticeLink: {
    textDecoration: "underline",
    color: "#0278A4",
    cursor: "pointer",
    backgroundColor: "transparent",
    border: "none",
    padding: 0
  },
  checkboxLabel: {
    marginLeft: "15px"
  },
  loading: {
    fontSize: "32px",
    padding: 6
  },
  loadingContainer: {
    margin: 24,
    textAlign: "center"
  },
  pleaseDescribeMargin: {
    margin: "12px 0 6px 0"
  }
};

class SelectedUserAccommodationFileUserAccommodationsProfile extends Component {
  state = {
    isLoading: false,
    // default values
    switchHeight: 25,
    switchWidth: 50
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoading: false });
        }
      );
    });
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };

    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <>
        <Container style={styles.mainContainer}>
          {this.state.isLoading ? (
            <div style={styles.loading}>
              {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
              <label className="fa fa-spinner fa-spin">
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            </div>
          ) : (
            <>
              <Row>
                <h2>{LOCALIZE.profile.accommodations.title}</h2>
              </Row>
              <div style={styles.questionsContainer}>
                {/* BUILT-IN ACCESSIBILITY */}
                <Row>
                  <h3>{LOCALIZE.profile.accommodations.builtInAccessibility.title}</h3>
                </Row>
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="built-in-accessibility"
                  >
                    {
                      LOCALIZE.profile.accommodations.builtInAccessibility
                        .hasBuiltInAccessibilityQuestion
                    }
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={() => {}}
                          checked={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.has_built_in_accessibility
                          }
                          className="switch-disabled-look"
                          aria-labelledby="built-in-accessibility"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                          aria-disabled={true}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.props.aaeSelectedUserAccommodationFileData.user_accommodations_profile
                  .has_built_in_accessibility && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="built-in-accessibility-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.builtInAccessibility.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="built-in-accessibility-description"
                          className="input-disabled-look"
                          aria-labelledby="built-in-accessibility-describe"
                          aria-required={true}
                          aria-invalid={false}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.built_in_accessibility.description
                          }
                          aria-disabled={true}
                        ></textarea>
                      </Col>
                    </Row>
                  </>
                )}
                {/* EXTRA TIME */}
                <Row>
                  <h3>{LOCALIZE.profile.accommodations.extraTime.title}</h3>
                </Row>
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="extra-time"
                  >
                    {LOCALIZE.profile.accommodations.extraTime.needsExtraTimeQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={() => {}}
                          checked={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.needs_extra_time
                          }
                          className="switch-disabled-look"
                          aria-labelledby="extra-time"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                          aria-disabled={true}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.props.aaeSelectedUserAccommodationFileData.user_accommodations_profile
                  .needs_extra_time && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="extra-time-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.extraTime.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="extra-time-description"
                          className="input-disabled-look"
                          aria-labelledby="extra-time-describe"
                          aria-required={true}
                          aria-invalid={false}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.extra_time.description
                          }
                          aria-disabled={true}
                        ></textarea>
                      </Col>
                    </Row>
                  </>
                )}
                {/* BREAKS */}
                <Row>
                  <h3>{LOCALIZE.profile.accommodations.breaks.title}</h3>
                </Row>
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="breaks"
                  >
                    {LOCALIZE.profile.accommodations.breaks.needsBreaksQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={() => {}}
                          checked={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.needs_breaks
                          }
                          className="switch-disabled-look"
                          aria-labelledby="breaks"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                          aria-disabled={true}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.props.aaeSelectedUserAccommodationFileData.user_accommodations_profile
                  .needs_breaks && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="breaks-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.breaks.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="breaks-description"
                          className="input-disabled-look"
                          aria-labelledby="breaks-describe"
                          aria-required={true}
                          aria-invalid={false}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.breaks.description
                          }
                          aria-disabled={true}
                        ></textarea>
                      </Col>
                    </Row>
                  </>
                )}
                {/* ADAPTIVE TECH */}
                <Row>
                  <h3>{LOCALIZE.profile.accommodations.adaptiveTech.title}</h3>
                </Row>
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="adaptive-tech"
                  >
                    {LOCALIZE.profile.accommodations.adaptiveTech.needsAdaptiveTechQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={() => {}}
                          checked={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.needs_adaptive_tech
                          }
                          className="switch-disabled-look"
                          aria-labelledby="adaptive-tech"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                          aria-disabled={true}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.props.aaeSelectedUserAccommodationFileData.user_accommodations_profile
                  .needs_adaptive_tech && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="adaptive-tech-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.adaptiveTech.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="adaptive-tech-description"
                          aria-labelledby="adaptive-tech-describe"
                          className="input-disabled-look"
                          aria-required={true}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.adaptive_tech.description
                          }
                          aria-disabled={true}
                        ></textarea>
                      </Col>
                    </Row>
                  </>
                )}
                {/* ERGONOMIC */}
                <Row>
                  <h3>{LOCALIZE.profile.accommodations.ergonomic.title}</h3>
                </Row>
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="ergonomic"
                  >
                    {LOCALIZE.profile.accommodations.ergonomic.needsErgonomicQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={() => {}}
                          checked={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.needs_ergonomic
                          }
                          className="switch-disabled-look"
                          aria-labelledby="ergonomic"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                          aria-disabled={true}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.props.aaeSelectedUserAccommodationFileData.user_accommodations_profile
                  .needs_ergonomic && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="ergonomic-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.ergonomic.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="ergonomic-description"
                          aria-labelledby="ergonomic-describe"
                          className="input-disabled-look"
                          aria-required={true}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.ergonomic.description
                          }
                          aria-disabled={true}
                        ></textarea>
                      </Col>
                    </Row>
                  </>
                )}
                {/* ACCESS ASSISTANCE */}
                <Row>
                  <h3>{LOCALIZE.profile.accommodations.accessAssistance.title}</h3>
                </Row>
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="access-assistance"
                  >
                    {LOCALIZE.profile.accommodations.accessAssistance.needsAccessAssistanceQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={() => {}}
                          checked={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.needs_access_assistance
                          }
                          className="switch-disabled-look"
                          aria-labelledby="access-assistance"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                          aria-disabled={true}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.props.aaeSelectedUserAccommodationFileData.user_accommodations_profile
                  .needs_access_assistance && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="access-assistance-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.accessAssistance.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="access-assistance-description"
                          aria-labelledby="access-assistance-describe"
                          className="input-disabled-look"
                          aria-required={true}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.access_assistance.description
                          }
                          aria-disabled={true}
                        ></textarea>
                      </Col>
                    </Row>
                  </>
                )}
                {/* ENVIRONMENT */}
                <Row>
                  <h3>{LOCALIZE.profile.accommodations.environment.title}</h3>
                </Row>
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="environment"
                  >
                    {LOCALIZE.profile.accommodations.environment.needsEnvironmentQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={() => {}}
                          checked={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.needs_environment
                          }
                          className="switch-disabled-look"
                          aria-labelledby="environment"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                          aria-disabled={true}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.props.aaeSelectedUserAccommodationFileData.user_accommodations_profile
                  .needs_environment && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="environment-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.environment.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="environment-description"
                          aria-labelledby="environment-describe"
                          className="input-disabled-look"
                          aria-required={true}
                          aria-invalid={!this.state.isValidEnvironment}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.environment.description
                          }
                          aria-disabled={true}
                        ></textarea>
                      </Col>
                    </Row>
                  </>
                )}
                {/* SCHEDULING ADAPTATION */}
                <Row>
                  <h3>{LOCALIZE.profile.accommodations.schedulingAdaptation.title}</h3>
                </Row>
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="scheduling-adaptation"
                  >
                    {
                      LOCALIZE.profile.accommodations.schedulingAdaptation
                        .needsSchedulingAdaptationQuestion
                    }
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={() => {}}
                          checked={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.needs_scheduling_adaptation
                          }
                          className="switch-disabled-look"
                          aria-labelledby="scheduling-adaptation"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                          aria-disabled={true}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.props.aaeSelectedUserAccommodationFileData.user_accommodations_profile
                  .needs_scheduling_adaptation && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="scheduling-adaptation-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.schedulingAdaptation.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="scheduling-adaptation-description"
                          aria-labelledby="scheduling-adaptation-describe"
                          className="input-disabled-look"
                          aria-required={true}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.scheduling_adaptation.description
                          }
                          aria-disabled={true}
                        ></textarea>
                      </Col>
                    </Row>
                  </>
                )}
                {/* OTHER NEEDS WRITTEN */}
                <Row>
                  <h3>{LOCALIZE.profile.accommodations.otherNeedsWritten.title}</h3>
                </Row>
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="other-needs-written"
                  >
                    {LOCALIZE.profile.accommodations.otherNeedsWritten.hasOtherNeedsWrittenQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={() => {}}
                          checked={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.has_other_needs_written
                          }
                          className="switch-disabled-look"
                          aria-labelledby="other-needs-written"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                          aria-disabled={true}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.props.aaeSelectedUserAccommodationFileData.user_accommodations_profile
                  .has_other_needs_written && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="other-needs-written-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.otherNeedsWritten.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="other-needs-written-description"
                          aria-labelledby="other-needs-written-describe"
                          className="input-disabled-look"
                          aria-required={true}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.other_needs_written.description
                          }
                          aria-disabled={true}
                        ></textarea>
                      </Col>
                    </Row>
                  </>
                )}
                {/* OTHER NEEDS ORAL */}
                <Row>
                  <h3>{LOCALIZE.profile.accommodations.otherNeedsOral.title}</h3>
                </Row>
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="other-needs-oral"
                  >
                    {LOCALIZE.profile.accommodations.otherNeedsOral.hasOtherNeedsOralQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={() => {}}
                          checked={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.has_other_needs_oral
                          }
                          className="switch-disabled-look"
                          aria-labelledby="other-needs-oral"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                          aria-disabled={true}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.props.aaeSelectedUserAccommodationFileData.user_accommodations_profile
                  .has_other_needs_oral && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="other-needs-oral-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.otherNeedsOral.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="other-needs-oral-description"
                          aria-labelledby="other-needs-oral-describe"
                          className="input-disabled-look"
                          aria-required={true}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.other_needs_oral.description
                          }
                          aria-disabled={true}
                        ></textarea>
                      </Col>
                    </Row>
                  </>
                )}
                {/* RELEATED ORAL QUESTIONS */}
                <Row>
                  <h3>{LOCALIZE.profile.accommodations.repeatedOralQuestions.title}</h3>
                </Row>
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="repeated-oral-questions"
                  >
                    {
                      LOCALIZE.profile.accommodations.repeatedOralQuestions
                        .needsRepeatedOralQuestionsQuestion
                    }
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={() => {}}
                          checked={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.needs_repeated_oral_questions
                          }
                          className="switch-disabled-look"
                          aria-labelledby="repeated-oral-questions"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                          aria-disabled={true}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.props.aaeSelectedUserAccommodationFileData.user_accommodations_profile
                  .needs_repeated_oral_questions && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="repeated-oral-questions-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.repeatedOralQuestions.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="repeated-oral-questions-description"
                          aria-labelledby="repeated-oral-questions-describe"
                          className="input-disabled-look"
                          aria-required={true}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.repeated_oral_questions.description
                          }
                          aria-disabled={true}
                        ></textarea>
                      </Col>
                    </Row>
                  </>
                )}
                {/* PREVIOUSLY ACCOMMODATED */}
                <Row>
                  <h3>{LOCALIZE.profile.accommodations.previouslyAccommodated.title}</h3>
                </Row>
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="previously-accommodated"
                  >
                    {
                      LOCALIZE.profile.accommodations.previouslyAccommodated
                        .previouslyAccommodatedQuestion
                    }
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={() => {}}
                          checked={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.previously_accommodated
                          }
                          className="switch-disabled-look"
                          aria-labelledby="previously-accommodated"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                          aria-disabled={true}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.props.aaeSelectedUserAccommodationFileData.user_accommodations_profile
                  .previously_accommodated && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="previously-accommodated-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.previouslyAccommodated.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="previously-accommodated-description"
                          aria-labelledby="previously-accommodated-describe"
                          className="input-disabled-look"
                          aria-required={true}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.props.aaeSelectedUserAccommodationFileData
                              .user_accommodations_profile.previous_accommodation.description
                          }
                          aria-disabled={true}
                        ></textarea>
                      </Col>
                    </Row>
                  </>
                )}
              </div>
            </>
          )}
        </Container>
      </>
    );
  }
}

export { SelectedUserAccommodationFileUserAccommodationsProfile as unconnectedSelectedUserAccommodationFileUserAccommodationsProfile };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    aaeInitialSelectedUserAccommodationFileData:
      state.aae.aaeInitialSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileData: state.aae.aaeSelectedUserAccommodationFileData
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectedUserAccommodationFileUserAccommodationsProfile);
