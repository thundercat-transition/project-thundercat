/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import {
  getUserAccommodationFileStatusOptions,
  getUserAccommodationFileComplexityOptions,
  setAaeSelectedUserAccommodationFileData
} from "../../modules/AaeRedux";
import * as _ from "lodash";
import { Row, Col } from "react-bootstrap";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { getUsersBasedOnSpecifiedPermission } from "../../modules/PermissionsRedux";
import { PERMISSION } from "../profile/Constants";
import DropdownSelect from "../commons/DropdownSelect";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { userAccommodationFileIsInEndState } from "./SelectedUserAccommodationFile";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 7
  }
};

const styles = {
  noMargin: {
    margin: 0
  },
  formContainer: {
    padding: 12
  },
  rowStyle: {
    padding: "6px 0px"
  },
  inputTitles: {
    fontWeight: "bold",
    margin: 0
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  loadingContainer: {
    textAlign: "center"
  },
  loading: {
    fontSize: "32px",
    padding: 6
  }
};

class SelectedUserAccommodationFileMainTabFileAdministration extends Component {
  state = {
    isLoadingAssignedToOptions: false,
    assignedToOptions: [],
    assignedToSelectedOption:
      this.props.aaeSelectedUserAccommodationFileData.assigned_to_user_id !== null
        ? {
            label: `${this.props.aaeSelectedUserAccommodationFileData.assigned_to_user_last_name}, ${this.props.aaeSelectedUserAccommodationFileData.assigned_to_user_first_name} (${this.props.aaeSelectedUserAccommodationFileData.assigned_to_user_email})`,
            value: this.props.aaeSelectedUserAccommodationFileData.assigned_to_user_id,
            first_name: this.props.aaeSelectedUserAccommodationFileData.assigned_to_user_first_name,
            last_name: this.props.aaeSelectedUserAccommodationFileData.assigned_to_user_last_name,
            email: this.props.aaeSelectedUserAccommodationFileData.assigned_to_user_email
          }
        : [
            {
              value: null,
              label: LOCALIZE.dropdownSelect.pleaseSelect,
              first_name: null,
              last_name: null,
              email: null
            }
          ],
    isLoadingFileStatusOptions: false,
    fileStatusOptions: [],
    fileStatusSelectedOption:
      this.props.aaeSelectedUserAccommodationFileData.status_id !== null
        ? {
            label:
              this.props.aaeSelectedUserAccommodationFileData.status_data[
                this.props.currentLanguage
              ][0].text,
            value: this.props.aaeSelectedUserAccommodationFileData.status_id,
            status_codename: this.props.aaeSelectedUserAccommodationFileData.status_codename
          }
        : [],
    isLoadingFileComplexityOptions: false,
    fileComplexityOptions: [],
    fileComplexitySelectedOption:
      this.props.aaeSelectedUserAccommodationFileData.complexity_id !== null
        ? {
            value: this.props.aaeSelectedUserAccommodationFileData.complexity_id
          }
        : [{ value: null, label: LOCALIZE.dropdownSelect.pleaseSelect, complexity_codename: null }]
  };

  componentDidMount = () => {
    // populating dropdown options
    this.populateAssignedToOptions();
    this.populateFileStatusOptions();
    this.populateFileComplexityOptions();
  };

  componentDidUpdate = prevProps => {
    // if triggerAaeRerender gets updated
    if (prevProps.triggerAaeRerender !== this.props.triggerAaeRerender) {
      this.setState({
        fileStatusSelectedOption:
          this.props.aaeSelectedUserAccommodationFileData.status_id !== null
            ? {
                label:
                  this.props.aaeSelectedUserAccommodationFileData.status_data[
                    this.props.currentLanguage
                  ][0].text,
                value: this.props.aaeSelectedUserAccommodationFileData.status_id,
                status_codename: this.props.aaeSelectedUserAccommodationFileData.status_codename
              }
            : []
      });
    }
  };

  populateAssignedToOptions = () => {
    const assignedToOptions = [];

    this.setState({ isLoadingAssignedToOptions: true }, () => {
      // getting AAE users
      this.props.getUsersBasedOnSpecifiedPermission(PERMISSION.aae).then(response => {
        // populating "unassign" option
        assignedToOptions.push({
          value: null,
          label: LOCALIZE.dropdownSelect.pleaseSelect,
          first_name: null,
          last_name: null,
          email: null
        });
        // looping in response
        for (let i = 0; i < response.length; i++) {
          assignedToOptions.push({
            label: `${response[i].last_name}, ${response[i].first_name} (${response[i].email})`,
            value: response[i].user_id,
            first_name: response[i].first_name,
            last_name: response[i].last_name,
            email: response[i].email
          });
        }
        this.setState({ assignedToOptions: assignedToOptions, isLoadingAssignedToOptions: false });
      });
    });
  };

  populateFileStatusOptions = () => {
    const fileStatusOptions = [];

    this.setState({ isLoadingFileStatusOptions: true }, () => {
      // getting user accommodation file status options
      this.props.getUserAccommodationFileStatusOptions().then(response => {
        // looping in response
        for (let i = 0; i < response.length; i++) {
          // there is a match with the accommodation file data
          if (response[i].status_id === this.props.aaeSelectedUserAccommodationFileData.status_id) {
            // setting the assignedToSelectedOption
            this.setState({
              fileStatusSelectedOption: {
                label: response[i].status_data[this.props.currentLanguage][0].text,
                value: response[i].status_id,
                status_codename: response[i].status_codename
              }
            });
          }
          fileStatusOptions.push({
            label: response[i].status_data[this.props.currentLanguage][0].text,
            value: response[i].status_id,
            status_codename: response[i].status_codename
          });
        }
        this.setState({ fileStatusOptions: fileStatusOptions, isLoadingFileStatusOptions: false });
      });
    });
  };

  populateFileComplexityOptions = () => {
    const fileComplexityOptions = [];

    this.setState({ isLoadingFileComplexityOptions: true }, () => {
      // getting user accommodation file complexity options
      this.props.getUserAccommodationFileComplexityOptions().then(response => {
        // populating "unassign" option
        fileComplexityOptions.push({
          value: null,
          label: LOCALIZE.dropdownSelect.pleaseSelect,
          complexity_codename: null
        });

        // looping in response
        for (let i = 0; i < response.length; i++) {
          // there is a match with the accommodation file data
          if (
            response[i].complexity_id ===
            this.props.aaeSelectedUserAccommodationFileData.complexity_id
          ) {
            // setting the fileComplexitySelectedOption
            this.setState({
              fileComplexitySelectedOption: {
                label: response[i].complexity_data[this.props.currentLanguage][0].text,
                value: response[i].complexity_id,
                complexity_codename: response[i].complexity_codename
              }
            });
          }
          fileComplexityOptions.push({
            label: response[i].complexity_data[this.props.currentLanguage][0].text,
            value: response[i].complexity_id,
            complexity_codename: response[i].complexity_codename
          });
        }
        this.setState({
          fileComplexityOptions: fileComplexityOptions,
          isLoadingFileComplexityOptions: false
        });
      });
    });
  };

  getSelectedAssignedToOption = selectedOption => {
    this.setState({ assignedToSelectedOption: selectedOption }, () => {
      // creating a copy of respective redux states
      const copyOfAaeSelectedUserAccommodationFileData = _.cloneDeep(
        this.props.aaeSelectedUserAccommodationFileData
      );

      // updating needed attributes
      copyOfAaeSelectedUserAccommodationFileData.assigned_to_user_id = selectedOption.value;
      copyOfAaeSelectedUserAccommodationFileData.assigned_to_user_email = selectedOption.email;
      copyOfAaeSelectedUserAccommodationFileData.assigned_to_user_first_name =
        selectedOption.first_name;
      copyOfAaeSelectedUserAccommodationFileData.assigned_to_user_last_name =
        selectedOption.last_name;

      // updating redux states
      this.props.setAaeSelectedUserAccommodationFileData(
        copyOfAaeSelectedUserAccommodationFileData
      );
    });
  };

  getSelectedFileStatusOption = selectedOption => {
    this.setState({ fileStatusSelectedOption: selectedOption }, () => {
      // creating a copy of respective redux states
      const copyOfAaeSelectedUserAccommodationFileData = _.cloneDeep(
        this.props.aaeSelectedUserAccommodationFileData
      );

      // updating needed attributes
      copyOfAaeSelectedUserAccommodationFileData.status_id = selectedOption.value;
      copyOfAaeSelectedUserAccommodationFileData.status_codename = selectedOption.status_codename;

      // updating redux states
      this.props.setAaeSelectedUserAccommodationFileData(
        copyOfAaeSelectedUserAccommodationFileData
      );
    });
  };

  getSelectedFileComplexityOption = selectedOption => {
    this.setState({ fileComplexitySelectedOption: selectedOption }, () => {
      // creating a copy of respective redux states
      const copyOfAaeSelectedUserAccommodationFileData = _.cloneDeep(
        this.props.aaeSelectedUserAccommodationFileData
      );

      // updating needed attributes
      copyOfAaeSelectedUserAccommodationFileData.complexity_id = selectedOption.value;
      copyOfAaeSelectedUserAccommodationFileData.complexity_codename =
        selectedOption.complexity_codename;

      // updating redux states
      this.props.setAaeSelectedUserAccommodationFileData(
        copyOfAaeSelectedUserAccommodationFileData
      );
    });
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div>
        <h2 style={styles.noMargin}>
          {LOCALIZE.formatString(
            LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
              .fileAdministration.title,
            typeof this.props.aaeSelectedUserAccommodationFileData.test_to_administer.test !==
              "undefined" &&
              this.props.aaeSelectedUserAccommodationFileData.test_to_administer.test !== null
              ? `[${this.props.aaeSelectedUserAccommodationFileData.test_to_administer.test_code} - v${this.props.aaeSelectedUserAccommodationFileData.test_to_administer.test_version}]`
              : typeof this.props.aaeSelectedUserAccommodationFileData.test_to_administer
                    .specified_test_description !== "undefined" &&
                  this.props.aaeSelectedUserAccommodationFileData.test_to_administer
                    .specified_test_description !== ""
                ? `[${this.props.aaeSelectedUserAccommodationFileData.test_to_administer.specified_test_description}]`
                : ""
          )}
        </h2>
        <div style={styles.formContainer}>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                htmlFor="user-accommodation-file-file-administration-request-id-input"
                style={styles.inputTitles}
              >
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.fileAdministration.requestId
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-file-administration-request-id-input"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={this.props.aaeSelectedUserAccommodationFileData.id}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                id="user-accommodation-file-file-administration-assigned-to-label"
                style={styles.inputTitles}
              >
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.fileAdministration.assignedToLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              {this.state.isLoadingAssignedToOptions ? (
                <div style={styles.loadingContainer}>
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} style={styles.loading} />
                  </label>
                </div>
              ) : (
                <DropdownSelect
                  idPrefix="user-accommodation-file-file-administration-assigned-to"
                  ariaLabelledBy="user-accommodation-file-file-administration-assigned-to-label"
                  ariaRequired={false}
                  options={this.state.assignedToOptions}
                  onChange={this.getSelectedAssignedToOption}
                  defaultValue={this.state.assignedToSelectedOption}
                  hasPlaceholder={true}
                  orderByLabels={false}
                />
              )}
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                id="user-accommodation-file-file-administration-file-status-label"
                style={styles.inputTitles}
              >
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.fileAdministration.fileStatusLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              {this.state.isLoadingFileStatusOptions ? (
                <div style={styles.loadingContainer}>
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} style={styles.loading} />
                  </label>
                </div>
              ) : (
                <DropdownSelect
                  idPrefix="user-accommodation-file-file-administration-file-status"
                  ariaLabelledBy="user-accommodation-file-file-administration-file-status-label"
                  ariaRequired={true}
                  options={this.state.fileStatusOptions}
                  onChange={this.getSelectedFileStatusOption}
                  defaultValue={this.state.fileStatusSelectedOption}
                  hasPlaceholder={true}
                  orderByLabels={false}
                  isDisabled={userAccommodationFileIsInEndState(
                    this.props.aaeSelectedUserAccommodationFileData.status_codename
                  )}
                />
              )}
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                htmlFor="user-accommodation-file-file-administration-file-submit-date"
                style={styles.inputTitles}
              >
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.fileAdministration.fileSubmitDateLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-file-administration-file-submit-date"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={this.props.aaeSelectedUserAccommodationFileData.created_date}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                id="user-accommodation-file-file-administration-file-complexity-label"
                style={styles.inputTitles}
              >
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.fileAdministration.fileComplexityLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              {this.state.isLoadingFileComplexityOptions ? (
                <div style={styles.loadingContainer}>
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} style={styles.loading} />
                  </label>
                </div>
              ) : (
                <DropdownSelect
                  idPrefix="user-accommodation-file-file-administration-file-complexity"
                  ariaLabelledBy="user-accommodation-file-file-administration-file-complexity-label"
                  ariaRequired={true}
                  options={this.state.fileComplexityOptions}
                  onChange={this.getSelectedFileComplexityOption}
                  defaultValue={this.state.fileComplexitySelectedOption}
                  hasPlaceholder={true}
                  orderByLabels={false}
                />
              )}
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                htmlFor="user-accommodation-file-file-administration-last-modified-date"
                style={styles.inputTitles}
              >
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.fileAdministration.lastModifiedDateLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-file-administration-last-modified-date"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={this.props.aaeSelectedUserAccommodationFileData.modify_date.split("T")[0]}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                htmlFor="user-accommodation-file-file-administration-last-modified-by"
                style={styles.inputTitles}
              >
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.fileAdministration.lastModifiedByLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-file-administration-last-modified-by"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={`${this.props.aaeSelectedUserAccommodationFileData.last_modified_by_user_last_name}, ${this.props.aaeSelectedUserAccommodationFileData.last_modified_by_user_first_name} (${this.props.aaeSelectedUserAccommodationFileData.last_modified_by_user_email})`}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                htmlFor="user-accommodation-file-file-administration-process-end-date"
                style={styles.inputTitles}
              >
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.fileAdministration.processEndDateLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-file-administration-process-end-date"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={this.props.aaeSelectedUserAccommodationFileData.process_end_date}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export { SelectedUserAccommodationFileMainTabFileAdministration as unconnectedSelectedUserAccommodationFileMainTabFileAdministration };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    aaeInitialSelectedUserAccommodationFileData:
      state.aae.aaeInitialSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileData: state.aae.aaeSelectedUserAccommodationFileData,
    triggerAaeRerender: state.aae.triggerAaeRerender
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setAaeSelectedUserAccommodationFileData,
      getUsersBasedOnSpecifiedPermission,
      getUserAccommodationFileStatusOptions,
      getUserAccommodationFileComplexityOptions
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectedUserAccommodationFileMainTabFileAdministration);
