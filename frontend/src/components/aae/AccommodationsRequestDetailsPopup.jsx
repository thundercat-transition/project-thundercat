/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useEffect, useRef, useState } from "react";
import { Col, Row } from "react-bootstrap";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../commons/PopupBox";
import {
  triggerUserAccommodationFileStatusUpdateAsACandidate,
  getAccommodationFileDataForDetailsPopup
} from "../../modules/AaeRedux";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import {
  faTimes,
  faTrash,
  faCheck,
  faPrint,
  faSpinner,
  faPencil
} from "@fortawesome/free-solid-svg-icons";
import USER_ACCOMMODATION_FILE_STATUS_CONST from "./Constants";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import { getTimeInHoursMinutes } from "../../helpers/timeConversion";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import getFormattedDate from "../../helpers/getFormattedDate";
import getAccommodationsRequestDetailsContentToPrint from "../../helpers/accommodationsRequestDetailsPrintContent";
import ReactToPrint from "../commons/ReactToPrint";
import formatMaskedPhoneNumber, { MASKS, MASK_FORMAT_CHARS } from "../commons/InputMask/helpers";
import InputMask from "react-input-mask";
import THEME from "../commons/CustomButtonTheme";
import CustomButton from "../commons/CustomButton";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";
import { editUserAccommodationFilePhoneNumber } from "../../modules/CandidateRedux";
import { TestSkillSleDescCodename } from "../testFactory/Constants";

export const ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE = {
  candidate: "candidate",
  aae: "aae",
  tcm: "tcm",
  ta: "ta",
  hr: "hr",
  scorerOla: "scorer_ola"
};

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  },
  secondColumnPhoneNumber: {
    xs: 6,
    sm: 6,
    md: 6,
    lg: 4,
    xl: 4
  },
  thirdColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 1,
    xl: 1
  }
};

export function getFormattedTotalTestTime(total_default_time, total_adjusted_time) {
  // initializing formattedTotalTestTime
  let formattedTotalTestTime = "";
  // getting time in hours / minutes
  const timeInHoursMinutes = getTimeInHoursMinutes(total_adjusted_time);
  // additional time set
  if (total_adjusted_time > total_default_time) {
    // getting calculated additional time
    const calculatedAdditionalTime = total_adjusted_time - total_default_time;
    formattedTotalTestTime = `${timeInHoursMinutes.formattedHours} ${LOCALIZE.commons.hours} ${
      timeInHoursMinutes.formattedMinutes
    } ${LOCALIZE.commons.minutes} (${LOCALIZE.formatString(
      LOCALIZE.accommodationsRequestDetailsPopup.additionalTimeDetails,
      calculatedAdditionalTime
    )})`;
    // default time (no time adjustments)
  } else {
    formattedTotalTestTime = `${timeInHoursMinutes.formattedHours} ${LOCALIZE.commons.hours} ${timeInHoursMinutes.formattedMinutes} ${LOCALIZE.commons.minutes}`;
  }

  return formattedTotalTestTime;
}

const styles = {
  formContainer: {
    width: "80%",
    margin: "18px auto"
  },
  rowContainer: {
    padding: "6px 0"
  },
  boldText: {
    fontWeight: "bold"
  },
  loadingContainer: {
    textAlign: "center",
    transform: "scale(1.3)"
  },
  loading: {
    fontSize: "32px",
    padding: 6
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  textAreaInput: {
    resize: "none",
    width: "100%"
  },
  textAreaCol: {
    textAlign: "center",
    margin: "0 12px"
  },
  indemptedCol: {
    paddingLeft: 60
  },
  printDownloadParagraph: {
    textAlign: "right"
  },
  printDownloadLink: {
    textDecoration: "underline",
    color: "#0278A4",
    cursor: "pointer",
    backgroundColor: "transparent",
    border: "none",
    padding: 0
  },
  printDownloadIcon: {
    color: "#00565e",
    marginLeft: 6
  },
  icon: {
    paddingRight: 6,
    verticalAlign: "middle"
  },
  transparentText: {
    color: "transparent"
  },
  customLoadingContainer: {
    display: "grid",
    gridTemplateColumns: "1fr"
  },
  loadingOverlappingStyle: {
    gridRowStart: "1",
    gridColumnStart: "1"
  },
  divTextArea: {
    color: "#808080",
    backgroundColor: "#f6f6f6",
    overflowY: "scroll",
    height: 200,
    marginTop: 10,
    marginBottom: 5,
    textAlign: "left"
  },
  editPhoneNumberButtonCol: {
    textAlign: "left",
    padding: 0
  },
  allUnset: {
    all: "unset"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  }
};

function AccommodationsRequestDetailsPopup(props) {
  // setting default props (working perfectly well, even though VS Code detects that this is not being used)
  const { popupSize = "xl" } = props;

  const componentToPrintRef = useRef(null);

  const [isLoadingData, setIsLoadingData] = useState(true);
  const [accommodationsRequestData, setAccommodationsRequestData] = useState({});
  const [showCancelRequestConfirmationPopup, setShowCancelRequestConfirmationPopup] =
    useState(false);
  const [actionInProgress, setActionInProgress] = useState(false);
  const [showApprovalSuccessPopup, setShowApprovalSuccessPopup] = useState(false);
  const [triggerReactToPrint, setTriggerReactToPrint] = useState(false);
  const [showEditPhoneNumberPopup, setShowEditPhoneNumberPopup] = useState(false);
  const [tempPhoneNumberContent, setTempPhoneNumberContent] = useState("");
  const [isValidTempPhoneNumberContent, setIsValidTempPhoneNumberContent] = useState(false);

  const showSimplifiedFooter = () => {
    // initializing showSimplifiedFooter
    let showSimplifiedFooter = false;

    // source is not candidate
    if (props.source !== ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.candidate) {
      showSimplifiedFooter = true;
    }

    return showSimplifiedFooter;
  };

  // getting respective user accommodation file data
  const getUserAccommodationFileData = () => {
    // initializing providedUserId
    let providedUserId = null;
    // called from othe then candidate
    if (props.source !== ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.candidate) {
      providedUserId = props.selectedUserAccommodationFileData.user_id;
    }
    // setting getHistoricalData value
    let getHistoricalData = false;
    if (typeof props.getHistoricalData !== "undefined" && props.getHistoricalData !== null) {
      getHistoricalData = props.getHistoricalData;
    }
    props
      .getAccommodationFileDataForDetailsPopup(
        props.userAccommodationFileId,
        getHistoricalData,
        getHistoricalData ? props.selectedUserAccommodationFileData.history_id : null,
        providedUserId
      )
      .then(response => {
        setAccommodationsRequestData(response);
      });
  };

  const openEditPhoneMumberPopup = phoneNumberContent => {
    setShowEditPhoneNumberPopup(true);
    setTempPhoneNumberContent(phoneNumberContent);
    setIsValidTempPhoneNumberContent(true);
  };

  // updating Phone Number (temp phone number for the edit functionality)
  const updateTempPhoneNumber = event => {
    const tempPhoneNumberContent = formatMaskedPhoneNumber(event.target.value);
    const isValidTempPhoneNumberContent = tempPhoneNumberContent.length === 10;
    setTempPhoneNumberContent(tempPhoneNumberContent);
    setIsValidTempPhoneNumberContent(isValidTempPhoneNumberContent);
  };

  const closeEditPhoneMumberPopup = () => {
    setShowEditPhoneNumberPopup(false);
    setTempPhoneNumberContent("");
    setIsValidTempPhoneNumberContent(true);
  };

  const handleEditPhoneNumber = () => {
    // updating phone number in DB
    props
      .editUserAccommodationFilePhoneNumber(
        props.selectedUserAccommodationFileData.user_accommodation_file_id,
        tempPhoneNumberContent
      )
      .then(response => {
        // if success request
        if (response.ok) {
          // getting updated user accommodation file data
          getUserAccommodationFileData();
          // closing popup
          closeEditPhoneMumberPopup();
          // should never happen
        } else {
          throw new Error("An error occurred during the Edit Phone Number Process");
        }
      });
  };

  const openCancelRequestConfirmationPopup = () => {
    setShowCancelRequestConfirmationPopup(true);
  };

  const closeCancelRequestConfirmationPopup = () => {
    setShowCancelRequestConfirmationPopup(false);
  };

  const handleCancelRequestAction = () => {
    // creating body
    const body = {
      user_accommodation_file_id: props.userAccommodationFileId,
      action: USER_ACCOMMODATION_FILE_STATUS_CONST.cancelledByCandidate
    };
    props.triggerUserAccommodationFileStatusUpdateAsACandidate(body).then(response => {
      // success
      if (response.ok) {
        // closing popups
        closeCancelRequestConfirmationPopup();
        props.closeAccommodationsRequestDetailsPopup();
        setTimeout(() => {
          setActionInProgress(false);
        }, 250);
        // should never happen
      } else {
        throw new Error("An error occurred during the cancellation of the accommodation request");
      }
    });
  };

  const getFormattedTestSkill = () => {
    // formatting test skill
    const formattedTestSkill =
      accommodationsRequestData.test_skill_sub_type_id !== null
        ? `${accommodationsRequestData[`test_skill_type_name_${props.currentLanguage}`]} - ${
            accommodationsRequestData[`test_skill_sub_type_name_${props.currentLanguage}`]
          }`
        : `${accommodationsRequestData[`test_skill_type_name_${props.currentLanguage}`]}`;

    return formattedTestSkill;
  };

  const isOla = () => {
    return (
      props.selectedUserAccommodationFileData.test_skill_sub_type_codename === TestSkillSleDescCodename.ORAL_EN ||
      props.selectedUserAccommodationFileData.test_skill_sub_type_codename === TestSkillSleDescCodename.ORAL_FR
    );
  };

  const handleApproveAccommodationsRequest = () => {
    // creating body
    const body = {
      user_accommodation_file_id: props.userAccommodationFileId,
      action: USER_ACCOMMODATION_FILE_STATUS_CONST.completed
    };
    props.triggerUserAccommodationFileStatusUpdateAsACandidate(body).then(response => {
      // success
      if (response.ok) {
        // show approval success popup
        setAccommodationsRequestData(response);
        setShowApprovalSuccessPopup(true);
        // should never happen
      } else {
        throw new Error("An error occurred during the approval of the accommodation request");
      }
    });
  };

  const closeApprovalSuccessPopup = () => {
    setShowApprovalSuccessPopup(false);
  };

  // setting textareas height automatically
  setTimeout(() => {
    // ==================== GENERAL INFO ====================
    // making sure that the id exists
    if (document.getElementById("accommodations-request-details-general-info-input")) {
      // updating element height style
      document.getElementById(
        "accommodations-request-details-general-info-input"
      ).style.height = `${
        // + 2 for adjustments
        document.getElementById("accommodations-request-details-general-info-input").scrollHeight +
        2
      }px`;
    }
    // ==================== GENERAL INFO (END) ====================

    // ==================== OTHER MEASURES ====================
    // making sure that the id exists
    if (document.getElementById("accommodations-request-details-other-measures-input")) {
      // updating element height style
      document.getElementById(
        "accommodations-request-details-other-measures-input"
      ).style.height = `${
        // + 2 for adjustments
        document.getElementById("accommodations-request-details-other-measures-input")
          .scrollHeight + 2
      }px`;
    }
    // ==================== OTHER MEASURES (END) ====================
  });

  let accommodationsStyle = {
    fontSize: props.accommodations.fontSize
  };
  const lineSpacingStyle = getLineSpacingCSS();
  if (props.accommodations.spacing) {
    accommodationsStyle = {
      ...accommodationsStyle,
      ...{
        lineHeight: lineSpacingStyle.lineHeight,
        letterSpacing: lineSpacingStyle.letterSpacing,
        wordSpacing: lineSpacingStyle.wordSpacing
      }
    };
  }

  // getting current date
  let currentDate = new Date();
  currentDate = getFormattedDate(
    `${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-${currentDate.getDate()}`
  );

  const handlePrint = () => {
    setTriggerReactToPrint(true);
  };

  // equivalent of componentDidMount
  useEffect(() => {
    getUserAccommodationFileData();
  }, []);

  // equivalent of componentDidUpdate
  // if triggerReactToPrint gets updated
  useEffect(() => {
    if (triggerReactToPrint) {
      setTriggerReactToPrint(false);
    }
  }, [triggerReactToPrint]);

  // equivalent of componentDidUpdate
  // if accommodationsRequestData gets updated
  useEffect(() => {
    if (Object.keys(accommodationsRequestData).length > 0) {
      setIsLoadingData(false);
    }
  }, [accommodationsRequestData]);

  // equivalent of componentDidUpdate
  // if actionInProgress gets updated
  useEffect(() => {
    if (actionInProgress) {
      handleCancelRequestAction();
    }
  }, [actionInProgress]);

  return (
    <>
      {!isLoadingData && (
        <PopupBox
          show={props.showPopup}
          title={
            props.selectedUserAccommodationFileData.is_alternate_test_request
              ? LOCALIZE.accommodationsRequestDetailsPopup.titleAlternateTestRequest
              : LOCALIZE.accommodationsRequestDetailsPopup.title
          }
          size={props.popupSize}
          handleClose={props.closeAccommodationsRequestDetailsPopup}
          closeOnButtonAction={false}
          shouldCloseOnEsc={true}
          closeButtonAction={props.closeAccommodationsRequestDetailsPopup}
          displayCloseButton={
            showSimplifiedFooter()
              ? false
              : (accommodationsRequestData.status_codename ===
                  USER_ACCOMMODATION_FILE_STATUS_CONST.pendingApproval ||
                  accommodationsRequestData.status_codename ===
                    USER_ACCOMMODATION_FILE_STATUS_CONST.completed ||
                  accommodationsRequestData.status_codename ===
                    USER_ACCOMMODATION_FILE_STATUS_CONST.administered) &&
                !props.selectedUserAccommodationFileData.is_alternate_test_request
          }
          isBackdropStatic={true}
          description={
            <div>
              <p>
                {props.selectedUserAccommodationFileData.is_alternate_test_request ? (
                  <div>
                    <div>
                      {LOCALIZE.formatString(
                        LOCALIZE.accommodationsRequestDetailsPopup.descriptionAlternateTestRequest1,
                        <span style={styles.boldText}>
                          {accommodationsRequestData.user_first_name}
                        </span>,
                        <span style={styles.boldText}>
                          {accommodationsRequestData.user_last_name}
                        </span>,
                        <span style={styles.boldText}>{accommodationsRequestData.user_email}</span>
                      )}
                    </div>
                    <div>
                      {LOCALIZE.formatString(
                        LOCALIZE.accommodationsRequestDetailsPopup.descriptionAlternateTestRequest2,
                        <span style={styles.boldText}>
                          {accommodationsRequestData[`test_name_${props.currentLanguage}`]}
                        </span>
                      )}
                    </div>
                    <div>
                      {LOCALIZE.accommodationsRequestDetailsPopup.descriptionAlternateTestRequest3}
                    </div>
                  </div>
                ) : (
                  LOCALIZE.formatString(
                    LOCALIZE.accommodationsRequestDetailsPopup.description,
                    <span style={styles.boldText}>
                      {props.source === ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.hr ||
                      accommodationsRequestData[`test_name_${props.currentLanguage}`] === null
                        ? getFormattedTestSkill()
                        : accommodationsRequestData[`test_name_${props.currentLanguage}`]}
                    </span>
                  )
                )}
              </p>
              <div style={styles.formContainer}>
                <Row className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                  >
                    <label
                      htmlFor="accommodations-request-details-request-date-input"
                      style={styles.boldText}
                    >
                      {LOCALIZE.accommodationsRequestDetailsPopup.requestDateLabel}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <input
                      id="accommodations-request-details-request-date-input"
                      tabIndex={0}
                      className={"valid-field input-disabled-look"}
                      type="text"
                      value={accommodationsRequestData.created_date}
                      style={{
                        ...styles.inputs,
                        ...accommodationsStyle
                      }}
                      aria-disabled={true}
                    />
                  </Col>
                </Row>
                <Row className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                  >
                    <label
                      htmlFor="accommodations-request-details-request-number-input"
                      style={styles.boldText}
                    >
                      {LOCALIZE.accommodationsRequestDetailsPopup.requestNumberLabel}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <input
                      id="accommodations-request-details-request-number-input"
                      tabIndex={0}
                      className={"valid-field input-disabled-look"}
                      type="text"
                      value={accommodationsRequestData.id}
                      style={{
                        ...styles.inputs,
                        ...accommodationsStyle
                      }}
                      aria-disabled={true}
                    />
                  </Col>
                </Row>
                <Row className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                  >
                    <label
                      htmlFor="accommodations-request-details-request-status-input"
                      style={styles.boldText}
                    >
                      {LOCALIZE.accommodationsRequestDetailsPopup.requestStatusLabel}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <input
                      id="accommodations-request-details-request-status-input"
                      tabIndex={0}
                      className={"valid-field input-disabled-look"}
                      type="text"
                      value={
                        (accommodationsRequestData.status_codename ===
                          USER_ACCOMMODATION_FILE_STATUS_CONST.completed &&
                          accommodationsRequestData.request_completed_date !== null) ||
                        (accommodationsRequestData.status_codename ===
                          USER_ACCOMMODATION_FILE_STATUS_CONST.administered &&
                          accommodationsRequestData.request_administered_date)
                          ? `${
                              accommodationsRequestData.status_data[props.currentLanguage][0].text
                            } (${
                              accommodationsRequestData.status_codename ===
                              USER_ACCOMMODATION_FILE_STATUS_CONST.completed
                                ? accommodationsRequestData.request_completed_date
                                : accommodationsRequestData.request_administered_date
                            })`
                          : accommodationsRequestData.status_data[props.currentLanguage][0].text
                      }
                      style={{
                        ...styles.inputs,
                        ...accommodationsStyle
                      }}
                      aria-disabled={true}
                    />
                  </Col>
                </Row>
                {/* OLA TEST */}
                {(accommodationsRequestData.test_skill_sub_type_codename ===
                  TestSkillSleDescCodename.ORAL_EN ||
                  accommodationsRequestData.test_skill_sub_type_codename ===
                    TestSkillSleDescCodename.ORAL_FR) && (
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        id="accommodations-request-details-phone-number-label"
                        htmlFor="accommodations-request-details-phone-number-input"
                        style={styles.boldText}
                      >
                        {LOCALIZE.accommodationsRequestDetailsPopup.phoneNumberLabel}
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumnPhoneNumber.xl}
                      lg={columnSizes.secondColumnPhoneNumber.lg}
                      md={columnSizes.secondColumnPhoneNumber.md}
                      sm={columnSizes.secondColumnPhoneNumber.sm}
                      xs={columnSizes.secondColumnPhoneNumber.xs}
                    >
                      <InputMask
                        id="accommodations-request-details-phone-number-input"
                        className={"valid-field input-disabled-look"}
                        aria-labelledby="accommodations-request-details-phone-number-label"
                        style={{ ...styles.inputs, ...accommodationsStyle }}
                        type="text"
                        value={accommodationsRequestData.candidate_phone_number}
                        mask={MASKS.phoneNumber}
                        formatChars={MASK_FORMAT_CHARS.phoneNumber}
                        onChange={() => {}}
                        aria-disabled={true}
                      />
                    </Col>
                    {props.source === ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.candidate && (
                      <Col
                        xl={columnSizes.thirdColumn.xl}
                        lg={columnSizes.thirdColumn.lg}
                        md={columnSizes.thirdColumn.md}
                        sm={columnSizes.thirdColumn.sm}
                        xs={columnSizes.thirdColumn.xs}
                        style={styles.editPhoneNumberButtonCol}
                      >
                        <StyledTooltip
                          id="accommodations-request-details-phone-number-edit-button"
                          place="top"
                          variant={TYPE.light}
                          effect={EFFECT.solid}
                          openOnClick={false}
                          tooltipElement={
                            <div style={styles.allUnset}>
                              <CustomButton
                                dataTip=""
                                dataFor="accommodations-request-details-phone-number-edit-button"
                                label={<FontAwesomeIcon icon={faPencil} />}
                                ariaLabel={LOCALIZE.commons.editButton}
                                action={() =>
                                  openEditPhoneMumberPopup(
                                    accommodationsRequestData.candidate_phone_number
                                  )
                                }
                                customStyle={styles.actionButton}
                                buttonTheme={THEME.PRIMARY}
                              />
                            </div>
                          }
                          tooltipContent={
                            <div>
                              <p>{LOCALIZE.commons.editButton}</p>
                            </div>
                          }
                        />
                      </Col>
                    )}
                  </Row>
                )}
                {/* show only if request is pending approval/completed/administered or if HR/AAE user AND not alternate test request */}
                {(accommodationsRequestData.status_codename ===
                  USER_ACCOMMODATION_FILE_STATUS_CONST.pendingApproval ||
                  accommodationsRequestData.status_codename ===
                    USER_ACCOMMODATION_FILE_STATUS_CONST.completed ||
                  accommodationsRequestData.status_codename ===
                    USER_ACCOMMODATION_FILE_STATUS_CONST.administered ||
                  props.source === ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.hr ||
                  props.source === ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.aae) && (
                  <>
                    {/* show only if accommodation request (not alternate test request) */}
                    {!props.selectedUserAccommodationFileData.is_alternate_test_request && (
                      <>
                        <Row className="align-items-center" style={styles.rowContainer}>
                          <Col
                            xl={columnSizes.firstColumn.xl}
                            lg={columnSizes.firstColumn.lg}
                            md={columnSizes.firstColumn.md}
                            sm={columnSizes.firstColumn.sm}
                            xs={columnSizes.firstColumn.xs}
                          >
                            <label
                              htmlFor="accommodations-request-details-aae-expert-input"
                              style={styles.boldText}
                            >
                              {LOCALIZE.accommodationsRequestDetailsPopup.aaeExpertLabel}
                            </label>
                          </Col>
                          <Col
                            xl={columnSizes.secondColumn.xl}
                            lg={columnSizes.secondColumn.lg}
                            md={columnSizes.secondColumn.md}
                            sm={columnSizes.secondColumn.sm}
                            xs={columnSizes.secondColumn.xs}
                          >
                            <input
                              id="accommodations-request-details-aae-expert-input"
                              tabIndex={0}
                              className={"valid-field input-disabled-look"}
                              type="text"
                              value={
                                accommodationsRequestData.assigned_to_user_id !== null
                                  ? `${accommodationsRequestData.assigned_to_user_first_name} ${accommodationsRequestData.assigned_to_user_last_name}`
                                  : ""
                              }
                              style={{
                                ...styles.inputs,
                                ...accommodationsStyle
                              }}
                              aria-disabled={true}
                            />
                          </Col>
                        </Row>
                        <Row className="align-items-center" style={styles.rowContainer}>
                          <Col
                            xl={columnSizes.firstColumn.xl}
                            lg={columnSizes.firstColumn.lg}
                            md={columnSizes.firstColumn.md}
                            sm={columnSizes.firstColumn.sm}
                            xs={columnSizes.firstColumn.xs}
                          >
                            <label
                              htmlFor="accommodations-request-details-aae-expert-email-input"
                              style={styles.boldText}
                            >
                              {LOCALIZE.accommodationsRequestDetailsPopup.aaeExpertEmailLabel}
                            </label>
                          </Col>
                          <Col
                            xl={columnSizes.secondColumn.xl}
                            lg={columnSizes.secondColumn.lg}
                            md={columnSizes.secondColumn.md}
                            sm={columnSizes.secondColumn.sm}
                            xs={columnSizes.secondColumn.xs}
                          >
                            <input
                              id="accommodations-request-details-aae-expert-email-input"
                              tabIndex={0}
                              className={"valid-field input-disabled-look"}
                              type="text"
                              value={
                                accommodationsRequestData.assigned_to_user_id !== null
                                  ? accommodationsRequestData.assigned_to_user_email
                                  : ""
                              }
                              style={{
                                ...styles.inputs,
                                ...accommodationsStyle
                              }}
                              aria-disabled={true}
                            />
                          </Col>
                        </Row>
                      </>
                    )}
                    {/* not an HR user */}
                    {props.source !== ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.hr && (
                      <>
                        {/* not a candidate and accommodation request only (not alternate test request) */}
                        {props.source !== ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.candidate &&
                          !props.selectedUserAccommodationFileData.is_alternate_test_request && (
                            <Row className="align-items-center" style={styles.rowContainer}>
                              <Col
                                xl={columnSizes.firstColumn.xl}
                                lg={columnSizes.firstColumn.lg}
                                md={columnSizes.firstColumn.md}
                                sm={columnSizes.firstColumn.sm}
                                xs={columnSizes.firstColumn.xs}
                              >
                                <label
                                  htmlFor="accommodations-request-details-hr-contact-input"
                                  style={styles.boldText}
                                >
                                  {LOCALIZE.accommodationsRequestDetailsPopup.hrContactInfoLabel}
                                </label>
                              </Col>
                              <Col
                                xl={columnSizes.secondColumn.xl}
                                lg={columnSizes.secondColumn.lg}
                                md={columnSizes.secondColumn.md}
                                sm={columnSizes.secondColumn.sm}
                                xs={columnSizes.secondColumn.xs}
                              >
                                <input
                                  id="accommodations-request-details-hr-contact-input"
                                  tabIndex={0}
                                  className={"valid-field input-disabled-look"}
                                  type="text"
                                  value={
                                    accommodationsRequestData.hr_user_id !== null
                                      ? `${accommodationsRequestData.hr_first_name} ${accommodationsRequestData.hr_last_name} (${accommodationsRequestData.hr_email})`
                                      : ""
                                  }
                                  style={{
                                    ...styles.inputs,
                                    ...accommodationsStyle
                                  }}
                                  aria-disabled={true}
                                />
                              </Col>
                            </Row>
                          )}
                        {/* candidate or AAE and for accommodation request only (not for alternate test request) */}
                        {(props.source === ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.candidate ||
                          props.source === ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.aae) &&
                          !props.selectedUserAccommodationFileData.is_alternate_test_request && (
                            <Row className="align-items-center" style={styles.rowContainer}>
                              <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                                <label
                                  htmlFor="accommodations-request-details-general-info-input"
                                  style={styles.boldText}
                                >
                                  {
                                    LOCALIZE.accommodationsRequestDetailsPopup.generalInfo
                                      .generalInfoLabel
                                  }
                                </label>
                              </Col>
                              <Col
                                xl={12}
                                lg={12}
                                md={12}
                                sm={12}
                                xs={12}
                                style={styles.textAreaCol}
                              >
                                <div
                                  style={{
                                    ...styles.inputs,
                                    ...styles.textAreaInput,
                                    ...accommodationsStyle,
                                    ...styles.divTextArea
                                  }}
                                >
                                  <h3>
                                    {
                                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo
                                        .generalInfoContent.introductionHeader
                                    }
                                  </h3>
                                  <p>
                                    {
                                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo
                                        .generalInfoContent.introductionParagraph1
                                    }
                                  </p>
                                  <p>
                                    {
                                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo
                                        .generalInfoContent.introductionParagraph2
                                    }
                                  </p>
                                  <p>
                                    {
                                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo
                                        .generalInfoContent.introductionParagraph3
                                    }
                                  </p>
                                  <p>
                                    {
                                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo
                                        .generalInfoContent.introductionParagraph4
                                    }
                                  </p>
                                  <h3>
                                    {
                                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo
                                        .generalInfoContent.importantNotesHeader
                                    }
                                  </h3>
                                  <p>
                                    {
                                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo
                                        .generalInfoContent.importantNotesParagraph1
                                    }
                                  </p>
                                  <p>
                                    {
                                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo
                                        .generalInfoContent.importantNotesParagraph2
                                    }
                                  </p>
                                  <p>
                                    {
                                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo
                                        .generalInfoContent.importantNotesParagraph3
                                    }
                                  </p>
                                  <h3>
                                    {
                                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo
                                        .generalInfoContent.candidateDeclarationHeader
                                    }
                                  </h3>
                                  <p>
                                    {
                                      LOCALIZE.accommodationsRequestDetailsPopup.generalInfo
                                        .generalInfoContent.candidateDeclarationParagraph
                                    }
                                  </p>
                                </div>
                              </Col>
                            </Row>
                          )}
                        <Row className="align-items-center" style={styles.rowContainer}>
                          <Col
                            xl={columnSizes.firstColumn.xl}
                            lg={columnSizes.firstColumn.lg}
                            md={columnSizes.firstColumn.md}
                            sm={columnSizes.firstColumn.sm}
                            xs={columnSizes.firstColumn.xs}
                          >
                            <label
                              htmlFor="accommodations-request-details-test-to-administer-input"
                              style={styles.boldText}
                            >
                              {LOCALIZE.accommodationsRequestDetailsPopup.testToAdministerLabel}
                            </label>
                          </Col>
                          <Col
                            xl={columnSizes.secondColumn.xl}
                            lg={columnSizes.secondColumn.lg}
                            md={columnSizes.secondColumn.md}
                            sm={columnSizes.secondColumn.sm}
                            xs={columnSizes.secondColumn.xs}
                          >
                            <input
                              id="accommodations-request-details-test-to-administer-input"
                              tabIndex={0}
                              className={"valid-field input-disabled-look"}
                              type="text"
                              value={
                                accommodationsRequestData.test_code !== null
                                  ? `${accommodationsRequestData.test_code} - ${
                                      accommodationsRequestData[
                                        `test_name_${props.currentLanguage}`
                                      ]
                                    }`
                                  : accommodationsRequestData.specified_test_description !== null
                                  ? accommodationsRequestData.specified_test_description
                                  : ""
                              }
                              style={{
                                ...styles.inputs,
                                ...accommodationsStyle
                              }}
                              aria-disabled={true}
                            />
                          </Col>
                        </Row>
                        {/* show only if accommodation request (not alternate test request) */}
                        {!props.selectedUserAccommodationFileData.is_alternate_test_request &&
                          !isOla() && (
                            <>
                              <Row className="align-items-center" style={styles.rowContainer}>
                                <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                                  <label
                                    id="accommodations-request-details-timing-and-breaks-label"
                                    style={styles.boldText}
                                  >
                                    {
                                      LOCALIZE.accommodationsRequestDetailsPopup
                                        .timingAndBreaksTitle
                                    }
                                  </label>
                                </Col>
                              </Row>
                              <Row className="align-items-center" style={styles.rowContainer}>
                                <Col
                                  xl={columnSizes.firstColumn.xl}
                                  lg={columnSizes.firstColumn.lg}
                                  md={columnSizes.firstColumn.md}
                                  sm={columnSizes.firstColumn.sm}
                                  xs={columnSizes.firstColumn.xs}
                                  style={styles.indemptedCol}
                                >
                                  <label
                                    id="accommodations-request-details-total-test-time-label"
                                    style={styles.boldText}
                                  >
                                    {LOCALIZE.accommodationsRequestDetailsPopup.totalTestTimeLabel}
                                  </label>
                                </Col>
                                <Col
                                  xl={columnSizes.secondColumn.xl}
                                  lg={columnSizes.secondColumn.lg}
                                  md={columnSizes.secondColumn.md}
                                  sm={columnSizes.secondColumn.sm}
                                  xs={columnSizes.secondColumn.xs}
                                >
                                  <input
                                    id="accommodations-request-details-total-test-time-input"
                                    tabIndex={0}
                                    className={"valid-field input-disabled-look"}
                                    aria-labelledby="accommodations-request-details-timing-and-breaks-label accommodations-request-details-total-test-time-label"
                                    type="text"
                                    value={getFormattedTotalTestTime(
                                      accommodationsRequestData.total_default_time,
                                      accommodationsRequestData.total_adjusted_time
                                    )}
                                    style={{
                                      ...styles.inputs,
                                      ...accommodationsStyle
                                    }}
                                    aria-disabled={true}
                                  />
                                </Col>
                              </Row>
                              {accommodationsRequestData.break_time !== null &&
                                accommodationsRequestData.break_time > 0 && (
                                  <Row className="align-items-center" style={styles.rowContainer}>
                                    <Col
                                      xl={columnSizes.firstColumn.xl}
                                      lg={columnSizes.firstColumn.lg}
                                      md={columnSizes.firstColumn.md}
                                      sm={columnSizes.firstColumn.sm}
                                      xs={columnSizes.firstColumn.xs}
                                      style={styles.indemptedCol}
                                    >
                                      <label
                                        id="accommodations-request-details-total-break-bank-label"
                                        style={styles.boldText}
                                      >
                                        {
                                          LOCALIZE.accommodationsRequestDetailsPopup
                                            .totalBreakBankLabel
                                        }
                                      </label>
                                    </Col>
                                    <Col
                                      xl={columnSizes.secondColumn.xl}
                                      lg={columnSizes.secondColumn.lg}
                                      md={columnSizes.secondColumn.md}
                                      sm={columnSizes.secondColumn.sm}
                                      xs={columnSizes.secondColumn.xs}
                                    >
                                      <input
                                        id="accommodations-request-details-total-test-time-input"
                                        tabIndex={0}
                                        className={"valid-field input-disabled-look"}
                                        aria-labelledby="accommodations-request-details-timing-and-breaks-label accommodations-request-details-total-break-bank-label"
                                        type="text"
                                        value={`${accommodationsRequestData.break_time} ${LOCALIZE.commons.minutes}`}
                                        style={{
                                          ...styles.inputs,
                                          ...accommodationsStyle
                                        }}
                                        aria-disabled={true}
                                      />
                                    </Col>
                                  </Row>
                                )}
                              <Row className="align-items-center" style={styles.rowContainer}>
                                <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                                  <label
                                    htmlFor="accommodations-request-details-other-measures-input"
                                    style={styles.boldText}
                                  >
                                    {LOCALIZE.accommodationsRequestDetailsPopup.otherMeasuresLabel}
                                  </label>
                                </Col>
                                <Col
                                  xl={12}
                                  lg={12}
                                  md={12}
                                  sm={12}
                                  xs={12}
                                  style={styles.textAreaCol}
                                >
                                  <textarea
                                    id="accommodations-request-details-other-measures-input"
                                    tabIndex={0}
                                    className={"valid-field input-disabled-look"}
                                    type="text"
                                    value={accommodationsRequestData.other_measures}
                                    style={{
                                      ...styles.inputs,
                                      ...styles.textAreaInput,
                                      ...accommodationsStyle
                                    }}
                                    aria-disabled={true}
                                  />
                                </Col>
                              </Row>
                              {/* status is pending_approval + user is a candidate */}
                              {accommodationsRequestData.status_codename ===
                                USER_ACCOMMODATION_FILE_STATUS_CONST.pendingApproval &&
                                props.source ===
                                  ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.candidate && (
                                  <p style={styles.printDownloadParagraph}>
                                    {LOCALIZE.formatString(
                                      LOCALIZE.accommodationsRequestDetailsPopup
                                        .printDownloadDescription,
                                      <>
                                        <button
                                          aria-label={
                                            LOCALIZE.accommodationsRequestDetailsPopup.printDownload
                                          }
                                          tabIndex="0"
                                          onClick={handlePrint}
                                          style={styles.printDownloadLink}
                                        >
                                          {LOCALIZE.accommodationsRequestDetailsPopup.printDownload}
                                        </button>
                                        <FontAwesomeIcon
                                          icon={faPrint}
                                          style={styles.printDownloadIcon}
                                        />
                                      </>
                                    )}
                                  </p>
                                )}
                            </>
                          )}
                      </>
                    )}
                  </>
                )}
              </div>
              {triggerReactToPrint && (
                <ReactToPrint
                  contentRef={componentToPrintRef}
                  documentTitle={LOCALIZE.formatString(
                    LOCALIZE.accommodationsRequestDetailsPopup.printDocumentTitle,
                    currentDate
                  )}
                  content={getAccommodationsRequestDetailsContentToPrint(
                    accommodationsRequestData,
                    props.currentLanguage,
                    // isDraft props provided
                    typeof props.isDraft !== "undefined"
                      ? // isDraft bool value
                        props.isDraft
                      : // called from candidate
                      props.source === ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.candidate
                      ? // status completed/administered ==> false (not a draft) else true (draft)
                        accommodationsRequestData.status_data[props.currentLanguage][0]
                          .user_accommodation_file_status_codename !==
                          USER_ACCOMMODATION_FILE_STATUS_CONST.completed &&
                        accommodationsRequestData.status_data[props.currentLanguage][0]
                          .user_accommodation_file_status_codename !==
                          USER_ACCOMMODATION_FILE_STATUS_CONST.administered
                      : // else draft
                        true
                  )}
                />
              )}
            </div>
          }
          leftButtonType={
            props.source === ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.hr
              ? BUTTON_TYPE.none
              : props.source !== ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.candidate &&
                props.selectedUserAccommodationFileData.is_alternate_test_request
              ? BUTTON_TYPE.none
              : showSimplifiedFooter()
              ? BUTTON_TYPE.secondary
              : accommodationsRequestData.status_data[props.currentLanguage][0]
                  .user_accommodation_file_status_codename !==
                  USER_ACCOMMODATION_FILE_STATUS_CONST.cancelled &&
                accommodationsRequestData.status_data[props.currentLanguage][0]
                  .user_accommodation_file_status_codename !==
                  USER_ACCOMMODATION_FILE_STATUS_CONST.cancelledByCandidate
              ? BUTTON_TYPE.danger
              : BUTTON_TYPE.none
          }
          leftButtonTitle={
            showSimplifiedFooter()
              ? LOCALIZE.accommodationsRequestDetailsPopup.printDownload
              : LOCALIZE.accommodationsRequestDetailsPopup.cancelRequestButton
          }
          leftButtonIcon={
            showSimplifiedFooter()
              ? faPrint
              : accommodationsRequestData.status_data[props.currentLanguage][0]
                  .user_accommodation_file_status_codename !==
                  USER_ACCOMMODATION_FILE_STATUS_CONST.cancelled &&
                accommodationsRequestData.status_data[props.currentLanguage][0]
                  .user_accommodation_file_status_codename !==
                  USER_ACCOMMODATION_FILE_STATUS_CONST.cancelledByCandidate
              ? faTrash
              : ""
          }
          leftButtonAction={
            showSimplifiedFooter() ? handlePrint : openCancelRequestConfirmationPopup
          }
          rightButtonType={
            showSimplifiedFooter()
              ? BUTTON_TYPE.primary
              : accommodationsRequestData.status_codename ===
                USER_ACCOMMODATION_FILE_STATUS_CONST.pendingApproval
              ? BUTTON_TYPE.success
              : BUTTON_TYPE.primary
          }
          rightButtonTitle={
            showSimplifiedFooter()
              ? LOCALIZE.commons.close
              : accommodationsRequestData.status_codename ===
                USER_ACCOMMODATION_FILE_STATUS_CONST.pendingApproval
              ? LOCALIZE.commons.agree
              : (accommodationsRequestData.status_codename ===
                  USER_ACCOMMODATION_FILE_STATUS_CONST.completed ||
                  accommodationsRequestData.status_codename ===
                    USER_ACCOMMODATION_FILE_STATUS_CONST.administered) &&
                !props.selectedUserAccommodationFileData.is_alternate_test_request
              ? LOCALIZE.accommodationsRequestDetailsPopup.printDownload
              : LOCALIZE.commons.close
          }
          rightButtonIcon={
            showSimplifiedFooter()
              ? faTimes
              : accommodationsRequestData.status_codename ===
                USER_ACCOMMODATION_FILE_STATUS_CONST.pendingApproval
              ? faCheck
              : (accommodationsRequestData.status_codename ===
                  USER_ACCOMMODATION_FILE_STATUS_CONST.completed ||
                  accommodationsRequestData.status_codename ===
                    USER_ACCOMMODATION_FILE_STATUS_CONST.administered) &&
                !props.selectedUserAccommodationFileData.is_alternate_test_request
              ? faPrint
              : faTimes
          }
          rightButtonAction={
            showSimplifiedFooter()
              ? props.closeAccommodationsRequestDetailsPopup
              : accommodationsRequestData.status_codename ===
                USER_ACCOMMODATION_FILE_STATUS_CONST.pendingApproval
              ? handleApproveAccommodationsRequest
              : (accommodationsRequestData.status_codename ===
                  USER_ACCOMMODATION_FILE_STATUS_CONST.completed ||
                  accommodationsRequestData.status_codename ===
                    USER_ACCOMMODATION_FILE_STATUS_CONST.administered) &&
                !props.selectedUserAccommodationFileData.is_alternate_test_request
              ? handlePrint
              : props.closeAccommodationsRequestDetailsPopup
          }
        />
      )}
      <PopupBox
        show={showCancelRequestConfirmationPopup}
        title={LOCALIZE.accommodationsRequestDetailsPopup.cancelRequestConfirmationPopup.title}
        handleClose={() => {}}
        size={"lg"}
        description={
          <div>
            <p>
              {
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.warning}
                  message={
                    props.selectedUserAccommodationFileData.is_alternate_test_request
                      ? LOCALIZE.accommodationsRequestDetailsPopup.cancelRequestConfirmationPopup
                          .systemMessageDescriptionAlternateTestRequest
                      : LOCALIZE.accommodationsRequestDetailsPopup.cancelRequestConfirmationPopup
                          .systemMessageDescription
                  }
                />
              }
            </p>
            <p>
              {
                LOCALIZE.accommodationsRequestDetailsPopup.cancelRequestConfirmationPopup
                  .description
              }
            </p>
          </div>
        }
        leftButtonType={BUTTON_TYPE.secondary}
        leftButtonTitle={LOCALIZE.commons.cancel}
        leftButtonIcon={faTimes}
        leftButtonAction={closeCancelRequestConfirmationPopup}
        rightButtonType={BUTTON_TYPE.danger}
        rightButtonTitle={
          actionInProgress ? (
            <div style={styles.customLoadingContainer}>
              {/* this div is useful to get the right size of the button while loading */}
              <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
                <FontAwesomeIcon icon={faTrash} style={styles.icon} />
                {LOCALIZE.accommodationsRequestDetailsPopup.cancelRequestButton}
              </div>
              <div style={styles.loadingOverlappingStyle}>
                <label className="fa fa-spinner fa-spin">
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              </div>
            </div>
          ) : (
            LOCALIZE.accommodationsRequestDetailsPopup.cancelRequestButton
          )
        }
        rightButtonIcon={actionInProgress ? "" : faTrash}
        rightButtonAction={() => setActionInProgress(true)}
        rightButtonState={actionInProgress ? BUTTON_STATE.disabled : BUTTON_STATE.enabled}
      />
      <PopupBox
        show={showApprovalSuccessPopup}
        title={LOCALIZE.accommodationsRequestDetailsPopup.approvalSuccessPopup.title}
        handleClose={() => {}}
        size={"lg"}
        description={
          <div>
            <p>
              {
                <SystemMessage
                  messageType={MESSAGE_TYPE.success}
                  title={LOCALIZE.commons.success}
                  message={
                    LOCALIZE.accommodationsRequestDetailsPopup.approvalSuccessPopup
                      .systemMessageDescription
                  }
                />
              }
            </p>
          </div>
        }
        rightButtonType={BUTTON_TYPE.primary}
        rightButtonTitle={LOCALIZE.commons.close}
        rightButtonIcon={faTimes}
        rightButtonAction={closeApprovalSuccessPopup}
      />
      <PopupBox
        show={showEditPhoneNumberPopup}
        title={
          LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup.editPhoneNumberPopop
            .title
        }
        handleClose={() => {}}
        size={"lg"}
        description={
          <div>
            <Row role="presentation" className="align-items-center">
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
              >
                <label
                  id="accommodations-request-details-edit-phone-number-popup-phone-number-label"
                  htmlFor="accommodations-request-details-edit-phone-number-popup-phone-number-input"
                  style={styles.formLabel}
                >
                  {LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup.phoneNumber}
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumnPhoneNumber.xl}
                lg={columnSizes.secondColumnPhoneNumber.lg}
                md={columnSizes.secondColumnPhoneNumber.md}
                sm={columnSizes.secondColumnPhoneNumber.sm}
                xs={columnSizes.secondColumnPhoneNumber.xs}
              >
                <InputMask
                  id="accommodations-request-details-edit-phone-number-popup-phone-number-input"
                  className={"valid-field"}
                  aria-labelledby="accommodations-request-details-edit-phone-number-popup-phone-number-label"
                  aria-required={true}
                  style={{ ...styles.inputs, ...accommodationsStyle }}
                  type="text"
                  value={tempPhoneNumberContent}
                  mask={MASKS.phoneNumber}
                  formatChars={MASK_FORMAT_CHARS.phoneNumber}
                  onChange={updateTempPhoneNumber}
                />
              </Col>
            </Row>
          </div>
        }
        leftButtonType={BUTTON_TYPE.secondary}
        leftButtonTitle={LOCALIZE.commons.cancel}
        leftButtonAction={closeEditPhoneMumberPopup}
        leftButtonIcon={faTimes}
        rightButtonType={BUTTON_TYPE.primary}
        rightButtonTitle={LOCALIZE.commons.editButton}
        rightButtonAction={handleEditPhoneNumber}
        rightButtonIcon={faPencil}
        rightButtonState={
          isValidTempPhoneNumberContent ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
        }
      />
    </>
  );
}

export { AccommodationsRequestDetailsPopup as unconnectedAccommodationsRequestDetailsPopup };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAccommodationFileDataForDetailsPopup,
      triggerUserAccommodationFileStatusUpdateAsACandidate,
      editUserAccommodationFilePhoneNumber
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AccommodationsRequestDetailsPopup);
