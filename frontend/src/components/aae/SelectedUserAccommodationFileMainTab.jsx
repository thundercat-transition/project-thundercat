/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import SideNavigation from "../eMIB/SideNavigation";
import SelectedUserAccommodationFileMainTabCandidateData from "./SelectedUserAccommodationFileMainTabCandidateData";
import SelectedUserAccommodationFileMainTabTestToAdminister from "./SelectedUserAccommodationFileMainTabTestToAdminister";
import SelectedUserAccommodationFileMainTabBreakBankAndTimeAdjustment from "./SelectedUserAccommodationFileMainTabBreakBankAndTimeAdjustment";
import SelectedUserAccommodationFileMainTabOtherMeasures from "./SelectedUserAccommodationFileMainTabOtherMeasures";
import { setAaeSelectedUserAccommodationFileSideNavigationSelectedItem } from "../../modules/AaeRedux";
import getSelectedUserAccommodationFileHeightCalculations from "../../helpers/aaeHeightCalculations";
import SelectedUserAccommodationFileMainTabFileAdministration from "./SelectedUserAccommodationFileMainTabFileAdministration";
import SelectedUserAccommodationFileUserAccommodationsProfile from "./SelectedUserAccommodationFileUserAccommodationsProfile";
import SelectedUserAccommodationFileMainTabReports from "./SelectedUserAccommodationFileMainTabReports";
import { TestSkillSleDescCodename } from "../testFactory/Constants";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 3,
    xl: 3
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 9,
    xl: 9
  }
};

const styles = {
  mainContainer: {
    padding: "12px 16px",
    overflowY: "auto",
    backgroundColor: "#FAFAFA",
    borderLeft: "1px solid #CDCDCD",
    borderRight: "1px solid #CDCDCD"
  },
  tabContainer: {
    zIndex: 1,
    padding: 0,
    paddingBottom: 0
  },
  tabContent: {
    backgroundColor: "transparent",
    padding: "12px 12px",
    position: "sticky",
    top: 0,
    width: "100%",
    margin: "0 auto",
    paddingBottom: 0
  },
  nav: {
    marginTop: 10,
    marginLeft: 10,
    textAlign: "center"
  },
  sideNavBodyContent: {
    marginTop: "-12px"
  }
};

class SelectedUserAccommodationFileMainTab extends Component {
  state = {
    triggerRerender: false
  };

  getMainTabSections = () => {
    // Check if test is an OLA
    let isOla = false;
    if (
      this.props.aaeSelectedUserAccommodationFileData.test_skill_sub_type_codename ===
        TestSkillSleDescCodename.ORAL_EN ||
      this.props.aaeSelectedUserAccommodationFileData.test_skill_sub_type_codename ===
        TestSkillSleDescCodename.ORAL_FR
    ) {
      // Break time not enabled for OLA
      isOla = true;
    }

    const tabSectionsArray = [];

    tabSectionsArray.push(
      {
        menuString:
          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
            .candidateData.title,
        body: <SelectedUserAccommodationFileMainTabCandidateData />
      },
      {
        menuString:
          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
            .userAccommodationsProfile.title,
        body: <SelectedUserAccommodationFileUserAccommodationsProfile />
      },
      {
        menuString:
          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
            .testToAdminister.sideNavTitle,
        body: <SelectedUserAccommodationFileMainTabTestToAdminister />
      }
    );

    // not an OLA test -> show break time
    if (!isOla) {
      tabSectionsArray.push({
        menuString:
          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
            .breakBankAndTimeAdjustment.sideNavTitle,
        body: <SelectedUserAccommodationFileMainTabBreakBankAndTimeAdjustment />
      });
    }

    tabSectionsArray.push(
      {
        menuString:
          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
            .otherMeasures.sideNavTitle,
        body: <SelectedUserAccommodationFileMainTabOtherMeasures />
      },
      {
        menuString:
          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
            .fileAdministration.sideNavTitle,
        body: <SelectedUserAccommodationFileMainTabFileAdministration />
      },
      {
        menuString:
          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
            .reports.sideNavTitle,
        body: <SelectedUserAccommodationFileMainTabReports />
      }
    );

    return tabSectionsArray;
  };

  render() {
    const specs = this.getMainTabSections();

    // calculating the height of the top tabs and above
    // =========================================================================================
    let heightOfBackToUserAccommodationFileSelectionButton = 0;
    let heightOfTopTabs = 0;
    let heightOfFooter = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-user-accommodation-file-selection-btn") != null) {
      heightOfBackToUserAccommodationFileSelectionButton = document.getElementById(
        "back-to-user-accommodation-file-selection-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight += heightOfBackToUserAccommodationFileSelectionButton + heightOfTopTabs + 14; // height of "user accommodation file selection button" + height of margin under button

    if (document.getElementById("selected-user-accommodation-file-footer-main-div") !== null) {
      heightOfFooter = document.getElementById(
        "selected-user-accommodation-file-footer-main-div"
      ).offsetHeight;
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    const customHeight = getSelectedUserAccommodationFileHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight,
      heightOfFooter
    );
    // =========================================================================================

    return (
      <div style={{ ...styles.mainContainer, ...customHeight }}>
        <SideNavigation
          specs={specs}
          startIndex={this.props.aaeSelectedUserAccommodationFileSideNavigationSelectedItem}
          displayNextPreviousButton={false}
          isMain={true}
          tabContainerStyle={styles.tabContainer}
          tabContentStyle={styles.tabContent}
          navStyle={styles.nav}
          bodyContentCustomStyle={styles.sideNavBodyContent}
          updateSelectedSideNavItem={
            this.props.setAaeSelectedUserAccommodationFileSideNavigationSelectedItem
          }
          customColumnSizes={columnSizes}
        />
      </div>
    );
  }
}

export { SelectedUserAccommodationFileMainTab as unconnectedSelectedUserAccommodationFileMainTab };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testNavBarHeight: state.testSection.testNavBarHeight,
    aaeInitialSelectedUserAccommodationFileData:
      state.aae.aaeInitialSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileData: state.aae.aaeSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileSideNavigationSelectedItem:
      state.aae.aaeSelectedUserAccommodationFileSideNavigationSelectedItem
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setAaeSelectedUserAccommodationFileSideNavigationSelectedItem
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SelectedUserAccommodationFileMainTab);
