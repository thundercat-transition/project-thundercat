/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import getSelectedUserAccommodationFileHeightCalculations from "../../helpers/aaeHeightCalculations";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { setAaeSelectedUserAccommodationFileData } from "../../modules/AaeRedux";
import * as _ from "lodash";
import LOCALIZE from "../../text_resources";

const styles = {
  mainContainer: {
    padding: 12,
    overflowY: "auto",
    backgroundColor: "#FAFAFA",
    borderLeft: "1px solid #CDCDCD",
    borderRight: "1px solid #CDCDCD"
  },
  textAreaContainer: {
    margin: "12px 24px"
  },
  textArea: {
    padding: 6,
    border: "1px solid #00565E",
    borderRadius: 4,
    width: "100%",
    height: 450,
    resize: "vertical"
  },
  bold: {
    fontWeight: "bold"
  },
  heightAdjustment: {
    paddingTop: 9
  },
  noMargin: {
    margin: 0
  }
};

class SelectedUserAccommodationFileRationaleTab extends Component {
  state = {
    triggerRerender: false,
    typingTimerRedux: 0,
    rationaleContent: this.props.aaeSelectedUserAccommodationFileData.rationale
  };

  updateRationaleContent = event => {
    this.setState({ rationaleContent: event.target.value });

    // Create a buffer before updating the redux state
    // Clear the old typingTimerRedux
    clearTimeout(this.state.typingTimerRedux);

    // Change the typingTimerRedux to a timeout of 200 millisec
    // When user stops writing, this will go through after that time
    // Otherwise, the timer will be reset to that time
    const newTimer = setTimeout(() => {
      // Update redux --> update after typing timer
      // creating a copy of respective redux states
      const copyOfAaeSelectedUserAccommodationFileData = _.cloneDeep(
        this.props.aaeSelectedUserAccommodationFileData
      );
      // updating needed attributes
      copyOfAaeSelectedUserAccommodationFileData.rationale = event.target.value;
      this.props.setAaeSelectedUserAccommodationFileData(
        copyOfAaeSelectedUserAccommodationFileData
      );
    }, 200);

    this.setState({ typingTimerRedux: newTimer });
  };

  render() {
    // calculating the height of the top tabs and above
    // =========================================================================================
    let heightOfBackToUserAccommodationFileSelectionButton = 0;
    let heightOfTopTabs = 0;
    let heightOfFooter = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-user-accommodation-file-selection-btn") != null) {
      heightOfBackToUserAccommodationFileSelectionButton = document.getElementById(
        "back-to-user-accommodation-file-selection-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight += heightOfBackToUserAccommodationFileSelectionButton + heightOfTopTabs + 14; // height of "back to user accommodation file selection button" + height of margin under button

    if (document.getElementById("selected-user-accommodation-file-footer-main-div") !== null) {
      heightOfFooter = document.getElementById(
        "selected-user-accommodation-file-footer-main-div"
      ).offsetHeight;
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    const customHeight = getSelectedUserAccommodationFileHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight,
      heightOfFooter
    );
    // =========================================================================================

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={{ ...styles.mainContainer, ...customHeight }}>
        <h2 style={{ ...styles.noMargin, ...styles.heightAdjustment }}>
          {LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.other.rationale.title}
        </h2>
        <div style={styles.textAreaContainer}>
          <label htmlFor="user-accommodation-file-rationale-input" style={styles.bold}>
            {
              LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.other.rationale
                .rationaleLabel
            }
          </label>
          <textarea
            id="user-accommodation-file-rationale-input"
            className={"valid-field"}
            style={styles.textArea}
            value={this.state.rationaleContent}
            onChange={this.updateRationaleContent}
            maxLength="15000"
          />
        </div>
      </div>
    );
  }
}

export { SelectedUserAccommodationFileRationaleTab as unconnectedSelectedUserAccommodationFileRationaleTab };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testNavBarHeight: state.testSection.testNavBarHeight,
    aaeInitialSelectedUserAccommodationFileData:
      state.aae.aaeInitialSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileData: state.aae.aaeSelectedUserAccommodationFileData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setAaeSelectedUserAccommodationFileData
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectedUserAccommodationFileRationaleTab);
