/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col } from "react-bootstrap";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import {
  getAvailableTestsToAdministerForUserAccommodationFile,
  getTestCentersForUserAccommodationRequest,
  setAaeSelectedUserAccommodationFileData,
  setAaeSelectedUserAccommodationFileTestToAdministerValidState,
  setAaeSelectedUserAccommodationFileTestCenterValidState
} from "../../modules/AaeRedux";
import * as _ from "lodash";
import Switch from "react-switch";
import getSwitchTransformScale from "../../helpers/switchTransformScale";
import { getDefaultTestSectionsTime } from "../../modules/TestAdministrationRedux";
import { setDefaultTimes, updateTimerState } from "../../modules/SetTimerRedux";
import { getTimeInHoursMinutes } from "../../helpers/timeConversion";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { LANGUAGES } from "../commons/Translation";
import DropdownSelect from "../commons/DropdownSelect";
import { userAccommodationFileIsInEndState } from "./SelectedUserAccommodationFile";
import { TestSkillSleDescCodename } from "../testFactory/Constants";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 8
  },
  firstColumnSwitch: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 8
  },
  secondColumnSwitch: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 4
  }
};

const styles = {
  noMargin: {
    margin: 0
  },
  formContainer: {
    padding: 12
  },
  rowStyle: {
    padding: "6px 0px"
  },
  rowStyleWithError: {
    padding: "6px 0px 0 0px"
  },
  inputTitles: {
    fontWeight: "bold"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  tableMainContainer: {
    padding: "24px 12px"
  },
  radioButton: {
    transform: "scale(1.3)"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6,
    marginLeft: 6
  },
  switchLabel: {
    fontWeight: "bold",
    paddingRight: 12,
    margin: 0
  },
  loadingContainer: {
    textAlign: "center",
    transform: "scale(1.3)"
  }
};

class SelectedUserAccommodationFileMainTabTestToAdminister extends Component {
  state = {
    currentlyLoading: false,
    testCenterOptionsLoading: false,
    testCenterOptions: [],
    testCenterSelectedOption: [],
    rowsDefinition: {},
    availableTestsToAdminister: [],
    testCannotBePerformedInCatSwitchState: false,
    selectedTestToAdminister: null,
    specifiedTestDescription: "",
    isValidSpecifiedTestDescription: true,
    isLoadingSwitch: false,
    // default values
    switchHeight: 25,
    switchWidth: 50
  };

  componentDidMount = () => {
    // calling initial setup functions
    this.handleInitialTestToAdministerDataSetup();
    this.getUpdatedSwitchDimensions();
    this.handleDefaultTimerTimes();
    // populate populateTestCenterOptions only if it's a supervised request
    if (!this.props.aaeSelectedUserAccommodationFileData.is_uit) {
      this.populateTestCenterOptions();
    }
  };

  componentDidUpdate = prevProps => {
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
    // if file status gets updated
    if (
      prevProps.aaeSelectedUserAccommodationFileData.status_id !==
      this.props.aaeSelectedUserAccommodationFileData.status_id
    ) {
      // refreshing table
      this.refreshTestsToAdministerTable();
    }
  };

  populateTestCenterOptions = () => {
    // initializing needed variables
    const testCenterOptions = [];
    let testCenterSelectedOption = [];

    this.setState({ testCenterOptionsLoading: true }, () => {
      // getting test centers
      this.props.getTestCentersForUserAccommodationRequest().then(response => {
        // initializing loopingData
        let loopingData = response.body;
        // if OLA Test
        if (
          this.props.aaeSelectedUserAccommodationFileData.test_skill_sub_type_codename ===
            TestSkillSleDescCodename.ORAL_EN ||
          this.props.aaeSelectedUserAccommodationFileData.test_skill_sub_type_codename ===
            TestSkillSleDescCodename.ORAL_FR
        ) {
          // setting loopingData to only consider OLA Authorized test centers
          loopingData = response.body.filter(obj => obj.ola_authorized === true);
        }
        // looping in test centers
        for (let i = 0; i < loopingData.length; i++) {
          // if id of current iteration matches the test_center_id of the aaeSelectedUserAccommodationFileData redux state
          if (
            loopingData[i].id === this.props.aaeSelectedUserAccommodationFileData.test_center_id
          ) {
            testCenterSelectedOption = {
              label: `${
                loopingData[i].test_center_city_text[this.props.currentLanguage][0].text
              } (${
                loopingData[i].test_center_address_text[this.props.currentLanguage][0].text
              }, ${loopingData[i].test_center_city_text[this.props.currentLanguage][0].text}, ${
                loopingData[i].test_center_province_text[this.props.currentLanguage][0].text
              },
              ${loopingData[i].postal_code},
              ${
                this.props.currentLanguage === LANGUAGES.english
                  ? loopingData[i].country_eabrv
                  : loopingData[i].country_fabrv
              })`,
              value: loopingData[i].id
            };
          }
          // populating testCenterOptions
          testCenterOptions.push({
            label: `${
              loopingData[i].test_center_city_text[this.props.currentLanguage][0].text
            } (${loopingData[i].test_center_address_text[this.props.currentLanguage][0].text}, ${
              loopingData[i].test_center_city_text[this.props.currentLanguage][0].text
            }, ${loopingData[i].test_center_province_text[this.props.currentLanguage][0].text},
            ${loopingData[i].postal_code},
            ${
              this.props.currentLanguage === LANGUAGES.english
                ? loopingData[i].country_eabrv
                : loopingData[i].country_fabrv
            })`,
            value: loopingData[i].id
          });
        }
        // updating test center valid state (valid if option is selected)
        this.props.setAaeSelectedUserAccommodationFileTestCenterValidState(
          typeof testCenterSelectedOption.value !== "undefined"
        );
        // updating state
        this.setState({
          testCenterOptions: testCenterOptions,
          testCenterSelectedOption: testCenterSelectedOption,
          testCenterOptionsLoading: false
        });
      });
    });
  };

  getSelectedTestCenterLocation = selectedOption => {
    this.setState({ testCenterSelectedOption: selectedOption }, () => {
      // creating a copy of respective redux states
      const copyOfAaeSelectedUserAccommodationFileData = _.cloneDeep(
        this.props.aaeSelectedUserAccommodationFileData
      );
      // updating redux state
      copyOfAaeSelectedUserAccommodationFileData.test_center_id = selectedOption.value;
      this.props.setAaeSelectedUserAccommodationFileData(
        copyOfAaeSelectedUserAccommodationFileData
      );
      this.props.setAaeSelectedUserAccommodationFileTestCenterValidState(true);
    });
  };

  handleInitialTestToAdministerDataSetup = () => {
    // checking if a test outside of CAT has been set
    if (
      typeof this.props.aaeSelectedUserAccommodationFileData.test_to_administer
        .specified_test_description !== "undefined" &&
      this.props.aaeSelectedUserAccommodationFileData.test_to_administer
        .specified_test_description !== null
    ) {
      // updating needed states
      this.setState({
        testCannotBePerformedInCatSwitchState: true,
        specifiedTestDescription:
          this.props.aaeSelectedUserAccommodationFileData.test_to_administer
            .specified_test_description
      });
    } else {
      // populating table
      this.populateTestsToAdministerTable();
    }
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoadingSwitch: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoadingSwitch: false });
        }
      );
    });
  };

  handleDefaultTimerTimes = () => {
    // initializing defaultTimes
    const defaultTimes = [0]; // 0 ==> break bank default time
    // looping in default_test_sections_time
    for (
      let i = 0;
      i < this.props.aaeSelectedUserAccommodationFileData.default_test_sections_time.length;
      i++
    ) {
      // populating defaultTimes
      defaultTimes.push(
        this.props.aaeSelectedUserAccommodationFileData.default_test_sections_time[i].default_time
      );
    }
    // updating redux state
    this.props.setDefaultTimes(defaultTimes);
  };

  handleRadioButtonClick = event => {
    this.setState({ selectedTestToAdminister: event.target.id }, () => {
      // creating a copy of respective redux states
      const copyOfAaeSelectedUserAccommodationFileData = _.cloneDeep(
        this.props.aaeSelectedUserAccommodationFileData
      );
      // getting and setting the selected test to administer data
      const selectedTestToAdministerData =
        this.state.availableTestsToAdminister[
          this.state.selectedTestToAdminister.split(
            "user-accommodation-file-selected-test-to-administer-"
          )[1]
        ];
      const testToAdministerObj = {
        id: this.props.aaeInitialSelectedUserAccommodationFileData.test_to_administer.id,
        user_accommodation_file: this.props.aaeSelectedUserAccommodationFileData.id,
        test: selectedTestToAdministerData.id,
        test_code: selectedTestToAdministerData.test_code,
        test_version: selectedTestToAdministerData.version,
        test_name_en: selectedTestToAdministerData.name_en,
        test_name_fr: selectedTestToAdministerData.name_fr,
        specified_test_description: null
      };
      // getting default test sections time
      this.props.getDefaultTestSectionsTime(selectedTestToAdministerData.id).then(response => {
        // initializing needed variables
        const testSectionsTime = []; // AaeRedux redux related
        const defaultTestSectionsTime = []; // AaeRedux redux related
        // initializing timerState with only the break bank value
        const timerState = [this.props.currentTimer[0]]; // SetTimerRedux redux related
        const defaultTimes = [0]; // SetTimerRedux redux related
        // looping in response
        for (let i = 0; i < response.length; i++) {
          // updating needed arrays
          testSectionsTime.push({
            test_section_id: response[i].test_section_id,
            test_section_time: response[i].default_time,
            title_en: response[i].title_en,
            title_fr: response[i].title_fr
          });
          defaultTestSectionsTime.push(response[i]);
          timerState.push({
            hours: getTimeInHoursMinutes(response[i].default_time).formattedHours,
            minutes: getTimeInHoursMinutes(response[i].default_time).formattedMinutes
          });
          // populating defaultTimes
          defaultTimes.push(response[i].default_time);
        }
        // updating needed attributes
        copyOfAaeSelectedUserAccommodationFileData.test_to_administer = testToAdministerObj;
        copyOfAaeSelectedUserAccommodationFileData.test_sections_time = testSectionsTime;
        copyOfAaeSelectedUserAccommodationFileData.default_test_sections_time = response;
        // updating needed redux states
        // important: needs to be set first otherwise the redux state will not be properly set for the rest of the logic
        this.props.updateTimerState(timerState);
        this.props.setDefaultTimes(defaultTimes);
        this.props.setAaeSelectedUserAccommodationFileData(
          copyOfAaeSelectedUserAccommodationFileData
        );
        this.props.setAaeSelectedUserAccommodationFileTestToAdministerValidState(true);

        // refreshing table
        this.refreshTestsToAdministerTable();
      });
    });
  };

  populateTestsToAdministerTable = () => {
    // initializing needed variables
    let testToAdministerFromProps = null;
    let testToAdministerSelectedRadioOption = null;
    // test_to_administer is defined (not an empty object) + test_id is defined and not null
    if (
      Object.keys(this.props.aaeSelectedUserAccommodationFileData.test_to_administer).length > 0 &&
      typeof this.props.aaeSelectedUserAccommodationFileData.test_to_administer.test !==
        "undefined" &&
      this.props.aaeSelectedUserAccommodationFileData.test_to_administer.test !== null
    ) {
      // setting testToAdministerFromProps
      testToAdministerFromProps =
        this.props.aaeSelectedUserAccommodationFileData.test_to_administer;
    }
    // triggering loading
    this.setState({ currentlyLoading: true }, () => {
      // getting available tests to administer
      this.props
        .getAvailableTestsToAdministerForUserAccommodationFile(
          this.props.aaeSelectedUserAccommodationFileData.test_skill_type_id,
          this.props.aaeSelectedUserAccommodationFileData.test_skill_sub_type_id
        )
        .then(response => {
          // initializing needed object and array for rowsDefinition state (needed for GenericTable component)
          let rowsDefinition = {};
          const data = [];
          const availableTestsToAdminister = response;
          for (let i = 0; i < response.length; i++) {
            // if we need to initialize the selected test to administer and it matches the id of the current iteration
            if (
              testToAdministerFromProps !== null &&
              testToAdministerFromProps.test === response[i].id
            ) {
              // updating testToAdministerSelectedRadioOption
              testToAdministerSelectedRadioOption = `user-accommodation-file-selected-test-to-administer-${i}`;
            }
            // pushing needed data in data array (needed for GenericTable component)
            data.push({
              column_1: (
                <input
                  id={`user-accommodation-file-selected-test-to-administer-${i}`}
                  type="radio"
                  name={`user-accommodation-file-selected-test-to-administer-radio-btn-${i}`}
                  aria-label={response[i].test_code}
                  style={styles.radioButton}
                  onChange={e => this.handleRadioButtonClick(e)}
                  checked={
                    this.state.selectedTestToAdminister ===
                    `user-accommodation-file-selected-test-to-administer-${i}`
                  }
                  disabled={userAccommodationFileIsInEndState(
                    this.props.aaeSelectedUserAccommodationFileData.status_codename
                  )}
                />
              ),
              column_2: response[i].test_code,
              column_3: response[i].version,
              column_4: response[i][`name_${this.props.currentLanguage}`]
            });
          }

          // updating rowsDefinition object with provided data and needed style
          rowsDefinition = {
            column_1_style: COMMON_STYLE.CENTERED_TEXT,
            column_2_style: {},
            column_3_style: COMMON_STYLE.CENTERED_TEXT,
            column_4_style: {},
            data: data
          };

          // setting needed states
          this.setState(
            {
              rowsDefinition: rowsDefinition,
              availableTestsToAdminister: availableTestsToAdminister,
              currentlyLoading: false
            },
            () => {
              // if we need to set the selectedTestToAdminister state
              if (testToAdministerFromProps !== null) {
                this.setState(
                  { selectedTestToAdminister: testToAdministerSelectedRadioOption },
                  () => {
                    this.props.setAaeSelectedUserAccommodationFileTestToAdministerValidState(true);
                    // refreshing table
                    this.refreshTestsToAdministerTable();
                  }
                );
              }
            }
          );
        });
    });
  };

  refreshTestsToAdministerTable = () => {
    // initializing needed object and array for rowsDefinition state (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that the data attribute is defined/exists
    if (typeof this.state.rowsDefinition.data !== "undefined") {
      for (let i = 0; i < this.state.rowsDefinition.data.length; i++) {
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: (
            <input
              id={`user-accommodation-file-selected-test-to-administer-${i}`}
              type="radio"
              name={`user-accommodation-file-selected-test-to-administer-radio-btn-${i}`}
              aria-label={this.state.rowsDefinition.data[i].column_2}
              style={styles.radioButton}
              onChange={e => this.handleRadioButtonClick(e)}
              checked={
                this.state.selectedTestToAdminister ===
                `user-accommodation-file-selected-test-to-administer-${i}`
              }
              disabled={userAccommodationFileIsInEndState(
                this.props.aaeSelectedUserAccommodationFileData.status_codename
              )}
            />
          ),
          column_2: this.state.rowsDefinition.data[i].column_2,
          column_3: this.state.rowsDefinition.data[i].column_3,
          column_4: this.state.rowsDefinition.data[i].column_4
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.CENTERED_TEXT,
      column_2_style: {},
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: {},
      data: data
    };

    this.setState({
      rowsDefinition: rowsDefinition
    });
  };

  handleSwitchStateUpdate = event => {
    this.setState(
      {
        testCannotBePerformedInCatSwitchState: event
      },
      () => {
        // creating a copy of respective redux states
        const copyOfAaeSelectedUserAccommodationFileData = _.cloneDeep(
          this.props.aaeSelectedUserAccommodationFileData
        );

        // switch is checked
        if (this.state.testCannotBePerformedInCatSwitchState) {
          // setting timer redux states
          const timerState = [this.props.currentTimer[0]]; // SetTimerRedux redux related
          const defaultTimes = [0, 0]; // SetTimerRedux redux related
          timerState.push({ hours: "00", minutes: "00" });
          this.props.updateTimerState(timerState);
          this.props.setDefaultTimes(defaultTimes);
          // setting the right test to administer
          let testToAdminister = {};
          if (
            typeof this.props.aaeInitialSelectedUserAccommodationFileData.test_to_administer
              .specified_test_description !== "undefined" &&
            this.props.aaeInitialSelectedUserAccommodationFileData.test_to_administer
              .specified_test_description !== null
          ) {
            testToAdminister =
              this.props.aaeInitialSelectedUserAccommodationFileData.test_to_administer;
          }
          // updating needed attributes
          copyOfAaeSelectedUserAccommodationFileData.test_to_administer = testToAdminister;
          copyOfAaeSelectedUserAccommodationFileData.test_sections_time = [
            {
              test_section_id: null,
              test_section_time: 0
            }
          ];
          copyOfAaeSelectedUserAccommodationFileData.default_test_sections_time = [
            {
              test_section_id: null,
              default_time: 0
            }
          ];
          // updating redux states
          this.props.setAaeSelectedUserAccommodationFileData(
            copyOfAaeSelectedUserAccommodationFileData
          );
          // setting test to administer valid state (valid if specified_test_description is defined and not null)
          this.props.setAaeSelectedUserAccommodationFileTestToAdministerValidState(
            this.props.aaeInitialSelectedUserAccommodationFileData.test_to_administer
              .specified_test_description !== "" &&
              this.props.aaeInitialSelectedUserAccommodationFileData.test_to_administer
                .specified_test_description !== null
          );
          // updating needed states
          this.setState({
            selectedTestToAdminister: null,
            // setting to specified_test_description is not null else ""
            specifiedTestDescription:
              this.props.aaeInitialSelectedUserAccommodationFileData.test_to_administer
                .specified_test_description !== null
                ? this.props.aaeInitialSelectedUserAccommodationFileData.test_to_administer
                    .specified_test_description
                : "",
            isValidSpecifiedTestDescription: true
          });
          // switch is unchecked
        } else {
          // getting default test sections time
          this.props
            .getDefaultTestSectionsTime(
              this.props.aaeInitialSelectedUserAccommodationFileData.test_to_administer.test
            )
            .then(response => {
              // initializing needed variables
              const testSectionsTime = []; // AaeRedux redux related
              const defaultTestSectionsTime = []; // AaeRedux redux related
              // initializing timerState with only the break bank value
              const timerState = [this.props.currentTimer[0]]; // SetTimerRedux redux related
              const defaultTimes = [0]; // SetTimerRedux redux related
              // looping in response
              for (let i = 0; i < response.length; i++) {
                // updating needed arrays
                testSectionsTime.push({
                  test_section_id: response[i].test_section_id,
                  test_section_time: response[i].default_time,
                  title_en: response[i].title_en,
                  title_fr: response[i].title_fr
                });
                defaultTestSectionsTime.push(response[i]);
                timerState.push({
                  hours: getTimeInHoursMinutes(response[i].default_time).formattedHours,
                  minutes: getTimeInHoursMinutes(response[i].default_time).formattedMinutes
                });
                // populating defaultTimes
                defaultTimes.push(response[i].default_time);
              }
              // setting the right test to administer
              let testToAdminister = {};
              if (
                typeof this.props.aaeInitialSelectedUserAccommodationFileData.test_to_administer
                  .test !== "undefined" &&
                this.props.aaeInitialSelectedUserAccommodationFileData.test_to_administer.test !==
                  null
              ) {
                testToAdminister =
                  this.props.aaeInitialSelectedUserAccommodationFileData.test_to_administer;
              }
              // updating needed attributes
              copyOfAaeSelectedUserAccommodationFileData.test_to_administer = testToAdminister;
              copyOfAaeSelectedUserAccommodationFileData.test_sections_time = testSectionsTime;
              copyOfAaeSelectedUserAccommodationFileData.default_test_sections_time = response;
              // updating needed redux states
              // important: needs to be set first otherwise the redux state will not be properly set for the rest of the logic
              this.props.updateTimerState(timerState);
              this.props.setDefaultTimes(defaultTimes);
              // updating redux states
              this.props.setAaeSelectedUserAccommodationFileData(
                copyOfAaeSelectedUserAccommodationFileData
              );
              // setting test to administer valid state to false
              this.props.setAaeSelectedUserAccommodationFileTestToAdministerValidState(false);
              // resetting needed states + populating table
              this.setState(
                {
                  selectedTestToAdminister: null,
                  specifiedTestDescription: ""
                },
                () => {
                  // adding small delay to make sure that the redux states are properly set
                  setTimeout(() => {
                    // populating table
                    this.populateTestsToAdministerTable();
                  }, 100);
                }
              );
            });
        }
      }
    );
  };

  handleSpecifiedTestDescriptionContentUpdate = event => {
    this.setState(
      {
        specifiedTestDescription: event.target.value,
        isValidSpecifiedTestDescription: event.target.value !== ""
      },
      () => {
        // creating a copy of respective redux states
        const copyOfAaeSelectedUserAccommodationFileData = _.cloneDeep(
          this.props.aaeSelectedUserAccommodationFileData
        );
        // getting and setting the selected test to administer data
        const selectedTestToAdministerData = {
          id: this.props.aaeSelectedUserAccommodationFileData.test_to_administer.id,
          user_accommodation_file: this.props.aaeSelectedUserAccommodationFileData.id,
          test: null,
          test_code: null,
          test_version: null,
          test_name_en: null,
          test_name_fr: null,
          specified_test_description: this.state.specifiedTestDescription
        };
        // updating needed attributes
        copyOfAaeSelectedUserAccommodationFileData.test_to_administer =
          selectedTestToAdministerData;
        // updating redux states
        this.props.setAaeSelectedUserAccommodationFileData(
          copyOfAaeSelectedUserAccommodationFileData
        );
        this.props.setAaeSelectedUserAccommodationFileTestToAdministerValidState(
          this.state.isValidSpecifiedTestDescription
        );
      }
    );
  };

  render() {
    const columnsDefinition = [
      {
        label: "",
        style: { ...COMMON_STYLE.CENTERED_TEXT, ...{ width: "7%" } }
      },
      {
        label:
          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
            .testToAdminister.table.columnOne,
        style: { ...COMMON_STYLE.LEFT_TEXT, ...{ width: "25%" } }
      },
      {
        label:
          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
            .testToAdminister.table.columnTwo,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
            .testToAdminister.table.columnThree,
        style: COMMON_STYLE.LEFT_TEXT
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div>
        <h2 style={styles.noMargin}>
          {LOCALIZE.formatString(
            LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
              .testToAdminister.title,
            // switch checked (specified test description input)
            this.state.testCannotBePerformedInCatSwitchState
              ? // no content
                `${
                  this.state.specifiedTestDescription === ""
                    ? ""
                    : // content provided
                      `[${this.state.specifiedTestDescription}]`
                }`
              : // switch not checked (table with radio buttons)
                // no selected test to administer
                this.state.selectedTestToAdminister === null
                ? ""
                : // selected test to administer
                  `[${
                    this.state.rowsDefinition.data[
                      this.state.selectedTestToAdminister.split(
                        "user-accommodation-file-selected-test-to-administer-"
                      )[1]
                    ].column_2
                  } - v${
                    this.state.rowsDefinition.data[
                      this.state.selectedTestToAdminister.split(
                        "user-accommodation-file-selected-test-to-administer-"
                      )[1]
                    ].column_3
                  }]`
          )}
        </h2>
        <div style={styles.formContainer}>
          {/* only show test center dropdown if supervised request */}
          {!this.props.aaeSelectedUserAccommodationFileData.is_uit && (
            <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
              >
                <label id="user-accommodation-file-location-label" style={styles.inputTitles}>
                  {
                    LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                      .sideNavigation.testToAdminister.testCenterLabel
                  }
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
              >
                {this.state.testCenterOptionsLoading ? (
                  <div style={styles.loadingContainer}>
                    <label className="fa fa-spinner fa-spin">
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  </div>
                ) : (
                  <DropdownSelect
                    idPrefix="user-accommodation-file-location"
                    ariaLabelledBy="user-accommodation-file-location-label"
                    ariaRequired={true}
                    options={this.state.testCenterOptions}
                    onChange={this.getSelectedTestCenterLocation}
                    defaultValue={this.state.testCenterSelectedOption}
                    hasPlaceholder={true}
                    isDisabled={userAccommodationFileIsInEndState(
                      this.props.aaeSelectedUserAccommodationFileData.status_codename
                    )}
                  />
                )}
              </Col>
            </Row>
          )}
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label htmlFor="user-accommodation-file-test-skill-type" style={styles.inputTitles}>
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.testToAdminister.testSkillTypeLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-test-skill-type"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={
                  this.props.aaeSelectedUserAccommodationFileData[
                    `test_skill_type_name_${this.props.currentLanguage}`
                  ]
                }
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                htmlFor="user-accommodation-file-test-skill-sub-type"
                style={styles.inputTitles}
              >
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.testToAdminister.testSkillSubTypeLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="user-accommodation-file-test-skill-sub-type"
                tabIndex={0}
                className={"valid-field input-disabled-look"}
                type="text"
                value={
                  this.props.aaeSelectedUserAccommodationFileData[
                    `test_skill_sub_type_name_${this.props.currentLanguage}`
                  ]
                }
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                aria-disabled={true}
              />
            </Col>
          </Row>
          <Row className="align-items-center" role="presentation" style={styles.rowStyle}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                htmlFor={"user-accommodation-file-test-cannot-be-performed-in-cat-switch"}
                style={styles.switchLabel}
              >
                {
                  LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                    .sideNavigation.testToAdminister.testCannotBePerformedInCatSwitchDescription
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              {!this.state.isLoadingSwitch && (
                <Switch
                  id={"user-accommodation-file-test-cannot-be-performed-in-cat-switch"}
                  onChange={e => {
                    this.handleSwitchStateUpdate(e);
                  }}
                  checked={this.state.testCannotBePerformedInCatSwitchState}
                  height={this.state.switchHeight}
                  width={this.state.switchWidth}
                  disabled={userAccommodationFileIsInEndState(
                    this.props.aaeSelectedUserAccommodationFileData.status_codename
                  )}
                />
              )}
            </Col>
          </Row>
          {this.state.testCannotBePerformedInCatSwitchState && (
            <>
              <Row
                className="align-items-center"
                role="presentation"
                style={
                  this.state.isValidSpecifiedTestDescription
                    ? styles.rowStyle
                    : styles.rowStyleWithError
                }
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label
                    htmlFor="user-accommodation-file-specified-test-description"
                    style={styles.inputTitles}
                  >
                    {
                      LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                        .sideNavigation.testToAdminister.specifiedTestDescriptionLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <input
                    id="user-accommodation-file-specified-test-description"
                    tabIndex={0}
                    className={
                      this.state.isValidSpecifiedTestDescription ? "valid-field" : "invalid-field"
                    }
                    type="text"
                    onChange={this.handleSpecifiedTestDescriptionContentUpdate}
                    value={this.state.specifiedTestDescription}
                    style={{
                      ...styles.inputs,
                      ...accommodationsStyle
                    }}
                  />
                </Col>
              </Row>
              <Row className="align-items-center" role="presentation">
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                ></Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  {!this.state.isValidSpecifiedTestDescription && (
                    <label
                      htmlFor="user-accommodation-file-specified-test-description"
                      style={styles.errorMessage}
                    >
                      {
                        LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                          .sideNavigation.testToAdminister.specifiedTestDescriptionError
                      }
                    </label>
                  )}
                </Col>
              </Row>
            </>
          )}
        </div>
        {!this.state.testCannotBePerformedInCatSwitchState && (
          <div style={styles.tableMainContainer}>
            <GenericTable
              classnamePrefix="user-accommodation-file-test-to-administer"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main
                  .sideNavigation.testToAdminister.table.noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
        )}
      </div>
    );
  }
}

export { SelectedUserAccommodationFileMainTabTestToAdminister as unconnectedSelectedUserAccommodationFileMainTabTestToAdminister };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    aaeInitialSelectedUserAccommodationFileData:
      state.aae.aaeInitialSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileData: state.aae.aaeSelectedUserAccommodationFileData,
    currentTimer: state.timer.currentTimer
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAvailableTestsToAdministerForUserAccommodationFile,
      setAaeSelectedUserAccommodationFileData,
      setAaeSelectedUserAccommodationFileTestToAdministerValidState,
      setAaeSelectedUserAccommodationFileTestCenterValidState,
      getDefaultTestSectionsTime,
      updateTimerState,
      setDefaultTimes,
      getTestCentersForUserAccommodationRequest
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectedUserAccommodationFileMainTabTestToAdminister);
