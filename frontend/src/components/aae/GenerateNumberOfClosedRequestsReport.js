import getFormattedTestSkill from "../../helpers/getFormattedTestSkill";
import LOCALIZE from "../../text_resources";

function GenerateNumberOfClosedRequestsReport(providedReportDataArray, currentLanguage) {
  // initializing reportData
  const reportData = [];

  // pushing columns definition
  reportData.push([
    LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfClosedRequests.requestId,
    LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfClosedRequests.receivedDate,
    LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfClosedRequests.sleSkill,
    LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfClosedRequests
      .firstCompletedByAaeDate,
    LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfClosedRequests
      .firstApprovedByCandidateDate,
    LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfClosedRequests.status
  ]);

  // looping in provided providedReportDataArray and formatting data
  for (let i = 0; i < providedReportDataArray.length; i++) {
    reportData.push([
      providedReportDataArray[i].id,
      providedReportDataArray[i].created_date,
      getFormattedTestSkill(providedReportDataArray[i], currentLanguage),
      providedReportDataArray[i].first_completed_by_aae_date !== null
        ? providedReportDataArray[i].first_completed_by_aae_date.split("T")[0]
        : "",
      providedReportDataArray[i].first_approved_by_candidate_date !== null
        ? providedReportDataArray[i].first_approved_by_candidate_date.split("T")[0]
        : "",
      LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfClosedRequests.statusValue
    ]);
  }

  // adding total number of requests row
  reportData.push(["", "", "", "", "", ""]);
  reportData.push([
    "",
    "",
    "",
    "",
    LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfClosedRequests
      .totalNumberOfRequests,
    providedReportDataArray.length
  ]);

  return reportData;
}

export default GenerateNumberOfClosedRequestsReport;
