import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { history } from "../../store-index";
import LOCALIZE from "../../text_resources";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import Pagination from "../commons/Pagination";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import SearchBarWithDisplayOptions from "../commons/SearchBarWithDisplayOptions";
import {
  updateCurrentAaeOpenRequestsPageState,
  updateAaeOpenRequestsPageSizeState,
  updateSearchAaeOpenRequestsStates,
  getAllUserAccommodationFiles,
  getFoundUserAccommodationFiles,
  getSelectedUserAccommodationFileData,
  setAaeInitialSelectedUserAccommodationFileData,
  setAaeSelectedUserAccommodationFileData,
  setAaeSelectedUserAccommodationFileTestToAdministerValidState
} from "../../modules/AaeRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars } from "@fortawesome/free-solid-svg-icons";
import StyledTooltip, { TYPE, EFFECT } from "../authentication/StyledTooltip";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { PATH } from "../commons/Constants";
import { updateTimerState } from "../../modules/SetTimerRedux";
import { getTimeInHoursMinutes } from "../../helpers/timeConversion";

const styles = {
  allUnset: {
    all: "unset"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  }
};

class OpenRequests extends Component {
  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    resultsFound: 0,
    openRequests: [],
    clearSearchTriggered: false
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentAaeOpenRequestsPageState(1);
    // populating associated test administrators
    this.populateOpenRequestsBasedOnActiveSearch();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateOpenRequestsBasedOnActiveSearch();
    }
    // if aaeOpenRequestsPaginationPage gets updated
    if (prevProps.aaeOpenRequestsPaginationPage !== this.props.aaeOpenRequestsPaginationPage) {
      this.populateOpenRequestsBasedOnActiveSearch();
    }
    // if aaeOpenRequestsPaginationPageSize get updated
    if (
      prevProps.aaeOpenRequestsPaginationPageSize !== this.props.aaeOpenRequestsPaginationPageSize
    ) {
      this.populateOpenRequestsBasedOnActiveSearch();
    }
    // if search aaeOpenRequestsKeyword gets updated
    if (prevProps.aaeOpenRequestsKeyword !== this.props.aaeOpenRequestsKeyword) {
      // if aaeOpenRequestsKeyword redux state is empty
      if (this.props.aaeOpenRequestsKeyword !== "") {
        this.populateFoundOpenRequests();
      } else if (
        this.props.aaeOpenRequestsKeyword === "" &&
        this.props.aaeOpenRequestsActiveSearch
      ) {
        this.populateFoundOpenRequests();
      }
    }
    // if aaeOpenRequestsActiveSearch gets updated
    if (prevProps.aaeOpenRequestsActiveSearch !== this.props.aaeOpenRequestsActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.aaeOpenRequestsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateOpenRequests();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundOpenRequests();
      }
    }
  };

  populateOpenRequestsBasedOnActiveSearch = () => {
    // current search
    if (this.props.aaeOpenRequestsActiveSearch) {
      this.populateFoundOpenRequests();
      // no search
    } else {
      this.populateOpenRequests();
    }
  };

  // populating open accommodation requests
  populateOpenRequests = () => {
    this.setState({ currentlyLoading: true }, () => {
      const openRequestsArray = [];
      this.props
        .getAllUserAccommodationFiles(
          this.props.aaeOpenRequestsPaginationPage,
          this.props.aaeOpenRequestsPaginationPageSize,
          true
        )
        .then(response => {
          this.populateAccommodationRequestsOpenRequestsObject(openRequestsArray, response);
        })
        .then(() => {
          if (this.state.clearSearchTriggered) {
            // go back to the first page to avoid display bugs
            this.props.updateCurrentAaeOpenRequestsPageState(1);
            this.setState({ clearSearchTriggered: false });
          }
        })
        .then(() => {
          this.setState({ currentlyLoading: false });
        });
    });
  };

  // populating found open accommodation requests
  populateFoundOpenRequests = () => {
    // making sure that the table is not currently loading
    if (!this.state.currentlyLoading) {
      this.setState(
        { currentlyLoading: true },
        () => {
          const openRequestsArray = [];
          setTimeout(() => {
            this.props
              .getFoundUserAccommodationFiles(
                this.props.aaeOpenRequestsKeyword,
                this.props.currentLanguage,
                this.props.aaeOpenRequestsPaginationPage,
                this.props.aaeOpenRequestsPaginationPageSize,
                true
              )
              .then(response => {
                // there are no results found
                if (response[0] === "no results found") {
                  this.setState({
                    testSesions: [],
                    rowsDefinition: {},
                    numberOfPages: 1,
                    nextPageNumber: 0,
                    previousPageNumber: 0,
                    resultsFound: 0
                  });
                  // there is at least one result found
                } else {
                  this.populateAccommodationRequestsOpenRequestsObject(openRequestsArray, response);
                  this.setState({ resultsFound: response.count });
                }
              })
              .then(() => {
                this.setState(
                  {
                    currentlyLoading: false
                  },
                  () => {
                    // make sure that this element exsits to avoid any error
                    if (
                      document.getElementById("accommodation-requests-open-requests-results-found")
                    ) {
                      document
                        .getElementById("accommodation-requests-open-requests-results-found")
                        .focus();
                    }
                  }
                );
              });
          });
        },
        250
      );
    }
  };

  populateAccommodationRequestsOpenRequestsObject = (openRequestsArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];

        // Add the item to open requests array
        openRequestsArray.push(currentResult);

        // getting formatted test skill / sub-skill
        let formattedTestSkill = `${
          currentResult[`test_skill_type_name_${this.props.currentLanguage}`]
        }`;
        if (currentResult.test_skill_sub_type_id !== null) {
          formattedTestSkill = formattedTestSkill.concat(
            ` - ${currentResult[`test_skill_sub_type_name_${this.props.currentLanguage}`]}`
          );
        }

        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult.id,
          column_2: currentResult.user_id,
          column_3: `${currentResult.user_last_name}, ${currentResult.user_first_name}`,
          column_4: `${
            currentResult.is_uit
              ? LOCALIZE.commons.testType.uit
              : LOCALIZE.commons.testType.supervised
          }`,
          column_5: `${currentResult.created_date}`,
          column_6: `${currentResult.process_end_date}`,
          column_7: currentResult[`status_name_${this.props.currentLanguage}`],
          column_8:
            currentResult.assigned_to_user_id !== null
              ? `${currentResult.assigned_to_user_last_name}, ${currentResult.assigned_to_user_first_name}`
              : LOCALIZE.commons.na,
          column_9: formattedTestSkill,
          column_10: (
            <>
              <StyledTooltip
                id={`user-accommodation-file-open-requests-view-row-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`user-accommodation-file-open-requests-view-row-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faBinoculars} />
                        </>
                      }
                      action={() => this.viewSelectedUserAccommodationFile(currentResult)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.aae.sideNav.accommodationRequests.topTabs.openRequests.table
                          .viewButtonAccessibility,
                        currentResult.user_first_name,
                        currentResult.user_last_name
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.aae.sideNav.accommodationRequests.topTabs.openRequests.table
                          .viewButtonTooltip
                      }
                    </p>
                  </div>
                }
              />
            </>
          )
        });
      }
    }
    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.CENTERED_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      column_6_style: COMMON_STYLE.CENTERED_TEXT,
      column_7_style: COMMON_STYLE.CENTERED_TEXT,
      column_8_style: COMMON_STYLE.CENTERED_TEXT,
      column_9_style: COMMON_STYLE.CENTERED_TEXT,
      column_10_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };
    // saving results in state
    this.setState({
      openRequests: openRequestsArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(response.count / this.props.aaeOpenRequestsPaginationPageSize),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  viewSelectedUserAccommodationFile = userAccommodationFileData => {
    // getting selected user accommodation file data
    this.props
      .getSelectedUserAccommodationFileData(userAccommodationFileData.id)
      .then(response => {
        // initializing timerState with only the break bank value
        const timerState = [];

        // adding break bank value to timerState array
        timerState.push({
          hours: getTimeInHoursMinutes(response.break_bank).formattedHours,
          minutes: getTimeInHoursMinutes(response.break_bank).formattedMinutes
        });

        // adding test sections time values to timerState array
        for (let i = 0; i < response.test_sections_time.length; i++) {
          timerState.push({
            hours: getTimeInHoursMinutes(response.test_sections_time[i].test_section_time)
              .formattedHours,
            minutes: getTimeInHoursMinutes(response.test_sections_time[i].test_section_time)
              .formattedMinutes
          });
        }

        // setting redux states
        this.props.setAaeInitialSelectedUserAccommodationFileData(response);
        this.props.setAaeSelectedUserAccommodationFileData(response);
        this.props.setAaeSelectedUserAccommodationFileTestToAdministerValidState(true);
        this.props.updateTimerState(timerState);
      })
      .then(() => {
        // redirecting user to selected user accommodation file page
        history.push(PATH.aaeSelectedUserAccommodationFile);
      });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.openRequests.table.columnOne,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.openRequests.table.columnTwo,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.openRequests.table.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.openRequests.table.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.openRequests.table.columnFive,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.openRequests.table.columnSix,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.openRequests.table.columnSeven,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.openRequests.table.columnEight,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.openRequests.table.columnNine,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.aae.sideNav.accommodationRequests.topTabs.openRequests.table.columnTen,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"accommodation-requests-open-requests"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchAaeOpenRequestsStates}
            updatePageState={this.props.updateCurrentAaeOpenRequestsPageState}
            paginationPageSize={Number(this.props.aaeOpenRequestsPaginationPageSize)}
            updatePaginationPageSize={this.props.updateAaeOpenRequestsPageSizeState}
            data={this.state.pendingRequests}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.aaeOpenRequestsKeyword}
          />
          <div>
            <GenericTable
              classnamePrefix="accommodation-requests-open-requests"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.aae.sideNav.accommodationRequests.topTabs.openRequests.table.noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <Pagination
            paginationContainerId={"accommodation-requests-open-requests-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.aaeOpenRequestsPaginationPage}
            updatePaginationPageState={this.props.updateCurrentAaeOpenRequestsPageState}
            firstTableRowId={"accommodation-requests-open-requests-table-row-0"}
          />
        </div>
      </div>
    );
  }
}

export { OpenRequests as unconnectedOpenRequests };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    aaeOpenRequestsPaginationPage: state.aae.aaeOpenRequestsPaginationPage,
    aaeOpenRequestsPaginationPageSize: state.aae.aaeOpenRequestsPaginationPageSize,
    aaeOpenRequestsKeyword: state.aae.aaeOpenRequestsKeyword,
    aaeOpenRequestsActiveSearch: state.aae.aaeOpenRequestsActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateCurrentAaeOpenRequestsPageState,
      updateAaeOpenRequestsPageSizeState,
      updateSearchAaeOpenRequestsStates,
      getAllUserAccommodationFiles,
      getFoundUserAccommodationFiles,
      getSelectedUserAccommodationFileData,
      setAaeInitialSelectedUserAccommodationFileData,
      setAaeSelectedUserAccommodationFileData,
      setAaeSelectedUserAccommodationFileTestToAdministerValidState,
      updateTimerState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(OpenRequests);
