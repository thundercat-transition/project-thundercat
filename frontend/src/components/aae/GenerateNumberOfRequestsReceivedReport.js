import LOCALIZE from "../../text_resources";

function generateNumberOfRequestsReceivedReport(providedReportDataArray, currentLanguage) {
  // initializing reportData
  const reportData = [];

  // initializing totalNumberOfRequests
  let totalNumberOfRequests = 0;

  // pushing columns definition
  reportData.push([
    LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfRequestsReceived.year,
    LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfRequestsReceived.quarter,
    LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfRequestsReceived.month,
    LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfRequestsReceived.requestsReceived
  ]);

  // looping in provided providedReportDataArray and formatting data
  for (let i = 0; i < providedReportDataArray.length; i++) {
    reportData.push([
      providedReportDataArray[i].year,
      providedReportDataArray[i].quarter,
      providedReportDataArray[i].month_data[currentLanguage],
      providedReportDataArray[i].number_of_requests
    ]);
    // incrementing totalNumberOfRequests
    totalNumberOfRequests += providedReportDataArray[i].number_of_requests;
  }

  // adding total number of requests row
  reportData.push(["", "", "", ""]);
  reportData.push([
    "",
    "",
    LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfRequestsReceived
      .totalNumberOfRequests,
    totalNumberOfRequests
  ]);

  return reportData;
}

export default generateNumberOfRequestsReceivedReport;
