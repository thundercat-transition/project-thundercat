import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ContentContainer from "../commons/ContentContainer";
import { Helmet } from "react-helmet";
import SideNavigation from "../eMIB/SideNavigation";
import AccommodationRequests from "./AccommodationRequests";
import AccommodationReports from "./AccommodationReports";
import { setAaeSelectedSideNav } from "../../modules/AaeRedux";

export const styles = {
  header: {
    marginBottom: 24
  },
  sectionContainerLabelDiv: {
    paddingLeft: 18,
    marginBottom: "-3px"
  },
  sectionContainerLabel: {
    padding: "4px 24px",
    marginBottom: 0,
    position: "relative"
  },
  tabStyleBorder: {
    borderBottom: "5px solid #00565e",
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    borderRadius: "4px 4px 0 0"
  },
  sectionContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    minHeight: 400,
    padding: "12px 0",
    marginBottom: 24
  },
  tabContainer: {
    zIndex: 1,
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent"
  },
  nav: {
    marginTop: 10,
    marginLeft: 10,
    width: 206,
    textAlign: "center"
  },
  body: {
    paddingLeft: 12
  },
  sideNavBodyContent: {
    marginTop: "-12px"
  },
  appPadding: {
    padding: "15px"
  }
};

class AaeDashboard extends Component {
  state = {};

  componentDidMount = () => {};

  componentWillUnmount = () => {};

  // Returns array where each item indicates specifications related to How To Page including the title and the body
  getAaeSections = () => {
    return [
      {
        menuString: LOCALIZE.aae.sideNav.accommodationRequests.title,
        body: <AccommodationRequests />
      },
      {
        menuString: LOCALIZE.aae.sideNav.accommodationReports.title,
        body: <AccommodationReports />
      }
    ];
  };

  render() {
    const specs = this.getAaeSections();
    return (
      <div className="app" style={styles.appPadding}>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title className="notranslate">{LOCALIZE.titles.aae}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <div
              id="user-welcome-message-div"
              style={styles.header}
              aria-labelledby="user-welcome-message"
              className="notranslate"
            >
              <h1 id="user-welcome-message" className="green-divider">
                {LOCALIZE.formatString(
                  LOCALIZE.profile.title,
                  this.props.firstName,
                  this.props.lastName
                )}
              </h1>
            </div>
            <div>
              <div style={styles.sectionContainerLabelDiv}>
                <div>
                  <label style={styles.sectionContainerLabel} className="notranslate">
                    {LOCALIZE.aae.title}
                    <span style={styles.tabStyleBorder}></span>
                  </label>
                </div>
              </div>
              <div style={styles.sectionContainer}>
                <section aria-label={LOCALIZE.ariaLabel.sideNavigationSection}>
                  <SideNavigation
                    specs={specs}
                    startIndex={this.props.aaeSelectedSideNav}
                    displayNextPreviousButton={false}
                    isMain={true}
                    tabContainerStyle={styles.tabContainer}
                    tabContentStyle={styles.tabContent}
                    navStyle={styles.nav}
                    bodyContentCustomStyle={styles.sideNavBodyContent}
                    updateSelectedSideNavItem={this.props.setAaeSelectedSideNav}
                  />
                </section>
              </div>
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { AaeDashboard as unconnectedAaeDashboard };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    aaeSelectedSideNav: state.aae.aaeSelectedSideNav
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setAaeSelectedSideNav
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AaeDashboard);
