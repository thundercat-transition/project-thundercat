/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import ContentContainer, { maxWidthUtils } from "../commons/ContentContainer";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { Col, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSpinner,
  faArrowAltCircleLeft,
  faTimes,
  faCheck,
  faBan,
  faPaperPlane,
  faFolderOpen
} from "@fortawesome/free-solid-svg-icons";
import { history } from "../../store-index";
import { PATH } from "../commons/Constants";

import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import TopTabs from "../commons/TopTabs";
import * as _ from "lodash";
import { triggerAppRerender } from "../../modules/AppRedux";
import {
  resetAaeSelectedUserAccommodationFileDataState,
  saveSelectedUserAccommodationFileData,
  setAaeInitialSelectedUserAccommodationFileData,
  setAaeSelectedUserAccommodationFileData,
  setAaeSelectedUserAccommodationFileFirstColumnTab,
  setAaeSelectedUserAccommodationFileSecondColumnTab,
  setAaeSelectedUserAccommodationFileTestToAdministerValidState,
  triggerAaeRerender,
  triggerSelectedUserAccommodationFileDataAction
} from "../../modules/AaeRedux";
import SelectedUserAccommodationFileMainTab from "./SelectedUserAccommodationFileMainTab";
import SelectedUserAccommodationFileCommentsTab from "./SelectedUserAccommodationFileCommentsTab";
import SelectedUserAccommodationFileFooter from "./SelectedUserAccommodationFileFooter";
import USER_ACCOMMODATION_FILE_STATUS_CONST from "./Constants";
import SelectedUserAccommodationFileRationaleTab from "./SelectedUserAccommodationFileRationaleTab";
import SelectedUserAccommodationFileStatsTab from "./SelectedUserAccommodationFileStatsTab";

export const AAE_SELECTED_USER_ACCOMMODATION_FILE_SECTIONS = {
  main: "main",
  other: "other"
};

const styles = {
  loadingContainer: {
    margin: 24,
    textAlign: "center"
  },
  loading: {
    fontSize: "32px",
    padding: 6
  },
  backButtonStyle: {
    marginBottom: 24
  },
  buttonLabel: {
    marginLeft: 6
  },
  appPadding: {
    padding: "15px 15px 0 15px",
    marginBottom: "-100px"
  },
  colOneStyle: {
    padding: 0,
    maxWidth: "70%"
  },
  colTwoStyle: {
    padding: 0,
    maxWidth: "30%"
  },
  generalTab: {
    paddingRight: 3
  },
  otherTab: {
    paddingLeft: 3
  },
  icon: {
    paddingRight: 6,
    verticalAlign: "middle"
  },
  transparentText: {
    color: "transparent"
  },
  customLoadingContainer: {
    display: "grid",
    gridTemplateColumns: "1fr"
  },
  loadingOverlappingStyle: {
    gridRowStart: "1",
    gridColumnStart: "1"
  }
};

// function to check if the user accommodation file is in an end state
export function userAccommodationFileIsInEndState(userAccommodationFileStatusCodename) {
  // initializing fileIsInEndState
  let fileIsInEndState = false;
  if (
    userAccommodationFileStatusCodename === USER_ACCOMMODATION_FILE_STATUS_CONST.pendingApproval ||
    userAccommodationFileStatusCodename === USER_ACCOMMODATION_FILE_STATUS_CONST.completed ||
    userAccommodationFileStatusCodename === USER_ACCOMMODATION_FILE_STATUS_CONST.administered ||
    userAccommodationFileStatusCodename === USER_ACCOMMODATION_FILE_STATUS_CONST.cancelled ||
    userAccommodationFileStatusCodename ===
      USER_ACCOMMODATION_FILE_STATUS_CONST.cancelledByCandidate
  ) {
    fileIsInEndState = true;
  }
  return fileIsInEndState;
}

class SelectedUserAccommodationFileTab extends Component {
  state = {
    isLoading: true,
    showBackToUserAccommodationFileSelectionPopup: false,
    triggerRerender: false,
    showRejectActionPopup: false,
    showCompleteActionPopup: false,
    showReOpenActionPopup: false,
    showSavedButCancelledByCandidatePopup: false,
    showActionDeniedPopup: false,
    actionInProgress: false
  };

  constructor(props) {
    super(props);
    this.topTabsHeightRef = React.createRef();
  }

  componentDidMount = () => {
    // making sure that the aaeSelectedUserAccommodationFileData is defined (otherwise, redirect the user to the user accommodation file selection page)
    // can happen if the URL is manually entered or if the back browser button has been pressed
    if (
      typeof this.props.aaeSelectedUserAccommodationFileData.id === "undefined" ||
      this.props.aaeSelectedUserAccommodationFileData.id === null
    ) {
      setTimeout(() => {
        if (
          typeof this.props.aaeSelectedUserAccommodationFileData.id === "undefined" ||
          this.props.item_bank_definition.id === null
        ) {
          history.push(PATH.aae);
        } else {
          this.setState({ isLoading: false });
        }
      }, 3000);
    } else {
      this.setState({ isLoading: false });
    }
    // triggering app rerender to make sure that the special page height and overflow is properly set
    this.props.triggerAppRerender();
  };

  componentDidUpdate = prevProps => {
    // if accommodations are updated
    if (
      prevProps.accommodations.fontSize !== this.props.accommodations.fontSize ||
      prevProps.accommodations.spacing !== this.props.accommodations.spacing ||
      prevProps.accommodations.fontFamily !== this.props.accommodations.fontFamily
    ) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }
  };

  openBackToUserAccommodationFileSelectionPopup = () => {
    this.setState({ showBackToUserAccommodationFileSelectionPopup: true });
  };

  closeBackToUserAccommodationFileSelectionPopup = () => {
    this.setState({ showBackToUserAccommodationFileSelectionPopup: false });
  };

  handleBackToUserAccommodationFileSelection = () => {
    // resetting respective aae redux states
    this.props.resetAaeSelectedUserAccommodationFileDataState();
    history.push(PATH.aae);
  };

  // will return true if changes are detected in redux states
  changesDetected = () => {
    if (
      this.props.aaeSelectedUserAccommodationFileTestToAdministerValidState &&
      this.props.aaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState &&
      _.isEqual(
        this.props.aaeInitialSelectedUserAccommodationFileData,
        this.props.aaeSelectedUserAccommodationFileData
      )
    ) {
      this.handleBackToUserAccommodationFileSelection();
    }
    this.openBackToUserAccommodationFileSelectionPopup();
  };

  getTABS = section => {
    switch (section) {
      case AAE_SELECTED_USER_ACCOMMODATION_FILE_SECTIONS.main:
        return [
          {
            key: `${AAE_SELECTED_USER_ACCOMMODATION_FILE_SECTIONS.main}-1`,
            tabName: LOCALIZE.formatString(
              LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.title,
              this.props.aaeSelectedUserAccommodationFileData.user_first_name,
              this.props.aaeSelectedUserAccommodationFileData.user_last_name
            ),
            body: <SelectedUserAccommodationFileMainTab />
          }
        ];
      case AAE_SELECTED_USER_ACCOMMODATION_FILE_SECTIONS.other:
        return [
          {
            key: `${AAE_SELECTED_USER_ACCOMMODATION_FILE_SECTIONS.other}-1`,
            tabName:
              LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.other.comments
                .title,
            body: <SelectedUserAccommodationFileCommentsTab />
          },
          {
            key: `${AAE_SELECTED_USER_ACCOMMODATION_FILE_SECTIONS.other}-2`,
            tabName:
              LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.other.rationale
                .title,
            body: <SelectedUserAccommodationFileRationaleTab />
          },
          {
            key: `${AAE_SELECTED_USER_ACCOMMODATION_FILE_SECTIONS.other}-3`,
            tabName:
              LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.other.stats.title,
            body: <SelectedUserAccommodationFileStatsTab />
          }
        ];
      default:
        return null;
    }
  };

  getRejectButtonDisableState = () => {
    // initializing isDisabled
    let isDisabled = false;

    // something is not valid OR changes are detected OR no test has been selected yet OR status is already completed
    if (
      this.props.aaeSelectedUserAccommodationFileData.status_codename ===
        USER_ACCOMMODATION_FILE_STATUS_CONST.cancelled ||
      this.props.aaeSelectedUserAccommodationFileData.status_codename ===
        USER_ACCOMMODATION_FILE_STATUS_CONST.cancelledByCandidate
    ) {
      isDisabled = true;
    }
    return isDisabled;
  };

  getSaveButtonDisabledState = () => {
    // initializing isDisabled
    let isDisabled = false;

    // something is not valid OR no changes
    if (
      !this.props.aaeSelectedUserAccommodationFileTestToAdministerValidState ||
      !this.props.aaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState ||
      _.isEqual(
        this.props.aaeInitialSelectedUserAccommodationFileData,
        this.props.aaeSelectedUserAccommodationFileData
      )
    ) {
      isDisabled = true;
    }
    return isDisabled;
  };

  getCompleteProcessDisableState = () => {
    // initializing isDisabled
    let isDisabled = false;

    // something is not valid OR changes are detected OR no test has been selected yet OR no test center selected YET OR status is already completed/administered OR no assigned AAE expert
    // rationale and other measures cannot be blank
    if (
      !this.props.aaeSelectedUserAccommodationFileTestToAdministerValidState ||
      !this.props.aaeSelectedUserAccommodationFileTestCenterValidState ||
      !this.props.aaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState ||
      !_.isEqual(
        this.props.aaeInitialSelectedUserAccommodationFileData,
        this.props.aaeSelectedUserAccommodationFileData
      ) ||
      Object.keys(this.props.aaeSelectedUserAccommodationFileData.test_to_administer).length <= 0 ||
      this.props.aaeSelectedUserAccommodationFileData.status_codename ===
        USER_ACCOMMODATION_FILE_STATUS_CONST.pendingApproval ||
      this.props.aaeSelectedUserAccommodationFileData.status_codename ===
        USER_ACCOMMODATION_FILE_STATUS_CONST.completed ||
      this.props.aaeSelectedUserAccommodationFileData.status_codename ===
        USER_ACCOMMODATION_FILE_STATUS_CONST.administered ||
      this.props.aaeSelectedUserAccommodationFileData.status_codename ===
        USER_ACCOMMODATION_FILE_STATUS_CONST.cancelled ||
      this.props.aaeSelectedUserAccommodationFileData.status_codename ===
        USER_ACCOMMODATION_FILE_STATUS_CONST.cancelledByCandidate ||
      this.props.aaeSelectedUserAccommodationFileData.assigned_to_user_id === null ||
      this.props.aaeInitialSelectedUserAccommodationFileData.rationale === "" ||
      this.props.aaeInitialSelectedUserAccommodationFileData.other_measures === ""
    ) {
      isDisabled = true;
    }
    return isDisabled;
  };

  openRejectActionPopup = () => {
    this.setState({ showRejectActionPopup: true });
  };

  closeRejectActionPopup = () => {
    this.setState(
      {
        showRejectActionPopup: false,
        showActionDeniedPopup: false
      },
      () => {
        setTimeout(() => {
          this.setState({
            actionInProgress: false
          });
        }, 250);
      }
    );
  };

  handleRejectAction = () => {
    this.setState({ actionInProgress: true }, () => {
      // generating body
      const body = {
        user_accommodation_file_id: this.props.aaeSelectedUserAccommodationFileData.id,
        action: USER_ACCOMMODATION_FILE_STATUS_CONST.cancelled
      };
      // triggering action
      this.props.triggerSelectedUserAccommodationFileDataAction(body).then(response => {
        // success request
        if (response.ok) {
          // setting needed redux states
          this.props.setAaeInitialSelectedUserAccommodationFileData(response);
          this.props.setAaeSelectedUserAccommodationFileData(response);
          this.props.setAaeSelectedUserAccommodationFileTestToAdministerValidState(true);
          this.props.triggerAaeRerender();
          // getting 405 (action denied)
          if (response.status === 405) {
            // showing action denied popup
            this.setState({ showActionDeniedPopup: true, actionInProgress: false });
            // success
          } else {
            // closing popup
            this.closeRejectActionPopup();
          }
          // should never happen
        } else {
          throw new Error(
            "An error occurred during the reject selected user accommodation file process"
          );
        }
      });
    });
  };

  openSavedButCancelledByCandidatePopup = () => {
    this.setState({ showSavedButCancelledByCandidatePopup: true });
  };

  closeSavedButCancelledByCandidatePopup = () => {
    this.setState({ showSavedButCancelledByCandidatePopup: false });
  };

  handleSaveAction = () => {
    this.props
      .saveSelectedUserAccommodationFileData(this.props.aaeSelectedUserAccommodationFileData)
      .then(response => {
        // success request
        if (response.status === 200 || response.status === 421) {
          // setting needed redux states
          this.props.setAaeInitialSelectedUserAccommodationFileData(response);
          this.props.setAaeSelectedUserAccommodationFileData(response);
          this.props.setAaeSelectedUserAccommodationFileTestToAdministerValidState(true);
          this.props.triggerAaeRerender();
          // response is 421
          if (response.status === 421) {
            // open popup
            this.openSavedButCancelledByCandidatePopup();
          }
          // should never happen
        } else {
          throw new Error(
            "An error occurred during the save selected user accommodation file process"
          );
        }
      });
  };

  openCompleteActionPopup = () => {
    this.setState({ showCompleteActionPopup: true });
  };

  closeCompleteActionPopup = () => {
    this.setState(
      {
        showCompleteActionPopup: false,
        showActionDeniedPopup: false
      },
      () => {
        setTimeout(() => {
          this.setState({
            actionInProgress: false
          });
        }, 250);
      }
    );
  };

  openReOpenActionPopup = () => {
    this.setState({ showReOpenActionPopup: true });
  };

  closeReOpenActionPopup = () => {
    this.setState(
      {
        showReOpenActionPopup: false,
        showActionDeniedPopup: false
      },
      () => {
        setTimeout(() => {
          this.setState({
            actionInProgress: false
          });
        }, 250);
      }
    );
  };

  handleCompleteAction = () => {
    this.setState({ actionInProgress: true }, () => {
      // generating body
      const body = {
        user_accommodation_file_id: this.props.aaeSelectedUserAccommodationFileData.id,
        action: USER_ACCOMMODATION_FILE_STATUS_CONST.pendingApproval
      };
      // triggering action
      this.props.triggerSelectedUserAccommodationFileDataAction(body).then(response => {
        // success request
        if (response.ok) {
          // setting needed redux states
          this.props.setAaeInitialSelectedUserAccommodationFileData(response);
          this.props.setAaeSelectedUserAccommodationFileData(response);
          this.props.setAaeSelectedUserAccommodationFileTestToAdministerValidState(true);
          this.props.triggerAaeRerender();
          // getting 405 (action denied)
          if (response.status === 405) {
            // showing action denied popup
            this.setState({ showActionDeniedPopup: true, actionInProgress: false });
            // success
          } else {
            // closing popup
            this.closeCompleteActionPopup();
          }
          // should never happen
        } else {
          throw new Error(
            "An error occurred during the complete selected user accommodation file process"
          );
        }
      });
    });
  };

  handleReOpenAction = () => {
    this.setState({ actionInProgress: true }, () => {
      // generating body
      const body = {
        user_accommodation_file_id: this.props.aaeSelectedUserAccommodationFileData.id,
        action: USER_ACCOMMODATION_FILE_STATUS_CONST.reOpened
      };
      // triggering action
      this.props.triggerSelectedUserAccommodationFileDataAction(body).then(response => {
        // success request
        if (response.ok) {
          // setting needed redux states
          this.props.setAaeInitialSelectedUserAccommodationFileData(response);
          this.props.setAaeSelectedUserAccommodationFileData(response);
          this.props.setAaeSelectedUserAccommodationFileTestToAdministerValidState(true);
          this.props.triggerAaeRerender();
          // getting 405 (action denied)
          if (response.status === 405) {
            // showing action denied popup
            this.setState({ showActionDeniedPopup: true, actionInProgress: false });
            // success
          } else {
            // closing popup
            this.closeReOpenActionPopup();
          }
          // should never happen
        } else {
          throw new Error(
            "An error occurred during the re-open selected user accommodation file process"
          );
        }
      });
    });
  };

  render() {
    const mainTab = this.getTABS(AAE_SELECTED_USER_ACCOMMODATION_FILE_SECTIONS.main);
    const otherTabs = this.getTABS(AAE_SELECTED_USER_ACCOMMODATION_FILE_SECTIONS.other);

    return (
      <div>
        {this.state.isLoading ? (
          <div style={styles.loadingContainer}>
            <label className="fa fa-spinner fa-spin">
              <FontAwesomeIcon icon={faSpinner} style={styles.loading} />
            </label>
          </div>
        ) : (
          <div className="app" style={styles.appPadding}>
            <Helmet>
              <html lang={this.props.currentLanguage} />
              <title>{LOCALIZE.titles.aae}</title>
            </Helmet>
            <ContentContainer customMaxWidth={maxWidthUtils.wide}>
              <div ref={this.topTabsHeightRef}>
                <div style={styles.backButtonStyle}>
                  <CustomButton
                    buttonId="back-to-user-accommodation-file-selection-btn"
                    label={
                      <>
                        <FontAwesomeIcon icon={faArrowAltCircleLeft} />
                        <span style={styles.buttonLabel}>
                          {
                            LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest
                              .backToAccommodationRequestSelectionButton
                          }
                        </span>
                      </>
                    }
                    action={this.changesDetected}
                    buttonTheme={THEME.SECONDARY}
                  />
                </div>
                <Row role="presentation">
                  <Col style={styles.colOneStyle}>
                    <TopTabs
                      TABS={mainTab}
                      defaultTab={`${AAE_SELECTED_USER_ACCOMMODATION_FILE_SECTIONS.main}-1`}
                      customStyle={styles.generalTab}
                      setTab={this.props.setAaeSelectedUserAccommodationFileFirstColumnTab}
                      customCurrentTabProp={
                        this.props.aaeSelectedUserAccommodationFileFirstColumnTab
                      }
                    />
                  </Col>
                  <Col style={styles.colTwoStyle}>
                    <TopTabs
                      TABS={otherTabs}
                      defaultTab={`${AAE_SELECTED_USER_ACCOMMODATION_FILE_SECTIONS.other}-1`}
                      customStyle={styles.otherTab}
                      setTab={this.props.setAaeSelectedUserAccommodationFileSecondColumnTab}
                      customCurrentTabProp={
                        this.props.aaeSelectedUserAccommodationFileSecondColumnTab
                      }
                    />
                  </Col>
                </Row>
              </div>
              <SelectedUserAccommodationFileFooter
                rejectAction={() => this.openRejectActionPopup()}
                saveAction={() => this.handleSaveAction()}
                completeRequestAction={() => this.openCompleteActionPopup()}
                reOpenRequestAction={() => this.openReOpenActionPopup()}
                disabledRejectButton={this.getRejectButtonDisableState()}
                disabledSaveButton={this.getSaveButtonDisabledState()}
                disabledCompleteProcessButton={this.getCompleteProcessDisableState()}
              />
            </ContentContainer>
            <PopupBox
              show={this.state.showBackToUserAccommodationFileSelectionPopup}
              handleClose={this.closeBackToUserAccommodationFileSelectionPopup}
              title={
                LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest
                  .backToAccommodationRequestSelectionPopup.title
              }
              description={
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p>
                        {
                          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest
                            .backToAccommodationRequestSelectionPopup.systemMessageDescription
                        }
                      </p>
                    }
                  />
                  <p>
                    {
                      LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest
                        .backToAccommodationRequestSelectionPopup.description
                    }
                  </p>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonIcon={faTimes}
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={LOCALIZE.commons.confirm}
              rightButtonAction={this.handleBackToUserAccommodationFileSelection}
              rightButtonIcon={faCheck}
              size="lg"
            />
          </div>
        )}
        <PopupBox
          show={this.state.showRejectActionPopup}
          handleClose={() => {}}
          size="lg"
          title={
            !this.state.showActionDeniedPopup
              ? LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer.rejectPopup.title
              : LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer.actionDeniedPopup
                  .title
          }
          description={
            <div>
              {!this.state.showActionDeniedPopup ? (
                <>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p>
                        {
                          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                            .rejectPopup.systemMessageDescription
                        }
                      </p>
                    }
                  />
                  <p>
                    {
                      LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer.rejectPopup
                        .description
                    }
                  </p>
                </>
              ) : (
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.warning}
                  message={
                    <p>
                      {
                        LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                          .actionDeniedPopup.systemMessageDescription
                      }
                    </p>
                  }
                />
              )}
            </div>
          }
          leftButtonType={
            !this.state.showActionDeniedPopup ? BUTTON_TYPE.secondary : BUTTON_TYPE.none
          }
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={!this.state.showActionDeniedPopup ? faTimes : ""}
          leftButtonAction={this.closeRejectActionPopup}
          rightButtonType={
            !this.state.showActionDeniedPopup ? BUTTON_TYPE.danger : BUTTON_TYPE.primary
          }
          rightButtonTitle={
            this.state.actionInProgress ? (
              <div style={styles.customLoadingContainer}>
                {/* this div is useful to get the right size of the button while loading */}
                <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
                  <FontAwesomeIcon icon={faBan} style={styles.icon} />
                  {LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer.rejectButton}
                </div>
                <div style={styles.loadingOverlappingStyle}>
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} />
                  </label>
                </div>
              </div>
            ) : !this.state.showActionDeniedPopup ? (
              LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer.rejectButton
            ) : (
              LOCALIZE.commons.close
            )
          }
          rightButtonAction={
            !this.state.showActionDeniedPopup
              ? this.handleRejectAction
              : this.closeRejectActionPopup
          }
          rightButtonIcon={
            this.state.actionInProgress ? "" : !this.state.showActionDeniedPopup ? faBan : faTimes
          }
          rightButtonState={
            this.state.actionInProgress ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
        />
        <PopupBox
          show={this.state.showCompleteActionPopup}
          handleClose={() => {}}
          size="lg"
          title={
            !this.state.showActionDeniedPopup
              ? LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer.completePopup
                  .title
              : LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer.actionDeniedPopup
                  .title
          }
          description={
            <div>
              {!this.state.showActionDeniedPopup ? (
                <>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p>
                        {
                          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                            .completePopup.systemMessageDescription
                        }
                      </p>
                    }
                  />
                  <p>
                    {
                      LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                        .completePopup.description
                    }
                  </p>
                </>
              ) : (
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.warning}
                  message={
                    <p>
                      {
                        LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                          .actionDeniedPopup.systemMessageDescription
                      }
                    </p>
                  }
                />
              )}
            </div>
          }
          leftButtonType={
            !this.state.showActionDeniedPopup ? BUTTON_TYPE.secondary : BUTTON_TYPE.none
          }
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={!this.state.showActionDeniedPopup ? faTimes : ""}
          leftButtonAction={this.closeCompleteActionPopup}
          rightButtonType={
            !this.state.showActionDeniedPopup ? BUTTON_TYPE.success : BUTTON_TYPE.primary
          }
          rightButtonTitle={
            this.state.actionInProgress ? (
              <div style={styles.customLoadingContainer}>
                {/* this div is useful to get the right size of the button while loading */}
                <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
                  <FontAwesomeIcon icon={faPaperPlane} style={styles.icon} />
                  {
                    LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                      .completeRequestButton
                  }
                </div>
                <div style={styles.loadingOverlappingStyle}>
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} />
                  </label>
                </div>
              </div>
            ) : !this.state.showActionDeniedPopup ? (
              LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                .completeRequestButton
            ) : (
              LOCALIZE.commons.close
            )
          }
          rightButtonAction={
            !this.state.showActionDeniedPopup
              ? this.handleCompleteAction
              : this.closeCompleteActionPopup
          }
          rightButtonIcon={
            this.state.actionInProgress
              ? ""
              : !this.state.showActionDeniedPopup
                ? faPaperPlane
                : faTimes
          }
          rightButtonState={
            this.state.actionInProgress ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
        />
        <PopupBox
          show={this.state.showReOpenActionPopup}
          handleClose={() => {}}
          size="lg"
          title={
            !this.state.showActionDeniedPopup
              ? LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer.reOpenPopup.title
              : LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer.actionDeniedPopup
                  .title
          }
          description={
            <div>
              {!this.state.showActionDeniedPopup ? (
                <>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p>
                        {
                          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                            .reOpenPopup.systemMessageDescription
                        }
                      </p>
                    }
                  />
                  <p>
                    {
                      LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer.reOpenPopup
                        .description
                    }
                  </p>
                </>
              ) : (
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.warning}
                  message={
                    <p>
                      {
                        LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                          .actionDeniedPopup.systemMessageDescription
                      }
                    </p>
                  }
                />
              )}
            </div>
          }
          leftButtonType={
            !this.state.showActionDeniedPopup ? BUTTON_TYPE.secondary : BUTTON_TYPE.none
          }
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={!this.state.showActionDeniedPopup ? faTimes : ""}
          leftButtonAction={this.closeReOpenActionPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            this.state.actionInProgress ? (
              <div style={styles.customLoadingContainer}>
                {/* this div is useful to get the right size of the button while loading */}
                <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
                  <FontAwesomeIcon icon={faFolderOpen} style={styles.icon} />
                  {
                    LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                      .reOpenRequestButton
                  }
                </div>
                <div style={styles.loadingOverlappingStyle}>
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} />
                  </label>
                </div>
              </div>
            ) : !this.state.showActionDeniedPopup ? (
              LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer.reOpenRequestButton
            ) : (
              LOCALIZE.commons.close
            )
          }
          rightButtonAction={
            !this.state.showActionDeniedPopup
              ? this.handleReOpenAction
              : this.closeReOpenActionPopup
          }
          rightButtonIcon={
            this.state.actionInProgress
              ? ""
              : !this.state.showActionDeniedPopup
                ? faFolderOpen
                : faTimes
          }
          rightButtonState={
            this.state.actionInProgress ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
        />
        <PopupBox
          show={this.state.showSavedButCancelledByCandidatePopup}
          handleClose={() => {}}
          title={
            LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
              .savedButCancelledByCandidatePopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.info}
                title={LOCALIZE.commons.info}
                message={
                  <p>
                    {
                      LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                        .savedButCancelledByCandidatePopup.systemMessageDescription
                    }
                  </p>
                }
              />
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonAction={this.closeSavedButCancelledByCandidatePopup}
          rightButtonIcon={faTimes}
          size="lg"
        />
      </div>
    );
  }
}

export { SelectedUserAccommodationFileTab as unconnectedSelectedUserAccommodationFileTab };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    aaeInitialSelectedUserAccommodationFileData:
      state.aae.aaeInitialSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileData: state.aae.aaeSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileFirstColumnTab:
      state.aae.aaeSelectedUserAccommodationFileFirstColumnTab,
    aaeSelectedUserAccommodationFileSecondColumnTab:
      state.aae.aaeSelectedUserAccommodationFileSecondColumnTab,
    aaeSelectedUserAccommodationFileTestToAdministerValidState:
      state.aae.aaeSelectedUserAccommodationFileTestToAdministerValidState,
    aaeSelectedUserAccommodationFileTestCenterValidState:
      state.aae.aaeSelectedUserAccommodationFileTestCenterValidState,
    aaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState:
      state.aae.aaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      triggerAppRerender,
      resetAaeSelectedUserAccommodationFileDataState,
      setAaeSelectedUserAccommodationFileFirstColumnTab,
      setAaeSelectedUserAccommodationFileSecondColumnTab,
      saveSelectedUserAccommodationFileData,
      setAaeInitialSelectedUserAccommodationFileData,
      setAaeSelectedUserAccommodationFileData,
      setAaeSelectedUserAccommodationFileTestToAdministerValidState,
      triggerSelectedUserAccommodationFileDataAction,
      triggerAaeRerender
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SelectedUserAccommodationFileTab);
