import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Col } from "react-bootstrap";
import {
  getNumberOfClosedRequestsAaeReportData,
  getNumberOfRequestsReceivedAaeReportData,
  setAaeRequestsSelectedTab
} from "../../modules/AaeRedux";
import DropdownSelect from "../commons/DropdownSelect";
import DatePicker, { validateDatePicked } from "../commons/DatePicker";
import { populateCustomYearsFutureDateOptions } from "../../helpers/populateCustomDatePickerOptions";
import { resetDatePickedStates, updateDatePicked } from "../../modules/DatePickerRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPoll, faSpinner } from "@fortawesome/free-solid-svg-icons";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import generateNumberOfRequestsReceivedReport from "./GenerateNumberOfRequestsReceivedReport";
import { CSVLink } from "react-csv";
import GenerateNumberOfClosedRequestsReport from "./GenerateNumberOfClosedRequestsReport";
import GenerateNumberOfRequestsByLimitationSleTestTypeReport from "./GenerateNumberOfRequestsByLimitationSleTestTypeReport";
import GenerateNumberOfRequestsByRecommendationReport from "./GenerateNumberOfRequestsByRecommendationReport";
import GenerateServiceStandardsReport from "./GenerateServiceStandardsReport";
import GenerateTimeInEachStatusReport from "./GenerateTimeInEachStatusReport";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  mainContainer: {
    width: "100%"
  },
  dropdownsMainContainer: {
    padding: 24,
    width: "100%"
  },
  labelContainer: {
    verticalAlign: "middle",
    paddingRight: 12
  },
  boldText: {
    fontWeight: "bold"
  },
  optionSelectionContainer: {
    padding: "12px 24px",
    margin: "0 auto",
    width: "80%"
  },
  dateError: {
    paddingTop: 0,
    color: "#923534",
    fontWeight: "bold"
  },
  dateLabelsOnError: {
    color: "#923534",
    fontWeight: "bold"
  },
  generateButtonContainer: {
    textAlign: "center",
    marginTop: 18
  },
  generateButtonIcon: {
    transform: "scale(1.5)",
    padding: "1.5px",
    marginRight: 12
  },
  generateButtonLabel: {
    minWidth: 175
  }
};

class AccommodationReports extends Component {
  constructor(props) {
    super(props);
    this.dateFromDayFieldRef = React.createRef();
    this.dateFromMonthFieldRef = React.createRef();
    this.dateFromYearFieldRef = React.createRef();
    this.dateToDayFieldRef = React.createRef();
    this.dateToMonthFieldRef = React.createRef();
    this.dateToYearFieldRef = React.createRef();
    this.createReportLinkButtonRef = React.createRef();

    this.PropTypes = {};
  }

  state = {
    reportTypeOptions: [],
    reportTypeSelectedOption: [],
    dateToVisible: false,
    displayDateError: false,
    displayInvalidDateFromError: false,
    displayInvalidDateToError: false,
    selectedDateFrom: null,
    selectedDateTo: null,
    currentlyLoading: false,
    generateReportButtonDisabled: true,
    reportCreationLoading: false,
    formattedReportData: [],
    reportName: "",
    displayNoDataError: false
  };

  componentDidMount = () => {
    // populating report types
    this.populateReportTypes();
  };

  populateReportTypes = () => {
    // initializing report type options
    const reportTypeOptions = [];

    // populating options
    reportTypeOptions.push({
      label: LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfRequestsReceived.title,
      value: 1
    });
    reportTypeOptions.push({
      label: LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfClosedRequests.title,
      value: 2
    });
    reportTypeOptions.push({
      label:
        LOCALIZE.aae.sideNav.accommodationReports.reportTypes
          .numberOfRequestsByLimitationAndTestType.title,
      value: 3
    });
    reportTypeOptions.push({
      label:
        LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfRequestsByRecommendation
          .title,
      value: 4
    });
    reportTypeOptions.push({
      label: LOCALIZE.aae.sideNav.accommodationReports.reportTypes.serviceStandards.title,
      value: 5
    });
    reportTypeOptions.push({
      label: LOCALIZE.aae.sideNav.accommodationReports.reportTypes.timeInEachStatus.title,
      value: 6
    });

    this.setState({ reportTypeOptions: reportTypeOptions });
  };

  // get selected report type
  getSelectedReportType = selectedOption => {
    // resetting date picked (redux states)
    this.props.resetDatePickedStates();

    this.setState({ reportTypeSelectedOption: [] }, () => {
      this.setState({
        reportTypeSelectedOption: selectedOption,
        dateToVisible: false,
        selectedDateFrom: null,
        selectedDateTo: null,
        generateReportButtonDisabled: true
      });
    });
  };

  // handling date updates
  handleDateUpdates = () => {
    let dateFrom = null;
    let dateTo = null;

    // date from date has been fully provided
    if (
      this.dateFromDayFieldRef.current.props.value !== "" &&
      this.dateFromMonthFieldRef.current.props.value !== "" &&
      this.dateFromYearFieldRef.current.props.value !== ""
    ) {
      dateFrom = `${this.dateFromDayFieldRef.current.props.value.value}-${this.dateFromMonthFieldRef.current.props.value.value}-${this.dateFromYearFieldRef.current.props.value.value}`;
      this.setState({ dateToVisible: true });
    }

    // date to field is visible, meaning that the date from date has been fully provided
    if (this.state.dateToVisible) {
      // date to date has been fully provided
      if (
        this.dateToDayFieldRef.current.props.value !== "" &&
        this.dateToMonthFieldRef.current.props.value !== "" &&
        this.dateToYearFieldRef.current.props.value !== ""
      ) {
        dateTo = `${this.dateToDayFieldRef.current.props.value.value}-${this.dateToMonthFieldRef.current.props.value.value}-${this.dateToYearFieldRef.current.props.value.value}`;
      }
    }
    // both dates have been provided
    if (dateFrom !== null && dateTo !== null) {
      // valid dates
      if (this.dateValidation(dateFrom, dateTo)) {
        //
        this.setState({
          generateReportButtonDisabled: false,
          selectedDateFrom: dateFrom,
          selectedDateTo: dateTo,
          displayInvalidDateFromError: false,
          displayInvalidDateToError: false,
          displayDateError: false
        });
      } else {
        this.setState({
          // disable generate button on invalid date
          generateReportButtonDisabled: true
        });
      }
    }
  };

  // date validation
  dateValidation = (from, to) => {
    // converting string dates in dates (provided format: 'dd-mm-yyyy')
    const dateFrom = new Date(from.split("-")[2], from.split("-")[1] - 1, from.split("-")[0]);
    const dateTo = new Date(to.split("-")[2], to.split("-")[1] - 1, to.split("-")[0]);

    // dates validation
    // reformatting date in format 'yyyy-mm-dd' for validation function
    const isValidDateFrom = validateDatePicked(
      `${from.split("-")[2]}-${from.split("-")[1]}-${from.split("-")[0]}`
    );
    const isValidDateTo = validateDatePicked(
      `${to.split("-")[2]}-${to.split("-")[1]}-${to.split("-")[0]}`
    );

    // invalid from date
    if (!isValidDateFrom || !isValidDateTo) {
      this.setState({
        displayInvalidDateFromError: !isValidDateFrom,
        displayInvalidDateToError: !isValidDateTo,
        displayDateError: false,
        generateReportButtonDisabled: true
      });
      return false;
    }
    // date from is greater to date to (invalid)
    if (dateFrom > dateTo) {
      this.setState({
        displayDateError: true,
        generateReportButtonDisabled: true,
        displayInvalidDateFromError: !isValidDateFrom,
        displayInvalidDateToError: !isValidDateTo
      });
      return false;
    }
    // everything is valid
    return true;
  };

  // this function is called for accessibility purposes
  // it will update the completeDatePicked redux state on focus, so the screen reader will read the current selected date
  onDateFromFocus = () => {
    this.props.updateDatePicked(
      `${
        this.dateFromYearFieldRef.current
          ? this.dateFromYearFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }-${
        this.dateFromMonthFieldRef.current
          ? this.dateFromMonthFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }-${
        this.dateFromDayFieldRef.current
          ? this.dateFromDayFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }`
    );
  };

  // this function is called for accessibility purposes
  // it will update the completeDatePicked redux state on focus, so the screen reader will read the current selected date
  onDateToFocus = () => {
    this.props.updateDatePicked(
      `${
        this.dateToYearFieldRef.current
          ? this.dateToYearFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }-${
        this.dateToMonthFieldRef.current
          ? this.dateToMonthFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }-${
        this.dateToDayFieldRef.current
          ? this.dateToDayFieldRef.current.props.value.label
          : LOCALIZE.datePicker.none
      }`
    );
  };

  // generating report extract
  generateReport = async () => {
    this.setState({ reportCreationLoading: true }, () => {
      switch (this.state.reportTypeSelectedOption.label) {
        // Number of Requests Received
        case LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfRequestsReceived.title:
          this.props
            .getNumberOfRequestsReceivedAaeReportData(
              // formatting dd-mm-yyyy to yyyy-mm-dd
              this.state.selectedDateFrom.split("-").reverse().join("-"),
              // formatting dd-mm-yyyy to yyyy-mm-dd
              this.state.selectedDateTo.split("-").reverse().join("-")
            )
            .then(response => {
              // getting data
              if (response.length >= 0) {
                this.handleReportDataFormatting(response);
                // should never happen
              } else {
                // display no data popup
                this.setState({ displayNoDataError: true });
              }
            });
          break;
        // Number of Closed Requests
        case LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfClosedRequests.title:
          this.props
            .getNumberOfClosedRequestsAaeReportData(
              // formatting dd-mm-yyyy to yyyy-mm-dd
              this.state.selectedDateFrom.split("-").reverse().join("-"),
              // formatting dd-mm-yyyy to yyyy-mm-dd
              this.state.selectedDateTo.split("-").reverse().join("-")
            )
            .then(response => {
              // getting data
              if (response.length >= 0) {
                this.handleReportDataFormatting(response);
                // should never happen
              } else {
                // display no data popup
                this.setState({ displayNoDataError: true });
              }
            });
          break;
        // Number of Requests by Limitation / by SLE Test Type
        case LOCALIZE.aae.sideNav.accommodationReports.reportTypes
          .numberOfRequestsByLimitationAndTestType.title:
          this.props
            .getNumberOfRequestsByLimitationBySleTestTypeAaeReportData(
              // TODO: add other parameters here
              // formatting dd-mm-yyyy to yyyy-mm-dd
              this.state.selectedDateFrom.split("-").reverse().join("-"),
              // formatting dd-mm-yyyy to yyyy-mm-dd
              this.state.selectedDateTo.split("-").reverse().join("-")
            )
            .then(response => {
              // getting data
              if (response.length >= 0) {
                this.handleReportDataFormatting(response);
                // should never happen
              } else {
                // display no data popup
                this.setState({ displayNoDataError: true });
              }
            });
          break;
        // Number of Requests by Recommendation
        case LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfRequestsByRecommendation
          .title:
          this.props
            .getNumberOfRequestsByRecommendationAaeReportData(
              // TODO: add other parameters here
              // formatting dd-mm-yyyy to yyyy-mm-dd
              this.state.selectedDateFrom.split("-").reverse().join("-"),
              // formatting dd-mm-yyyy to yyyy-mm-dd
              this.state.selectedDateTo.split("-").reverse().join("-")
            )
            .then(response => {
              // getting data
              if (response.length >= 0) {
                this.handleReportDataFormatting(response);
                // should never happen
              } else {
                // display no data popup
                this.setState({ displayNoDataError: true });
              }
            });
          break;
        // Service Standards
        case LOCALIZE.aae.sideNav.accommodationReports.reportTypes.serviceStandards.title:
          this.props
            .getServiceStandardsAaeReportData(
              // formatting dd-mm-yyyy to yyyy-mm-dd
              this.state.selectedDateFrom.split("-").reverse().join("-"),
              // formatting dd-mm-yyyy to yyyy-mm-dd
              this.state.selectedDateTo.split("-").reverse().join("-")
            )
            .then(response => {
              // getting data
              if (response.length >= 0) {
                this.handleReportDataFormatting(response);
                // should never happen
              } else {
                // display no data popup
                this.setState({ displayNoDataError: true });
              }
            });
          break;
        // Time in Each Status
        case LOCALIZE.aae.sideNav.accommodationReports.reportTypes.timeInEachStatus.title:
          this.props
            .getTimeInEachStatusAaeReportData(
              // formatting dd-mm-yyyy to yyyy-mm-dd
              this.state.selectedDateFrom.split("-").reverse().join("-"),
              // formatting dd-mm-yyyy to yyyy-mm-dd
              this.state.selectedDateTo.split("-").reverse().join("-")
            )
            .then(response => {
              // getting data
              if (response.length >= 0) {
                this.handleReportDataFormatting(response);
                // should never happen
              } else {
                // display no data popup
                this.setState({ displayNoDataError: true });
              }
            });
          break;
        default:
          break;
      }
    });
  };

  // formatting report data
  handleReportDataFormatting = reportData => {
    // if there is at least one entry based on the provided parameters
    if (reportData.length > 0) {
      // initializing variables
      let reportName = "";
      let formattedReportData = [];
      // formatting report data and creating report name
      switch (this.state.reportTypeSelectedOption.label) {
        // Number of Requests Received
        case LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfRequestsReceived.title:
          // generating report data
          formattedReportData = generateNumberOfRequestsReceivedReport(
            reportData,
            this.props.currentLanguage
          );
          // generating report name
          reportName = `${
            LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfRequestsReceived.title
          } (${this.getFormattedDate(this.state.selectedDateFrom)} - ${this.getFormattedDate(
            this.state.selectedDateTo
          )}).csv`;
          break;
        // Number of Closed Requests
        case LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfClosedRequests.title:
          // generating report data
          formattedReportData = GenerateNumberOfClosedRequestsReport(
            reportData,
            this.props.currentLanguage
          );
          // generating report name
          reportName = `${
            LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfClosedRequests.title
          } (${this.getFormattedDate(this.state.selectedDateFrom)} - ${this.getFormattedDate(
            this.state.selectedDateTo
          )}).csv`;
          break;
        // Number of Requests by Limitation / by SLE Test Type
        case LOCALIZE.aae.sideNav.accommodationReports.reportTypes
          .numberOfRequestsByLimitationAndTestType.title:
          // generating report data
          formattedReportData = GenerateNumberOfRequestsByLimitationSleTestTypeReport(
            reportData,
            this.props.currentLanguage
          );
          // generating report name
          reportName = `${
            LOCALIZE.aae.sideNav.accommodationReports.reportTypes
              .numberOfRequestsByLimitationAndTestType.title
          } (${this.getFormattedDate(this.state.selectedDateFrom)} - ${this.getFormattedDate(
            this.state.selectedDateTo
          )}).csv`;
          break;
        // Number of Requests by Recommendation
        case LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfRequestsByRecommendation
          .title:
          // generating report data
          formattedReportData = GenerateNumberOfRequestsByRecommendationReport(
            reportData,
            this.props.currentLanguage
          );
          // generating report name
          reportName = `${
            LOCALIZE.aae.sideNav.accommodationReports.reportTypes.numberOfRequestsByRecommendation
              .title
          } (${this.getFormattedDate(this.state.selectedDateFrom)} - ${this.getFormattedDate(
            this.state.selectedDateTo
          )}).csv`;
          break;
        // Service Standards
        case LOCALIZE.aae.sideNav.accommodationReports.reportTypes.serviceStandards.title:
          // generating report data
          formattedReportData = GenerateServiceStandardsReport(
            reportData,
            this.props.currentLanguage
          );
          // generating report name
          reportName = `${
            LOCALIZE.aae.sideNav.accommodationReports.reportTypes.serviceStandards.title
          } (${this.getFormattedDate(this.state.selectedDateFrom)} - ${this.getFormattedDate(
            this.state.selectedDateTo
          )}).csv`;
          break;
        // Time in Each Status
        case LOCALIZE.aae.sideNav.accommodationReports.reportTypes.timeInEachStatus.title:
          // generating report data
          formattedReportData = GenerateTimeInEachStatusReport(
            reportData,
            this.props.currentLanguage
          );
          // generating report name
          reportName = `${
            LOCALIZE.aae.sideNav.accommodationReports.reportTypes.timeInEachStatus.title
          } (${this.getFormattedDate(this.state.selectedDateFrom)} - ${this.getFormattedDate(
            this.state.selectedDateTo
          )}).csv`;
          break;
        // should never happen
        default:
          break;
      }

      this.setState({ formattedReportData: formattedReportData, reportName: reportName }, () => {
        this.setState({ reportCreationLoading: false }, () => {
          // once all data has been created, simulate click on hidden create report link to generate the downloadable csv file
          this.createReportLinkButtonRef.current.link.click();
          // clearing all fields
          this.resetAllFields();
        });
      });
      // there is no data to show based on the provided parameters
    } else {
      // display no data popup
      this.setState({ displayNoDataError: true });
    }
  };

  // getting formatted date
  getFormattedDate = date => {
    const day = parseInt(date.split("-")[0]) < 10 ? `0${date.split("-")[0]}` : date.split("-")[0];
    const month = parseInt(date.split("-")[1]) < 10 ? `0${date.split("-")[1]}` : date.split("-")[1];
    const year = date.split("-")[2];
    return `${year}-${month}-${day}`;
  };

  // resetting all fields
  resetAllFields = () => {
    this.setState({ reportCreationLoading: true }, () => {
      this.setState(
        {
          selectedReportType: "",
          generateReportButtonDisabled: true,
          reportCreationLoading: false,
          formattedReportData: [],
          reportName: "",
          displayNoDataError: false,
          reportTypeSelectedOption: [],
          dateToVisible: false,
          displayDateError: false,
          displayInvalidDateFromError: false,
          displayInvalidDateToError: false,
          selectedDateFrom: null,
          selectedDateTo: null
        },
        () => {
          this.setState({ reportCreationLoading: false });
        }
      );
    });
  };

  closeNoDataPopup = () => {
    this.setState({ displayNoDataError: false, reportCreationLoading: false });
  };

  render() {
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.aae.sideNav.accommodationReports.title}</h2>
          <p>{LOCALIZE.aae.sideNav.accommodationReports.description}</p>
        </div>
        <div style={styles.dropdownsMainContainer}>
          <Row
            className="align-items-center justify-content-start"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="aae-reports-report-type-label" style={styles.boldText}>
                {LOCALIZE.reports.reportTypeLabel}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
            >
              <DropdownSelect
                idPrefix="aae-reports-report-type"
                ariaLabelledBy="aae-reports-report-type-label"
                ariaRequired={true}
                options={this.state.reportTypeOptions}
                onChange={this.getSelectedReportType}
                hasPlaceholder={true}
                defaultValue={this.state.reportTypeSelectedOption}
              />
            </Col>
          </Row>
          {Object.keys(this.state.reportTypeSelectedOption).length > 0 && (
            <>
              <Row
                className="align-items-center justify-content-start"
                style={styles.optionSelectionContainer}
                role="presentation"
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={
                    this.state.displayDateError
                      ? { ...styles.labelContainer, ...styles.dateLabelsOnError }
                      : styles.labelContainer
                  }
                >
                  <label id="date-from-label" style={styles.boldText}>
                    {LOCALIZE.reports.dateFromLabel}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.singleDropdownContainer}
                  onFocus={this.onDateFromFocus}
                >
                  <DatePicker
                    dateDayFieldRef={this.dateFromDayFieldRef}
                    dateMonthFieldRef={this.dateFromMonthFieldRef}
                    dateYearFieldRef={this.dateFromYearFieldRef}
                    dateLabelId={"date-from-label"}
                    onInputChange={this.handleDateUpdates}
                    isValidCompleteDatePicked={!this.state.displayInvalidDateFromError}
                    customYearOptions={populateCustomYearsFutureDateOptions()}
                    disabledDropdowns={this.state.currentlyLoading}
                  />
                </Col>
              </Row>
              {this.state.dateToVisible && (
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.optionSelectionContainer}
                  role="presentation"
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={
                      this.state.displayDateError
                        ? { ...styles.labelContainer, ...styles.dateLabelsOnError }
                        : styles.labelContainer
                    }
                  >
                    <label id="date-to-label" style={styles.boldText}>
                      {LOCALIZE.reports.dateToLabel}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.singleDropdownContainer}
                    onFocus={this.onDateToFocus}
                  >
                    <DatePicker
                      dateDayFieldRef={this.dateToDayFieldRef}
                      dateMonthFieldRef={this.dateToMonthFieldRef}
                      dateYearFieldRef={this.dateToYearFieldRef}
                      dateLabelId={"date-to-label"}
                      onInputChange={this.handleDateUpdates}
                      isValidCompleteDatePicked={!this.state.displayInvalidDateToError}
                      customYearOptions={populateCustomYearsFutureDateOptions()}
                      disabledDropdowns={this.state.currentlyLoading}
                    />
                  </Col>
                </Row>
              )}
              {this.state.displayDateError && (
                <Row
                  className="align-items-center justify-content-end"
                  style={styles.optionSelectionContainer}
                >
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <p tabIndex={"0"} style={styles.dateError}>
                      {LOCALIZE.reports.dateError}
                    </p>
                  </Col>
                </Row>
              )}
            </>
          )}
        </div>
        <div style={styles.generateButtonContainer}>
          <CustomButton
            label={
              this.state.reportCreationLoading ? (
                // eslint-disable-next-line jsx-a11y/label-has-associated-control
                <label className="fa fa-spinner fa-spin">
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <>
                  <span>
                    <FontAwesomeIcon icon={faPoll} style={styles.generateButtonIcon} />
                  </span>
                  <span>{LOCALIZE.reports.generateButton}</span>
                </>
              )
            }
            action={this.generateReport}
            customStyle={styles.generateButtonLabel}
            buttonTheme={THEME.PRIMARY}
            disabled={
              this.state.generateReportButtonDisabled ? true : !!this.state.reportCreationLoading
            }
          />
        </div>
        <CSVLink
          ref={this.createReportLinkButtonRef}
          asyncOnClick={true}
          data={this.state.formattedReportData}
          filename={this.state.reportName}
          enclosingCharacter=""
        ></CSVLink>
        <PopupBox
          show={this.state.displayNoDataError}
          title={LOCALIZE.reports.noDataPopup.title}
          handleClose={() => {}}
          size={"lg"}
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.info}
                title={LOCALIZE.commons.info}
                message={<p>{LOCALIZE.reports.noDataPopup.description}</p>}
              />
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.closeNoDataPopup}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    aaeRequestsSelectedTab: state.aae.aaeRequestsSelectedTab,
    completeDatePicked: state.datePicker.completeDatePicked
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setAaeRequestsSelectedTab,
      updateDatePicked,
      resetDatePickedStates,
      getNumberOfRequestsReceivedAaeReportData,
      getNumberOfClosedRequestsAaeReportData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AccommodationReports);
