/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { setAaeSelectedUserAccommodationFileData } from "../../modules/AaeRedux";
import * as _ from "lodash";
import { userAccommodationFileIsInEndState } from "./SelectedUserAccommodationFile";

const styles = {
  noMargin: {
    margin: 0
  },
  formContainer: {
    padding: "12px 24px 0 24px"
  },
  label: {
    fontWeight: "bold"
  },
  inputContainer: {
    padding: "0 24px"
  },
  textArea: {
    padding: 6,
    border: "1px solid #00565E",
    borderRadius: 4,
    width: "100%",
    height: 450,
    resize: "vertical"
  }
};

class SelectedUserAccommodationFileMainTabOtherMeasures extends Component {
  state = {
    otherMeasuresContent: this.props.aaeSelectedUserAccommodationFileData.other_measures,
    typingTimerRedux: 0,
    triggerRerender: false
  };

  updateOtherMeasuresContent = event => {
    this.setState({ otherMeasuresContent: event.target.value });

    // Create a buffer before updating the redux state
    // Clear the old typingTimerRedux
    clearTimeout(this.state.typingTimerRedux);

    // Change the typingTimerRedux to a timeout of 200 millisec
    // When user stops writing, this will go through after that time
    // Otherwise, the timer will be reset to that time
    const newTimer = setTimeout(() => {
      // Update redux --> update after typing timer
      // creating a copy of respective redux states
      const copyOfAaeSelectedUserAccommodationFileData = _.cloneDeep(
        this.props.aaeSelectedUserAccommodationFileData
      );
      // updating needed attributes
      copyOfAaeSelectedUserAccommodationFileData.other_measures = event.target.value;
      this.props.setAaeSelectedUserAccommodationFileData(
        copyOfAaeSelectedUserAccommodationFileData
      );
    }, 200);

    this.setState({ typingTimerRedux: newTimer });
  };

  render() {
    return (
      <div>
        <h2 style={styles.noMargin}>
          {
            LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
              .otherMeasures.title
          }
        </h2>
        <div style={styles.formContainer}>
          <label htmlFor="user-accommodation-file-other-measures-input" style={styles.label}>
            {
              LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.topTabs.main.sideNavigation
                .otherMeasures.inputLabel
            }
          </label>
          <br />
          <textarea
            id="user-accommodation-file-other-measures-input"
            className={"valid-field"}
            style={styles.textArea}
            value={this.state.otherMeasuresContent}
            onChange={this.updateOtherMeasuresContent}
            maxLength="15000"
            disabled={userAccommodationFileIsInEndState(
              this.props.aaeSelectedUserAccommodationFileData.status_codename
            )}
          />
        </div>
      </div>
    );
  }
}

export { SelectedUserAccommodationFileMainTabOtherMeasures as unconnectedSelectedUserAccommodationFileMainTabOtherMeasures };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    aaeInitialSelectedUserAccommodationFileData:
      state.aae.aaeInitialSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileData: state.aae.aaeSelectedUserAccommodationFileData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setAaeSelectedUserAccommodationFileData
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectedUserAccommodationFileMainTabOtherMeasures);
