/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Col, Row, Navbar } from "react-bootstrap";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";

import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSave,
  faPaperPlane,
  faCheck,
  faBan,
  faFolderOpen
} from "@fortawesome/free-solid-svg-icons";
import USER_ACCOMMODATION_FILE_STATUS_CONST from "./Constants";

const styles = {
  footer: {
    borderTop: "1px solid #96a8b2",
    backgroundColor: "#D5DEE0"
  },
  footerIndex: {
    zIndex: 1
  },
  content: {
    width: "100%"
  },
  span: {
    paddingLeft: 5,
    paddingRight: 5
  },
  buttonMinWidth: {
    minWidth: 150
  }
};

class SelectedUserAccommodationFileFooter extends Component {
  constructor(props, context) {
    super(props, context);
    this.PropTypes = {
      rejectAction: PropTypes.func,
      saveAction: PropTypes.func,
      completeRequestAction: PropTypes.func,
      reOpenRequestAction: PropTypes.func,
      disabledRejectButton: PropTypes.bool,
      disabledSaveButton: PropTypes.bool,
      disabledCompleteProcessButton: PropTypes.bool
    };

    SelectedUserAccommodationFileFooter.defaultProps = {
      disabledRejectButton: true,
      disabledSaveButton: true,
      disabledCompleteProcessButton: true
    };
  }

  currentlyOnEndState = () => {
    // initializing currentlyOnEndState
    let currentlyOnEndState = false;
    if (
      this.props.aaeSelectedUserAccommodationFileData.status_codename ===
        USER_ACCOMMODATION_FILE_STATUS_CONST.pendingApproval ||
      this.props.aaeSelectedUserAccommodationFileData.status_codename ===
        USER_ACCOMMODATION_FILE_STATUS_CONST.completed ||
      this.props.aaeSelectedUserAccommodationFileData.status_codename ===
        USER_ACCOMMODATION_FILE_STATUS_CONST.administered ||
      this.props.aaeSelectedUserAccommodationFileData.status_codename ===
        USER_ACCOMMODATION_FILE_STATUS_CONST.cancelled ||
      this.props.aaeSelectedUserAccommodationFileData.status_codename ===
        USER_ACCOMMODATION_FILE_STATUS_CONST.cancelledByCandidate
    ) {
      currentlyOnEndState = true;
    }
    return currentlyOnEndState;
  };

  render() {
    return (
      <div role="contentinfo" className="fixed-bottom" style={styles.footerIndex}>
        <Navbar
          id="selected-user-accommodation-file-footer-main-div"
          fixed="bottom"
          style={styles.footer}
        >
          <div style={styles.content}>
            <Row className="justify-content-center align-items-center" role="presentation">
              <Col xl={4} lg={4} md={4} sm={4} xs={4} className="text-left">
                <CustomButton
                  label={
                    <>
                      <FontAwesomeIcon icon={faBan} />
                      <span style={styles.span} className="notranslate">
                        {
                          LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                            .rejectButton
                        }
                      </span>
                    </>
                  }
                  action={this.props.rejectAction}
                  buttonTheme={THEME.DANGER}
                  customStyle={styles.buttonMinWidth}
                  ariaLabel={
                    LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer.rejectButton
                  }
                  disabled={this.props.disabledRejectButton}
                />
              </Col>
              <Col xl={4} lg={4} md={4} sm={4} xs={4} className="text-center">
                <CustomButton
                  label={
                    <>
                      <FontAwesomeIcon
                        icon={
                          this.props.disabledSaveButton &&
                          this.props.aaeSelectedUserAccommodationFileTestToAdministerValidState &&
                          this.props
                            .aaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState
                            ? faCheck
                            : faSave
                        }
                      />
                      <span style={styles.span} className="notranslate">
                        {this.props.disabledSaveButton &&
                        this.props.aaeSelectedUserAccommodationFileTestToAdministerValidState &&
                        this.props.aaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState
                          ? LOCALIZE.commons.saved
                          : LOCALIZE.commons.saveButton}
                      </span>
                    </>
                  }
                  action={this.props.saveAction}
                  buttonTheme={
                    this.props.disabledSaveButton &&
                    this.props.aaeSelectedUserAccommodationFileTestToAdministerValidState &&
                    this.props.aaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState
                      ? THEME.SUCCESS
                      : THEME.PRIMARY
                  }
                  customStyle={styles.buttonMinWidth}
                  ariaLabel={LOCALIZE.commons.saveButton}
                  disabled={this.props.disabledSaveButton}
                />
              </Col>
              <Col xl={4} lg={4} md={4} sm={4} xs={4} className="text-right">
                <CustomButton
                  label={
                    <>
                      <FontAwesomeIcon
                        icon={this.currentlyOnEndState() ? faFolderOpen : faPaperPlane}
                      />
                      <span style={styles.span} className="notranslate">
                        {this.currentlyOnEndState()
                          ? LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                              .reOpenRequestButton
                          : LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                              .completeRequestButton}
                      </span>
                    </>
                  }
                  action={
                    this.currentlyOnEndState()
                      ? this.props.reOpenRequestAction
                      : this.props.completeRequestAction
                  }
                  buttonTheme={this.currentlyOnEndState() ? THEME.SECONDARY : THEME.SUCCESS}
                  customStyle={styles.buttonMinWidth}
                  ariaLabel={
                    this.currentlyOnEndState()
                      ? LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                          .reOpenRequestButton
                      : LOCALIZE.aae.sideNav.accommodationRequests.selectedRequest.footer
                          .completeRequestButton
                  }
                  disabled={
                    this.currentlyOnEndState() &&
                    this.props.aaeSelectedUserAccommodationFileData.status_codename !==
                      USER_ACCOMMODATION_FILE_STATUS_CONST.cancelledByCandidate
                      ? false
                      : this.props.disabledCompleteProcessButton
                  }
                />
              </Col>
            </Row>
          </div>
        </Navbar>
      </div>
    );
  }
}

export { SelectedUserAccommodationFileFooter as unconnectedSelectedUserAccommodationFileFooter };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    aaeInitialSelectedUserAccommodationFileData:
      state.aae.aaeInitialSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileData: state.aae.aaeSelectedUserAccommodationFileData,
    aaeSelectedUserAccommodationFileTestToAdministerValidState:
      state.aae.aaeSelectedUserAccommodationFileTestToAdministerValidState,
    aaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState:
      state.aae.aaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SelectedUserAccommodationFileFooter);
