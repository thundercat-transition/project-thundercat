// if you update these statuses, don't forget to also update ".../backend/static/user_accommodation_file_status_const.py" file (backend)
const USER_ACCOMMODATION_FILE_STATUS_CONST = {
  new: "new",
  estimatingServiceCost: "estimating_service_cost",
  waitingForFinancialCoding: "waiting_for_financial_coding",
  waitingForDocumentation: "waiting_for_documentation",
  ready: "ready",
  inProgress: "in_progress",
  onHold: "on_hold",
  reOpened: "re_opened",
  reOpenedHold: "re_opened_hold",
  pendingApproval: "pending_approval",
  completed: "completed",
  cancelled: "cancelled",
  cancelledByCandidate: "cancelled_by_candidate",
  expired: "expired",
  administered: "administered"
};

// if you update these codenames, don't forget to also update "...\backend\static\user_accommodation_file_candidate_limitations_const.py" file (backend)
export const USER_ACCOMMODATION_FILE_CANDIDATE_LIMITATIONS_CONST = {
  attentionDisorders: "attention_disorders",
  chronicMedicalDisabilities: "chronic_medical_disabilities",
  hearingDisabilities: "hearing_disabilities",
  learningDisabilities: "learning_disabilities",
  mobilityPhysicalDisabilities: "mobility_physical_disabilities",
  neurologicalDisorders: "neurological_disorders",
  psychiatricPsychologicalEmotionalDisabilities: "psychiatric_psychological_emotional_disabilities",
  speechAndLanguageDisabilities: "speech_and_language_disabilities",
  temporaryConditionsAndOther: "temporary_conditions_and_other",
  visualCondition: "visual_condition"
};

// if you update these codenames, don't forget to also update "...\backend\static\user_accommodation_file_recommendations_const.py" file (backend)
export const USER_ACCOMMODATION_FILE_RECOMMENDATIONS_CONST = {
  adaptedScoring: "adapted_scoring",
  adaptiveTechnology: "adaptive_technology",
  assessorOrTestAdministrator: "assessor_or_test_administrator",
  communicationSupport: "communication_support",
  environment: "environment",
  extraTime: "extra_time",
  formats: "formats",
  individualTestSession: "individual_test_session",
  material: "material",
  preparationToTest: "preparation_to_test",
  scheduling: "scheduling",
  sleUitLeveling: "sle_uit_leveling",
  breaks: "breaks",
  repetition: "repetition"
};

export default USER_ACCOMMODATION_FILE_STATUS_CONST;
