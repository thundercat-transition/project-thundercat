import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import ActiveSystemAlerts from "./ActiveSystemAlerts";
import ArchivedSystemAlerts from "./ArchivedSystemAlerts";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  }
};

class SystemAlerts extends Component {
  state = {
    selectedTab: ""
  };

  render() {
    const TABS = [
      {
        key: "active-system-alerts",
        tabName: LOCALIZE.systemAdministrator.systemAlerts.tabs.activeSystemAlerts.title,
        body: <ActiveSystemAlerts />
      },
      {
        key: "archived-system-alerts",
        tabName: LOCALIZE.systemAdministrator.systemAlerts.tabs.archivedSystemAlerts.title,
        body: <ArchivedSystemAlerts />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.systemAdministrator.systemAlerts.title}</h2>
          <p>{LOCALIZE.systemAdministrator.systemAlerts.description}</p>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey="active-system-alerts"
                id="system-alerts-tabs"
                style={styles.tabNavigation}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export { SystemAlerts as unconnectedSystemAlerts };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SystemAlerts);
