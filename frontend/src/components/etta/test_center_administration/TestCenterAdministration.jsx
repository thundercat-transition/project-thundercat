import React, { Component } from "react";
// import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import { COMMON_STYLE } from "../../commons/GenericTable";
import StyledTooltip, { EFFECT, TYPE } from "../../authentication/StyledTooltip";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars } from "@fortawesome/free-solid-svg-icons";
import { LANGUAGES } from "../../../modules/LocalizeRedux";
import { history } from "../../../store-index";
import { PATH } from "../../commons/Constants";
import TestCenters from "./TestCenters";
import ArchivedTestCenters from "./ArchivedTestCenters";
import {
  getAllTestCentersAsEtta,
  getFoundTestCentersAsEtta,
  getSelectedTestCenterDataAsEtta,
  setEttaTestCenterData,
  updateCurrentEttaTestCenterAdministrationPageState
} from "../../../modules/TestCenterRedux";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  allUnset: {
    all: "unset"
  }
};

class TestCenterAdministration extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {};

  state = {
    testCentersCurrentlyLoading: true,
    rowsDefinitionTestCenters: {},
    numberOfTestCentersPages: 1,
    testCentersResultsFound: 0
  };

  componentDidMount = () => {
    this.props.updateCurrentEttaTestCenterAdministrationPageState(1);
    this.getAllTestCentersBasedOnActiveSearch();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.getAllTestCentersBasedOnActiveSearch();
    }
    // if ettaTestCenterAdministrationPaginationPageSize get updated
    if (
      prevProps.ettaTestCenterAdministrationPaginationPageSize !==
      this.props.ettaTestCenterAdministrationPaginationPageSize
    ) {
      this.getAllTestCentersBasedOnActiveSearch();
    }
    // if testDefinitionAllTestsPaginationPage get updated
    if (
      prevProps.ettaTestCenterAdministrationPaginationPage !==
      this.props.ettaTestCenterAdministrationPaginationPage
    ) {
      this.getAllTestCentersBasedOnActiveSearch();
    }
    // if search ettaTestCenterAdministrationKeyword gets updated
    if (
      prevProps.ettaTestCenterAdministrationKeyword !==
      this.props.ettaTestCenterAdministrationKeyword
    ) {
      // if ettaTestCenterAdministrationKeyword redux state is empty
      if (
        this.props.ettaTestCenterAdministrationKeyword !== "" ||
        (this.props.ettaTestCenterAdministrationKeyword === "" &&
          this.props.ettaTestCenterAdministrationActiveSearch)
      ) {
        this.getAllTestCentersBasedOnActiveSearch();
      }
    }
    // if ettaTestCenterAdministrationActiveSearch gets updated
    if (
      prevProps.ettaTestCenterAdministrationActiveSearch !==
      this.props.ettaTestCenterAdministrationActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.ettaTestCenterAdministrationActiveSearch) {
        this.getAllTestCentersData();
        // there is a current active search (on search action)
      } else {
        this.getFoundTestCentersData();
      }
    }
    // if triggerTableUpdatesState gets updated
    if (prevProps.triggerTableUpdatesState !== this.props.triggerTableUpdatesState) {
      this.getAllTestCentersBasedOnActiveSearch();
    }
  };

  // get test centers data based on the ettaTestCenterAdministrationActiveSearch redux state
  getAllTestCentersBasedOnActiveSearch = () => {
    // current search
    if (this.props.ettaTestCenterAdministrationActiveSearch) {
      this.getFoundTestCentersData();
      // no current search
    } else {
      this.getAllTestCentersData();
    }
  };

  getAllTestCentersData = () => {
    this.setState({ testCentersCurrentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      this.props
        .getAllTestCentersAsEtta(
          this.props.ettaTestCenterAdministrationPaginationPage,
          this.props.ettaTestCenterAdministrationPaginationPageSize
        )
        .then(response => {
          for (let i = 0; i < response.results.length; i++) {
            // populating data object with provided data
            data.push({
              column_1: response.results[i].name,
              column_2: response.results[i].modify_date.split("T")[0],
              column_3:
                this.props.currentLanguage === LANGUAGES.english
                  ? `${response.results[i].dept_edesc} (${response.results[i].dept_eabrv})`
                  : `${response.results[i].dept_fdesc} (${response.results[i].dept_fabrv})`,
              column_4: this.populateTestCentersColumnFour(i, response)
            });
          }

          // updating rowsDefinition object with provided data and needed style
          rowsDefinition = {
            column_1_style: COMMON_STYLE.LEFT_TEXT,
            column_2_style: COMMON_STYLE.CENTERED_TEXT,
            column_3_style: COMMON_STYLE.CENTERED_TEXT,
            column_4_style: COMMON_STYLE.CENTERED_TEXT,
            data: data
          };
          // saving results in state
          this.setState({
            rowsDefinitionTestCenters: rowsDefinition,
            testCentersCurrentlyLoading: false,
            numberOfTestCentersPages: Math.ceil(
              parseInt(response.count) / this.props.ettaTestCenterAdministrationPaginationPageSize
            )
          });
        });
    });
  };

  getFoundTestCentersData = () => {
    this.setState({ testCentersCurrentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      setTimeout(() => {
        this.props
          .getFoundTestCentersAsEtta(
            this.props.ettaTestCenterAdministrationKeyword,
            this.props.currentLanguage,
            this.props.ettaTestCenterAdministrationPaginationPage,
            this.props.ettaTestCenterAdministrationPaginationPageSize
          )
          .then(response => {
            // there are no results found
            if (response[0] === "no results found") {
              this.setState(
                {
                  rowsDefinitionTestCenters: {},
                  numberOfTestCentersPages: 1,
                  testCentersResultsFound: 0
                },
                () => {
                  this.setState({ testCentersCurrentlyLoading: false }, () => {
                    // make sure that this element exsits to avoid any error
                    if (document.getElementById("test-centers-results-found")) {
                      document.getElementById("test-centers-results-found").focus();
                    }
                  });
                }
              );
              // there is at least one result found
            } else {
              for (let i = 0; i < response.results.length; i++) {
                // populating data object with provided data
                data.push({
                  column_1: response.results[i].name,
                  column_2: response.results[i].modify_date.split("T")[0],
                  column_3:
                    this.props.currentLanguage === LANGUAGES.english
                      ? `${response.results[i].dept_edesc} (${response.results[i].dept_eabrv})`
                      : `${response.results[i].dept_fdesc} (${response.results[i].dept_fabrv})`,
                  column_4: this.populateTestCentersColumnFour(i, response)
                });
              }

              // updating rowsDefinition object with provided data and needed style
              rowsDefinition = {
                column_1_style: COMMON_STYLE.LEFT_TEXT,
                column_2_style: COMMON_STYLE.CENTERED_TEXT,
                column_3_style: COMMON_STYLE.CENTERED_TEXT,
                column_4_style: COMMON_STYLE.CENTERED_TEXT,
                data: data
              };
              // saving results in state
              this.setState(
                {
                  rowsDefinitionTestCenters: rowsDefinition,
                  testCentersCurrentlyLoading: false,
                  numberOfTestCentersPages: Math.ceil(
                    parseInt(response.count) /
                      this.props.ettaTestCenterAdministrationPaginationPageSize
                  ),
                  testCentersResultsFound: response.count
                },
                () => {
                  // make sure that this element exsits to avoid any error
                  if (document.getElementById("test-centers-results-found")) {
                    document.getElementById("test-centers-results-found").focus();
                  }
                }
              );
            }
          });
      }, 100);
    });
  };

  populateTestCentersColumnFour = (i, response) => {
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`test-centers-view-button-${i}`}
          place="top"
          variant={TYPE.light}
          effect={EFFECT.solid}
          openOnClick={false}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`test-centers-view-button-${i}`}
                label={<FontAwesomeIcon icon={faBinoculars} />}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters.table
                    .viewTestCenterAriaLabel,
                  response.results[i].name
                )}
                action={() => {
                  this.viewSelectedTestCenter(response.results[i].id);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters.table
                    .viewButtonTooltip
                }
              </p>
            </div>
          }
        />
      </div>
    );
  };

  viewSelectedTestCenter = currentSelectedTestCenterId => {
    // getting selected test center data
    this.props
      .getSelectedTestCenterDataAsEtta(currentSelectedTestCenterId)
      .then(response => {
        this.props.setEttaTestCenterData(response);
      })
      .then(() => {
        // redirecting user to the test center accesses page
        history.push(PATH.testCenterAccesses);
      });
  };

  render() {
    const TABS = [
      {
        key: "test-centers",
        tabName: LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters.title,
        body: (
          <TestCenters
            currentlyLoading={this.state.testCentersCurrentlyLoading}
            rowsDefinition={this.state.rowsDefinitionTestCenters}
            resultsFound={this.state.testCentersResultsFound}
            numberOfTestCentersPages={this.state.numberOfTestCentersPages}
          />
        )
      },
      {
        key: "archived-test-centers",
        tabName:
          LOCALIZE.systemAdministrator.testCenterAdministration.tabs.archivedTestCenters.title,
        body: <ArchivedTestCenters />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.systemAdministrator.testCenterAdministration.title}</h2>
          <p>{LOCALIZE.systemAdministrator.testCenterAdministration.description}</p>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey="test-centers"
                id="test-centers-tabs"
                style={styles.tabNavigation}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    ettaTestCenterAdministrationPaginationPage:
      state.testCenter.ettaTestCenterAdministrationPaginationPage,
    ettaTestCenterAdministrationPaginationPageSize:
      state.testCenter.ettaTestCenterAdministrationPaginationPageSize,
    ettaTestCenterAdministrationKeyword: state.testCenter.ettaTestCenterAdministrationKeyword,
    ettaTestCenterAdministrationActiveSearch:
      state.testCenter.ettaTestCenterAdministrationActiveSearch,
    triggerTableUpdatesState: state.testCenter.triggerTableUpdatesState
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateCurrentEttaTestCenterAdministrationPageState,
      getAllTestCentersAsEtta,
      getFoundTestCentersAsEtta,
      getSelectedTestCenterDataAsEtta,
      setEttaTestCenterData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestCenterAdministration);
