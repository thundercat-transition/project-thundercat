import {
  faCaretLeft,
  faCaretRight,
  faPlusCircle,
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import {
  triggerTableUpdates,
  updateCurrentEttaTestCenterAdministrationPageState,
  updateEttaTestCenterAdministrationPageSizeState,
  updateSearchEttaTestCenterAdministrationStates,
  createNewTestCenterAsEtta
} from "../../../modules/TestCenterRedux";
import ReactPaginate from "react-paginate";
import { Col, Row } from "react-bootstrap";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import { LANGUAGES } from "../../commons/Translation";
import DropdownSelect from "../../commons/DropdownSelect";
import { getOrganization } from "../../../modules/ExtendedProfileOptionsRedux";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 7,
    xl: 7
  }
};

const styles = {
  mainContainer: {
    marginTop: 24
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  popupContainer: {
    padding: "10px 15px"
  },
  description: {
    marginBottom: 12
  },
  itemContainer: {
    width: "100%",
    margin: "12px 0"
  },
  labelContainer: {
    verticalAlign: "middle"
  },
  label: {
    paddingRight: 12,
    margin: 0
  },
  inputContainer: {
    verticalAlign: "middle"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  paginationContainer: {
    display: "flex"
  },
  paginationIcon: {
    padding: "0 6px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  }
};

class TestCenters extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    currentlyLoading: PropTypes.bool.isRequired,
    rowsDefinition: PropTypes.object.isRequired,
    resultsFound: PropTypes.number.isRequired,
    numberOfTestCentersPages: PropTypes.number.isRequired
  };

  state = {
    rowsDefinition: {},
    currentlyLoading: true,
    resultsFound: this.props.resultsFound,
    numberOfTestCentersPages: this.props.numberOfTestCentersPages,
    showNewTestCenterPopup: false,
    newTestCenterCreatedSuccesssfully: false,
    isValidNewTestCenterForm: false,
    organisationsOptions: [],
    newTestCenterSelectedOrganisationOption: [],
    newTestCenterName: ""
  };

  componentDidMount = () => {
    // populating organisation options
    this.populateOrganisationsOptions();
  };

  componentDidUpdate = prevProps => {
    // if rowsDefinition gets updated
    if (prevProps.rowsDefinition !== this.props.rowsDefinition) {
      this.setState({ rowsDefinition: this.props.rowsDefinition });
    }
    // if currentlyLoading gets updated
    if (prevProps.currentlyLoading !== this.props.currentlyLoading) {
      this.setState({ currentlyLoading: this.props.currentlyLoading });
    }
    // if resultsFound gets updated
    if (prevProps.resultsFound !== this.props.resultsFound) {
      this.setState({ resultsFound: this.props.resultsFound });
    }
    // if numberOfTestCentersPages gets updated
    if (prevProps.numberOfTestCentersPages !== this.props.numberOfTestCentersPages) {
      this.setState({ numberOfTestCentersPages: this.props.numberOfTestCentersPages });
    }
  };

  handlePageChange = id => {
    // "+1" because redux ettaTestCenterAdministrationPaginationPage uses index 1 and react-paginate (id) uses index 0
    const selectedPage = id.selected + 1;
    // update current page redux state
    this.props.updateCurrentEttaTestCenterAdministrationPageState(selectedPage);
    // focusing on the first table's row (make sure that the row exists before trying to focus to avoid errors)
    if (document.getElementById("test-centers-tbl-table-row-0")) {
      document.getElementById("test-centers-tbl-table-row-0").focus();
    }
  };

  populateOrganisationsOptions = () => {
    const organisationsOptions = [];
    this.props.getOrganization().then(response => {
      for (let i = 0; i < response.body.length; i++) {
        // Interface is in English
        if (this.props.currentLanguage === LANGUAGES.english) {
          organisationsOptions.push({
            label: `${response.body[i].edesc} (${response.body[i].eabrv})`,
            value: `${response.body[i].dept_id}`
          });
          // Interface is in French
        } else {
          organisationsOptions.push({
            label: `${response.body[i].fdesc} (${response.body[i].fabrv})`,
            value: `${response.body[i].dept_id}`
          });
        }
      }
      this.setState({ organisationsOptions: organisationsOptions });
    });
  };

  openNewTestCenterPopup = () => {
    this.setState({ showNewTestCenterPopup: true });
  };

  resetFormStates = () => {
    this.setState({
      showNewTestCenterPopup: false,
      newTestCenterCreatedSuccesssfully: false,
      isValidNewTestCenterForm: false,
      newTestCenterSelectedOrganisationOption: [],
      newTestCenterName: ""
    });
  };

  handleCreateNewTestCenter = () => {
    const body = {
      department_id: this.state.newTestCenterSelectedOrganisationOption.value,
      name: this.state.newTestCenterName
    };

    this.props.createNewTestCenterAsEtta(body).then(response => {
      // success
      if (response.ok) {
        // show successful popup
        this.setState({ newTestCenterCreatedSuccesssfully: true });
        // trigger table updates
        this.props.triggerTableUpdates();
        // should never happen
      } else {
        throw new Error("An error occurred during the create new test center process");
      }
    });
  };

  getSelectedOrganisationOption = option => {
    this.setState(
      {
        newTestCenterSelectedOrganisationOption: option
      },
      () => {
        this.validateNewTestCenterForm();
      }
    );
  };

  updateTestCenterNameContent = event => {
    const newTestCenterName = event.target.value;
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(newTestCenterName)) {
      this.setState({ newTestCenterName: newTestCenterName }, () => {
        this.validateNewTestCenterForm();
      });
    }
  };

  validateNewTestCenterForm = () => {
    // initializing needed variables
    let isValidForm = false;
    const isValidDepartment =
      typeof this.state.newTestCenterSelectedOrganisationOption.value !== "undefined";
    const isValidName = this.state.newTestCenterName !== "";

    // everything is valid
    if (isValidDepartment && isValidName) {
      isValidForm = true;
    }

    this.setState({ isValidNewTestCenterForm: isValidForm });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters.table.column1,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters.table.column2,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters.table.column3,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters.table.column4,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <CustomButton
          label={
            <>
              <FontAwesomeIcon icon={faPlusCircle} />
              <span id="new-test-center-button" style={styles.buttonLabel}>
                {
                  LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                    .newTestCenterButton
                }
              </span>
            </>
          }
          action={() => this.openNewTestCenterPopup()}
          buttonTheme={THEME.PRIMARY}
        />
        <SearchBarWithDisplayOptions
          idPrefix={"test-centers"}
          currentlyLoading={this.state.currentlyLoading}
          updateSearchStates={this.props.updateSearchEttaTestCenterAdministrationStates}
          updatePageState={this.props.updateCurrentEttaTestCenterAdministrationPageState}
          paginationPageSize={this.props.ettaTestCenterAdministrationPaginationPageSize}
          updatePaginationPageSize={this.props.updateEttaTestCenterAdministrationPageSizeState}
          data={this.state.rowsDefinition.results || []}
          resultsFound={this.state.resultsFound}
          searchBarContent={this.props.ettaTestCenterAdministrationKeyword}
        />
        <GenericTable
          classnamePrefix="test-centers-tbl"
          columnsDefinition={columnsDefinition}
          rowsDefinition={this.state.rowsDefinition}
          emptyTableMessage={
            LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters.table.noData
          }
          currentlyLoading={this.state.currentlyLoading}
        />
        <div style={styles.paginationContainer}>
          <ReactPaginate
            pageCount={this.state.numberOfTestCentersPages}
            containerClassName={"pagination"}
            breakClassName={"break"}
            activeClassName={"active-page"}
            marginPagesDisplayed={3}
            // "-1" because react-paginate uses index 0 and ettaTestCenterAdministrationPaginationPage redux state uses index 1
            forcePage={this.props.ettaTestCenterAdministrationPaginationPage - 1}
            onPageChange={page => {
              this.handlePageChange(page);
            }}
            previousLabel={
              <div style={styles.paginationIcon}>
                <label style={styles.hiddenText}>
                  {LOCALIZE.commons.pagination.previousPageButton}
                </label>
                <FontAwesomeIcon icon={faCaretLeft} />
              </div>
            }
            nextLabel={
              <div style={styles.paginationIcon}>
                <label style={styles.hiddenText}>
                  {LOCALIZE.commons.pagination.nextPageButton}
                </label>
                <FontAwesomeIcon icon={faCaretRight} />
              </div>
            }
          />
        </div>
        <PopupBox
          show={this.state.showNewTestCenterPopup}
          handleClose={() => {}}
          shouldCloseOnEsc={false}
          displayCloseButton={false}
          isBackdropStatic={false}
          size={this.state.newTestCenterCreatedSuccesssfully ? "lg" : "xl"}
          title={
            LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
              .newTestCenterPopup.title
          }
          description={
            <div style={styles.popupContainer}>
              {this.state.newTestCenterCreatedSuccesssfully ? (
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.success}
                    title={LOCALIZE.commons.success}
                    message={
                      <p>
                        {
                          LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                            .newTestCenterPopup.successMessage
                        }
                      </p>
                    }
                  />
                </div>
              ) : (
                <div>
                  <p style={styles.description}>
                    {
                      LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                        .newTestCenterPopup.description
                    }
                  </p>
                  <Row
                    className="align-items-center justify-content-start"
                    style={styles.itemContainer}
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                      style={styles.labelContainer}
                    >
                      <label style={styles.label}>
                        <label id="test-center-name-title" htmlFor="test-center-name">
                          {
                            LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                              .newTestCenterPopup.testCenterName
                          }
                        </label>
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                      style={styles.inputContainer}
                    >
                      <input
                        id="test-center-name"
                        className={"valid-field"}
                        aria-labelledby="test-center-name-title"
                        aria-required={true}
                        style={{ ...styles.input, ...accommodationsStyle }}
                        type="text"
                        value={this.state.newTestCenterName}
                        onChange={this.updateTestCenterNameContent}
                      ></input>
                    </Col>
                  </Row>
                  <Row
                    className="align-items-center justify-content-start"
                    style={styles.itemContainer}
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                      style={styles.labelContainer}
                    >
                      <label style={styles.label}>
                        <label id="test-center-department-title">
                          {
                            LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                              .newTestCenterPopup.testCenterDepartment
                          }
                        </label>
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                      style={styles.inputContainer}
                    >
                      <DropdownSelect
                        idPrefix="test-center-department"
                        isValid={true}
                        ariaRequired={true}
                        ariaLabelledBy="test-center-department-title"
                        hasPlaceholder={true}
                        options={this.state.organisationsOptions}
                        onChange={this.getSelectedOrganisationOption}
                        defaultValue={this.state.newTestCenterSelectedOrganisationOption}
                        isMulti={false}
                      />
                    </Col>
                  </Row>
                </div>
              )}
            </div>
          }
          leftButtonType={
            !this.state.newTestCenterCreatedSuccesssfully ? BUTTON_TYPE.secondary : BUTTON_TYPE.none
          }
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.resetFormStates}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={!this.state.newTestCenterCreatedSuccesssfully ? faPlusCircle : ""}
          rightButtonTitle={
            !this.state.newTestCenterCreatedSuccesssfully
              ? LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                  .newTestCenterPopup.rightButton
              : LOCALIZE.commons.ok
          }
          rightButtonLabel={
            !this.state.newTestCenterCreatedSuccesssfully
              ? LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                  .newTestCenterPopup.rightButton
              : LOCALIZE.commons.ok
          }
          rightButtonAction={
            !this.state.newTestCenterCreatedSuccesssfully
              ? this.handleCreateNewTestCenter
              : this.resetFormStates
          }
          rightButtonState={
            this.state.isValidNewTestCenterForm ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    ettaTestCenterAdministrationPaginationPage:
      state.testCenter.ettaTestCenterAdministrationPaginationPage,
    ettaTestCenterAdministrationPaginationPageSize:
      state.testCenter.ettaTestCenterAdministrationPaginationPageSize,
    ettaTestCenterAdministrationKeyword: state.testCenter.ettaTestCenterAdministrationKeyword,
    ettaTestCenterAettaTestCenterAdministrationActiveSearchdministrationKeyword:
      state.testCenter.ettaTestCenterAettaTestCenterAdministrationActiveSearchdministrationKeyword
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createNewTestCenterAsEtta,
      getOrganization,
      triggerTableUpdates,
      updateCurrentEttaTestCenterAdministrationPageState,
      updateEttaTestCenterAdministrationPageSizeState,
      updateSearchEttaTestCenterAdministrationStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestCenters);
