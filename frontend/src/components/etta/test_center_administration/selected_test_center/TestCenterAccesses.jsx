/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../../text_resources";
import { bindActionCreators } from "redux";
import { Col, Row } from "react-bootstrap";
import { getLineSpacingCSS } from "../../../../modules/AccommodationsRedux";
import CustomButton from "../../../commons/CustomButton";
import THEME from "../../../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCheck,
  faPlusCircle,
  faSpinner,
  faTimes,
  faTrashAlt
} from "@fortawesome/free-solid-svg-icons";
import { LANGUAGES } from "../../../commons/Translation";
import DropdownSelect from "../../../commons/DropdownSelect";
import { getOrganization } from "../../../../modules/ExtendedProfileOptionsRedux";
import { LANGUAGE_IDS } from "../../../../modules/LocalizeRedux";
import GenericTable, { COMMON_STYLE } from "../../../commons/GenericTable";
import StyledTooltip, { EFFECT, TYPE } from "../../../authentication/StyledTooltip";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../../commons/PopupBox";
import { getUsersBasedOnSpecifiedPermission } from "../../../../modules/PermissionsRedux";
import SystemMessage, { MESSAGE_TYPE } from "../../../commons/SystemMessage";
import { PERMISSION } from "../../../profile/Constants";
import {
  addTestCenterManager,
  deleteTestCenterManager,
  getSelectedTestCenterDataAsEtta,
  setEttaTestCenterData,
  triggerRerender,
  updateTestCenterDataAsEtta
} from "../../../../modules/TestCenterRedux";
import Switch from "react-switch";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 5,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 7,
    lg: 7,
    xl: 7
  }
};

const styles = {
  mainContainer: {
    width: "100%"
  },
  formContainer: {
    marginTop: 24
  },
  itemContainer: {
    width: "100%",
    margin: "12px 0"
  },
  labelContainer: {
    verticalAlign: "middle"
  },
  label: {
    paddingRight: 12,
    margin: 0
  },
  inputContainer: {
    verticalAlign: "middle"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  applyButtonContainer: {
    textAlign: "center",
    marginTop: 36
  },
  applyButton: {
    minWidth: 200
  },
  buttonIcon: {
    marginRight: 6
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  allUnset: {
    all: "unset"
  },
  addTDButtonContainer: {
    margin: "36px 0 24px 0"
  },
  loadingContainer: {
    textAlign: "center"
  },
  bold: {
    fontWeight: "bold"
  },
  customLoadingContainer: {
    display: "grid",
    gridTemplateColumns: "1fr"
  },
  loadingOverlappingStyle: {
    gridRowStart: "1",
    gridColumnStart: "1"
  },
  transparentText: {
    color: "transparent"
  },
  icon: {
    paddingRight: 6,
    verticalAlign: "middle"
  },
  loading: {
    minHeight: 38,
    padding: 6
  }
};

class TestCenterAccesses extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {};

  state = {
    dataHasBeenModified: false,
    isValidForm: true,
    isLoading: false,
    testCenterName: { content: this.props.testCenterDataEtta.name },
    initialTestCenterName: this.props.testCenterDataEtta.name,
    organisationsOptions: [],
    testCenterSelectedOrganisationOption: {
      label:
        this.props.currentLanguage === LANGUAGES.english
          ? `${this.props.testCenterDataEtta.dept_edesc} (${this.props.testCenterDataEtta.dept_eabrv})`
          : `${this.props.testCenterDataEtta.dept_fdesc} (${this.props.testCenterDataEtta.dept_fabrv})`,
      value: `${this.props.testCenterDataEtta.dept_id}`
    },
    testCenterInitialSelectedOrganisationOption: {
      label:
        this.props.currentLanguage === LANGUAGES.english
          ? `${this.props.testCenterDataEtta.dept_edesc} (${this.props.testCenterDataEtta.dept_eabrv})`
          : `${this.props.testCenterDataEtta.dept_fdesc} (${this.props.testCenterDataEtta.dept_fabrv})`,
      value: `${this.props.testCenterDataEtta.dept_id}`
    },
    rowsDefinition: {},
    rowsCurrentlyLoading: true,
    showAddTestCenterManagerPopup: false,
    testCenterManagerOptions: [],
    isLoadingTestCenterManagerOptions: true,
    isLoadingAddTestCenterManager: false,
    selectedTestCenterManagerToAdd: [],
    showDeleteTestCenterManagerPopup: false,
    isLoadingDeleteTestCenterManager: false,
    selectedTestCenterManagerToDelete: {},
    isLoadingUpdateTestCenterAccess: false,
    triggerRerender: false,
    isOlaAuthorized: this.props.testCenterDataEtta.ola_authorized
  };

  componentDidMount = () => {
    // populating organisation options
    this.populateOrganisationsOptions();
    // populating rows
    this.populateRows();
  };

  populateOrganisationsOptions = () => {
    const organisationsOptions = [];
    this.props.getOrganization().then(response => {
      for (let i = 0; i < response.body.length; i++) {
        // Interface is in English
        if (this.props.currentLanguage === LANGUAGES.english) {
          organisationsOptions.push({
            label: `${response.body[i].edesc} (${response.body[i].eabrv})`,
            value: `${response.body[i].dept_id}`
          });
          // Interface is in French
        } else {
          organisationsOptions.push({
            label: `${response.body[i].fdesc} (${response.body[i].fabrv})`,
            value: `${response.body[i].dept_id}`
          });
        }
      }
      this.setState({ organisationsOptions: organisationsOptions });
    });
  };

  populateRows = (triggerLoading = true, index = null) => {
    this.setState({ rowsCurrentlyLoading: triggerLoading }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      for (let i = 0; i < this.props.testCenterDataEtta.test_center_accesses.length; i++) {
        // triggerLoading is set to false
        if (!triggerLoading && index !== null) {
          // i matched the provided index
          if (i === index) {
            // populating data object with provided data (loading is triggered for current iteration)
            data.push({
              user_id: this.props.testCenterDataEtta.test_center_accesses[i].user_data[0].id,
              column_1: `${this.props.testCenterDataEtta.test_center_accesses[i].user_data[0].last_name}, ${this.props.testCenterDataEtta.test_center_accesses[i].user_data[0].first_name} (${this.props.testCenterDataEtta.test_center_accesses[i].user_data[0].username})`,
              column_2: this.populateTestCenterAccessesColumnTwo(
                i,
                this.props.testCenterDataEtta.test_center_accesses
              )
            });
          } else {
            // populating data object with provided data (with disabled dropdown)
            data.push({
              user_id: this.props.testCenterDataEtta.test_center_accesses[i].user_data[0].id,
              column_1: `${this.props.testCenterDataEtta.test_center_accesses[i].user_data[0].last_name}, ${this.props.testCenterDataEtta.test_center_accesses[i].user_data[0].first_name} (${this.props.testCenterDataEtta.test_center_accesses[i].user_data[0].email})`,
              column_2: this.populateTestCenterAccessesColumnTwo(
                i,
                this.props.testCenterDataEtta.test_center_accesses
              )
            });
          }
        } else {
          // populating data object with provided data
          data.push({
            user_id: this.props.testCenterDataEtta.test_center_accesses[i].user_data[0].id,
            column_1: `${this.props.testCenterDataEtta.test_center_accesses[i].user_data[0].last_name}, ${this.props.testCenterDataEtta.test_center_accesses[i].user_data[0].first_name} (${this.props.testCenterDataEtta.test_center_accesses[i].user_data[0].email})`,
            column_2: this.populateTestCenterAccessesColumnTwo(
              i,
              this.props.testCenterDataEtta.test_center_accesses
            )
          });
        }
      }

      // updating rowsDefinition object with provided data and needed style
      rowsDefinition = {
        column_1_style: COMMON_STYLE.LEFT_TEXT,
        column_2_style: COMMON_STYLE.CENTERED_TEXT,
        data: data
      };
      // saving results in state
      this.setState(
        {
          rowsDefinition: rowsDefinition,
          rowsCurrentlyLoading: false
        },
        () => {
          // populating test center manager options
          this.populateTestCenterManagerOptions();
        }
      );
    });
  };

  populateTestCenterAccessesColumnTwo = (i, test_center_accesses) => {
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`test-center-access-view-button-${i}`}
          place="top"
          variant={TYPE.light}
          effect={EFFECT.solid}
          openOnClick={false}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`test-center-access-view-button-${i}`}
                label={<FontAwesomeIcon icon={faTrashAlt} />}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                    .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems
                    .testCenterAccesses.table.deleteButtonAriaLabel,
                  test_center_accesses[i].user_data[0].username
                )}
                action={() => {
                  this.openDeleteTestCenterManagerPopup(test_center_accesses[i]);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                    .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems
                    .testCenterAccesses.table.deleteButtonTooltip
                }
              </p>
            </div>
          }
        />
      </div>
    );
  };

  populateTestCenterManagerOptions = () => {
    this.setState({ isLoadingTestCenterManagerOptions: true }, () => {
      // getting all existing TAs and removing the ones that exist in the table from the options
      const tcmInTable = [];
      // if there is data in the table
      if (this.state.rowsDefinition.data.length > 0) {
        for (let i = 0; i < this.state.rowsDefinition.data.length; i++) {
          // adding test center manager username (email) to tcmInTable
          tcmInTable.push(this.state.rowsDefinition.data[i].user_id);
        }
      }
      const testCenterManagerOptions = [];
      this.props.getUsersBasedOnSpecifiedPermission(PERMISSION.testCenterManager).then(response => {
        for (let i = 0; i < response.length; i++) {
          if (!tcmInTable.includes(response[i].user)) {
            testCenterManagerOptions.push({
              label: `${response[i].last_name}, ${response[i].first_name} (${response[i].email})`,
              value: response[i].user_id
            });
          }
        }
      });
      this.setState({
        testCenterManagerOptions: testCenterManagerOptions,
        isLoadingTestCenterManagerOptions: false
      });
    });
  };

  openDeleteTestCenterManagerPopup = currentSelectedTestCenterManager => {
    this.setState({
      selectedTestCenterManagerToDelete: currentSelectedTestCenterManager,
      showDeleteTestCenterManagerPopup: true
    });
  };

  closeDeleteTestCenterManagerPopup = () => {
    this.setState({
      selectedTestCenterManagerToDelete: {},
      showDeleteTestCenterManagerPopup: false
    });
  };

  handleDeleteTestCenterManager = () => {
    this.setState({ isLoadingDeleteTestCenterManager: true }, () => {
      this.props
        .deleteTestCenterManager(
          this.state.selectedTestCenterManagerToDelete.user,
          this.props.testCenterDataEtta.id
        )
        .then(response => {
          if (response.ok) {
            // getting selected test center data
            this.props
              .getSelectedTestCenterDataAsEtta(this.props.testCenterDataEtta.id)
              .then(response => {
                // making sure that the test center data redux state is up to date
                this.props.setEttaTestCenterData(response);
                // closing add test center manager popup and re-poplating rows
                this.setState(
                  {
                    showDeleteTestCenterManagerPopup: false,
                    selectedTestCenterManagerToDelete: {}
                  },
                  () => {
                    this.populateRows();
                    setTimeout(() => {
                      this.setState({ isLoadingDeleteTestCenterManager: false });
                    }, 250);
                  }
                );
              });
            // should never happen
          } else {
            throw new Error("An error occurred during the delete test center manager process");
          }
        });
    });
  };

  getCurrentLanguage = languageId => {
    switch (languageId) {
      case LANGUAGE_IDS.english:
        return {
          label: LOCALIZE.commons.english,
          value: LANGUAGE_IDS.english
        };
      case LANGUAGE_IDS.french:
        return {
          label: LOCALIZE.commons.french,
          value: LANGUAGE_IDS.french
        };
      default:
        return {
          label: LOCALIZE.commons.bilingual,
          value: null
        };
    }
  };

  updateTestCenterNameContent = event => {
    const testCenterName = event.target.value;
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(testCenterName)) {
      this.setState({ testCenterName: { content: testCenterName } }, () => {
        // checking if all the data is the same as in the DB
        this.checkForChangesInForm();
        // validating form
        this.validateForm();
      });
    }
  };

  getSelectedOrganisationOption = option => {
    this.setState(
      {
        testCenterSelectedOrganisationOption: option
      },
      () => {
        // checking if all the data is the same as in the DB
        this.checkForChangesInForm();
        // validating form
        this.validateForm();
      }
    );
  };

  checkForChangesInForm = () => {
    // initializing needed variables
    let dataHasBeenModified = false;
    let testCenterNameModified = false;
    let testCenterDepartmentModified = false;
    let testCenterOlaAuthorizedModified = false;

    // checking if testCenterName has been modified
    if (this.state.testCenterName.content !== this.props.testCenterDataEtta.name) {
      testCenterNameModified = true;
    }

    // checking if testCenterDepartment has been modified
    if (
      this.state.testCenterSelectedOrganisationOption.value.toString() !==
      this.props.testCenterDataEtta.dept_id.toString()
    ) {
      testCenterDepartmentModified = true;
    }

    // checking if testCenterName has been modified
    if (this.state.isOlaAuthorized !== this.props.testCenterDataEtta.ola_authorized) {
      testCenterOlaAuthorizedModified = true;
    }

    // if any data has been modified
    if (testCenterNameModified || testCenterDepartmentModified || testCenterOlaAuthorizedModified) {
      dataHasBeenModified = true;
    }

    this.setState({ dataHasBeenModified: dataHasBeenModified });
  };

  validateForm = () => {
    // initializing needed variables
    let isValidForm = false;

    // making sure that all mandatory fields are filled
    const isValidTestCenterName = this.state.testCenterName.content !== "";

    // if all mandatory fields are valid
    if (isValidTestCenterName) {
      isValidForm = true;
    }

    this.setState({ isValidForm: isValidForm });
  };

  handleApplyChanges = () => {
    this.setState({ isLoading: true }, () => {
      // updating testCenterDataEtta redux states
      const { testCenterDataEtta } = this.props;
      testCenterDataEtta.name = this.state.testCenterName.content;
      testCenterDataEtta.department_id = this.state.testCenterSelectedOrganisationOption.value;
      testCenterDataEtta.dept_id = this.state.testCenterSelectedOrganisationOption.value;
      testCenterDataEtta.ola_authorized = this.state.isOlaAuthorized;
      this.props.setEttaTestCenterData(testCenterDataEtta);
      // adding small delay to make sure that the props are properly set before updating the DB
      setTimeout(() => {
        // updating data in DB
        const body = this.props.testCenterDataEtta;
        this.props.updateTestCenterDataAsEtta(body).then(response => {
          if (response.ok) {
            // setting needed states
            this.checkForChangesInForm();
            this.setState({
              initialTestCenterName: this.props.testCenterDataEtta.name,
              testCenterInitialSelectedOrganisationOption: {
                label:
                  this.props.currentLanguage === LANGUAGES.english
                    ? `${this.props.testCenterDataEtta.dept_edesc} (${this.props.testCenterDataEtta.dept_eabrv})`
                    : `${this.props.testCenterDataEtta.dept_fdesc} (${this.props.testCenterDataEtta.dept_fabrv})`,
                value: this.props.testCenterDataEtta.dept_id
              }
            });
            // getting selected test center data
            this.props
              .getSelectedTestCenterDataAsEtta(this.props.testCenterDataEtta.id)
              .then(response => {
                // making sure that the test center data redux state is up to date
                this.props.setEttaTestCenterData(response);
                this.props.triggerRerender();
                this.setState({ isLoading: false });
              });
            // should never happen
          } else {
            throw new Error("An error occurred during the update test center data process");
          }
        });
      }, 250);
    });
  };

  getSelectedTestCenterManager = option => {
    this.setState({
      selectedTestCenterManagerToAdd: option
    });
  };

  openAddTestCenterManagerPopup = () => {
    this.setState({ showAddTestCenterManagerPopup: true });
  };

  closeAddTestCenterManagerPopup = () => {
    this.setState({ showAddTestCenterManagerPopup: false, selectedTestCenterManagerToAdd: [] });
  };

  handleAddTestCenterManager = () => {
    this.setState({ isLoadingAddTestCenterManager: true }, () => {
      this.props
        .addTestCenterManager(
          this.state.selectedTestCenterManagerToAdd.value,
          this.props.testCenterDataEtta.id
        )
        .then(response => {
          if (response.ok) {
            // getting selected test center data
            this.props
              .getSelectedTestCenterDataAsEtta(this.props.testCenterDataEtta.id)
              .then(response => {
                // making sure that the test center data redux state is up to date
                this.props.setEttaTestCenterData(response);
                // closing add test center manager popup and re-poplating rows
                this.setState(
                  { showAddTestCenterManagerPopup: false, selectedTestCenterManagerToAdd: [] },
                  () => {
                    this.populateRows();
                    setTimeout(() => {
                      this.setState({ isLoadingAddTestCenterManager: false });
                    }, 250);
                  }
                );
              });

            // should never happen
          } else {
            throw new Error("An error occurred during the add test center manager process");
          }
        });
    });
  };

  handleSwitchStateUpdate = event => {
    this.setState(
      {
        isOlaAuthorized: event
      },
      () => {
        // Enable save button
        this.checkForChangesInForm();
      }
    );
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters.selectedTestCenter
            .tabs.selectedTestCenter.sideNavigationItems.testCenterAccesses.table.column1,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters.selectedTestCenter
            .tabs.selectedTestCenter.sideNavigationItems.testCenterAccesses.table.column2,
        style: { width: "15%", ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    const currentTestCenterNameContent = {
      ...{ content: this.props.testCenterDataEtta.name },
      ...this.state.testCenterName
    };

    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>
            {LOCALIZE.formatString(
              LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems.testCenterAccesses
                .title,
              this.props.testCenterDataEtta.name
            )}
          </h2>
          <p>
            {
              LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems.testCenterAccesses
                .description
            }
          </p>
        </div>
        <div style={styles.formContainer}>
          <Row className="align-items-center justify-content-start" style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label style={styles.label}>
                <label id="test-center-name-title" htmlFor="test-center-name-input">
                  {
                    LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                      .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems
                      .testCenterAccesses.testCenterName
                  }
                </label>
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <input
                id="test-center-name-input"
                className={"valid-field"}
                aria-required={true}
                style={{ ...styles.input, ...accommodationsStyle }}
                type="text"
                value={currentTestCenterNameContent.content}
                onChange={this.updateTestCenterNameContent}
              ></input>
            </Col>
          </Row>
          <Row className="align-items-center justify-content-start" style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label style={styles.label}>
                <label id="test-center-department-title">
                  {
                    LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                      .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems
                      .testCenterAccesses.testCenterDepartment
                  }
                </label>
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <DropdownSelect
                idPrefix="test-center-department"
                isValid={true}
                ariaRequired={true}
                ariaLabelledBy="test-center-department-title"
                hasPlaceholder={true}
                options={this.state.organisationsOptions}
                onChange={this.getSelectedOrganisationOption}
                defaultValue={this.state.testCenterSelectedOrganisationOption}
                isMulti={false}
              />
            </Col>
          </Row>
          <Row className="align-items-center justify-content-start" style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label style={styles.label}>
                <label id="test-center-ola-authorized-title">
                  {
                    LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                      .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems
                      .testCenterAccesses.testCenterOlaAuthorized
                  }
                </label>
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <Switch
                onChange={e => {
                  this.handleSwitchStateUpdate(e);
                }}
                checked={this.state.isOlaAuthorized}
                aria-labelledby="test-center-ola-authorized-title"
                height={this.state.switchHeight}
                width={this.state.switchWidth}
              />
            </Col>
          </Row>
        </div>
        <div style={styles.applyButtonContainer}>
          <CustomButton
            label={
              <>
                {this.state.isLoading ? (
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} />
                  </label>
                ) : (
                  <>
                    <FontAwesomeIcon icon={faCheck} style={styles.buttonIcon} />
                    <span>
                      {this.state.dataHasBeenModified
                        ? LOCALIZE.commons.applyButton
                        : LOCALIZE.commons.saved}
                    </span>
                  </>
                )}
              </>
            }
            action={this.handleApplyChanges}
            customStyle={styles.applyButton}
            buttonTheme={this.state.dataHasBeenModified ? THEME.PRIMARY : THEME.SUCCESS}
            disabled={
              !this.state.isValidForm || !this.state.dataHasBeenModified || this.state.isLoading
            }
          />
        </div>
        <div>
          <div style={styles.addTDButtonContainer}>
            <CustomButton
              label={
                <>
                  <FontAwesomeIcon icon={faPlusCircle} />
                  <span id="new-test-center-test-center-manager" style={styles.buttonLabel}>
                    {
                      LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                        .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems
                        .testCenterAccesses.addTestCenterManagerButton
                    }
                  </span>
                </>
              }
              action={() => this.openAddTestCenterManagerPopup()}
              buttonTheme={THEME.PRIMARY}
            />
          </div>
          <div>
            <GenericTable
              classnamePrefix="test-center-accesses"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                  .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems.testCenterAccesses
                  .table.noData
              }
              currentlyLoading={this.state.rowsCurrentlyLoading}
            />
          </div>
        </div>
        <PopupBox
          show={this.state.showAddTestCenterManagerPopup}
          handleClose={() => {}}
          shouldCloseOnEsc={false}
          displayCloseButton={false}
          isBackdropStatic={true}
          overflowVisible={true}
          title={LOCALIZE.formatString(
            LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
              .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems.testCenterAccesses
              .addTestCenterManagerPopup.title,
            this.props.testCenterDataEtta.name
          )}
          description={
            <div style={styles.popupContainer}>
              <div>
                <p style={styles.description}>
                  {
                    LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                      .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems
                      .testCenterAccesses.addTestCenterManagerPopup.description
                  }
                </p>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.itemContainer}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.labelContainer}
                  >
                    <label style={styles.label}>
                      <label id="add-test-center-test-center-manager-title">
                        {
                          LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                            .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems
                            .testCenterAccesses.addTestCenterManagerPopup.testCenterManagerLabel
                        }
                      </label>
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.inputContainer}
                  >
                    {this.state.isLoadingTestCenterManagerOptions ? (
                      <div style={styles.loadingContainer}>
                        <label className="fa fa-spinner fa-spin">
                          <FontAwesomeIcon icon={faSpinner} />
                        </label>
                      </div>
                    ) : (
                      <DropdownSelect
                        idPrefix="add-test-center-test-center-manager"
                        isValid={true}
                        ariaRequired={true}
                        ariaLabelledBy="add-test-center-test-center-manager-title"
                        hasPlaceholder={true}
                        options={this.state.testCenterManagerOptions}
                        onChange={this.getSelectedTestCenterManager}
                        defaultValue={this.state.selectedTestCenterManagerToAdd}
                        isMulti={false}
                      />
                    )}
                  </Col>
                </Row>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeAddTestCenterManagerPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={!this.state.isLoadingAddTestCenterManager ? faPlusCircle : ""}
          rightButtonTitle={
            !this.state.isLoadingAddTestCenterManager ? (
              LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems.testCenterAccesses
                .addTestCenterManagerPopup.addButton
            ) : (
              <div style={styles.customLoadingContainer}>
                {/* this div is useful to get the right size of the button while loading */}
                <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
                  <FontAwesomeIcon icon={faPlusCircle} style={styles.icon} />
                  {
                    LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                      .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems
                      .testCenterAccesses.addTestCenterManagerPopup.addButton
                  }
                </div>
                <div style={styles.loadingOverlappingStyle}>
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} />
                  </label>
                </div>
              </div>
            )
          }
          rightButtonLabel={
            LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
              .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems.testCenterAccesses
              .addTestCenterManagerPopup.addButton
          }
          rightButtonAction={this.handleAddTestCenterManager}
          rightButtonState={
            this.state.isLoadingAddTestCenterManager
              ? BUTTON_STATE.disabled
              : this.state.selectedTestCenterManagerToAdd.length > 0 ||
                  Object.keys(this.state.selectedTestCenterManagerToAdd).length > 0
                ? BUTTON_STATE.enabled
                : BUTTON_STATE.disabled
          }
        />
        {Object.keys(this.state.selectedTestCenterManagerToDelete).length > 0 && (
          <PopupBox
            show={this.state.showDeleteTestCenterManagerPopup}
            handleClose={() => {}}
            shouldCloseOnEsc={false}
            displayCloseButton={false}
            isBackdropStatic={true}
            overflowVisible={true}
            title={LOCALIZE.formatString(
              LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems.testCenterAccesses
                .deleteTestCenterManagerPopup.title,
              this.props.testCenterDataEtta.name
            )}
            description={
              <div style={styles.popupContainer}>
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.warning}
                  message={
                    <p>
                      {LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                          .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems
                          .testCenterAccesses.deleteTestCenterManagerPopup.description,
                        <span style={styles.bold}>
                          {this.state.selectedTestCenterManagerToDelete.user_data[0].username}
                        </span>
                      )}
                    </p>
                  }
                />
              </div>
            }
            leftButtonType={BUTTON_TYPE.secondary}
            leftButtonIcon={faTimes}
            leftButtonTitle={LOCALIZE.commons.cancel}
            leftButtonLabel={LOCALIZE.commons.cancel}
            leftButtonAction={this.closeDeleteTestCenterManagerPopup}
            rightButtonType={BUTTON_TYPE.danger}
            rightButtonIcon={!this.state.isLoadingDeleteTestCenterManager ? faTrashAlt : ""}
            rightButtonTitle={
              !this.state.isLoadingDeleteTestCenterManager ? (
                LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                  .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems.testCenterAccesses
                  .deleteTestCenterManagerPopup.deleteButton
              ) : (
                <div style={styles.customLoadingContainer}>
                  {/* this div is useful to get the right size of the button while loading */}
                  <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
                    <FontAwesomeIcon icon={faPlusCircle} style={styles.icon} />
                    {
                      LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                        .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems
                        .testCenterAccesses.deleteTestCenterManagerPopup.deleteButton
                    }
                  </div>
                  <div style={styles.loadingOverlappingStyle}>
                    <label className="fa fa-spinner fa-spin">
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  </div>
                </div>
              )
            }
            rightButtonLabel={
              LOCALIZE.systemAdministrator.testCenterAdministration.tabs.testCenters
                .selectedTestCenter.tabs.selectedTestCenter.sideNavigationItems.testCenterAccesses
                .deleteTestCenterManagerPopup.deleteButton
            }
            rightButtonAction={this.handleDeleteTestCenterManager}
            rightButtonState={
              !this.state.isLoadingDeleteTestCenterManager
                ? BUTTON_STATE.enabled
                : BUTTON_STATE.disabled
            }
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testCenterDataEtta: state.testCenter.testCenterDataEtta
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setEttaTestCenterData,
      updateTestCenterDataAsEtta,
      triggerRerender,
      getOrganization,
      getSelectedTestCenterDataAsEtta,
      getUsersBasedOnSpecifiedPermission,
      addTestCenterManager,
      deleteTestCenterManager
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestCenterAccesses);
