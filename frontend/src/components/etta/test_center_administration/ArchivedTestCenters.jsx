import React, { Component } from "react";
// import PropTypes from "prop-types";
import { connect } from "react-redux";
// import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";

const styles = {};

class ArchivedTestCenters extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {};

  state = {};

  componentDidMount = () => {};

  render() {
    return <div>TO BE BUILT</div>;
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ArchivedTestCenters);
