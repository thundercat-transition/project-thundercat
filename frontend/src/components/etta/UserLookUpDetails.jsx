import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import SideNavigation from "../eMIB/SideNavigation";
import LOCALIZE from "../../text_resources";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowAltCircleLeft } from "@fortawesome/free-solid-svg-icons";
import { styles as SystemAdministrationStyles } from "./SystemAdministration";
import { history } from "../../store-index";
import { PATH } from "../commons/Constants";
import ContentContainer from "../commons/ContentContainer";
import UserTests from "./userDetails/UserTests";
import UserRightsAndPermissions from "./userDetails/UserRightsAndPermissions";
import UserPersonalInfo from "./userDetails/UserPersonalInfo";
import { Row } from "react-bootstrap";
import {
  resetUserLookUpSelectedUserState,
  setBOUserLookUpSideNavState
} from "../../modules/UserLookUpRedux";

const styles = {
  tabContainer: {
    zIndex: 1,
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent",
    overflowY: "auto"
  },
  nav: {
    marginTop: 10,
    marginLeft: 10,
    width: 206,
    textAlign: "center"
  },
  backButtonStyle: {
    marginBottom: 24,
    padding: "10px 30px 0px"
  },
  loadingLabel: {
    paddingTop: "20px"
  },
  buttonLabel: {
    marginLeft: 6
  }
};

class UserLookUpDetails extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  state = { backToUserLookUp: false, isLoading: false };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  goBackToUserLookUp = () => {
    // resetting needed redux states
    this.props.resetUserLookUpSelectedUserState();
    history.push(PATH.systemAdministration);
  };

  getSelectUserLookUpSections = () => {
    return [
      {
        menuString:
          LOCALIZE.systemAdministrator.userLookUpDetails.sideNavigationItems.userPersonalInfo,
        body: <UserPersonalInfo />
      },
      {
        menuString: LOCALIZE.systemAdministrator.userLookUpDetails.sideNavigationItems.myTests,
        body: <UserTests />
      },
      {
        menuString:
          LOCALIZE.systemAdministrator.userLookUpDetails.sideNavigationItems.rightsAndPermissions,
        body: <UserRightsAndPermissions />
      }
    ];
  };

  render() {
    const specs = this.getSelectUserLookUpSections();

    return (
      <div>
        <ContentContainer>
          <div id="main-content" role="main">
            <Row style={styles.backButtonStyle}>
              <CustomButton
                label={
                  <>
                    <FontAwesomeIcon icon={faArrowAltCircleLeft} />
                    <span style={styles.buttonLabel}>
                      {LOCALIZE.systemAdministrator.userLookUpDetails.backToUserLookUp}
                    </span>
                  </>
                }
                action={this.goBackToUserLookUp}
                buttonTheme={THEME.SECONDARY}
              />
            </Row>
            <div>
              <div style={SystemAdministrationStyles.sectionContainerLabelDiv}>
                <div>
                  <label style={SystemAdministrationStyles.sectionContainerLabel}>
                    {LOCALIZE.formatString(
                      LOCALIZE.systemAdministrator.userLookUpDetails.containerLabel,
                      this.props.selectedUserFirstname,
                      this.props.selectedUserLastname
                    )}

                    <span style={SystemAdministrationStyles.tabStyleBorder}></span>
                  </label>
                </div>
              </div>
              <div style={SystemAdministrationStyles.sectionContainer}>
                <SideNavigation
                  specs={specs}
                  startIndex={this.props.selectedUserLookUpSideNavItem}
                  displayNextPreviousButton={false}
                  isMain={true}
                  tabContainerStyle={styles.tabContainer}
                  tabContentStyle={styles.tabContent}
                  navStyle={styles.nav}
                  loadOnClick={true}
                  updateSelectedSideNavItem={this.props.setBOUserLookUpSideNavState}
                />
              </div>
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    selectedUserId: state.userLookUp.selectedUserId,
    selectedUserFirstname: state.userLookUp.selectedUserFirstname,
    selectedUserLastname: state.userLookUp.selectedUserLastname,
    selectedUserLookUpSideNavItem: state.userLookUp.selectedUserLookUpSideNavItem
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setBOUserLookUpSideNavState,
      resetUserLookUpSelectedUserState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(UserLookUpDetails);
