import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import { getAllTests, getFoundTests } from "../../../modules/MyTestsRedux";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTimesCircle,
  faTimes,
  faBinoculars,
  faDownload
} from "@fortawesome/free-solid-svg-icons";
import {
  faSquare,
  faCheckSquare,
  faTimesCircle as regularFaTimesCircle
} from "@fortawesome/free-regular-svg-icons";
import getRetestDate from "../../../helpers/retestPeriodCalculations";
import getConvertedScore from "../../../helpers/scoreConversion";
import getFormattedTestStatus from "../../ta/TestStatus";
import {
  invalidateCandidateAsEtta,
  validateCandidateAsEtta
} from "../../../modules/ActiveTestsRedux";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import PopupBox, { BUTTON_TYPE, BUTTON_STATE } from "../../commons/PopupBox";
import getInvalidateTestReasonRedux from "../../../modules/InvalidateTestRedux";
import StyledTooltip, { TYPE, EFFECT } from "../../authentication/StyledTooltip";
import { Row, Col } from "react-bootstrap";
import {
  getAllExistingTestOrderNumbers,
  getCandidateActionsReportData
} from "../../../modules/ReportsRedux";
import generateCandidateActionsReport from "../../commons/reports/GenerateCandidateActionsReport";
import { CSVLink } from "react-csv";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import Pagination from "../../commons/Pagination";
import {
  updateUserLookUpTestsPageState,
  updateUserLookUpTestsPageSizeState,
  updateSearchUserLookUpTestsStates
} from "../../../modules/UserLookUpRedux";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import moment from "moment";
import { convertDateTimeToUsersTimezone } from "../../../modules/helpers";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 7,
    xl: 7
  }
};

const styles = {
  buttonIcon: {
    marginRight: 6
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  textAreaInput: {
    height: 85,
    overflowY: "auto",
    resize: "none",
    display: "block",
    width: "calc(100% - 0.5px)"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  allUnset: {
    all: "unset"
  },
  popupTestInfoContainer: {
    margin: "12px 0"
  },
  popupTestInfoItem: {
    margin: "12px 36px"
  },
  popupTestInfoLeftColumn: {
    fontWeight: "bold"
  },
  popupTestInfoRightColumn: {
    verticalAlign: "middle"
  },
  faTimesCircle: {
    width: "100%",
    minHeight: 20,
    textAlign: "center",
    color: "#cc0000"
  },
  regularFaTimesCircle: {
    width: "100%",
    minHeight: 20,
    textAlign: "center",
    color: "#00565e"
  },
  faBinoculars: {
    width: "100%",
    minHeight: 20,
    textAlign: "center"
  },
  faCheck: {
    color: "#00565e"
  },
  bold: {
    fontWeight: "bold"
  },
  popupLabelIndentation: {
    paddingLeft: 36
  }
};
export class UserTests extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  constructor(props) {
    super(props);
    this.createReportLinkButtonRefTemp = React.createRef();
  }

  state = {
    rowsDefinition: {},
    currentlyLoading: false,
    showInvalidatePopup: false,
    showValidatePopup: false,
    selectedTestToAction: {},
    isValidReasonForInvalidatingTest: true,
    reasonForInvalidatingTest: "",
    invalidationLoading: false,
    isValidReasonForValidatingTest: true,
    reasonForValidatingTest: "",
    validationLoading: false,
    allTests: [],
    resultsFound: 0,
    clearSearchTriggered: false,
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    viewSelectedTest: {},
    viewSelectedTestPopup: false,
    reportCreationLoading: false,
    formattedReportData: [],
    reportName: ""
  };

  componentDidMount = () => {
    this.populateUserTestsBasedOnActiveSearch();
  };

  componentDidUpdate = prevProps => {
    // if accommodations gets updated
    if (prevProps.accommodations !== this.props.accommodations) {
      this.populateUserTests();
    }
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateUserTestsBasedOnActiveSearch();
    }
    // if userLookUpTestsPaginationPage gets updated
    if (prevProps.userLookUpTestsPaginationPage !== this.props.userLookUpTestsPaginationPage) {
      this.populateUserTestsBasedOnActiveSearch();
    }
    // // if userLookUpTestsPaginationPageSize get updated
    if (
      prevProps.userLookUpTestsPaginationPageSize !== this.props.userLookUpTestsPaginationPageSize
    ) {
      this.populateUserTestsBasedOnActiveSearch();
    }
    // // if search userLookUpTestsKeyword gets updated
    if (prevProps.userLookUpTestsKeyword !== this.props.userLookUpTestsKeyword) {
      // if userLookUpTestsKeyword redux state is empty
      if (this.props.userLookUpTestsKeyword !== "") {
        this.populateFoundUserTests();
      } else if (
        this.props.userLookUpTestsKeyword === "" &&
        this.props.userLookUpTestsActiveSearch
      ) {
        this.populateFoundUserTests();
      }
    }
    // // if userLookUpTestsActiveSearch gets updated
    if (prevProps.userLookUpTestsActiveSearch !== this.props.userLookUpTestsActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.userLookUpTestsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateUserTests();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundUserTests();
      }
    }
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  populateUserTestsBasedOnActiveSearch = () => {
    // current search
    if (this.props.userLookUpTestsActiveSearch) {
      this.populateFoundUserTests();
      // no search
    } else {
      this.populateUserTests();
    }
  };

  populateUserTests = () => {
    this._isMounted = true;
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    const allTestsArray = [];
    this.setState({ currentlyLoading: true }, () => {
      this.props
        .getAllTests(
          this.props.selectedUserId,
          this.props.userLookUpTestsPaginationPage,
          this.props.userLookUpTestsPaginationPageSize
        )
        .then(response => {
          if (this._isMounted) {
            for (let i = 0; i < response.results.length; i++) {
              const adjustedSubmitDate = convertDateTimeToUsersTimezone(
                response.results[i].utc_submit_date
              ).adjustedDate;
              // pushing needed data in allTestsArray
              allTestsArray.push({
                id: response.results[i].id,
                test_id: response.results[i].test_id,
                test_version: response.results[i].test_version,
                parent_code: response.results[i].parent_code,
                test_code: response.results[i].test_code,
                test_name: response.results[i][`test_name_${this.props.currentLanguage}`],
                retest_date:
                  parseInt(response.results[i].retest_period) !== 0
                    ? getRetestDate(adjustedSubmitDate, parseInt(response.results[i].retest_period))
                    : LOCALIZE.commons.na,
                score: getConvertedScore(
                  response.results[i][`${this.props.currentLanguage}_converted_score`],
                  response.results[i].status_codename
                ),
                status: getFormattedTestStatus(response.results[i].status_codename),
                test_access_code: response.results[i].test_access_code,
                ta_first_name: response.results[i].ta_first_name,
                ta_last_name: response.results[i].ta_last_name,
                ta_email: response.results[i].ta_email,
                ta_goc_email: response.results[i].ta_goc_email,
                ta_name: `${response.results[i].ta_last_name}, ${response.results[i].ta_first_name}`,
                test_order_number: response.results[i].test_order_number,
                total_score: response.results[i].total_score,
                invalidTestReason: response.results[i].invalidate_test_reason,
                assessment_process_reference_nbr:
                  response.results[i].assessment_process_reference_nbr,
                validity_end_date_or_process_closing_date:
                  response.results[i].validity_end_date_or_process_closing_date
              });
              // pushing needed data in data array (needed for GenericTable component)
              data.push({
                column_1: `${response.results[i].test_code}`,
                column_2: adjustedSubmitDate,
                column_3:
                  parseInt(response.results[i].retest_period) !== 0
                    ? getRetestDate(adjustedSubmitDate, parseInt(response.results[i].retest_period))
                    : LOCALIZE.commons.na,
                column_4: getConvertedScore(
                  response.results[i][`${this.props.currentLanguage}_converted_score`],
                  response.results[i].status_codename
                ),
                column_5: getFormattedTestStatus(response.results[i].status_codename),
                column_6: this.getFormattedTestValidityFlag(response.results[i].is_invalid),
                column_7: this.populateColumnSix(i, response.results[i])
              });
            }
          }
          // updating rowsDefinition object with provided data and needed style
          rowsDefinition = {
            column_1_style: COMMON_STYLE.LEFT_TEXT,
            column_2_style: COMMON_STYLE.LEFT_TEXT,
            column_3_style: COMMON_STYLE.LEFT_TEXT,
            column_4_style: COMMON_STYLE.CENTERED_TEXT,
            column_5_style: COMMON_STYLE.CENTERED_TEXT,
            column_6_style: COMMON_STYLE.CENTERED_TEXT,
            column_7_style: COMMON_STYLE.CENTERED_TEXT,
            data: data
          };

          // saving results in state
          this.setState({
            rowsDefinition: rowsDefinition,
            allTests: allTestsArray,
            numberOfPages: Math.ceil(response.count / this.props.userLookUpTestsPaginationPageSize),
            nextPageNumber: response.next_page_number,
            previousPageNumber: response.previous_page_number
          });
        })
        .then(() => {
          if (this._isMounted) {
            if (this.state.clearSearchTriggered) {
              // go back to the first page to avoid display bugs
              this.props.updateUserLookUpTestsPageState(1);
              this.setState({ clearSearchTriggered: false });
            }
            this.setState({ currentlyLoading: false });
          }
        });
    });
  };

  populateFoundUserTests = () => {
    this._isMounted = true;
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    const allTestsArray = [];
    this.setState({ currentlyLoading: true }, () => {
      this.props
        .getFoundTests(
          this.props.selectedUserId,
          this.props.userLookUpTestsKeyword,
          this.props.currentLanguage,
          this.props.userLookUpTestsPaginationPage,
          this.props.userLookUpTestsPaginationPageSize
        )
        .then(response => {
          if (this._isMounted) {
            // there are no results found
            if (response[0] === "no results found") {
              this.setState({
                allTests: [],
                rowsDefinition: {},
                numberOfPages: 1,
                nextPageNumber: 0,
                previousPageNumber: 0,
                resultsFound: 0
              });
              // there is at least one result found
            } else {
              for (let i = 0; i < response.results.length; i++) {
                const adjustedSubmitDate = convertDateTimeToUsersTimezone(
                  response.results[i].utc_submit_date
                ).adjustedDate;

                // pushing needed data in allTestsArray
                allTestsArray.push({
                  id: response.results[i].id,
                  test_id: response.results[i].test_id,
                  test_version: response.results[i].test_version,
                  parent_code: response.results[i].parent_code,
                  test_code: response.results[i].test_code,
                  test_name: response.results[i][`test_name_${this.props.currentLanguage}`],
                  retest_date:
                    parseInt(response.results[i].retest_period) !== 0
                      ? getRetestDate(
                          adjustedSubmitDate,
                          parseInt(response.results[i].retest_period)
                        )
                      : LOCALIZE.commons.na,
                  score: getConvertedScore(
                    response.results[i][`${this.props.currentLanguage}_converted_score`],
                    response.results[i].status_codename
                  ),
                  status: getFormattedTestStatus(response.results[i].status_codename),
                  test_access_code: response.results[i].test_access_code,
                  ta_first_name: response.results[i].ta_first_name,
                  ta_last_name: response.results[i].ta_last_name,
                  ta_email: response.results[i].ta_email,
                  ta_goc_email: response.results[i].ta_goc_email,
                  ta_name: `${response.results[i].ta_last_name}, ${response.results[i].ta_first_name}`,
                  test_order_number: response.results[i].test_order_number,
                  total_score: response.results[i].total_score,
                  invalidTestReason: response.results[i].invalidate_test_reason,
                  assessment_process_reference_nbr:
                    response.results[i].assessment_process_reference_nbr
                });
                // pushing needed data in data array (needed for GenericTable component)
                data.push({
                  column_1: `${response.results[i].test_code}`,
                  column_2: adjustedSubmitDate,
                  column_3:
                    parseInt(response.results[i].retest_period) !== 0
                      ? getRetestDate(
                          adjustedSubmitDate,
                          parseInt(response.results[i].retest_period)
                        )
                      : LOCALIZE.commons.na,
                  column_4: getConvertedScore(
                    response.results[i][`${this.props.currentLanguage}_converted_score`],
                    response.results[i].status_codename
                  ),
                  column_5: getFormattedTestStatus(response.results[i].status_codename),
                  column_6: this.getFormattedTestValidityFlag(response.results[i].is_invalid),
                  column_7: this.populateColumnSix(i, response.results[i])
                });
              }
              // updating rowsDefinition object with provided data and needed style
              rowsDefinition = {
                column_1_style: COMMON_STYLE.LEFT_TEXT,
                column_2_style: COMMON_STYLE.LEFT_TEXT,
                column_3_style: COMMON_STYLE.LEFT_TEXT,
                column_4_style: COMMON_STYLE.CENTERED_TEXT,
                column_5_style: COMMON_STYLE.CENTERED_TEXT,
                column_6_style: COMMON_STYLE.CENTERED_TEXT,
                column_7_style: COMMON_STYLE.CENTERED_TEXT,
                data: data
              };

              // saving results in state
              this.setState({
                rowsDefinition: rowsDefinition,
                allTests: allTestsArray,
                numberOfPages: Math.ceil(
                  response.count / this.props.userLookUpTestsPaginationPageSize
                ),
                nextPageNumber: response.next_page_number,
                previousPageNumber: response.previous_page_number,
                resultsFound: response.count
              });
            }
          }
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({ currentlyLoading: false });
          }
        });
    });
  };

  getFormattedTestValidityFlag = isInvalid => {
    const customCheckboxFontSize = {
      // adding 6px to selected font-size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 6}px`
    };
    return (
      <div>
        <FontAwesomeIcon
          icon={isInvalid ? faCheckSquare : faSquare}
          style={{ ...styles.faCheck, ...customCheckboxFontSize }}
        />
      </div>
    );
  };

  runGenerateCandidateActionsReport = response => {
    this.setState({
      reportName: `${LOCALIZE.reports.reportTypes.candidateActionsReport} - ${response.test_order_number} - ${response.test_code} - ${response.candidate_username}.csv`
    });

    this.props
      // .getCandidateActionsReportData(this.props.selectedAssignedTest.value)
      .getCandidateActionsReportData(response.id)
      .then(response => {
        // getting an array
        if (response.length >= 0) {
          this.handleReportDataFormatting(response);
          // should never happen
        } else {
          // display no data popup
          this.setState({ displayNoDataError: true });
        }
      });
  };

  // formatting report data
  handleReportDataFormatting = reportData => {
    // if there is at least one entry based on the provided parameters

    // initializing variables
    let formattedReportData = [];

    // generating report data
    formattedReportData = generateCandidateActionsReport(reportData);

    this.setState({ formattedReportData: formattedReportData }, () => {
      this.setState({ reportCreationLoading: false }, () => {
        // once all data has been created, simulate click on hidden create report link to generate the downloadable csv file
        this.createReportLinkButtonRefTemp.current.link.click();
      });
    });
  };

  populateColumnSix = (i, response) => {
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`generate-candidate-actions-report-specific-test-${i}`}
          place="top"
          variant={TYPE.light}
          effect={EFFECT.solid}
          openOnClick={false}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`generate-candidate-actions-report-specific-test-${i}`}
                label={<FontAwesomeIcon icon={faDownload} style={styles.faBinoculars} />}
                action={() => this.runGenerateCandidateActionsReport(response)}
                buttonTheme={THEME.SECONDARY}
                customStyle={styles.actionButton}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table
                    .candidateActionReportAria,
                  response.test_code
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table
                    .downloadCandidateActionReport
                }
              </p>
            </div>
          }
        />

        <StyledTooltip
          id={`view-selected-user-specific-test-${i}`}
          place="top"
          variant={TYPE.light}
          effect={EFFECT.solid}
          openOnClick={false}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`view-selected-user-specific-test-${i}`}
                label={<FontAwesomeIcon icon={faBinoculars} style={styles.faBinoculars} />}
                action={() => {
                  this.viewTestDetailsPopup(i);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table
                    .viewButtonAccessibility,
                  `${response.test_code}`
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.viewTestDetailsTooltip}
              </p>
            </div>
          }
        />
        <StyledTooltip
          id={`selected-user-invalidate-validate-specific-test-${i}`}
          place="top"
          variant={TYPE.light}
          effect={EFFECT.solid}
          openOnClick={false}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`selected-user-invalidate-validate-specific-test-${i}`}
                label={
                  <FontAwesomeIcon
                    icon={response.is_invalid ? regularFaTimesCircle : faTimesCircle}
                    style={response.is_invalid ? styles.regularFaTimesCircle : styles.faTimesCircle}
                  />
                }
                action={
                  response.is_invalid
                    ? () => {
                        this.openValidatePopup(i);
                      }
                    : () => {
                        this.openInvalidatePopup(i);
                      }
                }
                buttonTheme={THEME.SECONDARY}
                customStyle={styles.actionButton}
                ariaLabel={
                  response.is_invalid
                    ? LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table
                          .validateButtonAccessibility,
                        `${response.test_code}`
                      )
                    : LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table
                          .invalidateButtonAccessibility,
                        `${response.test_code}`
                      )
                }
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {response.is_invalid
                  ? LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.validateTestTooltip
                  : LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table
                      .invalidateTestTooltip}
              </p>
            </div>
          }
        />
      </div>
    );
  };

  // open view test details pop up
  viewTestDetailsPopup = id => {
    this.setState({
      viewSelectedTestPopup: true,
      viewSelectedTest: this.state.allTests[id]
    });
  };

  // close view test details pop up
  closeTestDetailsPopup = () => {
    this.setState({
      viewSelectedTestPopup: false,
      viewSelectedTest: {}
    });
  };

  // open pop up
  openInvalidatePopup = id => {
    this.setState({
      showInvalidatePopup: true,
      selectedTestToAction: this.state.allTests[id]
    });
  };

  //  close pop up
  closeInvalidatePopup = () => {
    this.setState({
      showInvalidatePopup: false,
      selectedTestToAction: {},
      isValidReasonForInvalidatingTest: true,
      reasonForInvalidatingTest: "",
      invalidationLoading: false
    });
  };

  // open pop up
  openValidatePopup = id => {
    this.setState({
      showValidatePopup: true,
      selectedTestToAction: this.state.allTests[id]
    });
  };

  //  close pop up
  closeValidatePopup = () => {
    this.setState({
      showValidatePopup: false,
      selectedTestToAction: {},
      isValidReasonForValidatingTest: true,
      reasonForValidatingTest: "",
      validationLoading: false
    });
  };

  updateReasonForInvalidatingTest = event => {
    const reasonForInvalidatingTest = event.target.value;
    if (Object.keys(reasonForInvalidatingTest).length !== 0) {
      this.setState({
        isValidReasonForInvalidatingTest: false,
        reasonForInvalidatingTest: reasonForInvalidatingTest
      });
    } else {
      this.setState({
        isValidReasonForInvalidatingTest: true,
        reasonForInvalidatingTest: reasonForInvalidatingTest
      });
    }
  };

  updateReasonForValidatingTest = event => {
    const reasonForValidatingTest = event.target.value;
    if (Object.keys(reasonForValidatingTest).length !== 0) {
      this.setState({
        isValidReasonForValidatingTest: false,
        reasonForValidatingTest: reasonForValidatingTest
      });
    } else {
      this.setState({
        isValidReasonForValidatingTest: true,
        reasonForValidatingTest: reasonForValidatingTest
      });
    }
  };

  handleInvalidate = () => {
    this.setState({ invalidationLoading: true }, () => {
      const data = {
        id: this.state.selectedTestToAction.id,
        user_id: this.props.selectedUserId,
        test_id: this.state.selectedTestToAction.test_id,
        invalidate_test_reason: this.state.reasonForInvalidatingTest
      };
      this.props.invalidateCandidateAsEtta(data).then(response => {
        // if request succeeded
        if (response.status === 200) {
          // update user tests table
          this.populateUserTests();
          // should never happen
        } else {
          throw new Error("Something went wrong during invalidating active tests");
        }
        // close pop up
        this.closeInvalidatePopup();
      });
    });
  };

  handleValidate = () => {
    this.setState({ validationLoading: true }, () => {
      const data = {
        id: this.state.selectedTestToAction.id,
        user_id: this.props.selectedUserId,
        test_id: this.state.selectedTestToAction.test_id,
        validate_test_reason: this.state.reasonForValidatingTest
      };
      this.props.validateCandidateAsEtta(data).then(response => {
        // if request succeeded
        if (response.status === 200) {
          // update user tests table
          this.populateUserTests();
          // should never happen
        } else {
          throw new Error("Something went wrong during validating active tests");
        }
        // close pop up
        this.closeValidatePopup();
      });
    });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.columnOne,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.columnTwo,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.columnThree,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.columnFour,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.columnFive,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.columnSix,
        style: { ...{ width: "10%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.columnSeven,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div>
        <h2>
          {LOCALIZE.formatString(
            LOCALIZE.systemAdministrator.userLookUpDetails.Tests.title,
            this.props.selectedUserFirstname,
            this.props.selectedUserLastname
          )}
        </h2>
        <div>
          <p>
            {LOCALIZE.formatString(
              LOCALIZE.systemAdministrator.userLookUpDetails.Tests.description,
              this.props.selectedUserFirstname,
              this.props.selectedUserLastname
            )}
          </p>
        </div>
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"user-look-up-tests"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchUserLookUpTestsStates}
            updatePageState={this.props.updateUserLookUpTestsPageState}
            paginationPageSize={Number(this.props.userLookUpTestsPaginationPageSize)}
            updatePaginationPageSize={this.props.updateUserLookUpTestsPageSizeState}
            data={this.state.allTests}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.userLookUpTestsKeyword}
          />
          <GenericTable
            classnamePrefix="selected-user-all-tests"
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={LOCALIZE.systemAdministrator.userLookUpDetails.Tests.table.noTests}
            currentlyLoading={this.state.currentlyLoading}
          />
          <Pagination
            paginationContainerId={"user-look-up-tests-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.userLookUpTestsPaginationPage}
            updatePaginationPageState={this.props.updateUserLookUpTestsPageState}
            firstTableRowId={"user-look-up-tests-table-row-0"}
          />
        </div>
        <CSVLink
          ref={this.createReportLinkButtonRefTemp}
          asyncOnClick={true}
          data={this.state.formattedReportData}
          filename={this.state.reportName}
          enclosingCharacter=""
        ></CSVLink>
        <PopupBox
          show={this.state.viewSelectedTestPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          overflowVisible={false}
          title={LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup.title}
          description={
            <>
              <p>
                {
                  LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                    .description
                }
              </p>
              <div style={styles.popupTestInfoContainer}>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <label htmlFor="popup-test-name">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .testName
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <input
                      id="popup-test-name"
                      aria-disabled={true}
                      className={"valid-field input-disabled-look"}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.viewSelectedTest.test_name}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <label htmlFor="popup-test-code">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .testCode
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <input
                      id="popup-test-code"
                      aria-disabled={true}
                      className={"valid-field input-disabled-look"}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.viewSelectedTest.test_code}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <label htmlFor="popup-test-version">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .version
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <input
                      id="popup-test-version"
                      aria-disabled={true}
                      className={"valid-field input-disabled-look"}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.viewSelectedTest.test_version}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <label htmlFor="popup-ta">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .testAdministrator
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <input
                      id="popup-ta"
                      aria-disabled={true}
                      className={"valid-field input-disabled-look"}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={`${this.state.viewSelectedTest.ta_last_name}, ${this.state.viewSelectedTest.ta_first_name}`}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <label htmlFor="popup-ta-email" style={styles.popupLabelIndentation}>
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .testAdministratorEmail
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <input
                      id="popup-ta-email"
                      aria-disabled={true}
                      className={"valid-field input-disabled-look"}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.viewSelectedTest.ta_email}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <label htmlFor="popup-ta-goc-email" style={styles.popupLabelIndentation}>
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .testAdministratorGocEmail
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <input
                      id="popup-ta-goc-email"
                      aria-disabled={true}
                      className={"valid-field input-disabled-look"}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.viewSelectedTest.ta_goc_email}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <label htmlFor="popup-test-access-code">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .testAccessCode
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <input
                      id="popup-test-access-code"
                      aria-disabled={true}
                      className={"valid-field input-disabled-look"}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.viewSelectedTest.test_access_code}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <label htmlFor="popup-test-order-number">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .testOrderNumber
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <input
                      id="popup-test-order-number"
                      aria-disabled={true}
                      className={"valid-field input-disabled-look"}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={
                        this.state.viewSelectedTest.test_order_number !== null
                          ? this.state.viewSelectedTest.test_order_number
                          : LOCALIZE.reports.orderlessRequest
                      }
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <label htmlFor="popup-assessment-process-reference-nbr">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .assessmentProcessOrReferenceNb
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <input
                      id="popup-assessment-process-reference-nbr"
                      aria-disabled={true}
                      className={"valid-field input-disabled-look"}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.viewSelectedTest.assessment_process_reference_nbr}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <label htmlFor="popup-validity-end-date-or-process-closing-date">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .validityEndDateOrProcessClosingDate
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <input
                      id="popup-validity-end-date-or-process-closing-date"
                      aria-disabled={true}
                      className={"valid-field input-disabled-look"}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.viewSelectedTest.validity_end_date_or_process_closing_date}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <label htmlFor="popup-total-score">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .totalScore
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <input
                      id="popup-total-score"
                      aria-disabled={true}
                      className={"valid-field input-disabled-look"}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.viewSelectedTest.total_score}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.popupTestInfoItem}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.popupTestInfoLeftColumn}
                  >
                    <label htmlFor="popup-invalid-test-reason">
                      {
                        LOCALIZE.systemAdministrator.userLookUpDetails.Tests.viewSelectedTestPopup
                          .invalidTestReason
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={styles.popupTestInfoRightColumn}
                  >
                    <textarea
                      id="popup-invalid-test-reason"
                      aria-disabled={true}
                      className={"valid-field input-disabled-look"}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.viewSelectedTest.invalidTestReason}
                      onChange={() => {}}
                    />
                  </Col>
                </Row>
              </div>
            </>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonIcon={faTimes}
          rightButtonAction={this.closeTestDetailsPopup}
        />
        <PopupBox
          show={this.state.showInvalidatePopup}
          handleClose={() => {}}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          title={LOCALIZE.systemAdministrator.activeTests.invalidatePopup.title}
          size="lg"
          overflowVisible={false}
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.systemAdministrator.activeTests.invalidatePopup.description,
                      <span style={styles.bold}>{this.props.selectedUserFirstname}</span>,
                      <span style={styles.bold}>{this.props.selectedUserLastname}</span>
                    )}
                  </p>
                }
              />
              <div style={styles.popupTestInfoContainer}>
                <label id="reason-for-invalidating-test">
                  {
                    LOCALIZE.systemAdministrator.activeTests.invalidatePopup
                      .reasonForInvalidatingTheTest
                  }
                </label>
                <textarea
                  id="reason-for-invalidating"
                  aria-labelledby="reason-for-invalidating-test"
                  aria-required={true}
                  style={{ ...styles.input, ...styles.textAreaInput }}
                  value={this.state.reasonForInvalidatingTest}
                  onChange={this.updateReasonForInvalidatingTest}
                  maxLength="300"
                ></textarea>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeInvalidatePopup}
          leftButtonState={
            this.state.invalidationLoading ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.systemAdministrator.activeTests.invalidatePopup.actionButton}
          rightButtonIcon={faTimesCircle}
          rightButtonState={
            this.state.invalidationLoading
              ? BUTTON_STATE.disabled
              : !this.state.isValidReasonForInvalidatingTest
                ? BUTTON_STATE.enabled
                : BUTTON_STATE.disabled
          }
          rightButtonAction={this.handleInvalidate}
        />
        <PopupBox
          show={this.state.showValidatePopup}
          handleClose={() => {}}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          title={LOCALIZE.systemAdministrator.activeTests.validatePopup.title}
          size="lg"
          overflowVisible={false}
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.systemAdministrator.activeTests.validatePopup.description,
                      <span style={styles.bold}>{this.props.selectedUserFirstname}</span>,
                      <span style={styles.bold}>{this.props.selectedUserLastname}</span>
                    )}
                  </p>
                }
              />
              <div style={styles.popupTestInfoContainer}>
                <label id="reason-for-validating-test">
                  {
                    LOCALIZE.systemAdministrator.activeTests.validatePopup
                      .reasonForInvalidatingTheTest
                  }
                </label>
                <textarea
                  id="reason-for-validating"
                  aria-labelledby="reason-for-validating-test"
                  aria-required={true}
                  style={{ ...styles.input, ...styles.textAreaInput }}
                  value={this.state.reasonForValidatingTest}
                  onChange={this.updateReasonForValidatingTest}
                  maxLength="300"
                ></textarea>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeValidatePopup}
          leftButtonState={
            this.state.validationLoading ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.systemAdministrator.activeTests.validatePopup.actionButton}
          rightButtonIcon={faTimesCircle}
          rightButtonState={
            this.state.validationLoading
              ? BUTTON_STATE.disabled
              : !this.state.isValidReasonForValidatingTest
                ? BUTTON_STATE.enabled
                : BUTTON_STATE.disabled
          }
          rightButtonAction={this.handleValidate}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    selectedUserId: state.userLookUp.selectedUserId,
    selectedUserFirstname: state.userLookUp.selectedUserFirstname,
    selectedUserLastname: state.userLookUp.selectedUserLastname,
    selectedAssignedTest: state.reports.selectedAssignedTest,
    userLookUpTestsPaginationPageSize: state.userLookUp.userLookUpTestsPaginationPageSize,
    userLookUpTestsPaginationPage: state.userLookUp.userLookUpTestsPaginationPage,
    userLookUpTestsKeyword: state.userLookUp.userLookUpTestsKeyword,
    userLookUpTestsActiveSearch: state.userLookUp.userLookUpTestsActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllTests,
      getFoundTests,
      invalidateCandidateAsEtta,
      validateCandidateAsEtta,
      getInvalidateTestReasonRedux,
      getAllExistingTestOrderNumbers,
      generateCandidateActionsReport,
      getCandidateActionsReportData,
      updateUserLookUpTestsPageState,
      updateUserLookUpTestsPageSizeState,
      updateSearchUserLookUpTestsStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(UserTests);
