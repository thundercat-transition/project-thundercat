import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import AssignTestAccesses from "./AssignTestAccesses";
import ActiveTestAccesses from "./ActiveTestAccesses";
import OrderlessTestAccesses from "./OrderlessTestAccesses";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  }
};

class TestAccesses extends Component {
  state = {
    selectedTab: ""
  };

  // saving tab key in selectedTab state on tab change
  // useful to trigger the reset date fields in assign test accesses form (to avoid wrong complete date picked redux
  // state when changing tabs, since there are two DatePicker under the same component)
  handleTabChange = key => {
    this.setState({ selectedTab: key });
  };

  render() {
    const TABS = [
      {
        key: "assign-test-access",
        tabName: LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.title,
        body: <AssignTestAccesses triggerResetDateFieldValuesProps={this.state.selectedTab} />
      },
      {
        key: "active-test-accesses",
        tabName: LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.title,
        body: <ActiveTestAccesses />
      },
      {
        key: "ta-orderless-test-accesses",
        tabName: LOCALIZE.systemAdministrator.testAccesses.tabs.orderlessTestAccesses.title,
        body: <OrderlessTestAccesses />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.systemAdministrator.testAccesses.title}</h2>
          <p>{LOCALIZE.systemAdministrator.testAccesses.description}</p>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey="assign-test-access"
                id="test-accesses-tabs"
                style={styles.tabNavigation}
                onSelect={this.handleTabChange}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export { TestAccesses as unconnectedTestAccesses };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TestAccesses);
