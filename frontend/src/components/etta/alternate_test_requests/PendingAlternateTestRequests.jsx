/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBinoculars,
  faShare,
  faSpinner,
  faTimes,
  faTrash
} from "@fortawesome/free-solid-svg-icons";
import StyledTooltip, { TYPE, EFFECT } from "../../authentication/StyledTooltip";
import "../../../css/etta.css";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import { Col, Row } from "react-bootstrap";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import Pagination from "../../commons/Pagination";
import {
  cancelPendingAlternateTestRequest,
  getAllPendingAlternateTestRequests,
  getFoundPendingAlternateTestRequests,
  submitPendingAlternateTestRequest,
  updatePendingAlternateTestRequestsPageSizeState,
  updatePendingAlternateTestRequestsPageState,
  updatePendingAlternateTestRequestsSearchParametersStates
} from "../../../modules/AlternateTestRequestsRedux";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import {
  getSelectedUserAccommodationFileData,
  getTestCentersForUserAccommodationRequest
} from "../../../modules/AaeRedux";
import DropdownSelect from "../../commons/DropdownSelect";
import getActiveNonPublicTests from "../../../modules/TestRedux";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import Switch from "react-switch";
import getSwitchTransformScale from "../../../helpers/switchTransformScale";
import { LANGUAGES } from "../../commons/Translation";

const columnSizes = {
  popupFirstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  popupSecondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  buttonIcon: {
    marginRight: 0
  },
  formContainer: {
    width: "100%",
    margin: "18px auto"
  },
  fieldRowContainer: {
    margin: "12px auto",
    width: "85%"
  },
  fieldRowContainerWithError: {
    margin: "12px auto 0 auto",
    width: "85%"
  },
  rowContainerWithError: {
    margin: "0 auto",
    width: "85%"
  },
  textInput: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    minHeight: 32
  },
  textAreaInput: {
    width: "100%",
    height: 115,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block"
  },
  standardLabel: {
    fontWeight: "bold",
    margin: 0
  },
  indentedLabel: {
    marginLeft: 24,
    marginBottom: 0,
    fontWeight: "bold"
  },
  input: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6,
    marginLeft: 6
  },
  loadingContainer: {
    textAlign: "center",
    transform: "scale(1.3)"
  },
  boldText: {
    fontWeight: "bold"
  },
  allUnset: {
    all: "unset"
  },
  icon: {
    paddingRight: 6,
    verticalAlign: "middle"
  },
  customLoadingContainer: {
    display: "grid",
    gridTemplateColumns: "1fr"
  },
  loadingOverlappingStyle: {
    gridRowStart: "1",
    gridColumnStart: "1"
  },
  transparentText: {
    color: "transparent"
  }
};

class PendingAlternateTestRequests extends Component {
  state = {
    clearSearchTriggered: false,
    resultsFound: 0,
    pendingAlternateTestRequests: [],
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    currentlyLoading: false,
    isLoadingDataToView: false,
    selectedPendingAlternateTestRequestData: {},
    showViewSelectedPendingAlternateTestRequestPopup: false,
    showSelectedPendingAlternateTestRequestSentSuccessfullyPopup: false,
    isLoadingTestCenterOptions: false,
    testCenterOptions: [],
    testCenterSelectedOption: [],
    isLoadingTestToAdministerOptions: false,
    testToAdministerOptions: [],
    testToAdministerSelectedOption: [],
    testCannotBePerformedInCatSwitchState: false,
    isLoadingSwitch: false,
    isValidSpecifiedTestDescription: true,
    specifiedTestDescription: "",
    rationale: "",
    submitButtonDisabledState: true,
    showCancelRequestConfirmationPopup: false,
    actionInProgress: false,
    // default values
    switchHeight: 25,
    switchWidth: 50
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updatePendingAlternateTestRequestsPageState(1);
    this.populatePendingAlternateTestRequestsBasedOnActiveSearch();
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populatePendingAlternateTestRequestsBasedOnActiveSearch();
    }
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
    // if pendingAlternateTestRequestsPage gets updated
    if (
      prevProps.pendingAlternateTestRequestsPage !== this.props.pendingAlternateTestRequestsPage
    ) {
      this.populatePendingAlternateTestRequestsBasedOnActiveSearch();
    }
    // if pendingAlternateTestRequestsPageSize get updated
    if (
      prevProps.pendingAlternateTestRequestsPageSize !==
      this.props.pendingAlternateTestRequestsPageSize
    ) {
      this.populatePendingAlternateTestRequestsBasedOnActiveSearch();
    }
    // if search pendingAlternateTestRequestsKeyword gets updated
    if (
      prevProps.pendingAlternateTestRequestsKeyword !==
      this.props.pendingAlternateTestRequestsKeyword
    ) {
      // if pendingAlternateTestRequestsKeyword redux state is empty
      if (this.props.pendingAlternateTestRequestsKeyword !== "") {
        this.populateFoundPendingAlternateTestRequests();
      } else if (
        this.props.pendingAlternateTestRequestsKeyword === "" &&
        this.props.pendingAlternateTestRequestsActiveSearch
      ) {
        this.populateFoundPendingAlternateTestRequests();
      }
    }
    // if pendingAlternateTestRequestsActiveSearch gets updated
    if (
      prevProps.pendingAlternateTestRequestsActiveSearch !==
      this.props.pendingAlternateTestRequestsActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.pendingAlternateTestRequestsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateAllPendingAlternateTestRequests();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundPendingAlternateTestRequests();
      }
    }
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoadingSwitch: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoadingSwitch: false });
        }
      );
    });
  };

  getFormattedTestSkill = userAccommodationFileData => {
    // formatting test skill
    const formattedTestSkill =
      userAccommodationFileData.test_skill_sub_type_id !== null
        ? `${userAccommodationFileData[`test_skill_type_name_${this.props.currentLanguage}`]} - ${
            userAccommodationFileData[`test_skill_sub_type_name_${this.props.currentLanguage}`]
          }`
        : `${userAccommodationFileData[`test_skill_type_name_${this.props.currentLanguage}`]}`;

    return formattedTestSkill;
  };

  populatePendingAlternateTestRequestsBasedOnActiveSearch = () => {
    // current search
    if (this.props.pendingAlternateTestRequestsActiveSearch) {
      this.populateFoundPendingAlternateTestRequests();
      // no search
    } else {
      this.populateAllPendingAlternateTestRequests();
    }
  };

  populateAllPendingAlternateTestRequests = () => {
    this.setState({ currentlyLoading: true }, () => {
      const pendingAlternateTestRequests = [];
      this.props
        .getAllPendingAlternateTestRequests(
          this.props.pendingAlternateTestRequestsPage,
          this.props.pendingAlternateTestRequestsPageSize
        )
        .then(response => {
          this.populatePendingAlternateTestRequestsObject(pendingAlternateTestRequests, response);
        })
        .then(() => {
          this.setState({ currentlyLoading: false });
        });
    });
  };

  populateFoundPendingAlternateTestRequests = () => {
    // making sure that the table is not currently loading
    if (!this.state.currentlyLoading) {
      this.setState({ currentlyLoading: true }, () => {
        const pendingAlternateTestRequests = [];
        setTimeout(() => {
          this.props
            .getFoundPendingAlternateTestRequests(
              this.props.pendingAlternateTestRequestsKeyword,
              this.props.currentLanguage,
              this.props.pendingAlternateTestRequestsPage,
              this.props.pendingAlternateTestRequestsPageSize
            )
            .then(response => {
              if (response[0] === "no results found") {
                this.setState({
                  pendingAlternateTestRequests: [],
                  rowsDefinition: {},
                  numberOfPages: 1,
                  nextPageNumber: 0,
                  previousPageNumber: 0,
                  resultsFound: 0
                });
                // there is at least one result found
              } else {
                this.populatePendingAlternateTestRequestsObject(
                  pendingAlternateTestRequests,
                  response
                );
                this.setState({ resultsFound: response.count });
              }
            })
            .then(() => {
              this.setState(
                {
                  currentlyLoading: false,
                  displayResultsFound: true
                },
                () => {
                  // make sure that this element exsits to avoid any error
                  if (document.getElementById("pending-alternate-test-requests-results-found")) {
                    document
                      .getElementById("pending-alternate-test-requests-results-found")
                      .focus();
                  }
                }
              );
            });
        }, 100);
      });
    }
  };

  populatePendingAlternateTestRequestsObject = (pendingAlternateTestRequests, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        // getting formatted test s;
        const currentResult = response.results[i];
        // pushing needed data in pendingAlternateTestRequests array
        pendingAlternateTestRequests.push(currentResult);
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult.id,
          column_2: currentResult.user_id,
          column_3: `${currentResult.user_last_name}, ${currentResult.user_first_name}`,
          column_4: currentResult.created_date,
          column_5: currentResult.process_end_date,
          column_6:
            currentResult.test_center_id !== null
              ? currentResult.test_center_name
              : `${LOCALIZE.commons.na} (${LOCALIZE.commons.testType.uit})`,
          column_7: this.getFormattedTestSkill(currentResult),
          column_8: (
            <div className="d-flex justify-content-center flex-nowrap">
              <StyledTooltip
                id={`pending-alternate-test-requests-view-button-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`pending-alternate-test-requests-view-button-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faBinoculars} style={styles.buttonIcon} />
                        </>
                      }
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests.table
                          .viewTooltipAccessibility,
                        currentResult.user_first_name,
                        currentResult.user_last_name
                      )}
                      action={() => this.openViewSelectedPendingAlternateTestRequestPopup(i)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.SECONDARY}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests.table
                          .viewTooltip
                      }
                    </p>
                  </div>
                }
              />
            </div>
          )
        });
      }
    }
    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.CENTERED_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      column_6_style: COMMON_STYLE.CENTERED_TEXT,
      column_7_style: COMMON_STYLE.CENTERED_TEXT,
      column_8_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };
    // saving results in state
    this.setState({
      pendingAlternateTestRequests: pendingAlternateTestRequests,
      rowsDefinition: rowsDefinition,
      previousPageNumber: response.previous_page_number
    });
  };

  openViewSelectedPendingAlternateTestRequestPopup = index => {
    // if not currently loading (avoid multi-clicking actions)
    if (!this.state.isLoadingDataToView) {
      this.setState({ isLoadingDataToView: true }, () => {
        // getting selected user accommodation file data
        this.props
          .getSelectedUserAccommodationFileData(this.state.pendingAlternateTestRequests[index].id)
          .then(response => {
            // setting selectedPendingAlternateTestRequestData object
            const selectedPendingAlternateTestRequestData = response;
            // adding formatted_test_skill attribute to the selectedPendingAlternateTestRequestData object
            selectedPendingAlternateTestRequestData.formatted_test_skill =
              this.getFormattedTestSkill(response);
            // updating needed states
            this.setState(
              {
                selectedPendingAlternateTestRequestData: selectedPendingAlternateTestRequestData,
                showViewSelectedPendingAlternateTestRequestPopup: true,
                isLoadingDataToView: false
              },
              () => {
                // populating available locations
                this.populateLocationOptions();
                // getting active non public tests
                this.getActiveNonPublictests();
              }
            );
          });
      });
    }
  };

  populateLocationOptions = () => {
    this.setState({ isLoadingTestCenterOptions: true }, () => {
      // getting available locations (test centers)
      this.props.getTestCentersForUserAccommodationRequest().then(response => {
        // initializing testCenterSelectedOption
        let testCenterSelectedOption = [];
        // initializing testToAdministerOptions
        const testCenterOptions = [];
        // looping in response
        for (let i = 0; i < response.body.length; i++) {
          // if id of current iteration matches the test_center_id of the aaeSelectedUserAccommodationFileData redux state
          if (
            response.body[i].id ===
            this.state.selectedPendingAlternateTestRequestData.test_center_id
          ) {
            testCenterSelectedOption = {
              label: `${
                response.body[i].test_center_city_text[this.props.currentLanguage][0].text
              } (${
                response.body[i].test_center_address_text[this.props.currentLanguage][0].text
              }, ${response.body[i].test_center_city_text[this.props.currentLanguage][0].text}, ${
                response.body[i].test_center_province_text[this.props.currentLanguage][0].text
              },
              ${response.body[i].postal_code},
              ${
                this.props.currentLanguage === LANGUAGES.english
                  ? response.body[i].country_eabrv
                  : response.body[i].country_fabrv
              })`,
              value: response.body[i].id
            };
          }
          // populating testCenterOptions
          testCenterOptions.push({
            label: `${
              response.body[i].test_center_city_text[this.props.currentLanguage][0].text
            } (${response.body[i].test_center_address_text[this.props.currentLanguage][0].text}, ${
              response.body[i].test_center_city_text[this.props.currentLanguage][0].text
            }, ${response.body[i].test_center_province_text[this.props.currentLanguage][0].text},
            ${response.body[i].postal_code},
            ${
              this.props.currentLanguage === LANGUAGES.english
                ? response.body[i].country_eabrv
                : response.body[i].country_fabrv
            })`,
            value: response.body[i].id
          });
        }
        this.setState({
          testCenterSelectedOption: testCenterSelectedOption,
          testCenterOptions: testCenterOptions,
          isLoadingTestCenterOptions: false
        });
      });
    });
  };

  getActiveNonPublictests = () => {
    this.setState({ isLoadingTestToAdministerOptions: true }, () => {
      // getting active non public tests
      this.props
        .getActiveNonPublicTests(
          this.state.selectedPendingAlternateTestRequestData.test_skill_type_id,
          this.state.selectedPendingAlternateTestRequestData.test_skill_sub_type_id
        )
        .then(response => {
          // initializing testToAdministerSelectedOption
          let testToAdministerSelectedOption = [];
          let definedTestToAdministerObj = false;
          // checking if the test_to_administer object is defined in the selectedPendingAlternateTestRequestData state
          if (Object.keys(this.state.selectedPendingAlternateTestRequestData).length > 0) {
            definedTestToAdministerObj = true;
          }
          // initializing testToAdministerOptions
          const testToAdministerOptions = [];
          // looping in response
          for (let i = 0; i < response.length; i++) {
            // if test_to_administer is defined (from selectedPendingAlternateTestRequestData state) + matching test ID of current iteration
            if (
              definedTestToAdministerObj &&
              this.state.selectedPendingAlternateTestRequestData.test_to_administer.test ===
                response[i].id
            ) {
              // setting testToAdministerSelectedOption
              testToAdministerSelectedOption = {
                label: `${response[i].test_code} - ${
                  response[i][`${this.props.currentLanguage}_name`]
                } (v${response[i].version})`,
                value: response[i].id
              };
            }
            testToAdministerOptions.push({
              label: `${response[i].test_code} - ${
                response[i][`${this.props.currentLanguage}_name`]
              } (v${response[i].version})`,
              value: response[i].id
            });
          }
          this.setState(
            {
              testToAdministerSelectedOption: testToAdministerSelectedOption,
              testToAdministerOptions: testToAdministerOptions,
              isLoadingTestToAdministerOptions: false
            },
            () => {
              // getting submit button disabled state
              this.getSubmitButtonDisabledState();
            }
          );
        });
    });
  };

  getLocationSelectedOption = selectedOption => {
    this.setState({ testCenterSelectedOption: selectedOption }, () => {
      // getting submit button disabled state
      this.getSubmitButtonDisabledState();
    });
  };

  handleSwitchStateUpdate = event => {
    this.setState({ testCannotBePerformedInCatSwitchState: event }, () => {
      // getting submit button disabled state
      this.getSubmitButtonDisabledState();
    });
  };

  getTestToAdministerSelectedOption = selectedOption => {
    this.setState({ testToAdministerSelectedOption: selectedOption }, () => {
      // getting submit button disabled state
      this.getSubmitButtonDisabledState();
    });
  };

  handleSpecifiedTestDescriptionContentUpdate = event => {
    this.setState(
      {
        specifiedTestDescription: event.target.value,
        isValidSpecifiedTestDescription: event.target.value !== ""
      },
      () => {
        // getting submit button disabled state
        this.getSubmitButtonDisabledState();
      }
    );
  };

  handleRationaleContentUpdate = event => {
    this.setState(
      {
        rationale: event.target.value
      },
      () => {
        // getting submit button disabled state
        this.getSubmitButtonDisabledState();
      }
    );
  };

  closeViewSelectedPendingAlternateTestRequestPopup = () => {
    this.setState({
      selectedPendingAlternateTestRequestData: {},
      showViewSelectedPendingAlternateTestRequestPopup: false,
      showSelectedPendingAlternateTestRequestSentSuccessfullyPopup: false,
      testToAdministerOptions: [],
      testToAdministerSelectedOption: [],
      testCannotBePerformedInCatSwitchState: false,
      isValidSpecifiedTestDescription: true,
      specifiedTestDescription: "",
      rationale: "",
      submitButtonDisabledState: true
    });
  };

  openCancelRequestConfirmationPopup = () => {
    this.setState({ showCancelRequestConfirmationPopup: true });
  };

  closeCancelRequestConfirmationPopup = () => {
    this.setState({ showCancelRequestConfirmationPopup: false });
  };

  getSubmitButtonDisabledState = () => {
    // initializing submitButtonDisabledState
    let submitButtonDisabledState = true;

    // switch is checked (testCannotBePerformedInCatSwitchState is set to true)
    if (this.state.testCannotBePerformedInCatSwitchState) {
      // no test center selected option (not UIT) ==> disabled
      // specifiedTestDescription === "" ==> disabled
      // no rationale defined ==> disabled
      submitButtonDisabledState =
        (!this.state.selectedPendingAlternateTestRequestData.is_uit &&
          Object.keys(this.state.testCenterSelectedOption).length <= 0) ||
        this.state.specifiedTestDescription === "" ||
        this.state.rationale === "";
      // switch is not checked (testCannotBePerformedInCatSwitchState is set to false)
    } else {
      // no test center selected option (not UIT) ==> disabled
      // no test to administer selected option ==> disabled
      // no rationale defined ==> disabled
      submitButtonDisabledState =
        (!this.state.selectedPendingAlternateTestRequestData.is_uit &&
          Object.keys(this.state.testCenterSelectedOption).length <= 0) ||
        Object.keys(this.state.testToAdministerSelectedOption).length <= 0 ||
        this.state.rationale === "";
    }
    this.setState({ submitButtonDisabledState: submitButtonDisabledState });
  };

  handleSubmitPendingAlternateTestRequest = () => {
    // creating body
    const body = {
      id: this.state.selectedPendingAlternateTestRequestData.id,
      test_center_id: !this.state.selectedPendingAlternateTestRequestData.is_uit
        ? this.state.testCenterSelectedOption.value
        : null,
      test_id:
        // test provided in CAT
        !this.state.testCannotBePerformedInCatSwitchState
          ? this.state.testToAdministerSelectedOption.value
          : // test cannot be provided in CAT
            null,
      specified_test_description: this.state.specifiedTestDescription,
      rationale: this.state.rationale
    };
    this.props.submitPendingAlternateTestRequest(body).then(response => {
      // success
      if (response.ok) {
        // show success popup
        this.setState({ showSelectedPendingAlternateTestRequestSentSuccessfullyPopup: true });
        // populating/updating table
        this.populatePendingAlternateTestRequestsBasedOnActiveSearch();
        // should never happen
      } else {
        throw new Error("An error occurred during the submit pending alternate test request");
      }
    });
  };

  handleCancelRequestAction = () => {
    this.setState({ actionInProgress: true }, () => {
      // creating body
      const body = {
        id: this.state.selectedPendingAlternateTestRequestData.id,
        rationale: this.state.rationale
      };
      this.props.cancelPendingAlternateTestRequest(body).then(response => {
        // success
        if (response.ok) {
          // closing popups
          this.closeViewSelectedPendingAlternateTestRequestPopup();
          setTimeout(() => {
            this.closeCancelRequestConfirmationPopup();
            // populating table
            this.populatePendingAlternateTestRequestsBasedOnActiveSearch();
          }, 100);
          this.setState({ actionInProgress: false });
          // should never happen
        } else {
          throw new Error("An error occurred during the cancel pending alternate test request");
        }
      });
    });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests.table.columnOne,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests.table.columnTwo,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests.table.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests.table.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests.table.columnFive,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests.table.columnSix,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests.table.columnSeven,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests.table.columnHeight,
        style: { ...COMMON_STYLE.CENTERED_TEXT, ...{ width: "15%" } }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <>
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"pending-alternate-test-requests"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updatePendingAlternateTestRequestsSearchParametersStates}
            updatePageState={this.props.updatePendingAlternateTestRequestsPageState}
            paginationPageSize={Number(this.props.pendingAlternateTestRequestsPageSize)}
            updatePaginationPageSize={this.props.updatePendingAlternateTestRequestsPageSizeState}
            data={this.state.pendingAlternateTestRequests}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.pendingAlternateTestRequestsKeyword}
          />
          <GenericTable
            classnamePrefix="pending-alternate-test-requests"
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={
              LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests.table.noData
            }
            currentlyLoading={this.state.currentlyLoading}
          />
          <Pagination
            paginationContainerId={"pending-alternate-test-requests-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.pendingAlternateTestRequestsPage}
            updatePaginationPageState={this.props.updatePendingAlternateTestRequestsPageState}
            firstTableRowId={"pending-alternate-test-requests-table-row-0"}
          />
        </div>
        {Object.keys(this.state.selectedPendingAlternateTestRequestData).length > 0 && (
          <>
            <PopupBox
              show={this.state.showViewSelectedPendingAlternateTestRequestPopup}
              title={
                !this.state.showSelectedPendingAlternateTestRequestSentSuccessfullyPopup
                  ? LOCALIZE.formatString(
                      LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                        .viewSelectedPendingAlternateTestRequestPopup.title,
                      this.state.selectedPendingAlternateTestRequestData.user_first_name,
                      this.state.selectedPendingAlternateTestRequestData.user_last_name
                    )
                  : LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                      .viewSelectedPendingAlternateTestRequestPopup.titleSuccess
              }
              handleClose={this.closeViewSelectedPendingAlternateTestRequestPopup}
              closeOnButtonAction={false}
              shouldCloseOnEsc={true}
              closeButtonAction={this.closeViewSelectedPendingAlternateTestRequestPopup}
              displayCloseButton={
                !this.state.showSelectedPendingAlternateTestRequestSentSuccessfullyPopup
              }
              isBackdropStatic={true}
              size={
                !this.state.showSelectedPendingAlternateTestRequestSentSuccessfullyPopup
                  ? "xl"
                  : "lg"
              }
              description={
                <>
                  {!this.state.showSelectedPendingAlternateTestRequestSentSuccessfullyPopup ? (
                    <div>
                      <p>
                        <div>
                          {LOCALIZE.formatString(
                            LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                              .viewSelectedPendingAlternateTestRequestPopup.description1,
                            <span style={styles.boldText}>
                              {this.state.selectedPendingAlternateTestRequestData.user_first_name}
                            </span>,
                            <span style={styles.boldText}>
                              {this.state.selectedPendingAlternateTestRequestData.user_last_name}
                            </span>,
                            <span style={styles.boldText}>
                              {this.state.selectedPendingAlternateTestRequestData.user_email}
                            </span>
                          )}
                        </div>
                        <div>
                          {LOCALIZE.formatString(
                            LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                              .viewSelectedPendingAlternateTestRequestPopup.description1continued,
                            <span style={styles.boldText}>
                              {Object.keys(
                                this.state.selectedPendingAlternateTestRequestData
                                  .test_to_administer
                              ).length > 0
                                ? this.state.selectedPendingAlternateTestRequestData
                                    .test_to_administer[`test_name_${this.props.currentLanguage}`]
                                : this.state.selectedPendingAlternateTestRequestData
                                    .formatted_test_skill}
                            </span>
                          )}
                        </div>
                      </p>
                      <p>
                        {
                          LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                            .viewSelectedPendingAlternateTestRequestPopup.description2
                        }
                      </p>
                      <div style={styles.formContainer}>
                        {/* Supervised Test */}
                        {!this.state.selectedPendingAlternateTestRequestData.is_uit && (
                          <Row className="align-items-center" style={styles.fieldRowContainer}>
                            <Col
                              xl={columnSizes.popupFirstColumn.xl}
                              lg={columnSizes.popupFirstColumn.lg}
                              md={columnSizes.popupFirstColumn.md}
                              sm={columnSizes.popupFirstColumn.sm}
                              xs={columnSizes.popupFirstColumn.xs}
                            >
                              <label
                                id="view-selected-pending-alternate-test-request-location-label"
                                style={styles.standardLabel}
                              >
                                {
                                  LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                                    .viewSelectedPendingAlternateTestRequestPopup.locationLabel
                                }
                              </label>
                            </Col>
                            <Col
                              xl={columnSizes.popupSecondColumn.xl}
                              lg={columnSizes.popupSecondColumn.lg}
                              md={columnSizes.popupSecondColumn.md}
                              sm={columnSizes.popupSecondColumn.sm}
                              xs={columnSizes.popupSecondColumn.xs}
                            >
                              {!this.state.isLoadingTestCenterOptions ? (
                                <DropdownSelect
                                  idPrefix="view-selected-pending-alternate-test-request-location"
                                  ariaLabelledBy="view-selected-pending-alternate-test-request-location-label"
                                  ariaRequired={true}
                                  hasPlaceholder={true}
                                  options={this.state.testCenterOptions}
                                  onChange={this.getLocationSelectedOption}
                                  defaultValue={this.state.testCenterSelectedOption}
                                />
                              ) : (
                                <div style={styles.loadingContainer}>
                                  <label className="fa fa-spinner fa-spin">
                                    <FontAwesomeIcon icon={faSpinner} />
                                  </label>
                                </div>
                              )}
                            </Col>
                          </Row>
                        )}
                        <Row style={styles.fieldRowContainer}>
                          <Col
                            xl={columnSizes.popupFirstColumn.xl}
                            lg={columnSizes.popupFirstColumn.lg}
                            md={columnSizes.popupFirstColumn.md}
                            sm={columnSizes.popupFirstColumn.sm}
                            xs={columnSizes.popupFirstColumn.xs}
                          >
                            <label
                              htmlFor="view-selected-pending-alternate-test-request-test-administered-details-input"
                              style={styles.standardLabel}
                            >
                              {
                                LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                                  .viewSelectedPendingAlternateTestRequestPopup
                                  .testAdministeredDetailsLabel
                              }
                            </label>
                          </Col>
                          <Col
                            xl={columnSizes.popupSecondColumn.xl}
                            lg={columnSizes.popupSecondColumn.lg}
                            md={columnSizes.popupSecondColumn.md}
                            sm={columnSizes.popupSecondColumn.sm}
                            xs={columnSizes.popupSecondColumn.xs}
                          >
                            <textarea
                              id="view-selected-pending-alternate-test-request-test-administered-details-input"
                              className={"valid-field input-disabled-look"}
                              style={{ ...styles.textAreaInput, ...accommodationsStyle }}
                              onChange={() => {}}
                              value={this.state.selectedPendingAlternateTestRequestData.comments}
                              maxLength="5000"
                            />
                          </Col>
                        </Row>
                        <Row className="align-items-center" style={styles.fieldRowContainer}>
                          <Col
                            xl={columnSizes.popupFirstColumn.xl}
                            lg={columnSizes.popupFirstColumn.lg}
                            md={columnSizes.popupFirstColumn.md}
                            sm={columnSizes.popupFirstColumn.sm}
                            xs={columnSizes.popupFirstColumn.xs}
                          >
                            <label
                              id="view-selected-pending-alternate-test-request-recommendations-label"
                              style={styles.standardLabel}
                            >
                              {
                                LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                                  .viewSelectedPendingAlternateTestRequestPopup.recommendationsLabel
                              }
                            </label>
                          </Col>
                          <Col
                            xl={columnSizes.popupSecondColumn.xl}
                            lg={columnSizes.popupSecondColumn.lg}
                            md={columnSizes.popupSecondColumn.md}
                            sm={columnSizes.popupSecondColumn.sm}
                            xs={columnSizes.popupSecondColumn.xs}
                          ></Col>
                        </Row>
                        <Row className="align-items-center" style={styles.fieldRowContainer}>
                          <Col
                            xl={columnSizes.popupFirstColumn.xl}
                            lg={columnSizes.popupFirstColumn.lg}
                            md={columnSizes.popupFirstColumn.md}
                            sm={columnSizes.popupFirstColumn.sm}
                            xs={columnSizes.popupFirstColumn.xs}
                          >
                            <label
                              htmlFor="view-selected-pending-alternate-test-request-cannot-be-performed-in-cat-switch"
                              style={styles.indentedLabel}
                            >
                              {
                                LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                                  .viewSelectedPendingAlternateTestRequestPopup
                                  .testCannotBePerformedInCatLabel
                              }
                            </label>
                          </Col>
                          <Col
                            xl={columnSizes.popupSecondColumn.xl}
                            lg={columnSizes.popupSecondColumn.lg}
                            md={columnSizes.popupSecondColumn.md}
                            sm={columnSizes.popupSecondColumn.sm}
                            xs={columnSizes.popupSecondColumn.xs}
                          >
                            {!this.state.isLoadingSwitch && (
                              <Switch
                                id={
                                  "view-selected-pending-alternate-test-request-cannot-be-performed-in-cat-switch"
                                }
                                onChange={e => {
                                  this.handleSwitchStateUpdate(e);
                                }}
                                checked={this.state.testCannotBePerformedInCatSwitchState}
                                height={this.state.switchHeight}
                                width={this.state.switchWidth}
                              />
                            )}
                          </Col>
                        </Row>
                        {!this.state.testCannotBePerformedInCatSwitchState ? (
                          <Row className="align-items-center" style={styles.fieldRowContainer}>
                            <Col
                              xl={columnSizes.popupFirstColumn.xl}
                              lg={columnSizes.popupFirstColumn.lg}
                              md={columnSizes.popupFirstColumn.md}
                              sm={columnSizes.popupFirstColumn.sm}
                              xs={columnSizes.popupFirstColumn.xs}
                            >
                              <label
                                id="view-selected-pending-alternate-test-request-test-to-administer-label"
                                style={styles.indentedLabel}
                              >
                                {
                                  LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                                    .viewSelectedPendingAlternateTestRequestPopup
                                    .testToAdministerLabel
                                }
                              </label>
                            </Col>
                            <Col
                              xl={columnSizes.popupSecondColumn.xl}
                              lg={columnSizes.popupSecondColumn.lg}
                              md={columnSizes.popupSecondColumn.md}
                              sm={columnSizes.popupSecondColumn.sm}
                              xs={columnSizes.popupSecondColumn.xs}
                            >
                              {!this.state.isLoadingTestToAdministerOptions ? (
                                <DropdownSelect
                                  idPrefix="view-selected-pending-alternate-test-request-test-to-administer"
                                  ariaLabelledBy="view-selected-pending-alternate-test-request-recommendations-label view-selected-pending-alternate-test-request-test-to-administer-label"
                                  ariaRequired={true}
                                  options={this.state.testToAdministerOptions}
                                  onChange={this.getTestToAdministerSelectedOption}
                                  defaultValue={this.state.testToAdministerSelectedOption}
                                  hasPlaceholder={true}
                                />
                              ) : (
                                <div style={styles.loadingContainer}>
                                  <label className="fa fa-spinner fa-spin">
                                    <FontAwesomeIcon icon={faSpinner} />
                                  </label>
                                </div>
                              )}
                            </Col>
                          </Row>
                        ) : (
                          <>
                            <Row
                              className="align-items-center"
                              style={
                                this.state.isValidSpecifiedTestDescription
                                  ? styles.fieldRowContainer
                                  : styles.fieldRowContainerWithError
                              }
                            >
                              <Col
                                xl={columnSizes.popupFirstColumn.xl}
                                lg={columnSizes.popupFirstColumn.lg}
                                md={columnSizes.popupFirstColumn.md}
                                sm={columnSizes.popupFirstColumn.sm}
                                xs={columnSizes.popupFirstColumn.xs}
                              >
                                <label
                                  htmlFor="view-selected-pending-alternate-specified-test-request-test-description-input"
                                  style={styles.indentedLabel}
                                >
                                  {
                                    LOCALIZE.systemAdministrator.alternateTestRequests
                                      .pendingRequests.viewSelectedPendingAlternateTestRequestPopup
                                      .specifiedTestDescriptionLabel
                                  }
                                </label>
                              </Col>
                              <Col
                                xl={columnSizes.popupSecondColumn.xl}
                                lg={columnSizes.popupSecondColumn.lg}
                                md={columnSizes.popupSecondColumn.md}
                                sm={columnSizes.popupSecondColumn.sm}
                                xs={columnSizes.popupSecondColumn.xs}
                              >
                                <input
                                  id="view-selected-pending-alternate-test-request-specified-test-description-input"
                                  tabIndex={0}
                                  className={
                                    this.state.isValidSpecifiedTestDescription
                                      ? "valid-field"
                                      : "invalid-field"
                                  }
                                  type="text"
                                  onChange={this.handleSpecifiedTestDescriptionContentUpdate}
                                  value={this.state.specifiedTestDescription}
                                  style={{
                                    ...styles.input,
                                    ...accommodationsStyle
                                  }}
                                />
                              </Col>
                            </Row>
                            {!this.state.isValidSpecifiedTestDescription && (
                              <Row
                                className="align-items-center"
                                style={styles.rowContainerWithError}
                              >
                                <Col
                                  xl={columnSizes.popupFirstColumn.xl}
                                  lg={columnSizes.popupFirstColumn.lg}
                                  md={columnSizes.popupFirstColumn.md}
                                  sm={columnSizes.popupFirstColumn.sm}
                                  xs={columnSizes.popupFirstColumn.xs}
                                ></Col>
                                <Col
                                  xl={columnSizes.popupSecondColumn.xl}
                                  lg={columnSizes.popupSecondColumn.lg}
                                  md={columnSizes.popupSecondColumn.md}
                                  sm={columnSizes.popupSecondColumn.sm}
                                  xs={columnSizes.popupSecondColumn.xs}
                                >
                                  <label
                                    htmlFor="view-selected-pending-alternate-test-request-specified-test-description-input"
                                    style={styles.errorMessage}
                                  >
                                    {
                                      LOCALIZE.systemAdministrator.alternateTestRequests
                                        .pendingRequests
                                        .viewSelectedPendingAlternateTestRequestPopup
                                        .specifiedTestDescriptionError
                                    }
                                  </label>
                                </Col>
                              </Row>
                            )}
                          </>
                        )}
                      </div>
                      <Row style={styles.fieldRowContainer}>
                        <Col
                          xl={columnSizes.popupFirstColumn.xl}
                          lg={columnSizes.popupFirstColumn.lg}
                          md={columnSizes.popupFirstColumn.md}
                          sm={columnSizes.popupFirstColumn.sm}
                          xs={columnSizes.popupFirstColumn.xs}
                        >
                          <label
                            htmlFor="view-selected-pending-alternate-specified-test-request-rationale-input"
                            style={styles.standardLabel}
                          >
                            {
                              LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                                .viewSelectedPendingAlternateTestRequestPopup.rationaleLabel
                            }
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.popupSecondColumn.xl}
                          lg={columnSizes.popupSecondColumn.lg}
                          md={columnSizes.popupSecondColumn.md}
                          sm={columnSizes.popupSecondColumn.sm}
                          xs={columnSizes.popupSecondColumn.xs}
                        >
                          <textarea
                            id="view-selected-pending-alternate-specified-test-request-rationale-input"
                            className={"valid-field"}
                            style={{ ...styles.textAreaInput, ...accommodationsStyle }}
                            onChange={this.handleRationaleContentUpdate}
                            value={this.state.rationale}
                            maxLength="5000"
                          />
                        </Col>
                      </Row>
                    </div>
                  ) : (
                    <SystemMessage
                      messageType={MESSAGE_TYPE.success}
                      title={LOCALIZE.commons.success}
                      message={
                        LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                          .viewSelectedPendingAlternateTestRequestPopup
                          .successSystemMessageDescription
                      }
                    />
                  )}
                </>
              }
              leftButtonType={
                !this.state.showSelectedPendingAlternateTestRequestSentSuccessfullyPopup
                  ? BUTTON_TYPE.danger
                  : BUTTON_TYPE.none
              }
              leftButtonTitle={
                LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                  .viewSelectedPendingAlternateTestRequestPopup.cancelRequestButton
              }
              leftButtonIcon={
                !this.state.showSelectedPendingAlternateTestRequestSentSuccessfullyPopup
                  ? faTrash
                  : ""
              }
              leftButtonAction={
                !this.state.showSelectedPendingAlternateTestRequestSentSuccessfullyPopup
                  ? this.openCancelRequestConfirmationPopup
                  : () => {}
              }
              leftButtonState={
                this.state.rationale !== "" ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
              }
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={
                !this.state.showSelectedPendingAlternateTestRequestSentSuccessfullyPopup
                  ? LOCALIZE.commons.submit
                  : LOCALIZE.commons.close
              }
              rightButtonIcon={
                !this.state.showSelectedPendingAlternateTestRequestSentSuccessfullyPopup
                  ? faShare
                  : faTimes
              }
              rightButtonAction={
                !this.state.showSelectedPendingAlternateTestRequestSentSuccessfullyPopup
                  ? this.handleSubmitPendingAlternateTestRequest
                  : this.closeViewSelectedPendingAlternateTestRequestPopup
              }
              rightButtonState={
                !this.state.showSelectedPendingAlternateTestRequestSentSuccessfullyPopup
                  ? this.state.submitButtonDisabledState
                    ? BUTTON_STATE.disabled
                    : BUTTON_STATE.enabled
                  : BUTTON_STATE.enabled
              }
            />
            <PopupBox
              show={this.state.showCancelRequestConfirmationPopup}
              title={
                LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                  .cancelRequestConfirmationPopup.title
              }
              handleClose={() => {}}
              size={"lg"}
              description={
                <div>
                  <p>
                    {
                      <SystemMessage
                        messageType={MESSAGE_TYPE.error}
                        title={LOCALIZE.commons.warning}
                        message={
                          LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                            .cancelRequestConfirmationPopup.systemMessageDescription
                        }
                      />
                    }
                  </p>
                  <p>
                    {
                      LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                        .cancelRequestConfirmationPopup.description
                    }
                  </p>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonIcon={faTimes}
              leftButtonAction={this.closeCancelRequestConfirmationPopup}
              rightButtonType={BUTTON_TYPE.danger}
              rightButtonTitle={
                this.state.actionInProgress ? (
                  <div style={styles.customLoadingContainer}>
                    {/* this div is useful to get the right size of the button while loading */}
                    <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
                      <FontAwesomeIcon icon={faTrash} style={styles.icon} />
                      {
                        LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                          .viewSelectedPendingAlternateTestRequestPopup.cancelRequestButton
                      }
                    </div>
                    <div style={styles.loadingOverlappingStyle}>
                      <label className="fa fa-spinner fa-spin">
                        <FontAwesomeIcon icon={faSpinner} />
                      </label>
                    </div>
                  </div>
                ) : (
                  LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests
                    .viewSelectedPendingAlternateTestRequestPopup.cancelRequestButton
                )
              }
              rightButtonIcon={this.state.actionInProgress ? "" : faTrash}
              rightButtonAction={this.handleCancelRequestAction}
              rightButtonState={
                this.state.actionInProgress ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
              }
            />
          </>
        )}
      </>
    );
  }
}

export { PendingAlternateTestRequests as unconnectedPendingAlternateTestRequests };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    pendingAlternateTestRequestsPage: state.alternateTestRequests.pendingAlternateTestRequestsPage,
    pendingAlternateTestRequestsPageSize:
      state.alternateTestRequests.pendingAlternateTestRequestsPageSize,
    pendingAlternateTestRequestsKeyword:
      state.alternateTestRequests.pendingAlternateTestRequestsKeyword,
    pendingAlternateTestRequestsActiveSearch:
      state.alternateTestRequests.pendingAlternateTestRequestsActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updatePendingAlternateTestRequestsSearchParametersStates,
      updatePendingAlternateTestRequestsPageState,
      updatePendingAlternateTestRequestsPageSizeState,
      getAllPendingAlternateTestRequests,
      getFoundPendingAlternateTestRequests,
      getSelectedUserAccommodationFileData,
      getActiveNonPublicTests,
      submitPendingAlternateTestRequest,
      getTestCentersForUserAccommodationRequest,
      cancelPendingAlternateTestRequest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(PendingAlternateTestRequests);
