import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import PendingAlternateTestRequests from "./PendingAlternateTestRequests";
import { setAlternateTestRequestsSelectedTopTabState } from "../../../modules/AlternateTestRequestsRedux";
import CompletedAlternateTestRequests from "./CompletedAlternateTestRequests";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  }
};

class AlternateTestRequests extends Component {
  state = {
    selectedTab: ""
  };

  render() {
    const TABS = [
      {
        key: "pending-alternate-test-requests",
        tabName: LOCALIZE.systemAdministrator.alternateTestRequests.pendingRequests.title,
        body: <PendingAlternateTestRequests />
      },
      {
        key: "completed-alternate-test-requests",
        tabName: LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests.title,
        body: <CompletedAlternateTestRequests />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.systemAdministrator.alternateTestRequests.title}</h2>
          <p>{LOCALIZE.systemAdministrator.alternateTestRequests.description}</p>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey={
                  this.props.alternateTestRequestsSelectedTopTab !== "" &&
                  this.props.alternateTestRequestsSelectedTopTab !== null
                    ? this.props.alternateTestRequestsSelectedTopTab
                    : "pending-alternate-test-requests"
                }
                id="alternate-test-requests-tabs"
                style={styles.tabNavigation}
                onSelect={event => this.props.setAlternateTestRequestsSelectedTopTabState(event)}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export { AlternateTestRequests as unconnectedAlternateTestRequests };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    alternateTestRequestsSelectedTopTab:
      state.alternateTestRequests.alternateTestRequestsSelectedTopTab
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setAlternateTestRequestsSelectedTopTabState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AlternateTestRequests);
