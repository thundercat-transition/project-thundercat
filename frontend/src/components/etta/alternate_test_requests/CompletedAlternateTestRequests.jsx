import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars, faTimes } from "@fortawesome/free-solid-svg-icons";
import StyledTooltip, { TYPE, EFFECT } from "../../authentication/StyledTooltip";
import "../../../css/etta.css";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import { Col, Row } from "react-bootstrap";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import Pagination from "../../commons/Pagination";
import {
  getAllCompletedAlternateTestRequests,
  getFoundCompletedAlternateTestRequests,
  updateCompletedAlternateTestRequestsPageSizeState,
  updateCompletedAlternateTestRequestsPageState,
  updateCompletedAlternateTestRequestsSearchParametersStates
} from "../../../modules/AlternateTestRequestsRedux";
import getSwitchTransformScale from "../../../helpers/switchTransformScale";
import {
  getSelectedUserAccommodationFileData,
  getTestCentersForUserAccommodationRequest
} from "../../../modules/AaeRedux";
import DropdownSelect from "../../commons/DropdownSelect";
import Switch from "react-switch";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import { LANGUAGES } from "../../commons/Translation";

const columnSizes = {
  popupFirstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  popupSecondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  buttonIcon: {
    marginRight: 0
  },
  formContainer: {
    width: "100%",
    margin: "18px auto"
  },
  fieldRowContainer: {
    margin: "12px auto",
    width: "85%"
  },
  textAreaInput: {
    width: "100%",
    height: 115,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block"
  },
  standardLabel: {
    fontWeight: "bold",
    margin: 0
  },
  indentedLabel: {
    marginLeft: 24,
    marginBottom: 0,
    fontWeight: "bold"
  },
  input: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  boldText: {
    fontWeight: "bold"
  },
  allUnset: {
    all: "unset"
  }
};

class CompletedAlternateTestRequests extends Component {
  state = {
    clearSearchTriggered: false,
    resultsFound: 0,
    completedAlternateTestRequests: [],
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    currentlyLoading: false,
    isLoadingDataToView: false,
    selectedCompletedAlternateTestRequestData: {},
    showViewSelectedCompletedAlternateTestRequestPopup: false,
    // default values
    switchHeight: 25,
    switchWidth: 50
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCompletedAlternateTestRequestsPageState(1);
    this.populateCompletedAlternateTestRequestsBasedOnActiveSearch();
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateCompletedAlternateTestRequestsBasedOnActiveSearch();
    }
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
    // if completedAlternateTestRequestsPage gets updated
    if (
      prevProps.completedAlternateTestRequestsPage !== this.props.completedAlternateTestRequestsPage
    ) {
      this.populateCompletedAlternateTestRequestsBasedOnActiveSearch();
    }
    // if completedAlternateTestRequestsPageSize get updated
    if (
      prevProps.completedAlternateTestRequestsPageSize !==
      this.props.completedAlternateTestRequestsPageSize
    ) {
      this.populateCompletedAlternateTestRequestsBasedOnActiveSearch();
    }
    // if search completedAlternateTestRequestsKeyword gets updated
    if (
      prevProps.completedAlternateTestRequestsKeyword !==
      this.props.completedAlternateTestRequestsKeyword
    ) {
      // if completedAlternateTestRequestsKeyword redux state is empty
      if (this.props.completedAlternateTestRequestsKeyword !== "") {
        this.populateFoundCompletedAlternateTestRequests();
      } else if (
        this.props.completedAlternateTestRequestsKeyword === "" &&
        this.props.completedAlternateTestRequestsActiveSearch
      ) {
        this.populateFoundCompletedAlternateTestRequests();
      }
    }
    // if completedAlternateTestRequestsActiveSearch gets updated
    if (
      prevProps.completedAlternateTestRequestsActiveSearch !==
      this.props.completedAlternateTestRequestsActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.completedAlternateTestRequestsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateAllCompletedAlternateTestRequests();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundCompletedAlternateTestRequests();
      }
    }
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoadingSwitch: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoadingSwitch: false });
        }
      );
    });
  };

  getFormattedTestSkill = userAccommodationFileData => {
    // formatting test skill
    const formattedTestSkill =
      userAccommodationFileData.test_skill_sub_type_id !== null
        ? `${userAccommodationFileData[`test_skill_type_name_${this.props.currentLanguage}`]} - ${
            userAccommodationFileData[`test_skill_sub_type_name_${this.props.currentLanguage}`]
          }`
        : `${userAccommodationFileData[`test_skill_type_name_${this.props.currentLanguage}`]}`;

    return formattedTestSkill;
  };

  populateCompletedAlternateTestRequestsBasedOnActiveSearch = () => {
    // current search
    if (this.props.completedAlternateTestRequestsActiveSearch) {
      this.populateFoundCompletedAlternateTestRequests();
      // no search
    } else {
      this.populateAllCompletedAlternateTestRequests();
    }
  };

  populateAllCompletedAlternateTestRequests = () => {
    this.setState({ currentlyLoading: true }, () => {
      const completedAlternateTestRequests = [];
      this.props
        .getAllCompletedAlternateTestRequests(
          this.props.completedAlternateTestRequestsPage,
          this.props.completedAlternateTestRequestsPageSize
        )
        .then(response => {
          this.populateCompletedAlternateTestRequestsObject(
            completedAlternateTestRequests,
            response
          );
        })
        .then(() => {
          this.setState({ currentlyLoading: false });
        });
    });
  };

  populateFoundCompletedAlternateTestRequests = () => {
    // making sure that the table is not currently loading
    if (!this.state.currentlyLoading) {
      this.setState({ currentlyLoading: true }, () => {
        const completedAlternateTestRequests = [];
        setTimeout(() => {
          this.props
            .getFoundCompletedAlternateTestRequests(
              this.props.completedAlternateTestRequestsKeyword,
              this.props.currentLanguage,
              this.props.completedAlternateTestRequestsPage,
              this.props.completedAlternateTestRequestsPageSize
            )
            .then(response => {
              if (response[0] === "no results found") {
                this.setState({
                  completedAlternateTestRequests: [],
                  rowsDefinition: {},
                  numberOfPages: 1,
                  nextPageNumber: 0,
                  previousPageNumber: 0,
                  resultsFound: 0
                });
                // there is at least one result found
              } else {
                this.populateCompletedAlternateTestRequestsObject(
                  completedAlternateTestRequests,
                  response
                );
                this.setState({ resultsFound: response.count });
              }
            })
            .then(() => {
              this.setState(
                {
                  currentlyLoading: false,
                  displayResultsFound: true
                },
                () => {
                  // make sure that this element exsits to avoid any error
                  if (document.getElementById("completed-alternate-test-requests-results-found")) {
                    document
                      .getElementById("completed-alternate-test-requests-results-found")
                      .focus();
                  }
                }
              );
            });
        }, 100);
      });
    }
  };

  populateCompletedAlternateTestRequestsObject = (completedAlternateTestRequests, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        // getting formatted test s;
        const currentResult = response.results[i];
        // pushing needed data in completedAlternateTestRequests array
        completedAlternateTestRequests.push(currentResult);
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult.id,
          column_2: currentResult.user_id,
          column_3: `${currentResult.user_last_name}, ${currentResult.user_first_name}`,
          column_4: currentResult.created_date,
          column_5: currentResult.process_end_date,
          column_6:
            currentResult.test_center_id !== null
              ? currentResult.test_center_name
              : `${LOCALIZE.commons.na} (${LOCALIZE.commons.testType.uit})`,
          column_7: this.getFormattedTestSkill(currentResult),
          column_8: (
            <div className="d-flex justify-content-center flex-nowrap">
              <StyledTooltip
                id={`completed-alternate-test-requests-view-button-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`completed-alternate-test-requests-view-button-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faBinoculars} style={styles.buttonIcon} />
                        </>
                      }
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests.table
                          .viewTooltipAccessibility,
                        currentResult.user_first_name,
                        currentResult.user_last_name
                      )}
                      action={() => this.openViewSelectedCompletedAlternateTestRequestPopup(i)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.SECONDARY}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests.table
                          .viewTooltip
                      }
                    </p>
                  </div>
                }
              />
            </div>
          )
        });
      }
    }
    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.CENTERED_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      column_6_style: COMMON_STYLE.CENTERED_TEXT,
      column_7_style: COMMON_STYLE.CENTERED_TEXT,
      column_8_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };
    // saving results in state
    this.setState({
      completedAlternateTestRequests: completedAlternateTestRequests,
      rowsDefinition: rowsDefinition,
      previousPageNumber: response.previous_page_number
    });
  };

  openViewSelectedCompletedAlternateTestRequestPopup = index => {
    // if not currently loading (avoid multi-clicking actions)
    if (!this.state.isLoadingDataToView) {
      this.setState({ isLoadingDataToView: true }, () => {
        // getting selected user accommodation file data
        this.props
          .getSelectedUserAccommodationFileData(this.state.completedAlternateTestRequests[index].id)
          .then(response => {
            // setting selectedCompletedAlternateTestRequestData object
            const selectedCompletedAlternateTestRequestData = response;
            // adding formatted_test_skill attribute to the selectedCompletedAlternateTestRequestData object
            selectedCompletedAlternateTestRequestData.formatted_test_skill =
              this.getFormattedTestSkill(response);
            // supervised test (there is a test center defined)
            if (!response.is_uit) {
              // getting defined test center data
              this.props
                .getTestCentersForUserAccommodationRequest(response.test_center_id)
                .then(testCenterDataResponse => {
                  // adding new testCenterSelectedOption attribute to selectedCompletedAlternateTestRequestData object
                  selectedCompletedAlternateTestRequestData.testCenterSelectedOption = {
                    label: `${
                      testCenterDataResponse.body.test_center_city_text[
                        this.props.currentLanguage
                      ][0].text
                    } (${
                      testCenterDataResponse.body.test_center_address_text[
                        this.props.currentLanguage
                      ][0].text
                    }, ${
                      testCenterDataResponse.body.test_center_city_text[
                        this.props.currentLanguage
                      ][0].text
                    }, ${
                      testCenterDataResponse.body.test_center_province_text[
                        this.props.currentLanguage
                      ][0].text
                    },
                    ${testCenterDataResponse.body.postal_code},
                    ${
                      this.props.currentLanguage === LANGUAGES.english
                        ? testCenterDataResponse.body.country_eabrv
                        : testCenterDataResponse.body.country_fabrv
                    })`,
                    value: testCenterDataResponse.body.id
                  };
                  // updating needed states
                  this.setState({
                    selectedCompletedAlternateTestRequestData:
                      selectedCompletedAlternateTestRequestData,
                    showViewSelectedCompletedAlternateTestRequestPopup: true,
                    isLoadingDataToView: false
                  });
                });
            } else {
              // updating needed states
              this.setState({
                selectedCompletedAlternateTestRequestData:
                  selectedCompletedAlternateTestRequestData,
                showViewSelectedCompletedAlternateTestRequestPopup: true,
                isLoadingDataToView: false
              });
            }
          });
      });
    }
  };

  closeViewSelectedCompletedAlternateTestRequestPopup = () => {
    this.setState({
      selectedCompletedAlternateTestRequestData: {},
      showViewSelectedCompletedAlternateTestRequestPopup: false,
      testCannotBePerformedInCatSwitchState: false
    });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests.table.columnOne,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests.table.columnTwo,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests.table.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests.table.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests.table.columnFive,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests.table.columnSix,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests.table.columnSeven,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests.table.columnHeight,
        style: { ...COMMON_STYLE.CENTERED_TEXT, ...{ width: "15%" } }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div>
        <SearchBarWithDisplayOptions
          idPrefix={"completed-alternate-test-requests"}
          currentlyLoading={this.state.currentlyLoading}
          updateSearchStates={this.props.updateCompletedAlternateTestRequestsSearchParametersStates}
          updatePageState={this.props.updateCompletedAlternateTestRequestsPageState}
          paginationPageSize={Number(this.props.completedAlternateTestRequestsPageSize)}
          updatePaginationPageSize={this.props.updateCompletedAlternateTestRequestsPageSizeState}
          data={this.state.completedAlternateTestRequests}
          resultsFound={this.state.resultsFound}
          searchBarContent={this.props.completedAlternateTestRequestsKeyword}
        />
        <GenericTable
          classnamePrefix="completed-alternate-test-requests"
          columnsDefinition={columnsDefinition}
          rowsDefinition={this.state.rowsDefinition}
          emptyTableMessage={
            LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests.table.noData
          }
          currentlyLoading={this.state.currentlyLoading}
        />
        <Pagination
          paginationContainerId={"completed-alternate-test-requests-pagination-pages-link"}
          pageCount={this.state.numberOfPages}
          paginationPage={this.props.completedAlternateTestRequestsPage}
          updatePaginationPageState={this.props.updateCompletedAlternateTestRequestsPageState}
          firstTableRowId={"completed-alternate-test-requests-table-row-0"}
        />
        {Object.keys(this.state.selectedCompletedAlternateTestRequestData).length > 0 && (
          <>
            <PopupBox
              show={this.state.showViewSelectedCompletedAlternateTestRequestPopup}
              title={LOCALIZE.formatString(
                LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests
                  .viewSelectedCompletedAlternateTestRequestPopup.title,
                this.state.selectedCompletedAlternateTestRequestData.user_first_name,
                this.state.selectedCompletedAlternateTestRequestData.user_last_name
              )}
              handleClose={this.closeViewSelectedCompletedAlternateTestRequestPopup}
              closeOnButtonAction={false}
              shouldCloseOnEsc={true}
              closeButtonAction={this.closeViewSelectedCompletedAlternateTestRequestPopup}
              isBackdropStatic={true}
              description={
                <div>
                  <p>
                    <div>
                      {LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests
                          .viewSelectedCompletedAlternateTestRequestPopup.description1,
                        <span style={styles.boldText}>
                          {this.state.selectedCompletedAlternateTestRequestData.user_first_name}
                        </span>,
                        <span style={styles.boldText}>
                          {this.state.selectedCompletedAlternateTestRequestData.user_last_name}
                        </span>,
                        <span style={styles.boldText}>
                          {this.state.selectedCompletedAlternateTestRequestData.user_email}
                        </span>
                      )}
                    </div>
                    <div>
                      {LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests
                          .viewSelectedCompletedAlternateTestRequestPopup.description1continued,
                        <span style={styles.boldText}>
                          {Object.keys(
                            this.state.selectedCompletedAlternateTestRequestData.test_to_administer
                          ).length > 0
                            ? this.state.selectedCompletedAlternateTestRequestData
                                .test_to_administer[`test_name_${this.props.currentLanguage}`]
                            : this.state.selectedCompletedAlternateTestRequestData
                                .formatted_test_skill}
                        </span>
                      )}
                    </div>
                  </p>
                  <p>
                    {
                      LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests
                        .viewSelectedCompletedAlternateTestRequestPopup.description2
                    }
                  </p>
                  <div style={styles.formContainer}>
                    {this.state.selectedCompletedAlternateTestRequestData.test_center_name !==
                      null && (
                      <Row className="align-items-center" style={styles.fieldRowContainer}>
                        <Col
                          xl={columnSizes.popupFirstColumn.xl}
                          lg={columnSizes.popupFirstColumn.lg}
                          md={columnSizes.popupFirstColumn.md}
                          sm={columnSizes.popupFirstColumn.sm}
                          xs={columnSizes.popupFirstColumn.xs}
                        >
                          <label
                            id="view-selected-completed-alternate-test-request-location-label"
                            style={styles.standardLabel}
                          >
                            {
                              LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests
                                .viewSelectedCompletedAlternateTestRequestPopup.locationLabel
                            }
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.popupSecondColumn.xl}
                          lg={columnSizes.popupSecondColumn.lg}
                          md={columnSizes.popupSecondColumn.md}
                          sm={columnSizes.popupSecondColumn.sm}
                          xs={columnSizes.popupSecondColumn.xs}
                        >
                          <DropdownSelect
                            idPrefix="view-selected-completed-alternate-test-request-location"
                            ariaLabelledBy="view-selected-completed-alternate-test-request-location-label"
                            ariaRequired={true}
                            options={[]}
                            onChange={() => {}}
                            defaultValue={
                              this.state.selectedCompletedAlternateTestRequestData
                                .testCenterSelectedOption
                            }
                            isDisabled={true}
                          />
                        </Col>
                      </Row>
                    )}
                    <Row style={styles.fieldRowContainer}>
                      <Col
                        xl={columnSizes.popupFirstColumn.xl}
                        lg={columnSizes.popupFirstColumn.lg}
                        md={columnSizes.popupFirstColumn.md}
                        sm={columnSizes.popupFirstColumn.sm}
                        xs={columnSizes.popupFirstColumn.xs}
                      >
                        <label
                          htmlFor="view-selected-completed-alternate-test-request-comments-input"
                          style={styles.standardLabel}
                        >
                          {
                            LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests
                              .viewSelectedCompletedAlternateTestRequestPopup.commentsLabel
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.popupSecondColumn.xl}
                        lg={columnSizes.popupSecondColumn.lg}
                        md={columnSizes.popupSecondColumn.md}
                        sm={columnSizes.popupSecondColumn.sm}
                        xs={columnSizes.popupSecondColumn.xs}
                      >
                        <textarea
                          id="view-selected-completed-alternate-test-request-comments-input"
                          className={"valid-field input-disabled-look"}
                          style={{ ...styles.textAreaInput, ...accommodationsStyle }}
                          onChange={() => {}}
                          value={this.state.selectedCompletedAlternateTestRequestData.comments}
                          maxLength="5000"
                        />
                      </Col>
                    </Row>
                    <Row className="align-items-center" style={styles.fieldRowContainer}>
                      <Col
                        xl={columnSizes.popupFirstColumn.xl}
                        lg={columnSizes.popupFirstColumn.lg}
                        md={columnSizes.popupFirstColumn.md}
                        sm={columnSizes.popupFirstColumn.sm}
                        xs={columnSizes.popupFirstColumn.xs}
                      >
                        <label
                          id="view-selected-completed-alternate-test-request-recommendations-label"
                          style={styles.standardLabel}
                        >
                          {
                            LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests
                              .viewSelectedCompletedAlternateTestRequestPopup.recommendationsLabel
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.popupSecondColumn.xl}
                        lg={columnSizes.popupSecondColumn.lg}
                        md={columnSizes.popupSecondColumn.md}
                        sm={columnSizes.popupSecondColumn.sm}
                        xs={columnSizes.popupSecondColumn.xs}
                      ></Col>
                    </Row>
                    {typeof this.state.selectedCompletedAlternateTestRequestData.test_to_administer
                      .id !== "undefined" && (
                      <Row className="align-items-center" style={styles.fieldRowContainer}>
                        <Col
                          xl={columnSizes.popupFirstColumn.xl}
                          lg={columnSizes.popupFirstColumn.lg}
                          md={columnSizes.popupFirstColumn.md}
                          sm={columnSizes.popupFirstColumn.sm}
                          xs={columnSizes.popupFirstColumn.xs}
                        >
                          <label
                            htmlFor="view-selected-completed-alternate-test-request-cannot-be-performed-in-cat-switch"
                            style={styles.indentedLabel}
                          >
                            {
                              LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests
                                .viewSelectedCompletedAlternateTestRequestPopup
                                .testCannotBePerformedInCatLabel
                            }
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.popupSecondColumn.xl}
                          lg={columnSizes.popupSecondColumn.lg}
                          md={columnSizes.popupSecondColumn.md}
                          sm={columnSizes.popupSecondColumn.sm}
                          xs={columnSizes.popupSecondColumn.xs}
                        >
                          {!this.state.isLoadingSwitch && (
                            <Switch
                              id={
                                "view-selected-completed-alternate-test-request-cannot-be-performed-in-cat-switch"
                              }
                              onChange={() => {}}
                              disabled={true}
                              // specified_test_description is defined
                              checked={
                                typeof this.state.selectedCompletedAlternateTestRequestData
                                  .test_to_administer.specified_test_description !== "undefined" &&
                                this.state.selectedCompletedAlternateTestRequestData
                                  .test_to_administer.specified_test_description !== null
                              }
                              height={this.state.switchHeight}
                              width={this.state.switchWidth}
                            />
                          )}
                        </Col>
                      </Row>
                    )}
                    {typeof this.state.selectedCompletedAlternateTestRequestData.test_to_administer
                      .id === "undefined" ? (
                      <Row className="align-items-center" style={styles.fieldRowContainer}>
                        <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                          <label
                            id="view-selected-completed-alternate-test-request-no-test-to-administer-defined-label"
                            style={styles.indentedLabel}
                          >
                            {
                              LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests
                                .viewSelectedCompletedAlternateTestRequestPopup
                                .noTestToAdministerDefinedLabel
                            }
                          </label>
                        </Col>
                      </Row>
                    ) : // there is a test defined
                    typeof this.state.selectedCompletedAlternateTestRequestData.test_to_administer
                        .test !== "undefined" &&
                      this.state.selectedCompletedAlternateTestRequestData.test_to_administer
                        .test !== null ? (
                      <Row className="align-items-center" style={styles.fieldRowContainer}>
                        <Col
                          xl={columnSizes.popupFirstColumn.xl}
                          lg={columnSizes.popupFirstColumn.lg}
                          md={columnSizes.popupFirstColumn.md}
                          sm={columnSizes.popupFirstColumn.sm}
                          xs={columnSizes.popupFirstColumn.xs}
                        >
                          <label
                            id="view-selected-completed-alternate-test-request-test-to-administer-label"
                            style={styles.indentedLabel}
                          >
                            {
                              LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests
                                .viewSelectedCompletedAlternateTestRequestPopup
                                .testToAdministerLabel
                            }
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.popupSecondColumn.xl}
                          lg={columnSizes.popupSecondColumn.lg}
                          md={columnSizes.popupSecondColumn.md}
                          sm={columnSizes.popupSecondColumn.sm}
                          xs={columnSizes.popupSecondColumn.xs}
                        >
                          <DropdownSelect
                            idPrefix="view-selected-completed-alternate-test-request-test-to-administer"
                            ariaLabelledBy="view-selected-completed-alternate-test-request-recommendations-label view-selected-completed-alternate-test-request-test-to-administer-label"
                            ariaRequired={true}
                            options={[]}
                            onChange={() => {}}
                            defaultValue={{
                              label: `${
                                this.state.selectedCompletedAlternateTestRequestData
                                  .test_to_administer.test_code
                              } - ${
                                this.state.selectedCompletedAlternateTestRequestData
                                  .test_to_administer[`test_name_${this.props.currentLanguage}`]
                              } (v${
                                this.state.selectedCompletedAlternateTestRequestData
                                  .test_to_administer.test_version
                              })`,
                              value:
                                this.state.selectedCompletedAlternateTestRequestData
                                  .test_to_administer.test
                            }}
                            isDisabled={true}
                          />
                        </Col>
                      </Row>
                    ) : (
                      // test cannot be performed in CAT
                      <>
                        <Row className="align-items-center" style={styles.fieldRowContainer}>
                          <Col
                            xl={columnSizes.popupFirstColumn.xl}
                            lg={columnSizes.popupFirstColumn.lg}
                            md={columnSizes.popupFirstColumn.md}
                            sm={columnSizes.popupFirstColumn.sm}
                            xs={columnSizes.popupFirstColumn.xs}
                          >
                            <label
                              htmlFor="view-selected-pending-alternate-specified-test-request-test-description-input"
                              style={styles.indentedLabel}
                            >
                              {
                                LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests
                                  .viewSelectedCompletedAlternateTestRequestPopup
                                  .specifiedTestDescriptionLabel
                              }
                            </label>
                          </Col>
                          <Col
                            xl={columnSizes.popupSecondColumn.xl}
                            lg={columnSizes.popupSecondColumn.lg}
                            md={columnSizes.popupSecondColumn.md}
                            sm={columnSizes.popupSecondColumn.sm}
                            xs={columnSizes.popupSecondColumn.xs}
                          >
                            <input
                              id="view-selected-completed-alternate-test-request-specified-test-description-input"
                              tabIndex={0}
                              className={"valid-field input-disabled-look"}
                              type="text"
                              onChange={() => {}}
                              value={
                                this.state.selectedCompletedAlternateTestRequestData
                                  .test_to_administer.specified_test_description
                              }
                              style={{
                                ...styles.input,
                                ...accommodationsStyle
                              }}
                            />
                          </Col>
                        </Row>
                      </>
                    )}
                  </div>
                  <Row style={styles.fieldRowContainer}>
                    <Col
                      xl={columnSizes.popupFirstColumn.xl}
                      lg={columnSizes.popupFirstColumn.lg}
                      md={columnSizes.popupFirstColumn.md}
                      sm={columnSizes.popupFirstColumn.sm}
                      xs={columnSizes.popupFirstColumn.xs}
                    >
                      <label
                        htmlFor="view-selected-pending-alternate-specified-test-request-rationale-input"
                        style={styles.standardLabel}
                      >
                        {
                          LOCALIZE.systemAdministrator.alternateTestRequests.completedRequests
                            .viewSelectedCompletedAlternateTestRequestPopup.rationaleLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.popupSecondColumn.xl}
                      lg={columnSizes.popupSecondColumn.lg}
                      md={columnSizes.popupSecondColumn.md}
                      sm={columnSizes.popupSecondColumn.sm}
                      xs={columnSizes.popupSecondColumn.xs}
                    >
                      <textarea
                        id="view-selected-pending-alternate-specified-test-request-rationale-input"
                        className={"valid-field input-disabled-look"}
                        style={{ ...styles.textAreaInput, ...accommodationsStyle }}
                        onChange={() => {}}
                        value={this.state.selectedCompletedAlternateTestRequestData.rationale}
                        maxLength="5000"
                      />
                    </Col>
                  </Row>
                </div>
              }
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={LOCALIZE.commons.close}
              rightButtonIcon={faTimes}
              rightButtonAction={this.closeViewSelectedCompletedAlternateTestRequestPopup}
            />
          </>
        )}
      </div>
    );
  }
}

export { CompletedAlternateTestRequests as unconnectedCompletedAlternateTestRequests };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    completedAlternateTestRequestsPage:
      state.alternateTestRequests.completedAlternateTestRequestsPage,
    completedAlternateTestRequestsPageSize:
      state.alternateTestRequests.completedAlternateTestRequestsPageSize,
    completedAlternateTestRequestsKeyword:
      state.alternateTestRequests.completedAlternateTestRequestsKeyword,
    completedAlternateTestRequestsActiveSearch:
      state.alternateTestRequests.completedAlternateTestRequestsActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateCompletedAlternateTestRequestsSearchParametersStates,
      updateCompletedAlternateTestRequestsPageState,
      updateCompletedAlternateTestRequestsPageSizeState,
      getAllCompletedAlternateTestRequests,
      getFoundCompletedAlternateTestRequests,
      getSelectedUserAccommodationFileData,
      getTestCentersForUserAccommodationRequest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CompletedAlternateTestRequests);
