import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import { faSpinner, faSave, faCheck } from "@fortawesome/free-solid-svg-icons";
import { Col, Row } from "react-bootstrap";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import LOCALIZE from "../../../text_resources";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import {
  setReduxOlaConfigs,
  getOlaPrioritization,
  setReduxOlaPrioritization,
  setOlaPrioritization
} from "../../../modules/OlaConfigRedux";
import { bindActionCreators } from "redux";
import { BUTTON_STATE } from "../../commons/PopupBox";
import { OLA_PRIORITIZATION_CONST } from "./Constants";

const styles = {
  mainContainer: {
    margin: "24px 0"
  },
  rowContainer: {
    width: "90%",
    margin: "12px auto"
  },
  labelCol: {
    padding: "0px"
  },
  inputCol: {
    textAlign: "left"
  },
  inputTitles: {
    fontWeight: "bold"
  },
  inputs: {
    width: "20%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4,
    textAlign: "center"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  applySavedButton: {
    minWidth: 250
  },
  applySavedButtonContainer: {
    margin: "36px auto",
    textAlign: "center"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0
  }
};

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

class Prioritization extends Component {
  state = {
    currentlyLoading: false,
    prioritizationHigh: 0,
    prioritizationMedium: 0,
    prioritizationLow: 0,
    isValidPrioritization: true,
    applyButtonLoading: false,
    changesDetected: false
  };

  // on load, get the values from the backend
  componentDidMount = () => {
    this.getOlaPrioritization();
  };

  componentDidUpdate = prevProps => {
    // if olaPrioritization gets updated
    if (prevProps.olaPrioritization !== this.props.olaPrioritization) {
      // detecting changes
      this.detectChangesAndValidateForm();
    }
  };

  // getting OLA Prioritization
  getOlaPrioritization = () => {
    this.setState({ currentlyLoading: true }, () => {
      // getting OLA priorization
      this.props.getOlaPrioritization().then(response => {
        // updating states
        this.setState(
          {
            prioritizationHigh: response.filter(
              obj => obj.codename === OLA_PRIORITIZATION_CONST.HIGH
            )[0].value,
            prioritizationMedium: response.filter(
              obj => obj.codename === OLA_PRIORITIZATION_CONST.MEDIUM
            )[0].value,
            prioritizationLow: response.filter(
              obj => obj.codename === OLA_PRIORITIZATION_CONST.LOW
            )[0].value,
            currentlyLoading: false
          },
          () => {
            // updating redux states
            this.props.setReduxOlaPrioritization(response);
          }
        );
      });
    });
  };

  updatePrioritizationHigh = event => {
    const prioritizationHigh = event.target.value;
    // only allow percentage number (0-100)
    const regex = /^((100)|(\d{1,2}(\.\d{1,8})?))$/;
    if (regex.test(prioritizationHigh) || prioritizationHigh === "") {
      this.setState(
        { prioritizationHigh: prioritizationHigh !== "" ? parseInt(prioritizationHigh) : 0 },
        () => {
          // detecting changes
          this.detectChangesAndValidateForm();
        }
      );
    }
  };

  updatePrioritizationMedium = event => {
    const prioritizationMedium = event.target.value;
    // only allow percentage number (0-100)
    const regex = /^((100)|(\d{1,2}(\.\d{1,8})?))$/;
    if (regex.test(prioritizationMedium) || prioritizationMedium === "") {
      this.setState(
        { prioritizationMedium: prioritizationMedium !== "" ? parseInt(prioritizationMedium) : 0 },
        () => {
          // detecting changes
          this.detectChangesAndValidateForm();
        }
      );
    }
  };

  updatePrioritizationLow = event => {
    const prioritizationLow = event.target.value;
    // only allow percentage number (0-100)
    const regex = /^((100)|(\d{1,2}(\.\d{1,8})?))$/;
    if (regex.test(prioritizationLow) || prioritizationLow === "") {
      this.setState(
        { prioritizationLow: prioritizationLow !== "" ? parseInt(prioritizationLow) : 0 },
        () => {
          // detecting changes
          this.detectChangesAndValidateForm();
        }
      );
    }
  };

  handleApplyChanges = () => {
    const { prioritizationHigh, prioritizationMedium, prioritizationLow } = this.state;
    this.setState({ applyButtonLoading: true }, () => {
      const data = {
        [OLA_PRIORITIZATION_CONST.HIGH]: prioritizationHigh,
        [OLA_PRIORITIZATION_CONST.MEDIUM]: prioritizationMedium,
        [OLA_PRIORITIZATION_CONST.LOW]: prioritizationLow
      };
      // save in backend
      this.props.setOlaPrioritization(data).then(response => {
        // if successful, then update the props and state
        if (response.ok) {
          // updating states
          this.setState({ applyButtonLoading: false, changesDetected: false }, () => {
            // getting most updated data from the DB by calling back getOlaPrioritization
            this.getOlaPrioritization();
          });
        } else {
          throw new Error("An error occurred while saving the Ola Prioritization");
        }
      });
    });
  };

  detectChangesAndValidateForm = () => {
    // ==================== DETECTING CHANGES ====================
    // initializing changesDetected
    let changesDetected = false;
    // if either of the fields has been updated
    if (
      this.state.prioritizationHigh !==
        this.props.olaPrioritization.filter(
          obj => obj.codename === OLA_PRIORITIZATION_CONST.HIGH
        )[0].value ||
      this.state.prioritizationMedium !==
        this.props.olaPrioritization.filter(
          obj => obj.codename === OLA_PRIORITIZATION_CONST.MEDIUM
        )[0].value ||
      this.state.prioritizationLow !==
        this.props.olaPrioritization.filter(obj => obj.codename === OLA_PRIORITIZATION_CONST.LOW)[0]
          .value
    ) {
      // setting changesDetected to true
      changesDetected = true;
    } else {
      changesDetected = false;
    }
    // ==================== DETECTING CHANGES (END) ====================

    // ==================== VALIDATING FORM ====================
    // initializing isValidPrioritization
    let isValidPrioritization = false;
    // sum of prioritization is less than or equal to 100
    if (
      this.state.prioritizationHigh +
        this.state.prioritizationMedium +
        this.state.prioritizationLow <=
      100
    ) {
      // setting isValidPrioritization to true
      isValidPrioritization = true;
    }
    // ==================== VALIDATING FORM (END) ====================

    // updating needed states
    this.setState({
      changesDetected: changesDetected,
      isValidPrioritization: isValidPrioritization
    });
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    // getting/setting prioritization text
    let prioritizationTextHigh = "";
    let prioritizationTextMedium = "";
    let prioritizationTextLow = "";
    if (
      this.props.olaPrioritization.filter(obj => obj.codename === OLA_PRIORITIZATION_CONST.HIGH)
        .length > 0
    ) {
      prioritizationTextHigh = this.props.olaPrioritization.filter(
        obj => obj.codename === OLA_PRIORITIZATION_CONST.HIGH
      )[0][`text_${this.props.currentLanguage}`];
    }
    if (
      this.props.olaPrioritization.filter(obj => obj.codename === OLA_PRIORITIZATION_CONST.MEDIUM)
        .length > 0
    ) {
      prioritizationTextMedium = this.props.olaPrioritization.filter(
        obj => obj.codename === OLA_PRIORITIZATION_CONST.MEDIUM
      )[0][`text_${this.props.currentLanguage}`];
    }
    if (
      this.props.olaPrioritization.filter(obj => obj.codename === OLA_PRIORITIZATION_CONST.LOW)
        .length > 0
    ) {
      prioritizationTextLow = this.props.olaPrioritization.filter(
        obj => obj.codename === OLA_PRIORITIZATION_CONST.LOW
      )[0][`text_${this.props.currentLanguage}`];
    }

    return (
      <div style={styles.mainContainer}>
        <div>
          <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelCol}
            >
              <label htmlFor="prioritization-high-input" style={styles.inputTitles}>
                {LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.olaConfigs.tabs.prioritization
                    .prioritizationLabelFormatter,
                  prioritizationTextHigh
                )}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputCol}
            >
              <input
                id="prioritization-high-input"
                className={this.state.isValidPrioritization ? "valid-field" : "invalid-field"}
                aria-required={true}
                type="text"
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                value={this.state.prioritizationHigh}
                onChange={this.updatePrioritizationHigh}
                disabled={this.state.currentlyLoading}
              ></input>
            </Col>
          </Row>
          <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelCol}
            >
              <label htmlFor="prioritization-medium-input" style={styles.inputTitles}>
                {LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.olaConfigs.tabs.prioritization
                    .prioritizationLabelFormatter,
                  prioritizationTextMedium
                )}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputCol}
            >
              <input
                id="prioritization-medium-input"
                className={this.state.isValidPrioritization ? "valid-field" : "invalid-field"}
                aria-required={true}
                type="text"
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                value={this.state.prioritizationMedium}
                onChange={this.updatePrioritizationMedium}
                disabled={this.state.currentlyLoading}
              ></input>
            </Col>
          </Row>
          <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelCol}
            >
              <label htmlFor="prioritization-low-input" style={styles.inputTitles}>
                {LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.olaConfigs.tabs.prioritization
                    .prioritizationLabelFormatter,
                  prioritizationTextLow
                )}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputCol}
            >
              <input
                id="prioritization-low-input"
                className={this.state.isValidPrioritization ? "valid-field" : "invalid-field"}
                aria-required={true}
                type="text"
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
                value={this.state.prioritizationLow}
                onChange={this.updatePrioritizationLow}
                disabled={this.state.currentlyLoading}
              ></input>
            </Col>
          </Row>
          {!this.state.isValidPrioritization && (
            <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
                style={styles.labelCol}
              ></Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
                style={styles.inputCol}
              >
                <label htmlFor="prioritization-high-input" style={styles.errorMessage}>
                  {LOCALIZE.systemAdministrator.olaConfigs.tabs.prioritization.prioritizationError}
                </label>
              </Col>
            </Row>
          )}
        </div>
        <div style={styles.applySavedButtonContainer}>
          <CustomButton
            label={
              this.state.applyButtonLoading ? (
                // eslint-disable-next-line jsx-a11y/label-has-associated-control
                <label className="fa fa-spinner fa-spin">
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <>
                  <FontAwesomeIcon icon={this.state.changesDetected ? faSave : faCheck} />
                  <span
                    id="ola-config-apply-saved-button"
                    style={styles.buttonLabel}
                    className="notranslate"
                  >
                    {this.state.changesDetected
                      ? LOCALIZE.commons.applyButton
                      : LOCALIZE.commons.saved}
                  </span>
                </>
              )
            }
            customStyle={styles.applySavedButton}
            action={this.state.changesDetected ? this.handleApplyChanges : () => {}}
            buttonTheme={this.state.changesDetected ? THEME.PRIMARY : THEME.SUCCESS}
            disabled={
              !this.state.isValidPrioritization
                ? BUTTON_STATE.disabled
                : this.state.applyButtonLoading
                  ? BUTTON_STATE.disabled
                  : this.state.changesDetected
                    ? BUTTON_STATE.enabled
                    : BUTTON_STATE.disabled
            }
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    olaPrioritization: state.olaConfig.olaPrioritization,
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setReduxOlaConfigs,
      setReduxOlaPrioritization,
      getOlaPrioritization,
      setOlaPrioritization
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Prioritization);
