import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import { faSpinner, faSave, faCheck } from "@fortawesome/free-solid-svg-icons";
import { Col, Row } from "react-bootstrap";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import LOCALIZE from "../../../text_resources";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import {
  setReduxOlaConfigs,
  getOlaGlobalConfigs,
  setOlaGlobalConfigs
} from "../../../modules/OlaConfigRedux";
import { bindActionCreators } from "redux";
import { BUTTON_STATE } from "../../commons/PopupBox";
import OLA_GLOBAL_CONFIGS_CONST from "./Constants";

const styles = {
  mainContainer: {
    margin: "24px 0"
  },
  rowContainer: {
    width: "90%",
    margin: "12px auto"
  },
  labelCol: {
    padding: "0px"
  },
  inputCol: {
    textAlign: "left"
  },
  inputTitles: {
    fontWeight: "bold"
  },
  inputs: {
    width: "20%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4,
    textAlign: "center"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  applySavedButton: {
    minWidth: 250
  },
  applySavedButtonContainer: {
    margin: "36px auto",
    textAlign: "center"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginLeft: 6
  }
};

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

class OlaConfig extends Component {
  state = {
    currentlyLoading: false,
    cancellationWindow: 0,
    openBookingWindow: 0,
    applyButtonLoading: false,
    changesDetected: false
  };

  // on load, get the values from the backend
  componentDidMount = () => {
    this.props.getOlaGlobalConfigs().then(response => {
      let { cancellationWindow, openBookingWindow } = this.state;
      if (response.status === 200) {
        for (let i = 0; i < response.length; i++) {
          const curVal = response[i];
          if (curVal.codename === OLA_GLOBAL_CONFIGS_CONST.CANCELLATION_WINDOW) {
            cancellationWindow = curVal.value;
          }
          if (curVal.codename === OLA_GLOBAL_CONFIGS_CONST.OPEN_BOOKING_WINDOW) {
            openBookingWindow = curVal.value;
          }
        }
        this.props.setReduxOlaConfigs(cancellationWindow, openBookingWindow);
        this.setState({
          currentlyLoading: false,
          cancellationWindow: cancellationWindow,
          openBookingWindow: openBookingWindow
        });
      } else {
        throw new Error("An error occurred while retrieving the Ola Global Configs");
      }
    });
  };

  componentDidUpdate(prevProps) {
    if (
      this.props.cancellationWindow !== prevProps.cancellationWindow ||
      this.props.openBookingWindow !== prevProps.openBookingWindow
    ) {
      this.detectChanges();
    }
  }

  // update the state variable on change
  updateCancellationWindow = event => {
    const cancellationWindow = event.target.value;
    // allow only numeric values (0 to 4 chars)
    const regexExpression = /^([0-9]{0,4})$/;
    if (regexExpression.test(cancellationWindow)) {
      this.setState(
        { cancellationWindow: cancellationWindow !== "" ? parseInt(cancellationWindow) : 0 },
        () => {
          // detecting changes
          this.detectChanges();
        }
      );
    }
  };

  // update the state variable on change
  updateOpenBookingWindow = event => {
    const openBookingWindow = event.target.value;
    // allow only numeric values (0 to 4 chars)
    const regexExpression = /^([0-9]{0,4})$/;
    if (regexExpression.test(openBookingWindow)) {
      this.setState(
        { openBookingWindow: openBookingWindow !== "" ? parseInt(openBookingWindow) : 0 },
        () => {
          // detecting changes
          this.detectChanges();
        }
      );
    }
  };

  handleApplyChanges = () => {
    const { cancellationWindow, openBookingWindow } = this.state;
    this.setState({ applyButtonLoading: true }, () => {
      const data = {
        [OLA_GLOBAL_CONFIGS_CONST.CANCELLATION_WINDOW]: cancellationWindow,
        [OLA_GLOBAL_CONFIGS_CONST.OPEN_BOOKING_WINDOW]: openBookingWindow
      };
      // save in backend
      this.props.setOlaGlobalConfigs(data).then(response => {
        // if successful, then update the props and state
        if (response.status === 200) {
          this.setState({ applyButtonLoading: false }, () => {
            this.props.setReduxOlaConfigs(cancellationWindow, openBookingWindow);
            this.detectChanges();
          });
        } else {
          throw new Error("An error occurred while saving the Ola Global Configs");
        }
      });
    });
  };

  detectChanges = () => {
    // initializing changesDetected
    let changesDetected = false;

    // if either of the fields has been updated
    if (
      String(this.state.cancellationWindow) !== this.props.cancellationWindow ||
      String(this.state.openBookingWindow) !== this.props.openBookingWindow
    ) {
      // setting changesDetected to true
      changesDetected = true;
    } else {
      changesDetected = false;
    }

    // updating needed states
    this.setState({ changesDetected: changesDetected });
  };

  render() {
    const { currentlyLoading } = this.state;
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <div>
          {!currentlyLoading && (
            <div>
              <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelCol}
                >
                  <label
                    id="last-minute-cancellation-label"
                    htmlFor="last-minute-cancellation"
                    style={styles.inputTitles}
                  >
                    {LOCALIZE.systemAdministrator.olaConfigs.tabs.olaConfig.cancellationWindow}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputCol}
                >
                  <input
                    id="last-minute-cancellation"
                    className={"valid-field"}
                    aria-required={true}
                    type="text"
                    style={{
                      ...styles.inputs,
                      ...accommodationsStyle
                    }}
                    value={this.state.cancellationWindow}
                    onChange={this.updateCancellationWindow}
                  ></input>
                </Col>
              </Row>
              <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelCol}
                >
                  <label
                    id="open-booking-window-label"
                    htmlFor="open-booking-window"
                    style={styles.inputTitles}
                  >
                    {LOCALIZE.systemAdministrator.olaConfigs.tabs.olaConfig.openBookingPeriod}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputCol}
                >
                  <input
                    id="open-booking-window"
                    className={"valid-field"}
                    aria-required={true}
                    type="text"
                    style={{
                      ...styles.inputs,
                      ...accommodationsStyle
                    }}
                    value={this.state.openBookingWindow}
                    onChange={this.updateOpenBookingWindow}
                  ></input>
                </Col>
              </Row>
            </div>
          )}
          {currentlyLoading && (
            <div className="fa fa-spinner fa-spin">
              <FontAwesomeIcon icon={faSpinner} />
            </div>
          )}
        </div>
        <div style={styles.applySavedButtonContainer}>
          <CustomButton
            label={
              this.state.applyButtonLoading ? (
                // eslint-disable-next-line jsx-a11y/label-has-associated-control
                <label className="fa fa-spinner fa-spin">
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <>
                  <FontAwesomeIcon icon={this.state.changesDetected ? faSave : faCheck} />
                  <span
                    id="ola-config-apply-saved-button"
                    style={styles.buttonLabel}
                    className="notranslate"
                  >
                    {this.state.changesDetected
                      ? LOCALIZE.commons.applyButton
                      : LOCALIZE.commons.saved}
                  </span>
                </>
              )
            }
            customStyle={styles.applySavedButton}
            action={this.state.changesDetected ? this.handleApplyChanges : () => {}}
            buttonTheme={this.state.changesDetected ? THEME.PRIMARY : THEME.SUCCESS}
            disabled={
              this.state.applyButtonLoading
                ? BUTTON_STATE.disabled
                : this.state.changesDetected
                  ? BUTTON_STATE.enabled
                  : BUTTON_STATE.disabled
            }
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    cancellationWindow: state.olaConfig.cancellationWindow,
    openBookingWindow: state.olaConfig.openBookingWindow,
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setReduxOlaConfigs,
      getOlaGlobalConfigs,
      setOlaGlobalConfigs
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(OlaConfig);
