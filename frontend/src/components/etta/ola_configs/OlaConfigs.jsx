import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import ServiceStandards from "./ServiceStandards";
import OlaCofig from "./OlaConfig";
import Prioritization from "./Prioritization";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  }
};

class OlaConfigs extends Component {
  state = {
    selectedTab: ""
  };

  render() {
    const TABS = [
      {
        key: "prioritization",
        tabName: LOCALIZE.systemAdministrator.olaConfigs.tabs.prioritization.title,
        body: <Prioritization />
      },
      {
        key: "service-standards",
        tabName: LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards.title,
        body: <ServiceStandards />
      },
      {
        key: "ola-config",
        tabName: LOCALIZE.systemAdministrator.olaConfigs.tabs.olaConfig.title,
        body: <OlaCofig />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.systemAdministrator.olaConfigs.title}</h2>
          <p>{LOCALIZE.systemAdministrator.olaConfigs.description}</p>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey="prioritization"
                id="ola-configs-tabs"
                style={styles.tabNavigation}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export { OlaConfigs as unconnectedOlaConfigs };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(OlaConfigs);
