import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTimes,
  faPencilAlt,
  faSave,
  faLanguage,
  faCheck
} from "@fortawesome/free-solid-svg-icons";
import StyledTooltip, { TYPE, EFFECT } from "../../authentication/StyledTooltip";
import "../../../css/etta.css";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import { Col, Row } from "react-bootstrap";
import {
  editReasonForTestingData,
  getOlaReasonsForTesting,
  getReasonForTestingPriorities
} from "../../../modules/UitRedux";
import { LANGUAGES } from "../../commons/Translation";
import MultilingualField, { FIELD_TYPES } from "../../commons/MultilingualField";
import getPropertyName from "../../../helpers/propertyName";
import DropdownSelect from "../../commons/DropdownSelect";
import Switch from "react-switch";
import getSwitchTransformScale from "../../../helpers/switchTransformScale";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 5,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 6,
    lg: 6,
    xl: 6
  },
  thirdColumn: {
    xs: 12,
    sm: 12,
    md: 1,
    lg: 1,
    xl: 1
  }
};

const styles = {
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  translationButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    margin: "0 3px",
    padding: 0,
    color: "#00565e",
    transform: "scale(1.5)"
  },
  buttonIcon: {
    marginRight: 0
  },
  formContainer: {
    margin: "auto 24px"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  labelContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  label: {
    overflowWrap: "break-word",
    margin: "0 12px 0 0"
  },
  inputContainer: {
    display: "table-cell",
    width: "100%",
    verticalAlign: "middle"
  },
  fieldSeparator: {
    marginTop: 12
  },
  colCenteredLabel: {
    display: "flex",
    alignItems: "center",
    width: "100%"
  },
  colCentered: {
    alignItems: "center",
    width: "100%"
  },
  contentFlex: {
    flex: "0 0 100%"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "4px 0 0 4px"
  },
  allUnset: {
    all: "unset"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  genericTable: {
    margin: "18px 0px 12px"
  },
  checkIcon: {
    color: "#278400"
  }
};

class ServiceStandards extends Component {
  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    selectedServiceStandardData: {},
    showEditServiceStandardPopup: false,
    showNamePopup: false,
    isValidForm: false,
    nameContentEn: "",
    isValidNameEn: true,
    nameContentFr: "",
    isValidNameFr: true,
    priorityOptions: [],
    prioritySelectedOption: [],
    minimumProcessLengthContent: 0,
    waitingPeriodContent: 0,
    isValidWaitingPeriod: true,
    isLoadingSwitch: false,
    serviceStandardActiveState: false
  };

  componentDidMount = () => {
    this.getUpdatedSwitchDimensions();
    // populating reason for testing priorities options
    this.populateReasonForTestingPrioritiesOptions();
    // populating service standards
    this.populateServiceStandards();
  };

  componentDidUpdate = prevProps => {
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoadingSwitch: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoadingSwitch: false });
        }
      );
    });
  };

  // populating reason for testing priorities options
  populateReasonForTestingPrioritiesOptions = () => {
    // getting reason for testing priorities
    this.props.getReasonForTestingPriorities().then(response => {
      // initializing priorityOptions
      const priorityOptions = [];

      // looping in response
      for (let i = 0; i < response.length; i++) {
        // populating priorityOptions
        priorityOptions.push({
          value: response[i].id,
          label: response[i][`reason_for_testing_priority_name_${this.props.currentLanguage}`]
        });
      }

      // updating state
      this.setState({ priorityOptions: priorityOptions });
    });
  };

  populateServiceStandards = () => {
    this.setState({ currentlyLoading: true }, () => {
      this.props.getOlaReasonsForTesting().then(response => {
        const olaReasonsForTesting = response;
        // ordering service standards by priority and name
        olaReasonsForTesting.sort(
          (a, b) =>
            a.reason_for_testing_priority - b.reason_for_testing_priority ||
            a[`reason_for_testing_name_${this.props.currentLanguage}`].localeCompare(
              b[`reason_for_testing_name_${this.props.currentLanguage}`]
            )
        );
        this.populateServiceStandardsObject(olaReasonsForTesting);
      });
    });
  };

  populateServiceStandardsObject = response => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.length; i++) {
        const currentResult = response[i];
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult[`reason_for_testing_name_${this.props.currentLanguage}`],
          column_2: currentResult.minimum_process_length,
          column_3: currentResult.waiting_period,
          column_4: currentResult[`reason_for_testing_priority_name_${this.props.currentLanguage}`],
          column_5: currentResult.active ? (
            <FontAwesomeIcon icon={faCheck} style={styles.checkIcon} />
          ) : (
            ""
          ),
          column_6: (
            <div className="d-flex justify-content-center flex-nowrap">
              <StyledTooltip
                id={`service-standards-edit-button-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`service-standards-edit-button-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faPencilAlt} style={styles.buttonIcon} />
                        </>
                      }
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards.table
                          .editButtonTooltipAccessibility,
                        currentResult[`reason_for_testing_name_${this.props.currentLanguage}`]
                      )}
                      action={() => this.openEditSelectedServiceStandardPopup(currentResult)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.SECONDARY}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards.table
                          .editButtonTooltip
                      }
                    </p>
                  </div>
                }
              />
            </div>
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      column_6_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState(
      {
        rowsDefinition: rowsDefinition
      },
      () => {
        this.setState({ currentlyLoading: false });
      }
    );
  };

  openNamePopup = () => {
    this.setState({ showNamePopup: true });
  };

  updateNameContentFromTranslationPopup = event => {
    const nameContent = event.target.value;
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(nameContent)) {
      // updating EN name
      if (
        event.target.name ===
        getPropertyName(
          LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards.serviceStandardPopup,
          obj => obj.en_name
        )
      ) {
        // updating state value
        this.setState({ nameContentEn: nameContent }, () => {
          // validating form
          this.validateForm();
        });
      }
      // updating FR name
      else if (
        event.target.name ===
        getPropertyName(
          LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards.serviceStandardPopup,
          obj => obj.fr_name
        )
      ) {
        // updating state value
        this.setState({ nameContentFr: nameContent }, () => {
          // validating form
          this.validateForm();
        });
      }
    }
  };

  closeNamePopup = () => {
    this.setState({ showNamePopup: false });
  };

  // get name content
  getNameContent = event => {
    const nameContent = event.target.value;

    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(nameContent)) {
      // interface is in EN
      if (this.props.currentLanguage === LANGUAGES.english) {
        // updating state
        this.setState({ nameContentEn: nameContent }, () => {
          // validating form
          this.validateForm();
        });
        // interface is in FR
      } else {
        // updating state
        this.setState({ nameContentFr: nameContent }, () => {
          // validating form
          this.validateForm();
        });
      }
    }
  };

  // getting selected priority option
  getSelectedPriorityOption = selectedOption => {
    this.setState({ prioritySelectedOption: selectedOption }, () => {
      // validating form
      this.validateForm();
    });
  };

  // get minimum process length content
  getMinimumProcessLengthContent = event => {
    const minimumProcessLengthContent = event.target.value;

    // only allow numbers to be entered (maximum of 4 chars)
    const regex = /^([0-9]{0,4})$/;
    if (event.target.value === "" || regex.test(event.target.value)) {
      // updating state
      this.setState(
        {
          minimumProcessLengthContent:
            minimumProcessLengthContent !== "" ? parseInt(minimumProcessLengthContent) : 0
        },
        () => {
          // validating form
          this.validateForm();
        }
      );
    }
  };

  // get waiting period content
  getWaitingPeriodContent = event => {
    const waitingPeriodContent = event.target.value;

    // only allow numbers to be entered (maximum of 4 chars)
    const regex = /^([0-9]{0,4})$/;
    if (event.target.value === "" || regex.test(event.target.value)) {
      // updating state
      this.setState(
        {
          waitingPeriodContent: waitingPeriodContent !== "" ? parseInt(waitingPeriodContent) : 0
        },
        () => {
          // validating form
          this.validateForm();
        }
      );
    }
  };

  // get active state
  handleSwitchUpdate = event => {
    this.setState({ serviceStandardActiveState: event });
  };

  validateForm = () => {
    // initializing variables
    let isValidForm = false;
    const isValidNameEn = this.state.nameContentEn !== "";
    const isValidNameFr = this.state.nameContentFr !== "";
    const isValidPrioritySelectedOption = Object.keys(this.state.prioritySelectedOption).length > 0;
    // making sure that the waiting period is less or equal to the minimum process length value
    const isValidWaitingPeriod =
      this.state.waitingPeriodContent <= this.state.minimumProcessLengthContent;

    // everything is valid
    if (isValidNameEn && isValidNameFr && isValidPrioritySelectedOption && isValidWaitingPeriod) {
      // setting isValidForm to true
      isValidForm = true;
    }

    // updating states
    this.setState({
      isValidForm: isValidForm,
      isValidNameEn: isValidNameEn,
      isValidNameFr: isValidNameFr,
      isValidWaitingPeriod: isValidWaitingPeriod
    });
  };

  openEditSelectedServiceStandardPopup = data => {
    this.setState(
      {
        showEditServiceStandardPopup: true,
        selectedServiceStandardData: data,
        nameContentEn: data.reason_for_testing_name_en,
        isValidNameEn: true,
        nameContentFr: data.reason_for_testing_name_fr,
        isValidNameFr: true,
        prioritySelectedOption: {
          value: data.reason_for_testing_priority,
          label: data[`reason_for_testing_priority_name_${this.props.currentLanguage}`]
        },
        minimumProcessLengthContent: data.minimum_process_length,
        waitingPeriodContent: data.waiting_period,
        serviceStandardActiveState: data.active
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  handleEditServiceStandard = () => {
    // setting body
    const body = {
      id: this.state.selectedServiceStandardData.id,
      reason_for_testing_priority_name_en: this.state.nameContentEn,
      reason_for_testing_priority_name_fr: this.state.nameContentFr,
      reason_for_testing_priority_id: this.state.prioritySelectedOption.value,
      minimum_process_length: this.state.minimumProcessLengthContent,
      waiting_period: this.state.waitingPeriodContent,
      active: this.state.serviceStandardActiveState
    };

    // editing reason for testing data
    this.props.editReasonForTestingData(body).then(response => {
      // if there is a validation error
      if (response.ok) {
        // updating table
        this.populateServiceStandards();
        // close popup
        this.closeEditServiceStandardPopup();
        // should never happen
      } else {
        throw new Error("An error occurred during the edit service standard request process");
      }
    });
  };

  closeEditServiceStandardPopup = () => {
    this.setState({
      showEditServiceStandardPopup: false,
      selectedServiceStandardData: {},
      nameContentEn: "",
      isValidNameEn: true,
      nameContentFr: "",
      isValidNameFr: true,
      prioritySelectedOption: [],
      minimumProcessLengthContent: 0,
      waitingPeriodContent: 0,
      isValidWaitingPeriod: true,
      serviceStandardActiveState: false
    });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards.table.column1,
        style: {}
      },
      {
        label: LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards.table.column2,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards.table.column3,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards.table.column4,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards.table.column5,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards.table.column6,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div>
        <div style={styles.genericTable}>
          <GenericTable
            classnamePrefix="active-system-alerts"
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            currentlyLoading={this.state.currentlyLoading}
          />
        </div>
        <PopupBox
          show={this.state.showEditServiceStandardPopup}
          handleClose={() => {}}
          title={
            LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards.serviceStandardPopup
              .editTitle
          }
          description={
            <div>
              <p>
                {
                  LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards.serviceStandardPopup
                    .description
                }
              </p>
              <div style={styles.formContainer}>
                <Row className="align-items-center" style={styles.fieldSeparator}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={{ ...styles.labelContainer, ...styles.colCenteredLabel }}
                  >
                    <label
                      id="service-standard-popup-name-label"
                      htmlFor="service-standard-popup-name-input"
                      style={{ ...styles.label, ...styles.contentFlex }}
                    >
                      {
                        LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards
                          .serviceStandardPopup.nameLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={{ ...styles.inputContainer, ...styles.colCentered }}
                  >
                    <input
                      id="service-standard-popup-name-input"
                      className={
                        this.state.isValidNameEn && this.state.isValidNameFr
                          ? "valid-field"
                          : "invalid-field"
                      }
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={
                        this.props.currentLanguage === LANGUAGES.english
                          ? this.state.nameContentEn
                          : this.state.nameContentFr
                      }
                      onChange={this.getNameContent}
                    ></input>
                  </Col>
                  <Col
                    xl={columnSizes.thirdColumn.xl}
                    lg={columnSizes.thirdColumn.lg}
                    md={columnSizes.thirdColumn.md}
                    sm={columnSizes.thirdColumn.sm}
                    xs={columnSizes.thirdColumn.xs}
                    className={"text-left"}
                  >
                    <StyledTooltip
                      id={`service-standard-name-translations`}
                      place="top"
                      variant={TYPE.light}
                      effect={EFFECT.solid}
                      openOnClick={false}
                      tooltipElement={
                        <div style={styles.allUnset}>
                          <CustomButton
                            className="h-100"
                            dataTip=""
                            dataFor={`service-standard-name-translations`}
                            label={<FontAwesomeIcon icon={faLanguage} />}
                            ariaLabel={
                              LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards
                                .serviceStandardPopup.translationsButtonAriaLabel
                            }
                            action={() => this.openNamePopup()}
                            customStyle={styles.translationButton}
                            buttonTheme={THEME.SECONDARY}
                          />
                        </div>
                      }
                      tooltipContent={
                        <div>
                          <p>
                            {
                              LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards
                                .serviceStandardPopup.translationsButtonTooltip
                            }
                          </p>
                        </div>
                      }
                    />
                  </Col>
                </Row>
                {(!this.state.isValidNameEn || !this.state.isValidNameFr) && (
                  <Row>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                      style={{ ...styles.labelContainer, ...styles.colCenteredLabel }}
                    ></Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                      style={{ ...styles.inputContainer, ...styles.colCentered }}
                    >
                      <label
                        htmlFor="service-standard-popup-name-input"
                        style={styles.errorMessage}
                      >
                        {
                          LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards
                            .serviceStandardPopup.nameError
                        }
                      </label>
                    </Col>
                  </Row>
                )}
                <Row className="align-items-center" style={styles.fieldSeparator}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={{ ...styles.labelContainer, ...styles.colCenteredLabel }}
                  >
                    <label
                      id="service-standard-popup-priority-label"
                      style={{ ...styles.label, ...styles.contentFlex }}
                    >
                      {
                        LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards
                          .serviceStandardPopup.priorityLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={{ ...styles.inputContainer, ...styles.colCentered }}
                  >
                    <DropdownSelect
                      idPrefix="service-standard-popup-priority"
                      ariaRequired={true}
                      ariaLabelledBy="service-standard-popup-priority-label"
                      options={this.state.priorityOptions}
                      onChange={this.getSelectedPriorityOption}
                      defaultValue={this.state.prioritySelectedOption}
                      isMulti={false}
                      orderByLabels={false}
                      orderByVales={true}
                    />
                  </Col>
                </Row>
                <Row className="align-items-center" style={styles.fieldSeparator}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={{ ...styles.labelContainer, ...styles.colCenteredLabel }}
                  >
                    <label
                      id="service-standard-popup-service-standard-label"
                      htmlFor="service-standard-popup-service-standard-input"
                      style={{ ...styles.label, ...styles.contentFlex }}
                    >
                      {
                        LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards
                          .serviceStandardPopup.minimumProcessLengthLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={{ ...styles.inputContainer, ...styles.colCentered }}
                  >
                    <input
                      id="service-standard-popup-service-standard-input"
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.minimumProcessLengthContent}
                      onChange={this.getMinimumProcessLengthContent}
                    ></input>
                  </Col>
                </Row>
                <Row className="align-items-center" style={styles.fieldSeparator}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={{ ...styles.labelContainer, ...styles.colCenteredLabel }}
                  >
                    <label
                      id="service-standard-popup-waiting-period-label"
                      htmlFor="service-standard-popup-waiting-period-input"
                      style={{ ...styles.label, ...styles.contentFlex }}
                    >
                      {
                        LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards
                          .serviceStandardPopup.waitingPeriodLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={{ ...styles.inputContainer, ...styles.colCentered }}
                  >
                    <input
                      id="service-standard-popup-waiting-period-input"
                      className={this.state.isValidWaitingPeriod ? "valid-field" : "invalid-field"}
                      style={{ ...styles.input, ...accommodationsStyle }}
                      type="text"
                      value={this.state.waitingPeriodContent}
                      onChange={this.getWaitingPeriodContent}
                    ></input>
                  </Col>
                </Row>
                {!this.state.isValidWaitingPeriod && (
                  <Row>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                      style={{ ...styles.labelContainer, ...styles.colCenteredLabel }}
                    ></Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                      style={{ ...styles.inputContainer, ...styles.colCentered }}
                    >
                      <label
                        htmlFor="service-standard-popup-waiting-period-input"
                        style={styles.errorMessage}
                      >
                        {
                          LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards
                            .serviceStandardPopup.waitingPeriodError
                        }
                      </label>
                    </Col>
                  </Row>
                )}
                <Row className="align-items-center" style={styles.fieldSeparator}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={{ ...styles.labelContainer, ...styles.colCenteredLabel }}
                  >
                    <label
                      id="service-standard-popup-active-state-label"
                      htmlFor="service-standard-popup-active-state-switch"
                      style={{ ...styles.label, ...styles.contentFlex }}
                    >
                      {
                        LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards
                          .serviceStandardPopup.activeStateLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={{ ...styles.inputContainer, ...styles.colCentered }}
                  >
                    {!this.state.isLoadingSwitch && (
                      <Switch
                        id="service-standard-popup-active-state-switch"
                        onChange={this.handleSwitchUpdate}
                        checked={this.state.serviceStandardActiveState}
                        aria-labelledby="service-standard-popup-active-state-label"
                        height={this.state.switchHeight}
                        width={this.state.switchWidth}
                      />
                    )}
                  </Col>
                </Row>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeEditServiceStandardPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faSave}
          rightButtonTitle={LOCALIZE.commons.saveButton}
          rightButtonLabel={LOCALIZE.commons.saveButton}
          rightButtonAction={this.handleEditServiceStandard}
          rightButtonState={this.state.isValidForm ? BUTTON_STATE.enabled : BUTTON_STATE.disabled}
        />
        <PopupBox
          show={this.state.showNamePopup}
          title={
            LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards.serviceStandardPopup
              .nameTranslationTitle
          }
          size="lg"
          handleClose={() => this.closeNamePopup()}
          description={
            <div>
              <MultilingualField
                handleChange={this.updateNameContentFromTranslationPopup}
                localizePath={
                  LOCALIZE.systemAdministrator.olaConfigs.tabs.serviceStandards.serviceStandardPopup
                }
                fieldName={"test-center-address"}
                values={{
                  en: [{ text: this.state.nameContentEn }],
                  fr: [{ text: this.state.nameContentFr }]
                }}
                fieldType={FIELD_TYPES.INPUT}
              />
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.saveButton}
          rightButtonAction={() => this.closeNamePopup()}
        />
      </div>
    );
  }
}

export { ServiceStandards as unconnectedServiceStandards };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    systemAlertData: state.systemAlerts.systemAlertData,
    systemAlertDataValidForm: state.systemAlerts.systemAlertDataValidForm,
    systemAlertDataPreviewValidState: state.systemAlerts.systemAlertDataPreviewValidState,
    triggerActiveSystemAlertTableRerender: state.systemAlerts.triggerActiveSystemAlertTableRerender
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getOlaReasonsForTesting,
      getReasonForTestingPriorities,
      editReasonForTestingData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ServiceStandards);
