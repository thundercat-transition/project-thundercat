// if you update these statuses, don't forget to also update ".../backend/static/ola_global_configs_const.py" and ".../backend/static/ola_prioritization_const.py" files (backend)
const OLA_GLOBAL_CONFIGS_CONST = {
  CANCELLATION_WINDOW: "cancellation_window",
  OPEN_BOOKING_WINDOW: "open_booking_window"
};

export const OLA_PRIORITIZATION_CONST = {
  HIGH: "high",
  MEDIUM: "medium",
  LOW: "low"
};

export default OLA_GLOBAL_CONFIGS_CONST;
