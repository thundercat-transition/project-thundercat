import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import SearchBarWithDisplayOptions from "../commons/SearchBarWithDisplayOptions";
import Pagination from "../commons/Pagination";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars, faCheck, faTimes } from "@fortawesome/free-solid-svg-icons";
import {
  getAllUserProfileChangeRequests,
  getFoundUserProfileChangeRequests,
  updateUserProfileChangeRequestsPageState,
  updateUserProfileChangeRequestsPageSizeState,
  updateSearchUserProfileChangeRequestsStates,
  approveUserProfileChangeRequest,
  denyUserProfileChangeRequest
} from "../../modules/UserProfileChangeRequestsRedux";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { Col, Row } from "react-bootstrap";
import DatePicker from "../commons/DatePicker";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";

const columnSizes = {
  popupFirstColumn: {
    xs: 12,
    sm: 12,
    md: 2,
    lg: 2,
    xl: 2
  },
  popupSecondColumn: {
    xs: 12,
    sm: 12,
    md: 5,
    lg: 5,
    xl: 5
  },
  popupThirdColumn: {
    xs: 12,
    sm: 12,
    md: 5,
    lg: 5,
    xl: 5
  }
};

const styles = {
  buttonLabel: {
    marginLeft: 6
  },
  popupInputsContainer: {
    padding: "24px 12px"
  },
  popupRow: {
    padding: "12px 0"
  },
  columnTitle: {
    display: "block",
    textAlign: "center",
    fontWeight: "bold"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  textAreaInput: {
    width: "100%",
    height: 85,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block",
    resize: "none"
  },
  boldText: {
    fontWeight: "bold"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0
  }
};

export class UserProfileChangeRequests extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  state = {
    rowsDefinition: {},
    currentlyLoading: false,
    displayResultsFound: false,
    resultsFound: 0,
    clearSearchTriggered: false,
    allUserProfileChangeRequests: [],
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    isSearchActive: this.props.userProfileChangeRequestsKeyword !== null,
    showRequestPopup: false,
    requestPopupData: {},
    isValidReviewPopupReasonForDeny: true,
    reviewPopupReasonForDenyContent: "",
    showSuccessPopup: false
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateUserProfileChangeRequestsPageState(1);
    this.populateUserProfileChangeRequestsBasedOnActiveSearch();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateUserProfileChangeRequestsBasedOnActiveSearch();
    }
    // if userProfileChangeRequestsPaginationPage gets updated
    if (
      prevProps.userProfileChangeRequestsPaginationPage !==
      this.props.userProfileChangeRequestsPaginationPage
    ) {
      this.populateUserProfileChangeRequestsBasedOnActiveSearch();
    }
    // // if userProfileChangeRequestsPaginationPageSize get updated
    if (
      prevProps.userProfileChangeRequestsPaginationPageSize !==
      this.props.userProfileChangeRequestsPaginationPageSize
    ) {
      this.populateUserProfileChangeRequestsBasedOnActiveSearch();
    }
    // // if search userProfileChangeRequestsKeyword gets updated
    if (
      prevProps.userProfileChangeRequestsKeyword !== this.props.userProfileChangeRequestsKeyword
    ) {
      // if userProfileChangeRequestsKeyword redux state is empty
      if (this.props.userProfileChangeRequestsKeyword !== "") {
        this.populateFoundUserProfileChangeRequests();
      } else if (
        this.props.userProfileChangeRequestsKeyword === "" &&
        this.props.userProfileChangeRequestsActiveSearch
      ) {
        this.populateFoundUserProfileChangeRequests();
      }
    }
    // // if userProfileChangeRequestsActiveSearch gets updated
    if (
      prevProps.userProfileChangeRequestsActiveSearch !==
      this.props.userProfileChangeRequestsActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.userProfileChangeRequestsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateAllUserProfileChangeRequests();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundUserProfileChangeRequests();
      }
    }
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  populateAllUserProfileChangeRequests = () => {
    this._isMounted = true;
    this.setState({ currentlyLoading: true }, () => {
      const allUserProfileChangeRequestsArray = [];
      this.props
        .getAllUserProfileChangeRequests(
          this.props.userProfileChangeRequestsPaginationPage,
          this.props.userProfileChangeRequestsPaginationPageSize
        )
        .then(response => {
          if (this._isMounted) {
            this.populateAllUserProfileChangeRequestsObject(
              allUserProfileChangeRequestsArray,
              response
            );
          }
        })
        .then(() => {
          if (this._isMounted) {
            if (this.state.clearSearchTriggered) {
              // go back to the first page to avoid display bugs
              this.props.updateUserProfileChangeRequestsPageState(1);
              this.setState({ clearSearchTriggered: false });
            }
          }
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({ currentlyLoading: false });
          }
        });
    });
  };

  // populating all found users based on a search
  populateFoundUserProfileChangeRequests = () => {
    // making sure that the table is not currently loading
    if (!this.state.currentlyLoading) {
      this.setState({ currentlyLoading: true }, () => {
        const allUserProfileChangeRequestsArray = [];
        setTimeout(() => {
          this.props
            .getFoundUserProfileChangeRequests(
              this.props.userProfileChangeRequestsKeyword,
              this.props.userProfileChangeRequestsPaginationPage,
              this.props.userProfileChangeRequestsPaginationPageSize
            )
            .then(response => {
              if (response[0] === "no results found") {
                this.setState({
                  allUserProfileChangeRequests: [],
                  rowsDefinition: {},
                  numberOfPages: 1,
                  nextPageNumber: 0,
                  previousPageNumber: 0,
                  resultsFound: 0
                });
                // there is at least one result found
              } else {
                this.populateAllUserProfileChangeRequestsObject(
                  allUserProfileChangeRequestsArray,
                  response
                );
                this.setState({ resultsFound: response.count });
              }
            })
            .then(() => {
              this.setState(
                {
                  currentlyLoading: false,
                  displayResultsFound: true
                },
                () => {
                  // make sure that this element exsits to avoid any error
                  if (document.getElementById("user-profile-change-requests-results-found")) {
                    document.getElementById("user-profile-change-requests-results-found").focus();
                  }
                }
              );
            });
        }, 100);
      });
    }
  };

  populateAllUserProfileChangeRequestsObject = (allUserProfileChangeRequestsArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in allUserProfileChangeRequestsArray
        allUserProfileChangeRequestsArray.push({
          username: currentResult.username,
          current_first_name: currentResult.current_first_name,
          current_last_name: currentResult.current_last_name,
          current_birth_date: currentResult.current_birth_date
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult.email,
          column_2: currentResult.current_first_name,
          column_3: currentResult.current_last_name,
          column_4: currentResult.current_birth_date,
          column_5: (
            <CustomButton
              buttonId={`user-profile-change-requests-view-row-${i}`}
              label={
                <div>
                  <FontAwesomeIcon icon={faBinoculars} />
                  <span style={styles.buttonLabel}>
                    {LOCALIZE.systemAdministrator.userProfileChangeRequests.table.reviewButton}
                  </span>
                </div>
              }
              buttonTheme={THEME.SECONDARY}
              action={e => this.openRequestPopup(currentResult)}
              ariaLabel={LOCALIZE.formatString(
                LOCALIZE.systemAdministrator.userProfileChangeRequests.table
                  .reviewButtonAccessibility,
                currentResult.current_first_name,
                currentResult.current_last_name
              )}
            />
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.LEFT_TEXT,
      column_3_style: COMMON_STYLE.LEFT_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState({
      allUserProfileChangeRequests: allUserProfileChangeRequestsArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(
        response.count / this.props.userProfileChangeRequestsPaginationPageSize
      ),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  populateUserProfileChangeRequestsBasedOnActiveSearch = () => {
    // current search
    if (this.props.userProfileChangeRequestsActiveSearch) {
      this.populateFoundUserProfileChangeRequests();
      // no search
    } else {
      this.populateAllUserProfileChangeRequests();
    }
  };

  openRequestPopup = data => {
    this.setState({ showRequestPopup: true, requestPopupData: data });
  };

  closeRequestPopup = () => {
    this.setState({
      showRequestPopup: false,
      requestPopupData: {},
      isValidReviewPopupReasonForDeny: true,
      reviewPopupReasonForDenyContent: ""
    });
  };

  openSuccessPopup = () => {
    this.setState({ showSuccessPopup: true });
  };

  closeSuccessPopup = () => {
    this.setState({ showSuccessPopup: false });
  };

  handleDenyRequest = () => {
    // reason for deny has NOT been provided
    if (this.state.reviewPopupReasonForDenyContent === "") {
      this.setState({ isValidReviewPopupReasonForDeny: false }, () => {
        // focusing on respective input
        if (document.getElementById("popup-reason-for-deny-input")) {
          document.getElementById("popup-reason-for-deny-input").focus();
        }
      });
      // reason for deny has been provided
    } else {
      const body = this.state.requestPopupData;
      body.reason_for_deny = this.state.reviewPopupReasonForDenyContent;

      // approving request
      this.props.denyUserProfileChangeRequest(body).then(response => {
        // success request
        if (response.ok) {
          // closing request popup
          this.closeRequestPopup();
          // showing success popup
          this.openSuccessPopup();
          // updating table
          this.populateUserProfileChangeRequestsBasedOnActiveSearch();
          // should never happen
        } else {
          throw new Error(
            "An error occurred during the approve user profile change request process"
          );
        }
      });
    }
  };

  handleApproveRequest = () => {
    // initializing body
    const body = this.state.requestPopupData;
    // approving request
    this.props.approveUserProfileChangeRequest(body).then(response => {
      // success request
      if (response.ok) {
        // closing request popup
        this.closeRequestPopup();
        // showing success popup
        this.openSuccessPopup();
        // updating table
        this.populateUserProfileChangeRequestsBasedOnActiveSearch();
        // should never happen
      } else {
        throw new Error("An error occurred during the approve user profile change request process");
      }
    });
  };

  updateReviewPopupReasonForDenyContent = event => {
    const reviewPopupReasonForDenyContent = event.target.value;
    this.setState({
      reviewPopupReasonForDenyContent: reviewPopupReasonForDenyContent,
      isValidReviewPopupReasonForDeny: true
    });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.userProfileChangeRequests.table.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.userProfileChangeRequests.table.columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.userProfileChangeRequests.table.columnThree,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.userProfileChangeRequests.table.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.systemAdministrator.userProfileChangeRequests.table.columnFive,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div>
        <h2>{LOCALIZE.systemAdministrator.userProfileChangeRequests.title}</h2>
        <SearchBarWithDisplayOptions
          idPrefix={"user-profile-change-requests"}
          currentlyLoading={this.state.currentlyLoading}
          updateSearchStates={this.props.updateSearchUserProfileChangeRequestsStates}
          updatePageState={this.props.updateUserProfileChangeRequestsPageState}
          paginationPageSize={Number(this.props.userProfileChangeRequestsPaginationPageSize)}
          updatePaginationPageSize={this.props.updateUserProfileChangeRequestsPageSizeState}
          data={this.state.allUserProfileChangeRequests}
          resultsFound={this.state.resultsFound}
          searchBarContent={this.props.userProfileChangeRequestsKeyword}
        />
        <GenericTable
          classnamePrefix="user-profile-change-requests"
          columnsDefinition={columnsDefinition}
          rowsDefinition={this.state.rowsDefinition}
          emptyTableMessage={LOCALIZE.systemAdministrator.userProfileChangeRequests.table.noData}
          currentlyLoading={this.state.currentlyLoading}
        />
        <Pagination
          paginationContainerId={"user-profile-change-requests-pagination-pages-link"}
          pageCount={this.state.numberOfPages}
          paginationPage={this.props.userProfileChangeRequestsPaginationPage}
          updatePaginationPageState={this.props.updateUserProfileChangeRequestsPageState}
          firstTableRowId={"user-profile-change-requests-table-row-0"}
        />
        {Object.keys(this.state.requestPopupData).length > 0 && (
          <PopupBox
            show={this.state.showRequestPopup}
            title={LOCALIZE.systemAdministrator.userProfileChangeRequests.popup.title}
            displayCloseButton={true}
            closeButtonAction={this.closeRequestPopup}
            handleClose={this.closeRequestPopup}
            closeOnButtonAction={false}
            shouldCloseOnEsc={true}
            isBackdropStatic={true}
            description={
              <div>
                <p>{LOCALIZE.systemAdministrator.userProfileChangeRequests.popup.description}</p>
                <div style={styles.popupInputsContainer}>
                  <Row className="align-items-center justify-content-end" style={styles.popupRow}>
                    <Col
                      xl={columnSizes.popupSecondColumn.xl}
                      lg={columnSizes.popupSecondColumn.lg}
                      md={columnSizes.popupSecondColumn.md}
                      sm={columnSizes.popupSecondColumn.sm}
                      xs={columnSizes.popupSecondColumn.xs}
                    >
                      <label style={styles.columnTitle} id="review-popup-current-label">
                        {LOCALIZE.systemAdministrator.userProfileChangeRequests.popup.current}
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.popupThirdColumn.xl}
                      lg={columnSizes.popupThirdColumn.lg}
                      md={columnSizes.popupThirdColumn.md}
                      sm={columnSizes.popupThirdColumn.sm}
                      xs={columnSizes.popupThirdColumn.xs}
                    >
                      <label style={styles.columnTitle} id="review-popup-new-label">
                        {LOCALIZE.systemAdministrator.userProfileChangeRequests.popup.new}
                      </label>
                    </Col>
                  </Row>
                  <Row
                    className="align-items-center justify-content-center"
                    style={styles.popupRow}
                  >
                    <Col
                      xl={columnSizes.popupFirstColumn.xl}
                      lg={columnSizes.popupFirstColumn.lg}
                      md={columnSizes.popupFirstColumn.md}
                      sm={columnSizes.popupFirstColumn.sm}
                      xs={columnSizes.popupFirstColumn.xs}
                    >
                      <label
                        id="review-popup-first-name-label"
                        htmlFor="review-popup-new-first-name-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.systemAdministrator.userProfileChangeRequests.popup
                            .firstNameLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.popupSecondColumn.xl}
                      lg={columnSizes.popupSecondColumn.lg}
                      md={columnSizes.popupSecondColumn.md}
                      sm={columnSizes.popupSecondColumn.sm}
                      xs={columnSizes.popupSecondColumn.xs}
                    >
                      <input
                        id="review-popup-current-first-name-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        aria-labelledby="review-popup-current-label review-popup-first-name-label"
                        style={{
                          ...styles.input,
                          ...accommodationsStyle
                        }}
                        type="text"
                        value={this.state.requestPopupData.current_first_name}
                        aria-disabled={true}
                      />
                    </Col>
                    <Col
                      xl={columnSizes.popupThirdColumn.xl}
                      lg={columnSizes.popupThirdColumn.lg}
                      md={columnSizes.popupThirdColumn.md}
                      sm={columnSizes.popupThirdColumn.sm}
                      xs={columnSizes.popupThirdColumn.xs}
                    >
                      <div>
                        <input
                          id="review-popup-new-first-name-input"
                          className={"valid-field input-disabled-look"}
                          aria-labelledby="review-popup-new-label review-popup-first-name-label"
                          style={{ ...styles.input, ...accommodationsStyle }}
                          type="text"
                          placeholder={LOCALIZE.commons.unchanged}
                          value={this.state.requestPopupData.new_first_name}
                          aria-disabled={true}
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row
                    className="align-items-center justify-content-center"
                    style={styles.popupRow}
                  >
                    <Col
                      xl={columnSizes.popupFirstColumn.xl}
                      lg={columnSizes.popupFirstColumn.lg}
                      md={columnSizes.popupFirstColumn.md}
                      sm={columnSizes.popupFirstColumn.sm}
                      xs={columnSizes.popupFirstColumn.xs}
                    >
                      <label
                        id="review-popup-last-name-label"
                        htmlFor="review-popup-new-last-name-input"
                        style={styles.boldText}
                      >
                        {LOCALIZE.systemAdministrator.userProfileChangeRequests.popup.lastNameLabel}
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.popupSecondColumn.xl}
                      lg={columnSizes.popupSecondColumn.lg}
                      md={columnSizes.popupSecondColumn.md}
                      sm={columnSizes.popupSecondColumn.sm}
                      xs={columnSizes.popupSecondColumn.xs}
                    >
                      <input
                        id="review-popup-current-last-name-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        aria-labelledby="review-popup-current-label review-popup-last-name-label"
                        style={{
                          ...styles.input,
                          ...accommodationsStyle
                        }}
                        type="text"
                        value={this.state.requestPopupData.current_last_name}
                        aria-disabled={true}
                      />
                    </Col>
                    <Col
                      xl={columnSizes.popupThirdColumn.xl}
                      lg={columnSizes.popupThirdColumn.lg}
                      md={columnSizes.popupThirdColumn.md}
                      sm={columnSizes.popupThirdColumn.sm}
                      xs={columnSizes.popupThirdColumn.xs}
                    >
                      <input
                        id="review-popup-new-last-name-input"
                        className={"valid-field input-disabled-look"}
                        aria-labelledby="review-popup-new-label review-popup-last-name-label"
                        style={{ ...styles.input, ...accommodationsStyle }}
                        type="text"
                        placeholder={LOCALIZE.commons.unchanged}
                        value={this.state.requestPopupData.new_last_name}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row
                    className="align-items-center justify-content-center"
                    style={styles.popupRow}
                  >
                    <Col
                      xl={columnSizes.popupFirstColumn.xl}
                      lg={columnSizes.popupFirstColumn.lg}
                      md={columnSizes.popupFirstColumn.md}
                      sm={columnSizes.popupFirstColumn.sm}
                      xs={columnSizes.popupFirstColumn.xs}
                    >
                      <label
                        id="review-popup-dob-label"
                        htmlFor="review-popup-new-dob-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.systemAdministrator.userProfileChangeRequests.popup
                            .dateOfBirthLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.popupSecondColumn.xl}
                      lg={columnSizes.popupSecondColumn.lg}
                      md={columnSizes.popupSecondColumn.md}
                      sm={columnSizes.popupSecondColumn.sm}
                      xs={columnSizes.popupSecondColumn.xs}
                    >
                      <DatePicker
                        dateLabelId={"review-popup-current-dob-fields"}
                        ariaLabelledBy={"review-popup-current-label review-popup-dob-label"}
                        customPlaceholder={LOCALIZE.commons.unchanged}
                        initialDateDayValue={{
                          value: Number(
                            this.state.requestPopupData.current_birth_date.split("-")[2]
                          ),
                          label: `${this.state.requestPopupData.current_birth_date.split("-")[2]}`
                        }}
                        initialDateMonthValue={{
                          value: Number(
                            this.state.requestPopupData.current_birth_date.split("-")[1]
                          ),
                          label: `${this.state.requestPopupData.current_birth_date.split("-")[1]}`
                        }}
                        initialDateYearValue={{
                          value: Number(
                            this.state.requestPopupData.current_birth_date.split("-")[0]
                          ),
                          label: `${this.state.requestPopupData.current_birth_date.split("-")[0]}`
                        }}
                        disabledDropdowns={true}
                      />
                    </Col>
                    <Col
                      xl={columnSizes.popupThirdColumn.xl}
                      lg={columnSizes.popupThirdColumn.lg}
                      md={columnSizes.popupThirdColumn.md}
                      sm={columnSizes.popupThirdColumn.sm}
                      xs={columnSizes.popupThirdColumn.xs}
                    >
                      {/* TODO: fix accessibility issues (screenreader is reading the current DOB values values) */}
                      <DatePicker
                        dateLabelId={"review-popup-new-dob-fields"}
                        ariaLabelledBy={"review-popup-new-label review-popup-dob-label"}
                        customPlaceholder={LOCALIZE.commons.unchanged}
                        initialDateDayValue={
                          this.state.requestPopupData.new_birth_date !== null && {
                            value: Number(this.state.requestPopupData.new_birth_date.split("-")[2]),
                            label: `${this.state.requestPopupData.new_birth_date.split("-")[2]}`
                          }
                        }
                        initialDateMonthValue={
                          this.state.requestPopupData.new_birth_date !== null && {
                            value: Number(this.state.requestPopupData.new_birth_date.split("-")[1]),
                            label: `${this.state.requestPopupData.new_birth_date.split("-")[1]}`
                          }
                        }
                        initialDateYearValue={
                          this.state.requestPopupData.new_birth_date !== null && {
                            value: Number(this.state.requestPopupData.new_birth_date.split("-")[0]),
                            label: `${this.state.requestPopupData.new_birth_date.split("-")[0]}`
                          }
                        }
                        disabledDropdowns={true}
                      />
                    </Col>
                  </Row>
                  <Row
                    className="align-items-center justify-content-center"
                    style={styles.popupRow}
                  >
                    <Col
                      xl={columnSizes.popupFirstColumn.xl}
                      lg={columnSizes.popupFirstColumn.lg}
                      md={columnSizes.popupFirstColumn.md}
                      sm={columnSizes.popupFirstColumn.sm}
                      xs={columnSizes.popupFirstColumn.xs}
                    >
                      <label
                        id="review-popup-comments-label"
                        htmlFor="review-popup-comments-input"
                        style={styles.boldText}
                      >
                        {LOCALIZE.systemAdministrator.userProfileChangeRequests.popup.commentsLabel}
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.popupSecondColumn.xl + columnSizes.popupThirdColumn.xl}
                      lg={columnSizes.popupSecondColumn.lg + columnSizes.popupThirdColumn.xl}
                      md={columnSizes.popupSecondColumn.md + columnSizes.popupThirdColumn.xl}
                      sm={columnSizes.popupSecondColumn.sm}
                      xs={columnSizes.popupSecondColumn.xs}
                    >
                      <textarea
                        id="popup-comments-input"
                        className={"valid-field input-disabled-look"}
                        style={{ ...styles.textAreaInput, ...accommodationsStyle }}
                        value={this.state.requestPopupData.comments}
                        aria-disabled={true}
                      ></textarea>
                    </Col>
                  </Row>
                  <Row
                    className="align-items-center justify-content-center"
                    style={styles.popupRow}
                  >
                    <Col
                      xl={columnSizes.popupFirstColumn.xl}
                      lg={columnSizes.popupFirstColumn.lg}
                      md={columnSizes.popupFirstColumn.md}
                      sm={columnSizes.popupFirstColumn.sm}
                      xs={columnSizes.popupFirstColumn.xs}
                    >
                      <label
                        id="review-popup-reason-for-deny-label"
                        htmlFor="review-popup-reason-for-deny-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.systemAdministrator.userProfileChangeRequests.popup
                            .reasonForDenyLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.popupSecondColumn.xl + columnSizes.popupThirdColumn.xl}
                      lg={columnSizes.popupSecondColumn.lg + columnSizes.popupThirdColumn.xl}
                      md={columnSizes.popupSecondColumn.md + columnSizes.popupThirdColumn.xl}
                      sm={columnSizes.popupSecondColumn.sm}
                      xs={columnSizes.popupSecondColumn.xs}
                    >
                      <textarea
                        id="popup-reason-for-deny-input"
                        className={
                          this.state.isValidReviewPopupReasonForDeny
                            ? "valid-field"
                            : "invalid-field"
                        }
                        style={{ ...styles.textAreaInput, ...accommodationsStyle }}
                        value={this.state.reviewPopupReasonForDenyContent}
                        onChange={this.updateReviewPopupReasonForDenyContent}
                        maxLength="255"
                      ></textarea>
                    </Col>
                  </Row>
                  {!this.state.isValidReviewPopupReasonForDeny && (
                    <Row className="align-items-center justify-content-end">
                      <Col
                        xl={columnSizes.popupSecondColumn.xl + columnSizes.popupThirdColumn.xl}
                        lg={columnSizes.popupSecondColumn.lg + columnSizes.popupThirdColumn.xl}
                        md={columnSizes.popupSecondColumn.md + columnSizes.popupThirdColumn.xl}
                        sm={columnSizes.popupSecondColumn.sm}
                        xs={columnSizes.popupSecondColumn.xs}
                      >
                        <label
                          id="review-popup-reason-for-deny-error"
                          htmlFor="review-popup-reason-for-deny-input"
                          style={styles.errorMessage}
                        >
                          {
                            LOCALIZE.systemAdministrator.userProfileChangeRequests.popup
                              .reasonForDenyError
                          }
                        </label>
                      </Col>
                    </Row>
                  )}
                </div>
              </div>
            }
            leftButtonType={BUTTON_TYPE.danger}
            leftButtonIcon={faTimes}
            leftButtonTitle={
              LOCALIZE.systemAdministrator.userProfileChangeRequests.popup.leftButton
            }
            leftButtonLabel={
              LOCALIZE.systemAdministrator.userProfileChangeRequests.popup.leftButton
            }
            leftButtonAction={this.handleDenyRequest}
            rightButtonType={BUTTON_TYPE.success}
            rightButtonIcon={faCheck}
            rightButtonTitle={
              LOCALIZE.systemAdministrator.userProfileChangeRequests.popup.rightButton
            }
            rightButtonLabel={
              LOCALIZE.systemAdministrator.userProfileChangeRequests.popup.rightButton
            }
            rightButtonAction={this.handleApproveRequest}
          />
        )}
        <PopupBox
          show={this.state.showSuccessPopup}
          title={LOCALIZE.systemAdministrator.userProfileChangeRequests.successPopup.title}
          closeButtonAction={this.closeRequestPopup}
          handleClose={() => {}}
          size="lg"
          description={
            <SystemMessage
              messageType={MESSAGE_TYPE.success}
              title={LOCALIZE.commons.success}
              message={
                <p>
                  {
                    LOCALIZE.systemAdministrator.userProfileChangeRequests.successPopup
                      .systemMessageDescription
                  }
                </p>
              }
            />
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faTimes}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonLabel={LOCALIZE.commons.close}
          rightButtonAction={this.closeSuccessPopup}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    userProfileChangeRequestsPaginationPageSize:
      state.userProfileChangeRequests.userProfileChangeRequestsPaginationPageSize,
    userProfileChangeRequestsPaginationPage:
      state.userProfileChangeRequests.userProfileChangeRequestsPaginationPage,
    userProfileChangeRequestsKeyword:
      state.userProfileChangeRequests.userProfileChangeRequestsKeyword,
    userProfileChangeRequestsActiveSearch:
      state.userProfileChangeRequests.userProfileChangeRequestsActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllUserProfileChangeRequests,
      getFoundUserProfileChangeRequests,
      updateUserProfileChangeRequestsPageState,
      updateUserProfileChangeRequestsPageSizeState,
      updateSearchUserProfileChangeRequestsStates,
      approveUserProfileChangeRequest,
      denyUserProfileChangeRequest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileChangeRequests);
