import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import FontSize from "../commons/FontSize";
import FontFamily from "../commons/FontFamily";
import Spacing from "../commons/Spacing";
import { Row } from "react-bootstrap";

const styles = {
  mainContainer: {
    width: "100%"
  },
  container: {
    padding: "12px 0 0 24px"
  },
  description: {
    padding: "0 0 0 12px"
  },
  subtitle: {
    fontWeight: "bold"
  },
  itemContainer: {
    padding: "12px 0 0 24px"
  },
  accessibilityElementsContainer: {
    padding: "0 0 0 24px"
  },
  checkbox: {
    margin: "0 24px 0 48px",
    transform: "scale(1.5)",
    verticalAlign: "middle"
  },
  label: {
    margin: 0,
    padding: 6
  }
};

// notifications preferences definition
const notificationsPreferences = () => {
  return [
    { text: LOCALIZE.profile.preferences.notifications.checkBoxOne, checked: false },
    { text: LOCALIZE.profile.preferences.notifications.checkBoxTwo, checked: false }
  ];
};

// display preferences definition
const displayPreferences = () => {
  return [
    { text: LOCALIZE.profile.preferences.display.checkBoxOne, checked: false },
    { text: LOCALIZE.profile.preferences.display.checkBoxTwo, checked: false }
  ];
};

class Preferences extends Component {
  state = {
    notificationsPreferences: notificationsPreferences(),
    displayPreferences: displayPreferences()
  };

  // update notifications checkboxes status
  toggleNotificationsCheckbox = id => {
    const updatedNotificationsPreferences = Array.from(this.state.notificationsPreferences);
    updatedNotificationsPreferences[id].checked = !updatedNotificationsPreferences[id].checked;
    this.setState({ notificationsPreferences: updatedNotificationsPreferences });
  };

  // update display checkboxes status
  toggleDisplayCheckbox = id => {
    const updatedDisplayPreferences = Array.from(this.state.displayPreferences);
    updatedDisplayPreferences[id].checked = !updatedDisplayPreferences[id].checked;
    this.setState({ displayPreferences: updatedDisplayPreferences });
  };

  render() {
    return (
      <div style={styles.mainContainer}>
        <Row>
          <h2>{LOCALIZE.profile.preferences.title}</h2>
        </Row>
        <Row>
          <p style={styles.description}>{LOCALIZE.profile.preferences.description}</p>
        </Row>
        <div style={styles.container}>
          <Row>
            <p style={styles.subtitle}>{LOCALIZE.profile.preferences.accessibility.title}</p>
          </Row>
          {/* TODO: Uncomment related code in Preferences.test when we'll put back these preferences /*}
            {/* <span style={styles.subtitle}>{LOCALIZE.profile.preferences.notifications.title}</span>
            <div>
              {this.state.notificationsPreferences.map((preference, id) => {
                return (
                  <div key={id}>
                    <input
                      id={`notification-checkbox-${id}`}
                      type="checkbox"
                      style={styles.checkbox}
                      checked={preference.checked}
                      onChange={() => {
                        this.toggleNotificationsCheckbox(id);
                      }}
                    ></input>
                    <label htmlFor={`notification-checkbox-${id}`} style={styles.label}>
                      {preference.text}
                    </label>
                  </div>
                );
              })}
            </div>
          </div>
          <div style={styles.contentContainer}>
            <span style={styles.subtitle}>{LOCALIZE.profile.preferences.display.title}</span>
            <div>
              {this.state.displayPreferences.map((preference, id) => {
                return (
                  <div key={id}>
                    <input
                      id={`display-checkbox-${id}`}
                      type="checkbox"
                      style={styles.checkbox}
                      checked={preference.checked}
                      onChange={() => {
                        this.toggleDisplayCheckbox(id);
                      }}
                    ></input>
                    <label htmlFor={`display-checkbox-${id}`} style={styles.label}>
                      {preference.text}
                    </label>
                  </div>
                );
              })}
            </div> */}
          <div>
            <Row>
              <p>{LOCALIZE.profile.preferences.accessibility.description}</p>
            </Row>
            <div style={styles.accessibilityElementsContainer}>
              <FontSize />
              <FontFamily />
              <Spacing />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export { Preferences as unconnectedPreferences };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Preferences);
