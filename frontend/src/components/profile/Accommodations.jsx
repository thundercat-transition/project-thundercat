import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Col, Container, Row } from "react-bootstrap";
import Switch from "react-switch";
import getSwitchTransformScale from "../../helpers/switchTransformScale";
import {
  getUserAccommodationsProfile,
  saveUserAccommodationsProfile
} from "../../modules/UserProfileRedux";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSave, faSpinner } from "@fortawesome/free-solid-svg-icons";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import * as _ from "lodash";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import getCheckboxTransformScale from "../../helpers/checkboxTransformScale";
import PrivacyNoticeStatement from "../commons/PrivacyNoticeStatement";

const DESCRIPTION_MAX_LENGTH = 4000;

const styles = {
  mainContainer: {
    width: "100%"
  },
  questionsContainer: {
    padding: "12px 25px",
    width: "100%"
  },
  updateButtonContainer: {
    marginTop: 24,
    textAlign: "center"
  },
  updateButton: {
    marginRight: 50,
    minWidth: 100
  },
  saveButtonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  textareaInput: {
    minHeight: 85,
    overflowY: "auto",
    resize: "none",
    display: "table-cell",
    width: "100%"
  },
  textareaErrorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginLeft: 4
  },
  switch: {
    float: "inline-end"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  span: {
    verticalAlign: "top"
  },
  questionRow: {
    marginTop: "24px"
  },
  checkboxContainer: {
    paddingLeft: "24px"
  },
  checkbox: {
    verticalAlign: "middle"
  },
  privacyNoticeLink: {
    textDecoration: "underline",
    color: "#0278A4",
    cursor: "pointer",
    backgroundColor: "transparent",
    border: "none",
    padding: 0
  },
  checkboxLabel: {
    marginLeft: "15px"
  },
  loading: {
    fontSize: "32px",
    padding: 6
  },
  loadingContainer: {
    margin: 24,
    textAlign: "center"
  },
  pleaseDescribeMargin: {
    margin: "12px 0 6px 0"
  }
};

const columnSizes = {
  firstColumn: {
    xs: 9,
    sm: 9,
    md: 11,
    lg: 11,
    xl: 11
  },
  secondColumn: {
    xs: 3,
    sm: 3,
    md: 1,
    lg: 1,
    xl: 1
  },
  firstFullColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  },
  secondFullColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  }
};

class Accommodations extends Component {
  static propTypes = {
    // Props from Redux
    getUserAccommodationsProfile: PropTypes.func,
    saveUserAccommodationsProfile: PropTypes.func
  };

  state = {
    // Ensures no errors are shown on page load
    isFirstLoad: true,

    isLoading: false,
    // default values
    switchHeight: 25,
    switchWidth: 50,
    userAccommodationsProfile: {},
    initialUserAccommodationsProfile: {},
    // Fields Validation
    isValidBuiltInAccessibility: true,
    isValidExtraTime: true,
    isValidBreaks: true,
    isValidAdaptiveTech: true,
    isValidErgonomic: true,
    isValidAccessAssistance: true,
    isValidEnvironment: true,
    isValidSchedulingAdaptation: true,
    isValidOtherNeedsWritten: true,
    isValidOtherNeedsOral: true,
    isValidRepeatedOralQuestions: true,
    isValidPreviouslyAccommodated: true,
    changeDetected: false,
    // Privacy Notice
    isPrivacyNoticeChecked: false,
    showPrivacyNoticePopup: false,
    // Understand Notice
    isUnderstandNoticeChecked: false,
    // Popup
    saveSuccessPopup: false,
    showErrorPopup: false
  };

  componentDidMount = () => {
    this.setState({ isLoading: true }, () => {
      this.getUpdatedSwitchDimensions();
      this.props.getUserAccommodationsProfile().then(response => {
        this.setState(
          {
            userAccommodationsProfile: _.cloneDeep(response),
            initialUserAccommodationsProfile: _.cloneDeep(response)
          },
          () => {
            this.setState({ isLoading: false });
          }
        );
      });
    });
  };

  componentDidUpdate = (prevProps, prevState) => {
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
    // if userAccommodationsProfile gets updated
    if (prevState.userAccommodationsProfile !== this.state.userAccommodationsProfile) {
      // If the page hasn't just loaded (empty object)
      if (Object.keys(prevState.userAccommodationsProfile).length !== 0) {
        // If is not equal to initial state, change the detected state
        if (
          !_.isEqual(
            this.state.userAccommodationsProfile,
            this.state.initialUserAccommodationsProfile
          )
        ) {
          this.setState({
            changeDetected: true
          });
        }
        // If we go back to initial state, change detected at false
        if (
          _.isEqual(
            this.state.userAccommodationsProfile,
            this.state.initialUserAccommodationsProfile
          )
        ) {
          this.setState({
            changeDetected: false
          });
        }
      }
    }
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoading: false });
        }
      );
    });
  };

  // Built-In Accommodations
  onChangeBuiltInAccommodations = checked => {
    const userAccommodationsProfile = { ...this.state.userAccommodationsProfile };
    userAccommodationsProfile.has_built_in_accessibility = checked;
    this.setState({
      userAccommodationsProfile: userAccommodationsProfile,
      isValidBuiltInAccessibility: true
    });
  };

  // Additional Time
  onChangeAdditionalTime = checked => {
    const userAccommodationsProfile = { ...this.state.userAccommodationsProfile };
    userAccommodationsProfile.needs_extra_time = checked;
    this.setState({
      userAccommodationsProfile: userAccommodationsProfile,
      isValidExtraTime: true
    });
  };

  // Breaks Time
  onChangeBreaks = checked => {
    const userAccommodationsProfile = { ...this.state.userAccommodationsProfile };
    userAccommodationsProfile.needs_breaks = checked;
    this.setState({
      userAccommodationsProfile: userAccommodationsProfile,
      isValidBreaks: true
    });
  };

  // Adaptive Tech
  onChangeAdaptiveTech = checked => {
    const userAccommodationsProfile = { ...this.state.userAccommodationsProfile };
    userAccommodationsProfile.needs_adaptive_tech = checked;
    this.setState({
      userAccommodationsProfile: userAccommodationsProfile,
      isValidAdaptiveTech: true
    });
  };

  // Ergonomic
  onChangeErgonomic = checked => {
    const userAccommodationsProfile = { ...this.state.userAccommodationsProfile };
    userAccommodationsProfile.needs_ergonomic = checked;
    this.setState({
      userAccommodationsProfile: userAccommodationsProfile,
      isValidErgonomic: true
    });
  };

  // Access Assistance
  onChangeAccessAssistance = checked => {
    const userAccommodationsProfile = { ...this.state.userAccommodationsProfile };
    userAccommodationsProfile.needs_access_assistance = checked;
    this.setState({
      userAccommodationsProfile: userAccommodationsProfile,
      isValidAccessAssistance: true
    });
  };

  // Environment
  onChangeEnvironment = checked => {
    const userAccommodationsProfile = { ...this.state.userAccommodationsProfile };
    userAccommodationsProfile.needs_environment = checked;
    this.setState({
      userAccommodationsProfile: userAccommodationsProfile,
      isValidEnvironment: true
    });
  };

  // Scheduling Adaptation
  onChangeSchedulingAdaptation = checked => {
    const userAccommodationsProfile = { ...this.state.userAccommodationsProfile };
    userAccommodationsProfile.needs_scheduling_adaptation = checked;
    this.setState({
      userAccommodationsProfile: userAccommodationsProfile,
      isValidSchedulingAdaptation: true
    });
  };

  // Other Needs Written
  onChangeOtherNeedsWritten = checked => {
    const userAccommodationsProfile = { ...this.state.userAccommodationsProfile };
    userAccommodationsProfile.has_other_needs_written = checked;
    this.setState({
      userAccommodationsProfile: userAccommodationsProfile,
      isValidOtherNeedsWritten: true
    });
  };

  // Other Needs Oral
  onChangeOtherNeedsOral = checked => {
    const userAccommodationsProfile = { ...this.state.userAccommodationsProfile };
    userAccommodationsProfile.has_other_needs_oral = checked;
    this.setState({
      userAccommodationsProfile: userAccommodationsProfile,
      isValidOtherNeedsOral: true
    });
  };

  // Repeated Oral Questions
  onChangeRepeatedOralQuestions = checked => {
    const userAccommodationsProfile = { ...this.state.userAccommodationsProfile };
    userAccommodationsProfile.needs_repeated_oral_questions = checked;
    this.setState({
      userAccommodationsProfile: userAccommodationsProfile,
      isValidRepeatedOralQuestions: true
    });
  };

  // Previously Accommodated
  onChangePreviouslyAccommodated = checked => {
    const userAccommodationsProfile = { ...this.state.userAccommodationsProfile };
    userAccommodationsProfile.previously_accommodated = checked;
    this.setState({
      userAccommodationsProfile: userAccommodationsProfile,
      isValidPreviouslyAccommodated: true
    });
  };

  // Description - Used for all fields that need a description text
  handleDescriptionChange = (value, prefix) => {
    const temp = { ...this.state.userAccommodationsProfile };
    const isValid = value !== "";

    // Built-in Accessibility
    if (prefix === "built-in-accessibility-description") {
      temp.built_in_accessibility.description = value;

      this.setState({
        userAccommodationsProfile: temp,
        isValidBuiltInAccessibility: isValid
      });
    }
    // Extra Time
    if (prefix === "extra-time-description") {
      temp.extra_time.description = value;

      this.setState({
        userAccommodationsProfile: temp,
        isValidExtraTime: isValid
      });
    }
    // Breaks
    if (prefix === "breaks-description") {
      temp.breaks.description = value;

      this.setState({
        userAccommodationsProfile: temp,
        isValidBreaks: isValid
      });
    }
    // Adaptive Tech
    if (prefix === "adaptive-tech-description") {
      temp.adaptive_tech.description = value;

      this.setState({
        userAccommodationsProfile: temp,
        isValidAdaptiveTech: isValid
      });
    }
    // Ergonomic
    else if (prefix === "ergonomic-description") {
      temp.ergonomic.description = value;

      this.setState({
        userAccommodationsProfile: temp,
        isValidErgonomic: isValid
      });
    }
    // Access Assistance
    else if (prefix === "access-assistance-description") {
      temp.access_assistance.description = value;

      this.setState({
        userAccommodationsProfile: temp,
        isValidAccessAssistance: isValid
      });
    }
    // Environment
    else if (prefix === "environment-description") {
      temp.environment.description = value;

      this.setState({
        userAccommodationsProfile: temp,
        isValidEnvironment: isValid
      });
    }
    // Scheduling Adaptation
    else if (prefix === "scheduling-adaptation-description") {
      temp.scheduling_adaptation.description = value;

      this.setState({
        userAccommodationsProfile: temp,
        isValidSchedulingAdaptation: isValid
      });
    }
    // Other Needs Written
    else if (prefix === "other-needs-written-description") {
      temp.other_needs_written.description = value;

      this.setState({
        userAccommodationsProfile: temp,
        isValidOtherNeedsWritten: isValid
      });
    }
    // Other Needs Oral
    else if (prefix === "other-needs-oral-description") {
      temp.other_needs_oral.description = value;

      this.setState({
        userAccommodationsProfile: temp,
        isValidOtherNeedsOral: isValid
      });
    }
    // Repeated Oral Questions
    else if (prefix === "repeated-oral-questions-description") {
      temp.repeated_oral_questions.description = value;

      this.setState({
        userAccommodationsProfile: temp,
        isValidRepeatedOralQuestions: isValid
      });
    }
    // Previously Accommodated
    else if (prefix === "previously-accommodated-description") {
      temp.previous_accommodation.description = value;

      this.setState({
        userAccommodationsProfile: temp,
        isValidPreviouslyAccommodated: isValid
      });
    }
  };

  // Gets the "OTHER" option from the options list
  getOtherCodenameOption = options => {
    return options.find(option => {
      return option.codename === "other";
    });
  };

  // Privacy Notice Checkbox
  changePrivacyCheckboxStatus = event => {
    this.setState(
      {
        isPrivacyNoticeChecked: event.target.checked
      },
      () => {
        this.validateForm();
      }
    );
  };

  // Privacy Notice Popup
  openPrivacyNoticePopup = () => {
    this.setState({
      showPrivacyNoticePopup: true
    });
  };

  closePrivacyNoticePopup = () => {
    this.setState({
      showPrivacyNoticePopup: false
    });
  };

  // Understand Notice Checkbox
  changeUnderstandCheckboxStatus = event => {
    this.setState(
      {
        isUnderstandNoticeChecked: event.target.checked
      },
      () => {
        this.validateForm();
      }
    );
  };

  // Error Popup
  handleErrorPopupClose = () => {
    this.setState({
      showErrorPopup: false
    });
  };

  // Complete validation of the form
  validateForm = () => {
    // Initial values for each field
    let isValidBuiltInAccessibility = true;
    let isValidAdaptiveTech = true;
    let isValidErgonomic = true;
    let isValidAccessAssistance = true;
    let isValidEnvironment = true;
    let isValidSchedulingAdaptation = true;
    let isValidOtherNeedsWritten = true;
    let isValidOtherNeedsOral = true;
    let isValidRepeatedOralQuestions = true;
    let isValidPreviouslyAccommodated = true;
    let isValidBreaks = true;
    let isValidExtraTime = true;

    // Validate Built-in Accessibility
    isValidBuiltInAccessibility = !(
      this.state.userAccommodationsProfile.has_built_in_accessibility &&
      this.state.userAccommodationsProfile.built_in_accessibility.description === ""
    );

    // Validate Extra Time
    isValidExtraTime = !(
      this.state.userAccommodationsProfile.needs_extra_time &&
      this.state.userAccommodationsProfile.extra_time.description === ""
    );

    // Validate Breaks
    isValidBreaks = !(
      this.state.userAccommodationsProfile.needs_breaks &&
      this.state.userAccommodationsProfile.breaks.description === ""
    );

    // Validate Adaptive Tech
    isValidAdaptiveTech = !(
      this.state.userAccommodationsProfile.needs_adaptive_tech &&
      this.state.userAccommodationsProfile.adaptive_tech.description === ""
    );

    // Validate Ergonomic
    isValidErgonomic = !(
      this.state.userAccommodationsProfile.needs_ergonomic &&
      this.state.userAccommodationsProfile.ergonomic.description === ""
    );

    // Validate Access Assistance
    isValidAccessAssistance = !(
      this.state.userAccommodationsProfile.needs_access_assistance &&
      this.state.userAccommodationsProfile.access_assistance.description === ""
    );

    // Validate Environment
    isValidEnvironment = !(
      this.state.userAccommodationsProfile.needs_environment &&
      this.state.userAccommodationsProfile.environment.description === ""
    );

    // Validate Scheduling Adaptation
    isValidSchedulingAdaptation = !(
      this.state.userAccommodationsProfile.needs_scheduling_adaptation &&
      this.state.userAccommodationsProfile.scheduling_adaptation.description === ""
    );

    // Validate Other Needs Written
    isValidOtherNeedsWritten = !(
      this.state.userAccommodationsProfile.has_other_needs_written &&
      this.state.userAccommodationsProfile.other_needs_written.description === ""
    );

    // Validate Other Needs Oral
    isValidOtherNeedsOral = !(
      this.state.userAccommodationsProfile.has_other_needs_oral &&
      this.state.userAccommodationsProfile.other_needs_oral.description === ""
    );

    // Validate Repeated Oral Questions
    isValidRepeatedOralQuestions = !(
      this.state.userAccommodationsProfile.needs_repeated_oral_questions &&
      this.state.userAccommodationsProfile.repeated_oral_questions.description === ""
    );

    // Validate Previously Accommodated
    isValidPreviouslyAccommodated = !(
      this.state.userAccommodationsProfile.previously_accommodated &&
      this.state.userAccommodationsProfile.previous_accommodation.description === ""
    );

    // Updating states
    this.setState(
      {
        isValidBuiltInAccessibility: isValidBuiltInAccessibility,
        isValidExtraTime: isValidExtraTime,
        isValidBreaks: isValidBreaks,
        isValidAdaptiveTech: isValidAdaptiveTech,
        isValidErgonomic: isValidErgonomic,
        isValidAccessAssistance: isValidAccessAssistance,
        isValidEnvironment: isValidEnvironment,
        isValidSchedulingAdaptation: isValidSchedulingAdaptation,
        isValidOtherNeedsWritten: isValidOtherNeedsWritten,
        isValidOtherNeedsOral: isValidOtherNeedsOral,
        isValidRepeatedOralQuestions: isValidRepeatedOralQuestions,
        isValidPreviouslyAccommodated: isValidPreviouslyAccommodated,
        isFirstLoad: false
      },
      // Focus if there is an error
      this.focusOnHighestErrorField
    );

    // Sending the variables
    return (
      isValidBuiltInAccessibility &&
      isValidAccessAssistance &&
      isValidAdaptiveTech &&
      isValidExtraTime &&
      isValidBreaks &&
      isValidEnvironment &&
      isValidErgonomic &&
      isValidSchedulingAdaptation &&
      isValidOtherNeedsWritten &&
      isValidOtherNeedsOral &&
      isValidRepeatedOralQuestions &&
      isValidPreviouslyAccommodated &&
      this.state.isPrivacyNoticeChecked &&
      this.state.isUnderstandNoticeChecked
    );
  };

  // analyses field by field and focus on the highest error field
  focusOnHighestErrorField = () => {
    // Built-in Accessibility
    if (!this.state.isValidBuiltInAccessibility) {
      document.getElementById("built-in-accessibility-description").focus();
    }
    // Additional Time
    if (!this.state.isValidExtraTime) {
      document.getElementById("extra-time-description").focus();
    }
    //  Break Time
    else if (!this.state.isValidBreaks) {
      document.getElementById("breaks-description").focus();
    }
    // Adaptive Tech
    else if (!this.state.isValidAdaptiveTech) {
      document.getElementById("adaptive-tech-description").focus();
    }
    // Ergonomic
    else if (!this.state.isValidErgonomic) {
      document.getElementById("ergonomic-description").focus();
    }
    // Access Assistance
    else if (!this.state.isValidAccessAssistance) {
      document.getElementById("access-assistance-description").focus();
    }
    // Environment
    else if (!this.state.isValidEnvironment) {
      document.getElementById("environment-description").focus();
    }
    // Scheduling Adaptation
    else if (!this.state.isValidSchedulingAdaptation) {
      document.getElementById("scheduling-adaptation-description").focus();
    }
    // Other Needs Written
    else if (!this.state.isValidOtherNeedsWritten) {
      document.getElementById("other-needs-written-description").focus();
    }
    // Other Needs Oral
    else if (!this.state.isValidOtherNeedsOral) {
      document.getElementById("other-needs-oral-description").focus();
    }
    // Repeated Oral Questions
    else if (!this.state.isValidRepeatedOralQuestions) {
      document.getElementById("repeated-oral-questions-description").focus();
    }
    // Previously Accommodated
    else if (!this.state.isValidPreviouslyAccommodated) {
      document.getElementById("previously-accommodated-description").focus();
    }
  };

  handleSaveUserAccommodationsProfile = () => {
    const validForm = this.validateForm();

    if (validForm) {
      this.setState({ isLoading: true }, () => {
        this.props
          .saveUserAccommodationsProfile(this.state.userAccommodationsProfile)
          .then(response => {
            if (response.ok) {
              // Re-load data into initial and current UserAccommodationsProfile state
              this.props.getUserAccommodationsProfile().then(response => {
                this.setState(
                  {
                    userAccommodationsProfile: _.cloneDeep(response),
                    initialUserAccommodationsProfile: _.cloneDeep(response),
                    changeDetected: false,
                    saveSuccessPopup: true,
                    // Privacy Notice
                    isPrivacyNoticeChecked: false,
                    // Understand Notice
                    isUnderstandNoticeChecked: false
                  },
                  () => {
                    this.setState({ isLoading: false });
                  }
                );
              });
            } else {
              this.setState({
                showErrorPopup: true
              });
            }
          });
      });
    }
  };

  handleCloseSaveSuccessPopup = () => {
    this.setState({
      saveSuccessPopup: false
    });
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };

    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    return (
      <>
        <Container style={styles.mainContainer}>
          {this.state.isLoading ? (
            <div style={styles.loading}>
              {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
              <label className="fa fa-spinner fa-spin">
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            </div>
          ) : (
            <>
              <Row>
                <h2>{LOCALIZE.profile.accommodations.title}</h2>
                <p>{LOCALIZE.profile.accommodations.description}</p>
              </Row>
              <div style={styles.questionsContainer}>
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="built-in-accessibility"
                  >
                    {
                      LOCALIZE.profile.accommodations.builtInAccessibility
                        .hasBuiltInAccessibilityQuestion
                    }
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={this.onChangeBuiltInAccommodations}
                          checked={this.state.userAccommodationsProfile.has_built_in_accessibility}
                          aria-labelledby="built-in-accessibility"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.state.userAccommodationsProfile.has_built_in_accessibility && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="built-in-accessibility-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.builtInAccessibility.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="built-in-accessibility-description"
                          className={
                            this.state.isValidBuiltInAccessibility ? "valid-field" : "invalid-field"
                          }
                          aria-labelledby="built-in-accessibility-describe built-in-accessibility-description-error"
                          aria-required={true}
                          aria-invalid={!this.state.isValidBuiltInAccessibility}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.state.userAccommodationsProfile.built_in_accessibility.description
                          }
                          onChange={event =>
                            this.handleDescriptionChange(
                              event.target.value,
                              "built-in-accessibility-description"
                            )
                          }
                          maxLength={DESCRIPTION_MAX_LENGTH}
                          placeholder={LOCALIZE.profile.accommodations.placeholder}
                        ></textarea>
                        {!this.state.isValidBuiltInAccessibility && (
                          <label
                            id="built-in-accessibility-description-error"
                            htmlFor="built-in-accessibility-description"
                            style={styles.textareaErrorMessage}
                          >
                            {
                              LOCALIZE.profile.accommodations.builtInAccessibility
                                .builtInAccessibilityError
                            }
                          </label>
                        )}
                      </Col>
                    </Row>
                  </>
                )}
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="extra-time"
                  >
                    {LOCALIZE.profile.accommodations.extraTime.needsExtraTimeQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={this.onChangeAdditionalTime}
                          checked={this.state.userAccommodationsProfile.needs_extra_time}
                          aria-labelledby="extra-time"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.state.userAccommodationsProfile.needs_extra_time && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="extra-time-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.extraTime.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="extra-time-description"
                          className={this.state.isValidExtraTime ? "valid-field" : "invalid-field"}
                          aria-labelledby="extra-time-describe extra-time-description-error"
                          aria-required={true}
                          aria-invalid={!this.state.isValidExtraTime}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={this.state.userAccommodationsProfile.extra_time.description}
                          onChange={event =>
                            this.handleDescriptionChange(
                              event.target.value,
                              "extra-time-description"
                            )
                          }
                          maxLength={DESCRIPTION_MAX_LENGTH}
                          placeholder={LOCALIZE.profile.accommodations.placeholder}
                        ></textarea>
                        {!this.state.isValidExtraTime && (
                          <label
                            id="extra-time-description-error"
                            htmlFor="extra-time-description"
                            style={styles.textareaErrorMessage}
                          >
                            {LOCALIZE.profile.accommodations.extraTime.extraTimeError}
                          </label>
                        )}
                      </Col>
                    </Row>
                  </>
                )}
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="breaks"
                  >
                    {LOCALIZE.profile.accommodations.breaks.needsBreaksQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={this.onChangeBreaks}
                          checked={this.state.userAccommodationsProfile.needs_breaks}
                          aria-labelledby="breaks"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.state.userAccommodationsProfile.needs_breaks && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="breaks-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.breaks.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="breaks-description"
                          className={this.state.isValidBreaks ? "valid-field" : "invalid-field"}
                          aria-labelledby="breaks-describe breaks-description-error"
                          aria-required={true}
                          aria-invalid={!this.state.isValidBreaks}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={this.state.userAccommodationsProfile.breaks.description}
                          onChange={event =>
                            this.handleDescriptionChange(event.target.value, "breaks-description")
                          }
                          maxLength={DESCRIPTION_MAX_LENGTH}
                          placeholder={LOCALIZE.profile.accommodations.placeholder}
                        ></textarea>
                        {!this.state.isValidBreaks && (
                          <label
                            id="breaks-description-error"
                            htmlFor="breaks-description"
                            style={styles.textareaErrorMessage}
                          >
                            {LOCALIZE.profile.accommodations.breaks.breaksError}
                          </label>
                        )}
                      </Col>
                    </Row>
                  </>
                )}
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="adaptive-tech"
                  >
                    {LOCALIZE.profile.accommodations.adaptiveTech.needsAdaptiveTechQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={this.onChangeAdaptiveTech}
                          checked={this.state.userAccommodationsProfile.needs_adaptive_tech}
                          aria-labelledby="adaptive-tech"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.state.userAccommodationsProfile.needs_adaptive_tech && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="adaptive-tech-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.adaptiveTech.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="adaptive-tech-description"
                          className={
                            this.state.isValidAdaptiveTech ? "valid-field" : "invalid-field"
                          }
                          aria-labelledby="adaptive-tech-describe adaptive-tech-description-error"
                          aria-required={true}
                          aria-invalid={!this.state.isValidAdaptiveTech}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={this.state.userAccommodationsProfile.adaptive_tech.description}
                          onChange={event =>
                            this.handleDescriptionChange(
                              event.target.value,
                              "adaptive-tech-description"
                            )
                          }
                          maxLength={DESCRIPTION_MAX_LENGTH}
                          placeholder={LOCALIZE.profile.accommodations.placeholder}
                        ></textarea>
                        {!this.state.isValidAdaptiveTech && (
                          <label
                            id="adaptive-tech-description-error"
                            htmlFor="adaptive-tech-description"
                            style={styles.textareaErrorMessage}
                          >
                            {LOCALIZE.profile.accommodations.adaptiveTech.adaptiveTechError}
                          </label>
                        )}
                      </Col>
                    </Row>
                  </>
                )}
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="ergonomic"
                  >
                    {LOCALIZE.profile.accommodations.ergonomic.needsErgonomicQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={this.onChangeErgonomic}
                          checked={this.state.userAccommodationsProfile.needs_ergonomic}
                          aria-labelledby="ergonomic"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.state.userAccommodationsProfile.needs_ergonomic && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="ergonomic-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.ergonomic.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="ergonomic-description"
                          className={this.state.isValidErgonomic ? "valid-field" : "invalid-field"}
                          aria-labelledby="ergonomic-describe ergonomic-description-error"
                          aria-required={true}
                          aria-invalid={!this.state.isValidErgonomic}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={this.state.userAccommodationsProfile.ergonomic.description}
                          onChange={event =>
                            this.handleDescriptionChange(
                              event.target.value,
                              "ergonomic-description"
                            )
                          }
                          maxLength={DESCRIPTION_MAX_LENGTH}
                          placeholder={LOCALIZE.profile.accommodations.placeholder}
                        ></textarea>
                        {!this.state.isValidErgonomic && (
                          <label
                            id="ergonomic-description-error"
                            htmlFor="ergonomic-description"
                            style={styles.textareaErrorMessage}
                          >
                            {LOCALIZE.profile.accommodations.ergonomic.ergonomicError}
                          </label>
                        )}
                      </Col>
                    </Row>
                  </>
                )}
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="access-assistance"
                  >
                    {LOCALIZE.profile.accommodations.accessAssistance.needsAccessAssistanceQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={this.onChangeAccessAssistance}
                          checked={this.state.userAccommodationsProfile.needs_access_assistance}
                          aria-labelledby="access-assistance"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.state.userAccommodationsProfile.needs_access_assistance && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="access-assistance-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.accessAssistance.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="access-assistance-description"
                          className={
                            this.state.isValidAccessAssistance ? "valid-field" : "invalid-field"
                          }
                          aria-labelledby="access-assistance-describe access-assistance-description-error"
                          aria-required={true}
                          aria-invalid={!this.state.isValidAccessAssistance}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={this.state.userAccommodationsProfile.access_assistance.description}
                          onChange={event =>
                            this.handleDescriptionChange(
                              event.target.value,
                              "access-assistance-description"
                            )
                          }
                          maxLength={DESCRIPTION_MAX_LENGTH}
                          placeholder={LOCALIZE.profile.accommodations.placeholder}
                        ></textarea>
                        {!this.state.isValidAccessAssistance && (
                          <label
                            id="access-assistance-description-error"
                            htmlFor="access-assistance-description"
                            style={styles.textareaErrorMessage}
                          >
                            {LOCALIZE.profile.accommodations.accessAssistance.accessAssistanceError}
                          </label>
                        )}
                      </Col>
                    </Row>
                  </>
                )}
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="environment"
                  >
                    {LOCALIZE.profile.accommodations.environment.needsEnvironmentQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={this.onChangeEnvironment}
                          checked={this.state.userAccommodationsProfile.needs_environment}
                          aria-labelledby="environment"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.state.userAccommodationsProfile.needs_environment && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="environment-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.environment.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="environment-description"
                          className={
                            this.state.isValidEnvironment ? "valid-field" : "invalid-field"
                          }
                          aria-labelledby="environment-describe environment-description-error"
                          aria-required={true}
                          aria-invalid={!this.state.isValidEnvironment}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={this.state.userAccommodationsProfile.environment.description}
                          onChange={event =>
                            this.handleDescriptionChange(
                              event.target.value,
                              "environment-description"
                            )
                          }
                          maxLength={DESCRIPTION_MAX_LENGTH}
                          placeholder={LOCALIZE.profile.accommodations.placeholder}
                        ></textarea>
                        {!this.state.isValidEnvironment && (
                          <label
                            id="environment-description-error"
                            htmlFor="environment-description"
                            style={styles.textareaErrorMessage}
                          >
                            {LOCALIZE.profile.accommodations.ergonomic.ergonomicError}
                          </label>
                        )}
                      </Col>
                    </Row>
                  </>
                )}
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="scheduling-adaptation"
                  >
                    {
                      LOCALIZE.profile.accommodations.schedulingAdaptation
                        .needsSchedulingAdaptationQuestion
                    }
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={this.onChangeSchedulingAdaptation}
                          checked={this.state.userAccommodationsProfile.needs_scheduling_adaptation}
                          aria-labelledby="scheduling-adaptation"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.state.userAccommodationsProfile.needs_scheduling_adaptation && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="scheduling-adaptation-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.schedulingAdaptation.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="scheduling-adaptation-description"
                          className={
                            this.state.isValidSchedulingAdaptation ? "valid-field" : "invalid-field"
                          }
                          aria-labelledby="scheduling-adaptation-describe scheduling-adaptation-description-error"
                          aria-required={true}
                          aria-invalid={!this.state.isValidSchedulingAdaptation}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.state.userAccommodationsProfile.scheduling_adaptation.description
                          }
                          onChange={event =>
                            this.handleDescriptionChange(
                              event.target.value,
                              "scheduling-adaptation-description"
                            )
                          }
                          maxLength={DESCRIPTION_MAX_LENGTH}
                          placeholder={LOCALIZE.profile.accommodations.placeholder}
                        ></textarea>
                        {!this.state.isValidSchedulingAdaptation && (
                          <label
                            id="scheduling-adaptation-description-error"
                            htmlFor="scheduling-adaptation-description"
                            style={styles.textareaErrorMessage}
                          >
                            {
                              LOCALIZE.profile.accommodations.schedulingAdaptation
                                .schedulingAdaptationError
                            }
                          </label>
                        )}
                      </Col>
                    </Row>
                  </>
                )}
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="other-needs-written"
                  >
                    {LOCALIZE.profile.accommodations.otherNeedsWritten.hasOtherNeedsWrittenQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={this.onChangeOtherNeedsWritten}
                          checked={this.state.userAccommodationsProfile.has_other_needs_written}
                          aria-labelledby="other-needs-written"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.state.userAccommodationsProfile.has_other_needs_written && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="other-needs-written-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.otherNeedsWritten.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="other-needs-written-description"
                          className={
                            this.state.isValidOtherNeedsWritten ? "valid-field" : "invalid-field"
                          }
                          aria-labelledby="other-needs-written-describe other-needs-written-description-error"
                          aria-required={true}
                          aria-invalid={!this.state.isValidOtherNeedsWritten}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.state.userAccommodationsProfile.other_needs_written.description
                          }
                          onChange={event =>
                            this.handleDescriptionChange(
                              event.target.value,
                              "other-needs-written-description"
                            )
                          }
                          maxLength={DESCRIPTION_MAX_LENGTH}
                          placeholder={LOCALIZE.profile.accommodations.placeholder}
                        ></textarea>
                        {!this.state.isValidOtherNeedsWritten && (
                          <label
                            id="other-needs-written-description-error"
                            htmlFor="other-needs-written-description"
                            style={styles.textareaErrorMessage}
                          >
                            {
                              LOCALIZE.profile.accommodations.otherNeedsWritten
                                .otherNeedsWrittenError
                            }
                          </label>
                        )}
                      </Col>
                    </Row>
                  </>
                )}
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="other-needs-oral"
                  >
                    {LOCALIZE.profile.accommodations.otherNeedsOral.hasOtherNeedsOralQuestion}
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={this.onChangeOtherNeedsOral}
                          checked={this.state.userAccommodationsProfile.has_other_needs_oral}
                          aria-labelledby="other-needs-oral"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.state.userAccommodationsProfile.has_other_needs_oral && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="other-needs-oral-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.otherNeedsOral.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="other-needs-oral-description"
                          className={
                            this.state.isValidOtherNeedsOral ? "valid-field" : "invalid-field"
                          }
                          aria-labelledby="other-needs-oral-describe other-needs-oral-description-error"
                          aria-required={true}
                          aria-invalid={!this.state.isValidOtherNeedsOral}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={this.state.userAccommodationsProfile.other_needs_oral.description}
                          onChange={event =>
                            this.handleDescriptionChange(
                              event.target.value,
                              "other-needs-oral-description"
                            )
                          }
                          maxLength={DESCRIPTION_MAX_LENGTH}
                          placeholder={LOCALIZE.profile.accommodations.placeholder}
                        ></textarea>
                        {!this.state.isValidOtherNeedsOral && (
                          <label
                            id="other-needs-oral-description-error"
                            htmlFor="other-needs-oral-description"
                            style={styles.textareaErrorMessage}
                          >
                            {LOCALIZE.profile.accommodations.otherNeedsOral.otherNeedsOralError}
                          </label>
                        )}
                      </Col>
                    </Row>
                  </>
                )}
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="repeated-oral-questions"
                  >
                    {
                      LOCALIZE.profile.accommodations.repeatedOralQuestions
                        .needsRepeatedOralQuestionsQuestion
                    }
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={this.onChangeRepeatedOralQuestions}
                          checked={
                            this.state.userAccommodationsProfile.needs_repeated_oral_questions
                          }
                          aria-labelledby="repeated-oral-questions"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.state.userAccommodationsProfile.needs_repeated_oral_questions && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="repeated-oral-questions-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.repeatedOralQuestions.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="repeated-oral-questions-description"
                          className={
                            this.state.isValidRepeatedOralQuestions
                              ? "valid-field"
                              : "invalid-field"
                          }
                          aria-labelledby="repeated-oral-questions-describe repeated-oral-questions-description-error"
                          aria-required={true}
                          aria-invalid={!this.state.isValidRepeatedOralQuestions}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.state.userAccommodationsProfile.repeated_oral_questions.description
                          }
                          onChange={event =>
                            this.handleDescriptionChange(
                              event.target.value,
                              "repeated-oral-questions-description"
                            )
                          }
                          maxLength={DESCRIPTION_MAX_LENGTH}
                          placeholder={LOCALIZE.profile.accommodations.placeholder}
                        ></textarea>
                        {!this.state.isValidRepeatedOralQuestions && (
                          <label
                            id="repeated-oral-questions-description-error"
                            htmlFor="repeated-oral-questions-description"
                            style={styles.textareaErrorMessage}
                          >
                            {
                              LOCALIZE.profile.accommodations.repeatedOralQuestions
                                .repeatedOralQuestionsError
                            }
                          </label>
                        )}
                      </Col>
                    </Row>
                  </>
                )}
                <Row className="align-items-center" style={styles.questionRow}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    id="previously-accommodated"
                  >
                    {
                      LOCALIZE.profile.accommodations.previouslyAccommodated
                        .previouslyAccommodatedQuestion
                    }
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoading && (
                      <div style={styles.switch}>
                        <Switch
                          onChange={this.onChangePreviouslyAccommodated}
                          checked={this.state.userAccommodationsProfile.previously_accommodated}
                          aria-labelledby="previously-accommodated"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                        />
                      </div>
                    )}
                  </Col>
                </Row>
                {this.state.userAccommodationsProfile.previously_accommodated && (
                  <>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        id="previously-accommodated-describe"
                        style={styles.pleaseDescribeMargin}
                      >
                        {LOCALIZE.profile.accommodations.previouslyAccommodated.pleaseDescribe}
                      </Col>
                    </Row>
                    <Row className="w100">
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <textarea
                          id="previously-accommodated-description"
                          className={
                            this.state.isValidPreviouslyAccommodated
                              ? "valid-field"
                              : "invalid-field"
                          }
                          aria-labelledby="previously-accommodated-describe previously-accommodated-description-error"
                          aria-required={true}
                          aria-invalid={!this.state.isValidPreviouslyAccommodated}
                          style={{
                            ...styles.input,
                            ...styles.textareaInput,
                            ...accommodationsStyle
                          }}
                          value={
                            this.state.userAccommodationsProfile.previous_accommodation.description
                          }
                          onChange={event =>
                            this.handleDescriptionChange(
                              event.target.value,
                              "previously-accommodated-description"
                            )
                          }
                          maxLength={DESCRIPTION_MAX_LENGTH}
                          placeholder={LOCALIZE.profile.accommodations.placeholder}
                        ></textarea>
                        {!this.state.isValidPreviouslyAccommodated && (
                          <label
                            id="previously-accommodated-description-error"
                            htmlFor="previously-accommodated-description"
                            style={styles.textareaErrorMessage}
                          >
                            {
                              LOCALIZE.profile.accommodations.previouslyAccommodated
                                .previouslyAccommodatedError
                            }
                          </label>
                        )}
                      </Col>
                    </Row>
                  </>
                )}
                <Row className="align-items-center" style={styles.questionRow}>
                  <div className="d-flex" style={styles.checkboxContainer}>
                    <div>
                      <input
                        aria-labelledby={"privacy-notice-description"}
                        id="privacy-notice-checkbox"
                        type="checkbox"
                        style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
                        onChange={this.changePrivacyCheckboxStatus}
                      />
                    </div>
                    <div style={styles.checkboxLabel}>
                      <label id="privacy-notice-description" htmlFor="privacy-notice-checkbox">
                        {LOCALIZE.formatString(
                          LOCALIZE.profile.accommodations.privacyNotice,
                          <button
                            aria-label={LOCALIZE.profile.accommodations.privacyNoticeLink}
                            tabIndex="0"
                            onClick={this.openPrivacyNoticePopup}
                            style={styles.privacyNoticeLink}
                          >
                            {LOCALIZE.profile.accommodations.privacyNoticeLink}
                          </button>
                        )}
                      </label>
                    </div>
                  </div>
                </Row>
                <Row className="align-items-center" style={styles.questionRow}>
                  <div className="d-flex" style={styles.checkboxContainer}>
                    <div>
                      <input
                        aria-labelledby={"understand-notice-description"}
                        id="understand-notice-checkbox"
                        type="checkbox"
                        style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
                        onChange={this.changeUnderstandCheckboxStatus}
                      />
                    </div>
                    <div style={styles.checkboxLabel}>
                      <label
                        id="understand-notice-description"
                        htmlFor="understand-notice-checkbox"
                      >
                        {LOCALIZE.profile.accommodations.understandAccommodationsMesures}
                      </label>
                    </div>
                  </div>
                </Row>
              </div>
              <div style={styles.updateButtonContainer}>
                <Row>
                  <Col>
                    <CustomButton
                      buttonId="my-password-save-button-id"
                      label={
                        <>
                          <span>
                            <FontAwesomeIcon icon={faSave} />
                          </span>
                          <span style={styles.saveButtonLabel}>{LOCALIZE.commons.saveButton}</span>
                        </>
                      }
                      action={this.handleSaveUserAccommodationsProfile}
                      customStyle={styles.updateButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.profile.saveButton}
                      disabled={
                        !this.state.changeDetected ||
                        !this.state.isValidAccessAssistance ||
                        !this.state.isValidAdaptiveTech ||
                        !this.state.isValidExtraTime ||
                        !this.state.isValidBreaks ||
                        !this.state.isValidEnvironment ||
                        !this.state.isValidErgonomic ||
                        !this.state.isValidOtherNeedsWritten ||
                        !this.state.isValidOtherNeedsOral ||
                        !this.state.isValidSchedulingAdaptation ||
                        !this.state.isValidRepeatedOralQuestions ||
                        !this.state.isValidPreviouslyAccommodated ||
                        !this.state.isPrivacyNoticeChecked ||
                        !this.state.isUnderstandNoticeChecked
                      }
                    />
                  </Col>
                </Row>
              </div>
            </>
          )}
        </Container>
        <PopupBox
          show={this.state.saveSuccessPopup}
          title={LOCALIZE.profile.accommodations.successPopup.title}
          handleClose={() => {}}
          size={"lg"}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.success}
                    title={LOCALIZE.commons.success}
                    message={LOCALIZE.profile.accommodations.successPopup.description}
                  />
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.handleCloseSaveSuccessPopup}
        />
        <PrivacyNoticeStatement
          showPopup={this.state.showPrivacyNoticePopup}
          handleClose={this.closePrivacyNoticePopup}
        />
        <PopupBox
          show={this.state.showErrorPopup}
          handleClose={this.handleErrorPopupClose}
          title={LOCALIZE.profile.accommodations.errorPopup.title}
          description={
            <div>
              <div>
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.error}
                  message={<p>{LOCALIZE.profile.accommodations.errorPopup.description}</p>}
                />
              </div>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.handleErrorPopupClose}
          size={"lg"}
        />
      </>
    );
  }
}

export { Accommodations as unconnectedAccommodations };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getUserAccommodationsProfile, saveUserAccommodationsProfile }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Accommodations);
