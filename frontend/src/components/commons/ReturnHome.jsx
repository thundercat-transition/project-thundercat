import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHouse } from "@fortawesome/free-solid-svg-icons";
import LOCALIZE from "../../text_resources";
import CustomButton from "./CustomButton";
import THEME from "./CustomButtonTheme";

const styles = {
  button: {
    minWidth: 40
  },
  iconLabel: {
    marginRight: 6
  },
  paddingNavButton: {
    padding: "0 5px 0 5px"
  }
};

class ReturnHome extends Component {
  render() {
    return (
      <div style={styles.paddingNavButton}>
        <CustomButton
          label={
            <div>
              <span style={styles.iconLabel} className="notranslate">
                {LOCALIZE.commons.returnHome}
              </span>
              <FontAwesomeIcon icon={faHouse} />
            </div>
          }
          action={this.props.handleReturnHome}
          customStyle={styles.button}
          buttonTheme={THEME.PRIMARY_IN_TEST}
          ariaLabel={LOCALIZE.commons.returnHome}
        />
      </div>
    );
  }
}

export { ReturnHome as UnconnectedReturnHome };
const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ReturnHome);
