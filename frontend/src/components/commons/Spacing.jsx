import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { Col, Row } from "react-bootstrap";
import Switch from "react-switch";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
  setSpacing,
  saveUserAccommodations,
  triggerRerender
} from "../../modules/AccommodationsRedux";
import "../../css/font-select.css";
import getSwitchTransformScale from "../../helpers/switchTransformScale";

class Spacing extends Component {
  state = {
    isLoading: false,
    // default values
    switchHeight: 25,
    switchWidth: 50
  };

  componentDidMount = () => {
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = prevProps => {
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoading: false });
        }
      );
    });
  };

  onChange = event => {
    this.props.setSpacing(event);
    if (this.props.authenticated) {
      this.props
        .saveUserAccommodations({ ...this.props.accommodations, spacing: event })
        .then(() => {
          // trigger rerender so dynamic height in test is updated
          this.props.triggerRerender();
        });
    }
  };

  render() {
    // defining styles here because it wouldn't allow modifcation of values when defined outside.
    const styles = {
      container: {
        padding: "12px 25px",
        width: "100%"
      }
    };

    return (
      <div style={styles.container}>
        <Row>
          <Col xl={6} lg={6} md={6} sm={6} xs={6} id="text-spacing">
            {LOCALIZE.settings.lineSpacing.label}
          </Col>
          <Col xl={6} lg={6} md={6} sm={6} xs={6}>
            {!this.state.isLoading && (
              <Switch
                onChange={this.onChange}
                checked={this.props.accommodations.spacing}
                aria-labelledby="text-spacing"
                height={this.state.switchHeight}
                width={this.state.switchWidth}
              />
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    fontFamily: state.accommodations.fontFamily,
    authenticated: state.login.authenticated,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setSpacing, saveUserAccommodations, triggerRerender }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Spacing);
