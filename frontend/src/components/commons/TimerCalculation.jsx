/* eslint-disable jsx-a11y/label-has-associated-control */
// This component simply displays a span containing time in hours, minutes, and seconds
// This is based off of properties passed in from the parent

import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

const styles = {
  container: {
    display: "table",
    width: "100%"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  loadingContainer: {
    display: "grid",
    gridTemplateColumns: "1fr"
  },
  loadingOverlappingStyle: {
    gridRowStart: "1",
    gridColumnStart: "1"
  },
  transparentText: {
    color: "transparent"
  },
  loadingSpinnerStyle: {
    margin: "-2px"
  }
};
// This is a seperate function so that it can be tested
export const calculateRemainingTime = (
  startTime,
  timeLimit,
  now,
  timeoutFunction,
  countUp = false
) => {
  // If there is no start time, then there is nothing to calculate
  if (startTime === null) {
    return 0;
  }
  // this should never happen, but placeholder
  if (now === null) {
    return 0;
  }
  // If there is no timelimit or it's a sample test and the count_up flag is set to true, then we count up
  if (countUp || timeLimit === null || typeof timeLimit === "undefined") {
    return now.diff(startTime, "milliseconds");
  }
  // Otherwise we count down
  // Convert time limit from minutes to milliseconds
  const limitInMilisecs = 60000 * timeLimit;
  const timeRemaining = limitInMilisecs - now.diff(startTime, "milliseconds");

  if (timeRemaining <= 0) {
    timeoutFunction();
    // return null to flag that it has ended
    return null;
  }
  return timeRemaining;
};

class TimerCalculation extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      currentTime: PropTypes.string,
      startTime: PropTypes.string,
      timeLimit: PropTypes.number,
      timeoutFunction: PropTypes.func.isRequired,
      customStyle: PropTypes.object,
      countUp: PropTypes.bool,
      enableLoadingLogic: PropTypes.bool
    };

    TimerCalculation.defaultProps = {
      enableLoadingLogic: false
    };
  }

  state = {
    millisecondTime: 0,
    isLoading: this.props.enableLoadingLogic,
    currentTimeState: this.props.currentTime
  };

  componentDidMount() {
    // enableLoadingLogic is set to true
    if (this.props.enableLoadingLogic) {
      // adding 1 second delay, so the startTime props is fully calculated when getting the remaining time
      setTimeout(() => {
        this.updateRemainingTime();
        this.timer = setInterval(this.updateRemainingTime, 1000);
      }, 1000);
      // enableLoadingLogic is set to false
    } else {
      this.updateRemainingTime();
      this.timer = setInterval(this.updateRemainingTime, 1000);
    }
  }

  componentDidUpdate = prevProps => {
    if (prevProps.currentTime !== this.props.currentTime) {
      this.setState({ currentTimeState: this.props.currentTime });
    }
  };

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  getUpdatedCurrentTime = currentTimeState => {
    // adding one second to current time state
    const newTime = currentTimeState.add(1, "seconds");
    return newTime;
  };

  updateRemainingTime = () => {
    const { startTime, timeLimit } = this.props;
    // If no start time, then we have nothing to calculate from
    if (startTime === null) {
      this.setState({ millisecondTime: 0 });
      clearInterval(this.timer);
      return;
    }
    this.setState(
      { currentTimeState: this.getUpdatedCurrentTime(this.state.currentTimeState) },
      () => {
        const newTime = calculateRemainingTime(
          startTime,
          timeLimit,
          this.state.currentTimeState,
          this.props.timeoutFunction,
          this.props.countUp
        );
        if (newTime == null) {
          clearInterval(this.timer);
          return; // this return is important, as it prevents an update on unmounted component warning
        }
        this.setState({
          millisecondTime: newTime,
          isLoading: false
        });
      }
    );
  };

  render() {
    const { millisecondTime } = this.state;
    // calcuate seconds, minutes, and hours
    let seconds = Math.floor((millisecondTime / 1000) % 60);
    let minutes = Math.floor((millisecondTime / (1000 * 60)) % 60);
    let hours = Math.floor(millisecondTime / (1000 * 60 * 60));
    // ensure that the numbers are nicely formatted
    hours = hours < 10 ? `0${hours}` : hours;
    minutes = minutes < 10 ? `0${minutes}` : minutes;
    seconds = seconds < 10 ? `0${seconds}` : seconds;
    return (
      <div style={styles.container}>
        {this.state.isLoading && this.props.enableLoadingLogic ? (
          <div style={styles.loadingContainer}>
            {/* this div is useful to get the right size of the timer box while loading */}
            <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
              00:00:00
            </div>
            <div style={{ ...styles.loadingOverlappingStyle, ...styles.loadingSpinnerStyle }}>
              <label className="fa fa-spinner fa-spin">
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            </div>
          </div>
        ) : (
          <>
            <span
              id="time-remaining-accessibility"
              style={styles.hiddenText}
              tabIndex={0}
              role="timer"
            >
              {LOCALIZE.formatString(
                LOCALIZE.emibTest.testFooter.timer.timeRemaining,
                hours,
                minutes,
                seconds
              )}
            </span>
            <span style={this.props.customStyle ? this.props.customStyle : {}}>
              {hours}:{minutes}:{seconds}
            </span>
          </>
        )}
      </div>
    );
  }
}

export default TimerCalculation;
