import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTimesCircle,
  faExclamationCircle,
  faCheckCircle,
  faInfoCircle
} from "@fortawesome/free-solid-svg-icons";

export const MESSAGE_TYPE = {
  warning: "warning",
  error: "error",
  success: "success",
  info: "info"
};

export const CLASS_NAME = {
  alert: "",
  icon: ""
};

export const styles = {
  boxPadding: {
    paddingTop: 0,
    paddingBottom: 0
  },
  logoZone: {
    minWidth: 80
  },
  textBox: {
    textAlign: "left",
    paddingLeft: 24,
    paddingRight: 24,
    width: "90%"
  },
  h5: {
    fontWeight: "bold",
    paddingTop: 12,
    margin: "5px 5px 0px 0px"
  },
  description: {
    padding: "12px 0"
  },
  alertContainer: {
    width: "100%",
    marginBottom: 0,
    borderLeft: "unset"
  }
};

class SystemMessage extends Component {
  static propTypes = {
    messageType: PropTypes.string.isRequired,
    title: PropTypes.string,
    message: PropTypes.object.isRequired
  };

  render() {
    const { title, message, messageType } = this.props;
    if (messageType === MESSAGE_TYPE.error) {
      CLASS_NAME.alert = "alert-icon alert-danger";
      CLASS_NAME.icon = faTimesCircle;
    } else if (messageType === MESSAGE_TYPE.warning) {
      CLASS_NAME.alert = "alert-icon alert-warning";
      CLASS_NAME.icon = faExclamationCircle;
    } else if (messageType === MESSAGE_TYPE.success) {
      CLASS_NAME.alert = "alert-icon alert-success";
      CLASS_NAME.icon = faCheckCircle;
    } else {
      CLASS_NAME.alert = "alert-icon alert-info";
      CLASS_NAME.icon = faInfoCircle;
    }

    const customFontSize = {
      // adding 6px to current selected font size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 6}px`
    };

    const customIconFontSize = {
      // adding 24px to current selected font size of the icon
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 24}px`
    };

    return (
      <div className="row" style={styles.boxPadding}>
        <div className={CLASS_NAME.alert} role="alert" style={styles.alertContainer}>
          <div className="icon" aria-hidden="true" style={styles.logoZone}>
            <FontAwesomeIcon icon={CLASS_NAME.icon} style={customIconFontSize} />
          </div>
          <div style={styles.textBox}>
            {title && <h5 style={{ ...styles.h5, ...customFontSize }}>{title}</h5>}
            <div style={styles.description}>{message}</div>
          </div>
        </div>
      </div>
    );
  }
}

export { SystemMessage as UnconnectedSystemMessage };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations
  };
};

export default connect(mapStateToProps, null)(SystemMessage);
