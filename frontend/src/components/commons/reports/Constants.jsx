import LOCALIZE from "../../../text_resources";

export const reportTypesDefinition = () => {
  return [
    { value: 1, label: LOCALIZE.reports.reportTypes.individualScoreSheet },
    { value: 2, label: LOCALIZE.reports.reportTypes.resultsReport },
    { value: 3, label: LOCALIZE.reports.reportTypes.financialReport },
    { value: 4, label: LOCALIZE.reports.reportTypes.testContentReport },
    { value: 5, label: LOCALIZE.reports.reportTypes.testTakerReport },
    { value: 6, label: LOCALIZE.reports.reportTypes.candidateActionsReport },
    { value: 7, label: LOCALIZE.reports.reportTypes.adaptedTestsReport },
    { value: 8, label: LOCALIZE.reports.reportTypes.taHistoryReport }
  ];
};

export const REPORT_TYPES = {
  individualScoreSheet: reportTypesDefinition()[0],
  resultsReport: reportTypesDefinition()[1],
  financialReport: reportTypesDefinition()[2],
  testContentReport: reportTypesDefinition()[3],
  testTakerReport: reportTypesDefinition()[4],
  candidateActionsReport: reportTypesDefinition()[5],
  adaptedTestsReport: reportTypesDefinition()[6],
  taHistoryReport: reportTypesDefinition()[7]
};

export const REPORT_REQUESTOR = {
  etta: "etta",
  ppc: "ppc",
  ta: "ta"
};
