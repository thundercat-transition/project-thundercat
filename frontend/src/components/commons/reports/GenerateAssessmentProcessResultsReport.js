import LOCALIZE from "../../../text_resources";
import getFormattedStatus from "./utils";
import getFormattedDate from "../../../helpers/getFormattedDate";

function generateAssessmentProcessResultsReport(providedReportDataArray) {
  // initializing reportData
  const reportData = [];

  // pushing columns definition
  reportData.push([
    LOCALIZE.reports.assessmentProcessResultsReport.firstName,
    LOCALIZE.reports.assessmentProcessResultsReport.lastName,
    LOCALIZE.reports.assessmentProcessResultsReport.email,
    LOCALIZE.reports.assessmentProcessResultsReport.invitationEmail,
    LOCALIZE.reports.assessmentProcessResultsReport.pri,
    LOCALIZE.reports.assessmentProcessResultsReport.militaryNbr,
    LOCALIZE.reports.assessmentProcessResultsReport.assessmentProcess,
    LOCALIZE.reports.assessmentProcessResultsReport.testDefinition,
    LOCALIZE.reports.assessmentProcessResultsReport.testDefinitionEnName,
    LOCALIZE.reports.assessmentProcessResultsReport.testDefinitionFrName,
    LOCALIZE.reports.assessmentProcessResultsReport.testDate,
    LOCALIZE.reports.assessmentProcessResultsReport.score,
    LOCALIZE.reports.assessmentProcessResultsReport.level,
    LOCALIZE.reports.assessmentProcessResultsReport.testStatus
  ]);

  // looping in provided providedReportDataArray and formatting data
  for (let i = 0; i < providedReportDataArray.length; i++) {
    reportData.push([
      providedReportDataArray[i].user_first_name,
      providedReportDataArray[i].user_last_name,
      providedReportDataArray[i].user_email,
      providedReportDataArray[i].assessment_process_candidate_email,
      providedReportDataArray[i].user_pri,
      providedReportDataArray[i].user_military_nbr,
      providedReportDataArray[i].assessment_process_reference_number,
      providedReportDataArray[i].test_definition_test_code,
      `${providedReportDataArray[i].test_definition_en_name}"`,
      `${providedReportDataArray[i].test_definition_fr_name}"`,
      providedReportDataArray[i].submit_date !== null
        ? getFormattedDate(providedReportDataArray[i].submit_date)
        : LOCALIZE.commons.na,
      providedReportDataArray[i].total_score,
      providedReportDataArray[i][`${LOCALIZE.getLanguage()}_converted_score`],
      getFormattedStatus(providedReportDataArray[i].assigned_test_status_codename)
    ]);
  }

  return reportData;
}

export default generateAssessmentProcessResultsReport;
