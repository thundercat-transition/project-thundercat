/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import withRouter from "../../withRouter";
import LOCALIZE from "../../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  setReportSelectedParameters,
  setGenerateReportDisabledState
} from "../../../modules/ReportsRedux";
import { Row, Col, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import StyledTooltip, { EFFECT, TYPE } from "../../authentication/StyledTooltip";
import * as _ from "lodash";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};
const styles = {
  optionSelectionContainer: {
    padding: "12px 24px",
    margin: "0 auto",
    width: "80%"
  },
  labelContainer: {
    verticalAlign: "middle",
    paddingRight: 12
  },
  tooltipIcon: {
    color: "#00565e"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: "15px 0px 0px 0px",
    marginTop: 6,
    width: "80%"
  },
  noMargin: {
    margin: 0
  },
  inputContainer: {
    verticalAlign: "middle",
    width: "100%"
  },
  input: {
    padding: 4,
    marginLeft: 15,
    width: "80%"
  },
  tooltipIconContainer: {
    padding: "0 6px"
  },
  errorContainer: {
    paddingLeft: "15px"
  }
};

class TaHistoryReport extends Component {
  state = {
    isValidPri: true,
    isValidMilitaryNbr: true,
    priContent: this.props.pri === undefined ? "" : _.cloneDeep(this.props.pri),
    militaryNbrContent:
      this.props.militaryNbr === undefined ? "" : _.cloneDeep(this.props.militaryNbr)
  };

  componentDidUpdate = prevProps => {
    // update pri field based on the prop value
    if (prevProps.pri !== this.props.pri) {
      this.setState({ priContent: _.cloneDeep(this.props.pri) });
      // Validate pri
      this.validatePriOrMilitaryNumber();
    }
    // update military number field based on the prop value
    if (prevProps.militaryNbr !== this.props.militaryNbr) {
      this.setState({ militaryNbrContent: _.cloneDeep(this.props.militaryNbr) });
      // Validate military num
      this.validatePriOrMilitaryNumber();
    }
  };

  // validate personal info data that is editable only (needs to be done before sending data in the database)
  validatePriOrMilitaryNumber = () => {
    // validating fields
    let isValidPri = true;
    let isValidMilitaryNbr = true;

    // You have to have at least 1 with text
    if (this.state.priContent === "" && this.state.militaryNbrContent === "") {
      isValidPri = false;
      isValidMilitaryNbr = false;
    }
    // updating fields state
    this.setState({
      isValidPri,
      isValidMilitaryNbr
    });

    // check if both PRI and Military Number are valid
    if (isValidPri && isValidMilitaryNbr) {
      // updating all needed redux states
      this.props.setReportSelectedParameters(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        this.state.priContent,
        this.state.militaryNbrContent
      );
      // enabling generate report button
      this.props.setGenerateReportDisabledState(false);
      // whole data is valid
      return true;
    }

    // check if PRI is valid
    if (isValidPri) {
      // updating all needed redux states
      this.props.setReportSelectedParameters(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        this.state.priContent,
        null
      );
      // enabling generate report button
      this.props.setGenerateReportDisabledState(false);
      // whole data is valid
      return true;

      // there is at least one invalid field
    }

    // check if Military Number is valid
    if (isValidMilitaryNbr) {
      this.setState({ isValidPri: true });

      // updating all needed redux states
      this.props.setReportSelectedParameters(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        this.state.militaryNbrContent
      );
      // enabling generate report button
      this.props.setGenerateReportDisabledState(false);
      // whole data is valid
      return true;

      // there is at least one invalid field
    }

    // enabling generate report button
    this.props.setGenerateReportDisabledState(true);
    return false;
  };

  // update military number value
  updateMilitaryNbrValue = event => {
    const militaryNbrContent = event.target.value;
    /* only the following can be inserted into this field:
              - 1 letter followed by 0 to 8 numbers
        */
    const regex = /^(([A-Za-z]{1})([0-9]{0,8}))$/;
    if (event.target.value === "" || regex.test(event.target.value)) {
      this.setState(
        {
          militaryNbrContent
        },
        () => {
          // Validate military num
          this.validatePriOrMilitaryNumber();
        }
      );
    }
  };

  // update pri value
  updatePriValue = event => {
    const priContent = event.target.value;
    /* only the following can be inserted into this field:
            - 0 to 9 numbers
    */
    const regex = /^([0-9]{0,9})$/;

    if (event.target.value === "" || regex.test(event.target.value)) {
      this.setState(
        {
          priContent
        },
        () => {
          // Validate pri
          this.validatePriOrMilitaryNumber();
        }
      );
    }
  };

  render() {
    const { isValidPri, isValidMilitaryNbr, priContent, militaryNbrContent } = this.state;
    return (
      <div>
        <Row
          id="personal-info-pri"
          className="align-items-center"
          style={styles.optionSelectionContainer}
          role="presentation"
        >
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            id="pri-title"
            style={styles.labelContainer}
          >
            <label id="pri" htmlFor="pri-input" style={styles.labelSpacing}>
              {LOCALIZE.profile.personalInfo.pri.title}
            </label>
            <StyledTooltip
              id="pri-tooltip"
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <Button data-tip="" data-for="pri-tooltip" tabIndex="-1" variant="link">
                  <FontAwesomeIcon icon={faQuestionCircle} style={styles.tooltipIcon} />
                </Button>
              }
              tooltipContent={
                <div>
                  <p>{LOCALIZE.reports.taHistoryReport.priTooltip}</p>
                </div>
              }
            />
          </Col>

          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
            style={styles.inputContainer}
          >
            <Row>
              <input
                id="pri-input"
                className={isValidPri ? "valid-field" : "invalid-field"}
                aria-labelledby="pri pri-tooltip-for-accessibility invalid-pri-msg"
                aria-required={false}
                aria-invalid={!isValidPri}
                type="text"
                value={priContent}
                onChange={this.updatePriValue}
                style={styles.input}
              />
            </Row>
          </Col>
        </Row>
        <Row
          id="personal-info-military-nbr"
          style={styles.optionSelectionContainer}
          role="presentation"
        >
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            id="military-nbr-title"
            style={styles.labelContainer}
          >
            <label id="military-number" htmlFor="military-number-input" style={styles.labelSpacing}>
              {LOCALIZE.profile.personalInfo.militaryNbr.title}
            </label>
            <StyledTooltip
              id="military-number-tooltip"
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <Button
                  data-tip=""
                  data-for="military-number-tooltip"
                  tabIndex="-1"
                  variant="link"
                  style={{
                    ...styles.tooltipIconContainer,
                    ...styles.tooltipMarginTop
                  }}
                >
                  <FontAwesomeIcon icon={faQuestionCircle} style={styles.tooltipIcon} />
                </Button>
              }
              tooltipContent={
                <div>
                  <p>{LOCALIZE.reports.taHistoryReport.militaryNbrTooltip}</p>
                </div>
              }
            />
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
            style={styles.inputContainer}
          >
            <Row>
              <input
                id="military-number-input"
                className={isValidMilitaryNbr ? "valid-field" : "invalid-field"}
                aria-labelledby="military-number military-number-tooltip-for-accessibility invalid-military-number-msg"
                aria-required={false}
                aria-invalid={!isValidMilitaryNbr}
                type="text"
                value={militaryNbrContent}
                onChange={this.updateMilitaryNbrValue}
                style={styles.input}
              />
            </Row>
            <Row style={styles.errorContainer}>
              {!isValidMilitaryNbr && (
                <label
                  id="invalid-military-number-msg"
                  style={{ ...styles.errorMessage, ...styles.noMargin }}
                >
                  {LOCALIZE.profile.personalInfo.priMilitaryNbrError}
                </label>
              )}
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

export { TaHistoryReport as unconnectedCandidateActionsReport };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    username: state.user.username
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setReportSelectedParameters,
      setGenerateReportDisabledState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TaHistoryReport));
