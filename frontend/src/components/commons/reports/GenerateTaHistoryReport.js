import LOCALIZE from "../../../text_resources";
import getFormattedStatus from "./utils";

function generateTaHistoryReport(providedReportDataArray) {
  // initializing reportData
  const reportData = [];

  // pushing columns definition
  reportData.push([
    LOCALIZE.reports.taHistoryReport.assignedTest,
    LOCALIZE.reports.taHistoryReport.is_invalid,
    LOCALIZE.reports.taHistoryReport.ta,
    LOCALIZE.reports.taHistoryReport.pri,
    LOCALIZE.reports.taHistoryReport.military_number,
    LOCALIZE.reports.taHistoryReport.ta_email,
    LOCALIZE.reports.taHistoryReport.goc_email,
    LOCALIZE.reports.taHistoryReport.requesting_organization,
    LOCALIZE.reports.taHistoryReport.test,
    LOCALIZE.reports.taHistoryReport.test_submit_date,
    LOCALIZE.reports.taHistoryReport.candidate_name,
    LOCALIZE.reports.taHistoryReport.candidate_username
  ]);

  // looping in provided providedReportDataArray and formatting data
  for (let i = 0; i < providedReportDataArray.length; i++) {
    // splitting english and french department name
    let org = providedReportDataArray[i].requesting_organization.split("/");
    const enOrg = org[0];
    const frOrg = org[1];

    // selecting department name based off interface language
    if (LOCALIZE.getLanguage() === "en") org = enOrg;
    else org = frOrg;

    // creating report data (escaping all special characters)
    reportData.push([
      `"${providedReportDataArray[i].assigned_test_id}"`,
      `"${providedReportDataArray[i].is_invalid}"`,
      `"${`${providedReportDataArray[i].ta_first_name}, ${providedReportDataArray[i].ta_last_name}`}"`,
      `"${providedReportDataArray[i].pri}"`,
      `"${providedReportDataArray[i].military_number}"`,
      `"${providedReportDataArray[i].ta_email}"`,
      `"${providedReportDataArray[i].goc_email}"`,
      `"${org}"`,
      `"${providedReportDataArray[i].test_code}"`,
      `"${providedReportDataArray[i].test_submit_date}"`,
      `"${providedReportDataArray[i].candidate_name}"`,
      `"${providedReportDataArray[i].candidate_username}"`
    ]);
  }

  return reportData;
}

export default generateTaHistoryReport;
