import LOCALIZE from "../../../text_resources";

function getFormattedSampleStatusFlag(sampleTestIndicator) {
  switch (sampleTestIndicator) {
    case true:
      return LOCALIZE.commons.true;
    case false:
      return LOCALIZE.commons.false;
    default:
      return LOCALIZE.commons.na;
  }
}

function generateTestContentReport(providedReportDataArray) {
  // initializing reportData
  const reportData = [];

  // pushing columns definition
  reportData.push([
    LOCALIZE.reports.testContentReport.questionSectionNumber,
    LOCALIZE.reports.testContentReport.questionSectionTitleFR,
    LOCALIZE.reports.testContentReport.questionSectionTitleEN,
    LOCALIZE.reports.testContentReport.rdItemID,
    LOCALIZE.reports.testContentReport.questionNumber,
    LOCALIZE.reports.testContentReport.questionTextFR,
    LOCALIZE.reports.testContentReport.questionTextEN,
    LOCALIZE.reports.testContentReport.questionTextFrHTML,
    LOCALIZE.reports.testContentReport.questionTextEnHTML
  ]);

  // initializing variables
  let questionSection = 1;
  const initialTestSectionComponentId = parseInt(providedReportDataArray[0].test_section_component);
  let questionId = 0;
  let questionNumber = 1;
  let questionTextFr = "";
  let questionTextEn = "";
  let questionTextHtmlFr = "";
  let questionTextHtmlEn = "";
  let lineNumber = 1;
  let maxNumberOfAnswers = 0;

  // Create the other columns based on the number of answer choices
  for (let i = 0; i < providedReportDataArray.length; i++) {
    for (let y = 0; y < providedReportDataArray[i].answers.length; y++) {
      // if we need new columns since we surpassed the max number of answers
      // OR
      // if it is the first time we go through the loop
      if (y > maxNumberOfAnswers || i === 0) {
        maxNumberOfAnswers = y;
        reportData[0].push(
          LOCALIZE.formatString(LOCALIZE.reports.testContentReport.answerChoiceID, y + 1),
          LOCALIZE.formatString(LOCALIZE.reports.testContentReport.answerChoiceTextFR, y + 1),
          LOCALIZE.formatString(LOCALIZE.reports.testContentReport.answerChoiceTextEN, y + 1),
          LOCALIZE.formatString(LOCALIZE.reports.testContentReport.answerChoiceTextFrHTML, y + 1),
          LOCALIZE.formatString(LOCALIZE.reports.testContentReport.answerChoiceTextEnHTML, y + 1),
          LOCALIZE.formatString(LOCALIZE.reports.testContentReport.scoringValue, y + 1)
        );
      }
    }
  }
  // add the last column
  reportData[0].push([LOCALIZE.reports.testContentReport.sampleTest]);

  // looping in provided providedReportDataArray and formatting data
  for (let i = 0; i < providedReportDataArray.length; i++) {
    // if question list section changes
    if (
      parseInt(providedReportDataArray[i].test_section_component) > initialTestSectionComponentId
    ) {
      // incrementing questionSection
      questionSection += 1;
      // reinitializing questionNumber
      questionNumber = 1;
    }
    questionTextFr = "";
    questionTextEn = "";
    questionTextHtmlFr = "";
    questionTextHtmlEn = "";
    // looping in question sections
    for (let y = 0; y < providedReportDataArray[i].question_sections.length; y++) {
      // Question ID
      questionId =
        providedReportDataArray[i].ppc_question_id !== ""
          ? providedReportDataArray[i].ppc_question_id
          : LOCALIZE.commons.na;
      // French Questions
      if (
        providedReportDataArray[i].question_sections[y].question_sections_type_markdown_fr !== null
      ) {
        // removing starting and ending double quotes from string
        questionTextFr += ` ${providedReportDataArray[i].question_sections[
          y
        ].question_sections_type_markdown_fr.substring(
          1,
          providedReportDataArray[i].question_sections[y].question_sections_type_markdown_fr
            .length - 1
        )}`;
        // removing starting and ending double quotes from string
        questionTextHtmlFr += ` ${providedReportDataArray[i].question_sections[
          y
        ].question_sections_type_markdown_html_fr.substring(
          1,
          providedReportDataArray[i].question_sections[y].question_sections_type_markdown_html_fr
            .length - 1
        )}`;
      }
      // English Questions
      if (
        providedReportDataArray[i].question_sections[y].question_sections_type_markdown_en !== null
      ) {
        // removing starting and ending double quotes from string
        questionTextEn += ` ${providedReportDataArray[i].question_sections[
          y
        ].question_sections_type_markdown_en.substring(
          1,
          providedReportDataArray[i].question_sections[y].question_sections_type_markdown_en
            .length - 1
        )}`;
        // removing starting and ending double quotes from string
        questionTextHtmlEn += ` ${providedReportDataArray[i].question_sections[
          y
        ].question_sections_type_markdown_html_en.substring(
          1,
          providedReportDataArray[i].question_sections[y].question_sections_type_markdown_html_en
            .length - 1
        )}`;
      }
    }
    // if question text is defined, remove first char (which is a space)
    if (questionTextFr !== "") {
      questionTextFr = questionTextFr.substring(1);
      questionTextHtmlFr = questionTextHtmlFr.substring(1);
    }
    if (questionTextEn !== "") {
      questionTextEn = questionTextEn.substring(1);
      questionTextHtmlEn = questionTextHtmlEn.substring(1);
    }

    // adding double quotes to final strings, so commas are not considered as separators and are well formatted in the CSV
    questionTextFr = `"${questionTextFr}"`;
    questionTextHtmlFr = `"${questionTextHtmlFr}"`;
    questionTextEn = `"${questionTextEn}"`;
    questionTextHtmlEn = `"${questionTextHtmlEn}"`;

    // creating report data for the question fields
    reportData.push([
      questionSection,
      providedReportDataArray[i].test_section_component_title_fr,
      providedReportDataArray[i].test_section_component_title_en,
      questionId,
      questionNumber,
      // HTML removed versions
      questionTextFr,
      questionTextEn,
      // HTML versions
      questionTextHtmlFr,
      questionTextHtmlEn
    ]);

    // looping in answers for the current question
    for (let y = 0; y < providedReportDataArray[i].answers.length; y++) {
      // make sure we have answers
      if (providedReportDataArray[i].answers.length > 0) {
        // creating report data for the answer fields
        reportData[lineNumber].push(
          providedReportDataArray[i].answers[y].ppc_answer_id !== ""
            ? providedReportDataArray[i].answers[y].ppc_answer_id
            : LOCALIZE.commons.na,
          // Answer Text without HTML
          providedReportDataArray[i].answers[y].answer_details_fr !== null || ""
            ? providedReportDataArray[i].answers[y].answer_details_fr
            : LOCALIZE.commons.na,
          providedReportDataArray[i].answers[y].answer_details_en !== null || ""
            ? providedReportDataArray[i].answers[y].answer_details_en
            : LOCALIZE.commons.na,
          // Answer Text with HTML
          providedReportDataArray[i].answers[y].answer_details_fr !== null || ""
            ? providedReportDataArray[i].answers[y].answer_details_html_fr
            : LOCALIZE.commons.na,
          providedReportDataArray[i].answers[y].answer_details_en !== null || ""
            ? providedReportDataArray[i].answers[y].answer_details_html_en
            : LOCALIZE.commons.na,
          providedReportDataArray[i].answers[y].scoring_value !== null || ""
            ? providedReportDataArray[i].answers[y].scoring_value
            : LOCALIZE.commons.na
        );
        // if we are at the last answer for the current question
        if (y === providedReportDataArray[i].answers.length - 1) {
          // verify if the maximum number of answers is higher than the number of answers of the current question, put N/A for all the rest of answers' information
          if (maxNumberOfAnswers > providedReportDataArray[i].answers.length) {
            for (let z = maxNumberOfAnswers; z >= providedReportDataArray[i].answers.length; z--) {
              reportData[lineNumber].push(
                LOCALIZE.commons.na,
                LOCALIZE.commons.na,
                LOCALIZE.commons.na,
                LOCALIZE.commons.na,
                LOCALIZE.commons.na,
                LOCALIZE.commons.na
              );
            }
          }
          // put the sample test information (since it is the last column)
          reportData[lineNumber].push(
            providedReportDataArray[i].sample_test_indicator !== null || ""
              ? getFormattedSampleStatusFlag(providedReportDataArray[i].sample_test_indicator)
              : LOCALIZE.commons.na
          );
        }
      }
      // if we don't have any answer, put N/A everywhere
      else {
        for (let z = 0; z < maxNumberOfAnswers; z++) {
          reportData[lineNumber].push(
            LOCALIZE.commons.na,
            LOCALIZE.commons.na,
            LOCALIZE.commons.na,
            LOCALIZE.commons.na,
            LOCALIZE.commons.na,
            LOCALIZE.commons.na
          );
        }
        // put the sample test information (since it is the last column)
        reportData[lineNumber].push(
          providedReportDataArray[i].sample_test_indicator !== null || ""
            ? getFormattedSampleStatusFlag(providedReportDataArray[i].sample_test_indicator)
            : LOCALIZE.commons.na
        );
      }
    }
    lineNumber += 1;
    questionNumber += 1;
  }

  return reportData;
}

export default generateTestContentReport;
