import { LANGUAGES } from "../../../modules/LocalizeRedux";
import LOCALIZE from "../../../text_resources";
import getFormattedStatus from "./utils";

function generateAdaptedTestsReport(providedReportDataArray, currentLanguage) {
  // initializing reportData
  const reportData = [];

  // pushing columns definition
  reportData.push([
    LOCALIZE.reports.adaptedTestsReport.candidateLastName,
    LOCALIZE.reports.adaptedTestsReport.candidateFirstName,
    LOCALIZE.reports.adaptedTestsReport.requestingOrganization,
    LOCALIZE.reports.adaptedTestsReport.taWorkEmail,
    LOCALIZE.reports.adaptedTestsReport.taLastName,
    LOCALIZE.reports.adaptedTestsReport.taFirstName,
    LOCALIZE.reports.adaptedTestsReport.taOrganization,
    LOCALIZE.reports.adaptedTestsReport.orderNumber,
    LOCALIZE.reports.adaptedTestsReport.referenceNumber,
    LOCALIZE.reports.adaptedTestsReport.testStatus,
    LOCALIZE.reports.adaptedTestsReport.isInvalid,
    LOCALIZE.reports.adaptedTestsReport.testStartDateAndTime,
    LOCALIZE.reports.adaptedTestsReport.testSubmitDateAndTime,
    LOCALIZE.reports.adaptedTestsReport.testVersion,
    LOCALIZE.reports.adaptedTestsReport.testDescriptionFr,
    LOCALIZE.reports.adaptedTestsReport.testDescriptionEn,
    LOCALIZE.reports.adaptedTestsReport.totalTimeAllotted,
    LOCALIZE.reports.adaptedTestsReport.totalTimeUsed,
    LOCALIZE.reports.adaptedTestsReport.breakBankAllotted,
    LOCALIZE.reports.adaptedTestsReport.breakBankUsed
  ]);
  // looping in provided providedReportDataArray and formatting data
  // enclose ta_org and requesting_dep in double quotes to escape any commas
  for (let i = 0; i < providedReportDataArray.length; i++) {
    reportData.push([
      providedReportDataArray[i].candidate_last_name,
      providedReportDataArray[i].candidate_first_name,
      providedReportDataArray[i].requesting_organization_dept_id !== null
        ? currentLanguage === LANGUAGES.english
          ? `"${providedReportDataArray[i].requesting_organization_dept_edesc} (${providedReportDataArray[i].requesting_organization_dept_eabrv})"`
          : `"${providedReportDataArray[i].requesting_organization_dept_fdesc} (${providedReportDataArray[i].requesting_organization_dept_fabrv})"`
        : "",
      providedReportDataArray[i].test_administrator_goc_email,
      providedReportDataArray[i].test_administrator_last_name,
      providedReportDataArray[i].test_administrator_first_name,
      providedReportDataArray[i].test_administrator_dept_id !== null
        ? currentLanguage === LANGUAGES.english
          ? `"${providedReportDataArray[i].test_administrator_dept_edesc} (${providedReportDataArray[i].test_administrator_dept_eabrv})"`
          : `"${providedReportDataArray[i].test_administrator_dept_fdesc} (${providedReportDataArray[i].test_administrator_dept_fabrv})"`
        : "",
      providedReportDataArray[i].test_order_number,
      providedReportDataArray[i].reference_number,
      getFormattedStatus(providedReportDataArray[i].test_status_codename),
      providedReportDataArray[i].is_invalid ? LOCALIZE.commons.true : LOCALIZE.commons.false,
      providedReportDataArray[i].test_start_time !== null
        ? providedReportDataArray[i].test_start_time.substring(0, 19)
        : "",
      providedReportDataArray[i].test_end_time !== null
        ? providedReportDataArray[i].test_end_time.substring(0, 19)
        : "",
      providedReportDataArray[i].test_code,
      `"${providedReportDataArray[i].test_description_fr}"`,
      `"${providedReportDataArray[i].test_description_en}"`,
      providedReportDataArray[i].total_time_allotted_in_minutes,
      providedReportDataArray[i].total_time_used_in_minutes,
      providedReportDataArray[i].break_bank_allotted_in_minutes,
      providedReportDataArray[i].break_bank_used_in_minutes
    ]);
  }

  return reportData;
}

export default generateAdaptedTestsReport;
