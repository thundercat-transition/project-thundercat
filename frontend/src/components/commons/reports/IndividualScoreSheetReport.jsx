/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import PropTypes from "prop-types";
import withRouter from "../../withRouter";
import LOCALIZE from "../../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  getTestsBasedOnTestOrderNumber,
  getCandidatesBasedOnSelectedTest,
  setReportSelectedParameters,
  setGenerateReportDisabledState
} from "../../../modules/ReportsRedux";
import DropdownSelect from "../DropdownSelect";
import { Row, Col } from "react-bootstrap";
import { REPORT_REQUESTOR } from "./Constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  optionSelectionContainer: {
    padding: "12px 24px",
    margin: "0 auto",
    width: "80%"
  },
  labelContainer: {
    verticalAlign: "middle",
    paddingRight: 12
  },
  singleDropdownContainer: {
    verticalAlign: "middle",
    width: "100%"
  },
  loading: {
    width: "100%",
    minHeight: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  }
};

class IndividualScoreSheetReport extends Component {
  static propTypes = {
    reportRequestor: PropTypes.string.isRequired,
    getTestOrderNumbers: PropTypes.func.isRequired,
    currentlyLoading: PropTypes.bool.isRequired
  };

  state = {
    isLoadingTestOrderNumber: true,
    testOrderNumberVisible: false,
    testOrderNumberOptions: [],
    selectedTestOrderNumber: "",
    isLoadingTests: true,
    testVisible: false,
    testOptions: [],
    selectedTest: "",
    isLoadingCandidates: true,
    candidateVisible: false,
    candidateOptions: [],
    selectedCandidate: ""
  };

  // setting all states to their initial values
  async getInitialState() {
    this.setState({
      isLoadingTestOrderNumber: true,
      testOrderNumberVisible: false,
      testOrderNumberOptions: [],
      selectedTestOrderNumber: "",
      isLoadingTests: true,
      testVisible: false,
      testOptions: [],
      selectedTest: "",
      isLoadingCandidates: true,
      candidateVisible: false,
      candidateOptions: [],
      selectedCandidate: ""
    });
  }

  componentDidMount = () => {
    this.populateTestOrderNumberOptions();
  };

  // populating test order number options
  populateTestOrderNumberOptions = () => {
    this.setState(
      {
        testOrderNumberVisible: true,
        testVisible: false,
        candidateVisible: false,
        isLoadingTestOrderNumber: true
      },
      () => {
        this.props.getTestOrderNumbers().then(response => {
          const testOrderNumberOptions = [];
          for (let i = 0; i < response.length; i++) {
            // requested by TA
            if (this.props.reportRequestor === REPORT_REQUESTOR.ta) {
              testOrderNumberOptions.push({
                // adding " - <ta_user_id>", so TAs that used the same reference number are not all selected in the dropdown
                value: `${
                  response[i].test_order_number !== null
                    ? response[i].test_order_number
                    : response[i].reference_number
                } - ${response[i].ta_user_id}`,
                label: `${
                  response[i].test_order_number !== null
                    ? response[i].test_order_number
                    : response[i].reference_number
                } (${
                  response[i].test_order_number !== null
                    ? response[i].staffing_process_number
                    : LOCALIZE.reports.orderlessRequest
                })`,
                orderless: response[i].test_order_number === null,
                ta_user_id: response[i].ta_user_id
              });
            } else {
              testOrderNumberOptions.push({
                // adding " - <ta_user_id>", so TAs that used the same reference number are not all selected in the dropdown
                value: `${
                  response[i].test_order_number !== null
                    ? response[i].test_order_number
                    : response[i].reference_number
                } - ${response[i].ta_user_id}`,
                label: `${
                  response[i].test_order_number !== null
                    ? response[i].test_order_number
                    : response[i].reference_number
                } (${
                  response[i].test_order_number !== null
                    ? response[i].staffing_process_number
                    : response[i].ta_email
                })`,
                orderless: response[i].test_order_number === null,
                ta_user_id: response[i].ta_user_id
              });
            }
          }
          this.getInitialState().then(() => {
            // disabling generate report button
            this.props.setGenerateReportDisabledState(true);
            this.setState({
              testOrderNumberOptions: testOrderNumberOptions,
              testOrderNumberVisible: true,
              isLoadingTestOrderNumber: false
            });
          });
        });
      }
    );
  };

  // get selected test order number
  getSelectedTestOrderNumber = selectedOption => {
    this.setState(
      {
        testVisible: true,
        candidateVisible: false,
        isLoadingTests: true
      },
      () => {
        this.props
          .getTestsBasedOnTestOrderNumber(
            // splitting with the " - " since we've added that in the value option to avoid multi selection in the dropdown
            selectedOption.value.split(" - ")[0],
            selectedOption.orderless,
            this.props.reportRequestor === REPORT_REQUESTOR.ta,
            this.props.reportRequestor !== REPORT_REQUESTOR.ta ? selectedOption.ta_user_id : null
          )
          .then(response => {
            const testOptions = [];
            for (let i = 0; i < response.length; i++) {
              testOptions.push({
                value: response[i].test_id,
                label: `${response[i][`${this.props.currentLanguage}_test_name`]} (v${
                  response[i].test_version
                })`,
                orderless: selectedOption.orderless
              });
            }
            // disabling generate report button
            this.props.setGenerateReportDisabledState(true);
            this.setState({
              selectedTestOrderNumber: selectedOption,
              selectedTest: "",
              selectedCandidate: "",
              testOptions: testOptions,
              testVisible: true,
              candidateVisible: false,
              isLoadingTests: false
            });
          });
      }
    );
  };

  // get selected test
  getSelectedTest = selectedOption => {
    this.setState(
      {
        candidateVisible: true,
        isLoadingCandidates: true
      },
      () => {
        this.props
          .getCandidatesBasedOnSelectedTest(
            // splitting with the " - " since we've added that in the value option to avoid multi selection in the dropdown
            this.state.selectedTestOrderNumber.value.split(" - ")[0],
            selectedOption.orderless,
            selectedOption.value,
            this.props.reportRequestor === REPORT_REQUESTOR.ta,
            this.props.reportRequestor === REPORT_REQUESTOR.ta
              ? null
              : // using the split " - " to get the repsective ta_user_id if triggered by BO or other privileged users
                this.state.selectedTestOrderNumber.value.split(" - ")[1]
          )
          .then(response => {
            // populating candidate options
            const candidateOptions = [];
            for (let i = 0; i < response.length; i++) {
              candidateOptions.push({
                value: response[i].candidate_user_id,
                label: `${response[i].candidate_last_name}, ${response[i].candidate_first_name} (${response[i].candidate_email})`,
                orderless: selectedOption.orderless
              });
            }
            // disabling generate report button
            this.props.setGenerateReportDisabledState(true);
            this.setState({
              selectedTest: selectedOption,
              selectedCandidate: "",
              candidateOptions: candidateOptions,
              // candidate dropdown visible only if the report type is 1 (individual Score Sheet)
              candidateVisible: true,
              isLoadingCandidates: false
            });
          });
      }
    );
  };

  // get selected candidate
  getSelectedCandidate = selectedOption => {
    this.setState(
      {
        selectedCandidate: selectedOption
      },
      () => {
        // updating all needed redux states
        this.props.setReportSelectedParameters(
          selectedOption.orderless,
          {
            value: this.state.selectedTestOrderNumber.value.split(" - ")[0],
            label: this.state.selectedTestOrderNumber.label
          },
          this.state.selectedTest,
          this.state.selectedCandidate,
          null,
          null,
          null,
          null,
          null,
          null
        );
        // enabling generate report button
        this.props.setGenerateReportDisabledState(false);
      }
    );
  };

  render() {
    const {
      isLoadingTestOrderNumber,
      testOrderNumberOptions,
      testOrderNumberVisible,
      selectedTestOrderNumber,
      testOptions,
      isLoadingTests,
      testVisible,
      selectedTest,
      isLoadingCandidates,
      candidateVisible,
      candidateOptions,
      selectedCandidate
    } = this.state;
    return (
      <div>
        {testOrderNumberVisible && (
          <Row
            className="align-items-center justify-content-start"
            id="unit-test-test-order-number-dropdown"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="test-order-number-label">{LOCALIZE.reports.testOrderNumberLabel}</label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
            >
              {isLoadingTestOrderNumber ? (
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <DropdownSelect
                  idPrefix="individual-report-test-order-number-dropdown"
                  ariaLabelledBy="test-order-number-label"
                  placeholder=""
                  isDisabled={this.props.currentlyLoading}
                  options={testOrderNumberOptions}
                  onChange={this.getSelectedTestOrderNumber}
                  defaultValue={selectedTestOrderNumber}
                  hasPlaceholder={true}
                ></DropdownSelect>
              )}
            </Col>
          </Row>
        )}
        {testVisible && (
          <Row
            className="align-items-center justify-content-start"
            id="unit-test-test-dropdown"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="test-label">{LOCALIZE.reports.testLabel}</label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
            >
              {isLoadingTests ? (
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <DropdownSelect
                  idPrefix="individual-report-test-dropdown"
                  ariaLabelledBy="test-label"
                  placeholder=""
                  isDisabled={this.props.currentlyLoading}
                  options={testOptions}
                  onChange={this.getSelectedTest}
                  defaultValue={selectedTest}
                  hasPlaceholder={true}
                ></DropdownSelect>
              )}
            </Col>
          </Row>
        )}
        {candidateVisible && (
          <Row
            className="align-items-center justify-content-start"
            id="unit-test-candidate-dropdown"
            style={styles.optionSelectionContainer}
            role="presentation"
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="candidate-label">{LOCALIZE.reports.candidateLabel}</label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.singleDropdownContainer}
            >
              {isLoadingCandidates ? (
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <DropdownSelect
                  idPrefix="individual-report-candidate-dropdown"
                  ariaLabelledBy="candidate-label"
                  placeholder=""
                  isDisabled={this.props.currentlyLoading}
                  options={candidateOptions}
                  onChange={this.getSelectedCandidate}
                  defaultValue={selectedCandidate}
                  hasPlaceholder={true}
                ></DropdownSelect>
              )}
            </Col>
          </Row>
        )}
      </div>
    );
  }
}

export { IndividualScoreSheetReport as unconnectedIndividualScoreSheetReport };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    username: state.user.username
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTestsBasedOnTestOrderNumber,
      getCandidatesBasedOnSelectedTest,
      setReportSelectedParameters,
      setGenerateReportDisabledState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(IndividualScoreSheetReport));
