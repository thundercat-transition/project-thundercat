// These are the choices available for button themes
// Note: THEME was extracted from frontend/src/components/commons/CustomButton.jsx so it can be imported for unit tests (we had to mock customButton) + for good practice
const THEME = {
  PRIMARY: "primary",
  PRIMARY_IN_TEST: "primary-in-test",
  SECONDARY: "secondary",
  SUCCESS: "success",
  DANGER: "danger",
  PRIMARY_WIDE: "primary-wide",
  PRIMARY_TIME: "primary-time"
};

export default THEME;
