import React from "react";
import PropTypes from "prop-types";

export const maxWidthUtils = {
  wide: 1920
};

export const styles = {
  container: {
    maxWidth: 1440,
    margin: "0px auto",
    textAlign: "left",
    paddingTop: 20
  },
  banner: {
    width: "100%"
  }
};

// Manages displaying content within correct width and with a banner.
// Should be used whenever adding a new view to CAT.
const ContentContainer = props => {
  return (
    <div
      style={
        props.customMaxWidth
          ? { ...styles.container, ...{ maxWidth: props.customMaxWidth } }
          : { ...styles.container }
      }
    >
      {props.children}
    </div>
  );
};

ContentContainer.propTypes = {
  children: PropTypes.node,
  hideBanner: PropTypes.bool,
  customMaxWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

export default ContentContainer;
