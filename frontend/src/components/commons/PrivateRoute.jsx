import React from "react";
import { Navigate } from "react-router-dom";

const PrivateRoute = ({ component: Component, auth, redirectTo, ...rest }) => {
  return auth ? <Component {...rest} /> : <Navigate to={redirectTo} />;
};

export default PrivateRoute;
