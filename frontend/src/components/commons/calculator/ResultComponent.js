import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { triggerRerender } from "../../../modules/CalculatorRedux";

const styles = {
  resultMainDiv: {
    minHeight: 90,
    lineHeight: "74px",
    backgroundColor: "transparent",
    width: "100%",
    border: "1px solid #00565e"
  },
  resultPTag: {
    overflowX: "auto",
    padding: "6px 12px",
    margin: "0 2px 2px 2px",
    textAlign: "right",
    fontWeight: "bold"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  }
};

class ResultComponent extends Component {
  componentDidUpdate = prevProps => {
    // if result gets updated
    if (prevProps.result !== this.props.result) {
      // scroll to left
      this.scrollToLeft();
      // trigger rerender (useful for notepad height calculation)
      this.props.triggerRerender();
    }
  };

  scrollToLeft = () => {
    const resultPTag = document.getElementById("result-p-tag");
    if (resultPTag) {
      resultPTag.scrollLeft += 100;
    }
  };

  render() {
    const { result } = this.props;

    let customFontSize = "25px";
    // if font size > 25
    if (parseInt(this.props.accommodations.fontSize.split("px")[0]) > 25) {
      customFontSize = this.props.accommodations.fontSize;
    }
    const accommodationStyles = {
      fontFamily: this.props.accommodations.fontFamily,
      //   fontSize: this.props.accommodations.fontSize
      fontSize: customFontSize
    };
    return (
      <div>
        <div id="calculator-result" tabIndex={0} style={styles.resultMainDiv}>
          <span id="calculator-result-accessibility" style={styles.hiddenText}>
            {LOCALIZE.ariaLabel.calculatorResult}
          </span>
          <p
            id="result-p-tag"
            aria-labelledby="calculator-result-accessibility"
            style={{ ...styles.resultPTag, ...accommodationStyles }}
          >
            {result}
          </p>
        </div>
      </div>
    );
  }
}

export { ResultComponent as UnconnectedResultComponent };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      triggerRerender
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ResultComponent);
