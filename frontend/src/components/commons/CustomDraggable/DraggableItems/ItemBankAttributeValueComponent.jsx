import { Col, Row } from "react-bootstrap";
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as _ from "lodash";
import { makeTextBoxField } from "../../../testBuilder/helpers";
import LOCALIZE from "../../../../text_resources";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  }
};

const styles = {
  textArea: {
    padding: "6px 12px",
    border: "1px solid #00565E",
    borderRadius: 4,
    width: "100%",
    height: 38,
    resize: "none",
    overflow: "hidden"
  },
  col: {
    padding: 0
  }
};

class ItemBankAttributeValueComponent extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      index: PropTypes.number,
      item_bank_attribute_index: PropTypes.number,
      item_bank_attribute_value: PropTypes.object,
      onTextAreaComponentChange: PropTypes.func,
      draggable: PropTypes.bool,
      getItemStyle: PropTypes.func,
      currentLanguage: PropTypes.string,
      validity: PropTypes.bool
    };

    ItemBankAttributeValueComponent.defaultProps = {
      draggable: false
    };
  }

  render() {
    return (
      <Row
        className="w-100 align-items-center justify-content-between"
        draggable={this.props.draggable}
      >
        {makeTextBoxField(
          "",
          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.itemAttributes
            .itemAttributeCollapsingItem.attributeValueCollapsingItem.value_identifier,
          this.props.item_bank_attribute_value.value.text,
          this.props.validity,
          this.props.onTextAreaComponentChange,
          `item-bank-attribute-value-input-${this.props.item_bank_attribute_index}-${this.props.index}`,
          false,
          {},
          false,
          {
            xl: columnSizes.firstColumn.xl,
            lg: columnSizes.firstColumn.lg,
            md: columnSizes.firstColumn.md,
            sm: columnSizes.firstColumn.sm,
            xs: columnSizes.firstColumn.xs
          }
        )}
      </Row>
    );
  }
}

export { ItemBankAttributeValueComponent as unconnectedItemBankAttributeValueComponent };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    itemData: state.itemBank.itemData,
    item_bank_data: state.itemBank.item_bank_data,
    selectedContentLanguage: state.itemBank.selectedContentLanguage
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ItemBankAttributeValueComponent);
