import { Row } from "react-bootstrap";
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as _ from "lodash";
import { makeTextBoxField } from "../../../testBuilder/helpers";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  }
};

const styles = {
  input: {
    textAlign: "center"
  }
};

class ItemBankBundleItemContentComponent extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      item: PropTypes.object,
      draggable: PropTypes.bool
    };

    ItemBankBundleItemContentComponent.defaultProps = {
      draggable: true
    };
  }

  render() {
    return (
      <Row
        className="w-100 align-items-center justify-content-between"
        draggable={this.props.draggable}
      >
        {makeTextBoxField(
          `item-content-item-input-${this.props.item.display_order}`,
          "",
          `${this.props.item.system_id_link} [${
            this.props.item.item_data.historical_id !== null
              ? this.props.item.item_data.historical_id
              : ""
          }] - ${
            this.props.item.item_data[`development_status_name_${this.props.currentLanguage}`]
          }`,
          true,
          () => {},
          `item-content-item-input-${this.props.item.system_id_link}-id`,
          true,
          styles.input,
          false,
          {
            xl: columnSizes.firstColumn.xl,
            lg: columnSizes.firstColumn.lg,
            md: columnSizes.firstColumn.md,
            sm: columnSizes.firstColumn.sm,
            xs: columnSizes.firstColumn.xs
          }
        )}
      </Row>
    );
  }
}

export { ItemBankBundleItemContentComponent as unconnectedItemBankBundleItemContentComponent };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ItemBankBundleItemContentComponent);
