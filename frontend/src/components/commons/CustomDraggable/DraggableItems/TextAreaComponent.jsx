import { Col, Row } from "react-bootstrap";
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  }
};

const styles = {
  textArea: {
    padding: "6px 12px",
    border: "1px solid #00565E",
    borderRadius: 4,
    width: "100%",
    minHeight: 90,
    resize: "none",
    overflow: "hidden"
    // maxHeight: 500
  },
  col: {
    padding: 0
  }
};

class TextAreaComponent extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      item: PropTypes.object,
      onTextAreaComponentChange: PropTypes.func,
      draggable: PropTypes.bool,
      getItemStyle: PropTypes.func
    };

    TextAreaComponent.defaultProps = {
      draggable: false
    };
  }

  componentDidMount = () => {
    this.triggerTextAreaHeightCalculation();
    // waiting 1 second and calling back the textarea height calculation to make sure that the hight is well set
    // sometimes height was not properly calculated on refresh actions because of slow redux state updates
    setTimeout(() => {
      this.triggerTextAreaHeightCalculation();
    }, 1000);
  };

  componentDidUpdate = prevProps => {
    // if accommodations or item_content or selectedContentLanguage gets updated
    if (
      prevProps.accommodations !== this.props.accommodations ||
      prevProps.itemData.item_content !== this.props.itemData.item_content ||
      prevProps.selectedContentLanguage !== this.props.selectedContentLanguage ||
      prevProps.secondColumnTab !== this.props.secondColumnTab
    ) {
      // calling input height calculation function
      this.triggerTextAreaHeightCalculation();
    }
  };

  triggerTextAreaHeightCalculation = () => {
    const textarea = document.getElementById(`item-stem-input-${this.props.item.id}`);
    if (textarea !== null) {
      textarea.style.height = "1px";
      textarea.style.height = `${textarea.scrollHeight + 3}px`;
    }
  };

  render() {
    return (
      <Row
        className="w-100 align-items-center justify-content-between"
        draggable={this.props.draggable}
      >
        <Col
          xl={columnSizes.firstColumn.xl}
          lg={columnSizes.firstColumn.lg}
          md={columnSizes.firstColumn.md}
          sm={columnSizes.firstColumn.sm}
          xs={columnSizes.firstColumn.xs}
          style={styles.col}
          className="d-flex justify-content-center"
        >
          <textarea
            id={`item-stem-input-${this.props.item.id}`}
            className="w-100 valid-field"
            style={styles.textArea}
            onChange={this.props.onTextAreaComponentChange}
            value={this.props.item.text}
          />
        </Col>
      </Row>
    );
  }
}

export { TextAreaComponent as unconnectedTextAreaComponent };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    itemData: state.itemBank.itemData,
    selectedContentLanguage: state.itemBank.selectedContentLanguage,
    secondColumnTab: state.itemBank.secondColumnTab
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TextAreaComponent);
