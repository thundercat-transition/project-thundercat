import { Col, Row } from "react-bootstrap";
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import CollapsingItemContainer from "../../../eMIB/CollapsingItemContainer";
import ItemAttributeForm from "../../../testBuilder/itemBank/ItemAttributeForm";
import LOCALIZE from "../../../../text_resources";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  }
};

const styles = {
  textArea: {
    padding: "6px 12px",
    border: "1px solid #00565E",
    borderRadius: 4,
    width: "100%",
    minHeight: 90,
    resize: "none",
    overflow: "hidden"
  },
  col: {
    padding: 0
  },
  bold: {
    fontWeight: "bold"
  },
  normal: {
    fontWeight: "normal"
  },
  tab: {
    display: "inline-block",
    marginLeft: "40px"
  }
};

// Variable used to specify max length of the values in the collapsable title
const MAX_VALUES_PRINT_LENGTH = 35;

class ItemBankAttributeComponent extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      index: PropTypes.number,
      item_bank_attribute: PropTypes.object,
      onTextAreaComponentChange: PropTypes.func,
      draggable: PropTypes.bool,
      getItemStyle: PropTypes.func,
      currentLanguage: PropTypes.string,
      deleteItem: PropTypes.func,
      draggableSwitch: PropTypes.func
    };

    ItemBankAttributeComponent.defaultProps = {
      draggable: false
    };
  }

  // Used to print the attribute name in the Collapsable's title
  printCollapsableTitleAttributeName = () => {
    let finalText = "";
    // Construct the final string with values we have
    if (
      this.props.item_bank_attribute.item_bank_attribute_text[`${this.props.currentLanguage}`] &&
      this.props.item_bank_attribute.item_bank_attribute_text[`${this.props.currentLanguage}`] !==
        ""
    ) {
      finalText =
        this.props.item_bank_attribute.item_bank_attribute_text[`${this.props.currentLanguage}`][0]
          .text;
    }
    // if we have more than MAX_VALUES_PRINT_LENGTH chars, trim the final string so we have MAX_VALUES_PRINT_LENGTH-3 chars + "..."
    if (finalText.length > MAX_VALUES_PRINT_LENGTH) {
      finalText = finalText.substring(0, MAX_VALUES_PRINT_LENGTH - 3);
      finalText += "...";
    }

    // if we don't have anything, print N/A
    if (finalText === "") {
      finalText = LOCALIZE.commons.na;
    }

    return finalText;
  };

  // Used to print the values in the Collapsable's title
  printCollapsableTitleValues = () => {
    let finalText = "";
    // Construct the final string with values we have
    if (this.props.item_bank_attribute.item_bank_attribute_values) {
      this.props.item_bank_attribute.item_bank_attribute_values.forEach(value => {
        // We won't be adding an empty values to the collapsable title, therefore, skip if equals to ""
        if (finalText === "" && value.value.text !== "") {
          finalText += `${value.value.text}`;
        } else if (value.value.text !== "") {
          finalText += `, ${value.value.text}`;
        }
      });
    }
    // if we have more than MAX_VALUES_PRINT_LENGTH chars, trim the final string so we have MAX_VALUES_PRINT_LENGTH-3 chars + "..."
    if (finalText.length > MAX_VALUES_PRINT_LENGTH) {
      finalText = finalText.substring(0, MAX_VALUES_PRINT_LENGTH - 3);
      finalText += "...";
    }

    // if we don't have anything, print N/A
    if (finalText === "") {
      finalText = LOCALIZE.commons.na;
    }

    return finalText;
  };

  render() {
    return (
      <Row
        className="w-100 align-items-center justify-content-between"
        draggable={this.props.draggable}
      >
        <Col
          xl={columnSizes.firstColumn.xl}
          lg={columnSizes.firstColumn.lg}
          md={columnSizes.firstColumn.md}
          sm={columnSizes.firstColumn.sm}
          xs={columnSizes.firstColumn.xs}
          style={styles.col}
          className="d-flex justify-content-center w-100"
          draggable={false}
        >
          <CollapsingItemContainer
            draggable={false}
            key={this.props.index}
            index={this.props.index}
            triggerCollapse={this.props.triggerCollapse}
            draggableSwitch={() => {
              this.props.draggableSwitch();
            }}
            title={
              <label>
                <span style={styles.bold}>
                  {LOCALIZE.formatString(
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                      .itemAttributes.itemAttributeCollapsingItem.title,
                    this.props.index + 1
                  )}
                </span>
                <span style={styles.normal}>{this.printCollapsableTitleAttributeName()}</span>
                <span style={styles.tab}></span>
                <span style={styles.bold}>
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                      .itemAttributes.itemAttributeCollapsingItem.values
                  }
                </span>
                <span style={styles.normal}>{this.printCollapsableTitleValues()}</span>
              </label>
            }
            body={
              <ItemAttributeForm
                item_bank_attribute={this.props.item_bank_attribute}
                INDEX={this.props.index}
                deleteItem={this.props.deleteItem}
              />
            }
          />
        </Col>
      </Row>
    );
  }
}

export { ItemBankAttributeComponent as unconnectedItemBankAttributeComponent };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    itemData: state.itemBank.itemData,
    selectedContentLanguage: state.itemBank.selectedContentLanguage
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ItemBankAttributeComponent);
