import React from "react";
import { Draggable } from "react-beautiful-dnd";
import PropTypes from "prop-types";
import { Row, Col, Container } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowsAlt, faTimes } from "@fortawesome/free-solid-svg-icons";
import "../../../css/draggableComponents/deleteButton.css";
import TextAreaComponent from "./DraggableItems/TextAreaComponent";
import CustomButton from "../CustomButton";
import TestOptionComponent from "./DraggableItems/TestOptionComponent";
import ItemBankAttributeComponent from "./DraggableItems/ItemBankAttributeComponent";
import ItemBankAttributeValueComponent from "./DraggableItems/ItemBankAttributeValueComponent";
import ItemBankBundleItemContentComponent from "./DraggableItems/ItemBankBundleItemContentComponent";
import ItemBankBundleBundleContentComponent from "./DraggableItems/ItemBankBundleBundleContentComponent";
import ItemBankBundleBundleContentBundleRuleComponent from "./DraggableItems/ItemBankBundleBundleContentBundleRuleComponent";
import ItemBankBundleBundleContentBundleRuleContentComponent from "./DraggableItems/ItemBankBundleBundleContentBundleRuleContentComponent";

// mirror of DraggableItemType in "..\backend\db_views\serializers\item_bank_serializers.py"
export const DRAGGABLE_ITEM_TYPE = {
  TextAreaComponent: "TextAreaComponent",
  TestOptionComponent: "TestOptionComponent"
};

const columnSizes = {
  firstColumn: {
    xs: 1,
    sm: 1,
    md: 1,
    lg: 1,
    xl: 1
  },
  secondColumn: {
    xs: 10,
    sm: 10,
    md: 10,
    lg: 10,
    xl: 10
  },
  thirdColumn: {
    xs: 1,
    sm: 1,
    md: 1,
    lg: 1,
    xl: 1
  },
  secondColumnDragDisabled: {
    xs: 11,
    sm: 11,
    md: 11,
    lg: 11,
    xl: 11
  },

  secondColumnDragAndDeleteDisabled: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 12
  },

  innerFirstColumn: {
    xs: 1,
    sm: 1,
    md: 1,
    lg: 1,
    xl: 1
  },
  innerSecondColumn: {
    xs: 11,
    sm: 11,
    md: 11,
    lg: 11,
    xl: 11
  }
};

export default class CustomDraggable extends React.Component {
  styles = {
    innerCol: {
      margin: 0,
      padding: 0
    },
    moveIcon: {
      color: "#00565e",
      transform: "scale(1.7)"
    },
    moveIconDiv: {
      alignItems: this.props.sideButtonsOnTop ? "start" : "center",
      paddingTop: this.props.sideButtonsOnTop ? "30px" : "0",
      display: "flex"
    },
    deleteButton: {
      color: "red",
      border: "none",
      minWidth: "inherit",
      display: "flex",
      alignItems: this.props.sideButtonsOnTop ? "start" : "center",
      paddingTop: this.props.sideButtonsOnTop ? "30px" : "",
      padding: this.props.sideButtonsOnTop ? "0px" : "",
      justifyContent: "center",
      borderRadius: "0.35em",
      width: "inherit",
      maxHeight: "80px"
    },
    textArea: {
      padding: "6px 12px",
      border: "1px solid #00565E",
      borderRadius: 4,
      width: "100%",
      height: 50,
      resize: "none"
    },
    col: {
      padding: 0
    },
    draggable: {
      padding: "6px 0 6px 0"
    }
  };

  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      item: PropTypes.object,
      item_bank_attribute: PropTypes.object,
      item_bank_attribute_value: PropTypes.object,
      item_bank_attribute_index: PropTypes.number,
      bundleAssociatedItemData: PropTypes.object,
      bundleAssociatedBundleData: PropTypes.object,
      bundleBundlesRuleContentData: PropTypes.object,
      bundleBundlesRuleData: PropTypes.object,
      index: PropTypes.number,
      getItemStyle: PropTypes.func,
      onTextAreaComponentChange: PropTypes.func,
      onTextInputChange: PropTypes.func,
      onHistoricalOptionIdInputChange: PropTypes.func,
      onRationaleInputChange: PropTypes.func,
      onScoreInputChange: PropTypes.func,
      changeExcludeFromShuffle: PropTypes.func,
      deleteItem: PropTypes.func,
      currentLanguage: PropTypes.string,
      sideButtonsOnTop: PropTypes.bool,
      // allowing the removal of the drag icon
      isDraggable: PropTypes.bool,
      // allowing the removal of the delete icon
      isDeletable: PropTypes.bool,
      draggableSwitch: PropTypes.func,
      validity: PropTypes.bool,
      customDeleteButtonClass: PropTypes.string
    };

    CustomDraggable.defaultProps = {
      sideButtonsOnTop: false,
      isDraggable: true,
      isDeletable: true
    };
  }

  state = {
    textAreaContent: ""
  };

  getTextAreaContent = event => {
    const textAreaContent = event.target.value;

    this.setState({ textAreaContent: textAreaContent });
  };

  // TODO: Improve CustomDraggable to specify exactly what type of draggable we want with global variables;
  // Example: customDraggableType.ItemBankAttribute (1) or customDraggableType.ItemBankAttributeValue (2)
  getKeyAndDraggableID = () => {
    // If we have an Item Bank Attribute, return the index
    if (this.props.item_bank_attribute) {
      return `${this.props.index}`;
    }
    if (this.props.item_bank_attribute_value) {
      return `${this.props.item_bank_attribute_index}-${this.props.index}`;
    }
    if (this.props.bundleAssociatedItemData) {
      return `item-content-item-${this.props.bundleAssociatedItemData.system_id_link}`;
    }
    if (this.props.bundleAssociatedBundleData) {
      return `bundle-content-bundle-${this.props.bundleAssociatedBundleData.bundle_link_id}`;
    }
    if (this.props.bundleBundlesRuleContentData) {
      return `bundle-content-bundle-test-${this.props.bundleBundlesRuleContentData.bundle_link_id}`;
    }
    if (this.props.bundleBundlesRuleData) {
      return `bundle-content-bundle-rule-${this.props.bundleBundlesRuleData.id}`;
    }
    // Otherwise, we know we have an item
    return this.props.item.id;
  };

  // Estimation of the result:
  /*------------------------------------------------------------------------*/
  // Col:           //   Col:                                   // Col:     //
  //   Move Icon    //       <DraggableItem />                  //  Button  //
  //                //                                          //          //
  /*------------------------------------------------------------------------*/

  render() {
    return (
      <Draggable
        key={this.getKeyAndDraggableID()}
        draggableId={this.getKeyAndDraggableID()}
        index={this.props.index}
      >
        {(provided, snapshot) => (
          <Container>
            <Row
              className={`align-items-center justify-content-between align-items-stretch`}
              ref={provided.innerRef}
              {...provided.draggableProps}
              style={{
                ...this.props.getItemStyle(snapshot.isDragging, provided.draggableProps.style),
                ...this.styles.draggable
              }}
            >
              {this.props.isDraggable && (
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  className="d-flex justify-content-center align-items-stretch"
                  {...provided.dragHandleProps}
                >
                  <div style={this.styles.moveIconDiv}>
                    <FontAwesomeIcon style={this.styles.moveIcon} icon={faArrowsAlt} />
                  </div>
                </Col>
              )}
              <Col
                xl={
                  !this.props.isDraggable && !this.props.isDeletable
                    ? columnSizes.secondColumnDragAndDeleteDisabled.xl
                    : !this.props.isDraggable || !this.props.isDeletable
                    ? columnSizes.secondColumnDragDisabled.xl
                    : columnSizes.secondColumn.xl
                }
                lg={
                  !this.props.isDraggable && !this.props.isDeletable
                    ? columnSizes.secondColumnDragAndDeleteDisabled.lg
                    : !this.props.isDraggable || !this.props.isDeletable
                    ? columnSizes.secondColumnDragDisabled.lg
                    : columnSizes.secondColumn.lg
                }
                md={
                  !this.props.isDraggable && !this.props.isDeletable
                    ? columnSizes.secondColumnDragAndDeleteDisabled.md
                    : !this.props.isDraggable || !this.props.isDeletable
                    ? columnSizes.secondColumnDragDisabled.md
                    : columnSizes.secondColumn.md
                }
                sm={
                  !this.props.isDraggable && !this.props.isDeletable
                    ? columnSizes.secondColumnDragAndDeleteDisabled.sm
                    : !this.props.isDraggable || !this.props.isDeletable
                    ? columnSizes.secondColumnDragDisabled.sm
                    : columnSizes.secondColumn.sm
                }
                xs={
                  !this.props.isDraggable && !this.props.isDeletable
                    ? columnSizes.secondColumnDragAndDeleteDisabled.xs
                    : !this.props.isDraggable || !this.props.isDeletable
                    ? columnSizes.secondColumnDragDisabled.xs
                    : columnSizes.secondColumn.xs
                }
                className="d-flex justify-content-center"
                style={this.styles.col}
                draggable={false}
              >
                {this.props.item &&
                  this.props.item.item_type === DRAGGABLE_ITEM_TYPE.TextAreaComponent && (
                    <TextAreaComponent
                      onTextAreaComponentChange={this.props.onTextAreaComponentChange}
                      item={this.props.item}
                    />
                  )}
                {this.props.item &&
                  this.props.item.item_type === DRAGGABLE_ITEM_TYPE.TestOptionComponent && (
                    <TestOptionComponent
                      item={this.props.item}
                      onTextInputChange={this.props.onTextInputChange}
                      onHistoricalOptionIdInputChange={this.props.onHistoricalOptionIdInputChange}
                      onRationaleInputChange={this.props.onRationaleInputChange}
                      onScoreInputChange={this.props.onScoreInputChange}
                      changeExcludeFromShuffle={this.props.changeExcludeFromShuffle}
                    />
                  )}
                {this.props.item_bank_attribute_value && (
                  <ItemBankAttributeValueComponent
                    draggable={false}
                    onTextAreaComponentChange={this.props.onTextAreaComponentChange}
                    item_bank_attribute_index={this.props.item_bank_attribute_index}
                    item_bank_attribute_value={this.props.item_bank_attribute_value}
                    index={this.props.index}
                    handleDelete={this.props.deleteItem}
                    validity={this.props.validity}
                  />
                )}
                {this.props.item_bank_attribute && !this.props.item_bank_attribute_value && (
                  <ItemBankAttributeComponent
                    draggable={false}
                    onTextAreaComponentChange={this.props.onTextAreaComponentChange}
                    getItemStyle={this.props.getItemStyle}
                    item_bank_attribute={this.props.item_bank_attribute}
                    deleteItem={this.props.deleteItem}
                    index={this.props.index}
                    currentLanguage={this.props.currentLanguage}
                    draggableSwitch={() => this.props.draggableSwitch()}
                    triggerCollapse={this.props.triggerCollapse}
                  />
                )}
                {this.props.bundleAssociatedItemData && (
                  <ItemBankBundleItemContentComponent item={this.props.bundleAssociatedItemData} />
                )}
                {this.props.bundleAssociatedBundleData && (
                  <ItemBankBundleBundleContentComponent
                    bundle={this.props.bundleAssociatedBundleData}
                  />
                )}
                {this.props.bundleBundlesRuleData && (
                  <ItemBankBundleBundleContentBundleRuleComponent
                    bundleRule={this.props.bundleBundlesRuleData}
                    index={this.props.index}
                    disabledBundleContentBundleRuleContentDroppable={
                      this.props.disabledBundleContentBundleRuleContentDroppable
                    }
                  />
                )}
                {this.props.bundleBundlesRuleContentData && (
                  <ItemBankBundleBundleContentBundleRuleContentComponent
                    ruleData={this.props.bundleBundlesRuleContentData}
                  />
                )}
              </Col>
              {this.props.isDeletable && (
                <Col
                  xl={columnSizes.thirdColumn.xl}
                  lg={columnSizes.thirdColumn.lg}
                  md={columnSizes.thirdColumn.md}
                  sm={columnSizes.thirdColumn.sm}
                  xs={columnSizes.thirdColumn.xs}
                  className={`d-flex justify-content-center ${this.props.customDeleteButtonClass}`}
                  draggable={false}
                >
                  <CustomButton
                    dataTip=""
                    // dataFor={`item-bank-item-duplicate-button-${i}`}
                    label={<FontAwesomeIcon icon={faTimes} />}
                    // ariaLabel={LOCALIZE.formatString(
                    //   LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                    //     .items.table.duplicateActionAccessibilityLabel,
                    //   response.results[i].system_id
                    // )}
                    action={this.props.deleteItem}
                    customStyle={this.styles.deleteButton}
                    className="deleteButton"
                  />
                </Col>
              )}
            </Row>
          </Container>
        )}
      </Draggable>
    );
  }
}
