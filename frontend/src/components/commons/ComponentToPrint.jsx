import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

const styles = {
  mainContainer: {
    margin: "48px 24px",
    overflow: "hidden"
  }
};

class ComponentToPrint extends Component {
  static propTypes = {
    customRef: PropTypes.object.isRequired,
    content: PropTypes.object.isRequired
  };

  render() {
    return (
      <div ref={this.props.customRef} style={styles.mainContainer}>
        {this.props.content}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ComponentToPrint);
