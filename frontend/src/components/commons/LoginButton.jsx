import React, { Component } from "react";
import PropTypes from "prop-types";
import withRouter from "../withRouter";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { PATH } from "./Constants";
import CustomButton from "./CustomButton";
import THEME from "./CustomButtonTheme";
import { history } from "../../store-index";

const styles = {
  navlink: {
    minWidth: 50
  },
  paddingNavButton: {
    padding: "0 5px 0 5px"
  }
};

class LoginButton extends Component {
  static propTypes = {
    // Props from Redux
    currentLanguage: PropTypes.string
  };

  handleClick = () => {
    history.push(PATH.login);
  };

  render() {
    return (
      <div>
        {!this.props.authenticated && (
          <div style={styles.paddingNavButton}>
            <CustomButton
              label={LOCALIZE.commons.login}
              action={this.handleClick}
              customStyle={styles.navlink}
              buttonTheme={THEME.PRIMARY}
            />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    authenticated: state.login.authenticated,
    currentLanguage: state.localize.language
  };
};

export default connect(mapStateToProps, null)(withRouter(LoginButton));
