export const MASKS = {
  phoneNumber: "(***) ***-****"
};

export const MASK_FORMAT_CHARS = {
  phoneNumber: { "*": "[0-9]" }
};

// formatting masked phone number
const formatMaskedPhoneNumber = maskedPhoneNumber => {
  // removing all characters related to the mask
  // basically only returning numbers
  let formattedPhoneNumber = maskedPhoneNumber;
  formattedPhoneNumber = formattedPhoneNumber.replaceAll("(", "");
  formattedPhoneNumber = formattedPhoneNumber.replaceAll(")", "");
  formattedPhoneNumber = formattedPhoneNumber.replaceAll("-", "");
  formattedPhoneNumber = formattedPhoneNumber.replaceAll(" ", "");
  formattedPhoneNumber = formattedPhoneNumber.replaceAll("_", "");
  return formattedPhoneNumber;
};

export default formatMaskedPhoneNumber;
