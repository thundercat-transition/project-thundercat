import React, { Component } from "react";
import PropTypes from "prop-types";
import { setLanguage, LANGUAGES } from "../../modules/LocalizeRedux";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import CustomButton from "./CustomButton";
import THEME from "./CustomButtonTheme";

// Strings won't get translated because they will appear
// in the opposite language of the one selected.
const strings = {
  en: "English",
  fr: "Français"
};

const styles = {
  paddingNavButton: {
    padding: "0 5px 0 5px"
  }
};

class Translation extends Component {
  static propTypes = {
    isTestInvalidated: PropTypes.bool,
    // Props from Redux
    setLanguage: PropTypes.func,
    currentLanguage: PropTypes.string
  };

  toggleLanguage = () => {
    if (this.props.currentLanguage === LANGUAGES.english) {
      this.props.setLanguage(LANGUAGES.french);
    } else {
      this.props.setLanguage(LANGUAGES.english);
    }
    window.location.reload();
  };

  render() {
    const languageString =
      this.props.currentLanguage === LANGUAGES.english ? strings.fr : strings.en;
    const htmlLang =
      this.props.currentLanguage === LANGUAGES.english ? LANGUAGES.french : LANGUAGES.english;
    return (
      <div style={styles.paddingNavButton}>
        <CustomButton
          label={languageString}
          action={this.toggleLanguage}
          buttonTheme={
            this.props.isTestActive ||
            this.props.isTestPreTestOrTransition ||
            this.props.isTestInvalidated
              ? THEME.PRIMARY_IN_TEST
              : THEME.SECONDARY
          }
          lang={htmlLang}
        />
      </div>
    );
  }
}

export { LANGUAGES };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    isTestActive: state.testStatus.isTestActive,
    isTestPreTestOrTransition: state.testStatus.isTestPreTestOrTransition
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLanguage
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Translation);
