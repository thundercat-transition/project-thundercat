import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Alert, Col, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faExclamationCircle,
  faExclamationTriangle,
  faInfoCircle,
  faCheckCircle,
  faStopCircle
} from "@fortawesome/free-solid-svg-icons";
import ReactMarkdown from "react-markdown";
import rehypeRaw from "rehype-raw";

export const ALERT_TYPE = {
  danger: "danger",
  success: "success",
  info: "info",
  warning: "warning",
  light: "light"
};

export const styles = {
  mainContainer: {
    wordBreak: "break-word"
  },
  iconContainer: {
    display: "table-cell",
    padding: "0 24px 0 12px",
    verticalAlign: "middle"
  },
  icon: {
    verticalAlign: "middle",
    transform: "scale(1.2)"
  },
  messageContainer: {
    display: "table-cell"
  }
};

class AlertMessage extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      alertType: PropTypes.string.isRequired,
      message: PropTypes.object.isRequired,
      messageStyle: PropTypes.object,
      // set this props to false when you want to put html like this: message={<><p>Wording here...</p></>}
      usesMarkdownOrHtmlTags: PropTypes.bool,
      customIcon: PropTypes.symbol,
      iconStyle: PropTypes.object
    };

    AlertMessage.defaultProps = {
      usesMarkdownOrHtmlTags: true
    };
  }

  render() {
    const { alertType, message, messageStyle, usesMarkdownOrHtmlTags, customIcon, iconStyle } =
      this.props;

    let iconBasedOnAlertType = faExclamationCircle;
    let iconStyleBasedOnAlertType = { color: "#00565e" };
    let messageStyleBasedOnAlertType = { color: "00565e" };

    if (alertType === ALERT_TYPE.danger) {
      iconBasedOnAlertType = faStopCircle;
      iconStyleBasedOnAlertType = { color: "#AE070A" };
      messageStyleBasedOnAlertType = { color: "#923534" };
    } else if (alertType === ALERT_TYPE.success) {
      iconBasedOnAlertType = faCheckCircle;
      iconStyleBasedOnAlertType = { color: "#278400" };
      messageStyleBasedOnAlertType = { color: "#1C5E00" };
    } else if (alertType === ALERT_TYPE.info) {
      iconBasedOnAlertType = faInfoCircle;
      iconStyleBasedOnAlertType = { color: "#0099A8" };
      messageStyleBasedOnAlertType = { color: "#024F58" };
    } else if (alertType === ALERT_TYPE.warning) {
      iconBasedOnAlertType = faExclamationTriangle;
      iconStyleBasedOnAlertType = { color: "#FBC710" };
      messageStyleBasedOnAlertType = { color: "#8C6604" };
    } else if (alertType === ALERT_TYPE.light) {
      iconBasedOnAlertType = faExclamationCircle;
      iconStyleBasedOnAlertType = { color: "#00565e" };
      messageStyleBasedOnAlertType = { color: "#00565e" };
    }

    const customIconFontSize = {
      // adding 6px to current selected font size of the icon
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 6}px`
    };

    return (
      <Row
        className="align-items-center justify-content-center"
        role="alert"
        style={styles.mainContainer}
      >
        <Col>
          <Alert key={alertType} variant={alertType}>
            <div style={{ ...messageStyleBasedOnAlertType, ...messageStyle }}>
              <span style={styles.iconContainer}>
                <FontAwesomeIcon
                  icon={customIcon || iconBasedOnAlertType}
                  style={{
                    ...customIconFontSize,
                    ...styles.icon,
                    ...iconStyleBasedOnAlertType,
                    ...iconStyle
                  }}
                />
              </span>
              <span style={styles.messageContainer}>
                {/* sometimes usesMarkdownOrHtmlTags is undefined on the first render even if the default props is set to true */}
                {usesMarkdownOrHtmlTags || typeof usesMarkdownOrHtmlTags === "undefined" ? (
                  <ReactMarkdown
                    rehypePlugins={[rehypeRaw]}
                    // eslint-disable-next-line react/no-children-prop
                    children={message}
                    // add className="notranslate" to 'p' tags to prevent Microsoft Edge from translating
                    components={{
                      p: ({ node, ...props }) => <p className="notranslate" {...props} />
                    }}
                  />
                ) : (
                  message
                )}
              </span>
            </div>
          </Alert>
        </Col>
      </Row>
    );
  }
}

export { AlertMessage as UnconnectedAlertMessage };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations
  };
};

export default connect(mapStateToProps, null)(AlertMessage);
