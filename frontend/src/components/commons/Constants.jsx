import LOCALIZE from "../../text_resources";

export const URL_PREFIX = {
  // all pages that is using this item bank prefix will have some special styling (in App.js) related to the height and the overflow of the page
  itemBank: "/item-bank/",
  aae: "/assessment-accommodations/"
};

const PATH = {
  splash: "/oec-cat",
  login: "/oec-cat/login",
  dashboard: "/oec-cat/my-tests",
  status: "/oec-cat/status",
  emibSampleTest: "/oec-cat/emib-sample",
  test: "/oec-cat/active",
  testBase: "/oec-cat/test",
  sampleTests: "/oec-cat/sample-tests/",
  instructions: "/oec-cat/instructions",
  overview: "/oec-cat/overview",
  submit: "/oec-cat/submit",
  quit: "/oec-cat/quit",
  testAdministration: "/oec-cat/test-sessions",
  profile: "/oec-cat/profile",
  incidentReport: "/oec-cat/incident-report",
  contactUs: "/oec-cat/contact-us",
  systemAdministration: "/oec-cat/system-administration",
  ppcAdministration: "/oec-cat/ppc-administration",
  testCenterManager: "/oec-cat/test-center-manager",
  testCenterAccesses: "/oec-cat/test-center-accesses",
  testCenterManagement: "/oec-cat/test-center-management",
  scorerBase: "/oec-cat/scorer",
  scorerTest: "/oec-cat/test",
  uit: "/oec-cat/uit",
  testBuilder: "/oec-cat/test-builder",
  itemBankAccesses: "/oec-cat/item-bank-accesses",
  itemBankEditor: `/oec-cat${URL_PREFIX.itemBank}item-bank-editor`,
  itemEditor: `/oec-cat${URL_PREFIX.itemBank}item-editor`,
  bundleEditor: `/oec-cat${URL_PREFIX.itemBank}bundle-editor`,
  testBuilderTest: "/oec-cat/test-builder-test",
  resetPassword: "/oec-cat/reset-password",
  invalidateTest: "/oec-cat/invalidate-test",
  userLookUpDetails: "/oec-cat/selected-user-details",
  orderlessTestAdministratorDetails: "/oec-cat/selected-orderless-test-administrator-details",
  superuserDashboard: "/oec-cat/superuser-dashboard",
  inPersonTesting: "/oec-cat/in-person-testing",
  aae: `/oec-cat${URL_PREFIX.aae}dashboard`,
  aaeSelectedUserAccommodationFile: `/oec-cat${URL_PREFIX.aae}selected-user-accommodation-file`,
  consultationServices: "/oec-cat/consultation-services"
};

// this function will put alternate colors (grey and white rows) in the respsective table
/* parameters:
    - id (from the map function)
    - height (desired row height)
*/
const alternateColorsStyle = (id, height) => {
  const styles = {
    rowContainerBasicStyle: {
      width: "100%",
      height: `${height}px`,
      padding: "8px 0 8px 12px"
    },
    rowContainerDark: {
      backgroundColor: "#F3F3F3"
    },
    rowContainerLight: {
      backgroundColor: "white"
    },
    rowContainerBorder: {
      borderTop: "1px solid #CECECE"
    }
  };

  if (id === 0) {
    return { ...styles.rowContainerBasicStyle, ...styles.rowContainerLight };
  }
  if (id % 2 === 0) {
    return {
      ...styles.rowContainerBasicStyle,
      ...styles.rowContainerBorder,
      ...styles.rowContainerLight
    };
  }
  return {
    ...styles.rowContainerBasicStyle,
    ...styles.rowContainerBorder,
    ...styles.rowContainerDark
  };
};

const fontSizeDefinition = () => {
  return [
    { label: LOCALIZE.accommodations.defaultFonts.fontSize, value: "16px" },
    { label: "8px", value: "8px" },
    { label: "10px", value: "10px" },
    { label: "11px", value: "11px" },
    { label: "12px", value: "12px" },
    { label: "13px", value: "13px" },
    { label: "14px", value: "14px" },
    { label: "15px", value: "15px" },
    { label: "17px", value: "17px" },
    { label: "18px", value: "18px" },
    { label: "19px", value: "19px" },
    { label: "20px", value: "20px" },
    { label: "22px", value: "22px" },
    { label: "24px", value: "24px" },
    { label: "25px", value: "25px" },
    { label: "26px", value: "26px" },
    { label: "28px", value: "28px" },
    { label: "30px", value: "30px" },
    { label: "36px", value: "36px" },
    { label: "40px", value: "40px" },
    { label: "48px", value: "48px" }
  ];
};

const fontFamilyDefinition = () => {
  return [
    { label: LOCALIZE.accommodations.defaultFonts.fontFamily, value: "Nunito Sans" },
    { label: "APHont", value: "APHont" },
    { label: "Arial", value: "Arial" },
    { label: "Arial Black", value: "Arial Black" },
    { label: "Arial Narrow", value: "Arial Narrow" },
    { label: "Calibri", value: "Calibri" },
    { label: "Times New Roman", value: "Times New Roman" },
    { label: "Verdana", value: "Verdana" },
    { label: "OpenDyslexic", value: "OpenDyslexic" },
    { label: "Tahoma", value: "Tahoma" }
  ];
};

// copy of that file: ...\backend\static\reason_for_testing.py
export const REASON_FOR_TESTING_TYPE_CODENAME_CONST = {
  READING: "reading",
  WRITING: "writing",
  ORAL: "oral"
};

export const REASON_FOR_TESTING_CODENAME_CONST = {
  // Reading/Writing SLEs
  IMP_STAFFING: "imp_staffing",
  NON_IMP_STAFFING: "non_imp_staffing",
  FT_LANGUAGE_TRAINING_FROM_PSC: "ft_language_training_from_psc",
  PT_LANGUAGE_TRAINING_FROM_PSC: "pt_language_training_from_psc",
  FT_LANGUAGE_TRAINING_FROM_DEP_PROG: "ft_language_training_from_dep_prog",
  PT_LANGUAGE_TRAINING_FROM_DEP_PROG: "pt_language_training_from_dep_prog",
  FT_LANGUAGE_TRAINING_FROM_PRIVATE: "ft_language_training_from_private",
  PT_LANGUAGE_TRAINING_FROM_PRIVATE: "pt_language_training_from_private",
  RE_IDENTIFICATION: "re_identification",
  BILINGUALISM_BONUS: "bilingualism_bonus",
  RECORD_OR_OTHER_PURPOSES: "record_or_other_purposes",
  COVID_19_OR_ESSENTIAL_SERVICE: "covid_19_or_essential_service",
  STAFFING_DOTATION: "staffing_dotation",
  LANGUAGE_TRAINING: "language_training",
  OTHER: "other",
  // OLA SLEs
  IMP_STAFFING_OLA: "imp_staffing_ola",
  LANGUAGE_TRAINING_OLA: "language_training_ola",
  CREATION_OF_PARTIALLY_ASSESSED_POOL_OLA: "creation_of_partially_assessed_pool",
  NON_IMP_STAFFING_OLA: "non_imp_staffing_ola",
  RE_IDENTIFICATION_OLA: "re_identification_ola",
  RECORD_OR_OTHER_PURPOSES_OLA: "record_or_other_purposes_ola",
  BILINGUALISM_BONUS_OLA: "bilingualism_bonus_ola"
};

export const REASON_FOR_TESTING_PRIORITY_CODENAME_CONST = {
  HIGH: "high",
  MEDIUM: "medium",
  LOW: "low"
};
// =============================================================

// copy of that file: ...\backend\static\day_of_week.py
export const DAY_OF_WEEK_ID_CONST = {
  SUNDAY: 1,
  MONDAY: 2,
  TUESDAY: 3,
  WEDNESDAY: 4,
  THURSDAY: 5,
  FRIDAY: 6,
  SATURDAY: 7
};

// copy of that file: ...\backend\static\scorer_ola_test_session_skip_option.py
export const SCORER_OLA_TEST_SESSION_SKIP_OPTION = {
  CONFLICT_FAMILY: "conflict_family",
  TESTED_RECENTLY: "tested_recently"
};

export { PATH, alternateColorsStyle, fontSizeDefinition, fontFamilyDefinition };
