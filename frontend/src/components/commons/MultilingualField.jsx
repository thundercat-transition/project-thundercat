import React, { Component } from "react";
import PropTypes from "prop-types";
import { makeLabel, makeTextBoxField } from "../testBuilder/helpers";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { Row, Col } from "react-bootstrap";

// Field Types
export const FIELD_TYPES = {
  INPUT: 0,
  TEXT_AREA: 1
};

// CSS Styles
const styles = {
  translationsFields: {
    margin: "24px 0px"
  },
  rowStyle: {
    margin: "5px"
  },
  textFieldContainer: {
    marginBottom: "auto"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  textAreaInput: {
    width: "100%",
    height: 75,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block"
  }
};

const customColSizes = {
  xl: 6,
  lg: 6,
  md: 6,
  sm: 6
};

class MultilingualField extends Component {
  constructor(props, context) {
    super(props, context);

    // Provided Variables
    this.PropTypes = {
      // Type of field
      // See FIELD_TYPES above
      fieldType: PropTypes.number.isRequired,

      // Function used to modify each field (and to validate)
      // If a validation (that shows an error message) is being made, the result will be in the prop "fieldsValidity"
      handleChange: PropTypes.func.isRequired,

      // Localize path to find each field's label
      localizePath: PropTypes.object.isRequired,

      // Name used in the HTML for the ID
      fieldName: PropTypes.string,

      // Values in the format of:
      // {"en": [{text: "english value"}], "fr": [{text: "french value"}]}
      values: PropTypes.object.isRequired,

      // Validity in the format of:
      // {"en": true, "fr": true}
      fieldsValidity: PropTypes.object,

      // Index --> Used for a more precise HTML ID
      index: PropTypes.number,

      // Text Area Max Length
      maxLength: PropTypes.string
    };
  }

  render() {
    return (
      <div>
        <div style={styles.translationsFields}>
          {LOCALIZE.getAvailableLanguages().map(language => (
            <>
              <Row style={styles.rowStyle}>
                {makeLabel(
                  `${language}_name`,
                  this.props.localizePath,
                  `${this.props.fieldName}-${language}-name-${this.props.index}`,
                  {},
                  false
                )}
                <Col
                  xl={customColSizes.xl}
                  lg={customColSizes.lg}
                  md={customColSizes.md}
                  sm={customColSizes.sm}
                  style={styles.textFieldContainer}
                >
                  {this.props.fieldType === FIELD_TYPES.INPUT && (
                    <input
                      name={`${language}_name`}
                      id={`${this.props.fieldName}-${language}-name-${this.props.index}-input`}
                      className={
                        this.props.fieldsValidity[language] ? "valid-field" : "invalid-field"
                      }
                      aria-labelledby={`${this.props.fieldName}-${language}-name-${this.props.index} ${this.props.fieldName}-${language}-name-${this.props.index}-tooltip-for-accessibility invalid-${this.props.fieldName}-${language}-name-${this.props.index}-msg`}
                      aria-required={false}
                      aria-invalid={!this.props.fieldsValidity[language]}
                      style={styles.input}
                      type="text"
                      value={
                        typeof this.props.values[language] !== "undefined"
                          ? this.props.values[language][0].text
                          : ""
                      }
                      onChange={(event, action) => this.props.handleChange(event, action)}
                      disabled={false}
                    />
                  )}
                  {this.props.fieldType === FIELD_TYPES.TEXT_AREA && (
                    <textarea
                      name={`${language}_name`}
                      id={`${this.props.fieldName}-${language}-name-${this.props.index}-input`}
                      className={
                        this.props.fieldsValidity[language] ? "valid-field" : "invalid-field"
                      }
                      aria-labelledby={`${this.props.fieldName}-${language}-name-${this.props.index} ${this.props.fieldName}-${language}-name-${this.props.index}-tooltip-for-accessibility invalid-${this.props.fieldName}-${language}-name-${this.props.index}-input-msg`}
                      aria-required={false}
                      aria-invalid={!this.props.fieldsValidity[language]}
                      style={styles.textAreaInput}
                      type="textarea"
                      value={
                        typeof this.props.values[language] !== "undefined"
                          ? this.props.values[language][0].text
                          : ""
                      }
                      onChange={(event, action) => this.props.handleChange(event, action)}
                      disabled={false}
                      maxLength={this.props.maxLength}
                    />
                  )}
                </Col>
              </Row>
              {!this.props.fieldsValidity[language] && (
                <Row style={styles.rowStyle}>
                  <Col
                    xl={customColSizes.xl}
                    lg={customColSizes.lg}
                    md={customColSizes.md}
                    sm={customColSizes.sm}
                    style={styles.textFieldContainer}
                  ></Col>
                  <Col
                    xl={customColSizes.xl}
                    lg={customColSizes.lg}
                    md={customColSizes.md}
                    sm={customColSizes.sm}
                    style={styles.textFieldContainer}
                  >
                    <label
                      id={`invalid-${this.props.fieldName}-${language}-name-${this.props.index}-msg`}
                      htmlFor={`${this.props.fieldName}-${language}-name-${this.props.index}-input`}
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {`${language}_name` !== ""
                        ? this.props.localizePath[`${language}_name`].errorMessage
                        : this.props.localizePath.errorMessage}
                    </label>
                  </Col>
                </Row>
              )}
            </>
          ))}
        </div>
      </div>
    );
  }
}

// Default Values for Not Required Variables
MultilingualField.defaultProps = {
  fieldName: "multilingual-field",
  index: 0,
  fieldsValidity: { en: true, fr: true },
  maxLength: "500"
};

export { MultilingualField as UnconnectedMultilingualField };

const mapStateToProps = (state, ownProps) => {
  return {};
};

export default connect(mapStateToProps, null)(MultilingualField);
