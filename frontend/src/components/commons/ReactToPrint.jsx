import React, { useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { useReactToPrint } from "react-to-print";
import ComponentToPrint from "./ComponentToPrint";

const styles = {
  displayNone: {
    display: "none"
  }
};

function ReactToPrint(props) {
  // setting default props (working perfectly well, even though VS Code detects that this is not being used)
  const { contentRef = null, documentTitle = "", content = "" } = props;

  const handlePrint = useReactToPrint({
    documentTitle: documentTitle,
    contentRef: contentRef
  });

  // Equivalent of componentDidMount
  useEffect(() => {
    handlePrint();
  }, []);

  return (
    <div style={styles.displayNone}>
      <ComponentToPrint customRef={contentRef} content={content} />
    </div>
  );
}

export { ReactToPrint as UnconnectedReactToPrint };
const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ReactToPrint);
