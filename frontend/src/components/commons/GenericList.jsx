import React, { Component } from "react";
import PropTypes from "prop-types";

class GenericList extends Component {
  static propTypes = {
    isOrdered: PropTypes.bool.isRequired,
    listItems: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
    splitItemsOnNewline: PropTypes.bool.isRequired,
    listStyle: PropTypes.object,
    listItemStyle: PropTypes.object
  };

  render() {
    const { isOrdered, listItems, splitItemsOnNewline, listStyle, listItemStyle } = this.props;

    // split the items on newline if needed
    let items = [];
    if (splitItemsOnNewline) {
      items = listItems.split("\n");
    } else {
      items = listItems;
    }

    return (
      <div>
        {!isOrdered && (
          <ul style={listStyle}>
            {items.map((item, i) => (
              <li key={i} style={listItemStyle}>
                {item}
              </li>
            ))}
          </ul>
        )}
        {isOrdered && (
          <ol style={listStyle}>
            {items.map((item, i) => (
              <li key={i} style={listItemStyle}>
                {item}
              </li>
            ))}
          </ol>
        )}
      </div>
    );
  }
}

export default GenericList;
