/* eslint-disable no-nested-ternary */
import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faPlay } from "@fortawesome/free-solid-svg-icons";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import {
  updateNonSubmittedActiveAssignedTests,
  invalidateLockedAssignedTests,
  unassignReadyAndPreTestAssignedTests,
  unpauseAssignedTests,
  expireOldAccommodations
} from "../../modules/SuperUserRedux";

const styles = {
  viewButton: {
    minWidth: 150
  },
  bold: {
    fontWeight: "bold"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  }
};

class TestTasks extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  state = {
    rowsDefinition: {},
    currentlyLoading: false,
    showCeleryTaskExecutionPopup: false
  };

  componentDidMount = () => {
    this.populateTableRows();
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  runUpdateNonSubmittedActiveAssignedTests = () => {
    this.props.updateNonSubmittedActiveAssignedTests();
    this.setState({
      ...this.state,
      showCeleryTaskExecutionPopup: true
    });
  };

  runInvalidateLockedAssignedTests = () => {
    this.props.invalidateLockedAssignedTests();
    this.setState({
      ...this.state,
      showCeleryTaskExecutionPopup: true
    });
  };

  runUnassignReadyAndPreTestAssignedTests = () => {
    this.props.unassignReadyAndPreTestAssignedTests();
    this.setState({
      ...this.state,
      showCeleryTaskExecutionPopup: true
    });
  };

  runUnpauseAssignedTests = () => {
    this.props.unpauseAssignedTests();
    this.setState({
      ...this.state,
      showCeleryTaskExecutionPopup: true
    });
  };

  runExpireOldAccommodations = () => {
    this.props.expireOldAccommodations();
    this.setState({
      ...this.state,
      showCeleryTaskExecutionPopup: true
    });
  };

  closeDialog = () => {
    this.setState({
      ...this.state,
      showCeleryTaskExecutionPopup: false
    });
  };

  // populating table rows for the GenericTable component
  populateTableRows = () => {
    // initializing needed object and array for rowsDefinition state (needed for GenericTable component) <FontAwesomeIcon icon={faPlayCircle} />
    let rowsDefinition = {};
    const data = [];

    data.push({
      column_1:
        LOCALIZE.superUserDashboard.celeryTasks.testTab.updateNonSubmittedActiveAssignedTests,
      column_2: (
        <CustomButton
          label={
            <>
              <FontAwesomeIcon icon={faPlay} />
              <span style={styles.buttonLabel} className="notranslate">
                {LOCALIZE.superUserDashboard.celeryTasks.table.runButton}
              </span>
            </>
          }
          action={this.runUpdateNonSubmittedActiveAssignedTests}
          customStyle={styles.viewButton}
          buttonTheme={THEME.SECONDARY}
        ></CustomButton>
      )
    });

    data.push({
      column_1: LOCALIZE.superUserDashboard.celeryTasks.testTab.invalidateLockedAssignedTests,
      column_2: (
        <CustomButton
          label={
            <>
              <FontAwesomeIcon icon={faPlay} />
              <span style={styles.buttonLabel} className="notranslate">
                {LOCALIZE.superUserDashboard.celeryTasks.table.runButton}
              </span>
            </>
          }
          action={this.runInvalidateLockedAssignedTests}
          customStyle={styles.viewButton}
          buttonTheme={THEME.SECONDARY}
        ></CustomButton>
      )
    });

    data.push({
      column_1:
        LOCALIZE.superUserDashboard.celeryTasks.testTab.unassignReadyAndPreTestAssignedTests,
      column_2: (
        <CustomButton
          label={
            <>
              <FontAwesomeIcon icon={faPlay} />
              <span style={styles.buttonLabel} className="notranslate">
                {LOCALIZE.superUserDashboard.celeryTasks.table.runButton}
              </span>
            </>
          }
          action={this.runUnassignReadyAndPreTestAssignedTests}
          customStyle={styles.viewButton}
          buttonTheme={THEME.SECONDARY}
        ></CustomButton>
      )
    });

    data.push({
      column_1: LOCALIZE.superUserDashboard.celeryTasks.testTab.unpauseAssignedTests,
      column_2: (
        <CustomButton
          label={
            <>
              <FontAwesomeIcon icon={faPlay} />
              <span style={styles.buttonLabel} className="notranslate">
                {LOCALIZE.superUserDashboard.celeryTasks.table.runButton}
              </span>
            </>
          }
          action={this.runUnpauseAssignedTests}
          customStyle={styles.viewButton}
          buttonTheme={THEME.SECONDARY}
        ></CustomButton>
      )
    });

    data.push({
      column_1: LOCALIZE.superUserDashboard.celeryTasks.testTab.expireOldAccommodations,
      column_2: (
        <CustomButton
          label={
            <>
              <FontAwesomeIcon icon={faPlay} />
              <span style={styles.buttonLabel} className="notranslate">
                {LOCALIZE.superUserDashboard.celeryTasks.table.runButton}
              </span>
            </>
          }
          action={this.runExpireOldAccommodations}
          customStyle={styles.viewButton}
          buttonTheme={THEME.SECONDARY}
        ></CustomButton>
      )
    });

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    this.setState({ rowsDefinition: rowsDefinition });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.superUserDashboard.celeryTasks.table.taskHeader,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.superUserDashboard.celeryTasks.table.actionHeader,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    return (
      <div>
        <div>
          <div>
            <GenericTable
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={LOCALIZE.superUserDashboard.celeryTasks.table.emptyTableMsg}
              tableWithButtons={true}
            />
          </div>
        </div>

        <PopupBox
          show={this.state.showCeleryTaskExecutionPopup}
          title={"Celery Task Executed"}
          handleClose={() => {}}
          size="lg"
          description="Check the docker logs to see the task execution"
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonTitle={"Close"}
          leftButtonLabel={"Close"}
          leftButtonAction={this.closeDialog}
        ></PopupBox>
      </div>
    );
  }
}

export { TestTasks as unconnectedActiveProcesses };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateNonSubmittedActiveAssignedTests,
      invalidateLockedAssignedTests,
      unassignReadyAndPreTestAssignedTests,
      unpauseAssignedTests,
      expireOldAccommodations
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestTasks);
