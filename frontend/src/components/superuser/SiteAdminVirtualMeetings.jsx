/* eslint-disable no-nested-ternary */
import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import Switch from "react-switch";
import {
  getSiteAdminSettings,
  getEnableWebex,
  getEnableMSTeamsAPI,
  setEnableMSTeamsAPI,
  setEnableWebex,
  setEnableMSTAppID,
  setEnableMSTTenantID,
  setEnableMSTSecret
} from "../../modules/SiteAdminSettingsRedux";
import SiteAdminSettingType from "./Constants";
import {
  validateTeamsID,
  validateTeamsName,
  validateTeamsSecret
} from "../../helpers/regexValidator";
import { Row, Col } from "react-bootstrap";

const styles = {
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  }
};

class SiteAdminFeatures extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  state = {
    rowsDefinition: {},
    isMSTeamsEnabled: false,
    isAppIDEnabled: false,
    isTenantIDEnabled: false,
    isSecretEnabled: false,
    isWebexEnabled: false,
    isAppIDValid: true,
    isTenantIDValid: true,
    isSecretValid: true,
    isWebexValid: true,
    webexBotToken: "",
    iswebexBotTokenValid: true,
    mstAppId: "",
    mstTenantID: "",
    mstSecret: "",
    tableResponse: []
  };

  componentDidMount = () => {
    this.populateTableRows();
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  // get the admin settings from the database ansd build the table
  populateTableRows = () => {
    this.props.getSiteAdminSettings().then(response => {
      this.buildTable(
        response,
        this.state.webexBotToken,
        this.state.isWebexValid,
        this.state.mstAppId,
        this.state.mstTenantID,
        this.state.mstSecret,
        this.state.isAppIDValid,
        this.state.isTenantIDValid,
        this.state.isSecretValid
      );
    });
  };

  buildMSTeamsRow = (id, label, value, onChange, isValid) => {
    return (
      <Row>
        <Col xl={12} lg={12} md={12} sm={12}>
          <label htmlFor={id}>{label}</label>
        </Col>
        <Col xl={12} lg={12} md={12} sm={12}>
          <input
            className="valid-field"
            type="text"
            id={id}
            style={{ ...styles.inputs }}
            onChange={onChange}
            value={value}
          />
          {!isValid && (
            <label
              id={`${id}-error`}
              htmlFor={id}
              style={styles.errorMessage}
              className="notranslate"
            >
              {LOCALIZE.superUserDashboard.settingsTab.invalidKey}
            </label>
          )}
        </Col>
      </Row>
    );
  };

  // render the Generic Table without necessarily calling the backend
  buildTable = (
    response,
    webexBotToken,
    isWebexValid,
    mstAppId,
    mstTenantID,
    mstSecret,
    isAppIDValid,
    isTenantIDValid,
    isSecretValid
  ) => {
    // initializing needed object and array for rowsDefinition state (needed for GenericTable component)
    const data = [];
    let rowsDefinition = {};
    let { isWebexEnabled, isMSTeamsEnabled, isAppIDEnabled, isTenantIDEnabled, isSecretEnabled } =
      this.state;
    for (let i = 0; i < response.length; i++) {
      const setting = response[i];
      let func = "";
      // This is normally just a label, but the API key will need an entry fiedl
      let col_1 = setting[`${this.props.currentLanguage}_description`];
      let display = true;
      switch (setting.codename) {
        case SiteAdminSettingType.ENABLE_WEBEX: // TODO change all login lock
          func = this.toggleWebex;
          isWebexEnabled = setting.is_active;
          break;
        case SiteAdminSettingType.ENABLE_MS_TEAMS_API:
          func = this.toggleMSTeams;
          isMSTeamsEnabled = setting.is_active;
          break;
        case SiteAdminSettingType.MS_TEAMS_API_APP_ID:
          func = this.toggleMSTAppID;
          isAppIDEnabled = setting.is_active;
          col_1 = this.buildMSTeamsRow(
            "mstAppID",
            col_1,
            mstAppId,
            this.handleMstAppIdChange,
            isAppIDValid
          );
          break;
        case SiteAdminSettingType.MS_TEAMS_API_TENANT_ID:
          func = this.toggleMSTTenantID;
          isTenantIDEnabled = setting.is_active;
          col_1 = this.buildMSTeamsRow(
            "mstTenantID",
            col_1,
            mstTenantID,
            this.handleMstTenantIDChange,
            isTenantIDValid
          );
          break;
        case SiteAdminSettingType.MS_TEAMS_API_SECRET:
          func = this.toggleMSTSecret;
          isSecretEnabled = setting.is_active;
          col_1 = this.buildMSTeamsRow(
            "mstSecret",
            col_1,
            mstSecret,
            this.handleMstSecretChange,
            isSecretValid
          );
          break;
        default:
          display = false;
          break;
      }
      if (display) {
        data.push({
          column_1: col_1,
          column_2: <Switch onChange={func} checked={setting.is_active} />
        });
      }
    }
    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    this.setState({
      rowsDefinition: rowsDefinition,
      isWebexEnabled: isWebexEnabled,
      isMSTeamsEnabled: isMSTeamsEnabled,
      isAppIDEnabled: isAppIDEnabled,
      isTenantIDEnabled: isTenantIDEnabled,
      isSecretEnabled: isSecretEnabled,
      tableResponse: response,
      webexBotToken: webexBotToken,
      mstAppId: mstAppId,
      mstTenantID: mstTenantID,
      mstSecret: mstSecret,
      isAppIDValid: isAppIDValid,
      isTenantIDValid: isTenantIDValid,
      isSecretValid: isSecretValid,
      isWebexValid: isWebexValid
    });
  };

  toggleMSTeams = _ => {
    const { isMSTeamsEnabled, isAppIDEnabled, isTenantIDEnabled, isSecretEnabled } = this.state;
    if (isAppIDEnabled && isTenantIDEnabled && isSecretEnabled) {
      // TODO (mcherry) check the other flags
      this.props.setEnableMSTeamsAPI(!isMSTeamsEnabled).then(_ => {
        this.setState({ isMSTeamsEnabled: !isMSTeamsEnabled });
        this.populateTableRows();
      });
    } else {
      this.setState({ isMSTeamsEnabled: false });
    }
  };

  toggleMSTAppID = _ => {
    const { mstAppId } = this.state;
    if (!this.state.isAppIDEnabled === false) {
      this.setState({ mstAppId: "", isMSTeamsEnabled: false });
    } else {
      // validate the key format; if it fails then do not update the setting
      if (!validateTeamsID(mstAppId)) {
        this.buildTable(
          this.state.tableResponse,
          this.state.webexBotToken,
          this.state.isWebexValid,
          mstAppId,
          this.state.mstTenantID,
          this.state.mstSecret,
          false,
          this.state.isTenantIDValid,
          this.state.isSecretValid
        );
        return;
      }
    }
    // if the key is valid or the setting is being turned off
    this.props.setEnableMSTAppID(!this.state.isAppIDEnabled, mstAppId).then(response => {
      switch (response.status) {
        case 200:
          this.populateTableRows();
          break;
        case 400:
          throw new Error("Something went wrong when saving Teams App ID setting: 400");
        case 404:
          throw new Error("Something went wrong when saving Teams App ID setting: 404");
        default:
          throw new Error("Something went wrong when saving Teams App ID setting");
      }
    });
  };

  toggleMSTTenantID = _ => {
    const { mstTenantID } = this.state;
    if (!this.state.isTenantIDEnabled === false) {
      this.setState({ mstTenantID: "", isMSTeamsEnabled: false });
    } else {
      // validate the key format; if it fails then do not update the setting
      if (!validateTeamsID(mstTenantID)) {
        this.buildTable(
          this.state.tableResponse,
          this.state.webexBotToken,
          this.state.isWebexValid,
          this.state.mstAppId,
          mstTenantID,
          this.state.mstSecret,
          this.state.isAppIDValid,
          false,
          this.state.isSecretValid
        );
        return;
      }
    }
    // if the key is valid or the setting is being turned off
    this.props.setEnableMSTTenantID(!this.state.isTenantIDEnabled, mstTenantID).then(response => {
      switch (response.status) {
        case 200:
          this.populateTableRows();
          break;
        case 400:
          throw new Error("Something went wrong when saving Teams Tenant ID setting: 400");
        case 404:
          throw new Error("Something went wrong when saving Teams Tenant ID setting: 404");
        default:
          throw new Error("Something went wrong when saving Teams Tenant ID setting");
      }
    });
  };

  toggleMSTSecret = _ => {
    const { mstSecret } = this.state;
    if (!this.state.isSecretEnabled === false) {
      this.setState({ mstSecret: "", isMSTeamsEnabled: false });
    } else {
      // validate the key format; if it fails then do not update the setting
      if (!validateTeamsSecret(mstSecret)) {
        this.buildTable(
          this.state.tableResponse,
          this.state.webexBotToken,
          this.state.isWebexValid,
          this.state.mstAppId,
          this.state.mstTenantID,
          mstSecret,
          this.state.isAppIDValid,
          this.state.isTenantIDValid,
          false
        );
        return;
      }
    }
    // if the key is valid or the setting is being turned off
    this.props.setEnableMSTSecret(!this.state.isSecretEnabled, mstSecret).then(response => {
      switch (response.status) {
        case 200:
          this.populateTableRows();
          break;
        case 400:
          throw new Error("Something went wrong when saving Teams Secret setting: 400");
        case 404:
          throw new Error("Something went wrong when saving Teams Secret setting: 404");
        default:
          throw new Error("Something went wrong when saving Teams Secret setting");
      }
    });
  };

  toggleWebex = _ => {
    // TODO enable when MS Teams integration is done and webex is started; leave empty for now
    // validateWebex setEnableWebex
  };

  handleWebexBotTokenChange = event => {
    // build the table with the new value
    this.buildTable(
      this.state.tableResponse,
      event.target.value,
      true,
      this.state.mstAppId,
      this.state.mstTenantID,
      this.state.mstSecret,
      this.state.isAppIDValid,
      this.state.isTenantIDValid,
      this.state.isSecretValid
    );
  };

  handleMstAppIdChange = event => {
    // build the table with the new value
    this.buildTable(
      this.state.tableResponse,
      this.state.webexBotToken,
      this.state.isWebexValid,
      event.target.value,
      this.state.mstTenantID,
      this.state.mstSecret,
      true,
      this.state.isTenantIDValid,
      this.state.isSecretValid
    );
  };

  handleMstTenantIDChange = event => {
    // build the table with the new value
    this.buildTable(
      this.state.tableResponse,
      this.state.webexBotToken,
      this.state.isWebexValid,
      this.state.mstAppId,
      event.target.value,
      this.state.mstSecret,
      this.state.isAppIDValid,
      true,
      this.state.isSecretValid
    );
  };

  handleMstSecretChange = event => {
    // build the table with the new value
    this.buildTable(
      this.state.tableResponse,
      this.state.webexBotToken,
      this.state.isWebexValid,
      this.state.mstAppId,
      this.state.mstTenantID,
      event.target.value,
      this.state.isAppIDValid,
      this.state.isTenantIDValid,
      true
    );
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.superUserDashboard.settingsTab.settingHeader,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.superUserDashboard.settingsTab.actionHeader,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    return (
      <div>
        <div>
          <div>
            <GenericTable
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={LOCALIZE.superUserDashboard.settingsTab.emptyTableMsg}
              tableWithButtons={true}
            />
          </div>
        </div>
      </div>
    );
  }
}

export { SiteAdminFeatures as unconnectedActiveProcesses };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getSiteAdminSettings,
      getEnableWebex,
      getEnableMSTeamsAPI,
      setEnableMSTeamsAPI,
      setEnableWebex,
      setEnableMSTAppID,
      setEnableMSTTenantID,
      setEnableMSTSecret
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SiteAdminFeatures);
