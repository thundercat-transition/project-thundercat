// mirror of SiteAdminSettingType under ...\backend\static\site_admin_setting_type.py

const SiteAdminSettingType = {
  LOCK_LOGIN: "locklogin",
  ENABLE_QUALITY_OF_LIFE: "enablequalityoflife",
  ENABLE_GC_NOTIFY: "enablegcnotify",
  ENABLE_MS_TEAMS_API: "enablemsteamsapi",
  ENABLE_WEBEX: "enablewebex",
  MS_TEAMS_API_APP_ID: "msteamsapiappid",
  MS_TEAMS_API_TENANT_ID: "msteamsapitenantid",
  MS_TEAMS_API_SECRET: "msteamsapisecret"
};

export default SiteAdminSettingType;
