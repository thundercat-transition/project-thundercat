import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { isTokenStillValid } from "../../modules/LoginRedux";
import { bindActionCreators } from "redux";
import ContentContainer from "../commons/ContentContainer";
import { Helmet } from "react-helmet";
import SideNavigation from "../eMIB/SideNavigation";
import { setSuperUserSideNavState } from "../../modules/SuperUserRedux";
import CeleryTasks from "./CeleryTasks";
import SiteAdmin from "./SiteAdmin";

export const styles = {
  header: {
    marginBottom: 24
  },
  sectionContainerLabelDiv: {
    paddingLeft: 18,
    marginBottom: "-3px"
  },
  sectionContainerLabel: {
    padding: "4px 24px",
    marginBottom: 0,
    position: "relative"
  },
  tabStyleBorder: {
    borderBottom: "5px solid #00565e",
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    borderRadius: "4px 4px 0 0"
  },
  sectionContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    minHeight: 400,
    padding: "12px 0",
    marginBottom: 24
  },
  tabContainer: {
    zIndex: 1,
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent"
  },
  nav: {
    marginTop: 10,
    marginLeft: 10,
    width: 206,
    textAlign: "center"
  },
  body: {
    paddingLeft: 12
  },
  sideNavBodyContent: {
    marginTop: "-12px"
  },
  appPadding: {
    padding: "15px"
  }
};

class SuperuserDashboard extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // Props from Redux
    isTokenStillValid: PropTypes.func
  };

  state = {
    isLoaded: false,
    isEttaDashboard: false
  };

  componentDidMount = () => {
    this._isMounted = true;
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        this.setState({
          isLoaded: true
        });
      }
    });
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  // Returns array where each item indicates specifications related to How To Page including the title and the body
  getSuperuserSections = () => {
    return [
      {
        menuString: LOCALIZE.superUserDashboard.sideNav.celeryTab,
        body: <CeleryTasks />
      },
      {
        menuString: "Site Admin", // TODO in text_resources
        body: <SiteAdmin />
      },
      {
        menuString: "Sidenav Item", // TODO in text_resources
        body: "todo" // TODO make component
      },
      {
        menuString: "System Status", // TODO in text_resources
        body: "todo" // TODO make component
      }
    ];
  };

  render() {
    const specs = this.getSuperuserSections();
    return (
      <div className="app" style={styles.appPadding}>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title className="notranslate">Superuser dashboard</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <div
              id="user-welcome-message-div"
              style={styles.header}
              aria-labelledby="user-welcome-message"
              className="notranslate"
            >
              <h1 id="user-welcome-message" className="green-divider">
                {LOCALIZE.formatString(
                  LOCALIZE.profile.title,
                  this.props.firstName,
                  this.props.lastName
                )}
              </h1>
            </div>
            <div>
              <div style={styles.sectionContainerLabelDiv}>
                <div>
                  <label style={styles.sectionContainerLabel} className="notranslate">
                    {LOCALIZE.superUserDashboard.title}
                    <span style={styles.tabStyleBorder}></span>
                  </label>
                </div>
              </div>
              <div style={styles.sectionContainer}>
                <section aria-label={LOCALIZE.ariaLabel.sideNavigationSection}>
                  <SideNavigation
                    specs={specs}
                    startIndex={this.props.selectedSideNavItem}
                    loadOnClick={true}
                    displayNextPreviousButton={false}
                    isMain={true}
                    tabContainerStyle={styles.tabContainer}
                    tabContentStyle={styles.tabContent}
                    navStyle={styles.nav}
                    bodyContentCustomStyle={styles.sideNavBodyContent}
                    updateSelectedSideNavItem={this.props.setSuperUserSideNavState}
                  />
                </section>
              </div>
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { SuperuserDashboard as unconnectedSuperuserDashboard };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    selectedSideNavItem: state.superUser.selectedSideNavItem
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid,
      setSuperUserSideNavState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SuperuserDashboard);
