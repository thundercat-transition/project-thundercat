import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import UitTasks from "./UitTasks";
import PermissionTasks from "./PermissionTasks";
import TestTasks from "./TestTasks";

const styles = {
  mainContainer: {
    width: "100%"
  }
};

class CeleryTasks extends Component {
  render() {
    const TABS = [
      {
        key: "uit-tasks",
        tabName: LOCALIZE.superUserDashboard.celeryTasks.uitTab.tabTitle,
        body: <UitTasks />
      },
      {
        key: "permission-tasks",
        tabName: LOCALIZE.superUserDashboard.celeryTasks.permissionTab.tabTitle,
        body: <PermissionTasks />
      },
      {
        key: "test-tasks",
        tabName: LOCALIZE.superUserDashboard.celeryTasks.testTab.tabTitle,
        body: <TestTasks />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.superUserDashboard.celeryTasks.title}</h2>
          <p>{LOCALIZE.superUserDashboard.celeryTasks.description}</p>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs defaultActiveKey="uit-tasks" id="uat-tabs" style={styles.tabNavigation}>
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                      defaultActiveKey={"uit-tasks"}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CeleryTasks);
