import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import UitTasks from "./UitTasks";
import PermissionTasks from "./PermissionTasks";
import TestTasks from "./TestTasks";
import SiteAdminFeatures from "./SiteAdminFeatures";
import SiteAdminVirtualTeamsRooms from "./SiteAdminVirtualTeamsRooms";
import TwoFAFeatures from "./TwoFAFeatures";
import SiteAdminVirtualMeetings from "./SiteAdminVirtualMeetings";

const styles = {
  mainContainer: {
    width: "100%"
  }
};

class SiteAdmin extends Component {
  render() {
    const TABS = [
      {
        key: "features",
        tabName: LOCALIZE.superUserDashboard.siteAdmin.features.title,
        body: <SiteAdminFeatures />
      },
      {
        key: "2fa",
        tabName: LOCALIZE.superUserDashboard.siteAdmin.tfa.title,
        body: <TwoFAFeatures />
      },
      {
        key: "virtualMeeting",
        tabName: LOCALIZE.superUserDashboard.siteAdmin.virtualMeeting.title,
        body: <SiteAdminVirtualMeetings />
      },
      {
        key: "teamsMeetingRooms",
        tabName: LOCALIZE.superUserDashboard.siteAdmin.virtualMeetingRooms.title,
        body: <SiteAdminVirtualTeamsRooms />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.superUserDashboard.siteAdmin.title}</h2>
          <p>{LOCALIZE.superUserDashboard.siteAdmin.paragraph}</p>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey="features"
                id="two-fa-status-table"
                style={styles.tabNavigation}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                      defaultActiveKey={"features"}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SiteAdmin);
