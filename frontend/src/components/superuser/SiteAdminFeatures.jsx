/* eslint-disable no-nested-ternary */
import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import { handleExpiredUitProcesses, resetCompletedProfileFlag } from "../../modules/SuperUserRedux";
import Switch from "react-switch";
import {
  getSiteAdminSettings,
  setLoginLock,
  setQualityofLife,
  setEnableGCNotify,
  setQualityOfLife
} from "../../modules/SiteAdminSettingsRedux";
import SiteAdminSettingType from "./Constants";
import { validateKey } from "../../helpers/regexValidator";
import { Row, Col } from "react-bootstrap";

const styles = {
  viewButton: {
    minWidth: 150
  },
  bold: {
    fontWeight: "bold"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  }
};

class SiteAdminFeatures extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  state = {
    rowsDefinition: {},
    showCeleryTaskExecutionPopup: false,
    isLoginDisabled: false,
    isGCNotifyActive: false,
    enableQualityOfLifeMode: false,
    apiKey: "",
    tableResponse: []
  };

  componentDidMount = () => {
    this.populateTableRows();
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  toggleLogin = _ => {
    // TODO, uncomment when toggle login is implemented
    // this.props.setLoginLock(!this.state.isLoginDisabled).then(response => {
    //   switch (response.status) {
    //     case 200:
    //       this.populateTableRows();
    //       break;
    //     case 400:
    //       throw new Error("Something went wrong when saving GC Notify setting: 400");
    //     case 404:
    //       throw new Error("Something went wrong when saving GC Notify setting: 404");
    //     default:
    //       throw new Error("Something went wrong when saving GC Notify setting");
    //   }
    // });
  };

  toggleQoL = _ => {
    const newQoL = !this.state.enableQualityOfLifeMode;
    this.props.setQualityofLife(newQoL).then(response => {
      switch (response.status) {
        case 200:
          this.props.setQualityOfLife(newQoL);
          this.populateTableRows();
          break;
        case 400:
          throw new Error("Something went wrong when saving Quality of Life setting: 400");
        case 404:
          throw new Error("Something went wrong when saving Quality of Life setting: 404");
        default:
          throw new Error("Something went wrong when saving Quality of Life setting");
      }
    });
  };

  // function to track if GC notify setting is active or inactive
  toggleGCNotify = _ => {
    const { apiKey } = this.state;
    if (!this.state.isGCNotifyActive === false) {
      this.setState({ apiKey: "" });
    } else {
      // validate the key format; if it fails then do not update the setting
      if (!validateKey(apiKey)) {
        this.buildTable(this.state.tableResponse, apiKey, false);
        return;
      }
    }
    // if the key is valid or the setting is being turned off
    this.props.setEnableGCNotify(!this.state.isGCNotifyActive, apiKey).then(response => {
      switch (response.status) {
        case 200:
          this.populateTableRows();
          break;
        case 400:
          throw new Error("Something went wrong when saving GC Notify setting: 400");
        case 404:
          throw new Error("Something went wrong when saving GC Notify setting: 404");
        default:
          throw new Error("Something went wrong when saving GC Notify setting");
      }
    });
  };

  // rebuild the table as the apiKey entry changes so the changes are visible
  handleKeyChange = event => {
    // build the table with the new APIKey
    this.buildTable(this.state.tableResponse, event.target.value, true);
  };

  // get the admin settings from the database ansd build the table
  populateTableRows = () => {
    this.props.getSiteAdminSettings().then(response => {
      this.buildTable(response, this.state.apiKey, true);
    });
  };

  // render the Generic Table without necessarily calling the backend
  buildTable = (response, apiKey, isKeyValid) => {
    // initializing needed object and array for rowsDefinition state (needed for GenericTable component)
    const data = [];
    let rowsDefinition = {};
    let { isLoginDisabled, isGCNotifyActive, enableQualityOfLifeMode } = this.state;
    for (let i = 0; i < response.length; i++) {
      const setting = response[i];
      let func = "";
      // This is normally just a label, but the API key will need an entry fiedl
      let col_1 = setting[`${this.props.currentLanguage}_description`];
      let display = true;
      switch (setting.codename) {
        case SiteAdminSettingType.LOCK_LOGIN:
          func = this.toggleLogin;
          isLoginDisabled = setting.is_active;
          break;
        case SiteAdminSettingType.ENABLE_QUALITY_OF_LIFE:
          func = this.toggleQoL;
          enableQualityOfLifeMode = setting.is_active;
          break;
        case SiteAdminSettingType.ENABLE_GC_NOTIFY:
          func = this.toggleGCNotify;
          isGCNotifyActive = setting.is_active;
          col_1 = (
            <Row>
              <Col xl={12} lg={12} md={12} sm={12}>
                <label htmlFor={"apikey"}>{col_1}</label>
              </Col>
              <Col xl={12} lg={12} md={12} sm={12}>
                <input
                  className="valid-field"
                  type="text"
                  id="apikey"
                  style={{ ...styles.inputs }}
                  onChange={this.handleKeyChange}
                  value={apiKey}
                />
                {!isKeyValid && (
                  <label
                    id="apikey-error"
                    htmlFor="apikey"
                    style={styles.errorMessage}
                    className="notranslate"
                  >
                    {LOCALIZE.superUserDashboard.settingsTab.invalidKey}
                  </label>
                )}
              </Col>
            </Row>
          );
          break;
        default:
          display = false;
          break;
      }
      if (display) {
        data.push({
          column_1: col_1,
          column_2: <Switch onChange={func} checked={setting.is_active} />
        });
      }
    }
    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    this.setState({
      rowsDefinition: rowsDefinition,
      isLoginDisabled: isLoginDisabled,
      enableQualityOfLifeMode: enableQualityOfLifeMode,
      isGCNotifyActive: isGCNotifyActive,
      tableResponse: response,
      apiKey: apiKey
    });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.superUserDashboard.settingsTab.settingHeader,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.superUserDashboard.settingsTab.actionHeader,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    return (
      <div>
        <div>
          <div>
            <GenericTable
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={LOCALIZE.superUserDashboard.settingsTab.emptyTableMsg}
              tableWithButtons={true}
            />
          </div>
        </div>
      </div>
    );
  }
}

export { SiteAdminFeatures as unconnectedActiveProcesses };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      resetCompletedProfileFlag,
      handleExpiredUitProcesses,
      getSiteAdminSettings,
      setLoginLock,
      setQualityofLife,
      setEnableGCNotify,
      setQualityOfLife
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SiteAdminFeatures);
