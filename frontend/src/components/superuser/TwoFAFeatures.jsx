/* eslint-disable no-nested-ternary */
import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import { handleExpiredUitProcesses, resetCompletedProfileFlag } from "../../modules/SuperUserRedux";
import { get2FASettings, set2FA } from "../../modules/Login2FARedux";
import Switch from "react-switch";

const styles = {
  viewButton: {
    minWidth: 150
  },
  bold: {
    fontWeight: "bold"
  }
};

class TwoFAFeatures extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  state = {
    rowsDefinition: {},
    currentlyLoading: false,
    showCeleryTaskExecutionPopup: false,
    checked: []
  };

  componentDidMount = () => {
    this.populateTableRows();
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  onChange = (event, index, permission_id) => {
    const { checked } = this.state;
    this.props.set2FA(permission_id, !checked[index]).then(_ => {
      this.populateTableRows();
    });
  };

  // populating table rows for the GenericTable component
  populateTableRows = () => {
    // initializing needed object and array for rowsDefinition state (needed for GenericTable component) <FontAwesomeIcon icon={faPlayCircle} />
    let rowsDefinition = {};
    const data = [];
    const checked = [];
    this.props.get2FASettings().then(response => {
      const data = [];
      for (let i = 0; i < response.length; i++) {
        // format the data for the table
        const twofa = response[i];
        checked.push(twofa.is_2fa_active);
        data.push({
          column_1: response[i][`${this.props.currentLanguage}_name`],
          column_2: (
            <Switch
              onChange={event => this.onChange(event, i, twofa.permission_id)}
              checked={checked[i]}
            />
          )
        });
      }

      // updating rowsDefinition object with provided data and needed style
      rowsDefinition = {
        column_1_style: COMMON_STYLE.LEFT_TEXT,
        column_2_style: COMMON_STYLE.CENTERED_TEXT,
        data: data
      };

      this.setState({ rowsDefinition: rowsDefinition, checked: checked });
    });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.superUserDashboard.celeryTasks.table.featureHeader,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.superUserDashboard.celeryTasks.table.actionHeader,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    return (
      <div>
        <div>
          <div>
            <GenericTable
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={LOCALIZE.superUserDashboard.celeryTasks.table.emptyTableMsg}
              tableWithButtons={true}
            />
          </div>
        </div>
      </div>
    );
  }
}

export { TwoFAFeatures as unconnectedTwoFAFeatures };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      resetCompletedProfileFlag,
      handleExpiredUitProcesses,
      get2FASettings,
      set2FA
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TwoFAFeatures);
