import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import {
  addVirtualRoom,
  deleteVirtualRoom,
  getTeamsRooms
} from "../../modules/SiteAdminSettingsRedux";
import { Row, Col } from "react-bootstrap";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faTimes, faTrash, faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import PopupBox, { BUTTON_TYPE, BUTTON_STATE } from "../commons/PopupBox";
import Pagination from "../commons/Pagination";
import StyledTooltip, { TYPE, EFFECT } from "../authentication/StyledTooltip";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import { validateEmail } from "../../helpers/regexValidator";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { changePage } from "../../modules/VirtualTeamsRoomsRedux";

const styles = {
  tableContainer: {
    margin: 24
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  itemContainer: {
    width: "100%",
    margin: "12px 0"
  },
  allUnset: {
    all: "unset"
  },
  labelContainer: {
    verticalAlign: "middle"
  },
  label: {
    paddingRight: 12,
    margin: 0
  },
  inputContainer: {
    verticalAlign: "middle"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  }
};

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 7,
    xl: 7
  }
};

class SiteAdminVirtualTeamsRooms extends Component {
  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    resultsFound: 0,
    numberOfPages: 1,
    showConfirmDeletePopup: false,
    showAddRoomPopup: false,
    selectedVirtualRoomName: "",
    addRoomName: "",
    addRoomUserId: "",
    nameAlreadyExists: false,
    isValidName: false,
    isValidUserId: false,
    isValidForm: false
  };

  componentDidMount = () => {
    this.getVirtualMeetingRooms();
  };

  componentDidUpdate = prevProps => {
    // if the page gets updated
    if (prevProps.page !== this.props.page) {
      this.getVirtualMeetingRooms();
    }
  };

  viewDeleteRoomPopup = name => {
    this.setState({
      showConfirmDeletePopup: true,
      selectedVirtualRoomName: name
    });
  };

  closeDeleteRoomPopup = () => {
    this.setState({
      showConfirmDeletePopup: false,
      selectedVirtualRoomName: ""
    });
  };

  confirmDeleteRoom = () => {
    this.props.deleteVirtualRoom(this.state.selectedVirtualRoomName).then(response => {
      this.setState({
        showConfirmDeletePopup: false,
        selectedVirtualRoomName: ""
      });
      this.getVirtualMeetingRooms();
    });
  };

  viewAddRoomPopup = () => {
    this.setState({
      showAddRoomPopup: true
    });
  };

  closeAddRoomPopup = () => {
    this.setState({
      showAddRoomPopup: false
    });
  };

  confirmAddRoom = () => {
    // if this is somehow triggered but invalid, then return
    if (!this.state.isValidForm) {
      return;
    }
    this.props.addVirtualRoom(this.state.addRoomName, this.state.addRoomUserId).then(response => {
      // TODO handle name already in use. Next PR
      this.setState({
        showAddRoomPopup: false,
        addRoomName: "",
        addRoomUserId: ""
      });
      this.getVirtualMeetingRooms();
    });
  };

  populateColumnThree = (i, response) => {
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`teams-virtual-room-${i}`}
          place="top"
          variant={TYPE.light}
          effect={EFFECT.solid}
          openOnClick={false}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                buttonId={`teams-virtual-room-id-${i}`}
                dataTip=""
                dataFor={`teams-virtual-room-${i}`}
                label={<FontAwesomeIcon icon={faTrash} />}
                action={() => {
                  this.viewDeleteRoomPopup(response.body.results[i].name);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.deleteRoomButtonLabel,
                  response.body.results[i].name
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>{LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.deleteRoomButtonTooltip}</p>
            </div>
          }
        />
      </div>
    );
  };

  updateAddRoomName = event => {
    const newAddRoomName = event.target.value;
    // allow emails
    if (newAddRoomName === "" || validateEmail(newAddRoomName)) {
      this.setState(
        { addRoomName: newAddRoomName, isValidName: true, nameAlreadyExists: false },
        () => {
          this.validateNewRoomForm();
        }
      );
    } else {
      this.setState({
        addRoomName: newAddRoomName,
        isValidName: false,
        nameAlreadyExists: false
      });
    }
  };

  updateAddRoomUserID = event => {
    const newAddRoomUserId = event.target.value;
    // allow 8 chars-4 chars-4 chars-4 chars-12 chars
    const regexExpression =
      /^([a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12})$/;
    if (newAddRoomUserId === "" || regexExpression.test(newAddRoomUserId)) {
      this.setState({ addRoomUserId: newAddRoomUserId, isValidUserId: true }, () => {
        this.validateNewRoomForm();
      });
    } else {
      this.setState({ addRoomUserId: newAddRoomUserId, isValidUserId: false });
    }
  };

  validateNewRoomForm = () => {
    // initializing needed variables
    let isValidForm = false;

    // everything is valid
    if (this.state.isValidName && this.state.isValidUserId) {
      isValidForm = true;
    }

    this.setState({ isValidForm: isValidForm });
  };

  getVirtualMeetingRooms = () => {
    this.setState({ currentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];
      this.props.getTeamsRooms(this.props.page, this.props.pageSize).then(response => {
        for (let i = 0; i < response.body.results.length; i++) {
          // populating data object with provided data
          data.push({
            column_1: response.body.results[i].name,
            column_2: response.body.results[i].user_id,
            column_3: this.populateColumnThree(i, response)
          });
        }
        // updating rowsDefinition object with provided data and needed style
        rowsDefinition = {
          column_1_style: COMMON_STYLE.LEFT_TEXT,
          column_2_style: COMMON_STYLE.LEFT_TEXT,
          column_3_style: COMMON_STYLE.CENTERED_TEXT,
          data: data
        };
        // saving results in state
        this.setState({
          rowsDefinition: rowsDefinition,
          currentlyLoading: false,
          numberOfPages: Math.ceil(parseInt(response.body.count) / this.props.pageSize)
        });
      });
    });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.table.column1,
        style: { ...{ width: "35%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.table.column2,
        style: { ...{ width: "50%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.table.column3,
        style: { width: "15%" }
      }
    ];
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    const hasNameError =
      (!this.state.isValidName || this.state.nameAlreadyExists) && this.state.addRoomName !== "";
    const hasUserIdError = !this.state.isValidUserId && this.state.addRoomUserId !== "";

    return (
      <div>
        <h2>{LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.title}</h2>
        <div style={styles.tableContainer}>
          <div>
            <CustomButton
              label={
                <>
                  <FontAwesomeIcon icon={faPlusCircle} />
                  <span id="new-virtual-room-button" style={styles.buttonLabel}>
                    {LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.addNewRoomButton}
                  </span>
                </>
              }
              action={() => {
                this.viewAddRoomPopup();
              }}
              buttonTheme={THEME.PRIMARY}
            />
            <br />
            <br />
            <GenericTable
              classnamePrefix="view-virtual-rooms-definition"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.table.noData}
              currentlyLoading={this.state.currentlyLoading}
            />
            <Pagination
              paginationContainerId={"select-virtual-rooms-all-tests-pagination-pages-link"}
              pageCount={this.state.numberOfPages}
              paginationPage={this.props.page}
              updatePaginationPageState={this.props.changePage}
              firstTableRowId={"teams-virtual-room-id-0"}
            />
          </div>
        </div>
        <PopupBox
          show={this.state.showAddRoomPopup}
          handleClose={() => {}}
          shouldCloseOnEsc={true}
          title={LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.addPopup.title}
          description={
            <div>
              <p style={styles.description}>
                {LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.addPopup.desc}
              </p>
              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label id="add-room-popup-name-title" htmlFor="add-room-popup-name">
                      {LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.addPopup.name}
                    </label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="add-room-popup-name"
                    className={!hasNameError ? "valid-field" : "invalid-field"}
                    aria-labelledby="add-room-popup-name-title add-room-popup-name-error"
                    aria-required={true}
                    aria-invalid={hasNameError}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.addRoomName}
                    onChange={this.updateAddRoomName}
                  ></input>
                  {this.state.nameAlreadyExists && (
                    <label
                      id="add-room-popup-name-error"
                      htmlFor="add-room-popup-name"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {
                        LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.addPopup
                          .nameAlreadyExists
                      }
                    </label>
                  )}
                  {hasNameError && !this.state.isValidName && (
                    <label
                      id="add-room-popup-name-error"
                      htmlFor="add-room-popup-name"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.addPopup.nameInvalid}
                    </label>
                  )}
                </Col>
              </Row>

              <Row
                className="align-items-center justify-content-start"
                style={styles.itemContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.labelContainer}
                >
                  <label style={styles.label}>
                    <label id="add-room-popup-user-id-title" htmlFor="add-room-popup-user-id">
                      {LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.addPopup.userId}
                    </label>
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={styles.inputContainer}
                >
                  <input
                    id="add-room-popup-user-id"
                    className={!hasUserIdError ? "valid-field" : "invalid-field"}
                    aria-labelledby="add-room-popup-user-id-title add-room-popup-user-id-error"
                    aria-required={true}
                    aria-invalid={hasUserIdError}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.addRoomUserId}
                    onChange={this.updateAddRoomUserID}
                  ></input>
                  {hasUserIdError && !this.state.isValidUserId && (
                    <label
                      id="add-room-popup-user-id-error"
                      htmlFor="add-room-popup-user-id"
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.addPopup.userIdInvalid}
                    </label>
                  )}
                </Col>
              </Row>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.addButton}
          rightButtonIcon={faCheck}
          rightButtonAction={this.confirmAddRoom}
          rightButtonState={this.state.isValidForm ? BUTTON_STATE.enabled : BUTTON_STATE.disabled}
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeAddRoomPopup}
        />
        <PopupBox
          show={this.state.showConfirmDeletePopup}
          handleClose={() => {}}
          shouldCloseOnEsc={true}
          title={LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.deletePopup.title}
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.deletePopup.desc,
                      this.state.selectedVirtualRoomName
                    )}
                  </p>
                }
              />

              <p>{LOCALIZE.superUserDashboard.virtualMeetingRoomsTab.deletePopup.desc2}</p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonIcon={faTrash}
          rightButtonAction={this.confirmDeleteRoom}
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeDeleteRoomPopup}
        />
      </div>
    );
  }
}

export { SiteAdminVirtualTeamsRooms as unconnectedSiteAdminVirtualTeamsRooms };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    page: state.virtualTeamsRooms.page,
    pageSize: state.virtualTeamsRooms.pageSize
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addVirtualRoom,
      deleteVirtualRoom,
      getTeamsRooms,
      changePage
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SiteAdminVirtualTeamsRooms);
