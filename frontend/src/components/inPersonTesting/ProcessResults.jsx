import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import SearchBarWithDisplayOptions from "../commons/SearchBarWithDisplayOptions";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import Pagination from "../commons/Pagination";
import {
  updateProcessResultsPageState,
  updateProcessResultsPageSizeState,
  updateSearchProcessResultsStates,
  getFoundAssessmentProcessResults,
  getAllAssessmentProcessResults,
  getAssessmentProcessData,
  getAssessmentProcessResultsCandidates,
  getAssessmentProcessResultsCandidatesReport
} from "../../modules/AssesmentProcessRedux";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faDownload,
  faBinoculars,
  faTimes,
  faFileCircleExclamation,
  faUniversalAccess
} from "@fortawesome/free-solid-svg-icons";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../commons/PopupBox";
import parse from "html-react-parser";
import { CSVLink } from "react-csv";
import generateAssessmentProcessResultsReport from "../commons/reports/GenerateAssessmentProcessResultsReport";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import { TestSkillSleDescCodename } from "../testFactory/Constants";
import { getAssignedTestSpecsRelatedStatus } from "./ManageAssessmentProcessData";
import AccommodationsRequestDetailsPopup, {
  ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE
} from "../aae/AccommodationsRequestDetailsPopup";
import { getAccommodationFileDataForDetailsPopup } from "../../modules/AaeRedux";

const styles = {
  mainContainer: {
    width: "100%"
  },
  processResultsTable: {
    addBillingContact: {
      button: {
        background: "transparent",
        padding: 12,
        border: "none",
        color: " #00565e"
      },
      icon: {
        color: "#00565e",
        verticalAlign: "middle"
      },
      label: {
        marginLeft: 18,
        verticalAlign: "middle",
        fontWeight: "bold"
      },
      content: {
        verticalAlign: "middle",
        backgroundColor: "white"
      }
    }
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  tableContainer: {
    margin: "0 24px"
  },
  allUnset: {
    all: "unset"
  },
  noPaddingTop: {
    paddingTop: 0
  },
  tableIcon: {
    transform: "scale(1.2)"
  },
  accommodationTooltipButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "0px 3px",
    margin: "0 3px",
    color: "#00565e"
  }
};

class ProcessResults extends Component {
  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    resultsFound: 0,
    processResults: [],
    clearSearchTriggered: false,
    showViewProcessResultsPopup: false,
    currentAssessmentProcess: {},
    assignedCandidatesRowsDefinition: {},
    isLoadingAssignedCandidatesData: false,
    formattedReportData: [],
    reportName: "",
    displayNoDataError: false,
    selectedAccommodationRequestData: {}
  };

  constructor(props) {
    super(props);
    this.createReportLinkButtonRef = React.createRef();
  }

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateProcessResultsPageState(1);
    // initialize active search redux state
    this.props.updateSearchProcessResultsStates("", false);
    // populate initial table
    this.populateAllProcessResults();
  };

  componentDidUpdate = prevProps => {
    // if processResultsPaginationPage gets updated
    if (prevProps.processResultsPaginationPage !== this.props.processResultsPaginationPage) {
      this.populateProcessResultsBasedOnActiveSearch();
    }
    // if processResultsPaginationPageSize get updated
    if (
      prevProps.processResultsPaginationPageSize !== this.props.processResultsPaginationPageSize
    ) {
      this.populateProcessResultsBasedOnActiveSearch();
    }
    // if search processResultsKeyword gets updated
    if (prevProps.processResultsKeyword !== this.props.processResultsKeyword) {
      // if activeTestsKeyword redux state is empty
      if (this.props.processResultsKeyword !== "") {
        this.populateFoundProcessResults();
      } else {
        this.populateProcessResultsBasedOnActiveSearch();
      }
    }
    // if processResultsActiveSearch gets updated
    if (prevProps.processResultsActiveSearch !== this.props.processResultsActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.processResultsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateAllProcessResults();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundProcessResults();
      }
    }

    // if triggerDataRerender gets updated
    if (prevProps.triggerDataRerender !== this.props.triggerDataRerender) {
      this.populateProcessResultsBasedOnActiveSearch();
    }
  };

  populateProcessResultsBasedOnActiveSearch = () => {
    // current search
    if (this.props.processResultsActiveSearch) {
      this.populateFoundProcessResults();
      // no search
    } else {
      this.populateAllProcessResults();
    }
  };

  // populating all assessment process results
  populateAllProcessResults = () => {
    this.setState({ currentlyLoading: true }, () => {
      const processResultsArray = [];
      this.props
        .getAllAssessmentProcessResults(
          this.props.processResultsPaginationPage,
          this.props.processResultsPaginationPageSize
        )
        .then(response => {
          this.populateAssessmentProcessObject(processResultsArray, response);
          this.setState({ resultsFound: response.count });
        })
        .then(() => {
          this.setState({
            currentlyLoading: false
          });
        });
    });
  };

  // populating found assessment process results based on a search
  populateFoundProcessResults = () => {
    this.setState({ currentlyLoading: true }, () => {
      const processResultsArray = [];
      this.props
        .getFoundAssessmentProcessResults(
          this.props.processResultsKeyword,
          this.props.processResultsPaginationPage,
          this.props.processResultsPaginationPageSize
        )
        .then(response => {
          // there are no results found
          if (response.results.length === 0) {
            this.setState({
              testCenters: [],
              rowsDefinition: {},
              numberOfPages: 1,
              nextPageNumber: 0,
              previousPageNumber: 0,
              resultsFound: 0
            });
            // there is at least one result found
          } else {
            this.populateAssessmentProcessObject(processResultsArray, response);
            this.setState({ resultsFound: response.count });
          }
        })
        .then(() => {
          this.setState(
            {
              currentlyLoading: false
            },
            () => {
              // make sure that this element exsits to avoid any error
              if (document.getElementById("assessment-process-results-results-found")) {
                document.getElementById("assessment-process-results-results-found").focus();
              }
            }
          );
        });
    });
  };

  populateAssessmentProcessObject = (processResultsArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in processResultsArray
        processResultsArray.push({
          id: currentResult.id,
          user_id: currentResult.user_id,
          reference_number: currentResult.reference_number,
          closing_date: currentResult.closing_date,
          number_of_candidates: currentResult.number_of_candidates,
          number_of_tests_taken: currentResult.number_of_tests_taken
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult.reference_number,
          column_2: currentResult.closing_date,
          column_3: currentResult.number_of_candidates,
          column_4: currentResult.number_of_tests_taken,
          column_5: (
            <div className="d-flex justify-content-center flex-nowrap">
              <StyledTooltip
                id={`assessment-process-view-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      buttonId={`assessment-process-view-${i}`}
                      dataTip=""
                      dataFor={`assessment-process-view-${i}`}
                      label={<FontAwesomeIcon icon={faBinoculars} />}
                      action={() => {
                        this.handleViewAssessmentProcessPopup(currentResult);
                      }}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.SECONDARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.inPersonTesting.assessmentProcesses.processResults.table
                          .viewAssessmentProcessButton.ariaLabel,
                        currentResult.reference_number
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.inPersonTesting.assessmentProcesses.processResults.table
                          .viewAssessmentProcessButton.tooltipContent
                      }
                    </p>
                  </div>
                }
              />
              <StyledTooltip
                id={`assessment-process-report-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      buttonId={`assessment-process-report-${i}`}
                      dataTip=""
                      dataFor={`assessment-process-report-${i}`}
                      label={<FontAwesomeIcon icon={faDownload} />}
                      action={() =>
                        this.runGenerateAssessmentProcessResultsReport(currentResult.id)
                      }
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.SECONDARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.inPersonTesting.assessmentProcesses.processResults.table
                          .downloadReportAssessmentProcessButton.ariaLabel,
                        currentResult.reference_number
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.inPersonTesting.assessmentProcesses.processResults.table
                          .downloadReportAssessmentProcessButton.tooltipContent
                      }
                    </p>
                  </div>
                }
              />
            </div>
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState({
      processResults: processResultsArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(response.count / this.props.processResultsPaginationPageSize),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  // handle view assessment process action
  handleViewAssessmentProcessPopup = currentAssessmentProcess => {
    // display popup box
    this.setState(
      {
        showViewProcessResultsPopup: true,
        currentAssessmentProcess: currentAssessmentProcess
      },
      this.populateAssignedCandidatesTable(currentAssessmentProcess.id)
    );
  };

  populateTestsDetailsColumn = testData => {
    // initializing highestReasonForTestingMinimumProcessLength
    let highestReasonForTestingMinimumProcessLength = 0;
    // initializing assignedCandidatesContainsOlaTest
    let assignedCandidatesContainsOlaTest = false;
    return testData.map((data, index) => {
      // if reason_for_testing_minimum_process_length is defined and greater than current value
      if (
        data.reason_for_testing_minimum_process_length !== null &&
        data.reason_for_testing_minimum_process_length > highestReasonForTestingMinimumProcessLength
      ) {
        // updating the highestReasonForTestingMinimumProcessLength variable
        highestReasonForTestingMinimumProcessLength = parseInt(
          data.reason_for_testing_minimum_process_length
        );
      }
      // assignedCandidatesContainsOlaTest is still false
      if (!assignedCandidatesContainsOlaTest) {
        // checking if current test is an ola test
        if (
          data.test_skill_sub_type_codename === TestSkillSleDescCodename.ORAL_EN ||
          data.test_skill_sub_type_codename === TestSkillSleDescCodename.ORAL_FR
        ) {
          assignedCandidatesContainsOlaTest = true;
        }
      }
      // formatting test skill
      const formattedTestSkill =
        data.test_skill_sub_type_id !== null
          ? `${data[`test_skill_sub_type_name_${this.props.currentLanguage}`]}`
          : `${data[`test_skill_type_name_${this.props.currentLanguage}`]}`;
      // getting assigned test specs related status
      let assignedTestSpecsRelatedStatus = LOCALIZE.commons.na;
      const currentIterationRelatedComplementaryData = data;
      // making sure that the currentIterationRelatedComplementaryData is defined
      if (typeof currentIterationRelatedComplementaryData !== "undefined") {
        // getting assigned test specs related status
        assignedTestSpecsRelatedStatus = getAssignedTestSpecsRelatedStatus(
          currentIterationRelatedComplementaryData
        );
      }

      // getting formatted test data
      const formattedTestData = `${formattedTestSkill} | ${
        data[`reason_for_testing_name_${this.props.currentLanguage}`].split("/")[0]
      } | ${data.level_required}`;

      // getting formatted related status
      const formattedRelatedStatus = LOCALIZE.formatString(
        LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.assignedCandidatesTable
          .columnFourFormattedRelatedStatus,
        <span style={styles.boldText}>{assignedTestSpecsRelatedStatus}</span>
      );

      return (
        <>
          <p style={index === 0 ? styles.noPaddingTop : {}}>
            {formattedTestData} {formattedRelatedStatus}
            {data.user_accommodation_file_id !== null && (
              <StyledTooltip
                id={`accommodation-requests-pending-requests-view-accommodation-details-row-${index}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`accommodation-requests-pending-requests-view-accommodation-details-row-${index}`}
                      label={
                        <>
                          <FontAwesomeIcon
                            icon={
                              data.is_alternate_test_request
                                ? faFileCircleExclamation
                                : faUniversalAccess
                            }
                            style={styles.tableIcon}
                          />
                        </>
                      }
                      action={() =>
                        this.openAccommodationsRequestDetailsPopup(
                          data.user_accommodation_file_id,
                          data.candidate_id
                        )
                      }
                      customStyle={styles.accommodationTooltipButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        data.is_alternate_test_request
                          ? LOCALIZE.accommodationsRequestDetailsPopup
                              .viewAlternateTestRequestDetailsAccessibilityLabel
                          : LOCALIZE.accommodationsRequestDetailsPopup
                              .viewAccommodationsRequestDetailsAccessibilityLabel,
                        formattedTestSkill
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {data.is_alternate_test_request
                        ? LOCALIZE.accommodationsRequestDetailsPopup.viewAlternateTestRequestDetails
                        : LOCALIZE.accommodationsRequestDetailsPopup
                            .viewAccommodationsRequestDetails}
                    </p>
                  </div>
                }
              />
            )}
          </p>
        </>
      );
    });
  };

  // populating assigned candidates table
  populateAssignedCandidatesTable = assessmentProcessId => {
    this.setState({
      isLoadingAssignedCandidatesData: true
    });
    // initializing needed object and array for assignedCandidatesRowsDefinition props (needed for GenericTable component)
    let assignedCandidatesRowsDefinition = {};
    const data = [];
    // getting related assigned candidates
    this.props.getAssessmentProcessResultsCandidates(assessmentProcessId).then(response => {
      // getting unique assigned candidates (removing duplicates ==> candidates with more than one tests)
      const uniqueAssignedCandidates = response.filter((obj, index) => {
        return (
          index ===
          response.findIndex(
            o => obj.assessment_process_candidate_email === o.assessment_process_candidate_email
          )
        );
      });
      // looping in response given
      for (let i = 0; i < uniqueAssignedCandidates.length; i++) {
        // initializing temp columns
        let temp_column_5 = "";
        // getting all tests related to current assigned candidate
        const temp_test_data = response.filter(
          obj =>
            obj.assessment_process_candidate_email ===
            uniqueAssignedCandidates[i].assessment_process_candidate_email
        );
        // looping in current assigned candidate tests
        for (let j = 0; j < temp_test_data.length; j++) {
          // populating columns
          temp_column_5 += `<p style="${j === 0 ? "padding-top: 0;" : ""}">${
            temp_test_data[j][`${this.props.currentLanguage}_converted_score`] != null
              ? temp_test_data[j][`${this.props.currentLanguage}_converted_score`]
              : LOCALIZE.commons.na
          }</p>`;
        }
        const currentResult = uniqueAssignedCandidates[i];
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          // additional info data
          billing_contact_id: currentResult.billing_contact_id,
          billing_contact_first_name: currentResult.billing_contact_first_name,
          billing_contact_last_name: currentResult.billing_contact_last_name,
          billing_contact_email: currentResult.billing_contact_email,
          candidate_assigned_test_data: temp_test_data,
          // columns data
          column_1: currentResult.assessment_process_candidate_first_name,
          column_2: currentResult.assessment_process_candidate_last_name,
          column_3: currentResult.assessment_process_candidate_email,
          column_4: this.populateTestsDetailsColumn(temp_test_data),
          column_5: parse(temp_column_5)
        });
      }

      // updating rowsDefinition object with provided data and needed style
      assignedCandidatesRowsDefinition = {
        column_1_style: { ...COMMON_STYLE.LEFT_TEXT },
        column_2_style: { ...COMMON_STYLE.LEFT_TEXT },
        column_3_style: { ...COMMON_STYLE.LEFT_TEXT },
        column_4_style: { ...COMMON_STYLE.LEFT_TEXT },
        column_5_style: { ...COMMON_STYLE.CENTERED_TEXT },
        data: data
      };

      // saving results in state
      this.setState({
        assignedCandidatesRowsDefinition: assignedCandidatesRowsDefinition,
        isLoadingAssignedCandidatesData: false
      });
    });
  };

  openAccommodationsRequestDetailsPopup = (userAccommodationFileId, userId) => {
    // getting accommodation file data
    this.props
      .getAccommodationFileDataForDetailsPopup(userAccommodationFileId, false, null, userId)
      .then(response => {
        // updating needed states
        this.setState({
          showAccommodationsRequestDetailsPopup: true,
          selectedAccommodationRequestData: response
        });
      });
  };

  closeAccommodationsRequestDetailsPopup = () => {
    this.setState({
      showAccommodationsRequestDetailsPopup: false,
      selectedAccommodationRequestData: {}
    });
  };

  // handle closing the view popup
  handlePopupClose = () => {
    this.setState({
      showViewProcessResultsPopup: false,
      currentAssessmentProcess: {}
    });
  };

  // Close No Data Popup
  closeNoDataPopup = () => {
    this.setState({ displayNoDataError: false });
  };

  runGenerateAssessmentProcessResultsReport = assessmentProcessId => {
    // getting assessment process results for the selected assessment process id
    this.props.getAssessmentProcessResultsCandidatesReport(assessmentProcessId).then(response => {
      // verify if we have any data
      if (response.length > 0) {
        // File Name
        const reportName = `${LOCALIZE.reports.reportTypes.assessmentProcessResultsReport} - ${response[0].assessment_process_reference_number}.csv`;
        // File Data
        const reportData = generateAssessmentProcessResultsReport(response);

        // Updating the states needed for the CSV to be generated
        this.setState(
          {
            formattedReportData: reportData,
            reportName: reportName
          },
          () => {
            // once all data has been created, simulate click on hidden create report link to generate the downloadable csv file
            this.createReportLinkButtonRef.current.link.click();
          }
        );
      } else {
        // display no data popup
        this.setState({ displayNoDataError: true });
      }
    });
  };

  render() {
    const { formattedReportData, reportName, displayNoDataError } = this.state;

    // Process Results Table
    const columnsDefinition = [
      {
        label: LOCALIZE.inPersonTesting.assessmentProcesses.processResults.table.topTabs.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.inPersonTesting.assessmentProcesses.processResults.table.topTabs.columnTwo,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.processResults.table.topTabs.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.inPersonTesting.assessmentProcesses.processResults.table.topTabs.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.inPersonTesting.assessmentProcesses.processResults.table.topTabs.columnFive,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    // For the View Popup
    const assignedCandidatesColumnsDefinition = [
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.assignedCandidatesTable
            .columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.assignedCandidatesTable
            .columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.assignedCandidatesTable
            .columnThree,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.assignedCandidatesTable
            .columnFour,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.processResults.table.viewProcessResultPopup
            .columnFive,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    return (
      <div style={styles.mainContainer}>
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"assessment-process-results"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchProcessResultsStates}
            updatePageState={this.props.updateProcessResultsPageState}
            paginationPageSize={Number(this.props.processResultsPaginationPageSize)}
            updatePaginationPageSize={this.props.updateProcessResultsPageSizeState}
            data={this.state.processResults}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.processResultsKeyword}
          />
          <div>
            <GenericTable
              classnamePrefix="assessment-process-results"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.inPersonTesting.assessmentProcesses.processResults.table.noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <Pagination
            paginationContainerId={"assessment-process-results-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.processResultsPaginationPage}
            updatePaginationPageState={this.props.updateProcessResultsPageState}
            firstTableRowId={"assessment-process-results-table-row-0"}
          />
        </div>
        <PopupBox
          show={this.state.showViewProcessResultsPopup}
          handleClose={() => {}}
          shouldCloseOnEsc={false}
          isBackdropStatic={true}
          onPopupClose={this.handlePopupClose}
          title={this.state.currentAssessmentProcess.reference_number}
          description={
            <div style={styles.tableContainer}>
              <GenericTable
                classnamePrefix="assessment-process-assigned-candidates"
                columnsDefinition={assignedCandidatesColumnsDefinition}
                rowsDefinition={this.state.assignedCandidatesRowsDefinition}
                currentlyLoading={this.state.isLoadingAssignedCandidatesData}
                emptyTableMessage={
                  LOCALIZE.inPersonTesting.assessmentProcesses.processResults.table
                    .viewProcessResultPopup.noData
                }
              />
            </div>
          }
          rightButtonState={BUTTON_STATE.enabled}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faTimes}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonLabel={LOCALIZE.commons.close}
          rightButtonAction={this.handlePopupClose}
          size={"xl"}
        />
        <CSVLink
          ref={this.createReportLinkButtonRef}
          asyncOnClick={true}
          data={formattedReportData}
          filename={reportName}
          enclosingCharacter=""
        ></CSVLink>
        <PopupBox
          show={displayNoDataError}
          title={LOCALIZE.reports.noDataPopup.title}
          handleClose={() => {}}
          size={"lg"}
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.info}
                title={LOCALIZE.commons.info}
                message={
                  <p>{LOCALIZE.reports.assessmentProcessResultsReport.noDataPopup.description}</p>
                }
              />
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.closeNoDataPopup()}
        />
        {Object.keys(this.state.selectedAccommodationRequestData).length > 0 &&
          this.state.showAccommodationsRequestDetailsPopup && (
            <AccommodationsRequestDetailsPopup
              source={ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.hr}
              userAccommodationFileId={this.state.selectedAccommodationRequestData.id}
              selectedUserAccommodationFileData={this.state.selectedAccommodationRequestData}
              showPopup={this.state.showAccommodationsRequestDetailsPopup}
              closeAccommodationsRequestDetailsPopup={this.closeAccommodationsRequestDetailsPopup}
              popupSize="lg"
              // not providing isDraft props, since the option to print/download is not available for this user
            />
          )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    processResults: state.assessmentProcess.processResultsData,
    processResultsPaginationPage: state.assessmentProcess.processResultsPaginationPage,
    processResultsPaginationPageSize: state.assessmentProcess.processResultsPaginationPageSize,
    processResultsKeyword: state.assessmentProcess.processResultsKeyword,
    processResultsActiveSearch: state.assessmentProcess.processResultsActiveSearch,
    triggerDataRerender: state.assessmentProcess.triggerDataRerender
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateProcessResultsPageState,
      updateProcessResultsPageSizeState,
      updateSearchProcessResultsStates,
      getFoundAssessmentProcessResults,
      getAllAssessmentProcessResults,
      getAssessmentProcessData,
      getAssessmentProcessResultsCandidates,
      getAssessmentProcessResultsCandidatesReport,
      getAccommodationFileDataForDetailsPopup
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ProcessResults);
