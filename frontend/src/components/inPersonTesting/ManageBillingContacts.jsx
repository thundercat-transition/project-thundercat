import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import SearchBarWithDisplayOptions from "../commons/SearchBarWithDisplayOptions";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import Pagination from "../commons/Pagination";
import {
  updateCurrentBillingContactsPageState,
  updateBillingContactsPageSizeState,
  updateSearchBillingContactsStates,
  getFoundBillingContacts,
  getAllBillingContacts,
  createBillingContact,
  modifyBillingContact,
  deleteBillingContact
} from "../../modules/BillingContactRedux";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilAlt, faPlusCircle, faSave, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../commons/PopupBox";
import AddModifyBillingContactForm from "./AddModifyBillingContactForm";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import { triggerDataRerenderFunction } from "../../modules/AssesmentProcessRedux";
import { LANGUAGES } from "../commons/Translation";

const styles = {
  mainContainer: {
    width: "100%"
  },
  billingContactsTable: {
    addBillingContact: {
      button: {
        background: "transparent",
        padding: 12,
        border: "none",
        color: " #00565e"
      },
      icon: {
        color: "#00565e",
        verticalAlign: "middle"
      },
      label: {
        marginLeft: 18,
        verticalAlign: "middle",
        fontWeight: "bold"
      },
      content: {
        verticalAlign: "middle",
        backgroundColor: "white"
      }
    }
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  }
};

class ManageBillingContacts extends Component {
  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    resultsFound: 0,
    billingContacts: [],
    clearSearchTriggered: false,
    showAddBillingContactPopup: false,
    errorAddBillingContactPopup: false,
    showModifyBillingContactPopup: false,
    errorModifyBillingContactPopup: false,
    showDeleteBillingContactPopup: false,
    errorDeleteBillingContactPopup: false,
    showErrorPopup: false,
    currentBillingContact: { validForm: false }
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentBillingContactsPageState(1);
    // initialize active search redux state
    this.props.updateSearchBillingContactsStates("", false);
    // populate initial table
    this.populateAllBillingContacts();
  };

  componentDidUpdate = prevProps => {
    // // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateBillingContactsBasedOnActiveSearch();
    }
    // if billingContactsPaginationPage gets updated
    if (prevProps.billingContactsPaginationPage !== this.props.billingContactsPaginationPage) {
      this.populateBillingContactsBasedOnActiveSearch();
    }
    // // if billingContactsPaginationPageSize get updated
    if (
      prevProps.billingContactsPaginationPageSize !== this.props.billingContactsPaginationPageSize
    ) {
      this.populateBillingContactsBasedOnActiveSearch();
    }
    // if search billingContactsKeyword gets updated
    if (prevProps.billingContactsKeyword !== this.props.billingContactsKeyword) {
      // if activeTestsKeyword redux state is empty
      if (this.props.billingContactsKeyword !== "") {
        this.populateFoundBillingContacts();
      } else {
        this.populateBillingContactsBasedOnActiveSearch();
      }
    }
    // if billingContactsActiveSearch gets updated
    if (prevProps.billingContactsActiveSearch !== this.props.billingContactsActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.billingContactsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateAllBillingContacts();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundBillingContacts();
      }
    }
  };

  populateBillingContactsBasedOnActiveSearch = () => {
    // current search
    if (this.props.billingContactsActiveSearch) {
      this.populateFoundBillingContacts();
      // no search
    } else {
      this.populateAllBillingContacts();
    }
  };

  // populating all billing contacts
  populateAllBillingContacts = () => {
    this.setState({ currentlyLoading: true }, () => {
      const billingContactsArray = [];
      this.props
        .getAllBillingContacts(
          this.props.billingContactsPaginationPage,
          this.props.billingContactsPaginationPageSize
        )
        .then(response => {
          this.populateBillingContactsObject(billingContactsArray, response);
          this.setState({ resultsFound: response.count });
        })
        .then(() => {
          this.setState({
            currentlyLoading: false
          });
        });
    });
  };

  // populating found billing contacts based on a search
  populateFoundBillingContacts = () => {
    this.setState({ currentlyLoading: true }, () => {
      const billingContactsArray = [];
      this.props
        .getFoundBillingContacts(
          this.props.billingContactsKeyword,
          this.props.currentLanguage,
          this.props.billingContactsPaginationPage,
          this.props.billingContactsPaginationPageSize
        )
        .then(response => {
          // there are no results found
          if (response.results.length === 0) {
            this.setState({
              testCenters: [],
              rowsDefinition: {},
              numberOfPages: 1,
              nextPageNumber: 0,
              previousPageNumber: 0,
              resultsFound: 0
            });
            // there is at least one result found
          } else {
            this.populateBillingContactsObject(billingContactsArray, response);
            this.setState({ resultsFound: response.count }, () => {});
          }
        })
        .then(() => {
          this.setState(
            {
              currentlyLoading: false
            },
            () => {
              // make sure that this element exsits to avoid any error
              if (document.getElementById("billing-contacts-results-found")) {
                document.getElementById("billing-contacts-results-found").focus();
              }
            }
          );
        });
    });
  };

  populateBillingContactsObject = (billingContactsArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in billingContactsArray
        billingContactsArray.push({
          id: currentResult.id,
          first_name: currentResult.first_name,
          last_name: currentResult.last_name,
          email: currentResult.email,
          department_id: currentResult.dept_id,
          fis_organisation_code: currentResult.fis_organisation_code,
          fis_reference_code: currentResult.fis_reference_code
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult.first_name,
          column_2: currentResult.last_name,
          column_3: currentResult.email,
          column_4:
            this.props.currentLanguage === LANGUAGES.english
              ? currentResult.dept_eabrv
              : currentResult.dept_fabrv,
          column_5: currentResult.fis_organisation_code,
          column_6: currentResult.fis_reference_code,
          column_7: (
            <div className="d-flex justify-content-center flex-nowrap">
              <StyledTooltip
                id={`billing-contact-modify-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      buttonId={`billing-contact-modify-${i}`}
                      dataTip=""
                      dataFor={`billing-contact-modify-${i}`}
                      label={<FontAwesomeIcon icon={faPencilAlt} />}
                      action={() => {
                        this.handleModifyBillingContactPopup(currentResult);
                      }}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.SECONDARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.inPersonTesting.manageBillingContacts.table
                          .modifyBillingContactButton.ariaLabel,
                        currentResult.first_name,
                        currentResult.last_name
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.inPersonTesting.manageBillingContacts.table
                          .modifyBillingContactButton.tooltipContent
                      }
                    </p>
                  </div>
                }
              />
              <StyledTooltip
                id={`billing-contact-delete-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      buttonId={`billing-contact-delete-${i}`}
                      dataTip=""
                      dataFor={`billing-contact-delete-${i}`}
                      label={<FontAwesomeIcon icon={faTrashAlt} />}
                      action={() => {
                        this.handleDeleteBillingContactPopup(currentResult);
                      }}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.SECONDARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.inPersonTesting.manageBillingContacts.table
                          .deleteBillingContactButton.ariaLabel,
                        currentResult.first_name,
                        currentResult.last_name
                      )}
                      disabled={
                        !currentResult.exists_in_non_expired_process
                          ? BUTTON_STATE.enabled
                          : BUTTON_STATE.disabled
                      }
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.inPersonTesting.manageBillingContacts.table
                          .deleteBillingContactButton.tooltipContent
                      }
                    </p>
                  </div>
                }
              />
            </div>
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.LEFT_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      column_6_style: COMMON_STYLE.CENTERED_TEXT,
      column_7_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState({
      billingContacts: billingContactsArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(response.count / this.props.billingContactsPaginationPageSize),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  // handle create billing contact popup action
  handleCreateBillingContactPopup = () => {
    // display popup box
    this.setState({ showAddBillingContactPopup: true });
  };

  // handle create billing contact action
  handleCreateBillingContact = () => {
    this.props.createBillingContact(this.state.currentBillingContact).then(response => {
      if (response.status === 201) {
        // remove popup box
        this.setState({
          showAddBillingContactPopup: false,
          currentBillingContact: {}
        });
        this.populateBillingContactsBasedOnActiveSearch();
        // trigger data rerender
        this.props.triggerDataRerenderFunction();
      }
      // error message if billing contact wasn't created
      else {
        this.setState({
          showErrorPopup: true,
          showAddBillingContactPopup: false,
          // putting a reminder that the error was on create
          errorAddBillingContactPopup: true
        });
      }
    });
  };

  // handle modify billing contact action
  handleModifyBillingContactPopup = currentBillingContact => {
    // display popup box
    this.setState({
      showModifyBillingContactPopup: true,
      currentBillingContact: currentBillingContact
    });
  };

  // handle modify billing contact action
  handleModifyBillingContact = () => {
    this.props.modifyBillingContact(this.state.currentBillingContact).then(response => {
      if (response.status === 200) {
        // remove popup box
        this.setState({
          showModifyBillingContactPopup: false,
          currentBillingContact: {}
        });
        this.populateBillingContactsBasedOnActiveSearch();
        // trigger data rerender
        this.props.triggerDataRerenderFunction();
      }
      // error message if billing contact wasn't modified
      else {
        this.setState({
          showErrorPopup: true,
          showModifyBillingContactPopup: false,
          // putting a reminder that the error was on modify
          errorModifyBillingContactPopup: true
        });
      }
    });
  };

  // When a field in AddModifyBillingContactForm changes, update currentBillingContact + form validity
  handleChangeBillingContactObject = (
    firstName,
    lastName,
    email,
    departmentId,
    fisOrgCode,
    fisRefCode,
    isValid
  ) => {
    this.setState({
      currentBillingContact: {
        ...this.state.currentBillingContact,
        first_name: firstName,
        last_name: lastName,
        email: email,
        department_id: departmentId,
        fis_organisation_code: fisOrgCode,
        fis_reference_code: fisRefCode,

        // field validity
        validForm: isValid
      }
    });
  };

  // handle delete billing contact popup action
  handleDeleteBillingContactPopup = currentBillingContact => {
    // display popup box
    this.setState({
      showDeleteBillingContactPopup: true,
      currentBillingContact: currentBillingContact
    });
  };

  // handle delete billing contact action
  handleDeleteBillingContact = () => {
    this.props.deleteBillingContact(this.state.currentBillingContact.id).then(response => {
      if (response.status === 200) {
        // remove popup box
        this.setState({
          showModifyBillingContactPopup: false,
          currentBillingContact: {}
        });
        this.populateBillingContactsBasedOnActiveSearch();
        // trigger data rerender
        this.props.triggerDataRerenderFunction();
      }
      // error message if billing contact wasn't deleted
      else {
        this.setState({
          showErrorPopup: true,
          showDeleteBillingContactPopup: false,
          // putting a reminder that the error was on delete
          errorDeleteBillingContactPopup: true
        });
      }
    });
  };

  // handle closing the create/modify popup
  handlePopupClose = () => {
    // display popup box
    this.setState({
      showAddBillingContactPopup: false,
      showModifyBillingContactPopup: false,
      showDeleteBillingContactPopup: false,
      currentBillingContact: { validForm: false }
    });
  };

  // handle closing the error popup
  handleErrorPopupClose = () => {
    // closing error popup + putting valid form to false
    this.setState({
      showErrorPopup: false,
      currentBillingContact: { ...this.state.currentBillingContact, validForm: false }
    });

    // if create billing contact caused the issue, re-open it
    if (this.state.errorAddBillingContactPopup) {
      this.setState({
        errorAddBillingContactPopup: false,
        showAddBillingContactPopup: true
      });
    }
    // if modify billing contact caused the issue, re-open it
    else if (this.state.errorModifyBillingContactPopup) {
      this.setState({
        errorModifyBillingContactPopup: false,
        showModifyBillingContactPopup: true
      });
    }
    // if delete billing contact caused the issue, re-open it
    else if (this.state.errorDeleteBillingContactPopup) {
      this.setState({
        errorDeleteBillingContactPopup: false,
        showDeleteBillingContactPopup: true
      });
    }
  };

  // Populate Last Row Content
  populateLastRowHtml = () => {
    return (
      <td colSpan={12} style={styles.billingContactsTable.addBillingContact.content}>
        <CustomButton
          buttonId="add-billing-contact-button-id"
          label={
            <>
              <FontAwesomeIcon
                icon={faPlusCircle}
                style={styles.billingContactsTable.addBillingContact.icon}
              />
              <span style={styles.billingContactsTable.addBillingContact.label}>
                {LOCALIZE.inPersonTesting.manageBillingContacts.table.addBillingContactButton}
              </span>
            </>
          }
          customStyle={styles.billingContactsTable.addBillingContact.button}
          action={this.handleCreateBillingContactPopup}
        ></CustomButton>
      </td>
    );
  };

  // Populate Empty Table with Generate Button
  populateEmptyTable = () => {
    return (
      <CustomButton
        buttonId="add-billing-contact-button-id"
        label={
          <>
            <FontAwesomeIcon
              icon={faPlusCircle}
              style={styles.billingContactsTable.addBillingContact.icon}
            />
            <span style={styles.billingContactsTable.addBillingContact.label}>
              {LOCALIZE.inPersonTesting.manageBillingContacts.table.addBillingContactButton}
            </span>
          </>
        }
        customStyle={styles.billingContactsTable.addBillingContact.button}
        action={this.handleCreateBillingContactPopup}
      ></CustomButton>
    );
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.inPersonTesting.manageBillingContacts.table.topTabs.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.inPersonTesting.manageBillingContacts.table.topTabs.columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.inPersonTesting.manageBillingContacts.table.topTabs.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.inPersonTesting.manageBillingContacts.table.topTabs.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.inPersonTesting.manageBillingContacts.table.topTabs.columnFive,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.inPersonTesting.manageBillingContacts.table.topTabs.columnSix,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.inPersonTesting.manageBillingContacts.table.topTabs.columnSeven,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.inPersonTesting.manageBillingContacts.title}</h2>
        </div>
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"billing-contacts"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchBillingContactsStates}
            updatePageState={this.props.updateCurrentBillingContactsPageState}
            paginationPageSize={Number(this.props.billingContactsPaginationPageSize)}
            updatePaginationPageSize={this.props.updateBillingContactsPageSizeState}
            data={this.state.billingContacts}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.billingContactsKeyword}
          />
          <div>
            <GenericTable
              classnamePrefix="billing-contacts"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                // if we are searching, show no data, otherwise show the add button
                this.props.billingContactsKeyword
                  ? LOCALIZE.inPersonTesting.manageBillingContacts.table.noData
                  : this.populateEmptyTable()
              }
              currentlyLoading={this.state.currentlyLoading}
              hasLastRowHtml={
                // if we have any billing contact or we are searching, show last row html
                this.state.billingContacts.length > 0 || this.props.billingContactsKeyword
              }
              lastRowHtml={this.populateLastRowHtml()}
            />
          </div>
          <Pagination
            paginationContainerId={"billing-contacts-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.billingContactsPaginationPage}
            updatePaginationPageState={this.props.updateCurrentBillingContactsPageState}
            firstTableRowId={"billing-contacts-table-row-0"}
          />
        </div>
        <PopupBox
          show={this.state.showAddBillingContactPopup || this.state.showModifyBillingContactPopup}
          handleClose={() => {}}
          shouldCloseOnEsc={false}
          isBackdropStatic={true}
          title={
            this.state.showAddBillingContactPopup
              ? LOCALIZE.inPersonTesting.manageBillingContacts.addBillingContactPopup.title
              : LOCALIZE.inPersonTesting.manageBillingContacts.modifyBillingContactPopup.title
          }
          description={
            <AddModifyBillingContactForm
              isAddBillingContactPopup={this.state.showAddBillingContactPopup}
              billingContactObject={this.state.currentBillingContact}
              changeBillingContactObject={this.handleChangeBillingContactObject}
            />
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.handlePopupClose}
          rightButtonState={
            this.state.currentBillingContact.validForm
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={this.state.showAddBillingContactPopup ? faPlusCircle : faSave}
          rightButtonTitle={
            this.state.showAddBillingContactPopup
              ? LOCALIZE.inPersonTesting.manageBillingContacts.addBillingContactPopup.addButton
              : LOCALIZE.commons.saveButton
          }
          rightButtonLabel={
            this.state.showAddBillingContactPopup
              ? LOCALIZE.inPersonTesting.manageBillingContacts.addBillingContactPopup.addButton
              : LOCALIZE.commons.saveButton
          }
          rightButtonAction={
            this.state.showAddBillingContactPopup
              ? this.handleCreateBillingContact
              : this.handleModifyBillingContact
          }
          size={"lg"}
        />
        <PopupBox
          show={this.state.showDeleteBillingContactPopup}
          handleClose={this.handlePopupClose}
          title={LOCALIZE.inPersonTesting.manageBillingContacts.deleteBillingContactPopup.title}
          description={
            <div>
              <div>
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.warning}
                  message={
                    <p>
                      {this.state.currentBillingContact.first_name &&
                        this.state.currentBillingContact.last_name &&
                        LOCALIZE.formatString(
                          LOCALIZE.inPersonTesting.manageBillingContacts.deleteBillingContactPopup
                            .message,
                          this.state.currentBillingContact.first_name,
                          this.state.currentBillingContact.last_name
                        )}
                    </p>
                  }
                />
              </div>
              <div>
                {
                  LOCALIZE.inPersonTesting.manageBillingContacts.deleteBillingContactPopup
                    .description
                }
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.handlePopupClose}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.handleDeleteBillingContact}
          size={"lg"}
        />
        <PopupBox
          show={this.state.showErrorPopup}
          handleClose={this.handleErrorPopupClose}
          title={
            this.state.errorAddBillingContactPopup
              ? LOCALIZE.inPersonTesting.manageBillingContacts.errorBillingContactPopup.title
                  .addBillingContact
              : this.state.errorModifyBillingContactPopup
                ? LOCALIZE.inPersonTesting.manageBillingContacts.errorBillingContactPopup.title
                    .modifyBillingContact
                : LOCALIZE.inPersonTesting.manageBillingContacts.errorBillingContactPopup.title
                    .deleteBillingContact
          }
          description={
            <div>
              <div>
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.error}
                  message={
                    <p>
                      {this.state.errorAddBillingContactPopup
                        ? LOCALIZE.inPersonTesting.manageBillingContacts.errorBillingContactPopup
                            .description.addBillingContact
                        : this.state.errorModifyBillingContactPopup
                          ? LOCALIZE.inPersonTesting.manageBillingContacts.errorBillingContactPopup
                              .description.modifyBillingContact
                          : LOCALIZE.inPersonTesting.manageBillingContacts.errorBillingContactPopup
                              .description.deleteBillingContact}
                    </p>
                  }
                />
              </div>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.handleErrorPopupClose}
          size={"lg"}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    billingContacts: state.billingContact.billingContactsData,
    billingContactsPaginationPage: state.billingContact.billingContactsPaginationPage,
    billingContactsPaginationPageSize: state.billingContact.billingContactsPaginationPageSize,
    billingContactsKeyword: state.billingContact.billingContactsKeyword,
    billingContactsActiveSearch: state.billingContact.billingContactsActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateCurrentBillingContactsPageState,
      updateBillingContactsPageSizeState,
      updateSearchBillingContactsStates,
      getFoundBillingContacts,
      getAllBillingContacts,
      createBillingContact,
      modifyBillingContact,
      deleteBillingContact,
      triggerDataRerenderFunction
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ManageBillingContacts);
