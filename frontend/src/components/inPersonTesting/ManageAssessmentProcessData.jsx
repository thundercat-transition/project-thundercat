import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import * as _ from "lodash";
import { bindActionCreators } from "redux";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import DropdownSelect from "../commons/DropdownSelect";
import {
  assessmentProcessAssignCandidate,
  deleteAssessmentProcessAssignedCandidate,
  editAssessmentProcessAssignedCandidate,
  getAssessmentProcessAssignedCandidates,
  getAssessmentProcessData,
  getAssessmentProcessReferenceNumbers,
  getAvailableTestSessionsForSpecificTestToAdminister,
  getBillingContactsList,
  sendAssessmentProcessAssignedCandidatesRequest,
  setAssessmentProcessDataAssignCandidates,
  setAssessmentProcessDataActiveProcesses,
  setSelectedAssessmentProcessRefNbrActiveProcesses,
  setSelectedAssessmentProcessRefNbrAssignCandidates,
  triggerDataRerenderFunction,
  sendAssessmentProcessUpdatesRequest,
  inviteSingleAssessmentProcessCandidateRequest,
  unassignAssessmentProcessAssignedTestToAdminister,
  updateNonSentAssessmentProcessData,
  updateReferenceNumber,
  setAssessmentProcessLabel,
  deleteAssessmentProcess
} from "../../modules/AssesmentProcessRedux";
import { Col, Row } from "react-bootstrap";
import DatePicker from "../commons/DatePicker";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import {
  REASON_FOR_TESTING_TYPE_CODENAME_CONST,
  REASON_FOR_TESTING_CODENAME_CONST,
  alternateColorsStyle
} from "../commons/Constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPencilAlt,
  faPlusCircle,
  faSave,
  faShare,
  faSpinner,
  faTimes,
  faTrashAlt,
  faCalendarCheck,
  faPaperPlane,
  faCheck,
  faTimesCircle,
  faUniversalAccess,
  faFileCircleExclamation
} from "@fortawesome/free-solid-svg-icons";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../commons/PopupBox";
import validateName, { validateEmail } from "../../helpers/regexValidator";
import Switch from "react-switch";
import getSwitchTransformScale from "../../helpers/switchTransformScale";
import { getReasonsForTesting } from "../../modules/UitRedux";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import { LANGUAGES } from "../commons/Translation";
import { getOrganization } from "../../modules/ExtendedProfileOptionsRedux";
import getFormattedDate from "../../helpers/getFormattedDate";
import "../../css/utils.css";
import TEST_STATUS from "../ta/Constants";
import staticConsumedReservationCodeStatus from "../candidate/Constants";
import AccommodationsRequestDetailsPopup, {
  ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE
} from "../aae/AccommodationsRequestDetailsPopup";
import { TestSkillSleDescCodename, TestSkillTypeCodename } from "../testFactory/Constants";
import getCheckboxTransformScale from "../../helpers/checkboxTransformScale";
import { convertDateTimeToUsersTimezone } from "../../modules/helpers";
import { referenceNumberRegexExpression } from "./CreateProcess";

export const ManageAssessmentProcessDataSource = {
  ASSIGN_CANDIDATES: "assign_candidates",
  ACTIVE_PROCESSES: "active_processes"
};

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  },
  thirdColumn: {
    xs: 12,
    sm: 12,
    md: 1,
    lg: 1,
    xl: 1
  }
};

const styles = {
  mainContainer: {
    width: "100%"
  },
  formContainer: {
    margin: 24
  },
  inputContainer: {
    margin: "12px 24px"
  },
  inputContainerWithError: {
    margin: "12px 24px 0 24px"
  },
  input: {
    minHeight: 38,
    border: "1px solid #00565e",
    width: "100%",
    padding: "3px 6px 3px 6px"
  },
  boldText: {
    fontWeight: "bold"
  },
  rowContainer: {
    margin: "12px 48px"
  },
  rowContainerWithError: {
    margin: "12px 48px 0px 48px"
  },
  inputTitles: {
    fontWeight: "bold"
  },
  noPadding: {
    padding: 0
  },
  tableLabel: {
    fontWeight: "bold",
    padding: "12px 0"
  },
  tableContainer: {
    margin: "0 24px"
  },
  assignedCandidatesTableContainer: {
    marginTop: 24
  },
  addCandidateButtonIcon: {
    marginRight: 6,
    color: "#00565e"
  },
  testsToAdministerInPopupContainer: {
    marginTop: 48
  },
  addCandidateButton: {
    background: "transparent",
    padding: 12,
    border: "none",
    color: " #00565e"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  alignTop: {
    verticalAlign: "top"
  },
  allUnset: {
    all: "unset"
  },
  tableIcon: {
    transform: "scale(1.2)"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  accommodationTooltipButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "0px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  tableHeaderTooltipButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "white",
    verticalAlign: "middle"
  },
  redButton: {
    color: "#cc0000"
  },
  sendUpdatesButtonContainer: {
    margin: 24,
    textAlign: "center"
  },
  sendUpdatesButton: {
    minWidth: 200
  },
  buttonRowPadding: {
    paddingBottom: 60,
    paddingTop: 10,
    margin: "0 auto",
    marginBottom: "2em"
  },
  sendRequestButtonContainer: {
    textAlign: "center"
  },
  sendRequestButtonContainerIcon: {
    marginRight: 6
  },
  deleteRequestButtonContainerIcon: {
    marginRight: 6
  },
  sendButton: {
    justifyContent: "center",
    width: "100%"
  },
  deleteButton: {
    float: "right"
  },
  olaCheckboxexMainContainer: {
    margin: "24px 12px"
  },
  olaCheckboxContainer: {
    display: "table",
    width: "100%",
    marginBottom: 12
  },
  olaCheckboxDisplayTableCell: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  olaCheckboxLabelDisplayTableCell: {
    display: "table-cell",
    verticalAlign: "middle",
    width: "100%"
  },
  checkbox: {
    margin: "0 36px 0 16px"
  },
  loading: {
    minHeight: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  },
  centerText: {
    textAlign: "center"
  },
  availableTestSessionsTableStyle: {
    padding: "12px 0"
  },
  noPaddingTop: {
    paddingTop: 0
  },
  colStyle: {
    position: "relative",
    width: 350
  },
  editButton: {
    minWidth: 25,
    minHeight: 38,
    borderRadius: "0 4px 4px 0",
    padding: "3px 12px",
    borderLeft: "none"
  },
  dropdownDiv: {
    width: "90%",
    minHeight: 38,
    float: "left",
    flex: "0 0 100%"
  },
  editButtonContainer: {
    position: "absolute",
    right: "15px"
  }
};

export function getAssignedTestSpecsRelatedStatus(assignedTestSpecsComplementaryData) {
  // test has been submitted
  if (assignedTestSpecsComplementaryData.assigned_test_status_codename === TEST_STATUS.SUBMITTED) {
    return LOCALIZE.commons.status.submitted;
    // test has been quit
  }
  if (assignedTestSpecsComplementaryData.assigned_test_status_codename === TEST_STATUS.QUIT) {
    return LOCALIZE.commons.status.quit;
    // test has been unassigned
  }
  if (assignedTestSpecsComplementaryData.assigned_test_status_codename === TEST_STATUS.UNASSIGNED) {
    return LOCALIZE.commons.status.unassigned;
    // currently in test
  }
  if (
    assignedTestSpecsComplementaryData.assigned_test_id !== null &&
    assignedTestSpecsComplementaryData.assigned_test_status_codename !== TEST_STATUS.SUBMITTED &&
    assignedTestSpecsComplementaryData.assigned_test_status_codename !== TEST_STATUS.QUIT
  ) {
    return LOCALIZE.commons.status.active;
    // reservation code ==> booked
  }
  if (
    assignedTestSpecsComplementaryData.consumed_reservation_codes_status_codename ===
    staticConsumedReservationCodeStatus.BOOKED
  ) {
    return LOCALIZE.commons.reservationCodeStatus.booked;
    // reservation code ==> reserved/consumed
  }
  if (
    assignedTestSpecsComplementaryData.consumed_reservation_codes_status_codename ===
    staticConsumedReservationCodeStatus.RESERVED
  ) {
    return LOCALIZE.commons.reservationCodeStatus.consumed;
    // reservation code ==> revoked
  }
  if (
    assignedTestSpecsComplementaryData.consumed_reservation_codes_status_codename ===
    staticConsumedReservationCodeStatus.REVOKED
  ) {
    return LOCALIZE.commons.reservationCodeStatus.revoked;
  }
  // reservation code ==> withdrawn
  if (
    assignedTestSpecsComplementaryData.consumed_reservation_codes_status_codename ===
    staticConsumedReservationCodeStatus.WITHDRAWN
  ) {
    return LOCALIZE.commons.reservationCodeStatus.withdrawn;
  }
  // none of the above
  return LOCALIZE.commons.sent;
}

class ManageAssessmentProcessData extends Component {
  static propTypes = {
    source: PropTypes.string.isRequired
  };

  state = {
    referenceNumberOptions: [],
    departmentOptions: [],
    departmentSelectedOption: [],
    isAssignCandidatesReferenceNumberSelected: false,
    isActiveProcessesReferenceNumberSelected: false,
    assignCandidatesDuration: "",
    activeProcessesDuration: "",
    calculatedAssignCandidatesClosingDate: null,
    isValidCalculatedAssignCandidatesClosingDate: true,
    calculatedActiveProcessesClosingDate: null,
    isValidCalculatedActiveProcessesClosingDate: true,
    assignCandidatesAllowBookingExternalTc: false,
    allowLastMinuteCancellationsTc: false,
    assignCandidatesContactEmailForCandidates: "",
    activeProcessesContactEmailForCandidates: "",
    isValidAssignCandidatesContactEmailForCandidates: true,
    isValidActiveProcessesContactEmailForCandidates: true,
    isValidAssignCandidatesChanges: true,
    isValidActiveProcessesChanges: true,
    assignCandidatesProcessDataUpdated: false,
    activeProcessesProcessDataUpdated: false,
    showNonSentAssessmentProcessDataUpdatedSuccessfullyPopup: false,
    showAssessmentProcessDataUpdatedSuccessfullyPopup: false,
    assignCandidatesTestsToAdministerRowsDefinition: {},
    activeProcessesTestsToAdministerRowsDefinition: {},
    assignedCandidatesRowsDefinition: {},
    showViewAvailableTestSessionsPopup: false,
    availableTestSessionsCurrentlyLoading: false,
    availableTestSessionsRowsDefinition: {},
    indexOfRowToView: null,
    showAddCandidatePopup: false,
    initialFirstName: "",
    firstName: "",
    isValidFirstName: true,
    initialLastName: "",
    lastName: "",
    isValidLastName: true,
    initialEmail: "",
    email: "",
    isValidEmail: true,
    popupDataUpdated: false,
    emailOrAssignedTestsToAdministerUpdated: false,
    showDuplicateEmailFoundError: false,
    billingContactOptions: [],
    initialBillingContactSelectedOption: [],
    billingContactSelectedOption: [],
    testsToAdministerInPopupRowsDefinition: {},
    reasonForTestingOptions: [],
    initialReasonForTestingStates: [],
    reasonForTestingStates: [],
    highestReasonForTestingMinimumProcessLength: 0,
    assignedCandidatesContainsOlaTest: false,
    assignedCandidatesContainsImperative: false,
    initialLevelRequiredStates: [],
    levelRequiredStates: [],
    testAssignedStates: [],
    initialTestAssignedStates: [],
    assignedTestSpecsIdsToBeUpdated: [],
    showUnassignTestPopup: false,
    assignedTestSpecsIdToBeUnassign: null,
    isValidAddEditCandidateForm: false,
    showEditAssignedCandidatePopup: false,
    showDeleteAssignedCandidatePopup: false,
    indexOfRowToEditOrDelete: null,
    showSendRequestConfirmationPopup: false,
    showRequestSentSuccessfullyPopup: false,
    showDeleteProcessPopup: false,
    isLoadingSentDateData: false,
    isLoadingClosingDateData: false,
    isLoadingTestsToAdminister: false,
    isLoadingAssignedCandidates: false,
    showAccommodationsRequestDetailsPopup: false,
    selectedAccommodationRequestData: {},
    olaCheckbox1: false,
    olaCheckbox2: false,
    // default values
    switchHeight: 25,
    switchWidth: 50,
    showEditNamePopup: false,
    referenceNumber: "",
    unassignLoading: false
  };

  componentDidMount = () => {
    // initializing needed redux states
    this.props.setSelectedAssessmentProcessRefNbrAssignCandidates({});
    this.props.setSelectedAssessmentProcessRefNbrActiveProcesses({});
    this.props.setAssessmentProcessDataAssignCandidates({});
    this.props.setAssessmentProcessDataActiveProcesses({});
    // populating process reference number options
    this.populateReferenceNumberOptions();
    if (this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES) {
      // populating department options
      this.populateDepartmentOptions();
    }
    // getting updated switch dimensions
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = prevProps => {
    // if assessmentProcessDataAssignCandidates or assessmentProcessDataActiveProcesses gets updated
    if (
      prevProps.assessmentProcessDataAssignCandidates !==
        this.props.assessmentProcessDataAssignCandidates ||
      prevProps.assessmentProcessDataActiveProcesses !==
        this.props.assessmentProcessDataActiveProcesses
    ) {
      // setting needed states
      this.setState(
        {
          isAssignCandidatesReferenceNumberSelected:
            Object.keys(this.props.assessmentProcessDataAssignCandidates).length > 0,
          isActiveProcessesReferenceNumberSelected:
            Object.keys(this.props.assessmentProcessDataActiveProcesses).length > 0
        },
        () => {
          // reference number has been selected
          if (
            this.state.isAssignCandidatesReferenceNumberSelected ||
            this.state.isActiveProcessesReferenceNumberSelected
          ) {
            // populating tables
            this.populateTestsToAdministerTable();
            // populating assigned candidates table
            this.populateAssignedCandidatesTable();
            this.setState({ isLoadingClosingDateData: false, isLoadingSentDateData: false });
          }
        }
      );
    }
    // if assessmentProcessDataAssignCandidates gets updated and is defined
    if (
      prevProps.assessmentProcessDataAssignCandidates !==
        this.props.assessmentProcessDataAssignCandidates &&
      Object.keys(this.props.assessmentProcessDataAssignCandidates).length > 0
    ) {
      // calculating closing date (current date + duration in days)
      let calculatedAssignCandidatesClosingDate = new Date(Date.now());
      calculatedAssignCandidatesClosingDate.setDate(
        calculatedAssignCandidatesClosingDate.getDate() +
          parseInt(this.props.assessmentProcessDataAssignCandidates.duration)
      );
      calculatedAssignCandidatesClosingDate = getFormattedDate(
        `${calculatedAssignCandidatesClosingDate.getFullYear()}-${
          calculatedAssignCandidatesClosingDate.getMonth() + 1
        }-${calculatedAssignCandidatesClosingDate.getDate()}`
      );
      this.setState({
        departmentSelectedOption: {
          value: this.props.assessmentProcessDataAssignCandidates.dept_id,
          label:
            this.props.currentLanguage === LANGUAGES.english
              ? `${this.props.assessmentProcessDataAssignCandidates.dept_edesc} (${this.props.assessmentProcessDataAssignCandidates.dept_eabrv})`
              : `${this.props.assessmentProcessDataAssignCandidates.dept_fdesc} (${this.props.assessmentProcessDataAssignCandidates.dept_fabrv})`
        },
        assignCandidatesDuration: this.props.assessmentProcessDataAssignCandidates.duration,
        calculatedAssignCandidatesClosingDate: calculatedAssignCandidatesClosingDate,
        isValidCalculatedAssignCandidatesClosingDate: true,
        assignCandidatesAllowBookingExternalTc:
          this.props.assessmentProcessDataAssignCandidates.allow_booking_external_tc,
        assignCandidatesContactEmailForCandidates:
          this.props.assessmentProcessDataAssignCandidates.contact_email_for_candidates,
        allowLastMinuteCancellationsTc:
          this.props.assessmentProcessDataAssignCandidates.allow_last_minute_cancellation_tc
      });
    }
    // if assessmentProcessDataActiveProcesses gets updated and is defined
    if (
      prevProps.assessmentProcessDataActiveProcesses !==
        this.props.assessmentProcessDataActiveProcesses &&
      Object.keys(this.props.assessmentProcessDataActiveProcesses).length > 0
    ) {
      // calculating closing date (sent date + duration in days)
      let calculatedActiveProcessesClosingDate = new Date(
        this.props.assessmentProcessDataActiveProcesses.sent_date
      );
      calculatedActiveProcessesClosingDate.setDate(
        calculatedActiveProcessesClosingDate.getDate() +
          parseInt(this.props.assessmentProcessDataActiveProcesses.duration) +
          1
      );
      calculatedActiveProcessesClosingDate = getFormattedDate(
        `${calculatedActiveProcessesClosingDate.getFullYear()}-${
          calculatedActiveProcessesClosingDate.getMonth() + 1
        }-${calculatedActiveProcessesClosingDate.getDate()}`
      );
      // updating needed states
      this.setState({
        activeProcessesDuration: this.props.assessmentProcessDataActiveProcesses.duration,
        activeProcessesContactEmailForCandidates:
          this.props.assessmentProcessDataActiveProcesses.contact_email_for_candidates,
        calculatedActiveProcessesClosingDate: calculatedActiveProcessesClosingDate,
        isValidCalculatedActiveProcessesClosingDate: true
      });
    }
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
    // if triggerDataRerender gets updated
    if (prevProps.triggerDataRerender !== this.props.triggerDataRerender) {
      // populating process reference number options
      this.populateReferenceNumberOptions();
      // populating billing contact options
      this.populateBillingContactOptions();
    }
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    this.setState({
      switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
      switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
    });
  };

  populateReferenceNumberOptions = () => {
    // initializing referenceNumberOptions
    const referenceNumberOptions = [];
    // source is ASSIGN_CANDIDATES ==> getting reference numbers of non-sent requests
    // source is ACTIVE_PROCESSES ==> getting reference numbers of sent requests where closing has not passed yet
    this.props
      .getAssessmentProcessReferenceNumbers(
        // only_get_sent_request parameter (true/false)
        this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES
      )
      .then(response => {
        // looping in results
        for (let i = 0; i < response.length; i++) {
          referenceNumberOptions.push({
            label: `${response[i].reference_number}`,
            value: response[i].id
          });
        }
        this.setState({ referenceNumberOptions: referenceNumberOptions });
      });
  };

  populateDepartmentOptions = () => {
    // initializing departmentOptions
    const departmentOptions = [];
    this.props.getOrganization().then(response => {
      for (let i = 0; i < response.body.length; i++) {
        // Interface is in English
        if (this.props.currentLanguage === LANGUAGES.english) {
          departmentOptions.push({
            label: `${response.body[i].edesc} (${response.body[i].eabrv})`,
            value: response.body[i].dept_id
          });
          // Interface is in French
        } else {
          departmentOptions.push({
            label: `${response.body[i].fdesc} (${response.body[i].fabrv})`,
            value: response.body[i].dept_id
          });
        }
      }
      this.setState({ departmentOptions: departmentOptions });
    });
  };

  getSelectedReferenceNumberOption = selectedOption => {
    this.setState(
      {
        isLoadingTestsToAdminister: true,
        isLoadingAssignedCandidates: true,
        isLoadingClosingDateData: true,
        isLoadingSentDateData: true
      },
      () => {
        // getting related assessment process data
        this.props
          .getAssessmentProcessData(selectedOption.value)
          .then(response => {
            // updating redux states
            // called from Assign Candidates
            if (this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES) {
              this.setState({ assignCandidatesProcessDataUpdated: false });
              this.props.setAssessmentProcessDataAssignCandidates(response);
              this.props.setSelectedAssessmentProcessRefNbrAssignCandidates(selectedOption);
              // called from Active Processes
            } else {
              this.setState({ activeProcessesProcessDataUpdated: false });
              this.props.setAssessmentProcessDataActiveProcesses(response);
              this.props.setSelectedAssessmentProcessRefNbrActiveProcesses(selectedOption);
            }
            // populating billing contact options
            this.populateBillingContactOptions();
            // populating reason for testing options
            this.populateReasonForTestingOptions();
          })
          .then(() => {
            // resetting ola checkbox states
            this.setState({
              olaCheckbox1: false,
              olaCheckbox2: false,
              assignedCandidatesContainsOlaTest: false
            });
          });
      }
    );
  };

  getSelectedDepartmentOption = selectedOption => {
    this.setState(
      {
        departmentSelectedOption: selectedOption
      },
      () => {
        // detecting and validating changes
        this.detectAssignCandidatesChanges();
        this.validateAssignCandidatesChanges();
      }
    );
  };

  getAssignCandidatesDurationContent = event => {
    const assignCandidatesDuration = event.target.value;
    // allow only numeric values (0 to 3 chars)
    const regexExpression = /^([0-9]{0,3})$/;
    if (regexExpression.test(assignCandidatesDuration)) {
      this.setState(
        {
          assignCandidatesDuration:
            assignCandidatesDuration !== "" ? parseInt(assignCandidatesDuration) : "",
          isLoadingClosingDateData: true
        },
        () => {
          // initializing calculatedAssignCandidatesClosingDate to redux value
          let calculatedAssignCandidatesClosingDate =
            this.props.assessmentProcessDataAssignCandidates.closing_date;
          // duration is defined
          if (assignCandidatesDuration !== "") {
            // calculating closing date (current date + duration in days)
            calculatedAssignCandidatesClosingDate = new Date(Date.now());
            calculatedAssignCandidatesClosingDate.setDate(
              calculatedAssignCandidatesClosingDate.getDate() +
                parseInt(this.state.assignCandidatesDuration)
            );
            calculatedAssignCandidatesClosingDate = getFormattedDate(
              `${calculatedAssignCandidatesClosingDate.getFullYear()}-${
                calculatedAssignCandidatesClosingDate.getMonth() + 1
              }-${calculatedAssignCandidatesClosingDate.getDate()}`
            );
          }
          // updating needed state
          this.setState(
            {
              calculatedAssignCandidatesClosingDate: calculatedAssignCandidatesClosingDate,
              isLoadingClosingDateData: false
            },
            () => {
              // detecting and validating changes
              this.detectAssignCandidatesChanges();
              this.validateAssignCandidatesChanges();
            }
          );
        }
      );
    }
  };

  getActiveProcessesDurationContent = event => {
    const activeProcessesDuration = event.target.value;
    // allow only numeric values (0 to 3 chars)
    const regexExpression = /^([0-9]{0,3})$/;
    if (regexExpression.test(activeProcessesDuration)) {
      this.setState(
        {
          activeProcessesDuration:
            activeProcessesDuration !== "" ? parseInt(activeProcessesDuration) : ""
        },
        () => {
          // detecting and validating changes
          this.detectActiveProcessesDataChanges();
          this.validateActiveProcessesChanges();
        }
      );
    }
  };

  handleAllowBookingExternalTcSwitchUpdates = event => {
    this.setState({ assignCandidatesAllowBookingExternalTc: event }, () => {
      // detecting and validating changes
      this.detectAssignCandidatesChanges();
      this.validateAssignCandidatesChanges();
    });
  };

  handleAllowLastMinuteCancellationsSwitchUpdates = event => {
    this.setState({ allowLastMinuteCancellationsTc: event }, () => {
      // detecting and validating changes
      this.detectAssignCandidatesChanges();
      this.validateAssignCandidatesChanges();
    });
  };

  getAssignCandidatesContactEmailForCandidatesContent = event => {
    const assignCandidatesContactEmailForCandidates = event.target.value;
    const isValidAssignCandidatesContactEmailForCandidates = validateEmail(event.target.value);
    this.setState(
      {
        assignCandidatesContactEmailForCandidates: assignCandidatesContactEmailForCandidates,
        isValidAssignCandidatesContactEmailForCandidates:
          isValidAssignCandidatesContactEmailForCandidates
      },
      () => {
        // detecting and validating changes
        this.detectAssignCandidatesChanges();
        this.validateAssignCandidatesChanges();
      }
    );
  };

  getActiveProcessesContactEmailForCandidatesContent = event => {
    const activeProcessesContactEmailForCandidates = event.target.value;
    const isValidActiveProcessesContactEmailForCandidates = validateEmail(event.target.value);
    this.setState(
      {
        activeProcessesContactEmailForCandidates: activeProcessesContactEmailForCandidates,
        isValidActiveProcessesContactEmailForCandidates:
          isValidActiveProcessesContactEmailForCandidates
      },
      () => {
        // detecting and validating changes
        this.detectActiveProcessesDataChanges();
        this.validateActiveProcessesChanges();
      }
    );
  };

  detectAssignCandidatesChanges = () => {
    // initializing needed variables
    let assignCandidatesProcessDataUpdated = false;
    const requestingOrganizationUpdated =
      this.props.assessmentProcessDataAssignCandidates.dept_id !==
      this.state.departmentSelectedOption.value;
    const durationUpdated =
      this.props.assessmentProcessDataAssignCandidates.duration !==
      this.state.assignCandidatesDuration;
    const allowBookingExternalTcUpdated =
      this.props.assessmentProcessDataAssignCandidates.allow_booking_external_tc !==
      this.state.assignCandidatesAllowBookingExternalTc;
    const contactEmailForCandidatesUpdated =
      this.props.assessmentProcessDataAssignCandidates.contact_email_for_candidates !==
      this.state.assignCandidatesContactEmailForCandidates;
    const allowLastMinuteCancellationsUpdated =
      this.props.assessmentProcessDataAssignCandidates.allow_last_minute_cancellation_tc !==
      this.state.allowLastMinuteCancellationsTc;

    // if at least one input has been updated
    if (
      requestingOrganizationUpdated ||
      durationUpdated ||
      allowBookingExternalTcUpdated ||
      contactEmailForCandidatesUpdated ||
      allowLastMinuteCancellationsUpdated
    ) {
      // updating assignCandidatesProcessDataUpdated
      assignCandidatesProcessDataUpdated = true;
    }

    this.setState({ assignCandidatesProcessDataUpdated: assignCandidatesProcessDataUpdated });
  };

  detectActiveProcessesDataChanges = () => {
    // initializing needed variables
    let activeProcessesProcessDataUpdated = false;
    const durationUpdated =
      this.props.assessmentProcessDataActiveProcesses.duration !==
      this.state.activeProcessesDuration;
    const contactEmailForCandidatesUpdated =
      this.props.assessmentProcessDataActiveProcesses.contact_email_for_candidates !==
      this.state.activeProcessesContactEmailForCandidates;

    // if at least one input has been updated
    if (durationUpdated || contactEmailForCandidatesUpdated) {
      // updating activeProcessesProcessDataUpdated
      activeProcessesProcessDataUpdated = true;
    }

    this.setState({ activeProcessesProcessDataUpdated: activeProcessesProcessDataUpdated });
  };

  detectPopupChanges = () => {
    // initializing needed variables
    let popupDataUpdated = false;
    const firstNameUpdated = this.state.firstName !== this.state.initialFirstName;
    const lastNameUpdated = this.state.lastName !== this.state.initialLastName;
    const emailUpdated = this.state.email !== this.state.initialEmail;
    const billingContactUpdated =
      this.state.billingContactSelectedOption.value !==
      this.state.initialBillingContactSelectedOption.value;
    const testAssignedStatesUpdated =
      JSON.stringify(this.state.testAssignedStates) !==
      JSON.stringify(this.state.initialTestAssignedStates);
    let testAssignedComplementaryDataUpdated = false;

    // checking if something has been updated related to the initial assigned tests to administer complementary data (reason for testing / level required)
    for (let i = 0; i < this.state.initialTestAssignedStates.length; i++) {
      // making sure that this is an initial assigned test
      if (this.state.initialTestAssignedStates[i]) {
        // reason for testing and/or level required is different of the initial values
        if (
          this.state.initialReasonForTestingStates[i].value !==
            this.state.reasonForTestingStates[i].value ||
          this.state.initialLevelRequiredStates[i] !== this.state.levelRequiredStates[i]
        ) {
          // test assigned complementary data has been updated
          testAssignedComplementaryDataUpdated = true;
        }
      }
    }

    // if at least one input has been updated
    if (
      firstNameUpdated ||
      lastNameUpdated ||
      emailUpdated ||
      billingContactUpdated ||
      testAssignedStatesUpdated ||
      testAssignedComplementaryDataUpdated
    ) {
      // updating activeProcessesProcessDataUpdated
      popupDataUpdated = true;
    }

    this.setState({
      popupDataUpdated: popupDataUpdated,
      emailOrAssignedTestsToAdministerUpdated: emailUpdated || testAssignedStatesUpdated
    });
  };

  validateAssignCandidatesChanges = () => {
    // initializing needed variables
    let isValidAssignCandidatesChanges = false;
    const isValidDuration =
      this.state.assignCandidatesDuration !== "" &&
      this.state.assignCandidatesDuration >= this.state.highestReasonForTestingMinimumProcessLength;
    const isValidContactEmailForCandidates =
      this.state.isValidAssignCandidatesContactEmailForCandidates;

    // all needed fields are valid
    if (isValidDuration && isValidContactEmailForCandidates) {
      isValidAssignCandidatesChanges = true;
    }

    // setting state
    this.setState({ isValidAssignCandidatesChanges: isValidAssignCandidatesChanges });
  };

  validateActiveProcessesChanges = () => {
    // initializing needed variables
    let isValidActiveProcessesChanges = false;
    const isValidDuration =
      this.state.activeProcessesDuration !== "" &&
      this.state.activeProcessesDuration >= this.state.highestReasonForTestingMinimumProcessLength;
    // getting calculated closing date (sent date + duration from props/DB)
    let calculatedActiveProcessesClosingDate = new Date(
      this.props.assessmentProcessDataActiveProcesses.sent_date
    );
    calculatedActiveProcessesClosingDate.setDate(
      calculatedActiveProcessesClosingDate.getDate() +
        parseInt(this.props.assessmentProcessDataActiveProcesses.duration) +
        1
    );
    let isValidCalculatedActiveProcessesClosingDate = true;

    // triggering closing date loading
    this.setState({ isLoadingClosingDateData: true }, () => {
      // duration and contact email for candidates are valid inputs
      if (isValidDuration && this.state.isValidActiveProcessesContactEmailForCandidates) {
        // calculating closing date (sent date + duration in days)
        calculatedActiveProcessesClosingDate = new Date(
          this.props.assessmentProcessDataActiveProcesses.sent_date
        );
        calculatedActiveProcessesClosingDate.setDate(
          calculatedActiveProcessesClosingDate.getDate() +
            parseInt(this.state.activeProcessesDuration) +
            1
        );
        // setting isValidActiveProcessesChanges to true
        isValidActiveProcessesChanges = true;
      }

      // formatting calculated closing date
      calculatedActiveProcessesClosingDate = getFormattedDate(
        `${calculatedActiveProcessesClosingDate.getFullYear()}-${
          calculatedActiveProcessesClosingDate.getMonth() + 1
        }-${calculatedActiveProcessesClosingDate.getDate()}`
      );

      // checking if calculated closing date is present or future day
      let now = new Date();
      now = getFormattedDate(`${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`);
      if (calculatedActiveProcessesClosingDate < now) {
        // setting isValidCalculatedActiveProcessesClosingDate to false
        isValidCalculatedActiveProcessesClosingDate = false;
        // setting isValidActiveProcessesChanges back to false
        isValidActiveProcessesChanges = false;
      }

      this.setState({
        isValidActiveProcessesChanges: isValidActiveProcessesChanges,
        calculatedActiveProcessesClosingDate: calculatedActiveProcessesClosingDate,
        isValidCalculatedActiveProcessesClosingDate: isValidCalculatedActiveProcessesClosingDate,
        isLoadingClosingDateData: false
      });
    });
  };

  handleSaveNonSentAssessmentProcessData = () => {
    // building body
    const body = {
      id: this.props.assessmentProcessDataAssignCandidates.id,
      department_id: this.state.departmentSelectedOption.value,
      duration: this.state.assignCandidatesDuration,
      allow_booking_external_tc: this.state.assignCandidatesAllowBookingExternalTc,
      allow_last_minute_cancellation_tc: this.state.allowLastMinuteCancellationsTc,
      contact_email_for_candidates: this.state.assignCandidatesContactEmailForCandidates
    };
    this.props.updateNonSentAssessmentProcessData(body).then(response => {
      // successful request
      if (response.ok) {
        // open success popup
        this.openUpdateNonSentAssessmentProcessDataSuccessfullyPopup();
        // getting related assessment process data
        this.props
          .getAssessmentProcessData(
            this.props.selectedAssessmentProcessRefNbrAssignCandidates.value
          )
          .then(response => {
            // updating redux states with most updated data
            this.props.setAssessmentProcessDataAssignCandidates(response);
          });
        // should never happen
      } else {
        throw new Error(
          "An error occurred during the update non-sent assessment process data process"
        );
      }
    });
  };

  openUpdateNonSentAssessmentProcessDataSuccessfullyPopup = () => {
    this.setState({
      showNonSentAssessmentProcessDataUpdatedSuccessfullyPopup: true,
      assignCandidatesProcessDataUpdated: false
    });
  };

  closeUpdateNonSentAssessmentProcessDataSuccessfullyPopup = () => {
    this.setState({ showNonSentAssessmentProcessDataUpdatedSuccessfullyPopup: false });
  };

  handleSendUpdates = () => {
    this.props
      .sendAssessmentProcessUpdatesRequest(
        this.props.assessmentProcessDataActiveProcesses.id,
        this.state.activeProcessesDuration,
        this.state.activeProcessesContactEmailForCandidates
      )
      .then(response => {
        // updating redux states
        this.props.setAssessmentProcessDataActiveProcesses(response);
        // calculating closing date (sent date + duration in days)
        let calculatedActiveProcessesClosingDate = new Date(response.sent_date);
        calculatedActiveProcessesClosingDate.setDate(
          calculatedActiveProcessesClosingDate.getDate() + parseInt(response.duration) + 1
        );
        calculatedActiveProcessesClosingDate = getFormattedDate(
          `${calculatedActiveProcessesClosingDate.getFullYear()}-${
            calculatedActiveProcessesClosingDate.getMonth() + 1
          }-${calculatedActiveProcessesClosingDate.getDate()}`
        );
        // updating needed states
        this.setState({
          showAssessmentProcessDataUpdatedSuccessfullyPopup: true,
          activeProcessesDuration: response.duration,
          activeProcessesContactEmailForCandidates: response.contact_email_for_candidates,
          calculatedActiveProcessesClosingDate: calculatedActiveProcessesClosingDate,
          isValidCalculatedActiveProcessesClosingDate: true,
          activeProcessesProcessDataUpdated: false
        });
      });
  };

  closeAssessmentProcessDataUpdatedSuccessfullyPopup = () => {
    this.setState({ showAssessmentProcessDataUpdatedSuccessfullyPopup: false });
  };

  // populating billing contact options
  populateBillingContactOptions = () => {
    // initializing defaultBillingContactOptions
    const billingContactOptions = [];
    this.props.getBillingContactsList().then(response => {
      // looping in results
      for (let i = 0; i < response.length; i++) {
        billingContactOptions.push({
          label: `${response[i].first_name} ${response[i].last_name} (${response[i].email})`,
          value: response[i].id
        });
        // if current option matches the default billing contact
        if (
          response[i].id ===
          this.props[
            this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
              ? "assessmentProcessDataAssignCandidates"
              : "assessmentProcessDataActiveProcesses"
          ].default_billing_contact_id
        ) {
          // preselecting respective billing contact
          this.setState({
            billingContactSelectedOption: {
              label: `${response[i].first_name} ${response[i].last_name} (${response[i].email})`,
              value: response[i].id
            }
          });
        }
      }
      this.setState({ billingContactOptions: billingContactOptions });
    });
  };

  populateReasonForTestingOptions = () => {
    // initializing reasonForTestingOptions
    const reasonForTestingOptions = [];
    this.props.getReasonsForTesting().then(response => {
      // looping in response body
      for (let i = 0; i < response.length; i++) {
        reasonForTestingOptions.push({
          label: response[i][`reason_for_testing_name_${this.props.currentLanguage}`],
          reason_for_testing_type_codename: response[i].reason_for_testing_type_codename,
          reason_for_testing_minimum_process_length: response[i].minimum_process_length,
          value: response[i].id
        });
      }
      this.setState({ reasonForTestingOptions: reasonForTestingOptions });
    });
  };

  getRespectiveReasonsForTesting = (testSkillTypeCodename, testSkillSubTypeCodename) => {
    let respectiveReasonsForTesting = [];
    // SLE
    if (testSkillTypeCodename === TestSkillTypeCodename.SLE) {
      // READING
      if (
        testSkillSubTypeCodename === TestSkillSleDescCodename.READING_EN ||
        testSkillSubTypeCodename === TestSkillSleDescCodename.READING_FR
      ) {
        respectiveReasonsForTesting = this.state.reasonForTestingOptions.filter(
          obj =>
            obj.reason_for_testing_type_codename === REASON_FOR_TESTING_TYPE_CODENAME_CONST.READING
        );
        // WRITING
      } else if (
        testSkillSubTypeCodename === TestSkillSleDescCodename.WRITING_EN ||
        testSkillSubTypeCodename === TestSkillSleDescCodename.WRITING_FR
      ) {
        respectiveReasonsForTesting = this.state.reasonForTestingOptions.filter(
          obj =>
            obj.reason_for_testing_type_codename === REASON_FOR_TESTING_TYPE_CODENAME_CONST.WRITING
        );
        // ORAL
      } else if (
        testSkillSubTypeCodename === TestSkillSleDescCodename.ORAL_EN ||
        testSkillSubTypeCodename === TestSkillSleDescCodename.ORAL_FR
      ) {
        respectiveReasonsForTesting = this.state.reasonForTestingOptions.filter(
          obj =>
            obj.reason_for_testing_type_codename === REASON_FOR_TESTING_TYPE_CODENAME_CONST.ORAL
        );
        // unsupported sub type (should never happen)
      } else {
        throw new Error("Unsupported SLE test skill sub type");
      }
      // OTHER
    } else {
      // getting "Reading" reasons for testing by default
      respectiveReasonsForTesting = this.state.reasonForTestingOptions.filter(
        obj =>
          obj.reason_for_testing_type_codename === REASON_FOR_TESTING_TYPE_CODENAME_CONST.READING
      );
    }

    return respectiveReasonsForTesting;
  };

  // populating tests to administer table
  populateTestsToAdministerTable = () => {
    // initializing needed object and array for testsToAdministerRowsDefinition props (needed for GenericTable component)
    let testsToAdministerRowsDefinition = {};
    const data = [];
    // making sure that the respective object is not empty
    if (
      Object.keys(
        this.props[
          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
            ? "assessmentProcessDataAssignCandidates"
            : "assessmentProcessDataActiveProcesses"
        ]
      ).length > 0
    ) {
      // looping in response given
      for (
        let i = 0;
        i <
        this.props[
          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
            ? "assessmentProcessDataAssignCandidates"
            : "assessmentProcessDataActiveProcesses"
        ].tests_to_administer.length;
        i++
      ) {
        const currentResult =
          this.props[
            this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
              ? "assessmentProcessDataAssignCandidates"
              : "assessmentProcessDataActiveProcesses"
          ].tests_to_administer[i];
        // formatting test skill
        const formattedTestSkill =
          currentResult.test_skill_sub_type_id !== null
            ? `${currentResult[`test_skill_type_name_${this.props.currentLanguage}`]} - ${
                currentResult[`test_skill_sub_type_name_${this.props.currentLanguage}`]
              }`
            : `${currentResult[`test_skill_type_name_${this.props.currentLanguage}`]}`;
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: formattedTestSkill,
          column_2:
            currentResult[`reason_for_testing_name_${this.props.currentLanguage}`] !== null
              ? currentResult[`reason_for_testing_name_${this.props.currentLanguage}`]
              : LOCALIZE.commons.na,
          column_3:
            currentResult.level_required !== null
              ? currentResult.level_required
              : LOCALIZE.commons.na,
          column_4: (
            <StyledTooltip
              id={`assign-candidates-view-available-test-sessions-row-${i}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`assign-candidates-view-available-test-sessions-row-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faCalendarCheck} style={styles.tableIcon} />
                      </>
                    }
                    action={() => {
                      this.openViewAvailableTestSessionsPopup(i);
                    }}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.PRIMARY}
                    ariaLabel={LOCALIZE.formatString(
                      LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                        .testsToAdministerTable.viewAvailableTestSessionsAccessibility,
                      formattedTestSkill
                    )}
                    disabled={
                      currentResult.test_skill_sub_type_codename ===
                        TestSkillSleDescCodename.ORAL_EN ||
                      currentResult.test_skill_sub_type_codename ===
                        TestSkillSleDescCodename.ORAL_FR
                        ? BUTTON_STATE.disabled
                        : BUTTON_STATE.enabled
                    }
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                        .testsToAdministerTable.viewAvailableTestSessions
                    }
                  </p>
                </div>
              }
            />
          ),
          test_skill_type_id: currentResult.test_skill_type_id,
          test_skill_sub_type_id: currentResult.test_skill_sub_type_id
        });
      }

      // updating rowsDefinition object with provided data and needed style
      testsToAdministerRowsDefinition = {
        column_1_style: COMMON_STYLE.LEFT_TEXT,
        column_2_style: COMMON_STYLE.LEFT_TEXT,
        column_3_style: COMMON_STYLE.CENTERED_TEXT,
        column_4_style: COMMON_STYLE.CENTERED_TEXT,
        data: data
      };
    }

    if (this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES) {
      // saving results in state
      this.setState({
        assignCandidatesTestsToAdministerRowsDefinition: testsToAdministerRowsDefinition,
        isLoadingTestsToAdminister: false
      });
    } else {
      // saving results in state
      this.setState({
        activeProcessesTestsToAdministerRowsDefinition: testsToAdministerRowsDefinition,
        isLoadingTestsToAdminister: false
      });
    }
  };

  // Populate Last Row Content
  populateLastRowHtml = () => {
    return (
      <td
        id="assign-candidate-add-candidate-button"
        colSpan="7"
        style={alternateColorsStyle(this.state.assignedCandidatesRowsDefinition.data.length, 60)}
      >
        <CustomButton
          label={
            <>
              <span style={styles.addCandidateButtonIcon}>
                <FontAwesomeIcon icon={faPlusCircle} />
              </span>
              <span>
                {this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                  ? LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                      .assignedCandidatesTable.addCandidateButton
                  : LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                      .assignedCandidatesTable.inviteCandidateButton}
              </span>
            </>
          }
          action={this.openAddCandidatePopup}
          customStyle={styles.addCandidateButton}
        />
      </td>
    );
  };

  // Populate Empty Table with Generate Button
  populateEmptyTable = () => {
    return (
      <CustomButton
        label={
          <>
            <span style={styles.addCandidateButtonIcon}>
              <FontAwesomeIcon icon={faPlusCircle} />
            </span>
            <span>
              {this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                    .assignedCandidatesTable.addCandidateButton
                : LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                    .assignedCandidatesTable.inviteCandidateButton}
            </span>
          </>
        }
        action={this.openAddCandidatePopup}
        customStyle={styles.addCandidateButton}
      />
    );
  };

  updateTestsToAdministerTableInPopup = () => {
    // initializing needed object and array for testsToAdministerInPopupRowsDefinition props (needed for GenericTable component)
    let testsToAdministerInPopupRowsDefinition = {};
    const data = [];
    // looping in response given
    for (
      let i = 0;
      i <
      this.props[
        this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
          ? "assessmentProcessDataAssignCandidates"
          : "assessmentProcessDataActiveProcesses"
      ].tests_to_administer.length;
      i++
    ) {
      const currentResult =
        this.props[
          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
            ? "assessmentProcessDataAssignCandidates"
            : "assessmentProcessDataActiveProcesses"
        ].tests_to_administer[i];
      // getting assigned test specs related status
      let assignedTestSpecsRelatedStatus = LOCALIZE.commons.na;
      if (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES) {
        // making sure that the data array is defined
        if (
          typeof this.state.assignedCandidatesRowsDefinition.data[
            this.state.indexOfRowToEditOrDelete
          ] !== "undefined"
        ) {
          // getting related complementary data
          const currentIterationRelatedComplementaryData =
            this.state.assignedCandidatesRowsDefinition.data[
              this.state.indexOfRowToEditOrDelete
            ].candidate_assigned_test_data.filter(
              obj => obj.id === this.state.assignedTestSpecsIdsToBeUpdated[i]
            )[0];
          // making sure that the currentIterationRelatedComplementaryData is defined
          if (typeof currentIterationRelatedComplementaryData !== "undefined") {
            // getting assigned test specs related status
            assignedTestSpecsRelatedStatus = getAssignedTestSpecsRelatedStatus(
              currentIterationRelatedComplementaryData
            );
          }
        }
      }
      // formatting test skill
      const formattedTestSkill =
        currentResult.test_skill_sub_type_id !== null
          ? `${currentResult[`test_skill_type_name_${this.props.currentLanguage}`]} - ${
              currentResult[`test_skill_sub_type_name_${this.props.currentLanguage}`]
            }`
          : `${currentResult[`test_skill_type_name_${this.props.currentLanguage}`]}`;
      // pushing needed data in data array (needed for GenericTable component)
      data.push({
        column_1: formattedTestSkill,
        column_2: (
          <DropdownSelect
            idPrefix="assign-candidate-add-candidate-popup-reason-for-testing"
            isValid={true}
            ariaRequired={true}
            hasPlaceholder={true}
            orderByLabels={false}
            options={this.getRespectiveReasonsForTesting(
              currentResult.test_skill_type_codename,
              currentResult.test_skill_sub_type_codename
            )}
            onChange={e => this.getSelectedReasonForTestingOption(e, i)}
            defaultValue={this.state.reasonForTestingStates[i]}
          />
        ),
        column_3: (
          <input
            id="assign-candidate-add-candidate-popup-level-required-input"
            className={"valid-field"}
            aria-required={true}
            type="text"
            style={styles.input}
            value={this.state.levelRequiredStates[i]}
            onChange={e => this.getLevelRequiredContent(e, i)}
          ></input>
        ),
        column_4: (
          <Switch
            className="switch-vertical-align-middle"
            onChange={e => this.changeTestAssignedState(e, i)}
            checked={this.state.testAssignedStates[i]}
            aria-labelledby={`item-bank-bundles-bundles-content-shuffle-bundles-${this.props.index}`}
            height={this.state.switchHeight}
            width={this.state.switchWidth}
            disabled={
              this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES &&
              this.state.initialTestAssignedStates[i]
                ? BUTTON_STATE.disabled
                : BUTTON_STATE.enabled
            }
          />
        )
      });

      if (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES) {
        data[i].column_5 = (
          <StyledTooltip
            id={`assessment-process-active-processes-unassign-test-${i}`}
            place="top"
            variant={TYPE.light}
            effect={EFFECT.solid}
            openOnClick={false}
            tooltipElement={
              <div style={styles.allUnset}>
                <CustomButton
                  dataTip=""
                  dataFor={`assessment-process-active-processes-unassign-test-${i}`}
                  label={
                    <>
                      <FontAwesomeIcon icon={faTimesCircle} style={styles.tableIcon} />
                    </>
                  }
                  action={() => this.openUnassignTestPopup(i)}
                  customStyle={{ ...styles.actionButton, ...styles.redButton }}
                  buttonTheme={THEME.PRIMARY}
                  ariaLabel={LOCALIZE.formatString(
                    LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses.editCandidatePopup
                      .unassignTestAccessibility,
                    formattedTestSkill
                  )}
                  disabled={
                    this.state.initialTestAssignedStates[i] &&
                    assignedTestSpecsRelatedStatus !== LOCALIZE.commons.status.submitted &&
                    assignedTestSpecsRelatedStatus !== LOCALIZE.commons.status.quit &&
                    assignedTestSpecsRelatedStatus !== LOCALIZE.commons.status.unassigned &&
                    assignedTestSpecsRelatedStatus !==
                      LOCALIZE.commons.reservationCodeStatus.revoked
                      ? BUTTON_STATE.enabled
                      : BUTTON_STATE.disabled
                  }
                />
              </div>
            }
            tooltipContent={
              <div>
                <p>
                  {
                    LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses.editCandidatePopup
                      .unassignTestTooltip
                  }
                </p>
              </div>
            }
          />
        );
        data[i].column_6 = assignedTestSpecsRelatedStatus;
      }
    }

    // updating rowsDefinition object with provided data and needed style
    testsToAdministerInPopupRowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.LEFT_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    if (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES) {
      testsToAdministerInPopupRowsDefinition.column_5_style = COMMON_STYLE.CENTERED_TEXT;
      testsToAdministerInPopupRowsDefinition.column_6_style = COMMON_STYLE.CENTERED_TEXT;
    }

    // saving results in state
    this.setState(
      {
        testsToAdministerInPopupRowsDefinition: testsToAdministerInPopupRowsDefinition
      },
      () => {
        // validating form
        this.validateAddEditCandidateForm();
      }
    );
  };

  openViewAvailableTestSessionsPopup = index => {
    this.setState(
      {
        availableTestSessionsCurrentlyLoading: true,
        showViewAvailableTestSessionsPopup: true,
        indexOfRowToView: index
      },
      () => {
        // getting available test sessions
        this.props
          .getAvailableTestSessionsForSpecificTestToAdminister(
            this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
              ? this.state.assignCandidatesDuration
              : this.state.activeProcessesDuration,
            this.state[
              this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? "assignCandidatesTestsToAdministerRowsDefinition"
                : "activeProcessesTestsToAdministerRowsDefinition"
            ].data[index].test_skill_type_id,
            this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
              ? this.state.assignCandidatesAllowBookingExternalTc
              : this.props.assessmentProcessDataActiveProcesses.allow_booking_external_tc,
            this.state[
              this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? "assignCandidatesTestsToAdministerRowsDefinition"
                : "activeProcessesTestsToAdministerRowsDefinition"
            ].data[index].test_skill_sub_type_id,
            this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
              ? this.state.departmentSelectedOption.value
              : this.props.assessmentProcessDataActiveProcesses.dept_id,
            this.props[
              this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? "assessmentProcessDataAssignCandidates"
                : "assessmentProcessDataActiveProcesses"
            ].id
          )
          .then(response => {
            // initializing needed object and array for availableTestSessionsRowsDefinition state (needed for GenericTable component)
            let availableTestSessionsRowsDefinition = {};
            const data = [];
            for (let i = 0; i < response.length; i++) {
              // convert time
              // Converting time to users timezone
              const adjustedStartTime = convertDateTimeToUsersTimezone(
                response[i].start_time
              ).adjustedTime;

              const adjustedEndTime = convertDateTimeToUsersTimezone(
                response[i].end_time
              ).adjustedTime;

              // pushing needed data in data array (needed for GenericTable component)
              data.push({
                column_1: `${response[i].date} ${adjustedStartTime} - ${adjustedEndTime}`,
                column_2: response[i].test_center_name,
                column_3:
                  this.props.currentLanguage === LANGUAGES.english
                    ? `${response[i].dept_edesc} (${response[i].dept_eabrv})`
                    : `${response[i].dept_fdesc} (${response[i].dept_fabrv})`,
                column_4: response[i].test_center_city,
                column_5: `${response[i].spaces_unbooked} / ${response[i].spaces_available}`
              });
            }

            // updating availableTestSessionsRowsDefinition object with provided data and needed style
            availableTestSessionsRowsDefinition = {
              column_1_style: COMMON_STYLE.LEFT_TEXT,
              column_2_style: COMMON_STYLE.LEFT_TEXT,
              column_3_style: COMMON_STYLE.LEFT_TEXT,
              column_4_style: COMMON_STYLE.LEFT_TEXT,
              column_5_style: COMMON_STYLE.CENTERED_TEXT,
              data: data
            };
            // updating needed states
            this.setState({
              availableTestSessionsRowsDefinition: availableTestSessionsRowsDefinition,
              availableTestSessionsCurrentlyLoading: false
            });
          });
      }
    );
  };

  closeViewAvailableTestSessionsPopup = () => {
    this.setState({ showViewAvailableTestSessionsPopup: false, indexOfRowToView: null });
  };

  openAddCandidatePopup = () => {
    // default billing contact is defined
    if (
      this.props[
        this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
          ? "assessmentProcessDataAssignCandidates"
          : "assessmentProcessDataActiveProcesses"
      ].default_billing_contact_id !== null
    ) {
      // looping in billingContactOptions
      for (let i = 0; i < this.state.billingContactOptions.length; i++) {
        // if current option matches the default billing contact
        if (
          this.state.billingContactOptions[i].value ===
          this.props[
            this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
              ? "assessmentProcessDataAssignCandidates"
              : "assessmentProcessDataActiveProcesses"
          ].default_billing_contact_id
        ) {
          // preselecting respective billing contact
          this.setState({
            billingContactSelectedOption: {
              label: this.state.billingContactOptions[i].label,
              value: this.state.billingContactOptions[i].value
            }
          });
          break;
        }
      }
    }
    // populating tests to administer table (in popup)
    this.populateTestsToAdministerTableInPopup();
    this.setState({ showAddCandidatePopup: true });
  };

  closeAddCandidatePopup = () => {
    this.setState({
      showAddCandidatePopup: false,
      initialFirstName: "",
      firstName: "",
      isValidFirstName: true,
      initialLastName: "",
      lastName: "",
      isValidLastName: true,
      initialEmail: "",
      email: "",
      isValidEmail: true,
      showDuplicateEmailFoundError: false,
      billingContactSelectedOption: [],
      initialReasonForTestingStates: [],
      reasonForTestingStates: [],
      initialLevelRequiredStates: [],
      levelRequiredStates: [],
      testAssignedStates: [],
      initialTestAssignedStates: [],
      isValidAddEditCandidateForm: false,
      popupDataUpdated: false,
      emailOrAssignedTestsToAdministerUpdated: false
    });
  };

  populateTestsToAdministerTableInPopup = (editMode = false) => {
    // initializing needed object and array for testsToAdministerInPopupRowsDefinition props (needed for GenericTable component)
    let testsToAdministerInPopupRowsDefinition = {};
    const data = [];
    // initializing reasonForTestingStates, levelRequiredStates and testAssignedStates
    // creating a copy states
    const copyOfReasonForTestingStates = _.cloneDeep(this.state.reasonForTestingStates);
    const copyOfLevelRequiredStates = _.cloneDeep(this.state.levelRequiredStates);
    const copyOfTestAssignedStates = _.cloneDeep(this.state.testAssignedStates);
    // looping in response given
    for (
      let i = 0;
      i <
      this.props[
        this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
          ? "assessmentProcessDataAssignCandidates"
          : "assessmentProcessDataActiveProcesses"
      ].tests_to_administer.length;
      i++
    ) {
      const currentResult =
        this.props[
          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
            ? "assessmentProcessDataAssignCandidates"
            : "assessmentProcessDataActiveProcesses"
        ].tests_to_administer[i];
      // not in edit mode (need to initialize the reasonForTestingStates, levelRequiredStates and testAssignedStates states)
      if (!editMode) {
        // pushing respective data to reasonForTestingStates array
        if (currentResult.reason_for_testing_id !== null) {
          copyOfReasonForTestingStates.push({
            label: currentResult[`reason_for_testing_name_${this.props.currentLanguage}`],
            value: currentResult.reason_for_testing_id
          });
        } else {
          copyOfReasonForTestingStates.push([]);
        }
        // pushing respective data to levelRequiredStates array
        if (currentResult.level_required !== null) {
          copyOfLevelRequiredStates.push(currentResult.level_required);
        } else {
          copyOfLevelRequiredStates.push("");
        }
        // pushing new "false" value to testAssignedStates array
        copyOfTestAssignedStates.push(false);
      }
    }
    // updating states
    this.setState(
      {
        initialReasonForTestingStates: copyOfReasonForTestingStates,
        reasonForTestingStates: copyOfReasonForTestingStates,
        initialLevelRequiredStates: copyOfLevelRequiredStates,
        levelRequiredStates: copyOfLevelRequiredStates,
        initialTestAssignedStates: copyOfTestAssignedStates,
        testAssignedStates: copyOfTestAssignedStates
      },
      () => {
        // looping in response given
        for (
          let i = 0;
          i <
          this.props[
            this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
              ? "assessmentProcessDataAssignCandidates"
              : "assessmentProcessDataActiveProcesses"
          ].tests_to_administer.length;
          i++
        ) {
          const currentResult =
            this.props[
              this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? "assessmentProcessDataAssignCandidates"
                : "assessmentProcessDataActiveProcesses"
            ].tests_to_administer[i];
          // getting assigned test specs related status
          let assignedTestSpecsRelatedStatus = LOCALIZE.commons.na;
          if (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES) {
            // making sure that the data array is defined
            if (
              typeof this.state.assignedCandidatesRowsDefinition.data[
                this.state.indexOfRowToEditOrDelete
              ] !== "undefined"
            ) {
              // getting related complementary data
              const currentIterationRelatedComplementaryData =
                this.state.assignedCandidatesRowsDefinition.data[
                  this.state.indexOfRowToEditOrDelete
                ].candidate_assigned_test_data.filter(
                  obj => obj.id === this.state.assignedTestSpecsIdsToBeUpdated[i]
                )[0];
              // making sure that the currentIterationRelatedComplementaryData is defined
              if (typeof currentIterationRelatedComplementaryData !== "undefined") {
                assignedTestSpecsRelatedStatus = getAssignedTestSpecsRelatedStatus(
                  currentIterationRelatedComplementaryData
                );
              }
            }
          }
          // formatting test skill
          const formattedTestSkill =
            currentResult.test_skill_sub_type_id !== null
              ? `${currentResult[`test_skill_type_name_${this.props.currentLanguage}`]} - ${
                  currentResult[`test_skill_sub_type_name_${this.props.currentLanguage}`]
                }`
              : `${currentResult[`test_skill_type_name_${this.props.currentLanguage}`]}`;
          // pushing needed data in data array (needed for GenericTable component)
          data.push({
            column_1: formattedTestSkill,
            column_2: (
              <DropdownSelect
                idPrefix="assign-candidate-add-candidate-popup-reason-for-testing"
                isValid={true}
                ariaRequired={true}
                hasPlaceholder={true}
                orderByLabels={false}
                options={this.getRespectiveReasonsForTesting(
                  currentResult.test_skill_type_codename,
                  currentResult.test_skill_sub_type_codename
                )}
                onChange={e => this.getSelectedReasonForTestingOption(e, i)}
                defaultValue={this.state.reasonForTestingStates[i]}
                isDisabled={
                  assignedTestSpecsRelatedStatus ===
                    LOCALIZE.commons.reservationCodeStatus.revoked ||
                  assignedTestSpecsRelatedStatus ===
                    LOCALIZE.commons.reservationCodeStatus.withdrawn
                    ? BUTTON_STATE.disabled
                    : BUTTON_STATE.enabled
                }
              />
            ),
            column_3: (
              <input
                id="assign-candidate-add-candidate-popup-level-required-input"
                className={"valid-field"}
                aria-required={true}
                type="text"
                style={styles.input}
                value={this.state.levelRequiredStates[i]}
                onChange={e => this.getLevelRequiredContent(e, i)}
                disabled={
                  assignedTestSpecsRelatedStatus ===
                    LOCALIZE.commons.reservationCodeStatus.revoked ||
                  assignedTestSpecsRelatedStatus ===
                    LOCALIZE.commons.reservationCodeStatus.withdrawn
                    ? BUTTON_STATE.disabled
                    : BUTTON_STATE.enabled
                }
              ></input>
            ),
            column_4: (
              <Switch
                className="switch-vertical-align-middle"
                onChange={e => this.changeTestAssignedState(e, i)}
                checked={this.state.testAssignedStates[i]}
                aria-labelledby={`item-bank-bundles-bundles-content-shuffle-bundles-${this.props.index}`}
                height={this.state.switchHeight}
                width={this.state.switchWidth}
                disabled={
                  this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES &&
                  this.state.initialTestAssignedStates[i]
                    ? BUTTON_STATE.disabled
                    : BUTTON_STATE.enabled
                }
              />
            )
          });

          if (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES) {
            data[i].column_5 = (
              <StyledTooltip
                id={`assessment-process-active-processes-unassign-test-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`assessment-process-active-processes-unassign-test-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faTimesCircle} style={styles.tableIcon} />
                        </>
                      }
                      action={() => this.openUnassignTestPopup(i)}
                      customStyle={{ ...styles.actionButton, ...styles.redButton }}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses
                          .editCandidatePopup.unassignTestAccessibility,
                        formattedTestSkill
                      )}
                      disabled={
                        this.state.initialTestAssignedStates[i] &&
                        assignedTestSpecsRelatedStatus !== LOCALIZE.commons.status.submitted &&
                        assignedTestSpecsRelatedStatus !== LOCALIZE.commons.status.quit &&
                        assignedTestSpecsRelatedStatus !== LOCALIZE.commons.status.unassigned &&
                        assignedTestSpecsRelatedStatus !== LOCALIZE.commons.status.active &&
                        assignedTestSpecsRelatedStatus !==
                          LOCALIZE.commons.reservationCodeStatus.revoked &&
                        assignedTestSpecsRelatedStatus !==
                          LOCALIZE.commons.reservationCodeStatus.withdrawn
                          ? BUTTON_STATE.enabled
                          : BUTTON_STATE.disabled
                      }
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses
                          .editCandidatePopup.unassignTestTooltip
                      }
                    </p>
                  </div>
                }
              />
            );
            data[i].column_6 = assignedTestSpecsRelatedStatus;
          }
        }

        // updating rowsDefinition object with provided data and needed style
        testsToAdministerInPopupRowsDefinition = {
          column_1_style: COMMON_STYLE.LEFT_TEXT,
          column_2_style: COMMON_STYLE.LEFT_TEXT,
          column_3_style: COMMON_STYLE.CENTERED_TEXT,
          column_4_style: COMMON_STYLE.CENTERED_TEXT,
          data: data
        };

        if (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES) {
          testsToAdministerInPopupRowsDefinition.column_5_style = COMMON_STYLE.CENTERED_TEXT;
          testsToAdministerInPopupRowsDefinition.column_6_style = COMMON_STYLE.CENTERED_TEXT;
        }

        // saving results in state
        this.setState({
          testsToAdministerInPopupRowsDefinition: testsToAdministerInPopupRowsDefinition
        });
      }
    );
  };

  getFirstNameContent = event => {
    const firstName = event.target.value;
    // allow maximum of 30 chars
    const regexExpression = /^(.{0,30})$/;
    if (regexExpression.test(firstName)) {
      this.setState({ firstName: firstName }, () => {
        // validating form
        this.validateAddEditCandidateForm();
        if (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES) {
          // detecting changes
          this.detectPopupChanges();
        }
      });
    }
  };

  getLastNameContent = event => {
    const lastName = event.target.value;
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(lastName)) {
      this.setState({ lastName: lastName }, () => {
        // validating form
        this.validateAddEditCandidateForm();
        if (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES) {
          // detecting changes
          this.detectPopupChanges();
        }
      });
    }
  };

  getEmailContent = event => {
    const email = event.target.value;
    this.setState({ email: email, showDuplicateEmailFoundError: false }, () => {
      // validating form
      this.validateAddEditCandidateForm();
      if (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES) {
        // detecting changes
        this.detectPopupChanges();
      }
    });
  };

  getSelectedBillingContactOption = selectedOption => {
    let formattedSelectedOption = selectedOption;
    // selected option is null (select option)
    if (selectedOption.value === null) {
      formattedSelectedOption = [];
    }
    this.setState(
      {
        billingContactSelectedOption: formattedSelectedOption
      },
      () => {
        // validating form
        this.validateAddEditCandidateForm();
        if (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES) {
          // detecting changes
          this.detectPopupChanges();
        }
      }
    );
  };

  getSelectedReasonForTestingOption = (event, index) => {
    // creating a copy of testAssignedStates
    const copyOfReasonForTestingStates = _.cloneDeep(this.state.reasonForTestingStates);
    // updating needed index
    copyOfReasonForTestingStates[index] = event;
    this.setState({ reasonForTestingStates: copyOfReasonForTestingStates }, () => {
      // updating table
      this.updateTestsToAdministerTableInPopup();
      if (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES) {
        // detecting changes
        this.detectPopupChanges();
      }
    });
  };

  getLevelRequiredContent = (event, index) => {
    // creating a copy of testAssignedStates
    const copyOfLevelRequiredStates = _.cloneDeep(this.state.levelRequiredStates);
    // updating needed index
    // allow only A/B/C
    const regexExpression = /^([a,A,b,B,c,C]{0,1})$/;
    if (regexExpression.test(event.target.value)) {
      copyOfLevelRequiredStates[index] = event.target.value.toUpperCase();
      this.setState({ levelRequiredStates: copyOfLevelRequiredStates }, () => {
        // updating table
        this.updateTestsToAdministerTableInPopup();
        if (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES) {
          // detecting changes
          this.detectPopupChanges();
        }
      });
    }
  };

  changeTestAssignedState = (event, index) => {
    // creating a copy of testAssignedStates
    const copyOfTestAssignedStates = _.cloneDeep(this.state.testAssignedStates);
    // updating needed index
    copyOfTestAssignedStates[index] = event;
    this.setState({ testAssignedStates: copyOfTestAssignedStates }, () => {
      // updating table
      this.updateTestsToAdministerTableInPopup();
      if (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES) {
        // detecting changes
        this.detectPopupChanges();
      }
    });
  };

  validateAddEditCandidateForm = () => {
    // initializing needed variables
    let isValidAddEditCandidateForm = false;

    const isValidFirstName =
      this.state.firstName !== "" ? validateName(this.state.firstName) : true;
    const isValidLastName = this.state.lastName !== "" ? validateName(this.state.lastName) : true;
    const isValidEmail = this.state.email !== "" ? validateEmail(this.state.email) : true;
    const isValidBillingContact = Object.keys(this.state.billingContactSelectedOption).length > 0;
    // making sure that at least one test is assigned
    let isValidTestsToAdminister =
      this.state.testAssignedStates.filter(obj => obj === true).length > 0;

    // if at least one test is set to be assigned
    if (isValidTestsToAdminister) {
      // validating tests to administer
      for (
        let i = 0;
        i <
        this.props[
          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
            ? "assessmentProcessDataAssignCandidates"
            : "assessmentProcessDataActiveProcesses"
        ].tests_to_administer.length;
        i++
      ) {
        // if test assigned
        if (this.state.testAssignedStates[i]) {
          // making sure that the reason for testing and the level required are provided
          if (
            Object.keys(this.state.reasonForTestingStates[i]).length <= 0 ||
            this.state.levelRequiredStates[i] === ""
          ) {
            // setting isValidTestsToAdminister to false
            isValidTestsToAdminister = false;
            break;
          }
        }
      }
    }

    // if all mandatory fields are valid
    if (
      isValidFirstName &&
      this.state.firstName !== "" &&
      isValidLastName &&
      this.state.lastName !== "" &&
      isValidEmail &&
      this.state.email !== "" &&
      isValidBillingContact &&
      isValidTestsToAdminister
    ) {
      isValidAddEditCandidateForm = true;
    }
    this.setState({
      isValidAddEditCandidateForm: isValidAddEditCandidateForm,
      isValidFirstName: isValidFirstName,
      isValidLastName: isValidLastName,
      isValidEmail: isValidEmail
    });
  };

  handleAddCandidate = () => {
    // formatting final array to send to the backend
    // initializing finalArray
    const finalArray = [];
    // looping in assessmentProcessDataAssignCandidates (tests_to_administer)
    for (
      let i = 0;
      i <
      this.props[
        this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
          ? "assessmentProcessDataAssignCandidates"
          : "assessmentProcessDataActiveProcesses"
      ].tests_to_administer.length;
      i++
    ) {
      // test of current iteration has been assigned
      if (this.state.testAssignedStates[i]) {
        // creating new assigned test obj
        const newAssignedTestObj = {
          first_name: this.state.firstName,
          last_name: this.state.lastName,
          email: this.state.email,
          assessment_process_id:
            this.props[
              this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? "assessmentProcessDataAssignCandidates"
                : "assessmentProcessDataActiveProcesses"
            ].id,
          test_skill_type_id:
            this.props[
              this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? "assessmentProcessDataAssignCandidates"
                : "assessmentProcessDataActiveProcesses"
            ].tests_to_administer[i].test_skill_type_id,
          test_skill_sub_type_id:
            this.props[
              this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? "assessmentProcessDataAssignCandidates"
                : "assessmentProcessDataActiveProcesses"
            ].tests_to_administer[i].test_skill_sub_type_id,
          billing_contact_id: this.state.billingContactSelectedOption.value,
          reason_for_testing_id: this.state.reasonForTestingStates[i].value,
          level_required: this.state.levelRequiredStates[i]
        };
        // adding new object to finalArray
        finalArray.push(newAssignedTestObj);
      }
    }
    // assigning candidate
    this.props.assessmentProcessAssignCandidate(finalArray).then(response => {
      if (response.ok) {
        // populating assigned candidates table
        this.populateAssignedCandidatesTable();
        // closing popup
        this.closeAddCandidatePopup();
        // duplicate email found
      } else if (response.status === 409) {
        this.setState(
          { showDuplicateEmailFoundError: true, isValidAddEditCandidateForm: false },
          () => {
            // focus on repective field
            document.getElementById("assign-candidate-email-input").focus();
          }
        );
        // should never happen
      } else {
        throw new Error("An error occurred during the assessment process assign candidate process");
      }
    });
  };

  handleInviteCandidate = () => {
    // formatting final array to send to the backend
    // initializing finalArray
    const finalArray = [];
    // looping in assessmentProcessDataActiveProcesses (tests_to_administer)
    for (
      let i = 0;
      i < this.props.assessmentProcessDataActiveProcesses.tests_to_administer.length;
      i++
    ) {
      // test of current iteration has been assigned
      if (this.state.testAssignedStates[i]) {
        // creating new assigned test obj
        const newAssignedTestObj = {
          first_name: this.state.firstName,
          last_name: this.state.lastName,
          email: this.state.email,
          assessment_process_id: this.props.assessmentProcessDataActiveProcesses.id,
          test_skill_type_id:
            this.props.assessmentProcessDataActiveProcesses.tests_to_administer[i]
              .test_skill_type_id,
          test_skill_sub_type_id:
            this.props.assessmentProcessDataActiveProcesses.tests_to_administer[i]
              .test_skill_sub_type_id,
          billing_contact_id: this.state.billingContactSelectedOption.value,
          reason_for_testing_id: this.state.reasonForTestingStates[i].value,
          level_required: this.state.levelRequiredStates[i]
        };
        // adding new object to finalArray
        finalArray.push(newAssignedTestObj);
      }
    }
    this.props.inviteSingleAssessmentProcessCandidateRequest(finalArray).then(response => {
      if (response.ok) {
        // populating assigned candidates table
        this.populateAssignedCandidatesTable();
        // closing popup
        this.closeAddCandidatePopup();
        // duplicate email found
      } else if (response.status === 409) {
        this.setState(
          { showDuplicateEmailFoundError: true, isValidAddEditCandidateForm: false },
          () => {
            // focus on repective field
            document.getElementById("assign-candidate-email-input").focus();
          }
        );
        // should never happen
      } else {
        throw new Error(
          "An error occurred during the invite single assessment process assign candidate process"
        );
      }
    });
  };

  handleSendAssignedCandidateUpdates = () => {
    // initializing needed arraus
    const newTestsToAdministerArray = [];
    const alreadyAssignedTestsToAdministerArray = [];

    // checking if the email has been updated
    const emailUpdated = this.state.email !== this.state.initialEmail;

    // formatting final array to send to the backend
    // looping in assessmentProcessDataActiveProcesses (tests_to_administer)
    for (
      let i = 0;
      i < this.props.assessmentProcessDataActiveProcesses.tests_to_administer.length;
      i++
    ) {
      // getting respective assigned test specs status
      let assignedTestSpecsRelatedStatus = LOCALIZE.commons.na;
      // getting related complementary data
      const currentIterationRelatedComplementaryData =
        this.state.assignedCandidatesRowsDefinition.data[
          this.state.indexOfRowToEditOrDelete
        ].candidate_assigned_test_data.filter(
          obj => obj.id === this.state.assignedTestSpecsIdsToBeUpdated[i]
        )[0];
      // making sure that the currentIterationRelatedComplementaryData is defined
      if (typeof currentIterationRelatedComplementaryData !== "undefined") {
        // getting status of the test for the current iteration
        assignedTestSpecsRelatedStatus = getAssignedTestSpecsRelatedStatus(
          currentIterationRelatedComplementaryData
        );
      }
      // test of current iteration has been added to be assigned
      if (this.state.testAssignedStates[i] && !this.state.initialTestAssignedStates[i]) {
        // creating new assigned test obj
        const newAssignedTestObj = {
          first_name: this.state.firstName,
          last_name: this.state.lastName,
          email: this.state.email,
          assessment_process_id: this.props.assessmentProcessDataActiveProcesses.id,
          test_skill_type_id:
            this.props.assessmentProcessDataActiveProcesses.tests_to_administer[i]
              .test_skill_type_id,
          test_skill_sub_type_id:
            this.props.assessmentProcessDataActiveProcesses.tests_to_administer[i]
              .test_skill_sub_type_id,
          billing_contact_id: this.state.billingContactSelectedOption.value,
          reason_for_testing_id: this.state.reasonForTestingStates[i].value,
          level_required: this.state.levelRequiredStates[i],
          assessment_process_assigned_test_specs_id: this.state.assignedTestSpecsIdsToBeUpdated[i],
          ids_to_be_updated: this.state.assignedTestSpecsIdsToBeUpdated
        };
        // adding new object to newTestsToAdministerArray
        newTestsToAdministerArray.push(newAssignedTestObj);
      }
      // test of current iteration has already been assigned before
      if (this.state.testAssignedStates[i] && this.state.initialTestAssignedStates[i]) {
        // creating new assigned test obj
        const newAssignedTestObj = {
          first_name: this.state.firstName,
          last_name: this.state.lastName,
          email: this.state.email,
          assessment_process_id: this.props.assessmentProcessDataActiveProcesses.id,
          test_skill_type_id:
            this.props.assessmentProcessDataActiveProcesses.tests_to_administer[i]
              .test_skill_type_id,
          test_skill_sub_type_id:
            this.props.assessmentProcessDataActiveProcesses.tests_to_administer[i]
              .test_skill_sub_type_id,
          billing_contact_id: this.state.billingContactSelectedOption.value,
          reason_for_testing_id: this.state.reasonForTestingStates[i].value,
          level_required: this.state.levelRequiredStates[i],
          assessment_process_assigned_test_specs_id: this.state.assignedTestSpecsIdsToBeUpdated[i],
          ids_to_be_updated: this.state.assignedTestSpecsIdsToBeUpdated,
          // only sending email(s) if status is SENT, CONSUMED or BOOKED
          send_email:
            assignedTestSpecsRelatedStatus === LOCALIZE.commons.sent ||
            assignedTestSpecsRelatedStatus === LOCALIZE.commons.reservationCodeStatus.consumed ||
            assignedTestSpecsRelatedStatus === LOCALIZE.commons.reservationCodeStatus.booked
        };
        // adding new object to alreadyAssignedTestsToAdministerArray
        alreadyAssignedTestsToAdministerArray.push(newAssignedTestObj);
      }
    }
    // creating request body
    const body = {
      called_from_active_processes: true,
      email_updated: emailUpdated,
      assessment_process_data_array: newTestsToAdministerArray,
      already_assigned_assessment_process_data_array: alreadyAssignedTestsToAdministerArray
    };
    // edit assigned candidate
    this.props.editAssessmentProcessAssignedCandidate(body).then(response => {
      if (response.ok) {
        // populating assigned candidates table
        this.populateAssignedCandidatesTable();
        // closing popup
        this.closeEditAssignedCandidatePopup();
        // duplicate email found
      } else if (response.status === 409) {
        this.setState(
          { showDuplicateEmailFoundError: true, isValidAddEditCandidateForm: false },
          () => {
            // focus on repective field
            document.getElementById("assign-candidate-email-input").focus();
          }
        );
        // should never happen
      } else {
        throw new Error(
          "An error occurred during the edit assigned candidate from assessment process process"
        );
      }
    });
  };

  // populating assigned candidates table
  populateAssignedCandidatesTable = () => {
    // initializing needed object and array for assignedCandidatesRowsDefinition props (needed for GenericTable component)
    let assignedCandidatesRowsDefinition = {};
    const data = [];

    // check if any of them contain imperative
    let assignedCandidatesContainsImperative = false;
    // making sure that the object is not empty
    if (
      Object.keys(
        this.props[
          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
            ? "assessmentProcessDataAssignCandidates"
            : "assessmentProcessDataActiveProcesses"
        ]
      ).length > 0
    ) {
      // getting related assigned candidates
      this.props
        .getAssessmentProcessAssignedCandidates(
          this.props[
            this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
              ? "assessmentProcessDataAssignCandidates"
              : "assessmentProcessDataActiveProcesses"
          ].id
        )
        .then(response => {
          // getting unique assigned candidates (removing duplicates ==> candidates with more than one tests)
          const uniqueAssignedCandidates = response.filter((obj, index) => {
            return index === response.findIndex(o => obj.email === o.email);
          });
          // looping in response given
          for (let i = 0; i < uniqueAssignedCandidates.length; i++) {
            // getting all tests related to current assigned candidate
            const temp_test_data = response.filter(
              obj => obj.email === uniqueAssignedCandidates[i].email
            );
            const currentResult = uniqueAssignedCandidates[i];

            // if reason for testing is imperative staffing, then update the flag accordingly
            if (!assignedCandidatesContainsImperative) {
              if (
                currentResult.reason_for_testing_codename ===
                  REASON_FOR_TESTING_CODENAME_CONST.IMP_STAFFING ||
                currentResult.reason_for_testing_codename ===
                  REASON_FOR_TESTING_CODENAME_CONST.IMP_STAFFING_OLA
              ) {
                assignedCandidatesContainsImperative = true;
              }
            }
            // pushing needed data in data array (needed for GenericTable component)
            data.push({
              // additional info data
              billing_contact_id: currentResult.billing_contact_id,
              billing_contact_first_name: currentResult.billing_contact_first_name,
              billing_contact_last_name: currentResult.billing_contact_last_name,
              billing_contact_email: currentResult.billing_contact_email,
              candidate_assigned_test_data: temp_test_data,
              // columns data
              column_1: currentResult.first_name,
              column_2: currentResult.last_name,
              column_3: currentResult.email,
              column_4: this.populateTestsDetailsColumn(temp_test_data),
              column_5: (
                <>
                  <StyledTooltip
                    id={`assessment-process-assigned-candidates-edit-${i}`}
                    place="top"
                    variant={TYPE.light}
                    effect={EFFECT.solid}
                    openOnClick={false}
                    tooltipElement={
                      <div style={styles.allUnset}>
                        <CustomButton
                          dataTip=""
                          dataFor={`assessment-process-assigned-candidates-edit-${i}`}
                          label={
                            <>
                              <FontAwesomeIcon icon={faPencilAlt} style={styles.tableIcon} />
                            </>
                          }
                          action={() => this.openEditAssignedCandidatePopup(i)}
                          customStyle={styles.actionButton}
                          buttonTheme={THEME.PRIMARY}
                          ariaLabel={LOCALIZE.formatString(
                            LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                              .assignedCandidatesTable.editAccessibility,
                            currentResult.first_name,
                            currentResult.last_name
                          )}
                        />
                      </div>
                    }
                    tooltipContent={
                      <div>
                        <p>
                          {
                            LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                              .assignedCandidatesTable.editTooltip
                          }
                        </p>
                      </div>
                    }
                  />
                  {this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES && (
                    <StyledTooltip
                      id={`assessment-process-assigned-candidates-delete-${i}`}
                      place="top"
                      variant={TYPE.light}
                      effect={EFFECT.solid}
                      openOnClick={false}
                      tooltipElement={
                        <div style={styles.allUnset}>
                          <CustomButton
                            dataTip=""
                            dataFor={`assessment-process-assigned-candidates-delete-${i}`}
                            label={
                              <>
                                <FontAwesomeIcon icon={faTrashAlt} style={styles.tableIcon} />
                              </>
                            }
                            action={() => this.openDeleteAssignedCandidatePopup(i)}
                            customStyle={styles.actionButton}
                            buttonTheme={THEME.PRIMARY}
                            ariaLabel={LOCALIZE.formatString(
                              LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                                .assignedCandidatesTable.deleteAccessibility,
                              currentResult.first_name,
                              currentResult.last_name
                            )}
                          />
                        </div>
                      }
                      tooltipContent={
                        <div>
                          <p>{LOCALIZE.commons.deleteButton}</p>
                        </div>
                      }
                    />
                  )}
                </>
              )
            });
          }

          // updating rowsDefinition object with provided data and needed style
          assignedCandidatesRowsDefinition = {
            column_1_style: { ...COMMON_STYLE.LEFT_TEXT },
            column_2_style: { ...COMMON_STYLE.LEFT_TEXT },
            column_3_style: { ...COMMON_STYLE.LEFT_TEXT },
            column_4_style: { ...COMMON_STYLE.LEFT_TEXT },
            column_5_style: { ...COMMON_STYLE.CENTERED_TEXT },
            data: data
          };

          // saving results in state
          this.setState({
            assignedCandidatesContainsImperative: assignedCandidatesContainsImperative,
            assignedCandidatesRowsDefinition: assignedCandidatesRowsDefinition,
            isLoadingAssignedCandidates: false
          });
        });
    }
  };

  populateTestsDetailsColumn = testData => {
    // initializing highestReasonForTestingMinimumProcessLength
    let highestReasonForTestingMinimumProcessLength = 0;
    // initializing assignedCandidatesContainsOlaTest
    let assignedCandidatesContainsOlaTest = false;
    let assignedCandidatesContainsImperative = false;
    return testData.map((data, index) => {
      // if reason_for_testing_minimum_process_length is defined and greater than current value
      if (
        data.reason_for_testing_minimum_process_length !== null &&
        data.reason_for_testing_minimum_process_length > highestReasonForTestingMinimumProcessLength
      ) {
        // updating the highestReasonForTestingMinimumProcessLength variable
        highestReasonForTestingMinimumProcessLength = parseInt(
          data.reason_for_testing_minimum_process_length
        );
      }
      // assignedCandidatesContainsOlaTest is still false
      if (!assignedCandidatesContainsOlaTest) {
        // checking if current test is an ola test
        if (
          data.test_skill_sub_type_codename === TestSkillSleDescCodename.ORAL_EN ||
          data.test_skill_sub_type_codename === TestSkillSleDescCodename.ORAL_FR
        ) {
          assignedCandidatesContainsOlaTest = true;
        }
      }
      if (!assignedCandidatesContainsImperative) {
        if (
          data.reason_for_testing_codename === REASON_FOR_TESTING_CODENAME_CONST.IMP_STAFFING ||
          data.reason_for_testing_codename === REASON_FOR_TESTING_CODENAME_CONST.IMP_STAFFING_OLA
        ) {
          assignedCandidatesContainsImperative = true;
        }
      }
      // formatting test skill
      const formattedTestSkill =
        data.test_skill_sub_type_id !== null
          ? `${data[`test_skill_sub_type_name_${this.props.currentLanguage}`]}`
          : `${data[`test_skill_type_name_${this.props.currentLanguage}`]}`;
      // getting assigned test specs related status
      let assignedTestSpecsRelatedStatus = LOCALIZE.commons.na;
      if (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES) {
        const currentIterationRelatedComplementaryData = data;
        // making sure that the currentIterationRelatedComplementaryData is defined
        if (typeof currentIterationRelatedComplementaryData !== "undefined") {
          // getting assigned test specs related status
          assignedTestSpecsRelatedStatus = getAssignedTestSpecsRelatedStatus(
            currentIterationRelatedComplementaryData
          );
        }
      }

      // getting formatted test data
      const formattedTestData = `${formattedTestSkill} | ${
        data[`reason_for_testing_name_${this.props.currentLanguage}`].split("/")[0]
      } | ${data.level_required}`;

      // getting formatted related status
      const formattedRelatedStatus =
        this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES
          ? LOCALIZE.formatString(
              LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.assignedCandidatesTable
                .columnFourFormattedRelatedStatus,
              <span style={styles.boldText}>{assignedTestSpecsRelatedStatus}</span>
            )
          : "";

      // if last iteration
      if (index === testData.length - 1) {
        // updating needed states
        this.setState({
          highestReasonForTestingMinimumProcessLength: highestReasonForTestingMinimumProcessLength,
          assignedCandidatesContainsOlaTest: assignedCandidatesContainsOlaTest,
          assignedCandidatesContainsImperative: assignedCandidatesContainsImperative
        });
      }

      return (
        <>
          <p style={index === 0 ? styles.noPaddingTop : {}}>
            {formattedTestData} {formattedRelatedStatus}
            {data.user_accommodation_file_id !== null && (
              <StyledTooltip
                id={`accommodation-requests-pending-requests-view-accommodation-details-row-${index}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`accommodation-requests-pending-requests-view-accommodation-details-row-${index}`}
                      label={
                        <>
                          <FontAwesomeIcon
                            icon={
                              data.is_alternate_test_request
                                ? faFileCircleExclamation
                                : faUniversalAccess
                            }
                            style={styles.tableIcon}
                          />
                        </>
                      }
                      action={() => this.openAccommodationsRequestDetailsPopup(data)}
                      customStyle={styles.accommodationTooltipButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        data.is_alternate_test_request
                          ? LOCALIZE.accommodationsRequestDetailsPopup
                              .viewAlternateTestRequestDetailsAccessibilityLabel
                          : LOCALIZE.accommodationsRequestDetailsPopup
                              .viewAccommodationsRequestDetailsAccessibilityLabel,
                        formattedTestSkill
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {data.is_alternate_test_request
                        ? LOCALIZE.accommodationsRequestDetailsPopup.viewAlternateTestRequestDetails
                        : LOCALIZE.accommodationsRequestDetailsPopup
                            .viewAccommodationsRequestDetails}
                    </p>
                  </div>
                }
              />
            )}
          </p>
        </>
      );
    });
  };

  openEditAssignedCandidatePopup = index => {
    const assignedTestSpecsIdsToBeUpdated = [];
    // creating copy of needed states
    const copyOfReasonForTestingStates = _.cloneDeep(this.state.reasonForTestingStates);
    const copyOfLevelRequiredStates = _.cloneDeep(this.state.levelRequiredStates);
    const copyOfTestAssignedStates = _.cloneDeep(this.state.testAssignedStates);

    // looping in tests to administer
    for (
      let i = 0;
      i <
      this.props[
        this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
          ? "assessmentProcessDataAssignCandidates"
          : "assessmentProcessDataActiveProcesses"
      ].tests_to_administer.length;
      i++
    ) {
      // looping in assigned candidate test data
      for (
        let j = 0;
        j <
        this.state.assignedCandidatesRowsDefinition.data[index].candidate_assigned_test_data.length;
        j++
      ) {
        // test skill type and test skill sub type matches
        if (
          this.props[
            this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
              ? "assessmentProcessDataAssignCandidates"
              : "assessmentProcessDataActiveProcesses"
          ].tests_to_administer[i].test_skill_type_id ===
            this.state.assignedCandidatesRowsDefinition.data[index].candidate_assigned_test_data[j]
              .test_skill_type_id &&
          this.props[
            this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
              ? "assessmentProcessDataAssignCandidates"
              : "assessmentProcessDataActiveProcesses"
          ].tests_to_administer[i].test_skill_sub_type_id ===
            this.state.assignedCandidatesRowsDefinition.data[index].candidate_assigned_test_data[j]
              .test_skill_sub_type_id
        ) {
          // populating needed states
          copyOfReasonForTestingStates[i] = {
            label:
              this.state.assignedCandidatesRowsDefinition.data[index].candidate_assigned_test_data[
                j
              ][`reason_for_testing_name_${this.props.currentLanguage}`],
            value:
              this.state.assignedCandidatesRowsDefinition.data[index].candidate_assigned_test_data[
                j
              ].reason_for_testing_id
          };
          copyOfLevelRequiredStates[i] =
            this.state.assignedCandidatesRowsDefinition.data[index].candidate_assigned_test_data[
              j
            ].level_required;
          copyOfTestAssignedStates[i] = true;
          assignedTestSpecsIdsToBeUpdated.push(
            this.state.assignedCandidatesRowsDefinition.data[index].candidate_assigned_test_data[j]
              .id
          );
          break;
          // not a match
        } else if (
          j ===
          this.state.assignedCandidatesRowsDefinition.data[index].candidate_assigned_test_data
            .length -
            1
        ) {
          copyOfReasonForTestingStates[i] = [];
          copyOfLevelRequiredStates[i] = "";
          copyOfTestAssignedStates[i] = false;
          // adding null to assignedTestSpecsIdsToBeUpdated array for non matching assigned tests
          assignedTestSpecsIdsToBeUpdated.push(null);
        }
      }
    }
    this.setState(
      {
        showEditAssignedCandidatePopup: true,
        indexOfRowToEditOrDelete: index,
        initialFirstName: this.state.assignedCandidatesRowsDefinition.data[index].column_1,
        firstName: this.state.assignedCandidatesRowsDefinition.data[index].column_1,
        initialLastName: this.state.assignedCandidatesRowsDefinition.data[index].column_2,
        lastName: this.state.assignedCandidatesRowsDefinition.data[index].column_2,
        initialEmail: this.state.assignedCandidatesRowsDefinition.data[index].column_3,
        email: this.state.assignedCandidatesRowsDefinition.data[index].column_3,
        initialBillingContactSelectedOption: {
          label: `${this.state.assignedCandidatesRowsDefinition.data[index].billing_contact_first_name} ${this.state.assignedCandidatesRowsDefinition.data[index].billing_contact_last_name} (${this.state.assignedCandidatesRowsDefinition.data[index].billing_contact_email})`,
          value: this.state.assignedCandidatesRowsDefinition.data[index].billing_contact_id
        },
        billingContactSelectedOption: {
          label: `${this.state.assignedCandidatesRowsDefinition.data[index].billing_contact_first_name} ${this.state.assignedCandidatesRowsDefinition.data[index].billing_contact_last_name} (${this.state.assignedCandidatesRowsDefinition.data[index].billing_contact_email})`,
          value: this.state.assignedCandidatesRowsDefinition.data[index].billing_contact_id
        },
        initialReasonForTestingStates: copyOfReasonForTestingStates,
        reasonForTestingStates: copyOfReasonForTestingStates,
        initialLevelRequiredStates: copyOfLevelRequiredStates,
        levelRequiredStates: copyOfLevelRequiredStates,
        testAssignedStates: copyOfTestAssignedStates,
        initialTestAssignedStates: copyOfTestAssignedStates,
        assignedTestSpecsIdsToBeUpdated: assignedTestSpecsIdsToBeUpdated
      },
      () => {
        // populating tests to administer table (in popup)
        this.populateTestsToAdministerTableInPopup(true);
        // validating form
        this.validateAddEditCandidateForm();
      }
    );
  };

  closeEditAssignedCandidatePopup = () => {
    this.setState({
      showEditAssignedCandidatePopup: false,
      indexOfRowToEditOrDelete: null,
      initialFirstName: "",
      firstName: "",
      isValidFirstName: true,
      initialLastName: "",
      lastName: "",
      isValidLastName: true,
      initialEmail: "",
      email: "",
      isValidEmail: true,
      showDuplicateEmailFoundError: false,
      initialBillingContactSelectedOption: [],
      billingContactSelectedOption: [],
      initialReasonForTestingStates: [],
      reasonForTestingStates: [],
      initialLevelRequiredStates: [],
      levelRequiredStates: [],
      testAssignedStates: [],
      initialTestAssignedStates: [],
      assignedTestSpecsIdsToBeUpdated: [],
      isValidAddEditCandidateForm: false,
      popupDataUpdated: false,
      emailOrAssignedTestsToAdministerUpdated: false
    });
  };

  handleEditAssignedCandidate = () => {
    // formatting final array to send to the backend
    // initializing finalArray
    const finalArray = [];
    // looping in assessmentProcessDataAssignCandidates (tests_to_administer)
    for (
      let i = 0;
      i <
      this.props[
        this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
          ? "assessmentProcessDataAssignCandidates"
          : "assessmentProcessDataActiveProcesses"
      ].tests_to_administer.length;
      i++
    ) {
      // test of current iteration has been assigned
      if (this.state.testAssignedStates[i]) {
        // creating new assigned test obj
        const newAssignedTestObj = {
          first_name: this.state.firstName,
          last_name: this.state.lastName,
          email: this.state.email,
          assessment_process_id:
            this.props[
              this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? "assessmentProcessDataAssignCandidates"
                : "assessmentProcessDataActiveProcesses"
            ].id,
          test_skill_type_id:
            this.props[
              this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? "assessmentProcessDataAssignCandidates"
                : "assessmentProcessDataActiveProcesses"
            ].tests_to_administer[i].test_skill_type_id,
          test_skill_sub_type_id:
            this.props[
              this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? "assessmentProcessDataAssignCandidates"
                : "assessmentProcessDataActiveProcesses"
            ].tests_to_administer[i].test_skill_sub_type_id,
          billing_contact_id: this.state.billingContactSelectedOption.value,
          reason_for_testing_id: this.state.reasonForTestingStates[i].value,
          level_required: this.state.levelRequiredStates[i],
          assessment_process_assigned_test_specs_id: this.state.assignedTestSpecsIdsToBeUpdated[i],
          ids_to_be_updated: this.state.assignedTestSpecsIdsToBeUpdated
        };
        // adding new object to finalArray
        finalArray.push(newAssignedTestObj);
      }
    }
    // creating request body
    const body = {
      called_from_active_processes: false,
      assessment_process_data_array: finalArray
    };
    // edit assigned candidate
    this.props.editAssessmentProcessAssignedCandidate(body).then(response => {
      if (response.ok) {
        // resetting ola checkbox states
        this.setState(
          {
            olaCheckbox1: false,
            olaCheckbox2: false,
            assignedCandidatesContainsOlaTest: false
          },
          () => {
            // populating assigned candidates table
            this.populateAssignedCandidatesTable();
            // closing popup
            this.closeEditAssignedCandidatePopup();
          }
        );
        // duplicate email found
      } else if (response.status === 409) {
        this.setState(
          { showDuplicateEmailFoundError: true, isValidAddEditCandidateForm: false },
          () => {
            // focus on repective field
            document.getElementById("assign-candidate-email-input").focus();
          }
        );
        // should never happen
      } else {
        throw new Error(
          "An error occurred during the edit assigned candidate from assessment process process"
        );
      }
    });
  };

  openDeleteAssignedCandidatePopup = index => {
    this.setState({ showDeleteAssignedCandidatePopup: true, indexOfRowToEditOrDelete: index });
  };

  closeDeleteAssignedCandidatePopup = () => {
    this.setState({ showDeleteAssignedCandidatePopup: false, indexOfRowToEditOrDelete: null });
  };

  handleDeleteAssignedCandidate = () => {
    // initializing body
    const body = {
      assessment_process_id:
        this.props[
          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
            ? "assessmentProcessDataAssignCandidates"
            : "assessmentProcessDataActiveProcesses"
        ].id,
      first_name:
        this.state.assignedCandidatesRowsDefinition.data[this.state.indexOfRowToEditOrDelete]
          .column_1,
      last_name:
        this.state.assignedCandidatesRowsDefinition.data[this.state.indexOfRowToEditOrDelete]
          .column_2,
      email:
        this.state.assignedCandidatesRowsDefinition.data[this.state.indexOfRowToEditOrDelete]
          .column_3
    };
    this.props.deleteAssessmentProcessAssignedCandidate(body).then(response => {
      if (response.ok) {
        // populating assigned candidates table
        this.populateAssignedCandidatesTable();
        // closing popup
        this.closeDeleteAssignedCandidatePopup();
        // should never happen
      } else {
        throw new Error(
          "An error occurred during the delete assigned candidate from assessment process process"
        );
      }
    });
  };

  openSendRequestConfirmationPopup = () => {
    this.setState({
      showSendRequestConfirmationPopup: true,
      showRequestSentSuccessfullyPopup: false
    });
  };

  closeSendRequestConfirmationPopup = () => {
    this.setState(
      {
        showSendRequestConfirmationPopup: false
      },
      () => {
        setTimeout(() => {
          this.setState({
            showRequestSentSuccessfullyPopup: false
          });
        }, 200);
      }
    );
  };

  handleSendRequest = () => {
    this.props
      .sendAssessmentProcessAssignedCandidatesRequest(
        this.props[
          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
            ? "assessmentProcessDataAssignCandidates"
            : "assessmentProcessDataActiveProcesses"
        ].id
      )
      .then(response => {
        if (response.ok) {
          // updating/resetting needed states
          this.setState(
            {
              isAssignCandidatesReferenceNumberSelected: false,
              showRequestSentSuccessfullyPopup: true
            },
            () => {
              // updating redux states
              this.props.setSelectedAssessmentProcessRefNbrAssignCandidates({});
              // populating process reference number options
              this.populateReferenceNumberOptions();
              // trigger data rerender
              this.props.triggerDataRerenderFunction();
            }
          );
          // should never happen
        } else {
          throw new Error(
            "An error occurred during the send assessment process assigned candidates process"
          );
        }
      });
  };

  openUnassignTestPopup = i => {
    // getting respective assigned test specs ID
    const respectiveAssignedTestSpecsId = this.state.assignedCandidatesRowsDefinition.data[
      this.state.indexOfRowToEditOrDelete
    ].candidate_assigned_test_data.filter(
      obj => obj.id === this.state.assignedTestSpecsIdsToBeUpdated[i]
    )[0].id;
    this.setState({
      showUnassignTestPopup: true,
      assignedTestSpecsIdToBeUnassign: respectiveAssignedTestSpecsId
    });
  };

  closeUnassignTestPopup = i => {
    this.setState({
      showUnassignTestPopup: false,
      assignedTestSpecsIdToBeUnassign: null,
      unassignLoading: false
    });
  };

  handleUnassignTest = () => {
    // Disable the button, show that it is loading umtil after it returns
    this.setState({ unassignLoading: true });
    // This could run slowly due to cancelling teams session
    this.props
      .unassignAssessmentProcessAssignedTestToAdminister(this.state.assignedTestSpecsIdToBeUnassign)
      .then(response => {
        if (response.status === 200 || response.status === 409) {
          // getting related assessment process data
          this.props
            .getAssessmentProcessData(
              this.props.selectedAssessmentProcessRefNbrActiveProcesses.value
            )
            .then(response => {
              // updating redux states with most updated data
              this.props.setAssessmentProcessDataActiveProcesses(response);
              // adding small delay to make sure that the redux states have been updated before updating the table (popup)
              setTimeout(() => {
                this.setState({ unassignLoading: false });
                // populating assigned candidates table
                this.populateTestsToAdministerTableInPopup();
                // closing unassign popup
                this.closeUnassignTestPopup();
              }, 100);
            });
          // should never happen
        } else {
          throw new Error(
            "An error occurred during the edit assigned candidate from assessment process process"
          );
        }
      });
  };

  openAccommodationsRequestDetailsPopup = accommodationRequestData => {
    // formatting/adding user_id attribute
    const formattedObj = _.cloneDeep(accommodationRequestData);
    formattedObj.user_id = accommodationRequestData.candidate_user_id;
    // updating needed states
    this.setState({
      showAccommodationsRequestDetailsPopup: true,
      selectedAccommodationRequestData: formattedObj
    });
  };

  closeAccommodationsRequestDetailsPopup = () => {
    this.setState({
      showAccommodationsRequestDetailsPopup: false,
      selectedAccommodationRequestData: {}
    });
  };

  getCheckbox1State = event => {
    this.setState({
      olaCheckbox1: event.target.checked
    });
  };

  getCheckbox2State = event => {
    this.setState({
      olaCheckbox2: event.target.checked
    });
  };

  getReferenceNumberContent = event => {
    const referenceNumber = event.target.value;
    if (referenceNumberRegexExpression.test(referenceNumber)) {
      this.setState({ referenceNumber: referenceNumber.toUpperCase() }, () => {});
    }
  };

  openEditNamePopup = () => {
    this.setState({
      showEditNamePopup: true,
      referenceNumber: this.props.selectedAssessmentProcessRefNbrAssignCandidates.label
    });
  };

  closeEditNamePopup = () => {
    this.setState({ showEditNamePopup: false, referenceNumber: "" });
  };

  saveEditNamePopup = () => {
    // update the reference number in the background
    this.props
      .updateReferenceNumber(
        this.props.selectedAssessmentProcessRefNbrAssignCandidates.value,
        this.state.referenceNumber
      )
      .then(response => {
        if (response.status === 200) {
          this.handleSaveNonSentAssessmentProcessData();
          this.props.setAssessmentProcessLabel(this.state.referenceNumber);
          this.getSelectedReferenceNumberOption(
            this.props.selectedAssessmentProcessRefNbrAssignCandidates
          );
          this.populateReferenceNumberOptions();
          this.setState({ showEditNamePopup: false, referenceNumber: "" });
        }
      });
  };

  handleShowDeleteProcessPopup = () => {
    this.setState({ showDeleteProcessPopup: true });
  };

  handleDeleteProcessPopupClose = () => {
    this.setState({ showDeleteProcessPopup: false });
  };

  handleDeleteProcess = () => {
    this.props
      .deleteAssessmentProcess(this.props.selectedAssessmentProcessRefNbrAssignCandidates.value)
      .then(response => {
        if (response.status === 200) {
          // force a reload as if brand new
          this.setState({ showDeleteProcessPopup: false });
          this.props.setSelectedAssessmentProcessRefNbrAssignCandidates({});
          this.componentDidMount();
        } else {
          throw new Error("An error occurred during the delete assessment process request");
        }
      });
  };

  render() {
    const testsToAdministerColumnsDefinition = [
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.testsToAdministerTable
            .columnOne,
        style: { ...{ width: "50%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.testsToAdministerTable
            .columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.testsToAdministerTable
            .columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.testsToAdministerTable
            .columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    const availableTestSessionsColumnsDefinition = [
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.testsToAdministerTable
            .viewAvailableTestSessionsPopup.table.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.testsToAdministerTable
            .viewAvailableTestSessionsPopup.table.columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.testsToAdministerTable
            .viewAvailableTestSessionsPopup.table.columnThree,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.testsToAdministerTable
            .viewAvailableTestSessionsPopup.table.columnFour,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.testsToAdministerTable
            .viewAvailableTestSessionsPopup.table.columnFive,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    const assignedCandidatesColumnsDefinition = [
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.assignedCandidatesTable
            .columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.assignedCandidatesTable
            .columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.assignedCandidatesTable
            .columnThree,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: (
          <>
            <span>
              {
                LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                  .assignedCandidatesTable.columnFour
              }
            </span>
            {this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES && (
              <span>
                <StyledTooltip
                  id={`assign-candidates-test-details`}
                  place="top"
                  variant={TYPE.light}
                  effect={EFFECT.solid}
                  openOnClick={false}
                  tooltipElement={
                    <div style={styles.allUnset}>
                      <CustomButton
                        dataTip=""
                        dataFor={`assign-candidates-test-details`}
                        label={
                          <>
                            <FontAwesomeIcon icon={faQuestionCircle} style={styles.tableIcon} />
                          </>
                        }
                        action={() => {}}
                        customStyle={styles.tableHeaderTooltipButton}
                        buttonTheme={THEME.PRIMARY}
                      />
                    </div>
                  }
                  tooltipContent={
                    <div>
                      <p>
                        {
                          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                            .assignedCandidatesTable.columnFourTooltip
                        }
                      </p>
                    </div>
                  }
                />
              </span>
            )}
          </>
        ),
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.assignedCandidatesTable
            .columnFive,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    const testsToAdministerInPopupColumnsDefinition = [
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.addCandidatePopup
            .testsToAdministerTable.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.addCandidatePopup
            .testsToAdministerTable.columnTwo,
        style: { ...{ width: "25%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.addCandidatePopup
            .testsToAdministerTable.columnThree,
        style: { ...{ width: "20%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.addCandidatePopup
            .testsToAdministerTable.columnFour,
        style: {
          ...{
            width: "15%"
          },
          ...COMMON_STYLE.CENTERED_TEXT
        }
      }
    ];

    // ACTIVE_PROCESSES + Edit Assigned Candidate Popup
    if (
      this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES &&
      this.state.showEditAssignedCandidatePopup &&
      !this.state.showAddCandidatePopup
    ) {
      testsToAdministerInPopupColumnsDefinition.push({
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.addCandidatePopup
            .testsToAdministerTable.columnFive,
        style: { ...{ width: "10%" }, ...COMMON_STYLE.CENTERED_TEXT }
      });
      testsToAdministerInPopupColumnsDefinition.push({
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.addCandidatePopup
            .testsToAdministerTable.columnSix,
        style: { ...{ width: "10%" }, ...COMMON_STYLE.CENTERED_TEXT }
      });
    }

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }
    let isSendRequestButtonDisabled = false;
    if (
      Object.keys(this.state.assignedCandidatesRowsDefinition).length > 0 &&
      this.state.assignedCandidatesRowsDefinition.data.length > 0 &&
      this.state.assignCandidatesDuration >= this.state.highestReasonForTestingMinimumProcessLength
    ) {
      if (!this.state.assignCandidatesProcessDataUpdated) {
        // if this is required and the button is not checked
        if (this.state.assignedCandidatesContainsOlaTest && !this.state.olaCheckbox1) {
          isSendRequestButtonDisabled = true;
        }

        // otherwise, if this is required and the button is not checked
        else {
          if (this.state.assignedCandidatesContainsImperative && !this.state.olaCheckbox2) {
            isSendRequestButtonDisabled = true;
          }
        }
      } else {
        isSendRequestButtonDisabled = true;
      }
    } else {
      isSendRequestButtonDisabled = true;
    }

    // if its empty, then diable the edit button
    const disableEditButton = _.isEmpty(this.props.selectedAssessmentProcessRefNbrAssignCandidates);

    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    return (
      <div style={styles.mainContainer}>
        <div style={styles.formContainer}>
          <Row className="align-items-center justify-content-center" style={styles.inputContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.noPadding}
            >
              <label id="assign-candidate-reference-number-label" style={styles.boldText}>
                {LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.referenceNumberLabel}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={
                this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                  ? styles.colStyle
                  : {}
              }
            >
              <div
                style={
                  this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                    ? styles.dropdownDiv
                    : {}
                }
              >
                <DropdownSelect
                  idPrefix="assign-candidate-reference-number"
                  isValid={true}
                  ariaRequired={true}
                  ariaLabelledBy="assign-candidate-reference-number-label"
                  hasPlaceholder={true}
                  options={this.state.referenceNumberOptions}
                  orderByLabels={false}
                  onChange={this.getSelectedReferenceNumberOption}
                  defaultValue={
                    this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                      ? this.props.selectedAssessmentProcessRefNbrAssignCandidates
                      : this.props.selectedAssessmentProcessRefNbrActiveProcesses
                  }
                />
              </div>
              {this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES && (
                <div styles={styles.editButtonContainer}>
                  <CustomButton
                    label={<FontAwesomeIcon icon={faPencilAlt} />}
                    customStyle={{
                      ...styles.actionButton,
                      ...styles.editButton
                    }}
                    action={this.openEditNamePopup}
                    disabled={disableEditButton}
                  />
                </div>
              )}
            </Col>
          </Row>
          {((this.state.isAssignCandidatesReferenceNumberSelected &&
            this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES) ||
            (this.state.isActiveProcessesReferenceNumberSelected &&
              this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES)) && (
            <div>
              {this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES && (
                <Row
                  className="align-items-center justify-content-center"
                  style={styles.inputContainer}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.noPadding}
                  >
                    <label id="assign-candidate-sent-date-label" style={styles.boldText}>
                      {LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.sentDateLabel}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                    style={this.state.isLoadingSentDateData ? styles.centerText : {}}
                  >
                    {!this.state.isLoadingSentDateData ? (
                      <DatePicker
                        dateLabelId="assign-candidate-sent-date"
                        ariaLabelledBy="assign-candidate-sent-date-label"
                        initialDateDayValue={{
                          value: Number(
                            this.props.assessmentProcessDataActiveProcesses.sent_date.split("-")[2]
                          ),
                          label: `${
                            this.props.assessmentProcessDataActiveProcesses.sent_date.split("-")[2]
                          }`
                        }}
                        initialDateMonthValue={{
                          value: Number(
                            this.props.assessmentProcessDataActiveProcesses.sent_date.split("-")[1]
                          ),
                          label: `${
                            this.props.assessmentProcessDataActiveProcesses.sent_date.split("-")[1]
                          }`
                        }}
                        initialDateYearValue={{
                          value: Number(
                            this.props.assessmentProcessDataActiveProcesses.sent_date.split("-")[0]
                          ),
                          label: `${
                            this.props.assessmentProcessDataActiveProcesses.sent_date.split("-")[0]
                          }`
                        }}
                        disabledDropdowns={true}
                      />
                    ) : (
                      // eslint-disable-next-line jsx-a11y/label-has-associated-control
                      <label className="fa fa-spinner fa-spin" style={styles.loading}>
                        <FontAwesomeIcon icon={faSpinner} />
                      </label>
                    )}
                  </Col>
                </Row>
              )}
              <Row
                className="align-items-center justify-content-center"
                style={styles.inputContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.noPadding}
                >
                  <label id="assign-candidate-department-label" style={styles.boldText}>
                    {LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.departmentLabel}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <DropdownSelect
                    idPrefix="assign-candidate-department-label"
                    isValid={true}
                    ariaRequired={true}
                    ariaLabelledBy="assign-candidate-department-label"
                    options={
                      this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                        ? this.state.departmentOptions
                        : []
                    }
                    onChange={this.getSelectedDepartmentOption}
                    isDisabled={
                      this.props.source !== ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                    }
                    defaultValue={{
                      value:
                        this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                          ? this.state.departmentSelectedOption.value
                          : this.props.assessmentProcessDataActiveProcesses.dept_id,
                      label:
                        this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                          ? this.state.departmentSelectedOption.label
                          : this.props.currentLanguage === LANGUAGES.english
                            ? `${this.props.assessmentProcessDataActiveProcesses.dept_edesc} (${this.props.assessmentProcessDataActiveProcesses.dept_eabrv})`
                            : `${this.props.assessmentProcessDataActiveProcesses.dept_fdesc} (${this.props.assessmentProcessDataActiveProcesses.dept_fabrv})`
                    }}
                  />
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-center"
                style={
                  (this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES &&
                    this.state.assignCandidatesDuration <
                      this.state.highestReasonForTestingMinimumProcessLength) ||
                  (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES &&
                    this.state.activeProcessesDuration <
                      this.state.highestReasonForTestingMinimumProcessLength)
                    ? styles.inputContainerWithError
                    : styles.inputContainer
                }
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.noPadding}
                >
                  <label htmlFor="assign-candidate-duration-input" style={styles.boldText}>
                    {LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.durationLabel}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <input
                    id="assign-candidate-duration-input"
                    className={
                      this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                        ? this.state.assignCandidatesDuration <
                          this.state.highestReasonForTestingMinimumProcessLength
                          ? "invalid-field"
                          : "valid-field"
                        : this.state.activeProcessesDuration <
                            this.state.highestReasonForTestingMinimumProcessLength
                          ? "invalid-field"
                          : "valid-field"
                    }
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={
                      this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                        ? this.state.assignCandidatesDuration
                        : this.state.activeProcessesDuration
                    }
                    onChange={
                      this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                        ? this.getAssignCandidatesDurationContent
                        : this.getActiveProcessesDurationContent
                    }
                  ></input>
                </Col>
              </Row>
              {((this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES &&
                this.state.assignCandidatesDuration <
                  this.state.highestReasonForTestingMinimumProcessLength) ||
                (this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES &&
                  this.state.activeProcessesDuration <
                    this.state.highestReasonForTestingMinimumProcessLength)) && (
                <Row className="align-items-center justify-content-end">
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label htmlFor="assign-candidate-duration-input" style={styles.errorMessage}>
                      {LOCALIZE.formatString(
                        LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.durationError,
                        this.state.highestReasonForTestingMinimumProcessLength
                      )}
                    </label>
                  </Col>
                </Row>
              )}
              <Row
                className="align-items-center justify-content-center"
                style={styles.inputContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.noPadding}
                >
                  <label id="assign-candidate-closing-date-label" style={styles.boldText}>
                    {LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.closingDateLabel}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                  style={this.state.isLoadingClosingDateData ? styles.centerText : {}}
                >
                  {!this.state.isLoadingClosingDateData ? (
                    <DatePicker
                      dateLabelId="assign-candidate-closing-date"
                      ariaLabelledBy="assign-candidate-closing-date-label"
                      initialDateDayValue={{
                        value:
                          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                            ? Number(this.state.calculatedAssignCandidatesClosingDate.split("-")[2])
                            : Number(this.state.calculatedActiveProcessesClosingDate.split("-")[2]),
                        label:
                          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                            ? `${this.state.calculatedAssignCandidatesClosingDate.split("-")[2]}`
                            : `${this.state.calculatedActiveProcessesClosingDate.split("-")[2]}`
                      }}
                      initialDateMonthValue={{
                        value:
                          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                            ? Number(this.state.calculatedAssignCandidatesClosingDate.split("-")[1])
                            : Number(this.state.calculatedActiveProcessesClosingDate.split("-")[1]),
                        label:
                          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                            ? `${this.state.calculatedAssignCandidatesClosingDate.split("-")[1]}`
                            : `${this.state.calculatedActiveProcessesClosingDate.split("-")[1]}`
                      }}
                      initialDateYearValue={{
                        value:
                          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                            ? Number(this.state.calculatedAssignCandidatesClosingDate.split("-")[0])
                            : Number(this.state.calculatedActiveProcessesClosingDate.split("-")[0]),
                        label:
                          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                            ? `${this.state.calculatedAssignCandidatesClosingDate.split("-")[0]}`
                            : `${this.state.calculatedActiveProcessesClosingDate.split("-")[0]}`
                      }}
                      disabledDropdowns={true}
                      isValidFutureDatePicked={
                        this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                          ? this.state.isValidCalculatedAssignCandidatesClosingDate
                          : this.state.isValidCalculatedActiveProcessesClosingDate
                      }
                    />
                  ) : (
                    // eslint-disable-next-line jsx-a11y/label-has-associated-control
                    <label className="fa fa-spinner fa-spin" style={styles.loading}>
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  )}
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-center"
                style={styles.inputContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.noPadding}
                >
                  <label
                    id="assign-candidate-allow-booking-external-tc-label"
                    style={styles.boldText}
                  >
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                        .allowBookingExternalTcLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  {!this.state.isLoadingSwitch && (
                    <Switch
                      className="switch-vertical-align-middle"
                      onChange={
                        this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                          ? this.handleAllowBookingExternalTcSwitchUpdates
                          : () => {}
                      }
                      checked={
                        this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                          ? this.state.assignCandidatesAllowBookingExternalTc
                          : this.props.assessmentProcessDataActiveProcesses
                              .allow_booking_external_tc
                      }
                      aria-labelledby="assign-candidate-allow-booking-external-tc-label"
                      disabled={
                        this.props.source !== ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                      }
                      height={this.state.switchHeight}
                      width={this.state.switchWidth}
                    />
                  )}
                </Col>
              </Row>
              {this.state.assignedCandidatesContainsOlaTest && (
                <Row
                  className="align-items-center justify-content-center"
                  style={styles.inputContainer}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                    style={styles.noPadding}
                  >
                    <label id="allow-last-minute-cancellation-tc-label" style={styles.boldText}>
                      {
                        LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                          .allowLastMinuteCancellationsToggle
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.isLoadingSwitchLMC && (
                      <Switch
                        className="switch-vertical-align-middle"
                        onChange={
                          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                            ? this.handleAllowLastMinuteCancellationsSwitchUpdates
                            : () => {}
                        }
                        checked={
                          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                            ? this.state.allowLastMinuteCancellationsTc
                            : this.props.assessmentProcessDataActiveProcesses
                                .allow_last_minute_cancellation_tc
                        }
                        aria-labelledby="allow-last-minute-cancellation-tc-label"
                        disabled={
                          this.props.source !== ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                        }
                        height={this.state.switchHeight}
                        width={this.state.switchWidth}
                      />
                    )}
                  </Col>
                </Row>
              )}
              <Row
                className="align-items-center justify-content-center"
                style={
                  this.state.isValidAssignCandidatesContactEmailForCandidates &&
                  this.state.isValidActiveProcessesContactEmailForCandidates
                    ? styles.inputContainer
                    : styles.inputContainerWithError
                }
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.noPadding}
                >
                  <label
                    htmlFor="assign-candidate-contact-email-for-candidates-input"
                    style={styles.boldText}
                  >
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                        .contactEmailForCandidates
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <input
                    id="assign-candidate-contact-email-for-candidates-input"
                    className={
                      this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                        ? this.state.isValidAssignCandidatesContactEmailForCandidates
                          ? "valid-field"
                          : "invalid-field"
                        : this.state.isValidActiveProcessesContactEmailForCandidates
                          ? "valid-field"
                          : "invalid-field"
                    }
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={
                      this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                        ? this.state.assignCandidatesContactEmailForCandidates
                        : this.state.activeProcessesContactEmailForCandidates
                    }
                    onChange={
                      this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                        ? this.getAssignCandidatesContactEmailForCandidatesContent
                        : this.getActiveProcessesContactEmailForCandidatesContent
                    }
                  ></input>
                </Col>
              </Row>
              {this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES &&
                !this.state.isValidAssignCandidatesContactEmailForCandidates && (
                  <Row className="align-items-center justify-content-end">
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <label
                        htmlFor="assign-candidate-contact-email-for-candidates-input"
                        style={styles.errorMessage}
                      >
                        {
                          LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                            .invalidContactEmailForCandidates
                        }
                      </label>
                    </Col>
                  </Row>
                )}
              {this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES &&
                !this.state.isValidActiveProcessesContactEmailForCandidates && (
                  <Row className="align-items-center justify-content-end">
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <label
                        htmlFor="assign-candidate-contact-email-for-candidates-input"
                        style={styles.errorMessage}
                      >
                        {
                          LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                            .invalidContactEmailForCandidates
                        }
                      </label>
                    </Col>
                  </Row>
                )}
              <div style={styles.sendUpdatesButtonContainer}>
                {
                  <CustomButton
                    label={
                      <>
                        <span style={styles.sendRequestButtonContainerIcon}>
                          <FontAwesomeIcon
                            icon={
                              this.props.source ===
                              ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                                ? !this.state.assignCandidatesProcessDataUpdated
                                  ? faCheck
                                  : faSave
                                : !this.state.activeProcessesProcessDataUpdated
                                  ? faCheck
                                  : faPaperPlane
                            }
                          />
                        </span>
                        <span>
                          {this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                            ? this.state.assignCandidatesProcessDataUpdated
                              ? LOCALIZE.commons.saveButton
                              : LOCALIZE.commons.saved
                            : this.state.activeProcessesProcessDataUpdated
                              ? LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses
                                  .sendUpdatesButton
                              : LOCALIZE.commons.saved}
                        </span>
                      </>
                    }
                    action={
                      this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                        ? this.handleSaveNonSentAssessmentProcessData
                        : this.handleSendUpdates
                    }
                    disabled={
                      this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                        ? !this.state.assignCandidatesProcessDataUpdated
                          ? BUTTON_STATE.disabled
                          : this.state.isValidAssignCandidatesChanges
                            ? BUTTON_STATE.enabled
                            : BUTTON_STATE.disabled
                        : !this.state.activeProcessesProcessDataUpdated
                          ? BUTTON_STATE.disabled
                          : this.state.isValidActiveProcessesChanges
                            ? BUTTON_STATE.enabled
                            : BUTTON_STATE.disabled
                    }
                    customStyle={styles.sendUpdatesButton}
                    buttonTheme={
                      this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                        ? this.state.assignCandidatesProcessDataUpdated
                          ? THEME.PRIMARY
                          : THEME.SUCCESS
                        : this.state.activeProcessesProcessDataUpdated
                          ? THEME.PRIMARY
                          : THEME.SUCCESS
                    }
                  />
                }
              </div>
              <div style={styles.formContainer}>
                <div>
                  <label
                    id="assign-candidate-tests-to-administer-table-label"
                    style={styles.tableLabel}
                  >
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                        .testsToAdministerLabel
                    }
                  </label>
                  <div style={styles.tableContainer}>
                    <GenericTable
                      classnamePrefix="assign-candidate-tests-to-administer"
                      columnsDefinition={testsToAdministerColumnsDefinition}
                      rowsDefinition={
                        this.state[
                          this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                            ? "assignCandidatesTestsToAdministerRowsDefinition"
                            : "activeProcessesTestsToAdministerRowsDefinition"
                        ]
                      }
                      currentlyLoading={this.state.isLoadingTestsToAdminister}
                    />
                  </div>
                </div>
                <div style={styles.assignedCandidatesTableContainer}>
                  <label
                    id="assign-candidate-assigned-candidates-table-label"
                    style={styles.tableLabel}
                  >
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                        .assignedCandidatesLabel
                    }
                  </label>
                  <div style={styles.tableContainer}>
                    <GenericTable
                      classnamePrefix="assign-candidate-assigned-candidates"
                      columnsDefinition={assignedCandidatesColumnsDefinition}
                      rowsDefinition={this.state.assignedCandidatesRowsDefinition}
                      currentlyLoading={this.state.isLoadingAssignedCandidates}
                      emptyTableMessage={this.populateEmptyTable()}
                      hasLastRowHtml={true}
                      lastRowHtml={
                        Object.keys(this.state.assignedCandidatesRowsDefinition).length > 0 &&
                        this.state.assignedCandidatesRowsDefinition.data.length > 0 &&
                        this.populateLastRowHtml()
                      }
                    />
                  </div>
                </div>
              </div>
              {this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES && (
                <div>
                  {(this.state.assignedCandidatesContainsOlaTest ||
                    this.state.assignedCandidatesContainsImperative) && (
                    <div style={styles.olaCheckboxexMainContainer}>
                      {this.state.assignedCandidatesContainsOlaTest && (
                        <div style={styles.olaCheckboxContainer}>
                          <div style={styles.olaCheckboxDisplayTableCell}>
                            <input
                              type="checkbox"
                              id={"ola-checkbox-1"}
                              value={this.state.olaCheckbox1}
                              style={{
                                ...styles.checkbox,
                                ...{ transform: checkboxTransformScale }
                              }}
                              onChange={this.getCheckbox1State}
                            />
                          </div>
                          <div style={styles.olaCheckboxLabelDisplayTableCell}>
                            <label htmlFor={"ola-checkbox-1"}>
                              {
                                LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                                  .olaCheckbox1
                              }
                            </label>
                          </div>
                        </div>
                      )}
                      {this.state.assignedCandidatesContainsImperative && (
                        <div style={styles.olaCheckboxContainer}>
                          <div style={styles.olaCheckboxDisplayTableCell}>
                            <input
                              type="checkbox"
                              id={"ola-checkbox-2"}
                              value={this.state.olaCheckbox2}
                              style={{
                                ...styles.checkbox,
                                ...{ transform: checkboxTransformScale }
                              }}
                              onChange={this.getCheckbox2State}
                            />
                          </div>
                          <div style={styles.olaCheckboxLabelDisplayTableCell}>
                            <label htmlFor={"ola-checkbox-2"}>
                              {
                                LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                                  .olaCheckbox2
                              }
                            </label>
                          </div>
                        </div>
                      )}
                    </div>
                  )}
                  <Row style={styles.buttonRowPadding} lang={this.props.lang}>
                    <Col sm>{/* empty col for spacing */}</Col>
                    <Col sm>
                      <CustomButton
                        label={
                          <>
                            <span style={styles.sendRequestButtonContainerIcon}>
                              <FontAwesomeIcon icon={faPaperPlane} />
                            </span>
                            <span>
                              {
                                LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                                  .sendRequestButton
                              }
                            </span>
                          </>
                        }
                        action={this.openSendRequestConfirmationPopup}
                        disabled={
                          isSendRequestButtonDisabled ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
                        }
                        buttonTheme={BUTTON_TYPE.primary}
                        customStyle={styles.sendButton}
                      />
                    </Col>
                    <Col sm>
                      <CustomButton
                        label={
                          <>
                            <span style={styles.deleteRequestButtonContainerIcon}>
                              <FontAwesomeIcon icon={faTrashAlt} />
                            </span>
                            <span>{LOCALIZE.commons.deleteButton}</span>
                          </>
                        }
                        action={this.handleShowDeleteProcessPopup}
                        buttonTheme={BUTTON_TYPE.danger}
                        customStyle={styles.deleteButton}
                      />
                    </Col>
                  </Row>
                </div>
              )}
            </div>
          )}
        </div>
        <PopupBox
          show={this.state.showAddCandidatePopup || this.state.showEditAssignedCandidatePopup}
          handleClose={() => {}}
          title={
            this.state.showAddCandidatePopup
              ? this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.addCandidatePopup
                    .title
                : LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses.inviteCandidatePopup
                    .title
              : LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                  .assignedCandidatesTable.editAssignedCandidatePopup.title
          }
          description={
            <div>
              <p>
                {
                  LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.addCandidatePopup
                    .description
                }
              </p>
              <Row
                className="align-items-center justify-content-center"
                style={
                  this.state.isValidFirstName ? styles.rowContainer : styles.rowContainerWithError
                }
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label htmlFor="assign-candidate-first-name-input" style={styles.inputTitles}>
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                        .addCandidatePopup.firstNameLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <input
                    id="assign-candidate-first-name-input"
                    className={this.state.isValidFirstName ? "valid-field" : "invalid-field"}
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={this.state.firstName}
                    onChange={this.getFirstNameContent}
                  ></input>
                </Col>
              </Row>
              {!this.state.isValidFirstName && (
                <Row className="align-items-center justify-content-end">
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label htmlFor="assign-candidate-first-name-input" style={styles.errorMessage}>
                      {
                        LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                          .addCandidatePopup.invalidFirstName
                      }
                    </label>
                  </Col>
                </Row>
              )}
              <Row
                className="align-items-center justify-content-center"
                style={
                  this.state.isValidLastName ? styles.rowContainer : styles.rowContainerWithError
                }
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label htmlFor="assign-candidate-last-name-input" style={styles.inputTitles}>
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                        .addCandidatePopup.lastNameLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <input
                    id="assign-candidate-last-name-input"
                    className={this.state.isValidLastName ? "valid-field" : "invalid-field"}
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={this.state.lastName}
                    onChange={this.getLastNameContent}
                  ></input>
                </Col>
              </Row>
              {!this.state.isValidLastName && (
                <Row className="align-items-center justify-content-end">
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label htmlFor="assign-candidate-last-name-input" style={styles.errorMessage}>
                      {
                        LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                          .addCandidatePopup.invalidLastName
                      }
                    </label>
                  </Col>
                </Row>
              )}
              <Row
                className="align-items-center justify-content-center"
                style={
                  this.state.isValidEmail && !this.state.showDuplicateEmailFoundError
                    ? styles.rowContainer
                    : styles.rowContainerWithError
                }
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label htmlFor="assign-candidate-email-input" style={styles.inputTitles}>
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                        .addCandidatePopup.emailLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <input
                    id="assign-candidate-email-input"
                    className={
                      this.state.isValidEmail && !this.state.showDuplicateEmailFoundError
                        ? "valid-field"
                        : "invalid-field"
                    }
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={this.state.email}
                    onChange={this.getEmailContent}
                  ></input>
                </Col>
              </Row>
              {!this.state.isValidEmail && (
                <Row className="align-items-center justify-content-end">
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label htmlFor="assign-candidate-email-input" style={styles.errorMessage}>
                      {
                        LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                          .addCandidatePopup.invalidEmail
                      }
                    </label>
                  </Col>
                </Row>
              )}
              {this.state.showDuplicateEmailFoundError && (
                <Row className="align-items-center justify-content-end">
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label htmlFor="assign-candidate-email-input" style={styles.errorMessage}>
                      {
                        LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                          .addCandidatePopup.duplicateEmailError
                      }
                    </label>
                  </Col>
                </Row>
              )}
              <Row
                className="align-items-center justify-content-center"
                style={styles.rowContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label id="assign-candidate-billing-contact-label" style={styles.inputTitles}>
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                        .addCandidatePopup.billingContactLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <DropdownSelect
                    idPrefix="assign-candidate-billing-contact"
                    isValid={true}
                    ariaRequired={true}
                    ariaLabelledBy="assign-candidate-billing-contact-label"
                    hasPlaceholder={true}
                    options={this.state.billingContactOptions}
                    onChange={this.getSelectedBillingContactOption}
                    defaultValue={this.state.billingContactSelectedOption}
                  />
                </Col>
              </Row>
              <div style={styles.testsToAdministerInPopupContainer}>
                <GenericTable
                  classnamePrefix="assign-candidate-tests-to-administer"
                  columnsDefinition={testsToAdministerInPopupColumnsDefinition}
                  rowsDefinition={this.state.testsToAdministerInPopupRowsDefinition}
                  currentlyLoading={false}
                />
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={
            this.state.showAddCandidatePopup
              ? this.closeAddCandidatePopup
              : this.closeEditAssignedCandidatePopup
          }
          rightButtonType={
            this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES &&
            this.state.showEditAssignedCandidatePopup &&
            !this.state.popupDataUpdated
              ? BUTTON_TYPE.success
              : BUTTON_TYPE.primary
          }
          rightButtonTitle={
            this.state.showAddCandidatePopup
              ? this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.addCandidatePopup
                    .addCandidateButton
                : LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses.inviteCandidatePopup
                    .inviteCandidateButton
              : this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? LOCALIZE.commons.saveButton
                : this.state.popupDataUpdated
                  ? this.state.emailOrAssignedTestsToAdministerUpdated
                    ? LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses
                        .editCandidatePopup.sendUpdatesButton
                    : LOCALIZE.commons.saveButton
                  : LOCALIZE.commons.saved
          }
          rightButtonIcon={
            this.state.showAddCandidatePopup
              ? this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? faPlusCircle
                : faPaperPlane
              : this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? faSave
                : this.state.popupDataUpdated
                  ? this.state.emailOrAssignedTestsToAdministerUpdated
                    ? faPaperPlane
                    : faSave
                  : faCheck
          }
          rightButtonState={
            this.props.source === ManageAssessmentProcessDataSource.ACTIVE_PROCESSES &&
            !this.state.popupDataUpdated
              ? BUTTON_STATE.disabled
              : this.state.isValidAddEditCandidateForm
                ? BUTTON_STATE.enabled
                : BUTTON_STATE.disabled
          }
          rightButtonAction={
            this.state.showAddCandidatePopup
              ? this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? this.handleAddCandidate
                : this.handleInviteCandidate
              : this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                ? this.handleEditAssignedCandidate
                : this.handleSendAssignedCandidateUpdates
          }
        />
        <PopupBox
          show={this.state.showUnassignTestPopup}
          handleClose={() => {}}
          size="lg"
          title={
            LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses.editCandidatePopup
              .unassignTestPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p className="notranslate">
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses
                        .editCandidatePopup.unassignTestPopup.systemMessageDescription
                    }
                  </p>
                }
              />
              <p>
                {
                  LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses.editCandidatePopup
                    .unassignTestPopup.description
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeUnassignTestPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={
            this.state.unassignLoading
              ? LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses.editCandidatePopup
                  .unassignTestPopup.unassignLoading
              : LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses.editCandidatePopup
                  .unassignTestPopup.unassignButton
          }
          rightButtonIcon={this.state.unassignLoading ? faSpinner : faTimesCircle}
          rightButtonAction={this.state.unassignLoading ? {} : this.handleUnassignTest}
          rightButtonState={
            this.state.unassignLoading ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
        />
        {this.state.indexOfRowToEditOrDelete !== null &&
          typeof this.state.assignedCandidatesRowsDefinition.data[
            this.state.indexOfRowToEditOrDelete
          ] !== "undefined" && (
            <PopupBox
              show={this.state.showDeleteAssignedCandidatePopup}
              handleClose={() => {}}
              size="lg"
              title={
                LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                  .assignedCandidatesTable.deleteAssignedCandidatePopup.title
              }
              description={
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.error}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p className="notranslate">
                        {LOCALIZE.formatString(
                          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                            .assignedCandidatesTable.deleteAssignedCandidatePopup
                            .systemMessageDescription,
                          <span style={styles.boldText}>
                            {
                              this.state.assignedCandidatesRowsDefinition.data[
                                this.state.indexOfRowToEditOrDelete
                              ].column_1
                            }
                          </span>,
                          <span style={styles.boldText}>
                            {
                              this.state.assignedCandidatesRowsDefinition.data[
                                this.state.indexOfRowToEditOrDelete
                              ].column_2
                            }
                          </span>
                        )}
                      </p>
                    }
                  />
                  <p>
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                        .assignedCandidatesTable.deleteAssignedCandidatePopup.description
                    }
                  </p>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonIcon={faTimes}
              leftButtonAction={this.closeDeleteAssignedCandidatePopup}
              rightButtonType={BUTTON_TYPE.danger}
              rightButtonTitle={LOCALIZE.commons.deleteButton}
              rightButtonIcon={faTrashAlt}
              rightButtonAction={this.handleDeleteAssignedCandidate}
            />
          )}
        {this.state.indexOfRowToView !== null &&
          typeof this.state[
            this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
              ? "assignCandidatesTestsToAdministerRowsDefinition"
              : "activeProcessesTestsToAdministerRowsDefinition"
          ].data[this.state.indexOfRowToView] !== "undefined" && (
            <PopupBox
              show={this.state.showViewAvailableTestSessionsPopup}
              handleClose={() => {}}
              title={
                LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.testsToAdministerTable
                  .viewAvailableTestSessionsPopup.title
              }
              description={
                <div>
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                        .testsToAdministerTable.viewAvailableTestSessionsPopup.description,
                      <span style={styles.boldText}>
                        {
                          this.state[
                            this.props.source ===
                            ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES
                              ? "assignCandidatesTestsToAdministerRowsDefinition"
                              : "activeProcessesTestsToAdministerRowsDefinition"
                          ].data[this.state.indexOfRowToView].column_1
                        }
                      </span>
                    )}
                  </p>
                  <div style={styles.availableTestSessionsTableStyle}>
                    <GenericTable
                      classnamePrefix="assign-candidates-tests-to-administer"
                      columnsDefinition={availableTestSessionsColumnsDefinition}
                      rowsDefinition={this.state.availableTestSessionsRowsDefinition}
                      emptyTableMessage={
                        LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                          .testsToAdministerTable.viewAvailableTestSessionsPopup.table.noData
                      }
                      currentlyLoading={this.state.availableTestSessionsCurrentlyLoading}
                    />
                  </div>
                </div>
              }
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={LOCALIZE.commons.close}
              rightButtonIcon={faTimes}
              rightButtonAction={this.closeViewAvailableTestSessionsPopup}
            />
          )}
        <PopupBox
          show={this.state.showSendRequestConfirmationPopup}
          handleClose={() => {}}
          size="lg"
          title={
            !this.state.showRequestSentSuccessfullyPopup
              ? LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                  .sendRequestConfirmationPopup.title
              : LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                  .sendRequestConfirmationPopup.titleSuccess
          }
          description={
            <div>
              <SystemMessage
                messageType={
                  !this.state.showRequestSentSuccessfullyPopup
                    ? MESSAGE_TYPE.warning
                    : MESSAGE_TYPE.success
                }
                title={
                  !this.state.showRequestSentSuccessfullyPopup
                    ? LOCALIZE.commons.warning
                    : LOCALIZE.commons.success
                }
                message={
                  <p className="notranslate">
                    {!this.state.showRequestSentSuccessfullyPopup
                      ? LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                          .sendRequestConfirmationPopup.systemMessageDescription
                      : LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                          .sendRequestConfirmationPopup.systemMessageDescriptionSuccess}
                  </p>
                }
              />
              {!this.state.showRequestSentSuccessfullyPopup && (
                <p>
                  {
                    LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                      .sendRequestConfirmationPopup.description
                  }
                </p>
              )}
            </div>
          }
          leftButtonType={
            !this.state.showRequestSentSuccessfullyPopup ? BUTTON_TYPE.secondary : BUTTON_TYPE.none
          }
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={!this.state.showRequestSentSuccessfullyPopup ? faTimes : ""}
          leftButtonAction={
            !this.state.showRequestSentSuccessfullyPopup
              ? this.closeSendRequestConfirmationPopup
              : () => {}
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            !this.state.showRequestSentSuccessfullyPopup
              ? LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                  .sendRequestConfirmationPopup.sendRequestButton
              : LOCALIZE.commons.close
          }
          rightButtonIcon={!this.state.showRequestSentSuccessfullyPopup ? faShare : faTimes}
          rightButtonAction={
            !this.state.showRequestSentSuccessfullyPopup
              ? this.handleSendRequest
              : this.closeSendRequestConfirmationPopup
          }
        />
        <PopupBox
          show={this.state.showAssessmentProcessDataUpdatedSuccessfullyPopup}
          handleClose={() => {}}
          size="lg"
          title={
            LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses.updatesSuccessPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.success}
                title={LOCALIZE.commons.success}
                message={
                  <p className="notranslate">
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses
                        .updatesSuccessPopup.systemMessageDescription
                    }
                  </p>
                }
              />
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonIcon={faTimes}
          rightButtonAction={this.closeAssessmentProcessDataUpdatedSuccessfullyPopup}
        />
        <PopupBox
          show={this.state.showNonSentAssessmentProcessDataUpdatedSuccessfullyPopup}
          handleClose={() => {}}
          size="lg"
          title={
            LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.updatesSuccessPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.success}
                title={LOCALIZE.commons.success}
                message={
                  <p className="notranslate">
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                        .updatesSuccessPopup.systemMessageDescription
                    }
                  </p>
                }
              />
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonIcon={faTimes}
          rightButtonAction={this.closeUpdateNonSentAssessmentProcessDataSuccessfullyPopup}
        />
        <PopupBox
          show={this.state.showEditNamePopup}
          handleClose={() => {}}
          size="lg"
          title={LOCALIZE.formatString(
            LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.editProcessNamePopup
              .title,
            LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.referenceNumberLabel
          )}
          description={
            <div>
              <Row
                className="align-items-center justify-content-center"
                style={
                  this.state.isValidFirstName ? styles.rowContainer : styles.rowContainerWithError
                }
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label
                    htmlFor="edit-assign-candidate-reference-number-input"
                    style={styles.inputTitles}
                  >
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                        .referenceNumberLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <input
                    id="edit-assign-candidate-reference-number-input"
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={this.state.referenceNumber}
                    onChange={this.getReferenceNumberContent}
                  ></input>
                </Col>
              </Row>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeEditNamePopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.saveButton}
          rightButtonIcon={faSave}
          rightButtonAction={this.saveEditNamePopup}
          rightButtonState={
            this.state.referenceNumber === "" ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
        />
        {Object.keys(this.state.selectedAccommodationRequestData).length > 0 &&
          this.state.showAccommodationsRequestDetailsPopup && (
            <AccommodationsRequestDetailsPopup
              source={ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.hr}
              userAccommodationFileId={
                this.state.selectedAccommodationRequestData.user_accommodation_file_id
              }
              selectedUserAccommodationFileData={this.state.selectedAccommodationRequestData}
              showPopup={this.state.showAccommodationsRequestDetailsPopup}
              closeAccommodationsRequestDetailsPopup={this.closeAccommodationsRequestDetailsPopup}
              // not providing isDraft props, since the option to print/download is not available for this user
            />
          )}
        {this.props.source === ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES && (
          <PopupBox
            show={this.state.showDeleteProcessPopup}
            handleClose={this.handleDeleteProcessPopupClose}
            title={
              LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.deleteProcessPopup.title
            }
            description={
              <div>
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.error}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p>
                        {LOCALIZE.formatString(
                          LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates
                            .deleteProcessPopup.message,
                          <span style={styles.boldText}>
                            {Object.keys(this.props.selectedAssessmentProcessRefNbrAssignCandidates)
                              .length > 0
                              ? this.props.selectedAssessmentProcessRefNbrAssignCandidates.label
                              : ""}
                          </span>
                        )}
                      </p>
                    }
                  />
                </div>
                <div>
                  {
                    LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.deleteProcessPopup
                      .description
                  }
                </div>
              </div>
            }
            leftButtonType={BUTTON_TYPE.secondary}
            leftButtonTitle={LOCALIZE.commons.cancel}
            leftButtonAction={this.handleDeleteProcessPopupClose}
            rightButtonType={BUTTON_TYPE.danger}
            rightButtonTitle={LOCALIZE.commons.deleteButton}
            rightButtonAction={this.handleDeleteProcess}
            size={"lg"}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    assessmentProcessDataAssignCandidates:
      state.assessmentProcess.assessmentProcessDataAssignCandidates,
    assessmentProcessDataActiveProcesses:
      state.assessmentProcess.assessmentProcessDataActiveProcesses,
    selectedAssessmentProcessRefNbrAssignCandidates:
      state.assessmentProcess.selectedAssessmentProcessRefNbrAssignCandidates,
    selectedAssessmentProcessRefNbrActiveProcesses:
      state.assessmentProcess.selectedAssessmentProcessRefNbrActiveProcesses,
    triggerDataRerender: state.assessmentProcess.triggerDataRerender
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAssessmentProcessReferenceNumbers,
      getAssessmentProcessData,
      setAssessmentProcessDataAssignCandidates,
      setAssessmentProcessDataActiveProcesses,
      getBillingContactsList,
      getReasonsForTesting,
      assessmentProcessAssignCandidate,
      getAssessmentProcessAssignedCandidates,
      editAssessmentProcessAssignedCandidate,
      deleteAssessmentProcessAssignedCandidate,
      sendAssessmentProcessAssignedCandidatesRequest,
      triggerDataRerenderFunction,
      getAvailableTestSessionsForSpecificTestToAdminister,
      setSelectedAssessmentProcessRefNbrAssignCandidates,
      setSelectedAssessmentProcessRefNbrActiveProcesses,
      getOrganization,
      sendAssessmentProcessUpdatesRequest,
      inviteSingleAssessmentProcessCandidateRequest,
      unassignAssessmentProcessAssignedTestToAdminister,
      updateNonSentAssessmentProcessData,
      updateReferenceNumber,
      setAssessmentProcessLabel,
      deleteAssessmentProcess
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ManageAssessmentProcessData);
