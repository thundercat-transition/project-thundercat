import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "../../css/profile-permissions.css";
import { Row, Col } from "react-bootstrap";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import validateName, { validateEmail } from "../../helpers/regexValidator";
import DropdownSelect from "../commons/DropdownSelect";
import { LANGUAGES } from "../commons/Translation";
import { getOrganization } from "../../modules/ExtendedProfileOptionsRedux";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  }
};

const styles = {
  container: {
    width: "100%",
    position: "relative"
  },
  formContainer: {
    padding: "24px 6px"
  },
  itemContainer: {
    margin: "12px 0"
  },
  label: {
    paddingRight: 12,
    verticalAlign: "middle"
  },
  inputContainer: {
    verticalAlign: "middle"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  iconContainer: {
    padding: 0,
    verticalAlign: "middle"
  },
  icon: {
    color: "#00565e",
    marginTop: "-1px",
    verticalAlign: "middle"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "4px 0 0 4px"
  }
};

class AddModifyBillingContactForm extends Component {
  static propTypes = {
    billingContactObject: PropTypes.object.isRequired,
    changeBillingContactObject: PropTypes.func.isRequired,
    // Determines if we are in the Add or Modify Popup
    isAddBillingContactPopup: PropTypes.bool.isRequired
  };

  state = {
    firstName: "",
    isFirstNameValid: true,
    lastName: "",
    isLastNameValid: true,
    email: "",
    isEmailValid: true,
    departmentId: null,
    fisOrgCode: "",
    isFisOrgCodeValid: true,
    fisRefCode: "",
    isFisRefCodeValid: true,
    departmentOptions: []
  };

  componentDidMount = () => {
    // populating department options
    this.populateDepartmentOptions();

    // populate the states, if we are editing
    if (Object.keys(this.props.billingContactObject).length !== 0) this.populateStates();
  };

  // complete validation of the form
  validFormChange = () => {
    const { firstName, lastName, email, fisOrgCode, fisRefCode } = this.state;

    // taking care of all the validity booleans
    const isValidFirstName = typeof firstName === "undefined" || validateName(firstName);
    const isValidLastName = typeof lastName === "undefined" || validateName(lastName);
    const isValidEmail = typeof email === "undefined" || validateEmail(email);
    const isValidfFisOrgCode = typeof fisOrgCode === "undefined" || fisOrgCode !== "";
    const isValidFisRefCode = typeof fisRefCode === "undefined" || fisRefCode !== "";

    // updating the validity booleans + sending the form validity+object to prop component
    this.setState(
      {
        isFirstNameValid: isValidFirstName,
        isLastNameValid: isValidLastName,
        isEmailValid: isValidEmail,
        isFisOrgCodeValid: isValidfFisOrgCode,
        isFisRefCodeValid: isValidFisRefCode
      },
      () => {
        const {
          firstName,
          lastName,
          email,
          departmentId,
          fisOrgCode,
          fisRefCode,
          isFirstNameValid,
          isLastNameValid,
          isEmailValid,
          isFisOrgCodeValid,
          isFisRefCodeValid
        } = this.state;

        // since we already checked that the fields are not empty, we use validity booleans + looking if fields are still undefined
        const validForm =
          isFirstNameValid &&
          typeof firstName !== "undefined" &&
          isLastNameValid &&
          typeof lastName !== "undefined" &&
          isEmailValid &&
          typeof email !== "undefined" &&
          departmentId !== null &&
          typeof departmentId !== "undefined" &&
          isFisOrgCodeValid &&
          typeof fisOrgCode !== "undefined" &&
          isFisRefCodeValid &&
          typeof fisRefCode !== "undefined";

        // updating object in ManageBillingContacts + validForm indicator
        this.props.changeBillingContactObject(
          firstName,
          lastName,
          email,
          departmentId,
          fisOrgCode,
          fisRefCode,
          validForm
        );
      }
    );
  };

  // In the case of a Modify, update the states to the selected billing contact
  populateStates = () => {
    this.setState({
      firstName: this.props.billingContactObject.first_name,
      lastName: this.props.billingContactObject.last_name,
      email: this.props.billingContactObject.email,
      departmentId: this.props.billingContactObject.dept_id,
      fisOrgCode: this.props.billingContactObject.fis_organisation_code,
      fisRefCode: this.props.billingContactObject.fis_reference_code
    });
  };

  // update first name content
  updateFirstNameContent = event => {
    const firstName = event.target.value;

    // allow maximum of 30 chars
    const regexExpression = /^(.{0,30})$/;
    if (regexExpression.test(firstName)) {
      this.setState(
        {
          firstName: firstName
        },
        () => {
          this.validFormChange();
        }
      );
    }
  };

  // update last name content
  updateLastNameContent = event => {
    const lastName = event.target.value;
    // allow maximum of 30 chars
    const regexExpression = /^(.{0,30})$/;
    if (regexExpression.test(lastName)) {
      this.setState(
        {
          lastName: lastName
        },
        () => {
          this.validFormChange();
        }
      );
    }
  };

  // update email content
  updateEmailContent = event => {
    const email = event.target.value;

    this.setState(
      {
        email: email
      },
      () => {
        this.validFormChange();
      }
    );
  };

  // update department id content
  updateDepartmentIdContent = event => {
    const departmentId = event.value;
    this.setState(
      {
        departmentId: departmentId
      },
      () => {
        this.validFormChange();
      }
    );
  };

  // update fisOrgCode content
  updateFisOrgCodeContent = event => {
    const fisOrgCode = event.target.value;
    // allow maximum of 16 chars (prevent use of comma)
    const regexExpression = /^([^,]{0,16})$/;
    if (regexExpression.test(fisOrgCode)) {
      this.setState(
        {
          fisOrgCode: fisOrgCode
        },
        () => {
          this.validFormChange();
        }
      );
    }
  };

  // update fisRefCode content
  updateFisRefCodeContent = event => {
    const fisRefCode = event.target.value;
    // allow maximum of 20 chars (prevent use of comma)
    const regexExpression = /^([^,]{0,20})$/;
    if (regexExpression.test(fisRefCode)) {
      this.setState(
        {
          fisRefCode: fisRefCode
        },
        () => {
          this.validFormChange();
        }
      );
    }
  };

  // Generate the Organizations Dropdown - Labels/Values
  populateDepartmentOptions = () => {
    // initializing departmentOptions
    const departmentOptions = [];
    this.props.getOrganization().then(response => {
      for (let i = 0; i < response.body.length; i++) {
        // Interface is in English
        if (this.props.currentLanguage === LANGUAGES.english) {
          departmentOptions.push({
            label: `${response.body[i].edesc} (${response.body[i].eabrv})`,
            value: response.body[i].dept_id
          });
          // Interface is in French
        } else {
          departmentOptions.push({
            label: `${response.body[i].fdesc} (${response.body[i].fabrv})`,
            value: response.body[i].dept_id
          });
        }
      }
      this.setState({ departmentOptions: departmentOptions });
    });
  };

  // Gets the value selected in the organization dropdown
  getSelectedDepartment = () => {
    if (this.state.departmentId) {
      const foundOrg = this.state.departmentOptions.find(element => {
        return element.value === this.state.departmentId;
      });
      if (foundOrg) {
        return {
          value: foundOrg.value,
          label: foundOrg.label
        };
      }
    }
    return [];
  };

  render() {
    const {
      firstName,
      isFirstNameValid,
      lastName,
      isLastNameValid,
      email,
      isEmailValid,
      fisOrgCode,
      isFisOrgCodeValid,
      fisRefCode,
      isFisRefCodeValid
    } = this.state;

    // value selected in the organization dropdown
    const department = this.getSelectedDepartment();

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.container}>
        <Row>
          <p>
            {this.props.isAddBillingContactPopup
              ? LOCALIZE.inPersonTesting.manageBillingContacts.addBillingContactPopup.description
              : LOCALIZE.inPersonTesting.manageBillingContacts.modifyBillingContactPopup
                  .description}
          </p>
        </Row>
        <div style={styles.formContainer}>
          <Row style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label id="first-name-title" htmlFor="first-name" style={styles.gocEmailTooltipLabel}>
                {LOCALIZE.inPersonTesting.manageBillingContacts.billingContactPopup.firstName.title}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <input
                id="first-name"
                className={isFirstNameValid ? "valid-field" : "invalid-field"}
                aria-labelledby="first-name-title first-name-error"
                aria-required={true}
                aria-invalid={!isFirstNameValid}
                style={{ ...styles.input, ...accommodationsStyle }}
                type="text"
                value={firstName}
                onChange={this.updateFirstNameContent}
              ></input>
              {!isFirstNameValid && (
                <label
                  id="first-name-error"
                  htmlFor="first-name"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {
                    LOCALIZE.inPersonTesting.manageBillingContacts.billingContactPopup.firstName
                      .error
                  }
                </label>
              )}
            </Col>
          </Row>
          <Row style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label id="last-name-title" htmlFor="last-name" style={styles.label}>
                {LOCALIZE.inPersonTesting.manageBillingContacts.billingContactPopup.lastName.title}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <input
                id="last-name"
                className={isLastNameValid ? "valid-field" : "invalid-field"}
                aria-labelledby="last-name-title last-name-error"
                aria-required={true}
                aria-invalid={!isLastNameValid}
                style={{ ...styles.input, ...accommodationsStyle }}
                type="text"
                value={lastName}
                onChange={this.updateLastNameContent}
              ></input>
              {!isLastNameValid && (
                <label
                  id="last-name-error"
                  htmlFor="last-name"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {
                    LOCALIZE.inPersonTesting.manageBillingContacts.billingContactPopup.lastName
                      .error
                  }
                </label>
              )}
            </Col>
          </Row>
          <Row style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label id="email-title" htmlFor="email" style={styles.label}>
                {LOCALIZE.inPersonTesting.manageBillingContacts.billingContactPopup.email.title}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <input
                id="email"
                className={isEmailValid ? "valid-field" : "invalid-field"}
                aria-labelledby="email-title email-error"
                aria-required={true}
                aria-invalid={!isEmailValid}
                style={{ ...styles.input, ...accommodationsStyle }}
                type="text"
                value={email}
                onChange={this.updateEmailContent}
              ></input>
              {!isEmailValid && (
                <label
                  id="email-error"
                  htmlFor="email"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {LOCALIZE.inPersonTesting.manageBillingContacts.billingContactPopup.email.error}
                </label>
              )}
            </Col>
          </Row>
          <Row style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label id="department-id-title" style={styles.label}>
                {
                  LOCALIZE.inPersonTesting.manageBillingContacts.billingContactPopup.departmentId
                    .title
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <DropdownSelect
                idPrefix="department-id"
                isValid={true}
                ariaRequired={true}
                ariaLabelledBy="department-id-label"
                hasPlaceholder={true}
                options={this.state.departmentOptions}
                onChange={this.updateDepartmentIdContent}
                defaultValue={department}
              />
            </Col>
          </Row>
          <Row style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                id="fis-organisation-code-title"
                htmlFor="fis-organisation-code"
                style={styles.label}
              >
                {
                  LOCALIZE.inPersonTesting.manageBillingContacts.billingContactPopup.fisOrgCode
                    .title
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <input
                id="fis-organisation-code"
                className={isFisOrgCodeValid ? "valid-field" : "invalid-field"}
                aria-labelledby="fis-organisation-code-title fis-organisation-code-error"
                aria-required={true}
                aria-invalid={!isFisOrgCodeValid}
                style={{ ...styles.input, ...accommodationsStyle }}
                type="text"
                value={fisOrgCode}
                onChange={this.updateFisOrgCodeContent}
              ></input>
              {!isFisOrgCodeValid && (
                <label
                  id="fis-organisation-code-error"
                  htmlFor="fis-organisation-code"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {
                    LOCALIZE.inPersonTesting.manageBillingContacts.billingContactPopup.fisOrgCode
                      .error
                  }
                </label>
              )}
            </Col>
          </Row>
          <Row style={styles.itemContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                id="fis-reference-code-title"
                htmlFor="fis-reference-code-code"
                style={styles.label}
              >
                {
                  LOCALIZE.inPersonTesting.manageBillingContacts.billingContactPopup.fisRefCode
                    .title
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputContainer}
            >
              <input
                id="fis-reference-code"
                className={isFisRefCodeValid ? "valid-field" : "invalid-field"}
                aria-labelledby="fis-reference-code-title fis-reference-code-error"
                aria-required={true}
                aria-invalid={!isFisRefCodeValid}
                style={{ ...styles.input, ...accommodationsStyle }}
                type="text"
                value={fisRefCode}
                onChange={this.updateFisRefCodeContent}
              ></input>
              {!isFisRefCodeValid && (
                <label
                  id="fis-reference-code-error"
                  htmlFor="fis-reference-code"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {
                    LOCALIZE.inPersonTesting.manageBillingContacts.billingContactPopup.fisRefCode
                      .error
                  }
                </label>
              )}
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export { AddModifyBillingContactForm as unconnectedAddModifyBillingContactForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({ getOrganization }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AddModifyBillingContactForm);
