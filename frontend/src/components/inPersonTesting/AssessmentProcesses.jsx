import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import CreateProcess from "./CreateProcess";
import ProcessResults from "./ProcessResults";
import ManageAssessmentProcessData, {
  ManageAssessmentProcessDataSource
} from "./ManageAssessmentProcessData";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  }
};

class AssessmentProcesses extends Component {
  render() {
    const TABS = [
      {
        key: "create-process",
        tabName: LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.title,
        body: <CreateProcess />
      },
      {
        key: "assign-candidates",
        tabName: LOCALIZE.inPersonTesting.assessmentProcesses.assignCandidates.title,
        body: (
          <ManageAssessmentProcessData
            source={ManageAssessmentProcessDataSource.ASSIGN_CANDIDATES}
          />
        )
      },
      {
        key: "active-processes",
        tabName: LOCALIZE.inPersonTesting.assessmentProcesses.activeProcesses.title,
        body: (
          <ManageAssessmentProcessData
            source={ManageAssessmentProcessDataSource.ACTIVE_PROCESSES}
          />
        )
      },
      {
        key: "process-results",
        tabName: LOCALIZE.inPersonTesting.assessmentProcesses.processResults.title,
        body: <ProcessResults />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.inPersonTesting.assessmentProcesses.title}</h2>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey="create-process"
                id="assessment-processes-tabs"
                style={styles.tabNavigation}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AssessmentProcesses);
