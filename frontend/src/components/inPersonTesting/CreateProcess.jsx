import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Col, Row } from "react-bootstrap";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import DropdownSelect from "../commons/DropdownSelect";
import { LANGUAGES } from "../commons/Translation";
import { getOrganization } from "../../modules/ExtendedProfileOptionsRedux";
import Switch from "react-switch";
import getSwitchTransformScale from "../../helpers/switchTransformScale";
import {
  createAssessmentProcess,
  getAvailableTestSessionsForSpecificTestToAdminister,
  getBillingContactsList,
  triggerDataRerenderFunction
} from "../../modules/AssesmentProcessRedux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import { alternateColorsStyle } from "../commons/Constants";
import {
  faPlusCircle,
  faShare,
  faTimes,
  faTrashAlt,
  faCalendarCheck
} from "@fortawesome/free-solid-svg-icons";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../commons/PopupBox";
import { getTestSkillSubTypes, getTestSkillTypes } from "../../modules/TestBuilderRedux";
import { getReasonsForTesting } from "../../modules/UitRedux";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import { TestSkillTypeCodename, TestSkillSleDescCodename } from "../testFactory/Constants";
import { validateEmail } from "../../helpers/regexValidator";
import { convertDateTimeToUsersTimezone } from "../../modules/helpers";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  }
};

const styles = {
  mainContainer: {
    width: "100%"
  },
  formContainer: {
    margin: 24
  },
  rowContainer: {
    margin: "12px 24px"
  },
  rowContainerWithError: {
    margin: "12px 24px 0px 24px"
  },
  inputTitles: {
    fontWeight: "bold"
  },
  input: {
    minHeight: 38,
    border: "1px solid #00565e",
    width: "100%",
    padding: "3px 6px 3px 6px"
  },
  tableContainer: {
    margin: "0 48px",
    width: "100%"
  },
  addTestToAdministerButtonIcon: {
    marginRight: 12,
    color: "#00565e"
  },
  addTestToAdministerButton: {
    background: "transparent",
    padding: 12,
    border: "none",
    color: " #00565e"
  },
  allUnset: {
    all: "unset"
  },
  tableIcon: {
    transform: "scale(1.2)"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  boldText: {
    fontWeight: "bold"
  },
  createProcessButtonContainer: {
    textAlign: "center",
    marginTop: 48
  },
  createProcessButtonIcon: {
    marginRight: 12
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6,
    width: 250
  },
  availableTestSessionsTableStyle: {
    padding: "12px 0"
  }
};

// allow only alphanumeric, slash and dash (0 to 30 chars)
const referenceNumberRegexExpression = /^([a-zA-zÀ-ÿ0-9-/]{0,30})$/;

class CreateProcess extends Component {
  constructor(props) {
    super(props);
    this.createProcessClosingDateDayFieldRef = React.createRef();
  }

  state = {
    isValidForm: false,
    referenceNumber: "",
    departmentOptions: [],
    departmentSelectedOption: [],
    triggerDateValidation: false,
    isLoadingSwitch: false,
    // default values
    switchHeight: 25,
    switchWidth: 50,
    duration: "",
    allowBookingExternalTc: false,
    defaultBillingContactOptions: [],
    defaultBillingContactSelectedOption: [],
    contactEmailForCandidates: "",
    isValidContactEmailForCandidates: true,
    rowsDefinition: {},
    showAddTestToAdministerPopup: false,
    isValidAddTestToAdministerForm: false,
    testSkillTypeOptions: [],
    testSkillTypeSelectedOption: [],
    testSkillSubTypeOptions: [],
    testSkillSubTypeSelectedOption: [],
    defaultReasonForTestingOptions: [],
    defaultReasonForTestingSelectedOption: [],
    defaultLevelRequired: "",
    showViewAvailableTestSessionsPopup: false,
    availableTestSessionsCurrentlyLoading: false,
    availableTestSessionsRowsDefinition: {},
    showDeleteTestToAdministerPopup: false,
    indexOfRowToViewOrDelete: null,
    showConfirmationPopup: false,
    triggerResetClosingDateFields: false
  };

  componentDidMount = () => {
    // populating department options
    this.populateDepartmentOptions();
    // getting updated switch dimensions
    this.getUpdatedSwitchDimensions();
    // populating default billing contact options
    this.populateDefaultBillingContactOptions();
    // initializing tests to administer table
    this.initializingTestsToAdministerObject();
  };

  componentDidUpdate = prevProps => {
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
    // if triggerDataRerender gets updated
    if (prevProps.triggerDataRerender !== this.props.triggerDataRerender) {
      // populating default billing contact options
      this.populateDefaultBillingContactOptions();
    }
  };

  populateDepartmentOptions = () => {
    // initializing departmentOptions
    const departmentOptions = [];
    this.props.getOrganization().then(response => {
      for (let i = 0; i < response.body.length; i++) {
        // Interface is in English
        if (this.props.currentLanguage === LANGUAGES.english) {
          departmentOptions.push({
            label: `${response.body[i].edesc} (${response.body[i].eabrv})`,
            value: `${response.body[i].dept_id}`
          });
          // Interface is in French
        } else {
          departmentOptions.push({
            label: `${response.body[i].fdesc} (${response.body[i].fabrv})`,
            value: `${response.body[i].dept_id}`
          });
        }
      }
      this.setState({ departmentOptions: departmentOptions });
    });
  };

  populateDefaultBillingContactOptions = () => {
    // initializing defaultBillingContactOptions
    const defaultBillingContactOptions = [];
    this.props.getBillingContactsList().then(response => {
      // if an option has been selected
      if (Object.keys(this.state.defaultBillingContactSelectedOption).length > 0) {
        // adding deselect option
        defaultBillingContactOptions.push({
          label: LOCALIZE.dropdownSelect.pleaseSelect,
          value: null
        });
      }
      // looping in results
      for (let i = 0; i < response.length; i++) {
        defaultBillingContactOptions.push({
          label: `${response[i].first_name} ${response[i].last_name} (${response[i].email})`,
          value: response[i].id
        });
      }
      this.setState({ defaultBillingContactOptions: defaultBillingContactOptions });
    });
  };

  getReferenceNumberContent = event => {
    const referenceNumber = event.target.value;
    if (referenceNumberRegexExpression.test(referenceNumber)) {
      this.setState({ referenceNumber: referenceNumber.toUpperCase() }, () => {
        // validating form
        this.validateForm();
      });
    }
  };

  getSelectedDepartmentOption = selectedOption => {
    this.setState(
      {
        departmentSelectedOption: selectedOption
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoadingSwitch: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoadingSwitch: false });
        }
      );
    });
  };

  getDurationContent = event => {
    const duration = event.target.value;
    // allow only numeric values (0 to 3 chars)
    const regexExpression = /^([0-9]{0,3})$/;
    if (regexExpression.test(duration)) {
      this.setState({ duration: duration !== "" ? parseInt(duration) : "" }, () => {
        // validating form
        this.validateForm();
      });
    }
  };

  handleAllowBookingExternalTcSwitchUpdates = event => {
    this.setState({ allowBookingExternalTc: event });
  };

  getSelectedDefaultBillingContactOption = selectedOption => {
    let formattedSelectedOption = selectedOption;
    // selected option is null (select option)
    if (selectedOption.value === null) {
      formattedSelectedOption = [];
    }
    this.setState(
      {
        defaultBillingContactSelectedOption: formattedSelectedOption
      },
      () => {
        // validating form
        this.validateForm();
        // populating default billing contact options
        this.populateDefaultBillingContactOptions();
      }
    );
  };

  getContactEmailForCandidatesContent = event => {
    const contactEmailForCandidates = event.target.value;
    this.setState({ contactEmailForCandidates: contactEmailForCandidates }, () => {
      // validating form
      this.validateForm();
    });
  };

  // Populate Last Row Content
  populateLastRowHtml = () => {
    return (
      <td
        id="assessment-process-add-test-to-administer-button"
        colSpan="4"
        style={alternateColorsStyle(this.state.rowsDefinition.data.length, 60)}
      >
        <CustomButton
          label={
            <>
              <span style={styles.addTestToAdministerButtonIcon}>
                <FontAwesomeIcon icon={faPlusCircle} />
              </span>
              <span>
                {
                  LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerTable
                    .addTestToAdministerButton
                }
              </span>
            </>
          }
          action={this.openAddTestToAdministerPopup}
          customStyle={styles.addTestToAdministerButton}
        />
      </td>
    );
  };

  // Populate Empty Table with Generate Button
  populateEmptyTable = () => {
    return (
      <CustomButton
        label={
          <>
            <span style={styles.addTestToAdministerButtonIcon}>
              <FontAwesomeIcon icon={faPlusCircle} />
            </span>
            <span>
              {
                LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerTable
                  .addTestToAdministerButton
              }
            </span>
          </>
        }
        action={this.openAddTestToAdministerPopup}
        customStyle={styles.addTestToAdministerButton}
      />
    );
  };

  openAddTestToAdministerPopup = () => {
    this.setState({ showAddTestToAdministerPopup: true });
  };

  closeAddTestToAdministerPopup = () => {
    this.setState({
      showAddTestToAdministerPopup: false,
      isValidAddTestToAdministerForm: false,
      testSkillTypeOptions: [],
      testSkillTypeSelectedOption: [],
      testSkillSubTypeOptions: [],
      testSkillSubTypeSelectedOption: [],
      defaultReasonForTestingOptions: [],
      defaultReasonForTestingSelectedOption: [],
      defaultLevelRequired: ""
    });
  };

  populatePopupDropdowns = () => {
    // populating test skill type options
    this.populateTestSkillTypeOptions();
  };

  populateTestSkillTypeOptions = () => {
    // initializing testSkillTypeOptions
    const testSkillTypeOptions = [];
    this.props.getTestSkillTypes().then(response => {
      // looping in response body
      for (let i = 0; i < response.body.length; i++) {
        // hiding NONE test skill type
        if (response.body[i].codename !== TestSkillTypeCodename.NONE) {
          // populating testSkillTypeOptions
          testSkillTypeOptions.push({
            label: response.body[i].test_skill_type_text[this.props.currentLanguage][0].text,
            value: response.body[i].id,
            codename: response.body[i].codename
          });
        }
      }
      this.setState({ testSkillTypeOptions: testSkillTypeOptions });
    });
  };

  populateTestSkillSubTypeOptions = testSkillTypeId => {
    // initializing testSkillTypeOptions
    const testSkillSubTypeOptions = [];
    this.props.getTestSkillSubTypes(testSkillTypeId).then(response => {
      // looping in response body
      for (let i = 0; i < response.body.length; i++) {
        // making sure that the current test skill sub type is not part of the tests to administer table
        if (
          typeof this.state.rowsDefinition.data !== "undefined" &&
          this.state.rowsDefinition.data.filter(
            obj =>
              obj.test_skill_sub_type_id === response.body[i].id &&
              obj.test_skill_type_id === testSkillTypeId
          ).length <= 0
        ) {
          testSkillSubTypeOptions.push({
            label: response.body[i].test_skill_sub_type_text[this.props.currentLanguage][0].text,
            value: response.body[i].id,
            codename: response.body[i].codename
          });
        }
      }
      this.setState({ testSkillSubTypeOptions: testSkillSubTypeOptions }, () => {
        // validating popup form
        this.validatePopupForm();
      });
    });
  };

  populateDefaultReasonForTestingOptions = () => {
    // initializing defaultReasonForTestingOptions
    const defaultReasonForTestingOptions = [];
    this.props
      .getReasonsForTesting(
        this.state.testSkillTypeSelectedOption.value,
        this.state.testSkillSubTypeSelectedOption.value
      )
      .then(response => {
        // if an option has been selected
        if (Object.keys(this.state.defaultReasonForTestingSelectedOption).length > 0) {
          // adding deselect option
          defaultReasonForTestingOptions.push({
            label: LOCALIZE.dropdownSelect.pleaseSelect,
            value: null
          });
        }
        // looping in response body
        for (let i = 0; i < response.length; i++) {
          defaultReasonForTestingOptions.push({
            label: response[i][`reason_for_testing_name_${this.props.currentLanguage}`],
            value: response[i].id
          });
        }
        this.setState({ defaultReasonForTestingOptions: defaultReasonForTestingOptions });
      });
  };

  getSelectedTestSkillTypeOption = selectedOption => {
    this.setState(
      {
        testSkillTypeSelectedOption: selectedOption,
        testSkillSubTypeSelectedOption: [],
        defaultReasonForTestingSelectedOption: []
      },
      () => {
        // populating test skill sub type options
        this.populateTestSkillSubTypeOptions(selectedOption.value);
      }
    );
  };

  getSelectedTestSkillSubTypeOption = selectedOption => {
    this.setState(
      {
        testSkillSubTypeSelectedOption: selectedOption,
        defaultReasonForTestingSelectedOption: []
      },
      () => {
        // populating default reason for testing options
        this.populateDefaultReasonForTestingOptions();
        // validating popup form
        this.validatePopupForm();
      }
    );
  };

  getSelectedDefaultReasonForTestingOption = selectedOption => {
    let formattedSelectedOption = selectedOption;
    // selected option is null (select option)
    if (selectedOption.value === null) {
      formattedSelectedOption = [];
    }
    this.setState(
      {
        defaultReasonForTestingSelectedOption: formattedSelectedOption
      },
      () => {
        // validating popup form
        this.validatePopupForm();
      }
    );
  };

  getDefaultLevelRequiredContent = event => {
    const defaultLevelRequired = event.target.value;
    // allow only A/B/C
    const regexExpression = /^([a,A,b,B,c,C]{0,1})$/;
    if (regexExpression.test(defaultLevelRequired)) {
      this.setState({ defaultLevelRequired: defaultLevelRequired.toUpperCase() }, () => {
        // validating popup form
        this.validatePopupForm();
      });
    }
  };

  validatePopupForm = () => {
    // initializing needed variables
    let isValidAddTestToAdministerForm = false;

    // valid if test skill type option has been selected
    const isValidTestSkillType = Object.keys(this.state.testSkillTypeSelectedOption).length > 0;
    // valid if test skill sub type options is defined and we have a selected option or if selected test skill type is not SLE or OCC
    const isValidTestSkillSubType =
      this.state.testSkillSubTypeOptions.length > 0
        ? Object.keys(this.state.testSkillSubTypeSelectedOption).length > 0
        : this.state.testSkillTypeSelectedOption.codename !== TestSkillTypeCodename.SLE &&
          this.state.testSkillTypeSelectedOption.codename !== TestSkillTypeCodename.OCC;

    // if all mandatory fields are valid
    if (isValidTestSkillType && isValidTestSkillSubType) {
      isValidAddTestToAdministerForm = true;
    }
    this.setState({
      isValidAddTestToAdministerForm: isValidAddTestToAdministerForm
    });
  };

  // initializing tests to administer table
  initializingTestsToAdministerObject = () => {
    const data = [];
    // initializing rowsDefinition object
    const rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.LEFT_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState({
      rowsDefinition: rowsDefinition
    });
  };

  handleAddTestToAdminister = () => {
    // creating copy of rowsDefinition
    const copyOfRowsDefinition = { ...this.state.rowsDefinition };
    // initializing index of next insert
    const indexOfNextInsert = copyOfRowsDefinition.data.length;
    // formatting test skill
    const formattedTestSkill =
      Object.keys(this.state.testSkillSubTypeSelectedOption).length > 0
        ? `${this.state.testSkillTypeSelectedOption.label} - ${this.state.testSkillSubTypeSelectedOption.label}`
        : `${this.state.testSkillTypeSelectedOption.label}`;
    // initializing newTestToAdministerObj
    const newTestToAdministerObj = {
      test_skill_type_id: this.state.testSkillTypeSelectedOption.value,
      test_skill_sub_type_id:
        Object.keys(this.state.testSkillSubTypeSelectedOption).length > 0
          ? this.state.testSkillSubTypeSelectedOption.value
          : null,
      test_skill_sub_type_codename:
        Object.keys(this.state.testSkillSubTypeSelectedOption).length > 0
          ? this.state.testSkillSubTypeSelectedOption.codename
          : null,
      formatted_test_skill: formattedTestSkill,
      default_reason_for_testing_id:
        Object.keys(this.state.defaultReasonForTestingSelectedOption).length > 0
          ? this.state.defaultReasonForTestingSelectedOption.value
          : null,
      default_level_required: this.state.defaultLevelRequired,
      column_1: formattedTestSkill,
      column_2:
        Object.keys(this.state.defaultReasonForTestingSelectedOption).length > 0
          ? this.state.defaultReasonForTestingSelectedOption.label
          : LOCALIZE.commons.na,
      column_3:
        this.state.defaultLevelRequired !== ""
          ? this.state.defaultLevelRequired
          : LOCALIZE.commons.na,
      column_4: (
        <>
          <StyledTooltip
            id={`create-process-view-available-test-sessions-row-${indexOfNextInsert}`}
            place="top"
            variant={TYPE.light}
            effect={EFFECT.solid}
            openOnClick={false}
            tooltipElement={
              <div style={styles.allUnset}>
                <CustomButton
                  dataTip=""
                  dataFor={`create-process-view-available-test-sessions-row-${indexOfNextInsert}`}
                  label={
                    <>
                      <FontAwesomeIcon icon={faCalendarCheck} style={styles.tableIcon} />
                    </>
                  }
                  action={() => {
                    this.openViewAvailableTestSessionsPopup(indexOfNextInsert);
                  }}
                  customStyle={styles.actionButton}
                  buttonTheme={THEME.PRIMARY}
                  ariaLabel={LOCALIZE.formatString(
                    LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                      .testsToAdministerTable.viewAvailableTestSessionsAccessibility,
                    formattedTestSkill
                  )}
                  disabled={
                    this.state.isValidForm &&
                    copyOfRowsDefinition[indexOfNextInsert - 1] !==
                      TestSkillSleDescCodename.ORAL_EN &&
                    copyOfRowsDefinition[indexOfNextInsert - 1] !== TestSkillSleDescCodename.ORAL_FR
                      ? BUTTON_STATE.enabled
                      : BUTTON_STATE.disabled
                  }
                />
              </div>
            }
            tooltipContent={
              <div>
                <p>
                  {
                    LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                      .testsToAdministerTable.viewAvailableTestSessions
                  }
                </p>
              </div>
            }
          />
          <StyledTooltip
            id={`create-process-delete-test-to-administer-row-${indexOfNextInsert}`}
            place="top"
            variant={TYPE.light}
            effect={EFFECT.solid}
            openOnClick={false}
            tooltipElement={
              <div style={styles.allUnset}>
                <CustomButton
                  dataTip=""
                  dataFor={`create-process-delete-test-to-administer-row-${indexOfNextInsert}`}
                  label={
                    <>
                      <FontAwesomeIcon icon={faTrashAlt} style={styles.tableIcon} />
                    </>
                  }
                  action={() => {
                    this.openDeleteTestToAdministerPopup(indexOfNextInsert);
                  }}
                  customStyle={styles.actionButton}
                  buttonTheme={THEME.PRIMARY}
                  ariaLabel={LOCALIZE.formatString(
                    LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                      .testsToAdministerTable.deleteTestAccessibility,
                    formattedTestSkill
                  )}
                />
              </div>
            }
            tooltipContent={
              <div>
                <p>{LOCALIZE.commons.deleteButton}</p>
              </div>
            }
          />
        </>
      )
    };
    // updating rowsDefinition state
    copyOfRowsDefinition.data.push(newTestToAdministerObj);
    this.setState({ rowsDefinition: copyOfRowsDefinition }, () => {
      // closing popup
      this.closeAddTestToAdministerPopup();
      // validating form
      this.validateForm();
    });
  };

  openViewAvailableTestSessionsPopup = index => {
    this.setState(
      {
        availableTestSessionsCurrentlyLoading: true,
        showViewAvailableTestSessionsPopup: true,
        indexOfRowToViewOrDelete: index
      },
      () => {
        // getting available test sessions
        this.props
          .getAvailableTestSessionsForSpecificTestToAdminister(
            this.state.duration,
            this.state.rowsDefinition.data[index].test_skill_type_id,
            this.state.allowBookingExternalTc,
            this.state.rowsDefinition.data[index].test_skill_sub_type_id,
            this.state.departmentSelectedOption.value,
            null
          )
          .then(response => {
            // initializing needed object and array for availableTestSessionsRowsDefinition state (needed for GenericTable component)
            let availableTestSessionsRowsDefinition = {};
            const data = [];
            for (let i = 0; i < response.length; i++) {

              // Converting time to users timezone
              const adjustedStartTime = convertDateTimeToUsersTimezone(
                response[i].start_time
              ).adjustedTime;

              const adjustedEndTime = convertDateTimeToUsersTimezone(response[i].end_time).adjustedTime;

              // pushing needed data in data array (needed for GenericTable component)
              data.push({
                column_1: `${response[i].date} ${adjustedStartTime} - ${adjustedEndTime}`,
                column_2: response[i].test_center_name,
                column_3:
                  this.props.currentLanguage === LANGUAGES.english
                    ? `${response[i].dept_edesc} (${response[i].dept_eabrv})`
                    : `${response[i].dept_fdesc} (${response[i].dept_fabrv})`,
                column_4: response[i].test_center_city,
                column_5: `${response[i].spaces_unbooked} / ${response[i].spaces_available}`
              });
            }

            // updating availableTestSessionsRowsDefinition object with provided data and needed style
            availableTestSessionsRowsDefinition = {
              column_1_style: COMMON_STYLE.LEFT_TEXT,
              column_2_style: COMMON_STYLE.LEFT_TEXT,
              column_3_style: COMMON_STYLE.LEFT_TEXT,
              column_4_style: COMMON_STYLE.LEFT_TEXT,
              column_5_style: COMMON_STYLE.CENTERED_TEXT,
              data: data
            };
            // updating needed states
            this.setState({
              availableTestSessionsRowsDefinition: availableTestSessionsRowsDefinition,
              availableTestSessionsCurrentlyLoading: false
            });
          });
      }
    );
  };

  closeViewAvailableTestSessionsPopup = () => {
    this.setState({ showViewAvailableTestSessionsPopup: false, indexOfRowToViewOrDelete: null });
  };

  openDeleteTestToAdministerPopup = index => {
    this.setState({ showDeleteTestToAdministerPopup: true, indexOfRowToViewOrDelete: index });
  };

  closeDeleteTestToAdministerPopup = () => {
    this.setState({ showDeleteTestToAdministerPopup: false, indexOfRowToViewOrDelete: null });
  };

  handleDeleteTestToAdminister = () => {
    // creating a copy of rowsDefinition
    const copyOfRowsDefinition = { ...this.state.rowsDefinition };
    // deleting respective row
    copyOfRowsDefinition.data.splice(this.state.indexOfRowToViewOrDelete, 1);
    // updating delete button indexes
    for (let i = 0; i < copyOfRowsDefinition.data.length; i++) {
      copyOfRowsDefinition.data[i].column_4 = (
        <>
          <StyledTooltip
            id={`create-process-view-available-test-sessions-row-${i}`}
            place="top"
            variant={TYPE.light}
            effect={EFFECT.solid}
            openOnClick={false}
            tooltipElement={
              <div style={styles.allUnset}>
                <CustomButton
                  dataTip=""
                  dataFor={`create-process-view-available-test-sessions-row-${i}`}
                  label={
                    <>
                      <FontAwesomeIcon icon={faCalendarCheck} style={styles.tableIcon} />
                    </>
                  }
                  action={() => {
                    this.openDeleteTestToAdministerPopup(i);
                  }}
                  customStyle={styles.actionButton}
                  buttonTheme={THEME.PRIMARY}
                  ariaLabel={LOCALIZE.formatString(
                    LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                      .testsToAdministerTable.viewAvailableTestSessionsAccessibility,
                    copyOfRowsDefinition.data[i].formatted_test_skill
                  )}
                  disabled={
                    this.state.isValidForm &&
                    copyOfRowsDefinition.data[i].test_skill_sub_type_codename !==
                      TestSkillSleDescCodename.ORAL_EN &&
                    copyOfRowsDefinition.data[i].test_skill_sub_type_codename !==
                      TestSkillSleDescCodename.ORAL_FR
                      ? BUTTON_STATE.enabled
                      : BUTTON_STATE.disabled
                  }
                />
              </div>
            }
            tooltipContent={
              <div>
                <p>
                  {
                    LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                      .testsToAdministerTable.viewAvailableTestSessions
                  }
                </p>
              </div>
            }
          />
          <StyledTooltip
            id={`create-process-delete-test-to-administer-row-${i}`}
            place="top"
            variant={TYPE.light}
            effect={EFFECT.solid}
            openOnClick={false}
            tooltipElement={
              <div style={styles.allUnset}>
                <CustomButton
                  dataTip=""
                  dataFor={`create-process-delete-test-to-administer-row-${i}`}
                  label={
                    <>
                      <FontAwesomeIcon icon={faTrashAlt} style={styles.tableIcon} />
                    </>
                  }
                  action={() => {
                    this.openDeleteTestToAdministerPopup(i);
                  }}
                  customStyle={styles.actionButton}
                  buttonTheme={THEME.PRIMARY}
                  ariaLabel={LOCALIZE.formatString(
                    LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                      .testsToAdministerTable.deleteTestAccessibility,
                    copyOfRowsDefinition.data[i].formatted_test_skill
                  )}
                />
              </div>
            }
            tooltipContent={
              <div>
                <p>{LOCALIZE.commons.deleteButton}</p>
              </div>
            }
          />
        </>
      );
    }
    // updating rowsDefinition state
    this.setState({ rowsDefinition: copyOfRowsDefinition }, () => {
      // closing popup
      this.closeDeleteTestToAdministerPopup();
      // validating form
      this.validateForm();
    });
  };

  handleUpdateDisableStateOfViewAvailableTestSessionsButton = () => {
    // creating a copy of rowsDefinition
    const copyOfRowsDefinition = { ...this.state.rowsDefinition };
    // updating delete button indexes
    for (let i = 0; i < copyOfRowsDefinition.data.length; i++) {
      copyOfRowsDefinition.data[i].column_4 = (
        <>
          <StyledTooltip
            id={`create-process-view-available-test-sessions-row-${i}`}
            place="top"
            variant={TYPE.light}
            effect={EFFECT.solid}
            openOnClick={false}
            tooltipElement={
              <div style={styles.allUnset}>
                <CustomButton
                  dataTip=""
                  dataFor={`create-process-view-available-test-sessions-row-${i}`}
                  label={
                    <>
                      <FontAwesomeIcon icon={faCalendarCheck} style={styles.tableIcon} />
                    </>
                  }
                  action={() => {
                    this.openViewAvailableTestSessionsPopup(i);
                  }}
                  customStyle={styles.actionButton}
                  buttonTheme={THEME.PRIMARY}
                  ariaLabel={LOCALIZE.formatString(
                    LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                      .testsToAdministerTable.viewAvailableTestSessionsAccessibility,
                    copyOfRowsDefinition.data[i].formatted_test_skill
                  )}
                  disabled={
                    this.state.isValidForm &&
                    copyOfRowsDefinition.data[i].test_skill_sub_type_codename !==
                      TestSkillSleDescCodename.ORAL_EN &&
                    copyOfRowsDefinition.data[i].test_skill_sub_type_codename !==
                      TestSkillSleDescCodename.ORAL_FR
                      ? BUTTON_STATE.enabled
                      : BUTTON_STATE.disabled
                  }
                />
              </div>
            }
            tooltipContent={
              <div>
                <p>
                  {
                    LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                      .testsToAdministerTable.viewAvailableTestSessions
                  }
                </p>
              </div>
            }
          />
          <StyledTooltip
            id={`create-process-delete-test-to-administer-row-${i}`}
            place="top"
            variant={TYPE.light}
            effect={EFFECT.solid}
            openOnClick={false}
            tooltipElement={
              <div style={styles.allUnset}>
                <CustomButton
                  dataTip=""
                  dataFor={`create-process-delete-test-to-administer-row-${i}`}
                  label={
                    <>
                      <FontAwesomeIcon icon={faTrashAlt} style={styles.tableIcon} />
                    </>
                  }
                  action={() => {
                    this.openDeleteTestToAdministerPopup(i);
                  }}
                  customStyle={styles.actionButton}
                  buttonTheme={THEME.PRIMARY}
                  ariaLabel={LOCALIZE.formatString(
                    LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                      .testsToAdministerTable.deleteTestAccessibility,
                    copyOfRowsDefinition.data[i].formatted_test_skill
                  )}
                />
              </div>
            }
            tooltipContent={
              <div>
                <p>{LOCALIZE.commons.deleteButton}</p>
              </div>
            }
          />
        </>
      );
    }
    // updating rowsDefinition state
    this.setState({ rowsDefinition: copyOfRowsDefinition });
  };

  validateForm = () => {
    // initializing needed variables
    let isValidForm = false;

    // valid if reference number is defined
    const isValidReferenceNumber = this.state.referenceNumber !== "";
    // valid if department option has been selected
    const isValidDepartment = Object.keys(this.state.departmentSelectedOption).length > 0;
    // valid if duration has been provided
    const isValidDuration = this.state.duration !== "";
    // valid if contactEmailForCandidates has been provided
    const isValidContactEmailForCandidates =
      this.state.contactEmailForCandidates !== ""
        ? validateEmail(this.state.contactEmailForCandidates)
        : true;
    // valid if rowsDefinition contains data
    const isValidTestsToAdministerTable =
      typeof this.state.rowsDefinition.data !== "undefined" &&
      this.state.rowsDefinition.data.length > 0;

    // if all mandatory fields are valid
    if (
      isValidReferenceNumber &&
      isValidDepartment &&
      isValidDuration &&
      isValidContactEmailForCandidates &&
      this.state.contactEmailForCandidates !== "" &&
      isValidTestsToAdministerTable
    ) {
      isValidForm = true;
    }
    this.setState(
      {
        isValidForm: isValidForm,
        isValidContactEmailForCandidates: isValidContactEmailForCandidates
      },
      () => {
        // updating test to administer table (make sure that the disable state of the view available test sessions buttons are properly set)
        this.handleUpdateDisableStateOfViewAvailableTestSessionsButton();
      }
    );
  };

  handleCreateProcess = () => {
    // creating body
    const body = {
      reference_number: this.state.referenceNumber,
      department_id: this.state.departmentSelectedOption.value,
      duration: this.state.duration,
      allow_booking_external_tc: this.state.allowBookingExternalTc,
      default_billing_contact_id:
        Object.keys(this.state.defaultBillingContactSelectedOption).length > 0
          ? this.state.defaultBillingContactSelectedOption.value
          : null,
      contact_email_for_candidates: this.state.contactEmailForCandidates,
      tests_to_administer: this.state.rowsDefinition.data
    };
    this.props.createAssessmentProcess(body).then(response => {
      if (response.ok) {
        // open confirmation popup
        this.openConfirmationPopup();
        // trigger data rerender
        this.props.triggerDataRerenderFunction();
        // should never happen
      } else {
        throw new Error("An error occurred during the create assessment process process");
      }
    });
  };

  openConfirmationPopup = () => {
    this.setState(
      {
        showConfirmationPopup: true,
        isValidForm: false,
        referenceNumber: "",
        departmentSelectedOption: [],
        duration: "",
        contactEmailForCandidates: "",
        isValidContactEmailForCandidates: true,
        allowBookingExternalTc: false,
        defaultBillingContactSelectedOption: [],
        testSkillTypeOptions: [],
        testSkillTypeSelectedOption: [],
        testSkillSubTypeOptions: [],
        testSkillSubTypeSelectedOption: [],
        defaultReasonForTestingOptions: [],
        defaultReasonForTestingSelectedOption: [],
        defaultLevelRequired: "",
        triggerResetClosingDateFields: !this.state.triggerResetClosingDateFields
      },
      () => {
        // reinitializing the rowsDefinition state
        this.initializingTestsToAdministerObject();
      }
    );
  };

  closeConfirmationPopup = () => {
    this.setState({ showConfirmationPopup: false });
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerTable
            .columnOne,
        style: { ...{ width: "50%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerTable
            .columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerTable
            .columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerTable
            .columnFour,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    const availableTestSessionsColumnsDefinition = [
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerTable
            .viewAvailableTestSessionsPopup.table.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerTable
            .viewAvailableTestSessionsPopup.table.columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerTable
            .viewAvailableTestSessionsPopup.table.columnThree,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerTable
            .viewAvailableTestSessionsPopup.table.columnFour,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerTable
            .viewAvailableTestSessionsPopup.table.columnFive,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }
    return (
      <div style={styles.mainContainer}>
        <div style={styles.formContainer}>
          <Row className="align-items-center justify-content-center" style={styles.rowContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label htmlFor="create-process-reference-number-input" style={styles.inputTitles}>
                {LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.referenceNumberLabel}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="create-process-reference-number-input"
                className={"valid-field"}
                aria-required={true}
                type="text"
                style={{ ...styles.input, ...accommodationsStyle }}
                value={this.state.referenceNumber}
                onChange={this.getReferenceNumberContent}
              ></input>
            </Col>
          </Row>
          <Row className="align-items-center justify-content-center" style={styles.rowContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label id="create-process-department-label" style={styles.inputTitles}>
                {LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.departmentLabel}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <DropdownSelect
                idPrefix="create-process-department"
                isValid={true}
                ariaRequired={true}
                ariaLabelledBy="create-process-department-label"
                hasPlaceholder={true}
                options={this.state.departmentOptions}
                onChange={this.getSelectedDepartmentOption}
                defaultValue={this.state.departmentSelectedOption}
              />
            </Col>
          </Row>
          <Row className="align-items-center justify-content-center" style={styles.rowContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label htmlFor="create-process-duration-input" style={styles.inputTitles}>
                {LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.durationLabel}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="create-process-duration-input"
                className={"valid-field"}
                aria-required={true}
                type="text"
                style={{ ...styles.input, ...accommodationsStyle }}
                value={this.state.duration}
                onChange={this.getDurationContent}
              ></input>
            </Col>
          </Row>
          <Row className="align-items-center justify-content-center" style={styles.rowContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label id="create-process-allow-booking-external-tc-label" style={styles.inputTitles}>
                {
                  LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                    .allowBookingExternalTcLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              {!this.state.isLoadingSwitch && (
                <Switch
                  onChange={this.handleAllowBookingExternalTcSwitchUpdates}
                  checked={this.state.allowBookingExternalTc}
                  aria-labelledby="create-process-allow-booking-external-tc-label"
                  height={this.state.switchHeight}
                  width={this.state.switchWidth}
                />
              )}
            </Col>
          </Row>
          <Row className="align-items-center justify-content-center" style={styles.rowContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label id="create-process-default-billing-contact-label" style={styles.inputTitles}>
                {
                  LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                    .defaultBillingContactLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <DropdownSelect
                idPrefix="create-process-default-billing-contact"
                isValid={true}
                ariaRequired={true}
                ariaLabelledBy="create-process-default-billing-contact-label"
                hasPlaceholder={true}
                options={this.state.defaultBillingContactOptions}
                orderByLabels={false}
                onChange={this.getSelectedDefaultBillingContactOption}
                defaultValue={this.state.defaultBillingContactSelectedOption}
              />
            </Col>
          </Row>
          <Row
            className="align-items-center justify-content-center"
            style={
              this.state.isValidContactEmailForCandidates
                ? styles.rowContainer
                : styles.rowContainerWithError
            }
          >
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                htmlFor="create-process-contact-email-for-candidates-input"
                style={styles.inputTitles}
              >
                {
                  LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                    .contactEmailForCandidates
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <input
                id="create-process-contact-email-for-candidates-input"
                className={
                  this.state.isValidContactEmailForCandidates ? "valid-field" : "invalid-field"
                }
                aria-required={true}
                type="text"
                style={{ ...styles.input, ...accommodationsStyle }}
                value={this.state.contactEmailForCandidates}
                onChange={this.getContactEmailForCandidatesContent}
              ></input>
            </Col>
          </Row>
          {!this.state.isValidContactEmailForCandidates && (
            <Row className="align-items-center justify-content-end">
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
              >
                <label
                  htmlFor="create-process-contact-email-for-candidates-input"
                  style={styles.errorMessage}
                >
                  {
                    LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                      .invalidContactEmailForCandidates
                  }
                </label>
              </Col>
            </Row>
          )}
        </div>
        <div style={styles.formContainer}>
          <Row className="align-items-center justify-content-start" style={styles.rowContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label id="create-process-tests-to-administer-label" style={styles.inputTitles}>
                {LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerLabel}
              </label>
            </Col>
          </Row>
          <Row className="align-items-center" style={styles.rowContainer}>
            <div style={styles.tableContainer}>
              <GenericTable
                classnamePrefix="create-process-tests-to-administer"
                columnsDefinition={columnsDefinition}
                rowsDefinition={this.state.rowsDefinition}
                emptyTableMessage={this.populateEmptyTable()}
                currentlyLoading={false}
                hasLastRowHtml={true}
                lastRowHtml={
                  Object.keys(this.state.rowsDefinition).length > 0 &&
                  this.state.rowsDefinition.data.length > 0 &&
                  this.populateLastRowHtml()
                }
              />
            </div>
          </Row>
        </div>
        <div style={styles.createProcessButtonContainer}>
          <CustomButton
            buttonTheme={BUTTON_TYPE.primary}
            label={
              <>
                <span style={styles.createProcessButtonIcon}>
                  <FontAwesomeIcon icon={faShare} />
                </span>
                <span>
                  {LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.createProcessButton}
                </span>
              </>
            }
            action={this.handleCreateProcess}
            disabled={this.state.isValidForm ? BUTTON_STATE.enabled : BUTTON_STATE.disabled}
          />
        </div>
        <PopupBox
          show={this.state.showAddTestToAdministerPopup}
          handleClose={() => {}}
          onPopupOpen={this.populatePopupDropdowns}
          title={
            LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerTable
              .addTestToAdministerPopup.title
          }
          description={
            <div>
              <Row
                className="align-items-center justify-content-center"
                style={styles.rowContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label
                    id="create-process-add-test-to-administer-test-skill-type-label"
                    style={styles.inputTitles}
                  >
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                        .testsToAdministerTable.addTestToAdministerPopup.testSkillTypeLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <DropdownSelect
                    idPrefix="create-process-add-test-to-administer-test-skill-type"
                    isValid={true}
                    ariaRequired={true}
                    ariaLabelledBy="create-process-add-test-to-administer-test-skill-type-label"
                    hasPlaceholder={true}
                    options={this.state.testSkillTypeOptions}
                    onChange={this.getSelectedTestSkillTypeOption}
                    defaultValue={this.state.testSkillTypeSelectedOption}
                  />
                </Col>
              </Row>
              {(this.state.testSkillTypeSelectedOption.codename === TestSkillTypeCodename.SLE ||
                this.state.testSkillTypeSelectedOption.codename === TestSkillTypeCodename.OCC) && (
                <Row
                  className="align-items-center justify-content-center"
                  style={styles.rowContainer}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                  >
                    <label
                      id="create-process-add-test-to-administer-test-skill-sub-type-label"
                      style={styles.inputTitles}
                    >
                      {
                        LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                          .testsToAdministerTable.addTestToAdministerPopup.testSkillSubTypeLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <DropdownSelect
                      idPrefix="create-process-add-test-to-administer-test-skill-sub-type"
                      isValid={true}
                      ariaRequired={true}
                      ariaLabelledBy="create-process-add-test-to-administer-test-skill-sub-type-label"
                      hasPlaceholder={true}
                      options={this.state.testSkillSubTypeOptions}
                      onChange={this.getSelectedTestSkillSubTypeOption}
                      defaultValue={this.state.testSkillSubTypeSelectedOption}
                    />
                  </Col>
                </Row>
              )}
              <Row
                className="align-items-center justify-content-center"
                style={styles.rowContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label
                    id="create-process-add-test-to-administer-default-reason-for-testing-label"
                    style={styles.inputTitles}
                  >
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                        .testsToAdministerTable.addTestToAdministerPopup
                        .defaultReasonForTestingLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <DropdownSelect
                    idPrefix="create-process-add-test-to-administer-default-reason-for-testing"
                    isValid={true}
                    ariaRequired={true}
                    ariaLabelledBy="create-process-add-test-to-administer-default-reason-for-testing-label"
                    hasPlaceholder={true}
                    orderByLabels={false}
                    options={this.state.defaultReasonForTestingOptions}
                    onChange={this.getSelectedDefaultReasonForTestingOption}
                    defaultValue={this.state.defaultReasonForTestingSelectedOption}
                    isDisabled={Object.keys(this.state.testSkillSubTypeSelectedOption).length <= 0}
                  />
                </Col>
              </Row>
              <Row
                className="align-items-center justify-content-center"
                style={styles.rowContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label
                    htmlFor="create-process-add-test-to-administer-default-level-required-input"
                    style={styles.inputTitles}
                  >
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                        .testsToAdministerTable.addTestToAdministerPopup.defaultLevelRequiredLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <input
                    id="create-process-add-test-to-administer-default-level-required-input"
                    className={"valid-field"}
                    aria-required={true}
                    type="text"
                    style={{ ...styles.input, ...accommodationsStyle }}
                    value={this.state.defaultLevelRequired}
                    onChange={this.getDefaultLevelRequiredContent}
                    disabled={Object.keys(this.state.testSkillSubTypeSelectedOption).length <= 0}
                  ></input>
                </Col>
              </Row>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeAddTestToAdministerPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerTable
              .addTestToAdministerPopup.addTestButton
          }
          rightButtonIcon={faPlusCircle}
          rightButtonState={
            this.state.isValidAddTestToAdministerForm ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
          rightButtonAction={this.handleAddTestToAdminister}
        />
        {this.state.indexOfRowToViewOrDelete !== null &&
          typeof this.state.rowsDefinition.data[this.state.indexOfRowToViewOrDelete] !==
            "undefined" && (
            <>
              <PopupBox
                show={this.state.showDeleteTestToAdministerPopup}
                handleClose={() => {}}
                size="lg"
                title={
                  LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerTable
                    .deleteTestToAdministerPopup.title
                }
                description={
                  <div>
                    <SystemMessage
                      messageType={MESSAGE_TYPE.error}
                      title={LOCALIZE.commons.warning}
                      message={
                        <p className="notranslate">
                          {LOCALIZE.formatString(
                            LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                              .testsToAdministerTable.deleteTestToAdministerPopup
                              .systemMessageDescription,
                            <span style={styles.boldText}>
                              {
                                this.state.rowsDefinition.data[this.state.indexOfRowToViewOrDelete]
                                  .formatted_test_skill
                              }
                            </span>
                          )}
                        </p>
                      }
                    />
                    <p>
                      {
                        LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                          .testsToAdministerTable.deleteTestToAdministerPopup.description
                      }
                    </p>
                  </div>
                }
                leftButtonType={BUTTON_TYPE.secondary}
                leftButtonTitle={LOCALIZE.commons.cancel}
                leftButtonIcon={faTimes}
                leftButtonAction={this.closeDeleteTestToAdministerPopup}
                rightButtonType={BUTTON_TYPE.danger}
                rightButtonTitle={LOCALIZE.commons.deleteButton}
                rightButtonIcon={faTrashAlt}
                rightButtonAction={this.handleDeleteTestToAdminister}
              />
              <PopupBox
                show={this.state.showViewAvailableTestSessionsPopup}
                handleClose={() => {}}
                title={
                  LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.testsToAdministerTable
                    .viewAvailableTestSessionsPopup.title
                }
                description={
                  <div>
                    <p>
                      {LOCALIZE.formatString(
                        LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                          .testsToAdministerTable.viewAvailableTestSessionsPopup.description,
                        <span style={styles.boldText}>
                          {
                            this.state.rowsDefinition.data[this.state.indexOfRowToViewOrDelete]
                              .column_1
                          }
                        </span>
                      )}
                    </p>
                    <div style={styles.availableTestSessionsTableStyle}>
                      <GenericTable
                        classnamePrefix="create-process-tests-to-administer"
                        columnsDefinition={availableTestSessionsColumnsDefinition}
                        rowsDefinition={this.state.availableTestSessionsRowsDefinition}
                        emptyTableMessage={
                          LOCALIZE.inPersonTesting.assessmentProcesses.createProcess
                            .testsToAdministerTable.viewAvailableTestSessionsPopup.table.noData
                        }
                        currentlyLoading={this.state.availableTestSessionsCurrentlyLoading}
                      />
                    </div>
                  </div>
                }
                rightButtonType={BUTTON_TYPE.primary}
                rightButtonTitle={LOCALIZE.commons.close}
                rightButtonIcon={faTimes}
                rightButtonAction={this.closeViewAvailableTestSessionsPopup}
              />
            </>
          )}
        <PopupBox
          show={this.state.showConfirmationPopup}
          handleClose={() => {}}
          size="lg"
          title={LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.confirmationPopup.title}
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.success}
                title={LOCALIZE.commons.success}
                message={
                  <p className="notranslate">
                    {
                      LOCALIZE.inPersonTesting.assessmentProcesses.createProcess.confirmationPopup
                        .systemMessageDescription
                    }
                  </p>
                }
              />
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.closeConfirmationPopup}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    triggerDataRerender: state.assessmentProcess.triggerDataRerender
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getOrganization,
      getBillingContactsList,
      getTestSkillTypes,
      getTestSkillSubTypes,
      getReasonsForTesting,
      createAssessmentProcess,
      triggerDataRerenderFunction,
      getAvailableTestSessionsForSpecificTestToAdminister
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CreateProcess);

export { referenceNumberRegexExpression };
