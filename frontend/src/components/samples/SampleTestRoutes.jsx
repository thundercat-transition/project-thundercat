import React, { Component } from "react";
import { Route, Routes, Navigate } from "react-router-dom";
import withRouter from "../withRouter";
import SampleTestsDashboard from "./SampleTestsDashboard";
import TestPage from "../testFactory/TestPage";

const SampleTestsRoutes = () => {
  return (
    <Routes>
      {/* If you're simply on /oec-cat/sample-tests */}
      <Route exact path={``} element={<SampleTestsDashboard />} />

      {/* Entering a Sample Test */}
      <Route exact path={`/test-factory`} element={<TestPage sampleTest={true} />} />
    </Routes>
  );
};

export default withRouter(SampleTestsRoutes);
