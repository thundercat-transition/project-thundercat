import React, { useReducer, useEffect } from "react";
import withRouter from "../withRouter";
import ContentContainer from "../commons/ContentContainer";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setCurrentTest } from "../../modules/TestStatusRedux";
import { TEST_TABLE_STYLES as AssignedTestTableStyles } from "../candidate/AssignedTestTable";
import { Helmet } from "react-helmet";
import { resetTestStatusState } from "../../modules/SampleTestStatusRedux";
import { resetNotepadState } from "../../modules/NotepadRedux";
import { resetTestFactoryState } from "../../modules/TestSectionRedux";
import { resetQuestionListState } from "../../modules/QuestionListRedux";
import { resetTopTabsState } from "../../modules/NavTabsRedux";
import { getPublicTests, updateAssignedTestId } from "../../modules/AssignedTestsRedux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlayCircle } from "@fortawesome/free-solid-svg-icons";
import { history } from "../../store-index";

import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";

const styles = {
  tableContainer: {
    paddingTop: 24,
    width: "75%",
    margin: "auto"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  appPadding: {
    padding: "15px"
  }
};

const sortAlphaNumericSampleTestNames = (a, b) => {
  return a.sampleTestName.toString().localeCompare(b.sampleTestName.toString(), { numeric: true });
};

const SampleTestDashboard = props => {
  // Props
  // testNameId: PropTypes.string
  // setCurrentTest: PropTypes.func

  // Reducer for set state
  const [state, setState] = useReducer((state, newState) => ({ ...state, ...newState }), {
    tests: [],
    loaded: false
  });

  const resetRedux = () => {
    props.resetTestStatusState();
    props.resetNotepadState();
    props.resetTestFactoryState();
    props.resetQuestionListState();
    props.resetTopTabsState();
  };

  // Equivalent of componentDidMount
  useEffect(() => {
    resetRedux();

    props.getPublicTests().then(response => {
      if (!response.ok) {
        console.log("error occured");
      } else {
        setState({ tests: response.body, loaded: true });
      }
    });
  }, []);

  const handleClick = testId => {
    // set the test definition
    props.updateAssignedTestId(null, testId, null, null);
    history.push(`${props.location.pathname}test-factory`);
  };

  const makeRows = () => {
    const localSampleTestTableData = [];

    state.tests.forEach(sampleTest => {
      const sampleTestName = sampleTest[`${props.currentLanguage}_name`];
      localSampleTestTableData.push({
        sampleTestName: sampleTestName,
        sampleTestId: sampleTest.id
      });
    });

    localSampleTestTableData.sort(sortAlphaNumericSampleTestNames);

    const data = localSampleTestTableData.map(sampleTest => {
      return {
        column_1: sampleTest.sampleTestName,
        column_2: (
          <CustomButton
            label={
              <>
                <FontAwesomeIcon icon={faPlayCircle} />
                <span style={styles.buttonLabel}>
                  {LOCALIZE.sampleTestDashboard.table.viewButton}
                </span>
              </>
            }
            action={() => handleClick(sampleTest.sampleTestId)}
            customStyle={AssignedTestTableStyles.viewButton}
            buttonTheme={THEME.PRIMARY}
            ariaLabel={LOCALIZE.formatString(
              LOCALIZE.sampleTestDashboard.table.viewButtonAccessibilityLabel,
              sampleTest.sampleTestName
            )}
          />
        )
      };
    });
    const rowsDefinition = {
      column_1_style: {},
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };
    return rowsDefinition;
  };

  const rowsDefinition = makeRows();
  const columnsDefinition = [
    {
      label: LOCALIZE.sampleTestDashboard.table.columnOne,
      style: { width: "75%" }
    },
    {
      label: LOCALIZE.sampleTestDashboard.table.columnTwo,
      style: { width: "25%", ...COMMON_STYLE.CENTERED_TEXT }
    }
  ];
  return (
    <div className="app" style={styles.appPadding}>
      <Helmet>
        <html lang={props.currentLanguage} />
        <title className="notranslate">{LOCALIZE.titles.sampleTests}</title>
      </Helmet>
      <ContentContainer>
        <div id="main-content" role="main">
          <h1 className="green-divider">{LOCALIZE.sampleTestDashboard.title}</h1>
          <p>
            {LOCALIZE.sampleTestDashboard.paragraph1p1}
            <b>{LOCALIZE.sampleTestDashboard.paragraph1bold}</b>
            {LOCALIZE.sampleTestDashboard.paragraph1p2}
          </p>
          <p>{LOCALIZE.sampleTestDashboard.paragraph2}</p>
          <p>
            <b>{LOCALIZE.sampleTestDashboard.paragraph3}</b>
          </p>
        </div>
        <div style={styles.tableContainer}>
          <GenericTable
            classnamePrefix="sample-tests-dashboard"
            columnsDefinition={columnsDefinition}
            rowsDefinition={rowsDefinition}
            emptyTableMessage={LOCALIZE.SearchBarWithDisplayOptions.noResultsFound}
            currentlyLoading={props.currentlyLoading}
          />
        </div>
      </ContentContainer>
    </div>
  );
};

export { SampleTestDashboard as UnconnectedSampleTestDashboard };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setCurrentTest,
      resetTestStatusState,
      resetNotepadState,
      resetTestFactoryState,
      resetQuestionListState,
      getPublicTests,
      updateAssignedTestId,
      resetTopTabsState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SampleTestDashboard));
