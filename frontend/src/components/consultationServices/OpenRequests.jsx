import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import Pagination from "../commons/Pagination";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import SearchBarWithDisplayOptions from "../commons/SearchBarWithDisplayOptions";
import {
  updateCurrentConsultationServicesOpenRequestsPageState,
  updateConsultationServicesOpenRequestsPageSizeState,
  updateSearchConsultationServicesOpenRequestsStates
} from "../../modules/ConsultationServicesRedux";

const styles = {};

class OpenRequests extends Component {
  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    resultsFound: 0,
    openRequests: [],
    clearSearchTriggered: false
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentConsultationServicesOpenRequestsPageState(1);
    // populating associated test administrators
    this.populateOpenRequestsBasedOnActiveSearch();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateOpenRequestsBasedOnActiveSearch();
    }
    // if consultationServicesOpenRequestsPaginationPage gets updated
    if (
      prevProps.consultationServicesOpenRequestsPaginationPage !==
      this.props.consultationServicesOpenRequestsPaginationPage
    ) {
      this.populateOpenRequestsBasedOnActiveSearch();
    }
    // if consultationServicesOpenRequestsPaginationPageSize get updated
    if (
      prevProps.consultationServicesOpenRequestsPaginationPageSize !==
      this.props.consultationServicesOpenRequestsPaginationPageSize
    ) {
      this.populateOpenRequestsBasedOnActiveSearch();
    }
    // if search consultationServicesOpenRequestsKeyword gets updated
    if (
      prevProps.consultationServicesOpenRequestsKeyword !==
      this.props.consultationServicesOpenRequestsKeyword
    ) {
      // if consultationServicesOpenRequestsKeyword redux state is empty
      if (this.props.consultationServicesOpenRequestsKeyword !== "") {
        this.populateFoundOpenRequests();
      } else if (
        this.props.consultationServicesOpenRequestsKeyword === "" &&
        this.props.consultationServicesOpenRequestsActiveSearch
      ) {
        this.populateFoundOpenRequests();
      }
    }
    // if consultationServicesOpenRequestsActiveSearch gets updated
    if (
      prevProps.consultationServicesOpenRequestsActiveSearch !==
      this.props.consultationServicesOpenRequestsActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.consultationServicesOpenRequestsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateOpenRequests();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundOpenRequests();
      }
    }
  };

  populateOpenRequestsBasedOnActiveSearch = () => {
    // current search
    if (this.props.consultationServicesOpenRequestsActiveSearch) {
      this.populateFoundOpenRequests();
      // no search
    } else {
      this.populateOpenRequests();
    }
  };

  // TODO
  // populating open accommodation requests
  populateOpenRequests = () => {
    // this.setState({ currentlyLoading: true }, () => {
    //   const openRequestsArray = [];
    //   this.props
    //     .getTestCenterAccommodationRequests(
    //       this.props.consultationServicesOpenRequestsPaginationPage,
    //       this.props.consultationServicesOpenRequestsPaginationPageSize
    //     )
    //     .then(response => {
    //       this.populateAccommodationRequestsOpenRequestsObject(openRequestsArray, response);
    //     })
    //     .then(() => {
    //       if (this.state.clearSearchTriggered) {
    //         // go back to the first page to avoid display bugs
    //         this.props.updateCurrentConsultationServicesOpenRequestsPageState(1);
    //         this.setState({ clearSearchTriggered: false });
    //       }
    //     })
    //     .then(() => {
    //       this.setState({ currentlyLoading: false });
    //     });
    // });
  };

  // TODO
  // populating found open accommodation requests
  populateFoundOpenRequests = () => {
    // // making sure that the table is not currently loading
    // if (!this.state.currentlyLoading) {
    //   this.setState(
    //     { currentlyLoading: true },
    //     () => {
    //       const openRequestsArray = [];
    //       setTimeout(() => {
    //         this.props
    //           .getFoundTestCenterAccommodationRequests(
    //             this.props.consultationServicesOpenRequestsKeyword,
    //             this.props.currentLanguage,
    //             this.props.consultationServicesOpenRequestsPaginationPage,
    //             this.props.consultationServicesOpenRequestsPaginationPageSize,
    //           )
    //           .then(response => {
    //             // there are no results found
    //             if (response[0] === "no results found") {
    //               this.setState({
    //                 testSesions: [],
    //                 rowsDefinition: {},
    //                 numberOfPages: 1,
    //                 nextPageNumber: 0,
    //                 previousPageNumber: 0,
    //                 resultsFound: 0
    //               });
    //               // there is at least one result found
    //             } else {
    //               this.populateAccommodationRequestsOpenRequestsObject(openRequestsArray, response);
    //               this.setState({ resultsFound: response.count });
    //             }
    //           })
    //           .then(() => {
    //             this.setState(
    //               {
    //                 currentlyLoading: false
    //               },
    //               () => {
    //                 // if there is at least one result found
    //                 if (openRequestsArray.length > 0) {
    //                   // make sure that this element exsits to avoid any error
    //                   if (document.getElementById("accommodation-requests-open-requests-results-found")) {
    //                     document.getElementById("accommodation-requests-open-requests-results-found").focus();
    //                   }
    //                   // no results found
    //                 } else {
    //                   // focusing on no results found row
    //                   document.getElementById("accommodation-requests-open-requests-no-data").focus();
    //                 }
    //               }
    //             );
    //           });
    //       });
    //     },
    //     250
    //   );
    // }
  };

  // TODO
  populateAccommodationRequestsOpenRequestsObject = (openRequestsArray, response) => {
    // // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    // let rowsDefinition = {};
    // const data = [];
    // // making sure that there are some results
    // if (typeof response.results !== "undefined") {
    //   // looping in response given
    //   for (let i = 0; i < response.results.length; i++) {
    //     let formattedTestSkill = `${
    //       currentResult[`test_skill_type_${this.props.currentLanguage}_name`]
    //     }`;
    //     if (currentResult.test_skill_sub_type_id !== null) {
    //       formattedTestSkill = formattedTestSkill.concat(
    //         ` - ${currentResult[`test_skill_sub_type_${this.props.currentLanguage}_name`]}`
    //       );
    //     }
    //     // pushing needed data in openRequestsArray
    //     openRequestsArray.push({
    //       id: currentResult.id,
    //     });
    //     // pushing needed data in data array (needed for GenericTable component)
    //     data.push({
    //       column_1: formattedTestSkill,
    //       column_2: `${currentResult.candidate_first_name} ${currentResult.candidate_last_name}`,
    //       column_3: this.props.currentLanguage === LANGUAGES.english ? `${currentResult.dept_edesc} (${currentResult.dept_eabrv})` : `${currentResult.dept_fdesc} (${currentResult.dept_fabrv})`,
    //       column_4: (
    //         <>
    //           <StyledTooltip
    //             id={`accommodation-requests-open-requests-view-row-${i}`}
    //             place="top"
    //             variant={TYPE.light}
    //             effect={EFFECT.solid}
    //             openOnClick={false}
    //             tooltipElement={
    //               <div style={styles.allUnset}>
    //                 <CustomButton
    //                   dataTip=""
    //                   dataFor={`accommodation-requests-open-requests-view-row-${i}`}
    //                   label={
    //                     <>
    //                       <FontAwesomeIcon icon={faMagic} style={styles.tableIcon} />
    //                     </>
    //                   }
    //                   action={() => {
    //                     () => {}
    //                   }}
    //                   customStyle={styles.actionButton}
    //                   buttonTheme={THEME.PRIMARY}
    //                   ariaLabel={LOCALIZE.formatString(
    //                     LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
    //                       .accommodationRequests.topTabs.pendingRequests.table.viewButtonAccessibility,
    //                     currentResult.candidate_first_name,
    //                     currentResult.candidate_last_name
    //                   )}
    //                 />
    //               </div>
    //             }
    //             tooltipContent={
    //               <div>
    //                 <p>
    //                   {
    //                     LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
    //                       .accommodationRequests.topTabs.pendingRequests.table.viewButtonTooltip
    //                   }
    //                 </p>
    //               </div>
    //             }
    //           />
    //         </>
    //       )
    //     });
    //   }
    // }
    // // updating rowsDefinition object with provided data and needed style
    // rowsDefinition = {
    //   column_1_style: COMMON_STYLE.LEFT_TEXT,
    //   column_2_style: COMMON_STYLE.LEFT_TEXT,
    //   column_3_style: COMMON_STYLE.LEFT_TEXT,
    //   column_4_style: COMMON_STYLE.CENTERED_TEXT,
    //   data: data
    // };
    // // saving results in state
    // this.setState({
    //   openRequests: openRequestsArray,
    //   rowsDefinition: rowsDefinition,
    //   numberOfPages: Math.ceil(
    //     response.count / this.props.consultationServicesOpenRequestsPaginationPageSize
    //   ),
    //   nextPageNumber: response.next_page_number,
    //   previousPageNumber: response.previous_page_number
    // });
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.consultationServices.sideNav.consultationServicesRequests.topTabs.openRequests
            .table.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.consultationServices.sideNav.consultationServicesRequests.topTabs.openRequests
            .table.columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.consultationServices.sideNav.consultationServicesRequests.topTabs.openRequests
            .table.columnThree,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.consultationServices.sideNav.consultationServicesRequests.topTabs.openRequests
            .table.columnFour,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"accommodation-requests-open-requests"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchConsultationServicesOpenRequestsStates}
            updatePageState={this.props.updateCurrentConsultationServicesOpenRequestsPageState}
            paginationPageSize={Number(
              this.props.consultationServicesOpenRequestsPaginationPageSize
            )}
            updatePaginationPageSize={
              this.props.updateConsultationServicesOpenRequestsPageSizeState
            }
            data={this.state.pendingRequests}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.consultationServicesOpenRequestsKeyword}
          />
          <div>
            <GenericTable
              classnamePrefix="accommodation-requests-open-requests"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.consultationServices.sideNav.consultationServicesRequests.topTabs
                  .openRequests.table.noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <Pagination
            paginationContainerId={"accommodation-requests-open-requests-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.consultationServicesOpenRequestsPaginationPage}
            updatePaginationPageState={
              this.props.updateCurrentConsultationServicesOpenRequestsPageState
            }
            firstTableRowId={"accommodation-requests-open-requests-table-row-0"}
          />
        </div>
      </div>
    );
  }
}

export { OpenRequests as unconnectedOpenRequests };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    consultationServicesOpenRequestsPaginationPage:
      state.consultationServices.consultationServicesOpenRequestsPaginationPage,
    consultationServicesOpenRequestsPaginationPageSize:
      state.consultationServices.consultationServicesOpenRequestsPaginationPageSize,
    consultationServicesOpenRequestsKeyword:
      state.consultationServices.consultationServicesOpenRequestsKeyword,
    consultationServicesOpenRequestsActiveSearch:
      state.consultationServices.consultationServicesOpenRequestsActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateCurrentConsultationServicesOpenRequestsPageState,
      updateConsultationServicesOpenRequestsPageSizeState,
      updateSearchConsultationServicesOpenRequestsStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(OpenRequests);
