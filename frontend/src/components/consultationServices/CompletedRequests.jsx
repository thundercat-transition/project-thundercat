import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import Pagination from "../commons/Pagination";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import SearchBarWithDisplayOptions from "../commons/SearchBarWithDisplayOptions";
import {
  updateCurrentConsultationServicesCompletedRequestsPageState,
  updateConsultationServicesCompletedRequestsPageSizeState,
  updateSearchConsultationServicesCompletedRequestsStates
} from "../../modules/ConsultationServicesRedux";

const styles = {};

class CompletedRequests extends Component {
  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    resultsFound: 0,
    completedRequests: [],
    clearSearchTriggered: false
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentConsultationServicesCompletedRequestsPageState(1);
    // populating associated test administrators
    this.populateCompletedRequestsBasedOnActiveSearch();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateCompletedRequestsBasedOnActiveSearch();
    }
    // if consultationServicesCompletedRequestsPaginationPage gets updated
    if (
      prevProps.consultationServicesCompletedRequestsPaginationPage !==
      this.props.consultationServicesCompletedRequestsPaginationPage
    ) {
      this.populateCompletedRequestsBasedOnActiveSearch();
    }
    // if consultationServicesCompletedRequestsPaginationPageSize get updated
    if (
      prevProps.consultationServicesCompletedRequestsPaginationPageSize !==
      this.props.consultationServicesCompletedRequestsPaginationPageSize
    ) {
      this.populateCompletedRequestsBasedOnActiveSearch();
    }
    // if search consultationServicesCompletedRequestsKeyword gets updated
    if (
      prevProps.consultationServicesCompletedRequestsKeyword !==
      this.props.consultationServicesCompletedRequestsKeyword
    ) {
      // if consultationServicesCompletedRequestsKeyword redux state is empty
      if (this.props.consultationServicesCompletedRequestsKeyword !== "") {
        this.populateFoundCompletedRequests();
      } else if (
        this.props.consultationServicesCompletedRequestsKeyword === "" &&
        this.props.consultationServicesCompletedRequestsActiveSearch
      ) {
        this.populateFoundCompletedRequests();
      }
    }
    // if consultationServicesCompletedRequestsActiveSearch gets updated
    if (
      prevProps.consultationServicesCompletedRequestsActiveSearch !==
      this.props.consultationServicesCompletedRequestsActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.consultationServicesCompletedRequestsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateCompletedRequests();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundCompletedRequests();
      }
    }
  };

  populateCompletedRequestsBasedOnActiveSearch = () => {
    // current search
    if (this.props.consultationServicesCompletedRequestsActiveSearch) {
      this.populateFoundCompletedRequests();
      // no search
    } else {
      this.populateCompletedRequests();
    }
  };

  // TODO
  // populating completed accommodation requests
  populateCompletedRequests = () => {
    // this.setState({ currentlyLoading: true }, () => {
    //   const completedRequestsArray = [];
    //   this.props
    //     .getTestCenterAccommodationRequests(
    //       this.props.consultationServicesCompletedRequestsPaginationPage,
    //       this.props.consultationServicesCompletedRequestsPaginationPageSize
    //     )
    //     .then(response => {
    //       this.populateAccommodationRequestsCompletedRequestsObject(completedRequestsArray, response);
    //     })
    //     .then(() => {
    //       if (this.state.clearSearchTriggered) {
    //         // go back to the first page to avoid display bugs
    //         this.props.updateCurrentConsultationServicesCompletedRequestsPageState(1);
    //         this.setState({ clearSearchTriggered: false });
    //       }
    //     })
    //     .then(() => {
    //       this.setState({ currentlyLoading: false });
    //     });
    // });
  };

  // TODO
  // populating found completed accommodation requests
  populateFoundCompletedRequests = () => {
    // // making sure that the table is not currently loading
    // if (!this.state.currentlyLoading) {
    //   this.setState(
    //     { currentlyLoading: true },
    //     () => {
    //       const completedRequestsArray = [];
    //       setTimeout(() => {
    //         this.props
    //           .getFoundTestCenterAccommodationRequests(
    //             this.props.consultationServicesCompletedRequestsKeyword,
    //             this.props.currentLanguage,
    //             this.props.consultationServicesCompletedRequestsPaginationPage,
    //             this.props.consultationServicesCompletedRequestsPaginationPageSize,
    //           )
    //           .then(response => {
    //             // there are no results found
    //             if (response[0] === "no results found") {
    //               this.setState({
    //                 testSesions: [],
    //                 rowsDefinition: {},
    //                 numberOfPages: 1,
    //                 nextPageNumber: 0,
    //                 previousPageNumber: 0,
    //                 resultsFound: 0
    //               });
    //               // there is at least one result found
    //             } else {
    //               this.populateAccommodationRequestsCompletedRequestsObject(completedRequestsArray, response);
    //               this.setState({ resultsFound: response.count });
    //             }
    //           })
    //           .then(() => {
    //             this.setState(
    //               {
    //                 currentlyLoading: false
    //               },
    //               () => {
    //                 // if there is at least one result found
    //                 if (completedRequestsArray.length > 0) {
    //                   // make sure that this element exsits to avoid any error
    //                   if (document.getElementById("accommodation-requests-completed-requests-results-found")) {
    //                     document.getElementById("accommodation-requests-completed-requests-results-found").focus();
    //                   }
    //                   // no results found
    //                 } else {
    //                   // focusing on no results found row
    //                   document.getElementById("accommodation-requests-completed-requests-no-data").focus();
    //                 }
    //               }
    //             );
    //           });
    //       });
    //     },
    //     250
    //   );
    // }
  };

  // TODO
  populateAccommodationRequestsCompletedRequestsObject = (completedRequestsArray, response) => {
    // // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    // let rowsDefinition = {};
    // const data = [];
    // // making sure that there are some results
    // if (typeof response.results !== "undefined") {
    //   // looping in response given
    //   for (let i = 0; i < response.results.length; i++) {
    //     let formattedTestSkill = `${
    //       currentResult[`test_skill_type_${this.props.currentLanguage}_name`]
    //     }`;
    //     if (currentResult.test_skill_sub_type_id !== null) {
    //       formattedTestSkill = formattedTestSkill.concat(
    //         ` - ${currentResult[`test_skill_sub_type_${this.props.currentLanguage}_name`]}`
    //       );
    //     }
    //     // pushing needed data in completedRequestsArray
    //     completedRequestsArray.push({
    //       id: currentResult.id,
    //     });
    //     // pushing needed data in data array (needed for GenericTable component)
    //     data.push({
    //       column_1: formattedTestSkill,
    //       column_2: `${currentResult.candidate_first_name} ${currentResult.candidate_last_name}`,
    //       column_3: this.props.currentLanguage === LANGUAGES.english ? `${currentResult.dept_edesc} (${currentResult.dept_eabrv})` : `${currentResult.dept_fdesc} (${currentResult.dept_fabrv})`,
    //       column_4: (
    //         <>
    //           <StyledTooltip
    //             id={`accommodation-requests-completed-requests-view-row-${i}`}
    //             place="top"
    //             variant={TYPE.light}
    //             effect={EFFECT.solid}
    //             openOnClick={false}
    //             tooltipElement={
    //               <div style={styles.allUnset}>
    //                 <CustomButton
    //                   dataTip=""
    //                   dataFor={`accommodation-requests-completed-requests-view-row-${i}`}
    //                   label={
    //                     <>
    //                       <FontAwesomeIcon icon={faMagic} style={styles.tableIcon} />
    //                     </>
    //                   }
    //                   action={() => {
    //                     () => {}
    //                   }}
    //                   customStyle={styles.actionButton}
    //                   buttonTheme={THEME.PRIMARY}
    //                   ariaLabel={LOCALIZE.formatString(
    //                     LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
    //                       .accommodationRequests.topTabs.pendingRequests.table.viewButtonAccessibility,
    //                     currentResult.candidate_first_name,
    //                     currentResult.candidate_last_name
    //                   )}
    //                 />
    //               </div>
    //             }
    //             tooltipContent={
    //               <div>
    //                 <p>
    //                   {
    //                     LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
    //                       .accommodationRequests.topTabs.pendingRequests.table.viewButtonTooltip
    //                   }
    //                 </p>
    //               </div>
    //             }
    //           />
    //         </>
    //       )
    //     });
    //   }
    // }
    // // updating rowsDefinition object with provided data and needed style
    // rowsDefinition = {
    //   column_1_style: COMMON_STYLE.LEFT_TEXT,
    //   column_2_style: COMMON_STYLE.LEFT_TEXT,
    //   column_3_style: COMMON_STYLE.LEFT_TEXT,
    //   column_4_style: COMMON_STYLE.CENTERED_TEXT,
    //   data: data
    // };
    // // saving results in state
    // this.setState({
    //   completedRequests: completedRequestsArray,
    //   rowsDefinition: rowsDefinition,
    //   numberOfPages: Math.ceil(
    //     response.count / this.props.consultationServicesCompletedRequestsPaginationPageSize
    //   ),
    //   nextPageNumber: response.next_page_number,
    //   previousPageNumber: response.previous_page_number
    // });
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.consultationServices.sideNav.consultationServicesRequests.topTabs
            .completedRequets.table.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.consultationServices.sideNav.consultationServicesRequests.topTabs
            .completedRequets.table.columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.consultationServices.sideNav.consultationServicesRequests.topTabs
            .completedRequets.table.columnThree,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.consultationServices.sideNav.consultationServicesRequests.topTabs
            .completedRequets.table.columnFour,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"accommodation-requests-completed-requests"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchConsultationServicesCompletedRequestsStates}
            updatePageState={this.props.updateCurrentConsultationServicesCompletedRequestsPageState}
            paginationPageSize={Number(
              this.props.consultationServicesCompletedRequestsPaginationPageSize
            )}
            updatePaginationPageSize={
              this.props.updateConsultationServicesCompletedRequestsPageSizeState
            }
            data={this.state.pendingRequests}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.consultationServicesCompletedRequestsKeyword}
          />
          <div>
            <GenericTable
              classnamePrefix="accommodation-requests-completed-requests"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.consultationServices.sideNav.consultationServicesRequests.topTabs
                  .completedRequets.table.noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <Pagination
            paginationContainerId={
              "accommodation-requests-completed-requests-pagination-pages-link"
            }
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.consultationServicesCompletedRequestsPaginationPage}
            updatePaginationPageState={
              this.props.updateCurrentConsultationServicesCompletedRequestsPageState
            }
            firstTableRowId={"accommodation-requests-completed-requests-table-row-0"}
          />
        </div>
      </div>
    );
  }
}

export { CompletedRequests as unconnectedCompletedRequests };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    consultationServicesCompletedRequestsPaginationPage:
      state.consultationServices.consultationServicesCompletedRequestsPaginationPage,
    consultationServicesCompletedRequestsPaginationPageSize:
      state.consultationServices.consultationServicesCompletedRequestsPaginationPageSize,
    consultationServicesCompletedRequestsKeyword:
      state.consultationServices.consultationServicesCompletedRequestsKeyword,
    consultationServicesCompletedRequestsActiveSearch:
      state.consultationServices.consultationServicesCompletedRequestsActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateCurrentConsultationServicesCompletedRequestsPageState,
      updateConsultationServicesCompletedRequestsPageSizeState,
      updateSearchConsultationServicesCompletedRequestsStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CompletedRequests);
