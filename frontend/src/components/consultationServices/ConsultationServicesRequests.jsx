import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import OpenRequests from "./OpenRequests";
import CompletedRequests from "./CompletedRequests";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  },
  viewEditDetailsBtn: {
    minWidth: 100
  },
  buttonIcon: {
    marginRight: 6
  },
  viewPermissionRequestButton: {
    marginLeft: 6
  }
};

class ConsultationServicesRequests extends Component {
  state = {};

  componentDidMount = () => {};

  componentDidUpdate = () => {};

  render() {
    const TABS = [
      {
        key: "consultation-services-requests-open-requests",
        tabName:
          LOCALIZE.consultationServices.sideNav.consultationServicesRequests.topTabs.openRequests
            .title,
        body: <OpenRequests />
      },
      {
        key: "accommodation-requests-completed-requests",
        tabName:
          LOCALIZE.consultationServices.sideNav.consultationServicesRequests.topTabs
            .completedRequets.title,
        body: <CompletedRequests />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.consultationServices.sideNav.consultationServicesRequests.title}</h2>
          <p>{LOCALIZE.consultationServices.sideNav.consultationServicesRequests.description}</p>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey="consultation-services-requests-open-requests"
                id="consultation-services-requests-open-requests-tabs"
                style={styles.tabNavigation}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ConsultationServicesRequests);
