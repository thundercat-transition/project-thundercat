import React, { Component } from "react";
import PropTypes from "prop-types";
import withRouter from "../withRouter";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { setLanguage } from "../../modules/LocalizeRedux";
import TestSectionComponentFactory from "./TestSectionComponentFactory";
import {
  updateAssignedTestId,
  resetAssignedTestState,
  getAssignedTests
} from "../../modules/AssignedTestsRedux";

import { resetQuestionListState } from "../../modules/QuestionListRedux";
import { BANNER_STYLE, TEST_STYLE } from "../eMIB/constants";
import { NextSectionButtonType } from "./Constants";
import {
  getTestSection,
  eraseOppositeLoadedLanguage,
  getCurrentTestSection,
  setTestHeight,
  updateTestSection,
  setStartTime,
  setCurrentTime,
  setContentLoaded,
  setContentUnLoaded,
  resetTestFactoryState,
  updateCheatingAttempts,
  getUpdatedTestStartTime,
  validateTimeoutState,
  getServerTime
} from "../../modules/TestSectionRedux";
import {
  setCurrentTest,
  deactivateTest,
  invalidateTest,
  quitTest,
  setPreviousTestStatusCodename,
  getPreviousTestStatus,
  getCurrentTestStatus,
  lockTest,
  pauseTest,
  resetTestStatusState,
  activateTest,
  preTestOrTransitionTest
} from "../../modules/TestStatusRedux";
import { resetInboxState } from "../../modules/EmailInboxRedux";
import { resetNotepadState } from "../../modules/NotepadRedux";
import scorescoreUitTestSection, { scoreUitTest } from "../../modules/ScoringRedux";
import { resetTopTabsState } from "../../modules/NavTabsRedux";
import { Helmet } from "react-helmet";
import TestNavBar from "../../TestNavBar";

import { PATH } from "../commons/Constants";
import LOCALIZE from "../../text_resources";
import LockScreen from "../commons/LockScreen";
import PauseScreen from "../commons/PauseScreen";
import TEST_STATUS from "../ta/Constants";
import { triggerFocusToMainNavLink, resetAccommodations } from "../../modules/AccommodationsRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { updateBackendAndDbStatusState } from "../../modules/UpdateResponseRedux";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";

import { logoutAction } from "../../modules/LoginRedux";
import { resetErrorStatusState } from "../../modules/ErrorStatusRedux";
import { resetUserState } from "../../modules/UserRedux";
import { resetPermissionsState } from "../../modules/PermissionsRedux";
import { history } from "../../store-index";

const styles = {
  container: {
    maxWidth: 1400,
    margin: "0px auto",
    textAlign: "left"
  },
  alignCenter: {
    textAlign: "center"
  },
  loadingContainer: {
    textAlign: "center"
  }
};

class TestPage extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      setLanguage: PropTypes.func,
      currentLanguage: PropTypes.string,
      sampleTest: PropTypes.bool,
      testAccessCode: PropTypes.string,
      taActionTriggered: PropTypes.bool
    };
  }

  state = {
    showCheatingPopup: false,
    isLoading: true,
    pollingState: undefined,
    pollingState2: undefined,
    showBackendAndDbDownPopup: false,
    needsApproval: this.props.testSection.needs_approval,
    currentTestStatusCodename: TEST_STATUS.CHECKED_IN,
    currentTestPreviousStatusCodename: null,
    isUitTest: false
  };

  componentDidMount = () => {
    let order = 0;
    if (this.props.testSection.order) {
      order = parseInt(this.props.testSection.order) - 1;
    }
    document.addEventListener("visibilitychange", this.visibilityChange);
    // not in a sample test
    if (!window.location.href.includes(PATH.sampleTests)) {
      // checking needs approval state right away
      this.handleNeedsApprovalVerificationAndTestSectionUpdates();
    }
    // get the test section from the parent page
    if (!this.props.testSection.order) {
      this.getTestSection(this.props.assignedTestId, order, "", this.props.testId);
      this.props.updateAssignedTestId(
        this.props.assignedTestId,
        this.props.testId,
        this.props.accommodationRequestId,
        this.props.userAccommodationFileId
      );
    } else {
      this.getCurrentTestSection(
        this.props.assignedTestId,
        this.props.testSection.id,
        this.props.testId
      );
    }
    if (
      this.props.testSectionLoaded &&
      this.props.assignedTestId !== null &&
      this.props.assignedTestId !== undefined
    ) {
      this.getUpdatedTestStartTime(this.props.assignedTestId, this.props.testSection.id);
    } else {
      // set isLoading state to false
      this.setState({ isLoading: false });
    }
    if (this.props.assignedTestId !== null && this.props.assignedTestId !== undefined) {
      this.props.setCurrentTest(this.props.assignedTestId, null, null);
    }
    this.props.getServerTime().then(response => {
      this.props.setCurrentTime(response);
    });
    // ========== polling functions ==========
    // test is not locaked/paused
    if (!this.props.isTestLocked && !this.props.isTestPaused) {
      // setting up the polling function
      if (this.state.pollingState) {
        clearInterval(this.state.pollingState);
      }
      // creating new polling interval (interval of 2 minutes 30 seconds)
      const interval = setInterval(this.handleTimeoutValidationFunction, 150000);
      this.setState({ pollingState: interval });
    }
    // test is not locaked/paused
    if (!this.props.isTestLocked && !this.props.isTestPaused) {
      // setting up the polling function
      if (this.state.pollingState2) {
        clearInterval(this.state.pollingState2);
      }
      // not in a sample test
      if (!window.location.href.includes(PATH.sampleTests)) {
        // creating new polling interval (interval of 5 seconds)
        const interval = setInterval(
          this.handleNeedsApprovalVerificationAndTestSectionUpdates,
          5000
        );
        this.setState({ pollingState2: interval });
      }
    }
    // ========== polling functions (END) ==========

    // Setting backend and db status to true
    this.props.updateBackendAndDbStatusState(true);
  };

  componentDidUpdate = prevProps => {
    // if isTestLocked, isTestPaused, isTestActive or isTestPreTestOrTransition gets updated
    if (
      prevProps.isTestLocked !== this.props.isTestLocked ||
      prevProps.isTestPaused !== this.props.isTestPaused ||
      prevProps.isTestActive !== this.props.isTestActive ||
      prevProps.isTestPreTestOrTransition !== this.props.isTestPreTestOrTransition
    ) {
      // getting updated test start time
      this.getUpdatedTestStartTime(this.props.assignedTestId, this.props.testSection.id);
      // getting previous status
      this.props
        .getPreviousTestStatus(this.props.assignedTestId)
        .then(previousTestStatusCodename => {
          // setting previous status
          this.props.setPreviousTestStatusCodename(previousTestStatusCodename);
        });
      // if test is locked/paused
      if (this.props.isTestLocked || this.props.isTestPaused) {
        // clearing polling interval
        clearInterval(this.state.pollingState);
        clearInterval(this.state.pollingState2);
      } else {
        // creating back the polling interval (interval of 2 minutes 30 seconds)
        const interval1 = setInterval(this.handleTimeoutValidationFunction, 150000);
        this.setState({ pollingState: interval1 });
        // not in a sample test
        if (!window.location.href.includes(PATH.sampleTests)) {
          const interval2 = setInterval(
            this.handleNeedsApprovalVerificationAndTestSectionUpdates,
            5000
          );
          this.setState({ pollingState2: interval2 });
        }
      }
    }
    // if testSectionLoaded get updated
    if (prevProps.testSectionLoaded !== this.props.testSectionLoaded) {
      // in a sasmple test
      if (window.location.href.includes(PATH.sampleTests)) {
        // setting assigned_test_id and accommodation_request_id/user_accommodation_file_id to null
        this.props.updateAssignedTestId(null, this.props.testId, null, null);
      }
      // clicking on main content link (if it exists)
      if (document.getElementById("main-content")) {
        document.getElementById("main-content").focus();
      }
      if (this.props.testSectionLoaded) {
        // set isLoading state to false
        this.setState({ isLoading: !this.props.testSectionLoaded });
      }
    }

    if (prevProps.isBackendAndDbUp !== this.props.isBackendAndDbUp) {
      // backend and/or DB are down
      if (!this.props.isBackendAndDbUp) {
        this.setState({ showBackendAndDbDownPopup: true });
      }
    }
  };

  handleTimeoutValidationFunction = () => {
    this.props
      .validateTimeoutState(this.props.assignedTestId, this.props.testSection.id)
      .then(response => {
        // user should be timed out
        if (response.status === 409) {
          // go to next section
          this.handleNext(true);
        }
      });
  };

  handleNeedsApprovalVerificationAndTestSectionUpdates = () => {
    // getting assigned test data
    this.props
      .getAssignedTests(this.props.currentLanguage, this.props.assignedTestId)
      .then(response => {
        // making sure that we're retrieving at least one test
        if (response.length > 0) {
          // updating needsApproval state
          this.setState(
            {
              needsApproval: response[0].needs_approval,
              currentTestStatusCodename: response[0].status_codename,
              currentTestPreviousStatusCodename: response[0].previous_status_codename,
              isUitTest: response[0].uit_invite_id !== null
            },
            () => {
              // if test is active
              if (response[0].status_codename === TEST_STATUS.ACTIVE) {
                this.props.activateTest();
                // if test is in pre-test or transition state
              } else if (
                response[0].status_codename === TEST_STATUS.PRE_TEST ||
                response[0].status_codename === TEST_STATUS.TRANSITION
              ) {
                this.props.preTestOrTransitionTest();
              }
              // updating assigned test redux state
              this.props.updateAssignedTestId(
                response[0].id,
                response[0].test_id,
                response[0].accommodation_request_id,
                response[0].user_accommodation_file_id
              );
            }
          );
        }
      });
  };

  getTestSection = (
    assignedTestId,
    order = 0,
    specialSection = "",
    testId,
    timedOut = false,
    triggerAccessTimeEntry = true
  ) => {
    this.setState({ isLoading: true }, () => {
      let isTimedSection = false;
      this.props.eraseOppositeLoadedLanguage(this.props.currentLanguage);
      this.props
        .getTestSection(
          assignedTestId,
          order,
          specialSection,
          testId,
          this.props.currentLanguage,
          timedOut,
          triggerAccessTimeEntry
        )
        .then(response => {
          if (response.error) {
            return;
          }
          this.props.setContentUnLoaded();

          this.props.setCurrentTime(response.current_time_utc);

          // initializing start time if the current section is a timed section
          if (this.isTimedSection(response.default_time)) {
            isTimedSection = true;
            if (!response.start_time) {
              this.props.setStartTime(response.current_time_utc);
            }
          }

          // current section is a timed section
          if (
            isTimedSection &&
            this.props.assignedTestId !== null &&
            this.props.assignedTestId !== undefined
          ) {
            this.getUpdatedTestStartTime(assignedTestId, response.id);
          }

          this.props.setTestHeight(this.props.isTestActive ? TEST_STYLE : BANNER_STYLE);
          this.props.updateTestSection(response, this.props.currentLanguage);
          this.props.setContentLoaded();
          // not in a sample test
          if (!window.location.href.includes(PATH.sampleTests)) {
            // checking needs approval state
            this.handleNeedsApprovalVerificationAndTestSectionUpdates();
          }
        })
        .then(() => {
          // set isLoading state to false
          this.setState({ isLoading: false });
        });
    });
  };

  getCurrentTestSection = (assignedTestId, testSectionId, testId) => {
    this.setState({ isLoading: true }, () => {
      if (this.props.languagesLoaded.includes(this.props.currentLanguage)) {
        return;
      }
      this.props
        .getCurrentTestSection(assignedTestId, testSectionId, testId, this.props.currentLanguage)
        .then(response => {
          if (response.error) {
            return;
          }

          this.props.updateTestSection(response, this.props.currentLanguage);
          this.props.setContentLoaded();
        })
        .then(() => {
          // set isLoading state to false
          this.setState({ isLoading: false });
        });
    });
  };

  handleNext = (timedOut = false) => {
    // making sure that this is not a sample test
    if (!this.props.sampleTest) {
      // getting current test status
      this.props.getCurrentTestStatus(this.props.assignedTestId).then(testStatusData => {
        // current section is a pre-test section
        if (this.props.testSection.default_time === null) {
          // expected test status = PRE_TEST/PAUSED/LOCKED
          if (
            testStatusData.status_codename !== TEST_STATUS.PRE_TEST &&
            testStatusData.status_codename !== TEST_STATUS.TRANSITION &&
            testStatusData.status_codename !== TEST_STATUS.LOCKED &&
            testStatusData.status_codename !== TEST_STATUS.PAUSED
          ) {
            // kicking candidate out
            this.handleCandidateKickout();
            // current section is LOCKED
          } else if (testStatusData.status_codename === TEST_STATUS.LOCKED) {
            // locking test (redux state)
            this.props.lockTest();
            // current section is PAUSED
          } else if (testStatusData.status_codename === TEST_STATUS.PAUSED) {
            // pausing test (redux state)
            this.props.pauseTest();
            // reloading page (in order for the timer to be updated)
            window.location.reload();
            // expected test status is the right one
          } else {
            // getting next test section
            this.getTestSection(
              this.props.assignedTestId,
              this.props.testSection.order,
              "",
              this.props.testId
            );
          }
        } // current section is a timed section
        else if (this.props.testSection.default_time !== null) {
          // expected test status = ACTIVE/PAUSED/LOCKED
          if (
            testStatusData.status_codename !== TEST_STATUS.ACTIVE &&
            testStatusData.status_codename !== TEST_STATUS.LOCKED &&
            testStatusData.status_codename !== TEST_STATUS.PAUSED
          ) {
            // kicking candidate out
            this.handleCandidateKickout();
            // current section is LOCKED or PAUSED
          } else if (testStatusData.status_codename === TEST_STATUS.LOCKED) {
            // locking test (redux state)
            this.props.lockTest();
            // current section is PAUSED
          } else if (testStatusData.status_codename === TEST_STATUS.PAUSED) {
            // pausing test (redux state)
            this.props.pauseTest();
            // reloading page (in order for the timer to be updated)
            window.location.reload();
            // expected test status is the right one
          } else {
            // getting next test section
            this.getTestSection(
              this.props.assignedTestId,
              this.props.testSection.order,
              "",
              this.props.testId,
              timedOut
            );
          }
        }
      });
    } else {
      // getting next test section
      this.getTestSection(
        this.props.assignedTestId,
        this.props.testSection.order,
        "",
        this.props.testId
      );
    }
  };

  handleCandidateKickout = () => {
    // invalidating test + redirecting to invalidated test page
    this.props.invalidateTest();
    this.resetAllRedux();
    history.push(PATH.invalidateTest);
  };

  handleFinishTest = () => {
    this.props.deactivateTest();
    this.resetAllRedux();
    history.push(PATH.dashboard);
    // trigger focus to Home link
    this.props.triggerFocusToMainNavLink();
  };

  // getting updated test start time (including lock/pause time calculation) + setting start time redux state
  getUpdatedTestStartTime = (assignedTestId, testSectionId) => {
    this.setState({ isLoading: true }, () => {
      this.props
        .getUpdatedTestStartTime(assignedTestId, testSectionId)
        .then(response => {
          // if a new date/time is received and it's not the same as the testSectionStartTime redux state
          if (!response.info && !response.error && this.props.assignedTestId) {
            // overriding start time in redux if there are lock/pause actions (timed sections only)
            this.props.setStartTime(response);
          }
        })
        .then(() => {
          // set isLoading state to false
          this.setState({ isLoading: false });
        });
    });
  };

  isTimedSection = defaultTime => {
    if (defaultTime || defaultTime === 0) return true;
    return false;
  };

  // resetting all needed redux states
  resetAllRedux = () => {
    this.props.resetTestFactoryState();
    this.props.resetInboxState();
    this.props.resetNotepadState();
    this.props.resetAssignedTestState();
    this.props.resetQuestionListState();
    this.props.resetTopTabsState();
  };

  visibilityChange = () => {
    if (this.props.testSection.block_cheating) {
      if (!this.state.showCheatingPopup) {
        this.openCheatingPopup();
      }
    }
  };

  openCheatingPopup = () => {
    this.setState({ showCheatingPopup: true });
    // preventing unwanted cheatingAttempts updates
    if (this.props.assignedTestId !== null) {
      this.props.updateCheatingAttempts(Number(this.props.cheatingAttempts) + 1);
    }
  };

  closeCheatingPopup = () => {
    this.setState({ showCheatingPopup: false });
  };

  timeout = () => {
    // clearing polling interval (in case the polling function is called at the same time as the timeout function - avoiding conflicts)
    clearInterval(this.state.pollingState);

    // making sure we are in a test, and not a sample test
    if (typeof this.props.assignedTestId !== "undefined" && this.props.assignedTestId !== null) {
      // making sure that the test is in a real timeout state
      this.props
        .validateTimeoutState(this.props.assignedTestId, this.props.testSection.id)
        .then(response => {
          // user should be timed out
          if (response.status === 409) {
            // go to next section
            this.handleNext(true);
            // creating back the polling interval (interval of 2 minutes 30 seconds) in case there is another timed section
            const interval = setInterval(this.handleTimeoutValidationFunction, 150000);
            this.setState({ pollingState: interval });
          } else {
            // triggering page refresh (should never happen)
            window.location.reload();
          }
        });
    }
    // if we are in a sample test, simply go to the next section
    else {
      // go to next section
      this.handleNext(true);
    }
  };

  componentWillUnmount = () => {
    document.removeEventListener("visibilitychange", this.visibilityChange);
    clearInterval(this.state.pollingState);
    clearInterval(this.state.pollingState2);
  };

  handleQuitTest = () => {
    // getting current test status
    this.props.getCurrentTestStatus(this.props.assignedTestId).then(testStatusData => {
      // current section is a pre-test section
      if (this.props.testSection.default_time === null) {
        // expected test status = PRE_TEST/LOCKED/PAUSED
        if (
          testStatusData.status_codename !== TEST_STATUS.PRE_TEST &&
          testStatusData.status_codename !== TEST_STATUS.LOCKED &&
          testStatusData.status_codename !== TEST_STATUS.PAUSED
        ) {
          // kicking candidate out
          this.handleCandidateKickout();
        }
      } // current section is a timed section
      else if (this.props.testSection.default_time !== null) {
        // expected test status = ACTIVE/LOCKED/PAUSED
        if (
          testStatusData.status_codename !== TEST_STATUS.ACTIVE &&
          testStatusData.status_codename !== TEST_STATUS.LOCKED &&
          testStatusData.status_codename !== TEST_STATUS.PAUSED
        ) {
          // kicking candidate out
          this.handleCandidateKickout();
        }
      }
      this.props.quitTest();
      this.getTestSection(
        this.props.assignedTestId,
        this.props.testSection.order,
        "quit",
        this.props.testId
      );
    });
  };

  loadingDiv = customFontSize => {
    return (
      <div>
        <h2 style={{ width: "900px", margin: "auto", paddingTop: "20px" }}>
          {LOCALIZE.commons.loading}
        </h2>
        <div style={{ ...styles.loadingContainer, ...customFontSize }}>
          {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
          <label className="fa fa-spinner fa-spin">
            <FontAwesomeIcon icon={faSpinner} />
          </label>
        </div>
      </div>
    );
  };

  handleBackToLogin = () => {
    // handling redux states reset
    this.handleLogout();
    // making sure that the redux states are being reset before redirecting user to login page
    setTimeout(() => {
      // redirecting user to login page
      history.push(PATH.login);
    }, 250);
  };

  handleLogout = () => {
    // resetting needed redux states
    this.props.resetTestStatusState();
    this.props.resetAccommodations();
    this.props.resetNotepadState();
    this.props.logoutAction();
    this.props.resetUserState();
    this.props.resetErrorStatusState();
    this.props.resetPermissionsState();
  };

  handleReturnHome = () => {
    this.props.resetTestStatusState();
    history.push(PATH.dashboard);
  };

  render() {
    const customFontSize = {
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 12}px`
    };

    if (!this.props.testSectionLoaded && !this.props.isTestLocked && !this.props.isTestPaused) {
      return this.loadingDiv(customFontSize);
    }
    if (!this.props.testSectionLoaded || !this.props.testSections[this.props.currentLanguage]) {
      return this.loadingDiv(customFontSize);
    }
    const { testSection, testSections } = this.props;

    return (
      <div>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title className="notranslate">
            {LOCALIZE.formatString(
              LOCALIZE.titles.uitTest,
              testSection[`${this.props.currentLanguage}_title`]
            )}
          </title>
        </Helmet>
        <TestNavBar
          updateBackendTest={() => {}}
          handleQuitTest={() => this.handleQuitTest()}
          showQuitTestButton={
            (this.state.currentTestStatusCodename === TEST_STATUS.ACTIVE ||
              this.state.currentTestStatusCodename === TEST_STATUS.LOCKED ||
              this.state.currentTestStatusCodename === TEST_STATUS.PAUSED) &&
            this.state.currentTestPreviousStatusCodename !== TEST_STATUS.CHECKED_IN &&
            this.state.currentTestPreviousStatusCodename !== TEST_STATUS.TRANSITION &&
            this.props.testSection.next_section_button_type !== NextSectionButtonType.NONE
          }
          showReturnHomeButton={
            (this.state.currentTestStatusCodename === TEST_STATUS.PRE_TEST ||
              this.state.currentTestStatusCodename === TEST_STATUS.TRANSITION) &&
            this.props.testSection.next_section_button_type !== NextSectionButtonType.NONE
          }
          handleReturnHome={this.handleReturnHome}
        />
        <div style={styles.container}>
          {this.props.isTestLocked ? (
            <LockScreen />
          ) : this.props.isTestPaused ? (
            <PauseScreen
              testId={this.props.testId}
              assignedTestId={this.props.assignedTestId}
              previousStatusCodename={this.props.previousStatusCodename}
              testAccessCode={this.props.testAccessCode}
            />
          ) : this.state.isLoading ? (
            this.loadingDiv(customFontSize)
          ) : (
            <TestSectionComponentFactory
              testSection={testSections[this.props.currentLanguage]}
              handleNext={this.handleNext}
              handleFinishTest={this.handleFinishTest}
              timeLimit={testSection.default_time}
              testSectionStartTime={this.props.testSectionStartTime}
              timeout={this.timeout}
              testSectionType={testSection.section_type}
              resetAllRedux={this.resetAllRedux}
              defaultTab={testSection.default_tab}
              usesNotepad={testSection.uses_notepad}
              usesCalculator={testSection.uses_calculator}
              assignedTestId={this.props.assignedTestId}
              accommodations={this.props.accommodations}
              isLoading={this.state.isLoading}
              needsApproval={this.state.needsApproval && this.props.testSection.needs_approval}
              isUitTest={this.state.isUitTest}
            />
          )}
        </div>
        <PopupBox
          show={this.state.showCheatingPopup && this.props.testSection.block_cheating}
          handleClose={this.closeCheatingPopup}
          title={LOCALIZE.testBuilder.errors.cheatingPopup.title}
          description={
            <div>
              <p>{LOCALIZE.testBuilder.errors.cheatingPopup.description}</p>
              <p>
                {LOCALIZE.formatString(
                  LOCALIZE.testBuilder.errors.cheatingPopup.warning,
                  3 - this.props.cheatingAttempts
                )}
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={"I understand"}
          rightButtonAction={this.closeCheatingPopup}
        />
        <PopupBox
          show={this.state.showBackendAndDbDownPopup}
          title={LOCALIZE.testBuilder.errors.backendAndDbDownPopup.title}
          handleClose={() => {}}
          size="lg"
          description={
            <SystemMessage
              messageType={MESSAGE_TYPE.error}
              title={LOCALIZE.commons.error}
              message={
                <p className="notranslate">
                  {LOCALIZE.testBuilder.errors.backendAndDbDownPopup.description}
                </p>
              }
            />
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.testBuilder.errors.backendAndDbDownPopup.rightButton}
          rightButtonAction={() => this.handleBackToLogin()}
        />
      </div>
    );
  }
}

export { TestPage as UnconnectedTestPage };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    assignedTestId: state.assignedTest.assignedTestId,
    accommodationRequestId: state.assignedTest.accommodationRequestId,
    userAccommodationFileId: state.assignedTest.userAccommodationFileId,
    testSection: state.testSection.testSection,
    testSections: state.testSection.testSections,
    languagesLoaded: state.testSection.languagesLoaded,
    testSectionLoaded: state.testSection.isLoaded,
    testSectionStartTime: state.testSection.startTime,
    cheatingAttempts: state.testSection.cheatingAttempts,
    currentQuestion: state.questionList.currentQuestion,
    startTime: state.questionList.startTime,
    testId: state.assignedTest.testId,
    isTestActive: state.testStatus.isTestActive,
    isTestPreTestOrTransition: state.testStatus.isTestPreTestOrTransition,
    isTestLocked: state.testStatus.isTestLocked,
    isTestPaused: state.testStatus.isTestPaused,
    previousStatusCodename: state.testStatus.previousStatusCodename,
    username: state.user.username,
    accommodations: state.accommodations,
    isBackendAndDbUp: state.testActions.isBackendAndDbUp
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLanguage,
      getTestSection,
      getCurrentTestSection,
      eraseOppositeLoadedLanguage,
      setTestHeight,
      updateTestSection,
      setContentLoaded,
      setContentUnLoaded,
      setStartTime,
      setCurrentTime,
      resetTestFactoryState,
      resetAssignedTestState,
      resetInboxState,
      resetNotepadState,
      resetQuestionListState,
      resetTopTabsState,
      scorescoreUitTestSection,
      scoreUitTest,
      updateCheatingAttempts,
      deactivateTest,
      activateTest,
      preTestOrTransitionTest,
      quitTest,
      getUpdatedTestStartTime,
      getAssignedTests,
      updateAssignedTestId,
      setCurrentTest,
      triggerFocusToMainNavLink,
      setPreviousTestStatusCodename,
      getPreviousTestStatus,
      getCurrentTestStatus,
      invalidateTest,
      lockTest,
      pauseTest,
      validateTimeoutState,
      getServerTime,
      updateBackendAndDbStatusState,
      resetTestStatusState,
      resetAccommodations,
      logoutAction,
      resetUserState,
      resetErrorStatusState,
      resetPermissionsState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TestPage));
