import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import NextPreviousButtonNav from "../commons/NextPreviousButtonNav";
import { useTabState, Tab, TabList, TabPanel } from "reakit/Tab";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";

const styles = {
  container: {
    display: "flex"
  }
};

function getTabs(props, tab, componentFunctions) {
  componentFunctions.onSelect(tab.currentId);

  let menuTabStyle = {
    display: "block",
    backgroundColor: "#00565e",
    color: "#fff",
    borderRadius: "0.35rem",
    width: "100%",
    margin: "0 auto",
    wordWrap: "break-word"
  };

  let menuTabInactive = {
    display: "block",
    backgroundColor: "#fafafa",
    border: "none",
    width: "100%",
    color: "#0278a4",
    margin: "0 auto",
    wordWrap: "break-word"
  };

  if (props.accommodations.spacing) {
    menuTabStyle = {
      ...menuTabStyle,
      ...getLineSpacingCSS()
    };
    menuTabInactive = {
      ...menuTabInactive,
      ...getLineSpacingCSS()
    };
  } else {
    menuTabStyle = {
      ...menuTabStyle,
      ...{ padding: 6 }
    };
    menuTabInactive = {
      ...menuTabInactive,
      ...{ padding: 6 }
    };
  }
  return (
    <>
      {props.specs.map((item, index) => {
        return (
          <Tab
            {...tab}
            id={index + 1}
            key={index + 1}
            style={tab.selectedId === index + 1 ? menuTabStyle : menuTabInactive}
          >
            {item.menuString}
            <span style={componentFunctions.styles.hiddenText}>{props.parentTabName}</span>
          </Tab>
        );
      })}
    </>
  );
}

function getTabPanels(props, tab, componentFunctions) {
  const { specs } = props;

  const dynamicPaddingBottom = { paddingBottom: props.testFooterHeight + 12 };

  return (
    <>
      {specs.map((item, index) => {
        return (
          <TabPanel className="container" key={index + 1} {...tab} style={props.tabContentStyle}>
            <div
              style={
                props.bodyContentCustomStyle
                  ? {
                      ...componentFunctions.styles.bodyContent,
                      ...props.bodyContentCustomStyle,
                      ...dynamicPaddingBottom
                    }
                  : { ...componentFunctions.styles.bodyContent, ...dynamicPaddingBottom }
              }
            >
              <Col>
                <Row>{item.body}</Row>
                {props.displayNextPreviousButton && (
                  <NextPreviousButtonNav
                    showNext={index + 1 < specs.length - 1}
                    showPrevious={index + 1 > 0}
                    onChangeToPrevious={() => {
                      componentFunctions.onChangeToPrevious(index + 1 - 1);
                    }}
                    onChangeToNext={() => {
                      componentFunctions.onChangeToNext(index + 1 + 1);
                    }}
                  />
                )}
              </Col>
            </div>
          </TabPanel>
        );
      })}
    </>
  );
}

function SideNavigationHooks(props) {
  const tab = useTabState({ orientation: "vertical", selectedId: Number(props.activeKey) || 1 });

  const dynamicPaddingBottom = { paddingBottom: props.testFooterHeight + 12 };

  return function func(componentFunctions) {
    return (
      <Row style={styles.container}>
        <Col lg={3}>
          <TabList
            {...tab}
            aria-label={LOCALIZE.ariaLabel.sideNavigationSection}
            id="navigation-items-section"
            style={{
              ...props.tabContentStyle,
              ...componentFunctions.styles.sideNavPadding,
              ...dynamicPaddingBottom
            }}
            className="container"
          >
            {getTabs(props, tab, componentFunctions)}
          </TabList>
        </Col>
        <Col lg={9} style={props.tabContainerStyle}>
          {getTabPanels(props, tab, componentFunctions)}
        </Col>
      </Row>
    );
  };
}

class SideNavigation extends Component {
  EVENT_KEYS = [];

  styles = {
    rowContainer: {
      margin: 0
    },
    bodyContent: {
      paddingTop: 10,
      paddingBottom: 10,
      minHeight: 400,
      textAlign: "left"
    },
    hiddenText: {
      position: "absolute",
      left: -10000,
      top: "auto",
      width: 1,
      height: 1,
      overflow: "hidden"
    },
    noStyle: {}
  };

  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      startIndex: PropTypes.number.isRequired,
      specs: PropTypes.arrayOf(
        PropTypes.shape({
          menuString: PropTypes.string,
          body: PropTypes.object
        })
      ).isRequired,
      isMain: PropTypes.bool,
      parentTabName: PropTypes.string,
      tabContainerStyle: PropTypes.object,
      tabContentStyle: PropTypes.object,
      navStyle: PropTypes.object,
      bodyContentCustomStyle: PropTypes.object
    };
    SideNavigation.defaultProps = {
      tabContainerStyle: this.styles.noStyle,
      tabContentStyle: this.styles.noStyle,
      navStyle: this.styles.noStyle,
      showNavButtons: true
    };
  }

  componentDidMount = () => {
    document.getElementById("navigation-items-section").focus();
  };

  onChangeToNext = index => {
    this.props.switchTab(index);
    document.getElementById("main-content").scrollTop = 0;
  };

  onChangeToPrevious = index => {
    this.props.switchTab(index);
    document.getElementById("main-content").scrollTop = 0;
  };

  onSelect = eventKey => {
    this.props.switchTab(eventKey);
    document.getElementById("main-content").scrollTop = 0;
  };

  render() {
    const { myHookValue } = this.props;

    return <>{myHookValue(this)}</>;
  }
}

function withMyHook(Component) {
  return function WrappedComponent(props) {
    const myHookValue = SideNavigationHooks(props);
    return <Component {...props} myHookValue={myHookValue} />;
  };
}

const mapStateToProps = (state, ownProps) => {
  return {
    testFooterHeight: state.testSection.testFooterHeight,
    accommodations: state.accommodations
  };
};

export default connect(mapStateToProps, null)(withMyHook(SideNavigation));
