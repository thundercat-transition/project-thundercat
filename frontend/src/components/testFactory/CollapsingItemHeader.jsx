import React, { Component } from "react";
import PropTypes from "prop-types";
import "../../css/collapsing-item.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleDown, faAngleUp } from "@fortawesome/free-solid-svg-icons";

const styles = {
  container: {
    padding: 5,
    position: "relative",
    wordWrap: "break-word"
  },
  button: {
    width: "100%",
    textAlign: "left"
  },
  title: {
    fontSize: 18
  },
  expandIcon: {
    position: "absolute",
    top: 0,
    right: 0,
    margin: "8px 24px 0 0",
    pointerEvents: "none",
    fontSize: 24
  }
};

class CollapsingItemHeader extends Component {
  static propTypes = {
    iconType: PropTypes.object,
    title: PropTypes.object.isRequired,
    body: PropTypes.object.isRequired
  };

  state = {
    isCollapsed: true
  };

  componentDidMount = () => {};

  expandItem = () => {
    this.setState({ isCollapsed: !this.state.isCollapsed });
  };

  render() {
    const { title, body } = this.props;
    const { isCollapsed } = this.state;
    let { iconClass, containerClass } = "";

    if (isCollapsed) {
      iconClass = faAngleDown;
    } else if (!isCollapsed) {
      iconClass = faAngleUp;
    }
    containerClass = "scorer-header-btn-primary expanded-header-button-style";
    return (
      <div
        className={`${containerClass}`}
        style={styles.container}
        onClick={this.expandItem}
        aria-expanded={!this.state.isCollapsed}
      >
        <span style={styles.title}>{title}</span>
        <FontAwesomeIcon
          icon={iconClass}
          style={styles.expandIcon}
          aria-hidden="true"
          focusable="false"
        />

        {!isCollapsed && (
          <div style={{}}>{React.cloneElement(body, { expandItem: this.expandItem })}</div>
        )}
      </div>
    );
  }
}

export default CollapsingItemHeader;
