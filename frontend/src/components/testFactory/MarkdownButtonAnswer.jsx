import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ReactMarkdown from "react-markdown";
import rehypeRaw from "rehype-raw";
import "../../css/MarkdownButtonAnswer.css";

const styles = {
  buttonContainer: {
    display: "table",
    marginLeft: 24,
    height: 36
  },
  radioButtonContainer: {
    display: "table-cell",
    verticalAlign: "middle",
    height: "100%"
  },
  radioButtonLabelContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  button: {
    width: "1em",
    height: "100%",
    verticalAlign: "sub"
  },
  label: {
    paddingLeft: 12
  }
};

class MarkdownButtonAnswer extends Component {
  render() {
    const { answer, answerId, questionId, currentAnswer } = this.props;
    return (
      <div style={styles.buttonContainer} lang={this.props.languageCode} role={"presentation"}>
        <div style={styles.radioButtonContainer}>
          <input
            name={`${questionId}-answers`}
            type={"radio"}
            checked={currentAnswer === answerId}
            style={styles.button}
            onChange={() => this.props.handleClick(questionId, answerId)}
            id={`question${questionId}-answer${answerId}`}
            lang={this.props.languageCode}
          />
        </div>
        <div id="unit-test-markdown-button-answer-text" style={styles.radioButtonLabelContainer}>
          <label
            style={styles.label}
            htmlFor={`question${questionId}-answer${answerId}`}
            lang={this.props.languageCode}
          >
            <ReactMarkdown
              rehypePlugins={[rehypeRaw]}
              // eslint-disable-next-line react/no-children-prop
              children={answer.content}
              // add className="notranslate" to 'p' tags to prevent Microsoft Edge from translating
              components={{
                p: ({ node, ...props }) => <p className="notranslate" {...props} />
              }}
            />
          </label>
        </div>
      </div>
    );
  }
}

export { MarkdownButtonAnswer as UnconnectedMarkdownButtonAnswer };
const mapStateToProps = (state, ownProps) => {
  let currentAnswer = -1;
  if (typeof state.questionList.questionSummaries[`${ownProps.questionId}`] !== "undefined")
    currentAnswer = state.questionList.questionSummaries[`${ownProps.questionId}`].answer;
  return {
    currentLanguage: state.localize.language,
    currentAnswer: currentAnswer
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MarkdownButtonAnswer);
