import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { PATH } from "../commons/Constants";
import TestPage from "./TestPage";
import { Routes, Route, Navigate } from "react-router-dom";
import {
  getAssignedTests,
  updateAssignedTestId,
  resetAssignedTestState
} from "../../modules/AssignedTestsRedux";
import {
  activateTest,
  deactivateTest,
  lockTest,
  unlockTest,
  pauseTest,
  unpauseTest,
  invalidateTest,
  setPreviousTestStatusCodename,
  getPreviousTestStatus,
  resetTestStatusState,
  getCurrentTestStatus,
  preTestOrTransitionTest
} from "../../modules/TestStatusRedux";
import { resetTestFactoryState } from "../../modules/TestSectionRedux";
import { resetInboxState } from "../../modules/EmailInboxRedux";
import { resetNotepadState } from "../../modules/NotepadRedux";
import { resetQuestionListState } from "../../modules/QuestionListRedux";
import { resetTopTabsState } from "../../modules/NavTabsRedux";
import TEST_STATUS from "../ta/Constants";
import { setErrorStatus } from "../../modules/ErrorStatusRedux";
import { history } from "../../store-index";

class TestLanding extends Component {
  state = {
    isLoading: true,
    testAccessCode: null,
    taActionTriggered: true,
    pollingState: undefined
  };

  componentDidMount = () => {
    this.checkForActiveAssignedTest();
    // setting up the polling function
    if (this.state.pollingState) {
      clearInterval(this.state.pollingState);
    }
    // getting assigned tests
    this.props
      .getAssignedTests(this.props.currentLanguage, this.props.assignedTestId)
      .then(assignedTests => {
        // looping in assigned tests
        for (let i = 0; i < assignedTests.length; i++) {
          // if current assigned test is PRE_TEST, ACTIVE, LOCKED or PAUSED
          if (
            assignedTests[i].status_codename === TEST_STATUS.PRE_TEST ||
            assignedTests[i].status_codename === TEST_STATUS.ACTIVE ||
            assignedTests[i].status_codename === TEST_STATUS.TRANSITION ||
            assignedTests[i].status_codename === TEST_STATUS.LOCKED ||
            assignedTests[i].status_codename === TEST_STATUS.PAUSED
          ) {
            let interval;
            // clearing existing polling interval(s)
            clearInterval(this.state.pollingState);
            // if supervised test
            if (
              assignedTests[i].uit_invite_id === null ||
              typeof assignedTests[i].uit_invite_id === "undefined"
            ) {
              // creating new polling interval (interval of 5 seconds)
              interval = setInterval(this.handleTestStatusUpdates, 5000);

              // if unsupervised test
            } else {
              // creating new polling interval (interval of 30 seconds)
              interval = setInterval(this.handleTestStatusUpdates, 30000);
            }
            this.setState({ pollingState: interval });
          }
        }
      });
  };

  componentWillUnmount = () => {
    clearInterval(this.state.pollingState);
  };

  handleCandidateKickout = () => {
    // invalidating test + redirecting to invalidated test page
    this.props.invalidateTest();
    this.resetAllRedux();
    history.push(PATH.invalidateTest);
  };

  // resetting all needed redux states
  resetAllRedux = () => {
    this.props.resetTestFactoryState();
    this.props.resetInboxState();
    this.props.resetNotepadState();
    this.props.resetAssignedTestState();
    this.props.resetQuestionListState();
    this.props.resetTopTabsState();
  };

  handleTestStatusUpdates = () => {
    // getting current test status
    this.props.getCurrentTestStatus(this.props.assignedTestId).then(testStatusData => {
      // getting error (basically mean that we cannot get the status data based on the provided assigned test ID)
      if (testStatusData.error) {
        // kicking out the candidate
        this.handleCandidateKickout();
      }
      // test status is LOCKED, PAUSED, CHECKED_IN or ACTIVE
      if (
        (testStatusData.status_codename === TEST_STATUS.LOCKED && !this.props.isTestLocked) ||
        (testStatusData.status_codename === TEST_STATUS.PAUSED && !this.props.isTestPaused) ||
        testStatusData.status_codename === TEST_STATUS.CHECKED_IN ||
        (testStatusData.status_codename === TEST_STATUS.ACTIVE &&
          (this.props.isTestLocked || this.props.isTestPaused)) ||
        (testStatusData.status_codename === TEST_STATUS.TRANSITION &&
          (this.props.isTestLocked || this.props.isTestPaused)) ||
        (testStatusData.status_codename === TEST_STATUS.PRE_TEST &&
          (this.props.isTestLocked || this.props.isTestPaused))
      ) {
        // reloading the page
        window.location.reload();
        // test status is UNASSIGNED or QUIT
      } else if (
        testStatusData.status_codename === TEST_STATUS.UNASSIGNED ||
        testStatusData.status_codename === TEST_STATUS.QUIT
      ) {
        // if test has been invalidated
        if (testStatusData.is_invalid) {
          // kicking out the candidate
          this.handleCandidateKickout();
        }
      }
    });
  };

  // checking for active assigned test (active or locked)
  checkForActiveAssignedTest = () => {
    this.setState({ isLoading: true }, () => {
      this.props
        .getAssignedTests(this.props.currentLanguage)
        .then(response => {
          let thereIsAnActiveTest = false;
          let thereIsAPausedTest = false;
          let thereIsALockedTest = false;

          // checking for active tests
          for (let i = 0; i < response.length; i++) {
            if (response[i].status_codename === TEST_STATUS.ACTIVE) {
              // updating assigned test redux states
              this.props.updateAssignedTestId(
                response[i].id,
                response[i].test_id,
                response[i].accommodation_request_id,
                response[i].user_accommodation_file_id
              );
              // activating test
              this.props.activateTest();
              // updating testAccessCode state
              this.setState({
                testAccessCode: response[i].test_access_code
              });
              thereIsAnActiveTest = true;
              break;
            }
          }

          // no active tests
          if (!thereIsAnActiveTest) {
            // eslint-disable-next-line no-unreachable-loop
            for (let i = 0; i < response.length; i++) {
              if (response[i].status_codename === TEST_STATUS.PAUSED) {
                // updating assigned test redux states
                this.props.updateAssignedTestId(
                  response[i].id,
                  response[i].test_id,
                  response[i].accommodation_request_id,
                  response[i].user_accommodation_file_id
                );
                // pausing test
                this.props.pauseTest(response[i].pause_test_time, response[i].pause_start_date);
                // updating testAccessCode state
                this.setState({
                  testAccessCode: response[i].test_access_code
                });
                thereIsAPausedTest = true;
                break;
              }
            }
          }

          // no active tests and no paused tests
          if (!thereIsAnActiveTest && !thereIsAPausedTest) {
            // eslint-disable-next-line no-unreachable-loop
            for (let i = 0; i < response.length; i++) {
              if (response[i].status_codename === TEST_STATUS.LOCKED) {
                // updating assigned test redux states
                this.props.updateAssignedTestId(
                  response[i].id,
                  response[i].test_id,
                  response[i].accommodation_request_id,
                  response[i].user_accommodation_file_id
                );
                // setting test to pre-test or transition state
                this.props.lockTest();
                // updating testAccessCode state
                this.setState({
                  testAccessCode: response[i].test_access_code
                });
                thereIsALockedTest = true;
                break;
              }
            }
          }

          // no active tests, no paused tests and no locked tests
          if (!thereIsAnActiveTest && !thereIsAPausedTest && !thereIsALockedTest) {
            // reset test status redux states
            this.props.resetTestStatusState();
            // eslint-disable-next-line no-unreachable-loop
            for (let i = 0; i < response.length; i++) {
              if (
                response[i].status_codename === TEST_STATUS.PRE_TEST ||
                response[i].status_codename === TEST_STATUS.TRANSITION
              ) {
                // setting test to pre-test or transition state
                this.props.preTestOrTransitionTest();
                // updating testAccessCode state
                this.setState({
                  testAccessCode: response[i].test_access_code
                });
                break;
              }
            }
          }
        })
        .then(() => {
          this.setState({ isLoading: false });
        });
    });
  };

  render() {
    return (
      <>
        {!this.state.isLoading && (
          <Routes>
            <Route
              path={""}
              element={
                this.props.isTestActive || this.props.isTestPreTestOrTransition ? (
                  <TestPage
                    sampleTest={false}
                    testAccessCode={this.state.testAccessCode}
                    taActionTriggered={this.state.taActionTriggered}
                  />
                ) : (
                  <Navigate
                    to={this.props.isTestInvalidated ? PATH.invalidateTest : PATH.dashboard}
                  />
                )
              }
            />
          </Routes>
        )}
      </>
    );
  }
}
export { TestLanding as UnconnectedUITLanding };
const mapStateToProps = (state, ownProps) => {
  return {
    assignedTestId: state.assignedTest.assignedTestId,
    isTestActive: state.testStatus.isTestActive,
    isTestPreTestOrTransition: state.testStatus.isTestPreTestOrTransition,
    isTestInvalidated: state.testStatus.isTestInvalidated,
    isTestLocked: state.testStatus.isTestLocked,
    isTestPaused: state.testStatus.isTestPaused,
    previousStatusCodename: state.testStatus.previousStatusCodename,
    username: state.user.username,
    testSection: state.testSection.testSection
  };
};
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAssignedTests,
      activateTest,
      preTestOrTransitionTest,
      deactivateTest,
      lockTest,
      unlockTest,
      pauseTest,
      unpauseTest,
      invalidateTest,
      setPreviousTestStatusCodename,
      getPreviousTestStatus,
      resetTestStatusState,
      setErrorStatus,
      updateAssignedTestId,
      getCurrentTestStatus,
      resetTestFactoryState,
      resetInboxState,
      resetNotepadState,
      resetAssignedTestState,
      resetQuestionListState,
      resetTopTabsState
    },
    dispatch
  );
export default connect(mapStateToProps, mapDispatchToProps)(TestLanding);
