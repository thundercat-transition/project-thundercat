/* eslint-disable react/no-children-prop */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import ReactMarkdown from "react-markdown";
import rehypeRaw from "rehype-raw";
import LOCALIZE from "../../text_resources";
import { QuestionSectionType } from "./Constants";

class QuestionFactory extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      isBilingualQuestion: PropTypes.bool,
      calledFromQuestionPreview: PropTypes.bool
    };
    QuestionFactory.defaultProps = {
      calledFromQuestionPreview: false
    };
  }

  componentDidMount = () => {};

  factory = questionSections => {
    return (
      <div id="unit-test-question-content">
        {this.props.isBilingualQuestion
          ? questionSections.map((section, index) => {
              switch (section.question_section_type) {
                case QuestionSectionType.MARKDOWN:
                  // calledFromQuestionPreview is defined and set to true
                  if (this.props.calledFromQuestionPreview) {
                    return (
                      // getting question sections in the right language
                      section.question_section_content.language === this.props.lang && (
                        <div key={`${section}-${index}`} id="unit-test-question-content-markdown">
                          <ReactMarkdown
                            rehypePlugins={[rehypeRaw]}
                            children={section.question_section_content.content}
                            // add className="notranslate" to 'p' tags to prevent Microsoft Edge from translating
                            components={{
                              p: ({ node, ...props }) => <p className="notranslate" {...props} />
                            }}
                          />
                        </div>
                      )
                    );
                  }
                  return (
                    // getting question sections in the right language
                    section.question_section_content.language === this.props.currentLanguage && (
                      <div key={`${section}-${index}`} id="unit-test-question-content-markdown">
                        <ReactMarkdown
                          rehypePlugins={[rehypeRaw]}
                          children={section.question_section_content.content}
                          // add className="notranslate" to 'p' tags to prevent Microsoft Edge from translating
                          components={{
                            p: ({ node, ...props }) => <p className="notranslate" {...props} />
                          }}
                        />
                      </div>
                    )
                  );

                default:
                  return (
                    <div key={index} id="unit-test-question-content-undefined-section">
                      <h1>Unsupported Question Section Type: {section.question_section_type}</h1>
                    </div>
                  );
              }
            })
          : questionSections.map((section, index) => {
              switch (section.question_section_type) {
                case QuestionSectionType.MARKDOWN:
                  return (
                    <div key={`${section}-${index}`} id="unit-test-question-content-markdown">
                      <ReactMarkdown
                        rehypePlugins={[rehypeRaw]}
                        children={section.question_section_content.content}
                        // add className="notranslate" to 'p' tags to prevent Microsoft Edge from translating
                        components={{
                          p: ({ node, ...props }) => <p className="notranslate" {...props} />
                        }}
                      />
                    </div>
                  );

                default:
                  return (
                    <div key={index} id="unit-test-question-content-undefined-section">
                      <h1>Unsupported Question Section Type: {section.question_section_type}</h1>
                    </div>
                  );
              }
            })}
      </div>
    );
  };

  render() {
    const { questionSections } = this.props;
    return (
      <>
        {/* add one to index since it starts at 0 */}
        <h1 id={`question-${this.props.index}`} lang={this.props.lang} className="notranslate">
          {LOCALIZE.formatString(LOCALIZE.mcTest.questionList.questionIdLong, this.props.index + 1)}
        </h1>
        {this.factory(questionSections)}
      </>
    );
  }
}

export { QuestionFactory as UnconnectedQuestionFactory };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

export default connect(mapStateToProps, null)(QuestionFactory);
