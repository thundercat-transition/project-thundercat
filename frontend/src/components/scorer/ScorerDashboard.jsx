import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import ContentContainer from "../commons/ContentContainer";
import ScorerTable from "./ScorerTable";
import withRouter from "../withRouter";

export const styles = {
  header: {
    marginBottom: 24
  },
  scoreTestBtn: {
    minWidth: 125,
    padding: "2px 10px"
  },
  scoreText: {
    paddingLeft: "10px",
    margin: 0
  }
};

class ScorerDashboard extends Component {
  render() {
    return (
      <div>
        <ContentContainer>
          <div>
            <div id="main-content" role="main">
              <div
                id="user-welcome-message-div"
                style={styles.header}
                tabIndex={0}
                aria-labelledby="user-welcome-message"
                className="notranslate"
              >
                <h1 id="user-welcome-message" className="green-divider">
                  {LOCALIZE.formatString(
                    LOCALIZE.profile.title,
                    this.props.firstName,
                    this.props.lastName
                  )}
                </h1>
              </div>
            </div>

            <ScorerTable />
          </div>
        </ContentContainer>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ScorerDashboard));
