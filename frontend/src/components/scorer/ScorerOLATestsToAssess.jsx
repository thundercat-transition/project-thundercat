/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Col, Row, Tabs, Tab } from "react-bootstrap";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faForwardStep,
  faHandshake,
  faHandshakeSlash,
  faSpinner,
  faTimes,
  faUserPlus
} from "@fortawesome/free-solid-svg-icons";
import { faQuestionCircle, faCircleXmark } from "@fortawesome/free-regular-svg-icons";
import {
  assignCandidateAsScorerOla,
  triggerRerenderScorerOlaTables,
  getScorerOlaTestsToAssessAsSupervisor,
  getScorerOlaAssignedCandidateDetails,
  unassignCandidateAsScorerOla,
  getAvailableTestAssessorsAsSupervisor,
  cancelTestSessionAsScorerOla
} from "../../modules/ScorerRedux";
import { addAttendee, removeAttendee } from "../../modules/AssignedTestsRedux";
import { convertDateTimeToUsersTimezone } from "../../modules/helpers";
import { LANGUAGES, LANGUAGE_IDS } from "../../modules/LocalizeRedux";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import moment from "moment";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../commons/PopupBox";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import DropdownSelect from "../commons/DropdownSelect";
import { cleanUpVirtualMeeting } from "../../modules/VirtualMeetingsRedux";
import { MASKS, MASK_FORMAT_CHARS } from "../commons/InputMask/helpers";
import InputMask from "react-input-mask";
import "../../css/scorer_ola.css";
import getSwitchTransformScale from "../../helpers/switchTransformScale";
import Switch from "react-switch";

// setting needed date global variables
const CURRENT_DATE = moment(new Date(Date.now())).format("YYYY-MM-DD");
const IN_ONE_DAY_DATE = moment(new Date(Date.now())).add(1, "days").format("YYYY-MM-DD");
const IN_TWO_DAYS_DATE = moment(new Date(Date.now())).add(2, "days").format("YYYY-MM-DD");
const IN_THREE_DAYS_DATE = moment(new Date(Date.now())).add(3, "days").format("YYYY-MM-DD");
const IN_FOUR_DAYS_DATE = moment(new Date(Date.now())).add(4, "days").format("YYYY-MM-DD");
const IN_FIVE_DAYS_DATE = moment(new Date(Date.now())).add(5, "days").format("YYYY-MM-DD");
const IN_SIX_DAYS_DATE = moment(new Date(Date.now())).add(6, "days").format("YYYY-MM-DD");

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  mainContainer: {
    margin: "24px 36px"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  },
  allUnset: {
    all: "unset"
  },
  actionIcon: {
    marginRight: 5,
    transform: "scale(1.3)"
  },
  redActionIcon: {
    marginRight: 5,
    transform: "scale(1.3)",
    color: "red"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  formContainer: {
    width: "90%",
    margin: "18px auto"
  },
  loadingContainer: {
    textAlign: "center",
    margin: "48px auto"
  },
  loadingIcon: {
    transform: "scale(2)"
  },
  rowContainer: {
    padding: "6px 0"
  },
  boldText: {
    fontWeight: "bold"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  dropdownLoadingContainer: {
    textAlign: "center"
  },
  dropdownLoadingIcon: {
    transform: "scale(1.3)"
  },
  showUnassignedOnlyRowContainer: {
    margin: "12px 0"
  },
  switchLabel: {
    fontWeight: "bold",
    paddingRight: 12,
    margin: 0
  }
};

class ScorerOlATestsToAssess extends Component {
  static props = {
    isOlaSupervisor: PropTypes.bool.isRequired
  };

  state = {
    isLoadingSwitch: false,
    showUnassignedOnlySwitchState: false,
    currentlyLoading: false,
    testsToAssessMainData: [],
    // disabling all tabs by default
    disabledTabs: [
      CURRENT_DATE,
      IN_ONE_DAY_DATE,
      IN_TWO_DAYS_DATE,
      IN_THREE_DAYS_DATE,
      IN_FOUR_DAYS_DATE,
      IN_FIVE_DAYS_DATE,
      IN_SIX_DAYS_DATE
    ],
    rowsDefinition: {},
    showAssignedCandidatePopup: false,
    showAssignCandidatePopup: false,
    selectedTestSessionData: {},
    loadingPopup: false,
    selectedOrChosenCandidateData: {},
    showAlreadyPairedPopup: false,
    showConflictingSchedulePopup: false,
    showUnassignCandidateConfirmationPopup: false,
    loadingTestAssessorOptions: false,
    testAssessorOptions: [],
    testAssessorSelectedOption: [],
    cancelingLoading: false,
    // default values
    switchHeight: 25,
    switchWidth: 50
  };

  componentDidMount = () => {
    // populating table
    this.populateTestsToAssess(this.state.showUnassignedOnlySwitchState);
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = (prevProps, prevState) => {
    // if rowsDefinition gets updated
    if (prevState.rowsDefinition !== this.state.rowsDefinition) {
      // if selectedTestSessionData is defined
      if (Object.keys(this.state.selectedTestSessionData).length > 0) {
        // getting respective row data
        const respectiveRowData = this.state.rowsDefinition.data.filter(
          obj =>
            obj.date === this.state.selectedTestSessionData.date &&
            obj.start_time === this.state.selectedTestSessionData.start_time &&
            obj.end_time === this.state.selectedTestSessionData.end_time
        );

        // making sure that respectiveRowData is defined
        if (respectiveRowData.length > 0) {
          // updating nbr_of_sessions attribute of the selectedTestSessionData state
          const copyOfSelectedTestSessionData = this.state.selectedTestSessionData;
          copyOfSelectedTestSessionData.nbr_of_sessions = respectiveRowData[0].nbr_of_sessions;

          // updating stated
          this.setState(
            {
              selectedTestSessionData: copyOfSelectedTestSessionData
            },
            () => {
              // triggering popup open to get and set the needed data
              this.handleOpenAssignCandidatePopup(this.state.selectedTestSessionData);
            }
          );
          // something happend
        } else {
          // add a 2 seconds delay (might need to improve that logic in the future)
          setTimeout(() => {
            // reloading the page
            window.location.reload();
          }, 2000);
        }
      }
    }
    // if triggerRerenderScorerTables gets updated
    if (prevProps.triggerRerenderScorerTables !== this.props.triggerRerenderScorerTables) {
      // populating table
      this.populateTestsToAssess(this.state.showUnassignedOnlySwitchState);
    }
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoadingSwitch: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoadingSwitch: false });
        }
      );
    });
  };

  handleSwitchStateUpdate = event => {
    this.setState(
      {
        showUnassignedOnlySwitchState: event
      },
      () => {
        // populating table (but setting showUnassignedOnly parameter based on event)
        this.populateTestsToAssess(event);
      }
    );
  };

  populateTestsToAssess = (showUnassignedOnly = false) => {
    this.setState({ currentlyLoading: true }, () => {
      this.props
        .getScorerOlaTestsToAssessAsSupervisor(CURRENT_DATE, IN_SIX_DAYS_DATE)
        .then(response => {
          // showUnassignedOnly is set to true
          if (showUnassignedOnly) {
            // initializing testsToAssessMainData
            const testsToAssessMainData = [];
            // looping in response
            for (let i = 0; i < response.length; i++) {
              // test session of current iteration is still unassigned
              if (response[i].test_assessor_user_id === null) {
                // populating testsToAssessMainData
                testsToAssessMainData.push(response[i]);
              }
            }
            // setting needed states
            this.setState({
              testsToAssessMainData: testsToAssessMainData,
              currentlyLoading: false,
              disabledTabs: [
                CURRENT_DATE,
                IN_ONE_DAY_DATE,
                IN_TWO_DAYS_DATE,
                IN_THREE_DAYS_DATE,
                IN_FOUR_DAYS_DATE,
                IN_FIVE_DAYS_DATE,
                IN_SIX_DAYS_DATE
              ]
            });
            // showUnassignedOnly is set to false (default)
          } else {
            // setting needed states
            this.setState({
              testsToAssessMainData: response,
              currentlyLoading: false,
              disabledTabs: [
                CURRENT_DATE,
                IN_ONE_DAY_DATE,
                IN_TWO_DAYS_DATE,
                IN_THREE_DAYS_DATE,
                IN_FOUR_DAYS_DATE,
                IN_FIVE_DAYS_DATE,
                IN_SIX_DAYS_DATE
              ]
            });
          }
        });
    });
  };

  generateTabContent = providedDate => {
    // setting columnsDefinition
    const columnsDefinition = [
      {
        label: LOCALIZE.scorer.ola.testsToAssess.table.testSessionTime,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.LEFT_TEXT }
      },
      {
        label: LOCALIZE.scorer.ola.testsToAssess.table.sessionLanguage,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.scorer.ola.testsToAssess.table.candidate,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.scorer.ola.testsToAssess.table.reasonForTesting,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.scorer.ola.testsToAssess.table.testAssessor,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.scorer.ola.testsToAssess.table.actions,
        style: { ...{ width: "20%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    // getting test to assess data for provided date
    const testToAssessDataBasedOnProvidedDate = this.state.testsToAssessMainData.filter(
      obj => obj.date === providedDate
    );

    // if data to render
    if (testToAssessDataBasedOnProvidedDate.length > 0) {
      // making sure that the provided date is no longer part of the disabledTabs state
      const copyOfDisabledTabs = [...this.state.disabledTabs];
      // initial array length
      const initialArrayLength = this.state.disabledTabs.length;
      const index = copyOfDisabledTabs.indexOf(providedDate);
      if (index > -1) {
        copyOfDisabledTabs.splice(index, 1);
      }
      // length of disabledTabs is different (updates have been made)
      if (copyOfDisabledTabs.length !== initialArrayLength) {
        this.setState({ disabledTabs: copyOfDisabledTabs });
      }
    }

    const rowsDefinition = this.populateScorerOlaTestsToAssessObject(
      testToAssessDataBasedOnProvidedDate
    );

    return (
      <GenericTable
        classnamePrefix={`scorer-ola-tests-to-assess-${providedDate}`}
        columnsDefinition={columnsDefinition}
        rowsDefinition={rowsDefinition}
        emptyTableMessage={LOCALIZE.scorer.ola.testsToAssess.table.noData}
        currentlyLoading={this.state.currentlyLoading}
      />
    );
  };

  populateScorerOlaTestsToAssessObject = providedData => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // looping in providedData given
    for (let i = 0; i < providedData.length; i++) {
      const currentResult = providedData[i];
      // formatting test session date
      const formattedTestSessionTime = `${
        convertDateTimeToUsersTimezone(currentResult.start_time).adjustedTime
      } - ${convertDateTimeToUsersTimezone(currentResult.end_time).adjustedTime}`;
      // pushing needed data in data array (needed for GenericTable component)
      data.push({
        date: currentResult.date,
        start_time: currentResult.start_time,
        end_time: currentResult.end_time,
        nbr_of_sessions: !this.props.isOlaSupervisor ? currentResult.nbr_of_sessions : null,
        column_1: formattedTestSessionTime,
        column_2:
          currentResult.language_id === LANGUAGE_IDS.english
            ? LOCALIZE.commons.english
            : LOCALIZE.commons.french,
        column_3: `${currentResult.candidate_user_first_name} ${currentResult.candidate_user_last_name} (${currentResult.candidate_user_email})`,
        column_4: currentResult[`reason_for_testing_name_${this.props.currentLanguage}`],
        column_5:
          currentResult.test_assessor_user_id !== null
            ? `${currentResult.test_assessor_user_first_name} ${currentResult.test_assessor_user_last_name}`
            : LOCALIZE.commons.na,
        column_6: (
          <>
            <StyledTooltip
              id={`tests-to-assess-view-details-${i}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`tests-to-assess-view-details-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faQuestionCircle} style={styles.actionIcon} />
                      </>
                    }
                    action={() => this.handleOpenAssignedCandidatePopup(currentResult)}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.PRIMARY}
                    ariaLabel={LOCALIZE.formatString(
                      LOCALIZE.scorer.ola.testsToAssess.table.viewDetailsButtonTooltipAccessibility,
                      `${currentResult.candidate_user_first_name} ${currentResult.candidate_user_last_name}`
                    )}
                    // disabled if no test assessor is assigned
                    disabled={currentResult.test_assessor_id === null}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>{LOCALIZE.scorer.ola.testsToAssess.table.viewDetailsButtonTooltip}</p>
                </div>
              }
            />
            <StyledTooltip
              id={`tests-to-assess-assign-unassign-${i}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`tests-to-assess-assign-unassign-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon
                          icon={
                            currentResult.test_assessor_user_id === null
                              ? faHandshake
                              : faHandshakeSlash
                          }
                          style={styles.actionIcon}
                        />
                      </>
                    }
                    action={
                      currentResult.test_assessor_user_id === null
                        ? () => this.handleOpenAssignCandidatePopup(currentResult)
                        : () => this.handleOpenUnassignCandidateConfirmationPopup(currentResult)
                    }
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.PRIMARY}
                    ariaLabel={LOCALIZE.formatString(
                      currentResult.test_assessor_user_id === null
                        ? LOCALIZE.scorer.ola.testsToAssess.table.assignButtonTooltipAccessibility
                        : LOCALIZE.scorer.ola.testsToAssess.table
                            .unassignButtonTooltipAccessibility,
                      formattedTestSessionTime
                    )}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {currentResult.test_assessor_user_id === null
                      ? LOCALIZE.scorer.ola.testsToAssess.table.assignButtonTooltip
                      : LOCALIZE.scorer.ola.testsToAssess.table.unassignButtonTooltip}
                  </p>
                </div>
              }
            />
            <StyledTooltip
              id={`tests-to-assess-assign-test-assessor-${i}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`tests-to-assess-assign-test-assessor-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faUserPlus} style={styles.actionIcon} />
                      </>
                    }
                    action={() => this.handleOpenAssignTestAssessorPopup(currentResult)}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.PRIMARY}
                    ariaLabel={LOCALIZE.formatString(
                      LOCALIZE.scorer.ola.testsToAssess.table
                        .assignTestAssessorTooltipAccessibility,
                      `${currentResult.candidate_user_first_name} ${currentResult.candidate_user_last_name}`
                    )}
                    // enable button only if user is supervisor in the test center of the respective time slot + test session is still unassigned
                    disabled={
                      !currentResult.is_supervisor || currentResult.test_assessor_user_id !== null
                    }
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>{LOCALIZE.scorer.ola.testsToAssess.table.assignTestAssessorTooltip}</p>
                </div>
              }
            />
            <StyledTooltip
              id={`tests-to-assess-cancel-test-session-${i}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`tests-to-assess-cancel-test-session-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faCircleXmark} style={styles.redActionIcon} />
                      </>
                    }
                    action={() => this.handleOpenCancelTestSessionConfirmationPopup(currentResult)}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.PRIMARY}
                    ariaLabel={LOCALIZE.formatString(
                      LOCALIZE.scorer.ola.testsToAssess.table.cancelTestSessionTooltipAccessibility,
                      `${currentResult.candidate_user_first_name} ${currentResult.candidate_user_last_name}`
                    )}
                    // enable button only if user is supervisor in the test center of the respective time slot
                    disabled={!currentResult.is_supervisor}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>{LOCALIZE.scorer.ola.testsToAssess.table.cancelTestSessionTooltip}</p>
                </div>
              }
            />
          </>
        )
      });
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      column_6_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    return rowsDefinition;
  };

  handleOpenAssignedCandidatePopup = testSessionData => {
    this.setState({ loadingPopup: true }, () => {
      this.props.getScorerOlaAssignedCandidateDetails(testSessionData.id).then(response => {
        // open popup
        this.setState({
          showAssignedCandidatePopup: true,
          showAssignCandidatePopup: false,
          selectedOrChosenCandidateData: response,
          loadingPopup: false
        });
      });
    });
  };

  closeAssignedCandidatePopup = () => {
    this.setState({ showAssignedCandidatePopup: false, selectedOrChosenCandidateData: {} });
  };

  handleOpenAssignCandidatePopup = testSessionData => {
    this.setState({ loadingPopup: true }, () => {
      // open popup
      this.setState({
        showAssignCandidatePopup: true,
        showAssignedCandidatePopup: false,
        selectedOrChosenCandidateData: testSessionData,
        loadingPopup: false,
        selectedTestSessionData: testSessionData
      });
    });
  };

  closeAssignCandidatePopup = () => {
    this.setState({
      showAssignCandidatePopup: false,
      selectedOrChosenCandidateData: {},
      selectedTestSessionData: {}
    });
  };

  handleOpenUnassignCandidateConfirmationPopup = testSessionData => {
    this.setState({
      showUnassignCandidateConfirmationPopup: true,
      selectedOrChosenCandidateData: testSessionData
    });
  };

  handleUnassignCandidate = () => {
    // setting body
    const body = {
      test_assessor_user_id: this.state.selectedOrChosenCandidateData.test_assessor_user_id,
      test_session_id: this.state.selectedOrChosenCandidateData.id
    };
    this.props.unassignCandidateAsScorerOla(body).then(response => {
      // success
      if (response.ok) {
        // remove TA from virtual meeting
        this.props.removeAttendee(
          this.state.selectedOrChosenCandidateData.test_assessor_user_id,
          this.state.selectedOrChosenCandidateData.id
        );
        // closing popup
        this.closeUnassignCandidateConfirmationPopup();
        // triggering table updates (My Tests Today & Tests to Assess)
        this.props.triggerRerenderScorerOlaTables();
        // assigned test session does not exist (might have been updated by the supervisor for example)
      } else if (response.status === 404) {
        // reloading the page
        window.location.reload();
        // should never happen
      } else {
        throw new Error("An error occurred during the unassign candidate request");
      }
    });
  };

  closeUnassignCandidateConfirmationPopup = () => {
    this.setState({
      showUnassignCandidateConfirmationPopup: false,
      selectedOrChosenCandidateData: {}
    });
  };

  handleOpenAssignTestAssessorPopup = testSessionData => {
    this.setState(
      {
        showAssignTestAssessorPopup: true,
        selectedOrChosenCandidateData: testSessionData
      },
      () => {
        // populating test assessor options
        this.populateTestAssessorOptions();
      }
    );
  };

  populateTestAssessorOptions = () => {
    this.setState({ loadingTestAssessorOptions: true }, () => {
      this.props
        .getAvailableTestAssessorsAsSupervisor(
          this.state.selectedOrChosenCandidateData.day_of_week_id,
          this.state.selectedOrChosenCandidateData.start_time,
          this.state.selectedOrChosenCandidateData.simplified_start_time,
          this.state.selectedOrChosenCandidateData.end_time,
          this.state.selectedOrChosenCandidateData.simplified_end_time,
          this.state.selectedOrChosenCandidateData.language_id
        )
        .then(response => {
          // initializing testAssessorOptions
          const testAssessorOptions = [];
          // looping in response
          for (let i = 0; i < response.length; i++) {
            testAssessorOptions.push({
              label: `${response[i].user_first_name} ${response[i].user_last_name} (${response[i].user_email})`,
              value: response[i].user_id
            });
          }

          this.setState({
            testAssessorOptions: testAssessorOptions,
            loadingTestAssessorOptions: false
          });
        });
    });
  };

  getSelectedTestAssessorOption = selectedOption => {
    this.setState({ testAssessorSelectedOption: selectedOption });
  };

  handleAssignTestAssessor = () => {
    // setting body
    const body = {
      test_assessor_user_id: this.state.testAssessorSelectedOption.value,
      consumed_reservation_code_id:
        this.state.selectedOrChosenCandidateData.consumed_reservation_code_id,
      test_session_id: this.state.selectedOrChosenCandidateData.id,
      candidate_user_id: this.state.selectedOrChosenCandidateData.candidate_user_id
    };
    this.props.assignCandidateAsScorerOla(body).then(response => {
      // success
      if (response.ok) {
        // add ta to teams meeting
        this.props.addAttendee(
          this.state.testAssessorSelectedOption.value,
          this.state.selectedOrChosenCandidateData.id
        );
        // closing popup
        this.closeAssignTestAssessorPopup();
        // triggering table updates (My Tests Today & Tests to Assess)
        this.props.triggerRerenderScorerOlaTables();
        // already paired (error_code 1)
      } else if (response.status === 409 && response.error_code === 1) {
        // close current popup
        this.closeAssignTestAssessorPopup();
        // open already paired popup
        this.setState({ showAlreadyPairedPopup: true });
        // updating tests to assess table
        this.populateTestsToAssess(this.state.showUnassignedOnlySwitchState);
        // conflicting schedule (error_code 2)
      } else if (response.status === 409 && response.error_code === 2) {
        // close current popup
        this.closeAssignTestAssessorPopup();
        // open already paired popup
        this.setState({ showConflictingSchedulePopup: true });
        // updating tests to assess table
        this.populateTestsToAssess(this.state.showUnassignedOnlySwitchState);
        // should never happen
      } else {
        throw new Error("An error occurred during the assign test assessor request");
      }
    });
  };

  closeAssignTestAssessorPopup = () => {
    this.setState({
      showAssignTestAssessorPopup: false,
      selectedOrChosenCandidateData: {},
      testAssessorSelectedOption: []
    });
  };

  handleOpenCancelTestSessionConfirmationPopup = testSessionData => {
    this.setState({
      showCancelTestSessionConfirmationPopup: true,
      selectedOrChosenCandidateData: testSessionData
    });
  };

  handleCancelTestSession = () => {
    this.setState({ cancelingLoading: true });
    // setting body
    const body = {
      consumed_reservation_code_id:
        this.state.selectedOrChosenCandidateData.consumed_reservation_code_id,
      test_session_id: this.state.selectedOrChosenCandidateData.id
    };
    // cancel the session. This will also run cleanUpVirtualMeeting behind the scenes at the appropriate time
    // this cannot be run in the front end, as the user id attached to the token is the supervisor's, not the candidate's
    // unfortunately, there is a noticable pause
    this.props.cancelTestSessionAsScorerOla(body).then(response => {
      // success
      if (response.ok) {
        this.setState({ cancelingLoading: false });
        // closing popup
        this.closeCancelTestSessionConfirmationPopup();
        // triggering table updates (My Tests Today & Tests to Assess)
        this.props.triggerRerenderScorerOlaTables();
        // should never happen
      } else {
        throw new Error("An error occurred during the assign test assessor request");
      }
    });
  };

  closeCancelTestSessionConfirmationPopup = () => {
    this.setState({
      showCancelTestSessionConfirmationPopup: false,
      selectedOrChosenCandidateData: {}
    });
  };

  handleAssignCandidateAction = () => {
    // setting body
    const body = {
      test_assessor_user_id: this.props.user_id,
      consumed_reservation_code_id:
        this.state.selectedOrChosenCandidateData.consumed_reservation_code_id,
      test_session_id: this.state.selectedOrChosenCandidateData.id,
      candidate_user_id: this.state.selectedOrChosenCandidateData.candidate_user_id
    };
    this.props.assignCandidateAsScorerOla(body).then(response => {
      // success
      if (response.ok) {
        // closing popup
        // add ta to teams meeting
        this.props.addAttendee(this.props.user_id, this.state.selectedOrChosenCandidateData.id);
        this.closeAssignCandidatePopup();
        // triggering table updates (My Tests Today & Tests to Assess)
        this.props.triggerRerenderScorerOlaTables();
        // already paired (error_code 1)
      } else if (response.status === 409 && response.error_code === 1) {
        // open already paired popup
        this.setState({ showAlreadyPairedPopup: true });
        // updating tests to assess table
        this.populateTestsToAssess(this.state.showUnassignedOnlySwitchState);
        // conflicting schedule (error_code 2)
      } else if (response.status === 409 && response.error_code === 2) {
        // open already paired popup
        this.setState({ showConflictingSchedulePopup: true });
        // updating tests to assess table
        this.populateTestsToAssess(this.state.showUnassignedOnlySwitchState);
        // should never happen
      } else {
        throw new Error("An error occurred during the assign candidate request");
      }
    });
  };

  closeAlreadyPairedPopup = () => {
    this.setState({ showAlreadyPairedPopup: false });
  };

  closeConflictingSchedulePopup = () => {
    // updating table
    this.populateTestsToAssess(this.state.showUnassignedOnlySwitchState);
    // closing popups
    this.setState({ showConflictingSchedulePopup: false, showAssignCandidatePopup: false });
  };

  render() {
    const TABS = [
      {
        key: `${CURRENT_DATE}`,
        tabName: `${CURRENT_DATE}`,
        body: this.generateTabContent(CURRENT_DATE)
      },
      {
        key: `${IN_ONE_DAY_DATE}`,
        tabName: `${IN_ONE_DAY_DATE}`,
        body: this.generateTabContent(IN_ONE_DAY_DATE)
      },
      {
        key: `${IN_TWO_DAYS_DATE}`,
        tabName: `${IN_TWO_DAYS_DATE}`,
        body: this.generateTabContent(IN_TWO_DAYS_DATE)
      },
      {
        key: `${IN_THREE_DAYS_DATE}`,
        tabName: `${IN_THREE_DAYS_DATE}`,
        body: this.generateTabContent(IN_THREE_DAYS_DATE)
      },
      {
        key: `${IN_FOUR_DAYS_DATE}`,
        tabName: `${IN_FOUR_DAYS_DATE}`,
        body: this.generateTabContent(IN_FOUR_DAYS_DATE)
      },
      {
        key: `${IN_FIVE_DAYS_DATE}`,
        tabName: `${IN_FIVE_DAYS_DATE}`,
        body: this.generateTabContent(IN_FIVE_DAYS_DATE)
      },
      {
        key: 7,
        tabName: `${IN_SIX_DAYS_DATE}`,
        body: this.generateTabContent(IN_SIX_DAYS_DATE)
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <Row
          className="align-items-center justify-content-end"
          style={styles.showUnassignedOnlyRowContainer}
        >
          <label
            htmlFor="scorer-ola-tests-to-assess-show-unassigned-only-switch"
            style={styles.switchLabel}
          >
            {LOCALIZE.scorer.ola.testsToAssess.showUnassignedOnly}
          </label>
          {!this.state.isLoadingSwitch && (
            <Switch
              id="scorer-ola-tests-to-assess-show-unassigned-only-switch"
              onChange={e => {
                this.handleSwitchStateUpdate(e);
              }}
              checked={this.state.showUnassignedOnlySwitchState}
              height={this.state.switchHeight}
              width={this.state.switchWidth}
            />
          )}
        </Row>
        <div>
          <Tabs
            defaultActiveKey={CURRENT_DATE}
            id="scorer-ola-tests-to-assess-tabs"
            style={styles.tabNavigation}
            justify={true}
          >
            {TABS.map((tab, index) => {
              return (
                <Tab
                  key={index}
                  eventKey={tab.key}
                  title={tab.tabName}
                  style={styles.tabContainer}
                  defaultActiveKey={CURRENT_DATE}
                  disabled={this.state.disabledTabs.indexOf(tab.tabName) > -1}
                >
                  {tab.body}
                </Tab>
              );
            })}
          </Tabs>
        </div>
        <PopupBox
          show={this.state.showAssignCandidatePopup || this.state.showAssignedCandidatePopup}
          title={
            this.state.showAssignCandidatePopup
              ? LOCALIZE.scorer.ola.testsToAssess.assignOrAssignedCandidatePopup.assignTitle
              : LOCALIZE.scorer.ola.testsToAssess.assignOrAssignedCandidatePopup.assignedTitle
          }
          handleClose={() => {}}
          displayCloseButton={this.state.showAssignCandidatePopup && !this.props.isOlaSupervisor}
          closeButtonAction={
            this.state.showAssignCandidatePopup &&
            !this.props.isOlaSupervisor &&
            this.closeAssignCandidatePopup
          }
          description={
            <div>
              {this.state.showAssignCandidatePopup && (
                <p>
                  {LOCALIZE.scorer.ola.testsToAssess.assignOrAssignedCandidatePopup.description}
                </p>
              )}
              {!this.state.loadingPopup ? (
                <div style={styles.formContainer}>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-tests-to-assess-assign-candidate-popup-candidate-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.testsToAssess.assignOrAssignedCandidatePopup
                            .candidateLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-tests-to-assess-assign-candidate-popup-candidate-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={`${this.state.selectedOrChosenCandidateData.candidate_user_first_name} ${this.state.selectedOrChosenCandidateData.candidate_user_last_name} (${this.state.selectedOrChosenCandidateData.candidate_user_email})`}
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        id="scorer-ola-tests-to-assess-assign-candidate-popup-ola-phone-number-label"
                        htmlFor="scorer-ola-tests-to-assess-assign-candidate-popup-ola-phone-number-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.myAssignedTests.assignedCandidatePopup
                            .olaPhoneNumberLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <InputMask
                        id="scorer-ola-tests-to-assess-assign-candidate-popup-ola-phone-number-input"
                        className={"valid-field input-disabled-look"}
                        aria-labelledby="scorer-ola-tests-to-assess-assign-candidate-popup-ola-phone-number-label"
                        aria-required={true}
                        style={{ ...styles.inputs, ...accommodationsStyle }}
                        type="text"
                        value={this.state.selectedOrChosenCandidateData.candidate_phone_number}
                        mask={MASKS.phoneNumber}
                        formatChars={MASK_FORMAT_CHARS.phoneNumber}
                        onChange={() => {}}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-tests-to-assess-assign-candidate-popup-candidate-pri-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.testsToAssess.assignOrAssignedCandidatePopup
                            .candidatePriLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-tests-to-assess-assign-candidate-popup-candidate-pri-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={this.state.selectedOrChosenCandidateData.candidate_pri}
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-tests-to-assess-assign-candidate-popup-reference-number-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.testsToAssess.assignOrAssignedCandidatePopup
                            .referenceNumberLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-tests-to-assess-assign-candidate-popup-reference-number-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={
                          this.state.selectedOrChosenCandidateData
                            .assessment_process_reference_number
                        }
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-tests-to-assess-assign-candidate-popup-requesting-department-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.testsToAssess.assignOrAssignedCandidatePopup
                            .requestingDepartmentLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-tests-to-assess-assign-candidate-popup-requesting-department-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={
                          this.props.currentLanguage === LANGUAGES.english
                            ? `${this.state.selectedOrChosenCandidateData.assessment_process_dept_edesc} (${this.state.selectedOrChosenCandidateData.assessment_process_dept_eabrv})`
                            : `${this.state.selectedOrChosenCandidateData.assessment_process_dept_fdesc} (${this.state.selectedOrChosenCandidateData.assessment_process_dept_fabrv})`
                        }
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-tests-to-assess-assign-candidate-popup-hr-contact-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.testsToAssess.assignOrAssignedCandidatePopup
                            .hrContactLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-tests-to-assess-assign-candidate-popup-hr-contact-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={`${this.state.selectedOrChosenCandidateData.hr_first_name} ${this.state.selectedOrChosenCandidateData.hr_last_name} (${this.state.selectedOrChosenCandidateData.hr_email})`}
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-tests-to-assess-assign-candidate-popup-test-to-administer-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.testsToAssess.assignOrAssignedCandidatePopup
                            .testToAdministerLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-tests-to-assess-assign-candidate-popup-test-to-administer-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={LOCALIZE.formatString(
                          LOCALIZE.scorer.ola.testsToAssess.assignOrAssignedCandidatePopup
                            .olaFieldInput,
                          this.state.selectedOrChosenCandidateData.language_id ===
                            LANGUAGE_IDS.english
                            ? LOCALIZE.commons.english
                            : LOCALIZE.commons.french
                        )}
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-tests-to-assess-assign-candidate-popup-language-of-test-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.testsToAssess.assignOrAssignedCandidatePopup
                            .languageOfTestLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-tests-to-assess-assign-candidate-popup-language-of-test-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={
                          this.state.selectedOrChosenCandidateData.language_id ===
                          LANGUAGE_IDS.english
                            ? LOCALIZE.commons.english
                            : LOCALIZE.commons.french
                        }
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-tests-to-assess-assign-candidate-popup-reason-for-testing-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.testsToAssess.assignOrAssignedCandidatePopup
                            .reasonForTestingLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-tests-to-assess-assign-candidate-popup-reason-for-testing-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={
                          this.state.selectedOrChosenCandidateData[
                            `reason_for_testing_name_${this.props.currentLanguage}`
                          ]
                        }
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-tests-to-assess-assign-candidate-popup-level-required-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.testsToAssess.assignOrAssignedCandidatePopup
                            .levelRequiredLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-tests-to-assess-assign-candidate-popup-level-required-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={this.state.selectedOrChosenCandidateData.level_required}
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                </div>
              ) : (
                <div style={styles.loadingContainer}>
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} style={styles.loadingIcon} />
                  </label>
                </div>
              )}
            </div>
          }
          leftButtonType={
            this.state.showAssignCandidatePopup ? BUTTON_TYPE.secondary : BUTTON_TYPE.none
          }
          leftButtonTitle={
            !this.props.isOlaSupervisor
              ? LOCALIZE.scorer.ola.testsToAssess.assignOrAssignedCandidatePopup.leftButton
              : LOCALIZE.commons.cancel
          }
          leftButtonIcon={
            this.state.showAssignCandidatePopup && !this.props.isOlaSupervisor
              ? faForwardStep
              : this.state.showAssignCandidatePopup && this.props.isOlaSupervisor
                ? faTimes
                : ""
          }
          leftButtonAction={
            this.state.showAssignCandidatePopup && this.props.isOlaSupervisor
              ? this.closeAssignCandidatePopup
              : () => {}
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            this.state.showAssignCandidatePopup
              ? LOCALIZE.scorer.ola.testsToAssess.assignOrAssignedCandidatePopup.rightButton
              : LOCALIZE.commons.close
          }
          rightButtonIcon={this.state.showAssignCandidatePopup ? faHandshake : faTimes}
          rightButtonAction={
            this.state.showAssignCandidatePopup
              ? this.handleAssignCandidateAction
              : this.closeAssignedCandidatePopup
          }
          rightButtonState={this.state.loadingPopup ? BUTTON_STATE.disabled : BUTTON_STATE.enabled}
        />
        <PopupBox
          show={this.state.showAlreadyPairedPopup}
          title={LOCALIZE.scorer.ola.testsToAssess.alreadyPairedPopup.title}
          handleClose={() => {}}
          size="lg"
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.scorer.ola.testsToAssess.alreadyPairedPopup.systemMessageDescription}
                  </p>
                }
              />
              <p>{LOCALIZE.scorer.ola.testsToAssess.alreadyPairedPopup.description}</p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonIcon={faTimes}
          rightButtonAction={this.closeAlreadyPairedPopup}
        />
        <PopupBox
          show={this.state.showConflictingSchedulePopup}
          title={LOCALIZE.scorer.ola.testsToAssess.conflictingSchedulePopup.title}
          handleClose={() => {}}
          size="lg"
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.scorer.ola.testsToAssess.conflictingSchedulePopup
                        .systemMessageDescription
                    }
                  </p>
                }
              />
              <p>{LOCALIZE.scorer.ola.testsToAssess.conflictingSchedulePopup.description}</p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonIcon={faTimes}
          rightButtonAction={this.closeConflictingSchedulePopup}
        />
        <PopupBox
          show={this.state.showUnassignCandidateConfirmationPopup}
          title={LOCALIZE.scorer.ola.testsToAssess.unassignCandidateConfirmationPopup.title}
          handleClose={() => {}}
          size="lg"
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.scorer.ola.testsToAssess.unassignCandidateConfirmationPopup
                        .systemMessageDescription
                    }
                  </p>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeUnassignCandidateConfirmationPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={
            LOCALIZE.scorer.ola.testsToAssess.unassignCandidateConfirmationPopup.rightButton
          }
          rightButtonIcon={faHandshakeSlash}
          rightButtonAction={this.handleUnassignCandidate}
        />
        <PopupBox
          show={this.state.showAssignTestAssessorPopup}
          title={LOCALIZE.scorer.ola.testsToAssess.assignTestAssessorPopup.title}
          handleClose={() => {}}
          size="lg"
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.scorer.ola.testsToAssess.assignTestAssessorPopup
                        .systemMessageDescription
                    }
                  </p>
                }
              />
              <div style={styles.formContainer}>
                <Row className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                  >
                    <label
                      id="scorer-ola-tests-to-assess-assign-test-assessor-popup-test-assessor-label"
                      style={styles.boldText}
                    >
                      {LOCALIZE.scorer.ola.testsToAssess.assignTestAssessorPopup.testAssessorLabel}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    {!this.state.loadingTestAssessorOptions ? (
                      <DropdownSelect
                        idPrefix="scorer-ola-tests-to-assess-assign-test-assessor-popup-test-assessor"
                        ariaLabelledBy="scorer-ola-tests-to-assess-assign-test-assessor-popup-test-assessor-label"
                        ariaRequired={true}
                        options={this.state.testAssessorOptions}
                        onChange={this.getSelectedTestAssessorOption}
                        defaultValue={this.state.testAssessorSelectedOption}
                        hasPlaceholder={true}
                      />
                    ) : (
                      <div style={styles.dropdownLoadingContainer}>
                        <label className="fa fa-spinner fa-spin">
                          <FontAwesomeIcon icon={faSpinner} style={styles.dropdownLoadingIcon} />
                        </label>
                      </div>
                    )}
                  </Col>
                </Row>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeAssignTestAssessorPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.scorer.ola.testsToAssess.assignTestAssessorPopup.rightButton}
          rightButtonIcon={faHandshake}
          rightButtonAction={this.handleAssignTestAssessor}
          rightButtonState={
            this.state.loadingTestAssessorOptions
              ? BUTTON_STATE.disabled
              : Object.keys(this.state.testAssessorSelectedOption).length > 0
                ? BUTTON_STATE.enabled
                : BUTTON_STATE.disabled
          }
        />
        <PopupBox
          show={this.state.showCancelTestSessionConfirmationPopup}
          title={LOCALIZE.scorer.ola.testsToAssess.cancelTestSessionConfirmationPopup.title}
          handleClose={() => {}}
          size="lg"
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.scorer.ola.testsToAssess.cancelTestSessionConfirmationPopup
                        .systemMessageDescription,
                      <span
                        style={styles.boldText}
                      >{`${this.state.selectedOrChosenCandidateData.candidate_user_first_name} ${this.state.selectedOrChosenCandidateData.candidate_user_last_name}`}</span>
                    )}
                  </p>
                }
              />
              <p>
                {LOCALIZE.scorer.ola.testsToAssess.cancelTestSessionConfirmationPopup.description}
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeCancelTestSessionConfirmationPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={
            this.state.cancelingLoading
              ? LOCALIZE.scorer.ola.testsToAssess.cancelTestSessionConfirmationPopup
                  .rightButtonLoading
              : LOCALIZE.scorer.ola.testsToAssess.cancelTestSessionConfirmationPopup.rightButton
          }
          rightButtonIcon={faCircleXmark}
          rightButtonAction={this.state.cancelingLoading ? {} : this.handleCancelTestSession}
          rightButtonState={
            this.state.cancelingLoading ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    currentLanguage: state.localize.language,
    user_id: state.user.user_id,
    triggerRerenderScorerTables: state.scorer.triggerRerenderScorerTables
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getScorerOlaTestsToAssessAsSupervisor,
      assignCandidateAsScorerOla,
      triggerRerenderScorerOlaTables,
      getScorerOlaAssignedCandidateDetails,
      unassignCandidateAsScorerOla,
      getAvailableTestAssessorsAsSupervisor,
      cancelTestSessionAsScorerOla,
      cleanUpVirtualMeeting,
      addAttendee,
      removeAttendee
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ScorerOlATestsToAssess);
