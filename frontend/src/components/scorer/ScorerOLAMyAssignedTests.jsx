/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { Col, Row, Tabs, Tab } from "react-bootstrap";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";

import {
  getScorerOlaMyAssignedTests,
  getScorerOlaAssignedCandidateDetails,
  unassignCandidateAsScorerOla,
  triggerRerenderScorerOlaTables
} from "../../modules/ScorerRedux";
import { removeAttendee } from "../../modules/AssignedTestsRedux";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";
import { faSpinner, faTimes, faVideo, faUniversalAccess } from "@fortawesome/free-solid-svg-icons";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from "moment";
import { convertDateTimeToUsersTimezone } from "../../modules/helpers";
import { LANGUAGES, LANGUAGE_IDS } from "../../modules/LocalizeRedux";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import AccommodationsRequestDetailsPopup, {
  ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE
} from "../aae/AccommodationsRequestDetailsPopup";
import { getSelectedUserAccommodationFileData } from "../../modules/AaeRedux";
import { MASKS, MASK_FORMAT_CHARS } from "../commons/InputMask/helpers";
import InputMask from "react-input-mask";
import "../../css/scorer_ola.css";

// setting needed date global variables
const CURRENT_DATE = moment(new Date(Date.now())).format("YYYY-MM-DD");
const IN_ONE_DAY_DATE = moment(new Date(Date.now())).add(1, "days").format("YYYY-MM-DD");
const IN_TWO_DAYS_DATE = moment(new Date(Date.now())).add(2, "days").format("YYYY-MM-DD");
const IN_THREE_DAYS_DATE = moment(new Date(Date.now())).add(3, "days").format("YYYY-MM-DD");
const IN_FOUR_DAYS_DATE = moment(new Date(Date.now())).add(4, "days").format("YYYY-MM-DD");
const IN_FIVE_DAYS_DATE = moment(new Date(Date.now())).add(5, "days").format("YYYY-MM-DD");
const IN_SIX_DAYS_DATE = moment(new Date(Date.now())).add(6, "days").format("YYYY-MM-DD");

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  mainContainer: {
    margin: "24px 36px"
  },
  allUnset: {
    all: "unset"
  },
  actionIcon: {
    marginRight: 5,
    transform: "scale(1.3)"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  formContainer: {
    width: "90%",
    margin: "18px auto"
  },
  loadingContainer: {
    textAlign: "center",
    margin: "48px auto"
  },
  loadingIcon: {
    transform: "scale(2)"
  },
  rowContainer: {
    padding: "6px 0"
  },
  boldText: {
    fontWeight: "bold"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  }
};

class ScorerOLAMyAssignedTests extends Component {
  state = {
    currentlyLoading: false,
    myAssignedTestsMainData: [],
    // disabling all tabs by default
    disabledTabs: [
      CURRENT_DATE,
      IN_ONE_DAY_DATE,
      IN_TWO_DAYS_DATE,
      IN_THREE_DAYS_DATE,
      IN_FOUR_DAYS_DATE,
      IN_FIVE_DAYS_DATE,
      IN_SIX_DAYS_DATE
    ],
    rowsDefinition: {},
    showAssignedCandidatePopup: false,
    loadingPopup: false,
    assignedCandidateData: {},
    selectedAccommodationRequestData: {},
    showAccommodationsRequestDetailsPopup: false
  };

  componentDidMount = () => {
    // populating table
    this.populateMyAssignedTests();
  };

  componentDidUpdate = prevProps => {
    // if triggerRerenderScorerTables gets updated
    if (prevProps.triggerRerenderScorerTables !== this.props.triggerRerenderScorerTables) {
      // populating table
      this.populateMyAssignedTests();
    }
  };

  populateMyAssignedTests = () => {
    this.setState({ currentlyLoading: true }, () => {
      this.props.getScorerOlaMyAssignedTests(CURRENT_DATE, IN_SIX_DAYS_DATE).then(response => {
        // setting needed states
        this.setState({
          myAssignedTestsMainData: response,
          currentlyLoading: false,
          disabledTabs: [
            CURRENT_DATE,
            IN_ONE_DAY_DATE,
            IN_TWO_DAYS_DATE,
            IN_THREE_DAYS_DATE,
            IN_FOUR_DAYS_DATE,
            IN_FIVE_DAYS_DATE,
            IN_SIX_DAYS_DATE
          ]
        });
      });
    });
  };

  generateTabContent = providedDate => {
    // setting columnsDefinition
    const columnsDefinition = [
      {
        label: LOCALIZE.scorer.ola.myAssignedTests.table.column1,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.scorer.ola.myAssignedTests.table.column2,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.scorer.ola.myAssignedTests.table.column3,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.scorer.ola.myAssignedTests.table.column4,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    // getting my assigned tests data for provided date
    const myAssignedTestsDataBasedOnProvidedDate = this.state.myAssignedTestsMainData.filter(
      obj => obj.date === providedDate
    );

    // if data to render
    if (myAssignedTestsDataBasedOnProvidedDate.length > 0) {
      // making sure that the provided date is no longer part of the disabledTabs state
      const copyOfDisabledTabs = [...this.state.disabledTabs];
      // initial array length
      const initialArrayLength = this.state.disabledTabs.length;
      const index = copyOfDisabledTabs.indexOf(providedDate);
      if (index > -1) {
        copyOfDisabledTabs.splice(index, 1);
      }
      // length of disabledTabs is different (updates have been made)
      if (copyOfDisabledTabs.length !== initialArrayLength) {
        this.setState({ disabledTabs: copyOfDisabledTabs });
      }
    }

    const rowsDefinition = this.populateScorerOlaMyAssignedTestsObject(
      myAssignedTestsDataBasedOnProvidedDate
    );

    return (
      <GenericTable
        classnamePrefix={`scorer-ola-my-assigned-tests-${providedDate}`}
        columnsDefinition={columnsDefinition}
        rowsDefinition={rowsDefinition}
        emptyTableMessage={LOCALIZE.scorer.ola.myAssignedTests.table.noData}
        currentlyLoading={this.state.currentlyLoading}
      />
    );
  };

  populateScorerOlaMyAssignedTestsObject = providedData => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // looping in providedData given
    for (let i = 0; i < providedData.length; i++) {
      const currentResult = providedData[i];
      // getting current datetime
      const currentDatetime = moment(new Date()).format("YYYY-MM-DDTHH:mm:ssZ");
      // formatting test session date
      const formattedTestSessionTime = `${
        convertDateTimeToUsersTimezone(currentResult.start_time).adjustedTime
      } - ${convertDateTimeToUsersTimezone(currentResult.end_time).adjustedTime}`;
      // if test session has not past (need that condition specifically for accommodation requests)
      if (currentDatetime < currentResult.end_time) {
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          date: currentResult.date,
          start_time: currentResult.start_time,
          end_time: currentResult.end_time,
          nbr_of_sessions: currentResult.nbr_of_sessions,
          column_1: formattedTestSessionTime,
          column_2:
            currentResult.language_id === LANGUAGE_IDS.english
              ? LOCALIZE.commons.english
              : LOCALIZE.commons.french,
          column_3: `${currentResult.candidate_user_first_name} ${currentResult.candidate_user_last_name} (${currentResult.candidate_user_email})`,
          column_4: (
            <>
              {/* non-standard test session (accommodation request) */}
              {!currentResult.is_standard && (
                <StyledTooltip
                  id={`my-tests-today-view-accommodation-request-details-${i}`}
                  place="top"
                  variant={TYPE.light}
                  effect={EFFECT.solid}
                  openOnClick={false}
                  tooltipElement={
                    <div style={styles.allUnset}>
                      <CustomButton
                        dataTip=""
                        dataFor={`my-tests-today-view-accommodation-request-details-${i}`}
                        label={
                          <>
                            <FontAwesomeIcon icon={faUniversalAccess} style={styles.actionIcon} />
                          </>
                        }
                        action={() =>
                          this.openAccommodationsRequestDetailsPopup(
                            currentResult.user_accommodation_file_id
                          )
                        }
                        customStyle={styles.actionButton}
                        buttonTheme={THEME.PRIMARY}
                        ariaLabel={LOCALIZE.formatString(
                          LOCALIZE.scorer.ola.myAssignedTests.table
                            .viewDetailsButtonTooltipAccessibility,
                          `${currentResult.candidate_user_first_name} ${currentResult.candidate_user_last_name}`
                        )}
                      />
                    </div>
                  }
                  tooltipContent={
                    <div>
                      <p>{LOCALIZE.scorer.ola.myAssignedTests.table.viewDetailsButtonTooltip}</p>
                    </div>
                  }
                />
              )}
              <StyledTooltip
                id={`my-tests-today-view-details-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`my-tests-today-view-details-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faQuestionCircle} style={styles.actionIcon} />
                        </>
                      }
                      action={() => this.handleOpenAssignedCandidatePopup(currentResult)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.scorer.ola.myAssignedTests.table
                          .viewDetailsButtonTooltipAccessibility,
                        `${currentResult.candidate_user_first_name} ${currentResult.candidate_user_last_name}`
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>{LOCALIZE.scorer.ola.myAssignedTests.table.viewDetailsButtonTooltip}</p>
                  </div>
                }
              />
              <StyledTooltip
                id={`my-tests-today-join-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`my-tests-today-join-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faVideo} style={styles.actionIcon} />
                        </>
                      }
                      action={() => this.openVideoCall(currentResult)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.scorer.ola.myAssignedTests.table.joinVideoCallTooltipAccessibility,
                        `${currentResult.candidate_user_first_name} ${currentResult.candidate_user_last_name}`
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>{LOCALIZE.scorer.ola.myAssignedTests.table.joinVideoCallTooltip}</p>
                  </div>
                }
              />
            </>
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    return rowsDefinition;
  };

  openAccommodationsRequestDetailsPopup = userAccommodationFileId => {
    // getting respective user accommodation file data
    this.props.getSelectedUserAccommodationFileData(userAccommodationFileId).then(response => {
      // updating needed states
      this.setState({
        showAccommodationsRequestDetailsPopup: true,
        selectedAccommodationRequestData: response
      });
    });
  };

  closeAccommodationsRequestDetailsPopup = () => {
    this.setState({
      showAccommodationsRequestDetailsPopup: false,
      selectedAccommodationRequestData: {}
    });
  };

  handleOpenAssignedCandidatePopup = testSessionData => {
    this.setState({ loadingPopup: true }, () => {
      this.props.getScorerOlaAssignedCandidateDetails(testSessionData.id).then(response => {
        // open popup
        this.setState({
          showAssignedCandidatePopup: true,
          assignedCandidateData: response,
          loadingPopup: false
        });
      });
    });
  };

  closeAssignedCandidatePopup = () => {
    this.setState({ showAssignedCandidatePopup: false, assignedCandidateData: {} });
  };

  openVideoCall = testSessionData => {
    // Just don't click this if you don't have teams setup properly.
    // Otherwise it will be null and explode. You have been warned
    // open the video link in a new tab
    const newWindow = window.open(testSessionData.meeting_link, "_blank", "noopener,noreferrer");
    if (newWindow) {
      newWindow.opener = null;
    }
  };

  render() {
    const TABS = [
      {
        key: `${CURRENT_DATE}`,
        tabName: `${CURRENT_DATE}`,
        body: this.generateTabContent(CURRENT_DATE)
      },
      {
        key: `${IN_ONE_DAY_DATE}`,
        tabName: `${IN_ONE_DAY_DATE}`,
        body: this.generateTabContent(IN_ONE_DAY_DATE)
      },
      {
        key: `${IN_TWO_DAYS_DATE}`,
        tabName: `${IN_TWO_DAYS_DATE}`,
        body: this.generateTabContent(IN_TWO_DAYS_DATE)
      },
      {
        key: `${IN_THREE_DAYS_DATE}`,
        tabName: `${IN_THREE_DAYS_DATE}`,
        body: this.generateTabContent(IN_THREE_DAYS_DATE)
      },
      {
        key: `${IN_FOUR_DAYS_DATE}`,
        tabName: `${IN_FOUR_DAYS_DATE}`,
        body: this.generateTabContent(IN_FOUR_DAYS_DATE)
      },
      {
        key: `${IN_FIVE_DAYS_DATE}`,
        tabName: `${IN_FIVE_DAYS_DATE}`,
        body: this.generateTabContent(IN_FIVE_DAYS_DATE)
      },
      {
        key: 7,
        tabName: `${IN_SIX_DAYS_DATE}`,
        body: this.generateTabContent(IN_SIX_DAYS_DATE)
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <div>
          <Tabs
            defaultActiveKey={CURRENT_DATE}
            id="scorer-ola-my-assigned-tests-tabs"
            style={styles.tabNavigation}
            justify={true}
          >
            {TABS.map((tab, index) => {
              return (
                <Tab
                  key={index}
                  eventKey={tab.key}
                  title={tab.tabName}
                  style={styles.tabContainer}
                  defaultActiveKey={CURRENT_DATE}
                  disabled={this.state.disabledTabs.indexOf(tab.tabName) > -1}
                >
                  {tab.body}
                </Tab>
              );
            })}
          </Tabs>
        </div>
        <PopupBox
          show={this.state.showAssignedCandidatePopup}
          title={LOCALIZE.scorer.ola.myAssignedTests.assignedCandidatePopup.title}
          handleClose={() => {}}
          description={
            <div>
              {!this.state.loadingPopup ? (
                <div style={styles.formContainer}>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-my-tests-today-assigned-candidate-popup-candidate-input"
                        style={styles.boldText}
                      >
                        {LOCALIZE.scorer.ola.myAssignedTests.assignedCandidatePopup.candidateLabel}
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-my-tests-today-assigned-candidate-popup-candidate-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={`${this.state.assignedCandidateData.candidate_user_first_name} ${this.state.assignedCandidateData.candidate_user_last_name} (${this.state.assignedCandidateData.candidate_user_email})`}
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        id="scorer-ola-my-tests-today-assigned-candidate-popup-ola-phone-number-label"
                        htmlFor="scorer-ola-my-tests-today-assigned-candidate-popup-ola-phone-number-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.myAssignedTests.assignedCandidatePopup
                            .olaPhoneNumberLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <InputMask
                        id="scorer-ola-my-tests-today-assigned-candidate-popup-ola-phone-number-input"
                        className={"valid-field input-disabled-look"}
                        aria-labelledby="scorer-ola-my-tests-today-assigned-candidate-popup-ola-phone-number-label"
                        aria-required={true}
                        style={{ ...styles.inputs, ...accommodationsStyle }}
                        type="text"
                        value={this.state.assignedCandidateData.candidate_phone_number}
                        mask={MASKS.phoneNumber}
                        formatChars={MASK_FORMAT_CHARS.phoneNumber}
                        onChange={() => {}}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-my-tests-today-assigned-candidate-popup-candidate-pri-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.myAssignedTests.assignedCandidatePopup
                            .candidatePriLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-my-tests-today-assigned-candidate-popup-candidate-pri-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={this.state.assignedCandidateData.candidate_pri}
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-my-tests-today-assigned-candidate-popup-reference-number-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.myAssignedTests.assignedCandidatePopup
                            .referenceNumberLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-my-tests-today-assigned-candidate-popup-reference-number-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={this.state.assignedCandidateData.assessment_process_reference_number}
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-my-tests-today-assigned-candidate-popup-requesting-department-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.myAssignedTests.assignedCandidatePopup
                            .requestingDepartmentLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-my-tests-today-assigned-candidate-popup-requesting-department-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={
                          this.props.currentLanguage === LANGUAGES.english
                            ? `${this.state.assignedCandidateData.assessment_process_dept_edesc} (${this.state.assignedCandidateData.assessment_process_dept_eabrv})`
                            : `${this.state.assignedCandidateData.assessment_process_dept_fdesc} (${this.state.assignedCandidateData.assessment_process_dept_fabrv})`
                        }
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-my-tests-today-assigned-candidate-popup-hr-contact-input"
                        style={styles.boldText}
                      >
                        {LOCALIZE.scorer.ola.myAssignedTests.assignedCandidatePopup.hrContactLabel}
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-my-tests-today-assigned-candidate-popup-hr-contact-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={`${this.state.assignedCandidateData.hr_first_name} ${this.state.assignedCandidateData.hr_last_name} (${this.state.assignedCandidateData.hr_email})`}
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-my-tests-today-assigned-candidate-popup-test-to-administer-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.myAssignedTests.assignedCandidatePopup
                            .testToAdministerLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-my-tests-today-assigned-candidate-popup-test-to-administer-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={LOCALIZE.formatString(
                          LOCALIZE.scorer.ola.myAssignedTests.assignedCandidatePopup.olaFieldInput,
                          this.state.assignedCandidateData.language_id === LANGUAGE_IDS.english
                            ? LOCALIZE.commons.english
                            : LOCALIZE.commons.french
                        )}
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-my-tests-today-assigned-candidate-popup-language-of-test-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.myAssignedTests.assignedCandidatePopup
                            .languageOfTestLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-my-tests-today-assigned-candidate-popup-language-of-test-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={
                          this.state.assignedCandidateData.language_id === LANGUAGE_IDS.english
                            ? LOCALIZE.commons.english
                            : LOCALIZE.commons.french
                        }
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-my-tests-today-assigned-candidate-popup-reason-for-testing-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.myAssignedTests.assignedCandidatePopup
                            .reasonForTestingLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-my-tests-today-assigned-candidate-popup-reason-for-testing-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={
                          this.state.assignedCandidateData[
                            `reason_for_testing_name_${this.props.currentLanguage}`
                          ]
                        }
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="scorer-ola-my-tests-today-assigned-candidate-popup-level-required-input"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.myAssignedTests.assignedCandidatePopup
                            .levelRequiredLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="scorer-ola-my-tests-today-assigned-candidate-popup-level-required-input"
                        tabIndex={0}
                        className={"valid-field input-disabled-look"}
                        type="text"
                        value={this.state.assignedCandidateData.level_required}
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                        aria-disabled={true}
                      />
                    </Col>
                  </Row>
                </div>
              ) : (
                <div style={styles.loadingContainer}>
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} style={styles.loadingIcon} />
                  </label>
                </div>
              )}
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonIcon={faTimes}
          rightButtonAction={this.closeAssignedCandidatePopup}
        />
        {Object.keys(this.state.selectedAccommodationRequestData).length > 0 &&
          this.state.showAccommodationsRequestDetailsPopup && (
            <AccommodationsRequestDetailsPopup
              source={ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.scorerOla}
              userAccommodationFileId={this.state.selectedAccommodationRequestData.id}
              selectedUserAccommodationFileData={this.state.selectedAccommodationRequestData}
              showPopup={this.state.showAccommodationsRequestDetailsPopup}
              closeAccommodationsRequestDetailsPopup={this.closeAccommodationsRequestDetailsPopup}
              // at this point, the report should be completed, so no draft view for this one
              isDraft={false}
            />
          )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    currentLanguage: state.localize.language,
    user_id: state.user.user_id,
    triggerRerenderScorerTables: state.scorer.triggerRerenderScorerTables
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getScorerOlaMyAssignedTests,
      getScorerOlaAssignedCandidateDetails,
      unassignCandidateAsScorerOla,
      triggerRerenderScorerOlaTables,
      removeAttendee,
      getSelectedUserAccommodationFileData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ScorerOLAMyAssignedTests);
