import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import TestFooter from "../testFactory/TestFooter";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { TestSectionScoringTypeObject } from "../testFactory/Constants";
import ScorerSidebar from "./ScorerSidebar";

const localStyles = {
  paddingTop5: { paddingTop: "5px" }
};

class ScorerFactory extends Component {
  state = {
    popupVisible: false
  };

  factory = () => {
    switch (this.props.scorerType) {
      case TestSectionScoringTypeObject.COMPETENCY.value:
        return (
          <>
            <ScorerSidebar />

            <div id="unit-test-scorer-popup-button">
              <TestFooter
                submitTest={() => {
                  this.setState({ popupVisible: !this.state.popupVisible });
                }}
                timeout={this.props.timeout}
                timeLimit={this.props.timeLimit}
                startTime={this.props.testSectionStartTime}
                buttonText={LOCALIZE.scorer.submitButton}
              />
              <PopupBox
                show={this.state.popupVisible}
                handleClose={() => {
                  this.setState({ popupVisible: !this.state.popupVisible, proceedCheckbox: false });
                }}
                title={LOCALIZE.scorer.submitButton}
                description={this.competencyPopupDescription()}
                leftButtonType={BUTTON_TYPE.secondary}
                leftButtonTitle={LOCALIZE.commons.cancel}
                rightButtonState={!this.state.proceedCheckbox}
                rightButtonType={BUTTON_TYPE.primary}
                rightButtonTitle={LOCALIZE.scorer.submitButton}
                rightButtonAction={this.props.handleNext}
              />
            </div>
          </>
        );
      default:
        return (
          <div id="unit-test-unsupported-scorer-type">
            <h1>
              {LOCALIZE.formatString(LOCALIZE.scorer.unsupportedScoringType, this.props.scorerType)}
            </h1>
          </div>
        );
    }
  };

  competencyPopupDescription = () => {
    return (
      <>
        <div id="unit-test-popup-description">{LOCALIZE.scorer.submitButton}</div>
        <div
          id="unit-test-popup-confirm-procced"
          className="custom-control custom-checkbox"
          style={localStyles.paddingTop5}
        >
          <input
            type="checkbox"
            className="custom-control-input"
            id={"procced-to-next-section-checkbox"}
            value={this.state.proceedCheckbox}
            onChange={() => {
              this.setState({ proceedCheckbox: !this.state.proceedCheckbox });
            }}
          />
          <label className="custom-control-label" htmlFor={"procced-to-next-section-checkbox"}>
            {LOCALIZE.commons.confirmStartTest.confirmProceed}
          </label>
        </div>
      </>
    );
  };

  render() {
    return <>{this.factory()}</>;
  }
}

export default ScorerFactory;
