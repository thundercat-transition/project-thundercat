/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { Tabs, Tab } from "react-bootstrap";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";

import { getScorerOlaMyAvailabilityData } from "../../modules/ScorerRedux";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { convertTimeToUsersTimezone } from "../../modules/helpers";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { DAY_OF_WEEK_ID_CONST } from "../commons/Constants";
import "../../css/scorer_ola.css";

const styles = {
  mainContainer: {
    margin: "24px 36px"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  },
  availableIcon: {
    transform: "scale(1.3)",
    color: "green"
  }
};

class ScorerOLAMyAvailability extends Component {
  state = {
    currentlyLoading: false,
    scorerOlaMyAvailabilityData: [],
    // disabling all tabs by default
    disabledTabs: [1, 2, 3, 4, 5, 6, 7]
  };

  componentDidMount = () => {
    // populating scorer my availability data
    this.populateScorerOlaMyAvailabilityData();
  };

  populateScorerOlaMyAvailabilityData = () => {
    // setting loading to true
    this.setState({ currentlyLoading: true }, () => {
      // getting scorer ola my availability data
      this.props.getScorerOlaMyAvailabilityData().then(response => {
        // setting states
        this.setState({ scorerOlaMyAvailabilityData: response, currentlyLoading: false });
      });
    });
  };

  generateTabContent = dayOfWeekId => {
    // setting columnsDefinition
    const columnsDefinition = [
      {
        label: LOCALIZE.scorer.ola.myAvailability.table.column1,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.scorer.ola.myAvailability.table.column2,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.scorer.ola.myAvailability.table.column3,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.scorer.ola.myAvailability.table.column4,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    // initializing needed variables
    let rowsDefinition = [];
    const data = [];

    // getting scorer OLA my availability data for provided day of week ID
    const availabilityDataForProvidedDayOfWeekId = this.state.scorerOlaMyAvailabilityData.filter(
      obj => obj.day_of_week_id === dayOfWeekId
    );

    // looping in availabilityDataForProvidedDayOfWeekId
    for (let i = 0; i < availabilityDataForProvidedDayOfWeekId.length; i++) {
      // adjusting UTC start/end time to user's timezone
      const adjustedStartTime = convertTimeToUsersTimezone(
        availabilityDataForProvidedDayOfWeekId[i].start_time
      );
      const adjustedEndTime = convertTimeToUsersTimezone(
        availabilityDataForProvidedDayOfWeekId[i].end_time
      );
      // pushing needed data in data array (needed for GenericTable component)
      data.push({
        column_1: `${adjustedStartTime} - ${adjustedEndTime}`,
        column_2:
          availabilityDataForProvidedDayOfWeekId[i][
            `assessed_language_text_${this.props.currentLanguage}`
          ],
        column_3: availabilityDataForProvidedDayOfWeekId[i].test_center_name,
        column_4: availabilityDataForProvidedDayOfWeekId[i].available && (
          <FontAwesomeIcon icon={faCheck} style={styles.availableIcon} />
        )
      });
    }

    // if data to render
    if (data.length > 0) {
      // making sure that the provided dayOfWeekId is no longer part of the disabledTabs state
      const copyOfDisabledTabs = [...this.state.disabledTabs];
      // initial array length
      const initialArrayLength = this.state.disabledTabs.length;
      const index = copyOfDisabledTabs.indexOf(dayOfWeekId);
      if (index > -1) {
        copyOfDisabledTabs.splice(index, 1);
      }
      // length of disabledTabs is different (updates have been made)
      if (copyOfDisabledTabs.length !== initialArrayLength) {
        this.setState({ disabledTabs: copyOfDisabledTabs });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    return (
      <GenericTable
        classnamePrefix={`scorer-ola-my-availability-day-of-week-${dayOfWeekId}`}
        columnsDefinition={columnsDefinition}
        rowsDefinition={rowsDefinition}
        emptyTableMessage={LOCALIZE.scorer.ola.myAvailability.table.noData}
        currentlyLoading={this.state.currentlyLoading}
      />
    );
  };

  render() {
    const TABS = [
      {
        key: DAY_OF_WEEK_ID_CONST.MONDAY,
        tabName: LOCALIZE.commons.daysOfWeek.monday,
        body: this.generateTabContent(DAY_OF_WEEK_ID_CONST.MONDAY)
      },
      {
        key: DAY_OF_WEEK_ID_CONST.TUESDAY,
        tabName: LOCALIZE.commons.daysOfWeek.tuesday,
        body: this.generateTabContent(DAY_OF_WEEK_ID_CONST.TUESDAY)
      },
      {
        key: DAY_OF_WEEK_ID_CONST.WEDNESDAY,
        tabName: LOCALIZE.commons.daysOfWeek.wednesday,
        body: this.generateTabContent(DAY_OF_WEEK_ID_CONST.WEDNESDAY)
      },
      {
        key: DAY_OF_WEEK_ID_CONST.THURSDAY,
        tabName: LOCALIZE.commons.daysOfWeek.thursday,
        body: this.generateTabContent(DAY_OF_WEEK_ID_CONST.THURSDAY)
      },
      {
        key: DAY_OF_WEEK_ID_CONST.FRIDAY,
        tabName: LOCALIZE.commons.daysOfWeek.friday,
        body: this.generateTabContent(DAY_OF_WEEK_ID_CONST.FRIDAY)
      },
      {
        key: DAY_OF_WEEK_ID_CONST.SATURDAY,
        tabName: LOCALIZE.commons.daysOfWeek.saturday,
        body: this.generateTabContent(DAY_OF_WEEK_ID_CONST.SATURDAY)
      },
      {
        key: DAY_OF_WEEK_ID_CONST.SUNDAY,
        tabName: LOCALIZE.commons.daysOfWeek.sunday,
        body: this.generateTabContent(DAY_OF_WEEK_ID_CONST.SUNDAY)
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <Tabs
          defaultActiveKey={DAY_OF_WEEK_ID_CONST.MONDAY}
          id="scorer-ola-my-availability-days-of-week-tabs"
          style={styles.tabNavigation}
          justify={true}
        >
          {TABS.map(tab => {
            return (
              <Tab
                key={tab.key}
                eventKey={tab.key}
                title={tab.tabName}
                style={styles.tabContainer}
                defaultActiveKey={DAY_OF_WEEK_ID_CONST.MONDAY}
                disabled={this.state.disabledTabs.indexOf(tab.key) > -1}
              >
                {tab.body}
              </Tab>
            );
          })}
        </Tabs>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    currentLanguage: state.localize.language,
    user_id: state.user.user_id,
    triggerRerenderScorerTables: state.scorer.triggerRerenderScorerTables
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getScorerOlaMyAvailabilityData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ScorerOLAMyAvailability);
