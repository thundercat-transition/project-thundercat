import React, { Component } from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { isTokenStillValid } from "../../modules/LoginRedux";
import { bindActionCreators } from "redux";
import ContentContainer from "../commons/ContentContainer";
import SideNavigation from "../eMIB/SideNavigation";
import ScorerOLATables from "./ScorerOLATables";

export const styles = {
  sectionContainerLabelDiv: {
    paddingLeft: 18,
    marginBottom: "-3px"
  },
  sectionContainerLabel: {
    padding: "4px 24px",
    marginBottom: 0,
    position: "relative"
  },
  tabStyleBorder: {
    borderBottom: "5px solid #00565e",
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    borderRadius: "4px 4px 0 0"
  },
  sectionContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    minHeight: 400,
    padding: "12px 0",
    marginBottom: 24
  },
  tabContainer: {
    zIndex: 1,
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent"
  },
  nav: {
    marginTop: 10,
    marginLeft: 10,
    width: 206,
    textAlign: "center"
  },
  body: {
    paddingLeft: 12
  },
  sideNavBodyContent: {
    marginTop: "-12px"
  },
  appPadding: {
    padding: "15px"
  }
};

class ScorerTable extends Component {
  getScorerSections = () => {
    return [
      {
        menuString: LOCALIZE.scorer.ola.tabName,
        body: <ScorerOLATables />
      }
    ];
  };

  render() {
    const specs = this.getScorerSections();

    return (
      <div>
        <div className="app" style={styles.appPadding}>
          <Helmet>
            <html lang={this.props.currentLanguage} />
            <title className="notranslate">{LOCALIZE.titles.scorer}</title>
          </Helmet>
          <ContentContainer>
            <div id="main-content" role="main">
              <div>
                <div style={styles.sectionContainerLabelDiv}>
                  <div>
                    <label style={styles.sectionContainerLabel} className="notranslate">
                      {LOCALIZE.scorer.ola.pageTitle}
                      <span style={styles.tabStyleBorder}></span>
                    </label>
                  </div>
                </div>
                <div style={styles.sectionContainer}>
                  <section aria-label={LOCALIZE.ariaLabel.sideNavigationSection}>
                    <SideNavigation
                      specs={specs}
                      startIndex={0}
                      displayNextPreviousButton={false}
                      isMain={true}
                      tabContainerStyle={styles.tabContainer}
                      tabContentStyle={styles.tabContent}
                      navStyle={styles.nav}
                      bodyContentCustomStyle={styles.sideNavBodyContent}
                    />
                  </section>
                </div>
              </div>
            </div>
          </ContentContainer>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ScorerTable);
