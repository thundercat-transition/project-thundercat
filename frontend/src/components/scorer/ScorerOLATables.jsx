import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import ScorerOLAMyAssignedTests from "./ScorerOLAMyAssignedTests";
import ScorerOLATestsToAssess from "./ScorerOLATestsToAssess";
import ScorerOlaAssessorsUnavailability from "./ScorerOLAAssessorsUnavailability";
import { isUserAssociatedToTestCenter, setScorerOLASelectedTab } from "../../modules/ScorerRedux";
import ScorerOLAMyAvailability from "./ScorerOLAMyAvailability";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  }
};

class ScorerOlATables extends Component {
  state = {
    isOlaTestAssessor: false,
    isOlaSupervisor: false
  };

  // Returns true if user is linked to a test center
  findIfUserAssociatedToTestCenter = () => {
    this.props.isUserAssociatedToTestCenter().then(response => {
      this.setState({
        isOlaTestAssessor: response.is_ola_test_assessor,
        isOlaSupervisor: response.is_ola_supervisor
      });
    });
  };

  componentDidMount = () => {
    this.findIfUserAssociatedToTestCenter();
  };

  render() {
    const TABS = [
      {
        key: "my-tests-today",
        tabName: LOCALIZE.scorer.ola.myAssignedTests.tabName,
        body: <ScorerOLAMyAssignedTests />
      }
    ];
    // OLA Supervisor
    if (this.state.isOlaSupervisor) {
      TABS.push({
        key: "tests-to-assess",
        tabName: LOCALIZE.scorer.ola.testsToAssess.tabName,
        body: <ScorerOLATestsToAssess isOlaSupervisor={this.state.isOlaSupervisor} />
      });
    }
    TABS.push(
      {
        key: "my-availability",
        tabName: LOCALIZE.scorer.ola.myAvailability.tabName,
        body: <ScorerOLAMyAvailability />
      },
      {
        key: "assessors-unavailability",
        tabName: LOCALIZE.scorer.ola.assessorsUnavailability.tabName,
        body: <ScorerOlaAssessorsUnavailability isOlaSupervisor={this.state.isOlaSupervisor} />
      }
    );
    return (
      <div style={styles.mainContainer}>
        {this.state.isOlaTestAssessor ? (
          <Row>
            <Col>
              <Tabs
                defaultActiveKey={
                  this.props.scorerOLASelectedTab !== "" && this.props.scorerOLASelectedTab !== null
                    ? this.props.scorerOLASelectedTab
                    : "my-tests-today"
                }
                id="scorer-tabs"
                style={styles.tabNavigation}
                onSelect={event => this.props.setScorerOLASelectedTab(event)}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                      defaultActiveKey={"my-tests-today"}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        ) : (
          <div>{LOCALIZE.scorer.ola.notLinkedToTestCenterMsg}</div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    scorerOLASelectedTab: state.scorer.scorerOLASelectedTab
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ isUserAssociatedToTestCenter, setScorerOLASelectedTab }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ScorerOlATables);
