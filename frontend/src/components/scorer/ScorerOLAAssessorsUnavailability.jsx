import React, { Component } from "react";
import PropTypes from "prop-types";
import { Col, Row } from "react-bootstrap";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import SearchBarWithDisplayOptions from "../commons/SearchBarWithDisplayOptions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSpinner,
  faTimes,
  faPlusCircle,
  faSave,
  faPencilAlt,
  faTrashAlt
} from "@fortawesome/free-solid-svg-icons";
import {
  updateAssessorsOLAUnavailabilityPageState,
  updateAssessorsOLAUnavailabilityPageSizeState,
  updateAssessorsOLAUnavailabilityStates,
  getAllAssessorsOlaUnavailabilityAsSupervisor,
  getFoundAssessorsOlaUnavailabilityAsSupervisor,
  getAllOlaAssessorsSupervisedByCurrentUser,
  getAllOlaTestCentersForSelectedAssessor,
  saveNewAssessorUnavailability,
  modifyAssessorUnavailability,
  deleteAssessorUnavailability
} from "../../modules/ScorerRedux";
import Pagination from "../commons/Pagination";
import { LANGUAGES } from "../../modules/LocalizeRedux";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import moment from "moment";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../commons/PopupBox";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import DropdownSelect from "../commons/DropdownSelect";
import DatePicker, { validateDatePicked } from "../commons/DatePicker";
import { populateCustomLastAndFutureYearsDateOptions } from "../../helpers/populateCustomDatePickerOptions";
import { convertUsersTimezoneDateTimeToUtc } from "../../modules/helpers";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  mainContainer: {
    width: "100%"
  },
  allUnset: {
    all: "unset"
  },
  actionIcon: {
    marginRight: 5,
    transform: "scale(1.3)"
  },
  redActionIcon: {
    marginRight: 5,
    transform: "scale(1.3)",
    color: "red"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  formContainer: {
    width: "90%",
    margin: "18px auto"
  },
  loadingContainer: {
    textAlign: "center",
    margin: "48px auto"
  },
  loadingIcon: {
    transform: "scale(2)"
  },
  reasonForSkipLoadingContainer: {
    textAlign: "center"
  },
  reasonForSkipLoadingIcon: {
    transform: "scale(1.3)"
  },
  rowContainer: {
    padding: "6px 0"
  },
  paddingDeletePopup: {
    padding: "25px 0"
  },
  boldText: {
    fontWeight: "bold"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  dropdownLoadingContainer: {
    textAlign: "center"
  },
  dropdownLoadingIcon: {
    transform: "scale(1.3)"
  },
  createNewUnavailabilityLabel: {
    minWidth: 175
  },
  optionSelectionContainer: {
    padding: "12px 24px",
    margin: "0 auto",
    width: "80%"
  },
  labelContainer: {
    verticalAlign: "middle",
    paddingRight: 12
  },
  dateLabelsOnError: {
    color: "#923534",
    fontWeight: "bold"
  },
  dateError: {
    paddingTop: 0,
    color: "#923534",
    fontWeight: "bold"
  },
  buttonRowContainer: {
    margin: "18px 0px 12px",
    width: "100%"
  },
  span: {
    paddingLeft: 5,
    paddingRight: 5
  },
  reasonInput: {
    minHeight: 80,
    height: "100%",
    resize: "none",
    marginTop: 6,
    width: "100%",
    padding: "3px 6px 3px 6px"
  }
};

class AssessorsOLAUnavailability extends Component {
  static props = {
    isOlaSupervisor: PropTypes.bool.isRequired,
    isTestCenterManager: PropTypes.bool.isRequired
  };

  constructor(props) {
    super(props);
    // Start date params
    this.dateStartDayFieldRef = React.createRef();
    this.dateStartMonthFieldRef = React.createRef();
    this.dateStartYearFieldRef = React.createRef();
    this.timeStartHourFieldRef = React.createRef();
    this.timeStartMinFieldRef = React.createRef();

    // End date params
    this.dateEndDayFieldRef = React.createRef();
    this.dateEndMonthFieldRef = React.createRef();
    this.dateEndYearFieldRef = React.createRef();
    this.timeEndHourFieldRef = React.createRef();
    this.timeEndMinFieldRef = React.createRef();
  }

  state = {
    // Table states
    currentlyLoading: false,
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    resultsFound: 0,
    clearSearchTriggered: false,

    // Popup states
    showAddUnavailabilityPopup: false,
    showModifyUnavailabilityPopup: false,
    showDeleteUnavailabilityPopup: false,
    showErrorPopup: false,
    errorTitle: "",
    errorDescription: "",

    // Selected Assessor Unavailability Data
    selectedAssessorUnavailabilityData: "",

    // Loading Popup
    loadingPopup: false,

    // Add Assessor Unavailability Popup
    loadingAssessorsOptions: false,
    loadingTestCentersOptions: false,

    // Test Assessor Options - Dropdown
    listOfAssessorsOptions: [],
    loadingOlaAssessorOptions: false,

    // Test Centers Options - Dropdown
    listOfTestCentersOptions: [],
    loadingOlaTestCentersOptions: false,

    // Test Assessor Options
    olaAssessorSelectedOption: [],

    // Test Centers Options
    olaTestCentersSelectedOption: [],

    // Start / End Date
    displayDateError: false,
    endDateVisible: false,
    isValidStartDatePicked: true,
    isValidEndDatePicked: true,
    isEndDateEqualOrAfterTodayPicked: true,

    // Reason
    reasonForUnavailability: ""
  };

  componentDidMount = () => {
    this.props.updateAssessorsOLAUnavailabilityPageState(1);
    // populating table
    this.populateUnavailabilityBasedOnActiveSearch();
  };

  componentDidUpdate = (prevProps, prevState) => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateTestsToAssessBasedOnActiveSearch();
    }
    // if assessorsOLAUnavailabilityPaginationPage gets updated
    if (
      prevProps.assessorsOLAUnavailabilityPaginationPage !==
      this.props.assessorsOLAUnavailabilityPaginationPage
    ) {
      this.populateUnavailabilityBasedOnActiveSearch();
    }
    // if assessorsOLAUnavailabilityPaginationPageSize get updated
    if (
      prevProps.assessorsOLAUnavailabilityPaginationPageSize !==
      this.props.assessorsOLAUnavailabilityPaginationPageSize
    ) {
      this.populateUnavailabilityBasedOnActiveSearch();
    }
    // if search assessorsOLAUnavailabilityKeyword gets updated
    if (
      prevProps.assessorsOLAUnavailabilityKeyword !== this.props.assessorsOLAUnavailabilityKeyword
    ) {
      // if assessorsOLAUnavailabilityKeyword redux state is empty
      if (this.props.assessorsOLAUnavailabilityKeyword !== "") {
        this.populateFoundOlaAssessorsUnavailabilityTable();
      } else if (
        this.props.assessorsOLAUnavailabilityKeyword === "" &&
        this.props.assessorsOLAUnavailabilityActiveSearch
      ) {
        this.populateFoundOlaAssessorsUnavailabilityTable();
      }
    }
    // if assessorsOLAUnavailabilityActiveSearch gets updated
    if (
      prevProps.assessorsOLAUnavailabilityActiveSearch !==
      this.props.assessorsOLAUnavailabilityActiveSearch
    ) {
      this.populateUnavailabilityBasedOnActiveSearch();
    }
  };

  populateUnavailabilityBasedOnActiveSearch = () => {
    // current search
    if (this.props.assessorsOLAUnavailabilityActiveSearch) {
      this.populateFoundOlaAssessorsUnavailabilityTable();
      // no search
    } else {
      this.populateAllOlaAssessorsUnavailabilityTable();
    }
  };

  populateAllOlaAssessorsUnavailabilityTable = () => {
    this.setState({ currentlyLoading: true }, () => {
      this.props
        .getAllAssessorsOlaUnavailabilityAsSupervisor(
          this.props.assessorsOLAUnavailabilityPaginationPage,
          this.props.assessorsOLAUnavailabilityPaginationPageSize
        )
        .then(response => {
          // populating columns
          this.populateOlaUnavailabilityObject(response);
        });
    });
  };

  populateFoundOlaAssessorsUnavailabilityTable = () => {
    this.setState({ currentlyLoading: true }, () => {
      this.props
        .getFoundAssessorsOlaUnavailabilityAsSupervisor(
          this.props.assessorsOLAUnavailabilityKeyword,
          this.props.currentLanguage,
          this.props.assessorsOLAUnavailabilityPaginationPage,
          this.props.assessorsOLAUnavailabilityPaginationPageSize
        )
        .then(response => {
          // there are no results found
          if (response[0] === "no results found") {
            this.setState(
              {
                rowsDefinition: {},
                currentlyLoading: false,
                numberOfPages: 1,
                nextPageNumber: 0,
                previousPageNumber: 0,
                resultsFound: 0
              },
              () => {
                // focusing on results found
                if (document.getElementById("ola-assessor-unavailability-dates-results-found")) {
                  document
                    .getElementById("ola-assessor-unavailability-dates-results-found")
                    .focus();
                }
              }
            );
            // there is at least one result found
          } else {
            // populating columns
            this.populateOlaUnavailabilityObject(response);
            this.setState({ resultsFound: response.count }, () => {
              // focusing on results found
              if (document.getElementById("ola-assessor-unavailability-dates-results-found")) {
                document.getElementById("ola-assessor-unavailability-dates-results-found").focus();
              }
            });
          }
        });
    });
  };

  // Validate of fields before creating unavailability
  areAllFieldsValid = () => {
    // if we are in Add Unavailability Popup
    if (this.state.showAddUnavailabilityPopup) {
      return (
        // Selected Test Assessor Option
        this.state.olaAssessorSelectedOption.length > 0 &&
        // Selected Test Center(s) Option
        this.state.olaTestCentersSelectedOption.length > 0 &&
        this.state.isValidEndDatePicked &&
        this.state.isValidStartDatePicked &&
        !this.state.displayDateError &&
        this.state.isEndDateEqualOrAfterTodayPicked &&
        // Verify that the Start and End Dates are Valid
        this.dateStartYearFieldRef.current &&
        this.dateStartMonthFieldRef.current &&
        this.dateStartDayFieldRef.current &&
        this.timeStartHourFieldRef.current &&
        this.timeStartMinFieldRef.current &&
        this.dateEndYearFieldRef.current &&
        this.dateEndMonthFieldRef.current &&
        this.dateEndDayFieldRef.current &&
        this.timeEndHourFieldRef.current &&
        this.timeEndMinFieldRef.current &&
        this.dateStartYearFieldRef.current.props.value !== "" &&
        this.dateStartMonthFieldRef.current.props.value !== "" &&
        this.dateStartDayFieldRef.current.props.value !== "" &&
        this.timeStartHourFieldRef.current.props.value !== "" &&
        this.timeStartMinFieldRef.current.props.value !== "" &&
        this.dateEndYearFieldRef.current.props.value !== "" &&
        this.dateEndMonthFieldRef.current.props.value !== "" &&
        this.dateEndDayFieldRef.current.props.value !== "" &&
        this.timeEndHourFieldRef.current.props.value !== "" &&
        this.timeEndMinFieldRef.current.props.value !== ""
      );
    }
    // if we are in Modify Unavailability Popup
    if (this.state.showModifyUnavailabilityPopup) {
      const startDate = this.state.selectedAssessorUnavailabilityData.start_date.split("-");
      const endDate = this.state.selectedAssessorUnavailabilityData.end_date.split("-");

      // Verify if the date has been changed
      // Start Date
      const isStartDateDayChanged = this.dateStartDayFieldRef.current
        ? Number(startDate[2]) !== this.dateStartDayFieldRef.current.props.value.value
        : false;

      const isStartDateMonthChanged = this.dateStartMonthFieldRef.current
        ? Number(startDate[1]) !== this.dateStartMonthFieldRef.current.props.value.value
        : false;
      const isStartDateYearChanged = this.dateStartYearFieldRef.current
        ? Number(startDate[0]) !== this.dateStartYearFieldRef.current.props.value.value
        : false;

      // End Date
      const isEndDateDayChanged = this.dateEndDayFieldRef.current
        ? Number(endDate[2]) !== this.dateEndDayFieldRef.current.props.value.value
        : false;
      const isEndDateMonthChanged = this.dateEndMonthFieldRef.current
        ? Number(endDate[1]) !== this.dateEndMonthFieldRef.current.props.value.value
        : false;
      const isEndDateYearChanged = this.dateEndYearFieldRef.current
        ? Number(endDate[0]) !== this.dateEndYearFieldRef.current.props.value.value
        : false;

      // Reason
      const isReasonChanged =
        this.state.selectedAssessorUnavailabilityData.reason !== this.state.reasonForUnavailability;

      return (
        this.state.isValidEndDatePicked &&
        this.state.isValidStartDatePicked &&
        !this.state.displayDateError &&
        this.state.isEndDateEqualOrAfterTodayPicked &&
        (isStartDateDayChanged ||
          isStartDateMonthChanged ||
          isStartDateYearChanged ||
          isEndDateDayChanged ||
          isEndDateMonthChanged ||
          isEndDateYearChanged ||
          isReasonChanged)
      );
    }
    return false;
  };

  populateOlaUnavailabilityObject = response => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];

        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1:
            this.props.currentLanguage === LANGUAGES.english
              ? `${currentResult.test_center_name} (${currentResult.test_center_city_en})`
              : `${currentResult.test_center_name} (${currentResult.test_center_city_fr})`,
          column_2: `${currentResult.first_name} ${currentResult.last_name}`,
          column_3: `${currentResult.start_date.split("T")[0]} ${currentResult.start_date
            .split("T")[1]
            .substring(0, 5)}`,
          column_4: `${currentResult.end_date.split("T")[0]} ${currentResult.end_date
            .split("T")[1]
            .substring(0, 5)}`
        });

        if (this.props.isOlaSupervisor || this.props.isTestCenterManager) {
          data[i].column_5 = (
            <>
              <StyledTooltip
                id={`modify-assessor-unavailability-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                isFocusable={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`modify-assessor-unavailability-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faPencilAlt} style={styles.actionIcon} />
                        </>
                      }
                      action={() => this.handleOpenModifyAssessorUnavailabilityPopup(currentResult)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.scorer.ola.assessorsUnavailability.table
                          .modifyButtonTooltipAccessibility,
                        `${currentResult.first_name} ${currentResult.last_name}`,
                        `${currentResult.test_center_name}`,
                        `${currentResult.start_date.split("T")[0]} ${currentResult.start_date
                          .split("T")[1]
                          .substring(0, 5)}`,
                        `${currentResult.end_date.split("T")[0]} ${currentResult.end_date
                          .split("T")[1]
                          .substring(0, 5)}`
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>{LOCALIZE.scorer.ola.assessorsUnavailability.table.modifyButtonTooltip}</p>
                  </div>
                }
              />
              <StyledTooltip
                id={`delete-assessor-unavailability-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                isFocusable={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`delete-assessor-unavailability-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faTrashAlt} style={styles.actionIcon} />
                        </>
                      }
                      action={() => this.handleOpenDeleteAssessorUnavailabilityPopup(currentResult)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.scorer.ola.assessorsUnavailability.table
                          .deleteButtonTooltipAccessibility,
                        `${currentResult.first_name} ${currentResult.last_name}`,
                        `${currentResult.test_center_name}`,
                        `${currentResult.start_date.split("T")[0]} ${currentResult.start_date
                          .split("T")[1]
                          .substring(0, 5)}`,
                        `${currentResult.end_date.split("T")[0]} ${currentResult.end_date
                          .split("T")[1]
                          .substring(0, 5)}`
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>{LOCALIZE.scorer.ola.assessorsUnavailability.table.deleteButtonTooltip}</p>
                  </div>
                }
              />
            </>
          );
        }
      }
      // updating rowsDefinition object with provided data and needed style
      rowsDefinition = {
        column_1_style: COMMON_STYLE.LEFT_TEXT,
        column_2_style: COMMON_STYLE.CENTERED_TEXT,
        column_3_style: COMMON_STYLE.CENTERED_TEXT,
        column_4_style: COMMON_STYLE.CENTERED_TEXT,
        column_5_style: COMMON_STYLE.CENTERED_TEXT,
        data: data
      };

      // saving results in state
      this.setState({
        rowsDefinition: rowsDefinition,
        currentlyLoading: false,
        numberOfPages: Math.ceil(
          response.count / this.props.assessorsOLAUnavailabilityPaginationPageSize
        ),
        nextPageNumber: response.next_page_number,
        previousPageNumber: response.previous_page_number
      });
    }
  };

  // Open Add Assessor Unavailability Popup
  handleOpenAddAssessorUnavailabilityPopup = () => {
    this.setState({ loadingPopup: true }, () => {
      this.populateOlaTestAssessorOptions();
      this.setState({
        showAddUnavailabilityPopup: true,
        loadingPopup: false
      });
    });
  };

  // Action - Select an OLA Assessor in the dropdown
  getSelectedOlaAssessorOption = selectedOption => {
    this.setState(
      { olaAssessorSelectedOption: [selectedOption], olaTestCentersSelectedOption: [] },
      () => {
        this.populateOlaTestCentersOptions(selectedOption.value);
      }
    );
  };

  // Action - Select Test Centers in the dropdown
  getSelectedOlaTestCentersOption = selectedOption => {
    this.setState({ olaTestCentersSelectedOption: selectedOption });
  };

  // Action - Add Assessor Unavailability
  handleAddAssessorUnavailability = () => {
    // Create the converted DateTime
    const convertedStartDate = convertUsersTimezoneDateTimeToUtc(
      `${this.dateStartYearFieldRef.current.props.value.value}-${this.dateStartMonthFieldRef.current.props.value.value}-${this.dateStartDayFieldRef.current.props.value.value} ${this.timeStartHourFieldRef.current.props.value.value}:${this.timeStartMinFieldRef.current.props.value.value}`
    );

    const convertedEndDate = convertUsersTimezoneDateTimeToUtc(
      `${this.dateEndYearFieldRef.current.props.value.value}-${this.dateEndMonthFieldRef.current.props.value.value}-${this.dateEndDayFieldRef.current.props.value.value} ${this.timeEndHourFieldRef.current.props.value.value}:${this.timeEndMinFieldRef.current.props.value.value}`
    );

    const body = {
      assessor_id: this.state.olaAssessorSelectedOption[0].value,
      test_center_ids: this.state.olaTestCentersSelectedOption.map(option => option.value),
      start_date: `${convertedStartDate}`,
      end_date: `${convertedEndDate}`,
      reason: this.state.reasonForUnavailability
    };
    this.props.saveNewAssessorUnavailability(body).then(response => {
      if (response.ok) {
        this.setState(
          {
            showAddUnavailabilityPopup: false,
            olaAssessorSelectedOption: [],
            olaTestCentersSelectedOption: [],
            endDateVisible: false
          },
          () => {
            // triggering unavailability table refresh
            this.populateUnavailabilityBasedOnActiveSearch();
          }
        );
        // should never happen
      } else {
        // Show the error popup with the correct message
        this.setState({
          showErrorPopup: true,
          errorTitle: LOCALIZE.scorer.ola.assessorsUnavailability.errorPopup.addErrorTitle,
          errorDescription:
            LOCALIZE.scorer.ola.assessorsUnavailability.errorPopup.addErrorDescription
        });
        throw new Error(
          "Something went wrong during the creation of the assessor unavailability. Please try again."
        );
      }
    });
  };

  // Action - Handling date updates
  handleDateUpdates = () => {
    let dateStart = null;
    let dateEnd = null;
    let startDate = null;

    // verify if date start date has been fully provided
    if (
      this.dateStartDayFieldRef.current.props.value !== "" &&
      this.dateStartMonthFieldRef.current.props.value !== "" &&
      this.dateStartYearFieldRef.current.props.value !== "" &&
      this.timeStartHourFieldRef.current.props.value !== "" &&
      this.timeStartMinFieldRef.current.props.value !== ""
    ) {
      // create the date format
      dateStart = `${this.dateStartYearFieldRef.current.props.value.value}-${this.dateStartMonthFieldRef.current.props.value.value}-${this.dateStartDayFieldRef.current.props.value.value}`;

      // verify if the date is valid
      const isStartValidDate = validateDatePicked(dateStart);

      // if the date is valid, show the End Date field + set isValidStartDatePicked to true
      if (isStartValidDate) {
        startDate = new Date(
          this.dateStartYearFieldRef.current.props.value.value,
          this.dateStartMonthFieldRef.current.props.value.value - 1,
          this.dateStartDayFieldRef.current.props.value.value,
          this.timeStartHourFieldRef.current.props.value.value,
          this.timeStartMinFieldRef.current.props.value.value,
          0,
          0
        );
        // set the value of startDateState to the chosen date in format "YYYY-MM-DDTHH:mm:ss"
        dateStart = moment(startDate).format("YYYY-MM-DDTHH:mm:ss");

        this.setState({
          endDateVisible: true,
          isValidStartDatePicked: true
        });
      } else {
        this.setState({ isValidStartDatePicked: false });
      }

      // verify if Date To field is visible, meaning that the Date From date has been fully provided and is valid at least once
      if (this.state.endDateVisible) {
        // date to date has been fully provided
        if (
          this.dateEndDayFieldRef.current.props.value !== "" &&
          this.dateEndMonthFieldRef.current.props.value !== "" &&
          this.dateEndYearFieldRef.current.props.value !== "" &&
          this.timeEndHourFieldRef.current.props.value !== "" &&
          this.timeEndMinFieldRef.current.props.value !== ""
        ) {
          // create the date format
          dateEnd = `${this.dateEndYearFieldRef.current.props.value.value}-${this.dateEndMonthFieldRef.current.props.value.value}-${this.dateEndDayFieldRef.current.props.value.value}`;

          // verify if the date is valid
          const isEndValidDate = validateDatePicked(dateEnd);

          // if the date is valid, set isValidEndDatePicked to true
          if (isEndValidDate) {
            let endDate = new Date(
              this.dateEndYearFieldRef.current.props.value.value,
              this.dateEndMonthFieldRef.current.props.value.value - 1,
              this.dateEndDayFieldRef.current.props.value.value,
              this.timeEndHourFieldRef.current.props.value.value,
              this.timeEndMinFieldRef.current.props.value.value,
              0,
              0
            );

            // create a moment object with our date so we can compare
            endDate = moment(endDate);

            // if the end date is older than start date, we can go through
            if (endDate.isAfter(moment(startDate))) {
              // if the date is older than now, we can say that the date is valid
              if (endDate.isAfter(moment())) {
                // set the value of startDateState to the chosen date in format "YYYY-MM-DDTHH:mm:ss"
                endDate = moment(endDate).format("YYYY-MM-DDTHH:mm:ss");

                this.setState({
                  isValidEndDatePicked: true,
                  isEndDateEqualOrAfterTodayPicked: true,
                  displayDateError: false
                });
              } else {
                this.setState({ isEndDateEqualOrAfterTodayPicked: false, displayDateError: false });
              }
            } else {
              this.setState({ displayDateError: true });
            }
          }
          // if the date is not valid, set isValidEndDatePicked to false
          else {
            this.setState({ isValidEndDatePicked: false });
          }
        }
      }
    }
  };

  // Close Add Assessor Unavailability Popup
  closeAddAssessorUnavailabilityPopup = () => {
    this.setState({
      showAddUnavailabilityPopup: false,
      selectedAssessorUnavailabilityData: "",
      isValidStartDatePicked: true,
      isValidEndDatePicked: true,
      isEndDateEqualOrAfterTodayPicked: true,
      endDateVisible: false,
      displayDateError: false,
      reasonForUnavailability: "",
      olaAssessorSelectedOption: [],
      olaTestCentersSelectedOption: [],
      listOfTestCentersOptions: []
    });
  };

  // Open Modify Assessor Unavailability Popup
  handleOpenModifyAssessorUnavailabilityPopup = unavailabilityData => {
    this.setState({ loadingPopup: true }, () => {
      this.setState({
        showModifyUnavailabilityPopup: true,
        endDateVisible: true,
        selectedAssessorUnavailabilityData: unavailabilityData,
        reasonForUnavailability: unavailabilityData.reason,
        loadingPopup: false
      });
    });
  };

  // Action - Modify Assessor Unavailability
  handleModifyAssessorUnavailability = () => {
    // Create the converted DateTime
    const convertedStartDate = convertUsersTimezoneDateTimeToUtc(
      `${this.dateStartYearFieldRef.current.props.value.value}-${this.dateStartMonthFieldRef.current.props.value.value}-${this.dateStartDayFieldRef.current.props.value.value} ${this.timeStartHourFieldRef.current.props.value.value}:${this.timeStartMinFieldRef.current.props.value.value}`
    );

    const convertedEndDate = convertUsersTimezoneDateTimeToUtc(
      `${this.dateEndYearFieldRef.current.props.value.value}-${this.dateEndMonthFieldRef.current.props.value.value}-${this.dateEndDayFieldRef.current.props.value.value} ${this.timeEndHourFieldRef.current.props.value.value}:${this.timeEndMinFieldRef.current.props.value.value}`
    );

    const body = {
      test_center_ola_assessor_unavailability_id: this.state.selectedAssessorUnavailabilityData.id,
      start_date: `${convertedStartDate}`,
      end_date: `${convertedEndDate}`,
      reason: this.state.reasonForUnavailability
    };
    this.props.modifyAssessorUnavailability(body).then(response => {
      if (response.ok) {
        this.setState(
          {
            showModifyUnavailabilityPopup: false
          },
          () => {
            // triggering unavailability table refresh
            this.populateUnavailabilityBasedOnActiveSearch();
          }
        );
        // should never happen
      } else {
        // Show the error popup with the correct message
        this.setState({
          showErrorPopup: true,
          errorTitle: LOCALIZE.scorer.ola.assessorsUnavailability.errorPopup.modifyErrorTitle,
          errorDescription:
            LOCALIZE.scorer.ola.assessorsUnavailability.errorPopup.modifyErrorDescription
        });
        throw new Error(
          "Something went wrong during the modification of the assessor unavailability. Please try again."
        );
      }
    });
  };

  // Close Modify Assessor Unavailability Popup
  closeModifyAssessorUnavailabilityPopup = () => {
    this.setState({
      showModifyUnavailabilityPopup: false,
      endDateVisible: false,
      selectedAssessorUnavailabilityData: "",
      isValidStartDatePicked: true,
      isValidEndDatePicked: true,
      isEndDateEqualOrAfterTodayPicked: true,
      displayDateError: false,
      reasonForUnavailability: "",
      olaAssessorSelectedOption: [],
      olaTestCentersSelectedOption: [],
      listOfTestCentersOptions: []
    });
  };

  // Open Delete Assessor Unavailability Popup
  handleOpenDeleteAssessorUnavailabilityPopup = unavailabilityData => {
    this.setState({ loadingPopup: true }, () => {
      this.setState({
        showDeleteUnavailabilityPopup: true,
        selectedAssessorUnavailabilityData: unavailabilityData,
        loadingPopup: false
      });
    });
  };

  // Action - Delete Assessor Unavailability
  handleDeleteAssessorUnavailability = () => {
    const body = {
      test_center_ola_assessor_unavailability_id: this.state.selectedAssessorUnavailabilityData.id
    };
    this.props.deleteAssessorUnavailability(body).then(response => {
      if (response.ok) {
        this.setState(
          {
            showDeleteUnavailabilityPopup: false
          },
          () => {
            // triggering unavailability table refresh
            this.populateUnavailabilityBasedOnActiveSearch();
          }
        );
        // should never happen
      } else {
        // Show the error popup with the correct message
        this.setState({
          showDeleteUnavailabilityPopup: false,
          showErrorPopup: true,
          errorTitle: LOCALIZE.scorer.ola.assessorsUnavailability.errorPopup.deleteErrorTitle,
          errorDescription:
            LOCALIZE.scorer.ola.assessorsUnavailability.errorPopup.deleteErrorDescription
        });
        throw new Error(
          "Something went wrong during the deletion of the assessor unavailability. Please try again."
        );
      }
    });
  };

  // Close Modify Assessor Unavailability Popup
  closeDeleteAssessorUnavailabilityPopup = () => {
    this.setState({
      showDeleteUnavailabilityPopup: false,
      selectedAssessorUnavailabilityData: "",
      reasonForUnavailability: ""
    });
  };

  // Close Error Popup
  closeErrorPopup = () => {
    this.setState(
      {
        showErrorPopup: false,
        errorTitle: "",
        errorDescription: ""
      },
      () => {
        // triggering unavailability table refresh
        this.populateUnavailabilityBasedOnActiveSearch();
      }
    );
  };

  // update reasonForUnavailability content
  updateReasonForUnavailabilityContent = event => {
    const reasonForUnavailability = event.target.value;
    this.setState({
      reasonForUnavailability: reasonForUnavailability
    });
  };

  // List of OLA Assessors Supervised by Current User
  populateOlaTestAssessorOptions = () => {
    this.setState({ loadingOlaAssessorOptions: true }, () => {
      this.props.getAllOlaAssessorsSupervisedByCurrentUser().then(response => {
        // initializing olaAssessorOptions
        const olaAssessorOptions = [];

        // looping in response
        for (let i = 0; i < response.length; i++) {
          olaAssessorOptions.push({
            label: `${response[i].first_name} ${response[i].last_name} (${response[i].email})`,
            value: response[i].user_id
          });
        }

        this.setState({
          listOfAssessorsOptions: olaAssessorOptions,
          loadingOlaAssessorOptions: false
        });
      });
    });
  };

  // List of OLA Test Centers the Selected OLA Assessor is Assigned to
  populateOlaTestCentersOptions = user_id => {
    this.setState({ loadingOlaTestCentersOptions: true }, () => {
      this.props.getAllOlaTestCentersForSelectedAssessor(user_id).then(response => {
        // initializing olaAssessorOptions
        const olaTestCentersOptions = [];

        // looping in response
        for (let i = 0; i < response.length; i++) {
          olaTestCentersOptions.push({
            label: `${response[i].name}`,
            value: response[i].id
          });
        }

        this.setState({
          listOfTestCentersOptions: olaTestCentersOptions,
          loadingOlaTestCentersOptions: false
        });
      });
    });
  };

  render() {
    // converting dates
    let convertedStartDate = null;

    if (this.state.selectedAssessorUnavailabilityData.start_date) {
      convertedStartDate = new Date(this.state.selectedAssessorUnavailabilityData.start_date);
    } else if (
      this.dateStartYearFieldRef.current &&
      this.dateStartMonthFieldRef.current &&
      this.dateStartDayFieldRef.current &&
      this.timeStartHourFieldRef.current &&
      this.timeStartMinFieldRef.current &&
      this.dateStartYearFieldRef.current.props.value !== "" &&
      this.dateStartMonthFieldRef.current.props.value !== "" &&
      this.dateStartDayFieldRef.current.props.value !== "" &&
      this.timeStartHourFieldRef.current.props.value !== "" &&
      this.timeStartMinFieldRef.current.props.value !== ""
    ) {
      convertedStartDate = new Date(
        this.dateStartYearFieldRef.current.props.value.value,
        this.dateStartMonthFieldRef.current.props.value.value - 1,
        this.dateStartDayFieldRef.current.props.value.value,
        this.timeStartHourFieldRef.current.props.value.value,
        this.timeStartMinFieldRef.current.props.value.value,
        0,
        0
      );
    }

    // converting dates
    let convertedEndDate = null;

    if (this.state.selectedAssessorUnavailabilityData.end_date) {
      convertedEndDate = new Date(this.state.selectedAssessorUnavailabilityData.end_date);
    } else if (
      this.dateEndYearFieldRef.current &&
      this.dateEndMonthFieldRef.current &&
      this.dateEndDayFieldRef.current &&
      this.timeEndHourFieldRef.current &&
      this.timeEndMinFieldRef.current &&
      this.dateEndYearFieldRef.current.props.value !== "" &&
      this.dateEndMonthFieldRef.current.props.value !== "" &&
      this.dateEndDayFieldRef.current.props.value !== "" &&
      this.timeEndHourFieldRef.current.props.value !== "" &&
      this.timeEndMinFieldRef.current.props.value !== ""
    ) {
      convertedEndDate = new Date(
        this.dateEndYearFieldRef.current.props.value.value,
        this.dateEndMonthFieldRef.current.props.value.value - 1,
        this.dateEndDayFieldRef.current.props.value.value,
        this.timeEndHourFieldRef.current.props.value.value,
        this.timeEndMinFieldRef.current.props.value.value,
        0,
        0
      );
    }

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };

    const lineSpacingStyle = getLineSpacingCSS();

    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    const columnsDefinition = [
      {
        label: LOCALIZE.scorer.ola.assessorsUnavailability.table.column1,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.scorer.ola.assessorsUnavailability.table.column2,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.scorer.ola.assessorsUnavailability.table.column3,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.scorer.ola.assessorsUnavailability.table.column4,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    if (this.props.isOlaSupervisor || this.props.isTestCenterManager) {
      columnsDefinition.push({
        label: LOCALIZE.scorer.ola.assessorsUnavailability.table.column5,
        style: COMMON_STYLE.CENTERED_TEXT
      });
    }

    return (
      <div style={styles.mainContainer}>
        {(this.props.isOlaSupervisor || this.props.isTestCenterManager) && (
          <Row className="align-items-center" style={styles.buttonRowContainer}>
            <div>
              <CustomButton
                label={
                  this.state.loadingPopup ? (
                    // eslint-disable-next-line jsx-a11y/label-has-associated-control
                    <label className="fa fa-spinner fa-spin">
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  ) : (
                    <>
                      <span>
                        <FontAwesomeIcon icon={faPlusCircle} />
                      </span>
                      <span style={styles.span}>
                        {LOCALIZE.scorer.ola.assessorsUnavailability.addUnavailabilityButton}
                      </span>
                    </>
                  )
                }
                action={this.handleOpenAddAssessorUnavailabilityPopup}
                customStyle={styles.createNewUnavailabilityLabel}
                buttonTheme={THEME.PRIMARY}
              />
            </div>
          </Row>
        )}
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"ola-assessor-unavailability-dates"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateAssessorsOLAUnavailabilityStates}
            updatePageState={this.props.updateAssessorsOLAUnavailabilityPageState}
            paginationPageSize={Number(this.props.assessorsOLAUnavailabilityPaginationPageSize)}
            updatePaginationPageSize={this.props.updateAssessorsOLAUnavailabilityPageSizeState}
            data={this.state.rowsDefinition}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.assessorsOLAUnavailabilityKeyword}
          />
          <div>
            <GenericTable
              classnamePrefix="ola-assessor-unavailability-dates"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={LOCALIZE.scorer.ola.assessorsUnavailability.table.noData}
              currentlyLoading={this.state.currentlyLoading}
              tableWithButtons={this.props.isOlaSupervisor || this.props.isTestCenterManager}
            />
            <Pagination
              paginationContainerId={"ola-assessor-unavailability-dates-pagination-pages-link"}
              pageCount={this.state.numberOfPages}
              paginationPage={this.props.assessorsOLAUnavailabilityPaginationPage}
              updatePaginationPageState={this.props.updateAssessorsOLAUnavailabilityPageState}
              firstTableRowId={"ola-assessor-unavailability-dates-table-row-0"}
            />
          </div>
        </div>

        {/* CREATE / MODIFY UNAVAILABILITY */}
        <PopupBox
          show={this.state.showAddUnavailabilityPopup || this.state.showModifyUnavailabilityPopup}
          title={
            this.state.showAddUnavailabilityPopup
              ? LOCALIZE.scorer.ola.assessorsUnavailability.addUnavailabilityPopup.title
              : LOCALIZE.scorer.ola.assessorsUnavailability.modifyUnavailabilityPopup.title
          }
          handleClose={
            this.state.showAddUnavailabilityPopup
              ? this.closeAddAssessorUnavailabilityPopup
              : this.closeModifyAssessorUnavailabilityPopup
          }
          displayCloseButton={false}
          closeButtonAction={
            this.state.showAddUnavailabilityPopup
              ? this.closeAddAssessorUnavailabilityPopup
              : this.closeModifyAssessorUnavailabilityPopup
          }
          description={
            <div>
              {this.state.showAddUnavailabilityPopup && (
                <p>
                  {LOCALIZE.scorer.ola.assessorsUnavailability.addUnavailabilityPopup.description}
                </p>
              )}
              {this.state.showModifyUnavailabilityPopup && (
                <p>
                  {
                    LOCALIZE.scorer.ola.assessorsUnavailability.modifyUnavailabilityPopup
                      .description
                  }
                </p>
              )}
              {!this.state.loadingPopup ? (
                <div style={styles.formContainer}>
                  {/* Assessors - Dropdown */}
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        id="assessor-unavailability-popup-assessor-dropdown-label"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.assessorsUnavailability.addUnavailabilityPopup
                            .assessor
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      {this.state.showAddUnavailabilityPopup && (
                        // If we are in Add, show dropdown
                        <>
                          {this.state.loadingOlaAssessorOptions ? (
                            <div style={styles.loadingContainer}>
                              {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                              <label className="fa fa-spinner fa-spin">
                                <FontAwesomeIcon icon={faSpinner} style={styles.loading} />
                              </label>
                            </div>
                          ) : (
                            <DropdownSelect
                              idPrefix="assessor-unavailability-popup-assessor-dropdown"
                              ariaLabelledBy="assessor-unavailability-popup-assessor-dropdown-label"
                              ariaRequired={true}
                              options={this.state.listOfAssessorsOptions}
                              onChange={this.getSelectedOlaAssessorOption}
                              defaultValue={this.state.olaAssessorSelectedOption}
                              hasPlaceholder={true}
                              isDisabled={this.state.showEditTestAssessorPopup}
                            />
                          )}
                        </>
                      )}
                      {this.state.showModifyUnavailabilityPopup && (
                        // If we are in Modify, show selected assessor name
                        <p>
                          {this.state.selectedAssessorUnavailabilityData.first_name}{" "}
                          {this.state.selectedAssessorUnavailabilityData.last_name}
                        </p>
                      )}
                    </Col>
                  </Row>

                  {/* Test Centers - Dropdown */}
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        id="assessor-unavailability-popup-test-centers-dropdown-label"
                        style={styles.boldText}
                      >
                        {
                          LOCALIZE.scorer.ola.assessorsUnavailability.addUnavailabilityPopup
                            .testCenters
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      {this.state.showAddUnavailabilityPopup && (
                        // If we are in Add, show dropdown
                        <>
                          {this.state.loadingAssessorsOptions ? (
                            <div style={styles.loadingContainer}>
                              {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                              <label className="fa fa-spinner fa-spin">
                                <FontAwesomeIcon icon={faSpinner} style={styles.loading} />
                              </label>
                            </div>
                          ) : (
                            <DropdownSelect
                              idPrefix="assessor-unavailability-popup-test-centers-dropdown"
                              ariaLabelledBy="assessor-unavailability-popup-test-centers-dropdown-label"
                              ariaRequired={true}
                              options={this.state.listOfTestCentersOptions}
                              onChange={this.getSelectedOlaTestCentersOption}
                              defaultValue={this.state.olaTestCentersSelectedOption}
                              isMulti={true}
                              hasPlaceholder={true}
                              // If empty, disable the dropdown
                              isDisabled={this.state.listOfTestCentersOptions.length === 0}
                            />
                          )}
                        </>
                      )}
                      {this.state.showModifyUnavailabilityPopup && (
                        // If we are in Modify, show selected assessor name
                        <p>{this.state.selectedAssessorUnavailabilityData.test_center_name}</p>
                      )}
                    </Col>
                  </Row>

                  {/* Start Date - DatePicker */}
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                      style={
                        this.state.displayDateError
                          ? { ...styles.labelContainer, ...styles.dateLabelsOnError }
                          : styles.labelContainer
                      }
                    >
                      <label id="start-date-label" style={styles.boldText}>
                        {
                          LOCALIZE.scorer.ola.assessorsUnavailability.addUnavailabilityPopup
                            .startDate
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                      style={styles.singleDropdownContainer}
                      onFocus={this.onDateFromFocus}
                    >
                      <DatePicker
                        dateDayFieldRef={this.dateStartDayFieldRef}
                        dateMonthFieldRef={this.dateStartMonthFieldRef}
                        dateYearFieldRef={this.dateStartYearFieldRef}
                        timeHourFieldRef={this.timeStartHourFieldRef}
                        timeMinFieldRef={this.timeStartMinFieldRef}
                        initialDateDayValue={
                          this.state.selectedAssessorUnavailabilityData !== "" && {
                            value: convertedStartDate.getDate(),
                            label:
                              convertedStartDate.getDate() < 10
                                ? `0${convertedStartDate.getDate()}`
                                : convertedStartDate.getDate()
                          }
                        }
                        initialDateMonthValue={
                          this.state.selectedAssessorUnavailabilityData !== "" && {
                            value: convertedStartDate.getMonth() + 1,
                            label:
                              convertedStartDate.getMonth() + 1 < 10
                                ? `0${convertedStartDate.getMonth() + 1}`
                                : convertedStartDate.getMonth() + 1
                          }
                        }
                        initialDateYearValue={
                          this.state.selectedAssessorUnavailabilityData !== "" && {
                            value: convertedStartDate.getFullYear(),
                            label: convertedStartDate.getFullYear()
                          }
                        }
                        initialTimeHourValue={
                          this.state.selectedAssessorUnavailabilityData !== "" && {
                            value: convertedStartDate.getHours(),
                            label:
                              convertedStartDate.getHours() < 10
                                ? `0${convertedStartDate.getHours()}`
                                : convertedStartDate.getHours()
                          }
                        }
                        initialTimeMinValue={
                          this.state.selectedAssessorUnavailabilityData !== "" && {
                            value: convertedStartDate.getMinutes(),
                            label:
                              convertedStartDate.getMinutes() < 10
                                ? `0${convertedStartDate.getMinutes()}`
                                : convertedStartDate.getMinutes()
                          }
                        }
                        dateLabelId={"start-date-label"}
                        onInputChange={this.handleDateUpdates}
                        isValidCompleteDatePicked={this.state.isValidStartDatePicked}
                        showDatePickerInInvalidStyling={this.state.displayDateError}
                        customYearOptions={populateCustomLastAndFutureYearsDateOptions(3)}
                        disabledDropdowns={this.state.currentlyLoading}
                        allowTimeValue={true}
                      />
                    </Col>
                  </Row>

                  {/* End Date - DatePicker */}
                  {this.state.endDateVisible && (
                    <Row className="align-items-center" style={styles.rowContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={
                          this.state.displayDateError
                            ? { ...styles.labelContainer, ...styles.dateLabelsOnError }
                            : styles.labelContainer
                        }
                      >
                        <label id="end-date-label" style={styles.boldText}>
                          {
                            LOCALIZE.scorer.ola.assessorsUnavailability.addUnavailabilityPopup
                              .endDate
                          }
                        </label>
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.singleDropdownContainer}
                        onFocus={this.onDateToFocus}
                      >
                        <DatePicker
                          dateDayFieldRef={this.dateEndDayFieldRef}
                          dateMonthFieldRef={this.dateEndMonthFieldRef}
                          dateYearFieldRef={this.dateEndYearFieldRef}
                          timeHourFieldRef={this.timeEndHourFieldRef}
                          timeMinFieldRef={this.timeEndMinFieldRef}
                          initialDateDayValue={
                            this.state.selectedAssessorUnavailabilityData !== "" && {
                              value: convertedEndDate.getDate(),
                              label:
                                convertedEndDate.getDate() < 10
                                  ? `0${convertedEndDate.getDate()}`
                                  : convertedEndDate.getDate()
                            }
                          }
                          initialDateMonthValue={
                            this.state.selectedAssessorUnavailabilityData !== "" && {
                              value: convertedEndDate.getMonth() + 1,
                              label:
                                convertedEndDate.getMonth() + 1 < 10
                                  ? `0${convertedEndDate.getMonth() + 1}`
                                  : convertedEndDate.getMonth() + 1
                            }
                          }
                          initialDateYearValue={
                            this.state.selectedAssessorUnavailabilityData !== "" && {
                              value: convertedEndDate.getFullYear(),
                              label: convertedEndDate.getFullYear()
                            }
                          }
                          initialTimeHourValue={
                            this.state.selectedAssessorUnavailabilityData !== "" && {
                              value: convertedEndDate.getHours(),
                              label:
                                convertedEndDate.getHours() < 10
                                  ? `0${convertedEndDate.getHours()}`
                                  : convertedEndDate.getHours()
                            }
                          }
                          initialTimeMinValue={
                            this.state.selectedAssessorUnavailabilityData !== "" && {
                              value: convertedEndDate.getMinutes(),
                              label:
                                convertedEndDate.getMinutes() < 10
                                  ? `0${convertedEndDate.getMinutes()}`
                                  : convertedEndDate.getMinutes()
                            }
                          }
                          dateLabelId={"end-date-label"}
                          onInputChange={this.handleDateUpdates}
                          isValidCompleteDatePicked={this.state.isValidEndDatePicked}
                          showDatePickerInInvalidStyling={
                            this.state.displayDateError ||
                            !this.state.isEndDateEqualOrAfterTodayPicked
                          }
                          customYearOptions={populateCustomLastAndFutureYearsDateOptions(3)}
                          disabledDropdowns={this.state.currentlyLoading}
                          allowTimeValue={true}
                        />
                      </Col>
                    </Row>
                  )}
                  {!this.state.isEndDateEqualOrAfterTodayPicked && (
                    <Row className="align-items-center" style={styles.rowContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={
                          this.state.displayDateError
                            ? { ...styles.labelContainer, ...styles.dateLabelsOnError }
                            : styles.labelContainer
                        }
                      ></Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      >
                        <p tabIndex={"0"} style={styles.dateError}>
                          {LOCALIZE.scorer.ola.assessorsUnavailability.errors.endDateError}
                        </p>
                      </Col>
                    </Row>
                  )}
                  {this.state.displayDateError && (
                    <Row className="align-items-center" style={styles.rowContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={
                          this.state.displayDateError
                            ? { ...styles.labelContainer, ...styles.dateLabelsOnError }
                            : styles.labelContainer
                        }
                      ></Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      >
                        <p tabIndex={"0"} style={styles.dateError}>
                          {LOCALIZE.reports.dateError}
                        </p>
                      </Col>
                    </Row>
                  )}
                  <Row
                    className="align-items-center justify-content-start"
                    style={styles.rowContainer}
                  >
                    <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                      <label htmlFor="reason-for-unavailability" style={styles.boldText}>
                        {
                          LOCALIZE.scorer.ola.assessorsUnavailability.addUnavailabilityPopup
                            .reasonForUnavailability
                        }
                      </label>
                    </Col>
                  </Row>
                  <Row
                    className="align-items-center justify-content-start"
                    style={styles.rowContainer}
                  >
                    <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                      <textarea
                        id="reason-for-unavailability"
                        className="valid-field"
                        style={{
                          ...styles.reasonInput
                        }}
                        value={this.state.reasonForUnavailability}
                        onChange={this.updateReasonForUnavailabilityContent}
                        maxLength="300"
                      ></textarea>
                    </Col>
                  </Row>
                </div>
              ) : (
                <div style={styles.loadingContainer}>
                  <label htmlFor="loading-icon" className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon
                      id="loading-icon"
                      icon={faSpinner}
                      style={styles.loadingIcon}
                    />
                  </label>
                </div>
              )}
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={
            this.state.showAddUnavailabilityPopup
              ? this.closeAddAssessorUnavailabilityPopup
              : this.closeModifyAssessorUnavailabilityPopup
          }
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            this.state.showAddUnavailabilityPopup
              ? LOCALIZE.scorer.ola.assessorsUnavailability.addUnavailabilityPopup.rightButton
              : LOCALIZE.scorer.ola.assessorsUnavailability.modifyUnavailabilityPopup.rightButton
          }
          rightButtonAction={
            this.state.showAddUnavailabilityPopup
              ? this.handleAddAssessorUnavailability
              : this.handleModifyAssessorUnavailability
          }
          rightButtonIcon={this.state.showAddUnavailabilityPopup ? faPlusCircle : faSave}
          rightButtonState={this.areAllFieldsValid() ? BUTTON_STATE.enabled : BUTTON_STATE.disabled}
        />

        {/* DELETE UNAVAILABILITY */}
        <PopupBox
          show={this.state.showDeleteUnavailabilityPopup}
          title={
            LOCALIZE.scorer.ola.assessorsUnavailability.deleteUnavailabilityConfirmationPopup.title
          }
          handleClose={() => {}}
          size="xl"
          description={
            <div>
              <div style={styles.formContainer}>
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.warning}
                  message={
                    <p>
                      {
                        LOCALIZE.scorer.ola.assessorsUnavailability
                          .deleteUnavailabilityConfirmationPopup.systemMessageDescription
                      }
                    </p>
                  }
                />
                <div style={styles.paddingDeletePopup}>
                  {/* Assessor */}
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label style={styles.boldText}>
                        {
                          LOCALIZE.scorer.ola.assessorsUnavailability.addUnavailabilityPopup
                            .assessor
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <p>
                        {this.state.selectedAssessorUnavailabilityData
                          ? `${this.state.selectedAssessorUnavailabilityData.first_name} ${this.state.selectedAssessorUnavailabilityData.last_name}`
                          : ""}
                      </p>
                    </Col>
                  </Row>
                  {/* Test Center */}
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label style={styles.boldText}>
                        {
                          LOCALIZE.scorer.ola.assessorsUnavailability.addUnavailabilityPopup
                            .testCenters
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <p>
                        {this.state.selectedAssessorUnavailabilityData
                          ? `${this.state.selectedAssessorUnavailabilityData.test_center_name}`
                          : ""}
                      </p>
                    </Col>
                  </Row>
                  {/* Start Date */}
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label id="start-date-label" style={styles.boldText}>
                        {
                          LOCALIZE.scorer.ola.assessorsUnavailability.addUnavailabilityPopup
                            .startDate
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <p>
                        {this.state.selectedAssessorUnavailabilityData
                          ? `${this.state.selectedAssessorUnavailabilityData.start_date.split("T")[0]} ${this.state.selectedAssessorUnavailabilityData.start_date
                              .split("T")[1]
                              .substring(0, 5)}`
                          : ""}
                      </p>
                    </Col>
                  </Row>
                  {/* End Date */}
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label id="end-date-label" style={styles.boldText}>
                        {LOCALIZE.scorer.ola.assessorsUnavailability.addUnavailabilityPopup.endDate}
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <p>
                        {this.state.selectedAssessorUnavailabilityData
                          ? `${this.state.selectedAssessorUnavailabilityData.end_date.split("T")[0]} ${this.state.selectedAssessorUnavailabilityData.end_date
                              .split("T")[1]
                              .substring(0, 5)}`
                          : ""}
                      </p>
                    </Col>
                  </Row>
                  {/* Reason For Unavailability */}
                  <Row className="align-items-center" style={styles.rowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label id="reason-label" style={styles.boldText}>
                        {
                          LOCALIZE.scorer.ola.assessorsUnavailability.addUnavailabilityPopup
                            .reasonForUnavailability
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <p>{this.state.selectedAssessorUnavailabilityData.reason}</p>
                    </Col>
                  </Row>
                </div>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeDeleteAssessorUnavailabilityPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={
            LOCALIZE.scorer.ola.assessorsUnavailability.deleteUnavailabilityConfirmationPopup
              .rightButton
          }
          rightButtonIcon={faTrashAlt}
          rightButtonAction={this.handleDeleteAssessorUnavailability}
          rightButtonState={BUTTON_STATE.enabled}
        />
        <PopupBox
          show={this.state.showErrorPopup}
          handleClose={() => {}}
          title={this.state.errorTitle}
          size="lg"
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.error}
                message={<p>{this.state.errorDescription}</p>}
              />
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonAction={this.closeErrorPopup}
          rightButtonIcon={faTimes}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    currentLanguage: state.localize.language,
    user_id: state.user.user_id,
    assessorsOLAUnavailabilityPaginationPage: state.scorer.assessorsOLAUnavailabilityPaginationPage,
    assessorsOLAUnavailabilityPaginationPageSize:
      state.scorer.assessorsOLAUnavailabilityPaginationPageSize,
    assessorsOLAUnavailabilityKeyword: state.scorer.assessorsOLAUnavailabilityKeyword,
    assessorsOLAUnavailabilityActiveSearch: state.scorer.assessorsOLAUnavailabilityActiveSearch,
    triggerRerenderScorerTables: state.scorer.triggerRerenderScorerTables
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getFoundAssessorsOlaUnavailabilityAsSupervisor,
      getAllAssessorsOlaUnavailabilityAsSupervisor,
      updateAssessorsOLAUnavailabilityPageState,
      updateAssessorsOLAUnavailabilityPageSizeState,
      updateAssessorsOLAUnavailabilityStates,
      getAllOlaAssessorsSupervisedByCurrentUser,
      getAllOlaTestCentersForSelectedAssessor,
      saveNewAssessorUnavailability,
      modifyAssessorUnavailability,
      deleteAssessorUnavailability
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AssessorsOLAUnavailability);
