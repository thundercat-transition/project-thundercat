/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from "react";
import PropTypes from "prop-types";
import withRouter from "../withRouter";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import getConvertedScore from "../../helpers/scoreConversion";
import { Row, Col } from "react-bootstrap";
import TEST_STATUS from "../ta/Constants";
import MutationObserver, { WatchForRemoval } from "react-mutation-observer";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  }
};

const styles = {
  mainDiv: {
    marginTop: 12,
    textAlign: "center"
  },
  popupStyles: {
    popupDescriptionContainer: {
      position: "relative"
    },
    itemContainer: {
      width: "100%",
      padding: "6px 0"
    },
    label: {
      verticalAlign: "middle",
      paddingRight: 18
    },
    value: {
      verticalAlign: "middle"
    },
    inputDisabled: {
      width: "100%",
      minHeight: 38,
      padding: "3px 6px 3px 6px",
      borderRadius: 4,
      wordBreak: "break-word"
    },
    maskLabel: {
      position: "absolute",
      opacity: "0.2",
      width: "100%",
      height: "100%"
    }
  }
};

class ArchivedTests extends Component {
  static propTypes = {
    currentlyLoading: PropTypes.bool.isRequired,
    rowsDefinition: PropTypes.object.isRequired,
    selectedTestDetails: PropTypes.object.isRequired,
    triggerPopup: PropTypes.bool.isRequired
  };

  state = {
    displayResultsDetailsPopup: false,
    hideFields: false
  };

  componentDidUpdate = prevProps => {
    // if triggerPopup gets updated
    if (prevProps.triggerPopup !== this.props.triggerPopup) {
      this.handlePopupDisplay();
    }
  };

  // open popup
  handlePopupDisplay = () => {
    this.setState({ displayResultsDetailsPopup: true });
  };

  // close popup
  closePopup = () => {
    this.setState({ displayResultsDetailsPopup: false });
  };

  onArchivedWatermarkRemoveOrUpdate = () => {
    // hiding fields and reloading page
    this.setState({ hideFields: true }, () => {
      window.location.reload();
    });
  };

  render() {
    // column definition
    const columnsDefinition = [
      {
        label: LOCALIZE.myTests.table.nameOfTest,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.myTests.table.testDate,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.myTests.table.actions,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    return (
      <div style={styles.mainDiv}>
        <GenericTable
          classnamePrefix="my-tests"
          columnsDefinition={columnsDefinition}
          rowsDefinition={this.props.rowsDefinition}
          emptyTableMessage={LOCALIZE.myTests.table.noDataMessage}
          currentlyLoading={this.props.currentlyLoading}
        />
        {Object.keys(this.props.selectedTestDetails).length !== 0 && (
          <PopupBox
            show={this.state.displayResultsDetailsPopup}
            handleClose={this.closePopup}
            isBackdropStatic={true}
            shouldCloseOnEsc={true}
            title={LOCALIZE.myTests.table.viewResultsPopup.title}
            description={
              <div style={styles.popupStyles.popupDescriptionContainer}>
                <MutationObserver
                  onAttributeChange={this.onArchivedWatermarkRemoveOrUpdate}
                  onContentChange={this.onArchivedWatermarkRemoveOrUpdate}
                  onChildRemoval={this.onArchivedWatermarkRemoveOrUpdate}
                >
                  <WatchForRemoval onRemoval={this.onArchivedWatermarkRemoveOrUpdate}>
                    <div id="archived-watermark-text">
                      {LOCALIZE.myTests.table.viewResultsPopup.archivedWatermark}
                    </div>
                  </WatchForRemoval>
                </MutationObserver>
                {/* hideFields will be set to true if the <div id="archived-watermark-text"> is being removed in the browser (preventing users to take screenshot of the popup without the mask)  */}
                {!this.state.hideFields && (
                  <>
                    <Row style={styles.popupStyles.itemContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.popupStyles.label}
                      >
                        {LOCALIZE.myTests.table.viewResultsPopup.testLabel}
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.popupStyles.value}
                      >
                        <input
                          style={styles.popupStyles.inputDisabled}
                          className={"valid-field"}
                          disabled={true}
                          type="text"
                          value={this.props.selectedTestDetails.test_code}
                        />
                      </Col>
                    </Row>
                    <Row style={styles.popupStyles.itemContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.popupStyles.label}
                      >
                        {LOCALIZE.myTests.table.viewResultsPopup.testDescriptionLabel}
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.popupStyles.value}
                      >
                        <textarea
                          style={styles.popupStyles.inputDisabled}
                          className={"valid-field"}
                          disabled={true}
                          type="text"
                          value={
                            this.props.selectedTestDetails[
                              `test_name_${this.props.currentLanguage}`
                            ]
                          }
                        />
                      </Col>
                    </Row>
                    <Row style={styles.popupStyles.itemContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.popupStyles.label}
                      >
                        {LOCALIZE.myTests.table.viewResultsPopup.testDateLabel}
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.popupStyles.value}
                      >
                        <input
                          style={styles.popupStyles.inputDisabled}
                          className={"valid-field"}
                          disabled={true}
                          type="text"
                          value={this.props.selectedTestDetails.adjusted_submit_date}
                        />
                      </Col>
                    </Row>
                    <Row style={styles.popupStyles.itemContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.popupStyles.label}
                      >
                        {LOCALIZE.myTests.table.viewResultsPopup.testStatus}
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.popupStyles.value}
                      >
                        <input
                          style={styles.popupStyles.inputDisabled}
                          className={"valid-field"}
                          disabled={true}
                          type="text"
                          value={
                            this.props.selectedTestDetails.status_codename === TEST_STATUS.SUBMITTED
                              ? LOCALIZE.commons.status.submitted
                              : LOCALIZE.commons.status.quit
                          }
                        />
                      </Col>
                    </Row>
                    <Row style={styles.popupStyles.itemContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.popupStyles.label}
                      >
                        {LOCALIZE.myTests.table.viewResultsPopup.scoreLabel}
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.popupStyles.value}
                      >
                        <input
                          style={styles.popupStyles.inputDisabled}
                          className={"valid-field"}
                          disabled={true}
                          type="text"
                          value={
                            this.props.selectedTestDetails.is_invalid
                              ? LOCALIZE.commons.status.invalid
                              : this.props.selectedTestDetails.pending_score
                                ? LOCALIZE.commons.status.pending
                                : this.props.selectedTestDetails.total_score !== null
                                  ? this.props.selectedTestDetails.show_score
                                    ? this.props.selectedTestDetails.total_score
                                    : LOCALIZE.commons.status.cannotBeShared
                                  : LOCALIZE.commons.na
                          }
                        />
                      </Col>
                    </Row>
                    <Row style={styles.popupStyles.itemContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.popupStyles.label}
                      >
                        {LOCALIZE.myTests.table.viewResultsPopup.resultLabel}
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.popupStyles.value}
                      >
                        <input
                          style={styles.popupStyles.inputDisabled}
                          className={"valid-field"}
                          disabled={true}
                          type="text"
                          value={
                            this.props.selectedTestDetails.is_invalid
                              ? LOCALIZE.commons.status.invalid
                              : this.props.selectedTestDetails.pending_score
                                ? LOCALIZE.commons.status.pending
                                : this.props.selectedTestDetails.show_result ||
                                    this.props.selectedTestDetails.status_codename ===
                                      TEST_STATUS.QUIT
                                  ? getConvertedScore(
                                      this.props.selectedTestDetails[
                                        `${this.props.currentLanguage}_converted_score`
                                      ],
                                      this.props.selectedTestDetails.status_codename
                                    )
                                  : LOCALIZE.commons.status.cannotBeShared
                          }
                        />
                      </Col>
                    </Row>
                    <Row style={styles.popupStyles.itemContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.popupStyles.label}
                      >
                        {LOCALIZE.myTests.table.viewResultsPopup.scoreValidity}
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.popupStyles.value}
                      >
                        <input
                          style={styles.popupStyles.inputDisabled}
                          className={"valid-field"}
                          disabled={true}
                          type="text"
                          value={
                            this.props.selectedTestDetails.is_invalid
                              ? LOCALIZE.commons.na
                              : this.props.selectedTestDetails.pending_score
                                ? LOCALIZE.commons.status.pending
                                : this.props.selectedTestDetails.result_valid_indefinitely
                                  ? LOCALIZE.commons.indefinite
                                  : this.props.selectedTestDetails.score_valid_until !== null
                                    ? `${this.props.selectedTestDetails.validity_period} ${LOCALIZE.myTests.table.viewResultsPopup.scoreValidityUnits}`
                                    : LOCALIZE.commons.na
                          }
                        />
                      </Col>
                    </Row>
                    <Row style={styles.popupStyles.itemContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.popupStyles.label}
                      >
                        {LOCALIZE.myTests.table.viewResultsPopup.expiration}
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.popupStyles.value}
                      >
                        <input
                          style={styles.popupStyles.inputDisabled}
                          className={"valid-field"}
                          disabled={true}
                          type="text"
                          value={
                            this.props.selectedTestDetails.is_invalid
                              ? LOCALIZE.commons.na
                              : this.props.selectedTestDetails.pending_score
                                ? LOCALIZE.commons.status.pending
                                : this.props.selectedTestDetails.result_valid_indefinitely &&
                                    this.props.selectedTestDetails.status_codename !==
                                      TEST_STATUS.QUIT
                                  ? LOCALIZE.commons.never
                                  : this.props.selectedTestDetails.score_valid_until !== null
                                    ? this.props.selectedTestDetails.score_valid_until
                                    : LOCALIZE.commons.na
                          }
                        />
                      </Col>
                    </Row>
                    <Row style={styles.popupStyles.itemContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.popupStyles.label}
                      >
                        {LOCALIZE.myTests.table.viewResultsPopup.retestPeriodLabel}
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.popupStyles.value}
                      >
                        <input
                          style={styles.popupStyles.inputDisabled}
                          className={"valid-field"}
                          disabled={true}
                          type="text"
                          value={
                            this.props.selectedTestDetails.pending_score &&
                            !this.props.selectedTestDetails.is_invalid
                              ? LOCALIZE.commons.status.pending
                              : this.props.selectedTestDetails.most_updated_retest_period !== null
                                ? `${this.props.selectedTestDetails.most_updated_retest_period} ${LOCALIZE.myTests.table.viewResultsPopup.retestPeriodUnits}`
                                : LOCALIZE.commons.na
                          }
                        />
                      </Col>
                    </Row>
                    <Row style={styles.popupStyles.itemContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                        style={styles.popupStyles.label}
                      >
                        {LOCALIZE.myTests.table.viewResultsPopup.retestDateLabel}
                      </Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                        style={styles.popupStyles.value}
                      >
                        <input
                          style={styles.popupStyles.inputDisabled}
                          className={"valid-field"}
                          disabled={true}
                          type="text"
                          value={
                            this.props.selectedTestDetails.pending_score &&
                            !this.props.selectedTestDetails.is_invalid
                              ? LOCALIZE.commons.status.pending
                              : this.props.selectedTestDetails.adjusted_retest_date
                          }
                        />
                      </Col>
                    </Row>
                  </>
                )}
              </div>
            }
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={LOCALIZE.commons.ok}
            rightButtonAction={this.closePopup}
          />
        )}
      </div>
    );
  }
}

export { ArchivedTests as UnconnectedArchivedTests };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ArchivedTests));

export { styles as TEST_TABLE_STYLES };
