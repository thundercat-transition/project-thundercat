/* eslint-disable react/no-unknown-property */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Container } from "react-bootstrap";
import LOCALIZE from "../../text_resources";
import Notepad from "../commons/Notepad";
import Calculator from "../commons/calculator/Calculator";
import { BANNER_STYLE, TEST_STYLE } from "./constants";
import { TestSectionComponentType } from "../testFactory/Constants";
import SingleComponentFactory from "../testFactory/SingleComponentFactory";
import { switchTopTab } from "../../modules/NavTabsRedux";
import { getTestLanguage } from "../testBuilder/helpers";
import { useTabState, Tab, TabList, TabPanel } from "reakit/Tab";
import { setTopTabsHeight } from "../../modules/TestSectionRedux";
import getInTestHeightCalculations from "../../helpers/inTestHeightCalculations";

const styles = {
  container: {
    maxWidth: 1400,
    // if you update the padding top, don't forget to update the value in ".../helpers/inTestHeightCalculations.js" (getInTestHeightCalculations function)
    padding: "20px 0 0 0",
    margin: "0px auto"
  },
  tabsContainer: {
    display: "table",
    width: "100%"
  },
  tabsContainerWidth: {
    padding: 0,
    borderTop: "1px solid #cdcdcd",
    display: "table-cell",
    backgroundColor: "#fff"
  },
  panelContent: {
    backgroundColor: "#fff"
  },
  toolsContainer: {
    padding: 0,
    display: "table-cell",
    verticalAlign: "top",
    width: "25%"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  menuTabStyle: {
    backgroundColor: "#fafafa",
    padding: "0.5rem 1rem 0.2rem 1rem",
    border: "2px solid transparent",
    borderTopLeftRadius: "0.35rem",
    borderTopRightRadius: "0.35rem",
    borderBottomWidth: "4px",
    color: "#00565e",
    borderColor: "transparent transparent #00565e"
  },
  menuTabInactive: {
    backgroundColor: "#fafafa",
    padding: "0.5rem 1rem 0.2rem 1rem",
    border: "none",
    color: "#00565e"
  }
};

function getTabs(props, tab, componentFunctions) {
  componentFunctions.switchTab(tab.currentId);
  return (
    <>
      {componentFunctions.tabs.map((item, index) => {
        return (
          <Tab
            {...tab}
            id={index + 1}
            key={index + 1}
            style={tab.selectedId === index + 1 ? styles.menuTabStyle : styles.menuTabInactive}
            className="notranslate"
          >
            <div lang={item.lang}>{item.tabName}</div>
          </Tab>
        );
      })}
    </>
  );
}

function getTabPanels(props, tab, componentFunctions) {
  return (
    <>
      {componentFunctions.tabs.map((item, index) => {
        let customStyle = {};
        // if current test section is not a questions list and not an item bank list
        if (
          componentFunctions.props.testSection.components[index].component_type !==
            TestSectionComponentType.QUESTION_LIST &&
          componentFunctions.props.testSection.components[index].component_type !==
            TestSectionComponentType.ITEM_BANK
        ) {
          let { topTabsHeight } = props;

          // add extra height if in test builder
          if (props.calledInTestBuilder) {
            let heightOfBackToTestBuilderButton = 0;
            if (document.getElementById("back-to-test-builder-button") != null) {
              heightOfBackToTestBuilderButton = document.getElementById(
                "back-to-test-builder-button"
              ).offsetHeight;
            }
            topTabsHeight += heightOfBackToTestBuilderButton + 20; // height of "back to test builder button" + height of padding under button
          }

          const customHeight = getInTestHeightCalculations(
            props.accommodations.fontSize,
            props.accommodations.spacing,
            props.testNavBarHeight,
            topTabsHeight,
            props.testFooterHeight
          );
          customStyle = {
            ...customHeight,
            overflowY: "scroll"
          };
        }
        return (
          <TabPanel
            // if you update this id, please don't forget to also update the id in MultipleChoiceQuestionList (in scroll to top functionality)
            id={`top-tab-component-${index}`}
            key={index + 1}
            {...tab}
            style={{ ...styles.panelContent, ...customStyle }}
          >
            {item.body}
          </TabPanel>
        );
      })}
    </>
  );
}

function TopTabHooks(props) {
  const tab = useTabState({
    orientation: "horizontal",
    selectedId: props.currentTab || props.defaultTab
  });

  return function func(componentFunctions) {
    return (
      <section aria-label={LOCALIZE.ariaLabel.sideNavigationSection}>
        <Container style={styles.container}>
          <div aria-label={LOCALIZE.ariaLabel.topNavigationSection}>
            <TabList
              selectedId={props.currentTab}
              {...tab}
              aria-label={LOCALIZE.ariaLabel.sideNavigationSection}
              id="navigation-items-section"
              ref={componentFunctions.topTabsHeightRef}
            >
              {getTabs(props, tab, componentFunctions)}
            </TabList>
          </div>
          <div style={styles.tabsContainer}>
            <div style={styles.tabsContainerWidth} role="region">
              {getTabPanels(props, tab, componentFunctions)}
            </div>
            {/* Notepad only */}
            {props.usesNotepad && !props.usesCalculator && !props.readOnly && (
              <div md="auto" style={!props.isNotepadHidden ? styles.toolsContainer : {}}>
                {!props.isNotepadHidden && (
                  <Notepad
                    headerFooterPX={props.isTestActive ? TEST_STYLE : BANNER_STYLE}
                    calledInTestBuilder={props.calledInTestBuilder}
                  />
                )}
              </div>
            )}
            {/* Calculator only */}
            {!props.usesNotepad && props.usesCalculator && !props.readOnly && (
              <div md="auto" style={!props.isCalculatorHidden ? styles.toolsContainer : {}}>
                {!props.isCalculatorHidden && (
                  <Calculator
                    showHeader={true}
                    calledInTestBuilder={props.calledInTestBuilder}
                    usesNotepad={props.usesNotepad}
                  />
                )}
              </div>
            )}
            {/* Notepad and Calculator */}
            {props.usesNotepad && props.usesCalculator && !props.readOnly && (
              <div
                md="auto"
                style={
                  props.isCalculatorHidden && props.isNotepadHidden ? {} : styles.toolsContainer
                }
              >
                {!props.isCalculatorHidden && (
                  <Calculator
                    showHeader={true}
                    calledInTestBuilder={props.calledInTestBuilder}
                    usesNotepad={props.usesNotepad}
                  />
                )}
                {!props.isNotepadHidden && (
                  <Notepad
                    headerFooterPX={props.isTestActive ? TEST_STYLE : BANNER_STYLE}
                    calledInTestBuilder={props.calledInTestBuilder}
                    calledWithCalculator={true}
                  />
                )}
              </div>
            )}
          </div>
        </Container>
      </section>
    );
  };
}

class TopTabsClass extends Component {
  constructor(props) {
    super(props);
    this.topTabsHeightRef = React.createRef();
    this.PropTypes = { calledInTestBuilder: PropTypes.bool };
  }

  state = {
    // to enable or disable hidden message related to the only tab available as far as the test is not fully started (for screen reader users only)
    oneTabOnlyHiddenMsgEnabled: false
  };

  componentDidUpdate = prevProps => {
    // if defaultTab gets updated
    if (prevProps.defaultTab !== this.props.defaultTab) {
      this.switchTab(this.props.defaultTab);
    }
    // updating top tabs height redux state
    if (this.topTabsHeightRef.current) {
      this.props.setTopTabsHeight(this.topTabsHeightRef.current.clientHeight);
    }
  };

  // enabling the "single tab available" message for screen reader users on enter eMIB action, meaning before the test fully starts
  componentDidMount = () => {
    this.switchTab(this.props.currentTab >= 1 ? this.props.currentTab : this.props.defaultTab);
  };

  switchTab = tab => {
    this.props.switchTopTab(tab);
  };

  getTabs = () => {
    return this.props.testSection.components.map(tab => {
      return {
        key: tab.order,
        tabName: tab.title[this.props.currentLanguage],
        lang: getTestLanguage(tab.language, this.props.currentLanguage),
        body: (
          <SingleComponentFactory
            testSection={tab}
            type={tab.component_type}
            calledInTestBuilder={this.props.calledInTestBuilder}
          />
        )
      };
    });
  };

  render() {
    const TABS = this.getTabs();
    const { myHookValue } = this.props;
    return <>{myHookValue({ ...this, tabs: TABS })}</>;
  }
}

function withMyHook(Component) {
  return function WrappedComponent(props) {
    const myHookValue = TopTabHooks(props);
    return <Component {...props} myHookValue={myHookValue} />;
  };
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    currentTab: state.navTabs.currentTopTab,
    isTestActive: state.testStatus.isTestActive,
    readOnly: state.testSection.readOnly,
    isNotepadHidden: state.notepad.isNotepadHidden,
    isCalculatorHidden: state.calculator.isCalculatorHidden,
    topTabsHeight: state.testSection.topTabsHeight,
    testNavBarHeight: state.testSection.testNavBarHeight,
    testFooterHeight: state.testSection.testFooterHeight,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      switchTopTab,
      setTopTabsHeight
    },
    dispatch
  );

const TopTabs = withMyHook(TopTabsClass);
export { TopTabs as UnconnectedTopTabs };
export default connect(mapStateToProps, mapDispatchToProps)(TopTabs);
