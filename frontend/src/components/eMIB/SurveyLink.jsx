import React, { Component } from "react";
import "../../css/collapsing-item.css";
import PropTypes from "prop-types";
import withRouter from "../withRouter";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

const styles = {
  surveyLinkContainer: {
    textAlign: "left",
    textDecoration: "underline",
    textDecorationColor: "#00565e"
  },
  leftPadding: {
    paddingLeft: 6
  },
  icon: {
    color: "#00565e"
  }
};

class SurveyLink extends Component {
  static propTypes = {
    surveyLinkData: PropTypes.object.isRequired
  };

  render() {
    // setting link custom font size
    const linkCustomFontSize = {
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 4}px`
    };

    // formatting link value with needed parameters
    let formatted_link_value =
      this.props.surveyLinkData.pageSection.page_section_content.link_value;
    if (this.props.assignedTestId !== null) {
      formatted_link_value = `${this.props.surveyLinkData.pageSection.page_section_content.link_value}?assigned_test_id=${this.props.assignedTestId}`;
    }

    return (
      <p style={{ ...styles.surveyLinkContainer, ...linkCustomFontSize }}>
        <a href={formatted_link_value} target="_blank" rel="noreferrer">
          <>
            <span>
              {<FontAwesomeIcon icon={faUpRightFromSquare} style={styles.icon}></FontAwesomeIcon>}
            </span>
            <span style={styles.leftPadding}>
              {
                this.props.surveyLinkData.pageSection.page_section_content.content[
                  this.props.currentLanguage
                ][0].text
              }
            </span>
          </>
        </a>
      </p>
    );
  }
}

export { SurveyLink as UnconnectedSurveyLink };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    assignedTestId: state.assignedTest.assignedTestId
  };
};

const mapDispatchToProps = dispatch => {
  bindActionCreators({}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SurveyLink));
