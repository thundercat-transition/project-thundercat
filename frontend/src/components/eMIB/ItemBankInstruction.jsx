import React, { Component } from "react";
import "../../css/collapsing-item.css";
import rehypeRaw from "rehype-raw";
import ReactMarkdown from "react-markdown";
import { getFormattedItemStems } from "../testBuilder/helpers";
import withRouter from "../withRouter";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { LANGUAGES } from "../commons/Translation";

class ItemBankInstruction extends Component {
  render() {
    // format the found content (seperate with new lines)
    const stemsObj = getFormattedItemStems(
      this.props.instruction.pageSection.page_section_content.content,
      "--",
      this.props.instruction.pageSection.page_section_content.content_type_id
    );

    return (
      <div>
        <ReactMarkdown
          rehypePlugins={[rehypeRaw]}
          // eslint-disable-next-line react/no-children-prop
          children={
            this.props.currentLanguage === LANGUAGES.english ? stemsObj.stemsEn : stemsObj.stemsFr
          }
          // add className="notranslate" to 'p' tags to prevent Microsoft Edge from translating
          components={{
            p: ({ node, ...props }) => <p className="notranslate" {...props} />
          }}
        />
      </div>
    );
  }
}

export { ItemBankInstruction as UnconnectedItemBankInstruction };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => {
  bindActionCreators({}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ItemBankInstruction));
