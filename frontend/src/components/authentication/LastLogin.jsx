import React from "react";
import LOCALIZE from "../../text_resources";
import PropTypes from "prop-types";
import moment from "moment";

const styles = {
  lastLogin: {
    paddingTop: "10px",
    marginLeft: "5px",
    textAlign: "left"
  }
};

const LastLogin = props => {
  const dateString = props.lastLoginDate;
  const formattedDate = moment(dateString).format("YYYY-MM-DD");
  return (
    <div>
      <p style={styles.lastLogin} className="notranslate">
        {LOCALIZE.formatString(
          LOCALIZE.dashboard.lastLogin,
          dateString !== "" ? formattedDate : LOCALIZE.dashboard.lastLoginNever
        )}
      </p>
    </div>
  );
};

LastLogin.propTypes = {
  lastLoginDate: PropTypes.string.isRequired
};
export default LastLogin;
