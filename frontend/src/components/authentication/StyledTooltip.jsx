import React, { Component } from "react";
import PropTypes from "prop-types";
import { Tooltip } from "react-tooltip";
import { connect } from "react-redux";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import "../../css/styled-tooltip.css";

export const TYPE = {
  dark: "dark",
  error: "error",
  info: "info",
  light: "light",
  success: "success",
  warning: "warning"
};

export const EFFECT = {
  float: "float",
  solid: "solid"
};

class StyledTooltip extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      tooltipElement: PropTypes.object.isRequired,
      tooltipContent: PropTypes.object.isRequired,
      customStyle: PropTypes.object,
      // Used to determine if you have to click on the element to see the tooltip
      openOnClick: PropTypes.bool,
      // Used to make the element focusable when going through the app using tab
      // Note: the tooltip will be shown automatically if you focus on the element
      isFocusable: PropTypes.bool,
      id: PropTypes.string.isRequired
    };
    StyledTooltip.defaultProps = {
      openOnClick: false,
      isFocusable: true
    };
  }

  render() {
    let style = {
      fontFamily: this.props.accommodations.fontFamily,
      fontSize: this.props.accommodations.fontSize
    };
    if (this.props.accommodations.spacing) {
      style = { ...style, ...getLineSpacingCSS() };
    }

    const tooltipContent = (
      <div style={{ ...style, ...this.props.customStyle }}>{this.props.tooltipContent}</div>
    );
    return (
      <>
        <span
          tabIndex={this.props.isFocusable ? "0" : "-1"}
          data-tooltip-id={this.props.id}
          data-tooltip-delay-hide={1}
        >
          {this.props.tooltipElement}
        </span>
        <Tooltip
          {...this.props}
          className="react-tooltip-custom-class"
          variant="light"
          border="1px solid #00565e"
          openOnClick={this.props.openOnClick}
          style={{ zIndex: 9999 }}
          id={this.props.id}
          positionStrategy="fixed"
          hide={0}
        >
          {tooltipContent}
        </Tooltip>
      </>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations
  };
};

export default connect(mapStateToProps, null)(StyledTooltip);
