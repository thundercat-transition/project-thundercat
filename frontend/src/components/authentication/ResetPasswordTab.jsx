import React from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import ResetPasswordForm from "./ResetPasswordForm";

import { Container, Col } from "react-bootstrap";
import { useTabState, Tab, TabList, TabPanel } from "reakit/Tab";

const styles = {
  resetPasswordComponent: {
    maxWidth: 600,
    margin: "0 auto",
    position: "absolute",
    left: 0,
    right: 0,
    padding: "24px 0"
  },
  menuTabStyle: {
    backgroundColor: "#fafafa",
    padding: 8,
    border: "2px solid transparent",
    borderTopLeftRadius: "0.35rem",
    borderTopRightRadius: "0.35rem",
    borderBottomWidth: "4px",
    color: "#00565e",
    borderColor: "transparent transparent #00565e",
    marginBottom: "-2px"
  }
};

function ResetPasswordTabHooks(props) {
  const tab = useTabState();
  return (
    <Container>
      <Col style={styles.resetPasswordComponent}>
        <TabList {...tab}>
          <Tab {...tab} id={"tab1"} style={styles.menuTabStyle}>
            {LOCALIZE.authentication.resetPassword.title}
          </Tab>
        </TabList>
        <TabPanel {...tab}>
          <ResetPasswordForm passwordResetToken={props.passwordResetToken} />
        </TabPanel>
      </Col>
    </Container>
  );
}

function withMyHook(Component) {
  return function WrappedComponent(props) {
    const myHookValue = ResetPasswordTabHooks(props);
    return <Component {...props} myHookValue={myHookValue} />;
  };
}

class ResetPasswordTab extends React.Component {
  static propTypes = {
    passwordResetToken: PropTypes.string.isRequired
  };

  render() {
    const { myHookValue } = this.props;

    return <>{myHookValue}</>;
  }
}

export default withMyHook(ResetPasswordTab);
