import React, { Component } from "react";
import { bindActionCreators } from "redux";
import LoginForm from "./LoginForm";
import { updateShow2FA } from "../../modules/LoginRedux";
import RegistrationForm from "./RegistrationForm";
import LOCALIZE from "../../text_resources";
import { Container } from "react-bootstrap";
import { useTabState, Tab, TabList, TabPanel } from "reakit/Tab";
import { connect } from "react-redux";
import TwoFactorForm from "./TwoFactorForm";

const styles = {
  row: {
    position: "relative"
  },
  bottomMargin: {
    marginBottom: 32
  },
  menuTabStyle: {
    backgroundColor: "#fafafa",
    padding: 8,
    border: "2px solid transparent",
    borderTopLeftRadius: "0.35rem",
    borderTopRightRadius: "0.35rem",
    borderBottomWidth: "4px",
    color: "#00565e",
    borderColor: "transparent transparent #00565e",
    marginBottom: "-2px"
  },
  menuTabInactive: {
    backgroundColor: "#fafafa",
    padding: 8,
    border: "none"
  },
  mainContainer: {
    maxWidth: "800px"
  },
  disabled: {
    opacity: 0.4,
    cursor: "not-allowed"
  },
  visibilityHidden: {
    visibility: "hidden"
  }
};

function AuthenticationTabsHooks(props) {
  const tab = useTabState();
  if (props.show2FA) {
    tab.selectedId = "tab3";
  }
  return (
    <Container
      style={
        parseInt(props.accommodations.fontSize.split("px")[0]) < 30 ? styles.mainContainer : {}
      }
    >
      <TabList {...tab}>
        <Tab
          {...tab}
          id={"tab1"}
          style={{
            ...(tab.selectedId === "tab1" ? styles.menuTabStyle : styles.menuTabInactive),
            ...(props.show2FA ? styles.disabled : {})
          }}
          disabled={props.show2FA}
        >
          {LOCALIZE.authentication.login.title}
        </Tab>
        <Tab
          {...tab}
          id={"tab2"}
          style={{
            ...(tab.selectedId === "tab2" ? styles.menuTabStyle : styles.menuTabInactive),
            ...(props.show2FA ? styles.disabled : {})
          }}
          disabled={props.show2FA}
        >
          {LOCALIZE.authentication.createAccount.title}
        </Tab>
        <Tab
          {...tab}
          id={"tab3"}
          style={{
            ...(tab.selectedId === "tab3" ? styles.menuTabStyle : styles.menuTabInactive),
            ...(props.show2FA === false ? styles.visibilityHidden : {})
          }}
        >
          {LOCALIZE.authentication.twoFactorAuth.title}
        </Tab>
      </TabList>
      <TabPanel {...tab}>
        <LoginForm authorizeAfter2FA={props.authorizeAfter2FA} />
      </TabPanel>
      <TabPanel {...tab}>
        <RegistrationForm />
      </TabPanel>
      <TabPanel {...tab}>
        <TwoFactorForm authorizeAfter2FA={props.authorizeAfter2FA} />
      </TabPanel>
    </Container>
  );
}

function withMyHook(Component) {
  return function WrappedComponent(props) {
    const myHookValue = AuthenticationTabsHooks(props);
    return <Component {...props} myHookValue={myHookValue} />;
  };
}

class AuthenticationTabs extends Component {
  render() {
    const { myHookValue } = this.props;

    return <>{myHookValue}</>;
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    show2FA: state.login.show2FA
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateShow2FA
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withMyHook(AuthenticationTabs));
