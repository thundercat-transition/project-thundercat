import React, { Component } from "react";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import { Row, Col } from "react-bootstrap";
import withRouter from "../withRouter";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { reSend2FACodeToUser, check2FACodeForUser } from "../../modules/Login2FARedux";
import { logoutAction, updateShow2FA } from "../../modules/LoginRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faSpinner, faCheck } from "@fortawesome/free-solid-svg-icons";

const styles = {
  twoFAContent: {
    padding: "12px 32px 0 32px",
    border: "1px solid #cdcdcd"
  },
  inputTitles: {
    fontWeight: "bold"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  successMessage: {
    color: "#749F41",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  formRow: {
    padding: "15px 0 15px 0"
  },
  resendCodeButton: {
    background: "transparent",
    border: "none",
    color: "#0278A4",
    fontWeight: "inherit",
    padding: 4
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  buttonLeft: {
    marginTop: "24px",
    marginBottom: "24px",
    marginLeft: "0",
    marginRight: "24px"
  },
  buttonRight: {
    marginTop: "24px",
    marginBottom: "24px",
    marginLeft: "24px",
    marginRight: "0",
    float: "right"
  }
};

class TwoFactorForm extends Component {
  static propTypes = {
    logoutAction: PropTypes.func,
    updateShow2FA: PropTypes.func,
    reSend2FACodeToUser: PropTypes.func,
    check2FACodeForUser: PropTypes.func
  };

  state = {
    code: "",
    isWrongCode: false,
    isCodeExpired: false,
    currentlyLoading: false,
    isCodeResent: false
  };

  handleSubmit = event => {
    event.preventDefault(); // this prevents the force refresh which kiboshes the authetication
    // this.setState({ currentlyLoading: true });
    const { code } = this.state;
    this.props.check2FACodeForUser(this.props.user_id, code).then(response => {
      // handle the code
      switch (response.status) {
        case 404: // code DNE for this user
          this.setState({
            currentlyLoading: false,
            isWrongCode: true,
            isCodeExpired: false,
            isCodeResent: false
          });
          break;
        case 408: // code was sent over 15 minutes ago
          this.setState({
            currentlyLoading: false,
            isWrongCode: false,
            isCodeExpired: true,
            isCodeResent: false
          });
          break;
        case 200: // successfully found the code, deleted the row in the table
          this.props.authorizeAfter2FA();
          break;
        default: // should NEVER go here, logout and get out of there
          this.props.logoutAction();
          throw new Error("Something went wrong when verifying the 2 factor authentication code");
      }
    });
  };

  handleResendCode = () => {
    this.setState({
      currentlyLoading: false,
      isWrongCode: false,
      isCodeExpired: false,
      code: "",
      isCodeResent: true
    });
    this.props.reSend2FACodeToUser(this.props.user_id);
  };

  handleCodeChange = event => {
    this.setState({ code: event.target.value });
  };

  handleCancel = _ => {
    this.props.updateShow2FA(false, "");
  };

  render() {
    const accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    return (
      <div>
        <div role="form">
          <div style={styles.twoFAContent}>
            <Row>
              <Col xl={12} lg={12} md={12} sm={12}>
                <h2>{LOCALIZE.authentication.twoFactorAuth.content.title}</h2>
              </Col>
            </Row>
            <p>{LOCALIZE.authentication.twoFactorAuth.content.description}</p>
            <form onSubmit={this.handleSubmit}>
              <Row style={styles.formRow}>
                <Col xl={12} lg={12} md={12} sm={12} style={styles.inputTitles}>
                  <label htmlFor={"code"}>
                    {LOCALIZE.authentication.twoFactorAuth.content.label}
                  </label>
                </Col>
                <Col xl={12} lg={12} md={12} sm={12}>
                  <input
                    className={!this.state.isWrongCode ? "valid-field" : "invalid-field"}
                    aria-required={"true"}
                    type="text"
                    id="code"
                    style={{ ...styles.inputs, ...accommodationsStyle }}
                    onChange={this.handleCodeChange}
                    value={this.state.code}
                  />
                </Col>
              </Row>

              <Row role="presentation">
                <Col xl={6} lg={6} md={6} sm={6}>
                  {this.state.isWrongCode && (
                    <label
                      id="invalid-code-error-label"
                      htmlFor={"code"}
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.authentication.twoFactorAuth.content.invalidCode}
                    </label>
                  )}
                  {this.state.isCodeExpired && (
                    <label
                      id="expired-code-error-label"
                      htmlFor={"code"}
                      style={styles.errorMessage}
                      className="notranslate"
                    >
                      {LOCALIZE.authentication.twoFactorAuth.content.expiredCode}
                    </label>
                  )}
                  {this.state.isCodeResent && (
                    <label id="email-sent-successfully" style={styles.successMessage} tabIndex={0}>
                      {LOCALIZE.authentication.twoFactorAuth.content.newCode}
                    </label>
                  )}
                </Col>
                <Col xl={6} lg={6} md={6} sm={6} className="align-top text-right">
                  <CustomButton
                    label={LOCALIZE.authentication.twoFactorAuth.content.resend}
                    action={this.handleResendCode}
                    type={"button"}
                    customStyle={styles.resendCodeButton}
                    buttonTheme={THEME.SECONDARY}
                    className="text-right"
                  />
                </Col>
              </Row>
              <Row>
                <Col xl={6} lg={6} md={6} sm={6} className="align-top text-left">
                  <CustomButton
                    label={
                      <>
                        <FontAwesomeIcon icon={faTimes} />
                        <span style={styles.buttonLabel} className="notranslate">
                          {LOCALIZE.commons.cancel}
                        </span>
                      </>
                    }
                    type={"button"}
                    customStyle={styles.buttonLeft}
                    buttonTheme={THEME.SECONDARY}
                    disabled={this.state.currentlyLoading}
                    action={this.handleCancel}
                  />
                </Col>
                <Col xl={6} lg={6} md={6} sm={6} className="align-top text-right">
                  <CustomButton
                    label={
                      this.state.currentlyLoading ? (
                        // eslint-disable-next-line jsx-a11y/label-has-associated-control
                        <label className="fa fa-spinner fa-spin">
                          <FontAwesomeIcon icon={faSpinner} />
                        </label>
                      ) : (
                        <>
                          <FontAwesomeIcon icon={faCheck} />
                          <span style={styles.buttonLabel} className="notranslate">
                            {LOCALIZE.commons.confirm}
                          </span>
                        </>
                      )
                    }
                    type={"submit"}
                    customStyle={styles.buttonRight}
                    buttonTheme={THEME.PRIMARY}
                    disabled={this.state.currentlyLoading}
                  />
                </Col>
              </Row>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    show2FA: state.login.show2FA,
    user_id: state.login.user_id
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      logoutAction,
      updateShow2FA,
      reSend2FACodeToUser,
      check2FACodeForUser
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TwoFactorForm));
