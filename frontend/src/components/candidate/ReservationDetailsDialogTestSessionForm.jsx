import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Col, Row } from "react-bootstrap";
import PropTypes from "prop-types";
import { LANGUAGES } from "../commons/Translation";
import DatePicker from "../commons/DatePicker";
import DropdownSelect from "../commons/DropdownSelect";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import formatMaskedPhoneNumber, { MASKS, MASK_FORMAT_CHARS } from "../commons/InputMask/helpers";
import InputMask from "react-input-mask";
import getCheckboxTransformScale from "../../helpers/checkboxTransformScale";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../commons/PopupBox";
import {
  editTestSessionPhoneNumber,
  setReserveTestSessionPhoneNumberState,
  setReserveTestSessionValidFormStateState
} from "../../modules/CandidateRedux";
import TermsOfUseContent from "./TermsOfUseContent";
import PrivacyNoticeStatementContent from "./PrivacyNoticeStatementContent";
import { RESERVATION_DETAILS_DIALOG_SOURCE } from "./ReservationDetailsDialog";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { faPencil, faCheck, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  },
  secondColumnTime: {
    xs: 6,
    sm: 6,
    md: 6,
    lg: 2,
    xl: 2
  },
  secondColumnPhoneNumber: {
    xs: 6,
    sm: 6,
    md: 6,
    lg: 4,
    xl: 4
  },
  thirdColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 1,
    xl: 1
  }
};

const styles = {
  multiSessionsMainContainer: {
    border: "1px solid #00565e",
    padding: 12
  },
  fieldRowContainer: {
    margin: "12px auto",
    width: "90%"
  },
  formLabel: {
    fontWeight: "bold",
    margin: 0
  },
  indentedFormLabel: {
    fontWeight: "bold",
    margin: 0,
    paddingLeft: 12
  },
  timeLabel: {
    textAlign: "center",
    marginTop: "-8px"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  additionalInfoParagraph: {
    padding: "0 0 0 36px",
    whiteSpace: "pre-wrap"
  },
  checkboxSectionContainer: {
    margin: "48px 0 24px 0"
  },
  checkboxContainer: {
    display: "flex",
    alignItems: "center"
  },
  checkbox: {
    verticalAlign: "middle",
    marginRight: 16
  },
  checkboxLink: {
    textDecoration: "underline",
    color: "#0278A4",
    cursor: "pointer",
    backgroundColor: "transparent",
    border: "none",
    padding: 0
  },
  noMargin: {
    margin: 0
  },
  allUnset: {
    all: "unset"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  editPhoneNumberButtonCol: {
    textAlign: "left",
    padding: 0
  }
};

class ReservationDetailsDialogTestSessionForm extends Component {
  static propTypes = {
    isOla: PropTypes.bool.isRequired,
    testSessionFormData: PropTypes.object.isRequired,
    multiSessions: PropTypes.bool.isRequired,
    source: PropTypes.string.isRequired
  };

  state = {
    showEditPhoneNumberPopup: false,
    phoneNumberContent:
      typeof this.props.testSessionFormData.candidate_phone_number !== "undefined" &&
      this.props.testSessionFormData.candidate_phone_number !== null
        ? this.props.testSessionFormData.candidate_phone_number
        : "",
    // phone number state needed for the edit functionality
    tempPhoneNumberContent: "",
    isValidTempPhoneNumberContent: false,
    showTermsOfUsePopup: false,
    mainTermsOfUseChecked: false,
    termsOfUseChecked: false,
    showPrivacyNoticeStatementPopup: false,
    mainPrivacyNoticeStatementChecked: false,
    privacyNoticeStatementChecked: false
  };

  componentDidMount = () => {
    // initializing redux states
    this.props.setReserveTestSessionPhoneNumberState("");

    // if quality of life is enabled, populate the phone number
    if (this.props.qualityOfLife) {
      const tempNumber = formatMaskedPhoneNumber("1111111111");
      this.setState({ phoneNumberContent: tempNumber });
      this.props.setReserveTestSessionPhoneNumberState(tempNumber);
    }

    // validating form
    this.validateForm();
  };

  openEditPhoneMumberPopup = () => {
    this.setState({
      showEditPhoneNumberPopup: true,
      tempPhoneNumberContent: this.state.phoneNumberContent,
      isValidTempPhoneNumberContent: true
    });
  };

  // updating Phone Number (temp phone number for the edit functionality)
  updateTempPhoneNumber = event => {
    const tempPhoneNumberContent = formatMaskedPhoneNumber(event.target.value);
    const isValidTempPhoneNumberContent = tempPhoneNumberContent.length === 10;
    this.setState({
      tempPhoneNumberContent: tempPhoneNumberContent,
      isValidTempPhoneNumberContent: isValidTempPhoneNumberContent
    });
  };

  handleEditPhoneNumber = () => {
    // updating phone number in DB
    this.props
      .editTestSessionPhoneNumber(
        parseInt(this.props.testSessionFormData.id),
        this.state.tempPhoneNumberContent
      )
      .then(response => {
        // if success request
        if (response.ok) {
          // updating states
          this.setState({ phoneNumberContent: this.state.tempPhoneNumberContent });
          // updating redux states
          this.props.setReserveTestSessionPhoneNumberState(
            formatMaskedPhoneNumber(this.state.tempPhoneNumberContent)
          );
          // closing popup
          this.closeEditPhoneMumberPopup();
          // should never happen
        } else {
          throw new Error("An error occurred during the Edit Phone Number Process");
        }
      });
  };

  closeEditPhoneMumberPopup = () => {
    this.setState({
      showEditPhoneNumberPopup: false,
      tempPhoneNumberContent: "",
      isValidTempPhoneNumberContent: true
    });
  };

  // update Phone Number
  updatePhoneNumber = event => {
    const phoneNumberContent = event.target.value;
    this.setState(
      {
        phoneNumberContent
      },
      () => {
        // updating redux states
        this.props.setReserveTestSessionPhoneNumberState(
          formatMaskedPhoneNumber(phoneNumberContent)
        );
        // validating form
        this.validateForm();
      }
    );
  };

  openTermsOfUsePopup = event => {
    this.setState({
      showTermsOfUsePopup: true,
      termsOfUseChecked: this.state.mainTermsOfUseChecked
    });
    event.preventDefault();
  };

  handleAcceptTermsOfUseCheckbox = event => {
    this.setState({ termsOfUseChecked: event.target.checked });
  };

  handleAcceptTermsOfUse = () => {
    this.setState({ mainTermsOfUseChecked: true, showTermsOfUsePopup: false }, () => {
      // validating form
      this.validateForm();
    });
  };

  closeTermsOfUsePopup = () => {
    this.setState(
      {
        showTermsOfUsePopup: false,
        termsOfUseChecked: false,
        mainTermsOfUseChecked: false
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  openPrivacyNoticeStatementPopup = event => {
    this.setState({
      showPrivacyNoticeStatementPopup: true,
      privacyNoticeStatementChecked: this.state.mainPrivacyNoticeStatementChecked
    });
    event.preventDefault();
  };

  handleAcceptPrivacyNoticeStatementCheckbox = event => {
    this.setState({ privacyNoticeStatementChecked: event.target.checked });
  };

  handleAcceptPrivacyNoticeStatement = () => {
    this.setState(
      { mainPrivacyNoticeStatementChecked: true, showPrivacyNoticeStatementPopup: false },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  closePrivacyNoticeStatementPopup = () => {
    this.setState(
      {
        showPrivacyNoticeStatementPopup: false,
        privacyNoticeStatementChecked: false,
        mainPrivacyNoticeStatementChecked: false
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  validateForm = () => {
    // initializing needed variables
    let isValidForm = false;
    const isValidPhoneNumber = formatMaskedPhoneNumber(this.state.phoneNumberContent).length === 10;
    let isValidTermsOfUse = true;
    let isValidPrivacyNoticeStatement = true;
    // called from makeReservation
    if (this.props.source === RESERVATION_DETAILS_DIALOG_SOURCE.makeReservation) {
      isValidTermsOfUse = this.state.mainTermsOfUseChecked;
      isValidPrivacyNoticeStatement = this.state.mainPrivacyNoticeStatementChecked;
    }

    // not an OLA test session
    if (!this.props.isOla) {
      // valid form (there is nothing to validate)
      isValidForm = true;
      // OLA test session
    } else {
      // all needed fields/checkboxes are valid
      if (isValidPhoneNumber && isValidTermsOfUse && isValidPrivacyNoticeStatement) {
        // valid form
        isValidForm = true;
      }
    }

    // updating redux states
    this.props.setReserveTestSessionValidFormStateState(isValidForm);
  };

  render() {
    const { isOla, testSessionFormData, multiSessions } = this.props;

    const {
      test_center_address_text,
      test_center_city_text,
      test_center_province_text,
      test_center_other_details_text,
      test_center_postal_code,
      country_edesc,
      country_fdesc,
      dept_eabrv,
      dept_edesc,
      dept_fabrv,
      dept_fdesc,
      test_room_other_details_text
    } = testSessionFormData;

    const sessionDate = `${testSessionFormData.date}`;
    const sessionStartTime = `${testSessionFormData.simplified_start_time}`;
    const sessionEndTime = `${testSessionFormData.simplified_end_time}`;

    // formatting test skill type / sub-type
    let formattedTestSkill =
      testSessionFormData[`test_skill_type_${this.props.currentLanguage}_name`];
    if (testSessionFormData.test_skill_sub_type_id !== null) {
      formattedTestSkill += ` - ${
        testSessionFormData[`test_skill_sub_type_${this.props.currentLanguage}_name`]
      }`;
    }

    // formatting location
    let formattedLocation = "";
    if (!isOla) {
      formattedLocation =
        this.props.currentLanguage === LANGUAGES.english
          ? `${test_center_address_text[this.props.currentLanguage][0].text}, ${
              test_center_city_text[this.props.currentLanguage][0].text
            }, ${
              test_center_province_text[this.props.currentLanguage][0].text
            }, ${test_center_postal_code}, ${country_edesc}`
          : `${test_center_address_text[this.props.currentLanguage][0].text}, ${
              test_center_city_text[this.props.currentLanguage][0].text
            }, ${
              test_center_province_text[this.props.currentLanguage][0].text
            }, ${test_center_postal_code}, ${country_fdesc}`;
    }

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    return (
      <div style={multiSessions ? styles.multiSessionsMainContainer : {}}>
        {/* test session date, start time and end time will only be displayed is "date" is defined (might not be defined for withdraw from process when accommodation is not complete) */}
        {testSessionFormData.date !== null && (
          <>
            <Row
              role="presentation"
              className="align-items-center"
              style={styles.fieldRowContainer}
            >
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
              >
                <label id="reservation-details-test-session-date-label" style={styles.formLabel}>
                  {
                    LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                      .testSessionDateLabel
                  }
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
              >
                <DatePicker
                  dateLabelId={"reservation-details-test-session-date"}
                  ariaLabelledBy={"reservation-details-test-session-date-label"}
                  initialDateDayValue={{
                    value: Number(sessionDate.split("-")[2]),
                    label: `${sessionDate.split("-")[2]}`
                  }}
                  initialDateMonthValue={{
                    value: Number(sessionDate.split("-")[1]),
                    label: `${sessionDate.split("-")[1]}`
                  }}
                  initialDateYearValue={{
                    value: Number(sessionDate.split("-")[0]),
                    label: `${sessionDate.split("-")[0]}`
                  }}
                  disabledDropdowns={true}
                />
              </Col>
            </Row>
            <Row
              role="presentation"
              className="align-items-center"
              style={styles.fieldRowContainer}
            >
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
              >
                <label
                  id="reservation-details-test-session-start-time-label"
                  style={styles.formLabel}
                >
                  {
                    LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                      .testSessionStartTimeLabel
                  }
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumnTime.xl}
                lg={columnSizes.secondColumnTime.lg}
                md={columnSizes.secondColumnTime.md}
                sm={columnSizes.secondColumnTime.sm}
                xs={columnSizes.secondColumnTime.xs}
              >
                <DropdownSelect
                  idPrefix="reservation-details-test-session-start-time-hour"
                  ariaLabelledBy="reservation-details-test-session-start-time-label reservation-details-test-session-start-time-hour-label"
                  options={[]}
                  onChange={() => {}}
                  defaultValue={{
                    label: sessionStartTime.split(":")[0],
                    value: parseInt(sessionStartTime.split(":")[0])
                  }}
                  isDisabled={true}
                />
              </Col>
              <Col
                xl={columnSizes.secondColumnTime.xl}
                lg={columnSizes.secondColumnTime.lg}
                md={columnSizes.secondColumnTime.md}
                sm={columnSizes.secondColumnTime.sm}
                xs={columnSizes.secondColumnTime.xs}
              >
                <DropdownSelect
                  idPrefix="reservation-details-test-session-start-time-minute"
                  ariaLabelledBy="reservation-details-test-session-start-time-label reservation-details-test-session-start-time-minute-label"
                  options={[]}
                  onChange={() => {}}
                  defaultValue={{
                    label: sessionStartTime.split(":")[1],
                    value: parseInt(sessionStartTime.split(":")[1])
                  }}
                  isDisabled={true}
                />
              </Col>
            </Row>
            <Row
              role="presentation"
              className="align-items-center"
              style={styles.fieldRowContainer}
            >
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
                style={styles.timeLabel}
              />
              <Col
                xl={columnSizes.secondColumnTime.xl}
                lg={columnSizes.secondColumnTime.lg}
                md={columnSizes.secondColumnTime.md}
                sm={columnSizes.secondColumnTime.sm}
                xs={columnSizes.secondColumnTime.xs}
                style={styles.timeLabel}
              >
                <label id="reservation-details-test-session-start-time-hour-label">
                  {LOCALIZE.datePicker.hourField}
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumnTime.xl}
                lg={columnSizes.secondColumnTime.lg}
                md={columnSizes.secondColumnTime.md}
                sm={columnSizes.secondColumnTime.sm}
                xs={columnSizes.secondColumnTime.xs}
                style={styles.timeLabel}
              >
                <label id="reservation-details-test-session-start-time-minute-label">
                  {LOCALIZE.datePicker.minuteField}
                </label>
              </Col>
            </Row>
            <Row
              role="presentation"
              className="align-items-center"
              style={styles.fieldRowContainer}
            >
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
              >
                <label
                  id="reservation-details-test-session-end-time-label"
                  style={styles.formLabel}
                >
                  {
                    LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                      .testSessionEndTimeLabel
                  }
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumnTime.xl}
                lg={columnSizes.secondColumnTime.lg}
                md={columnSizes.secondColumnTime.md}
                sm={columnSizes.secondColumnTime.sm}
                xs={columnSizes.secondColumnTime.xs}
              >
                <DropdownSelect
                  idPrefix="reservation-details-test-session-end-time-hour"
                  ariaLabelledBy="reservation-details-test-session-end-time-label reservation-details-test-session-end-time-hour-label"
                  options={[]}
                  onChange={() => {}}
                  defaultValue={{
                    label: sessionEndTime.split(":")[0],
                    value: parseInt(sessionEndTime.split(":")[0])
                  }}
                  isDisabled={true}
                />
              </Col>
              <Col
                xl={columnSizes.secondColumnTime.xl}
                lg={columnSizes.secondColumnTime.lg}
                md={columnSizes.secondColumnTime.md}
                sm={columnSizes.secondColumnTime.sm}
                xs={columnSizes.secondColumnTime.xs}
              >
                <DropdownSelect
                  idPrefix="reservation-details-test-session-end-time-minute"
                  ariaLabelledBy="reservation-details-test-session-end-time-label reservation-details-test-session-end-time-minute-label"
                  options={[]}
                  onChange={() => {}}
                  defaultValue={{
                    label: sessionEndTime.split(":")[1],
                    value: parseInt(sessionEndTime.split(":")[1])
                  }}
                  isDisabled={true}
                />
              </Col>
            </Row>
            <Row
              role="presentation"
              className="align-items-center"
              style={styles.fieldRowContainer}
            >
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
                style={styles.timeLabel}
              />
              <Col
                xl={columnSizes.secondColumnTime.xl}
                lg={columnSizes.secondColumnTime.lg}
                md={columnSizes.secondColumnTime.md}
                sm={columnSizes.secondColumnTime.sm}
                xs={columnSizes.secondColumnTime.xs}
                style={styles.timeLabel}
              >
                <label id="reservation-details-test-session-end-time-hour-label">
                  {LOCALIZE.datePicker.hourField}
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumnTime.xl}
                lg={columnSizes.secondColumnTime.lg}
                md={columnSizes.secondColumnTime.md}
                sm={columnSizes.secondColumnTime.sm}
                xs={columnSizes.secondColumnTime.xs}
                style={styles.timeLabel}
              >
                <label id="reservation-details-test-session-end-time-minute-label">
                  {LOCALIZE.datePicker.minuteField}
                </label>
              </Col>
            </Row>
          </>
        )}
        <Row role="presentation" className="align-items-center" style={styles.fieldRowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label htmlFor="reservation-details-test-skill-input" style={styles.formLabel}>
              {LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup.testSkillLabel}
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
          >
            <input
              id="reservation-details-test-skill-input"
              className={"valid-field input-disabled-look"}
              style={{ ...styles.input, ...accommodationsStyle }}
              type="text"
              value={formattedTestSkill}
              onChange={() => {}}
              aria-disabled={true}
            ></input>
          </Col>
        </Row>
        {/* not an OLA test */}
        {!isOla && (
          <>
            <Row
              role="presentation"
              className="align-items-center"
              style={styles.fieldRowContainer}
            >
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
              >
                <label htmlFor="reservation-details-location-input" style={styles.formLabel}>
                  {
                    LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                      .departmentLabel
                  }
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
              >
                <input
                  id="reservation-details-location-input"
                  className={"valid-field input-disabled-look"}
                  style={{ ...styles.input, ...accommodationsStyle }}
                  type="text"
                  value={
                    this.props.currentLanguage === LANGUAGES.english
                      ? `${dept_edesc} (${dept_eabrv})`
                      : `${dept_fdesc} (${dept_fabrv})`
                  }
                  onChange={() => {}}
                  aria-disabled={true}
                ></input>
              </Col>
            </Row>
            <Row
              role="presentation"
              className="align-items-center"
              style={styles.fieldRowContainer}
            >
              <Col
                xl={columnSizes.firstColumn.xl}
                lg={columnSizes.firstColumn.lg}
                md={columnSizes.firstColumn.md}
                sm={columnSizes.firstColumn.sm}
                xs={columnSizes.firstColumn.xs}
              >
                <label htmlFor="reservation-details-location-input" style={styles.formLabel}>
                  {
                    LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                      .locationLabel
                  }
                </label>
              </Col>
              <Col
                xl={columnSizes.secondColumn.xl}
                lg={columnSizes.secondColumn.lg}
                md={columnSizes.secondColumn.md}
                sm={columnSizes.secondColumn.sm}
                xs={columnSizes.secondColumn.xs}
              >
                <input
                  id="reservation-details-location-input"
                  className={"valid-field input-disabled-look"}
                  style={{ ...styles.input, ...accommodationsStyle }}
                  type="text"
                  value={formattedLocation}
                  onChange={() => {}}
                  aria-disabled={true}
                ></input>
              </Col>
            </Row>
          </>
        )}
        {/* Test Center Additional Info OR Room Additional Info is defined */}
        {typeof test_center_other_details_text !== "undefined" &&
          Object.keys(test_center_other_details_text).length > 0 &&
          (test_center_other_details_text[this.props.currentLanguage][0].text !== "" ||
            (test_room_other_details_text !== null &&
              test_room_other_details_text[this.props.currentLanguage][0].text !== "")) && (
            <>
              <Row
                role="presentation"
                className="align-items-center"
                style={styles.fieldRowContainer}
              >
                <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                  <div>
                    <label style={styles.formLabel}>
                      {
                        LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                          .additionalDetailsLabel
                      }
                    </label>
                  </div>
                </Col>
              </Row>
              {test_center_other_details_text[this.props.currentLanguage][0].text !== "" && (
                <>
                  <Row role="presentation" style={styles.fieldRowContainer}>
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label style={styles.indentedFormLabel}>
                        {
                          LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                            .testCenterAdditionalInfoLabel
                        }
                      </label>
                    </Col>
                  </Row>
                  <Row role="presentation" style={styles.fieldRowContainer}>
                    <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                      <p style={styles.additionalInfoParagraph}>
                        {test_center_other_details_text[this.props.currentLanguage][0].text}
                      </p>
                    </Col>
                  </Row>
                </>
              )}
              {test_room_other_details_text !== null &&
                test_room_other_details_text[this.props.currentLanguage][0].text !== "" && (
                  <>
                    <Row role="presentation" style={styles.fieldRowContainer}>
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      >
                        <label style={styles.indentedFormLabel}>
                          {
                            LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                              .roomAdditionalInfo
                          }
                        </label>
                      </Col>
                    </Row>
                    <Row role="presentation" style={styles.fieldRowContainer}>
                      <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                        <p style={styles.additionalInfoParagraph}>
                          {test_room_other_details_text[this.props.currentLanguage][0].text}
                        </p>
                      </Col>
                    </Row>
                  </>
                )}
            </>
          )}
        {/* OLA (without any accommodations) */}
        {isOla &&
          (typeof testSessionFormData.user_accommodation_file_id === "undefined" ||
            testSessionFormData.user_accommodation_file_id === null) && (
            <>
              <Row
                role="presentation"
                className="align-items-center"
                style={styles.fieldRowContainer}
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label
                    id="reservation-details-phone-number-label"
                    htmlFor="reservation-details-phone-number-input"
                    style={styles.formLabel}
                  >
                    {
                      LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                        .phoneNumber
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumnPhoneNumber.xl}
                  lg={columnSizes.secondColumnPhoneNumber.lg}
                  md={columnSizes.secondColumnPhoneNumber.md}
                  sm={columnSizes.secondColumnPhoneNumber.sm}
                  xs={columnSizes.secondColumnPhoneNumber.xs}
                >
                  <InputMask
                    id="reservation-details-phone-number-input"
                    className={
                      this.props.source === RESERVATION_DETAILS_DIALOG_SOURCE.manageReservation
                        ? "valid-field input-disabled-look"
                        : "valid-field"
                    }
                    aria-labelledby="reservation-details-phone-number-label"
                    aria-required={true}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.phoneNumberContent}
                    mask={MASKS.phoneNumber}
                    formatChars={MASK_FORMAT_CHARS.phoneNumber}
                    onChange={this.updatePhoneNumber}
                    aria-disabled={
                      this.props.source === RESERVATION_DETAILS_DIALOG_SOURCE.manageReservation
                    }
                  />
                </Col>
                {this.props.source === RESERVATION_DETAILS_DIALOG_SOURCE.manageReservation && (
                  <Col
                    xl={columnSizes.thirdColumn.xl}
                    lg={columnSizes.thirdColumn.lg}
                    md={columnSizes.thirdColumn.md}
                    sm={columnSizes.thirdColumn.sm}
                    xs={columnSizes.thirdColumn.xs}
                    style={styles.editPhoneNumberButtonCol}
                  >
                    <StyledTooltip
                      id="reservation-details-phone-number-edit-button"
                      place="top"
                      variant={TYPE.light}
                      effect={EFFECT.solid}
                      openOnClick={false}
                      tooltipElement={
                        <div style={styles.allUnset}>
                          <CustomButton
                            dataTip=""
                            dataFor="reservation-details-phone-number-edit-button"
                            label={<FontAwesomeIcon icon={faPencil} />}
                            ariaLabel={LOCALIZE.commons.editButton}
                            action={this.openEditPhoneMumberPopup}
                            customStyle={styles.actionButton}
                            buttonTheme={THEME.PRIMARY}
                          />
                        </div>
                      }
                      tooltipContent={
                        <div>
                          <p>{LOCALIZE.commons.editButton}</p>
                        </div>
                      }
                    />
                  </Col>
                )}
              </Row>
              {/* called from makeReservation */}
              {this.props.source === RESERVATION_DETAILS_DIALOG_SOURCE.makeReservation && (
                <div style={styles.checkboxSectionContainer}>
                  <Row
                    role="presentation"
                    className="align-items-center"
                    style={styles.fieldRowContainer}
                  >
                    <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                      <div style={styles.checkboxContainer}>
                        <input
                          type="checkbox"
                          id="reservation-details-main-terms-of-use-checkbox"
                          checked={this.state.mainTermsOfUseChecked}
                          style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
                          onClick={this.openTermsOfUsePopup}
                        />
                        <label
                          htmlFor="reservation-details-main-terms-of-use-checkbox"
                          style={styles.noMargin}
                        >
                          {LOCALIZE.formatString(
                            LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                              .termsOfUseCheckboxLabel,
                            <button
                              aria-label={
                                LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                                  .termsOfUseCheckboxLink
                              }
                              tabIndex="0"
                              onClick={this.openTermsOfUsePopup}
                              style={styles.checkboxLink}
                            >
                              {
                                LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                                  .termsOfUseCheckboxLink
                              }
                            </button>
                          )}
                        </label>
                      </div>
                    </Col>
                  </Row>
                  <Row
                    role="presentation"
                    className="align-items-center"
                    style={styles.fieldRowContainer}
                  >
                    <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                      <div style={styles.checkboxContainer}>
                        <input
                          type="checkbox"
                          id="reservation-details-main-privacy-notice-statement-checkbox"
                          checked={this.state.mainPrivacyNoticeStatementChecked}
                          style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
                          onClick={this.openPrivacyNoticeStatementPopup}
                        />
                        <label
                          htmlFor="reservation-details-main-privacy-notice-statement-checkbox"
                          style={styles.noMargin}
                        >
                          {LOCALIZE.formatString(
                            LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                              .privacyNoticeStatementCheckboxLabel,
                            <button
                              aria-label={
                                LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                                  .privacyNoticeStatementCheckboxLink
                              }
                              tabIndex="0"
                              onClick={this.openPrivacyNoticeStatementPopup}
                              style={styles.checkboxLink}
                            >
                              {
                                LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                                  .privacyNoticeStatementCheckboxLink
                              }
                            </button>
                          )}
                        </label>
                      </div>
                    </Col>
                  </Row>
                </div>
              )}
            </>
          )}
        <PopupBox
          show={this.state.showTermsOfUsePopup}
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup.termsOfUsePopup
              .title
          }
          handleClose={() => {}}
          size={"lg"}
          description={
            <TermsOfUseContent
              handleAcceptTermsOfUseCheckbox={this.handleAcceptTermsOfUseCheckbox}
              termsOfUseChecked={this.state.termsOfUseChecked}
            />
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeTermsOfUsePopup}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.accept}
          rightButtonAction={this.handleAcceptTermsOfUse}
          rightButtonIcon={faCheck}
          rightButtonState={
            this.state.termsOfUseChecked ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
        />
        <PopupBox
          show={this.state.showPrivacyNoticeStatementPopup}
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
              .privacyNoticeStatementPopup.title
          }
          handleClose={() => {}}
          size={"lg"}
          description={
            <PrivacyNoticeStatementContent
              handleAcceptPrivacyNoticeStatementCheckbox={
                this.handleAcceptPrivacyNoticeStatementCheckbox
              }
              privacyNoticeStatementChecked={this.state.privacyNoticeStatementChecked}
            />
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closePrivacyNoticeStatementPopup}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.accept}
          rightButtonAction={this.handleAcceptPrivacyNoticeStatement}
          rightButtonIcon={faCheck}
          rightButtonState={
            this.state.privacyNoticeStatementChecked ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
        />
        <PopupBox
          show={this.state.showEditPhoneNumberPopup}
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup.editPhoneNumberPopop
              .title
          }
          handleClose={() => {}}
          size={"lg"}
          description={
            <div>
              <Row role="presentation" className="align-items-center">
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label
                    id="reservation-details-edit-phone-number-popup-phone-number-label"
                    htmlFor="reservation-details-phone-reservation-details-edit-phone-number-popup-phone-number-input"
                    style={styles.formLabel}
                  >
                    {
                      LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
                        .phoneNumber
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumnPhoneNumber.xl}
                  lg={columnSizes.secondColumnPhoneNumber.lg}
                  md={columnSizes.secondColumnPhoneNumber.md}
                  sm={columnSizes.secondColumnPhoneNumber.sm}
                  xs={columnSizes.secondColumnPhoneNumber.xs}
                >
                  <InputMask
                    id="reservation-details-edit-phone-number-popup-phone-number-input"
                    className={"valid-field"}
                    aria-labelledby="reservation-details-edit-phone-number-popup-phone-number-label"
                    aria-required={true}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.tempPhoneNumberContent}
                    mask={MASKS.phoneNumber}
                    formatChars={MASK_FORMAT_CHARS.phoneNumber}
                    onChange={this.updateTempPhoneNumber}
                  />
                </Col>
              </Row>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeEditPhoneMumberPopup}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.editButton}
          rightButtonAction={this.handleEditPhoneNumber}
          rightButtonIcon={faPencil}
          rightButtonState={
            this.state.isValidTempPhoneNumberContent ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    qualityOfLife: state.siteAdminSettings.qualityOfLife
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setReserveTestSessionValidFormStateState,
      setReserveTestSessionPhoneNumberState,
      editTestSessionPhoneNumber
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReservationDetailsDialogTestSessionForm);
