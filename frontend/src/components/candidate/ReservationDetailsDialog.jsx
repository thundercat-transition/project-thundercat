import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import PopupBox, { BUTTON_STATE } from "../commons/PopupBox";
import getCheckboxTransformScale from "../../helpers/checkboxTransformScale";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import ReservationDetailsDialogTestSessionForm from "./ReservationDetailsDialogTestSessionForm";
import TopTabs from "../commons/TopTabs";
import "../../css/top-tabs.css";
import { getEnableQualityofLife, setQualityOfLife } from "../../modules/SiteAdminSettingsRedux";

export const RESERVATION_DETAILS_DIALOG_SOURCE = {
  makeReservation: "makeReservation",
  manageReservation: "manageReservation"
};

// format the test skill to return "SkillType" or "SkillType - SubskillType"
const getFormattedTestSkill = (language, data) => {
  let formattedTestSkill = `${data[`test_skill_type_${language}_name`]}`;

  // if there is a subskill
  if (data.test_skill_sub_type_id !== null) {
    formattedTestSkill = formattedTestSkill.concat(
      ` - ${data[`test_skill_sub_type_${language}_name`]}`
    );
  }
  return formattedTestSkill;
};

// format the date as "Day Start-End"
const getFormattedDate = data => {
  // if there is no Date, return an empty string
  if (data.date === "" || data.date === undefined || data.date === null) {
    return "";
  }
  // otherwise return the formatted date
  return `${data.date} ${data.simplified_start_time} - ${data.simplified_end_time}`;
};

// default empty object to mimic the fromat returned from the DB
export const emptyReservation = [
  {
    assessment_process_closing_date: "",
    date: "",
    end_time: "",
    id: "",
    request_sent: "",
    reservation_code: "",
    start_time: "",
    test_center_address_text: { en: [{ text: "" }], fr: [{ text: "" }] },
    test_center_city_text: { en: [{ text: "" }], fr: [{ text: "" }] },
    country_eabrv: "",
    country_edesc: "",
    country_fabrv: "",
    country_fdesc: "",
    test_center_name: "",
    test_center_postal_code: "",
    test_center_province_text: { en: [{ text: "" }], fr: [{ text: "" }] },
    test_center_other_details_text: { en: [{ text: "" }], fr: [{ text: "" }] },
    test_center_test_session_data_id: "",
    test_skill_sub_type_en_name: "",
    test_skill_sub_type_fr_name: "",
    test_skill_sub_type_id: "",
    test_skill_type_en_name: "",
    test_skill_type_fr_name: "",
    test_skill_type_id: "",
    test_room_other_details_text: { en: [{ text: "" }], fr: [{ text: "" }] }
  }
];

const styles = {
  checkbox: {
    verticalAlign: "middle",
    margin: "0 16px"
  },
  contentTab: {
    paddingLeft: 3,
    paddingRight: 3
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2
  }
};

class ReservationDetailsDialog extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    isOla: PropTypes.bool.isRequired,
    reservationDetails: PropTypes.object.isRequired,
    showPopup: PropTypes.bool.isRequired,
    closePopup: PropTypes.func.isRequired,
    leftButtonType: PropTypes.string.isRequired,
    leftButtonIcon: PropTypes.object.isRequired,
    leftButtonTitle: PropTypes.string.isRequired,
    leftButtonLabel: PropTypes.string.isRequired,
    leftButtonAction: PropTypes.func.isRequired,
    hasRightButton: PropTypes.bool.isRequired,
    rightButtonType: PropTypes.string,
    rightButtonIcon: PropTypes.object,
    rightButtonTitle: PropTypes.string,
    rightButtonLabel: PropTypes.string,
    rightButtonAction: PropTypes.func,
    extraText: PropTypes.object,
    displayCloseButton: PropTypes.bool,
    showRetestPeriodWarningPopup: PropTypes.bool,
    source: PropTypes.string.isRequired
  };

  state = {
    popupCheckboxChecked: false
  };

  componentDidMount = () => {
    this.props
      .getEnableQualityofLife()
      .then(response => this.props.setQualityOfLife(response.is_active));
  };

  componentDidUpdate = prevProps => {
    // if showRetestPeriodWarningPopup gets updated
    if (prevProps.showRetestPeriodWarningPopup !== this.props.showRetestPeriodWarningPopup) {
      // unchecking checkbox
      this.setState({ popupCheckboxChecked: false });
    }
  };

  getTABS = () => {
    const conditionalContentTabs = [];
    // looping in testSessionData array
    for (let i = 0; i < this.props.reservationDetails.length; i++) {
      conditionalContentTabs.push({
        key: `test-session-${i + 1}`,
        tabName: LOCALIZE.formatString(
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
            .testSessionsData.addTestSessionPopup.tabName,
          i + 1
        ),
        body: (
          <ReservationDetailsDialogTestSessionForm
            isOla={this.props.isOla}
            testSessionFormData={this.props.reservationDetails[i]}
            multiSessions={true}
            source={this.props.source}
          />
        )
      });
    }
    return conditionalContentTabs;
  };

  render() {
    const {
      title,
      isOla,
      reservationDetails,
      showPopup,
      closePopup,
      leftButtonType,
      leftButtonIcon,
      leftButtonTitle,
      leftButtonLabel,
      leftButtonAction,
      hasRightButton,
      rightButtonType,
      rightButtonIcon,
      rightButtonTitle,
      rightButtonLabel,
      rightButtonAction,
      displayCloseButton,
      showRetestPeriodWarningPopup
    } = this.props;

    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <PopupBox
        show={showPopup}
        title={title}
        handleClose={() => {}}
        description={
          <>
            {!showRetestPeriodWarningPopup ? (
              <>
                <p>
                  {LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup.description}
                </p>
                {/* contains more than 1 test session */}
                {reservationDetails.length > 1 ? (
                  <div>
                    <TopTabs
                      TABS={this.getTABS()}
                      defaultTab={`test-session-1`}
                      customStyle={styles.contentTab}
                      tabsCustomStyle={styles.tabNavigation}
                      tabClassName={"top-tabs-transparent-tab-style"}
                      tabCustomStyle={styles.tabContainer}
                    />
                  </div>
                ) : (
                  // contains less than or equal to 1 test session
                  <ReservationDetailsDialogTestSessionForm
                    isOla={isOla}
                    testSessionFormData={reservationDetails[0]}
                    multiSessions={false}
                    source={this.props.source}
                  />
                )}
              </>
            ) : (
              <div>
                <p>
                  {
                    <SystemMessage
                      messageType={MESSAGE_TYPE.warning}
                      title={LOCALIZE.commons.warning}
                      message={LOCALIZE.retestPeriodWarningPopup.systemMessageDescription}
                    />
                  }
                </p>
                <p>{LOCALIZE.retestPeriodWarningPopup.description}</p>
                <div>
                  <div>
                    <input
                      type="checkbox"
                      id={"wish-to-proceed-checkbox"}
                      value={this.state.popupCheckboxChecked}
                      style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
                      onChange={() => {
                        this.setState({ popupCheckboxChecked: !this.state.popupCheckboxChecked });
                      }}
                    />
                    <label htmlFor={"wish-to-proceed-checkbox"}>
                      {LOCALIZE.retestPeriodWarningPopup.checkboxLabel}
                    </label>
                  </div>
                </div>
              </div>
            )}
          </>
        }
        leftButtonType={leftButtonType}
        leftButtonIcon={leftButtonIcon}
        leftButtonTitle={leftButtonTitle}
        leftButtonLabel={leftButtonLabel}
        leftButtonAction={leftButtonAction}
        rightButtonType={hasRightButton ? rightButtonType : undefined}
        rightButtonIcon={hasRightButton ? rightButtonIcon : undefined}
        rightButtonTitle={hasRightButton ? rightButtonTitle : undefined}
        rightButtonLabel={hasRightButton ? rightButtonLabel : undefined}
        rightButtonAction={
          hasRightButton
            ? !showRetestPeriodWarningPopup
              ? rightButtonAction
              : // calling with "true" to bypass the retest period validation
                () => rightButtonAction(true)
            : undefined
        }
        rightButtonState={
          this.props.qualityOfLife
            ? BUTTON_STATE.enabled
            : !this.props.reserveTestSessionValidFormState
              ? BUTTON_STATE.disabled
              : showRetestPeriodWarningPopup && !this.state.popupCheckboxChecked
                ? BUTTON_STATE.disabled
                : BUTTON_STATE.enabled
        }
        displayCloseButton={displayCloseButton}
        closeButtonAction={closePopup}
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    reserveTestSessionValidFormState: state.candidate.reserveTestSessionValidFormState,
    qualityOfLife: state.siteAdminSettings.qualityOfLife
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getEnableQualityofLife, setQualityOfLife }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ReservationDetailsDialog);

export { getFormattedTestSkill, getFormattedDate };
