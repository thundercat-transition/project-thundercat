/* eslint-disable prefer-destructuring */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import PropTypes from "prop-types";
import {
  faBinoculars,
  faTrash,
  faTimes,
  faCalendarCheck,
  faCalendarDays,
  faUniversalAccess,
  faFileCircleExclamation,
  faTimesCircle,
  faVideo
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import ReservationDetailsDialog, {
  getFormattedTestSkill,
  emptyReservation,
  RESERVATION_DETAILS_DIALOG_SOURCE
} from "./ReservationDetailsDialog";
import StyledTooltip, { TYPE, EFFECT } from "../authentication/StyledTooltip";
import {
  withdrawBookedReservationCode,
  bookLater,
  findBookedSessionsForReservationCode,
  findRequestedAccommodationsForReservationCode
} from "../../modules/CandidateRedux";
import { convertDateTimeToUsersTimezone } from "../../modules/helpers";
import AccommodationsRequestDetailsPopup, {
  ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE
} from "../aae/AccommodationsRequestDetailsPopup";
import { TestSkillSleDescCodename, TestSkillTypeCodename } from "../testFactory/Constants";
import { cleanUpVirtualMeeting } from "../../modules/VirtualMeetingsRedux";
import { getDataOfTestCenterTestSessionToViewOrEdit } from "../../modules/TestCenterRedux";

const styles = {
  mainDiv: {
    marginTop: 12,
    textAlign: "center"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  withdrawActionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#cc0000"
  },
  firstRowStyle: {
    textAlign: "left"
  },
  allUnset: {
    all: "unset"
  },
  boldText: {
    fontWeight: "bold"
  }
};

class ManageReservation extends Component {
  state = {
    rowsDefinition: {},
    rawData: [],
    currentlyLoading: false,
    reservation: emptyReservation,
    viewReservationPopup: false,
    showAccommodationsRequestDetailsPopup: false,
    selectedAccommodationRequestData: {},
    editReservationPopup: false,
    cancelReservationPopup: false,
    showBookingActionSuccessfulPopup: false,
    showWithdrawSuccessfulPopup: false,
    showActionViolatesBookingDelayPopup: false,
    showConfirmWithdrawPopup: false,
    selectedReservationIndex: 0,
    earliest_booking: 0,
    bookLaterText: "",
    isOla: false
  };

  static propTypes = {
    setRebuildMakeTables: PropTypes.func,
    rebuildManage: PropTypes.bool,
    setRebuildManageTables: PropTypes.func
  };

  componentDidMount = () => {
    // this is a little forced; but without it Generic Table doesn't "Refresh" or load any data
    this._isMounted = true;
    this.populateTable();
  };

  componentDidUpdate = prevProps => {
    // if the other table had a change, rebuild this one
    if (prevProps.rebuildManage !== this.props.rebuildManage && this.props.rebuildManage) {
      this.populateTable();
    }
  };

  populateTable = () => {
    // set the "currently loading" flag
    this.setState({
      currentlyLoading: true,
      selectedReservationIndex: 0
    });

    this._isMounted = true;
    this.props.findBookedSessionsForReservationCode().then(response => {
      // if the session was booked successfully
      if (response.status === 200) {
        this.populateRowsDefinition(response);
      } else {
        throw new Error("An error occurred during the populating of the Manage Reservations Table");
      }
    });
  };

  populateRowsDefinition = rawData => {
    // create the default empty variables
    let rowsDefinition = {};
    const data = [];
    // looping in results
    for (let i = 0; i < rawData.length; i++) {
      this.pushData(data, rawData[i], i);
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: styles.firstRowStyle,
      column_2_style: styles.basicRowStyle,
      column_3_style: styles.basicRowStyle,
      column_4_style: styles.basicRowStyle,
      data: data
    };

    this.setState({
      rawData: rawData,
      rowsDefinition: rowsDefinition,
      currentlyLoading: false,
      selectedReservationIndex: 0
    });

    // If this was triggered by a change on the managed tables, then do not rebuild the managed tables
    if (this.props.rebuildManage) {
      this.props.setRebuildManageTables(false);
      // otherwise trigger the logic to rebuild the managed tables
    } else {
      this.props.setRebuildMakeTables(true);
    }
  };

  pushData = (data, currentResult, index) => {
    let formattedDate = LOCALIZE.commons.na;
    // Check if start time exists (Accommodation requests don't have a start time)
    if (currentResult.start_time !== null) {
      // eslint-disable-next-line no-param-reassign, prefer-destructuring
      currentResult.simplified_start_time = convertDateTimeToUsersTimezone(
        currentResult.utc_start_time
      ).adjustedTimeBaseOnCurrentUtcOffset;
      // eslint-disable-next-line no-param-reassign, prefer-destructuring
      currentResult.simplified_end_time = convertDateTimeToUsersTimezone(
        currentResult.utc_end_time
      ).adjustedTimeBaseOnCurrentUtcOffset;
      formattedDate = `${currentResult.date} ${currentResult.simplified_start_time} - ${currentResult.simplified_end_time}`;
    }

    // format the data for the table
    const formattedTestSkill = getFormattedTestSkill(this.props.currentLanguage, currentResult);

    // add the formatted data to the table
    data.push({
      column_1: formattedTestSkill,
      column_2: formattedDate,
      column_3:
        // if OLA Test ==> N/A
        currentResult.test_skill_type_codename === TestSkillTypeCodename.SLE &&
        (currentResult.test_skill_sub_type_codename === TestSkillSleDescCodename.ORAL_EN ||
          currentResult.test_skill_sub_type_codename === TestSkillSleDescCodename.ORAL_FR)
          ? LOCALIZE.commons.na
          : currentResult.test_center_name,
      column_4: (
        <div>
          <StyledTooltip
            id={`manage-reservations-view-button-${index}`}
            place="top"
            variant={TYPE.light}
            effect={EFFECT.solid}
            openOnClick={false}
            tooltipElement={
              <div style={styles.allUnset}>
                <CustomButton
                  dataTip=""
                  dataFor={`manage-reservations-view-button-${index}`}
                  label={<FontAwesomeIcon icon={faBinoculars} />}
                  // ariaLabel
                  action={() => {
                    this.viewReservationDialog(index, currentResult);
                  }}
                  customStyle={styles.actionButton}
                  buttonTheme={THEME.SECONDARY}
                  // no date defined means no test session yet, so nothing to view at this point in time
                  disabled={currentResult.date === null}
                />
              </div>
            }
            tooltipContent={
              <div>
                <p>
                  {LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.table.infoButton}
                </p>
              </div>
            }
          />
          {currentResult.user_accommodation_file_id !== null && (
            <StyledTooltip
              id={`manage-reservations-view-accommodations-request-details-button-${index}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`manage-reservations-view-accommodations-request-details-button-${index}`}
                    label={
                      <>
                        <FontAwesomeIcon
                          icon={
                            currentResult.is_alternate_test_request
                              ? faFileCircleExclamation
                              : faUniversalAccess
                          }
                        />
                      </>
                    }
                    action={() => this.openAccommodationsRequestDetailsPopup(currentResult)}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.PRIMARY}
                    ariaLabel={LOCALIZE.formatString(
                      currentResult.is_alternate_test_request
                        ? LOCALIZE.accommodationsRequestDetailsPopup
                            .viewAlternateTestRequestDetailsAccessibilityLabel
                        : LOCALIZE.accommodationsRequestDetailsPopup
                            .viewAccommodationsRequestDetailsAccessibilityLabel,
                      formattedTestSkill
                    )}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {currentResult.is_alternate_test_request
                      ? LOCALIZE.accommodationsRequestDetailsPopup.viewAlternateTestRequestDetails
                      : LOCALIZE.accommodationsRequestDetailsPopup.viewAccommodationsRequestDetails}
                  </p>
                </div>
              }
            />
          )}
          {currentResult.user_accommodation_file_id == null && (
            <StyledTooltip
              id={`manage-reservations-edit-button-${index}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`manage-reservations-edit-button-${index}`}
                    label={<FontAwesomeIcon icon={faCalendarDays} />}
                    // ariaLabel
                    action={() => {
                      this.viewEditSelectedReservationDialog(index, currentResult);
                    }}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.SECONDARY}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.table.editButton}
                  </p>
                </div>
              }
            />
          )}
          <StyledTooltip
            id={`manage-reservations-withdraw-button-${index}`}
            place="top"
            variant={TYPE.light}
            effect={EFFECT.solid}
            openOnClick={false}
            tooltipElement={
              <div style={styles.allUnset}>
                <CustomButton
                  dataTip=""
                  dataFor={`manage-reservations-withdraw-button-${index}`}
                  label={<FontAwesomeIcon icon={faTimesCircle} />}
                  // ariaLabel
                  action={() => {
                    this.viewCancelSelectedReservationDialog(index, currentResult);
                  }}
                  customStyle={styles.withdrawActionButton}
                  buttonTheme={THEME.SECONDARY}
                />
              </div>
            }
            tooltipContent={
              <div>
                <p>
                  {LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.table.deleteButton}
                </p>
              </div>
            }
          />
        </div>
      )
    });
  };

  viewReservationDialog = (index, reservation) => {
    this.props.getDataOfTestCenterTestSessionToViewOrEdit(reservation.id).then(response => {
      // initializing formattedReservation
      const formattedReservation = [];
      // looping in response
      for (let i = 0; i < response.length; i++) {
        // eslint-disable-no-undef
        const tempReservation = response[i];
        // eslint-disable-next-line no-param-reassign, prefer-destructuring
        tempReservation.simplified_start_time = convertDateTimeToUsersTimezone(
          response[i].utc_start_time
        ).adjustedTime;
        // eslint-disable-next-line no-param-reassign, prefer-destructuring
        tempReservation.simplified_end_time = convertDateTimeToUsersTimezone(
          response[i].utc_end_time
        ).adjustedTime;
        // populating formattedReservation
        formattedReservation.push(tempReservation);
      }

      // Collect all information and set state variables for the view popup
      this.setState({
        reservation: formattedReservation,
        viewReservationPopup: true,
        selectedReservationIndex: index,
        isOla:
          reservation.test_skill_type_codename === TestSkillTypeCodename.SLE &&
          (reservation.test_skill_sub_type_codename === TestSkillSleDescCodename.ORAL_EN ||
            reservation.test_skill_sub_type_codename === TestSkillSleDescCodename.ORAL_FR)
      });
    });
  };

  closeReservationDialog = () => {
    // Clear all information for the view popup
    this.setState({
      reservation: emptyReservation,
      viewReservationPopup: false,
      selectedReservationIndex: 0,
      isOla: false
    });
  };

  openAccommodationsRequestDetailsPopup = accommodationRequestData => {
    // updating needed states
    this.setState({
      showAccommodationsRequestDetailsPopup: true,
      selectedAccommodationRequestData: accommodationRequestData
    });
  };

  closeAccommodationsRequestDetailsPopup = () => {
    this.setState(
      {
        showAccommodationsRequestDetailsPopup: false,
        selectedAccommodationRequestData: {}
      },
      () => {
        // refreshing both tables (make a reservation and manage reservations)
        this.props.setRebuildMakeTables(true);
        this.props.setRebuildManageTables(true);
      }
    );
  };

  viewEditSelectedReservationDialog = (index, reservation) => {
    // eslint-disable-next-line no-param-reassign, prefer-destructuring
    reservation.simplified_start_time = convertDateTimeToUsersTimezone(
      reservation.utc_start_time
    ).adjustedTime;
    // eslint-disable-next-line no-param-reassign, prefer-destructuring
    reservation.simplified_end_time = convertDateTimeToUsersTimezone(
      reservation.utc_end_time
    ).adjustedTime;

    // Collect all information and set state variables for the view popup
    this.setState({
      reservation: [reservation],
      editReservationPopup: true,
      selectedReservationIndex: index,
      isOla:
        reservation.test_skill_type_codename === TestSkillTypeCodename.SLE &&
        (reservation.test_skill_sub_type_codename === TestSkillSleDescCodename.ORAL_EN ||
          reservation.test_skill_sub_type_codename === TestSkillSleDescCodename.ORAL_FR)
    });
  };

  closeEditReservationDialog = () => {
    // Clear all information for the view popup
    this.setState({
      reservation: emptyReservation,
      editReservationPopup: false,
      selectedReservationIndex: 0,
      isOla: false
    });
  };

  changeReservation = () => {
    // TODO implement change reservation functionality. Additional popup?
    // for now, same functionality as a close dialog
    this.setState({
      reservation: emptyReservation,
      editReservationPopup: false,
      selectedReservationIndex: 0,
      isOla: false
    });
  };

  dropRowFromData = indexToRemove => {
    const { rawData } = this.state;
    rawData.splice(indexToRemove, 1);
    this.populateRowsDefinition(rawData);
  };

  bookReservationLater = () => {
    this.props
      .bookLater(
        this.state.reservation[0].id,
        this.state.reservation[0].user_accommodation_file_id,
        this.state.isOla
      )
      .then(response => {
        if (response.status === 200) {
          const test_session_id = this.state.reservation[0].id;
          if (this.state.isOla) {
            // If successful (and an OLA), then delete the virtual meeting and cancel the invitation
            this.props.cleanUpVirtualMeeting(test_session_id);
          }
          const bookLaterText =
            LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation
              .bookingActionSuccessfulPopup.paragraphLater;
          this.setState({
            ...this.state,
            editReservationPopup: false,
            showBookingActionSuccessfulPopup: true,
            bookLaterText: bookLaterText
          });
        } else {
          if (response.status === 406) {
            const { earliest_booking } = response;
            this.setState({
              ...this.state,
              showActionViolatesBookingDelayPopup: true,
              earliest_booking: `${convertDateTimeToUsersTimezone(earliest_booking).adjustedDate} ${
                convertDateTimeToUsersTimezone(earliest_booking).adjustedTime
              }`
            });
            // should never happen
          } else {
            throw new Error("An error occurred during the book later process");
          }
        }
      });
    // TODO add functionality to book the reservation later
    // release the reservation code and re-send an email (clear the extra columns of the reservation code)
    this.dropRowFromData(this.state.selectedReservationIndex);
  };

  viewCancelSelectedReservationDialog = (index, reservation) => {
    this.props.getDataOfTestCenterTestSessionToViewOrEdit(reservation.id).then(response => {
      // initializing formattedReservation
      let formattedReservation = [];
      // looping in response
      for (let i = 0; i < response.length; i++) {
        // eslint-disable-no-undef
        const tempReservation = response[i];
        // eslint-disable-next-line no-param-reassign, prefer-destructuring
        tempReservation.simplified_start_time = convertDateTimeToUsersTimezone(
          response[i].utc_start_time
        ).adjustedTime;
        // eslint-disable-next-line no-param-reassign, prefer-destructuring
        tempReservation.simplified_end_time = convertDateTimeToUsersTimezone(
          response[i].utc_end_time
        ).adjustedTime;
        // populating formattedReservation
        formattedReservation.push(tempReservation);
      }

      // no test session data yet
      if (formattedReservation.length <= 0) {
        // resetting formattedReservation
        formattedReservation = [reservation];
      }

      // Collect all information and set state variables for the view popup
      this.setState({
        reservation: formattedReservation,
        cancelReservationPopup: true,
        selectedReservationIndex: index,
        isOla:
          reservation.test_skill_type_codename === TestSkillTypeCodename.SLE &&
          (reservation.test_skill_sub_type_codename === TestSkillSleDescCodename.ORAL_EN ||
            reservation.test_skill_sub_type_codename === TestSkillSleDescCodename.ORAL_FR)
      });
    });
  };

  closeCancelSelectedReservation = () => {
    // Clear all information for the view popup
    this.setState({
      cancelReservationPopup: false,
      selectedReservationIndex: 0
    });
  };

  withdrawFromSession = () => {
    this.setState({
      ...this.state,
      cancelReservationPopup: false,
      showConfirmWithdrawPopup: true
    });
  };

  confirmWithdraw = () => {
    // checking if test_session_id contains "-CRC"
    let test_session_id = this.state.reservation[0].id;
    if (test_session_id.includes("-CRC")) {
      test_session_id = test_session_id.split("-CRC")[0];
    }
    const body = {
      reservation_code: null,
      test_session_id: test_session_id,
      is_ola: this.state.isOla
    };
    // this calls cleanUpVirtualMeeting behind the scenes at the appropriate time for OLAs
    this.props.withdrawBookedReservationCode(body).then(response => {
      if (response.status === 200) {
        this.setState({
          ...this.state,
          reservation: emptyReservation,
          showWithdrawSuccessfulPopup: true,
          isOla: false
        });
      } else {
        if (response.status === 406) {
          const { earliest_booking } = response;
          this.setState({
            ...this.state,
            showActionViolatesBookingDelayPopup: true,
            earliest_booking: `${convertDateTimeToUsersTimezone(earliest_booking).adjustedDate} ${
              convertDateTimeToUsersTimezone(earliest_booking).adjustedTime
            }`
          });
          // should never happen
        } else {
          throw new Error("An error occurred during the withdrawal from an assessment process");
        }
      }
    });
  };

  cancelConfirmWithdraw = () => {
    this.setState({
      ...this.state,
      showConfirmWithdrawPopup: false
    });
  };

  closeShowBookingActionSuccessfulDialog = () => {
    // Clear all information for the view popup
    this.setState({
      reservation: emptyReservation,
      showBookingActionSuccessfulPopup: false,
      selectedReservationIndex: 0,
      bookLaterText: "",
      isOla: false
    });
    // update the table
    this.populateTable();
  };

  closeShowWithdrawSuccessfulDialog = () => {
    // Clear all information for the view popup
    this.setState({
      reservation: emptyReservation,
      showWithdrawSuccessfulPopup: false,
      selectedReservationIndex: 0,
      isOla: false
    });
    // update the table
    this.populateTable();
  };

  closeShowActionViolatesBookingDelayDialog = () => {
    // Clear all information for the view popup
    this.setState({
      reservation: emptyReservation,
      showActionViolatesBookingDelayPopup: false,
      editReservationPopup: false,
      selectedReservationIndex: 0,
      earliest_booking: 0,
      isOla: false
    });
    // update the table
    this.populateTable();
  };

  openTeamsCall = () => {
    const meeting_link = this.state.reservation[0].meeting_link;
    // Just don't click this if you don't have teams setup properly.
    // Otherwise it will be null and explode. You have been warned
    // open the video link in a new tab
    const newWindow = window.open(meeting_link, "_blank", "noopener,noreferrer");
    if (newWindow) {
      newWindow.opener = null;
    }
  };

  render() {
    // column definition
    const columnsDefinition = [
      {
        label: LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.table.columnOne,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.table.columnTwo,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.table.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.table.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    // reservation info from the DB
    const {
      rowsDefinition,
      currentlyLoading,
      reservation,
      viewReservationPopup,
      editReservationPopup,
      cancelReservationPopup,
      showBookingActionSuccessfulPopup,
      showWithdrawSuccessfulPopup,
      showActionViolatesBookingDelayPopup,
      earliest_booking,
      bookLaterText,
      isOla
    } = this.state;

    return (
      <div style={styles.mainDiv}>
        <GenericTable
          classnamePrefix="booked-sessions"
          columnsDefinition={columnsDefinition}
          rowsDefinition={rowsDefinition}
          emptyTableMessage={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.table.noDataMessage
          }
          currentlyLoading={currentlyLoading}
          tableWithButtons={true}
        />
        <ReservationDetailsDialog
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.reservationPopup.titleView
          }
          source={RESERVATION_DETAILS_DIALOG_SOURCE.manageReservation}
          isOla={isOla}
          reservationDetails={reservation}
          showPopup={viewReservationPopup}
          closePopup={this.closeReservationDialog}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={isOla ? faVideo : faTimes}
          rightButtonTitle={
            isOla
              ? LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.reservationPopup
                  .joinMeetingButton
              : LOCALIZE.commons.close
          }
          rightButtonLabel={
            isOla
              ? LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.reservationPopup
                  .joinMeetingButton
              : LOCALIZE.commons.close
          }
          rightButtonAction={isOla ? this.openTeamsCall : this.closeReservationDialog}
          leftButtonType={isOla ? BUTTON_TYPE.secondary : BUTTON_TYPE.none}
          leftButtonIcon={isOla ? faTimes : undefined}
          leftButtonTitle={isOla ? LOCALIZE.commons.cancel : undefined}
          leftButtonLabel={isOla ? LOCALIZE.commons.cancel : undefined}
          leftButtonAction={isOla ? this.closeReservationDialog : undefined}
          hasRightButton={true}
        />
        <ReservationDetailsDialog
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.reservationPopup
              .titleChange
          }
          source={RESERVATION_DETAILS_DIALOG_SOURCE.manageReservation}
          isOla={isOla}
          reservationDetails={reservation}
          showPopup={editReservationPopup}
          closePopup={this.closeEditReservationDialog}
          hasRightButton={true}
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeEditReservationDialog}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faCalendarCheck}
          rightButtonTitle={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.reservationPopup
              .bookLaterButton
          }
          rightButtonLabel={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.reservationPopup
              .bookLaterButton
          } // TODO more accessible
          rightButtonAction={this.bookReservationLater}
          extraText={
            <div>
              <p>
                {
                  LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.reservationPopup
                    .paragraphLater
                }
              </p>
            </div>
          }
          displayCloseButton={false}
        />
        <ReservationDetailsDialog
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.reservationPopup
              .titleWithdraw
          }
          source={RESERVATION_DETAILS_DIALOG_SOURCE.manageReservation}
          isOla={isOla}
          reservationDetails={reservation}
          showPopup={cancelReservationPopup}
          closePopup={this.closeCancelSelectedReservation}
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeCancelSelectedReservation}
          hasRightButton={true}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonIcon={faTrash}
          rightButtonTitle={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.reservationPopup
              .withdrawButton
          }
          rightButtonLabel={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.reservationPopup
              .withdrawButton
          } // TODO more accessible
          rightButtonAction={this.withdrawFromSession}
          extraText={
            <p>
              {
                LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.reservationPopup
                  .paragraphWithdraw
              }
            </p>
          }
        />
        <PopupBox
          show={showBookingActionSuccessfulPopup}
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation
              .bookingActionSuccessfulPopup.title
          }
          handleClose={() => this.closeShowBookingActionSuccessfulDialog()}
          size={"lg"}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.success}
                    title={LOCALIZE.commons.success}
                    message={bookLaterText}
                  />
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.closeShowBookingActionSuccessfulDialog()}
        />
        <PopupBox
          show={showWithdrawSuccessfulPopup}
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.withdrawSuccessfulPopup
              .title
          }
          handleClose={() => this.closeShowWithdrawSuccessfulDialog()}
          size={"lg"}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.success}
                    title={LOCALIZE.commons.success}
                    message={
                      LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation
                        .withdrawSuccessfulPopup.paragraph
                    }
                  />
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.closeShowWithdrawSuccessfulDialog()}
        />
        <PopupBox
          show={showActionViolatesBookingDelayPopup}
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.actionViolatesPopup.title
          }
          handleClose={() => this.closeShowActionViolatesBookingDelayDialog()}
          size={"lg"}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.error}
                    title={LOCALIZE.commons.error}
                    message={LOCALIZE.formatString(
                      LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation
                        .actionViolatesPopup.paragraph,
                      <span style={styles.boldText}>{earliest_booking}</span>
                    )}
                  />
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.closeShowActionViolatesBookingDelayDialog()}
        />
        <PopupBox
          show={this.state.showConfirmWithdrawPopup}
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.confirmWithdrawPopup.title
          }
          handleClose={() => this.cancelConfirmWithdraw()}
          size={"lg"}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation
                        .confirmWithdrawPopup.paragraph
                    }
                  />
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={() => this.cancelConfirmWithdraw()}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.confirm}
          rightButtonAction={() => this.confirmWithdraw()}
        />
        {Object.keys(this.state.selectedAccommodationRequestData).length > 0 &&
          this.state.showAccommodationsRequestDetailsPopup && (
            <AccommodationsRequestDetailsPopup
              source={ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.candidate}
              userAccommodationFileId={
                this.state.selectedAccommodationRequestData.user_accommodation_file_id
              }
              selectedUserAccommodationFileData={this.state.selectedAccommodationRequestData}
              showPopup={this.state.showAccommodationsRequestDetailsPopup}
              closeAccommodationsRequestDetailsPopup={this.closeAccommodationsRequestDetailsPopup}
              // not providing isDraft props, since the bool value is defined inside that component by the status of the file because it is triggered by the candidate
            />
          )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      withdrawBookedReservationCode,
      bookLater,
      findBookedSessionsForReservationCode,
      findRequestedAccommodationsForReservationCode,
      cleanUpVirtualMeeting,
      getDataOfTestCenterTestSessionToViewOrEdit
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ManageReservation);
