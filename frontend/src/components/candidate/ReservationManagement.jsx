import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import MakeReservation from "./MakeReservation";
import ManageReservation from "./ManageReservation";
import { Tabs, Tab, Row, Col } from "react-bootstrap";

const styles = {
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  },
  tabNavigation: {
    borderBottom: "none"
  }
};
class ReservationManagement extends Component {
  state = {
    selectedTab: "",
    rebuildMake: false,
    rebuildManage: false
  };

  handleTabChange = key => {
    this.setState({ selectedTab: key });
  };

  // trigger a rebuild of the make tables (or mark the rebuild as done)
  setRebuildMakeTables = value => {
    this.setState({ rebuildMake: value });
  };

  // trigger a rebuild of the manage tables (or mark the rebuild as done)
  setRebuildManageTables = value => {
    this.setState({ rebuildManage: value });
  };

  render() {
    const TABS = [
      {
        key: "makeReservation",
        tabName: LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.title,
        body: (
          <MakeReservation
            rebuildMake={this.state.rebuildMake}
            setRebuildMakeTables={this.setRebuildMakeTables}
            setRebuildManageTables={this.setRebuildManageTables}
          />
        )
      },
      {
        key: "manageReservation",
        tabName: LOCALIZE.dashboard.sideNavItems.reserveSeat.manageReservation.title,
        body: (
          <ManageReservation
            setRebuildMakeTables={this.setRebuildMakeTables}
            rebuildManage={this.state.rebuildManage}
            setRebuildManageTables={this.setRebuildManageTables}
          />
        )
      }
    ];
    return (
      <div>
        <h2>{LOCALIZE.dashboard.sideNavItems.reserveSeat.title}</h2>
        <p>{LOCALIZE.dashboard.sideNavItems.reserveSeat.text}</p>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey="makeReservation"
                id="reservation-tabs"
                style={styles.tabNavigation}
                onSelect={this.handleTabChange}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ReservationManagement);
