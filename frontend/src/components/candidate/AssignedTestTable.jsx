/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import PropTypes from "prop-types";
import withRouter from "../withRouter";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Col, Row } from "react-bootstrap";
import { connect } from "react-redux";
import {
  getAssignedTests,
  getAssignedVirtualTests,
  updateAssignedTestId,
  resetAssignedTestState
} from "../../modules/AssignedTestsRedux";
import { PATH } from "../commons/Constants";
import CandidateCheckIn from "../../CandidateCheckIn";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlayCircle,
  faShare,
  faUniversalAccess,
  faFileCircleExclamation,
  faTimes,
  faPaperPlane,
  faSpinner,
  faVideo
} from "@fortawesome/free-solid-svg-icons";
import SessionStorage, { ACTION, ITEM } from "../../SessionStorage";
import TEST_STATUS from "../ta/Constants";
import {
  activateTest,
  deactivateTest,
  updateTestStatus,
  getCurrentTestStatus,
  invalidateTest,
  preTestOrTransitionTest
} from "../../modules/TestStatusRedux";
import { resetTestFactoryState, setContentUnLoaded } from "../../modules/TestSectionRedux";
import { resetNotepadState } from "../../modules/NotepadRedux";
import { resetQuestionListState } from "../../modules/QuestionListRedux";
import { resetTopTabsState } from "../../modules/NavTabsRedux";
import { setErrorStatus } from "../../modules/ErrorStatusRedux";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import { resetInboxState } from "../../modules/EmailInboxRedux";
import isWithinTheRetestPeriod from "../../modules/RetestPeriodValidationRedux";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import getCheckboxTransformScale from "../../helpers/checkboxTransformScale";
import getFormattedDate from "../../helpers/getFormattedDate";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import {
  checkForOngoingUserAccommodationRequestProcess,
  getTestCentersForUserAccommodationRequest,
  sendUserAccommodationRequest
} from "../../modules/AaeRedux";
import { setMyProfileSideNavigationSelectedItem } from "../../modules/UserProfileRedux";
import AccommodationsRequestDetailsPopup, {
  ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE
} from "../aae/AccommodationsRequestDetailsPopup";
import DropdownSelect from "../commons/DropdownSelect";
import { getAccommodationRequestTypeOptions } from "./Constants";
import { history } from "../../store-index";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  table: {
    width: "80%",
    margin: "24px auto"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  checkbox: {
    verticalAlign: "middle",
    margin: "0 16px"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 8px",
    margin: "0 3px",
    color: "#00565e",
    transform: "scale(1.3)"
  },
  allUnset: {
    all: "unset"
  },
  boldText: {
    fontWeight: "bold"
  },
  fieldRowContainer: {
    margin: "12px auto",
    width: "80%"
  },
  rowContainer: {
    margin: "12px 0"
  },
  formContainer: {
    width: "100%",
    margin: "18px auto"
  },
  textAreaInput: {
    width: "100%",
    height: 115,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  noMargin: {
    margin: 0
  },
  loadingContainer: {
    textAlign: "center",
    transform: "scale(1.3)"
  },
  loading: {
    fontSize: "32px",
    padding: 6
  },
  profileLink: {
    textDecoration: "underline",
    color: "#0278A4",
    cursor: "pointer",
    backgroundColor: "transparent",
    border: "none",
    padding: 0
  },
  icon: {
    paddingRight: 6,
    verticalAlign: "middle"
  },
  transparentText: {
    color: "transparent"
  },
  customLoadingContainer: {
    display: "grid",
    gridTemplateColumns: "1fr"
  },
  loadingOverlappingStyle: {
    gridRowStart: "1",
    gridColumnStart: "1"
  }
};

class AssignedTestTable extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    username: PropTypes.string,
    // Props from Redux
    getAssignedTests: PropTypes.func,
    getAssignedVirtualTests: PropTypes.func,
    updateAssignedTestId: PropTypes.func,
    activateTest: PropTypes.func,
    deactivateTest: PropTypes.func,
    setErrorStatus: PropTypes.func
  };

  state = {
    username: "",
    assigned_tests: [],
    assigned_virtual_tests: [],
    rowsDefinition: {},
    activeUitTest: false,
    id: null,
    testId: null,
    startTime: null,
    isLoading: true,
    pollingState: undefined,
    selectedTestData: {},
    showRetestPeriodWarningPopup: false,
    popupCheckboxChecked: false,
    showRequestAccommodationsPopup: false,
    showNeedAccommodationsProfilePopup: false,
    showAccommodationsRequestDetailsPopup: false,
    requestTypeOptions: [],
    requestTypeSelectedOption: [],
    commentsOrTestAdministeredDetails: "",
    profileUpToDateCheckboxChecked: false,
    isValidForm: false,
    showAccommodationRequestSentSuccessfullyPopup: false,
    showOngoingAccommodationRequestProcessPopup: false,
    ongoingAccommodationRequestPopupCheckboxChecked: false,
    showCancelRequestConfirmationPopup: false,
    actionInProgress: false
  };

  componentDidMount = () => {
    this._isMounted = true;
    // remove active test variable if it exists
    SessionStorage(ACTION.REMOVE, ITEM.TEST_ACTIVE);

    if (this._isMounted) {
      this.setState({ isLoading: true }, () => {
        this.populateTable();
      });
    }
  };

  componentDidUpdate = prevProps => {
    // if language gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      // updating assigned tests table
      this.props.getAssignedTests(this.props.currentLanguage).then(response => {
        this.setState({ assigned_tests: response });
      });
    }
  };

  componentWillUnmount = () => {
    this._isMounted = false;
    clearInterval(this.state.pollingState);
  };

  handleTestStatusUpdates = () => {
    // looping in assigned tests
    for (let i = 0; i < this.state.assigned_tests.length; i++) {
      // making sure that the assigned_test_id is defined (might not be defined for accommodated tests done outside of CAT that needs approval by the candidate)
      if (this.state.assigned_tests[i].id !== null) {
        // getting current test status
        this.props.getCurrentTestStatus(this.state.assigned_tests[i].id).then(testStatusData => {
          // getting error (basically mean that we cannot get the status data based on the provided assigned test ID)
          if (testStatusData.error) {
            // reloading the page
            window.location.reload();
          }
          // test status is LOCKED, PAUSED or UNASSIGNED
          if (
            testStatusData.status_codename === TEST_STATUS.LOCKED ||
            testStatusData.status_codename === TEST_STATUS.PAUSED ||
            testStatusData.status_codename === TEST_STATUS.UNASSIGNED
          ) {
            // reloading the page
            window.location.reload();
            // test status is CHECKED_IN
          } else if (testStatusData.status_codename === TEST_STATUS.CHECKED_IN) {
            // updating respective assigned_tests status state
            const copyOfAssignedTest = Array.from(this.state.assigned_tests);
            copyOfAssignedTest[i].status_codename = TEST_STATUS.CHECKED_IN;
            this.setState({ assigned_tests: copyOfAssignedTest }, () => {
              // populating table rows
              this.populateTableRows();
            });
          }
        });
      }
    }
  };

  // populating assigned tests table
  populateTable = () => {
    let assignedTests = null;
    let activeTestObj = {};

    /* displayTableContent state is used here to avoid unwanted table content display, such as
        the flashing view button when refreshing the page */
    if (this._isMounted) {
      // getting assigned tests
      this.props
        .getAssignedTests(this.props.currentLanguage)
        .then(assigned_tests => {
          // verifying if there is an active test
          activeTestObj = this.checkForActiveTest(assigned_tests);
          assignedTests = assigned_tests;
        })
        .then(() => {
          // there is an active test
          if (activeTestObj.activeTest && !activeTestObj.unsupervisedTest) {
            // redirecting user to test page
            history.push(PATH.testBase);
            // there is no active test
          } else {
            // initializing pollingFunctionNeeded
            let pollingFunctionNeeded = false;
            // looping in assigned tests
            for (let i = 0; i < assignedTests.length; i++) {
              // making sure that this is a supervised test before calling the polling function
              if (assignedTests[i].uit_invite_id === null) {
                pollingFunctionNeeded = true;
              }
            }
            if (pollingFunctionNeeded) {
              // clearing existing polling interval(s) and creating new one (interval of 5 seconds)
              clearInterval(this.state.pollingState);
              const interval = setInterval(this.handleTestStatusUpdates, 5000);
              this.setState({ pollingState: interval });
            }
          }
        })
        .then(() => {
          this.props.getAssignedVirtualTests().then(assigned_virtual_tests => {
            this.setState(
              {
                assigned_tests: assignedTests,
                assigned_virtual_tests: assigned_virtual_tests,
                isLoading: false
              },
              () => {
                // populating table rows
                this.populateTableRows();
              }
            );
          });
        });
    }
  };

  openTeamsCall = meeting_link => {
    // Just don't click this if you don't have teams setup properly.
    // Otherwise it will be null and explode. You have been warned
    // open the video link in a new tab
    const newWindow = window.open(meeting_link, "_blank", "noopener,noreferrer");
    if (newWindow) {
      newWindow.opener = null;
    }
  };

  // populating table rows for the GenericTable component
  populateTableRows = () => {
    const { assigned_tests, assigned_virtual_tests } = this.state;

    // initializing needed object and array for rowsDefinition state (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    for (let i = 0; i < assigned_tests.length; i++) {
      // setting accommodatedTestOutsideOfCat
      let accommodatedTestOutsideOfCat = false;
      if (
        typeof assigned_tests[i].accommodated_test_outside_of_cat !== "undefined" &&
        assigned_tests[i].accommodated_test_outside_of_cat
      ) {
        accommodatedTestOutsideOfCat = true;
      }
      // pushing needed data in data array (needed for GenericTable component)
      data.push({
        column_1: assigned_tests[i][`test_name_${this.props.currentLanguage}`],
        column_2: (
          <>
            {/* only show accommodation request button if this is a UIT test (where uit_invite_id is defined) OR if this is an accommodated test done oustide of CAT (pending approval request) OR if accommodations have already been applied from the TA (UIT invites) */}
            {(accommodatedTestOutsideOfCat ||
              (assigned_tests[i].uit_invite_id !== null &&
                assigned_tests[i].accommodation_request_id === null)) && (
              <StyledTooltip
                id={`my-tests-request-accommodation-row-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`my-tests-request-accommodation-row-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon
                            icon={
                              assigned_tests[i].is_alternate_test_request
                                ? faFileCircleExclamation
                                : faUniversalAccess
                            }
                          />
                        </>
                      }
                      action={
                        assigned_tests[i].user_accommodation_file_id === null
                          ? () => this.openRequestAccommodationsPopup(assigned_tests[i])
                          : () => this.openAccommodationsRequestDetailsPopup(assigned_tests[i])
                      }
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={
                        assigned_tests[i].user_accommodation_file_id === null
                          ? LOCALIZE.formatString(
                              LOCALIZE.dashboard.table.requestAccommodationsAccessibilityLabel,
                              assigned_tests[i][`test_name_${this.props.currentLanguage}`]
                            )
                          : LOCALIZE.formatString(
                              assigned_tests[i].is_alternate_test_request
                                ? LOCALIZE.accommodationsRequestDetailsPopup
                                    .viewAlternateTestRequestDetailsAccessibilityLabel
                                : LOCALIZE.accommodationsRequestDetailsPopup
                                    .viewAccommodationsRequestDetailsAccessibilityLabel,
                              assigned_tests[i][`test_name_${this.props.currentLanguage}`]
                            )
                      }
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {assigned_tests[i].user_accommodation_file_id === null
                        ? LOCALIZE.dashboard.table.requestAccommodations
                        : assigned_tests[i].is_alternate_test_request
                          ? LOCALIZE.accommodationsRequestDetailsPopup
                              .viewAlternateTestRequestDetails
                          : LOCALIZE.accommodationsRequestDetailsPopup
                              .viewAccommodationsRequestDetails}
                    </p>
                  </div>
                }
              />
            )}
            {/* show start/resume test button for all assigned tests except accommodated tests done outside of CAT */}
            {!accommodatedTestOutsideOfCat && (
              <StyledTooltip
                id={`my-tests-start-resume-test-row-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`my-tests-start-resume-test-row-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon
                            icon={assigned_tests[i].test_started ? faShare : faPlayCircle}
                          />
                        </>
                      }
                      action={() =>
                        this.viewTest(
                          assigned_tests[i].id,
                          assigned_tests[i].test_id,
                          assigned_tests[i].accommodation_request,
                          assigned_tests[i].user_accommodation_file_id,
                          assigned_tests[i].test_started
                        )
                      }
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={
                        assigned_tests[i].test_started
                          ? LOCALIZE.formatString(
                              LOCALIZE.dashboard.table.resumeTestAccessibilityLabel,
                              assigned_tests[i][`test_name_${this.props.currentLanguage}`]
                            )
                          : LOCALIZE.formatString(
                              LOCALIZE.dashboard.table.startTestAccessibilityLabel,
                              assigned_tests[i][`test_name_${this.props.currentLanguage}`]
                            )
                      }
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {assigned_tests[i].test_started
                        ? LOCALIZE.dashboard.table.resumeTest
                        : LOCALIZE.dashboard.table.startTest}
                    </p>
                  </div>
                }
              />
            )}
          </>
        )
      });
    }
    // getting current date
    let currentDate = new Date();
    currentDate = getFormattedDate(
      `${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-${currentDate.getDate()}`
    );

    // add assigned virtual tests
    for (let i = 0; i < assigned_virtual_tests.length; i++) {
      const currAvt = assigned_virtual_tests[i];
      const disabled = currAvt.date !== currentDate;

      // skill_type - skill_sub_type
      // formatting test skill type / sub-type
      let name = currAvt[`test_skill_type_${this.props.currentLanguage}_name`];
      if (currAvt.test_skill_sub_type_id !== null) {
        name += ` - ${currAvt[`test_skill_sub_type_${this.props.currentLanguage}_name`]}`;
      }
      data.push({
        column_1: name,
        column_2: (
          <>
            <StyledTooltip
              id={`my-tests-virtual-meeting-test-row-${i}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`my-tests-virtual-meeting-test-row-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faVideo} />
                      </>
                    }
                    action={() => this.openTeamsCall(currAvt.meeting_link)}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.PRIMARY}
                    ariaLabel={LOCALIZE.formatString(
                      LOCALIZE.dashboard.table.joinMeetingButtonLabel,
                      name
                    )}
                    disabled={disabled}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>{LOCALIZE.dashboard.table.joinMeetingButton}</p>
                </div>
              }
            />
          </>
        )
      });
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: {},
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    this.setState({
      rowsDefinition: rowsDefinition
    });
  };

  handleUserProfileRedirect = () => {
    // setting My Profile side navigation item redux states
    this.props.setMyProfileSideNavigationSelectedItem("profile-accommodations");
    // redirecting user to profile page
    setTimeout(() => {
      history.push(PATH.profile);
    }, 100);
  };

  openRequestAccommodationsPopup = assignedTestData => {
    // getting available accommodation request type options
    const requestTypeOptions = getAccommodationRequestTypeOptions();
    this.setState({
      showRequestAccommodationsPopup: true,
      showNeedAccommodationsProfilePopup: !assignedTestData.candidate_has_accommodations_profile,
      selectedTestData: assignedTestData,
      requestTypeOptions: requestTypeOptions
    });
  };

  closeRequestAccommodationsPopup = () => {
    this.setState({
      showRequestAccommodationsPopup: false,
      showNeedAccommodationsProfilePopup: false,
      selectedTestData: {},
      commentsOrTestAdministeredDetails: "",
      profileUpToDateCheckboxChecked: false,
      showAccommodationRequestSentSuccessfullyPopup: false,
      isValidForm: false,
      requestTypeSelectedOption: []
    });
  };

  openAccommodationsRequestDetailsPopup = assignedTestData => {
    // updating needed states
    this.setState({
      showAccommodationsRequestDetailsPopup: true,
      selectedTestData: assignedTestData
    });
  };

  closeAccommodationsRequestDetailsPopup = () => {
    this.setState(
      {
        showAccommodationsRequestDetailsPopup: false,
        selectedTestData: {}
      },
      () => {
        // populating table
        this.populateTable();
      }
    );
  };

  openCancelRequestConfirmationPopup = () => {
    this.setState({ showCancelRequestConfirmationPopup: true });
  };

  closeCancelRequestConfirmationPopup = () => {
    this.setState({ showCancelRequestConfirmationPopup: false });
  };

  getSelectedRequestTypeOption = selectedOption => {
    this.setState({ requestTypeSelectedOption: selectedOption }, () => {
      // validating form
      this.validateForm();
    });
  };

  getCommentsOrTestAdministeredDetailsContent = event => {
    const commentsOrTestAdministeredDetails = event.target.value;
    this.setState(
      {
        commentsOrTestAdministeredDetails: commentsOrTestAdministeredDetails
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  getCheckboxState = event => {
    this.setState(
      {
        profileUpToDateCheckboxChecked: event.target.checked
      },
      () => {
        this.validateForm();
      }
    );
  };

  validateForm = () => {
    // initializing needed variables
    let isValidForm = false;
    let isValidTextBoxField = true;

    // alternate test or both request
    if (
      this.state.requestTypeSelectedOption.label ===
        LOCALIZE.dashboard.requestAccommodationsPopup.requestTypeOptions.alternateTest ||
      this.state.requestTypeSelectedOption.label ===
        LOCALIZE.dashboard.requestAccommodationsPopup.requestTypeOptions.both
    ) {
      isValidTextBoxField = this.state.commentsOrTestAdministeredDetails !== "";
    }

    // all required fields have been filled properly
    if (this.state.profileUpToDateCheckboxChecked && isValidTextBoxField) {
      // setting isValidForm to true
      isValidForm = true;
    }

    // updating state
    this.setState({ isValidForm: isValidForm });
  };

  handleSubmitAccommodationsRequest = () => {
    this.setState({ actionInProgress: true }, () => {
      // creating body
      const body = {
        comments: this.state.commentsOrTestAdministeredDetails,
        is_uit: true,
        assigned_test_id: this.state.selectedTestData.id,
        is_alternate_test_request: this.state.requestTypeSelectedOption.is_alternate_test_request
      };
      // creating new accommodation request
      this.props.sendUserAccommodationRequest(body).then(response => {
        // success
        if (response.ok) {
          // refreshing table
          this.populateTable();
          // show success popup
          this.setState(
            {
              showAccommodationRequestSentSuccessfullyPopup: true
            },
            () => {
              this.setState({ actionInProgress: false });
            }
          );
          // should never happen
        } else {
          throw new Error("An error occurred during the creation of the accommodation request");
        }
      });
    });
  };

  viewTest = (
    currentAssignedTestId,
    testId,
    accommodationRequest,
    userAccommodationFile,
    testStarted = false,
    bypassRetestValidation = false,
    bypassAccommodationRequestValidation = false
  ) => {
    // checking if there is an ongoing accommodations request in progress (not completed yet)
    this.props.checkForOngoingUserAccommodationRequestProcess(currentAssignedTestId).then(bool => {
      // not bypassing the accommodation request validation
      if (!bypassAccommodationRequestValidation) {
        // ongoing user accommodation request process
        if (bool) {
          // show popups display
          this.setState({
            showOngoingAccommodationRequestProcessPopup: true,
            selectedTestData: {
              id: currentAssignedTestId,
              testId: testId,
              accommodationRequest: accommodationRequest,
              userAccommodationFile: userAccommodationFile,
              testStarted: testStarted
            }
          });
          // no ongoing user accommodation request process
        } else {
          // bypassing the retest validation
          if (bypassRetestValidation) {
            // calling the view test logic
            this.handleViewTestLogic(
              currentAssignedTestId,
              testId,
              accommodationRequest,
              userAccommodationFile
            );
            // validate the restest period
          } else {
            this.props
              .isWithinTheRetestPeriod(testId, null, null, null)
              .then(isWithinRetestPeriod => {
                // if is within retest period + bypassRetestValidation is set to false
                if (isWithinRetestPeriod) {
                  this.setState({
                    showRetestPeriodWarningPopup: true,
                    showOngoingAccommodationRequestProcessPopup: false,
                    popupCheckboxChecked: false,
                    selectedTestData: {
                      id: currentAssignedTestId,
                      testId: testId,
                      accommodationRequest: accommodationRequest,
                      testStarted: testStarted
                    }
                  });
                  // respects the retest period
                } else {
                  // calling the view test logic
                  this.handleViewTestLogic(currentAssignedTestId, testId, accommodationRequest);
                }
              });
          }
        }
        // bypassing the accommodation request validation
      } else {
        // bypassing the retest validation
        if (bypassRetestValidation) {
          // calling the view test logic
          this.handleViewTestLogic(currentAssignedTestId, testId, accommodationRequest);
          // validate the restest period
        } else {
          this.props
            .isWithinTheRetestPeriod(testId, null, null, null)
            .then(isWithinRetestPeriod => {
              // if is within retest period + bypassRetestValidation is set to false
              if (isWithinRetestPeriod) {
                this.setState({
                  showRetestPeriodWarningPopup: true,
                  showOngoingAccommodationRequestProcessPopup: false,
                  popupCheckboxChecked: false,
                  selectedTestData: {
                    id: currentAssignedTestId,
                    testId: testId,
                    accommodationRequest: accommodationRequest,
                    testStarted: testStarted
                  }
                });
                // respects the retest period
              } else {
                // calling the view test logic
                this.handleViewTestLogic(currentAssignedTestId, testId, accommodationRequest);
              }
            });
        }
      }
    });
  };

  handleViewTestLogic = (
    currentAssignedTestId,
    testId,
    accommodationRequest,
    userAccommodationFile
  ) => {
    // getting current test status
    this.props.getCurrentTestStatus(currentAssignedTestId).then(testStatusData => {
      // current test status is either CHECKED_IN, PRE_TEST or TRANSITION
      if (
        testStatusData.status_codename === TEST_STATUS.CHECKED_IN ||
        testStatusData.status_codename === TEST_STATUS.PRE_TEST ||
        testStatusData.status_codename === TEST_STATUS.TRANSITION
      ) {
        // resetting needed redux states
        this.resetAllRedux();
        // updating needed redux states
        if (testStatusData.status_codename === TEST_STATUS.CHECKED_IN) {
          this.props.updateTestStatus(currentAssignedTestId, TEST_STATUS.PRE_TEST);
        }
        this.props.updateAssignedTestId(
          currentAssignedTestId,
          testId,
          accommodationRequest,
          userAccommodationFile
        );
        this.props.preTestOrTransitionTest();
        this.props.setContentUnLoaded();
        // redirecting user to the test page
        history.push(PATH.testBase);
        // current test status is LOCKED, PAUSED or UNASSIGNED
      } else if (
        testStatusData.status_codename === TEST_STATUS.LOCKED ||
        testStatusData.status_codename === TEST_STATUS.PAUSED ||
        testStatusData.status_codename === TEST_STATUS.UNASSIGNED
      ) {
        // reloading the page
        window.location.reload();
        // current test status is not the right one (not READY/LOCKED/PAUSED)
      } else {
        // invalidating test + redirecting to invalidated test page
        this.props.invalidateTest();
        this.resetAllRedux();
        history.push(PATH.invalidateTest);
      }
    });
  };

  closeRetestPeriodWarningPopup = () => {
    this.setState({
      showRetestPeriodWarningPopup: false,
      popupCheckboxChecked: false,
      ongoingAccommodationRequestPopupCheckboxChecked: false,
      selectedTestData: {}
    });
  };

  closeOngoingAccommodationRequestProcessPopup = () => {
    this.setState({
      showOngoingAccommodationRequestProcessPopup: false,
      ongoingAccommodationRequestPopupCheckboxChecked: false
    });
  };

  resetAllRedux = () => {
    this.props.resetTestFactoryState();
    this.props.resetInboxState();
    this.props.resetNotepadState();
    this.props.resetAssignedTestState();
    this.props.resetQuestionListState();
    this.props.resetTopTabsState();
  };

  /* checking in the assigned tests list if there is an active test (active, locked or paused)
  return true if that's the case */
  checkForActiveTest = assigned_tests => {
    let activeTest = false;
    let testAccessCode = null;
    let unsupervisedTest = false;
    for (let i = 0; i < assigned_tests.length; i++) {
      // if test is active
      if (
        assigned_tests[i].status_codename === TEST_STATUS.ACTIVE ||
        assigned_tests[i].status_codename === TEST_STATUS.LOCKED ||
        assigned_tests[i].status_codename === TEST_STATUS.PAUSED
      ) {
        this.props.updateAssignedTestId(
          assigned_tests[i].id,
          assigned_tests[i].test_id,
          assigned_tests[i].accommodation_request_id,
          assigned_tests[i].user_accommodation_file_id
        );
        testAccessCode = assigned_tests[i].test_access_code;
        activeTest = true;
        if (assigned_tests[i].uit_invite_id !== null) {
          unsupervisedTest = true;
        }
      }
    }
    return {
      activeTest: activeTest,
      testAccessCode: testAccessCode,
      unsupervisedTest: unsupervisedTest
    };
  };

  handleCheckIn = () => {
    this.populateTable();
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.dashboard.table.columnOne,
        style: { width: "50%" }
      },
      {
        label: LOCALIZE.dashboard.table.columnTwo,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);
    return (
      <div>
        <h2>{LOCALIZE.dashboard.sideNavItems.takeATest}</h2>
        {this.props.profileCompleted && <p>{LOCALIZE.candidateCheckIn.page.description}</p>}
        <div id="candidate-check-in">
          <CandidateCheckIn handleCheckIn={this.handleCheckIn} />
        </div>
        {(this.state.assigned_tests.length > 0 || this.state.assigned_virtual_tests.length > 0) && (
          <div role="region" aria-labelledby="test-assignment-table">
            <div style={styles.table}>
              <GenericTable
                classnamePrefix="assigned-tests"
                columnsDefinition={columnsDefinition}
                rowsDefinition={this.state.rowsDefinition}
                emptyTableMessage={LOCALIZE.dashboard.table.noTests}
                currentlyLoading={this.state.isLoading}
                tableWithButtons={true}
              />
            </div>
          </div>
        )}
        <PopupBox
          show={this.state.showRetestPeriodWarningPopup}
          title={LOCALIZE.retestPeriodWarningPopup.dashboardTitle}
          handleClose={() => {}}
          size={"lg"}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={LOCALIZE.retestPeriodWarningPopup.systemMessageDescription}
                  />
                }
              </p>
              <p>{LOCALIZE.retestPeriodWarningPopup.description}</p>
              <div>
                <div>
                  <input
                    type="checkbox"
                    id={"retest-period-wish-to-proceed-checkbox"}
                    value={this.state.popupCheckboxChecked}
                    style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
                    onChange={() => {
                      this.setState({ popupCheckboxChecked: !this.state.popupCheckboxChecked });
                    }}
                  />
                  <label htmlFor={"retest-period-wish-to-proceed-checkbox"}>
                    {LOCALIZE.retestPeriodWarningPopup.checkboxLabel}
                  </label>
                </div>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeRetestPeriodWarningPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            <>
              <FontAwesomeIcon
                icon={this.state.selectedTestData.testStarted ? faShare : faPlayCircle}
              />
              <span style={styles.buttonLabel}>
                {this.state.selectedTestData.testStarted
                  ? LOCALIZE.dashboard.table.resumeTest
                  : LOCALIZE.dashboard.table.startTest}
              </span>
            </>
          }
          rightButtonAction={() =>
            this.viewTest(
              this.state.selectedTestData.id,
              this.state.selectedTestData.testId,
              this.state.selectedTestData.accommodationRequest,
              this.state.selectedTestData.userAccommodationFile,
              this.state.selectedTestData.testStarted,
              // bypassing retest validation
              true,
              // bypassing retest validation
              true
            )
          }
          rightButtonState={
            this.state.popupCheckboxChecked ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
        />
        <PopupBox
          show={this.state.showOngoingAccommodationRequestProcessPopup}
          title={LOCALIZE.dashboard.ongoingAccommodationRequestProcessPopup.title}
          handleClose={() => {}}
          size={"lg"}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.error}
                    title={LOCALIZE.commons.warning}
                    message={
                      LOCALIZE.dashboard.ongoingAccommodationRequestProcessPopup
                        .systemMessageDescription
                    }
                  />
                }
              </p>
              <div>
                <div>
                  <input
                    type="checkbox"
                    id={"ongoing-accommodation-request-wish-to-proceed-checkbox"}
                    value={this.state.ongoingAccommodationRequestPopupCheckboxChecked}
                    style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
                    onChange={() => {
                      this.setState({
                        ongoingAccommodationRequestPopupCheckboxChecked:
                          !this.state.ongoingAccommodationRequestPopupCheckboxChecked
                      });
                    }}
                  />
                  <label htmlFor={"ongoing-accommodation-request-wish-to-proceed-checkbox"}>
                    {LOCALIZE.dashboard.ongoingAccommodationRequestProcessPopup.checkboxDescription}
                  </label>
                </div>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeOngoingAccommodationRequestProcessPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            <>
              <FontAwesomeIcon
                icon={this.state.selectedTestData.testStarted ? faShare : faPlayCircle}
              />
              <span style={styles.buttonLabel}>
                {this.state.selectedTestData.testStarted
                  ? LOCALIZE.dashboard.table.resumeTest
                  : LOCALIZE.dashboard.table.startTest}
              </span>
            </>
          }
          rightButtonAction={() =>
            this.viewTest(
              this.state.selectedTestData.id,
              this.state.selectedTestData.testId,
              this.state.selectedTestData.accommodationRequest,
              this.state.selectedTestData.userAccommodationFile,
              this.state.selectedTestData.testStarted,
              false,
              // bypassing accommodation request validation
              true
            )
          }
          rightButtonState={
            this.state.ongoingAccommodationRequestPopupCheckboxChecked
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
        />
        {Object.keys(this.state.selectedTestData).length > 0 && (
          <PopupBox
            show={
              this.state.showRequestAccommodationsPopup ||
              this.state.showNeedAccommodationsProfilePopup ||
              this.state.showAccommodationRequestSentSuccessfullyPopup
            }
            title={
              !this.state.showAccommodationRequestSentSuccessfullyPopup
                ? LOCALIZE.dashboard.requestAccommodationsPopup.alternativeTitle
                : LOCALIZE.dashboard.requestAccommodationsPopup.successTitle
            }
            handleClose={() => {}}
            size={
              !this.state.showAccommodationRequestSentSuccessfullyPopup &&
              !this.state.showNeedAccommodationsProfilePopup
                ? "xl"
                : "lg"
            }
            description={
              <>
                {!this.state.showAccommodationRequestSentSuccessfullyPopup ? (
                  <>
                    {this.state.showNeedAccommodationsProfilePopup ? (
                      <div>
                        <SystemMessage
                          messageType={MESSAGE_TYPE.info}
                          title={LOCALIZE.commons.info}
                          message={LOCALIZE.formatString(
                            LOCALIZE.dashboard.requestAccommodationsPopup
                              .needAccommodationsProfileSystemMessageDescription,
                            <button
                              aria-label={
                                LOCALIZE.dashboard.requestAccommodationsPopup
                                  .incompletedAccommodationsProfileLink
                              }
                              tabIndex="0"
                              onClick={this.handleUserProfileRedirect}
                              style={styles.profileLink}
                            >
                              {
                                LOCALIZE.dashboard.requestAccommodationsPopup
                                  .incompletedAccommodationsProfileLink
                              }
                            </button>
                          )}
                        />
                      </div>
                    ) : (
                      <div>
                        <p>
                          {LOCALIZE.formatString(
                            LOCALIZE.dashboard.requestAccommodationsPopup.description,
                            <span style={styles.boldText}>
                              {
                                this.state.selectedTestData[
                                  `test_name_${this.props.currentLanguage}`
                                ]
                              }
                            </span>
                          )}
                        </p>
                        <div style={styles.formContainer}>
                          <Row style={styles.fieldRowContainer}>
                            <Col
                              xl={columnSizes.firstColumn.xl}
                              lg={columnSizes.firstColumn.lg}
                              md={columnSizes.firstColumn.md}
                              sm={columnSizes.firstColumn.sm}
                              xs={columnSizes.firstColumn.xs}
                            >
                              <label
                                id="request-accommodations-request-type-label"
                                style={styles.boldText}
                              >
                                {LOCALIZE.dashboard.requestAccommodationsPopup.requestTypeLabel}
                              </label>
                            </Col>
                            <Col
                              xl={columnSizes.secondColumn.xl}
                              lg={columnSizes.secondColumn.lg}
                              md={columnSizes.secondColumn.md}
                              sm={columnSizes.secondColumn.sm}
                              xs={columnSizes.secondColumn.xs}
                            >
                              <DropdownSelect
                                idPrefix="request-accommodations-request-type"
                                ariaLabelledBy="request-accommodations-request-type-label"
                                ariaRequired={true}
                                options={this.state.requestTypeOptions}
                                onChange={this.getSelectedRequestTypeOption}
                                defaultValue={this.state.requestTypeSelectedOption}
                                hasPlaceholder={true}
                                orderByValues={true}
                              />
                            </Col>
                          </Row>
                          {Object.keys(this.state.requestTypeSelectedOption).length > 0 && (
                            <>
                              <Row style={styles.fieldRowContainer}>
                                <Col
                                  xl={columnSizes.firstColumn.xl}
                                  lg={columnSizes.firstColumn.lg}
                                  md={columnSizes.firstColumn.md}
                                  sm={columnSizes.firstColumn.sm}
                                  xs={columnSizes.firstColumn.xs}
                                >
                                  <label
                                    htmlFor="request-accommodations-comments-or-test-administered-details-input"
                                    style={styles.boldText}
                                  >
                                    {this.state.requestTypeSelectedOption.label ===
                                    LOCALIZE.dashboard.requestAccommodationsPopup.requestTypeOptions
                                      .accommodations
                                      ? // accommodation request
                                        LOCALIZE.dashboard.requestAccommodationsPopup.commentLabel
                                      : // alternate test or both request
                                        LOCALIZE.dashboard.requestAccommodationsPopup
                                          .testAdministeredDetailsLabel}
                                  </label>
                                </Col>
                                <Col
                                  xl={columnSizes.secondColumn.xl}
                                  lg={columnSizes.secondColumn.lg}
                                  md={columnSizes.secondColumn.md}
                                  sm={columnSizes.secondColumn.sm}
                                  xs={columnSizes.secondColumn.xs}
                                >
                                  <textarea
                                    id="request-accommodations-comments-or-test-administered-details-input"
                                    className={"valid-field"}
                                    style={{ ...styles.textAreaInput, ...accommodationsStyle }}
                                    onChange={this.getCommentsOrTestAdministeredDetailsContent}
                                    value={this.state.commentsOrTestAdministeredDetails}
                                    maxLength="5000"
                                  ></textarea>
                                </Col>
                              </Row>
                              <Row className="align-items-center" style={styles.rowContainer}>
                                <input
                                  type="checkbox"
                                  id="request-accommodations-profile-up-to-date-checkbox-input"
                                  value={this.state.profileUpToDateCheckboxChecked}
                                  style={{
                                    ...styles.checkbox,
                                    ...{ transform: checkboxTransformScale }
                                  }}
                                  onChange={this.getCheckboxState}
                                />

                                <label
                                  htmlFor="request-accommodations-profile-up-to-date-checkbox-input"
                                  style={styles.noMargin}
                                >
                                  {LOCALIZE.formatString(
                                    LOCALIZE.dashboard.requestAccommodationsPopup
                                      .checkboxDescription,
                                    <button
                                      aria-label={
                                        LOCALIZE.dashboard.requestAccommodationsPopup
                                          .checkboxDescriptionLink
                                      }
                                      tabIndex="0"
                                      onClick={this.handleUserProfileRedirect}
                                      style={styles.profileLink}
                                    >
                                      {
                                        LOCALIZE.dashboard.requestAccommodationsPopup
                                          .checkboxDescriptionLink
                                      }
                                    </button>
                                  )}
                                </label>
                              </Row>
                            </>
                          )}
                        </div>
                      </div>
                    )}
                  </>
                ) : (
                  <div>
                    <SystemMessage
                      messageType={MESSAGE_TYPE.success}
                      title={LOCALIZE.commons.success}
                      message={
                        LOCALIZE.dashboard.requestAccommodationsPopup
                          .successSystemMessageDescription
                      }
                    />
                  </div>
                )}
              </>
            }
            leftButtonType={
              !this.state.showAccommodationRequestSentSuccessfullyPopup
                ? BUTTON_TYPE.secondary
                : BUTTON_TYPE.none
            }
            leftButtonTitle={LOCALIZE.commons.cancel}
            leftButtonIcon={
              !this.state.showAccommodationRequestSentSuccessfullyPopup ? faTimes : ""
            }
            leftButtonAction={
              !this.state.showAccommodationRequestSentSuccessfullyPopup
                ? this.closeRequestAccommodationsPopup
                : () => {}
            }
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={
              this.state.actionInProgress ? (
                <div style={styles.customLoadingContainer}>
                  {/* this div is useful to get the right size of the button while loading */}
                  <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
                    <FontAwesomeIcon icon={faPaperPlane} style={styles.icon} />
                    {LOCALIZE.dashboard.requestAccommodationsPopup.rightButton}
                  </div>
                  <div style={styles.loadingOverlappingStyle}>
                    <label className="fa fa-spinner fa-spin">
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  </div>
                </div>
              ) : !this.state.showAccommodationRequestSentSuccessfullyPopup ? (
                LOCALIZE.dashboard.requestAccommodationsPopup.rightButton
              ) : (
                LOCALIZE.commons.close
              )
            }
            rightButtonIcon={
              this.state.actionInProgress
                ? ""
                : !this.state.showAccommodationRequestSentSuccessfullyPopup
                  ? faPaperPlane
                  : faTimes
            }
            rightButtonAction={
              !this.state.showAccommodationRequestSentSuccessfullyPopup
                ? this.handleSubmitAccommodationsRequest
                : this.closeRequestAccommodationsPopup
            }
            rightButtonState={
              this.state.actionInProgress
                ? BUTTON_STATE.disabled
                : this.state.isValidForm
                  ? BUTTON_STATE.enabled
                  : BUTTON_STATE.disabled
            }
          />
        )}
        {Object.keys(this.state.selectedTestData).length > 0 &&
          this.state.showAccommodationsRequestDetailsPopup && (
            <AccommodationsRequestDetailsPopup
              source={ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.candidate}
              userAccommodationFileId={this.state.selectedTestData.user_accommodation_file_id}
              selectedUserAccommodationFileData={this.state.selectedTestData}
              showPopup={this.state.showAccommodationsRequestDetailsPopup}
              closeAccommodationsRequestDetailsPopup={this.closeAccommodationsRequestDetailsPopup}
              // not providing isDraft props, since the bool value is defined inside that component by the status of the file because it is triggered by the candidate
            />
          )}
      </div>
    );
  }
}

export { AssignedTestTable as UnconnectedAssignedTestTable };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    isTestActive: state.testStatus.isTestActive,
    username: state.user.username,
    accommodations: state.accommodations,
    profileCompleted: state.user.profileCompleted
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAssignedTests,
      getAssignedVirtualTests,
      updateAssignedTestId,
      activateTest,
      preTestOrTransitionTest,
      deactivateTest,
      resetTestFactoryState,
      setContentUnLoaded,
      resetAssignedTestState,
      resetNotepadState,
      resetQuestionListState,
      resetTopTabsState,
      updateTestStatus,
      setErrorStatus,
      getCurrentTestStatus,
      invalidateTest,
      resetInboxState,
      isWithinTheRetestPeriod,
      getTestCentersForUserAccommodationRequest,
      sendUserAccommodationRequest,
      checkForOngoingUserAccommodationRequestProcess,
      setMyProfileSideNavigationSelectedItem
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AssignedTestTable));

export { styles as TEST_TABLE_STYLES };
