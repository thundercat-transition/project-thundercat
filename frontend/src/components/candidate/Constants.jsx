// mirror of StaticConsumedReservationCodeStatus under ...\backend\static\consumed_reservation_code_statuses.py

import LOCALIZE from "../../text_resources";

const staticConsumedReservationCodeStatus = {
  // reservation code is associated to a test session (displayed in Manage Reservations table)
  BOOKED: "booked",
  // candidate withdrawn from reservation code (no longer displayed in any tables)
  WITHDRAWN: "withdrawn",
  // candidate never used their reservation code, so it has been expired by the system (no longer displayed in any tables)
  EXPIRED: "expired",
  // candidate consumed their reservation code (displayed in My Reservation Codes table)
  RESERVED: "reserved",
  // reservation code has been revoked by an HR Coordinator
  REVOKED: "revoked",
  // candidate consumed their reservation code & is now paired with an OLA Test Assessor (displayed in Manage Reservations table)
  PAIRED_OLA: "paired_ola"
};

export function getAccommodationRequestTypeOptions() {
  // initializing options array
  const options = [];
  // accommodations option
  options.push({
    label: LOCALIZE.dashboard.requestAccommodationsPopup.requestTypeOptions.accommodations,
    value: 1,
    is_alternate_test_request: false
  });
  // alternate test option
  options.push({
    label: LOCALIZE.dashboard.requestAccommodationsPopup.requestTypeOptions.alternateTest,
    value: 2,
    is_alternate_test_request: true
  });
  // both option
  options.push({
    label: LOCALIZE.dashboard.requestAccommodationsPopup.requestTypeOptions.both,
    value: 3,
    is_alternate_test_request: false // considered false when both option
  });
  // returning options array
  return options;
}

export default staticConsumedReservationCodeStatus;
