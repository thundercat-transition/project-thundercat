import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import getCheckboxTransformScale from "../../helpers/checkboxTransformScale";

const styles = {
  checkboxContainer: {
    margin: "48px 0 24px 0",
    display: "flex",
    alignItems: "center"
  },
  checkbox: {
    verticalAlign: "middle",
    marginRight: 16
  },
  boldText: {
    fontWeight: "bold"
  },
  noMargin: {
    margin: 0
  }
};
class TermsOfUseContent extends Component {
  static propTypes = {
    handleAcceptTermsOfUseCheckbox: PropTypes.func.isRequired,
    termsOfUseChecked: PropTypes.bool.isRequired
  };

  render() {
    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    return (
      <div>
        {/* GENERAL */}
        <div>
          <h2>{LOCALIZE.termsOfUse.general.title}</h2>
          <p>{LOCALIZE.termsOfUse.general.para1}</p>
          <p>{LOCALIZE.termsOfUse.general.para2}</p>
          <p>{LOCALIZE.termsOfUse.general.para3}</p>
        </div>
        {/* DEFINITIONS */}
        <div>
          <h2>{LOCALIZE.termsOfUse.definitions.title}</h2>
          <p>
            <span style={styles.boldText}>{LOCALIZE.termsOfUse.definitions.para1Bold}</span>
            <span>{LOCALIZE.termsOfUse.definitions.para1}</span>
          </p>
        </div>
        {/* ACCEPTABLE USES */}
        <div>
          <h2>{LOCALIZE.termsOfUse.acceptableUses.title}</h2>
          <p>{LOCALIZE.termsOfUse.acceptableUses.para1}</p>
          <ul>
            <li>{LOCALIZE.termsOfUse.acceptableUses.bullet1}</li>
            <li>{LOCALIZE.termsOfUse.acceptableUses.bullet2}</li>
          </ul>
        </div>
        {/* PROHIBITED USES */}
        <div>
          <h2>{LOCALIZE.termsOfUse.prohibitedUses.title}</h2>
          <p>{LOCALIZE.termsOfUse.prohibitedUses.para1}</p>
          <p>{LOCALIZE.termsOfUse.prohibitedUses.para2}</p>
          <ul>
            <li>{LOCALIZE.termsOfUse.prohibitedUses.bullet1}</li>
            <li>{LOCALIZE.termsOfUse.prohibitedUses.bullet2}</li>
            <li>{LOCALIZE.termsOfUse.prohibitedUses.bullet3}</li>
            <li>{LOCALIZE.termsOfUse.prohibitedUses.bullet4}</li>
            <li>{LOCALIZE.termsOfUse.prohibitedUses.bullet5}</li>
            <li>{LOCALIZE.termsOfUse.prohibitedUses.bullet6}</li>
            <li>{LOCALIZE.termsOfUse.prohibitedUses.bullet7}</li>
            <li>{LOCALIZE.termsOfUse.prohibitedUses.bullet8}</li>
            <li>{LOCALIZE.termsOfUse.prohibitedUses.bullet9}</li>
          </ul>
        </div>
        {/* SECURITY */}
        <div>
          <h2>{LOCALIZE.termsOfUse.security.title}</h2>
          <p>{LOCALIZE.termsOfUse.security.para1}</p>
          <p>{LOCALIZE.termsOfUse.security.para2}</p>
          <ul>
            <li>{LOCALIZE.termsOfUse.security.bullet1}</li>
            <li>{LOCALIZE.termsOfUse.security.bullet2}</li>
            <li>{LOCALIZE.termsOfUse.security.bullet3}</li>
            <li>{LOCALIZE.termsOfUse.security.bullet4}</li>
            <li>
              {LOCALIZE.formatString(
                LOCALIZE.termsOfUse.security.bullet5,
                <a href={`mailto:${LOCALIZE.termsOfUse.security.bullet5Link}`}>
                  {LOCALIZE.termsOfUse.security.bullet5Link}
                </a>
              )}
            </li>
          </ul>
        </div>
        {/* INTELLECTUAL PROPERTY RIGHTS */}
        <div>
          <h2>{LOCALIZE.termsOfUse.intellectualPropertyRights.title}</h2>
          <p>{LOCALIZE.termsOfUse.intellectualPropertyRights.para1}</p>
          <ul>
            <li>{LOCALIZE.termsOfUse.intellectualPropertyRights.bullet1}</li>
            <li>{LOCALIZE.termsOfUse.intellectualPropertyRights.bullet2}</li>
            <li>{LOCALIZE.termsOfUse.intellectualPropertyRights.bullet3}</li>
          </ul>
        </div>
        {/* ENSURING THE INTEGRITY OF THE TESTING PROCESS */}
        <div>
          <h2>{LOCALIZE.termsOfUse.ensuringTheIntegrityOfTheTestingProcess.title}</h2>
          <ul>
            <li>{LOCALIZE.termsOfUse.ensuringTheIntegrityOfTheTestingProcess.bullet1}</li>
            <li>{LOCALIZE.termsOfUse.ensuringTheIntegrityOfTheTestingProcess.bullet2}</li>
            <li>{LOCALIZE.termsOfUse.ensuringTheIntegrityOfTheTestingProcess.bullet3}</li>
            <li>{LOCALIZE.termsOfUse.ensuringTheIntegrityOfTheTestingProcess.bullet4}</li>
          </ul>
          <p>{LOCALIZE.termsOfUse.ensuringTheIntegrityOfTheTestingProcess.para1}</p>
          <p>
            {LOCALIZE.formatString(
              LOCALIZE.termsOfUse.ensuringTheIntegrityOfTheTestingProcess.para2,
              <a
                href={`mailto:${
                  LOCALIZE.termsOfUse.ensuringTheIntegrityOfTheTestingProcess.para2Link
                }`}
              >
                {LOCALIZE.termsOfUse.ensuringTheIntegrityOfTheTestingProcess.para2Link}
              </a>
            )}
          </p>
        </div>
        {/* CONSEQUENCES OF A BREACH OF TERMS OF USE */}
        <div>
          <h2>{LOCALIZE.termsOfUse.consequencesOfABreachOfTermsOfUse.title}</h2>
          <ul>
            <li>{LOCALIZE.termsOfUse.consequencesOfABreachOfTermsOfUse.bullet1}</li>
            <li>{LOCALIZE.termsOfUse.consequencesOfABreachOfTermsOfUse.bullet2}</li>
            <li>
              {LOCALIZE.formatString(
                LOCALIZE.termsOfUse.consequencesOfABreachOfTermsOfUse.bullet3,
                <a
                  href={`${LOCALIZE.termsOfUse.consequencesOfABreachOfTermsOfUse.bullet3Link}`}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {LOCALIZE.termsOfUse.consequencesOfABreachOfTermsOfUse.bullet3LinkLabel}
                </a>
              )}
            </li>
            <li>{LOCALIZE.termsOfUse.consequencesOfABreachOfTermsOfUse.bullet4}</li>
          </ul>
        </div>
        {/* CHECKBOX */}
        <div style={styles.checkboxContainer}>
          <input
            type="checkbox"
            id="reservation-details-terms-of-use-popup-checkbox"
            checked={this.props.termsOfUseChecked}
            style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
            onClick={this.props.handleAcceptTermsOfUseCheckbox}
          />
          <label htmlFor="reservation-details-terms-of-use-popup-checkbox" style={styles.noMargin}>
            {LOCALIZE.termsOfUse.checkboxLabel}
          </label>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TermsOfUseContent);
