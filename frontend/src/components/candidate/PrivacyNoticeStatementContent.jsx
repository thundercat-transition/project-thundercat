import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import getCheckboxTransformScale from "../../helpers/checkboxTransformScale";

const styles = {
  checkboxContainer: {
    margin: "48px 0 24px 0",
    display: "flex",
    alignItems: "center"
  },
  checkbox: {
    verticalAlign: "middle",
    marginRight: 16
  },
  boldText: {
    fontWeight: "bold"
  },
  noMargin: {
    margin: 0
  }
};
class PrivacyNoticeStatementContent extends Component {
  static propTypes = {
    handleAcceptPrivacyNoticeStatementCheckbox: PropTypes.func.isRequired,
    privacyNoticeStatementChecked: PropTypes.bool.isRequired
  };

  render() {
    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    return (
      <div>
        {/* GENERAL */}
        <div>
          <p>
            {LOCALIZE.formatString(
              LOCALIZE.privacyNoticeStatementOLA.para1,
              <a
                href={`${LOCALIZE.privacyNoticeStatementOLA.para1Link1}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                {LOCALIZE.privacyNoticeStatementOLA.para1Link1Label}
              </a>,
              <a
                href={`${LOCALIZE.privacyNoticeStatementOLA.para1Link1}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                {LOCALIZE.privacyNoticeStatementOLA.para1Link1Label}
              </a>,
              <a
                href={`${LOCALIZE.privacyNoticeStatementOLA.para1Link1}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                {LOCALIZE.privacyNoticeStatementOLA.para1Link1Label}
              </a>,
              <a
                href={`${LOCALIZE.privacyNoticeStatementOLA.para1Link2}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                {LOCALIZE.privacyNoticeStatementOLA.para1Link2Label}
              </a>
            )}
          </p>
          <p>
            {LOCALIZE.formatString(
              LOCALIZE.privacyNoticeStatementOLA.para2,
              <a
                href={`${LOCALIZE.privacyNoticeStatementOLA.para2Link}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                {LOCALIZE.privacyNoticeStatementOLA.para2LinkLabel}
              </a>
            )}
          </p>
          <p>
            {LOCALIZE.formatString(
              LOCALIZE.privacyNoticeStatementOLA.para3,
              <a
                href={`${LOCALIZE.privacyNoticeStatementOLA.para3Link}`}
                target="_blank"
                rel="noopener noreferrer"
              >
                {LOCALIZE.privacyNoticeStatementOLA.para3LinkLabel}
              </a>
            )}
          </p>
          {/* RECORDING */}
          <div>
            <p>{LOCALIZE.privacyNoticeStatementOLA.recording.para1}</p>
          </div>
        </div>
        {/* CHECKBOX */}
        <div style={styles.checkboxContainer}>
          <input
            type="checkbox"
            id="reservation-details-privacy-notice-statement-checkbox"
            checked={this.props.privacyNoticeStatementChecked}
            style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
            onClick={this.props.handleAcceptPrivacyNoticeStatementCheckbox}
          />
          <label
            htmlFor="reservation-details-privacy-notice-statement-checkbox"
            style={styles.noMargin}
          >
            {LOCALIZE.privacyNoticeStatementOLA.checkboxLabel}
          </label>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PrivacyNoticeStatementContent);
