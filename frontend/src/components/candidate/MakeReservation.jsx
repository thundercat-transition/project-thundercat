/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import { Row, Col, Button } from "react-bootstrap";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import {
  faSearch,
  faShare,
  faCheck,
  faTimes,
  faUniversalAccess,
  faPaperPlane,
  faSpinner
} from "@fortawesome/free-solid-svg-icons";
import {
  faCalendarCheck,
  faCalendarAlt,
  faQuestionCircle,
  faTimesCircle
} from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import {
  verifyReservationCode,
  reserveAndVerifyReservationCode,
  findSessionsForReservationCode,
  findAllReservedCodesForUser,
  withdrawUnbookedReservationCode,
  bookNowReservationCode
} from "../../modules/CandidateRedux";
import createVirtualMeeting from "../../modules/VirtualMeetingsRedux";
import StyledTooltip, { TYPE, EFFECT } from "../authentication/StyledTooltip";
import ReservationDetailsDialog, {
  getFormattedTestSkill,
  emptyReservation,
  RESERVATION_DETAILS_DIALOG_SOURCE
} from "./ReservationDetailsDialog";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import isWithinTheRetestPeriod from "../../modules/RetestPeriodValidationRedux";
import {
  getTestCentersForUserAccommodationRequest,
  sendUserAccommodationRequest
} from "../../modules/AaeRedux";
import { LANGUAGES } from "../commons/Translation";
import DropdownSelect from "../commons/DropdownSelect";
import getCheckboxTransformScale from "../../helpers/checkboxTransformScale";
import { PATH } from "../commons/Constants";
import { setMyProfileSideNavigationSelectedItem } from "../../modules/UserProfileRedux";
import { convertDateTimeToUsersTimezone } from "../../modules/helpers";
import { getAccommodationRequestTypeOptions } from "./Constants";
import { TestSkillSleDescCodename, TestSkillTypeCodename } from "../testFactory/Constants";
import moment from "moment";
import { history } from "../../store-index";
import formatMaskedPhoneNumber, { MASKS, MASK_FORMAT_CHARS } from "../commons/InputMask/helpers";
import InputMask from "react-input-mask";
import TermsOfUseContent from "./TermsOfUseContent";
import PrivacyNoticeStatementContent from "./PrivacyNoticeStatementContent";
import { getEnableQualityofLife, setQualityOfLife } from "../../modules/SiteAdminSettingsRedux";

const columnSizes = {
  labelTooltipColumn: {
    xs: 6,
    sm: 6,
    md: 4,
    lg: 4,
    xl: 4
  },
  inputColumn: {
    xs: 6,
    sm: 6,
    md: 4,
    lg: 4,
    xl: 4
  },
  buttonColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 12,
    xl: 4
  },
  popupFirstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  popupSecondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  reservationCodeLabel: {
    fontWeight: "bold",
    textAlign: "right"
  },
  reservationCodeAndDisplayContainer: {
    margin: "15px 0 12px 0",
    width: "100%"
  },
  checkbox: {
    verticalAlign: "middle",
    margin: "0 16px"
  },
  useCodeButtonContainer: {
    fontWeight: "bold",
    textAlign: "center",
    paddingright: 0
  },
  reservationCodeContainer: {
    position: "relative"
  },
  textInput: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    minHeight: 32
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  checkInBtn: {
    minWidth: 250
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  firstRowStyle: {
    textAlign: "left"
  },
  viewButton: {
    minWidth: 150
  },
  tooltipIcon: {
    color: "#00565e"
  },
  mainDiv: {
    marginTop: 12,
    textAlign: "center"
  },
  hidden: {
    display: "none"
  },
  tablePrefix: {
    textAlign: "left",
    fontWeight: "bold"
  },
  tableIcon: {
    transform: "scale(1.2)"
  },
  tooltipButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e",
    transform: "scale(1.2)"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  dangerButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#a94442"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  allUnset: {
    all: "unset"
  },
  label: {
    paddingRight: 6
  },
  inputContainer: {
    marginTop: 2
  },
  boldText: {
    fontWeight: "bold"
  },
  fieldRowContainer: {
    margin: "12px auto",
    width: "80%"
  },
  rowContainer: {
    margin: "12px 0"
  },
  checkboxRowContainer: {
    marginTop: 12
  },
  formContainer: {
    width: "100%",
    margin: "18px auto"
  },
  textAreaInput: {
    width: "100%",
    height: 115,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block"
  },
  noMargin: {
    margin: 0
  },
  loadingContainer: {
    textAlign: "center",
    transform: "scale(1.3)"
  },
  profileLink: {
    textDecoration: "underline",
    color: "#0278A4",
    cursor: "pointer",
    backgroundColor: "transparent",
    border: "none",
    padding: 0
  },
  icon: {
    paddingRight: 6,
    verticalAlign: "middle"
  },
  transparentText: {
    color: "transparent"
  },
  customLoadingContainer: {
    display: "grid",
    gridTemplateColumns: "1fr"
  },
  loadingOverlappingStyle: {
    gridRowStart: "1",
    gridColumnStart: "1"
  },
  checkboxSectionContainer: {
    margin: "48px 0 24px 0"
  },
  checkboxLink: {
    textDecoration: "underline",
    color: "#0278A4",
    cursor: "pointer",
    backgroundColor: "transparent",
    border: "none",
    padding: 0
  }
};

const defaultSessionRowsDefinition = {
  column_1_style: styles.firstRowStyle,
  column_2_style: styles.basicRowStyle,
  column_3_style: styles.basicRowStyle,
  column_4_style: styles.basicRowStyle,
  data: []
};

// The default "empty" table for reserved reservation codes for a given candidate
const defaultReservedRowsDefinition = {
  column_1_style: styles.firstRowStyle,
  column_2_style: styles.basicRowStyle,
  column_3_style: styles.basicRowStyle,
  data: []
};

class MakeReservation extends Component {
  state = {
    reservationBarContent: "",
    currentReservationCode: "",
    isOla: false,
    currentTestSkillTypeId: "",
    currentTestSkillTypeCodename: "",
    currentTestSkillTypeNameEn: "",
    currentTestSkillTypeNameFr: "",
    currentTestSkillSubTypeId: "",
    currentTestSkillSubTypeCodename: "",
    currentTestSkillSubTypeNameEn: "",
    currentTestSkillSubTypeNameFr: "",
    currentlyLoading: false,
    showNoSessionsAvailablePopup: false,
    isInvalidReservationCode: false,
    isReservationCodeAlreadyUsed: false,
    isSearchingForSeat: false,
    sessionRowsDefinition: {},
    reservedRowsDefinition: {},
    showReserveSessionPopup: false,
    showBookingSuccessfulPopup: false,
    showWithdrawSuccessfulPopup: false,
    showConfirmWithdrawPopup: false,
    showErrorNotEnoughSeatsPopup: false,
    showErrorNotEnoughTimePopup: false,
    earliestBookingTime: "",
    reservationClosingDate: "",
    reservation: emptyReservation,
    showRetestPeriodWarningPopup: false,
    popupCheckboxChecked: false,
    selectedReservationCode: "",
    selectedFormattedTestSkill: "",
    showRequestAccommodationsPopup: false,
    requestTypeOptions: [],
    requestTypeSelectedOption: [],
    showNeedAccommodationsProfilePopup: false,
    testCenterOptionsLoading: false,
    testCenterOptions: [],
    testCenterSelectedOption: [],
    commentsOrTestAdministeredDetails: "",
    profileUpToDateCheckboxChecked: false,
    isValidForm: false,
    showAccommodationRequestSentSuccessfullyPopup: false,
    actionInProgress: false,
    phoneNumberContent: "",
    isOlaSelectedReservationCode: false,
    showTermsOfUsePopup: false,
    mainTermsOfUseChecked: false,
    termsOfUseChecked: false,
    showPrivacyNoticeStatementPopup: false,
    mainPrivacyNoticeStatementChecked: false,
    privacyNoticeStatementChecked: false
  };

  static propTypes = {
    rebuildMake: PropTypes.bool,
    setRebuildMakeTables: PropTypes.func,
    setRebuildManageTables: PropTypes.func
  };

  componentDidUpdate = prevProps => {
    // if the other table had a change; rebuild this one
    if (prevProps.rebuildMake !== this.props.rebuildMake && this.props.rebuildMake) {
      this.loadReservedCodes();
    }
  };

  componentDidMount = () => {
    // this is a little forced; but without it Generic Table doesn't "Refresh" or load any data
    this._isMounted = true;
    this.loadReservedCodes();
    // getting/setting qualityOfLife redux state
    this.props
      .getEnableQualityofLife()
      .then(response => this.props.setQualityOfLife(response.is_active));
  };

  populateReservationCodeSearchTable = (
    reservationCode,
    testSkillTypeId,
    testSkillTypeCodename,
    testSkillTypeNameEn,
    testSkillTypeNameFr,
    testSkillSubTypeId,
    testSkillSubTypeCodename,
    testSkillSubTypeNameEn,
    testSkillSubTypeNameFr
  ) => {
    const reservationClosingDate = "";
    let sessionRowsDefinition = {};
    const data = [];
    // if not empty, set currently loading
    this.setState({
      currentlyLoading: true
    });
    // check if it is valid/if the closing date has passed
    this.props
      .findSessionsForReservationCode(
        reservationCode,
        testSkillTypeCodename,
        testSkillSubTypeCodename
      )
      .then(response => {
        // if the code is not a valid code
        if (response.status === 404) {
          this.setState({
            ...this.state,
            showNoSessionsAvailablePopup: false,
            isInvalidReservationCode: true,
            isReservationCodeAlreadyUsed: false,
            isSearchingForSeat: false,
            currentlyLoading: false,
            showErrorNotEnoughSeatsPopup: false,
            showErrorNotEnoughTimePopup: false,
            earliestBookingTime: "",
            reservation: emptyReservation
          });
          return;
        }

        // looping in results
        for (let i = 0; i < response.data.length; i++) {
          // format the data for the table
          const currentResult = response.data[i];

          // inintializing needed variables
          let formattedTestSkill = null;
          let formattedDate = null;
          let formattedTime = null;

          // OLA test
          if (response.is_ola) {
            formattedTestSkill =
              this.props.currentLanguage === LANGUAGES.english
                ? `${testSkillTypeNameEn} - ${testSkillSubTypeNameEn}`
                : `${testSkillTypeNameFr} - ${testSkillSubTypeNameFr}`;

            // formatting start/end time as UTC datetime
            const formattedUtcStartTime = moment
              .utc(`${currentResult.date}T${currentResult.start_time}+00:00`)
              .format("YYYY-MM-DDTHH:mm:ssZ");
            const formattedUtcEndTime = moment
              .utc(`${currentResult.date}T${currentResult.end_time}+00:00`)
              .format("YYYY-MM-DDTHH:mm:ssZ");

            // formatting/converting start/end time datetimes
            const convertedStartTime = convertDateTimeToUsersTimezone(formattedUtcStartTime);
            const convertedEndTime = convertDateTimeToUsersTimezone(formattedUtcEndTime);

            currentResult.utc_start_time = formattedUtcStartTime;
            currentResult.utc_end_time = formattedUtcEndTime;

            // formatting time
            formattedTime = `${convertedStartTime.adjustedTimeBaseOnCurrentUtcOffset} - ${convertedEndTime.adjustedTimeBaseOnCurrentUtcOffset}`;
            // Other Test
          } else {
            formattedTestSkill = getFormattedTestSkill(this.props.currentLanguage, currentResult);

            // formatting/converting start/end time datetimes
            const convertedStartTime = convertDateTimeToUsersTimezone(currentResult.utc_start_time);
            const convertedEndTime = convertDateTimeToUsersTimezone(currentResult.utc_end_time);

            formattedDate = `${currentResult.date} ${convertedStartTime.adjustedTime} - ${convertedEndTime.adjustedTime}`;
          }

          // store the code for later
          currentResult.reservation_code = reservationCode;

          // add the formatted data to the table
          data.push({
            column_1: formattedTestSkill,
            column_2: response.is_ola ? currentResult.date : formattedDate,
            column_3: response.is_ola ? formattedTime : currentResult.test_center_name,
            column_4: (
              <CustomButton
                buttonId={`unit-test-button-${i}`}
                label={
                  <>
                    <FontAwesomeIcon icon={faCalendarAlt} />
                    <span style={styles.buttonLabel}>
                      {LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.table.infoButton}
                    </span>
                  </>
                }
                action={() => this.viewSessionDialog(currentResult)}
                customStyle={styles.viewButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.table
                    .infoButtonAccessibilityLabel,
                  currentResult.test_center_name
                )}
              />
            )
          });
          // updating sessionRowsDefinition  object with provided data and needed style
          sessionRowsDefinition = {
            column_1_style: styles.firstRowStyle,
            column_2_style: styles.basicRowStyle,
            column_3_style: styles.basicRowStyle,
            column_4_style: styles.basicRowStyle,
            data: data
          };
        }
        this._isMounted = true;

        // saving results in state
        this.setState({
          ...this.state,
          sessionRowsDefinition: sessionRowsDefinition,
          currentReservationCode: reservationCode,
          isOla: response.is_ola,
          currentTestSkillTypeId: testSkillTypeId,
          currentTestSkillTypeCodename: testSkillTypeCodename,
          currentTestSkillTypeNameEn: testSkillTypeNameEn,
          currentTestSkillTypeNameFr: testSkillTypeNameFr,
          currentTestSkillSubTypeId: testSkillSubTypeId,
          currentTestSkillSubTypeCodename: testSkillSubTypeCodename,
          currentTestSkillSubTypeNameEn: testSkillSubTypeNameEn,
          currentTestSkillSubTypeNameFr: testSkillSubTypeNameFr,
          showNoSessionsAvailablePopup: false,
          isInvalidReservationCode: false,
          isReservationCodeAlreadyUsed: false,
          isSearchingForSeat: true,
          currentlyLoading: false,
          showErrorNotEnoughSeatsPopup: false,
          showErrorNotEnoughTimePopup: false,
          earliestBookingTime: "",
          reservation: emptyReservation
        });
      });
  };

  populateTestCenterOptions = () => {
    // initializing testCenterOptions
    const testCenterOptions = [];

    this.setState({ testCenterOptionsLoading: true }, () => {
      // getting test centers
      this.props.getTestCentersForUserAccommodationRequest().then(response => {
        // looping in test centers
        for (let i = 0; i < response.body.length; i++) {
          // populating testCenterOptions
          testCenterOptions.push({
            label: `${
              response.body[i].test_center_city_text[this.props.currentLanguage][0].text
            } (${response.body[i].test_center_address_text[this.props.currentLanguage][0].text}, ${
              response.body[i].test_center_city_text[this.props.currentLanguage][0].text
            }, ${response.body[i].test_center_province_text[this.props.currentLanguage][0].text},
            ${response.body[i].postal_code},
            ${
              this.props.currentLanguage === LANGUAGES.english
                ? response.body[i].country_eabrv
                : response.body[i].country_fabrv
            })`,
            value: response.body[i].id
          });
        }
        // updating state
        this.setState({
          testCenterOptions: testCenterOptions,
          testCenterOptionsLoading: false
        });
      });
    });
  };

  handleUserProfileRedirect = () => {
    // setting My Profile side navigation item redux states
    this.props.setMyProfileSideNavigationSelectedItem("profile-accommodations");
    // redirecting user to profile page
    setTimeout(() => {
      history.push(PATH.profile);
    }, 100);
  };

  openRequestAccommodationsPopup = (
    reservationCode,
    candidateHasAccommodationsProfile,
    formattedTestSkill,
    testSkillSubTypeCodename
  ) => {
    // getting available accommodation request type options
    const requestTypeOptions = getAccommodationRequestTypeOptions();
    // setting phoneNumberContent
    let phoneNumberContent = "";
    // OLA TEST + qualityOfLife enabled
    if (
      (testSkillSubTypeCodename === TestSkillSleDescCodename.ORAL_EN ||
        testSkillSubTypeCodename === TestSkillSleDescCodename.ORAL_FR) &&
      this.props.qualityOfLife
    ) {
      // simply putting some valid data
      phoneNumberContent = formatMaskedPhoneNumber("1111111111");
    }
    this.setState(
      {
        showRequestAccommodationsPopup: true,
        showNeedAccommodationsProfilePopup: !candidateHasAccommodationsProfile,
        selectedReservationCode: reservationCode,
        selectedFormattedTestSkill: formattedTestSkill,
        requestTypeOptions: requestTypeOptions,
        isOlaSelectedReservationCode:
          // true if ORAL sub type codename
          testSkillSubTypeCodename === TestSkillSleDescCodename.ORAL_EN ||
          testSkillSubTypeCodename === TestSkillSleDescCodename.ORAL_FR,
        phoneNumberContent: phoneNumberContent
      },
      () => {
        // populating test center options
        this.populateTestCenterOptions();
      }
    );
  };

  closeRequestAccommodationsPopup = () => {
    this.setState({
      showRequestAccommodationsPopup: false,
      showNeedAccommodationsProfilePopup: false,
      selectedReservationCode: "",
      selectedFormattedTestSkill: "",
      testCenterSelectedOption: [],
      commentsOrTestAdministeredDetails: "",
      profileUpToDateCheckboxChecked: false,
      showAccommodationRequestSentSuccessfullyPopup: false,
      isValidForm: false,
      requestTypeSelectedOption: [],
      phoneNumberContent: "",
      isOlaSelectedReservationCode: false,
      mainTermsOfUseChecked: false,
      termsOfUseChecked: false,
      mainPrivacyNoticeStatementChecked: false,
      privacyNoticeStatementChecked: false
    });
  };

  getSelectedRequestTypeOption = selectedOption => {
    this.setState({ requestTypeSelectedOption: selectedOption }, () => {
      // setting commentsOrTestAdministeredDetails
      let commentsOrTestAdministeredDetails = "";
      // Alternate/Both Request + qualityOfLife enabled
      if (
        (selectedOption.label ===
          LOCALIZE.dashboard.requestAccommodationsPopup.requestTypeOptions.alternateTest ||
          selectedOption.label ===
            LOCALIZE.dashboard.requestAccommodationsPopup.requestTypeOptions.both) &&
        this.props.qualityOfLife
      ) {
        // simply putting some valid data
        commentsOrTestAdministeredDetails = "Some reason here...";
      }
      this.setState(
        { commentsOrTestAdministeredDetails: commentsOrTestAdministeredDetails },
        () => {
          // validating form
          this.validateForm();
        }
      );
    });
  };

  getSelectedTestCenterLocation = selectedOption => {
    this.setState({ testCenterSelectedOption: selectedOption }, () => {
      // validating form
      this.validateForm();
    });
  };

  updatePhoneNumberContent = event => {
    const phoneNumberContent = formatMaskedPhoneNumber(event.target.value);
    this.setState(
      {
        phoneNumberContent: phoneNumberContent
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  getCommentsOrTestAdministeredDetailsContent = event => {
    const commentsOrTestAdministeredDetails = event.target.value;
    this.setState(
      {
        commentsOrTestAdministeredDetails: commentsOrTestAdministeredDetails
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  getCheckboxState = event => {
    this.setState(
      {
        profileUpToDateCheckboxChecked: event.target.checked
      },
      () => {
        this.validateForm();
      }
    );
  };

  openTermsOfUsePopup = event => {
    this.setState({
      showTermsOfUsePopup: true,
      termsOfUseChecked: this.state.mainTermsOfUseChecked
    });
    event.preventDefault();
  };

  handleAcceptTermsOfUseCheckbox = event => {
    this.setState({ termsOfUseChecked: event.target.checked });
  };

  handleAcceptTermsOfUse = () => {
    this.setState({ mainTermsOfUseChecked: true, showTermsOfUsePopup: false }, () => {
      // validating form
      this.validateForm();
    });
  };

  closeTermsOfUsePopup = () => {
    this.setState(
      {
        showTermsOfUsePopup: false,
        termsOfUseChecked: false,
        mainTermsOfUseChecked: false
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  openPrivacyNoticeStatementPopup = event => {
    this.setState({
      showPrivacyNoticeStatementPopup: true,
      privacyNoticeStatementChecked: this.state.mainPrivacyNoticeStatementChecked
    });
    event.preventDefault();
  };

  handleAcceptPrivacyNoticeStatementCheckbox = event => {
    this.setState({ privacyNoticeStatementChecked: event.target.checked });
  };

  handleAcceptPrivacyNoticeStatement = () => {
    this.setState(
      { mainPrivacyNoticeStatementChecked: true, showPrivacyNoticeStatementPopup: false },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  closePrivacyNoticeStatementPopup = () => {
    this.setState(
      {
        showPrivacyNoticeStatementPopup: false,
        privacyNoticeStatementChecked: false,
        mainPrivacyNoticeStatementChecked: false
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  validateForm = () => {
    // initializing needed variables
    let isValidForm = false;
    let isValidSelectedTestCenter = true;
    let isValidPhoneNumber = true;
    let isValidTextBoxField = true;
    let isValidTermsOfUse = true;
    let isValidPrivacyNoticeStatement = true;

    // OLA Reservation Code
    if (this.state.isOlaSelectedReservationCode) {
      isValidPhoneNumber = formatMaskedPhoneNumber(this.state.phoneNumberContent).length === 10;
      // not an OLA Reservation Code
    } else {
      isValidSelectedTestCenter = Object.keys(this.state.testCenterSelectedOption).length > 0;
    }

    // alternate test or both request
    if (
      this.state.requestTypeSelectedOption.label ===
        LOCALIZE.dashboard.requestAccommodationsPopup.requestTypeOptions.alternateTest ||
      this.state.requestTypeSelectedOption.label ===
        LOCALIZE.dashboard.requestAccommodationsPopup.requestTypeOptions.both
    ) {
      isValidTextBoxField = this.state.commentsOrTestAdministeredDetails !== "";
    }

    // OLA Test
    if (this.state.isOlaSelectedReservationCode) {
      isValidTermsOfUse = this.state.mainTermsOfUseChecked;
      isValidPrivacyNoticeStatement = this.state.mainPrivacyNoticeStatementChecked;
    }

    // all required fields have been filled properly
    if (
      isValidSelectedTestCenter &&
      this.state.profileUpToDateCheckboxChecked &&
      isValidPhoneNumber &&
      isValidTextBoxField &&
      isValidTermsOfUse &&
      isValidPrivacyNoticeStatement
    ) {
      // setting isValidForm to true
      isValidForm = true;
    }

    // updating state
    this.setState({ isValidForm: isValidForm });
  };

  handleSubmitAccommodationsRequest = () => {
    this.setState({ actionInProgress: true }, () => {
      // creating body
      const body = {
        comments: this.state.commentsOrTestAdministeredDetails,
        is_uit: false,
        test_center_id:
          typeof this.state.testCenterSelectedOption.value !== "undefined"
            ? this.state.testCenterSelectedOption.value
            : null,
        reservation_code: this.state.selectedReservationCode,
        is_alternate_test_request: this.state.requestTypeSelectedOption.is_alternate_test_request,
        phone_number: this.state.phoneNumberContent !== "" ? this.state.phoneNumberContent : null
      };
      // creating new accommodation request
      this.props.sendUserAccommodationRequest(body).then(response => {
        // success
        if (response.ok) {
          // refreshing table
          this.loadReservedCodes();
          // show success popup
          this.setState(
            {
              showAccommodationRequestSentSuccessfullyPopup: true
            },
            () => {
              this.setState({ actionInProgress: false });
            }
          );
          // should never happen
        } else {
          throw new Error("An error occurred during the creation of the accommodation request");
        }
      });
    });
  };

  // this is the force refresh of the reserved codes
  loadReservedCodes = () => {
    let reservedRowsDefinition = {};
    const data = [];
    this.props.findAllReservedCodesForUser().then(response => {
      // if the code is not a valid code
      if (response.status === 400) {
        this.setState({
          ...this.state,
          currentlyLoading: false,
          reservedRowsDefinition: defaultReservedRowsDefinition,
          isSearchingForSeat: false
        });
        return;
      }

      // itterate over the response
      for (let i = 0; i < response.length; i++) {
        // format the data for the table
        const currentResult = response[i];
        // store the code for later
        const formattedTestSkill = getFormattedTestSkill(this.props.currentLanguage, currentResult);
        const {
          reservation_code,
          test_skill_type_id,
          test_skill_type_codename,
          test_skill_type_en_name,
          test_skill_type_fr_name,
          test_skill_sub_type_id,
          test_skill_sub_type_codename,
          test_skill_sub_type_en_name,
          test_skill_sub_type_fr_name,
          candidate_has_accommodations_profile
        } = currentResult;

        // add the formatted data to the table
        data.push({
          test_skill_type_codename: currentResult.test_skill_type_codename,
          test_skill_sub_type_codename: currentResult.test_skill_sub_type_codename,
          column_1: formattedTestSkill,
          column_2: reservation_code,
          column_3: (
            <>
              <StyledTooltip
                id={`find-reservation-button-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`find-reservation-button-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faSearch} style={styles.tableIcon} />
                        </>
                      }
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      action={() =>
                        this.populateReservationCodeSearchTable(
                          reservation_code,
                          test_skill_type_id,
                          test_skill_type_codename,
                          test_skill_type_en_name,
                          test_skill_type_fr_name,
                          test_skill_sub_type_id,
                          test_skill_sub_type_codename,
                          test_skill_sub_type_en_name,
                          test_skill_sub_type_fr_name
                        )
                      }
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.reservedTable
                          .findSessionsAccessibilityLabel,
                        reservation_code
                      )}
                      style={styles.tooltipButton}
                      variant="link"
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.reservedTable
                          .findSessions
                      }
                    </p>
                  </div>
                }
              />
              <StyledTooltip
                id={`request-accommodations-button-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`request-accommodations-button-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faUniversalAccess} style={styles.tableIcon} />
                        </>
                      }
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      action={() =>
                        this.openRequestAccommodationsPopup(
                          reservation_code,
                          candidate_has_accommodations_profile,
                          formattedTestSkill,
                          currentResult.test_skill_sub_type_codename
                        )
                      }
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.reservedTable
                          .requestAccommodationsAccessibilityLabel,
                        formattedTestSkill
                      )}
                      style={styles.tooltipButton}
                      variant="link"
                      disabled={
                        currentResult.user_accommodation_file_id === null
                          ? BUTTON_STATE.enabled
                          : BUTTON_STATE.disabled
                      }
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.reservedTable
                          .requestAccommodationsButton
                      }
                    </p>
                  </div>
                }
              />
              {/* <StyledTooltip
                id={`search-for-results-button-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`search-for-results-button-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faShare} style={styles.tableIcon} />
                        </>
                      }
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      action={() => {}}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.reservedTable
                          .searchResultsAccessibilityLabel,
                        reservation_code
                      )}
                      style={styles.tooltipButton}
                      variant="link"
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.reservedTable
                          .searchResultsButton
                      }
                    </p>
                  </div>
                }
              /> */}
              <StyledTooltip
                id={`withdraw-button-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`withdraw-button-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faTimesCircle} style={styles.tableIcon} />
                        </>
                      }
                      customStyle={styles.dangerButton}
                      buttonTheme={THEME.PRIMARY}
                      action={() => this.triggerWithdrawPopup(reservation_code)}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.reservedTable
                          .withdrawAccessibilityLabel,
                        reservation_code
                      )}
                      style={styles.tooltipButton}
                      variant="link"
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.reservedTable
                          .withdrawButton
                      }
                    </p>
                  </div>
                }
              />
            </>
          )
        });
        // updating reservedRowsDefinition object with provided data and needed style
        reservedRowsDefinition = {
          column_1_style: styles.firstRowStyle,
          column_2_style: styles.basicRowStyle,
          column_3_style: styles.basicRowStyle,
          data: data
        };
      }
      this._isMounted = true;

      // saving results in state
      this.setState({
        ...this.state,
        currentlyLoading: false,
        isSearchingForSeat: false,
        reservedRowsDefinition: reservedRowsDefinition
      });

      // If this was triggered by a change on the managed tables, then do not rebuild the managed tables
      if (this.props.rebuildMake) {
        this.props.setRebuildMakeTables(false);
        // otherwise trigger the logic to rebuild the managed tables
      } else {
        this.props.setRebuildManageTables(true);
      }
    });
  };

  // handle the cancel button on the Use Reservation Code Popup
  closeReservationCodeDialog = () => {
    this.closeDialogAndReset();
  };

  closeBookingSuccessfulDialog = () => {
    this.closeDialogAndReset();
  };

  closeWithdrawSuccessfulDialog = () => {
    this.closeDialogAndReset();
  };

  closeNoSessionsAvailableDialog = () => {
    this.closeDialogAndReset();
  };

  closeDialogAndReset = () => {
    this.setState({
      ...this.state,
      showNoSessionsAvailablePopup: false,
      isInvalidReservationCode: false,
      isReservationCodeAlreadyUsed: false,
      isSearchingForSeat: false,
      sessionRowsDefinition: defaultSessionRowsDefinition,
      showReserveSessionPopup: false,
      showBookingSuccessfulPopup: false,
      showWithdrawSuccessfulPopup: false,
      reservationClosingDate: "",
      reservation: emptyReservation
    });
  };

  // update searchBarContent content
  updateReservationBarContent = event => {
    const reservationBarContent = event.target.value;
    this.setState({
      ...this.state,
      reservationBarContent: reservationBarContent,
      reservation: emptyReservation
    });
  };

  triggerWithdrawPopup = reservationCode => {
    this.setState({
      ...this.state,
      currentReservationCode: reservationCode,
      isInvalidReservationCode: false,
      isReservationCodeAlreadyUsed: false,
      isSearchingForSeat: false,
      showReserveSessionPopup: false,
      showBookingSuccessfulPopup: false,
      showConfirmWithdrawPopup: true,
      reservation: emptyReservation
    });
  };

  confirmWithdraw = () => {
    const body = {
      reservation_code: this.state.currentReservationCode,
      test_session_id: null,
      is_ola: this.state.isOla
    };
    this.props.withdrawUnbookedReservationCode(body).then(response => {
      if (response.status === 200) {
        this.setState({
          ...this.state,
          reservationBarContent: "",
          currentReservationCode: "",
          isOla: false,
          currentTestSkillTypeId: "",
          currentTestSkillTypeCodename: "",
          currentTestSkillTypeNameEn: "",
          currentTestSkillTypeNameFr: "",
          currentTestSkillSubTypeId: "",
          currentTestSkillSubTypeCodename: "",
          currentTestSkillSubTypeNameEn: "",
          currentTestSkillSubTypeNameFr: "",
          showConfirmWithdrawPopup: false,
          showWithdrawSuccessfulPopup: true
        });
        this.loadReservedCodes();
        // should never happen
      } else {
        throw new Error("An error occurred during the withdrawal from an assessment process");
      }
    });
  };

  cancelConfirmWithdraw = () => {
    this.setState({
      ...this.state,
      showConfirmWithdrawPopup: false
    });
  };

  closeErrorAndReSearch = () => {
    // re-populating table
    this.populateReservationCodeSearchTable(
      this.state.currentReservationCode,
      this.state.currentTestSkillTypeId,
      this.state.currentTestSkillTypeCodename,
      this.state.currentTestSkillTypeNameEn,
      this.state.currentTestSkillTypeNameFr,
      this.state.currentTestSkillSubTypeId,
      this.state.currentTestSkillSubTypeCodename,
      this.state.currentTestSkillSubTypeNameEn,
      this.state.currentTestSkillSubTypeNameFr
    );
    // closing popup
    this.setState({ showErrorNotEnoughSeatsPopup: false });
  };

  // triggered when a session is selected from the table
  viewSessionDialog = sessionInfo => {
    // OLA Test
    if (this.state.isOla) {
      // eslint-disable-next-line no-param-reassign, prefer-destructuring
      sessionInfo.id = null;
      // eslint-disable-next-line no-param-reassign, prefer-destructuring
      sessionInfo.test_skill_type_id = this.state.currentTestSkillTypeId;
      // eslint-disable-next-line no-param-reassign, prefer-destructuring
      sessionInfo.test_skill_type_en_name = this.state.currentTestSkillTypeNameEn;
      // eslint-disable-next-line no-param-reassign, prefer-destructuring
      sessionInfo.test_skill_type_fr_name = this.state.currentTestSkillTypeNameFr;
      // eslint-disable-next-line no-param-reassign, prefer-destructuring
      sessionInfo.test_skill_sub_type_id = this.state.currentTestSkillSubTypeId;
      // eslint-disable-next-line no-param-reassign, prefer-destructuring
      sessionInfo.test_skill_sub_type_en_name = this.state.currentTestSkillSubTypeNameEn;
      // eslint-disable-next-line no-param-reassign, prefer-destructuring
      sessionInfo.test_skill_sub_type_fr_name = this.state.currentTestSkillSubTypeNameFr;
      // Convert time into users timezone
      // eslint-disable-next-line no-param-reassign, prefer-destructuring
      sessionInfo.simplified_start_time = convertDateTimeToUsersTimezone(
        sessionInfo.utc_start_time
      ).adjustedTime;
      // eslint-disable-next-line no-param-reassign, prefer-destructuring
      sessionInfo.simplified_end_time = convertDateTimeToUsersTimezone(
        sessionInfo.utc_end_time
      ).adjustedTime;
      // Other Test
    } else {
      // Convert time into users timezone
      // eslint-disable-next-line no-param-reassign, prefer-destructuring
      sessionInfo.simplified_start_time = convertDateTimeToUsersTimezone(
        sessionInfo.utc_start_time
      ).adjustedTime;
      // eslint-disable-next-line no-param-reassign, prefer-destructuring
      sessionInfo.simplified_end_time = convertDateTimeToUsersTimezone(
        sessionInfo.utc_end_time
      ).adjustedTime;
    }

    // nicely format the data to populate the popup
    this.setState({
      ...this.state,
      showNoSessionsAvailablePopup: false,
      isInvalidReservationCode: false,
      isReservationCodeAlreadyUsed: false,
      isSearchingForSeat: true,
      showReserveSessionPopup: true,
      showBookingSuccessfulPopup: false,
      showWithdrawSuccessfulPopup: false,
      reservation: [sessionInfo]
    });
  };

  // tiggered by hitting "cancel" on the Session Information popup
  closeSessionDialog = () => {
    this.setState(
      {
        showReserveSessionPopup: false,
        showBookingSuccessfulPopup: false,
        showWithdrawSuccessfulPopup: false
      },
      () => {
        // adding small delay to avoid interface glitch
        setTimeout(() => {
          this.setState({
            ...this.state,
            showNoSessionsAvailablePopup: false,
            isInvalidReservationCode: false,
            isReservationCodeAlreadyUsed: false,
            isSearchingForSeat: true,
            showRetestPeriodWarningPopup: false
          });
        }, 250);
      }
    );
  };

  bookSession = (bypassRetestValidation = false) => {
    // bypassing the retest validation
    if (bypassRetestValidation) {
      // calling the book reservation logic
      this.handleBookReservationLogic();
      // validate the restest period
    } else {
      this.props
        .isWithinTheRetestPeriod(
          null,
          this.state.reservation[0].test_skill_type_id,
          this.state.reservation[0].test_skill_sub_type_id,
          this.state.reservation[0].start_time
        )
        .then(isWithinRetestPeriod => {
          // if is within retest period + bypassRetestValidation is set to false
          if (isWithinRetestPeriod) {
            this.setState({
              showRetestPeriodWarningPopup: true
            });
            // respects the retest period
          } else {
            // calling the book reservation logic
            this.handleBookReservationLogic();
          }
        });
    }
  };

  handleBookReservationLogic = () => {
    const body = {
      reservation_code: this.state.reservation[0].reservation_code,
      test_session_id: this.state.reservation[0].id,
      is_ola: this.state.isOla,
      date: this.state.reservation[0].date,
      utc_start_time: this.state.reservation[0].utc_start_time,
      utc_end_time: this.state.reservation[0].utc_end_time,
      start_time: this.state.reservation[0].simplified_start_time,
      end_time: this.state.reservation[0].simplified_end_time,
      test_skill_type_id: this.state.currentTestSkillTypeId,
      test_skill_sub_type_id: this.state.currentTestSkillSubTypeId,
      candidate_phone_number: this.props.reserveTestSessionPhoneNumberState
    };
    this.props.bookNowReservationCode(body).then(response => {
      // handle the various statuseseses
      switch (response.status) {
        case 406:
          // if the session violates the delay period:
          const { earliest_booking } = response;
          this.setState({
            ...this.state,
            showReserveSessionPopup: false,
            showBookingSuccessfulPopup: false,
            showWithdrawSuccessfulPopup: false,
            showErrorNotEnoughTimePopup: true,
            earliestBookingTime: earliest_booking
          });
          break;
        case 409:
          // if there are not enough seats
          // Clear things out and alert the user that there are not enough seats
          this.setState({
            ...this.state,
            showReserveSessionPopup: false,
            showBookingSuccessfulPopup: false,
            showWithdrawSuccessfulPopup: false,
            showErrorNotEnoughSeatsPopup: true,
            showErrorNotEnoughTimePopup: false,
            earliestBookingTime: ""
          });
          break;
        case 200:
          const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
          const meetingBody = {
            date: this.state.reservation[0].date,
            start_time: this.state.reservation[0].simplified_start_time,
            end_time: this.state.reservation[0].simplified_end_time,
            timezone: timezone,
            test_session_id: response.test_session_id
          };
          // if the session was booked successfully
          if (this.state.isOla) {
            this.props.createVirtualMeeting(meetingBody).then(response => {
              switch (response.status) {
                case 200:
                  break;
                default:
                  // this should not occur
                  throw new Error(
                    "Something went wrong during the virtual meeting booking process"
                  );
              }
            });
          }
          this.setState({
            ...this.state,
            reservationBarContent: "",
            currentReservationCode: "",
            isOla: false,
            currentTestSkillTypeId: "",
            currentTestSkillTypeCodename: "",
            currentTestSkillTypeNameEn: "",
            currentTestSkillTypeNameFr: "",
            currentTestSkillSubTypeId: "",
            currentTestSkillSubTypeCodename: "",
            currentTestSkillSubTypeNameEn: "",
            currentTestSkillSubTypeNameFr: "",
            currentlyLoading: false,
            showNoSessionsAvailablePopup: false,
            isInvalidReservationCode: false,
            isReservationCodeAlreadyUsed: false,
            isSearchingForSeat: false,
            sessionRowsDefinition: defaultSessionRowsDefinition,
            showReserveSessionPopup: false,
            showBookingSuccessfulPopup: true,
            showWithdrawSuccessfulPopup: false,
            reservationClosingDate: "",
            reservation: emptyReservation,
            showRetestPeriodWarningPopup: false
          });
          // reload the reserved codes table
          this.loadReservedCodes();
          break;
        default:
          // this should not occur
          throw new Error("Something went wrong during the book reservation code now process");
      }
    });
  };

  // reload the reserved codes table
  showReservedCodes = () => {
    this.setState({
      ...this.state,
      currentlyLoading: true
    });
    this.loadReservedCodes();
  };

  reserveReservationCode = () => {
    const reservationCode = this.state.reservationBarContent;
    this.props.reserveAndVerifyReservationCode(reservationCode).then(response => {
      // if the code is not a valid code or
      if (response.status === 404 || response.status === 400) {
        this.setState({
          ...this.state,
          currentReservationCode: "",
          isOla: false,
          currentTestSkillTypeId: "",
          currentTestSkillTypeCodename: "",
          currentTestSkillTypeNameEn: "",
          currentTestSkillTypeNameFr: "",
          currentTestSkillSubTypeId: "",
          currentTestSkillSubTypeCodename: "",
          currentTestSkillSubTypeNameEn: "",
          currentTestSkillSubTypeNameFr: "",
          showNoSessionsAvailablePopup: false,
          isInvalidReservationCode: true,
          isReservationCodeAlreadyUsed: false,
          isSearchingForSeat: false,
          showErrorNotEnoughSeatsPopup: false,
          showErrorNotEnoughTimePopup: false,
          earliestBookingTime: ""
        });
        return;
      }

      // if the code has been used already
      if (response.status === 409) {
        this.setState({
          ...this.state,
          currentReservationCode: "",
          isOla: false,
          currentTestSkillTypeId: "",
          currentTestSkillTypeCodename: "",
          currentTestSkillTypeNameEn: "",
          currentTestSkillTypeNameFr: "",
          currentTestSkillSubTypeId: "",
          currentTestSkillSubTypeCodename: "",
          currentTestSkillSubTypeNameEn: "",
          currentTestSkillSubTypeNameFr: "",
          showNoSessionsAvailablePopup: false,
          isInvalidReservationCode: false,
          isReservationCodeAlreadyUsed: true,
          isSearchingForSeat: false,
          showErrorNotEnoughSeatsPopup: false,
          showErrorNotEnoughTimePopup: false,
          earliestBookingTime: ""
        });
        return;
      }

      // if it worked, reserve the code for the user
      if (response.status === 200) {
        this.setState({
          ...this.state,
          currentReservationCode: "",
          isOla: false,
          currentTestSkillTypeId: "",
          currentTestSkillTypeCodename: "",
          currentTestSkillTypeNameEn: "",
          currentTestSkillTypeNameFr: "",
          currentTestSkillSubTypeId: "",
          currentTestSkillSubTypeCodename: "",
          currentTestSkillSubTypeNameEn: "",
          currentTestSkillSubTypeNameFr: "",
          showNoSessionsAvailablePopup: false,
          isInvalidReservationCode: false,
          isReservationCodeAlreadyUsed: false,
          reservation: emptyReservation,
          showErrorNotEnoughSeatsPopup: false,
          showErrorNotEnoughTimePopup: false,
          earliestBookingTime: "",
          reservationBarContent: ""
        });
        // reload the table with the new code
        this.loadReservedCodes();
        // this should never occur
      } else {
        throw new Error("Error durring reservation of reservation code");
      }
    });
  };

  render() {
    // get the needed values from props and state
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };

    const {
      reservationClosingDate,
      isSearchingForSeat,
      reservation,
      showNoSessionsAvailablePopup,
      sessionRowsDefinition,
      currentlyLoading,
      reservedRowsDefinition,
      showRetestPeriodWarningPopup
    } = this.state;

    const dataCount = sessionRowsDefinition.data ? sessionRowsDefinition.data.length : 0;

    const lineSpacingStyle = getLineSpacingCSS();

    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    const sessionColumnsDefinition = [
      {
        label: LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.table.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.table.columnTwo,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          // if OLA test
          this.state.currentTestSkillTypeCodename === TestSkillTypeCodename.SLE &&
          (this.state.currentTestSkillSubTypeCodename === TestSkillSleDescCodename.ORAL_EN ||
            this.state.currentTestSkillSubTypeCodename === TestSkillSleDescCodename.ORAL_FR)
            ? LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.table.columnThreeOla
            : LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.table.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.table.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    const reservedColumnsDefinition = [
      {
        label: LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.reservedTable.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.reservedTable.columnTwo,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.reservedTable.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    const checkboxTransformScale = getCheckboxTransformScale(this.props.accommodations.fontSize);

    return (
      <div>
        <Row
          className={
            !this.state.isInvalidReservationCode && !this.state.isReservationCodeAlreadyUsed
              ? "align-items-center align-items-start justify-content-center"
              : "align-items-start justify-content-center"
          }
          style={styles.reservationCodeAndDisplayContainer}
        >
          <Col
            xl={columnSizes.labelTooltipColumn.xl}
            lg={columnSizes.labelTooltipColumn.lg}
            md={columnSizes.labelTooltipColumn.md}
            sm={columnSizes.labelTooltipColumn.sm}
            xs={columnSizes.labelTooltipColumn.xs}
            style={styles.reservationCodeLabel}
          >
            <Row className="align-items-center justify-content-center">
              <label
                htmlFor={`reservation-code-field`}
                id={`reservation-code-field-title`}
                style={styles.label}
              >
                {LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.reservationCode}
              </label>
              <StyledTooltip
                id="your-response-tooltip"
                place="left"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <Button
                    data-tip=""
                    data-for="your-response-tooltip"
                    tabIndex="-1"
                    aria-label={LOCALIZE.ariaLabel.emailResponseTooltip}
                    style={styles.tooltipButton}
                    variant="link"
                  >
                    <FontAwesomeIcon
                      icon={faQuestionCircle}
                      style={styles.tooltipIcon}
                    ></FontAwesomeIcon>
                  </Button>
                }
                tooltipContent={
                  <div>
                    {
                      LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation
                        .reservationCodeHelp
                    }
                  </div>
                }
              />
            </Row>
          </Col>
          <Col
            xl={columnSizes.inputColumn.xl}
            lg={columnSizes.inputColumn.lg}
            md={columnSizes.inputColumn.md}
            sm={columnSizes.inputColumn.sm}
            xs={columnSizes.inputColumn.xs}
            style={styles.inputContainer}
          >
            <div className="input-group" style={styles.reservationCodeContainer}>
              <input
                id={`reservation-code-field`}
                className={
                  this.state.isInvalidReservationCode || this.state.isReservationCodeAlreadyUsed
                    ? "invalid-field"
                    : "valid-field"
                }
                aria-labelledby={`reservation-code-field-title`}
                style={{ ...styles.textInput, ...accommodationsStyle }}
                type="text"
                value={this.state.reservationBarContent}
                onChange={this.updateReservationBarContent}
                onKeyPress={event => {
                  if (event.key === "Enter" && !this.state.currentlyLoading) {
                    this.reserveReservationCode();
                  }
                }}
              ></input>
            </div>
            <div>
              {this.state.isInvalidReservationCode && (
                <label
                  id="invalid-reservation-code-error"
                  htmlFor="reservation-code-field"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {
                    LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation
                      .invalidReservationCode
                  }
                </label>
              )}
              {this.state.isReservationCodeAlreadyUsed && (
                <label
                  id="reservation-code-already-used"
                  htmlFor="reservation-code-field"
                  style={styles.errorMessage}
                  className="notranslate"
                >
                  {
                    LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation
                      .reservationCodeAlreadyUsed
                  }
                </label>
              )}
            </div>
          </Col>
          <Col
            xl={columnSizes.buttonColumn.xl}
            lg={columnSizes.buttonColumn.lg}
            md={columnSizes.buttonColumn.md}
            sm={columnSizes.buttonColumn.sm}
            xs={columnSizes.buttonColumn.xs}
            style={styles.useCodeButtonContainer}
          >
            <CustomButton
              label={
                <>
                  <FontAwesomeIcon icon={faCheck} />
                  <span
                    id="use-reservation-code"
                    style={styles.buttonLabel}
                    className="notranslate"
                  >
                    {LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.button}
                  </span>
                </>
              }
              customStyle={styles.checkInBtn}
              action={this.reserveReservationCode}
              buttonTheme={THEME.PRIMARY}
            />
          </Col>
        </Row>
        <div style={styles.mainDiv}>
          <p style={styles.tablePrefix}>
            {
              LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation
                .reservedTablePrefixParagraph
            }
          </p>
          <GenericTable
            classnamePrefix="reserved-codes"
            columnsDefinition={reservedColumnsDefinition}
            rowsDefinition={reservedRowsDefinition}
            emptyTableMessage={
              LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.reservedTable
                .noDataMessage
            }
          />
        </div>
        <div style={isSearchingForSeat ? styles.mainDiv : styles.hidden}>
          <p style={styles.tablePrefix}>
            {LOCALIZE.formatString(
              LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.tablePrefixParagraph,
              dataCount,
              this.state.currentReservationCode
            )}
          </p>
          <GenericTable
            classnamePrefix="bookable-sessions"
            columnsDefinition={sessionColumnsDefinition}
            rowsDefinition={sessionRowsDefinition}
            emptyTableMessage={
              LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.table.noDataMessage
            }
            currentlyLoading={currentlyLoading}
            tableWithButtons={true}
          />
        </div>
        <ReservationDetailsDialog
          title={
            !showRetestPeriodWarningPopup
              ? LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.sessionPopup.title
              : LOCALIZE.retestPeriodWarningPopup.bookingTitle
          }
          source={RESERVATION_DETAILS_DIALOG_SOURCE.makeReservation}
          isOla={this.state.isOla}
          reservationDetails={reservation}
          showPopup={this.state.showReserveSessionPopup}
          closePopup={this.closeSessionDialog}
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeSessionDialog}
          hasRightButton={true}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faCalendarCheck}
          rightButtonTitle={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.sessionPopup.bookItButton
          }
          rightButtonLabel={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.sessionPopup.bookItButton
          } // TODO more accessible
          rightButtonAction={this.bookSession}
          extraText={
            <p>
              {LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.sessionPopup.paragraph}
            </p>
          }
          displayCloseButton={false}
          showRetestPeriodWarningPopup={showRetestPeriodWarningPopup}
        />
        <PopupBox
          show={this.state.showWithdrawSuccessfulPopup}
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.withdrawnSuccessPopup.title
          }
          handleClose={() => this.closeWithdrawSuccessfulDialog()}
          size={"lg"}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.success}
                    title={LOCALIZE.commons.success}
                    message={
                      LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation
                        .withdrawnSuccessPopup.paragraph
                    }
                  />
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.closeWithdrawSuccessfulDialog()}
        />
        <PopupBox
          show={this.state.showBookingSuccessfulPopup}
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.bookingSuccessPopup.title
          }
          handleClose={() => this.closeBookingSuccessfulDialog()}
          size={"lg"}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.success}
                    title={LOCALIZE.commons.success}
                    message={
                      LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation
                        .bookingSuccessPopup.paragraph
                    }
                  />
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.closeBookingSuccessfulDialog()}
        />
        <PopupBox
          show={this.state.showNoSessionsAvailablePopup}
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.noSessionsAvailablePopup
              .title
          }
          handleClose={() => this.closeNoSessionsAvailableDialog()}
          description={
            <div>
              <p>
                {
                  LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation
                    .noSessionsAvailablePopup.paragraph
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.closeNoSessionsAvailableDialog()}
        />
        <PopupBox
          show={this.state.showConfirmWithdrawPopup}
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.confirmWithdrawPopup.title
          }
          handleClose={() => this.cancelConfirmWithdraw()}
          size={"lg"}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation
                        .confirmWithdrawPopup.paragraph
                    }
                  />
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={() => this.cancelConfirmWithdraw()}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.confirm}
          rightButtonAction={() => this.confirmWithdraw()}
        />
        <PopupBox
          show={this.state.showErrorNotEnoughSeatsPopup}
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.notEnoughSeatsPopup.title
          }
          handleClose={() => this.closeErrorAndReSearch()}
          size={"lg"}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.error}
                    title={LOCALIZE.commons.error}
                    message={
                      LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation
                        .notEnoughSeatsPopup.paragraph
                    }
                  />
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.closeErrorAndReSearch()}
        />
        <PopupBox
          show={this.state.showErrorNotEnoughTimePopup}
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.notEnoughTimePopup.title
          }
          handleClose={() => this.closeErrorAndReSearch()}
          size={"lg"}
          description={
            <div>
              <p>
                {
                  <SystemMessage
                    messageType={MESSAGE_TYPE.error}
                    title={LOCALIZE.commons.error}
                    message={LOCALIZE.formatString(
                      LOCALIZE.dashboard.sideNavItems.reserveSeat.makeReservation.notEnoughTimePopup
                        .paragraph,
                      this.state.earliestBookingTime
                    )}
                  />
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.closeErrorAndReSearch()}
        />
        {this.state.selectedFormattedTestSkill !== "" && (
          <PopupBox
            show={
              this.state.showRequestAccommodationsPopup ||
              this.state.showNeedAccommodationsProfilePopup ||
              this.state.showAccommodationRequestSentSuccessfullyPopup
            }
            title={
              !this.state.showAccommodationRequestSentSuccessfullyPopup
                ? LOCALIZE.dashboard.requestAccommodationsPopup.title
                : LOCALIZE.dashboard.requestAccommodationsPopup.successTitle
            }
            handleClose={() => {}}
            size={
              !this.state.showAccommodationRequestSentSuccessfullyPopup &&
              !this.state.showNeedAccommodationsProfilePopup
                ? "xl"
                : "lg"
            }
            description={
              <>
                {!this.state.showAccommodationRequestSentSuccessfullyPopup ? (
                  <>
                    {this.state.showNeedAccommodationsProfilePopup ? (
                      <div>
                        <SystemMessage
                          messageType={MESSAGE_TYPE.info}
                          title={LOCALIZE.commons.info}
                          message={LOCALIZE.formatString(
                            LOCALIZE.dashboard.requestAccommodationsPopup
                              .needAccommodationsProfileSystemMessageDescription,
                            <button
                              aria-label={
                                LOCALIZE.dashboard.requestAccommodationsPopup
                                  .incompletedAccommodationsProfileLink
                              }
                              tabIndex="0"
                              onClick={this.handleUserProfileRedirect}
                              style={styles.profileLink}
                            >
                              {
                                LOCALIZE.dashboard.requestAccommodationsPopup
                                  .incompletedAccommodationsProfileLink
                              }
                            </button>
                          )}
                        />
                      </div>
                    ) : (
                      <div>
                        <p>
                          {LOCALIZE.formatString(
                            LOCALIZE.dashboard.requestAccommodationsPopup.description,
                            <span style={styles.boldText}>
                              {this.state.selectedFormattedTestSkill}
                            </span>
                          )}
                        </p>
                        <div style={styles.formContainer}>
                          <Row style={styles.fieldRowContainer}>
                            <Col
                              xl={columnSizes.popupFirstColumn.xl}
                              lg={columnSizes.popupFirstColumn.lg}
                              md={columnSizes.popupFirstColumn.md}
                              sm={columnSizes.popupFirstColumn.sm}
                              xs={columnSizes.popupFirstColumn.xs}
                            >
                              <label
                                id="request-accommodations-request-type-label"
                                style={styles.boldText}
                              >
                                {LOCALIZE.dashboard.requestAccommodationsPopup.requestTypeLabel}
                              </label>
                            </Col>
                            <Col
                              xl={columnSizes.popupSecondColumn.xl}
                              lg={columnSizes.popupSecondColumn.lg}
                              md={columnSizes.popupSecondColumn.md}
                              sm={columnSizes.popupSecondColumn.sm}
                              xs={columnSizes.popupSecondColumn.xs}
                            >
                              <DropdownSelect
                                idPrefix="request-accommodations-request-type"
                                ariaLabelledBy="request-accommodations-request-type-label"
                                ariaRequired={true}
                                options={this.state.requestTypeOptions}
                                onChange={this.getSelectedRequestTypeOption}
                                defaultValue={this.state.requestTypeSelectedOption}
                                hasPlaceholder={true}
                              />
                            </Col>
                          </Row>
                          {Object.keys(this.state.requestTypeSelectedOption).length > 0 && (
                            <>
                              {/* NOT OLA RESERVATION CODE */}
                              {!this.state.isOlaSelectedReservationCode && (
                                <Row
                                  className="align-items-center"
                                  style={styles.fieldRowContainer}
                                >
                                  <Col
                                    xl={columnSizes.popupFirstColumn.xl}
                                    lg={columnSizes.popupFirstColumn.lg}
                                    md={columnSizes.popupFirstColumn.md}
                                    sm={columnSizes.popupFirstColumn.sm}
                                    xs={columnSizes.popupFirstColumn.xs}
                                  >
                                    <label
                                      id="request-accommodations-test-center-location-label"
                                      style={styles.boldText}
                                    >
                                      {
                                        LOCALIZE.dashboard.requestAccommodationsPopup
                                          .testCenterLabel
                                      }
                                    </label>
                                  </Col>
                                  <Col
                                    xl={columnSizes.popupSecondColumn.xl}
                                    lg={columnSizes.popupSecondColumn.lg}
                                    md={columnSizes.popupSecondColumn.md}
                                    sm={columnSizes.popupSecondColumn.sm}
                                    xs={columnSizes.popupSecondColumn.xs}
                                  >
                                    {this.state.testCenterOptionsLoading ? (
                                      <div style={styles.loadingContainer}>
                                        <label className="fa fa-spinner fa-spin">
                                          <FontAwesomeIcon icon={faSpinner} />
                                        </label>
                                      </div>
                                    ) : (
                                      <DropdownSelect
                                        idPrefix="request-accommodations-test-center-location"
                                        ariaLabelledBy="request-accommodations-test-center-location-label"
                                        ariaRequired={true}
                                        options={this.state.testCenterOptions}
                                        onChange={this.getSelectedTestCenterLocation}
                                        defaultValue={this.state.testCenterSelectedOption}
                                        hasPlaceholder={true}
                                      />
                                    )}
                                  </Col>
                                </Row>
                              )}
                              {/* OLA RESERVATION CODE */}
                              {this.state.isOlaSelectedReservationCode && (
                                <Row
                                  className="align-items-center"
                                  style={styles.fieldRowContainer}
                                >
                                  <Col
                                    xl={columnSizes.popupFirstColumn.xl}
                                    lg={columnSizes.popupFirstColumn.lg}
                                    md={columnSizes.popupFirstColumn.md}
                                    sm={columnSizes.popupFirstColumn.sm}
                                    xs={columnSizes.popupFirstColumn.xs}
                                  >
                                    <label
                                      htmlFor="request-accommodations-phone-number-label"
                                      style={styles.boldText}
                                    >
                                      {
                                        LOCALIZE.dashboard.requestAccommodationsPopup
                                          .phoneNumberLabel
                                      }
                                    </label>
                                  </Col>
                                  <Col
                                    xl={columnSizes.popupSecondColumn.xl}
                                    lg={columnSizes.popupSecondColumn.lg}
                                    md={columnSizes.popupSecondColumn.md}
                                    sm={columnSizes.popupSecondColumn.sm}
                                    xs={columnSizes.popupSecondColumn.xs}
                                  >
                                    <InputMask
                                      id="request-accommodations-phone-number-input"
                                      className={"valid-field"}
                                      aria-labelledby="request-accommodations-phone-number-label"
                                      aria-required={true}
                                      style={{ ...styles.inputs, ...accommodationsStyle }}
                                      type="text"
                                      value={this.state.phoneNumberContent}
                                      mask={MASKS.phoneNumber}
                                      formatChars={MASK_FORMAT_CHARS.phoneNumber}
                                      onChange={this.updatePhoneNumberContent}
                                    />
                                  </Col>
                                </Row>
                              )}
                              <Row style={styles.fieldRowContainer}>
                                <Col
                                  xl={columnSizes.popupFirstColumn.xl}
                                  lg={columnSizes.popupFirstColumn.lg}
                                  md={columnSizes.popupFirstColumn.md}
                                  sm={columnSizes.popupFirstColumn.sm}
                                  xs={columnSizes.popupFirstColumn.xs}
                                >
                                  <label
                                    htmlFor="request-accommodations-comments-or-test-administered-details-input"
                                    style={styles.boldText}
                                  >
                                    {this.state.requestTypeSelectedOption.label ===
                                    LOCALIZE.dashboard.requestAccommodationsPopup.requestTypeOptions
                                      .accommodations
                                      ? // accommodation request
                                        LOCALIZE.dashboard.requestAccommodationsPopup.commentLabel
                                      : // alternate test or both request
                                        LOCALIZE.dashboard.requestAccommodationsPopup
                                          .testAdministeredDetailsLabel}
                                  </label>
                                </Col>
                                <Col
                                  xl={columnSizes.popupSecondColumn.xl}
                                  lg={columnSizes.popupSecondColumn.lg}
                                  md={columnSizes.popupSecondColumn.md}
                                  sm={columnSizes.popupSecondColumn.sm}
                                  xs={columnSizes.popupSecondColumn.xs}
                                >
                                  <textarea
                                    id="request-accommodations-comments-or-test-administered-details-input"
                                    className={"valid-field"}
                                    style={{ ...styles.textAreaInput, ...accommodationsStyle }}
                                    onChange={this.getCommentsOrTestAdministeredDetailsContent}
                                    value={this.state.commentsOrTestAdministeredDetails}
                                    maxLength="5000"
                                  ></textarea>
                                </Col>
                              </Row>
                              <Row className="align-items-center" style={styles.rowContainer}>
                                <input
                                  type="checkbox"
                                  id="request-accommodations-profile-up-to-date-checkbox-input"
                                  value={this.state.profileUpToDateCheckboxChecked}
                                  style={{
                                    ...styles.checkbox,
                                    ...{ transform: checkboxTransformScale }
                                  }}
                                  onChange={this.getCheckboxState}
                                />

                                <label
                                  htmlFor="request-accommodations-profile-up-to-date-checkbox-input"
                                  style={styles.noMargin}
                                >
                                  {LOCALIZE.formatString(
                                    LOCALIZE.dashboard.requestAccommodationsPopup
                                      .checkboxDescription,
                                    <button
                                      aria-label={
                                        LOCALIZE.dashboard.requestAccommodationsPopup
                                          .checkboxDescriptionLink
                                      }
                                      tabIndex="0"
                                      onClick={this.handleUserProfileRedirect}
                                      style={styles.profileLink}
                                    >
                                      {
                                        LOCALIZE.dashboard.requestAccommodationsPopup
                                          .checkboxDescriptionLink
                                      }
                                    </button>
                                  )}
                                </label>
                              </Row>
                              {this.state.isOlaSelectedReservationCode && (
                                <>
                                  <Row role="presentation" className="align-items-center">
                                    <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                                      <div>
                                        <input
                                          type="checkbox"
                                          id="make-reservation-main-terms-of-use-checkbox"
                                          checked={this.state.mainTermsOfUseChecked}
                                          style={{
                                            ...styles.checkbox,
                                            ...{ transform: checkboxTransformScale }
                                          }}
                                          onClick={this.openTermsOfUsePopup}
                                        />
                                        <label
                                          htmlFor="make-reservation-main-terms-of-use-checkbox"
                                          style={styles.noMargin}
                                        >
                                          {LOCALIZE.formatString(
                                            LOCALIZE.dashboard.sideNavItems.reserveSeat
                                              .reservationDetailsPopup.termsOfUseCheckboxLabel,
                                            <button
                                              aria-label={
                                                LOCALIZE.dashboard.sideNavItems.reserveSeat
                                                  .reservationDetailsPopup.termsOfUseCheckboxLink
                                              }
                                              tabIndex="0"
                                              onClick={this.openTermsOfUsePopup}
                                              style={styles.checkboxLink}
                                            >
                                              {
                                                LOCALIZE.dashboard.sideNavItems.reserveSeat
                                                  .reservationDetailsPopup.termsOfUseCheckboxLink
                                              }
                                            </button>
                                          )}
                                        </label>
                                      </div>
                                    </Col>
                                  </Row>
                                  <Row
                                    role="presentation"
                                    className="align-items-center"
                                    style={styles.checkboxRowContainer}
                                  >
                                    <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                                      <div>
                                        <input
                                          type="checkbox"
                                          id="make-reservation-main-privacy-notice-statement-checkbox"
                                          checked={this.state.mainPrivacyNoticeStatementChecked}
                                          style={{
                                            ...styles.checkbox,
                                            ...{ transform: checkboxTransformScale }
                                          }}
                                          onClick={this.openPrivacyNoticeStatementPopup}
                                        />
                                        <label
                                          htmlFor="make-reservation-main-privacy-notice-statement-checkbox"
                                          style={styles.noMargin}
                                        >
                                          {LOCALIZE.formatString(
                                            LOCALIZE.dashboard.sideNavItems.reserveSeat
                                              .reservationDetailsPopup
                                              .privacyNoticeStatementCheckboxLabel,
                                            <button
                                              aria-label={
                                                LOCALIZE.dashboard.sideNavItems.reserveSeat
                                                  .reservationDetailsPopup
                                                  .privacyNoticeStatementCheckboxLink
                                              }
                                              tabIndex="0"
                                              onClick={this.openPrivacyNoticeStatementPopup}
                                              style={styles.checkboxLink}
                                            >
                                              {
                                                LOCALIZE.dashboard.sideNavItems.reserveSeat
                                                  .reservationDetailsPopup
                                                  .privacyNoticeStatementCheckboxLink
                                              }
                                            </button>
                                          )}
                                        </label>
                                      </div>
                                    </Col>
                                  </Row>
                                </>
                              )}
                            </>
                          )}
                        </div>
                      </div>
                    )}
                  </>
                ) : (
                  <div>
                    <SystemMessage
                      messageType={MESSAGE_TYPE.success}
                      title={LOCALIZE.commons.success}
                      message={
                        LOCALIZE.dashboard.requestAccommodationsPopup
                          .successSystemMessageDescription
                      }
                    />
                  </div>
                )}
              </>
            }
            leftButtonType={
              !this.state.showAccommodationRequestSentSuccessfullyPopup
                ? BUTTON_TYPE.secondary
                : BUTTON_TYPE.none
            }
            leftButtonTitle={LOCALIZE.commons.cancel}
            leftButtonIcon={
              !this.state.showAccommodationRequestSentSuccessfullyPopup ? faTimes : ""
            }
            leftButtonAction={
              !this.state.showAccommodationRequestSentSuccessfullyPopup
                ? this.closeRequestAccommodationsPopup
                : () => {}
            }
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={
              this.state.actionInProgress ? (
                <div style={styles.customLoadingContainer}>
                  {/* this div is useful to get the right size of the button while loading */}
                  <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
                    <FontAwesomeIcon icon={faPaperPlane} style={styles.icon} />
                    {LOCALIZE.dashboard.requestAccommodationsPopup.rightButton}
                  </div>
                  <div style={styles.loadingOverlappingStyle}>
                    <label className="fa fa-spinner fa-spin">
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  </div>
                </div>
              ) : !this.state.showAccommodationRequestSentSuccessfullyPopup ? (
                LOCALIZE.dashboard.requestAccommodationsPopup.rightButton
              ) : (
                LOCALIZE.commons.close
              )
            }
            rightButtonIcon={
              this.state.actionInProgress
                ? ""
                : !this.state.showAccommodationRequestSentSuccessfullyPopup
                  ? faPaperPlane
                  : faTimes
            }
            rightButtonAction={
              !this.state.showAccommodationRequestSentSuccessfullyPopup
                ? this.handleSubmitAccommodationsRequest
                : this.closeRequestAccommodationsPopup
            }
            rightButtonState={
              // qualityOfLife enabled + request type selected
              // if not OLA ==> test center selected
              this.props.qualityOfLife &&
              Object.keys(this.state.requestTypeSelectedOption).length > 0 &&
              (!this.state.isOlaSelectedReservationCode
                ? Object.keys(this.state.testCenterSelectedOption).length > 0
                : true)
                ? BUTTON_STATE.enabled
                : this.state.actionInProgress
                  ? BUTTON_STATE.disabled
                  : this.state.isValidForm
                    ? BUTTON_STATE.enabled
                    : BUTTON_STATE.disabled
            }
          />
        )}
        <PopupBox
          show={this.state.showTermsOfUsePopup}
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup.termsOfUsePopup
              .title
          }
          handleClose={() => {}}
          size={"lg"}
          description={
            <TermsOfUseContent
              handleAcceptTermsOfUseCheckbox={this.handleAcceptTermsOfUseCheckbox}
              termsOfUseChecked={this.state.termsOfUseChecked}
            />
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeTermsOfUsePopup}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.accept}
          rightButtonAction={this.handleAcceptTermsOfUse}
          rightButtonIcon={faCheck}
          rightButtonState={
            this.state.termsOfUseChecked ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
        />
        <PopupBox
          show={this.state.showPrivacyNoticeStatementPopup}
          title={
            LOCALIZE.dashboard.sideNavItems.reserveSeat.reservationDetailsPopup
              .privacyNoticeStatementPopup.title
          }
          handleClose={() => {}}
          size={"lg"}
          description={
            <PrivacyNoticeStatementContent
              handleAcceptPrivacyNoticeStatementCheckbox={
                this.handleAcceptPrivacyNoticeStatementCheckbox
              }
              privacyNoticeStatementChecked={this.state.privacyNoticeStatementChecked}
            />
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closePrivacyNoticeStatementPopup}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.accept}
          rightButtonAction={this.handleAcceptPrivacyNoticeStatement}
          rightButtonIcon={faCheck}
          rightButtonState={
            this.state.privacyNoticeStatementChecked ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    reserveTestSessionPhoneNumberState: state.candidate.reserveTestSessionPhoneNumberState,
    qualityOfLife: state.siteAdminSettings.qualityOfLife
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      verifyReservationCode,
      reserveAndVerifyReservationCode,
      findSessionsForReservationCode,
      findAllReservedCodesForUser,
      withdrawUnbookedReservationCode,
      bookNowReservationCode,
      createVirtualMeeting,
      isWithinTheRetestPeriod,
      getTestCentersForUserAccommodationRequest,
      sendUserAccommodationRequest,
      setMyProfileSideNavigationSelectedItem,
      getEnableQualityofLife,
      setQualityOfLife
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(MakeReservation);
