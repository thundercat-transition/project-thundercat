import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { styles as AssignTestAccessesStyle } from "../etta/test_accesses/AssignTestAccesses";
import DropdownSelect from "../commons/DropdownSelect";
import {
  updateTestToAdministerState,
  updateTestSessionOfficersState,
  updateGenerateButtonDisabledState
} from "../../modules/TestAdministrationRedux";
import { BUTTON_STATE } from "../commons/PopupBox";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";
import { Col, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { getTestSessionRelatedTestOfficers } from "../../modules/TestCenterRedux";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 7,
    xl: 7
  }
};

const styles = {
  mainPopupContainer: {
    padding: "10px 15px",
    width: "100%"
  },
  col: {
    width: "100%"
  },
  row: {
    marginBottom: "15px"
  },
  loading: {
    width: "100%",
    textAlign: "center",
    fontSize: "26px",
    padding: 6
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  }
};

class TestSessionInformationPopup extends Component {
  static propTypes = {
    testSessionSelectedOption: PropTypes.array.isRequired,
    testToAdministerOptions: PropTypes.array.isRequired,
    isLoadingTestToAdministerOptions: PropTypes.bool.isRequired,
    showCannotGenerateTestAccessCodeError: PropTypes.bool.isRequired
  };

  state = {
    testSessionOfficerOptions: [],
    isLoadingTestSessionOfficerOptions: false
  };

  componentDidMount = () => {
    // initializing generate button disabled state to disabled
    this.props.updateGenerateButtonDisabledState(BUTTON_STATE.disabled);
  };

  componentDidUpdate = prevProps => {
    // if testToAdminister or testSessionOfficers gets updated
    if (
      prevProps.testToAdminister !== this.props.testToAdminister ||
      prevProps.testSessionOfficers !== this.props.testSessionOfficers
    ) {
      // validating form
      this.validateForm();
    }
  };

  // get selected test to administer option + setting needed states
  onSelectedTestToAdministerOptionChange = selectedOption => {
    // updating redux states
    this.props.updateTestToAdministerState(selectedOption);
    this.setState({ isLoadingTestSessionOfficerOptions: true }, () => {
      // adding small delay to make sure that the props are well set
      setTimeout(() => {
        // populating test session officers options
        this.populateTestSessionOfficersOptions();
      }, 250);
    });
  };

  populateTestSessionOfficersOptions = () => {
    const testSessionOfficerOptions = [];

    this.setState({ isLoadingTestSessionOfficerOptions: true }, () => {
      // getting test session related test administrators
      this.props
        .getTestSessionRelatedTestOfficers(
          this.props.testSessionSelectedOption.value,
          this.props.testToAdminister.value
        )
        .then(response => {
          // looping in response
          for (let i = 0; i < response.length; i++) {
            // populating testSessionOfficerOptions
            testSessionOfficerOptions.push({
              value: response[i].user_data[0].id,
              label: `${response[i].user_data[0].last_name}, ${response[i].user_data[0].first_name} (${response[i].user_data[0].username})`
            });

            // if user of current iteration matches current user
            if (response[i].user_data[0].username === this.props.username) {
              // updating test session officer redux state
              this.props.updateTestSessionOfficersState([
                {
                  value: response[i].user_data[0].id,
                  label: `${response[i].user_data[0].last_name}, ${response[i].user_data[0].first_name} (${response[i].user_data[0].username})`
                }
              ]);
            }
          }

          // saving results in state
          this.setState({
            testSessionOfficerOptions: testSessionOfficerOptions,
            isLoadingTestSessionOfficerOptions: false
          });
        });
    });
  };

  // get selected test session officer option + setting needed states
  onSelectedTestSessionOfficersOptionChange = selectedOption => {
    let options = null;
    if (selectedOption === null) {
      options = [];
    } else {
      options = selectedOption;
    }
    // updating redux states
    this.props.updateTestSessionOfficersState(options);
  };

  // validating form
  validateForm = () => {
    // initializing needed variables
    const isValidTestToAdminister = Object.keys(this.props.testToAdminister).length > 0;
    const isValidTestSessionOfficers = Object.keys(this.props.testSessionOfficers).length > 0;

    // all required fields are provided
    if (isValidTestToAdminister && isValidTestSessionOfficers) {
      // updating generate button disabled state
      this.props.updateGenerateButtonDisabledState(BUTTON_STATE.enabled);
      // at least one field is missing
    } else {
      // updating generate button disabled state
      this.props.updateGenerateButtonDisabledState(BUTTON_STATE.disabled);
    }
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }
    return (
      <div style={styles.mainPopupContainer}>
        <Row className="align-items-center" style={styles.row}>
          <Col style={styles.col}>
            <p>
              {
                LOCALIZE.testAdministration.supervisedTesting.generateTestAccessCodePopup
                  .description
              }
            </p>
          </Col>
        </Row>
        <Row className="align-items-center" style={styles.row}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            style={styles.col}
          >
            <label id="test-session-label" style={AssignTestAccessesStyle.label}>
              {
                LOCALIZE.testAdministration.supervisedTesting.generateTestAccessCodePopup
                  .testSession
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
            style={styles.col}
          >
            <DropdownSelect
              idPrefix="generate-test-access-code-popup-test-session-dropdown"
              ariaLabelledBy="test-session-label"
              options={[]}
              onChange={() => {}}
              defaultValue={this.props.testSessionSelectedOption}
              hasPlaceholder={false}
              isDisabled={true}
            ></DropdownSelect>
          </Col>
        </Row>
        <Row className="align-items-center" style={styles.row}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            style={styles.col}
          >
            <label id="test-to-administer-label" style={AssignTestAccessesStyle.label}>
              {
                LOCALIZE.testAdministration.supervisedTesting.generateTestAccessCodePopup
                  .testToAdminister
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
            style={styles.col}
          >
            {!this.props.isLoadingTestToAdministerOptions ? (
              <>
                <DropdownSelect
                  idPrefix="generate-test-access-code-popup-test-dropdown"
                  ariaLabelledBy="test-to-administer-label cannot-generate-test-access-code-error"
                  options={this.props.testToAdministerOptions}
                  onChange={this.onSelectedTestToAdministerOptionChange}
                  defaultValue={this.props.testToAdminister}
                  hasPlaceholder={true}
                  isDisabled={this.props.testToAdministerDropdownDisabled}
                  isValid={!this.props.showCannotGenerateTestAccessCodeError}
                ></DropdownSelect>
                {this.props.showCannotGenerateTestAccessCodeError && (
                  <label
                    id="cannot-generate-test-access-code-error"
                    style={styles.errorMessage}
                    className="notranslate"
                  >
                    {
                      LOCALIZE.testAdministration.supervisedTesting.generateTestAccessCodePopup
                        .cannotGenerateTestAccessCodeError
                    }
                  </label>
                )}
              </>
            ) : (
              // eslint-disable-next-line jsx-a11y/label-has-associated-control
              <label className="fa fa-spinner fa-spin" style={styles.loading}>
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            )}
          </Col>
        </Row>
        <Row className="align-items-center" style={styles.row}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            style={styles.col}
          >
            <label id="test-session-officer-label" style={AssignTestAccessesStyle.label}>
              {
                LOCALIZE.testAdministration.supervisedTesting.generateTestAccessCodePopup
                  .testSessionOfficers
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
            style={styles.col}
          >
            {!this.state.isLoadingTestSessionOfficerOptions ? (
              <DropdownSelect
                idPrefix="generate-test-access-code-popup-test-session-officers-dropdown"
                ariaLabelledBy="test-session-officer-label"
                options={this.state.testSessionOfficerOptions}
                onChange={this.onSelectedTestSessionOfficersOptionChange}
                defaultValue={this.props.testSessionOfficers}
                hasPlaceholder={true}
                isMulti={true}
                isDisabled={Object.keys(this.props.testToAdminister).length <= 0}
              ></DropdownSelect>
            ) : (
              // eslint-disable-next-line jsx-a11y/label-has-associated-control
              <label className="fa fa-spinner fa-spin" style={styles.loading}>
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    currentLanguage: state.localize.language,
    username: state.user.username,
    testToAdminister: state.testAdministration.testToAdminister,
    testSessionOfficers: state.testAdministration.testSessionOfficers,
    testToAdministerDropdownDisabled: state.testAdministration.testToAdministerDropdownDisabled
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateTestToAdministerState,
      updateTestSessionOfficersState,
      updateGenerateButtonDisabledState,
      getTestSessionRelatedTestOfficers
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestSessionInformationPopup);
