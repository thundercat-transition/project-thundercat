import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { styles as ActivePermissionsStyles } from "../etta/permissions/ActivePermissions";
import { alternateColorsStyle } from "../commons/Constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faClock,
  faWandSparkles,
  faTimes,
  faThumbsUp,
  faLock,
  faLockOpen,
  faTrashAlt,
  faSpinner,
  faListCheck,
  faPrint,
  faUniversalAccess,
  faFileCircleExclamation
} from "@fortawesome/free-solid-svg-icons";
import { faThumbsUp as regularFaThumbsUp } from "@fortawesome/free-regular-svg-icons";
import {
  deleteTestAccessCode,
  resetTestAdministrationStates,
  getNewTestAccessCode,
  getActiveTestAccessCodesRedux,
  getTestAdministratorAssignedCandidates,
  unAssignCandidate,
  approveCandidate,
  lockCandidateTest,
  unlockCandidateTest,
  unlockAllCandidatesTest,
  lockAllCandidatesTest,
  approveAllCandidates
} from "../../modules/TestAdministrationRedux";

import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import TestSessionInformationPopup from "./TestSessionInformationPopup";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import { Col, Row } from "react-bootstrap";
import DropdownSelect from "../commons/DropdownSelect";
import {
  getTaRelatedTestCenterTestSessions,
  getTestSessionAndTaRelatedTestToAdministerOptions,
  getTestSessionAttendees
} from "../../modules/TestCenterRedux";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";
import getFormattedTestStatus from "./TestStatus";
import TEST_STATUS from "./Constants";
import { getTimeInHoursMinutes } from "../../helpers/timeConversion";
import { taActions } from "./TestAdministration";
import { getAssignedTests } from "../../modules/AssignedTestsRedux";
import { setDefaultTimes, updateTimerState } from "../../modules/SetTimerRedux";
import getContentToPrint from "../../helpers/testSessionAttendees";
import AccommodationsRequestDetailsPopup, {
  ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE
} from "../aae/AccommodationsRequestDetailsPopup";
import ReactToPrint from "../commons/ReactToPrint";
import { convertDateTimeToUsersTimezone } from "../../modules/helpers";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 2,
    xl: 2
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 10,
    xl: 10
  },
  firstColumnPopup: {
    xs: 12,
    sm: 12,
    md: 4,
    lg: 4,
    xl: 4
  },
  secondColumnPopup: {
    xs: 12,
    sm: 12,
    md: 8,
    lg: 8,
    xl: 8
  }
};

export const styles = {
  mainContainer: {
    width: "100%"
  },
  testSessionContainer: {
    margin: "24px 0px 36px 0px"
  },
  labelContainer: {
    paddingRight: 12
  },
  dropdownContainer: {
    width: "100%",
    display: "table"
  },
  testSessionDropdown: {
    display: "table-cell",
    width: "80%"
  },
  testSessionAttendees: {
    display: "table-cell",
    paddingLeft: 12
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: 0,
    margin: "0 3px",
    color: "#00565e"
  },
  testAccessCodesTitleLabel: {
    fontWeight: "bold",
    paddingBottom: 20
  },
  generateCodeButton: {
    background: "transparent",
    padding: 12,
    border: "none",
    color: " #00565e"
  },
  generateCodeButtonIcon: {
    marginRight: 12,
    color: "#00565e"
  },
  assignedCandidatesContainer: {
    margin: "48px 0 24px 0"
  },
  activeCandidatesTableTitleContainer: {
    marginBottom: 12
  },
  activeCandidatesTableTitleLabel: {
    float: "left"
  },
  activeCandidatesTableTitleButton: {
    textAlign: "right"
  },
  approveAllButtonContainer: {
    paddingRight: 18
  },
  boldText: {
    fontWeight: "bold"
  },
  allUnset: {
    all: "unset"
  },
  buttonIcon: {
    transform: "scale(1.5)",
    paddingRight: 6
  },
  buttonIconTooltip: {
    transform: "scale(1.5)",
    color: "#00565e",
    margin: "0 6px"
  },
  visibilityHidden: {
    visibility: "hidden"
  },
  disabledActionButton: {
    // default disabled color
    color: "#DDDDDD"
  }
};

class SupervisedTesting extends Component {
  constructor(props) {
    super(props);
    this.componentToPrintRef = React.createRef();
  }

  state = {
    testSessionOptions: [],
    testSessionSelectedOption: [],
    viewAttendeesButtonDisabled: true,
    showTestSessionAttendeesPopup: false,
    testSessionAttendeesLoading: false,
    testSessionAttendeesRowsDefinition: {},
    showAccommodationsRequestDetailsPopup: false,
    selectedAccommodationRequestData: {},
    testToAdministerOptions: [],
    isLoadingTestToAdministerOptions: true,
    showGenerateTestAccessCodePopup: false,
    showCannotGenerateTestAccessCodeError: false,
    showDeleteTacConfirmationPopup: false,
    testAccessCodeToDelete: "",
    testAccessCodesRowsDefinition: {},
    testAccessCodes: [],
    assignedCandidatesRowsDefinition: {},
    assignedCandidates: [],
    pollingState: undefined,
    triggerReRender: false,
    selectedCandidateData: {},
    isRightPopupButtonDisabled: false,
    showUnAssignPopup: false,
    showLockPopup: false,
    showUnlockPopup: false,
    atLeastOneTestToLockUnlock: false,
    allTestsLockedFlag: false,
    showLockAllPopup: false,
    showUnlockAllPopup: false,
    showApproveAllPopup: false,
    atLeastOneTestToApprove: false,
    isLoadingApproveAll: false,
    triggerReactToPrint: false
  };

  componentDidMount = () => {
    // populating test session options
    this.populateTestSessionOptions();
  };

  componentDidUpdate = (prevProps, prevState) => {
    // if testSessionSelectedOption gets updated
    if (prevState.testSessionSelectedOption !== this.state.testSessionSelectedOption) {
      // clearing existing polling interval(s) and creating new one (interval of 5 seconds)
      clearInterval(this.state.pollingState);
      // updating table data every 5 seconds
      const interval = setInterval(this.updateTablesData, 5000);
      this.setState({ pollingState: interval });
    }
    // if assignedCandidates gets updated
    if (prevState.assignedCandidates !== this.state.assignedCandidates) {
      // initializing allTestsLockedFlag to true
      let allTestsLockedFlag = true;
      let atLeastOneTestToLockUnlock = false;
      // looping in testAdministratorAssignedCandidates array
      for (let i = 0; i < this.state.assignedCandidates.length; i++) {
        // if these is at least one test that is checked-in, pre-test, active or paused
        if (
          this.state.assignedCandidates[i].status_codename === TEST_STATUS.CHECKED_IN ||
          this.state.assignedCandidates[i].status_codename === TEST_STATUS.PRE_TEST ||
          this.state.assignedCandidates[i].status_codename === TEST_STATUS.ACTIVE ||
          this.state.assignedCandidates[i].status_codename === TEST_STATUS.TRANSITION ||
          this.state.assignedCandidates[i].status_codename === TEST_STATUS.PAUSED
        ) {
          // set allTestsLockedFlag to false
          allTestsLockedFlag = false;
        }
        // if there is at least one test to lock/unlock
        if (
          this.state.assignedCandidates[i].status_codename === TEST_STATUS.CHECKED_IN ||
          this.state.assignedCandidates[i].status_codename === TEST_STATUS.PRE_TEST ||
          this.state.assignedCandidates[i].status_codename === TEST_STATUS.ACTIVE ||
          this.state.assignedCandidates[i].status_codename === TEST_STATUS.TRANSITION ||
          this.state.assignedCandidates[i].status_codename === TEST_STATUS.LOCKED ||
          this.state.assignedCandidates[i].status_codename === TEST_STATUS.PAUSED
        ) {
          // set allTestsLockedFlag to false
          atLeastOneTestToLockUnlock = true;
        }
      }
      this.setState({
        allTestsLockedFlag: allTestsLockedFlag,
        atLeastOneTestToLockUnlock: atLeastOneTestToLockUnlock
      });
    }
    // if selectedCandidateData gets updated
    if (
      prevState.selectedCandidateData !== this.state.selectedCandidateData &&
      Object.keys(this.state.selectedCandidateData).length > 0
    ) {
      // updating related states
      const defaultTimes = [];
      for (let i = 0; i < this.state.selectedCandidateData.assigned_test_sections.length; i++) {
        if (this.state.selectedCandidateData.assigned_test_sections[i].test_section_time !== null) {
          defaultTimes.push(
            this.state.selectedCandidateData.assigned_test_sections[i].test_section.default_time
          );
        }
      }
      this.props.setDefaultTimes(defaultTimes);
    }
  };

  componentWillUnmount = () => {
    clearInterval(this.state.pollingState);
  };

  // Update to users timezone here:
  populateTestSessionOptions = () => {
    // initializing testSessionOptions
    const testSessionOptions = [];
    this.props.getTaRelatedTestCenterTestSessions().then(response => {
      // looping in response
      for (let i = 0; i < response.length; i++) {
        // Converting time to users timezone
        const adjustedStartTime = convertDateTimeToUsersTimezone(
          response[i].start_time
        ).adjustedTime;

        const adjustedEndTime = convertDateTimeToUsersTimezone(response[i].end_time).adjustedTime;

        let formattedTestSkill = `${
          response[i][`test_skill_type_${this.props.currentLanguage}_name`]
        }`;
        if (response[i].test_skill_sub_type_id !== null) {
          formattedTestSkill = formattedTestSkill.concat(
            ` - ${response[i][`test_skill_sub_type_${this.props.currentLanguage}_name`]}`
          );
        }
        testSessionOptions.push({
          value: response[i].id,
          label: `${response[i].test_center_name} - ${response[i].room_name}: ${response[i].date} (${adjustedStartTime} - ${adjustedEndTime})`,
          formatted_date: `${response[i].date} (${adjustedStartTime} - ${adjustedEndTime}`,
          room_name: `${response[i].room_name}`,
          formatted_test_skill: formattedTestSkill,
          user_accommodation_file_id: response[i].user_accommodation_file_id,
          is_alternate_test_request: response[i].is_alternate_test_request,
          user_id: response[i].user_id,
          test_name_en: response[i].test_name_en,
          test_name_fr: response[i].test_name_fr,
          version: response[i].version
        });
      }
      // updating state
      this.setState({ testSessionOptions: testSessionOptions });
    });
  };

  // get selected test session
  getSelectedTestSession = selectedOption => {
    this.setState(
      { testSessionSelectedOption: selectedOption, viewAttendeesButtonDisabled: true },
      () => {
        // populating/updating tables
        this.updateTablesData();
        // getting test session attendees
        this.getTestSessionAttendees();
      }
    );
  };

  getTestSessionAttendees = () => {
    // getting related attendees
    this.setState(
      {
        testSessionAttendeesLoading: true
      },
      () => {
        this.props
          .getTestSessionAttendees(this.state.testSessionSelectedOption.value)
          .then(response => {
            // initializing needed object and array for testSessionAttendeesRowsDefinition props (needed for GenericTable component)
            let testSessionAttendeesRowsDefinition = {};
            const data = [];

            // looping in response given
            for (let i = 0; i < response.length; i++) {
              const currentResult = response[i];
              // formatting CAT user data
              const formattedCatUserData = `${currentResult.cat_user_last_name}, ${currentResult.cat_user_first_name} (${currentResult.cat_user_email})`;
              // pushing needed data in data array (needed for GenericTable component)
              data.push({
                column_1: currentResult.first_name,
                column_2: currentResult.last_name,
                column_3: currentResult.email,
                column_4: formattedCatUserData
              });
            }

            // updating testSessionAttendeesRowsDefinition object with provided data and needed style
            testSessionAttendeesRowsDefinition = {
              column_1_style: COMMON_STYLE.LEFT_TEXT,
              column_2_style: COMMON_STYLE.LEFT_TEXT,
              column_3_style: COMMON_STYLE.LEFT_TEXT,
              column_4_style: COMMON_STYLE.LEFT_TEXT,
              data: data
            };

            // saving results in state
            this.setState({
              testSessionAttendeesRowsDefinition: testSessionAttendeesRowsDefinition,
              testSessionAttendeesLoading: false,
              viewAttendeesButtonDisabled: !(response.length > 0)
            });
          });
      }
    );
  };

  openTestSessionAttendeesPopup = () => {
    this.setState({ showTestSessionAttendeesPopup: true }, () => {
      this.getTestSessionAttendees();
    });
  };

  closeTestSessionAttendeesPopup = () => {
    this.setState({
      showTestSessionAttendeesPopup: false
    });
  };

  openAccommodationsRequestDetailsPopup = accommodationRequestData => {
    // updating needed states
    this.setState({
      showAccommodationsRequestDetailsPopup: true,
      selectedAccommodationRequestData: accommodationRequestData
    });
  };

  closeAccommodationsRequestDetailsPopup = () => {
    this.setState({
      showAccommodationsRequestDetailsPopup: false,
      selectedAccommodationRequestData: {}
    });
  };

  openGenerateTestAccessCodePopup = () => {
    this.setState({
      showGenerateTestAccessCodePopup: true
    });
  };

  closeGenerateTestAccessCodePopup = () => {
    this.setState(
      {
        showGenerateTestAccessCodePopup: false,
        showCannotGenerateTestAccessCodeError: false
      },
      () => {
        // adding small delay to avoid UI loading glitch
        setTimeout(() => {
          this.setState({
            isLoadingTestToAdministerOptions: true
          });
          // resetting inputs
          this.resetInputs();
        }, 250);
      }
    );
  };

  // populating test to administer options
  populateTestToAdministerOptions = () => {
    const testToAdministerOptions = [];

    this.setState({ isLoadingTestToAdministerOptions: true }, () => {
      // getting test session and TA related test to administer options
      this.props
        .getTestSessionAndTaRelatedTestToAdministerOptions(
          this.state.testSessionSelectedOption.value
        )
        .then(response => {
          // looping in response
          for (let i = 0; i < response.length; i++) {
            // populating testToAdministerOptions
            testToAdministerOptions.push({
              value: response[i].id,
              label: `${response[i].test_code} - ${
                response[i][`${this.props.currentLanguage}_name`]
              } (v${response[i].version})`
            });
          }
        });

      // saving results in state
      this.setState({
        testToAdministerOptions: testToAdministerOptions,
        isLoadingTestToAdministerOptions: false
      });
    });
  };

  openDeleteTacConfirmationPopup = index => {
    this.setState({
      showDeleteTacConfirmationPopup: true,
      testAccessCodeToDelete: this.state.testAccessCodes[index].test_access_code
    });
  };

  closeDeleteTacConfirmationPopup = () => {
    this.setState({ showDeleteTacConfirmationPopup: false, testAccessCodeToDelete: "" });
  };

  // resetting test administration redux states
  resetInputs = () => {
    this.props.resetTestAdministrationStates();
  };

  // handle disable selected test access code
  handleDeleteTestAccessCode = () => {
    this.props.deleteTestAccessCode(this.state.testAccessCodeToDelete).then(response => {
      // if request succeeded
      if (response.status === 200) {
        // re-populating active test access codes table
        this.populateTestAccessCodesTable(this.state.testSessionSelectedOption.value);
        // reseting testAccessCodeToDelete state
        this.setState({ testAccessCodeToDelete: "" });
        // should never happen
      } else {
        throw new Error("Something went wrong during delete test access code process");
      }
    });
  };

  // handling generate new test access code functionality
  handleGenerateTestAccessCode = () => {
    // formatting test session officers array
    const formattedTestSessionOfficersArray = [];
    for (let i = 0; i < this.props.testSessionOfficers.length; i++) {
      formattedTestSessionOfficersArray.push(this.props.testSessionOfficers[i].value);
    }
    // creating body
    const body = {
      test_session_id: this.state.testSessionSelectedOption.value,
      test_id: this.props.testToAdminister.value,
      test_session_officers: formattedTestSessionOfficersArray
    };

    // getting new test access code based on selected parameters
    this.props.getNewTestAccessCode(body).then(response => {
      // request successful
      if (response.status === 200) {
        // re-populating active test access codes table
        this.populateTestAccessCodesTable(this.state.testSessionSelectedOption.value);
        // closing popup
        this.closeGenerateTestAccessCodePopup();
        // resetting inputs
        this.resetInputs();
      } else if (response.status === 405) {
        // displaying cannot generate TAC error
        this.setState({ showCannotGenerateTestAccessCodeError: true });
        // should never happen
      } else {
        throw new Error("Something went wrong during generate new test access code process");
      }
    });
  };

  // populating Test Access Codes table
  populateTestAccessCodesTable = testSessionId => {
    // initializing needed object and array for testAccessCodesRowsDefinition state (needed for GenericTable component)
    let testAccessCodesRowsDefinition = {};
    const data = [];
    const testAccessCodes = [];

    // getting test access codes
    this.props.getActiveTestAccessCodesRedux(testSessionId).then(response => {
      // looping in response
      for (let i = 0; i < response.length; i++) {
        // populating data
        data.push({
          column_1: response[i].test_access_code,
          column_2: response[i][`${this.props.currentLanguage}_test_name`],
          column_3: (
            <StyledTooltip
              id={`test-session-delete-row-${i}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`test-session-delete-row-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faTrashAlt} />
                      </>
                    }
                    ariaLabel={LOCALIZE.formatString(
                      LOCALIZE.testAdministration.supervisedTesting.testAccessCodesTable
                        .actionButtonAriaLabel,
                      response[i].test_access_code
                    )}
                    action={() => this.openDeleteTacConfirmationPopup(i)}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.SECONDARY}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {
                      LOCALIZE.testAdministration.supervisedTesting.testAccessCodesTable
                        .actionButton
                    }
                  </p>
                </div>
              }
            />
          )
        });

        // populating testAccessCodes
        testAccessCodes.push(response[i]);
      }

      // updating testAccessCodesRowsDefinition object with provided data and needed style
      testAccessCodesRowsDefinition = {
        column_1_style: COMMON_STYLE.LEFT_TEXT,
        column_2_style: COMMON_STYLE.LEFT_TEXT,
        column_3_style: COMMON_STYLE.CENTERED_TEXT,
        data: data
      };

      // updating states
      this.setState({
        testAccessCodesRowsDefinition: testAccessCodesRowsDefinition,
        testAccessCodes: testAccessCodes
      });
    });
  };

  // populating TA assigned candidates table
  populateTaAssignedCandidatesTable = testSessionId => {
    // initializing needed object and array for assignedCandidatesRowsDefinition state (needed for GenericTable component)
    let assignedCandidatesRowsDefinition = {};
    const data = [];
    let assignedCandidates = [];
    let atLeastOneTestToApprove = false;

    this.props.getTestAdministratorAssignedCandidates(testSessionId).then(response => {
      // looping in response given
      for (let i = 0; i < response.length; i++) {
        // candidate of current iteration needs approval
        if (response[i].needs_approval) {
          atLeastOneTestToApprove = true;
        }
        // populating data object with provided data
        data.push({
          column_1: `${response[i].candidate_last_name}, ${response[i].candidate_first_name} (${response[i].candidate_email})`,
          column_2: response[i].candidate_dob,
          column_3: response[i].test_code,
          column_4: getFormattedTestStatus(response[i].status_codename),
          column_5: this.getTotalTestTime(response[i].assigned_test_sections),
          column_6: this.getTestTimeRemaining(response[i].test_time_remaining),
          column_7: this.populateColumnSix(i, response)
        });
      }

      // updating assignedCandidates array with provided data
      assignedCandidates = response;

      // updating assignedCandidatesRowsDefinition object with provided data and needed style
      assignedCandidatesRowsDefinition = {
        column_1_style: COMMON_STYLE.LEFT_TEXT,
        column_2_style: COMMON_STYLE.CENTERED_TEXT,
        column_3_style: COMMON_STYLE.CENTERED_TEXT,
        column_4_style: COMMON_STYLE.CENTERED_TEXT,
        column_5_style: COMMON_STYLE.CENTERED_TEXT,
        column_6_style: COMMON_STYLE.CENTERED_TEXT,
        column_7_style: COMMON_STYLE.CENTERED_TEXT,
        data: data
      };

      // saving results in state
      this.setState({
        assignedCandidatesRowsDefinition: assignedCandidatesRowsDefinition,
        assignedCandidates: assignedCandidates,
        atLeastOneTestToApprove: atLeastOneTestToApprove
      });
    });
  };

  updateTaAssignedCandidatesTable = testSessionId => {
    // initializing variables
    const assignedCandidates = [];
    const newData = [];
    let atLeastOneTestToApprove = false;
    const { assignedCandidatesRowsDefinition } = this.state;
    // getting assigned candidates
    this.props.getTestAdministratorAssignedCandidates(testSessionId).then(response => {
      // populating table data
      for (let i = 0; i < response.length; i++) {
        // candidate of current iteration needs approval
        if (response[i].needs_approval) {
          atLeastOneTestToApprove = true;
        }
        assignedCandidates.push(response[i]);
        newData.push({
          column_1: `${response[i].candidate_last_name}, ${response[i].candidate_first_name} (${response[i].candidate_email})`,
          column_2: response[i].candidate_dob,
          column_3: response[i].test_code,
          column_4: getFormattedTestStatus(response[i].status_codename),
          column_5: this.getTotalTestTime(response[i].assigned_test_sections),
          column_6: this.getTestTimeRemaining(response[i].test_time_remaining),
          column_7: this.populateColumnSix(i, response)
        });
      }
      // rebuilding assignedCandidatesRowsDefinition state
      const newAssignedCandidatesRowsDefinition = {
        column_1_style: assignedCandidatesRowsDefinition.column_1_style,
        column_2_style: assignedCandidatesRowsDefinition.column_2_style,
        column_3_style: assignedCandidatesRowsDefinition.column_3_style,
        column_4_style: assignedCandidatesRowsDefinition.column_4_style,
        column_5_style: assignedCandidatesRowsDefinition.column_5_style,
        column_6_style: assignedCandidatesRowsDefinition.column_6_style,
        column_7_style: COMMON_STYLE.CENTERED_TEXT,
        data: newData
      };
      // saving new data in state + triggering re-render
      this.setState({
        assignedCandidatesRowsDefinition: newAssignedCandidatesRowsDefinition,
        assignedCandidates: assignedCandidates,
        triggerReRender: !this.state.triggerReRender,
        atLeastOneTestToApprove: atLeastOneTestToApprove
      });
    });
  };

  // getting total test time based on all timed test sections
  getTotalTestTime = assignedTestSections => {
    // initializing totalTestTime
    let totalTestTime = 0;
    // looping in all assigned test sections
    for (let i = 0; i < assignedTestSections.length; i++) {
      // test_section_time is not null
      if (assignedTestSections[i].test_section_time !== null) {
        // incrementing totalTestTime
        totalTestTime += assignedTestSections[i].test_section_time;
      }
    }
    // returning formatted test time
    return `${getTimeInHoursMinutes(Number(totalTestTime)).formattedHours} : ${
      getTimeInHoursMinutes(Number(totalTestTime)).formattedMinutes
    }`;
  };

  // getting total test time based on all timed test sections
  getTotalTestTimeInMinutes = assignedTestSections => {
    // initializing totalTestTime
    let totalTestTime = 0;
    // looping in all assigned test sections
    for (let i = 0; i < assignedTestSections.length; i++) {
      // test_section_time is not null
      if (assignedTestSections[i].test_section_time !== null) {
        // incrementing totalTestTime
        totalTestTime += assignedTestSections[i].test_section_time;
      }
    }
    // returning total test time in minutes
    return totalTestTime;
  };

  getTestTimeRemaining = timeRemaining => {
    // divided by 60, since we're receiving time in seconds
    return `${getTimeInHoursMinutes(Number(timeRemaining / 60)).formattedHours} : ${
      getTimeInHoursMinutes(Number(timeRemaining / 60)).formattedMinutes
    }`;
  };

  populateColumnSix = (i, response) => {
    return (
      <div className="d-flex justify-content-center flex-nowrap">
        <StyledTooltip
          id={`approve-candidate-tooltip-${i}`}
          place="top"
          variant={TYPE.light}
          effect={EFFECT.solid}
          openOnClick={false}
          disabled={!response[i].needs_approval}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`approve-candidate-tooltip-${i}`}
                label={
                  <FontAwesomeIcon
                    icon={!response[i].needs_approval ? faThumbsUp : regularFaThumbsUp}
                  />
                }
                action={() => {
                  this.taAction(i, taActions.APPROVE);
                }}
                customStyle={
                  response[i].needs_approval
                    ? styles.actionButton
                    : { ...styles.actionButton, ...styles.disabledActionButton }
                }
                buttonTheme={THEME.SECONDARY}
                disabled={!response[i].needs_approval}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable
                    .approveAriaLabel,
                  response[i].candidate_first_name,
                  `${response[i].candidate_last_name} (${response[i].candidate_email})`,
                  response[i].candidate_dob,
                  response[i].test_code,
                  getFormattedTestStatus(response[i].status_codename),
                  getTimeInHoursMinutes(
                    Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                  ).formattedHours,
                  getTimeInHoursMinutes(
                    Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                  ).formattedMinutes
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable
                    .actionTooltips.approve
                }
              </p>
            </div>
          }
        />
        <StyledTooltip
          id={`unassign-candidate-tooltip-${i}`}
          place="top"
          variant={TYPE.light}
          effect={EFFECT.solid}
          openOnClick={false}
          disabled={
            response[i].status_codename !== TEST_STATUS.CHECKED_IN &&
            response[i].status_codename !== TEST_STATUS.PRE_TEST
          }
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`unassign-candidate-tooltip-${i}`}
                label={<FontAwesomeIcon icon={faTrashAlt} />}
                action={() => {
                  this.taAction(i, taActions.UNASSIGN);
                }}
                customStyle={
                  response[i].status_codename === TEST_STATUS.CHECKED_IN ||
                  response[i].status_codename === TEST_STATUS.PRE_TEST
                    ? styles.actionButton
                    : { ...styles.actionButton, ...styles.disabledActionButton }
                }
                buttonTheme={THEME.SECONDARY}
                disabled={
                  response[i].status_codename !== TEST_STATUS.CHECKED_IN &&
                  response[i].status_codename !== TEST_STATUS.PRE_TEST
                }
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable
                    .unAssignAriaLabel,
                  response[i].candidate_first_name,
                  `${response[i].candidate_last_name} (${response[i].candidate_email})`,
                  response[i].candidate_dob,
                  response[i].test_code,
                  getFormattedTestStatus(response[i].status_codename),
                  getTimeInHoursMinutes(
                    Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                  ).formattedHours,
                  getTimeInHoursMinutes(
                    Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                  ).formattedMinutes
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {
                  LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable
                    .actionTooltips.unAssign
                }
              </p>
            </div>
          }
        />
        <StyledTooltip
          id={`lock-candidate-tooltip-${i}`}
          place="top"
          variant={TYPE.light}
          effect={EFFECT.solid}
          openOnClick={false}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`lock-candidate-tooltip-${i}`}
                label={
                  <FontAwesomeIcon
                    icon={response[i].status_codename === TEST_STATUS.LOCKED ? faLockOpen : faLock}
                  />
                }
                action={() => {
                  this.taAction(i, taActions.LOCK_UNLOCK);
                }}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  response[i].status_codename === TEST_STATUS.LOCKED
                    ? LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable
                        .unlockAriaLabel
                    : LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable
                        .lockAriaLabel,
                  response[i].candidate_first_name,
                  `${response[i].candidate_last_name} (${response[i].candidate_email})`,
                  response[i].candidate_dob,
                  response[i].test_code,
                  getFormattedTestStatus(response[i].status_codename),
                  getTimeInHoursMinutes(
                    Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                  ).formattedHours,
                  getTimeInHoursMinutes(
                    Number(this.getTotalTestTimeInMinutes(response[i].assigned_test_sections))
                  ).formattedMinutes
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>
                {response[i].status_codename === TEST_STATUS.LOCKED
                  ? LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable
                      .actionTooltips.unlock
                  : LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable
                      .actionTooltips.lock}
              </p>
            </div>
          }
        />
      </div>
    );
  };

  getCurrentTableIndex = () => {
    // initializing currentIndex
    let currentIndex = null;
    // looping in TA assigned candidates table to get the index of the current selected candidate
    for (let i = 0; i < this.state.assignedCandidates.length; i++) {
      if (this.state.selectedCandidateData.id === this.state.assignedCandidates[i].id) {
        // updating currentIndex once found
        currentIndex = i;
        break;
      }
    }
    return currentIndex;
  };

  triggerTaActionLoading = index => {
    const { assignedCandidates, assignedCandidatesRowsDefinition } = this.state;
    const tempAssignedCandidatesRowsDefinition = assignedCandidatesRowsDefinition;
    for (let i = 0; i < assignedCandidatesRowsDefinition.data.length; i++) {
      // provided index is the same as the current index
      if (i === index) {
        tempAssignedCandidatesRowsDefinition.data[i].column_7 = (
          // eslint-disable-next-line jsx-a11y/label-has-associated-control
          <label className="fa fa-spinner fa-spin">
            <FontAwesomeIcon icon={faSpinner} />
          </label>
        );
        // other candidates than the one selected
      } else {
        tempAssignedCandidatesRowsDefinition.data[i].column_7 = (
          <div>
            <CustomButton
              label={<FontAwesomeIcon icon={faClock} />}
              customStyle={{
                ...styles.actionButton,
                ...styles.disabledActionButton
              }}
              disabled={true}
            />
            <CustomButton
              label={<FontAwesomeIcon icon={faThumbsUp} />}
              customStyle={{
                ...styles.actionButton,
                ...styles.disabledActionButton
              }}
              disabled={true}
            />
            <CustomButton
              label={<FontAwesomeIcon icon={faTrashAlt} />}
              customStyle={{
                ...styles.actionButton,
                ...styles.disabledActionButton
              }}
              disabled={true}
            />
            <CustomButton
              label={
                <FontAwesomeIcon
                  icon={
                    assignedCandidates[i].status_codename === TEST_STATUS.LOCKED
                      ? faLockOpen
                      : faLock
                  }
                />
              }
              customStyle={{
                ...styles.actionButton,
                ...styles.disabledActionButton
              }}
              disabled={true}
            />
            {/* <CustomButton
              label={<FontAwesomeIcon icon={faEdit} />}
              customStyle={{
                ...styles.actionButton,
                ...styles.disabledActionButton
              }}
              disabled={true}
            /> */}
          </div>
        );
      }
    }
    this.setState({ assignedCandidatesRowsDefinition: tempAssignedCandidatesRowsDefinition });
  };

  taAction = (i, action) => {
    switch (action) {
      case taActions.APPROVE:
        this.handleApproveCandidate(i);
        break;
      case taActions.UNASSIGN:
        this.openUnAssignCandidate(i);
        break;
      case taActions.LOCK_UNLOCK:
        this.openLockUnlockCandidatePopup(i);
        break;
      // no action needed for now
      case taActions.LOCK_UNLOCK_ALL:
        break;
      default:
        break;
    }
  };

  // reset lock popup states + timer state
  resetNeededLockPopupStates = () => {
    this.props.updateTimerState([]);
  };

  onPopupClose = () => {
    // initializing currentTimer redux state to []
    this.props.updateTimerState([]);
  };

  // approve candidate
  handleApproveCandidate = index => {
    const { candidate_user_id } = this.state.assignedCandidates[index];
    const test = this.state.assignedCandidates[index].test_id;

    // approving candidate (updating candidate's test status)
    this.props
      .approveCandidate(candidate_user_id, test, this.state.testSessionSelectedOption.value)
      .then(response => {
        // request successful
        if (response.status === 200) {
          this.triggerTaActionLoading(index);
          // updating TA's assigned candidates table
          this.updateTaAssignedCandidatesTable(this.state.testSessionSelectedOption.value);
          // should never happen
        } else {
          throw new Error("Something went wrong during approve candidate process");
        }
      });
  };

  // un-assign candidate's test
  openUnAssignCandidate = i => {
    this.setState({
      selectedCandidateData: this.state.assignedCandidates[i],
      showUnAssignPopup: true
    });
  };

  closeUnAssignPopup = () => {
    this.setState({ showUnAssignPopup: false, isRightPopupButtonDisabled: false });
  };

  handleUnAssign = () => {
    // disabling right popup button (to avoid multiple clicks)
    this.setState({ isRightPopupButtonDisabled: true }, () => {
      const currentIndex = this.getCurrentTableIndex();
      const { candidate_user_id } = this.state.assignedCandidates[currentIndex];
      const test = this.state.assignedCandidates[currentIndex].test_id;

      this.props
        .unAssignCandidate(candidate_user_id, test, this.state.testSessionSelectedOption.value)
        .then(response => {
          // request successful
          if (response.status === 200) {
            this.triggerTaActionLoading(currentIndex);
            // close un-assign popup
            this.closeUnAssignPopup();
            // updating TA's assigned candidates table
            this.updateTaAssignedCandidatesTable(this.state.testSessionSelectedOption.value);
            // should never happen
          } else {
            throw new Error("Something went wrong during unassign candidate process");
          }
        });
    });
  };

  // lock/unlock candidate
  openLockUnlockCandidatePopup = i => {
    // test is locked
    if (this.state.assignedCandidates[i].status_codename === TEST_STATUS.LOCKED) {
      this.setState({
        showUnlockPopup: true,
        selectedCandidateData: this.state.assignedCandidates[i]
      });
      // test is active
    } else {
      this.setState({
        showLockPopup: true,
        selectedCandidateData: this.state.assignedCandidates[i]
      });
    }
  };

  closeLockPopup = () => {
    this.setState({
      showLockPopup: false,
      isRightPopupButtonDisabled: false
    });
  };

  closeUnlockPopup = () => {
    this.setState({
      showUnlockPopup: false,
      isRightPopupButtonDisabled: false
    });
  };

  handleLockUnlock = () => {
    // disabling right popup button (to avoid multiple clicks)
    this.setState({ isRightPopupButtonDisabled: true }, () => {
      // getting candidate's assigned tests data to get the updated test section id
      this.props
        .getAssignedTests(this.props.currentLanguage, this.state.selectedCandidateData.id)
        .then(assignedTest => {
          const { candidate_user_id } = this.state.selectedCandidateData;
          const test = this.state.selectedCandidateData.test_id;
          const currentTestStatus = this.state.selectedCandidateData.status_codename;
          const testSectionId = assignedTest[0].test_section_id;

          const currentIndex = this.getCurrentTableIndex();

          if (
            currentTestStatus === TEST_STATUS.PRE_TEST ||
            currentTestStatus === TEST_STATUS.ACTIVE ||
            currentTestStatus === TEST_STATUS.TRANSITION ||
            currentTestStatus === TEST_STATUS.CHECKED_IN ||
            currentTestStatus === TEST_STATUS.PAUSED
          ) {
            this.props
              .lockCandidateTest(
                candidate_user_id,
                test,
                testSectionId,
                this.state.testSessionSelectedOption.value
              )
              .then(response => {
                // request successful
                if (response.status === 200) {
                  this.triggerTaActionLoading(currentIndex);
                  // closing popup
                  this.closeLockPopup();
                  this.populateTaAssignedCandidatesTable(
                    this.state.testSessionSelectedOption.value
                  );
                  // should never happen
                } else {
                  throw new Error("Something went wrong during lock candidate process");
                }
              });
          } else if (currentTestStatus === TEST_STATUS.LOCKED) {
            this.props
              .unlockCandidateTest(
                candidate_user_id,
                test,
                testSectionId,
                this.state.testSessionSelectedOption.value
              )
              .then(response => {
                // request successful
                if (response.status === 200) {
                  this.triggerTaActionLoading(currentIndex);
                  // closing popup
                  this.closeUnlockPopup();
                  this.populateTaAssignedCandidatesTable(
                    this.state.testSessionSelectedOption.value
                  );
                  // should never happen
                } else {
                  throw new Error("Something went wrong during unlock candidate process");
                }
              });
          }
        });
    });
  };

  openLockAllPopup = () => {
    this.setState({ showLockAllPopup: true });
  };

  closeLockAllPopup = () => {
    this.setState({ showLockAllPopup: false, isRightPopupButtonDisabled: false });
  };

  openUnlockAllPopup = () => {
    this.setState({ showUnlockAllPopup: true });
  };

  closeUnlockAllPopup = () => {
    this.setState({
      showUnlockAllPopup: false,
      isRightPopupButtonDisabled: false
    });
  };

  openApproveAllPopup = () => {
    this.setState({ showApproveAllPopup: true });
  };

  closeApproveAllPopup = () => {
    this.setState({
      showApproveAllPopup: false,
      isRightPopupButtonDisabled: false
    });
  };

  handleLockUnlockAll = () => {
    // disabling right popup button (to avoid multiple clicks)
    this.setState({ isRightPopupButtonDisabled: true }, () => {
      // unlock all candidates
      if (this.state.allTestsLockedFlag) {
        this.props
          .unlockAllCandidatesTest(this.state.testSessionSelectedOption.value)
          .then(response => {
            // request successful
            if (response.status === 200) {
              // closing popup
              this.closeUnlockAllPopup();
              // re-populating active candidates table
              this.populateTaAssignedCandidatesTable(this.state.testSessionSelectedOption.value);
              // should never happen
            } else {
              throw new Error("Something went wrong during approve all candidates process");
            }
          });
        // lock all candidates
      } else {
        this.props
          .lockAllCandidatesTest(this.state.testSessionSelectedOption.value)
          .then(response => {
            // request successful
            if (response.status === 200) {
              // closing popup
              this.closeLockAllPopup();
              // re-populating active candidates table
              this.populateTaAssignedCandidatesTable(this.state.testSessionSelectedOption.value);
              // should never happen
            } else {
              throw new Error("Something went wrong during lock all candidates process");
            }
          });
      }
    });
  };

  handleApproveAll = () => {
    // disabling right popup button (to avoid multiple clicks) + setting isLoadingApproveAll to true
    this.setState({ isRightPopupButtonDisabled: true, isLoadingApproveAll: true }, () => {
      // building assignedTestIdsOfCandidatesToApprove array
      const assignedTestIdsOfCandidatesToApprove = [];
      for (let i = 0; i < this.state.assignedCandidates.length; i++) {
        if (this.state.assignedCandidates[i].needs_approval) {
          assignedTestIdsOfCandidatesToApprove.push(this.state.assignedCandidates[i].id);
        }
      }
      // approving all candidates
      this.props.approveAllCandidates(assignedTestIdsOfCandidatesToApprove).then(response => {
        // request successful
        if (response.status === 200) {
          // closing popup
          this.closeApproveAllPopup();
          // re-populating active candidates table
          this.populateTaAssignedCandidatesTable(this.state.testSessionSelectedOption.value);
          // removing loading button (adding delay to make sure that the popup is closed before re-enabling the button)
          setTimeout(() => {
            this.setState({ isLoadingApproveAll: false });
          }, 500);
          // should never happen
        } else {
          throw new Error("Something went wrong during approve candidate process");
        }
      });
    });
  };

  // // TODO (fnormand): implement report candidate functionality
  // reportCandidate = i => {
  //   console.log("Report candidate #", i);
  // };

  // updating the test access codes and TA assigned candidates tables
  updateTablesData = () => {
    // test access code table
    this.populateTestAccessCodesTable(this.state.testSessionSelectedOption.value);
    // TA assigned candidates table
    this.populateTaAssignedCandidatesTable(this.state.testSessionSelectedOption.value);
  };

  // Populate Last Row Content (Test Access Codes)
  populateLastTestAccessCodesRowHtml = () => {
    return (
      <td
        id="generate-test-access-code-button"
        colSpan="4"
        style={alternateColorsStyle(this.state.testAccessCodes.length, 60)}
      >
        <CustomButton
          label={
            <>
              <span style={styles.generateCodeButtonIcon}>
                <FontAwesomeIcon icon={faWandSparkles} />
              </span>
              <span>
                {LOCALIZE.testAdministration.supervisedTesting.testAccessCodesTable.generateNewCode}
              </span>
            </>
          }
          action={this.openGenerateTestAccessCodePopup}
          customStyle={styles.generateCodeButton}
        />
      </td>
    );
  };

  // Populate Empty Table with Generate Button (Test Access Codes)
  populateEmptyTestAccessCodesTable = () => {
    return (
      <CustomButton
        label={
          <>
            <span style={styles.generateCodeButtonIcon}>
              <FontAwesomeIcon icon={faWandSparkles} />
            </span>
            <span>
              {LOCALIZE.testAdministration.supervisedTesting.testAccessCodesTable.generateNewCode}
            </span>
          </>
        }
        action={this.openGenerateTestAccessCodePopup}
        customStyle={styles.generateCodeButton}
      />
    );
  };

  handlePrint = () => {
    // triggering the react to print component
    this.setState({ triggerReactToPrint: true }, () => {
      // need to set to false as soon as we have called the ReactToPrint component, so we can re-open it (if needed)
      this.setState({ triggerReactToPrint: false });
    });
  };

  render() {
    // Test Access Codes Table Columns
    const testAccessCodesTableColumnsDefinition = [
      {
        label: LOCALIZE.testAdministration.supervisedTesting.testAccessCodesTable.testAccessCode,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.testAdministration.supervisedTesting.testAccessCodesTable.test,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.testAdministration.supervisedTesting.testAccessCodesTable.action,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    // Assigned Candidates Table Columns
    const assignedCandidatesTableColumnsDefinition = [
      {
        label: LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable.candidate,
        style: { width: "30%" }
      },
      {
        label: LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable.dob,
        style: { ...{ width: "11%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable.testCode,
        style: { ...{ width: "11%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable.status,
        style: { ...{ width: "10%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable.timer,
        style: { ...{ width: "10%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable.timeRemaining,
        style: { ...{ width: "10%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable.actions,
        style: { ...{ width: "18%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    const testSessionAttendeesColumnsDefinition = [
      {
        label:
          LOCALIZE.testAdministration.supervisedTesting.testSessionAttendees.popup.table.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testAdministration.supervisedTesting.testSessionAttendees.popup.table.columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testAdministration.supervisedTesting.testSessionAttendees.popup.table
            .columnThree,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testAdministration.supervisedTesting.testSessionAttendees.popup.table.columnFour,
        style: COMMON_STYLE.LEFT_TEXT
      }
    ];

    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.testAdministration.sideNavItems.supervisedTesting}</h2>
          <p>{LOCALIZE.testAdministration.supervisedTesting.description}</p>
        </div>
        <div style={styles.testSessionContainer}>
          <Row className="align-items-center justify-content-start" role="presentation">
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelContainer}
            >
              <label id="test-session-label">
                {LOCALIZE.testAdministration.supervisedTesting.testSessionLabel}
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <div style={styles.dropdownContainer}>
                <div style={styles.testSessionDropdown}>
                  <DropdownSelect
                    idPrefix="test-session-dropdown"
                    ariaLabelledBy="test-session-label"
                    options={this.state.testSessionOptions}
                    onChange={this.getSelectedTestSession}
                    defaultValue={this.state.testSessionSelectedOption}
                    hasPlaceholder={true}
                    orderByLabels={false}
                  />
                </div>
                <span
                  style={
                    Object.keys(this.state.testSessionSelectedOption).length > 0
                      ? styles.testSessionAttendees
                      : styles.visibilityHidden
                  }
                >
                  <StyledTooltip
                    id="test-sessions-view-attendees"
                    place="top"
                    variant={TYPE.light}
                    effect={EFFECT.solid}
                    openOnClick={false}
                    tooltipElement={
                      <div style={styles.allUnset}>
                        <CustomButton
                          dataTip=""
                          dataFor="test-sessions-view-attendees"
                          label={
                            <>
                              <FontAwesomeIcon
                                icon={faListCheck}
                                style={styles.buttonIconTooltip}
                              />
                            </>
                          }
                          action={() => this.openTestSessionAttendeesPopup()}
                          customStyle={styles.actionButton}
                          buttonTheme={THEME.PRIMARY}
                          ariaLabel={
                            LOCALIZE.testAdministration.supervisedTesting.testSessionAttendees
                              .viewAttendeesTooltip
                          }
                          // no candidate reserved this test session yet ==> disable button
                          disabled={this.state.viewAttendeesButtonDisabled}
                        />
                      </div>
                    }
                    tooltipContent={
                      <div>
                        <p>
                          {
                            LOCALIZE.testAdministration.supervisedTesting.testSessionAttendees
                              .viewAttendeesTooltip
                          }
                        </p>
                      </div>
                    }
                  />
                  {this.state.testSessionSelectedOption.user_accommodation_file_id !== null && (
                    <StyledTooltip
                      id="test-sessions-view-accommodation-details-popup"
                      place="top"
                      variant={TYPE.light}
                      effect={EFFECT.solid}
                      openOnClick={false}
                      tooltipElement={
                        <div style={styles.allUnset}>
                          <CustomButton
                            dataTip=""
                            dataFor="test-sessions-view-accommodation-details-popup"
                            label={
                              <>
                                <FontAwesomeIcon
                                  icon={
                                    this.state.testSessionSelectedOption.is_alternate_test_request
                                      ? faFileCircleExclamation
                                      : faUniversalAccess
                                  }
                                  style={styles.buttonIconTooltip}
                                />
                              </>
                            }
                            action={() =>
                              this.openAccommodationsRequestDetailsPopup(
                                this.state.testSessionSelectedOption
                              )
                            }
                            customStyle={styles.actionButton}
                            buttonTheme={THEME.PRIMARY}
                            ariaLabel={LOCALIZE.formatString(
                              this.state.testSessionSelectedOption.is_alternate_test_request
                                ? LOCALIZE.accommodationsRequestDetailsPopup
                                    .viewAlternateTestRequestDetailsAccessibilityLabel
                                : LOCALIZE.accommodationsRequestDetailsPopup
                                    .viewAccommodationsRequestDetailsAccessibilityLabel,
                              `${
                                this.state.testSessionSelectedOption[
                                  `test_name_${this.props.currentLanguage}`
                                ]
                              } - v${this.state.testSessionSelectedOption.version}`
                            )}
                          />
                        </div>
                      }
                      tooltipContent={
                        <div>
                          <p>
                            {this.state.testSessionSelectedOption.is_alternate_test_request
                              ? LOCALIZE.accommodationsRequestDetailsPopup
                                  .viewAlternateTestRequestDetails
                              : LOCALIZE.accommodationsRequestDetailsPopup
                                  .viewAccommodationsRequestDetails}
                          </p>
                        </div>
                      }
                    />
                  )}
                </span>
              </div>
            </Col>
          </Row>
        </div>
        {Object.keys(this.state.testSessionSelectedOption).length > 0 && (
          <div>
            <div>
              <p style={styles.testAccessCodesTitleLabel}>
                {LOCALIZE.testAdministration.supervisedTesting.testSessionTestAccessCodes}
              </p>
              <GenericTable
                classnamePrefix="test-access-codes"
                columnsDefinition={testAccessCodesTableColumnsDefinition}
                rowsDefinition={this.state.testAccessCodesRowsDefinition}
                // we don't want a loading icon here, because the table is updated every 5 seconds
                currentlyLoading={false}
                emptyTableMessage={this.populateEmptyTestAccessCodesTable()}
                hasLastRowHtml={this.state.testAccessCodes.length > 0}
                lastRowHtml={this.populateLastTestAccessCodesRowHtml()}
              />
            </div>
            <div style={styles.assignedCandidatesContainer}>
              <div style={styles.activeCandidatesTableTitleContainer}>
                <div style={styles.activeCandidatesTableTitleLabel}>
                  <p style={styles.boldText}>
                    {LOCALIZE.testAdministration.supervisedTesting.testSessionAssignedCandidates}
                  </p>
                </div>
                <div style={styles.activeCandidatesTableTitleButton}>
                  <span style={styles.approveAllButtonContainer}>
                    <CustomButton
                      buttonId="approve-all-button-id"
                      label={
                        <>
                          <span>
                            <FontAwesomeIcon icon={regularFaThumbsUp} style={styles.buttonIcon} />
                          </span>
                          <span>
                            {LOCALIZE.testAdministration.supervisedTesting.approveAllButton}
                          </span>
                        </>
                      }
                      action={this.openApproveAllPopup}
                      buttonTheme={THEME.SECONDARY}
                      disabled={this.props.currentlyLoading || !this.state.atLeastOneTestToApprove}
                    />
                  </span>
                  <span>
                    <CustomButton
                      buttonId="lock-unlock-all-button-id"
                      label={
                        <>
                          <span>
                            <FontAwesomeIcon
                              icon={this.state.allTestsLockedFlag ? faLockOpen : faLock}
                              style={styles.buttonIcon}
                            />
                          </span>
                          <span>
                            {!this.state.atLeastOneTestToLockUnlock
                              ? LOCALIZE.testAdministration.supervisedTesting.lockAllButton
                              : this.state.allTestsLockedFlag
                                ? LOCALIZE.testAdministration.supervisedTesting.unlockAllButton
                                : LOCALIZE.testAdministration.supervisedTesting.lockAllButton}
                          </span>
                        </>
                      }
                      action={
                        this.state.allTestsLockedFlag
                          ? this.openUnlockAllPopup
                          : this.openLockAllPopup
                      }
                      buttonTheme={THEME.SECONDARY}
                      disabled={
                        this.props.currentlyLoading || !this.state.atLeastOneTestToLockUnlock
                      }
                    />
                  </span>
                </div>
              </div>
              <GenericTable
                classnamePrefix="test-access-codes"
                columnsDefinition={assignedCandidatesTableColumnsDefinition}
                rowsDefinition={this.state.assignedCandidatesRowsDefinition}
                currentlyLoading={this.state.isLoadingAssignedCandidates}
                emptyTableMessage={
                  LOCALIZE.testAdministration.supervisedTesting.assignedCandidatesTable
                    .noActiveCandidates
                }
              />
            </div>
          </div>
        )}
        <PopupBox
          show={this.state.showGenerateTestAccessCodePopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          onPopupClose={this.resetInputs}
          onPopupOpen={() => this.populateTestToAdministerOptions()}
          title={LOCALIZE.testAdministration.supervisedTesting.generateTestAccessCodePopup.title}
          description={
            <TestSessionInformationPopup
              testSessionSelectedOption={this.state.testSessionSelectedOption}
              testToAdministerOptions={this.state.testToAdministerOptions}
              isLoadingTestToAdministerOptions={this.state.isLoadingTestToAdministerOptions}
              showCannotGenerateTestAccessCodeError={
                this.state.showCannotGenerateTestAccessCodeError
              }
            />
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeGenerateTestAccessCodePopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testAdministration.supervisedTesting.generateTestAccessCodePopup.generateButton
          }
          rightButtonIcon={faWandSparkles}
          rightButtonState={this.props.generateButtonDisabled}
          rightButtonAction={this.handleGenerateTestAccessCode}
          customModalStyle={{ width: "100%" }}
        />
        <PopupBox
          show={this.state.showDeleteTacConfirmationPopup}
          handleClose={this.closeDeleteTacConfirmationPopup}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          size={"lg"}
          title={
            LOCALIZE.testAdministration.supervisedTesting.disableTestAccessCodeConfirmationPopup
              .title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.testAdministration.supervisedTesting
                        .disableTestAccessCodeConfirmationPopup.description,
                      <span style={ActivePermissionsStyles.boldText}>
                        {this.state.testAccessCodeToDelete}
                      </span>
                    )}
                  </p>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeDeleteTacConfirmationPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={
            LOCALIZE.testAdministration.supervisedTesting.testAccessCodesTable.actionButton
          }
          rightButtonIcon={faTrashAlt}
          rightButtonIconCustomStyle={styles.customPopupButtonStyle}
          rightButtonAction={this.handleDeleteTestAccessCode}
        />
        {Object.keys(this.state.selectedCandidateData).length > 0 && (
          <div>
            <PopupBox
              show={this.state.showUnAssignPopup}
              handleClose={() => {}}
              isBackdropStatic={true}
              shouldCloseOnEsc={false}
              title={LOCALIZE.testAdministration.supervisedTesting.unAssignPopup.title}
              size="lg"
              description={
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.error}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p>
                        {LOCALIZE.formatString(
                          LOCALIZE.testAdministration.supervisedTesting.unAssignPopup.description,
                          <span style={styles.boldText}>
                            {this.state.selectedCandidateData.candidate_first_name}
                          </span>,
                          <span style={styles.boldText}>
                            {this.state.selectedCandidateData.candidate_last_name}
                          </span>
                        )}
                      </p>
                    }
                  />
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonIcon={faTimes}
              leftButtonAction={this.closeUnAssignPopup}
              rightButtonType={BUTTON_TYPE.danger}
              rightButtonTitle={LOCALIZE.testAdministration.supervisedTesting.unAssignPopup.confirm}
              rightButtonIcon={faTrashAlt}
              rightButtonAction={this.handleUnAssign}
              rightButtonState={
                this.state.isRightPopupButtonDisabled ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
              }
            />
            <PopupBox
              show={this.state.showLockPopup}
              handleClose={() => {}}
              isBackdropStatic={true}
              shouldCloseOnEsc={false}
              onPopupClose={this.resetNeededLockPopupStates}
              title={LOCALIZE.testAdministration.supervisedTesting.lockPopup.title}
              size="lg"
              description={
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <p>
                        {LOCALIZE.formatString(
                          LOCALIZE.testAdministration.supervisedTesting.lockPopup.description1,
                          <span style={styles.boldText}>
                            {this.state.selectedCandidateData.candidate_first_name}
                          </span>,
                          <span style={styles.boldText}>
                            {this.state.selectedCandidateData.candidate_last_name}
                          </span>
                        )}
                      </p>
                    }
                  />
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonLabel={LOCALIZE.commons.cancel}
              leftButtonCustomStyle={{ minWidth: 150 }}
              leftButtonIcon={faTimes}
              leftButtonAction={this.closeLockPopup}
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={LOCALIZE.testAdministration.supervisedTesting.lockPopup.lockButton}
              rightButtonLabel={LOCALIZE.testAdministration.supervisedTesting.lockPopup.lockButton}
              rightButtonIcon={faLock}
              rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
              rightButtonAction={this.handleLockUnlock}
            />
            <PopupBox
              show={this.state.showUnlockPopup}
              handleClose={() => {}}
              isBackdropStatic={true}
              shouldCloseOnEsc={false}
              title={LOCALIZE.testAdministration.supervisedTesting.unlockPopup.title}
              size="lg"
              description={
                <div>
                  <SystemMessage
                    messageType={MESSAGE_TYPE.warning}
                    title={LOCALIZE.commons.warning}
                    message={
                      <>
                        <p>
                          {LOCALIZE.formatString(
                            LOCALIZE.testAdministration.supervisedTesting.unlockPopup.description1,
                            <span style={styles.boldText}>
                              {this.state.selectedCandidateData.candidate_first_name}
                            </span>,
                            <span style={styles.boldText}>
                              {this.state.selectedCandidateData.candidate_last_name}
                            </span>
                          )}
                        </p>
                        <p>
                          {LOCALIZE.testAdministration.supervisedTesting.unlockPopup.description2}
                        </p>
                        <p>
                          {LOCALIZE.testAdministration.supervisedTesting.unlockPopup.description3}
                        </p>
                      </>
                    }
                  />
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonCustomStyle={{ minWidth: 150 }}
              leftButtonIcon={faTimes}
              leftButtonAction={this.closeUnlockPopup}
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={
                LOCALIZE.testAdministration.supervisedTesting.unlockPopup.unlockButton
              }
              rightButtonIcon={faLockOpen}
              rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
              rightButtonAction={this.handleLockUnlock}
              rightButtonState={
                this.state.isRightPopupButtonDisabled ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
              }
            />
          </div>
        )}
        <PopupBox
          show={this.state.showLockAllPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          title={LOCALIZE.testAdministration.supervisedTesting.lockAllPopup.title}
          size="lg"
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <>
                    <p>{LOCALIZE.testAdministration.supervisedTesting.lockAllPopup.description1}</p>
                    <p>{LOCALIZE.testAdministration.supervisedTesting.lockAllPopup.description2}</p>
                    <p>{LOCALIZE.testAdministration.supervisedTesting.lockAllPopup.description3}</p>
                  </>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonCustomStyle={{ minWidth: 150 }}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeLockAllPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testAdministration.supervisedTesting.lockAllPopup.lockTestsButton
          }
          rightButtonIcon={faLock}
          rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
          rightButtonAction={this.handleLockUnlockAll}
          rightButtonState={
            this.state.isRightPopupButtonDisabled ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
        />
        <PopupBox
          show={this.state.showUnlockAllPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          title={LOCALIZE.testAdministration.supervisedTesting.unlockAllPopup.title}
          size="lg"
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <>
                    <p>
                      {LOCALIZE.testAdministration.supervisedTesting.unlockAllPopup.description1}
                    </p>
                    <p>
                      {LOCALIZE.testAdministration.supervisedTesting.unlockAllPopup.description2}
                    </p>
                    <p>
                      {LOCALIZE.testAdministration.supervisedTesting.unlockAllPopup.description3}
                    </p>
                  </>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonCustomStyle={{ minWidth: 150 }}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeUnlockAllPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testAdministration.supervisedTesting.unlockAllPopup.unlockTestsButton
          }
          rightButtonIcon={faLockOpen}
          rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
          rightButtonAction={this.handleLockUnlockAll}
          rightButtonState={
            this.state.isRightPopupButtonDisabled ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
        />
        <PopupBox
          show={this.state.showApproveAllPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          title={LOCALIZE.testAdministration.supervisedTesting.approveAllPopup.title}
          size="lg"
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <>
                    <p>
                      {LOCALIZE.testAdministration.supervisedTesting.approveAllPopup.description}
                    </p>
                  </>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonCustomStyle={{ minWidth: 150 }}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeApproveAllPopup}
          leftButtonState={
            this.state.isLoadingApproveAll ? BUTTON_STATE.disabled : BUTTON_STATE.enabled
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            !this.state.isLoadingApproveAll ? (
              LOCALIZE.testAdministration.supervisedTesting.approveAllPopup.approveAllButton
            ) : (
              // eslint-disable-next-line jsx-a11y/label-has-associated-control
              <label className="fa fa-spinner fa-spin">
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            )
          }
          rightButtonCustomStyle={{ minWidth: 150 }}
          rightButtonIcon={!this.state.isLoadingApproveAll ? faThumbsUp : ""}
          rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
          rightButtonAction={this.handleApproveAll}
          rightButtonState={
            this.state.isRightPopupButtonDisabled || this.state.isLoadingApproveAll
              ? BUTTON_STATE.disabled
              : BUTTON_STATE.enabled
          }
        />
        {Object.keys(this.state.testSessionSelectedOption).length > 0 && (
          <PopupBox
            show={this.state.showTestSessionAttendeesPopup}
            handleClose={() => {}}
            title={LOCALIZE.testAdministration.supervisedTesting.testSessionAttendees.popup.title}
            description={
              <div>
                <div>
                  <ul>
                    <li>
                      {LOCALIZE.formatString(
                        LOCALIZE.testAdministration.supervisedTesting.testSessionAttendees.popup
                          .description1,
                        <span style={styles.boldText}>
                          {this.state.testSessionSelectedOption.formatted_date}
                        </span>
                      )}
                    </li>
                    <li>
                      {LOCALIZE.formatString(
                        LOCALIZE.testAdministration.supervisedTesting.testSessionAttendees.popup
                          .description2,
                        <span style={styles.boldText}>
                          {this.state.testSessionSelectedOption.room_name}
                        </span>
                      )}
                    </li>
                    <li>
                      {LOCALIZE.formatString(
                        LOCALIZE.testAdministration.supervisedTesting.testSessionAttendees.popup
                          .description3,
                        <span style={styles.boldText}>
                          {this.state.testSessionSelectedOption.formatted_test_skill}
                        </span>
                      )}
                    </li>
                  </ul>
                </div>
                <div>
                  <GenericTable
                    classnamePrefix="test-session-attendees"
                    columnsDefinition={testSessionAttendeesColumnsDefinition}
                    rowsDefinition={this.state.testSessionAttendeesRowsDefinition}
                    emptyTableMessage={
                      LOCALIZE.testAdministration.supervisedTesting.testSessionAttendees.popup.table
                        .noData
                    }
                    currentlyLoading={this.state.testSessionAttendeesLoading}
                  />
                </div>
                {this.state.triggerReactToPrint && (
                  <ReactToPrint
                    contentRef={this.componentToPrintRef}
                    documentTitle={LOCALIZE.formatString(
                      LOCALIZE.formatString(
                        LOCALIZE.testAdministration.supervisedTesting.testSessionAttendees.popup
                          .printDocumentTitle,
                        this.state.testSessionSelectedOption.formatted_date
                      )
                    )}
                    content={getContentToPrint(
                      testSessionAttendeesColumnsDefinition,
                      this.state.testSessionSelectedOption,
                      this.state.testSessionAttendeesRowsDefinition
                    )}
                  />
                )}
              </div>
            }
            leftButtonType={BUTTON_TYPE.secondary}
            leftButtonTitle={
              LOCALIZE.testAdministration.supervisedTesting.testSessionAttendees.popup.printButton
            }
            leftButtonIcon={faPrint}
            leftButtonAction={this.handlePrint}
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={LOCALIZE.commons.close}
            rightButtonIcon={faTimes}
            rightButtonAction={this.closeTestSessionAttendeesPopup}
          />
        )}
        {Object.keys(this.state.selectedAccommodationRequestData).length > 0 &&
          this.state.showAccommodationsRequestDetailsPopup && (
            <AccommodationsRequestDetailsPopup
              source={ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.ta}
              userAccommodationFileId={
                this.state.selectedAccommodationRequestData.user_accommodation_file_id
              }
              selectedUserAccommodationFileData={this.state.selectedAccommodationRequestData}
              showPopup={this.state.showAccommodationsRequestDetailsPopup}
              closeAccommodationsRequestDetailsPopup={this.closeAccommodationsRequestDetailsPopup}
              // at this point, the report should be completed, so no draft view for this one
              isDraft={false}
            />
          )}
      </div>
    );
  }
}

export { SupervisedTesting as unconnectedSupervisedTesting };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    currentLanguage: state.localize.language,
    username: state.user.username,
    currentTimer: state.timer.currentTimer,
    generateButtonDisabled: state.testAdministration.generateButtonDisabled,
    testSessionOfficers: state.testAdministration.testSessionOfficers,
    testToAdminister: state.testAdministration.testToAdminister
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      deleteTestAccessCode,
      resetTestAdministrationStates,
      getNewTestAccessCode,
      getTaRelatedTestCenterTestSessions,
      getActiveTestAccessCodesRedux,
      getTestSessionAndTaRelatedTestToAdministerOptions,
      getTestAdministratorAssignedCandidates,
      unAssignCandidate,
      approveCandidate,
      approveAllCandidates,
      getAssignedTests,
      lockCandidateTest,
      unlockCandidateTest,
      lockAllCandidatesTest,
      unlockAllCandidatesTest,
      setDefaultTimes,
      updateTimerState,
      getTestSessionAttendees
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SupervisedTesting);
