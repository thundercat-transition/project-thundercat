import LOCALIZE from "../../text_resources";
import TEST_STATUS from "./Constants";

// getting formatted test statuses
function getFormattedTestStatus(statusCodename) {
  // see 'assigned_test_status.py' for references
  if (statusCodename === TEST_STATUS.CHECKED_IN) {
    return LOCALIZE.commons.status.checkedIn;
  }
  if (statusCodename === TEST_STATUS.PRE_TEST) {
    return LOCALIZE.commons.status.preTest;
  }
  if (statusCodename === TEST_STATUS.ACTIVE) {
    return LOCALIZE.commons.status.active;
  }
  if (statusCodename === TEST_STATUS.TRANSITION) {
    return LOCALIZE.commons.status.transition;
  }
  if (statusCodename === TEST_STATUS.LOCKED) {
    return LOCALIZE.commons.status.locked;
  }
  if (statusCodename === TEST_STATUS.PAUSED) {
    return LOCALIZE.commons.status.paused;
  }
  if (statusCodename === TEST_STATUS.NEVER_STARTED) {
    return LOCALIZE.commons.status.neverStarted;
  }
  if (statusCodename === TEST_STATUS.INACTIVITY) {
    return LOCALIZE.commons.status.inactivity;
  }
  if (statusCodename === TEST_STATUS.TIMED_OUT) {
    return LOCALIZE.commons.status.timedOut;
  }
  if (statusCodename === TEST_STATUS.QUIT) {
    return LOCALIZE.commons.status.quit;
  }
  if (statusCodename === TEST_STATUS.SUBMITTED) {
    return LOCALIZE.commons.status.submitted;
  }
  if (statusCodename === TEST_STATUS.UNASSIGNED) {
    return LOCALIZE.commons.status.unassigned;
  }
  // should never happen
  return "non defined status";
}

export default getFormattedTestStatus;
