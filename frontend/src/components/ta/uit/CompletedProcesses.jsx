import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../../text_resources";
import { connect } from "react-redux";
import * as _ from "lodash";
import { bindActionCreators } from "redux";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import {
  getAllCompletedUITProcessesForTA,
  updateUITCompletedPaginationPage,
  updateUITCompletedPaginationPageSize,
  updateSearchUitCompletedStates,
  getFoundCompletedUITProcessesForTA,
  getSelectedUITCompletedProcessesDetails,
  resetUitStates
} from "../../../modules/UitRedux";
import StyledTooltip, { TYPE, EFFECT } from "../../authentication/StyledTooltip";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBinoculars,
  faTimes,
  faFileCircleExclamation,
  faUniversalAccess
} from "@fortawesome/free-solid-svg-icons";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import Pagination from "../../commons/Pagination";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import { getUitTestStatus } from "../Constants";
import { LANGUAGES } from "../../commons/Translation";
import AccommodationsRequestDetailsPopup, {
  ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE
} from "../../aae/AccommodationsRequestDetailsPopup";
import { getAccommodationFileDataForDetailsPopup } from "../../../modules/AaeRedux";

const styles = {
  allUnset: {
    all: "unset"
  },
  faBinoculars: {
    width: "100%",
    minHeight: 20,
    textAlign: "center"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  popupTableLabel: {
    padding: "12px 0 24px 0"
  },
  tableIcon: {
    transform: "scale(1.2)"
  },
  accommodationTooltipButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "0px 3px",
    margin: "0 3px",
    color: "#00565e"
  }
};

class CompletedProcesses extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    getAllCompletedUITProcessesForTA: PropTypes.func,
    updateUITCompletedPaginationPage: PropTypes.func,
    updateUITCompletedPaginationPageSize: PropTypes.func,
    updateSearchUitCompletedStates: PropTypes.func
  };

  state = {
    rowsDefinition: {},
    currentlyLoading: false,
    allCompletedProcesses: [],
    nextPageNumber: 0,
    previousPageNumber: 0,
    numberOfPages: 1,
    resultsFound: 0,
    callingGetFoundCompletedUIT: false,
    displayResultsFound: false,
    showCompletedProcessDetailsPopup: false,
    completedProcessDetails: {},
    selectedCompletedProcessesDetails: {},
    rowsDefinitionPopup: {},
    currentlyLoadingForPopUp: false,
    selectedAccommodationRequestData: {}
  };

  componentDidMount = () => {
    // resetting redux states
    this.props.resetUitStates();
    // initialize pagination page redux state
    this.props.updateUITCompletedPaginationPage(1);
    this.populateCompletedProcesses();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateUitCompletedBasedOnActiveSearch();
    }
    // if uitCompletedPaginationPage gets updated
    if (prevProps.uitCompletedPaginationPage !== this.props.uitCompletedPaginationPage) {
      this.populateUitCompletedBasedOnActiveSearch();
    }
    // if uitCompletedPaginationPageSize get updated
    if (prevProps.uitCompletedPaginationPageSize !== this.props.uitCompletedPaginationPageSize) {
      this.populateUitCompletedBasedOnActiveSearch();
    }
    // if search uitCompletedKeyword gets updated
    if (prevProps.uitCompletedKeyword !== this.props.uitCompletedKeyword) {
      // if uitCompletedKeyword redux state is empty
      if (this.props.uitCompletedKeyword !== "") {
        this.populateUitCompletedBasedOnActiveSearch();
      } else if (this.props.uitCompletedKeyword === "" && this.props.uitCompletedActiveSearch) {
        this.populateUitCompletedBasedOnActiveSearch();
      }
    }
    // if uitCompletedActiveSearch gets updated
    if (prevProps.uitCompletedActiveSearch !== this.props.uitCompletedActiveSearch) {
      this.populateUitCompletedBasedOnActiveSearch();
    }
    // if triggerUitTablesRerender gets updated
    if (prevProps.triggerUitTablesRerender !== this.props.triggerUitTablesRerender) {
      this.populateUitCompletedBasedOnActiveSearch();
    }
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  populateCompletedProcesses = () => {
    this._isMounted = true;
    const allCompletedProcessesArray = [];
    this.setState({ currentlyLoading: true }, () => {
      this.props
        .getAllCompletedUITProcessesForTA(
          this.props.uitCompletedPaginationPage,
          this.props.uitCompletedPaginationPageSize
        )
        .then(response => {
          if (this._isMounted) {
            this.populateCompletedProcessesObject(allCompletedProcessesArray, response);
          }
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({
              currentlyLoading: false
            });
          }
        });
    });
  };

  populateCompletedProcessesObject = (allCompletedProcessesArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in allCompletedProcessesArray
        allCompletedProcessesArray.push({
          id: currentResult.id,
          process_number: currentResult.assessment_process_or_reference_nbr,
          test: currentResult.test_code,
          total_candidates: currentResult.total_number_of_utac,
          total_tests_taken: currentResult.number_of_tests_taken,
          validity_end_date: currentResult.validity_end_date
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult.assessment_process_or_reference_nbr,
          column_2: currentResult.test_code,
          column_3: `${currentResult.number_of_tests_taken}/${currentResult.total_number_of_utac}`,
          column_4:
            this.props.currentLanguage === LANGUAGES.english
              ? `${currentResult.dept_edesc} (${currentResult.dept_eabrv})`
              : `${currentResult.dept_fdesc} (${currentResult.dept_fabrv})`,
          column_5: currentResult.validity_end_date,
          column_6: this.populateColumnSix(i, currentResult)
        });

        // updating rowsDefinition object with provided data and needed style
        rowsDefinition = {
          column_1_style: COMMON_STYLE.LEFT_TEXT,
          column_2_style: COMMON_STYLE.CENTERED_TEXT,
          column_3_style: COMMON_STYLE.CENTERED_TEXT,
          column_4_style: COMMON_STYLE.CENTERED_TEXT,
          column_5_style: COMMON_STYLE.CENTERED_TEXT,
          column_6_style: COMMON_STYLE.CENTERED_TEXT,
          data: data
        };

        // saving results in state
        this.setState({
          rowsDefinition: rowsDefinition,
          allCompletedProcesses: allCompletedProcessesArray,
          nextPageNumber: response.next_page_number,
          previousPageNumber: response.previous_page_number,
          results_found: response.results.count
        });
      }
    }
    this.setState({
      numberOfPages: Math.ceil(response.count / this.props.uitCompletedPaginationPageSize)
    });
  };

  populateColumnSix = (i, response) => {
    return (
      <div>
        <StyledTooltip
          id={`view-selected-completed-processes-${i}`}
          place="top"
          variant={TYPE.light}
          effect={EFFECT.solid}
          openOnClick={false}
          tooltipElement={
            <div style={styles.allUnset}>
              <CustomButton
                dataTip=""
                dataFor={`view-selected-completed-processes-${i}`}
                label={<FontAwesomeIcon icon={faBinoculars} style={styles.faBinoculars} />}
                action={() => this.openCompletedProcessDetailsPopup(i)}
                customStyle={styles.actionButton}
                buttonTheme={THEME.SECONDARY}
                ariaLabel={LOCALIZE.formatString(
                  LOCALIZE.testAdministration.uit.tabs.completedProcessess.table
                    .viewButtonAccessibility,
                  response.assessment_process_or_reference_nbr
                )}
              />
            </div>
          }
          tooltipContent={
            <div>
              <p>{LOCALIZE.testAdministration.uit.tabs.completedProcessess.table.actionTooltip}</p>
            </div>
          }
        />
      </div>
    );
  };

  populateUitCompletedBasedOnActiveSearch = () => {
    // current search
    if (this.props.uitCompletedActiveSearch) {
      this.populateFoundCompletedProcesses();
      // no search
    } else {
      this.populateCompletedProcesses();
    }
  };

  populateFoundCompletedProcesses = () => {
    this.setState(
      { currentlyLoading: true },
      () => {
        // adding small delay to avoi calling that API multiple times
        setTimeout(() => {
          // if callingGetFoundCompletedUIT is set to false
          if (!this.state.callingGetFoundCompletedUIT) {
            // updating callingGetFoundCompletedUIT to true (while the API is being called)
            this.setState({ callingGetFoundCompletedUIT: true });
            const allCompletedProcessesArray = [];
            this.props
              .getFoundCompletedUITProcessesForTA(
                this.props.currentLanguage,
                this.props.uitCompletedKeyword,
                this.props.uitCompletedPaginationPage,
                this.props.uitCompletedPaginationPageSize
              )
              .then(response => {
                // there are no results found
                if (response[0] === "no results found" || response.results.length <= 0) {
                  this.setState({
                    allCompletedProcesses: [],
                    rowsDefinition: {},
                    numberOfPages: 1,
                    nextPageNumber: 0,
                    previousPageNumber: 0,
                    resultsFound: 0
                  });
                  // there is at least one result found
                } else {
                  this.populateCompletedProcessesObject(allCompletedProcessesArray, response);
                  this.setState({ resultsFound: response.count });
                }
              })
              .then(() => {
                this.setState(
                  {
                    currentlyLoading: false,
                    displayResultsFound: true,
                    callingGetFoundCompletedUIT: false
                  },
                  () => {
                    // make sure that this element exsits to avoid any error
                    if (document.getElementById("completed-processes-results-found")) {
                      document.getElementById("completed-processes-results-found").focus();
                    }
                  }
                );
              });
          }
        });
      },
      100
    );
  };

  populateSelectedCompletedProcessDetailsTable = () => {
    let rowsDefinitionPopup = {};
    const data = [];
    const selectedCompletedProcessesArray = [];
    this.setState({ currentlyLoadingForPopUp: true }, () => {
      this.props
        .getSelectedUITCompletedProcessesDetails(this.state.completedProcessDetails.id)
        .then(response => {
          for (let i = 0; i < response.length; i++) {
            selectedCompletedProcessesArray.push({
              candidate_email: response[i].email,
              test_status: response[i].uit_test_status
            });
            // pushing needed data in data array (needed for GenericTable component)
            data.push({
              column_1: response[i].email,
              column_2: (
                <>
                  <span>{getUitTestStatus(response[i].uit_test_status)}</span>
                  {response[i].user_accommodation_file_id !== null && (
                    <StyledTooltip
                      id={`completed-processes-view-accommodation-details-row-${i}`}
                      place="top"
                      variant={TYPE.light}
                      effect={EFFECT.solid}
                      openOnClick={false}
                      tooltipElement={
                        <div style={styles.allUnset}>
                          <CustomButton
                            dataTip=""
                            dataFor={`completed-processes-view-accommodation-details-row-${i}`}
                            label={
                              <>
                                <FontAwesomeIcon
                                  icon={
                                    response[i].is_alternate_test_request
                                      ? faFileCircleExclamation
                                      : faUniversalAccess
                                  }
                                  style={styles.tableIcon}
                                />
                              </>
                            }
                            action={() =>
                              this.openAccommodationsRequestDetailsPopup(
                                response[i].user_accommodation_file_id,
                                response[i].user_id
                              )
                            }
                            customStyle={styles.accommodationTooltipButton}
                            buttonTheme={THEME.PRIMARY}
                            ariaLabel={LOCALIZE.formatString(
                              data.is_alternate_test_request
                                ? LOCALIZE.accommodationsRequestDetailsPopup
                                    .viewAlternateTestRequestDetailsAccessibilityLabelOf
                                : LOCALIZE.accommodationsRequestDetailsPopup
                                    .viewAccommodationsRequestDetailsAccessibilityLabelOf,
                              response[i].email
                            )}
                          />
                        </div>
                      }
                      tooltipContent={
                        <div>
                          <p>
                            {data.is_alternate_test_request
                              ? LOCALIZE.accommodationsRequestDetailsPopup
                                  .viewAlternateTestRequestDetails
                              : LOCALIZE.accommodationsRequestDetailsPopup
                                  .viewAccommodationsRequestDetails}
                          </p>
                        </div>
                      }
                    />
                  )}
                </>
              )
            });

            // updating rowsDefinition object with provided data and needed style
            rowsDefinitionPopup = {
              column_1_style: COMMON_STYLE.LEFT_TEXT,
              column_2_style: COMMON_STYLE.LEFT_TEXT,
              data: data
            };

            // saving results in state
            this.setState({
              rowsDefinitionPopup: rowsDefinitionPopup,
              selectedCompletedProcessesDetails: selectedCompletedProcessesArray
            });
          }
        })
        .then(() => {
          this.setState({ currentlyLoadingForPopUp: false });
        });
    });
  };

  openCompletedProcessDetailsPopup = index => {
    this.setState(
      {
        showCompletedProcessDetailsPopup: true,
        completedProcessDetails: this.state.allCompletedProcesses[index]
      },
      () => {
        this.populateSelectedCompletedProcessDetailsTable();
      }
    );
  };

  closeCompletedProcessDetailsPopup = () => {
    this.setState({ showCompletedProcessDetailsPopup: false, completedProcessDetails: {} });
  };

  openAccommodationsRequestDetailsPopup = (userAccommodationFileId, userId) => {
    // getting accommodation file data
    this.props
      .getAccommodationFileDataForDetailsPopup(userAccommodationFileId, false, null, userId)
      .then(response => {
        // updating needed states
        this.setState({
          showAccommodationsRequestDetailsPopup: true,
          selectedAccommodationRequestData: response
        });
      });
  };

  closeAccommodationsRequestDetailsPopup = () => {
    this.setState({
      showAccommodationsRequestDetailsPopup: false,
      selectedAccommodationRequestData: {}
    });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.testAdministration.uit.tabs.completedProcessess.table.process,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.completedProcessess.table.test,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.completedProcessess.table.totalCandidates,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.completedProcessess.table.testsTaken,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.completedProcessess.table.endDate,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.completedProcessess.table.actions,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    const columnsDefinitionPopup = [
      {
        label: LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup.columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      }
    ];

    return (
      <div>
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"completed-processes"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchUitCompletedStates}
            updatePageState={this.props.updateUITCompletedPaginationPage}
            paginationPageSize={Number(this.props.uitCompletedPaginationPageSize)}
            updatePaginationPageSize={this.props.updateUITCompletedPaginationPageSize}
            data={this.state.allCompletedProcesses}
            resultsFound={this.state.resultsFound}
          />
          <div>
            <GenericTable
              classnamePrefix="completed-processes"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testAdministration.uit.tabs.completedProcessess.noCompletedProcessess
              }
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <Pagination
            paginationContainerId={"completed-processes-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.uitCompletedPaginationPage}
            updatePaginationPageState={this.props.updateUITCompletedPaginationPage}
            firstTableRowId={"completed-processes-table-row-0"}
          />
        </div>
        {Object.keys(this.state.completedProcessDetails).length > 0 && (
          <PopupBox
            show={this.state.showCompletedProcessDetailsPopup}
            title={LOCALIZE.formatString(
              LOCALIZE.testAdministration.uit.tabs.completedProcessess.popup.title,
              this.state.completedProcessDetails.test,
              this.state.completedProcessDetails.process_number
            )}
            handleClose={() => {}}
            description={
              <div>
                <p style={styles.popupTableLabel}>
                  {LOCALIZE.formatString(
                    LOCALIZE.testAdministration.uit.tabs.completedProcessess.popup.description,
                    `${
                      this.state.currentlyLoadingForPopUp
                        ? LOCALIZE.commons.na
                        : typeof this.state.rowsDefinitionPopup.data !== "undefined" &&
                          this.state.rowsDefinitionPopup.data.length
                    }`
                  )}
                </p>
                <GenericTable
                  classnamePrefix="view-selected-active-processes"
                  columnsDefinition={columnsDefinitionPopup}
                  rowsDefinition={this.state.rowsDefinitionPopup}
                  emptyTableMessage={""}
                  currentlyLoading={this.state.currentlyLoadingForPopUp}
                />
              </div>
            }
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={LOCALIZE.commons.close}
            rightButtonLabel={LOCALIZE.commons.close}
            rightButtonIcon={faTimes}
            rightButtonAction={this.closeCompletedProcessDetailsPopup}
          />
        )}
        {Object.keys(this.state.selectedAccommodationRequestData).length > 0 &&
          this.state.showAccommodationsRequestDetailsPopup && (
            <AccommodationsRequestDetailsPopup
              source={ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.ta}
              userAccommodationFileId={this.state.selectedAccommodationRequestData.id}
              selectedUserAccommodationFileData={this.state.selectedAccommodationRequestData}
              showPopup={this.state.showAccommodationsRequestDetailsPopup}
              closeAccommodationsRequestDetailsPopup={this.closeAccommodationsRequestDetailsPopup}
              popupSize="lg"
              // not providing isDraft props, since the option to print/download is not available for this user
            />
          )}
      </div>
    );
  }
}

export { CompletedProcesses as unconnectedCompletedProcesses };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    uitCompletedPaginationPageSize: state.uit.uitCompletedPaginationPageSize,
    uitCompletedPaginationPage: state.uit.uitCompletedPaginationPage,
    uitCompletedKeyword: state.uit.uitCompletedKeyword,
    uitCompletedActiveSearch: state.uit.uitCompletedActiveSearch,
    triggerUitTablesRerender: state.uit.triggerUitTablesRerender
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllCompletedUITProcessesForTA,
      updateUITCompletedPaginationPage,
      updateUITCompletedPaginationPageSize,
      updateSearchUitCompletedStates,
      getFoundCompletedUITProcessesForTA,
      getSelectedUITCompletedProcessesDetails,
      resetUitStates,
      getAccommodationFileDataForDetailsPopup
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CompletedProcesses);
