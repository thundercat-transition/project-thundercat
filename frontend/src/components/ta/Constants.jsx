import LOCALIZE from "../../text_resources";

// reference: assigned_test_status.py
const TEST_STATUS = {
  ASSIGNED: "assigned",
  CHECKED_IN: "checked_in",
  UNASSIGNED: "unassigned",
  PRE_TEST: "pre_test",
  ACTIVE: "active",
  TRANSITION: "transition",
  LOCKED: "locked",
  PAUSED: "paused",
  SUBMITTED: "submitted",
  QUIT: "quit"
};

// UIT test status
const UIT_TEST_STATUS = {
  TAKEN: 91,
  NOT_TAKEN: 92,
  IN_PROGRESS: 93,
  UNASSIGNED: 94,
  DEACTIVATED: 95
};

function getUitTestStatus(uitTestStatus, codeDeactivated = null) {
  switch (uitTestStatus) {
    case UIT_TEST_STATUS.DEACTIVATED:
      return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
        .testCodeDeactivated;
    case UIT_TEST_STATUS.IN_PROGRESS:
      return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
        .testInProgress;
    case UIT_TEST_STATUS.TAKEN:
      return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup.testTaken;
    case UIT_TEST_STATUS.UNASSIGNED:
      return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
        .testUnassigned;
    default:
      if (codeDeactivated) {
        return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
          .testCodeDeactivated;
      }
      return LOCALIZE.testAdministration.uit.tabs.activeProcessess.selectedProcessPopup
        .testNotTaken;
  }
}

export default TEST_STATUS;
export { UIT_TEST_STATUS, getUitTestStatus };
