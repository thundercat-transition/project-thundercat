import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { isTokenStillValid, getUserInformation } from "../../modules/LoginRedux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import ContentContainer from "../commons/ContentContainer";
import "../../css/test-administration.css";
import { styles as SystemAdministrationStyles } from "../etta/SystemAdministration";
import SideNavigation from "../eMIB/SideNavigation";
import SupervisedTesting from "./SupervisedTesting";
import UnsupervisedInternetTesting from "./uit/UnsupervisedInternetTesting";
import Reports from "./Reports";
import {
  getTestAdministratorAssignedCandidates,
  setTAUserSideNavState
} from "../../modules/TestAdministrationRedux";
import { getTestPermissions } from "../../modules/PermissionsRedux";
import { PATH } from "../commons/Constants";
import LastLogin from "../authentication/LastLogin";

export const styles = {
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  appPadding: {
    padding: "15px"
  }
};

export const taActions = {
  APPROVE: "APPROVE",
  UNASSIGN: "UN_ASSIGN",
  LOCK_UNLOCK: "LOCK_UNLOCK",
  LOCK_UNLOCK_ALL: "LOCK_UNLOCK_ALL"
};

class TestAdministration extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // Props from Redux
    isTokenStillValid: PropTypes.func,
    getTestPermissions: PropTypes.func,
    getTestAdministratorAssignedCandidates: PropTypes.func,
    getUserInformation: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.languageFieldRef = React.createRef();
    this.testFieldRef = React.createRef();
  }

  state = {
    isLoaded: false,
    testPermissions: [],
    testAdministratorAssignedCandidates: [],
    triggerReRender: false,
    isTaDashboard: false
  };

  // calling redux state on page load to get the first name and last name
  componentDidMount = () => {
    this._isMounted = true;
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        this.populateTestPermissions();
        this.setState({
          isLoaded: true
        });
      }
    });

    if (this.props.currentHomePage === PATH.testAdministration) {
      this.setState({
        isTaDashboard: true
      });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        // if CAT language toggle button has been selected
        if (prevProps.currentLanguage !== this.props.currentLanguage) {
          this.populateTestPermissions();
        }
      }
    });
  }

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  // populating user' test permissions object
  populateTestPermissions = () => {
    this.props.getTestPermissions(false).then(response => {
      this.setState({ testPermissions: response });
    });
  };

  // Returns array where each item indicates specifications related to How To Page including the title and the body
  getTestAdministrationSections = () => {
    return [
      {
        menuString: LOCALIZE.testAdministration.sideNavItems.uat,
        body: <UnsupervisedInternetTesting triggerReRender={this.state.triggerReRender} />
      },
      {
        menuString: LOCALIZE.testAdministration.sideNavItems.supervisedTesting,
        body: <SupervisedTesting />
      },
      {
        menuString: LOCALIZE.testAdministration.sideNavItems.reports,
        body: <Reports />
      }
    ];
  };

  render() {
    const specs = this.getTestAdministrationSections();
    return (
      <div>
        {this.state.isTaDashboard && <LastLogin lastLoginDate={this.props.lastLogin} />}
        <div className="app" style={styles.appPadding}>
          <Helmet>
            <html lang={this.props.currentLanguage} />
            <title className="notranslate">{LOCALIZE.titles.testAdministration}</title>
          </Helmet>
          <ContentContainer>
            <div id="main-content" role="main">
              <div
                id="user-welcome-message-div"
                style={SystemAdministrationStyles.header}
                aria-labelledby="user-welcome-message"
                className="notranslate"
              >
                <h1 id="user-welcome-message" className="green-divider">
                  {LOCALIZE.formatString(
                    LOCALIZE.testAdministration.title,
                    this.props.firstName,
                    this.props.lastName
                  )}
                </h1>
              </div>
              <div>
                <div style={SystemAdministrationStyles.sectionContainerLabelDiv}>
                  <div>
                    <label style={SystemAdministrationStyles.sectionContainerLabel}>
                      {LOCALIZE.testAdministration.containerLabel}
                      <span style={SystemAdministrationStyles.tabStyleBorder}></span>
                    </label>
                  </div>
                </div>
                <section aria-label={LOCALIZE.ariaLabel.sideNavigationSection}>
                  <div style={SystemAdministrationStyles.sectionContainer}>
                    <SideNavigation
                      specs={specs}
                      startIndex={this.props.selectedSideNavItem}
                      loadOnClick={true}
                      displayNextPreviousButton={false}
                      isMain={true}
                      tabContainerStyle={SystemAdministrationStyles.tabContainer}
                      tabContentStyle={SystemAdministrationStyles.tabContent}
                      navStyle={SystemAdministrationStyles.nav}
                      bodyContentCustomStyle={SystemAdministrationStyles.sideNavBodyContent}
                      updateSelectedSideNavItem={this.props.setTAUserSideNavState}
                    />
                  </div>
                </section>
              </div>
            </div>
          </ContentContainer>
        </div>
      </div>
    );
  }
}

export { TestAdministration as UnconnectedTestAdministration };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    lastLogin: state.user.lastLogin,
    currentHomePage: state.userPermissions.currentHomePage,
    selectedSideNavItem: state.testAdministration.selectedSideNavItem
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid,
      getTestPermissions,
      getTestAdministratorAssignedCandidates,
      getUserInformation,
      setTAUserSideNavState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestAdministration);
