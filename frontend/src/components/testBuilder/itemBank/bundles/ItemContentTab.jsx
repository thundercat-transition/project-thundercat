import React, { Component } from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getItemBankHeightCalculations } from "../../../../helpers/inTestHeightCalculations";
import { getLineSpacingCSS } from "../../../../modules/AccommodationsRedux";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import CustomDraggable from "../../../commons/CustomDraggable/CustomDraggable";
import {
  setBundleData,
  setBundleLocalUnlinkedSystemIds,
  setValidBundleItemsBasicRuleState
} from "../../../../modules/ItemBankRedux";
import LOCALIZE from "../../../../text_resources";
import { Col, Row } from "react-bootstrap";

const styles = {
  mainContainer: {
    width: "100%",
    padding: "24px 12px",
    border: "1px solid #cdcdcd",
    borderTop: "none",
    borderBottom: "none",
    overflowY: "auto",
    overflowX: "hidden"
  },
  basicRyleContainer: {
    width: "80%"
  },
  rowContainer: {
    margin: "12px 24px"
  },
  basicRuleLabel: {
    marginRight: 12,
    paddingTop: 6
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6,
    width: 250
  },
  input: {
    width: 100,
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4,
    textAlign: "center"
  }
};

// Reordering of a list after onDragEnd
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

class ItemContentTab extends Component {
  state = {
    numberOfItemsToPull: this.props.bundleData.bundle_items_basic_rule_data,
    isValidNumberOfItemsToPull: true,
    typingTimer: 0
  };

  componentDidMount = () => {
    // validating number of items to pull
    this.validateNumberOfItemsToPull();
  };

  componentDidUpdate = prevProps => {
    // if bundleData gets updated
    if (prevProps.bundleData !== this.props.bundleData) {
      this.setState(
        { numberOfItemsToPull: this.props.bundleData.bundle_items_basic_rule_data },
        () => {
          // validating number of items to pull
          this.validateNumberOfItemsToPull();
        }
      );
    }

    // if ID of bundleData gets updated (meaning selected bundle has been changed)
    if (prevProps.bundleData.id !== this.props.bundleData.id) {
      this.setState(
        { numberOfItemsToPull: this.props.bundleData.bundle_items_basic_rule_data },
        () => {
          // validating number of items to pull
          this.validateNumberOfItemsToPull();
        }
      );
    }
  };

  // onDragEnd - Item
  onDragEndItem(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    // creating deep copy of bundleData
    const copyOfBundleData = _.cloneDeep(this.props.bundleData);

    // initializing needed variables
    let newItems = [];
    let filteredBundleItemsAssociationContent = [];
    let filteredBundleBundlesAssociationContent = [];

    // getting respective content
    filteredBundleItemsAssociationContent = copyOfBundleData.bundle_associations.filter(obj => {
      return obj.system_id_link !== null;
    });
    filteredBundleBundlesAssociationContent = copyOfBundleData.bundle_associations.filter(obj => {
      return obj.system_id_link === null;
    });

    // reordering content
    newItems = reorder(
      filteredBundleItemsAssociationContent,
      result.source.index,
      result.destination.index
    );

    // updating display orders
    for (let i = 0; i < newItems.length; i++) {
      newItems[i].display_order = i + 1;
    }

    // updating bundle items associations content
    filteredBundleItemsAssociationContent = newItems;

    const mergedArrays = filteredBundleItemsAssociationContent.concat(
      filteredBundleBundlesAssociationContent
    );

    // updating the redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const new_obj = {
      ...this.props.bundleData,
      bundle_associations: mergedArrays
    };
    // setting new bundleData redux state
    this.props.setBundleData(new_obj);
  }

  // Remove item from bundle_associations
  removeItem = index => {
    // creating deep copy of bundleData
    const copyOfBundleData = _.cloneDeep(this.props.bundleData);

    // initializing needed variables
    const newItems = [];
    let filteredBundleItemsAssociationContent = [];
    let filteredBundleBundlesAssociationContent = [];

    // getting respective content
    filteredBundleItemsAssociationContent = copyOfBundleData.bundle_associations.filter(obj => {
      return obj.system_id_link !== null;
    });
    filteredBundleBundlesAssociationContent = copyOfBundleData.bundle_associations.filter(obj => {
      return obj.system_id_link === null;
    });

    // adding deleted item to bundleLocalUnlinkedSystemIds redux state
    const copyOfBundleLocalUnlinkedSystemIds = _.cloneDeep(this.props.bundleLocalUnlinkedSystemIds);
    copyOfBundleLocalUnlinkedSystemIds.push(
      filteredBundleItemsAssociationContent[index].system_id_link
    );
    this.props.setBundleLocalUnlinkedSystemIds(copyOfBundleLocalUnlinkedSystemIds);

    // deleting respective item
    filteredBundleItemsAssociationContent.splice(index, 1);

    // re-ordering display order
    for (let i = 0; i < filteredBundleItemsAssociationContent.length; i++) {
      const data = filteredBundleItemsAssociationContent[i];
      // note that if you update this ID, you'll need to update it in the backend as well (needed for draggable logic)
      // file ref: ...\backend\db_views\serializers\item_bank_serializers.py
      data.display_order = i + 1;
      newItems.push(data);
    }

    // updating bundle items associations content
    filteredBundleItemsAssociationContent = newItems;

    const mergedArrays = filteredBundleItemsAssociationContent.concat(
      filteredBundleBundlesAssociationContent
    );

    // updating the redux states
    // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
    const new_obj = {
      ...this.props.bundleData,
      bundle_associations: mergedArrays
    };
    // setting new bundleData redux state
    this.props.setBundleData(new_obj);
  };

  // Draggable Styling
  getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: "none",

    // change background colour if dragging
    background: isDragging ? "#0099A8" : "white",
    color: "#00565e",

    // styles we need to apply on draggables
    ...draggableStyle
  });

  // Droppable Styling
  getListStyle = isDraggingOver => ({
    background: isDraggingOver ? "#cdcdcd" : "white",
    padding: "12px 24px 12px 24px"
  });

  updateNumberOfItemsToPullContent = event => {
    const numberOfItemsToPull = event.target.value;
    // allow only numeric values (0 to 5 chars)
    const regexExpression = /^([0-9]{0,5})$/;
    if (regexExpression.test(numberOfItemsToPull)) {
      this.setState(
        {
          numberOfItemsToPull: numberOfItemsToPull !== "" ? parseInt(numberOfItemsToPull) : ""
        },
        () => {
          // validating number of items to pull
          this.validateNumberOfItemsToPull();
        }
      );

      // Create a buffer before updating the redux state
      // Clear the old typingTimer
      clearTimeout(this.state.typingTimer);

      // Change the typingTimer to a timeout of 200 millisec
      // When user stops writing, this will go through after that time
      // Otherwise, the timer will be reset to that time
      const newTimer = setTimeout(() => {
        // updating redux states
        // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
        const new_obj = {
          ...this.props.bundleData,
          bundle_items_basic_rule_data: this.state.numberOfItemsToPull
        };

        // setting new bundleData redux state
        this.props.setBundleData(new_obj);
      }, 200);

      this.setState({ typingTimer: newTimer });
    }
  };

  validateNumberOfItemsToPull = () => {
    let isValidNumberOfItemsToPull = true;

    // numberOfItemsToPull is 0
    // numberOfItemsToPull is greater than total amount of associated items
    if (
      this.state.numberOfItemsToPull === 0 ||
      this.state.numberOfItemsToPull >
        this.props.bundleData.bundle_associations.filter(obj => obj.system_id_link !== null).length
    ) {
      // setting isValidNumberOfItemsToPull to false
      isValidNumberOfItemsToPull = false;
    }

    // updating needed states
    this.setState({ isValidNumberOfItemsToPull: isValidNumberOfItemsToPull });

    // updating needed redux states
    this.props.setValidBundleItemsBasicRuleState(isValidNumberOfItemsToPull);
  };

  render() {
    // calculating the height of the top tabs and above (without considering the nav bar)
    // =========================================================================================
    let heightOfBackToItemBankSelectionButton = 0;
    let heightOfTopTabs = 0;
    let heightOfFooter = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-item-bank-editor-btn") != null) {
      heightOfBackToItemBankSelectionButton = document.getElementById(
        "back-to-item-bank-editor-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight += heightOfBackToItemBankSelectionButton + heightOfTopTabs + 12; // height of "back to item bank selection button" + height of margin under button

    if (document.getElementById("item-bank-footer-main-div") !== null) {
      heightOfFooter = document.getElementById("item-bank-footer-main-div").offsetHeight;
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    const customHeight = getItemBankHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight,
      heightOfFooter
    );
    // =========================================================================================

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={{ ...styles.mainContainer, ...customHeight.height }}>
        {/* displaying "number of items to pull" only when at least one item has been assigned to the current bundle */}
        {this.props.bundleData.bundle_associations.filter(obj => obj.system_id_link !== null)
          .length > 0 && (
          <div style={styles.basicRyleContainer}>
            <Row className="align-items-top" style={styles.rowContainer}>
              <label
                htmlFor="item-bank-bundles-items-content-number-of-items-to-pull"
                style={styles.basicRuleLabel}
              >
                {
                  LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.bundles
                    .itemContent.basicRuleLabel
                }
              </label>
              <div>
                <input
                  id="item-bank-bundles-items-content-number-of-items-to-pull"
                  className={
                    this.state.isValidNumberOfItemsToPull ? "valid-field" : "invalid-field"
                  }
                  aria-required={false}
                  aria-invalid={!this.state.isValidNumberOfItemsToPull}
                  style={{ ...styles.input, ...accommodationsStyle }}
                  type="text"
                  placeholder={LOCALIZE.commons.all}
                  value={this.state.numberOfItemsToPull}
                  onChange={this.updateNumberOfItemsToPullContent}
                ></input>
                {!this.state.isValidNumberOfItemsToPull && (
                  <div>
                    <label
                      htmlFor="item-bank-bundles-items-content-number-of-items-to-pull"
                      style={styles.errorMessage}
                    >
                      {
                        LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                          .bundles.itemContent.basicRuleErrorLabel
                      }
                    </label>
                  </div>
                )}
              </div>
            </Row>
          </div>
        )}
        <div>
          <DragDropContext
            onDragEnd={result => {
              this.onDragEndItem(result);
            }}
          >
            <Droppable droppableId="item-content-item-droppable">
              {(provided, snapshot) => (
                <div
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                  style={this.getListStyle(snapshot.isDraggingOver)}
                >
                  {this.props.bundleData.bundle_associations
                    .filter(obj => {
                      return obj.system_id_link !== null;
                    })
                    .map((item, index) => (
                      <CustomDraggable
                        key={`item-content-item-key-${index}`}
                        bundleAssociatedItemData={item}
                        index={index}
                        getItemStyle={this.getItemStyle}
                        onTextAreaComponentChange={() => {}}
                        deleteItem={() => this.removeItem(index)}
                        customDeleteButtonClass="align-items-center"
                      />
                    ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        </div>
      </div>
    );
  }
}

export { ItemContentTab as unconnectedItemContentTab };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testNavBarHeight: state.testSection.testNavBarHeight,
    bundleData: state.itemBank.bundleData,
    bundleLocalUnlinkedSystemIds: state.itemBank.bundleLocalUnlinkedSystemIds
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setBundleData,
      setBundleLocalUnlinkedSystemIds,
      setValidBundleItemsBasicRuleState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ItemContentTab);
