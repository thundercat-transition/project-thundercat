// reference: bundles_utils.py (../backend/cms/static/bundles_utils.py)
const BUNDLE_RYLE_TYPE = {
  bundleItemsBasic: "bundle_items_basic_rule",
  bundleBundles: "bundle_bundles_rule"
};

export default BUNDLE_RYLE_TYPE;
