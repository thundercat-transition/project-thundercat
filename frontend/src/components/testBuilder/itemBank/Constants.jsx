// reference: item_bank_utils.py (../backend/cms/static/item_bank_utils.py)
const ELEMENT_TO_UPDATE = {
  itemBankDefinition: "item_bank_definition",
  itemBankAttribute: "item_bank_attributes"
};

export const ITEM_ACTION_TYPE = {
  saveDraft: "save_draft",
  upload: "upload",
  uploadOverwrite: "upload_overwrite",
  activateDeactivate: "activate_deactivate"
};

export const ITEM_BANK_ACCESS_TYPE = {
  none: "is_none",
  owner: "is_owner",
  contributor: "is_contributor"
};

export const DRAFT_VERSION = "DRAFT";

export default ELEMENT_TO_UPDATE;
