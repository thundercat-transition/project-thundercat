/* eslint-disable react/sort-comp */
import React, { Component } from "react";
import * as _ from "lodash";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getItemBankHeightCalculations } from "../../../../helpers/inTestHeightCalculations";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import CustomButton from "../../../commons/CustomButton";
import THEME from "../../../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import CustomDraggable, {
  DRAGGABLE_ITEM_TYPE
} from "../../../commons/CustomDraggable/CustomDraggable";
import { Row, Col, Container } from "react-bootstrap";
import { ITEM_CONTENT_TYPE, ITEM_OPTION_TYPE, ITEM_TYPE_CODENAMES } from "./Constants";
import DropdownSelect from "../../../commons/DropdownSelect";
import LOCALIZE from "../../../../text_resources";
import { LANGUAGES, LANGUAGE_IDS } from "../../../../modules/LocalizeRedux";
import { setItemData, setSelectedContentLanguage } from "../../../../modules/ItemBankRedux";

export const CHANGE_TYPE = {
  content: "content",
  historicalOptionId: "historical_option_id",
  rationale: "rationale",
  score: "score",
  excludeFromShuffle: "exclude_from_shuffle"
};

const styles = {
  mainContainer: {
    width: "100%",
    padding: "24px 0px",
    border: "1px solid #cdcdcd",
    borderTop: "none",
    borderBottom: "none",
    overflowY: "auto"
  },
  dropdownContainer: {
    width: "60%"
  },
  buttonText: {
    paddingLeft: "12px"
  },
  container: {
    paddingTop: "12px"
  },
  title: {
    fontWeight: "bold"
  }
};

// TODO: Move functions in a helper file and make more open for other lists
/*-----------------------------------------------------------------------------*/

// Reordering of a list after onDragEnd
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

/*-----------------------------------------------------------------------------*/

class ContentTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      triggerRerender: false,
      // initial language ==> English
      selectedLanguageOption: {
        label: LOCALIZE.commons.english,
        value: LANGUAGE_IDS.english,
        languageCode: LANGUAGES.english
      },
      languageOptions: [],
      isInstruction: this.props.itemData.item_type_codename === ITEM_TYPE_CODENAMES.instruction,
      itemContentState: this.props.itemData.item_content,
      itemOptionsState: this.props.itemData.item_options,
      typingTimer: 0
    };
  }

  componentDidMount = () => {
    // bilingual item bank
    if (this.props.item_bank_data.item_bank_definition[0].language_id === null) {
      // populating initial language data
      this.populateInitialLanguageData();
    }
  };

  componentDidUpdate = prevProps => {
    // if currentTab gets updated
    if (prevProps.currentTab !== this.props.currentTab) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }
    // if itemData gets updated
    if (prevProps.itemData !== this.props.itemData) {
      this.setState({
        isInstruction: this.props.itemData.item_type_codename === ITEM_TYPE_CODENAMES.instruction,
        itemContentState: this.props.itemData.item_content,
        itemOptionsState: this.props.itemData.item_options,
        triggerRerender: !this.state.triggerRerender
      });
    }
  };

  // populating language options and selected language data
  populateInitialLanguageData = () => {
    const languageOptions = [];
    // defaulting English as default selected language (in case the item bank language is bilingual)
    let selectedLanguageOption = {
      label: LOCALIZE.commons.english,
      value: LANGUAGE_IDS.english,
      languageCode: LANGUAGES.english
    };
    // looping in languageData
    for (let i = 0; i < this.props.languageData.length; i++) {
      languageOptions.push({
        label: this.props.languageData[i].language_text[this.props.currentLanguage][0].text,
        value: this.props.languageData[i].language_id,
        languageCode: this.props.languageData[i].ISO_Code_1
      });
      // checking if item bank language ID matches
      if (
        this.props.item_bank_data.item_bank_definition[0].language_id ===
        this.props.languageData[i].language_id
      ) {
        selectedLanguageOption = {
          label: this.props.languageData[i].language_text[this.props.currentLanguage][i].text,
          value: this.props.languageData[i].language_id,
          languageCode: this.props.languageData[i].ISO_Code_1
        };
      }
    }
    // updating redux states
    this.props.setSelectedContentLanguage(selectedLanguageOption);
    // updating needed states
    this.setState({
      languageOptions: languageOptions,
      selectedLanguageOption: selectedLanguageOption
    });
  };

  getSelectedLanguage = selectedOption => {
    // updating redux states
    this.props.setSelectedContentLanguage(selectedOption);
    // updated needed state
    this.setState({ selectedLanguageOption: selectedOption });
  };

  /*--------------------------*/
  //                          //
  //      Stem Section        //
  //                          //
  /*--------------------------*/

  // onDragEnd - Stem Section
  onDragEndStemSection(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    // creating deep copy of item_content (itemData)
    const copyOfItemContent = _.cloneDeep(this.props.itemData.item_content);

    // initializing needed variables
    let items = [];
    let filteredContent = [];

    // bilingual item bank
    if (this.props.item_bank_data.item_bank_definition[0].language_id === null) {
      // looping in all available languages
      for (let i = 0; i < this.props.languageData.length; i++) {
        // item content of language of current iteration is defined and contains data (content type markdown)
        if (
          typeof copyOfItemContent[this.props.languageData[i].ISO_Code_1] !== "undefined" &&
          copyOfItemContent[this.props.languageData[i].ISO_Code_1].length > 0 &&
          copyOfItemContent[this.props.languageData[i].ISO_Code_1].filter(obj => {
            return obj.content_type === ITEM_CONTENT_TYPE.markdown;
          }).length > 0
        ) {
          // getting respective content
          filteredContent = copyOfItemContent[this.props.languageData[i].ISO_Code_1].filter(obj => {
            return obj.content_type === ITEM_CONTENT_TYPE.markdown;
          });
          // reordering content
          items = reorder(filteredContent, result.source.index, result.destination.index);
        }

        // updating the order attribute
        // initializing newItems as non markdown content type
        let newItems =
          typeof copyOfItemContent[this.props.languageData[i].ISO_Code_1] !== "undefined"
            ? copyOfItemContent[this.props.languageData[i].ISO_Code_1].filter(obj => {
                return obj.content_type !== ITEM_CONTENT_TYPE.markdown;
              })
            : [];

        // updating content orders
        for (let i = 0; i < items.length; i++) {
          items[i].content_order = i + 1;
        }

        // combining arrays
        newItems = newItems.concat(items);

        // updating item_content in langauge of current iteration
        copyOfItemContent[this.props.languageData[i].ISO_Code_1] = newItems;
      }

      // updating the redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.itemData,
        item_content: copyOfItemContent
      };

      // updating itemContentState
      this.setState({ itemContentState: new_obj.item_content });

      // setting new itemData redux state
      this.props.setItemData(new_obj, this.props.languageData);
      // unilangual item bank
    } else {
      // getting language code
      let languageCode = null;
      for (let i = 0; i < this.props.languageData.length; i++) {
        if (
          this.props.languageData[i].language_id ===
          this.props.item_bank_data.item_bank_definition[0].language_id
        ) {
          languageCode = this.props.languageData[i].ISO_Code_1;
          break;
        }
      }

      // getting respective content
      filteredContent = copyOfItemContent[languageCode].filter(obj => {
        return obj.content_type === ITEM_CONTENT_TYPE.markdown;
      });
      // reordering content
      items = reorder(filteredContent, result.source.index, result.destination.index);

      // updating the order attribute
      // initializing newItems as non markdown content type
      let newItems =
        typeof copyOfItemContent[languageCode] !== "undefined"
          ? copyOfItemContent[languageCode].filter(obj => {
              return obj.content_type !== ITEM_CONTENT_TYPE.markdown;
            })
          : [];

      // updating content orders
      for (let i = 0; i < items.length; i++) {
        items[i].content_order = i + 1;
      }

      // combining arrays
      newItems = newItems.concat(items);

      // updating item_content in langauge of current iteration
      copyOfItemContent[languageCode] = newItems;

      // updating the redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.itemData,
        item_content: copyOfItemContent
      };

      // updating itemContentState
      this.setState({ itemContentState: new_obj.item_content });

      // setting new itemData redux state
      this.props.setItemData(new_obj, this.props.languageData);
    }
  }

  // Function used to get and replace value in textarea of an item
  onTextAreaComponentChange = (event, index, itemContentData) => {
    // Stems have a max length of 3000, instructions have a max length of 15000
    const max_length = this.state.isInstruction ? 15000 : 3000;

    if (event.target.value.length <= max_length) {
      // getting language code
      let { languageCode } = this.state.selectedLanguageOption;
      for (let i = 0; i < this.props.languageData.length; i++) {
        if (
          this.props.languageData[i].language_id ===
          this.props.item_bank_data.item_bank_definition[0].language_id
        ) {
          languageCode = this.props.languageData[i].ISO_Code_1;
          break;
        }
      }

      // creating deep copy of itemContentState and updating respective stem content
      const copyOfItemContentState = _.cloneDeep(this.state.itemContentState);
      const indexToUpdate2 = this.state.itemContentState[languageCode].findIndex(obj => {
        return obj.content_type === ITEM_CONTENT_TYPE.markdown && obj.id === itemContentData.id;
      });
      copyOfItemContentState[languageCode][indexToUpdate2].text = event.target.value;
      this.setState({ itemContentState: copyOfItemContentState });

      // Create a buffer before updating the redux state
      // Clear the old typingTimer
      clearTimeout(this.state.typingTimer);

      // Change the typingTimer to a timeout of 300 millisec
      // When user stops writing, this will go through after that time
      // Otherwise, the timer will be reset to that time
      const newTimer = setTimeout(() => {
        // updating the redux states
        // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
        const new_obj = {
          ...this.props.itemData,
          item_content: this.state.itemContentState
        };

        // setting new itemData redux state
        this.props.setItemData(new_obj, this.props.languageData);
      }, 300);
      this.setState({ typingTimer: newTimer });
    }
  };

  // Add stem item to stemSectionItems
  addStemItem = () => {
    // creating deep copy of item_content
    const copyOfItemContent = _.cloneDeep(this.props.itemData.item_content);

    // bilingual item bank
    if (this.props.item_bank_data.item_bank_definition[0].language_id === null) {
      // looping in languageData
      for (let i = 0; i < this.props.languageData.length; i++) {
        // intitializing newMap every iteration
        let newMap = [];

        // copyOfItemContent in language of current iteration is defined
        if (typeof copyOfItemContent[this.props.languageData[i].ISO_Code_1] !== "undefined") {
          // setting newMap
          newMap = [...copyOfItemContent[this.props.languageData[i].ISO_Code_1]];
        }

        // Building the new item
        const newItem = {
          // note that if you update this ID, you'll need to update it in the backend as well (needed for draggable logic)
          // file ref: ...\backend\db_views\serializers\item_bank_view_serializers.py
          id: `item-stem-${this.props.languageData[i].ISO_Code_1}-${
            newMap.filter(obj => {
              return obj.content_type === ITEM_CONTENT_TYPE.markdown;
            }).length + 1
          }`,
          item_id: this.props.itemData.item_id,
          content_type: ITEM_CONTENT_TYPE.markdown,
          item_type: DRAGGABLE_ITEM_TYPE.TextAreaComponent,
          content_order:
            newMap.filter(obj => {
              return obj.content_type === ITEM_CONTENT_TYPE.markdown;
            }).length + 1,
          language_id: this.props.languageData[i].language_id,
          text: ""
        };

        newMap.push(newItem);
        copyOfItemContent[this.props.languageData[i].ISO_Code_1] = newMap;
      }
      // updating redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.itemData,
        item_content: copyOfItemContent
      };

      // updating itemContentState
      this.setState({ itemContentState: new_obj.item_content });

      // setting new itemData redux state
      this.props.setItemData(new_obj, this.props.languageData);

      // unilangual item bank
    } else {
      let newMap = [];

      // getting language code
      let languageCode = null;
      for (let i = 0; i < this.props.languageData.length; i++) {
        if (
          this.props.languageData[i].language_id ===
          this.props.item_bank_data.item_bank_definition[0].language_id
        ) {
          languageCode = this.props.languageData[i].ISO_Code_1;
          break;
        }
      }

      if (typeof this.props.itemData.item_content[languageCode] !== "undefined") {
        newMap = copyOfItemContent[languageCode];
      }

      // Building the new item
      const newItem = {
        // note that if you update this ID, you'll need to update it in the backend as well (needed for draggable logic)
        // file ref: ...\backend\db_views\serializers\item_bank_view_serializers.py
        id: `item-stem-${languageCode}-${
          newMap.filter(obj => {
            return obj.content_type === ITEM_CONTENT_TYPE.markdown;
          }).length + 1
        }`,
        item_id: this.props.itemData.item_id,
        content_type: ITEM_CONTENT_TYPE.markdown,
        item_type: DRAGGABLE_ITEM_TYPE.TextAreaComponent,
        content_order:
          newMap.filter(obj => {
            return obj.content_type === ITEM_CONTENT_TYPE.markdown;
          }).length + 1,
        language_id: this.props.item_bank_data.item_bank_definition[0].language_id,
        text: ""
      };

      newMap.push(newItem);

      // updating redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.itemData,
        item_content: {
          ...this.props.itemData.item_content,
          [languageCode]: newMap
        }
      };

      // updating itemContentState
      this.setState({ itemContentState: new_obj.item_content });

      // setting new itemData redux state
      this.props.setItemData(new_obj, this.props.languageData);
    }
  };

  // Remove stem item from stemSectionItems
  removeStemItem = index => {
    // bilingual item bank
    if (this.props.item_bank_data.item_bank_definition[0].language_id === null) {
      // creating deep copy of item_content
      const copyOfItemContent = _.cloneDeep(this.props.itemData.item_content);
      // looping in languageData
      for (let i = 0; i < this.props.languageData.length; i++) {
        // making sure that copyOfItemContent in language of current iteration is defined (can be undefined if number of stems are not the same in all languages - was possible with old logic)
        if (typeof copyOfItemContent[this.props.languageData[i].ISO_Code_1] !== "undefined") {
          const filteredItemContent = copyOfItemContent[
            this.props.languageData[i].ISO_Code_1
          ].filter(obj => {
            return obj.content_type === ITEM_CONTENT_TYPE.markdown;
          });

          // deleting respective item content
          filteredItemContent.splice(index, 1);

          // re-ordering content order attributes
          // initializing finalMap with non markdown content
          const finalMap = copyOfItemContent[this.props.languageData[i].ISO_Code_1].filter(obj => {
            return obj.content_type !== ITEM_CONTENT_TYPE.markdown;
          });
          for (let j = 0; j < filteredItemContent.length; j++) {
            const data = filteredItemContent[j];
            // note that if you update this ID, you'll need to update it in the backend as well (needed for draggable logic)
            // file ref: ...\backend\db_views\serializers\item_bank_serializers.py
            data.id = `item-stem-${this.props.languageData[i].ISO_Code_1}-${j + 1}`;
            data.content_order = j + 1;
            finalMap.push(data);
          }
          copyOfItemContent[this.props.languageData[i].ISO_Code_1] = finalMap;
        }
      }

      // updating redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.itemData,
        item_content: copyOfItemContent
      };

      // updating itemContentState
      this.setState({ itemContentState: new_obj.item_content });

      // setting new itemData redux state
      this.props.setItemData(new_obj, this.props.languageData);

      // unilangual item bank
    } else {
      // getting language code
      let languageCode = null;
      for (let i = 0; i < this.props.languageData.length; i++) {
        if (
          this.props.languageData[i].language_id ===
          this.props.item_bank_data.item_bank_definition[0].language_id
        ) {
          languageCode = this.props.languageData[i].ISO_Code_1;
          break;
        }
      }

      const newMap = [
        ...this.props.itemData.item_content[languageCode].filter(obj => {
          return obj.content_type === ITEM_CONTENT_TYPE.markdown;
        })
      ];

      newMap.splice(index, 1);

      // re-ordering content order attributes
      // initializing finalMap with non markdown content
      const finalMap = this.props.itemData.item_content[languageCode].filter(obj => {
        return obj.content_type !== ITEM_CONTENT_TYPE.markdown;
      });
      for (let i = 0; i < newMap.length; i++) {
        const data = newMap[i];
        // note that if you update this ID, you'll need to update it in the backend as well (needed for draggable logic)
        // file ref: ...\backend\db_views\serializers\item_bank_serializers.py
        data.id = `item-stem-${languageCode}-${i + 1}`;
        data.content_order = i + 1;
        finalMap.push(data);
      }

      // updating redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.itemData,
        item_content: {
          ...this.props.itemData.item_content,
          [languageCode]: finalMap
        }
      };

      // updating itemContentState
      this.setState({ itemContentState: new_obj.item_content });

      // setting new itemData redux state
      this.props.setItemData(new_obj, this.props.languageData);
    }
  };

  /*--------------------------*/
  //                          //
  //     Option Section       //
  //                          //
  /*--------------------------*/

  // onDragEnd - Option Section
  onDragEndOptionSection(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    // creating deep copy of item_options (itemData)
    const copyOfItemOptions = _.cloneDeep(this.props.itemData.item_options);

    // initializing needed variables
    let items = [];
    let filteredContent = [];

    // bilingual item bank
    if (this.props.item_bank_data.item_bank_definition[0].language_id === null) {
      // looping in all available languages
      for (let i = 0; i < this.props.languageData.length; i++) {
        // item content of language of current iteration is defined and contains data
        if (
          typeof copyOfItemOptions[this.props.languageData[i].ISO_Code_1] !== "undefined" &&
          copyOfItemOptions[this.props.languageData[i].ISO_Code_1].length > 0 &&
          copyOfItemOptions[this.props.languageData[i].ISO_Code_1].filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
          }).length > 0
        ) {
          // getting respective content
          filteredContent = copyOfItemOptions[this.props.languageData[i].ISO_Code_1].filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
          });
          // reordering content
          items = reorder(filteredContent, result.source.index, result.destination.index);
        }

        // updating the order attribute
        // initializing newItems as non multiple choice option type
        let newItems =
          typeof copyOfItemOptions[this.props.languageData[i].ISO_Code_1] !== "undefined"
            ? copyOfItemOptions[this.props.languageData[i].ISO_Code_1].filter(obj => {
                return obj.option_type !== ITEM_OPTION_TYPE.multipleChoice;
              })
            : [];

        // updating option orders
        for (let i = 0; i < items.length; i++) {
          items[i].option_order = i + 1;
        }

        // combining arrays
        newItems = newItems.concat(items);

        // updating item_options in langauge of current iteration
        copyOfItemOptions[this.props.languageData[i].ISO_Code_1] = newItems;
      }

      // updating the redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.itemData,
        item_options: copyOfItemOptions
      };

      // updating itemOptionsState
      this.setState({ itemOptionsState: new_obj.item_options });

      // setting new itemData redux state
      this.props.setItemData(new_obj, this.props.languageData);
      // unilangual item bank
    } else {
      // getting language code
      let languageCode = null;
      for (let i = 0; i < this.props.languageData.length; i++) {
        if (
          this.props.languageData[i].language_id ===
          this.props.item_bank_data.item_bank_definition[0].language_id
        ) {
          languageCode = this.props.languageData[i].ISO_Code_1;
          break;
        }
      }
      // getting respective content
      filteredContent = copyOfItemOptions[languageCode].filter(obj => {
        return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
      });
      // reordering content
      items = reorder(filteredContent, result.source.index, result.destination.index);

      // updating the order attribute
      // initializing newItems as non multiple choice option type
      let newItems =
        typeof copyOfItemOptions[languageCode] !== "undefined"
          ? copyOfItemOptions[languageCode].filter(obj => {
              return obj.option_type !== ITEM_OPTION_TYPE.multipleChoice;
            })
          : [];

      // updating option orders
      for (let i = 0; i < items.length; i++) {
        items[i].option_order = i + 1;
      }

      // combining arrays
      newItems = newItems.concat(items);

      // updating item_content in langauge of current iteration
      copyOfItemOptions[languageCode] = newItems;

      // updating the redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.itemData,
        item_options: copyOfItemOptions
      };

      // updating itemOptionsState
      this.setState({ itemOptionsState: new_obj.item_options });

      // setting new itemData redux state
      this.props.setItemData(new_obj, this.props.languageData);
    }
  }

  onTextInputChange = (event, index, itemOptionData) => {
    // allow maximum of 1500 chars
    if (event.target.value.length <= 1500) {
      this.onTestOptionComponentChange(event, index, CHANGE_TYPE.content, itemOptionData);
    }
  };

  onHistoricalOptionIdInputChange = (event, index, itemOptionData) => {
    // allow only alphanumeric, slash, underscore and dash (0 to 50 chars)
    const regexExpression = /^([a-zA-z0-9-/_]{0,50})$/;
    if (regexExpression.test(event.target.value)) {
      this.onTestOptionComponentChange(
        event,
        index,
        CHANGE_TYPE.historicalOptionId,
        itemOptionData
      );
    }
  };

  onRationaleInputChange = (event, index, itemOptionData) => {
    // allow maximum of 500 chars
    if (event.target.value.length <= 500) {
      this.onTestOptionComponentChange(event, index, CHANGE_TYPE.rationale, itemOptionData);
    }
  };

  onScoreInputChange = (event, index, itemOptionData) => {
    // allow only positive/negative numeric/decimal entries
    // maximum of 5 chars
    const regex = /^[-?\d]*\.?[\d]*$/;
    if (regex.test(event.target.value) && event.target.value.length <= 5) {
      this.onTestOptionComponentChange(event, index, CHANGE_TYPE.score, itemOptionData);
    }
  };

  changeExcludeFromShuffle = (event, index, itemOptionData) => {
    this.onTestOptionComponentChange(event, index, CHANGE_TYPE.excludeFromShuffle, itemOptionData);
  };

  // Function used to get and replace value in optionSectionItems of an item
  onTestOptionComponentChange = (event, index, variableName, itemOptionData) => {
    if (variableName === CHANGE_TYPE.content) {
      // getting language code
      let { languageCode } = this.state.selectedLanguageOption;
      for (let i = 0; i < this.props.languageData.length; i++) {
        if (
          this.props.languageData[i].language_id ===
          this.props.item_bank_data.item_bank_definition[0].language_id
        ) {
          languageCode = this.props.languageData[i].ISO_Code_1;
          break;
        }
      }

      // creating deep copy of itemContentState and updating respective stem content
      const copyOfItemOptionsState = _.cloneDeep(this.state.itemOptionsState);
      const indexToUpdate2 = this.state.itemOptionsState[languageCode].findIndex(obj => {
        return (
          obj.option_type === ITEM_OPTION_TYPE.multipleChoice &&
          obj.id.split(`${languageCode}`)[0] === itemOptionData.id.split(`${languageCode}`)[0] &&
          obj.id.split(`${languageCode}`)[1] === itemOptionData.id.split(`${languageCode}`)[1]
        );
      });
      copyOfItemOptionsState[languageCode][indexToUpdate2].text = event.target.value;
      this.setState({ itemOptionsState: copyOfItemOptionsState });

      // Create a buffer before updating the redux state
      // Clear the old typingTimer
      clearTimeout(this.state.typingTimer);

      // Change the typingTimer to a timeout of 300 millisec
      // When user stops writing, this will go through after that time
      // Otherwise, the timer will be reset to that time
      const newTimer = setTimeout(() => {
        // updating the redux states
        // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
        const new_obj = {
          ...this.props.itemData,
          item_options: this.state.itemOptionsState
        };

        // setting new itemData redux state
        this.props.setItemData(new_obj, this.props.languageData);
      }, 300);
      this.setState({ typingTimer: newTimer });

      // other change type than CONTENT
    } else {
      // bilingual item bank
      if (this.props.item_bank_data.item_bank_definition[0].language_id === null) {
        // creating deep copy of itemContentState and getting index to update
        const copyOfItemOptionsState = _.cloneDeep(this.state.itemOptionsState);
        const indexToUpdate2 = this.state.itemOptionsState[
          this.state.selectedLanguageOption.languageCode
        ].findIndex(obj => {
          return (
            obj.option_type === ITEM_OPTION_TYPE.multipleChoice && obj.id === itemOptionData.id
          );
        });

        // looping in languageData
        for (let i = 0; i < this.props.languageData.length; i++) {
          const languageCode = this.props.languageData[i].ISO_Code_1;

          // HISTORICAL OPTION ID
          if (variableName === CHANGE_TYPE.historicalOptionId) {
            // updating historical_option_id (from copyOfItemOptionsState)
            copyOfItemOptionsState[languageCode][indexToUpdate2].historical_option_id =
              event.target.value.toUpperCase();

            // RATIONALE
          } else if (variableName === CHANGE_TYPE.rationale) {
            // updating rationale (from copyOfItemOptionsState)
            copyOfItemOptionsState[languageCode][indexToUpdate2].rationale = event.target.value;
            // SCORE
          } else if (variableName === CHANGE_TYPE.score) {
            // updating score (from copyOfItemOptionsState)
            // if blank ==> display "0"
            copyOfItemOptionsState[languageCode][indexToUpdate2].score =
              // if value contains more than 1 char + first char is not "0" + second char is not "." ==> display current value minus the first char
              // else display current value
              event.target.value === ""
                ? "0"
                : event.target.value.length > 1 &&
                    event.target.value.charAt(0) === "0" &&
                    event.target.value.charAt(1) !== "."
                  ? event.target.value.substring(1)
                  : event.target.value;
            // EXCLUDE FROM SHUFFLE
          } else if (variableName === CHANGE_TYPE.excludeFromShuffle) {
            // updating historical_option_id (from copyOfItemOptionsState)
            copyOfItemOptionsState[languageCode][indexToUpdate2].exclude_from_shuffle = event;
          }
        }

        // updating itemOptionsState state
        this.setState({ itemOptionsState: copyOfItemOptionsState });

        // Create a buffer before updating the redux state
        // Clear the old typingTimer
        clearTimeout(this.state.typingTimer);

        // Change the typingTimer to a timeout of 300 millisec
        // When user stops writing, this will go through after that time
        // Otherwise, the timer will be reset to that time
        const newTimer = setTimeout(() => {
          // updating the redux states
          const new_obj = {
            ...this.props.itemData,
            item_options: this.state.itemOptionsState
          };

          // setting new itemData redux state
          this.props.setItemData(new_obj, this.props.languageData);
        }, 300);
        this.setState({ typingTimer: newTimer });

        // unilangual item bank
      } else {
        // getting language code
        let languageCode = null;
        for (let i = 0; i < this.props.languageData.length; i++) {
          if (
            this.props.languageData[i].language_id ===
            this.props.item_bank_data.item_bank_definition[0].language_id
          ) {
            languageCode = this.props.languageData[i].ISO_Code_1;
            break;
          }
        }

        // creating deep copy of itemContentState and getting index to update
        const copyOfItemOptionsState = _.cloneDeep(this.state.itemOptionsState);
        const indexToUpdate2 = this.state.itemOptionsState[languageCode].findIndex(obj => {
          return (
            obj.option_type === ITEM_OPTION_TYPE.multipleChoice && obj.id === itemOptionData.id
          );
        });

        // HISTORICAL OPTION ID
        if (variableName === CHANGE_TYPE.historicalOptionId) {
          // updating historical_option_id (from copyOfItemOptionsState)
          copyOfItemOptionsState[languageCode][indexToUpdate2].historical_option_id =
            event.target.value.toUpperCase();
          // RATIONALE
        } else if (variableName === CHANGE_TYPE.rationale) {
          // updating rationale (from copyOfItemOptionsState)
          copyOfItemOptionsState[languageCode][indexToUpdate2].rationale = event.target.value;
          // SCORE
        } else if (variableName === CHANGE_TYPE.score) {
          // updating score (from copyOfItemOptionsState)
          // if blank ==> display "0"
          copyOfItemOptionsState[languageCode][indexToUpdate2].score =
            // if value contains more than 1 char + first char is not "0" + second char is not "." ==> display current value minus the first char
            // else display current value
            event.target.value === ""
              ? "0"
              : event.target.value.length > 1 &&
                  event.target.value.charAt(0) === "0" &&
                  event.target.value.charAt(1) !== "."
                ? event.target.value.substring(1)
                : event.target.value;
          // EXCLUDE FROM SHUFFLE
        } else if (variableName === CHANGE_TYPE.excludeFromShuffle) {
          // updating historical_option_id (from copyOfItemOptionsState)
          copyOfItemOptionsState[languageCode][indexToUpdate2].exclude_from_shuffle = event;
        }

        // updating itemOptionsState state
        this.setState({ itemOptionsState: copyOfItemOptionsState });

        // Create a buffer before updating the redux state
        // Clear the old typingTimer
        clearTimeout(this.state.typingTimer);

        // Change the typingTimer to a timeout of 300 millisec
        // When user stops writing, this will go through after that time
        // Otherwise, the timer will be reset to that time
        const newTimer = setTimeout(() => {
          // updating the redux states
          const new_obj = {
            ...this.props.itemData,
            item_options: this.state.itemOptionsState
          };

          // setting new itemData redux state
          this.props.setItemData(new_obj, this.props.languageData);
        }, 300);
        this.setState({ typingTimer: newTimer });
      }
    }
  };

  // Add item option
  addOptionItem = () => {
    // creating deep copy of item_options
    const copyOfItemOptions = _.cloneDeep(this.props.itemData.item_options);

    // bilingual item bank
    if (this.props.item_bank_data.item_bank_definition[0].language_id === null) {
      // looping in languageData
      for (let i = 0; i < this.props.languageData.length; i++) {
        // intitializing newMap every iteration
        let newMap = [];

        // copyOfItemOptions in language of current iteration is defined
        if (typeof copyOfItemOptions[this.props.languageData[i].ISO_Code_1] !== "undefined") {
          // setting newMap
          newMap = [...copyOfItemOptions[this.props.languageData[i].ISO_Code_1]];
        }

        // Building the new item
        const newItem = {
          // note that if you update this ID, you'll need to update it in the backend as well (needed for draggable logic)
          // file ref: ...\backend\db_views\serializers\item_bank_view_serializers.py
          id: `item-option-${this.props.languageData[i].ISO_Code_1}-${
            newMap.filter(obj => {
              return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
            }).length + 1
          }`,
          item_id: this.props.itemData.item_id,
          score: "0",
          option_type: ITEM_OPTION_TYPE.multipleChoice,
          item_type: DRAGGABLE_ITEM_TYPE.TestOptionComponent,
          option_order:
            newMap.filter(obj => {
              return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
            }).length + 1,
          exclude_from_shuffle: false,
          language_id: this.props.languageData[i].language_id,
          text: "",
          rationale: "",
          historical_option_id: ""
        };

        newMap.push(newItem);
        copyOfItemOptions[this.props.languageData[i].ISO_Code_1] = newMap;
      }
      // updating redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.itemData,
        item_options: copyOfItemOptions
      };

      // updating itemOptionsState
      this.setState({ itemOptionsState: new_obj.item_options });

      // setting new itemData redux state
      this.props.setItemData(new_obj, this.props.languageData);

      // unilangual item bank
    } else {
      let newMap = [];

      // getting language code
      let languageCode = null;
      for (let i = 0; i < this.props.languageData.length; i++) {
        if (
          this.props.languageData[i].language_id ===
          this.props.item_bank_data.item_bank_definition[0].language_id
        ) {
          languageCode = this.props.languageData[i].ISO_Code_1;
          break;
        }
      }

      if (typeof this.props.itemData.item_options[languageCode] !== "undefined") {
        newMap = copyOfItemOptions[languageCode];
      }

      // Building the new item
      const newItem = {
        // note that if you update this ID, you'll need to update it in the backend as well (needed for draggable logic)
        // file ref: ...\backend\db_views\serializers\item_bank_view_serializers.py
        id: `item-option-${languageCode}-${
          newMap.filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
          }).length + 1
        }`,
        item_id: this.props.itemData.item_id,
        score: "0",
        option_type: ITEM_OPTION_TYPE.multipleChoice,
        item_type: DRAGGABLE_ITEM_TYPE.TestOptionComponent,
        option_order:
          newMap.filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
          }).length + 1,
        exclude_from_shuffle: false,
        language_id: this.props.item_bank_data.item_bank_definition[0].language_id,
        text: "",
        rationale: "",
        historical_option_id: ""
      };

      newMap.push(newItem);

      // updating the redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.itemData,
        item_options: {
          ...this.props.itemData.item_options,
          [languageCode]: newMap
        }
      };

      // updating itemOptionsState
      this.setState({ itemOptionsState: new_obj.item_options });

      // setting new itemData redux state
      this.props.setItemData(new_obj, this.props.languageData);
    }
  };

  // Remove stem item from stemSectionItems
  removeOptionItem = index => {
    // bilingual item bank
    if (this.props.item_bank_data.item_bank_definition[0].language_id === null) {
      // creating deep copy of item_option
      const copyOfItemOptions = _.cloneDeep(this.props.itemData.item_options);
      // looping in languageData
      for (let i = 0; i < this.props.languageData.length; i++) {
        // making sure that copyOfItemOptions in language of current iteration is defined (can be undefined if number of options are not the same in all languages - was possible with old logic)
        if (typeof copyOfItemOptions[this.props.languageData[i].ISO_Code_1] !== "undefined") {
          const filteredItemOptions = copyOfItemOptions[
            this.props.languageData[i].ISO_Code_1
          ].filter(obj => {
            return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
          });

          // deleting respective item option
          filteredItemOptions.splice(index, 1);

          // re-ordering options order attributes
          // initializing finalMap with non multiple choice content
          const finalMap = copyOfItemOptions[this.props.languageData[i].ISO_Code_1].filter(obj => {
            return obj.option_type !== ITEM_OPTION_TYPE.multipleChoice;
          });
          for (let j = 0; j < filteredItemOptions.length; j++) {
            const data = filteredItemOptions[j];
            // note that if you update this ID, you'll need to update it in the backend as well (needed for draggable logic)
            // file ref: ...\backend\db_views\serializers\item_bank_serializers.py
            data.id = `item-option-${this.props.languageData[i].ISO_Code_1}-${j + 1}`;
            data.option_order = j + 1;
            finalMap.push(data);
          }
          copyOfItemOptions[this.props.languageData[i].ISO_Code_1] = finalMap;
        }
      }

      // updating redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.itemData,
        item_options: copyOfItemOptions
      };

      // updating itemOptionsState
      this.setState({ itemOptionsState: new_obj.item_options });

      // setting new itemData redux state
      this.props.setItemData(new_obj, this.props.languageData);

      // unilangual item bank
    } else {
      // getting language code
      let languageCode = null;
      for (let i = 0; i < this.props.languageData.length; i++) {
        if (
          this.props.languageData[i].language_id ===
          this.props.item_bank_data.item_bank_definition[0].language_id
        ) {
          languageCode = this.props.languageData[i].ISO_Code_1;
          break;
        }
      }

      const newMap = [
        ...this.props.itemData.item_options[languageCode].filter(obj => {
          return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
        })
      ];

      newMap.splice(index, 1);

      // re-ordering content order attributes
      // initializing finalMap with non multiple choice content
      const finalMap = this.props.itemData.item_options[languageCode].filter(obj => {
        return obj.option_type !== ITEM_OPTION_TYPE.multipleChoice;
      });
      for (let i = 0; i < newMap.length; i++) {
        const data = newMap[i];
        // note that if you update this ID, you'll need to update it in the backend as well (needed for draggable logic)
        // file ref: ...\backend\db_views\serializers\item_bank_serializers.py
        data.id = `item-option-${languageCode}-${i + 1}`;
        data.option_order = i + 1;
        finalMap.push(data);
      }

      // updating the redux states
      // ref: https://redux.js.org/usage/structuring-reducers/immutable-update-patterns
      const new_obj = {
        ...this.props.itemData,
        item_options: {
          ...this.props.itemData.item_options,
          [languageCode]: finalMap
        }
      };

      // updating itemOptionsState
      this.setState({ itemOptionsState: new_obj.item_options });

      // setting new itemData redux state
      this.props.setItemData(new_obj, this.props.languageData);
    }
  };

  /*--------------------------*/
  //                          //
  //        Styling           //
  //                          //
  /*--------------------------*/

  // Draggable Styling
  getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: "none",

    // change background colour if dragging
    background: isDragging ? "#0099A8" : "white",
    color: "#00565e",

    // styles we need to apply on draggables
    ...draggableStyle
  });

  // Droppable Styling
  getListStyle = isDraggingOver => ({
    background: isDraggingOver ? "#cdcdcd" : "white",
    padding: "12px 24px 12px 24px"
  });

  render() {
    // calculating the height of the top tabs and above (without considering the nav bar)
    // =========================================================================================
    let heightOfBackToItemBankSelectionButton = 0;
    let heightOfTopTabs = 0;
    let heightOfFooter = 0;
    let topPageHeight = 0;
    if (document.getElementById("back-to-item-bank-editor-btn") != null) {
      heightOfBackToItemBankSelectionButton = document.getElementById(
        "back-to-item-bank-editor-btn"
      ).offsetHeight;
      if (document.getElementsByClassName("nav nav-tabs").length > 0) {
        heightOfTopTabs = document.getElementsByClassName("nav nav-tabs")[0].offsetHeight;
      }
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    topPageHeight += heightOfBackToItemBankSelectionButton + heightOfTopTabs + 12; // height of "back to item bank selection button" + height of margin under button

    if (document.getElementById("item-bank-footer-main-div") !== null) {
      heightOfFooter = document.getElementById("item-bank-footer-main-div").offsetHeight;
    } else {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }

    const customHeight = getItemBankHeightCalculations(
      this.props.accommodations.fontSize,
      this.props.accommodations.spacing,
      this.props.testNavBarHeight,
      topPageHeight,
      heightOfFooter
    );
    // =========================================================================================

    // getting language code
    let { languageCode } = this.state.selectedLanguageOption;
    for (let i = 0; i < this.props.languageData.length; i++) {
      if (
        this.props.languageData[i].language_id ===
        this.props.item_bank_data.item_bank_definition[0].language_id
      ) {
        languageCode = this.props.languageData[i].ISO_Code_1;
        break;
      }
    }

    return (
      <div style={{ ...styles.mainContainer, ...customHeight.height }}>
        {/* bilingual item bank */}
        {this.props.item_bank_data.item_bank_definition[0].language_id === null && (
          <Container style={styles.container}>
            <Row className="justify-content-end align-items-center">
              <Col xs={3} sm={3} md={3} lg={3} xl={3}>
                <label
                  htmlFor="language-react-select-dropdown"
                  id="language-dropdown-label"
                  style={styles.title}
                >
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                      .content.languageDropdownLabel
                  }
                </label>
              </Col>
              <Col xs={8} sm={8} md={8} lg={8} xl={8}>
                <div>
                  <div style={styles.dropdownContainer}>
                    <DropdownSelect
                      idPrefix="language"
                      onChange={this.getSelectedLanguage}
                      defaultValue={this.state.selectedLanguageOption}
                      options={this.state.languageOptions}
                      ariaLabelledBy="language-dropdown-label"
                      orderByLabels={true}
                    />
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        )}

        {!this.state.isInstruction ? (
          <Container style={styles.container}>
            <Row className="justify-content-end">
              <Col xs={11} sm={11} md={11} lg={11} xl={11}>
                <div style={styles.title}>
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                      .content.stemSections
                  }
                </div>
              </Col>
            </Row>
          </Container>
        ) : (
          <Container style={styles.container}>
            <Row className="justify-content-end">
              <Col xs={11} sm={11} md={11} lg={11} xl={11}>
                <div style={styles.title}>
                  {
                    LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                      .content.instructionSections
                  }
                </div>
              </Col>
            </Row>
          </Container>
        )}

        <DragDropContext
          onDragEnd={result => {
            this.onDragEndStemSection(result);
          }}
        >
          <Droppable droppableId="stem-section-droppable">
            {(provided, snapshot) => (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
                style={this.getListStyle(snapshot.isDraggingOver)}
              >
                {typeof this.state.itemContentState[languageCode] !== "undefined" &&
                  this.state.itemContentState[languageCode]
                    .filter(obj => {
                      return obj.content_type === ITEM_CONTENT_TYPE.markdown;
                    })
                    .map((item, index) => (
                      <CustomDraggable
                        key={`item-content-key-${index}`}
                        item={item}
                        index={index}
                        getItemStyle={this.getItemStyle}
                        onTextAreaComponentChange={e =>
                          this.onTextAreaComponentChange(e, index, item)
                        }
                        deleteItem={() => this.removeStemItem(index)}
                        customDeleteButtonClass="align-items-center"
                      />
                    ))}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>

        {!this.state.isInstruction ? (
          <Container style={styles.container}>
            <Row className="justify-content-end">
              <Col xs={11} sm={11} md={11} lg={11} xl={11}>
                <CustomButton
                  className="d-flex align-items-center"
                  label={
                    <>
                      <FontAwesomeIcon icon={faPlusCircle} />
                      <span id="add-new-stem" className="notranslate" style={styles.buttonText}>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.content.addStemButton
                        }
                      </span>
                    </>
                  }
                  action={this.addStemItem}
                  buttonTheme={THEME.PRIMARY}
                />
              </Col>
            </Row>
          </Container>
        ) : (
          <Container style={styles.container}>
            <Row className="justify-content-end">
              <Col xs={11} sm={11} md={11} lg={11} xl={11}>
                <CustomButton
                  className="d-flex align-items-center"
                  label={
                    <>
                      <FontAwesomeIcon icon={faPlusCircle} />
                      <span id="add-new-stem" className="notranslate" style={styles.buttonText}>
                        {
                          LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                            .items.content.addInstructionButton
                        }
                      </span>
                    </>
                  }
                  action={this.addStemItem}
                  buttonTheme={THEME.PRIMARY}
                />
              </Col>
            </Row>
          </Container>
        )}

        {!this.state.isInstruction && (
          <div>
            <Container style={styles.container}>
              <Row className="justify-content-end" style={{ marginTop: "24px" }}>
                <Col xs={11} sm={11} md={11} lg={11} xl={11}>
                  <div style={styles.title}>
                    {
                      LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor.items
                        .content.options
                    }
                  </div>
                </Col>
              </Row>
            </Container>

            <DragDropContext
              onDragEnd={result => {
                this.onDragEndOptionSection(result);
              }}
            >
              <Droppable droppableId="option-section-droppable">
                {(provided, snapshot) => (
                  <div
                    {...provided.droppableProps}
                    ref={provided.innerRef}
                    style={this.getListStyle(snapshot.isDraggingOver)}
                  >
                    {typeof this.state.itemOptionsState[languageCode] !== "undefined" &&
                      this.state.itemOptionsState[languageCode]
                        .filter(obj => {
                          return obj.option_type === ITEM_OPTION_TYPE.multipleChoice;
                        })
                        .map((item, index) => (
                          <CustomDraggable
                            key={`item-option-key-${index}`}
                            item={item}
                            index={index}
                            getItemStyle={this.getItemStyle}
                            onTextInputChange={e => this.onTextInputChange(e, index, item)}
                            onHistoricalOptionIdInputChange={e =>
                              this.onHistoricalOptionIdInputChange(e, index, item)
                            }
                            onRationaleInputChange={e =>
                              this.onRationaleInputChange(e, index, item)
                            }
                            onScoreInputChange={e => this.onScoreInputChange(e, index, item)}
                            changeExcludeFromShuffle={e =>
                              this.changeExcludeFromShuffle(e, index, item)
                            }
                            deleteItem={() => this.removeOptionItem(index)}
                            customDeleteButtonClass="align-items-center"
                          />
                        ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>

            <Container style={styles.container}>
              <Row className="justify-content-end">
                <Col xs={11} sm={11} md={11} lg={11} xl={11}>
                  <CustomButton
                    className="d-flex align-items-center"
                    label={
                      <>
                        <FontAwesomeIcon icon={faPlusCircle} />
                        <span
                          id="candidate-check-in"
                          className="notranslate"
                          style={styles.buttonText}
                        >
                          {
                            LOCALIZE.testBuilder.testDefinition.selectTestDefinition.itemBankEditor
                              .items.content.addOptionButton
                          }
                        </span>
                      </>
                    }
                    action={this.addOptionItem}
                    buttonTheme={THEME.PRIMARY}
                  />
                </Col>
              </Row>
            </Container>
          </div>
        )}
      </div>
    );
  }
}

export { ContentTab as unconnectedContentTab };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    languageData: state.localize.languageData,
    accommodations: state.accommodations,
    itemData: state.itemBank.itemData,
    item_bank_data: state.itemBank.item_bank_data,
    testNavBarHeight: state.testSection.testNavBarHeight,
    currentTab: state.navTabs.currentTopTab
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setItemData,
      setSelectedContentLanguage
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ContentTab);
