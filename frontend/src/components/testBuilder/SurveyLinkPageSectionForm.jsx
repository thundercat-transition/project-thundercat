import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import {
  allValid,
  getNextInNumberSeries,
  makeLabel,
  makeSaveDeleteButtons,
  makeTextBoxField
} from "./helpers";
import {
  getItemBanksForTestBuilder,
  getAllItemBanks,
  getAllItems,
  getItemContent
} from "../../modules/ItemBankRedux";

import getItemContentToPreview from "../../helpers/itemBankItemPreview";
import { PageSectionType } from "../testFactory/Constants";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  addTestDefinitionField,
  PAGE_SECTION_DEFINITIONS
} from "../../modules/TestBuilderRedux";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { LANGUAGES } from "../commons/Translation";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";
import { faLanguage } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";

export const styles = {
  rowStyle: {
    margin: "5px"
  },
  deleteApplyButtonContainer: {
    marginTop: 24
  },
  allUnset: {
    all: "unset"
  },
  translateButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: 0,
    margin: "0 3px",
    color: "#00565e"
  },
  translationIconContainer: {
    position: "absolute",
    right: 0,
    padding: "0 12px"
  }
};

class SurveyLinkPageSectionForm extends Component {
  state = {
    textToDisplayEn:
      typeof this.props.pageDefinition.text_to_display_en !== "undefined"
        ? this.props.pageDefinition.text_to_display_en
        : "",
    textToDisplayFr:
      typeof this.props.pageDefinition.text_to_display_fr !== "undefined"
        ? this.props.pageDefinition.text_to_display_fr
        : "",
    linkValue:
      typeof this.props.pageDefinition.link_value !== "undefined"
        ? this.props.pageDefinition.link_value
        : "",
    showTranslationPopup: false
  };

  componentDidMount = () => {};

  handleSave = () => {
    if (allValid(this.state)) {
      const objArray = this.props.pageSectionDefinitions.filter(
        obj => obj.page_section !== this.props.componentPageSection.id
      );

      this.props.replaceTestDefinitionField(PAGE_SECTION_DEFINITIONS, objArray);

      const newObj = {
        page_section: this.props.componentPageSection.id,
        page_section_type: PageSectionType.SURVEY_LINK,
        text_to_display_en: this.state.textToDisplayEn,
        text_to_display_fr: this.state.textToDisplayFr,
        link_value: this.state.linkValue
      };

      newObj.id = getNextInNumberSeries(this.props.pageSectionDefinitions, "id");

      this.props.addTestDefinitionField(PAGE_SECTION_DEFINITIONS, newObj, newObj.id);

      this.props.handleSave(PageSectionType.SURVEY_LINK);
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete();
    this.closeDialog();
  };

  onTextToDisplayChange = (name, value, language) => {
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(value)) {
      // provided language is in English
      if (language === LANGUAGES.english) {
        this.setState({ textToDisplayEn: value });
        // provided language is in French
      } else {
        this.setState({ textToDisplayFr: value });
      }
    }
  };

  openTranslationsPopup = () => {
    this.setState({ showTranslationPopup: true });
  };

  closeTranslationsPopup = () => {
    this.setState({ showTranslationPopup: false });
  };

  onLinkValueChange = (name, value) => {
    // allow maximum of 300 chars
    const regexExpression = /^(.{0,300})$/;
    if (regexExpression.test(value)) {
      this.setState({ linkValue: value });
    }
  };

  render() {
    return (
      <div id="unit-test-markdown-page-section">
        <Row style={styles.rowStyle}>
          {makeLabel("survey_link_text_to_display", LOCALIZE.testBuilder.componentPageSections)}
          {makeTextBoxField(
            "survey_link_text_to_display",
            LOCALIZE.testBuilder.componentPageSections,
            this.props.currentLanguage === LANGUAGES.english
              ? this.state.textToDisplayEn
              : this.state.textToDisplayFr,
            true,
            (name, value, language) =>
              this.onTextToDisplayChange(name, value, this.props.currentLanguage)
          )}
          <div style={styles.translationIconContainer}>
            <StyledTooltip
              id={`survey-link-text-to-display-translations`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    className="h-100"
                    dataTip=""
                    dataFor={`survey-link-text-to-display-translations`}
                    label={<FontAwesomeIcon icon={faLanguage} />}
                    ariaLabel={
                      LOCALIZE.testBuilder.componentPageSections.survey_link_text_to_display
                        .translationButtonTooltip
                    }
                    action={() => this.openTranslationsPopup()}
                    customStyle={{
                      ...styles.translateButton,
                      fontSize: this.props.accommodations.fontSize.replace("px", "") * 1.5
                    }}
                    buttonTheme={THEME.SECONDARY}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {
                      LOCALIZE.testBuilder.componentPageSections.survey_link_text_to_display
                        .translationButtonTooltip
                    }
                  </p>
                </div>
              }
            />
          </div>
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("survey_link_value", LOCALIZE.testBuilder.componentPageSections)}
          {makeTextBoxField(
            "survey_link_value",
            LOCALIZE.testBuilder.componentPageSections,
            this.state.linkValue,
            true,
            this.onLinkValueChange
          )}
        </Row>

        <Row className="justify-content-between" style={styles.deleteApplyButtonContainer}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            !(
              this.state.textToDisplayEn !== "" &&
              this.state.textToDisplayFr !== "" &&
              this.state.linkValue !== ""
            )
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed. Are you sure you want to
                delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
        {/* TODO: Create a new Component for Translations PopupBox */}
        <PopupBox
          show={this.state.showTranslationPopup}
          title={
            LOCALIZE.testBuilder.componentPageSections.survey_link_text_to_display.translationPopup
              .title
          }
          handleClose={() => {}}
          description={
            <div>
              <div style={styles.translationsAttributeNameFields}>
                {LOCALIZE.getAvailableLanguages().map(language => (
                  <Row style={styles.rowStyle}>
                    {makeLabel(
                      `${language}_name`,
                      LOCALIZE.testBuilder.componentPageSections.survey_link_text_to_display
                        .translationPopup,
                      "",
                      {},
                      false
                    )}
                    {makeTextBoxField(
                      `${language}_name`,
                      LOCALIZE.testBuilder.componentPageSections.survey_link_text_to_display
                        .translationPopup,
                      language === LANGUAGES.english
                        ? this.state.textToDisplayEn
                        : this.state.textToDisplayFr,
                      true,
                      (name, value, customLanguage) =>
                        this.onTextToDisplayChange(name, value, language)
                    )}
                  </Row>
                ))}
              </div>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.closeTranslationsPopup()}
        />
      </div>
    );
  }
}

export { SurveyLinkPageSectionForm as UnconnectedSurveyLinkPageSectionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    pageSectionDefinitions: state.testBuilder.page_section_definitions,
    pageSections: state.testBuilder.page_sections
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getItemBanksForTestBuilder,
      getAllItemBanks,
      getAllItems,
      getItemContent,
      getItemContentToPreview,
      modifyTestDefinitionField,
      replaceTestDefinitionField,
      addTestDefinitionField
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SurveyLinkPageSectionForm);
