import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Button, Row, Container } from "react-bootstrap";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import {
  makeDropDownField,
  makeLabel,
  getNextInNumberSeries,
  collapsingItemMoveUpFunctionality,
  collapsingItemMoveDownFunctionality
} from "./helpers";
import SectionComponentPageForm from "./SectionComponentPageForm";
import {
  SECTION_COMPONENT_PAGES,
  PAGE_SECTIONS,
  PAGE_SECTION_DEFINITIONS,
  replaceTestDefinitionField,
  addTestDefinitionField,
  replaceTestDefinitionArray,
  setTestDefinitionValidationErrors
} from "../../modules/TestBuilderRedux";
import { sectionComponentPagesValidation } from "./validation";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  label: {
    padding: "3px 0 0 4px"
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red"
  },
  rowStyle: {
    margin: "5px"
  },
  validationErrorMessageContainer: {
    border: "1px solid #923534",
    padding: 12,
    marginBottom: 12
  },
  validationErrorMessage: {
    textAlign: "center",
    color: "#923534",
    fontWeight: "bold"
  }
};

class SectionComponentPages extends Component {
  state = {
    testSections: [
      {
        order: 1,
        en_title: "temp",
        fr_title: "FR temp",
        test_section_component: ""
      }
    ],
    selectedParentTestSection: "",
    selectedParentTestSectionComponent: "",
    showMustContainPageContentTitlesError: false
  };

  componentDidMount = () => {
    // getting orders
    this.getSectionComponentPagesOrders();
    // get validation errors
    this.getValidationErrors();
  };

  componentDidUpdate = prevProps => {
    // if length of sectionComponentPages gets updated
    if (prevProps.sectionComponentPages.length !== this.props.sectionComponentPages.length) {
      // getting orders
      this.getSectionComponentPagesOrders();
      // calling section component pages (page content title) validation
      this.callSectionComponentPagesValidation();
      // get validation errors
      this.getValidationErrors();
    }
    // if test definition field gets updated
    if (
      prevProps.triggerTestDefinitionFieldUpdates !== this.props.triggerTestDefinitionFieldUpdates
    ) {
      // getting orders
      this.getSectionComponentPagesOrders();
      // get validation errors
      this.getValidationErrors();
    }
    // if validationErrors gets updated
    if (prevProps.validationErrors !== this.props.validationErrors) {
      // get validation errors
      this.getValidationErrors();
    }
    // if testSectionComponents gets updated
    if (prevProps.testSectionComponents !== this.props.testSectionComponents) {
      // get validation errors
      this.getValidationErrors();
    }
  };

  callSectionComponentPagesValidation = () => {
    // calling section component pages (page content titles) validation
    const validationErrors = sectionComponentPagesValidation(
      this.props.validationErrors,
      this.props.testSections,
      this.props.testSectionComponents,
      this.props.sectionComponentPages
    );
    // setting redux test definition validation errors
    this.props.setTestDefinitionValidationErrors(validationErrors);
  };

  getValidationErrors = () => {
    // initializing needed variables
    let showMustContainPageContentTitlesError = false;

    // making sure the the validation errors state is defined
    if (typeof this.props.validationErrors !== "undefined") {
      // question rules error
      if (!this.props.validationErrors.testDefinitionContainsValidSectionComponentPages) {
        showMustContainPageContentTitlesError = true;
      }
    }
    // updating needed states
    this.setState({
      showMustContainPageContentTitlesError: showMustContainPageContentTitlesError
    });
  };

  addTestSectionComponent = () => {
    const newObj = {
      order: 1,
      en_title: "",
      fr_title: "",
      test_section_component: this.state.selectedParentTestSectionComponent.value
    };
    newObj.id = getNextInNumberSeries(this.props.sectionComponentPages, "id");
    this.props.addTestDefinitionField(SECTION_COMPONENT_PAGES, newObj, 1);
    // getting orders
    this.getSectionComponentPagesOrders();
    // calling section component pages (page content title) validation
    this.callSectionComponentPagesValidation();
    // get validation errors
    this.getValidationErrors();
  };

  makeTestSectionOptions = (array, lang) => {
    return array.map(item => {
      return { label: item[`${lang}_title`], value: item.id };
    });
  };

  makeTestSectionComponentOptions = (array, lang) => {
    return array
      .filter(item => item.test_section.indexOf(this.state.selectedParentTestSection.value) >= 0)
      .map(item => {
        if (item.test_section.indexOf(this.state.selectedParentTestSection.value) >= 0) {
          return { label: item[`${lang}_title`], value: item.id };
        }
        return null;
      });
  };

  selectParentTestSection = event => {
    if (event !== this.state.selectedParentTestSection)
      this.setState({
        selectedParentTestSection: event,
        selectedParentTestSectionComponent: ""
      });
  };

  selectParentTestSectionComponent = event => {
    this.setState({ selectedParentTestSectionComponent: event }, () => {
      // getting orders
      this.getSectionComponentPagesOrders();
    });
  };

  makeHeader = customFontSize => {
    const tsOptions = this.makeTestSectionOptions(
      this.props.testSections,
      this.props.currentLanguage
    );
    let tscOptions = [];
    if (this.state.selectedParentTestSection !== "") {
      tscOptions = this.makeTestSectionComponentOptions(
        this.props.testSectionComponents,
        this.props.currentLanguage
      );
    }
    return (
      <>
        <h2>{LOCALIZE.testBuilder.sectionComponentPages.title}</h2>
        {this.state.showMustContainPageContentTitlesError && (
          <div style={styles.validationErrorMessageContainer}>
            <p style={{ ...styles.validationErrorMessage, ...customFontSize }}>
              {
                LOCALIZE.testBuilder.sectionComponentPages.validationErrors
                  .mustContainPageContentTitlesError
              }
            </p>
          </div>
        )}
        <p>{LOCALIZE.testBuilder.sectionComponentPages.description}</p>
        <Container>
          <Row style={styles.rowStyle}>
            {makeLabel("parentTestSection", LOCALIZE.testBuilder.testSectionComponents)}
            {makeDropDownField(
              "parentTestSection",
              LOCALIZE.testBuilder.testSectionComponents,
              this.state.selectedParentTestSection,
              tsOptions,
              this.selectParentTestSection
            )}
          </Row>
          <Row style={styles.rowStyle}>
            {makeLabel("parentTestSectionComponent", LOCALIZE.testBuilder.sectionComponentPages)}
            {makeDropDownField(
              "parentTestSectionComponent",
              LOCALIZE.testBuilder.sectionComponentPages,
              this.state.selectedParentTestSectionComponent,
              tscOptions,
              this.selectParentTestSectionComponent,
              this.state.selectedParentTestSection === ""
            )}
          </Row>
        </Container>
        <Button
          variant={"primary"}
          onClick={this.addTestSectionComponent}
          disabled={this.state.selectedParentTestSectionComponent === ""}
        >
          <FontAwesomeIcon icon={faPlusCircle} style={{ margins: "auto", marginRight: "5px" }} />
          {LOCALIZE.testBuilder.sectionComponentPages.addButton}
        </Button>
        <div style={styles.borderBox} />
      </>
    );
  };

  handleDelete = id => {
    // delete the section component page
    let objArray = this.props.sectionComponentPages.filter(obj => obj.id !== id);
    this.props.replaceTestDefinitionField(SECTION_COMPONENT_PAGES, objArray);
    // delete the page section
    const pageSectionDefinitionsToDelete = [];
    objArray = this.props.pageSections.filter(obj => {
      if (obj.section_component_page !== id) {
        return obj;
      }
      pageSectionDefinitionsToDelete.push(obj.id);
      return null;
    });
    this.props.replaceTestDefinitionField(PAGE_SECTIONS, objArray);
    // delete the page section definitions
    objArray = this.props.pageSectionDefinitions.filter(
      obj => pageSectionDefinitionsToDelete.indexOf(obj.page_section) === -1
    );
    this.props.replaceTestDefinitionField(PAGE_SECTION_DEFINITIONS, objArray);
  };

  getPageSections = section => {
    return this.props.pageSections.filter(obj => {
      if (obj.section_component_page === section.id) {
        return obj;
      }
      return null;
    });
  };

  handleMoveUp = index => {
    // getting new section component page array
    collapsingItemMoveUpFunctionality(index, this.props.sectionComponentPages)
      .then(objArray => {
        // updating redux states
        this.props.replaceTestDefinitionArray(SECTION_COMPONENT_PAGES, objArray);
      })
      .then(() => {
        // getting orders
        this.getSectionComponentPagesOrders();
      });
  };

  handleMoveDown = index => {
    // getting new section component page array
    collapsingItemMoveDownFunctionality(index, this.props.sectionComponentPages)
      .then(objArray => {
        // updating redux states
        this.props.replaceTestDefinitionArray(SECTION_COMPONENT_PAGES, objArray);
      })
      .then(() => {
        // getting orders
        this.getSectionComponentPagesOrders();
      });
  };

  getSectionComponentPagesOrders = () => {
    const { sectionComponentPages } = this.props;
    // initializing sectionComponentPages array
    const newSectionComponentPages = [];

    // grouping array by section component page
    // source: https://edisondevadoss.medium.com/javascript-group-an-array-of-objects-by-key-afc85c35d07e
    const grouped_by_test_section_component = sectionComponentPages.reduce((r, a) => {
      // eslint-disable-next-line no-param-reassign
      r[a.test_section_component] = [...(r[a.test_section_component] || []), a];
      return r;
    }, {});
    const grouped_by_test_section_component_array = Object.keys(
      grouped_by_test_section_component
    ).map(key => [grouped_by_test_section_component[key]]);

    // looping in grouped_by_test_section_component_array
    for (let i = 0; i < grouped_by_test_section_component_array.length; i++) {
      // looping in array of current grouped_by_test_section_component_array
      for (let j = 0; j < grouped_by_test_section_component_array[i][0].length; j++) {
        // pushing new object (with the right order) in newSectionComponentPages array
        newSectionComponentPages.push({
          id: grouped_by_test_section_component_array[i][0][j].id,
          en_title: grouped_by_test_section_component_array[i][0][j].en_title,
          fr_title: grouped_by_test_section_component_array[i][0][j].fr_title,
          test_section_component:
            grouped_by_test_section_component_array[i][0][j].test_section_component,
          order: j + 1
        });
      }
    }
    // updating redux state
    this.props.replaceTestDefinitionArray(SECTION_COMPONENT_PAGES, newSectionComponentPages);
  };

  render() {
    const { sectionComponentPages } = this.props;

    const customFontSize = {
      // adding 5px to current font size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 5}px`
    };

    return (
      <div style={styles.mainContainer}>
        {this.makeHeader(customFontSize)}

        {this.state.selectedParentTestSectionComponent !== "" &&
          sectionComponentPages.map((section, index) => {
            if (
              section.test_section_component === this.state.selectedParentTestSectionComponent.value
            ) {
              const pageSections = this.getPageSections(section);
              return (
                <CollapsingItemContainer
                  key={index}
                  index={index}
                  usesMoveUpAndDownFunctionalities={true}
                  moveUpAction={() => this.handleMoveUp(index)}
                  moveUpButtonDisabled={section.order === 1}
                  moveDownAction={() => this.handleMoveDown(index)}
                  moveDownButtonDisabled={
                    index === sectionComponentPages.length - 1
                      ? true
                      : sectionComponentPages[index + 1].order <= section.order
                  }
                  title={
                    <label>
                      {LOCALIZE.formatString(
                        LOCALIZE.testBuilder.sectionComponentPages.collapsableItemName,
                        section.order,
                        section[`${this.props.currentLanguage}_title`]
                      )}
                    </label>
                  }
                  body={
                    <SectionComponentPageForm
                      sectionComponentPage={section}
                      pageSections={pageSections}
                      INDEX={index}
                      handleDelete={this.handleDelete}
                    />
                  }
                />
              );
            }
            return null;
          })}
      </div>
    );
  }
}

export { SectionComponentPages as unconnectedSectionComponentPages };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testSections: state.testBuilder.test_sections,
    testSectionComponents: state.testBuilder.test_section_components,
    sectionComponentPages: state.testBuilder.section_component_pages,
    pageSections: state.testBuilder.page_sections,
    pageSectionDefinitions: state.testBuilder.page_section_definitions,
    triggerTestDefinitionFieldUpdates: state.testBuilder.triggerTestDefinitionFieldUpdates,
    validationErrors: state.testBuilder.validation_errors
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      replaceTestDefinitionField,
      addTestDefinitionField,
      replaceTestDefinitionArray,
      setTestDefinitionValidationErrors
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SectionComponentPages);
