import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Button, Row, Container, Col } from "react-bootstrap";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import {
  makeDropDownField,
  makeLabel,
  getNextInNumberSeries,
  makeMultiDropDownField,
  testSectionComponentLanguage,
  testLanguageHasEnglish,
  testLanguageHasFrench,
  styles,
  collapsingItemMoveUpFunctionality,
  collapsingItemMoveDownFunctionality
} from "./helpers";
import {
  replaceTestDefinitionField,
  addTestDefinitionField,
  QUESTIONS,
  EMAIL_DETAILS,
  EMAILS,
  MULTIPLE_CHOICE_QUESTION_DETAILS,
  QUESTION_SECTIONS,
  QUESTION_SECTION_DEFINITIONS,
  ANSWER_DETAILS,
  ANSWERS,
  replaceTestDefinitionArray,
  setTestDefinitionValidationErrors
} from "../../modules/TestBuilderRedux";
import { LANGUAGES } from "../../modules/LocalizeRedux";
import QuestionForm from "./QuestionForm";
import {
  QuestionType,
  QuestionDifficultyType,
  TestSectionComponentType
} from "../testFactory/Constants";
import cascadeDeleteQuestion from "../../modules/TestBuilderReduxUtility";
import questionListSubSectionValidation from "./validation";
import { removeHtmlFromString } from "../commons/reports/utils";

const componentStyles = {
  validationErrorMessageContainer: {
    border: "1px solid #923534",
    padding: 12,
    marginBottom: 12
  },
  validationErrorMessage: {
    textAlign: "center",
    color: "#923534",
    fontWeight: "bold"
  },
  boldText: {
    fontWeight: "bold"
  },
  nonBoldText: {
    fontWeight: "normal"
  }
};

class Questions extends Component {
  state = {
    selectedParentTestSection: "",
    selectedParentTestSectionComponent: "",
    selectedBlock: "",
    componentType: 0,
    showMissingQuestionsErrorMessage: false
  };

  componentDidMount = () => {
    // getting orders
    this.getQuestionsOrders();
    // get validation errors
    this.getValidationErrors();
  };

  componentDidUpdate = prevProps => {
    // if length of testSectionComponents or testSectionComponents gets updated
    if (
      prevProps.questions.length !== this.props.questions.length ||
      prevProps.testSectionComponents !== this.props.testSectionComponents
    ) {
      // getting orders
      this.getQuestionsOrders();
      // creating a copy of validationErrors and updating testDefinitionContainsValidQuestionLists attribute
      let copyOfValidationErrors = this.props.validationErrors;
      // calling question list sub-section validation
      copyOfValidationErrors = questionListSubSectionValidation(
        copyOfValidationErrors,
        this.props.testSectionComponents,
        this.props.questions
      );
      // updating redux states
      this.props.setTestDefinitionValidationErrors(copyOfValidationErrors);
      // get validation errors
      this.getValidationErrors();
    }
    // if test definition field gets updated
    if (
      prevProps.triggerTestDefinitionFieldUpdates !== this.props.triggerTestDefinitionFieldUpdates
    ) {
      // getting orders
      this.getQuestionsOrders();
    }
    // if any of the upload validation error state or questions gets updated
    if (
      prevProps.validationErrors !== this.props.validationErrors ||
      prevProps.questions !== this.props.questions
    ) {
      // get validation errors
      this.getValidationErrors();
    }
  };

  getValidationErrors = () => {
    // initializing needed variables
    let showMissingQuestionsErrorMessage = false;

    // making sure the the validation errors state is defined
    if (typeof this.props.validationErrors !== "undefined") {
      // scoring method error
      if (!this.props.validationErrors.testDefinitionContainsValidQuestionLists) {
        showMissingQuestionsErrorMessage = true;
      }
    }
    // updating needed states
    this.setState({
      showMissingQuestionsErrorMessage: showMissingQuestionsErrorMessage
    });
  };

  addQuestion = () => {
    const { componentType } = this.state;
    const testLanguage = testSectionComponentLanguage(
      this.state.selectedParentTestSectionComponent.value,
      this.props.testSectionComponents
    );

    let newQuestion = null;

    // add a new inbox question
    if (componentType === TestSectionComponentType.INBOX) {
      newQuestion = {
        question_type: QuestionType.EMAIL,
        pilot: false,
        test_section_component: this.state.selectedParentTestSectionComponent.value,
        dependencies: [],
        order: 1,
        dependent_order: 0,
        shuffle_answer_choices: false,
        ppc_question_id: ""
      };
      newQuestion.id = getNextInNumberSeries(this.props.questions, "id");
      this.props.addTestDefinitionField(QUESTIONS, newQuestion, newQuestion.id);
      // create new email question
      const newEmail = {
        question: newQuestion.id,
        from_field: 0,
        to_field: [],
        cc_field: []
      };
      newEmail.id = getNextInNumberSeries(this.props.emails, "id");
      newEmail.email_id = getNextInNumberSeries(this.props.emails, "email_id");
      this.props.addTestDefinitionField(EMAILS, newEmail, newEmail.email_id);

      // create test language email details stuff
      let newEmailDetails = {};
      if (testLanguageHasEnglish(testLanguage)) {
        newEmailDetails = {
          subject_field: "Temp",
          date_field: "Temp",
          body: "Temp",
          email_question: newEmail.id,
          language: LANGUAGES.english
        };
        newEmailDetails.id = getNextInNumberSeries(this.props.emailDetails, "id");
        this.props.addTestDefinitionField(EMAIL_DETAILS, newEmailDetails, newEmailDetails.id);
      }
      if (testLanguageHasFrench(testLanguage)) {
        newEmailDetails = {
          subject_field: "Temp",
          date_field: "Temp",
          body: "Temp",
          email_question: newEmail.id,
          language: LANGUAGES.french
        };
        newEmailDetails.id = getNextInNumberSeries(this.props.emailDetails, "id") + 1;
        this.props.addTestDefinitionField(EMAIL_DETAILS, newEmailDetails, newEmailDetails.id);
      }
    }
    if (componentType === TestSectionComponentType.QUESTION_LIST) {
      newQuestion = {
        question_type: QuestionType.MULTIPLE_CHOICE,
        pilot: false,
        test_section_component: this.state.selectedParentTestSectionComponent.value,
        dependencies: [],
        order: 1,
        dependent_order: 0,
        shuffle_answer_choices: false,
        ppc_question_id: ""
      };
      newQuestion.id = getNextInNumberSeries(this.props.questions, "id");
      this.props.addTestDefinitionField(QUESTIONS, newQuestion, newQuestion.id);

      const newMCQuestion = {
        question_difficulty_type: QuestionDifficultyType.EASY,
        question: newQuestion.id
      };
      newMCQuestion.id = getNextInNumberSeries(this.props.multipleChoiceQuestions, "id");
      this.props.addTestDefinitionField(
        MULTIPLE_CHOICE_QUESTION_DETAILS,
        newMCQuestion,
        newMCQuestion.id
      );
    }
    // getting orders
    this.getQuestionsOrders();

    // creating copy of questions
    const copyOfQuestions = this.props.questions;
    // adding new question to copyOfQuestions
    copyOfQuestions.push(newQuestion);
    // creating a copy of validationErrors and updating testDefinitionContainsValidQuestionLists attribute
    let copyOfValidationErrors = this.props.validationErrors;
    // calling question list sub-section validation
    copyOfValidationErrors = questionListSubSectionValidation(
      copyOfValidationErrors,
      this.props.testSectionComponents,
      copyOfQuestions
    );
    // updating redux states
    this.props.setTestDefinitionValidationErrors(copyOfValidationErrors);
  };

  onSelectChange = event => {
    this.setState({ selectedBlock: event }, () => {
      // getting orders
      this.getQuestionsOrders();
    });
  };

  makeTestSectionOptions = (array, lang) => {
    return array.map(item => {
      return { label: item[`${lang}_title`], value: item.id };
    });
  };

  makeTestSectionComponentOptions = (array, lang) => {
    const array2 = array
      .filter(item => item.test_section.indexOf(this.state.selectedParentTestSection.value) >= 0)
      .map(item => {
        if (item.test_section.indexOf(this.state.selectedParentTestSection.value) >= 0) {
          return { label: item[`${lang}_title`], value: item.id };
        }
        return null;
      });
    return array2;
  };

  selectParentTestSection = event => {
    if (event !== this.state.selectedParentTestSection) {
      this.setState({
        selectedParentTestSection: event,
        selectedParentTestSectionComponent: ""
      });
    }
  };

  selectParentTestSectionComponent = event => {
    let componentType = 0;
    for (const component of this.props.testSectionComponents) {
      if (component.id === event.value) {
        componentType = component.component_type;
      }
    }
    this.setState(
      { selectedParentTestSectionComponent: event, componentType: componentType },
      () => {
        // getting orders
        this.getQuestionsOrders();
      }
    );
  };

  // Creates the main Questions section (title, filters and Add Question button)
  makeHeader = (questions, customFontSize) => {
    const tsOptions = this.makeTestSectionOptions(
      this.props.testSections,
      this.props.currentLanguage
    );
    let tscOptions = [];
    if (this.state.selectedParentTestSection !== "") {
      tscOptions = this.makeTestSectionComponentOptions(
        this.props.testSectionComponents,
        this.props.currentLanguage
      );
    }
    const blockOptions = this.props.questionBlockTypes.map(block => {
      return { label: block.name, value: block.id };
    });
    const hideControls =
      this.state.selectedParentTestSectionComponent === "" ||
      (this.state.componentType !== TestSectionComponentType.INBOX &&
        this.state.componentType !== TestSectionComponentType.QUESTION_LIST);
    return (
      <>
        <h2>{LOCALIZE.testBuilder.questions.topTitle}</h2>
        {this.state.showMissingQuestionsErrorMessage && (
          <div style={componentStyles.validationErrorMessageContainer}>
            <p style={{ ...componentStyles.validationErrorMessage, ...customFontSize }}>
              {
                LOCALIZE.testBuilder.questions.validationErrors
                  .uploadFailedMissingQuestionListQuestions
              }
            </p>
          </div>
        )}
        <p>{LOCALIZE.testBuilder.questions.description}</p>
        <Container>
          <Row style={styles.rowStyle}>
            {makeLabel("parentTestSection", LOCALIZE.testBuilder.testSectionComponents)}
            {makeDropDownField(
              "parentTestSection",
              LOCALIZE.testBuilder.testSectionComponents,
              this.state.selectedParentTestSection,
              tsOptions,
              this.selectParentTestSection
            )}
          </Row>
          <Row style={styles.rowStyle}>
            {makeLabel("parentTestSectionComponent", LOCALIZE.testBuilder.sectionComponentPages)}
            {makeDropDownField(
              "parentTestSectionComponent",
              LOCALIZE.testBuilder.sectionComponentPages,
              this.state.selectedParentTestSectionComponent,
              tscOptions,
              this.selectParentTestSectionComponent,
              this.state.selectedParentTestSection === ""
            )}
          </Row>
          <Row style={styles.rowStyle}>
            {makeLabel("questionBlockSelection", LOCALIZE.testBuilder.questions)}
            {makeMultiDropDownField(
              "questionBlockSelection",
              LOCALIZE.testBuilder.questions,
              this.state.selectedBlock,
              blockOptions,
              this.onSelectChange,
              this.state.selectedParentTestSectionComponent === ""
            )}
          </Row>
        </Container>
        <Container>
          <Row>
            <Col sm={2}>
              <Button variant={"primary"} onClick={this.addQuestion} disabled={hideControls}>
                <FontAwesomeIcon
                  icon={faPlusCircle}
                  style={{ margins: "auto", marginRight: "5px" }}
                />
                {LOCALIZE.testBuilder.questions.addButton}
              </Button>
            </Col>
            {!hideControls && (
              <Col sm={3} style={styles.centerLabel}>
                {LOCALIZE.formatString(
                  LOCALIZE.testBuilder.questions.searchResults,
                  questions.length
                )}
              </Col>
            )}
          </Row>
        </Container>
        <div style={styles.borderBox} />
      </>
    );
  };

  // Used to delete a CollapsingItemContainer containing the question/answers
  handleDelete = id => {
    // creating copy of questions array (props)
    let objArray = Array.from(this.props.questions);
    // filter objArray, so the deleted question is not part of the array
    objArray = objArray.filter(obj => obj.id !== id);
    // looping in questions (except the one we're deleting)
    for (let i = 0; i < objArray.length; i++) {
      // looping in question dependencies
      for (let j = 0; j < objArray[i].dependencies.length; j++) {
        if (objArray[i].dependencies[j] === id) {
          // deleting dependency
          objArray[i].dependencies.splice(j, 1);
        }
      }
      // if dependencies array is empty, update dependent order to 0
      if (objArray[i].dependencies.length === 0) {
        objArray[i].dependent_order = 0;
      }
    }
    // delete the question
    this.props.replaceTestDefinitionField(QUESTIONS, objArray);
    if (this.state.componentType === TestSectionComponentType.INBOX) {
      // delete the email questions
      const emailDetailsToDelete = [];
      objArray = this.props.emails.filter(obj => {
        if (obj.question !== id) {
          return obj;
        }
        emailDetailsToDelete.push(obj.id);
        return null;
      });

      this.props.replaceTestDefinitionField(EMAILS, objArray);
      // delete the page section definitions
      objArray = this.props.emailDetails.filter(
        obj => emailDetailsToDelete.indexOf(obj.email_question) === -1
      );
      this.props.replaceTestDefinitionField(EMAIL_DETAILS, objArray);
    }
    if (this.state.componentType === TestSectionComponentType.QUESTION_LIST) {
      // delete the mc questions
      const mcToDelete = [];
      objArray = this.props.multipleChoiceQuestions.filter(obj => {
        if (obj.question !== id) {
          return obj;
        }
        mcToDelete.push(obj.id);
        return null;
      });
      this.props.replaceTestDefinitionField(MULTIPLE_CHOICE_QUESTION_DETAILS, objArray);

      // delete the question sections
      const sectionsToDelete = [];
      objArray = this.props.questionSections.filter(obj => {
        if (obj.question === id) {
          sectionsToDelete.push(obj.id);
          return null;
        }
        return obj;
      });
      this.props.replaceTestDefinitionField(QUESTION_SECTIONS, objArray);

      // delete the question section definitions
      objArray = this.props.questionSectionDefinitions.filter(
        obj => sectionsToDelete.indexOf(obj.question_section) === -1
      );
      this.props.replaceTestDefinitionField(QUESTION_SECTION_DEFINITIONS, objArray);

      // getting answers to delete
      const answersToDelete = [];
      for (let i = 0; i < this.props.answers.length; i++) {
        if (this.props.answers[i].question === id) {
          answersToDelete.push(this.props.answers[i].id);
        }
      }

      // delete the answers
      objArray = this.props.answers.filter(obj => answersToDelete.indexOf(obj.id) < 0);
      this.props.replaceTestDefinitionField(ANSWERS, objArray);

      // delete the answer definitions
      objArray = this.props.answerDetails.filter(obj => answersToDelete.indexOf(obj.answer) < 0);
      this.props.replaceTestDefinitionField(ANSWER_DETAILS, objArray);
    }
  };

  getQuestionText = questionSectionDefinitions => {
    // initializing questionText
    let questionText = LOCALIZE.commons.none;
    // making sure that we have content in questionSectionDefinitions
    if (questionSectionDefinitions.length > 0) {
      // current language is English
      if (this.props.currentLanguage === LANGUAGES.english) {
        // prioritizing English display (if possible)
        // getting questionSectionDefinitions (english content)
        const questionSectionDefinition = questionSectionDefinitions.filter(
          obj => obj.language === LANGUAGES.english
        );
        // checking if we have an available english version
        if (questionSectionDefinition.length > 0) {
          // making sure that there is text content
          if (questionSectionDefinition[0].content !== "") {
            // getting first 15 chars of first question section
            questionText = `${removeHtmlFromString(questionSectionDefinition[0].content).substring(
              0,
              15
            )}...`;
          }
        } else {
          // getting questionSectionDefinitions (french content)
          const questionSectionDefinition = questionSectionDefinitions.filter(
            obj => obj.language === LANGUAGES.french
          );
          // checking if we have an available french version
          if (questionSectionDefinition.length > 0) {
            // making sure that there is text content
            if (questionSectionDefinition[0].content !== "") {
              // getting first 15 chars of first question section
              questionText = `${removeHtmlFromString(
                questionSectionDefinition[0].content
              ).substring(0, 15)}...`;
            }
          }
        }
        // current language is French
      } else {
        // prioritizing French display (if possible)
        // getting questionSectionDefinitions (french content)
        const questionSectionDefinition = questionSectionDefinitions.filter(
          obj => obj.language === LANGUAGES.french
        );
        // checking if we have an available french version
        if (questionSectionDefinition.length > 0) {
          // making sure that there is text content
          if (questionSectionDefinition[0].content !== "") {
            // getting first 15 chars of first question section
            questionText = `${removeHtmlFromString(questionSectionDefinition[0].content).substring(
              0,
              15
            )}...`;
          }
        } else {
          // getting questionSectionDefinitions (english content)
          const questionSectionDefinition = questionSectionDefinitions.filter(
            obj => obj.language === LANGUAGES.english
          );
          // checking if we have an available english version
          if (questionSectionDefinition.length > 0) {
            // making sure that there is text content
            if (questionSectionDefinition[0].content !== "") {
              // getting first 15 chars of first question section
              questionText = `${removeHtmlFromString(
                questionSectionDefinition[0].content
              ).substring(0, 15)}...`;
            }
          }
        }
      }
    }
    return questionText;
  };

  // Create an object with the question object's data that needs to be displayed (with language)
  getQuestionDetails = question => {
    const returnObj = {};

    switch (question.question_type) {
      case QuestionType.EMAIL:
        for (const email of this.props.emails) {
          if (email.question === question.id) {
            returnObj.email = email;
            let fromField = "";
            for (const contact of this.props.addressBook) {
              if (contact.id === email.from_field) {
                fromField = contact.name;
              }
            }
            returnObj.title = LOCALIZE.formatString(
              LOCALIZE.testBuilder.questions.emailCollapsableItemName,
              question.order,
              fromField
            );
            for (const obj of this.props.emailDetails) {
              if (obj.email_question === email.id && obj.language === LANGUAGES.english) {
                returnObj.en = obj;
              } else if (obj.email_question === email.id && obj.language === LANGUAGES.french) {
                returnObj.fr = obj;
              }
            }

            for (const obj of this.props.questionSituations) {
              if (obj.question === question.id && obj.language === LANGUAGES.english) {
                returnObj.enSituation = obj;
              } else if (obj.question === question.id && obj.language === LANGUAGES.french) {
                returnObj.frSituation = obj;
              }
            }
            let newId = getNextInNumberSeries(this.props.questionSituations);
            if (!returnObj.enSituation) {
              returnObj.enSituation = {
                id: newId,
                question: question.id,
                language: LANGUAGES.english
              };
              newId += 1;
            }
            if (!returnObj.frSituation) {
              returnObj.frSituation = {
                id: newId,
                question: question.id,
                language: LANGUAGES.french
              };
            }
          }
        }

        break;
      case QuestionType.MULTIPLE_CHOICE:
        const sectionDefs = [];
        // question sections
        returnObj.questionSections = this.props.questionSections.filter(obj => {
          if (obj.question === question.id) {
            sectionDefs.push(obj.id);
            return obj;
          }
          return null;
        });
        // question section defs
        returnObj.questionSectionDefinitions = this.props.questionSectionDefinitions.filter(
          obj => sectionDefs.indexOf(obj.question_section) >= 0
        );
        // answers
        const answers = [];
        returnObj.answers = this.props.answers.filter(obj => {
          if (obj.question === question.id) {
            answers.push(obj.id);
            return obj;
          }
          return null;
        });
        returnObj.answerDetails = this.props.answerDetails.filter(
          obj => answers.indexOf(obj.answer) >= 0
        );
        const blockType = this.props.questionBlockTypes.filter(
          obj => obj.id === question.question_block_type
        );
        returnObj.title = (
          <label style={componentStyles.boldText}>
            <span>{`${question.order}. ${LOCALIZE.testBuilder.questions.collapsableItemNames.block}`}</span>{" "}
            <span style={componentStyles.nonBoldText}>
              {blockType[0] ? blockType[0].name : LOCALIZE.commons.none}{" "}
            </span>
            <span>{LOCALIZE.testBuilder.questions.collapsableItemNames.id} </span>
            <span style={componentStyles.nonBoldText}>
              {question.ppc_question_id !== "" && typeof question.ppc_question_id !== "undefined"
                ? question.ppc_question_id
                : LOCALIZE.commons.none}{" "}
            </span>
            <span>{LOCALIZE.testBuilder.questions.collapsableItemNames.text} </span>
            <span style={componentStyles.nonBoldText}>
              {this.getQuestionText(returnObj.questionSectionDefinitions)}
            </span>
          </label>
        );
        returnObj.titlePart1 = `${question.order}. ${LOCALIZE.testBuilder.questions.collapsableItemNames.block}`;
        returnObj.titlePart2 = blockType[0] ? blockType[0].name : LOCALIZE.commons.none;

        break;
      default:
        break;
    }
    return returnObj;
  };

  // Used to move a CollapsingItemContainer component up a position
  handleMoveUp = index => {
    // getting all questions list ordered data
    const allQuestionsOrderedData = this.getAllQuestionsOrderedData(
      this.state.selectedParentTestSectionComponent.value
    );
    // getting new questions array
    collapsingItemMoveUpFunctionality(index, allQuestionsOrderedData.currentQuestionsList)
      .then(objArray => {
        // creating final array
        const final_array = objArray.concat(allQuestionsOrderedData.otherQuestionsList);
        // updating redux states
        this.props.replaceTestDefinitionField(QUESTIONS, final_array);
      })
      .then(() => {
        // getting orders
        this.getQuestionsOrders();
      });
  };

  // Used to move a CollapsingItemContainer component down a position
  handleMoveDown = index => {
    // getting all questions list ordered data
    const allQuestionsOrderedData = this.getAllQuestionsOrderedData(
      this.state.selectedParentTestSectionComponent.value
    );
    // getting new questions array
    collapsingItemMoveDownFunctionality(index, allQuestionsOrderedData.currentQuestionsList)
      .then(objArray => {
        // creating final array
        const final_array = objArray.concat(allQuestionsOrderedData.otherQuestionsList);
        // updating redux states
        this.props.replaceTestDefinitionField(QUESTIONS, final_array);
      })
      .then(() => {
        // getting orders
        this.getQuestionsOrders();
      });
  };

  getQuestionsOrders = () => {
    // initializing props variables
    const { questions } = this.props;

    // initializing newQuestions array
    const newQuestions = [];

    // grouping array by test section component
    // source: https://edisondevadoss.medium.com/javascript-group-an-array-of-objects-by-key-afc85c35d07e
    const grouped_by_test_section_component = questions.reduce((r, a) => {
      // eslint-disable-next-line no-param-reassign
      r[a.test_section_component] = [...(r[a.test_section_component] || []), a];
      return r;
    }, {});
    const grouped_by_test_section_component_array = Object.keys(
      grouped_by_test_section_component
    ).map(key => [grouped_by_test_section_component[key]]);

    // looping in grouped_by_test_section_component_array
    for (let i = 0; i < grouped_by_test_section_component_array.length; i++) {
      // looping in array of current grouped_by_test_section_component_array
      for (let j = 0; j < grouped_by_test_section_component_array[i][0].length; j++) {
        // pushing new object (with the right order) in newQuestions array
        newQuestions.push({
          id: grouped_by_test_section_component_array[i][0][j].id,
          dependencies: grouped_by_test_section_component_array[i][0][j].dependencies,
          pilot: grouped_by_test_section_component_array[i][0][j].pilot,
          question_block_type: grouped_by_test_section_component_array[i][0][j].question_block_type,
          question_type: grouped_by_test_section_component_array[i][0][j].question_type,
          test_section_component:
            grouped_by_test_section_component_array[i][0][j].test_section_component,
          dependent_order: grouped_by_test_section_component_array[i][0][j].dependent_order,
          shuffle_answer_choices:
            grouped_by_test_section_component_array[i][0][j].shuffle_answer_choices,
          ppc_question_id: grouped_by_test_section_component_array[i][0][j].ppc_question_id,
          order: j + 1
        });
      }
    }
    // updating redux state
    this.props.replaceTestDefinitionArray(QUESTIONS, newQuestions);
  };

  // Gets an array with the selected question + an array with the rest of the questions
  getAllQuestionsOrderedData = testSectionComponentId => {
    // initializing needed arrays
    const currentQuestionsList = [];
    const otherQuestionsList = [];
    // looping in all questions
    for (let i = 0; i < this.props.questions.length; i++) {
      // pushing current questions to currentQuestionsList array
      if (testSectionComponentId === this.props.questions[i].test_section_component) {
        currentQuestionsList.push(this.props.questions[i]);
        // pushing other questions to otherQuestionsList array
      } else {
        otherQuestionsList.push(this.props.questions[i]);
      }
    }
    return {
      currentQuestionsList: currentQuestionsList,
      otherQuestionsList: otherQuestionsList
    };
  };

  render() {
    let { questions } = this.props;
    if (this.state.selectedParentTestSectionComponent) {
      questions = questions.filter(
        obj => obj.test_section_component === this.state.selectedParentTestSectionComponent.value
      );
    }
    if (this.state.selectedBlock.length > 0) {
      questions = questions.filter(
        obj => this.state.selectedBlock.map(ob => ob.value).indexOf(obj.question_block_type) >= 0
      );
    }

    const customFontSize = {
      // adding 5px to current font size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 5}px`
    };

    return (
      <div style={styles.mainContainer}>
        {this.makeHeader(questions, customFontSize)}

        {this.state.selectedParentTestSectionComponent !== "" &&
          questions.map((question, index) => {
            if (
              question.test_section_component ===
              this.state.selectedParentTestSectionComponent.value
            ) {
              const questionDetails = this.getQuestionDetails(question);
              const testLanguage = testSectionComponentLanguage(
                question.test_section_component,
                this.props.testSectionComponents
              );

              return (
                <CollapsingItemContainer
                  key={index}
                  index={index}
                  usesMoveUpAndDownFunctionalities={true}
                  moveUpAction={() => this.handleMoveUp(index)}
                  moveUpButtonDisabled={question.order === 1}
                  moveDownAction={() => this.handleMoveDown(index)}
                  moveDownButtonDisabled={
                    index === questions.length - 1
                      ? true
                      : questions[index + 1].order <= question.order
                  }
                  title={questionDetails.title}
                  body={
                    <QuestionForm
                      question={question}
                      questionDetails={questionDetails}
                      dependencyOptions={questions.filter(obj => obj.id !== question.id)}
                      questionBlockTypes={this.props.questionBlockTypes}
                      handleDelete={this.handleDelete}
                      testLanguage={testLanguage}
                      testSection={
                        this.props.testSections.filter(
                          obj => obj.id === this.state.selectedParentTestSection.value
                        )[0]
                      }
                    />
                  }
                />
              );
            }
            return null;
          })}
      </div>
    );
  }
}

export { Questions as unconnectedQuestions };

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations,
    currentLanguage: state.localize.language,
    testSections: state.testBuilder.test_sections,
    testSectionComponents: state.testBuilder.test_section_components,
    sectionComponentPages: state.testBuilder.section_component_pages,
    questions: state.testBuilder.questions,
    emails: state.testBuilder.emails,
    emailDetails: state.testBuilder.email_details,
    addressBook: state.testBuilder.address_book,
    contactDetails: state.testBuilder.address_book_contacts,
    questionSections: state.testBuilder.question_sections,
    questionSectionDefinitions: state.testBuilder.question_section_definitions,
    answers: state.testBuilder.answers,
    answerDetails: state.testBuilder.answer_details,
    multipleChoiceQuestions: state.testBuilder.multiple_choice_question_details,
    questionBlockTypes: state.testBuilder.question_block_types,
    questionSituations: state.testBuilder.question_situations,
    triggerTestDefinitionFieldUpdates: state.testBuilder.triggerTestDefinitionFieldUpdates,
    validationErrors: state.testBuilder.validation_errors
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      replaceTestDefinitionField,
      cascadeDeleteQuestion,
      addTestDefinitionField,
      replaceTestDefinitionArray,
      setTestDefinitionValidationErrors
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Questions);
