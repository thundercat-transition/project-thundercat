import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Col } from "react-bootstrap";
import {
  makeLabel,
  makeTextBoxField,
  makeSaveDeleteButtons,
  isNumberOrEmpty,
  allValid,
  makeDropDownField
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  QUESTION_LIST_RULES,
  triggerTestDefinitionFieldUpdates,
  setQuestionRulesValidState,
  setQuestionRulesValidStateWhenItemExposureEnabled,
  setTestDefinitionValidationErrors,
  QUESTION_BLOCK_TYPE
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import getSwitchTransformScale from "../../helpers/switchTransformScale";
import Switch from "react-switch";
import {
  validateQuestionRulesData,
  validateQuestionRulesDataWithEnabledItemExposure
} from "./validation";

class QuestionListRuleForm extends Component {
  state = {
    showDialog: false,
    selectedType: undefined,
    rule: {},
    isValidQuestionBlockType: true,
    isValidRule: true,
    // default values
    switchHeight: 25,
    switchWidth: 50,
    areSwitchStatesLoading: false
  };

  modifyField = (inputName, value) => {
    const newObj = { ...this.props.rule, ...this.state.rule };
    // if question block type is updated
    if (inputName === QUESTION_BLOCK_TYPE) {
      // assign value in an array
      newObj[`${inputName}`] = [value];
    } else {
      newObj[`${inputName}`] = value;
    }
    this.setState({ rule: newObj });
  };

  numberValidation = (name, value) => {
    if (isNumberOrEmpty(value)) {
      this.setState({ isOrderValid: true });
      this.modifyField(name, parseInt(value) || 0);
    }
  };

  onSelectChange = (event, action) => {
    this.modifyField(action.name, event.value);
  };

  handleSave = () => {
    const { rule } = this.props;

    // setting isValidQuestionBlockType depending on the question block type value of the current select rule
    this.setState(
      { isValidQuestionBlockType: { ...rule, ...this.state.rule }.question_block_type.length > 0 },
      () => {
        const areQuestionsRulesValid = validateQuestionRulesData(this.props.questions, [
          { ...rule, ...this.state.rule }
        ]);
        // setting redux question rules validity state
        this.props.setQuestionRulesValidState(areQuestionsRulesValid);

        // if test section has item_exposure enabled
        const currentTestSection = this.props.testSections.filter(
          obj => obj.id === this.props.testSectionComponent.test_section[0]
        )[0];
        // initializing areQuestionsRulesValidWhenItemExposureEnabled
        let areQuestionsRulesValidWhenItemExposureEnabled = { is_valid: true, error_type: null };
        if (currentTestSection.item_exposure) {
          // getting current rule
          const currentRule = { ...rule, ...this.state.rule };
          // creating copy of questionListRules
          const copyOfQuestionListRules = this.props.questionListRules;
          // getting index of current question rule
          const indexOfCurrentQuestionRule = this.props.questionListRules.findIndex(obj => {
            return obj.id === currentRule.id;
          });
          // updating copyOfQuestionListRules with current rule content
          copyOfQuestionListRules[indexOfCurrentQuestionRule] = currentRule;
          areQuestionsRulesValidWhenItemExposureEnabled =
            validateQuestionRulesDataWithEnabledItemExposure(
              this.props.questions,
              copyOfQuestionListRules
            );
          // setting question rules redux states
          this.props.setQuestionRulesValidStateWhenItemExposureEnabled(
            areQuestionsRulesValidWhenItemExposureEnabled.is_valid,
            areQuestionsRulesValidWhenItemExposureEnabled.error_type
          );
        }

        // creating a copy of validationErrors and updating testDefinitionContainsValidQuestionRules attribute
        const copyOfValidationErrors = this.props.validationErrors;
        copyOfValidationErrors.testDefinitionContainsValidQuestionRules =
          areQuestionsRulesValid && areQuestionsRulesValidWhenItemExposureEnabled.is_valid;

        // if invalid question rules
        if (!copyOfValidationErrors.testDefinitionContainsValidQuestionRules) {
          // setting testDefinitionQuestionRulesErrorType
          copyOfValidationErrors.testDefinitionQuestionRulesErrorType =
            areQuestionsRulesValidWhenItemExposureEnabled.error_type;
        }

        // setting redux test definition validation errors
        this.props.setTestDefinitionValidationErrors(copyOfValidationErrors);

        this.setState({ isValidRule: areQuestionsRulesValid }, () => {
          if (allValid(this.state) && areQuestionsRulesValid) {
            this.props.modifyTestDefinitionField(
              QUESTION_LIST_RULES,
              { ...rule, ...this.state.rule },
              rule.id
            );
          } else {
            // trigger test definition field updates
            this.props.triggerTestDefinitionFieldUpdates();
          }
        });
      }
    );
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    // delete the question list rule
    const { id } = this.props.rule;
    const objArray = this.props.questionListRules.filter(obj => obj.id !== id);
    this.props.replaceTestDefinitionField(QUESTION_LIST_RULES, objArray);

    this.closeDialog();
    this.props.expandItem();
    // trigger test definition field updates
    this.props.triggerTestDefinitionFieldUpdates();
  };

  selectedBlock = rule => {
    for (const block of this.props.questionBlockTypes) {
      if (rule.question_block_type === block.id) {
        return { label: block.name, value: block.id };
      }
    }
    return undefined;
  };

  selectedBlocks = field => {
    const fieldVar = Array.isArray(field) ? field : [field];
    const array = [];
    for (const field of fieldVar) {
      for (const block of this.props.questionBlockTypes) {
        if (field === block.id) {
          array.push({ label: block.name, value: block.id });
        }
      }
    }
    return array;
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ areSwitchStatesLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ areSwitchStatesLoading: false });
        }
      );
    });
  };

  handleEnableShuffleQuestions = () => {
    this.modifyField("shuffle", true);
  };

  handleDisableShuffleQuestions = () => {
    this.modifyField("shuffle", false);
  };

  gettingQuestionBlockTypeOptions = () => {
    const { questionListRules, questionBlockTypes, rule } = this.props;

    // initializing needed variables
    const options = [];
    const usedOptions = [];

    // looping in question list rules (already existing rules)
    for (let i = 0; i < questionListRules.length; i++) {
      // populating usedOptions (all question list rules that are already used)
      if (questionListRules[i].question_block_type.length > 0) {
        // make sure to keep the current selected rule as part of the available options
        if (rule.question_block_type[0] !== questionListRules[i].question_block_type[0]) {
          usedOptions.push(questionListRules[i].question_block_type[0]);
        }
      }
    }

    // looping in questionBlockTypes
    for (let i = 0; i < questionBlockTypes.length; i++) {
      // if current questionBlockTypes is not part of the used array
      if (usedOptions.indexOf(questionBlockTypes[i].id) < 0) {
        // add option
        options.push(questionBlockTypes[i]);
      }
    }

    const formattedOptions = options.map(block => {
      return { label: block.name, value: block.id };
    });

    return formattedOptions;
  };

  render() {
    const rule = { ...this.props.rule, ...this.state.rule };
    const blockOptions = this.gettingQuestionBlockTypeOptions();
    const selectedBlocks = this.selectedBlocks(rule.question_block_type);
    return (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel("number_of_questions", LOCALIZE.testBuilder.questionListRules)}
          {makeTextBoxField(
            "number_of_questions",
            LOCALIZE.testBuilder.questionListRules,
            rule.number_of_questions,
            this.state.isValidRule,
            this.numberValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("order", LOCALIZE.testBuilder.questionListRules)}
          {makeTextBoxField(
            "order",
            LOCALIZE.testBuilder.questionListRules,
            rule.order,
            true,
            () => {},
            "question-list-rules-order",
            true
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("question_block_type", LOCALIZE.testBuilder.questionListRules)}
          {makeDropDownField(
            "question_block_type",
            LOCALIZE.testBuilder.questionListRules,
            selectedBlocks,
            blockOptions,
            this.onSelectChange,
            false,
            this.state.isValidQuestionBlockType
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "shuffle",
            LOCALIZE.testBuilder.questionListRules,
            "shuffle-questions-in-rules-label"
          )}
          <Col>
            <div style={styles.rightColumnContainer}>
              {!this.state.areSwitchStatesLoading && (
                <Switch
                  onChange={
                    rule.shuffle
                      ? this.handleDisableShuffleQuestions
                      : this.handleEnableShuffleQuestions
                  }
                  checked={rule.shuffle}
                  aria-labelledby="shuffle-questions-in-rules-label"
                  height={this.state.switchHeight}
                  width={this.state.switchWidth}
                  className="switch-custom-style"
                />
              )}
            </div>
          </Col>
        </Row>

        <Row className="justify-content-between" style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed. Are you sure you want to
                delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </>
    );
  }
}

export { QuestionListRuleForm as unconnectedQuestionListRuleForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    questionBlockTypes: state.testBuilder.question_block_types,
    questionListRules: state.testBuilder.question_list_rules,
    questions: state.testBuilder.questions,
    validationErrors: state.testBuilder.validation_errors,
    testSections: state.testBuilder.test_sections
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      modifyTestDefinitionField,
      replaceTestDefinitionField,
      triggerTestDefinitionFieldUpdates,
      setQuestionRulesValidState,
      setQuestionRulesValidStateWhenItemExposureEnabled,
      setTestDefinitionValidationErrors
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(QuestionListRuleForm);
