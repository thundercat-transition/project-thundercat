import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Col } from "react-bootstrap";
import {
  makeLabel,
  makeDropDownField,
  allValid,
  makeMultiDropDownField,
  isNumberOrEmpty,
  makeTextBoxField,
  getNextInNumberSeries
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  QUESTIONS,
  setQuestionRulesValidState,
  setQuestionRulesValidStateWhenItemExposureEnabled,
  setTestDefinitionValidationErrors,
  triggerTestDefinitionFieldUpdates,
  QUESTION_BLOCK_TYPE,
  DEPENDENCIES,
  addTestDefinitionField,
  QUESTION_SECTIONS,
  QUESTION_SECTION_DEFINITIONS,
  ANSWERS,
  ANSWER_DETAILS,
  MULTIPLE_CHOICE_QUESTION_DETAILS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { QuestionDifficultyType, QuestionType } from "../testFactory/Constants";
import EmailQuestionForm from "./EmailQuestionForm";
import MultipleChoiceQuestionForm from "./MultipleChoiceQuestionForm";
import Switch from "react-switch";
import getSwitchTransformScale from "../../helpers/switchTransformScale";
import {
  validateQuestionRulesData,
  validateQuestionRulesDataWithEnabledItemExposure
} from "./validation";
import { LANGUAGES } from "../commons/Translation";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import QuestionFactory from "../testFactory/QuestionFactory";
import MarkdownButtonAnswer from "../testFactory/MarkdownButtonAnswer";

class QuestionForm extends Component {
  state = {
    showDialog: false,
    isDependentOrderValid: true,
    question: {},
    selectedParent: {},
    // default values
    switchHeight: 25,
    switchWidth: 50,
    areSwitchStatesLoading: false,
    showPreviewPopup: false,
    previewQuestionBilingualQuestion: false,
    previewQuestionLanguage: this.props.currentLanguage,
    previewQuestionQuestionNumber: null,
    previewPopupQuestionSections: null,
    previewPopupQuestionAnswers: null
  };

  componentDidMount = () => {
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = prevProps => {
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
  };

  modifyField = (inputName, value) => {
    const newObj = { ...this.props.question, ...this.state.question };
    newObj[`${inputName}`] = value;
    // if question block type is updated
    if (inputName === QUESTION_BLOCK_TYPE) {
      // removing all related dependencies
      newObj[DEPENDENCIES] = [];
    }
    this.setState({ question: newObj });
  };

  onPpcQuestionIdChange = (name, value) => {
    // allow only alphanumeric, slash and dash (0 to 25 chars)
    const regexExpression = /^([a-zA-zÀ-ÿ0-9-/]{0,25})$/;
    if (regexExpression.test(value)) {
      this.modifyField(name, value);
    }
  };

  onSelectChange = (event, action) => {
    let customEvent = event;
    // event can be null when clearing the dropdown selected item
    if (customEvent === null) {
      customEvent = { label: "", value: null };
    }
    this.modifyField(action.name, customEvent.value);
  };

  onMultiSelectChange = (event, action) => {
    const question = { ...this.props.question, ...this.state.question };
    let newArray = [];
    if (action.action === "remove-value") {
      newArray = [...question[`${action.name}`]].filter(obj => obj !== action.removedValue.value);
    } else if (action.action !== "clear") {
      newArray = [...question[`${action.name}`], action.option.value];
    }

    this.modifyField(action.name, newArray);
  };

  dependentOrderValidation = (name, value) => {
    if (isNumberOrEmpty(value)) {
      this.setState({ isDependentOrderValid: true });
      this.modifyField(name, parseInt(value) || 0);
    }
  };

  handleSave = () => {
    if (allValid(this.state)) {
      // initializing question
      const question = { ...this.props.question, ...this.state.question };

      // creating a copy of the current questions props (before the update)
      const questions = Array.from(this.props.questions);

      // getting initial dependencies (before changes)
      const initialDependenciesArray = questions.filter(obj => obj.id === question.id)[0]
        .dependencies;

      // getting updated dependencies (after changes)
      const updatedDependenciesArray = question.dependencies;

      // updating questions array based on current updates
      for (let i = 0; i < questions.length; i++) {
        if (questions[i].id === question.id) {
          questions[i] = {
            id: question.id,
            dependencies: question.dependencies,
            dependent_order: question.dependencies.length > 0 ? question.dependent_order : 0,
            order: question.order,
            pilot: question.pilot,
            question_block_type: question.question_block_type,
            question_type: question.question_type,
            test_section_component: question.test_section_component,
            shuffle_answer_choices: question.shuffle_answer_choices,
            ppc_question_id: question.ppc_question_id
          };
        }
      }

      // ========== DELETING ALL EXISTING DEPENDENCIES BASED ON THE initialDependenciesArray ==========
      // looping in initialDependenciesArray
      for (let i = 0; i < initialDependenciesArray.length; i++) {
        // looping in dependencies_array
        for (let j = 0; j < questions.length; j++) {
          // if current dependency is the same as the current question
          if (initialDependenciesArray[i] === questions[j].id) {
            // deleting dependency of current question
            questions[j].dependencies = [];
          }
        }
      }

      // ========== DELETING ALL EXISTING DEPENDENCIES (DEPENDENCIES OF DEPENDENCIES) BASED ON THE updatedDependenciesArray ==========
      // looping in updatedDependenciesArray
      for (let i = 0; i < updatedDependenciesArray.length; i++) {
        // looping in questions
        for (let j = 0; j < questions.length; j++) {
          // if current dependency is the same as the current question
          if (updatedDependenciesArray[i] === questions[j].id) {
            // looping in current question dependencies
            for (let k = 0; k < questions[j].dependencies.length; k++) {
              // getting related question
              const related_question = questions.filter(
                obj => obj.id === questions[j].dependencies[k]
              )[0];
              // deleting dependency of related question
              related_question.dependencies = [];
            }
            // deleting dependency of current question
            questions[j].dependencies = [];
          }
        }
      }

      // ========== ADDING ALL NEEDED DEPENDENCIES TO QUESTIONS BASED ON THE updatedDependenciesArray ==========
      // looping in updatedDependenciesArray
      for (let i = 0; i < updatedDependenciesArray.length; i++) {
        // looping in questions
        for (let j = 0; j < questions.length; j++) {
          // if current dependency is the same as the current question
          if (updatedDependenciesArray[i] === questions[j].id) {
            // adding needed dependencies to current question
            questions[j].dependencies.push(question.id);
            questions[j].dependencies = questions[j].dependencies.concat(
              updatedDependenciesArray.filter(obj => obj !== questions[j].id)
            );
          }
        }
      }

      this.props.replaceTestDefinitionField(QUESTIONS, questions);
      this.props.expandItem();

      // calling question rules validation
      const areQuestionsRulesValid = validateQuestionRulesData(
        questions,
        this.props.allQuestionListRules
      );
      // if test section has item_exposure enabled
      // initializing areQuestionsRulesValidWhenItemExposureEnabled
      let areQuestionsRulesValidWhenItemExposureEnabled = { is_valid: true, error_type: null };
      if (this.props.testSection.item_exposure) {
        areQuestionsRulesValidWhenItemExposureEnabled =
          validateQuestionRulesDataWithEnabledItemExposure(
            questions,
            this.props.allQuestionListRules
          );
        // setting question rules redux states
        this.props.setQuestionRulesValidStateWhenItemExposureEnabled(
          areQuestionsRulesValidWhenItemExposureEnabled.is_valid,
          areQuestionsRulesValidWhenItemExposureEnabled.error_type
        );
      }

      // setting redux question rules validity state
      this.props.setQuestionRulesValidState(areQuestionsRulesValid);

      // creating a copy of validationErrors and updating testDefinitionContainsValidQuestionRules attribute
      const copyOfValidationErrors = this.props.validationErrors;
      copyOfValidationErrors.testDefinitionContainsValidQuestionRules =
        areQuestionsRulesValid && areQuestionsRulesValidWhenItemExposureEnabled.is_valid;

      // if invalid question rules
      if (!copyOfValidationErrors.testDefinitionContainsValidQuestionRules) {
        // setting testDefinitionQuestionRulesErrorType
        copyOfValidationErrors.testDefinitionQuestionRulesErrorType =
          areQuestionsRulesValidWhenItemExposureEnabled.error_type;
      }

      // setting redux test definition validation errors
      this.props.setTestDefinitionValidationErrors(copyOfValidationErrors);

      // trigger test definition field updates
      this.props.triggerTestDefinitionFieldUpdates();
    }
  };

  handleDuplicate = () => {
    // initializing needed variables
    const question = { ...this.props.question, ...this.state.question };
    const { questions } = this.props;

    // ==================== REORGANIZING QUESTIONS ORDER ====================
    // updating questions' order (questions where order is greater than the current selected question)
    // initializing newOrder
    let newOrder = question.order + 2;
    // looping in questions
    for (let i = 0; i < questions.length; i++) {
      // current question's order > question.order
      if (questions[i].order > question.order) {
        // updating order
        questions[i].order = newOrder;
        // incrementing newOrder
        newOrder += 1;
      }
    }
    // ==================== REORGANIZING QUESTIONS ORDER (END) ====================

    // ==================== NEW QUESTION ====================
    // initializing newQuestion
    let newQuestion = null;
    // setting new question data
    newQuestion = {
      question_type: question.question_type,
      pilot: false,
      test_section_component: question.test_section_component,
      dependencies: [],
      order: question.order + 1,
      dependent_order: 0,
      shuffle_answer_choices: question.shuffle_answer_choices,
      ppc_question_id: "",
      question_block_type: question.question_block_type
    };
    newQuestion.id = getNextInNumberSeries(this.props.questions, "id");
    // adding new question to questions array
    questions.push(newQuestion);
    // ==================== NEW QUESTION (END) ====================

    // ==================== QUESTION SECTION(S) DATA ====================
    // getting current question sections
    const questionSections = this.props.questionSections.filter(
      obj => obj.question === question.id
    );
    // initializing needed variables
    let newQuestionSectionId = getNextInNumberSeries(this.props.questionSections, "id");
    let newQuestionSectionOrder = 1;
    let newQuestionSectionDefinitionId = getNextInNumberSeries(
      this.props.questionSectionDefinitions,
      "id"
    );
    // looping in questionSections
    for (let i = 0; i < questionSections.length; i++) {
      // creating new question section
      const newQuestionSection = {
        id: newQuestionSectionId,
        order: newQuestionSectionOrder,
        question_section_type: questionSections[i].question_section_type,
        question: newQuestion.id
      };
      // updating redux states
      this.props.addTestDefinitionField(
        QUESTION_SECTIONS,
        newQuestionSection,
        newQuestionSection.id
      );
      // getting related question section definitions
      const questionSectionDefinitions = this.props.questionSectionDefinitions.filter(
        obj => obj.question_section === questionSections[i].id
      );
      // looping in questionSectionDefinitions
      for (let i = 0; i < questionSectionDefinitions.length; i++) {
        // creating new question section definition
        let newQuestionSectionDefinition = {};
        if (questionSectionDefinitions[i].language === LANGUAGES.english) {
          newQuestionSectionDefinition = {
            id: newQuestionSectionDefinitionId,
            content: questionSectionDefinitions[i].content,
            question_section: newQuestionSection.id,
            language: LANGUAGES.english,
            question_section_type: newQuestionSection.question_section_type
          };
          this.props.addTestDefinitionField(
            QUESTION_SECTION_DEFINITIONS,
            newQuestionSectionDefinition,
            newQuestionSectionDefinition.id
          );
        } else if (questionSectionDefinitions[i].language === LANGUAGES.french) {
          newQuestionSectionDefinition = {
            id: newQuestionSectionDefinitionId,
            content: questionSectionDefinitions[i].content,
            question_section: newQuestionSection.id,
            language: LANGUAGES.french,
            question_section_type: newQuestionSection.question_section_type
          };
          this.props.addTestDefinitionField(
            QUESTION_SECTION_DEFINITIONS,
            newQuestionSectionDefinition,
            newQuestionSectionDefinition.id
          );
        }
        // incrementing variables
        newQuestionSectionDefinitionId += 1;
      }
      // incrementing variables
      newQuestionSectionId += 1;
      newQuestionSectionOrder += 1;
    }
    // ==================== QUESTION SECTION(S) DATA (END) ====================

    // ==================== ANSWERS DATA ====================
    // getting current answers
    const answers = this.props.answers.filter(obj => obj.question === question.id);
    // initializing needed variables
    let newAnswerId = getNextInNumberSeries(this.props.answers, "id");
    let newAnswerOrder = 1;
    let newAnswerDetailId = getNextInNumberSeries(this.props.answerDetails, "id");
    // looping in answers
    for (let i = 0; i < answers.length; i++) {
      // creating new answer
      const newAnswer = {
        id: newAnswerId,
        question: newQuestion.id,
        order: newAnswerOrder,
        scoring_value: 0,
        ppc_answer_id: ""
      };
      // updating redux states
      this.props.addTestDefinitionField(ANSWERS, newAnswer, newAnswer.id);
      // getting related answer details
      const answerDetails = this.props.answerDetails.filter(obj => obj.answer === answers[i].id);
      // looping in questionSectionDefinitions
      for (let i = 0; i < answerDetails.length; i++) {
        // creating new answer detail
        let newAnswerDetail = {};
        if (answerDetails[i].language === LANGUAGES.english) {
          newAnswerDetail = {
            id: newAnswerDetailId,
            answer: newAnswer.id,
            content: answerDetails[i].content,
            language: LANGUAGES.english
          };
          this.props.addTestDefinitionField(ANSWER_DETAILS, newAnswerDetail, newAnswerDetail.id);
        } else if (answerDetails[i].language === LANGUAGES.french) {
          newAnswerDetail = {
            id: newAnswerDetailId,
            answer: newAnswer.id,
            content: answerDetails[i].content,
            language: LANGUAGES.french
          };
          this.props.addTestDefinitionField(ANSWER_DETAILS, newAnswerDetail, newAnswerDetail.id);
        }
        // incrementing variables
        newAnswerDetailId += 1;
      }
      newAnswerId += 1;
      newAnswerOrder += 1;
    }
    // ==================== ANSWERS DATA (END) ====================

    // updating redux states
    this.props.replaceTestDefinitionField(QUESTIONS, questions);
    const newMCQuestion = {
      question_difficulty_type: QuestionDifficultyType.EASY,
      question: newQuestion.id
    };
    newMCQuestion.id = getNextInNumberSeries(this.props.multipleChoiceQuestions, "id");
    this.props.addTestDefinitionField(
      MULTIPLE_CHOICE_QUESTION_DETAILS,
      newMCQuestion,
      newMCQuestion.id
    );
    // trigger test definition field updates
    this.props.triggerTestDefinitionFieldUpdates();
  };

  handlePreview = () => {
    // initializing needed variables
    const question = { ...this.props.question, ...this.state.question };
    let isBilingualQuestion = false;
    // ==================== QUESTION SECTIONS ====================
    // getting question sections of current selected question
    const questionSections = this.props.questionSections.filter(
      obj => obj.question === question.id
    );
    // initializing finalQuestionSections
    const finalQuestionSections = [];
    // looping in questionSections
    for (let i = 0; i < questionSections.length; i++) {
      // getting related question section definitions
      const questionSectionDefinitions = this.props.questionSectionDefinitions.filter(
        obj => obj.question_section === questionSections[i].id
      );
      // looping in questionSectionDefinitions
      for (let j = 0; j < questionSectionDefinitions.length; j++) {
        // english text
        if (questionSectionDefinitions[j].language === LANGUAGES.english) {
          // populating finalQuestionSections array
          finalQuestionSections.push({
            id: questionSections[i].id,
            order: questionSections[i].order,
            question: questionSections[i].question,
            question_section_content: {
              content: questionSectionDefinitions[j].content,
              language: LANGUAGES.english
            },
            question_section_type: questionSections[i].question_section_type
          });
          // french text
        } else {
          // populating finalQuestionSections array
          finalQuestionSections.push({
            id: questionSections[i].id,
            order: questionSections[i].order,
            question: questionSections[i].question,
            question_section_content: {
              content: questionSectionDefinitions[j].content,
              language: LANGUAGES.french
            },
            question_section_type: questionSections[i].question_section_type
          });
        }
      }
    }
    // ==================== QUESTION SECTIONS (END) ====================

    // ==================== ANSWERS ====================
    // getting answers of current selected question
    const answers = this.props.answers.filter(obj => obj.question === question.id);
    // initializing finalQuestionAnswers
    const finalQuestionAnswers = [];
    // looping in answers
    for (let i = 0; i < answers.length; i++) {
      // getting related answer details
      const answerDetails = this.props.answerDetails.filter(obj => obj.answer === answers[i].id);
      // looping in answerDetails
      for (let j = 0; j < answerDetails.length; j++) {
        // english text
        if (answerDetails[j].language === LANGUAGES.english) {
          // populating finalQuestionAnswers array
          finalQuestionAnswers.push({
            id: answers[i].id,
            order: answers[i].order,
            question: answers[i].question,
            scoring_value: answers[i].scoring_value,
            content: answerDetails[j].content,
            language: LANGUAGES.english
          });
          // french text
        } else {
          // populating finalQuestionAnswers array
          finalQuestionAnswers.push({
            id: answers[i].id,
            order: answers[i].order,
            question: answers[i].question,
            scoring_value: answers[i].scoring_value,
            content: answerDetails[j].content,
            language: LANGUAGES.french
          });
        }
      }
    }

    // checking if this is a bilingual question
    // questions
    if (
      finalQuestionSections.filter(
        obj => obj.question_section_content.language === LANGUAGES.english
      ).length > 0 &&
      finalQuestionSections.filter(
        obj => obj.question_section_content.language === LANGUAGES.french
      ).length > 0
    ) {
      // set isBilingualQuestion to true
      isBilingualQuestion = true;
    }
    // answers
    else if (
      finalQuestionAnswers.filter(obj => obj.language === LANGUAGES.english).length > 0 &&
      finalQuestionAnswers.filter(obj => obj.language === LANGUAGES.french).length > 0
    ) {
      // set isBilingualQuestion to true
      isBilingualQuestion = true;
    }

    // updating states
    this.setState({
      showPreviewPopup: true,
      previewQuestionBilingualQuestion: isBilingualQuestion,
      previewQuestionQuestionNumber: question.order - 1,
      previewPopupQuestionSections: finalQuestionSections,
      previewPopupQuestionAnswers: finalQuestionAnswers
    });
    // ==================== ANSWERS (END) ====================
  };

  handleClosePreviewPopup = () => {
    this.setState({
      showPreviewPopup: false,
      previewQuestionBilingualQuestion: false,
      previewQuestionQuestionNumber: null,
      previewPopupQuestionSections: null,
      previewPopupQuestionAnswers: null
    });
  };

  handlePreviewQuestionLanguageSwitch = () => {
    if (this.state.previewQuestionLanguage === LANGUAGES.english) {
      this.setState({ previewQuestionLanguage: LANGUAGES.french });
    } else {
      this.setState({ previewQuestionLanguage: LANGUAGES.english });
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete(this.props.question.id);
    this.props.expandItem();
    // calling question rules validation
    // getting all questions except the one that has been deleted
    const questions = this.props.questions.filter(obj => obj.id !== this.props.question.id);
    const areQuestionsRulesValid = validateQuestionRulesData(
      questions,
      this.props.allQuestionListRules
    );
    // if test section has item_exposure enabled
    // initializing areQuestionsRulesValidWhenItemExposureEnabled
    let areQuestionsRulesValidWhenItemExposureEnabled = { is_valid: true, error_type: null };
    if (this.props.testSection.item_exposure) {
      areQuestionsRulesValidWhenItemExposureEnabled =
        validateQuestionRulesDataWithEnabledItemExposure(
          questions,
          this.props.allQuestionListRules
        );
      // setting question rules redux states
      this.props.setQuestionRulesValidStateWhenItemExposureEnabled(
        areQuestionsRulesValid.is_valid,
        areQuestionsRulesValid.error_type
      );
    }
    // setting redux question rules validity state
    this.props.setQuestionRulesValidState(areQuestionsRulesValid);

    // creating a copy of validationErrors and updating testDefinitionContainsValidQuestionRules attribute
    const copyOfValidationErrors = this.props.validationErrors;
    copyOfValidationErrors.testDefinitionContainsValidQuestionRules =
      areQuestionsRulesValid && areQuestionsRulesValidWhenItemExposureEnabled.is_valid;

    // if invalid question rules
    if (!copyOfValidationErrors.testDefinitionContainsValidQuestionRules) {
      // setting testDefinitionQuestionRulesErrorType
      copyOfValidationErrors.testDefinitionQuestionRulesErrorType =
        areQuestionsRulesValidWhenItemExposureEnabled.error_type;
    }

    // setting redux test definition validation errors
    this.props.setTestDefinitionValidationErrors(copyOfValidationErrors);

    // trigger test definition field updates
    this.props.triggerTestDefinitionFieldUpdates();

    this.closeDialog();
  };

  selectedOption = question => {
    for (const property of Object.getOwnPropertyNames(QuestionType)) {
      if (question.question_type === QuestionType[`${property}`]) {
        return { label: property, value: question.question_type };
      }
    }
    return undefined;
  };

  selectedBlock = question => {
    for (const block of this.props.questionBlockTypes) {
      if (question.question_block_type === block.id) {
        return { label: block.name, value: block.id };
      }
    }
    return "";
  };

  selectedDependencies = field => {
    const fieldVar = Array.isArray(field) ? field : [field];
    const array = [];
    for (const field of fieldVar) {
      for (const option of this.props.dependencyOptions) {
        if (field === option.id) {
          array.push({ label: option.order, value: option.id });
        }
      }
    }
    return array;
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ areSwitchStatesLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ areSwitchStatesLoading: false });
        }
      );
    });
  };

  handleEnablePilotQuestion = () => {
    this.modifyField("pilot", true);
  };

  handleDisablePilotQuestion = () => {
    this.modifyField("pilot", false);
  };

  handleEnableShuffleAnswerChoices = () => {
    this.modifyField("shuffle_answer_choices", true);
  };

  handleDisableShuffleAnswerChoices = () => {
    this.modifyField("shuffle_answer_choices", false);
  };

  render() {
    const question = {
      ...this.props.question,
      ...this.state.question
    };
    const questionType = question.question_type;

    const typeOptions = Object.getOwnPropertyNames(QuestionType).map(property => {
      return { label: property, value: QuestionType[property] };
    });

    const blockOptions = this.props.questionBlockTypes.map(block => {
      return { label: block.name, value: block.id };
    });

    // getting dependency options based on question_block_type
    const dependencyOptions = this.props.dependencyOptions
      .filter(obj => obj.question_block_type === question.question_block_type)
      .map(question => {
        return { label: `${question.order}`, value: question.id };
      });

    const selectedOption = this.selectedOption(question);
    const selectedBlock = this.selectedBlock(question);
    const selectedDependencies = this.selectedDependencies(question.dependencies);

    return (
      <div style={styles.mainContainer}>
        <Row style={styles.rowStyle}>
          {makeLabel("order", LOCALIZE.testBuilder.questions)}
          {makeTextBoxField(
            "order",
            LOCALIZE.testBuilder.questions,
            question.order,
            true,
            () => {},
            "question-order",
            true
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("ppc_question_id", LOCALIZE.testBuilder.questions)}
          {makeTextBoxField(
            "ppc_question_id",
            LOCALIZE.testBuilder.questions,
            question.ppc_question_id,
            true,
            this.onPpcQuestionIdChange,
            "ppc-question-id"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("question_type", LOCALIZE.testBuilder.questions)}
          {makeDropDownField(
            "question_type",
            LOCALIZE.testBuilder.questions,
            selectedOption,
            typeOptions,
            this.onSelectChange,
            true
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("question_block_type", LOCALIZE.testBuilder.questions)}
          {makeDropDownField(
            "question_block_type",
            LOCALIZE.testBuilder.questions,
            selectedBlock,
            blockOptions,
            this.onSelectChange,
            false,
            true,
            true
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("dependencies", LOCALIZE.testBuilder.questions)}
          {makeMultiDropDownField(
            "dependencies",
            LOCALIZE.testBuilder.questions,
            selectedDependencies,
            dependencyOptions,
            this.onMultiSelectChange
          )}
        </Row>
        {question.dependencies !== null && question.dependencies.length !== 0 && (
          <Row style={styles.rowStyle}>
            {makeLabel("dependent_order", LOCALIZE.testBuilder.questions)}
            {makeTextBoxField(
              "dependent_order",
              LOCALIZE.testBuilder.questions,
              question.dependent_order,
              this.state.isDependentOrderValid,
              this.dependentOrderValidation
            )}
          </Row>
        )}
        <Row style={styles.rowStyle}>
          {makeLabel("pilot", LOCALIZE.testBuilder.questions, "pilot-question-label")}
          <Col>
            <div style={styles.rightColumnContainer}>
              {!this.state.areSwitchStatesLoading && (
                <Switch
                  onChange={
                    question.pilot
                      ? this.handleDisablePilotQuestion
                      : this.handleEnablePilotQuestion
                  }
                  checked={question.pilot}
                  aria-labelledby="pilot-question-label-tooltip-make-label"
                  height={this.state.switchHeight}
                  width={this.state.switchWidth}
                  className="switch-custom-style"
                />
              )}
            </div>
          </Col>
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "shuffle_answer_choices",
            LOCALIZE.testBuilder.questions,
            "shuffle-answer-choices-label"
          )}
          <Col>
            <div style={styles.rightColumnContainer}>
              {!this.state.areSwitchStatesLoading && (
                <Switch
                  onChange={
                    question.shuffle_answer_choices
                      ? this.handleDisableShuffleAnswerChoices
                      : this.handleEnableShuffleAnswerChoices
                  }
                  checked={question.shuffle_answer_choices}
                  aria-labelledby="shuffle-answer-choices-label-tooltip-make-label"
                  height={this.state.switchHeight}
                  width={this.state.switchWidth}
                  className="switch-custom-style"
                />
              )}
            </div>
          </Col>
        </Row>

        {questionType === QuestionType.EMAIL && (
          <EmailQuestionForm
            question={question}
            questionDetails={this.props.questionDetails}
            handleSave={this.handleSave}
            handleDelete={this.confirmDelete}
            testLanguage={this.props.testLanguage}
          />
        )}
        {questionType === QuestionType.MULTIPLE_CHOICE && (
          <MultipleChoiceQuestionForm
            question={question}
            questionDetails={this.props.questionDetails}
            handleSave={this.handleSave}
            handleDelete={this.confirmDelete}
            testLanguage={this.props.testLanguage}
            handleDuplicate={this.handleDuplicate}
            handlePreview={this.handlePreview}
          />
        )}
        {this.state.previewPopupQuestionSections !== null && (
          <PopupBox
            show={this.state.showPreviewPopup}
            title={LOCALIZE.testBuilder.questions.previewPopup.title}
            handleClose={() => {}}
            description={
              <div>
                <div>
                  <QuestionFactory
                    questionSections={this.state.previewPopupQuestionSections}
                    index={this.state.previewQuestionQuestionNumber}
                    lang={this.state.previewQuestionLanguage}
                    isBilingualQuestion={this.state.previewQuestionBilingualQuestion}
                    calledFromQuestionPreview={true}
                  />
                  {this.state.previewQuestionBilingualQuestion
                    ? this.state.previewPopupQuestionAnswers.map(
                        (answer, answerIndex) =>
                          // getting answers in the right language
                          answer.language === this.state.previewQuestionLanguage && (
                            <div key={answerIndex}>
                              <MarkdownButtonAnswer
                                key={answerIndex}
                                answer={answer}
                                answerId={answer.id}
                                questionId={question.id}
                                handleClick={() => {}}
                                lang={this.state.previewQuestionLanguage}
                              />
                            </div>
                          )
                      )
                    : this.state.previewPopupQuestionAnswers.map((answer, answerIndex) => (
                        <div key={answerIndex}>
                          <MarkdownButtonAnswer
                            key={answerIndex}
                            answer={answer}
                            answerId={answer.id}
                            questionId={question.id}
                            handleClick={() => {}}
                            lang={this.state.previewQuestionLanguage}
                          />
                        </div>
                      ))}
                </div>
              </div>
            }
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={LOCALIZE.commons.close}
            rightButtonAction={this.handleClosePreviewPopup}
            leftButtonType={
              this.state.previewQuestionBilingualQuestion ? BUTTON_TYPE.secondary : BUTTON_TYPE.none
            }
            leftButtonTitle={
              this.state.previewQuestionLanguage === LANGUAGES.english
                ? LOCALIZE.commons.french
                : LOCALIZE.commons.english
            }
            leftButtonAction={this.handlePreviewQuestionLanguageSwitch}
          />
        )}
      </div>
    );
  }
}

export { QuestionForm as unconnectedQuestionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    questions: state.testBuilder.questions,
    multipleChoiceQuestions: state.testBuilder.multiple_choice_question_details,
    answers: state.testBuilder.answers,
    answerDetails: state.testBuilder.answer_details,
    questionSections: state.testBuilder.question_sections,
    questionSectionDefinitions: state.testBuilder.question_section_definitions,
    accommodations: state.accommodations,
    allQuestionListRules: state.testBuilder.question_list_rules,
    validationErrors: state.testBuilder.validation_errors
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      modifyTestDefinitionField,
      replaceTestDefinitionField,
      setQuestionRulesValidState,
      setTestDefinitionValidationErrors,
      triggerTestDefinitionFieldUpdates,
      setQuestionRulesValidStateWhenItemExposureEnabled,
      addTestDefinitionField
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(QuestionForm);
