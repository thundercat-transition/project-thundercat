import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Container, Col } from "react-bootstrap";
import {
  makeLabel,
  makeDropDownField,
  makeTextBoxField,
  makeTextAreaField,
  noSpecialCharacters,
  isNumberOrEmpty
} from "./helpers";
import {
  modifyTestDefinitionField,
  TEST_DEFINITION,
  setTestDefinitionValidationErrors,
  getTestSkillTypes,
  getTestSkillSubTypes
} from "../../modules/TestBuilderRedux";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faSpinner } from "@fortawesome/free-solid-svg-icons";
import getSwitchTransformScale from "../../helpers/switchTransformScale";
import Switch from "react-switch";
import { validateTestDefinitionData } from "./validation";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  borderBox: {
    borderTop: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red",
    margin: "5px"
  },
  rowStyle: {
    margin: "5px"
  },
  applyButtonContainer: {
    textAlign: "center",
    marginTop: 36
  },
  applyButton: {
    minWidth: 200
  },
  buttonLabel: {
    marginLeft: 6
  },
  customLabelForTextAreaStyle: {
    marginTop: 0,
    marginBottom: 0
  },
  customTextAreaStyle: {
    height: 60
  },
  activeState: {
    fontWeight: "bold",
    color: "#278400"
  },
  inactiveState: {
    fontWeight: "bold",
    color: "#923534"
  },
  rightColumnContainer: {
    display: "table-cell",
    verticalAlign: "middle",
    height: 32
  },
  validationErrorMessageContainer: {
    border: "1px solid #923534",
    padding: 12,
    marginBottom: 12
  },
  validationErrorMessage: {
    textAlign: "center",
    color: "#923534",
    fontWeight: "bold"
  },
  loading: {
    width: "100%",
    minHeight: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  }
};

class TestDefinition extends Component {
  localize = LOCALIZE.testBuilder.testDefinition;

  state = {
    test_definition: {},
    isVersionValid: true,
    isRetestValid: true,
    isTestCodeValid: true,
    isParentCodeValid: true,
    isFrNameValid: true,
    isEnNameValid: true,
    // default values
    switchHeight: 25,
    switchWidth: 50,
    areSwitchStatesLoading: false,
    showInvalidTestDefinitionDataError: false,
    // Dropdown - Test Skill Types
    isTestSkillTypeValid: true,
    testSkillTypeOptions: [],
    isLoadingTestSkillTypes: false,
    // Dropdown - Test Skill Sub-Types
    isTestSkillSubTypeValid: true,
    testSkillSubTypeOptions: [],
    isLoadingTestSkillSubTypes: false
  };

  componentDidMount = () => {
    this.getUpdatedSwitchDimensions();
    this.getValidationErrors();

    // Generate the Test Skill Types Dropdown
    this.getTestSkillTypeOptions();

    // If we have a selected Test Skill Type, then generate the Sub-Types Dropdown
    // --> in the case of a Null value for test_skill_type_id, the function won't do anything
    this.getTestSkillSubTypeOptions(this.props.test_definition.test_skill_type_id);
  };

  componentDidUpdate = prevProps => {
    // if font size gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
    // if test definition field gets updated
    if (
      prevProps.triggerTestDefinitionFieldUpdates !== this.props.triggerTestDefinitionFieldUpdates
    ) {
      // getting orders
      this.getValidationErrors();
    }
  };

  modifyField = (inputName, value) => {
    const newObj = { ...this.props.test_definition, ...this.state.test_definition };
    newObj[`${inputName}`] = value;
    this.setState({ test_definition: newObj });
  };

  retestValidation = (name, value) => {
    if (isNumberOrEmpty(value)) {
      this.setState({ isRetestValid: true });
      this.modifyField(name, parseInt(value) || 0);
    }
  };

  testCodeValidation = (name, value) => {
    const orginalValue = value;
    // allow only alphanumeric, slash and dash (0 to 25 chars)
    const regexExpression = /^([a-zA-Z0-9-]{0,25})$/;
    if (regexExpression.test(orginalValue)) {
      if (noSpecialCharacters(value)) {
        this.setState({ isTestCodeValid: true });
        this.modifyField(name, value.toUpperCase());
      } else {
        this.setState({ isTestCodeValid: false });
        this.modifyField(name, orginalValue);
      }
    }
  };

  parentCodeValidation = (name, value) => {
    const orginalValue = value;
    // allow only alphanumeric, slash and dash (0 to 25 chars)
    const regexExpression = /^([a-zA-Z0-9-]{0,25})$/;
    if (regexExpression.test(orginalValue)) {
      if (noSpecialCharacters(value)) {
        this.setState({ isParentCodeValid: true });
        this.modifyField(name, value.toUpperCase());
      } else {
        this.setState({ isParentCodeValid: false });
        this.modifyField(name, orginalValue);
      }
    }
  };

  enNameValidation = (name, value) => {
    // allow maximum of 175 chars (minimum 1 char)
    const regexExpression = /^(.{1,175})$/;
    if (regexExpression.test(value)) {
      this.setState({ isEnNameValid: true });
      this.modifyField(name, value);
    } else {
      this.setState({ isEnNameValid: false });
      this.modifyField(name, value);
    }
  };

  frNameValidation = (name, value) => {
    // allow maximum of 175 chars (minimum 1 char)
    const regexExpression = /^(.{1,175})$/;
    if (regexExpression.test(value)) {
      this.setState({ isFrNameValid: true });
      this.modifyField(name, value);
    } else {
      this.setState({ isFrNameValid: false });
      this.modifyField(name, value);
    }
  };

  getValidationErrors = () => {
    // initializing needed variables
    let showInvalidTestDefinitionDataError = false;

    // making sure the the validation errors state is defined
    if (typeof this.props.validationErrors !== "undefined") {
      // scoring method error
      if (!this.props.validationErrors.testDefinitionContainsValidTestDefinitionData) {
        showInvalidTestDefinitionDataError = true;
      }
    }
    // updating needed states
    this.setState({
      showInvalidTestDefinitionDataError: showInvalidTestDefinitionDataError
    });
  };

  // Generate the Test Skill Types Dropdown
  getTestSkillTypeOptions = () => {
    this.setState(
      {
        isLoadingTestSkillTypes: true
      },
      () => {
        this.props.getTestSkillTypes().then(response => {
          // updating needed states
          this.setState({
            testSkillTypeOptions: this.generateDropdownTestSkillTypesOptions(response.body),
            isLoadingTestSkillTypes: false
          });
        });
      }
    );
  };

  // Generate the Test Skill Sub-Types Dropdown - By providing the selected test_skill_type_id
  getTestSkillSubTypeOptions = test_skill_type_id => {
    if (test_skill_type_id) {
      this.setState(
        {
          isLoadingTestSkillSubTypes: true
        },
        () => {
          this.props.getTestSkillSubTypes(test_skill_type_id).then(response => {
            // updating needed states
            this.setState({
              testSkillSubTypeOptions: this.generateDropdownTestSkillSubTypesOptions(response.body),
              isLoadingTestSkillSubTypes: false
            });
          });
        }
      );
    }
  };

  // Generate the Test Skill Types Dropdown - Labels/Values
  generateDropdownTestSkillTypesOptions = options => {
    let newArray = [];
    for (let i = 0; i < options.length; i++) {
      newArray = [
        ...newArray,
        {
          label: options[i].test_skill_type_text[this.props.currentLanguage][0].text,
          value: options[i].id
        }
      ];
    }

    return newArray;
  };

  // Generate the Test Skill Sub-Types Dropdown - Labels/Values
  generateDropdownTestSkillSubTypesOptions = options => {
    let newArray = [];
    for (let i = 0; i < options.length; i++) {
      newArray = [
        ...newArray,
        {
          label: options[i].test_skill_sub_type_text[this.props.currentLanguage][0].text,
          value: options[i].id
        }
      ];
    }

    return newArray;
  };

  // Select the Test Skill Type in Dropdown
  testSkillTypeSelect = (event, action) => {
    // updating test_definition props
    setTimeout(() => {
      this.modifyField(action.name, event.value);
      this.setState({
        isTestSkillTypeValid: true
      });
    }, 150);

    // reset test skill sub type
    setTimeout(() => {
      this.modifyField("test_skill_sub_type_id", null);
    }, 100);

    this.setState(
      {
        isLoadingTestSkillSubTypes: true
      },
      () => {
        this.props.getTestSkillSubTypes(event.value).then(response => {
          // updating needed states
          this.setState({
            testSkillSubTypeOptions: this.generateDropdownTestSkillSubTypesOptions(response.body),
            isLoadingTestSkillSubTypes: false
          });
        });
      }
    );
  };

  // Select the Test Skill Sub-Type in Dropdown
  testSkillSubTypeSelect = (event, action) => {
    // updating test_definition props
    this.modifyField(action.name, event.value);
    this.setState({
      isTestSkillSubTypeValid: true
    });
  };

  // Get the Selected Test Skill Type in Options - Label/Value
  getSelectedTestSkillType = test_skill_type_id => {
    if (test_skill_type_id) {
      return this.state.testSkillTypeOptions.find(element => {
        return element.value === test_skill_type_id;
      });
    }
    return [];
  };

  // Get the Selected Test Skill Sub-Type in Options - Label/Value
  getSelectedTestSkillSubType = test_skill_sub_type_id => {
    if (test_skill_sub_type_id) {
      return this.state.testSkillSubTypeOptions.find(element => {
        return element.value === test_skill_sub_type_id;
      });
    }
    return [];
  };

  handleSave = () => {
    // updating redux state
    const newObj = {
      ...this.props.test_definition,
      ...this.state.test_definition,
      active: false
    };
    this.props.modifyTestDefinitionField(TEST_DEFINITION, newObj, this.props.test_definition.id);
    // calling field validation
    this.handleFieldsValidation(newObj);
  };

  handleFieldsValidation = testDefinitionData => {
    // initializing needed variables
    let isParentCodeValid = true;
    let isTestCodeValid = true;
    let isEnNameValid = true;
    let isFrNameValid = true;
    let isTestSkillTypeValid = true;
    let isTestSkillSubTypeValid = true;

    // making sure that all required fields are filled
    // Parent Code
    if (
      typeof testDefinitionData.parent_code === "undefined" ||
      testDefinitionData.parent_code === ""
    ) {
      isParentCodeValid = false;
    }
    // Test Code
    if (
      typeof testDefinitionData.test_code === "undefined" ||
      testDefinitionData.test_code === ""
    ) {
      isTestCodeValid = false;
    }
    // English Name
    if (typeof testDefinitionData.en_name === "undefined" || testDefinitionData.en_name === "") {
      isEnNameValid = false;
    }
    // French Name
    if (typeof testDefinitionData.fr_name === "undefined" || testDefinitionData.fr_name === "") {
      isFrNameValid = false;
    }

    // Test Skill Type
    // Verify that the test definition has a test skill type selected
    if (!testDefinitionData.test_skill_type_id) {
      isTestSkillTypeValid = false;
    }

    // Test Skill Sub Type
    // Verify that if we have selected a test skill type that has sub types, we selected one
    if (
      testDefinitionData.test_skill_type_id &&
      this.state.testSkillSubTypeOptions.length > 0 &&
      !testDefinitionData.test_skill_sub_type_id
    ) {
      isTestSkillSubTypeValid = false;
    }

    // updating states
    this.setState({
      isParentCodeValid: isParentCodeValid,
      isTestCodeValid: isTestCodeValid,
      isEnNameValid: isEnNameValid,
      isFrNameValid: isFrNameValid,
      isTestSkillTypeValid: isTestSkillTypeValid,
      isTestSkillSubTypeValid: isTestSkillSubTypeValid
    });

    // calling test definition data validation
    const validationErrors = validateTestDefinitionData(
      this.props.validationErrors,
      testDefinitionData,
      this.state.testSkillSubTypeOptions
    );

    // updating redux states
    this.props.setTestDefinitionValidationErrors(validationErrors);
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ areSwitchStatesLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ areSwitchStatesLoading: false });
        }
      );
    });
  };

  handleShowScoreSwitchUpdate = event => {
    this.modifyField("show_score", event);
  };

  handleShowResultSwitchUpdate = event => {
    this.modifyField("show_result", event);
  };

  // Note: the timeout is fixing an issue displaying the state properly (need to refactor)
  handleUITSwitchUpdate = event => {
    setTimeout(() => {
      this.modifyField("is_uit", event);
    }, 0);
    this.modifyField("is_public", false);
  };

  handleEnableSampleTest = () => {
    this.modifyField("show_score", false);
    this.modifyField("show_result", false);
    this.modifyField("is_uit", false);
    setTimeout(() => {
      this.modifyField("is_public", true);
    }, 0);
  };

  handleDisableSampleTest = () => {
    setTimeout(() => {
      this.modifyField("is_public", false);
    }, 0);
    this.modifyField("count_up", false);
  };

  handleEnableCountUp = () => {
    this.modifyField("count_up", true);
  };

  handleDisableCountUp = () => {
    this.modifyField("count_up", false);
  };

  render() {
    const test_definition = { ...this.props.test_definition, ...this.state.test_definition };
    const testName = `: ${this.props.test_definition.test_code} v${this.props.test_definition.version}`;

    const customFontSize = {
      // adding 5px to current font size
      fontSize: `${parseInt(this.props.accommodations.fontSize.split("px")[0]) + 5}px`
    };

    const selectedTestSkillType = this.getSelectedTestSkillType(test_definition.test_skill_type_id);
    const selectedTestSkillSubType = this.getSelectedTestSkillSubType(
      test_definition.test_skill_sub_type_id
    );

    return (
      <div style={styles.mainContainer}>
        <h2>{this.localize.title + testName}</h2>
        {this.state.showInvalidTestDefinitionDataError && (
          <div style={styles.validationErrorMessageContainer}>
            <p style={{ ...styles.validationErrorMessage, ...customFontSize }}>
              {LOCALIZE.testBuilder.testDefinition.validationErrors.invalidTestDefinitionDataError}
            </p>
          </div>
        )}

        <Container>
          <div style={styles.borderBox} />
          {this.props.test_definition && (
            <>
              <Row style={styles.rowStyle}>
                {makeLabel("parent_code", this.localize)}
                {makeTextBoxField(
                  "parent_code",
                  this.localize,
                  test_definition.parent_code,
                  this.state.isParentCodeValid,
                  this.parentCodeValidation
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("test_code", this.localize)}
                {makeTextBoxField(
                  "test_code",
                  this.localize,
                  test_definition.test_code,
                  this.state.isTestCodeValid,
                  this.testCodeValidation
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("en_name", this.localize)}
                {makeTextBoxField(
                  "en_name",
                  this.localize,
                  test_definition.en_name,
                  this.state.isEnNameValid,
                  this.enNameValidation
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("fr_name", this.localize)}
                {makeTextBoxField(
                  "fr_name",
                  this.localize,
                  test_definition.fr_name,
                  this.state.isFrNameValid,
                  this.frNameValidation
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("version", this.localize)}
                {makeTextBoxField(
                  "version",
                  this.localize,
                  test_definition.version,
                  this.state.isVersionValid,
                  {},
                  "",
                  true
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("test_skill_type_id", this.localize)}
                {this.state.isLoadingTestSkillTypes ? (
                  <Col>
                    <div className="fa fa-spinner fa-spin" style={styles.loading}>
                      <FontAwesomeIcon icon={faSpinner} />
                    </div>
                  </Col>
                ) : (
                  makeDropDownField(
                    "test_skill_type_id",
                    this.localize,
                    selectedTestSkillType,
                    this.state.testSkillTypeOptions,
                    this.testSkillTypeSelect,
                    false,
                    this.state.isTestSkillTypeValid,
                    false,
                    // orderByValues
                    true
                  )
                )}
              </Row>
              {this.state.testSkillSubTypeOptions.length > 0 && (
                <Row style={styles.rowStyle}>
                  {makeLabel("test_skill_sub_type_id", this.localize)}
                  {this.state.isLoadingTestSkillSubTypes ? (
                    <Col>
                      <div className="fa fa-spinner fa-spin" style={styles.loading}>
                        <FontAwesomeIcon icon={faSpinner} />
                      </div>
                    </Col>
                  ) : (
                    makeDropDownField(
                      "test_skill_sub_type_id",
                      this.localize,
                      selectedTestSkillSubType,
                      this.state.testSkillSubTypeOptions,
                      this.testSkillSubTypeSelect,
                      false,
                      this.state.isTestSkillSubTypeValid,
                      false,
                      // orderByValues
                      true
                    )
                  )}
                </Row>
              )}
              <Row style={styles.rowStyle}>
                {makeLabel("retest_period", this.localize)}
                {makeTextBoxField(
                  "retest_period",
                  this.localize,
                  String(test_definition.retest_period),
                  this.state.isRetestValid,
                  this.retestValidation
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel(
                  "version_notes",
                  this.localize,
                  "test-definition-version-notes-label",
                  styles.customLabelForTextAreaStyle
                )}
                {makeTextAreaField(
                  "version_notes",
                  this.localize,
                  test_definition.version_notes,
                  true,
                  {},
                  "test-definition-version-notes",
                  true,
                  styles.customTextAreaStyle
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("show_score", this.localize, "activate-deactivate-show-score-switch")}
                <Col>
                  <div style={styles.rightColumnContainer}>
                    {!this.state.areSwitchStatesLoading && (
                      <Switch
                        onChange={this.handleShowScoreSwitchUpdate}
                        checked={test_definition.show_score}
                        aria-labelledby="activate-deactivate-show-score-switch"
                        height={this.state.switchHeight}
                        width={this.state.switchWidth}
                        className="switch-custom-style"
                      />
                    )}
                  </div>
                </Col>
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("show_result", this.localize, "activate-deactivate-show-result-switch")}
                <Col>
                  <div style={styles.rightColumnContainer}>
                    {!this.state.areSwitchStatesLoading && (
                      <Switch
                        onChange={this.handleShowResultSwitchUpdate}
                        checked={test_definition.show_result}
                        aria-labelledby="activate-deactivate-show-result-switch"
                        height={this.state.switchHeight}
                        width={this.state.switchWidth}
                        className="switch-custom-style"
                      />
                    )}
                  </div>
                </Col>
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("is_uit", this.localize, "activate-deactivate-is-uit-switch")}
                <Col>
                  <div style={styles.rightColumnContainer}>
                    {!this.state.areSwitchStatesLoading && (
                      <Switch
                        onChange={this.handleUITSwitchUpdate}
                        checked={test_definition.is_uit}
                        aria-labelledby="activate-deactivate-is-uit-switch"
                        height={this.state.switchHeight}
                        width={this.state.switchWidth}
                        className="switch-custom-style"
                      />
                    )}
                  </div>
                </Col>
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("is_public", this.localize, "activate-deactivate-sample-test-switch")}
                <Col>
                  <div style={styles.rightColumnContainer}>
                    {!this.state.areSwitchStatesLoading && (
                      <Switch
                        onChange={
                          test_definition.is_public
                            ? this.handleDisableSampleTest
                            : this.handleEnableSampleTest
                        }
                        checked={test_definition.is_public}
                        aria-labelledby="activate-deactivate-sample-test-switch-tooltip-make-label"
                        height={this.state.switchHeight}
                        width={this.state.switchWidth}
                        className="switch-custom-style"
                      />
                    )}
                  </div>
                </Col>
              </Row>
              {test_definition.is_public && (
                <Row style={styles.rowStyle}>
                  {makeLabel("count_up", this.localize, "activate-deactivate-count-up-switch")}
                  <Col>
                    <div style={styles.rightColumnContainer}>
                      {!this.state.areSwitchStatesLoading && (
                        <Switch
                          onChange={
                            test_definition.count_up
                              ? this.handleDisableCountUp
                              : this.handleEnableCountUp
                          }
                          checked={test_definition.count_up}
                          aria-labelledby="activate-deactivate-count-up-switch"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                          className="switch-custom-style"
                        />
                      )}
                    </div>
                  </Col>
                </Row>
              )}
              <Row style={styles.rowStyle}>
                {makeLabel("active_status", this.localize)}
                <Col>
                  <div style={styles.rightColumnContainer}>
                    <span
                      style={test_definition.active ? styles.activeState : styles.inactiveState}
                    >
                      {test_definition.active ? LOCALIZE.commons.active : LOCALIZE.commons.inactive}
                    </span>
                  </div>
                </Col>
              </Row>

              <div style={styles.applyButtonContainer}>
                <CustomButton
                  label={
                    <>
                      <FontAwesomeIcon icon={faCheck} />
                      <span style={styles.buttonLabel}>{LOCALIZE.commons.applyButton}</span>
                    </>
                  }
                  action={this.handleSave}
                  customStyle={styles.applyButton}
                  buttonTheme={THEME.PRIMARY}
                />
              </div>
            </>
          )}
        </Container>
      </div>
    );
  }
}

export { TestDefinition as unconnectedTestDefinition };

const mapStateToProps = (state, ownProps) => {
  // done because only one test definition is returned ever
  const INDEX = 0;
  return {
    test_definition: state.testBuilder.test_definition[INDEX],
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    triggerTestDefinitionFieldUpdates: state.testBuilder.triggerTestDefinitionFieldUpdates,
    validationErrors: state.testBuilder.validation_errors
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      modifyTestDefinitionField,
      setTestDefinitionValidationErrors,
      getTestSkillTypes,
      getTestSkillSubTypes
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestDefinition);
