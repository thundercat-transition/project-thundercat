import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Col, Row } from "react-bootstrap";
import { makeLabel, makeTextBoxField, makeSaveDeleteButtons, isNumber } from "./helpers";
import { modifyScoringThresholdField, SCORING_THRESHOLD } from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import getSwitchTransformScale from "../../helpers/switchTransformScale";
import Switch from "react-switch";

class ScoringThresholdForm extends Component {
  static propTypes = {
    selectedScoringThresholdIndex: PropTypes.number.isRequired,
    scoringThreshold: PropTypes.array.isRequired,
    scoringThreasholdElementId: PropTypes.number.isRequired,
    handleDelete: PropTypes.func.isRequired
  };

  state = {
    showDialog: false,
    minimumScoreContent:
      this.props.scoringThreshold[this.props.selectedScoringThresholdIndex].minimum_score !==
      LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.minimumScore.placeholder
        ? this.props.scoringThreshold[this.props.selectedScoringThresholdIndex].minimum_score
        : "",
    maximumScoreContent:
      this.props.scoringThreshold[this.props.selectedScoringThresholdIndex].maximum_score !==
      LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.maximumScore.placeholder
        ? this.props.scoringThreshold[this.props.selectedScoringThresholdIndex].maximum_score
        : "",
    enConversionValueContent:
      this.props.scoringThreshold[this.props.selectedScoringThresholdIndex].en_conversion_value !==
      LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.enConversionValue
        .placeholder
        ? this.props.scoringThreshold[this.props.selectedScoringThresholdIndex].en_conversion_value
        : "",
    frConversionValueContent:
      this.props.scoringThreshold[this.props.selectedScoringThresholdIndex].fr_conversion_value !==
      LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.frConversionValue
        .placeholder
        ? this.props.scoringThreshold[this.props.selectedScoringThresholdIndex].fr_conversion_value
        : "",
    scoringMethodResultValidIndefinitelyState:
      this.props.scoringThreshold[this.props.selectedScoringThresholdIndex]
        .scoring_method_result_valid_indefinitely,
    isMinimumScoreFieldValid: true,
    isMaximumScoreFieldValid: true,
    isEnConversionValueFieldValid: true,
    isFrConversionValueFieldValid: true,
    minScoreGreaterThanMaxScoreError: false,
    // default values
    switchHeight: 25,
    switchWidth: 50,
    areSwitchStatesLoading: false
  };

  componentDidMount = () => {
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = prevProps => {
    // if font size gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ areSwitchStatesLoading: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ areSwitchStatesLoading: false });
        }
      );
    });
  };

  getMinimumScoreContent = (inputName, value) => {
    // allow only numeric entries (positive or negative)
    const regex = /^-?\d*$/;
    if (value === "" || regex.test(value)) {
      this.setState({ minimumScoreContent: value });
    }
  };

  getMaximumScoreContent = (inputName, value) => {
    // allow only numeric entries (positive or negative)
    const regex = /^-?\d*$/;
    if (value === "" || regex.test(value)) {
      this.setState({ maximumScoreContent: value });
    }
  };

  getEnConversionValueContent = (inputName, value) => {
    // allow maximum of 50 chars
    const regexExpression = /^(.{0,50})$/;
    if (regexExpression.test(value)) {
      this.setState({ enConversionValueContent: value });
    }
  };

  getFrConversionValueContent = (inputName, value) => {
    // allow maximum of 50 chars
    const regexExpression = /^(.{0,50})$/;
    if (regexExpression.test(value)) {
      this.setState({ frConversionValueContent: value });
    }
  };

  orderValidation = (name, value) => {
    if (isNumber(value)) {
      this.setState({ isOrderValid: true });
      this.modifyField(name, parseInt(value));
    }
  };

  onSelectChange = (event, action) => {
    this.modifyField(action.name, event.value);
  };

  selectScoringMethodResultValidIndefinitely = event => {
    // reinitializing error message states
    this.setState({
      scoringMethodResultValidIndefinitelyState: event
    });
  };

  handleSave = () => {
    if (this.isValidScoringThresholdItem()) {
      this.props.modifyScoringThresholdField(
        SCORING_THRESHOLD,
        {
          id: this.props.scoringThreasholdElementId,
          minimum_score: this.state.minimumScoreContent,
          maximum_score: this.state.maximumScoreContent,
          en_conversion_value: this.state.enConversionValueContent,
          fr_conversion_value: this.state.frConversionValueContent,
          scoring_method_result_valid_indefinitely:
            this.state.scoringMethodResultValidIndefinitelyState
        },
        this.props.scoringThreasholdElementId,
        "minimum_score"
      );
    } else {
      this.getValidationErrors();
    }
  };

  getValidationErrors = () => {
    if (
      this.state.minimumScoreContent === "" ||
      this.state.minimumScoreContent ===
        LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.minimumScore.placeholder
    ) {
      this.setState({ isMinimumScoreFieldValid: false });
    } else {
      this.setState({ isMinimumScoreFieldValid: true });
    }
    if (
      this.state.maximumScoreContent === "" ||
      this.state.maximumScoreContent ===
        LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.maximumScore.placeholder
    ) {
      this.setState({ isMaximumScoreFieldValid: false });
    } else {
      this.setState({ isMaximumScoreFieldValid: true });
    }
    if (
      this.state.enConversionValueContent === "" ||
      this.state.enConversionValueContent ===
        LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.enConversionValue
          .placeholder
    ) {
      this.setState({ isEnConversionValueFieldValid: false });
    } else {
      this.setState({ isEnConversionValueFieldValid: true });
    }
    if (
      this.state.frConversionValueContent === "" ||
      this.state.frConversionValueContent ===
        LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.frConversionValue
          .placeholder
    ) {
      this.setState({ isFrConversionValueFieldValid: false });
    } else {
      this.setState({ isFrConversionValueFieldValid: true });
    }
  };

  isValidScoringThresholdItem = () => {
    if (
      this.state.minimumScoreContent !== "" &&
      this.state.minimumScoreContent !==
        LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.minimumScore
          .placeholder &&
      this.state.maximumScoreContent !== "" &&
      this.state.maximumScoreContent !==
        LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.maximumScore
          .placeholder &&
      this.state.enConversionValueContent !== "" &&
      this.state.enConversionValueContent !==
        LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.enConversionValue
          .placeholder &&
      this.state.frConversionValueContent !== "" &&
      this.state.frConversionValueContent !==
        LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem.frConversionValue
          .placeholder
    ) {
      if (parseInt(this.state.minimumScoreContent) > parseInt(this.state.maximumScoreContent)) {
        this.setState({ minScoreGreaterThanMaxScoreError: true });
        return false;
      }
      this.setState({
        isMinimumScoreFieldValid: true,
        isMaximumScoreFieldValid: true,
        isEnConversionValueFieldValid: true,
        isFrConversionValueFieldValid: true,
        minScoreGreaterThanMaxScoreError: false
      });

      return true;
    }
    return false;
  };

  confirmDelete = () => {
    this.props.handleDelete(
      this.props.scoringThreshold[this.props.selectedScoringThresholdIndex].id
    );
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  render() {
    return (
      <div style={styles.mainContainer}>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "minimumScore",
            LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem
          )}
          {makeTextBoxField(
            "minimumScore",
            LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem,
            this.state.minimumScoreContent,
            this.state.isMinimumScoreFieldValid,
            this.getMinimumScoreContent
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "maximumScore",
            LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem
          )}
          {makeTextBoxField(
            "maximumScore",
            LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem,
            this.state.maximumScoreContent,
            this.state.isMaximumScoreFieldValid && !this.state.minScoreGreaterThanMaxScoreError,
            this.getMaximumScoreContent
          )}
        </Row>

        <Row style={styles.rowStyle}>
          {makeLabel(
            "enConversionValue",
            LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem
          )}
          {makeTextBoxField(
            "enConversionValue",
            LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem,
            this.state.enConversionValueContent,
            this.state.isEnConversionValueFieldValid,
            this.getEnConversionValueContent
          )}
        </Row>

        <Row style={styles.rowStyle}>
          {makeLabel(
            "frConversionValue",
            LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem
          )}
          {makeTextBoxField(
            "frConversionValue",
            LOCALIZE.testBuilder.scoringMethod.scoringThreshold.collapsingItem,
            this.state.frConversionValueContent,
            this.state.isFrConversionValueFieldValid,
            this.getFrConversionValueContent
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "resultValidIndefinitely",
            LOCALIZE.testBuilder.scoringMethod,
            "result-valid-indefinitely-switch"
          )}
          <Col>
            <div style={styles.rightColumnContainer}>
              {!this.state.areSwitchStatesLoading && (
                <Switch
                  onChange={this.selectScoringMethodResultValidIndefinitely}
                  checked={this.state.scoringMethodResultValidIndefinitelyState}
                  aria-labelledby="result-valid-indefinitely-switch"
                  height={this.state.switchHeight}
                  width={this.state.switchWidth}
                  className="switch-custom-style"
                />
              )}
            </div>
          </Col>
        </Row>
        <Row className="justify-content-between" style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={LOCALIZE.testBuilder.scoringMethod.scoringThreshold.deletePopup.title}
          description={
            <div>
              <p>{LOCALIZE.testBuilder.scoringMethod.scoringThreshold.deletePopup.content}</p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { ScoringThresholdForm as unconnectedScoringThresholdForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    scoringThreshold: state.testBuilder.scoring_threshold,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyScoringThresholdField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ScoringThresholdForm);
