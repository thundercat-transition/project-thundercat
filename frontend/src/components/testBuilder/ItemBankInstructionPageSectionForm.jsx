import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import {
  makeLabel,
  makeDropDownField,
  testLanguageHasEnglish,
  testLanguageHasFrench,
  makeSaveDeleteButtons,
  allValid,
  getNextInNumberSeries,
  getFormattedItemStems
} from "./helpers";
import {
  getItemBanksForTestBuilder,
  getAllItemBanks,
  getAllInstructions,
  getItemContent
} from "../../modules/ItemBankRedux";
import { UnconnectedTopTabs as TopTabs } from "../commons/TopTabs";

import getItemContentToPreview from "../../helpers/itemBankItemPreview";
import { ITEM_CONTENT_TYPE, ITEM_DEVELOPMENT_STATUS } from "./itemBank/items/Constants";
import { PageSectionType } from "../testFactory/Constants";
import {
  PAGE_SECTION_DEFINITIONS,
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  addTestDefinitionField
} from "../../modules/TestBuilderRedux";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { LANGUAGES } from "../commons/Translation";

export const styles = {
  instructionPreviewStyle: {
    width: "100%",
    height: "auto",
    minHeight: 150
  },
  rowStyle: {
    margin: "5px"
  }
};

class ItemBankInstructionPageSectionForm extends Component {
  state = {
    pageDefinition: { page_section_type: PageSectionType.ITEM_BANK_INSTRUCTION },
    itemBankOptions: [],
    itemBankInstructionOptions: [],
    selectedItemBank:
      typeof this.props.pageDefinition.selected_item_bank !== "undefined"
        ? this.props.pageDefinition.selected_item_bank
        : [],
    selectedItemBankInstruction:
      typeof this.props.pageDefinition.selected_item !== "undefined"
        ? this.props.pageDefinition.selected_item
        : [],
    displayContentTypeDropdown: [],
    selectedContentType:
      typeof this.props.pageDefinition.selected_content_type !== "undefined"
        ? this.props.pageDefinition.selected_content_type
        : [],
    displaySelectedInstruction: false,
    selectedItemBankInstructionContentEn: "",
    selectedItemBankInstructionContentFr: ""
  };

  modifyPageDefinition = (inputName, value) => {
    const newObj = { ...this.props.pageDefinition, ...this.state.pageDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ pageDefinition: newObj });
  };

  componentDidMount = () => {
    // if all dropdowns have data provided
    if (
      Object.keys(this.state.selectedItemBank).length > 0 &&
      Object.keys(this.state.selectedItemBankInstruction).length > 0 &&
      Object.keys(this.state.selectedContentType).length > 0
    ) {
      this.populateItemBankOptions();
      this.populateItemBankInstructionOptions();
      this.populateItemContentTypeOptions();

      this.displaySelectedInstruction();
    } else {
      this.populateItemBankOptions();
    }
  };

  populateItemBankOptions = () => {
    // initializing needed variables
    const itemBankOptions = [];

    this.setState({ isLoadingItemBankOptions: true }, () => {
      // getting item banks
      this.props.getItemBanksForTestBuilder().then(response => {
        // looping in response
        for (let i = 0; i < response.length; i++) {
          itemBankOptions.push({
            label: `${response[i].custom_item_bank_id} (${
              response[i][`${this.props.currentLanguage}_name`]
            })`,
            value: response[i].id,
            language_id: response[i].language
          });
        }
        this.setState({
          itemBankOptions: itemBankOptions
        });
      });
    });
  };

  populateItemBankInstructionOptions = () => {
    const itemBankInstructionOptions = [];

    // getting all item bank instructions for selected item bank
    this.props
      // second parameter set to true, so we're ignoring drafts
      .getAllInstructions(this.state.selectedItemBank.value, true)
      .then(response => {
        // looping in response
        for (let i = 0; i < response.length; i++) {
          if (response[i].development_status_codename !== ITEM_DEVELOPMENT_STATUS.draft) {
            itemBankInstructionOptions.push({
              label: LOCALIZE.formatString(
                LOCALIZE.testBuilder.itemBankInstructionPageSectionForm.instructionDropdownTitle,
                response[i].system_id,
                response[i].historical_id,
                response[i].version
              ),
              value: response[i].system_id
            });
          }
        }
        this.setState({
          itemBankInstructionOptions: itemBankInstructionOptions
        });
      });
  };

  populateItemContentTypeOptions = () => {
    const ItemContentTypeOptions = [];
    ItemContentTypeOptions.push({
      label:
        LOCALIZE.testBuilder.itemBankInstructionPageSectionForm.itemContentDropdownOptions.regular,
      value: ITEM_CONTENT_TYPE.markdown
    });
    ItemContentTypeOptions.push({
      label:
        LOCALIZE.testBuilder.itemBankInstructionPageSectionForm.itemContentDropdownOptions
          .screenReader,
      value: ITEM_CONTENT_TYPE.screenReader
    });
    this.setState({ ItemContentTypeOptions: ItemContentTypeOptions });

    this.setState({
      ItemContentTypeOptions: ItemContentTypeOptions
    });
  };

  selectItemBank = (event, action) => {
    this.modifyPageDefinition(action.name, event.value);
    this.setState(
      {
        selectedItemBank: event,
        selectedItemBankInstruction: [],
        selectedContentType: [],
        displaySelectedInstruction: false
      },
      () => {
        this.populateItemBankInstructionOptions();
      }
    );
  };

  selectItemBankInstruction = (event, action) => {
    this.modifyPageDefinition(action.name, event.value);
    // this.modifyPageDefinition(action.name, event.value);
    this.setState(
      {
        selectedItemBankInstruction: event,
        selectedContentType: [],
        displayContentTypeDropdown: true
      },
      () => {
        this.populateItemContentTypeOptions();
      }
    );
  };

  selectContentType = (event, action) => {
    this.modifyPageDefinition(action.name, event.value);
    this.setState(
      {
        selectedContentType: event
      },
      () => {
        this.displaySelectedInstruction();
      }
    );
  };

  displaySelectedInstruction = () => {
    if (this.state.selectedItemBankInstruction.value) {
      // Get text from selected instruction
      this.props
        .getItemContent(this.state.selectedItemBankInstruction.value, null)
        .then(response => {
          const stemsObj = getFormattedItemStems(
            response,
            this.props.testLanguage,
            this.state.selectedContentType.value
          );
          this.setState({ selectedItemBankInstructionContentEn: stemsObj.stemsEn });
          this.setState({ selectedItemBankInstructionContentFr: stemsObj.stemsFr });
          this.setState({ displaySelectedInstruction: true });
        });
    }
  };

  getLanguageTabs = () => {
    const TABS = [];
    // If Bilingual
    if (this.props.testLanguage === "--") {
      // Render both tabs
      TABS.push({
        key: LOCALIZE.commons.english,
        tabName: LOCALIZE.commons.english,
        body: (
          <div>
            <textarea
              value={this.state.selectedItemBankInstructionContentEn}
              style={styles.instructionPreviewStyle}
              disabled={true}
            ></textarea>
          </div>
        )
      });
      TABS.push({
        key: LOCALIZE.commons.french,
        tabName: LOCALIZE.commons.french,
        body: (
          <div>
            <textarea
              value={this.state.selectedItemBankInstructionContentFr}
              style={styles.instructionPreviewStyle}
              disabled={true}
            ></textarea>
          </div>
        )
      });
    } else {
      // Checking which tab to show (language)
      if (testLanguageHasEnglish(this.props.testLanguage)) {
        TABS.push({
          key: LOCALIZE.commons.english,
          tabName: LOCALIZE.commons.english,
          body: (
            <div>
              <textarea
                value={this.state.selectedItemBankInstructionContentEn}
                style={styles.instructionPreviewStyle}
                disabled={true}
              ></textarea>
            </div>
          )
        });
      }
      if (testLanguageHasFrench(this.props.testLanguage)) {
        TABS.push({
          key: LOCALIZE.commons.french,
          tabName: LOCALIZE.commons.french,
          body: (
            <div>
              <textarea
                value={this.state.selectedItemBankInstructionContentFr}
                style={styles.instructionPreviewStyle}
                disabled={true}
              ></textarea>
            </div>
          )
        });
      }
    }
    return TABS;
  };

  handleSave = () => {
    if (allValid(this.state)) {
      const objArray = this.props.pageSectionDefinitions.filter(
        obj => obj.page_section !== this.props.componentPageSection.id
      );

      this.props.replaceTestDefinitionField(PAGE_SECTION_DEFINITIONS, objArray);

      const newObj = {
        page_section: this.props.componentPageSection.id,
        system_id: this.state.selectedItemBankInstruction.value,
        page_section_type: PageSectionType.ITEM_BANK_INSTRUCTION,
        selected_item: this.state.selectedItemBankInstruction,
        selected_item_bank: this.state.selectedItemBank,
        selected_content_type: this.state.selectedContentType
      };

      newObj.id = getNextInNumberSeries(this.props.pageSectionDefinitions, "id");

      this.props.addTestDefinitionField(PAGE_SECTION_DEFINITIONS, newObj, newObj.id);

      this.props.handleSave(PageSectionType.ITEM_BANK_INSTRUCTION);
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete();
    this.closeDialog();
  };

  render() {
    const TABS = this.getLanguageTabs();
    const { itemBankOptions } = this.state;
    const { itemBankInstructionOptions } = this.state;
    const { ItemContentTypeOptions } = this.state;

    return (
      <div id="unit-test-markdown-page-section">
        <Row style={styles.rowStyle}>
          {
            // Select Item Bank
          }
          {makeLabel("item_bank", LOCALIZE.testBuilder.componentPageSections)}
          {makeDropDownField(
            "item_bank",
            LOCALIZE.testBuilder.componentPageSections,
            this.state.selectedItemBank,
            itemBankOptions,
            this.selectItemBank
          )}
        </Row>
        {this.state.selectedItemBank.value && (
          <Row style={styles.rowStyle}>
            {
              // Select Instruction
            }
            {makeLabel("item", LOCALIZE.testBuilder.componentPageSections)}
            {makeDropDownField(
              "item",
              LOCALIZE.testBuilder.componentPageSections,
              this.state.selectedItemBankInstruction,
              itemBankInstructionOptions,
              this.selectItemBankInstruction
            )}
          </Row>
        )}
        {this.state.selectedItemBankInstruction.value && (
          <Row style={styles.rowStyle}>
            {
              // Select Item Content Type
            }
            {makeLabel("content_type", LOCALIZE.testBuilder.componentPageSections)}
            {makeDropDownField(
              "content_type",
              LOCALIZE.testBuilder.componentPageSections,
              this.state.selectedContentType,
              ItemContentTypeOptions,
              this.selectContentType
            )}
          </Row>
        )}
        {this.state.selectedContentType.value && (
          <div>
            <div id="unit-test-markdown-page-section">
              <TopTabs TABS={TABS} defaultTab={TABS[0].key} />
            </div>
            <Row className="justify-content-between" style={styles.buttonRowStyle}>
              {makeSaveDeleteButtons(
                this.handleSave,
                this.openDialog,
                LOCALIZE.commons.applyButton,
                LOCALIZE.commons.deleteButton
              )}
            </Row>
            <PopupBox
              show={this.state.showDialog}
              handleClose={this.closeDialog}
              title={"Delete Confirmation"}
              description={
                <div>
                  <p>
                    Deleting this item will delete all the content displayed. Are you sure you want
                    to delete this object?
                  </p>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              rightButtonType={BUTTON_TYPE.danger}
              rightButtonTitle={LOCALIZE.commons.deleteButton}
              rightButtonAction={this.confirmDelete}
            />
          </div>
        )}
      </div>
    );
  }
}

export { ItemBankInstructionPageSectionForm as UnconnectedItemBankInstructionPageSectionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    pageSectionDefinitions: state.testBuilder.page_section_definitions,
    pageSections: state.testBuilder.page_sections
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getItemBanksForTestBuilder,
      getAllItemBanks,
      getAllInstructions,
      getItemContent,
      getItemContentToPreview,
      modifyTestDefinitionField,
      replaceTestDefinitionField,
      addTestDefinitionField
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ItemBankInstructionPageSectionForm);
