import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import OlaConfigs from "./OlaConfigs";
import TestAssessors from "./TestAssessors";
import TimeSlots from "./TimeSlots";
import { setTestCenterOlaSelectedTopTabState } from "../../../modules/TestCenterRedux";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  }
};

class Ola extends Component {
  render() {
    const TABS = [
      {
        key: "ola-config",
        tabName:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs.olaConfigs
            .title,
        body: <OlaConfigs />
      },
      {
        key: "test-assessors",
        tabName:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
            .testAssessors.title,
        body: <TestAssessors />
      },
      {
        key: "time-slots",
        tabName:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs.timeSlots
            .title,
        body: <TimeSlots />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.title}</h2>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey={
                  this.props.testCenterOlaSelectedTopTab !== "" &&
                  this.props.testCenterOlaSelectedTopTab !== null
                    ? this.props.testCenterOlaSelectedTopTab
                    : "ola-config"
                }
                id="ola-tabs"
                style={styles.tabNavigation}
                onSelect={event => this.props.setTestCenterOlaSelectedTopTabState(event)}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testCenterOlaSelectedTopTab: state.testCenter.testCenterOlaSelectedTopTab
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setTestCenterOlaSelectedTopTabState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Ola);
