/* eslint-disable lines-between-class-members */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Col } from "react-bootstrap";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSave,
  faCheck,
  faTimes,
  faSpinner,
  faPlusCircle,
  faPencilAlt,
  faTrashAlt
} from "@fortawesome/free-solid-svg-icons";
import {
  createNewTestCenterOlaVacationBlock,
  deleteTestCenterOlaVacationBlock,
  editTestCenterOlaVacationBlock,
  getSelectedTestCenterData,
  setTestCenterData,
  updateTestCenterOlaConfigsData
} from "../../../modules/TestCenterRedux";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import DatePicker, { ERROR_MESSAGE, validateDatePicked } from "../../commons/DatePicker";
import populateCustomFutureYearsDateOptions from "../../../helpers/populateCustomDatePickerOptions";
import { resetDatePickedStates } from "../../../modules/DatePickerRedux";
import StyledTooltip, { EFFECT, TYPE } from "../../authentication/StyledTooltip";

export const DATE_SOURCE = {
  dateFrom: "dateFrom",
  dateTo: "dateTo"
};

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  },
  firstColumnPopup: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumnPopup: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  mainContainer: {
    margin: "24px 0"
  },
  rowContainer: {
    width: "90%",
    margin: "12px auto"
  },
  rowContainerWithError: {
    width: "90%",
    margin: "0px auto"
  },
  labelCol: {
    padding: "0px"
  },
  inputCol: {
    textAlign: "left"
  },
  inputTitles: {
    fontWeight: "bold"
  },
  inputs: {
    width: "20%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4,
    textAlign: "center"
  },
  applySavedButtonContainer: {
    margin: "36px auto",
    textAlign: "center"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  applySavedButton: {
    minWidth: 250
  },
  addVacationBlockButtonContainer: {
    width: "90%",
    margin: "12px auto"
  },
  tableContainer: {
    width: "90%",
    margin: "0 auto"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  actionIcon: {
    transform: "scale(1.2)"
  },
  allUnset: {
    all: "unset"
  },
  boldText: {
    fontWeight: "bold"
  }
};

class OlaConfigs extends Component {
  dateFromDayRef = React.createRef();
  dateFromMonthRef = React.createRef();
  dateFromYearRef = React.createRef();

  dateToDayRef = React.createRef();
  dateToMonthRef = React.createRef();
  dateToYearRef = React.createRef();

  state = {
    bookingDelay:
      this.props.testCenterData.test_center_ola_configs !== null
        ? this.props.testCenterData.test_center_ola_configs.booking_delay
        : 0,
    advancedBookingDelay:
      this.props.testCenterData.test_center_ola_configs !== null
        ? this.props.testCenterData.test_center_ola_configs.advanced_booking_delay
        : 0,
    changesDetected: false,
    showSavedConfirmationPopup: false,
    applyButtonLoading: false,
    rowsDefinition: {},
    currentlyLoading: false,
    showAddVacationBlockPopup: false,
    showEditVacationBlockPopup: false,
    triggerDateFromValidation: false,
    isValidDateFrom: false,
    dateFrom: null,
    triggerDateToValidation: false,
    isValidDateTo: false,
    dateTo: null,
    isValidDateCombination: true,
    isValidAddEditVacationBlockPopupForm: false,
    enAvailabilityContent: 100,
    frAvailabilityContent: 100,
    vacationBlockToEdit: {},
    showVacationBlockAlreadyExistsError: false,
    vacationBlockToDelete: {}
  };

  componentDidMount = () => {
    // populating vacation blocks table
    this.populateVacationBlocks();
  };

  componentDidUpdate = prevProps => {
    // if testCenterData gets updated
    if (prevProps.testCenterData !== this.props.testCenterData) {
      // making sure that the states are up to date + setting applyButtonLoading to false
      this.setState(
        {
          applyButtonLoading: false,
          bookingDelay: this.props.testCenterData.test_center_ola_configs.booking_delay,
          advancedBookingDelay:
            this.props.testCenterData.test_center_ola_configs.advanced_booking_delay
        },
        () => {
          // detecting changes
          this.detectChanges();
        }
      );
      // populating vacation blocks table
      this.populateVacationBlocks();
    }
  };

  getBookingDelayContent = event => {
    const bookingDelay = event.target.value;
    // only allow numbers to be entered (maximum of 4 chars)
    const regex = /^([0-9]{0,4})$/;
    if (event.target.value === "" || regex.test(event.target.value)) {
      this.setState(
        {
          bookingDelay: event.target.value !== "" ? parseInt(bookingDelay) : 0
        },
        () => {
          // detecting changes
          this.detectChanges();
        }
      );
    }
  };

  getAdvancedBookingDelayContent = event => {
    const advancedBookingDelay = event.target.value;
    // only allow numbers to be entered (maximum of 4 chars)
    const regex = /^([0-9]{0,4})$/;
    if (event.target.value === "" || regex.test(event.target.value)) {
      this.setState(
        {
          advancedBookingDelay: event.target.value !== "" ? parseInt(advancedBookingDelay) : 0
        },
        () => {
          // detecting changes
          this.detectChanges();
        }
      );
    }
  };

  detectChanges = () => {
    // initializing changesDetected
    let changesDetected = false;

    // if either of the fields has been updated
    if (
      this.props.testCenterData.test_center_ola_configs === null ||
      this.state.bookingDelay !== this.props.testCenterData.test_center_ola_configs.booking_delay ||
      this.state.advancedBookingDelay !==
        this.props.testCenterData.test_center_ola_configs.advanced_booking_delay
    ) {
      // setting changesDetected to true
      changesDetected = true;
    }

    // updating needed states
    this.setState({ changesDetected: changesDetected });
  };

  handleApplyOlaConfigsChanges = () => {
    this.setState({ applyButtonLoading: true }, () => {
      // setting body
      const body = {
        test_center_id: this.props.testCenterData.id,
        booking_delay: this.state.bookingDelay,
        advanced_booking_delay: this.state.advancedBookingDelay
      };
      this.props.updateTestCenterOlaConfigsData(body).then(response => {
        if (response.ok) {
          this.openSavedConfirmationPopup();
          // getting and setting test center data
          this.props.getSelectedTestCenterData(this.props.testCenterData.id).then(response => {
            this.props.setTestCenterData(response);
          });
          // should never happen
        } else {
          throw new Error(
            "An error occurred during the update test center OLA configs data process"
          );
        }
      });
    });
  };

  openSavedConfirmationPopup = () => {
    this.setState({ showSavedConfirmationPopup: true });
  };

  closeSavedConfirmationPopup = () => {
    this.setState({ showSavedConfirmationPopup: false });
  };

  populateVacationBlocks = () => {
    this.setState({ currentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];

      // looping in response given
      for (let i = 0; i < this.props.testCenterData.test_center_ola_vacation_blocks.length; i++) {
        const currentResult = this.props.testCenterData.test_center_ola_vacation_blocks[i];
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: `${currentResult.date_from} - ${currentResult.date_to}`,
          column_2: `${currentResult.availability_en}%`,
          column_3: `${currentResult.availability_fr}%`,
          column_4: (
            <>
              <StyledTooltip
                id={`test-center-ola-vacation-block-edit-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`test-center-ola-vacation-block-edit-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faPencilAlt} style={styles.actionIcon} />
                        </>
                      }
                      action={() => {
                        this.openEditVacationBlockPopup(currentResult);
                      }}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.olaConfigs.vacationBlocksTable.editButtonTooltipAccessibility,
                        currentResult.date_from,
                        currentResult.date_to
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.olaConfigs.vacationBlocksTable.editButtonTooltip
                      }
                    </p>
                  </div>
                }
              />
              <StyledTooltip
                id={`test-center-ola-vacation-block-delete-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`test-center-ola-vacation-block-delete-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faTrashAlt} style={styles.actionIcon} />
                        </>
                      }
                      action={() => {
                        this.openDeleteVacationBlockConfirmationPopup(currentResult);
                      }}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.olaConfigs.vacationBlocksTable.deleteButtonTooltipAccessibility,
                        currentResult.date_from,
                        currentResult.date_to
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>{LOCALIZE.commons.deleteButton}</p>
                  </div>
                }
              />
            </>
          )
        });
      }

      // updating rowsDefinition object with provided data and needed style
      rowsDefinition = {
        column_1_style: COMMON_STYLE.LEFT_TEXT,
        column_2_style: COMMON_STYLE.CENTERED_TEXT,
        column_3_style: COMMON_STYLE.CENTERED_TEXT,
        column_4_style: COMMON_STYLE.CENTERED_TEXT,
        data: data
      };

      // saving results in state
      this.setState({
        rowsDefinition: rowsDefinition,
        currentlyLoading: false
      });
    });
  };

  openEditVacationBlockPopup = vacationBlockData => {
    this.setState(
      {
        showEditVacationBlockPopup: true,
        showAddVacationBlockPopup: false,
        vacationBlockToEdit: vacationBlockData,
        dateFrom: vacationBlockData.date_from,
        isValidDateFrom: true,
        dateTo: vacationBlockData.date_to,
        isValidDateTo: true,
        enAvailabilityContent: vacationBlockData.availability_en,
        frAvailabilityContent: vacationBlockData.availability_fr
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  openDeleteVacationBlockConfirmationPopup = vacationBlockData => {
    this.setState({
      showDeleteVacationBlockConfirmationPopup: true,
      vacationBlockToDelete: vacationBlockData
    });
  };

  closeDeleteVacationBlockConfirmationPopup = () => {
    this.setState({ showDeleteVacationBlockConfirmationPopup: false, vacationBlockToDelete: {} });
  };

  handleDeleteVacationBlock = () => {
    // setting body
    const body = {
      id: this.state.vacationBlockToDelete.id
    };
    this.props.deleteTestCenterOlaVacationBlock(body).then(response => {
      if (response.ok) {
        // getting and setting test center data
        this.props.getSelectedTestCenterData(this.props.testCenterData.id).then(response => {
          this.props.setTestCenterData(response);
        });
        // closing popup
        this.closeDeleteVacationBlockConfirmationPopup();
        // should never happen
      } else {
        throw new Error(
          "An error occurred during the delete test center OLA vacation block process"
        );
      }
    });
  };

  openAddVacationBlockPopup = () => {
    this.setState({ showAddVacationBlockPopup: true, showEditVacationBlockPopup: false });
  };

  closeAddEditVacationBlockPopup = () => {
    this.props.resetDatePickedStates();
    this.setState({
      showAddVacationBlockPopup: false,
      showEditVacationBlockPopup: false,
      vacationBlockToEdit: {},
      isValidDateFrom: false,
      dateFrom: null,
      isValidDateTo: false,
      dateTo: null,
      isValidDateCombination: true,
      isValidAddEditVacationBlockPopupForm: false,
      enAvailabilityContent: 100,
      frAvailabilityContent: 100,
      showVacationBlockAlreadyExistsError: false
    });
  };

  handleAddVacationBlock = () => {
    // setting body
    const body = {
      test_center_id: this.props.testCenterData.id,
      date_from: this.state.dateFrom,
      date_to: this.state.dateTo,
      en_availability: this.state.enAvailabilityContent,
      fr_availability: this.state.frAvailabilityContent
    };
    this.props.createNewTestCenterOlaVacationBlock(body).then(response => {
      if (response.status === 409) {
        this.setState({ showVacationBlockAlreadyExistsError: true }, () => {
          // focusing on error
          if (
            document.getElementById(
              "ola-configs-add-vacation-block-popup-vacation-block-already-exists-error"
            )
          ) {
            document
              .getElementById(
                "ola-configs-add-vacation-block-popup-vacation-block-already-exists-error"
              )
              .focus();
          }
        });
      } else if (response.ok) {
        // getting and setting test center data
        this.props.getSelectedTestCenterData(this.props.testCenterData.id).then(response => {
          this.props.setTestCenterData(response);
        });
        // closing popup
        this.closeAddEditVacationBlockPopup();
        // should never happen
      } else {
        throw new Error(
          "An error occurred during the create new test center OLA vacation block process"
        );
      }
    });
  };

  handleEditVacationBlock = () => {
    // setting body
    const body = {
      id: this.state.vacationBlockToEdit.id,
      test_center_id: this.props.testCenterData.id,
      date_from: this.state.dateFrom,
      date_to: this.state.dateTo,
      en_availability: this.state.enAvailabilityContent,
      fr_availability: this.state.frAvailabilityContent
    };
    this.props.editTestCenterOlaVacationBlock(body).then(response => {
      if (response.status === 409) {
        this.setState({ showVacationBlockAlreadyExistsError: true }, () => {
          // focusing on error
          if (
            document.getElementById(
              "ola-configs-add-vacation-block-popup-vacation-block-already-exists-error"
            )
          ) {
            document
              .getElementById(
                "ola-configs-add-vacation-block-popup-vacation-block-already-exists-error"
              )
              .focus();
          }
        });
      } else if (response.ok) {
        // close popup
        this.closeAddEditVacationBlockPopup();
        // getting and setting test center data
        this.props.getSelectedTestCenterData(this.props.testCenterData.id).then(response => {
          this.props.setTestCenterData(response);
        });
        // should never happen
      } else {
        throw new Error("An error occurred during the update test center OLA configs data process");
      }
    });
  };

  handleDateUpdates = dateSource => {
    // date from
    if (dateSource === DATE_SOURCE.dateFrom) {
      setTimeout(() => {
        // date is fully provided
        if (
          this.props.completeDatePicked.split("-")[0] !== "" &&
          this.props.completeDatePicked.split("-")[1] !== "" &&
          this.props.completeDatePicked.split("-")[2] !== ""
        ) {
          // triggering date validation
          this.setState(
            { triggerDateFromValidation: !this.state.triggerDateFromValidation },
            () => {
              setTimeout(() => {
                // validating dates and set states
                this.setState(
                  {
                    isValidDateCombination: this.dateValidation(),
                    isValidDateFrom: this.props.completeDateValidState,
                    dateFrom: this.props.completeDatePicked,
                    showVacationBlockAlreadyExistsError: false
                  },
                  () => {
                    // validating form
                    this.validateForm();
                  }
                );
              }, 100);
            }
          );
        }
      }, 100);
      // date to
    } else if (dateSource === DATE_SOURCE.dateTo) {
      setTimeout(() => {
        // date is fully provided
        if (
          this.props.completeDatePicked.split("-")[0] !== "" &&
          this.props.completeDatePicked.split("-")[1] !== "" &&
          this.props.completeDatePicked.split("-")[2] !== ""
        ) {
          // triggering date validation
          this.setState({ triggerDateToValidation: !this.state.triggerDateToValidation }, () => {
            setTimeout(() => {
              // validating dates and set states
              this.setState(
                {
                  isValidDateCombination: this.dateValidation(),
                  isValidDateTo: this.props.completeDateValidState,
                  dateTo: this.props.completeDatePicked,
                  showVacationBlockAlreadyExistsError: false
                },
                () => {
                  // validating form
                  this.validateForm();
                }
              );
            }, 100);
          });
        }
      }, 100);
    }
  };

  // date validation
  dateValidation = () => {
    // initializing from and to
    let from = null;
    let to = null;

    // date from has been fully provided
    if (
      this.dateFromDayRef.current.props.value !== "" &&
      this.dateFromMonthRef.current.props.value !== "" &&
      this.dateFromYearRef.current.props.value !== ""
    ) {
      from = `${this.dateFromDayRef.current.props.value.value}-${this.dateFromMonthRef.current.props.value.value}-${this.dateFromYearRef.current.props.value.value}`;
    }

    // date to has been fully provided
    if (
      this.dateToDayRef.current.props.value !== "" &&
      this.dateToMonthRef.current.props.value !== "" &&
      this.dateToYearRef.current.props.value !== ""
    ) {
      to = `${this.dateToDayRef.current.props.value.value}-${this.dateToMonthRef.current.props.value.value}-${this.dateToYearRef.current.props.value.value}`;
    }

    // both dates are fully provided
    if (from !== null && to !== null) {
      // converting string dates in dates (provided format: 'dd-mm-yyyy')
      const dateFrom = new Date(from.split("-")[2], from.split("-")[1] - 1, from.split("-")[0]);
      const dateTo = new Date(to.split("-")[2], to.split("-")[1] - 1, to.split("-")[0]);

      // dates validation
      // reformatting date in format 'yyyy-mm-dd' for validation function
      const isValidDateFrom = validateDatePicked(
        `${from.split("-")[2]}-${from.split("-")[1]}-${from.split("-")[0]}`
      );
      const isValidDateTo = validateDatePicked(
        `${to.split("-")[2]}-${to.split("-")[1]}-${to.split("-")[0]}`
      );

      // invalid from date
      if (!isValidDateFrom || !isValidDateTo) {
        this.setState({
          displayInvalidDateFromError: !isValidDateFrom,
          displayInvalidDateToError: !isValidDateTo,
          displayDateError: false,
          generateReportButtonDisabled: true
        });
        return false;
      }
      // date from is greater to date to (invalid)
      if (dateFrom > dateTo) {
        this.setState({
          displayDateError: true,
          generateReportButtonDisabled: true,
          displayInvalidDateFromError: !isValidDateFrom,
          displayInvalidDateToError: !isValidDateTo
        });
        return false;
      }
      // everything is valid
      return true;
    }
    // dates are not fully provided (temporarily return true)
    return true;
  };

  getEnAvailabilityContent = event => {
    const enAvailabilityContent = event.target.value;
    // only allow percentage number (0-100)
    const regex = /^((100)|(\d{1,2}(\.\d{1,8})?))$/;
    if (event.target.value === "" || regex.test(event.target.value)) {
      this.setState(
        {
          enAvailabilityContent: event.target.value !== "" ? parseInt(enAvailabilityContent) : 0
        },
        () => {
          // validating form
          this.validateForm();
        }
      );
    }
  };

  getFrAvailabilityContent = event => {
    const frAvailabilityContent = event.target.value;
    // only allow percentage number (0-100)
    const regex = /^((100)|(\d{1,2}(\.\d{1,8})?))$/;
    if (event.target.value === "" || regex.test(event.target.value)) {
      this.setState(
        {
          frAvailabilityContent: event.target.value !== "" ? parseInt(frAvailabilityContent) : 0
        },
        () => {
          // validating form
          this.validateForm();
        }
      );
    }
  };

  validateForm = () => {
    // initializing isValidAddEditVacationBlockPopupForm
    let isValidAddEditVacationBlockPopupForm = false;

    // getting valid state from all needed fields
    const { isValidDateFrom, isValidDateTo, isValidDateCombination } = this.state;

    // if all needed fields are valid
    if (isValidDateFrom && isValidDateTo && isValidDateCombination) {
      isValidAddEditVacationBlockPopupForm = true;
    }

    this.setState({ isValidAddEditVacationBlockPopupForm: isValidAddEditVacationBlockPopupForm });
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs.olaConfigs
            .vacationBlocksTable.dateRange,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs.olaConfigs
            .vacationBlocksTable.enAvailability,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs.olaConfigs
            .vacationBlocksTable.frAvailability,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs.olaConfigs
            .vacationBlocksTable.actions,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <div>
          <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelCol}
            >
              <label htmlFor="ola-configs-booking-delay-input" style={styles.inputTitles}>
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                    .olaConfigs.bookingDelayLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputCol}
            >
              <input
                id="ola-configs-booking-delay-input"
                tabIndex={0}
                className={"valid-field"}
                type="text"
                value={this.state.bookingDelay}
                onChange={this.getBookingDelayContent}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
              />
            </Col>
          </Row>
          <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
              style={styles.labelCol}
            >
              <label htmlFor="ola-configs-advanced-booking-delay-input" style={styles.inputTitles}>
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                    .olaConfigs.advancedBookingDelayLabel
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
              style={styles.inputCol}
            >
              <input
                id="ola-configs-advanced-booking-delay-input"
                tabIndex={0}
                className={"valid-field"}
                type="text"
                value={this.state.advancedBookingDelay}
                onChange={this.getAdvancedBookingDelayContent}
                style={{
                  ...styles.inputs,
                  ...accommodationsStyle
                }}
              />
            </Col>
          </Row>
        </div>
        <div style={styles.applySavedButtonContainer}>
          <CustomButton
            label={
              this.state.applyButtonLoading ? (
                // eslint-disable-next-line jsx-a11y/label-has-associated-control
                <label className="fa fa-spinner fa-spin">
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              ) : (
                <>
                  <FontAwesomeIcon icon={this.state.changesDetected ? faSave : faCheck} />
                  <span
                    id="ola-configs-apply-saved-button"
                    style={styles.buttonLabel}
                    className="notranslate"
                  >
                    {this.state.changesDetected
                      ? LOCALIZE.commons.applyButton
                      : LOCALIZE.commons.saved}
                  </span>
                </>
              )
            }
            customStyle={styles.applySavedButton}
            action={this.state.changesDetected ? this.handleApplyOlaConfigsChanges : () => {}}
            buttonTheme={this.state.changesDetected ? THEME.PRIMARY : THEME.SUCCESS}
            disabled={
              this.state.applyButtonLoading
                ? BUTTON_STATE.disabled
                : this.state.changesDetected
                  ? BUTTON_STATE.enabled
                  : BUTTON_STATE.disabled
            }
          />
        </div>
        <div>
          <div style={styles.addVacationBlockButtonContainer}>
            <CustomButton
              label={
                <>
                  <FontAwesomeIcon icon={faPlusCircle} />
                  <span
                    id="ola-configs-add-vacation-block"
                    style={styles.buttonLabel}
                    className="notranslate"
                  >
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                        .topTabs.olaConfigs.addVacationBlockButton
                    }
                  </span>
                </>
              }
              action={this.openAddVacationBlockPopup}
              buttonTheme={THEME.PRIMARY}
            />
          </div>
          <div style={styles.tableContainer}>
            <GenericTable
              classnamePrefix="ola-configs-vacation-blocks"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                  .olaConfigs.vacationBlocksTable.noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
        </div>
        <PopupBox
          show={this.state.showSavedConfirmationPopup}
          handleClose={() => {}}
          size="lg"
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
              .olaConfigs.savedConfirmationPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.success}
                title={LOCALIZE.commons.success}
                message={
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                        .topTabs.olaConfigs.savedConfirmationPopup.systemMessageDescription
                    }
                  </p>
                }
              />
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonAction={this.closeSavedConfirmationPopup}
          rightButtonIcon={faTimes}
        />
        <PopupBox
          show={this.state.showAddVacationBlockPopup || this.state.showEditVacationBlockPopup}
          handleClose={() => {}}
          title={
            this.state.showAddVacationBlockPopup
              ? LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                  .olaConfigs.addEditVacationBlockPopup.addTitle
              : LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                  .olaConfigs.addEditVacationBlockPopup.editTitle
          }
          description={
            <div>
              <p>
                {this.state.showAddVacationBlockPopup
                  ? LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                      .olaConfigs.addEditVacationBlockPopup.addDescription
                  : LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                      .olaConfigs.addEditVacationBlockPopup.editDescription}
              </p>
              <div>
                <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumnPopup.xl}
                    lg={columnSizes.firstColumnPopup.lg}
                    md={columnSizes.firstColumnPopup.md}
                    sm={columnSizes.firstColumnPopup.sm}
                    xs={columnSizes.firstColumnPopup.xs}
                  >
                    <label
                      id="ola-configs-add-vacation-block-popup-date-from-label"
                      style={styles.inputTitles}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.olaConfigs.addEditVacationBlockPopup.dateFromLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnPopup.xl}
                    lg={columnSizes.secondColumnPopup.lg}
                    md={columnSizes.secondColumnPopup.md}
                    sm={columnSizes.secondColumnPopup.sm}
                    xs={columnSizes.secondColumnPopup.xs}
                  >
                    <DatePicker
                      dateDayFieldRef={this.dateFromDayRef}
                      dateMonthFieldRef={this.dateFromMonthRef}
                      dateYearFieldRef={this.dateFromYearRef}
                      dateLabelId={"ola-configs-add-vacation-block-popup-date-from"}
                      ariaLabelledBy={"ola-configs-add-vacation-block-popup-date-from-label"}
                      triggerValidation={this.state.triggerDateFromValidation}
                      displayValidIcon={false}
                      customYearOptions={populateCustomFutureYearsDateOptions(3)}
                      onInputChange={() => this.handleDateUpdates(DATE_SOURCE.dateFrom)}
                      showDatePickerInInvalidStyling={
                        // valid date picked, but there is a vacation block already exists error or an invalid date combination
                        this.state.isValidDateTo &&
                        (this.state.showVacationBlockAlreadyExistsError ||
                          !this.state.isValidDateCombination)
                      }
                      initialDateYearValue={
                        this.state.showEditVacationBlockPopup
                          ? {
                              label: this.state.dateFrom.split("-")[0],
                              value: Number(this.state.dateFrom.split("-")[0])
                            }
                          : null
                      }
                      initialDateMonthValue={
                        this.state.showEditVacationBlockPopup
                          ? {
                              label: this.state.dateFrom.split("-")[1],
                              value: Number(this.state.dateFrom.split("-")[1])
                            }
                          : null
                      }
                      initialDateDayValue={
                        this.state.showEditVacationBlockPopup
                          ? {
                              label: this.state.dateFrom.split("-")[2],
                              value: Number(this.state.dateFrom.split("-")[2])
                            }
                          : null
                      }
                      skipDatePickedReduxUpdateOnInitialProvidedData={
                        this.state.showEditVacationBlockPopup
                      }
                      futureDateValidation={true}
                      menuPlacement={"top"}
                    />
                  </Col>
                </Row>
                <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumnPopup.xl}
                    lg={columnSizes.firstColumnPopup.lg}
                    md={columnSizes.firstColumnPopup.md}
                    sm={columnSizes.firstColumnPopup.sm}
                    xs={columnSizes.firstColumnPopup.xs}
                  >
                    <label
                      id="ola-configs-add-vacation-block-popup-date-to-label"
                      style={styles.inputTitles}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.olaConfigs.addEditVacationBlockPopup.dateToLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnPopup.xl}
                    lg={columnSizes.secondColumnPopup.lg}
                    md={columnSizes.secondColumnPopup.md}
                    sm={columnSizes.secondColumnPopup.sm}
                    xs={columnSizes.secondColumnPopup.xs}
                  >
                    <DatePicker
                      dateDayFieldRef={this.dateToDayRef}
                      dateMonthFieldRef={this.dateToMonthRef}
                      dateYearFieldRef={this.dateToYearRef}
                      dateLabelId={"ola-configs-add-vacation-block-popup-date-to"}
                      ariaLabelledBy={"ola-configs-add-vacation-block-popup-date-to-label"}
                      triggerValidation={this.state.triggerDateToValidation}
                      displayValidIcon={false}
                      customYearOptions={populateCustomFutureYearsDateOptions(3)}
                      onInputChange={() => this.handleDateUpdates(DATE_SOURCE.dateTo)}
                      showDatePickerInInvalidStyling={
                        // valid date picked, but there is a vacation block already exists error or an invalid date combination
                        this.state.isValidDateTo &&
                        (this.state.showVacationBlockAlreadyExistsError ||
                          !this.state.isValidDateCombination)
                      }
                      initialDateYearValue={
                        this.state.showEditVacationBlockPopup
                          ? {
                              label: this.state.dateTo.split("-")[0],
                              value: Number(this.state.dateTo.split("-")[0])
                            }
                          : null
                      }
                      initialDateMonthValue={
                        this.state.showEditVacationBlockPopup
                          ? {
                              label: this.state.dateTo.split("-")[1],
                              value: Number(this.state.dateTo.split("-")[1])
                            }
                          : ""
                      }
                      initialDateDayValue={
                        this.state.showEditVacationBlockPopup
                          ? {
                              label: this.state.dateTo.split("-")[2],
                              value: Number(this.state.dateTo.split("-")[2])
                            }
                          : ""
                      }
                      skipDatePickedReduxUpdateOnInitialProvidedData={
                        this.state.showEditVacationBlockPopup
                      }
                      futureDateValidation={true}
                      menuPlacement={"top"}
                    />
                  </Col>
                </Row>
                {!this.state.isValidDateCombination &&
                  this.state.isValidDateFrom &&
                  this.state.isValidDateTo && (
                    <Row
                      role="presentation"
                      className="align-items-center"
                      style={styles.rowContainerWithError}
                    >
                      <Col
                        xl={columnSizes.firstColumnPopup.xl}
                        lg={columnSizes.firstColumnPopup.lg}
                        md={columnSizes.firstColumnPopup.md}
                        sm={columnSizes.firstColumnPopup.sm}
                        xs={columnSizes.firstColumnPopup.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.secondColumnPopup.xl}
                        lg={columnSizes.secondColumnPopup.lg}
                        md={columnSizes.secondColumnPopup.md}
                        sm={columnSizes.secondColumnPopup.sm}
                        xs={columnSizes.secondColumnPopup.xs}
                      >
                        <label
                          id="ola-configs-add-vacation-block-popup-date-to-label"
                          style={styles.errorMessage}
                        >
                          {
                            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                              .topTabs.olaConfigs.addEditVacationBlockPopup
                              .invalidDateCombinationError
                          }
                        </label>
                      </Col>
                    </Row>
                  )}
                {this.state.isValidDateCombination &&
                  this.state.isValidDateFrom &&
                  this.state.isValidDateTo &&
                  this.state.showVacationBlockAlreadyExistsError && (
                    <Row
                      role="presentation"
                      className="align-items-center"
                      style={styles.rowContainerWithError}
                    >
                      <Col
                        xl={columnSizes.firstColumnPopup.xl}
                        lg={columnSizes.firstColumnPopup.lg}
                        md={columnSizes.firstColumnPopup.md}
                        sm={columnSizes.firstColumnPopup.sm}
                        xs={columnSizes.firstColumnPopup.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.secondColumnPopup.xl}
                        lg={columnSizes.secondColumnPopup.lg}
                        md={columnSizes.secondColumnPopup.md}
                        sm={columnSizes.secondColumnPopup.sm}
                        xs={columnSizes.secondColumnPopup.xs}
                      >
                        <label
                          id="ola-configs-add-vacation-block-popup-vacation-block-already-exists-error"
                          style={styles.errorMessage}
                          tabIndex={0}
                        >
                          {
                            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                              .topTabs.olaConfigs.addEditVacationBlockPopup
                              .vacationBlockAlreadyExistsError
                          }
                        </label>
                      </Col>
                    </Row>
                  )}
                <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumnPopup.xl}
                    lg={columnSizes.firstColumnPopup.lg}
                    md={columnSizes.firstColumnPopup.md}
                    sm={columnSizes.firstColumnPopup.sm}
                    xs={columnSizes.firstColumnPopup.xs}
                  >
                    <label
                      htmlFor="ola-configs-add-vacation-block-popup-en-availability-input"
                      style={styles.inputTitles}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.olaConfigs.addEditVacationBlockPopup.enAvailabilityLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnPopup.xl}
                    lg={columnSizes.secondColumnPopup.lg}
                    md={columnSizes.secondColumnPopup.md}
                    sm={columnSizes.secondColumnPopup.sm}
                    xs={columnSizes.secondColumnPopup.xs}
                  >
                    <input
                      id="ola-configs-add-vacation-block-popup-en-availability-input"
                      tabIndex={0}
                      className={"valid-field"}
                      type="text"
                      value={this.state.enAvailabilityContent}
                      onChange={this.getEnAvailabilityContent}
                      style={{
                        ...styles.inputs,
                        ...accommodationsStyle
                      }}
                    />
                  </Col>
                </Row>
                <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumnPopup.xl}
                    lg={columnSizes.firstColumnPopup.lg}
                    md={columnSizes.firstColumnPopup.md}
                    sm={columnSizes.firstColumnPopup.sm}
                    xs={columnSizes.firstColumnPopup.xs}
                  >
                    <label
                      htmlFor="ola-configs-add-vacation-block-popup-fr-availability-input"
                      style={styles.inputTitles}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.olaConfigs.addEditVacationBlockPopup.frAvailabilityLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnPopup.xl}
                    lg={columnSizes.secondColumnPopup.lg}
                    md={columnSizes.secondColumnPopup.md}
                    sm={columnSizes.secondColumnPopup.sm}
                    xs={columnSizes.secondColumnPopup.xs}
                  >
                    <input
                      id="ola-configs-add-vacation-block-popup-fr-availability-input"
                      tabIndex={0}
                      className={"valid-field"}
                      type="text"
                      value={this.state.frAvailabilityContent}
                      onChange={this.getFrAvailabilityContent}
                      style={{
                        ...styles.inputs,
                        ...accommodationsStyle
                      }}
                    />
                  </Col>
                </Row>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeAddEditVacationBlockPopup}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            this.state.showAddVacationBlockPopup
              ? LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                  .olaConfigs.addEditVacationBlockPopup.rightButton
              : LOCALIZE.commons.saveButton
          }
          rightButtonAction={
            this.state.showAddVacationBlockPopup
              ? this.handleAddVacationBlock
              : this.handleEditVacationBlock
          }
          rightButtonIcon={this.state.showAddVacationBlockPopup ? faPlusCircle : faSave}
          rightButtonState={
            this.state.isValidAddEditVacationBlockPopupForm
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
        />
        <PopupBox
          show={this.state.showDeleteVacationBlockConfirmationPopup}
          handleClose={() => {}}
          size="lg"
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
              .olaConfigs.deleteVacationBlockConfirmationPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                        .topTabs.olaConfigs.deleteVacationBlockConfirmationPopup
                        .systemMessageDescription,

                      <span style={styles.boldText}>
                        {`${this.state.vacationBlockToDelete.date_from}`}
                      </span>,

                      <span style={styles.boldText}>
                        {
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                            .topTabs.olaConfigs.deleteVacationBlockConfirmationPopup.separator
                        }
                      </span>,

                      <span style={styles.boldText}>
                        {`${this.state.vacationBlockToDelete.date_to}`}
                      </span>
                    )}
                  </p>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeDeleteVacationBlockConfirmationPopup}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.handleDeleteVacationBlock}
          rightButtonIcon={faTrashAlt}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testCenterData: state.testCenter.testCenterData,
    completeDatePicked: state.datePicker.completeDatePicked,
    completeDateValidState: state.datePicker.completeDateValidState
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateTestCenterOlaConfigsData,
      getSelectedTestCenterData,
      setTestCenterData,
      resetDatePickedStates,
      createNewTestCenterOlaVacationBlock,
      editTestCenterOlaVacationBlock,
      deleteTestCenterOlaVacationBlock
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(OlaConfigs);
