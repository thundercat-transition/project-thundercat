/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable prefer-destructuring */
/* eslint-disable new-cap */
/* eslint-disable no-param-reassign */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { Row, Col } from "react-bootstrap";
import { bindActionCreators } from "redux";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlusCircle,
  faTimes,
  faSave,
  faPencilAlt,
  faTrashAlt
} from "@fortawesome/free-solid-svg-icons";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import DropdownSelect from "../../commons/DropdownSelect";
import { LANGUAGES, LANGUAGE_IDS, getLanguageData } from "../../../modules/LocalizeRedux";
import {
  addTestCenterOlaTimeSlot,
  deleteTestCenterOlaTimeSlot,
  editTestCenterOlaTimeSlot,
  getSelectedTestCenterData,
  getTestCenterOlaTimeSlotAssessorAvailabilities,
  setTestCenterData
} from "../../../modules/TestCenterRedux";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import StyledTooltip, { EFFECT, TYPE } from "../../authentication/StyledTooltip";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import getDaysOfWeek from "../../../modules/DaysOfWeekRedux";
import {
  populateTimeHourOptions,
  populateTimeMinWith15MinSlotsOptions
} from "../../../helpers/populateCustomDatePickerOptions";
import moment from "moment";
import { convertTimeToUsersTimezone } from "../../../modules/helpers";
import getSwitchTransformScale from "../../../helpers/switchTransformScale";
import Switch from "react-switch";
import { getReasonForTestingPriorities } from "../../../modules/UitRedux";
import { REASON_FOR_TESTING_PRIORITY_CODENAME_CONST } from "../../commons/Constants";

const columnSizes = {
  firstColumnPopup: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumnPopup: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  },
  secondColumnTime: {
    xs: 6,
    sm: 6,
    md: 6,
    lg: 2,
    xl: 2
  }
};

const styles = {
  mainContainer: {
    margin: "24px auto",
    width: "90%"
  },
  tableContainer: {
    margin: "24px 0"
  },
  rowContainer: {
    width: "90%",
    margin: "12px auto"
  },
  parentRowContainerWithError: {
    width: "90%",
    margin: "12px auto 0px auto"
  },
  rowContainerWithError: {
    width: "90%",
    margin: "0px auto"
  },
  indentedCol: {
    paddingLeft: 48
  },
  inputTitles: {
    fontWeight: "bold"
  },
  inputs: {
    width: "20%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4,
    textAlign: "center"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  actionIcon: {
    transform: "scale(1.2)"
  },
  allUnset: {
    all: "unset"
  },
  boldText: {
    fontWeight: "bold"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "6px 0 12px 0"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  prioritizationCalculatedNumber: {
    fontWeight: "bold",
    marginLeft: 12
  }
};

class TimeSlots extends Component {
  state = {
    showAddTimeSlotPopup: false,
    showEditTimeSlotPopup: false,
    showDeleteTimeSlotPopup: false,
    rowsDefinition: {},
    currentlyLoading: false,
    isLoadingDayOfWeekOptions: false,
    dayOfWeekOptions: [],
    dayOfWeekSelectedOption: {},
    hourOptions: populateTimeHourOptions(),
    minuteOptions: populateTimeMinWith15MinSlotsOptions(),
    startTimeHourSelectedOption: "",
    startTimeMinuteSelectedOption: "",
    endTimeHourSelectedOption: "",
    endTimeMinuteSelectedOption: "",
    isValidStartEndTimeCombination: true,
    showOverlappingTimeError: false,
    isLoadingLanguageToAssessOptions: false,
    languageToAssessOptions: [],
    languageToAssessSelectedOption: [],
    assessorAvailability: 0,
    assessorAvailabilityRowsDefinition: {},
    currentlyLoadingAssessorAvailability: false,
    // PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
    // slotsToPrioritizeContent: 0,
    // isValidSlotsToPrioritizeContent: true,
    // prioritizationHighContent: 0,
    // prioritizationMediumContent: 0,
    // prioritizationLowContent: 0,
    // isValidPrioritizationValues: true,
    isLoadingSwitch: false,
    // default values
    switchHeight: 25,
    switchWidth: 50,
    timeSlotToEdit: {},
    timeSlotToDelete: {},
    isValidAddEditTimeSlotPopupForm: false,
    priorityOptions: []
  };

  componentDidMount = () => {
    this.getUpdatedSwitchDimensions();
    // populating test assessors table
    this.populateTimeSlots();
    // populating language certification options
    this.populateLanguageToAssessOptions();
    // populating reason for testing priority options
    this.populateReasonForTestingPrioritiesOptions();
  };

  componentDidUpdate = prevProps => {
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
    // if testCenterData gets updated
    if (prevProps.testCenterData !== this.props.testCenterData) {
      // populating test assessors table
      this.populateTimeSlots();
    }
  };

  // populating reason for testing priorities options
  populateReasonForTestingPrioritiesOptions = () => {
    // getting reason for testing priorities
    this.props.getReasonForTestingPriorities().then(response => {
      // initializing priorityOptions
      const priorityOptions = [];

      // looping in response
      for (let i = 0; i < response.length; i++) {
        // populating priorityOptions
        priorityOptions.push({
          value: response[i].id,
          label: response[i][`reason_for_testing_priority_name_${this.props.currentLanguage}`],
          codename: response[i].codename
        });
      }

      // updating state
      this.setState({ priorityOptions: priorityOptions });
    });
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoadingSwitch: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoadingSwitch: false });
        }
      );
    });
  };

  populateTimeSlots = () => {
    this.setState({ currentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];

      // looping in response given
      for (let i = 0; i < this.props.testCenterData.test_center_ola_time_slots.length; i++) {
        const currentResult = this.props.testCenterData.test_center_ola_time_slots[i];

        // formatting and converting time to user's timezone
        const formattedStartTime = currentResult.start_time.substring(0, 5);
        const formattedEndTime = currentResult.end_time.substring(0, 5);

        const formattedConvertedStartTime = convertTimeToUsersTimezone(formattedStartTime);
        const formattedConvertedEndTime = convertTimeToUsersTimezone(formattedEndTime);

        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult[`day_of_week_text_${this.props.currentLanguage}`],
          column_2: `${formattedConvertedStartTime} - ${formattedConvertedEndTime}`,
          column_3: currentResult[`assessed_language_text_${this.props.currentLanguage}`],
          column_4: currentResult.number_of_selected_assessors,
          // PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
          // column_4:
          //   this.getCalculatedPrioritizationValue(
          //     currentResult.prioritization_high,
          //     currentResult.slots_to_prioritize
          //   ) +
          //   this.getCalculatedPrioritizationValue(
          //     currentResult.prioritization_medium,
          //     currentResult.slots_to_prioritize
          //   ) +
          //   this.getCalculatedPrioritizationValue(
          //     currentResult.prioritization_low,
          //     currentResult.slots_to_prioritize
          //   ),
          column_5: (
            <>
              <StyledTooltip
                id={`test-center-ola-time-slot-edit-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`test-center-ola-time-slot-edit-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faPencilAlt} style={styles.actionIcon} />
                        </>
                      }
                      action={() => {
                        this.openEditTimeSlotPopup(currentResult);
                      }}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.timeSlots.timeSlotsTable.editButtonTooltipAccessibility,
                        currentResult[`day_of_week_text_${this.props.currentLanguage}`],
                        currentResult.start_time.substring(0, 5),
                        currentResult.end_time.substring(0, 5)
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.timeSlots.timeSlotsTable.editButtonTooltip
                      }
                    </p>
                  </div>
                }
              />
              <StyledTooltip
                id={`test-center-ola-time-slot-delete-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`test-center-ola-time-slot-delete-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faTrashAlt} style={styles.actionIcon} />
                        </>
                      }
                      action={() => {
                        this.openDeleteTimeSlotConfirmationPopup(currentResult);
                      }}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.timeSlots.timeSlotsTable.deleteButtonTooltipAccessibility,
                        currentResult[`day_of_week_text_${this.props.currentLanguage}`],
                        currentResult.start_time.substring(0, 5),
                        currentResult.end_time.substring(0, 5)
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>{LOCALIZE.commons.deleteButton}</p>
                  </div>
                }
              />
            </>
          )
        });
      }

      // updating rowsDefinition object with provided data and needed style
      rowsDefinition = {
        column_1_style: COMMON_STYLE.LEFT_TEXT,
        column_2_style: COMMON_STYLE.CENTERED_TEXT,
        column_3_style: COMMON_STYLE.CENTERED_TEXT,
        column_4_style: COMMON_STYLE.CENTERED_TEXT,
        column_5_style: COMMON_STYLE.CENTERED_TEXT,
        data: data
      };

      // saving results in state
      this.setState({
        rowsDefinition: rowsDefinition,
        currentlyLoading: false
      });
    });
  };

  openAddTimeSlotPopup = () => {
    this.setState(
      {
        showAddTimeSlotPopup: true,
        showEditTimeSlotPopup: false
      },
      () => {
        // populating available assessors table
        this.populateAvailableAssessorsTable();
      }
    );
  };

  closeAddEditTimeSlotPopup = () => {
    this.setState({
      showAddTimeSlotPopup: false,
      showEditTimeSlotPopup: false,
      dayOfWeekSelectedOption: {},
      startTimeHourSelectedOption: "",
      startTimeMinuteSelectedOption: "",
      endTimeHourSelectedOption: "",
      endTimeMinuteSelectedOption: "",
      languageToAssessSelectedOption: [],
      assessorAvailability: 0,
      showOverlappingTimeError: false,
      isValidAddEditTimeSlotPopupForm: false,
      timeSlotToEdit: {},
      // PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
      // slotsToPrioritizeContent: 0,
      // isValidSlotsToPrioritizeContent: true,
      // prioritizationHighContent: 0,
      // prioritizationMediumContent: 0,
      // prioritizationLowContent: 0,
      // isValidPrioritizationValues: true,
      assessorAvailabilityRowsDefinition: {}
    });
  };

  openEditTimeSlotPopup = timeSlotData => {
    // formatting and converting time to user's timezone
    const formattedStartTime = timeSlotData.start_time.substring(0, 5);
    const formattedEndTime = timeSlotData.end_time.substring(0, 5);

    const formattedConvertedStartTime = convertTimeToUsersTimezone(formattedStartTime);
    const formattedConvertedEndTime = convertTimeToUsersTimezone(formattedEndTime);

    this.setState(
      {
        showEditTimeSlotPopup: true,
        showAddTimeSlotPopup: false,
        timeSlotToEdit: timeSlotData,
        dayOfWeekSelectedOption: {
          label: timeSlotData[`day_of_week_text_${this.props.currentLanguage}`],
          value: timeSlotData.day_of_week_id
        },
        startTimeHourSelectedOption: {
          label: formattedConvertedStartTime.split(":")[0],
          value: parseInt(formattedConvertedStartTime.split(":")[0])
        },
        startTimeMinuteSelectedOption: {
          label: formattedConvertedStartTime.split(":")[1],
          value: parseInt(formattedConvertedStartTime.split(":")[1])
        },
        endTimeHourSelectedOption: {
          label: formattedConvertedEndTime.split(":")[0],
          value: parseInt(formattedConvertedEndTime.split(":")[0])
        },
        endTimeMinuteSelectedOption: {
          label: formattedConvertedEndTime.split(":")[1],
          value: parseInt(formattedConvertedEndTime.split(":")[1])
        },
        languageToAssessSelectedOption: {
          label: timeSlotData[`assessed_language_text_${this.props.currentLanguage}`],
          value: timeSlotData.assessed_language_id
        },
        assessorAvailability: timeSlotData.availability
        // PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
        // slotsToPrioritizeContent: timeSlotData.slots_to_prioritize,
        // prioritizationHighContent: timeSlotData.prioritization_high,
        // prioritizationMediumContent: timeSlotData.prioritization_medium,
        // prioritizationLowContent: timeSlotData.prioritization_low
      },
      () => {
        // populating available assessors table
        this.populateAvailableAssessorsTable();
      }
    );
  };

  openDeleteTimeSlotConfirmationPopup = timeSlotData => {
    // formatting and converting time to user's timezone
    const formattedStartTime = timeSlotData.start_time.substring(0, 5);
    const formattedEndTime = timeSlotData.end_time.substring(0, 5);

    const formattedConvertedStartTime = convertTimeToUsersTimezone(formattedStartTime);
    const formattedConvertedEndTime = convertTimeToUsersTimezone(formattedEndTime);

    timeSlotData.formattedConvertedStartTime = formattedConvertedStartTime;
    timeSlotData.formattedConvertedEndTime = formattedConvertedEndTime;

    this.setState({
      showDeleteTimeSlotPopup: true,
      timeSlotToDelete: timeSlotData
    });
  };

  closeDeleteTimeSlotConfirmationPopup = () => {
    this.setState({
      showDeleteTimeSlotPopup: false,
      timeSlotToDelete: {}
    });
  };

  handleDeleteTestAssessor = () => {
    // setting body
    const body = {
      id: this.state.timeSlotToDelete.id
    };
    this.props.deleteTestCenterOlaTimeSlot(body).then(response => {
      if (response.ok) {
        // closing popup
        this.closeDeleteTimeSlotConfirmationPopup();
        // getting and setting test center data
        this.props.getSelectedTestCenterData(this.props.testCenterData.id).then(response => {
          this.props.setTestCenterData(response);
        });
        // should never happen
      } else {
        throw new Error("An error occurred during the delete test center OLA time slot process");
      }
    });
  };

  changeAvailableAssessorSwitchState = (event, testCenterOlaTestAssessorId) => {
    // getting assessorAvailabilityRowsDefinition
    const { assessorAvailabilityRowsDefinition } = this.state;

    const indexOfRespectiveAssessor = assessorAvailabilityRowsDefinition.data.findIndex(
      obj => obj.test_center_ola_test_assessor_id === testCenterOlaTestAssessorId
    );

    // updating available state of respective row
    assessorAvailabilityRowsDefinition.data[indexOfRespectiveAssessor].available = event;

    // updating state
    this.setState(
      { assessorAvailabilityRowsDefinition: assessorAvailabilityRowsDefinition },
      () => {
        this.reRenderAvailableAssessorsTable();
      }
    );
  };

  populateDayOfWeekOptions = () => {
    this.setState({ isLoadingDayOfWeekOptions: true }, () => {
      // initializing dayOfWeekOptions
      const dayOfWeekOptions = [];
      // getting all scorer users
      this.props.getDaysOfWeek().then(response => {
        // getting language ID from current selected interface language
        let currentLanguageId = LANGUAGE_IDS.english;
        if (this.props.currentLanguage === LANGUAGES.french) {
          currentLanguageId = LANGUAGE_IDS.french;
        }
        // filtering data to get the text in the right language
        const filteredResponse = response.filter(obj => obj.language_id === currentLanguageId);
        // looping in filteredResponse
        for (let i = 0; i < filteredResponse.length; i++) {
          // populating dayOfWeekOptions
          dayOfWeekOptions.push({
            label: filteredResponse[i].text,
            value: filteredResponse[i].day_of_week_id
          });
        }
      });
      this.setState({
        dayOfWeekOptions: dayOfWeekOptions,
        isLoadingDayOfWeekOptions: false
      });
    });
  };

  populateAvailableAssessorsTable = () => {
    // initializing needed variables
    let rowsDefinition = {};
    const data = [];
    // only getting assessor availabilities if the language to assess is defined
    if (typeof this.state.languageToAssessSelectedOption.value !== "undefined") {
      this.setState({ currentlyLoadingAssessorAvailability: true }, () => {
        // getting time slot assessor availabilities
        this.props
          .getTestCenterOlaTimeSlotAssessorAvailabilities(
            this.props.testCenterData.id,
            this.state.languageToAssessSelectedOption.value,
            // timeSlotToEdit is defined ==> provided the time slot ID, else null
            Object.keys(this.state.timeSlotToEdit).length > 0 ? this.state.timeSlotToEdit.id : null
          )
          .then(response => {
            // looping in test_center_ola_test_assessors
            for (let i = 0; i < response.length; i++) {
              // pushing needed data in data array (needed for GenericTable component)
              data.push({
                column_1: `${response[i].user_first_name} ${response[i].user_last_name}`,
                column_2: !this.state.isLoadingSwitch && (
                  <Switch
                    onChange={e =>
                      this.changeAvailableAssessorSwitchState(
                        e,
                        response[i].test_center_ola_test_assessor_id
                      )
                    }
                    checked={response[i].available}
                    aria-labelledby={`add-edit-time-slot-popup-assessors-availabilities-column-1-row-${i}-label`}
                    height={this.state.switchHeight}
                    width={this.state.switchWidth}
                  />
                ),
                test_center_ola_test_assessor_id: response[i].test_center_ola_test_assessor_id,
                available: response[i].available
              });
            }

            // updating rowsDefinition object with provided data and needed style
            rowsDefinition = {
              column_1_style: COMMON_STYLE.LEFT_TEXT,
              column_2_style: COMMON_STYLE.RIGHT_TEXT,
              data: data
            };

            // updating state
            this.setState(
              {
                assessorAvailabilityRowsDefinition: rowsDefinition,
                currentlyLoadingAssessorAvailability: false
              },
              () => {
                // validating form
                this.validateForm();
              }
            );
          });
      });
    }
  };

  reRenderAvailableAssessorsTable = () => {
    // initializing needed variables
    let rowsDefinition = this.state.assessorAvailabilityRowsDefinition;
    const data = [];
    // looping in test_center_ola_test_assessors
    for (let i = 0; i < this.state.assessorAvailabilityRowsDefinition.data.length; i++) {
      // pushing needed data in data array (needed for GenericTable component)
      data.push({
        column_1: this.state.assessorAvailabilityRowsDefinition.data[i].column_1,
        column_2: !this.state.isLoadingSwitch && (
          <Switch
            onChange={e =>
              this.changeAvailableAssessorSwitchState(
                e,
                this.state.assessorAvailabilityRowsDefinition.data[i]
                  .test_center_ola_test_assessor_id
              )
            }
            checked={this.state.assessorAvailabilityRowsDefinition.data[i].available}
            aria-labelledby={`add-edit-time-slot-popup-assessors-availabilities-column-1-row-${i}-label`}
            height={this.state.switchHeight}
            width={this.state.switchWidth}
          />
        ),
        test_center_ola_test_assessor_id:
          this.state.assessorAvailabilityRowsDefinition.data[i].test_center_ola_test_assessor_id,
        available: this.state.assessorAvailabilityRowsDefinition.data[i].available
      });
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.RIGHT_TEXT,
      data: data
    };

    // updating state
    this.setState(
      {
        assessorAvailabilityRowsDefinition: rowsDefinition
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  // PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
  // formatTimeSlotPrioritization = () => {
  //   // initializing final_array
  //   const finalArray = [];

  //   // populating "high"
  //   finalArray.push({
  //     codename: REASON_FOR_TESTING_PRIORITY_CODENAME_CONST.HIGH,
  //     value: this.state.prioritizationHighContent
  //   });
  //   // populating "medium"
  //   finalArray.push({
  //     codename: REASON_FOR_TESTING_PRIORITY_CODENAME_CONST.MEDIUM,
  //     value: this.state.prioritizationMediumContent
  //   });
  //   // populating "low"
  //   finalArray.push({
  //     codename: REASON_FOR_TESTING_PRIORITY_CODENAME_CONST.LOW,
  //     value: this.state.prioritizationLowContent
  //   });

  //   return finalArray;
  // };

  formatAssessorAvailabilitiesArray = assessorAvailabilities => {
    // initializing finalArray
    const finalArray = [];
    // looping in assessorAvailabilities
    for (let i = 0; i < assessorAvailabilities.length; i++) {
      // populating finalArray (only populating user if available is set to true)
      if (assessorAvailabilities[i].available) {
        finalArray.push({
          test_center_ola_test_assessor_id:
            assessorAvailabilities[i].test_center_ola_test_assessor_id,
          available: assessorAvailabilities[i].available
        });
      }
    }
    // returning finalArray
    return finalArray;
  };

  onAddEditPopupOpen = () => {
    this.populateDayOfWeekOptions();
  };

  populateLanguageToAssessOptions = () => {
    this.setState({ isLoadingLanguageToAssessOptions: true }, () => {
      // initializing dayOfWeekOptions
      const languageToAssessOptions = [];
      // getting all scorer users
      this.props.getLanguageData().then(response => {
        for (let i = 0; i < response.length; i++) {
          languageToAssessOptions.push({
            label: response[i].language_text[this.props.currentLanguage][0].text,
            value: response[i].language_id
          });
        }
      });
      this.setState({
        languageToAssessOptions: languageToAssessOptions,
        isLoadingLanguageToAssessOptions: false
      });
    });
  };

  getSelectedDayOfWeekOption = selectedOption => {
    this.setState(
      { dayOfWeekSelectedOption: selectedOption, showOverlappingTimeError: false },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  getSelectedStartTimeHour = selectedOption => {
    this.setState(
      {
        startTimeHourSelectedOption: selectedOption,
        showOverlappingTimeError: false
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  getSelectedStartTimeMinute = selectedOption => {
    this.setState(
      {
        startTimeMinuteSelectedOption: selectedOption,
        showOverlappingTimeError: false
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  getSelectedEndTimeHour = selectedOption => {
    this.setState(
      {
        endTimeHourSelectedOption: selectedOption,
        showOverlappingTimeError: false
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  getSelectedEndTimeMinute = selectedOption => {
    this.setState(
      {
        endTimeMinuteSelectedOption: selectedOption,
        showOverlappingTimeError: false
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  getSelectedLanguageToAssessOption = selectedOption => {
    this.setState(
      { languageToAssessSelectedOption: selectedOption, showOverlappingTimeError: false },
      () => {
        // populating available assessors table
        this.populateAvailableAssessorsTable();
      }
    );
  };

  // PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
  // getSlotsToPrioritizeContent = event => {
  //   const slotsToPrioritizeContent = event.target.value;
  //   // allow only numeric values (0 to 3 chars)
  //   const regexExpression = /^([0-9]{0,3})$/;
  //   if (event.target.value === "" || regexExpression.test(event.target.value)) {
  //     this.setState(
  //       {
  //         slotsToPrioritizeContent:
  //           event.target.value !== "" ? parseInt(slotsToPrioritizeContent) : 0
  //       },
  //       () => {
  //         // validating form
  //         this.validateForm();
  //       }
  //     );
  //   }
  // };

  // getCalculatedSlotsToPriotitizeBuffer = () => {
  //   // initializing buffer
  //   let buffer = null;

  //   // initializing needed variables
  //   let totalNumberOfAvailableAssessors = 0;

  //   // if data attribute of assessorAvailabilityRowsDefinition state is defined
  //   if (typeof this.state.assessorAvailabilityRowsDefinition.data !== "undefined") {
  //     totalNumberOfAvailableAssessors = this.state.assessorAvailabilityRowsDefinition.data.filter(
  //       obj => obj.available === true
  //     ).length;
  //   }

  //   // making sure that the slots to prioritize content and all related prioritization values are valid + making sure that the total number of assessors is greater than 0 before proceeding to any calculations
  //   if (
  //     this.state.isValidSlotsToPrioritizeContent &&
  //     this.state.isValidPrioritizationValues &&
  //     totalNumberOfAvailableAssessors > 0
  //   ) {
  //     // calculating the total prioritization value of the high, medium and low  calculated slots
  //     const calculatedTotalPriotizationValue =
  //       this.getCalculatedPrioritizationValue(this.state.prioritizationHighContent) +
  //       this.getCalculatedPrioritizationValue(this.state.prioritizationMediumContent) +
  //       this.getCalculatedPrioritizationValue(this.state.prioritizationLowContent);

  //     // calculating buffer based on calculated total prioritization value and number of available assessors
  //     buffer = parseInt(
  //       100 - (calculatedTotalPriotizationValue / totalNumberOfAvailableAssessors) * 100
  //     );
  //   }

  //   // returning buffer value
  //   return buffer;
  // };

  // getCalculatedPrioritizationValue = (
  //   prioritizationValue,
  //   providedSlotsToPrioritizeContent = null
  // ) => {
  //   // initializing slotsToPrioritizeContent
  //   let slotsToPrioritizeContent = this.state.slotsToPrioritizeContent;
  //   // if provided slots to prioritize
  //   if (providedSlotsToPrioritizeContent !== null) {
  //     slotsToPrioritizeContent = providedSlotsToPrioritizeContent;
  //   }
  //   // calculating the prioritization value based on the slots to prioritize value and the provided prioritization value (in percentage) value
  //   const calculatedPrioritizationValue = Math.floor(
  //     (prioritizationValue / 100) * slotsToPrioritizeContent
  //   );

  //   return calculatedPrioritizationValue;
  // };

  // getPrioritizationHighContent = event => {
  //   const prioritizationHighContent = event.target.value;
  //   // only allow percentage number (0-100)
  //   const regex = /^((100)|(\d{1,2}(\.\d{1,8})?))$/;
  //   if (event.target.value === "" || regex.test(event.target.value)) {
  //     this.setState(
  //       {
  //         prioritizationHighContent:
  //           event.target.value !== "" ? parseInt(prioritizationHighContent) : 0
  //       },
  //       () => {
  //         // validating form
  //         this.validateForm();
  //       }
  //     );
  //   }
  // };

  // getPrioritizationMediumContent = event => {
  //   const prioritizationMediumContent = event.target.value;
  //   // only allow percentage number (0-100)
  //   const regex = /^((100)|(\d{1,2}(\.\d{1,8})?))$/;
  //   if (event.target.value === "" || regex.test(event.target.value)) {
  //     this.setState(
  //       {
  //         prioritizationMediumContent:
  //           event.target.value !== "" ? parseInt(prioritizationMediumContent) : 0
  //       },
  //       () => {
  //         // validating form
  //         this.validateForm();
  //       }
  //     );
  //   }
  // };

  // getPrioritizationLowContent = event => {
  //   const prioritizationLowContent = event.target.value;
  //   // only allow percentage number (0-100)
  //   const regex = /^((100)|(\d{1,2}(\.\d{1,8})?))$/;
  //   if (event.target.value === "" || regex.test(event.target.value)) {
  //     this.setState(
  //       {
  //         prioritizationLowContent:
  //           event.target.value !== "" ? parseInt(prioritizationLowContent) : 0
  //       },
  //       () => {
  //         // validating form
  //         this.validateForm();
  //       }
  //     );
  //   }
  // };

  validateForm = () => {
    // initializing isValidAddEditTimeSlotPopupForm
    let isValidAddEditTimeSlotPopupForm = false;

    // getting/setting valid states
    const isValidDayOfWeek = Object.keys(this.state.dayOfWeekSelectedOption).length > 0;
    const isValidStartTime =
      Object.keys(this.state.startTimeHourSelectedOption).length > 0 &&
      Object.keys(this.state.startTimeMinuteSelectedOption).length > 0;
    const isValidEndTime =
      Object.keys(this.state.endTimeHourSelectedOption).length > 0 &&
      Object.keys(this.state.endTimeMinuteSelectedOption).length > 0;
    let isValidStartEndTimeCombination = true;
    // start time and end time are provided
    if (isValidStartTime && isValidEndTime) {
      const formattedStartTime = moment(
        `${this.state.startTimeHourSelectedOption.label}:${this.state.startTimeMinuteSelectedOption.label}`,
        "HH:mm"
      );
      const formattedEndTime = moment(
        `${this.state.endTimeHourSelectedOption.label}:${this.state.endTimeMinuteSelectedOption.label}`,
        "HH:mm"
      );
      // validating times (making sure that the start date is before the end date)
      isValidStartEndTimeCombination = formattedStartTime.isBefore(formattedEndTime);
    }
    const isValidLanguageToAssess =
      Object.keys(this.state.languageToAssessSelectedOption).length > 0;

    // PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
    // // slotsToPrioritizeContent <= number of available assessors
    // let isValidSlotsToPrioritizeContent = true;
    // if (typeof this.state.assessorAvailabilityRowsDefinition.data !== "undefined") {
    //   isValidSlotsToPrioritizeContent =
    //     this.state.slotsToPrioritizeContent <=
    //     this.state.assessorAvailabilityRowsDefinition.data.filter(obj => obj.available === true)
    //       .length;
    // }

    // // prioritization values must be less or equal than 100 (%)
    // const isValidPrioritizationValues =
    //   this.state.prioritizationHighContent +
    //     this.state.prioritizationMediumContent +
    //     this.state.prioritizationLowContent <=
    //   100;

    // all mandatory fields have been provided
    if (
      isValidDayOfWeek &&
      isValidStartTime &&
      isValidEndTime &&
      isValidStartEndTimeCombination &&
      isValidLanguageToAssess
      // PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
      // isValidSlotsToPrioritizeContent &&
      // isValidPrioritizationValues
    ) {
      isValidAddEditTimeSlotPopupForm = true;
    }

    this.setState({
      isValidAddEditTimeSlotPopupForm: isValidAddEditTimeSlotPopupForm,
      isValidStartEndTimeCombination: isValidStartEndTimeCombination
      // PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
      // isValidSlotsToPrioritizeContent: isValidSlotsToPrioritizeContent,
      // isValidPrioritizationValues: isValidPrioritizationValues
    });
  };

  handleAddTimeSlot = () => {
    // setting time variables
    const utcStartTime = new moment(
      `${this.state.startTimeHourSelectedOption.value}:${this.state.startTimeMinuteSelectedOption.value}`,
      "HH:mm"
    ).utc();
    const utcEndTime = new moment(
      `${this.state.endTimeHourSelectedOption.value}:${this.state.endTimeMinuteSelectedOption.value}`,
      "HH:mm"
    ).utc();
    // PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
    // formatting time slot prioritization
    // const formattedTimeSlotPrioritization = this.formatTimeSlotPrioritization();
    // formatting assessor availabilities
    const formattedAssessorAvailabilities = this.formatAssessorAvailabilitiesArray(
      this.state.assessorAvailabilityRowsDefinition.data
    );
    // setting body
    const body = {
      test_center_id: this.props.testCenterData.id,
      day_of_week_id: this.state.dayOfWeekSelectedOption.value,
      start_time: utcStartTime,
      end_time: utcEndTime,
      assessed_language_id: this.state.languageToAssessSelectedOption.value,
      availability: this.state.assessorAvailability,
      // PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
      // slots_to_prioritize: this.state.slotsToPrioritizeContent,
      // prioritization: formattedTimeSlotPrioritization,
      assessor_availabilities: formattedAssessorAvailabilities
    };
    this.props.addTestCenterOlaTimeSlot(body).then(response => {
      // overlapping time found
      if (response.status === 409) {
        this.setState({ showOverlappingTimeError: true }, () => {
          // focusing on error
          if (
            document.getElementById("ola-time-slots-add-time-slot-popup-overlapping-time-error")
          ) {
            document
              .getElementById("ola-time-slots-add-time-slot-popup-overlapping-time-error")
              .focus();
          }
        });
      } else if (response.ok) {
        // triggering table loading
        this.setState({ currentlyLoading: true }, () => {
          // closing popup
          this.closeAddEditTimeSlotPopup();
          // getting and setting test center data
          this.props.getSelectedTestCenterData(this.props.testCenterData.id).then(response => {
            this.props.setTestCenterData(response);
          });
        });
        // should never happen
      } else {
        throw new Error("An error occurred during the add test center OLA time slot process");
      }
    });
  };

  handleEditTimeSlot = () => {
    // setting time variables
    const utcStartTime = new moment(
      `${this.state.startTimeHourSelectedOption.value}:${this.state.startTimeMinuteSelectedOption.value}`,
      "HH:mm"
    ).utc();
    const utcEndTime = new moment(
      `${this.state.endTimeHourSelectedOption.value}:${this.state.endTimeMinuteSelectedOption.value}`,
      "HH:mm"
    ).utc();
    // PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
    // formatting time slot prioritization
    // const formattedTimeSlotPrioritization = this.formatTimeSlotPrioritization();
    // formatting assessor availabilities
    const formattedAssessorAvailabilities = this.formatAssessorAvailabilitiesArray(
      this.state.assessorAvailabilityRowsDefinition.data
    );
    // setting body
    const body = {
      id: this.state.timeSlotToEdit.id,
      test_center_id: this.props.testCenterData.id,
      day_of_week_id: this.state.dayOfWeekSelectedOption.value,
      start_time: utcStartTime,
      end_time: utcEndTime,
      assessed_language_id: this.state.languageToAssessSelectedOption.value,
      availability: this.state.assessorAvailability,
      // PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
      // slots_to_prioritize: this.state.slotsToPrioritizeContent,
      // prioritization: formattedTimeSlotPrioritization,
      assessor_availabilities: formattedAssessorAvailabilities
    };
    this.props.editTestCenterOlaTimeSlot(body).then(response => {
      // overlapping time found
      if (response.status === 409) {
        this.setState({ showOverlappingTimeError: true }, () => {
          // focusing on error
          if (
            document.getElementById("ola-time-slots-add-time-slot-popup-overlapping-time-error")
          ) {
            document
              .getElementById("ola-time-slots-add-time-slot-popup-overlapping-time-error")
              .focus();
          }
        });
      } else if (response.ok) {
        // triggering table loading
        this.setState({ currentlyLoading: true }, () => {
          // closing popup
          this.closeAddEditTimeSlotPopup();
          // getting and setting test center data
          this.props.getSelectedTestCenterData(this.props.testCenterData.id).then(response => {
            this.props.setTestCenterData(response);
          });
        });
        // should never happen
      } else {
        throw new Error(
          "An error occurred during the update test center OLA test assessor data process"
        );
      }
    });
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs.timeSlots
            .timeSlotsTable.dayOfWeek,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs.timeSlots
            .timeSlotsTable.timeOfDay,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs.timeSlots
            .timeSlotsTable.assessedLanguage,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs.timeSlots
            .timeSlotsTable.nbrOfSelectedAssessors,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs.timeSlots
            .timeSlotsTable.actions,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    const assessorAvailabilityColumnsDefinition = [
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs.timeSlots
            .addEditTimeSlotPopup.assessorAvailabilityTable.column1,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs.timeSlots
            .addEditTimeSlotPopup.assessorAvailabilityTable.column2,
        style: { ...COMMON_STYLE.CENTERED_TEXT, ...{ textAlign: "right" } }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <div>
          <CustomButton
            label={
              <>
                <FontAwesomeIcon icon={faPlusCircle} />
                <span
                  id="ola-time-slots-add-time-slot"
                  style={styles.buttonLabel}
                  className="notranslate"
                >
                  {
                    LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                      .timeSlots.addTimeSlotButton
                  }
                </span>
              </>
            }
            action={this.openAddTimeSlotPopup}
            buttonTheme={THEME.PRIMARY}
          />
        </div>
        <div style={styles.tableContainer}>
          <GenericTable
            classnamePrefix="ola-configs-time-slots"
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={
              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                .timeSlots.timeSlotsTable.noData
            }
            currentlyLoading={this.state.currentlyLoading}
          />
        </div>
        <PopupBox
          show={this.state.showAddTimeSlotPopup || this.state.showEditTimeSlotPopup}
          handleClose={() => {}}
          onPopupOpen={this.onAddEditPopupOpen}
          title={
            this.state.showAddTimeSlotPopup
              ? LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                  .timeSlots.addEditTimeSlotPopup.addTitle
              : LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                  .timeSlots.addEditTimeSlotPopup.editTitle
          }
          description={
            <div>
              <p>
                {this.state.showAddTimeSlotPopup
                  ? LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                      .timeSlots.addEditTimeSlotPopup.addDescription
                  : LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                      .timeSlots.addEditTimeSlotPopup.editDescription}
              </p>
              <div>
                <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumnPopup.xl}
                    lg={columnSizes.firstColumnPopup.lg}
                    md={columnSizes.firstColumnPopup.md}
                    sm={columnSizes.firstColumnPopup.sm}
                    xs={columnSizes.firstColumnPopup.xs}
                  >
                    <label
                      id="ola-time-slots-add-time-slot-popup-day-of-week-label"
                      style={styles.inputTitles}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.timeSlots.addEditTimeSlotPopup.dayOfWeekLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnPopup.xl}
                    lg={columnSizes.secondColumnPopup.lg}
                    md={columnSizes.secondColumnPopup.md}
                    sm={columnSizes.secondColumnPopup.sm}
                    xs={columnSizes.secondColumnPopup.xs}
                  >
                    <DropdownSelect
                      idPrefix="ola-time-slots-add-time-slot-popup-day-of-week"
                      ariaLabelledBy="ola-time-slots-add-time-slot-popup-day-of-week-label"
                      ariaRequired={true}
                      options={this.state.dayOfWeekOptions}
                      onChange={this.getSelectedDayOfWeekOption}
                      defaultValue={this.state.dayOfWeekSelectedOption}
                      hasPlaceholder={true}
                      orderByLabel={false}
                      orderByValues={true}
                      isDisabled={this.state.showEditTimeSlotPopup}
                    />
                  </Col>
                </Row>
                <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumnPopup.xl}
                    lg={columnSizes.firstColumnPopup.lg}
                    md={columnSizes.firstColumnPopup.md}
                    sm={columnSizes.firstColumnPopup.sm}
                    xs={columnSizes.firstColumnPopup.xs}
                  >
                    <>
                      <label
                        id="ola-time-slots-add-time-slot-popup-start-time-label"
                        style={styles.inputTitles}
                      >
                        {
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                            .topTabs.timeSlots.addEditTimeSlotPopup.startTimeLabel
                        }
                      </label>
                      <label
                        style={styles.hiddenText}
                        id="ola-time-slots-add-time-slot-popup-start-time-hour-hidden-label"
                      >
                        {LOCALIZE.commons.hours}
                      </label>
                      <label
                        style={styles.hiddenText}
                        id="ola-time-slots-add-time-slot-popup-start-time-minute-hidden-label"
                      >
                        {LOCALIZE.commons.minutes}
                      </label>
                    </>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnTime.xl}
                    lg={columnSizes.secondColumnTime.lg}
                    md={columnSizes.secondColumnTime.md}
                    sm={columnSizes.secondColumnTime.sm}
                    xs={columnSizes.secondColumnTime.xs}
                  >
                    <DropdownSelect
                      idPrefix="ola-time-slots-add-time-slot-popup-start-time-hour"
                      ariaLabelledBy="ola-time-slots-add-time-slot-popup-start-time-label ola-time-slots-add-time-slot-popup-start-time-hour-hidden-label ola-time-slots-add-time-slot-popup-start-end-time-combination-error ola-time-slots-add-time-slot-popup-overlapping-time-error"
                      ariaRequired={true}
                      isValid={
                        this.state.isValidStartEndTimeCombination &&
                        !this.state.showOverlappingTimeError
                      }
                      hasPlaceholder={true}
                      customPlaceholderWording={LOCALIZE.datePicker.hourField}
                      options={this.state.hourOptions}
                      onChange={this.getSelectedStartTimeHour}
                      defaultValue={this.state.startTimeHourSelectedOption}
                      isDisabled={this.state.showEditTimeSlotPopup}
                    />
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnTime.xl}
                    lg={columnSizes.secondColumnTime.lg}
                    md={columnSizes.secondColumnTime.md}
                    sm={columnSizes.secondColumnTime.sm}
                    xs={columnSizes.secondColumnTime.xs}
                  >
                    <DropdownSelect
                      idPrefix="ola-time-slots-add-time-slot-popup-start-time-minute"
                      ariaLabelledBy="ola-time-slots-add-time-slot-popup-start-time-label ola-time-slots-add-time-slot-popup-start-time-minute-hidden-label ola-time-slots-add-time-slot-popup-start-end-time-combination-error ola-time-slots-add-time-slot-popup-overlapping-time-error"
                      ariaRequired={true}
                      isValid={
                        this.state.isValidStartEndTimeCombination &&
                        !this.state.showOverlappingTimeError
                      }
                      hasPlaceholder={true}
                      customPlaceholderWording={LOCALIZE.datePicker.minuteField}
                      options={this.state.minuteOptions}
                      onChange={this.getSelectedStartTimeMinute}
                      defaultValue={this.state.startTimeMinuteSelectedOption}
                      isDisabled={this.state.showEditTimeSlotPopup}
                    />
                  </Col>
                </Row>
                <Row
                  role="presentation"
                  className="align-items-center"
                  style={
                    !this.state.isValidStartEndTimeCombination ||
                    (this.state.isValidStartEndTimeCombination &&
                      this.state.showOverlappingTimeError)
                      ? styles.parentRowContainerWithError
                      : styles.rowContainer
                  }
                >
                  <Col
                    xl={columnSizes.firstColumnPopup.xl}
                    lg={columnSizes.firstColumnPopup.lg}
                    md={columnSizes.firstColumnPopup.md}
                    sm={columnSizes.firstColumnPopup.sm}
                    xs={columnSizes.firstColumnPopup.xs}
                  >
                    <>
                      <label
                        id="ola-time-slots-add-time-slot-popup-end-time-label"
                        style={styles.inputTitles}
                      >
                        {
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                            .topTabs.timeSlots.addEditTimeSlotPopup.endTimeLabel
                        }
                      </label>
                      <label
                        style={styles.hiddenText}
                        id="ola-time-slots-add-time-slot-popup-end-time-hour-hidden-label"
                      >
                        {LOCALIZE.commons.hours}
                      </label>
                      <label
                        style={styles.hiddenText}
                        id="ola-time-slots-add-time-slot-popup-end-time-minute-hidden-label"
                      >
                        {LOCALIZE.commons.minutes}
                      </label>
                    </>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnTime.xl}
                    lg={columnSizes.secondColumnTime.lg}
                    md={columnSizes.secondColumnTime.md}
                    sm={columnSizes.secondColumnTime.sm}
                    xs={columnSizes.secondColumnTime.xs}
                  >
                    <DropdownSelect
                      idPrefix="ola-time-slots-add-time-slot-popup-end-time-hour"
                      ariaLabelledBy="ola-time-slots-add-time-slot-popup-end-time-label ola-time-slots-add-time-slot-popup-end-time-hour-hidden-label ola-time-slots-add-time-slot-popup-start-end-time-combination-error ola-time-slots-add-time-slot-popup-overlapping-time-error"
                      ariaRequired={true}
                      isValid={
                        this.state.isValidStartEndTimeCombination &&
                        !this.state.showOverlappingTimeError
                      }
                      hasPlaceholder={true}
                      customPlaceholderWording={LOCALIZE.datePicker.hourField}
                      options={this.state.hourOptions}
                      onChange={this.getSelectedEndTimeHour}
                      defaultValue={this.state.endTimeHourSelectedOption}
                      isDisabled={this.state.showEditTimeSlotPopup}
                    />
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnTime.xl}
                    lg={columnSizes.secondColumnTime.lg}
                    md={columnSizes.secondColumnTime.md}
                    sm={columnSizes.secondColumnTime.sm}
                    xs={columnSizes.secondColumnTime.xs}
                  >
                    <DropdownSelect
                      idPrefix="ola-time-slots-add-time-slot-popup-end-time-minute"
                      ariaLabelledBy="ola-time-slots-add-time-slot-popup-end-time-label ola-time-slots-add-time-slot-popup-end-time-minute-hidden-label ola-time-slots-add-time-slot-popup-start-end-time-combination-error ola-time-slots-add-time-slot-popup-overlapping-time-error"
                      ariaRequired={true}
                      isValid={
                        this.state.isValidStartEndTimeCombination &&
                        !this.state.showOverlappingTimeError
                      }
                      hasPlaceholder={true}
                      customPlaceholderWording={LOCALIZE.datePicker.minuteField}
                      options={this.state.minuteOptions}
                      onChange={this.getSelectedEndTimeMinute}
                      defaultValue={this.state.endTimeMinuteSelectedOption}
                      isDisabled={this.state.showEditTimeSlotPopup}
                    />
                  </Col>
                </Row>
                {!this.state.isValidStartEndTimeCombination && (
                  <Row
                    role="presentation"
                    className="align-items-center justify-content-end"
                    style={styles.rowContainerWithError}
                  >
                    <Col
                      xl={columnSizes.secondColumnPopup.xl}
                      lg={columnSizes.secondColumnPopup.lg}
                      md={columnSizes.secondColumnPopup.md}
                      sm={columnSizes.secondColumnPopup.sm}
                      xs={columnSizes.secondColumnPopup.xs}
                    >
                      <label
                        id="ola-time-slots-add-time-slot-popup-start-end-time-combination-error"
                        style={styles.errorMessage}
                        tabIndex={0}
                      >
                        {
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                            .topTabs.timeSlots.addEditTimeSlotPopup.startEndTimeCombinationError
                        }
                      </label>
                    </Col>
                  </Row>
                )}
                {this.state.isValidStartEndTimeCombination &&
                  this.state.showOverlappingTimeError && (
                    <Row
                      role="presentation"
                      className="align-items-center justify-content-end"
                      style={styles.rowContainerWithError}
                    >
                      <Col
                        xl={columnSizes.secondColumnPopup.xl}
                        lg={columnSizes.secondColumnPopup.lg}
                        md={columnSizes.secondColumnPopup.md}
                        sm={columnSizes.secondColumnPopup.sm}
                        xs={columnSizes.secondColumnPopup.xs}
                      >
                        <label
                          id="ola-time-slots-add-time-slot-popup-overlapping-time-error"
                          style={styles.errorMessage}
                          tabIndex={0}
                        >
                          {
                            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                              .topTabs.timeSlots.addEditTimeSlotPopup.overlappingTimeError
                          }
                        </label>
                      </Col>
                    </Row>
                  )}
                <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumnPopup.xl}
                    lg={columnSizes.firstColumnPopup.lg}
                    md={columnSizes.firstColumnPopup.md}
                    sm={columnSizes.firstColumnPopup.sm}
                    xs={columnSizes.firstColumnPopup.xs}
                  >
                    <label
                      id="ola-time-slots-add-time-slot-popup-language-to-assess-label"
                      style={styles.inputTitles}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.timeSlots.addEditTimeSlotPopup.languageToAssessLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnPopup.xl}
                    lg={columnSizes.secondColumnPopup.lg}
                    md={columnSizes.secondColumnPopup.md}
                    sm={columnSizes.secondColumnPopup.sm}
                    xs={columnSizes.secondColumnPopup.xs}
                  >
                    <DropdownSelect
                      idPrefix="ola-time-slots-add-time-slot-popup-language-to-assess"
                      ariaLabelledBy="ola-time-slots-add-time-slot-popup-language-to-assess-label"
                      ariaRequired={true}
                      options={this.state.languageToAssessOptions}
                      onChange={this.getSelectedLanguageToAssessOption}
                      defaultValue={this.state.languageToAssessSelectedOption}
                      hasPlaceholder={true}
                      isDisabled={this.state.showEditTimeSlotPopup}
                    />
                  </Col>
                </Row>
                {/* PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL) */}
                {/* <Row
                  role="presentation"
                  className="align-items-center"
                  style={
                    !this.state.isValidSlotsToPrioritizeContent
                      ? styles.parentRowContainerWithError
                      : styles.rowContainer
                  }
                >
                  <Col
                    xl={columnSizes.firstColumnPopup.xl}
                    lg={columnSizes.firstColumnPopup.lg}
                    md={columnSizes.firstColumnPopup.md}
                    sm={columnSizes.firstColumnPopup.sm}
                    xs={columnSizes.firstColumnPopup.xs}
                  >
                    <label
                      htmlFor="ola-time-slots-add-time-slot-popup-slots-to-prioritize-input"
                      style={styles.inputTitles}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.timeSlots.addEditTimeSlotPopup.slotsToPrioritizeLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnPopup.xl}
                    lg={columnSizes.secondColumnPopup.lg}
                    md={columnSizes.secondColumnPopup.md}
                    sm={columnSizes.secondColumnPopup.sm}
                    xs={columnSizes.secondColumnPopup.xs}
                  >
                    <div>
                      <input
                        id="ola-time-slots-add-time-slot-popup-slots-to-prioritize-input"
                        className={
                          this.state.isValidSlotsToPrioritizeContent
                            ? "valid-field"
                            : "invalid-field"
                        }
                        type="text"
                        value={this.state.slotsToPrioritizeContent}
                        onChange={this.getSlotsToPrioritizeContent}
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                      />
                      {this.getCalculatedSlotsToPriotitizeBuffer() !== null && (
                        <span style={styles.prioritizationCalculatedNumber}>
                          {`${LOCALIZE.formatString(
                            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                              .topTabs.timeSlots.addEditTimeSlotPopup.slotsToPrioritizeValueLabel,
                            this.getCalculatedSlotsToPriotitizeBuffer()
                          )}`}
                        </span>
                      )}
                    </div>
                  </Col>
                </Row>
                {!this.state.isValidSlotsToPrioritizeContent && (
                  <Row
                    role="presentation"
                    className="align-items-center"
                    style={styles.rowContainerWithError}
                  >
                    <Col
                      xl={columnSizes.firstColumnPopup.xl}
                      lg={columnSizes.firstColumnPopup.lg}
                      md={columnSizes.firstColumnPopup.md}
                      sm={columnSizes.firstColumnPopup.sm}
                      xs={columnSizes.firstColumnPopup.xs}
                    ></Col>
                    <Col
                      xl={columnSizes.secondColumnPopup.xl}
                      lg={columnSizes.secondColumnPopup.lg}
                      md={columnSizes.secondColumnPopup.md}
                      sm={columnSizes.secondColumnPopup.sm}
                      xs={columnSizes.secondColumnPopup.xs}
                    >
                      <label
                        htmlFor="ola-time-slots-add-time-slot-popup-slots-to-prioritize-input"
                        style={styles.errorMessage}
                      >
                        {
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                            .topTabs.timeSlots.addEditTimeSlotPopup.slotsToPrioritizeError
                        }
                      </label>
                    </Col>
                  </Row>
                )}
                <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                  <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                    <label
                      id="ola-time-slots-add-time-slot-popup-prioritization-label"
                      style={styles.inputTitles}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.timeSlots.addEditTimeSlotPopup.prioritizationLabel
                      }
                    </label>
                  </Col>
                </Row>
                <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumnPopup.xl}
                    lg={columnSizes.firstColumnPopup.lg}
                    md={columnSizes.firstColumnPopup.md}
                    sm={columnSizes.firstColumnPopup.sm}
                    xs={columnSizes.firstColumnPopup.xs}
                    style={styles.indentedCol}
                  >
                    <label
                      id="ola-time-slots-add-time-slot-popup-prioritization-high-label"
                      htmlFor="ola-time-slots-add-time-slot-popup-prioritization-high-input"
                      style={styles.inputTitles}
                    >
                      {this.state.priorityOptions.length > 0
                        ? LOCALIZE.formatString(
                            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                              .topTabs.timeSlots.addEditTimeSlotPopup.prioritizationOptionLabel,
                            this.state.priorityOptions.filter(
                              obj =>
                                obj.codename === REASON_FOR_TESTING_PRIORITY_CODENAME_CONST.HIGH
                            )[0].label
                          )
                        : ""}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnPopup.xl}
                    lg={columnSizes.secondColumnPopup.lg}
                    md={columnSizes.secondColumnPopup.md}
                    sm={columnSizes.secondColumnPopup.sm}
                    xs={columnSizes.secondColumnPopup.xs}
                  >
                    <div>
                      <input
                        id="ola-time-slots-add-time-slot-popup-prioritization-high-input"
                        className={
                          this.state.isValidPrioritizationValues ? "valid-field" : "invalid-field"
                        }
                        type="text"
                        aria-labelledby="ola-time-slots-add-time-slot-popup-prioritization-label ola-time-slots-add-time-slot-popup-prioritization-high-label ola-time-slots-add-time-slot-popup-prioritization-error"
                        value={this.state.prioritizationHighContent}
                        onChange={this.getPrioritizationHighContent}
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                      />
                      <span style={styles.prioritizationCalculatedNumber}>
                        {`${LOCALIZE.formatString(
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                            .topTabs.timeSlots.addEditTimeSlotPopup.prioritizationValueLabel,
                          this.getCalculatedPrioritizationValue(
                            this.state.prioritizationHighContent
                          )
                        )}`}
                      </span>
                    </div>
                  </Col>
                </Row>
                <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumnPopup.xl}
                    lg={columnSizes.firstColumnPopup.lg}
                    md={columnSizes.firstColumnPopup.md}
                    sm={columnSizes.firstColumnPopup.sm}
                    xs={columnSizes.firstColumnPopup.xs}
                    style={styles.indentedCol}
                  >
                    <label
                      id="ola-time-slots-add-time-slot-popup-prioritization-medium-label"
                      htmlFor="ola-time-slots-add-time-slot-popup-prioritization-medium-input"
                      style={styles.inputTitles}
                    >
                      {this.state.priorityOptions.length > 0
                        ? LOCALIZE.formatString(
                            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                              .topTabs.timeSlots.addEditTimeSlotPopup.prioritizationOptionLabel,
                            this.state.priorityOptions.filter(
                              obj =>
                                obj.codename === REASON_FOR_TESTING_PRIORITY_CODENAME_CONST.MEDIUM
                            )[0].label
                          )
                        : ""}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnPopup.xl}
                    lg={columnSizes.secondColumnPopup.lg}
                    md={columnSizes.secondColumnPopup.md}
                    sm={columnSizes.secondColumnPopup.sm}
                    xs={columnSizes.secondColumnPopup.xs}
                  >
                    <div>
                      <input
                        id="ola-time-slots-add-time-slot-popup-prioritization-medium-input"
                        className={
                          this.state.isValidPrioritizationValues ? "valid-field" : "invalid-field"
                        }
                        type="text"
                        aria-labelledby="ola-time-slots-add-time-slot-popup-prioritization-label ola-time-slots-add-time-slot-popup-prioritization-medium-label ola-time-slots-add-time-slot-popup-prioritization-error"
                        value={this.state.prioritizationMediumContent}
                        onChange={this.getPrioritizationMediumContent}
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                      />
                      <span style={styles.prioritizationCalculatedNumber}>
                        {`${LOCALIZE.formatString(
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                            .topTabs.timeSlots.addEditTimeSlotPopup.prioritizationValueLabel,
                          this.getCalculatedPrioritizationValue(
                            this.state.prioritizationMediumContent
                          )
                        )}`}
                      </span>
                    </div>
                  </Col>
                </Row>
                <Row
                  role="presentation"
                  className="align-items-center"
                  style={
                    !this.state.isValidPrioritizationValues
                      ? styles.parentRowContainerWithError
                      : styles.rowContainer
                  }
                >
                  <Col
                    xl={columnSizes.firstColumnPopup.xl}
                    lg={columnSizes.firstColumnPopup.lg}
                    md={columnSizes.firstColumnPopup.md}
                    sm={columnSizes.firstColumnPopup.sm}
                    xs={columnSizes.firstColumnPopup.xs}
                    style={styles.indentedCol}
                  >
                    <label
                      id="ola-time-slots-add-time-slot-popup-prioritization-low-label"
                      htmlFor="ola-time-slots-add-time-slot-popup-prioritization-low-input"
                      style={styles.inputTitles}
                    >
                      {this.state.priorityOptions.length > 0
                        ? LOCALIZE.formatString(
                            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                              .topTabs.timeSlots.addEditTimeSlotPopup.prioritizationOptionLabel,
                            this.state.priorityOptions.filter(
                              obj => obj.codename === REASON_FOR_TESTING_PRIORITY_CODENAME_CONST.LOW
                            )[0].label
                          )
                        : ""}
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnPopup.xl}
                    lg={columnSizes.secondColumnPopup.lg}
                    md={columnSizes.secondColumnPopup.md}
                    sm={columnSizes.secondColumnPopup.sm}
                    xs={columnSizes.secondColumnPopup.xs}
                  >
                    <div>
                      <input
                        id="ola-time-slots-add-time-slot-popup-prioritization-low-input"
                        className={
                          this.state.isValidPrioritizationValues ? "valid-field" : "invalid-field"
                        }
                        type="text"
                        aria-labelledby="ola-time-slots-add-time-slot-popup-prioritization-label ola-time-slots-add-time-slot-popup-prioritization-low-label ola-time-slots-add-time-slot-popup-prioritization-error"
                        value={this.state.prioritizationLowContent}
                        onChange={this.getPrioritizationLowContent}
                        style={{
                          ...styles.inputs,
                          ...accommodationsStyle
                        }}
                      />
                      <span style={styles.prioritizationCalculatedNumber}>
                        {`${LOCALIZE.formatString(
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                            .topTabs.timeSlots.addEditTimeSlotPopup.prioritizationValueLabel,
                          this.getCalculatedPrioritizationValue(this.state.prioritizationLowContent)
                        )}`}
                      </span>
                    </div>
                  </Col>
                </Row>
                {!this.state.isValidPrioritizationValues && (
                  <Row
                    role="presentation"
                    className="align-items-center"
                    style={styles.rowContainerWithError}
                  >
                    <Col
                      xl={columnSizes.firstColumnPopup.xl}
                      lg={columnSizes.firstColumnPopup.lg}
                      md={columnSizes.firstColumnPopup.md}
                      sm={columnSizes.firstColumnPopup.sm}
                      xs={columnSizes.firstColumnPopup.xs}
                    ></Col>
                    <Col
                      xl={columnSizes.secondColumnPopup.xl}
                      lg={columnSizes.secondColumnPopup.lg}
                      md={columnSizes.secondColumnPopup.md}
                      sm={columnSizes.secondColumnPopup.sm}
                      xs={columnSizes.secondColumnPopup.xs}
                    >
                      <label
                        id="ola-time-slots-add-time-slot-popup-prioritization-error"
                        style={styles.errorMessage}
                      >
                        {
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                            .topTabs.timeSlots.addEditTimeSlotPopup.prioritizationError
                        }
                      </label>
                    </Col>
                  </Row>
                )} */}
                <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                  <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                    <label
                      id="ola-time-slots-add-time-slot-popup-assessor-availability-input"
                      style={styles.inputTitles}
                    >
                      {LOCALIZE.formatString(
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.timeSlots.addEditTimeSlotPopup.assessorAvailabilityTable.title,
                        typeof this.state.assessorAvailabilityRowsDefinition.data !== "undefined"
                          ? this.state.assessorAvailabilityRowsDefinition.data.filter(
                              obj => obj.available === true
                            ).length
                          : "-",
                        typeof this.state.assessorAvailabilityRowsDefinition.data !== "undefined"
                          ? this.state.assessorAvailabilityRowsDefinition.data.length
                          : "-"
                      )}
                    </label>
                  </Col>
                  <Col xl={12} lg={12} md={12} sm={12} xs={12}>
                    <GenericTable
                      classnamePrefix="add-edit-time-slot-popup-assessors-availabilities"
                      columnsDefinition={assessorAvailabilityColumnsDefinition}
                      rowsDefinition={this.state.assessorAvailabilityRowsDefinition}
                      emptyTableMessage={
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.timeSlots.addEditTimeSlotPopup.assessorAvailabilityTable.noData
                      }
                      currentlyLoading={this.state.currentlyLoadingAssessorAvailability}
                    />
                  </Col>
                </Row>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeAddEditTimeSlotPopup}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            this.state.showAddTimeSlotPopup
              ? LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                  .timeSlots.addEditTimeSlotPopup.rightButton
              : LOCALIZE.commons.saveButton
          }
          rightButtonAction={
            this.state.showAddTimeSlotPopup ? this.handleAddTimeSlot : this.handleEditTimeSlot
          }
          rightButtonIcon={this.state.showAddTimeSlotPopup ? faPlusCircle : faSave}
          rightButtonState={
            this.state.isValidAddEditTimeSlotPopupForm
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
        />
        <PopupBox
          show={this.state.showDeleteTimeSlotPopup}
          handleClose={() => {}}
          size="lg"
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
              .timeSlots.deleteTimeSlotConfirmationPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                        .topTabs.timeSlots.deleteTimeSlotConfirmationPopup.systemMessageDescription,
                      <span style={styles.boldText}>
                        {
                          this.state.timeSlotToDelete[
                            `day_of_week_text_${this.props.currentLanguage}`
                          ]
                        }
                      </span>,
                      <span style={styles.boldText}>
                        {Object.keys(this.state.timeSlotToDelete).length > 0
                          ? this.state.timeSlotToDelete.formattedConvertedStartTime
                          : ""}
                      </span>,
                      <span style={styles.boldText}>
                        {Object.keys(this.state.timeSlotToDelete).length > 0
                          ? this.state.timeSlotToDelete.formattedConvertedEndTime
                          : ""}
                      </span>
                    )}
                  </p>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeDeleteTimeSlotConfirmationPopup}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.handleDeleteTestAssessor}
          rightButtonIcon={faTrashAlt}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testCenterData: state.testCenter.testCenterData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getLanguageData,
      getSelectedTestCenterData,
      setTestCenterData,
      getDaysOfWeek,
      addTestCenterOlaTimeSlot,
      deleteTestCenterOlaTimeSlot,
      editTestCenterOlaTimeSlot,
      getTestCenterOlaTimeSlotAssessorAvailabilities,
      getReasonForTestingPriorities
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TimeSlots);
