/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { Row, Col } from "react-bootstrap";
import { bindActionCreators } from "redux";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlusCircle,
  faTimes,
  faSave,
  faSpinner,
  faCheck,
  faPencilAlt,
  faTrashAlt
} from "@fortawesome/free-solid-svg-icons";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import DropdownSelect from "../../commons/DropdownSelect";
import { getUsersBasedOnSpecifiedPermission } from "../../../modules/PermissionsRedux";
import { PERMISSION } from "../../profile/Constants";
import { LANGUAGE_IDS, getLanguageData } from "../../../modules/LocalizeRedux";
import Switch from "react-switch";
import getSwitchTransformScale from "../../../helpers/switchTransformScale";
import {
  addTestCenterOlaTestAssessor,
  deleteTestCenterOlaTestAssessor,
  editTestCenterOlaTestAssessor,
  getSelectedTestCenterData,
  setTestCenterData
} from "../../../modules/TestCenterRedux";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import StyledTooltip, { EFFECT, TYPE } from "../../authentication/StyledTooltip";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";

const columnSizes = {
  firstColumnPopup: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumnPopup: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  mainContainer: {
    margin: "24px auto",
    width: "90%"
  },
  tableContainer: {
    margin: "24px 0"
  },
  rowContainer: {
    width: "90%",
    margin: "12px auto"
  },
  rowContainerWithError: {
    width: "90%",
    margin: "0px auto"
  },
  labelCol: {
    padding: "0px"
  },
  inputCol: {
    textAlign: "left"
  },
  inputTitles: {
    fontWeight: "bold"
  },
  buttonLabel: {
    margin: 0,
    paddingLeft: 6
  },
  loadingContainer: {
    margin: 24,
    textAlign: "center"
  },
  loading: {
    fontSize: "32px",
    padding: 6
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  actionIcon: {
    transform: "scale(1.2)"
  },
  allUnset: {
    all: "unset"
  },
  faCheckStyle: {
    color: "green"
  },
  boldText: {
    fontWeight: "bold"
  }
};

class TestAssessors extends Component {
  state = {
    showAddTestAssessorPopup: false,
    showEditTestAssessorPopup: false,
    showDeleteTestAssessorPopup: false,
    rowsDefinition: {},
    currentlyLoading: false,
    isLoadingTestAssessorOptions: false,
    testAssessorOptions: [],
    testAssessorSelectedOption: {},
    isLoadingLanguageCertificationOptions: false,
    languageCertificationOptions: [],
    languageCertificationSelectedOptions: [],
    languageCertificationSelectedOptionsAccessibility: LOCALIZE.commons.none,
    supervisorSwitchState: false,
    isLoadingSwitch: false,
    testAssessorToEdit: {},
    testAssessorToDelete: {},
    // default values
    switchHeight: 25,
    switchWidth: 50,
    isValidAddEditTestAssessorPopupForm: false
  };

  componentDidMount = () => {
    // populating test assessors table
    this.populateTestAssessors();
    // populating language certification options
    this.populateLanguageCertificationOptions();
    // getting switch dimensions
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = prevProps => {
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      // getting switch dimensions
      this.getUpdatedSwitchDimensions();
    }
    // if testCenterData gets updated
    if (prevProps.testCenterData !== this.props.testCenterData) {
      // populating test assessors table
      this.populateTestAssessors();
    }
  };

  getFormattedUserLanguageCertifications = certifiedLanguageIdsStr => {
    // initializing formattedUserLanguageCertifications
    let formattedUserLanguageCertifications = "";

    // English is part of the provided certifiedLanguageIds
    if (certifiedLanguageIdsStr.includes(LANGUAGE_IDS.english)) {
      // concatenating formattedUserLanguageCertifications
      formattedUserLanguageCertifications += LOCALIZE.commons.english;
    }

    // French is part of the provided certifiedLanguageIds
    if (certifiedLanguageIdsStr.includes(LANGUAGE_IDS.french)) {
      // formattedUserLanguageCertifications is still empty
      if (formattedUserLanguageCertifications === "") {
        // concatenating formattedUserLanguageCertifications
        formattedUserLanguageCertifications += LOCALIZE.commons.french;
        // formattedUserLanguageCertifications already contains a value (need to add a comma)
      } else {
        // concatenating formattedUserLanguageCertifications
        formattedUserLanguageCertifications += `, ${LOCALIZE.commons.french}`;
      }
    }

    return formattedUserLanguageCertifications;
  };

  populateTestAssessors = () => {
    this.setState({ currentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
      let rowsDefinition = {};
      const data = [];

      // looping in response given
      for (let i = 0; i < this.props.testCenterData.test_center_ola_test_assessors.length; i++) {
        const currentResult = this.props.testCenterData.test_center_ola_test_assessors[i];
        // formatting test assessor name/email
        const formattedTestAssessorNameEmail = `${currentResult.user_last_name}, ${currentResult.user_first_name} (${currentResult.user_email})`;
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          user_id: currentResult.user_id,
          column_1: formattedTestAssessorNameEmail,
          column_2: this.getFormattedUserLanguageCertifications(
            currentResult.certified_language_ids
          ),
          column_3: currentResult.supervisor ? (
            <FontAwesomeIcon icon={faCheck} style={styles.faCheckStyle} />
          ) : (
            ""
          ),
          column_4: (
            <>
              <StyledTooltip
                id={`test-center-ola-test-assessor-edit-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`test-center-ola-test-assessor-edit-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faPencilAlt} style={styles.actionIcon} />
                        </>
                      }
                      action={() => {
                        this.openEditTestAssessorPopup(currentResult);
                      }}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.testAssessors.testAssessorsTable.editButtonTooltipAccessibility,
                        formattedTestAssessorNameEmail
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.testAssessors.testAssessorsTable.editButtonTooltip
                      }
                    </p>
                  </div>
                }
              />
              <StyledTooltip
                id={`test-center-ola-test-assessor-delete-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`test-center-ola-test-assessor-delete-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faTrashAlt} style={styles.actionIcon} />
                        </>
                      }
                      action={() => {
                        this.openDeleteTestAssessorConfirmationPopup(currentResult);
                      }}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.testAssessors.testAssessorsTable
                          .deleteButtonTooltipAccessibility,
                        formattedTestAssessorNameEmail
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>{LOCALIZE.commons.deleteButton}</p>
                  </div>
                }
              />
            </>
          )
        });
      }

      // updating rowsDefinition object with provided data and needed style
      rowsDefinition = {
        column_1_style: COMMON_STYLE.LEFT_TEXT,
        column_2_style: COMMON_STYLE.CENTERED_TEXT,
        column_3_style: COMMON_STYLE.CENTERED_TEXT,
        column_4_style: COMMON_STYLE.CENTERED_TEXT,
        data: data
      };

      // saving results in state
      this.setState(
        {
          rowsDefinition: rowsDefinition,
          currentlyLoading: false
        },
        () => {
          // populating test assessor options (needs to be called here, since those test assessor options are dependent of the rowsDefinition state)
          this.populateTestAssessorOptions();
        }
      );
    });
  };

  openAddTestAssessorPopup = () => {
    this.setState({
      showAddTestAssessorPopup: true,
      showEditTestAssessorPopup: false
    });
  };

  closeAddEditTestAssessorPopup = () => {
    this.setState({
      showAddTestAssessorPopup: false,
      showEditTestAssessorPopup: false,
      testAssessorSelectedOption: {},
      languageCertificationSelectedOptions: [],
      supervisorSwitchState: false,
      isValidAddEditTestAssessorPopupForm: false
    });
  };

  openEditTestAssessorPopup = testAssessorData => {
    this.setState(
      {
        showEditTestAssessorPopup: true,
        showAddTestAssessorPopup: false,
        testAssessorToEdit: testAssessorData,
        testAssessorSelectedOption: {
          label: `${testAssessorData.user_last_name}, ${testAssessorData.user_first_name} (${testAssessorData.user_email})`,
          value: testAssessorData.user_id
        },
        languageCertificationSelectedOptions: this.getFormattedSelectedLanguageCertificationOption(
          testAssessorData.certified_language_ids
        ),
        supervisorSwitchState: testAssessorData.supervisor
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  openDeleteTestAssessorConfirmationPopup = testAssessorData => {
    this.setState({
      showDeleteTestAssessorPopup: true,
      testAssessorToDelete: testAssessorData
    });
  };

  closeDeleteTestAssessorConfirmationPopup = () => {
    this.setState({
      showDeleteTestAssessorPopup: false,
      testAssessorToDelete: {}
    });
  };

  handleDeleteTestAssessor = () => {
    // setting body
    const body = {
      id: this.state.testAssessorToDelete.id
    };
    this.props.deleteTestCenterOlaTestAssessor(body).then(response => {
      if (response.ok) {
        // closing popup
        this.closeDeleteTestAssessorConfirmationPopup();
        // getting and setting test center data
        this.props.getSelectedTestCenterData(this.props.testCenterData.id).then(response => {
          this.props.setTestCenterData(response);
        });
        // should never happen
      } else {
        throw new Error(
          "An error occurred during the delete test center OLA test assessor process"
        );
      }
    });
  };

  populateTestAssessorOptions = () => {
    this.setState({ isLoadingTestAssessorOptions: true }, () => {
      // initializing alreadyAssociatedTestAssessorIds
      const alreadyAssociatedTestAssessorIds = [];

      // looping in rowsDefinition
      for (let i = 0; i < this.state.rowsDefinition.data.length; i++) {
        // populating alreadyAssociatedTestAssessorIds
        alreadyAssociatedTestAssessorIds.push(this.state.rowsDefinition.data[i].user_id);
      }

      // initializing testAssessorOptions
      const testAssessorOptions = [];
      // getting all scorer users
      this.props.getUsersBasedOnSpecifiedPermission(PERMISSION.scorer).then(response => {
        for (let i = 0; i < response.length; i++) {
          // user of current iteration is not already associated to this test center
          if (!alreadyAssociatedTestAssessorIds.includes(response[i].user_id)) {
            // populating testAssessorOptions
            testAssessorOptions.push({
              label: `${response[i].last_name}, ${response[i].first_name} (${response[i].email})`,
              value: response[i].user_id
            });
          }
        }
      });
      this.setState({
        testAssessorOptions: testAssessorOptions,
        isLoadingTestAssessorOptions: false
      });
    });
  };

  populateLanguageCertificationOptions = () => {
    this.setState({ isLoadingLanguageCertificationOptions: true }, () => {
      // initializing testAssessorOptions
      const languageCertificationOptions = [];
      // getting all scorer users
      this.props.getLanguageData().then(response => {
        for (let i = 0; i < response.length; i++) {
          languageCertificationOptions.push({
            label: response[i].language_text[this.props.currentLanguage][0].text,
            value: response[i].language_id
          });
        }
      });
      this.setState({
        languageCertificationOptions: languageCertificationOptions,
        isLoadingLanguageCertificationOptions: false
      });
    });
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoadingSwitch: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoadingSwitch: false });
        }
      );
    });
  };

  getSelectedTestAssessorOption = selectedOption => {
    this.setState({ testAssessorSelectedOption: selectedOption }, () => {
      // validating form
      this.validateForm();
    });
  };

  // get selected test order number option
  getSelectedLanguageCertificationOptions = selectedOptions => {
    let languageCertificationSelectedOptions = null;
    if (selectedOptions === null) {
      languageCertificationSelectedOptions = [];
    } else {
      languageCertificationSelectedOptions = selectedOptions;
    }
    this.setState({
      languageCertificationSelectedOptions: languageCertificationSelectedOptions
    });
    this.getSelectedLanguageCertificationOptionAccessibility(languageCertificationSelectedOptions);
  };

  // getting/formatting current selected users options (for accessibility only)
  getSelectedLanguageCertificationOptionAccessibility = languageCertificationSelectedOptions => {
    let languageCertificationSelectedOptionsAccessibility = "";
    if (languageCertificationSelectedOptions.length > 0) {
      for (let i = 0; i < languageCertificationSelectedOptions.length; i++) {
        languageCertificationSelectedOptionsAccessibility += `${languageCertificationSelectedOptions[i].label}, `;
      }
    } else {
      languageCertificationSelectedOptionsAccessibility = LOCALIZE.commons.none;
    }
    this.setState(
      {
        languageCertificationSelectedOptionsAccessibility:
          languageCertificationSelectedOptionsAccessibility
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  getFormattedSelectedLanguageCertificationOption = certifiedLanguageIdsStr => {
    // initializing languageCertificationSelectedOptions
    const languageCertificationSelectedOptions = [];

    // English is part of the provided certifiedLanguageIds
    if (certifiedLanguageIdsStr.includes(LANGUAGE_IDS.english)) {
      languageCertificationSelectedOptions.push(
        this.state.languageCertificationOptions.filter(obj => obj.value === LANGUAGE_IDS.english)[0]
      );
    }

    // French is part of the provided certifiedLanguageIds
    if (certifiedLanguageIdsStr.includes(LANGUAGE_IDS.french)) {
      languageCertificationSelectedOptions.push(
        this.state.languageCertificationOptions.filter(obj => obj.value === LANGUAGE_IDS.french)[0]
      );
    }

    return languageCertificationSelectedOptions;
  };

  handleSupervisorSwitchStateUpdate = event => {
    this.setState({ supervisorSwitchState: event }, () => {
      // validating form
      this.validateForm();
    });
  };

  validateForm = () => {
    // initializing isValidAddEditTestAssessorPopupForm
    let isValidAddEditTestAssessorPopupForm = false;

    // getting/setting valid states
    const isValidTestAssessor = Object.keys(this.state.testAssessorSelectedOption).length > 0;
    const isValidLanguageCertification =
      Object.keys(this.state.languageCertificationSelectedOptions).length > 0;

    // all mandatory fields have been provided
    if (isValidTestAssessor && isValidLanguageCertification) {
      isValidAddEditTestAssessorPopupForm = true;
    }

    this.setState({ isValidAddEditTestAssessorPopupForm: isValidAddEditTestAssessorPopupForm });
  };

  handleAddTestAssessor = () => {
    // setting body
    const body = {
      test_center_id: this.props.testCenterData.id,
      user_id: this.state.testAssessorSelectedOption.value,
      language_certification_data: this.state.languageCertificationSelectedOptions,
      supervisor: this.state.supervisorSwitchState
    };
    this.props.addTestCenterOlaTestAssessor(body).then(response => {
      if (response.ok) {
        // closing popup
        this.closeAddEditTestAssessorPopup();
        // getting and setting test center data
        this.props.getSelectedTestCenterData(this.props.testCenterData.id).then(response => {
          this.props.setTestCenterData(response);
        });
        // should never happen
      } else {
        throw new Error("An error occurred during the add test center OLA test assessor process");
      }
    });
  };

  handleEditTestAssessor = () => {
    // setting body
    const body = {
      id: this.state.testAssessorToEdit.id,
      test_center_id: this.props.testCenterData.id,
      user_id: this.state.testAssessorSelectedOption.value,
      language_certification_data: this.state.languageCertificationSelectedOptions,
      supervisor: this.state.supervisorSwitchState
    };
    this.props.editTestCenterOlaTestAssessor(body).then(response => {
      if (response.ok) {
        // closing popup
        this.closeAddEditTestAssessorPopup();
        // getting and setting test center data
        this.props.getSelectedTestCenterData(this.props.testCenterData.id).then(response => {
          this.props.setTestCenterData(response);
        });
        // should never happen
      } else {
        throw new Error(
          "An error occurred during the update test center OLA test assessor data process"
        );
      }
    });
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
            .testAssessors.testAssessorsTable.assessor,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
            .testAssessors.testAssessorsTable.languageCertification,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
            .testAssessors.testAssessorsTable.supervisor,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
            .testAssessors.testAssessorsTable.actions,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <div>
          <CustomButton
            label={
              <>
                <FontAwesomeIcon icon={faPlusCircle} />
                <span
                  id="ola-test-assessor-add-test-assessor"
                  style={styles.buttonLabel}
                  className="notranslate"
                >
                  {
                    LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                      .testAssessors.addTestAssessorButton
                  }
                </span>
              </>
            }
            action={this.openAddTestAssessorPopup}
            buttonTheme={THEME.PRIMARY}
          />
        </div>
        <div style={styles.tableContainer}>
          <GenericTable
            classnamePrefix="ola-configs-test-assessors"
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={
              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                .testAssessors.testAssessorsTable.noData
            }
            currentlyLoading={this.state.currentlyLoading}
          />
        </div>
        <PopupBox
          show={this.state.showAddTestAssessorPopup || this.state.showEditTestAssessorPopup}
          handleClose={() => {}}
          onPopupOpen={this.populateTestAssessorOptions}
          title={
            this.state.showAddTestAssessorPopup
              ? LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                  .testAssessors.addEditTestAssessorPopup.addTitle
              : LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                  .testAssessors.addEditTestAssessorPopup.editTitle
          }
          description={
            <div>
              <p>
                {this.state.showAddTestAssessorPopup
                  ? LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                      .testAssessors.addEditTestAssessorPopup.addDescription
                  : LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                      .testAssessors.addEditTestAssessorPopup.editDescription}
              </p>
              <div>
                <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumnPopup.xl}
                    lg={columnSizes.firstColumnPopup.lg}
                    md={columnSizes.firstColumnPopup.md}
                    sm={columnSizes.firstColumnPopup.sm}
                    xs={columnSizes.firstColumnPopup.xs}
                  >
                    <label
                      id="ola-test-assessor-add-test-assessor-popup-test-assessor-label"
                      style={styles.inputTitles}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.testAssessors.addEditTestAssessorPopup.testAssessorLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnPopup.xl}
                    lg={columnSizes.secondColumnPopup.lg}
                    md={columnSizes.secondColumnPopup.md}
                    sm={columnSizes.secondColumnPopup.sm}
                    xs={columnSizes.secondColumnPopup.xs}
                  >
                    {this.state.isLoadingTestAssessorOptions ? (
                      <div style={styles.loadingContainer}>
                        <label className="fa fa-spinner fa-spin">
                          <FontAwesomeIcon icon={faSpinner} style={styles.loading} />
                        </label>
                      </div>
                    ) : (
                      <DropdownSelect
                        idPrefix="ola-test-assessor-add-test-assessor-popup-test-assessor"
                        ariaLabelledBy="ola-test-assessor-add-test-assessor-popup-test-assessor-label"
                        ariaRequired={true}
                        options={this.state.testAssessorOptions}
                        onChange={this.getSelectedTestAssessorOption}
                        defaultValue={this.state.testAssessorSelectedOption}
                        hasPlaceholder={true}
                        isDisabled={this.state.showEditTestAssessorPopup}
                      />
                    )}
                  </Col>
                </Row>
                <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumnPopup.xl}
                    lg={columnSizes.firstColumnPopup.lg}
                    md={columnSizes.firstColumnPopup.md}
                    sm={columnSizes.firstColumnPopup.sm}
                    xs={columnSizes.firstColumnPopup.xs}
                  >
                    <label
                      id="ola-test-assessor-add-test-assessor-popup-language-certification-label"
                      style={styles.inputTitles}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.testAssessors.addEditTestAssessorPopup.languageCertificationLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnPopup.xl}
                    lg={columnSizes.secondColumnPopup.lg}
                    md={columnSizes.secondColumnPopup.md}
                    sm={columnSizes.secondColumnPopup.sm}
                    xs={columnSizes.secondColumnPopup.xs}
                  >
                    {this.state.isLoadingLanguageCertificationOptions ? (
                      <div style={styles.loadingContainer}>
                        <label className="fa fa-spinner fa-spin">
                          <FontAwesomeIcon icon={faSpinner} style={styles.loading} />
                        </label>
                      </div>
                    ) : (
                      <>
                        <DropdownSelect
                          idPrefix="ola-test-assessor-add-test-assessor-popup-language-certification"
                          ariaLabelledBy="ola-test-assessor-add-test-assessor-popup-language-certification-label ola-test-assessor-add-test-assessor-popup-language-certification-current-value-accessibility"
                          ariaRequired={true}
                          options={this.state.languageCertificationOptions}
                          onChange={this.getSelectedLanguageCertificationOptions}
                          defaultValue={this.state.languageCertificationSelectedOptions}
                          hasPlaceholder={true}
                          isMulti={true}
                        />
                        <label
                          id="ola-test-assessor-add-test-assessor-popup-language-certification-current-value-accessibility"
                          style={styles.hiddenText}
                        >
                          {this.state.languageCertificationSelectedOptionsAccessibility}
                        </label>
                      </>
                    )}
                  </Col>
                </Row>
                <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumnPopup.xl}
                    lg={columnSizes.firstColumnPopup.lg}
                    md={columnSizes.firstColumnPopup.md}
                    sm={columnSizes.firstColumnPopup.sm}
                    xs={columnSizes.firstColumnPopup.xs}
                  >
                    <label
                      htmlFor="ola-test-assessor-add-test-assessor-popup-supervisor"
                      style={styles.inputTitles}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                          .topTabs.testAssessors.addEditTestAssessorPopup.supervisorLabel
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumnPopup.xl}
                    lg={columnSizes.secondColumnPopup.lg}
                    md={columnSizes.secondColumnPopup.md}
                    sm={columnSizes.secondColumnPopup.sm}
                    xs={columnSizes.secondColumnPopup.xs}
                  >
                    {!this.state.isLoadingSwitch && (
                      <Switch
                        id="ola-test-assessor-add-test-assessor-popup-supervisor"
                        onChange={e => {
                          this.handleSupervisorSwitchStateUpdate(e);
                        }}
                        checked={this.state.supervisorSwitchState}
                        height={this.state.switchHeight}
                        width={this.state.switchWidth}
                      />
                    )}
                  </Col>
                </Row>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeAddEditTestAssessorPopup}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            this.state.showAddTestAssessorPopup
              ? LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
                  .testAssessors.addEditTestAssessorPopup.rightButton
              : LOCALIZE.commons.saveButton
          }
          rightButtonAction={
            this.state.showAddTestAssessorPopup
              ? this.handleAddTestAssessor
              : this.handleEditTestAssessor
          }
          rightButtonIcon={this.state.showAddTestAssessorPopup ? faPlusCircle : faSave}
          rightButtonState={
            this.state.isValidAddEditTestAssessorPopupForm
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
        />
        <PopupBox
          show={this.state.showDeleteTestAssessorPopup}
          handleClose={() => {}}
          size="lg"
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.topTabs
              .testAssessors.deleteTestAssessorConfirmationPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola
                        .topTabs.testAssessors.deleteTestAssessorConfirmationPopup
                        .systemMessageDescription,
                      <span
                        style={styles.boldText}
                      >{`${this.state.testAssessorToDelete.user_last_name}, ${this.state.testAssessorToDelete.user_first_name} (${this.state.testAssessorToDelete.user_email})`}</span>
                    )}
                  </p>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeDeleteTestAssessorConfirmationPopup}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.handleDeleteTestAssessor}
          rightButtonIcon={faTrashAlt}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testCenterData: state.testCenter.testCenterData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getUsersBasedOnSpecifiedPermission,
      getLanguageData,
      addTestCenterOlaTestAssessor,
      getSelectedTestCenterData,
      setTestCenterData,
      editTestCenterOlaTestAssessor,
      deleteTestCenterOlaTestAssessor
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestAssessors);
