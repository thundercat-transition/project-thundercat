/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Pagination } from "react-bootstrap";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import * as _ from "lodash";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faWandSparkles,
  faTimes,
  faPlusCircle,
  faUniversalAccess,
  faFileCircleExclamation,
  faTrashAlt,
  faSpinner
} from "@fortawesome/free-solid-svg-icons";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import {
  updateCurrentTestCenterAccommodationRequestsPageState,
  updateTestCenterAccommodationRequestsPageSizeState,
  updateSearchTestCenterAccommodationRequestsStates,
  addNonStandardTestSessionToTestCenter,
  triggerTableUpdates,
  updateTestSessionNonStandardTestSessionData
} from "../../../modules/TestCenterRedux";
import StyledTooltip, { EFFECT, TYPE } from "../../authentication/StyledTooltip";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import {
  getFoundPendingAccommodationFilesAsATcm,
  getAllPendingAccommodationFilesAsATcm
} from "../../../modules/AaeRedux";
import moment from "moment";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import AccommodationsRequestDetailsPopup, {
  ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE
} from "../../aae/AccommodationsRequestDetailsPopup";
import TopTabs from "../../commons/TopTabs";
import AddTestSessionPopupContent from "./AddTestSessionPopupContent";
import "../../../css/top-tabs.css";
import { TestSkillSleDescCodename } from "../../testFactory/Constants";
import { convertUsersTimezoneDateTimeToUtc } from "../../../modules/helpers";

export const TEST_SESSION_OBJ_TEMPLATE = {
  roomSelectedOption: { label: LOCALIZE.dropdownSelect.pleaseSelect, value: null },
  date: "",
  isValidDate: false,
  isValidFutureDate: false,
  startTimeHour: "",
  startTimeMinute: "",
  endTimeHour: "",
  endTimeMinute: "",
  testAssessorSelectedOption: []
};

const styles = {
  mainContainer: {
    margin: "24px 0"
  },
  tableIcon: {
    transform: "scale(1.2)"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  noPaddingTopBottom: {
    paddingTop: 0,
    paddingBottom: 0
  },
  contentTab: {
    paddingLeft: 3,
    paddingRight: 3
  },
  allUnset: {
    all: "unset"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2
  },
  icon: {
    paddingRight: 6,
    verticalAlign: "middle"
  },
  transparentText: {
    color: "transparent"
  },
  customLoadingContainer: {
    display: "grid",
    gridTemplateColumns: "1fr"
  },
  loadingOverlappingStyle: {
    gridRowStart: "1",
    gridColumnStart: "1"
  }
};

class PendingRequests extends Component {
  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    resultsFound: 0,
    pendingRequests: [],
    clearSearchTriggered: false,
    showAccommodationsRequestDetailsPopup: false,
    selectedAccommodationRequestData: {},
    showAddTestSessionPopup: false,
    isLoadingAddTestSession: false,
    selectedPendingRequestData: {},
    showTestSessionTimeOverlappingErrorPopup: false,
    showDeleteTestSessionTabPopup: false,
    indexToBeDeleted: null,
    showInvalidTestSessionDateAndTimeCombinationPopup: false
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentTestCenterAccommodationRequestsPageState(1);
    // populating associated test administrators
    this.populatePendingRequestsBasedOnActiveSearch();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populatePendingRequestsBasedOnActiveSearch();
    }
    // if testCenterAccommodationRequestsPaginationPage gets updated
    if (
      prevProps.testCenterAccommodationRequestsPaginationPage !==
      this.props.testCenterAccommodationRequestsPaginationPage
    ) {
      this.populatePendingRequestsBasedOnActiveSearch();
    }
    // if testCenterAccommodationRequestsPaginationPageSize get updated
    if (
      prevProps.testCenterAccommodationRequestsPaginationPageSize !==
      this.props.testCenterAccommodationRequestsPaginationPageSize
    ) {
      this.populatePendingRequestsBasedOnActiveSearch();
    }
    // if search testCenterAccommodationRequestsKeyword gets updated
    if (
      prevProps.testCenterAccommodationRequestsKeyword !==
      this.props.testCenterAccommodationRequestsKeyword
    ) {
      // if testCenterAccommodationRequestsKeyword redux state is empty
      if (this.props.testCenterAccommodationRequestsKeyword !== "") {
        this.populateFoundPendingRequests();
      } else if (
        this.props.testCenterAccommodationRequestsKeyword === "" &&
        this.props.testCenterAccommodationRequestsActiveSearch
      ) {
        this.populateFoundPendingRequests();
      }
    }
    // if testCenterAccommodationRequestsActiveSearch gets updated
    if (
      prevProps.testCenterAccommodationRequestsActiveSearch !==
      this.props.testCenterAccommodationRequestsActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.testCenterAccommodationRequestsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populatePendingRequests();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundPendingRequests();
      }
    }
    // if triggerTableUpdatesState gets updated
    if (prevProps.triggerTableUpdatesState !== this.props.triggerTableUpdatesState) {
      this.populatePendingRequestsBasedOnActiveSearch();
    }
  };

  getTABS = () => {
    const conditionalContentTabs = [];
    if (typeof this.props.testCenterNonStandardTestSessionData !== "undefined") {
      // looping in testCenterNonStandardTestSessionData array
      for (let i = 0; i < this.props.testCenterNonStandardTestSessionData.length; i++) {
        // first tab
        if (i === 0) {
          conditionalContentTabs.push({
            key: `test-session-${i + 1}`,
            tabName: LOCALIZE.formatString(
              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                .topTabs.testSessionsData.addTestSessionPopup.tabName,
              i + 1
            ),
            body: (
              <AddTestSessionPopupContent
                indexOfCurrentSelectedTab={i}
                selectedTestSessionData={this.state.selectedPendingRequestData}
              />
            )
          });
        } else {
          conditionalContentTabs.push({
            key: `test-session-${i + 1}`,
            tabName: (
              <>
                <span>
                  {LOCALIZE.formatString(
                    LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                      .topTabs.testSessionsData.addTestSessionPopup.tabName,
                    i + 1
                  )}
                </span>
                <StyledTooltip
                  id={`delete-testing-${i + 1}-tooltip`}
                  place="top"
                  type={TYPE.light}
                  effect={EFFECT.solid}
                  openOnClick={false}
                  tooltipElement={
                    <div style={styles.allUnset}>
                      <CustomButton
                        dataTip=""
                        dataFor={`delete-testing-${i + 1}-tooltip`}
                        label={
                          <>
                            <FontAwesomeIcon icon={faTrashAlt} />
                          </>
                        }
                        action={() => this.openDeleteTestSessionTabPopup(i)}
                        customStyle={{ ...styles.actionButton, ...styles.noPaddingTopBottom }}
                        buttonTheme={THEME.SECONDARY}
                      />
                    </div>
                  }
                  tooltipContent={
                    <div>
                      <p>{LOCALIZE.commons.deleteButton}</p>
                    </div>
                  }
                />
              </>
            ),
            body: (
              <AddTestSessionPopupContent
                indexOfCurrentSelectedTab={i}
                selectedTestSessionData={this.state.selectedPendingRequestData}
              />
            )
          });
        }
      }
    }
    return conditionalContentTabs;
  };

  openDeleteTestSessionTabPopup = index => {
    this.setState({ showDeleteTestSessionTabPopup: true, indexToBeDeleted: index });
  };

  handleDeleteTestSessionTab = () => {
    const copyOfTestCenterNonStandardTestSessionData = _.cloneDeep(
      this.props.testCenterNonStandardTestSessionData
    );
    // deleting respective test session tab
    copyOfTestCenterNonStandardTestSessionData.splice(this.state.indexToBeDeleted, 1);
    // updating redux states
    this.props.updateTestSessionNonStandardTestSessionData(
      copyOfTestCenterNonStandardTestSessionData
    );
    // closing popup
    this.closeDeleteTestSessionTabPopup();
  };

  closeDeleteTestSessionTabPopup = () => {
    this.setState({ showDeleteTestSessionTabPopup: false, indexToBeDeleted: null });
  };

  handleAddTestSessionTabPopup = () => {
    // creating a copy of the testCenterNonStandardTestSessionData redux state
    const copyOfTestCenterNonStandardTestSessionData = _.cloneDeep(
      this.props.testCenterNonStandardTestSessionData
    );
    // adding new test session object
    copyOfTestCenterNonStandardTestSessionData.push(TEST_SESSION_OBJ_TEMPLATE);
    // updating redux states
    this.props.updateTestSessionNonStandardTestSessionData(
      copyOfTestCenterNonStandardTestSessionData
    );
    // switching to new tab
    setTimeout(() => {
      document
        .getElementById(
          `top-tabs-tab-test-session-${copyOfTestCenterNonStandardTestSessionData.length}`
        )
        .click();
    }, 100);
  };

  populatePendingRequestsBasedOnActiveSearch = () => {
    // current search
    if (this.props.testCenterAccommodationRequestsActiveSearch) {
      this.populateFoundPendingRequests();
      // no search
    } else {
      this.populatePendingRequests();
    }
  };

  // populating pending accommodation requests
  populatePendingRequests = () => {
    this.setState({ currentlyLoading: true }, () => {
      const pendingRequestsArray = [];
      this.props
        .getAllPendingAccommodationFilesAsATcm(
          this.props.testCenterAccommodationRequestsPaginationPage,
          this.props.testCenterAccommodationRequestsPaginationPageSize,
          this.props.testCenterData.id
        )
        .then(response => {
          this.populateTestCenterAccommodationRequestsObject(pendingRequestsArray, response);
        })
        .then(() => {
          if (this.state.clearSearchTriggered) {
            // go back to the first page to avoid display bugs
            this.props.updateCurrentTestCenterAccommodationRequestsPageState(1);
            this.setState({ clearSearchTriggered: false });
          }
        })
        .then(() => {
          this.setState({ currentlyLoading: false });
        });
    });
  };

  // populating found pending accommodation requests
  populateFoundPendingRequests = () => {
    // making sure that the table is not currently loading
    if (!this.state.currentlyLoading) {
      this.setState(
        { currentlyLoading: true },
        () => {
          const pendingRequestsArray = [];
          setTimeout(() => {
            this.props
              .getFoundPendingAccommodationFilesAsATcm(
                this.props.testCenterAccommodationRequestsKeyword,
                this.props.currentLanguage,
                this.props.testCenterAccommodationRequestsPaginationPage,
                this.props.testCenterAccommodationRequestsPaginationPageSize,
                this.props.testCenterData.id
              )
              .then(response => {
                // there are no results found
                if (response[0] === "no results found") {
                  this.setState({
                    testSesions: [],
                    rowsDefinition: {},
                    numberOfPages: 1,
                    nextPageNumber: 0,
                    previousPageNumber: 0,
                    resultsFound: 0
                  });
                  // there is at least one result found
                } else {
                  this.populateTestCenterAccommodationRequestsObject(
                    pendingRequestsArray,
                    response
                  );
                  this.setState({ resultsFound: response.count });
                }
              })
              .then(() => {
                this.setState(
                  {
                    currentlyLoading: false
                  },
                  () => {
                    // if there is at least one result found
                    if (pendingRequestsArray.length > 0) {
                      // make sure that this element exsits to avoid any error
                      if (
                        document.getElementById(
                          "accommodation-requests-pending-requests-results-found"
                        )
                      ) {
                        document
                          .getElementById("accommodation-requests-pending-requests-results-found")
                          .focus();
                      }
                      // no results found
                    } else {
                      // focusing on no results found row
                      document
                        .getElementById("accommodation-requests-pending-requests-no-data")
                        .focus();
                    }
                  }
                );
              });
          });
        },
        250
      );
    }
  };

  populateTestCenterAccommodationRequestsObject = (pendingRequestsArray, response) => {
    // // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // looping in response given
    for (let i = 0; i < response.results.length; i++) {
      const currentResult = response.results[i];
      // pushing needed data in pendingRequestsArray
      pendingRequestsArray.push(response.results[i]);
      // pushing needed data in data array (needed for GenericTable component)
      data.push({
        column_1:
          currentResult.test_id !== null
            ? `${currentResult[`test_name_${this.props.currentLanguage}`]} - v${
                currentResult.version
              }`
            : currentResult.specified_test_description,
        column_2: `${currentResult.user_last_name}, ${currentResult.user_first_name}`,
        column_3: `${currentResult[`requesting_dept_desc_${this.props.currentLanguage}`]} (${
          currentResult[`requesting_dept_abrv_${this.props.currentLanguage}`]
        })`,
        column_4: currentResult.created_date,
        column_5: currentResult.closing_date,
        column_6: (
          <>
            <StyledTooltip
              id={`accommodation-requests-pending-requests-view-accommodation-details-row-${i}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`accommodation-requests-pending-requests-view-accommodation-details-row-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon
                          icon={
                            currentResult.is_alternate_test_request
                              ? faFileCircleExclamation
                              : faUniversalAccess
                          }
                          style={styles.tableIcon}
                        />
                      </>
                    }
                    action={() => this.openAccommodationsRequestDetailsPopup(currentResult)}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.PRIMARY}
                    ariaLabel={LOCALIZE.formatString(
                      currentResult.is_alternate_test_request
                        ? LOCALIZE.accommodationsRequestDetailsPopup
                            .viewAlternateTestRequestDetailsAccessibilityLabel
                        : LOCALIZE.accommodationsRequestDetailsPopup
                            .viewAccommodationsRequestDetailsAccessibilityLabel,
                      `${currentResult[`test_name_${this.props.currentLanguage}`]} - v${
                        currentResult.version
                      }`
                    )}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {currentResult.is_alternate_test_request
                      ? LOCALIZE.accommodationsRequestDetailsPopup.viewAlternateTestRequestDetails
                      : LOCALIZE.accommodationsRequestDetailsPopup.viewAccommodationsRequestDetails}
                  </p>
                </div>
              }
            />
            <StyledTooltip
              id={`accommodation-requests-pending-requests-view-row-${i}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`accommodation-requests-pending-requests-view-row-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faWandSparkles} style={styles.tableIcon} />
                      </>
                    }
                    action={() => this.openAddTestSessionPopup(currentResult)}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.PRIMARY}
                    ariaLabel={LOCALIZE.formatString(
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .accommodationRequests.topTabs.pendingRequests.table
                        .viewButtonAccessibility,
                      currentResult.user_first_name,
                      currentResult.user_last_name
                    )}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>{LOCALIZE.commons.addButton}</p>
                </div>
              }
            />
          </>
        )
      });
    }
    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.LEFT_TEXT,
      column_3_style: COMMON_STYLE.LEFT_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      column_6_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };
    // saving results in state
    this.setState({
      pendingRequests: pendingRequestsArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(
        response.count / this.props.testCenterAccommodationRequestsPaginationPageSize
      ),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  openAccommodationsRequestDetailsPopup = accommodationRequestData => {
    // updating needed states
    this.setState({
      showAccommodationsRequestDetailsPopup: true,
      selectedAccommodationRequestData: accommodationRequestData
    });
  };

  closeAccommodationsRequestDetailsPopup = () => {
    this.setState({
      showAccommodationsRequestDetailsPopup: false,
      selectedAccommodationRequestData: {}
    });
  };

  openAddTestSessionPopup = data => {
    const testCenterNonStandardTestSessionData = [TEST_SESSION_OBJ_TEMPLATE];
    this.props.updateTestSessionNonStandardTestSessionData(testCenterNonStandardTestSessionData);
    setTimeout(() => {
      this.setState({
        showAddTestSessionPopup: true,
        selectedPendingRequestData: data,
        isLoadingAddTestSession: false
      });
    }, 100);
  };

  closeAddTestSessionPopup = () => {
    this.setState({
      showAddTestSessionPopup: false,
      selectedPendingRequestData: {},
      showTestSessionTimeOverlappingErrorPopup: false,
      showInvalidTestSessionDateAndTimeCombinationPopup: false
    });
  };

  closeTimeOverlappingErrorPopup = () => {
    this.setState({ showTestSessionTimeOverlappingErrorPopup: false });
  };

  handleAddTestSession = () => {
    // building the finalArray that will be sent to the backend
    const finalArray = [];
    // setting loading logic
    this.setState({ isLoadingAddTestSession: true }, () => {
      // looping in testCenterNonStandardTestSessionData
      for (let i = 0; i < this.props.testCenterNonStandardTestSessionData.length; i++) {
        // converting start time and end time from user's timezone to UTC
        const utcStartDateTime = convertUsersTimezoneDateTimeToUtc(
          `${this.props.testCenterNonStandardTestSessionData[i].date} ${this.props.testCenterNonStandardTestSessionData[i].startTimeHour.label}:${this.props.testCenterNonStandardTestSessionData[i].startTimeMinute.label}`
        );
        const utcEndDateTime = convertUsersTimezoneDateTimeToUtc(
          `${this.props.testCenterNonStandardTestSessionData[i].date} ${this.props.testCenterNonStandardTestSessionData[i].endTimeHour.label}:${this.props.testCenterNonStandardTestSessionData[i].endTimeMinute.label}`
        );

        // for second iteration and up, making sure that the start date and time is same or later than previous test session end date and time
        if (i > 0) {
          // getting previous end date time
          const previousEndDateTime = `${
            this.props.testCenterNonStandardTestSessionData[i - 1].date
          } ${this.props.testCenterNonStandardTestSessionData[i - 1].endTimeHour.label}:${
            this.props.testCenterNonStandardTestSessionData[i - 1].endTimeMinute.label
          }`;
          // getting previous start date time
          const previousStartDateTime = `${this.props.testCenterNonStandardTestSessionData[i].date} ${this.props.testCenterNonStandardTestSessionData[i].startTimeHour.label}:${this.props.testCenterNonStandardTestSessionData[i].startTimeMinute.label}`;
          const isValidTestSessionDateAndTimeCombination = moment(
            previousEndDateTime
          ).isSameOrBefore(moment(previousStartDateTime));
          // if not valid
          if (!isValidTestSessionDateAndTimeCombination) {
            // open invalid test session date and time combination popup
            this.openInvalidTestSessionDateAndTimeCombinationPopup();
            // breaking the loop/function
            return;
          }
        }

        finalArray.push({
          assessment_process_assigned_test_specs_id:
            this.state.selectedPendingRequestData.assessment_process_assigned_test_specs_id,
          test_center_id: this.props.testCenterData.id,
          test_center_room_id:
            this.props.testCenterNonStandardTestSessionData[i].roomSelectedOption.value,
          date: this.props.testCenterNonStandardTestSessionData[i].date,
          // "yyyy-mm-dd hh:mm:ss Z"
          utc_start_time: utcStartDateTime,
          // "yyyy-mm-dd hh:mm:ss Z"
          utc_end_time: utcEndDateTime,
          user_accommodation_file_id: this.state.selectedPendingRequestData.id,
          // will be defined if ORAL test
          test_assessor_user_id:
            this.state.selectedPendingRequestData.test_skill_sub_type_codename ===
              TestSkillSleDescCodename.ORAL_EN ||
            this.state.selectedPendingRequestData.test_skill_sub_type_codename ===
              TestSkillSleDescCodename.ORAL_FR
              ? this.props.testCenterNonStandardTestSessionData[i].testAssessorSelectedOption.value
              : null
        });
      }

      this.props.addNonStandardTestSessionToTestCenter(finalArray).then(response => {
        // successful request
        if (response.ok) {
          // triggering table updates on the test sessions page
          this.props.triggerTableUpdates();
          // closing popup
          this.closeAddTestSessionPopup();
          setTimeout(() => {
            // stopping loading logic
            this.setState({ isLoadingAddTestSession: false });
          }, 250);
          // time is conflicting with other test session(s)
        } else if (response.status === 409) {
          this.setState({
            showTestSessionTimeOverlappingErrorPopup: true,
            isLoadingAddTestSession: false
          });
          // should never happen
        } else {
          throw new Error("An error occurred during the add non-standard test session process");
        }
      });
    });
  };

  openInvalidTestSessionDateAndTimeCombinationPopup = () => {
    this.setState({
      showInvalidTestSessionDateAndTimeCombinationPopup: true,
      isLoadingAddTestSession: false
    });
  };

  closeInvalidTestSessionDateAndTimeCombinationPopup = () => {
    this.setState({ showInvalidTestSessionDateAndTimeCombinationPopup: false });
  };

  render() {
    const TABS = this.getTABS();

    const columnsDefinition = [
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.accommodationRequests
            .topTabs.pendingRequests.table.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.accommodationRequests
            .topTabs.pendingRequests.table.columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.accommodationRequests
            .topTabs.pendingRequests.table.columnThree,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.accommodationRequests
            .topTabs.pendingRequests.table.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.accommodationRequests
            .topTabs.pendingRequests.table.columnFive,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.accommodationRequests
            .topTabs.pendingRequests.table.columnSix,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"accommodation-requests-pending-requests"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchTestCenterAccommodationRequestsStates}
            updatePageState={this.props.updateCurrentTestCenterAccommodationRequestsPageState}
            paginationPageSize={Number(
              this.props.testCenterAccommodationRequestsPaginationPageSize
            )}
            updatePaginationPageSize={this.props.updateTestCenterAccommodationRequestsPageSizeState}
            data={this.state.pendingRequests}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.testCenterAccommodationRequestsKeyword}
          />
          <div>
            <GenericTable
              classnamePrefix="accommodation-requests-pending-requests"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .accommodationRequests.topTabs.pendingRequests.table.noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <Pagination
            paginationContainerId={"accommodation-requests-pending-requests-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.testCenterAccommodationRequestsPaginationPage}
            updatePaginationPageState={
              this.props.updateCurrentTestCenterAccommodationRequestsPageState
            }
            firstTableRowId={"accommodation-requests-pending-requests-table-row-0"}
          />
        </div>
        <PopupBox
          show={this.state.showAddTestSessionPopup}
          handleClose={() => {}}
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
              .testSessionsData.addTestSessionPopup.title
          }
          description={
            <div>
              <p>
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                    .topTabs.testSessionsData.addTestSessionPopup.description
                }
              </p>
              <TopTabs
                TABS={TABS}
                defaultTab={`test-session-1`}
                customStyle={styles.contentTab}
                containsAddTabButton={true}
                addTabButtonAction={this.handleAddTestSessionTabPopup}
                tabsCustomStyle={styles.tabNavigation}
                tabClassName={"top-tabs-transparent-tab-style"}
                tabCustomStyle={styles.tabContainer}
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeAddTestSessionPopup}
          leftButtonState={
            !this.state.isLoadingAddTestSession ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            this.state.isLoadingAddTestSession ? (
              <div style={styles.customLoadingContainer}>
                {/* this div is useful to get the right size of the button while loading */}
                <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
                  <FontAwesomeIcon icon={faPlusCircle} style={styles.icon} />
                  {
                    LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                      .topTabs.testSessionsData.addTestSessionPopup.addTestSessionButton
                  }
                </div>
                <div style={styles.loadingOverlappingStyle}>
                  <label className="fa fa-spinner fa-spin">
                    <FontAwesomeIcon icon={faSpinner} />
                  </label>
                </div>
              </div>
            ) : (
              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                .topTabs.testSessionsData.addTestSessionPopup.addTestSessionButton
            )
          }
          rightButtonAction={
            !this.state.isLoadingAddTestSession ? this.handleAddTestSession : () => {}
          }
          rightButtonIcon={!this.state.isLoadingAddTestSession ? faPlusCircle : ""}
          rightButtonState={
            this.state.isLoadingAddTestSession
              ? BUTTON_STATE.disabled
              : this.props.testCenterNonStandardTestSessionValidFormState
                ? BUTTON_STATE.enabled
                : BUTTON_STATE.disabled
          }
        />
        <PopupBox
          show={this.state.showTestSessionTimeOverlappingErrorPopup}
          handleClose={() => {}}
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
              .testSessionsData.timeOverlappingErrorPopup.title
          }
          size="lg"
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.error}
                message={
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.timeOverlappingErrorPopup
                        .systemMessageDescription
                    }
                  </p>
                }
              />
              <p>
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                    .topTabs.testSessionsData.timeOverlappingErrorPopup.description
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonAction={this.closeTimeOverlappingErrorPopup}
          rightButtonIcon={faTimes}
        />
        {Object.keys(this.state.selectedAccommodationRequestData).length > 0 &&
          this.state.showAccommodationsRequestDetailsPopup && (
            <AccommodationsRequestDetailsPopup
              source={ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.tcm}
              userAccommodationFileId={this.state.selectedAccommodationRequestData.id}
              selectedUserAccommodationFileData={this.state.selectedAccommodationRequestData}
              showPopup={this.state.showAccommodationsRequestDetailsPopup}
              closeAccommodationsRequestDetailsPopup={this.closeAccommodationsRequestDetailsPopup}
              // at this point, the report should be completed, so no draft view for this one
              isDraft={false}
            />
          )}
        <PopupBox
          show={this.state.showDeleteTestSessionTabPopup}
          handleClose={() => {}}
          size="lg"
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
              .testSessionsData.addTestSessionPopup.deleteTestSessionTabPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.addTestSessionPopup
                        .deleteTestSessionTabPopup.systemMessageDescription
                    }
                  </p>
                }
              />
              <p>
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                    .topTabs.testSessionsData.addTestSessionPopup.deleteTestSessionTabPopup
                    .description
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeDeleteTestSessionTabPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.handleDeleteTestSessionTab}
          rightButtonIcon={faTrashAlt}
        />
        <PopupBox
          show={this.state.showInvalidTestSessionDateAndTimeCombinationPopup}
          handleClose={() => {}}
          size="lg"
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
              .testSessionsData.addTestSessionPopup.invalidTestSessionDateAndTimeCombinationPopup
              .title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.addTestSessionPopup
                        .invalidTestSessionDateAndTimeCombinationPopup.systemMessageDescription
                    }
                  </p>
                }
              />
              <p>
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                    .topTabs.testSessionsData.addTestSessionPopup
                    .invalidTestSessionDateAndTimeCombinationPopup.description
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonAction={this.closeInvalidTestSessionDateAndTimeCombinationPopup}
          rightButtonIcon={faTimes}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testCenterData: state.testCenter.testCenterData,
    testCenterAccommodationRequestsPaginationPage:
      state.testCenter.testCenterAccommodationRequestsPaginationPage,
    testCenterAccommodationRequestsPaginationPageSize:
      state.testCenter.testCenterAccommodationRequestsPaginationPageSize,
    testCenterAccommodationRequestsKeyword: state.testCenter.testCenterAccommodationRequestsKeyword,
    testCenterAccommodationRequestsActiveSearch:
      state.testCenter.testCenterAccommodationRequestsActiveSearch,
    completeDatePicked: state.datePicker.completeDatePicked,
    completeDateValidState: state.datePicker.completeDateValidState,
    triggerTableUpdatesState: state.testCenter.triggerTableUpdatesState,
    testCenterNonStandardTestSessionData: state.testCenter.testCenterNonStandardTestSessionData,
    testCenterNonStandardTestSessionValidFormState:
      state.testCenter.testCenterNonStandardTestSessionValidFormState
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateCurrentTestCenterAccommodationRequestsPageState,
      updateTestCenterAccommodationRequestsPageSizeState,
      updateSearchTestCenterAccommodationRequestsStates,
      getAllPendingAccommodationFilesAsATcm,
      getFoundPendingAccommodationFilesAsATcm,
      addNonStandardTestSessionToTestCenter,
      triggerTableUpdates,
      updateTestSessionNonStandardTestSessionData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(PendingRequests);
