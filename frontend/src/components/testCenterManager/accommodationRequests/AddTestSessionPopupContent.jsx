/* eslint-disable prefer-destructuring */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Col } from "react-bootstrap";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import DropdownSelect from "../../commons/DropdownSelect";
import DatePicker, { validateDatePicked } from "../../commons/DatePicker";
import * as _ from "lodash";
import populateCustomFutureYearsDateOptions, {
  populateTimeHourOptions,
  populateTimeMinOptions
} from "../../../helpers/populateCustomDatePickerOptions";
import moment from "moment";
import {
  updateTestSessionNonStandardTestSessionData,
  updateTestSessionNonStandardTestSessionValidFormState
} from "../../../modules/TestCenterRedux";
import { TestSkillSleDescCodename } from "../../testFactory/Constants";
import { getUsersBasedOnSpecifiedPermission } from "../../../modules/PermissionsRedux";
import { PERMISSION } from "../../profile/Constants";
import { convertDateTimeToUsersTimezone } from "../../../modules/helpers";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  },
  secondColumnTime: {
    xs: 6,
    sm: 6,
    md: 6,
    lg: 2,
    xl: 2
  }
};

const styles = {
  mainContainer: {
    border: "1px solid #00565e",
    padding: 12,
    backgroundColor: "#F3F3F3"
  },
  rowContainer: {
    margin: "12px 48px"
  },
  rowContainerWithError: {
    margin: "0 48px"
  },
  inputTitles: {
    fontWeight: "bold"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "6px 0 12px 0"
  }
};

export function getIsPastTestSessionDisabledState(testSessionData) {
  // initializing dateFormat
  const dateFormat = "YYYY-MM-DD HH:mm";
  // getting current datetime (local timezone)
  const currentDatetimeUtc = moment().format();
  const utcToLocalConvertedObj = convertDateTimeToUsersTimezone(currentDatetimeUtc);
  const currentDatetimeLocal = moment(
    `${utcToLocalConvertedObj.adjustedDate} ${utcToLocalConvertedObj.adjustedTime}`,
    dateFormat
  );
  // formatting test session datetime (str provided from testCenterNonStandardTestSessionData props)
  const formattedTestSessionDatetime = moment(
    `${testSessionData.date} ${testSessionData.endTimeHour.value}:${testSessionData.endTimeMinute.value}`,
    dateFormat
  );
  // return true if formattedTestSessionDatetime is before currentDatetimeLocal, else false
  return formattedTestSessionDatetime.isBefore(currentDatetimeLocal);
}

class AddTestSessionPopupContent extends Component {
  constructor(props) {
    super(props);

    props.testCenterNonStandardTestSessionData.forEach((data, index) => {
      this[`testSessionDateDayFieldRef_${index}`] = React.createRef();
      this[`testSessionDateMonthFieldRef_${index}`] = React.createRef();
      this[`testSessionDateYearFieldRef_${index}`] = React.createRef();
    });

    this.PropTypes = {
      indexOfCurrentSelectedTab: PropTypes.number.isRequired,
      selectedTestSessionData: PropTypes.object.isRequired
    };
  }

  state = {
    roomOptions: [],
    date: "",
    isValidDate: true,
    isValidFutureDate: true,
    hourOptions: populateTimeHourOptions(),
    minuteOptions: populateTimeMinOptions(),
    showTestSessionTimeOverlappingErrorPopup: false,
    isValidStartEndTimeCombination: true,
    testAssessorOptions: [],
    isDisabledForm: false
  };

  componentDidMount = () => {
    // populating room options
    this.populateRoomOptions();
    // validating form
    this.validateForm();
    // if ORAL test
    if (
      this.props.selectedTestSessionData.test_skill_sub_type_codename ===
        TestSkillSleDescCodename.ORAL_EN ||
      this.props.selectedTestSessionData.test_skill_sub_type_codename ===
        TestSkillSleDescCodename.ORAL_FR
    ) {
      // populating test assessor options
      this.populateTestAssessorOptions();
    }
    // gettiing/setting isDisabledForm
    this.setState({
      isDisabledForm: getIsPastTestSessionDisabledState(
        this.props.testCenterNonStandardTestSessionData[this.props.indexOfCurrentSelectedTab]
      )
    });
    setTimeout(() => {
      // calling handleDateUpdates in order to properly set the date states
      this.handleDateUpdates();
    }, 100);
  };

  componentDidUpdate = prevProps => {
    // if testCenterNonStandardTestSessionData gets updated
    if (
      prevProps.testCenterNonStandardTestSessionData !==
      this.props.testCenterNonStandardTestSessionData
    ) {
      // validating form
      this.validateForm();
    }
  };

  // populate room options
  populateRoomOptions = () => {
    const roomOptions = [];
    // populating "select" option
    roomOptions.push({
      value: null,
      label: LOCALIZE.dropdownSelect.pleaseSelect
    });
    for (let i = 0; i < this.props.testCenterData.rooms.length; i++) {
      // room of current iteration is active
      if (this.props.testCenterData.rooms[i].active) {
        // populating testAdministratorOptions
        roomOptions.push({
          label: `${this.props.testCenterData.rooms[i].name} [${LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs.testSessionsData.addTestSessionPopup.roomCapacity} ${this.props.testCenterData.rooms[i].max_occupancy}]`,
          value: this.props.testCenterData.rooms[i].id
        });
      }
    }
    this.setState({ roomOptions: roomOptions });
  };

  getSelectedRoomOption = selectedOption => {
    // creating a copy of the testCenterNonStandardTestSessionData redux state
    const copyOfTestCenterNonStandardTestSessionData = _.cloneDeep(
      this.props.testCenterNonStandardTestSessionData
    );
    // updating copy obj
    copyOfTestCenterNonStandardTestSessionData[
      this.props.indexOfCurrentSelectedTab
    ].roomSelectedOption = selectedOption;
    // updating redux states
    this.props.updateTestSessionNonStandardTestSessionData(
      copyOfTestCenterNonStandardTestSessionData
    );
    // populating room options
    this.populateRoomOptions();
  };

  handleDateUpdates = () => {
    // getting today's date
    const today = new Date().toISOString().slice(0, 10);
    // date is fully provided
    if (
      this[`testSessionDateYearFieldRef_${this.props.indexOfCurrentSelectedTab}`].current.props
        .value !== "" &&
      this[`testSessionDateMonthFieldRef_${this.props.indexOfCurrentSelectedTab}`].current.props
        .value !== "" &&
      this[`testSessionDateDayFieldRef_${this.props.indexOfCurrentSelectedTab}`].current.props
        .value !== ""
    ) {
      // formatting date based on refs
      const formattedDate = `${
        this[`testSessionDateYearFieldRef_${this.props.indexOfCurrentSelectedTab}`].current.props
          .value.value
      }-${
        this[`testSessionDateMonthFieldRef_${this.props.indexOfCurrentSelectedTab}`].current.props
          .value.value
      }-${
        this[`testSessionDateDayFieldRef_${this.props.indexOfCurrentSelectedTab}`].current.props
          .value.value
      }`;
      // creating a copy of the testCenterNonStandardTestSessionData redux state
      const copyOfTestCenterNonStandardTestSessionData = _.cloneDeep(
        this.props.testCenterNonStandardTestSessionData
      );
      // updating copy obj
      copyOfTestCenterNonStandardTestSessionData[this.props.indexOfCurrentSelectedTab].date =
        formattedDate;
      copyOfTestCenterNonStandardTestSessionData[this.props.indexOfCurrentSelectedTab].isValidDate =
        !this.state.isDisabledForm ? validateDatePicked(formattedDate) : true;
      copyOfTestCenterNonStandardTestSessionData[
        this.props.indexOfCurrentSelectedTab
      ].isValidFutureDate = !this.state.isDisabledForm
        ? moment(formattedDate).isSameOrAfter(moment(today))
        : true;
      // updating redux states
      this.props.updateTestSessionNonStandardTestSessionData(
        copyOfTestCenterNonStandardTestSessionData
      );
    }
  };

  getSelectedStartTimeHour = selectedOption => {
    // creating a copy of the testCenterNonStandardTestSessionData redux state
    const copyOfTestCenterNonStandardTestSessionData = _.cloneDeep(
      this.props.testCenterNonStandardTestSessionData
    );
    // updating copy obj
    copyOfTestCenterNonStandardTestSessionData[this.props.indexOfCurrentSelectedTab].startTimeHour =
      selectedOption;
    // updating redux states
    this.props.updateTestSessionNonStandardTestSessionData(
      copyOfTestCenterNonStandardTestSessionData
    );
  };

  getSelectedStartTimeMinute = selectedOption => {
    // creating a copy of the testCenterNonStandardTestSessionData redux state
    const copyOfTestCenterNonStandardTestSessionData = _.cloneDeep(
      this.props.testCenterNonStandardTestSessionData
    );
    // updating copy obj
    copyOfTestCenterNonStandardTestSessionData[
      this.props.indexOfCurrentSelectedTab
    ].startTimeMinute = selectedOption;
    // updating redux states
    this.props.updateTestSessionNonStandardTestSessionData(
      copyOfTestCenterNonStandardTestSessionData
    );
  };

  getSelectedEndTimeHour = selectedOption => {
    // creating a copy of the testCenterNonStandardTestSessionData redux state
    const copyOfTestCenterNonStandardTestSessionData = _.cloneDeep(
      this.props.testCenterNonStandardTestSessionData
    );
    // updating copy obj
    copyOfTestCenterNonStandardTestSessionData[this.props.indexOfCurrentSelectedTab].endTimeHour =
      selectedOption;
    // updating redux states
    this.props.updateTestSessionNonStandardTestSessionData(
      copyOfTestCenterNonStandardTestSessionData
    );
  };

  getSelectedEndTimeMinute = selectedOption => {
    // creating a copy of the testCenterNonStandardTestSessionData redux state
    const copyOfTestCenterNonStandardTestSessionData = _.cloneDeep(
      this.props.testCenterNonStandardTestSessionData
    );
    // updating copy obj
    copyOfTestCenterNonStandardTestSessionData[this.props.indexOfCurrentSelectedTab].endTimeMinute =
      selectedOption;
    // updating redux states
    this.props.updateTestSessionNonStandardTestSessionData(
      copyOfTestCenterNonStandardTestSessionData
    );
  };

  populateTestAssessorOptions = () => {
    // initializing testAssessorOptions
    const testAssessorOptions = [];
    // getting all existing test assessors (scorers)
    this.props.getUsersBasedOnSpecifiedPermission(PERMISSION.scorer).then(response => {
      for (let i = 0; i < response.length; i++) {
        // populating testAdministratorOptions
        testAssessorOptions.push({
          label: `${response[i].first_name} ${response[i].last_name} (${response[i].email})`,
          value: response[i].user_id
        });
      }
      this.setState({ testAssessorOptions: testAssessorOptions });
    });
  };

  getSelectedTestAssessor = selectedOption => {
    // creating a copy of the testCenterNonStandardTestSessionData redux state
    const copyOfTestCenterNonStandardTestSessionData = _.cloneDeep(
      this.props.testCenterNonStandardTestSessionData
    );
    // updating copy obj
    copyOfTestCenterNonStandardTestSessionData[
      this.props.indexOfCurrentSelectedTab
    ].testAssessorSelectedOption = selectedOption;
    // updating redux states
    this.props.updateTestSessionNonStandardTestSessionData(
      copyOfTestCenterNonStandardTestSessionData
    );
  };

  validateForm = () => {
    // initializing needed variables
    let isValidForm = true;
    // looping in testCenterNonStandardTestSessionData
    for (let i = 0; i < this.props.testCenterNonStandardTestSessionData.length; i++) {
      // initializing isValidDate and isValidFutureDate
      let isValidDate = true;
      let isValidFutureDate = true;
      let isValidStartTime = true;
      let isValidEndTime = true;
      let isValidStartEndTimeCombination = true;
      // fields are still enabled (end time has not past yet)
      if (!this.state.isDisabledForm) {
        // date is fully provided
        if (this.props.testCenterNonStandardTestSessionData[i].date !== "") {
          // validating date
          isValidDate = this.props.testCenterNonStandardTestSessionData[i].isValidDate;
          isValidFutureDate = this.props.testCenterNonStandardTestSessionData[i].isValidFutureDate;
        }

        isValidStartTime =
          this.props.testCenterNonStandardTestSessionData[i].startTimeHour !== "" &&
          this.props.testCenterNonStandardTestSessionData[i].startTimeMinute !== "";
        isValidEndTime =
          this.props.testCenterNonStandardTestSessionData[i].endTimeHour !== "" &&
          this.props.testCenterNonStandardTestSessionData[i].endTimeMinute !== "";
        // start time and end time are provided
        if (isValidStartTime && isValidEndTime) {
          const formattedStartTime = moment(
            `${this.props.testCenterNonStandardTestSessionData[i].startTimeHour.label}:${this.props.testCenterNonStandardTestSessionData[i].startTimeMinute.label}`,
            "HH:mm"
          );
          const formattedEndTime = moment(
            `${this.props.testCenterNonStandardTestSessionData[i].endTimeHour.label}:${this.props.testCenterNonStandardTestSessionData[i].endTimeMinute.label}`,
            "HH:mm"
          );
          // validating times (making sure that the start date is before the end date)
          isValidStartEndTimeCombination = formattedStartTime.isBefore(formattedEndTime);
        }
      }
      let isValidTestAssessor = true;
      // if ORAL test
      if (
        this.props.selectedTestSessionData.test_skill_sub_type_codename ===
          TestSkillSleDescCodename.ORAL_EN ||
        this.props.selectedTestSessionData.test_skill_sub_type_codename ===
          TestSkillSleDescCodename.ORAL_FR
      ) {
        isValidTestAssessor =
          typeof this.props.testCenterNonStandardTestSessionData[i].testAssessorSelectedOption !==
            "undefined" &&
          Object.keys(this.props.testCenterNonStandardTestSessionData[i].testAssessorSelectedOption)
            .length > 0;
      }

      // if index of current iteration matches the indexOfCurrentSelectedTab
      if (i === this.props.indexOfCurrentSelectedTab) {
        this.setState({
          isValidStartEndTimeCombination: isValidStartEndTimeCombination,
          isValidDate: isValidDate,
          isValidFutureDate: isValidFutureDate
        });
      }

      // if any of the mandatory fields is invalid
      if (
        !isValidDate ||
        !isValidFutureDate ||
        !isValidStartTime ||
        !isValidEndTime ||
        !isValidStartEndTimeCombination ||
        !isValidTestAssessor
      ) {
        // setting isValidForm to false
        isValidForm = false;
      }
    }
    // updating redux state
    this.props.updateTestSessionNonStandardTestSessionValidFormState(isValidForm);
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label
              id="pending-requests-add-test-sessions-popup-room-label"
              style={styles.inputTitles}
            >
              {
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                  .topTabs.testSessionsData.addTestSessionPopup.roomOptionalLabel
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
          >
            <DropdownSelect
              idPrefix="pending-requests-add-test-sessions-popup-room"
              isValid={true}
              ariaRequired={true}
              ariaLabelledBy="pending-requests-add-test-sessions-popup-room-label"
              hasPlaceholder={true}
              options={this.state.roomOptions}
              onChange={this.getSelectedRoomOption}
              defaultValue={
                this.props.testCenterNonStandardTestSessionData[
                  this.props.indexOfCurrentSelectedTab
                ].roomSelectedOption
              }
              orderByLabels={false}
              isDisabled={this.state.isDisabledForm}
            />
          </Col>
        </Row>
        <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label
              id="pending-requests-add-test-sessions-popup-date-label"
              style={styles.inputTitles}
            >
              {
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                  .topTabs.testSessionsData.addTestSessionPopup.dateLabel
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
          >
            <DatePicker
              dateDayFieldRef={
                this[`testSessionDateDayFieldRef_${this.props.indexOfCurrentSelectedTab}`]
              }
              dateMonthFieldRef={
                this[`testSessionDateMonthFieldRef_${this.props.indexOfCurrentSelectedTab}`]
              }
              dateYearFieldRef={
                this[`testSessionDateYearFieldRef_${this.props.indexOfCurrentSelectedTab}`]
              }
              dateLabelId={"pending-requests-add-test-sessions-popup-date-input"}
              ariaLabelledBy={
                "pending-requests-add-test-sessions-popup-date-label pending-requests-add-test-sessions-popup-date-picked-error"
              }
              initialDateDayValue={
                this.props.testCenterNonStandardTestSessionData[
                  this.props.indexOfCurrentSelectedTab
                ].date !== "" && {
                  label:
                    this.props.testCenterNonStandardTestSessionData[
                      this.props.indexOfCurrentSelectedTab
                    ].date.split("-")[2],
                  value:
                    this.props.testCenterNonStandardTestSessionData[
                      this.props.indexOfCurrentSelectedTab
                    ].date.split("-")[2]
                }
              }
              initialDateMonthValue={
                this.props.testCenterNonStandardTestSessionData[
                  this.props.indexOfCurrentSelectedTab
                ].date !== "" && {
                  label:
                    this.props.testCenterNonStandardTestSessionData[
                      this.props.indexOfCurrentSelectedTab
                    ].date.split("-")[1],
                  value:
                    this.props.testCenterNonStandardTestSessionData[
                      this.props.indexOfCurrentSelectedTab
                    ].date.split("-")[1]
                }
              }
              initialDateYearValue={
                this.props.testCenterNonStandardTestSessionData[
                  this.props.indexOfCurrentSelectedTab
                ].date !== "" && {
                  label:
                    this.props.testCenterNonStandardTestSessionData[
                      this.props.indexOfCurrentSelectedTab
                    ].date.split("-")[0],
                  value:
                    this.props.testCenterNonStandardTestSessionData[
                      this.props.indexOfCurrentSelectedTab
                    ].date.split("-")[0]
                }
              }
              onInputChange={this.handleDateUpdates}
              customYearOptions={populateCustomFutureYearsDateOptions(1)}
              isValidCompleteDatePicked={!this.state.isDisabledForm ? this.state.isValidDate : true}
              isValidFutureDatePicked={
                !this.state.isDisabledForm ? this.state.isValidFutureDate : true
              }
              disabledDropdowns={this.state.isDisabledForm}
            />
          </Col>
        </Row>
        <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label
              id="pending-requests-add-test-sessions-popup-start-time-label"
              style={styles.inputTitles}
            >
              {
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                  .topTabs.testSessionsData.addTestSessionPopup.startTimeLabel
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumnTime.xl}
            lg={columnSizes.secondColumnTime.lg}
            md={columnSizes.secondColumnTime.md}
            sm={columnSizes.secondColumnTime.sm}
            xs={columnSizes.secondColumnTime.xs}
          >
            <DropdownSelect
              idPrefix="pending-requests-add-test-sessions-popup-start-time-hour"
              isValid={this.state.isValidStartEndTimeCombination}
              ariaRequired={true}
              ariaLabelledBy="pending-requests-add-test-sessions-popup-start-time-label pending-requests-add-test-sessions-popup-start-time-hour-label pending-requests-add-test-sessions-popup-start-end-time-combination-error pending-requests-add-test-sessions-popup-start-date-and-time-based-on-booking-delay-error"
              hasPlaceholder={true}
              customPlaceholderWording={LOCALIZE.datePicker.hourField}
              options={this.state.hourOptions}
              onChange={this.getSelectedStartTimeHour}
              defaultValue={
                this.props.testCenterNonStandardTestSessionData[
                  this.props.indexOfCurrentSelectedTab
                ].startTimeHour
              }
              isDisabled={this.state.isDisabledForm}
            />
          </Col>
          <Col
            xl={columnSizes.secondColumnTime.xl}
            lg={columnSizes.secondColumnTime.lg}
            md={columnSizes.secondColumnTime.md}
            sm={columnSizes.secondColumnTime.sm}
            xs={columnSizes.secondColumnTime.xs}
          >
            <DropdownSelect
              idPrefix="pending-requests-add-test-sessions-popup-start-time-minute"
              isValid={this.state.isValidStartEndTimeCombination}
              ariaRequired={true}
              ariaLabelledBy="pending-requests-add-test-sessions-popup-start-time-label pending-requests-add-test-sessions-popup-start-time-minute-label pending-requests-add-test-sessions-popup-start-end-time-combination-error pending-requests-add-test-sessions-popup-start-date-and-time-based-on-booking-delay-error"
              hasPlaceholder={true}
              customPlaceholderWording={LOCALIZE.datePicker.minuteField}
              options={this.state.minuteOptions}
              onChange={this.getSelectedStartTimeMinute}
              defaultValue={
                this.props.testCenterNonStandardTestSessionData[
                  this.props.indexOfCurrentSelectedTab
                ].startTimeMinute
              }
              isDisabled={this.state.isDisabledForm}
            />
          </Col>
        </Row>
        <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            style={styles.centerText}
          />
          <Col
            xl={columnSizes.secondColumnTime.xl}
            lg={columnSizes.secondColumnTime.lg}
            md={columnSizes.secondColumnTime.md}
            sm={columnSizes.secondColumnTime.sm}
            xs={columnSizes.secondColumnTime.xs}
            style={styles.centerText}
          >
            <label id="pending-requests-add-test-sessions-popup-start-time-hour-label">
              {LOCALIZE.datePicker.hourField}
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumnTime.xl}
            lg={columnSizes.secondColumnTime.lg}
            md={columnSizes.secondColumnTime.md}
            sm={columnSizes.secondColumnTime.sm}
            xs={columnSizes.secondColumnTime.xs}
            style={styles.centerText}
          >
            <label id="pending-requests-add-test-sessions-popup-start-time-minute-label">
              {LOCALIZE.datePicker.minuteField}
            </label>
          </Col>
        </Row>
        <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label
              id="pending-requests-add-test-sessions-popup-end-time-label"
              style={styles.inputTitles}
            >
              {
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                  .topTabs.testSessionsData.addTestSessionPopup.endTimeLabel
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumnTime.xl}
            lg={columnSizes.secondColumnTime.lg}
            md={columnSizes.secondColumnTime.md}
            sm={columnSizes.secondColumnTime.sm}
            xs={columnSizes.secondColumnTime.xs}
          >
            <DropdownSelect
              idPrefix="pending-requests-add-test-sessions-popup-end-time-hour"
              isValid={this.state.isValidStartEndTimeCombination}
              ariaRequired={true}
              ariaLabelledBy="pending-requests-add-test-sessions-popup-end-time-label pending-requests-add-test-sessions-popup-end-time-hour-label pending-requests-add-test-sessions-popup-start-end-time-combination-error pending-requests-add-test-sessions-popup-start-date-and-time-based-on-booking-delay-error"
              hasPlaceholder={true}
              customPlaceholderWording={LOCALIZE.datePicker.hourField}
              options={this.state.hourOptions}
              onChange={this.getSelectedEndTimeHour}
              defaultValue={
                this.props.testCenterNonStandardTestSessionData[
                  this.props.indexOfCurrentSelectedTab
                ].endTimeHour
              }
              isDisabled={this.state.isDisabledForm}
            />
          </Col>
          <Col
            xl={columnSizes.secondColumnTime.xl}
            lg={columnSizes.secondColumnTime.lg}
            md={columnSizes.secondColumnTime.md}
            sm={columnSizes.secondColumnTime.sm}
            xs={columnSizes.secondColumnTime.xs}
          >
            <DropdownSelect
              idPrefix="pending-requests-add-test-sessions-popup-end-time-minute"
              isValid={this.state.isValidStartEndTimeCombination}
              ariaRequired={true}
              ariaLabelledBy="pending-requests-add-test-sessions-popup-end-time-label pending-requests-add-test-sessions-popup-end-time-minute-label pending-requests-add-test-sessions-popup-start-end-time-combination-error pending-requests-add-test-sessions-popup-start-date-and-time-based-on-booking-delay-error"
              hasPlaceholder={true}
              customPlaceholderWording={LOCALIZE.datePicker.minuteField}
              options={this.state.minuteOptions}
              onChange={this.getSelectedEndTimeMinute}
              defaultValue={
                this.props.testCenterNonStandardTestSessionData[
                  this.props.indexOfCurrentSelectedTab
                ].endTimeMinute
              }
              isDisabled={this.state.isDisabledForm}
            />
          </Col>
        </Row>
        <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
            style={styles.centerText}
          />
          <Col
            xl={columnSizes.secondColumnTime.xl}
            lg={columnSizes.secondColumnTime.lg}
            md={columnSizes.secondColumnTime.md}
            sm={columnSizes.secondColumnTime.sm}
            xs={columnSizes.secondColumnTime.xs}
            style={styles.centerText}
          >
            <label id="pending-requests-add-test-sessions-popup-end-time-hour-label">
              {LOCALIZE.datePicker.hourField}
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumnTime.xl}
            lg={columnSizes.secondColumnTime.lg}
            md={columnSizes.secondColumnTime.md}
            sm={columnSizes.secondColumnTime.sm}
            xs={columnSizes.secondColumnTime.xs}
            style={styles.centerText}
          >
            <label id="pending-requests-add-test-sessions-popup-end-time-minute-label">
              {LOCALIZE.datePicker.minuteField}
            </label>
          </Col>
        </Row>
        {!this.state.isValidStartEndTimeCombination && (
          <Row
            role="presentation"
            className="align-items-center justify-content-end"
            style={styles.rowContainerWithError}
          >
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <label
                id="pending-requests-add-test-sessions-popup-start-end-time-combination-error"
                style={styles.errorMessage}
              >
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                    .topTabs.testSessionsData.addTestSessionPopup.invalidStartEndTimeCombination
                }
              </label>
            </Col>
          </Row>
        )}
        <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label
              htmlFor="pending-requests-add-test-sessions-popup-test-input"
              style={styles.inputTitles}
            >
              {
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                  .topTabs.testSessionsData.addTestSessionPopup.test
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
          >
            <input
              id="pending-requests-add-test-sessions-popup-test-input"
              tabIndex={0}
              className={"valid-field input-disabled-look"}
              type="text"
              value={
                this.props.selectedTestSessionData.test_id !== null
                  ? `${
                      this.props.selectedTestSessionData[`test_name_${this.props.currentLanguage}`]
                    } - v${this.props.selectedTestSessionData.version}`
                  : this.props.selectedTestSessionData.specified_test_description
              }
              style={{
                ...styles.inputs,
                ...accommodationsStyle
              }}
              aria-disabled={true}
            />
          </Col>
        </Row>
        {(this.props.selectedTestSessionData.test_skill_sub_type_codename ===
          TestSkillSleDescCodename.ORAL_EN ||
          this.props.selectedTestSessionData.test_skill_sub_type_codename ===
            TestSkillSleDescCodename.ORAL_FR) && (
          <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            >
              <label
                id="pending-requests-add-test-sessions-popup-test-assessor-label"
                style={styles.inputTitles}
              >
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                    .topTabs.testSessionsData.addTestSessionPopup.testAssessor
                }
              </label>
            </Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <DropdownSelect
                idPrefix="pending-requests-add-test-sessions-popup-test-assessor"
                ariaRequired={true}
                ariaLabelledBy="pending-requests-add-test-sessions-popup-test-assessor-label"
                hasPlaceholder={true}
                options={this.state.testAssessorOptions}
                onChange={this.getSelectedTestAssessor}
                defaultValue={
                  this.props.testCenterNonStandardTestSessionData[
                    this.props.indexOfCurrentSelectedTab
                  ].testAssessorSelectedOption
                }
                menuPlacement={"top"}
                isDisabled={this.state.isDisabledForm}
              />
            </Col>
          </Row>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testCenterData: state.testCenter.testCenterData,
    testCenterAccommodationRequestsPaginationPage:
      state.testCenter.testCenterAccommodationRequestsPaginationPage,
    testCenterAccommodationRequestsPaginationPageSize:
      state.testCenter.testCenterAccommodationRequestsPaginationPageSize,
    testCenterAccommodationRequestsKeyword: state.testCenter.testCenterAccommodationRequestsKeyword,
    testCenterAccommodationRequestsActiveSearch:
      state.testCenter.testCenterAccommodationRequestsActiveSearch,
    testCenterNonStandardTestSessionData: state.testCenter.testCenterNonStandardTestSessionData,
    completeDatePicked: state.datePicker.completeDatePicked,
    completeDateValidState: state.datePicker.completeDateValidState,
    triggerTableUpdatesState: state.testCenter.triggerTableUpdatesState
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateTestSessionNonStandardTestSessionData,
      updateTestSessionNonStandardTestSessionValidFormState,
      getUsersBasedOnSpecifiedPermission
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AddTestSessionPopupContent);
