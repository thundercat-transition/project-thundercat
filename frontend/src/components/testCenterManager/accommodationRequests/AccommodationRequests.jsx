import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import PendingRequests from "./PendingRequests";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  }
};

class AccommodationRequests extends Component {
  render() {
    const TABS = [
      {
        key: "accommodations-pending-requests",
        tabName:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.accommodationRequests
            .topTabs.pendingRequests.title,
        body: <PendingRequests />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>
            {
              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                .accommodationRequests.title
            }
          </h2>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey="accommodations-pending-requests"
                id="test-sessions-tabs"
                style={styles.tabNavigation}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AccommodationRequests);
