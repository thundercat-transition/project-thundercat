import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { isTokenStillValid } from "../../modules/LoginRedux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import ContentContainer from "../commons/ContentContainer";
import "../../css/test-administration.css";
import { styles as SystemAdministrationStyles } from "../etta/SystemAdministration";
import { Button, Row, Col, Tab, Tabs } from "react-bootstrap";
import { history } from "../../store-index";
import { PATH } from "../commons/Constants";
import { resetTopTabsState, switchSideTab, switchTopTab } from "../../modules/NavTabsRedux";
import TopTabs from "../commons/TopTabs";
import { triggerAppRerender } from "../../modules/AppRedux";
import TestCenters from "./TestCenters";
import TestCentersOla from "./TestCentersOla";

const styles = {
  backButtonStyle: {
    marginBottom: 24
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    margin: "0px 15px 0px 15px",
    padding: "15px",
    borderColor: "#CECECE"
  },
  tabContent: {
    backgroundColor: "transparent",
    overflowY: "auto"
  },
  loadingLabel: {
    paddingTop: "20px"
  },
  buttonLabel: {
    marginLeft: 6
  },
  appPadding: {
    padding: "15px"
  },
  backgroundColor: {
    backgroundColor: "#F8F8F8"
  }
};

class TestCenterManager extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  state = {
    viewTestSectionOrderNumber: undefined,
    isTestDefinitionSelected: this.props.selectedTestDefinitionId !== null,
    isLoading: false,
    showBackToTestDefinitionSelectionPopup: false,
    displayAddressBookContacts: false,
    displayCompetencyTypes: false,
    triggerRerender: false,
    isTestCenterManagerDashboard: false
  };

  // calling redux state on page load to get the first name and last name
  componentDidMount = () => {
    this._isMounted = true;
    // to display last login if it is a Test Center Manager dashboard
    if (this.props.currentHomePage === PATH.testCenterManager) {
      this.setState({
        isTestCenterManagerDashboard: true
      });
    }
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        // populate test definition
        this.props.switchTopTab(100);
      }
    });
    // triggering app rerender to make sure that the special page height and overflow is properly set
    this.props.triggerAppRerender();
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        // if selectedTestDefinitionId gets updated
        if (prevProps.selectedTestDefinitionId !== this.props.selectedTestDefinitionId) {
          // if defined
          if (this.props.selectedTestDefinitionId !== null) {
            this.setState({
              isTestDefinitionSelected: true
            });
          }
        }
      }
    });
    // if triggerTestDefinitionLoading gets updated
    if (prevProps.triggerTestDefinitionLoading !== this.props.triggerTestDefinitionLoading) {
      this.populateTestDefinition();
    }
    // if triggerTestDefinitionLoadingFromJson gets updated
    if (
      prevProps.triggerTestDefinitionLoadingFromJson !==
      this.props.triggerTestDefinitionLoadingFromJson
    ) {
      this.populateTestDefinitionBasedOnJsonContent();
    }
    // if testSections gets updated
    if (prevProps.testSections !== this.props.testSections) {
      // getting dynamic side nav items
      this.getSideNavigationItemsToDisplay();
    }
    // if testSectionComponents gets updated
    if (prevProps.testSectionComponents !== this.props.testSectionComponents) {
      // getting dynamic side nav items
      this.getSideNavigationItemsToDisplay();
    }
    // if scoringMethodValidState gets updated
    if (prevProps.scoringMethodValidState !== this.props.scoringMethodValidState) {
      this.setState({ triggerRerender: !this.state.triggerRerender });
    }
  }

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  goToTestView = () => {
    this.props.setTestSectionToView(this.state.viewTestSectionOrderNumber);
    this.resetAllRedux();
    history.push(PATH.testBuilderTest);
  };

  resetAllRedux = () => {
    this.props.resetTestFactoryState();
    this.props.resetNotepadState();
    this.props.resetQuestionListState();
    this.props.resetTopTabsState();
  };

  selectTestSection = event => {
    this.setState({ viewTestSectionOrderNumber: event.value });
  };

  render() {
    const TABS = [];

    TABS.push(
      {
        key: "test-centers",
        tabName: LOCALIZE.testCenterManager.containerTestCentersLabel,
        body: (
          <div
            style={{
              ...SystemAdministrationStyles.sectionContainer,
              ...styles.appPadding,
              ...styles.backgroundColor
            }}
          >
            <TestCenters />
          </div>
        )
      },
      {
        key: "test-centers-ola",
        tabName: LOCALIZE.testCenterManager.containerOlaLabel,
        body: (
          <div
            style={{
              ...SystemAdministrationStyles.sectionContainer,
              ...styles.appPadding,
              ...styles.backgroundColor
            }}
          >
            <TestCentersOla />
          </div>
        )
      }
    );

    return (
      <div className="app" style={styles.appPadding}>
        {/* {this.state.isTestCenterManagerDashboard && (
          <LastLogin lastLoginDate={this.props.lastLogin} />
        )} */}
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title className="notranslate">{LOCALIZE.titles.testCenterManager}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <div
              id="user-welcome-message-div"
              style={styles.header}
              aria-labelledby="user-welcome-message"
              className="notranslate"
            >
              <h1 id="user-welcome-message" className="green-divider">
                {LOCALIZE.formatString(
                  LOCALIZE.profile.title,
                  this.props.firstName,
                  this.props.lastName
                )}
              </h1>
            </div>
            {!this.state.isLoading && (
              <Row>
                <Col>
                  <TopTabs
                    TABS={TABS}
                    setTab={this.props.setTestBuilderTopTab}
                    customCurrentTabProp={this.props.testBuilderTopTab}
                  />
                </Col>
              </Row>
            )}
            {this.state.isLoading && (
              <h2 style={styles.loadingLabel}>{LOCALIZE.commons.loading}</h2>
            )}
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { TestCenterManager as UnconnectedTestCenterManager };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    lastLogin: state.user.lastLogin,
    currentHomePage: state.userPermissions.currentHomePage
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid,
      resetTopTabsState,
      switchSideTab,
      switchTopTab,
      triggerAppRerender
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestCenterManager);
