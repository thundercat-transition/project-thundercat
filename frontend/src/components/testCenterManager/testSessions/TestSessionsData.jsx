/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-return-assign */
/* eslint-disable new-cap */
/* eslint-disable prefer-destructuring */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import LOCALIZE from "../../../text_resources";
import * as _ from "lodash";
import { bindActionCreators } from "redux";
import { Row, Col, Pagination } from "react-bootstrap";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faListCheck,
  faPencilAlt,
  faPlusCircle,
  faSave,
  faTimes,
  faTrashAlt,
  faPrint,
  faBan,
  faUniversalAccess,
  faFileCircleExclamation,
  faSpinner
} from "@fortawesome/free-solid-svg-icons";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import { getUsersBasedOnSpecifiedPermission } from "../../../modules/PermissionsRedux";
import {
  associateTestAdministratorsToTestCenter,
  getAssociatedTestAdministratorsToTestCenter,
  getFoundAssociatedTestAdministratorsToTestCenter,
  deleteTestAdministratorFromTestCenter,
  updateCurrentTestCenterTestSessionsPageState,
  updateTestCenterTestSessionsPageSizeState,
  updateSearchTestCenterTestSessionsStates,
  getFoundTestCenterTestSessions,
  deleteTestCenterTestSession,
  editTestCenterTestSession,
  addTestSessionToTestCenter,
  getTestCenterTestSessions,
  getTestSessionAttendees,
  triggerTableUpdates,
  getDataOfTestCenterTestSessionToViewOrEdit,
  updateTestSessionNonStandardTestSessionData,
  editNonStandardTestCenterTestSession
} from "../../../modules/TestCenterRedux";
import StyledTooltip, { EFFECT, TYPE } from "../../authentication/StyledTooltip";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import Switch from "react-switch";
import getSwitchTransformScale from "../../../helpers/switchTransformScale";
import DropdownSelect from "../../commons/DropdownSelect";
import DatePicker from "../../commons/DatePicker";
import populateCustomFutureYearsDateOptions, {
  populateTimeHourOptions,
  populateTimeMinOptions
} from "../../../helpers/populateCustomDatePickerOptions";
import { updateDatePicked, updateDatePickedValidState } from "../../../modules/DatePickerRedux";
import moment from "moment";
import getFormattedDate from "../../../helpers/getFormattedDate";
import { getTestSkillSubTypes, getTestSkillTypes } from "../../../modules/TestBuilderRedux";
import getContentToPrint from "../../../helpers/testSessionAttendees";
import { TestSkillTypeCodename } from "../../testFactory/Constants";
import {
  convertDateTimeToUsersTimezone,
  convertUsersTimezoneDateTimeToUtc
} from "../../../modules/helpers";
import AccommodationsRequestDetailsPopup, {
  ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE
} from "../../aae/AccommodationsRequestDetailsPopup";
import TopTabs from "../../commons/TopTabs";
import "../../../css/top-tabs.css";
import AddTestSessionPopupContent, {
  getIsPastTestSessionDisabledState
} from "../accommodationRequests/AddTestSessionPopupContent";
import { TEST_SESSION_OBJ_TEMPLATE } from "../accommodationRequests/PendingRequests";
import ReactToPrint from "../../commons/ReactToPrint";

// mirror of TEST_SESSION_DATA_SOURCE (...\backend\backend\views\utils.py)
export const TEST_SESSION_DATA_SOURCE = {
  standard: "standard",
  nonStandard: "non-standard"
};

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 6,
    xl: 6
  },
  secondColumnTime: {
    xs: 6,
    sm: 6,
    md: 6,
    lg: 2,
    xl: 2
  }
};

const styles = {
  mainContainer: {
    margin: "24px 0"
  },
  rowContainer: {
    margin: "12px 48px"
  },
  rowContainerWithError: {
    margin: "0 48px"
  },
  inputTitles: {
    fontWeight: "bold"
  },
  buttonIcon: {
    marginRight: 6
  },
  tableIcon: {
    transform: "scale(1.2)"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  noPaddingTopBottom: {
    paddingTop: 0,
    paddingBottom: 0
  },
  allUnset: {
    all: "unset"
  },
  boldText: {
    fontWeight: "bold"
  },
  centerText: {
    textAlign: "center"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "6px 0 12px 0"
  },
  contentTab: {
    paddingLeft: 3,
    paddingRight: 3
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2
  },
  icon: {
    paddingRight: 6,
    verticalAlign: "middle"
  },
  transparentText: {
    color: "transparent"
  },
  customLoadingContainer: {
    display: "grid",
    gridTemplateColumns: "1fr"
  },
  loadingOverlappingStyle: {
    gridRowStart: "1",
    gridColumnStart: "1"
  }
};

class TestSessionsData extends Component {
  static propTypes = {
    source: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.testSessionDateDayFieldRef = React.createRef();
    this.componentToPrintRef = React.createRef();
  }

  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    resultsFound: 0,
    testSesions: [],
    clearSearchTriggered: false,
    showTestSessionAttendeesPopup: false,
    testSessionAttendeesLoading: false,
    testSessionAttendeesRowsDefinition: {},
    triggerPrint: false,
    showAddTestSessionPopup: false,
    roomOptions: [],
    roomSelectedOption: {},
    openToOgdSwitchState: false,
    triggerDateValidation: false,
    hourOptions: populateTimeHourOptions(),
    minuteOptions: populateTimeMinOptions(),
    startTimeHour: "",
    startTimeMinute: "",
    endTimeHour: "",
    endTimeMinute: "",
    isValidStartEndTimeCombination: true,
    isValidTimeAndDateBasedOnBookingDelay: true,
    isValidDatePickedBasedOnClosingDate: true,
    spacesAvailable: "",
    isValidSpacesAvailable: true,
    testSkillTypeOptions: [],
    testSkillTypeSelectedOption: [],
    testSkillSubTypeOptions: [],
    testSkillSubTypeSelectedOption: [],
    isValidForm: false,
    showEditTestSessionPopup: false,
    conditionalContentTabs: [],
    isLoadingEditTestSession: false,
    showInvalidTestSessionDateAndTimeCombinationPopup: false,
    testSessionUpdatedSuccessfully: false,
    showTestSessionTimeOverlappingErrorPopup: false,
    showDeleteTestSessionPopup: false,
    isLoadingDeleteTestSession: false,
    selectedTestSessionToViewEditOrDelete: {},
    showAccommodationsRequestDetailsPopup: false,
    selectedAccommodationRequestData: {},
    // default values (switch style)
    isLoadingSwitch: false,
    switchHeight: 25,
    switchWidth: 50,
    triggerReactToPrint: false
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentTestCenterTestSessionsPageState(1);
    // populating associated test administrators
    this.populateTestSessionsBasedOnActiveSearch();
    // poplating test skill type options
    this.populateTestSkillTypeOptions();
    // getting updated switch dimensions
    this.getUpdatedSwitchDimensions();
    // initializing completeDatePicked and completeDateValidState redux states
    this.props.updateDatePicked(`${null}-${null}-${null}`);
    this.props.updateDatePickedValidState(true);
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateTestSessionsBasedOnActiveSearch();
    }
    // if testCenterTestSessionsPaginationPage gets updated
    if (
      prevProps.testCenterTestSessionsPaginationPage !==
      this.props.testCenterTestSessionsPaginationPage
    ) {
      this.populateTestSessionsBasedOnActiveSearch();
    }
    // if testCenterTestSessionsPaginationPageSize get updated
    if (
      prevProps.testCenterTestSessionsPaginationPageSize !==
      this.props.testCenterTestSessionsPaginationPageSize
    ) {
      this.populateTestSessionsBasedOnActiveSearch();
    }
    // if search testCenterTestSessionsKeyword gets updated
    if (prevProps.testCenterTestSessionsKeyword !== this.props.testCenterTestSessionsKeyword) {
      // if testCenterTestSessionsKeyword redux state is empty
      if (this.props.testCenterTestSessionsKeyword !== "") {
        this.populateFoundTestSessions();
      } else if (
        this.props.testCenterTestSessionsKeyword === "" &&
        this.props.testCenterTestSessionsActiveSearch
      ) {
        this.populateFoundTestSessions();
      }
    }
    // if testCenterTestSessionsActiveSearch gets updated
    if (
      prevProps.testCenterTestSessionsActiveSearch !== this.props.testCenterTestSessionsActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.testCenterTestSessionsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateTestSessions();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundTestSessions();
      }
    }
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
    // if completeDatePicked gets updated
    if (
      prevProps.completeDatePicked !== this.props.completeDatePicked ||
      prevProps.completeDateValidState !== this.props.completeDateValidState
    ) {
      // validating form
      this.validateForm();
    }
    // if triggerTableUpdatesState gets updated
    if (prevProps.triggerTableUpdatesState !== this.props.triggerTableUpdatesState) {
      this.populateTestSessionsBasedOnActiveSearch();
    }
  };

  getTABS = testSessionData => {
    const conditionalContentTabs = [];
    // looping in testSessionData array
    for (let i = 0; i < testSessionData.length; i++) {
      // first tab
      if (i === 0) {
        conditionalContentTabs.push({
          key: `test-session-${i + 1}`,
          tabName: LOCALIZE.formatString(
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
              .testSessionsData.addTestSessionPopup.tabName,
            i + 1
          ),
          body: (
            <AddTestSessionPopupContent
              indexOfCurrentSelectedTab={i}
              selectedTestSessionData={testSessionData[i]}
            />
          )
        });
      } else {
        conditionalContentTabs.push({
          key: `test-session-${i + 1}`,
          tabName: (
            <>
              <span>
                {LOCALIZE.formatString(
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                    .topTabs.testSessionsData.addTestSessionPopup.tabName,
                  i + 1
                )}
              </span>
              <StyledTooltip
                id={`delete-testing-${i + 1}-tooltip`}
                place="top"
                type={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`delete-testing-${i + 1}-tooltip`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faTrashAlt} />
                        </>
                      }
                      action={() => this.openDeleteTestSessionTabPopup(i)}
                      customStyle={{ ...styles.actionButton, ...styles.noPaddingTopBottom }}
                      buttonTheme={THEME.SECONDARY}
                      disabled={getIsPastTestSessionDisabledState(testSessionData[i])}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>{LOCALIZE.commons.deleteButton}</p>
                  </div>
                }
              />
            </>
          ),
          body: (
            <AddTestSessionPopupContent
              indexOfCurrentSelectedTab={i}
              selectedTestSessionData={testSessionData[i]}
            />
          )
        });
      }
    }
    return conditionalContentTabs;
  };

  populateTestSessionsBasedOnActiveSearch = () => {
    // current search
    if (this.props.testCenterTestSessionsActiveSearch) {
      this.populateFoundTestSessions();
      // no search
    } else {
      this.populateTestSessions();
    }
  };

  // populating associated test center test administrators
  populateTestSessions = () => {
    this.setState({ currentlyLoading: true }, () => {
      const testSessionsArray = [];
      this.props
        .getTestCenterTestSessions(
          this.props.testCenterTestSessionsPaginationPage,
          this.props.testCenterTestSessionsPaginationPageSize,
          this.props.testCenterData.id,
          this.props.source
        )
        .then(response => {
          this.populateTestCenterTestSessionsObject(testSessionsArray, response);
        })
        .then(() => {
          if (this.state.clearSearchTriggered) {
            // go back to the first page to avoid display bugs
            this.props.updateCurrentTestCenterTestSessionsPageState(1);
            this.setState({ clearSearchTriggered: false });
          }
        })
        .then(() => {
          this.setState({ currentlyLoading: false });
        });
    });
  };

  // populating found associated test center test administrators
  populateFoundTestSessions = () => {
    // making sure that the table is not currently loading
    if (!this.state.currentlyLoading) {
      this.setState(
        { currentlyLoading: true },
        () => {
          const testSessionsArray = [];
          setTimeout(() => {
            this.props
              .getFoundTestCenterTestSessions(
                this.props.testCenterTestSessionsKeyword,
                this.props.currentLanguage,
                this.props.testCenterTestSessionsPaginationPage,
                this.props.testCenterTestSessionsPaginationPageSize,
                this.props.testCenterData.id,
                this.props.source
              )
              .then(response => {
                // there are no results found
                if (response[0] === "no results found") {
                  this.setState({
                    testSesions: [],
                    rowsDefinition: {},
                    numberOfPages: 1,
                    nextPageNumber: 0,
                    previousPageNumber: 0,
                    resultsFound: 0
                  });
                  // there is at least one result found
                } else {
                  this.populateTestCenterTestSessionsObject(testSessionsArray, response);
                  this.setState({ resultsFound: response.count });
                }
              })
              .then(() => {
                this.setState(
                  {
                    currentlyLoading: false
                  },
                  () => {
                    // make sure that this element exsits to avoid any error
                    if (document.getElementById("test-center-rooms-results-found")) {
                      document.getElementById("test-center-rooms-results-found").focus();
                    }
                  }
                );
              });
          });
        },
        250
      );
    }
  };

  populateTestCenterTestSessionsObject = (testSessionsArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];

        // eslint-disable-next-line no-param-reassign, prefer-destructuring
        currentResult.simplified_start_time = convertDateTimeToUsersTimezone(
          currentResult.utc_start_time
        ).adjustedTime;
        // eslint-disable-next-line no-param-reassign, prefer-destructuring
        currentResult.simplified_end_time = convertDateTimeToUsersTimezone(
          currentResult.utc_end_time
        ).adjustedTime;

        // formatting needed data
        const formattedDate = `${currentResult.date} ${convertDateTimeToUsersTimezone(currentResult.utc_start_time).adjustedTime} - ${convertDateTimeToUsersTimezone(currentResult.utc_end_time).adjustedTime}`;
        let formattedTestSkill = `${
          currentResult[`test_skill_type_${this.props.currentLanguage}_name`]
        }`;
        if (currentResult.test_skill_sub_type_id !== null) {
          formattedTestSkill = formattedTestSkill.concat(
            ` - ${currentResult[`test_skill_sub_type_${this.props.currentLanguage}_name`]}`
          );
        }
        const formattedSpacesAvailable = `${currentResult.spaces_unbooked} / ${currentResult.spaces_available}`;
        const currentDateTime = moment().format();
        // pushing needed data in testSessionsArray
        testSessionsArray.push({
          id: currentResult.id,
          date: currentResult.date,
          utc_start_time: currentResult.utc_start_time,
          utc_end_time: currentResult.utc_end_time,
          start_time: currentResult.start_time,
          end_time: currentResult.end_time,
          simplified_start_time: currentResult.simplified_start_time,
          simplified_end_time: currentResult.simplified_end_time,
          formatted_date: formattedDate,
          test_center_room_id: currentResult.test_center_room_id,
          room_name: currentResult.room_name,
          room_max_occupancy: currentResult.room_max_occupancy,
          open_to_ogd: currentResult.open_to_ogd,
          test_skill_type_id: currentResult.test_skill_type_id,
          test_skill_type_codename: currentResult.test_skill_type_codename,
          test_skill_type_name: currentResult[`test_skill_type_${this.props.currentLanguage}_name`],
          test_skill_sub_type_id: currentResult.test_skill_sub_type_id,
          test_skill_sub_type_name:
            currentResult[`test_skill_sub_type_${this.props.currentLanguage}_name`],
          formatted_test_skill: formattedTestSkill,
          spaces_available: currentResult.spaces_available,
          test_id: currentResult.test_id,
          parent_code: currentResult.parent_code,
          test_code: currentResult.test_code,
          test_name_en: currentResult.test_name_en,
          test_name_fr: currentResult.test_name_fr,
          version: currentResult.version,
          specified_test_description: currentResult.specified_test_description,
          closing_date: currentResult.closing_date,
          user_accommodation_file_id: currentResult.user_accommodation_file_id
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: formattedDate,
          column_2:
            currentResult.room_name !== null ? currentResult.room_name : LOCALIZE.commons.na,
          column_3:
            // STANDARD SESSIONS
            this.props.source === TEST_SESSION_DATA_SOURCE.standard
              ? formattedTestSkill
              : // NON-STANDARD SESSIONS
                this.props.source === TEST_SESSION_DATA_SOURCE.nonStandard
                ? currentResult.test_id !== null
                  ? `${currentResult[`test_name_${this.props.currentLanguage}`]} - v${
                      currentResult.version
                    }`
                  : currentResult.specified_test_description
                : // should not happen
                  LOCALIZE.commons.na
        });

        const column_4_or_5 = (
          <>
            {this.props.source === TEST_SESSION_DATA_SOURCE.nonStandard && (
              <StyledTooltip
                id={`test-center-test-sessions-view-accommodation-details-row-${i}`}
                place="top"
                type={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`test-center-test-sessions-view-accommodation-details-row-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon
                            icon={
                              currentResult.is_alternate_test_request
                                ? faFileCircleExclamation
                                : faUniversalAccess
                            }
                            style={styles.tableIcon}
                          />
                        </>
                      }
                      action={() => this.openAccommodationsRequestDetailsPopup(currentResult)}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        currentResult.is_alternate_test_request
                          ? LOCALIZE.accommodationsRequestDetailsPopup
                              .viewAlternateTestRequestDetailsAccessibilityLabel
                          : LOCALIZE.accommodationsRequestDetailsPopup
                              .viewAccommodationsRequestDetailsAccessibilityLabel,
                        `${testSessionsArray[i][`test_name_${this.props.currentLanguage}`]} - v${
                          testSessionsArray[i].version
                        }`
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {currentResult.is_alternate_test_request
                        ? LOCALIZE.accommodationsRequestDetailsPopup.viewAlternateTestRequestDetails
                        : LOCALIZE.accommodationsRequestDetailsPopup
                            .viewAccommodationsRequestDetails}
                    </p>
                  </div>
                }
              />
            )}
            <StyledTooltip
              id={`test-center-test-sessions-view-row-${i}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`test-center-test-sessions-view-row-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faListCheck} style={styles.tableIcon} />
                      </>
                    }
                    action={() => {
                      this.openViewTestSessionAttendeesPopup(testSessionsArray[i]);
                    }}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.PRIMARY}
                    ariaLabel={LOCALIZE.formatString(
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.table.viewButtonAccessibility,
                      currentResult.room_name !== null
                        ? currentResult.room_name
                        : LOCALIZE.commons.na,
                      formattedDate
                    )}
                    // no candidate reserved this test session yet ==> disable button (if standard sessions)
                    disabled={
                      // STANDARD SESSIONS
                      this.props.source === TEST_SESSION_DATA_SOURCE.standard
                        ? parseInt(currentResult.spaces_booked) <= 0
                        : // NON-STANDARD SESSIONS
                          false
                    }
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.table.viewButtonTooltip
                    }
                  </p>
                </div>
              }
            />
            <StyledTooltip
              id={`test-center-test-sessions-edit-row-${i}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`test-center-test-sessions-edit-row-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faPencilAlt} style={styles.tableIcon} />
                      </>
                    }
                    action={
                      // STANDARD SESSIONS
                      this.props.source === TEST_SESSION_DATA_SOURCE.standard
                        ? () => {
                            this.openEditStandardTestSessionPopup(testSessionsArray[i]);
                          }
                        : // NON-STANDARD SESSIONS
                          this.props.source === TEST_SESSION_DATA_SOURCE.nonStandard
                          ? () => {
                              this.openEditNonStandardTestSessionPopup(testSessionsArray[i]);
                            }
                          : // should never happen
                            () => {}
                    }
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.PRIMARY}
                    ariaLabel={LOCALIZE.formatString(
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.table.editButtonAccessibility,
                      formattedDate,
                      currentResult.room_name
                    )}
                    // at least one candidate reserved this test session ==> disable button (if standard sessions)
                    disabled={
                      // STANDARD SESSIONS
                      this.props.source === TEST_SESSION_DATA_SOURCE.standard
                        ? parseInt(currentResult.spaces_booked) > 0
                        : // NON-STANDARD SESSIONS
                          false
                    }
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.table.editButtonTooltip
                    }
                  </p>
                </div>
              }
            />
            <StyledTooltip
              id={`test-center-test-sessions-delete-row-${i}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`test-center-test-sessions-delete-row-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faBan} style={styles.tableIcon} />
                      </>
                    }
                    action={() => {
                      this.openDeleteTestSessionPopup(testSessionsArray[i]);
                    }}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.PRIMARY}
                    ariaLabel={LOCALIZE.formatString(
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.table.deleteButtonAccessibility,
                      formattedDate,
                      currentResult.room_name
                    )}
                    // session is already started + at least one candidate reserved this test session ==> disable button
                    disabled={
                      parseInt(currentResult.spaces_booked) > 0 &&
                      currentDateTime > currentResult.start_time
                    }
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.table.deleteButtonTooltip
                    }
                  </p>
                </div>
              }
            />
          </>
        );

        // STANDARD SESSIONS
        if (this.props.source === TEST_SESSION_DATA_SOURCE.standard) {
          data[i].column_4 = formattedSpacesAvailable;
          data[i].column_5 = column_4_or_5;
          // NON-STANDARD SESSIONS
        } else if (this.props.source === TEST_SESSION_DATA_SOURCE.nonStandard) {
          data[i].column_4 = column_4_or_5;
        }
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState({
      testSesions: testSessionsArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(
        response.count / this.props.testCenterTestSessionsPaginationPageSize
      ),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  populateTestSkillTypeOptions = () => {
    // initializing testSkillTypeOptions
    const testSkillTypeOptions = [];
    // getting test skill types
    this.props.getTestSkillTypes().then(response => {
      // looping in test skill types
      for (let i = 0; i < response.body.length; i++) {
        // hiding NONE test skill type
        if (response.body[i].codename !== TestSkillTypeCodename.NONE) {
          // populating testSkillTypeOptions
          testSkillTypeOptions.push({
            label: response.body[i].test_skill_type_text[`${this.props.currentLanguage}`][0].text,
            value: response.body[i].id,
            codename: response.body[i].codename
          });
        }
        this.setState({ testSkillTypeOptions: testSkillTypeOptions });
      }
    });
  };

  // populate room options
  populateRoomOptions = () => {
    const roomOptions = [];
    for (let i = 0; i < this.props.testCenterData.rooms.length; i++) {
      // room of current iteration is active
      if (this.props.testCenterData.rooms[i].active) {
        // populating testAdministratorOptions
        roomOptions.push({
          label: `${this.props.testCenterData.rooms[i].name} [${LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs.testSessionsData.addTestSessionPopup.roomCapacity} ${this.props.testCenterData.rooms[i].max_occupancy}]`,
          value: this.props.testCenterData.rooms[i].id,
          room_name: this.props.testCenterData.rooms[i].name
        });
      }
    }
    this.setState({ roomOptions: roomOptions });
  };

  openViewTestSessionAttendeesPopup = testSessionData => {
    // getting related attendees
    this.setState(
      {
        testSessionAttendeesLoading: true,
        selectedTestSessionToViewEditOrDelete: testSessionData,
        showTestSessionAttendeesPopup: true
      },
      () => {
        this.props.getTestSessionAttendees(testSessionData.id).then(response => {
          // initializing needed object and array for testSessionAttendeesRowsDefinition props (needed for GenericTable component)
          let testSessionAttendeesRowsDefinition = {};
          const data = [];

          // looping in response given
          for (let i = 0; i < response.length; i++) {
            const currentResult = response[i];
            // formatting CAT user data
            const formattedCatUserData = `${currentResult.cat_user_last_name}, ${currentResult.cat_user_first_name} (${currentResult.cat_user_email})`;
            // pushing needed data in data array (needed for GenericTable component)
            data.push({
              column_1: currentResult.first_name,
              column_2: currentResult.last_name,
              column_3: currentResult.email,
              column_4: formattedCatUserData
            });
          }

          // updating testSessionAttendeesRowsDefinition object with provided data and needed style
          testSessionAttendeesRowsDefinition = {
            column_1_style: COMMON_STYLE.LEFT_TEXT,
            column_2_style: COMMON_STYLE.LEFT_TEXT,
            column_3_style: COMMON_STYLE.LEFT_TEXT,
            column_4_style: COMMON_STYLE.LEFT_TEXT,
            data: data
          };

          // saving results in state
          this.setState({
            testSessionAttendeesRowsDefinition: testSessionAttendeesRowsDefinition,
            testSessionAttendeesLoading: false
          });
        });
      }
    );
  };

  closeTestSessionAttendeesPopup = () => {
    this.setState({
      showTestSessionAttendeesPopup: false,
      selectedTestSessionToViewEditOrDelete: {}
    });
  };

  handlePrint = () => {
    // triggering the react to print component
    this.setState({ triggerReactToPrint: true }, () => {
      // need to set to false as soon as we have called the ReactToPrint component, so we can re-open it (if needed)
      this.setState({ triggerReactToPrint: false });
    });
  };

  openEditStandardTestSessionPopup = testSessionData => {
    this.populateTestSkillSubTypeOptions(testSessionData.test_skill_type_id).then(
      testSkillSubTypeOptions => {
        this.props.updateDatePicked(testSessionData.date);
        setTimeout(() => {
          // calling this function in order to call the date validation (needed to get the right date picker redux states)
          this.handleDateUpdates();

          // formatting start and end times
          const formattedStartTime = convertDateTimeToUsersTimezone(
            testSessionData.utc_start_time
          ).adjustedTime;
          const formattedEndTime = convertDateTimeToUsersTimezone(
            testSessionData.utc_end_time
          ).adjustedTime;

          this.setState(
            {
              showEditTestSessionPopup: true,
              selectedTestSessionToViewEditOrDelete: testSessionData,
              roomSelectedOption: {
                label: `${testSessionData.room_name} [${LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs.testSessionsData.addTestSessionPopup.roomCapacity} ${testSessionData.room_max_occupancy}]`,
                value: Number(testSessionData.test_center_room_id)
              },
              openToOgdSwitchState: testSessionData.open_to_ogd,
              startTimeHour: {
                label: formattedStartTime.split(":")[0],
                value: parseInt(formattedStartTime.split(":")[0])
              },
              startTimeMinute: {
                label: formattedStartTime.split(":")[1],
                value: parseInt(formattedStartTime.split(":")[1])
              },
              endTimeHour: {
                label: formattedEndTime.split(":")[0],
                value: parseInt(formattedEndTime.split(":")[0])
              },
              endTimeMinute: {
                label: formattedEndTime.split(":")[1],
                value: parseInt(formattedEndTime.split(":")[1])
              },
              spacesAvailable: testSessionData.spaces_available,
              isValidSpacesAvailable: true,
              testSkillTypeSelectedOption: {
                label: testSessionData.test_skill_type_name,
                value: testSessionData.test_skill_type_id
              },
              testSkillSubTypeOptions: testSkillSubTypeOptions,
              testSkillSubTypeSelectedOption: {
                label: testSessionData.test_skill_sub_type_name,
                value: Number(testSessionData.test_skill_sub_type_id)
              }
            },
            () => {
              // validating form
              this.validateForm();
            }
          );
        }, 100);
      }
    );
  };

  openEditNonStandardTestSessionPopup = testSessionData => {
    // getting all related test sessions
    this.props.getDataOfTestCenterTestSessionToViewOrEdit(testSessionData.id).then(response => {
      // setting testCenterNonStandardTestSessionData
      const testCenterNonStandardTestSessionData = response;
      // looping in testCenterNonStandardTestSessionData
      for (let i = 0; i < testCenterNonStandardTestSessionData.length; i++) {
        // initializing roomSelectedOption
        testCenterNonStandardTestSessionData[i].roomSelectedOption = {
          label: LOCALIZE.dropdownSelect.pleaseSelect,
          value: null
        };
        // setting needed attributes in order to match the desired format (same format as TEST_SESSION_OBJ_TEMPLATE)
        for (let j = 0; j < this.props.testCenterData.rooms.length; j++) {
          if (
            this.props.testCenterData.rooms[j].id ===
            testCenterNonStandardTestSessionData[i].test_center_room_id
          ) {
            testCenterNonStandardTestSessionData[i].roomSelectedOption = {
              label: `${this.props.testCenterData.rooms[j].name} [${LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs.testSessionsData.addTestSessionPopup.roomCapacity} ${this.props.testCenterData.rooms[j].max_occupancy}]`,
              value: this.props.testCenterData.rooms[j].id
            };
            break;
          }
        }
        testCenterNonStandardTestSessionData[i].isValidDate = true;
        const localStartTime = convertDateTimeToUsersTimezone(
          testCenterNonStandardTestSessionData[i].utc_start_time
        ).adjustedTime;
        testCenterNonStandardTestSessionData[i].startTimeHour = {
          label: localStartTime.split(":")[0],
          value: parseInt(localStartTime.split(":")[0])
        };
        testCenterNonStandardTestSessionData[i].startTimeMinute = {
          label: localStartTime.split(":")[1],
          value: parseInt(localStartTime.split(":")[1])
        };
        const localEndTime = convertDateTimeToUsersTimezone(
          testCenterNonStandardTestSessionData[i].utc_end_time
        ).adjustedTime;
        testCenterNonStandardTestSessionData[i].endTimeHour = {
          label: localEndTime.split(":")[0],
          value: parseInt(localEndTime.split(":")[0])
        };
        testCenterNonStandardTestSessionData[i].endTimeMinute = {
          label: localEndTime.split(":")[1],
          value: parseInt(localEndTime.split(":")[1])
        };
        testCenterNonStandardTestSessionData[i].testAssessorSelectedOption = {
          label: `${testCenterNonStandardTestSessionData[i].test_assessor_user_first_name} ${testCenterNonStandardTestSessionData[i].test_assessor_user_last_name} (${testCenterNonStandardTestSessionData[i].test_assessor_user_email})`,
          value: testCenterNonStandardTestSessionData[i].test_assessor_user_id
        };
      }
      this.props.updateTestSessionNonStandardTestSessionData(testCenterNonStandardTestSessionData);
      this.setState({
        showEditTestSessionPopup: true,
        conditionalContentTabs: this.getTABS(testCenterNonStandardTestSessionData)
      });
    });
  };

  openDeleteTestSessionTabPopup = index => {
    this.setState({ showDeleteTestSessionTabPopup: true, indexToBeDeleted: index });
  };

  handleDeleteTestSessionTab = () => {
    const copyOfTestCenterNonStandardTestSessionData = _.cloneDeep(
      this.props.testCenterNonStandardTestSessionData
    );
    // deleting respective test session tab
    copyOfTestCenterNonStandardTestSessionData.splice(this.state.indexToBeDeleted, 1);
    this.setState(
      { conditionalContentTabs: this.getTABS(copyOfTestCenterNonStandardTestSessionData) },
      () => {
        // updating redux states
        this.props.updateTestSessionNonStandardTestSessionData(
          copyOfTestCenterNonStandardTestSessionData
        );
        // closing popup
        this.closeDeleteTestSessionTabPopup();
      }
    );
  };

  closeDeleteTestSessionTabPopup = () => {
    this.setState({ showDeleteTestSessionTabPopup: false, indexToBeDeleted: null });
  };

  handleAddTestSessionTabPopup = () => {
    // creating a copy of the testCenterNonStandardTestSessionData redux state
    const copyOfTestCenterNonStandardTestSessionData = _.cloneDeep(
      this.props.testCenterNonStandardTestSessionData
    );
    // adding needed attributes to TEST_SESSION_OBJ_TEMPLATE
    const objTemplateToAdd = TEST_SESSION_OBJ_TEMPLATE;
    objTemplateToAdd.id = null;
    objTemplateToAdd.test_id = this.props.testCenterNonStandardTestSessionData[0].test_id;
    objTemplateToAdd.test_name_en = this.props.testCenterNonStandardTestSessionData[0].test_name_en;
    objTemplateToAdd.test_name_fr = this.props.testCenterNonStandardTestSessionData[0].test_name_fr;
    objTemplateToAdd.version = this.props.testCenterNonStandardTestSessionData[0].version;
    objTemplateToAdd.specified_test_description =
      this.props.testCenterNonStandardTestSessionData[0].specified_test_description;
    objTemplateToAdd.user_accommodation_file_id =
      this.props.testCenterNonStandardTestSessionData[0].user_accommodation_file_id;
    objTemplateToAdd.assessment_process_assigned_test_specs_id =
      this.props.testCenterNonStandardTestSessionData[0].assessment_process_assigned_test_specs_id;
    objTemplateToAdd.test_skill_type_id =
      this.props.testCenterNonStandardTestSessionData[0].test_skill_type_id;
    objTemplateToAdd.test_skill_type_codename =
      this.props.testCenterNonStandardTestSessionData[0].test_skill_type_codename;
    objTemplateToAdd.test_skill_sub_type_id =
      this.props.testCenterNonStandardTestSessionData[0].test_skill_sub_type_id;
    objTemplateToAdd.test_skill_sub_type_codename =
      this.props.testCenterNonStandardTestSessionData[0].test_skill_sub_type_codename;
    // adding new test session object
    copyOfTestCenterNonStandardTestSessionData.push(objTemplateToAdd);
    // updating redux states
    this.props.updateTestSessionNonStandardTestSessionData(
      copyOfTestCenterNonStandardTestSessionData
    );
    // switching to new tab
    setTimeout(() => {
      this.setState({
        conditionalContentTabs: this.getTABS(copyOfTestCenterNonStandardTestSessionData)
      });
      document
        .getElementById(
          `top-tabs-tab-test-session-${copyOfTestCenterNonStandardTestSessionData.length}`
        )
        .click();
    }, 100);
  };

  closeEditTestSessionPopup = () => {
    // initializing completeDatePicked and completeDateValidState redux states
    this.props.updateDatePicked(`${null}-${null}-${null}`);
    this.props.updateDatePickedValidState(true);
    this.setState(
      {
        showEditTestSessionPopup: false,
        roomSelectedOption: {},
        openToOgdSwitchState: false,
        startTimeHour: "",
        startTimeMinute: "",
        endTimeHour: "",
        endTimeMinute: "",
        isValidStartEndTimeCombination: true,
        isValidTimeAndDateBasedOnBookingDelay: true,
        spacesAvailable: "",
        isValidSpacesAvailable: true,
        testSkillTypeSelectedOption: [],
        testSkillSubTypeOptions: [],
        testSkillSubTypeSelectedOption: [],
        isValidForm: false,
        showTestSessionTimeOverlappingErrorPopup: false,
        showInvalidTestSessionDateAndTimeCombinationPopup: false
      },
      () => {
        // adding small delay to reset the testSessionUpdatedSuccessfully state to avoid closing anymation glitch
        setTimeout(() => {
          this.setState({ testSessionUpdatedSuccessfully: false });
        }, 200);
      }
    );
  };

  openDeleteTestSessionPopup = testSessionData => {
    this.setState({
      showDeleteTestSessionPopup: true,
      selectedTestSessionToViewEditOrDelete: testSessionData
    });
  };

  closeDeleteRoomPopup = () => {
    // initializing completeDatePicked and completeDateValidState redux states
    this.props.updateDatePicked(`${null}-${null}-${null}`);
    this.props.updateDatePickedValidState(true);
    this.setState({
      showDeleteTestSessionPopup: false,
      selectedTestSessionToViewEditOrDelete: {}
    });
  };

  handleDeleteTestSession = () => {
    this.setState({ isLoadingDeleteTestSession: true }, () => {
      // creating body
      const body = {
        source: this.props.source,
        id: this.state.selectedTestSessionToViewEditOrDelete.id,
        test_center_id: this.props.testCenterData.id,
        user_accommodation_file_id:
          this.state.selectedTestSessionToViewEditOrDelete.user_accommodation_file_id
      };
      this.props.deleteTestCenterTestSession(body).then(response => {
        // successful request
        if (response.ok) {
          // triggering table updates
          this.props.triggerTableUpdates();
          // closing popup and updating table
          this.closeDeleteRoomPopup();
          this.setState({ isLoadingDeleteTestSession: false });
          // should never happen
        } else {
          throw new Error("An error occurred during the delete test session process");
        }
      });
    });
  };

  openAddTestSessionPopup = () => {
    // populating room options
    this.populateRoomOptions();
    setTimeout(() => {
      this.setState({ showAddTestSessionPopup: true });
    }, 100);
  };

  closeAddTestSessionPopup = () => {
    // initializing completeDatePicked and completeDateValidState redux states
    this.props.updateDatePicked(`${null}-${null}-${null}`);
    this.props.updateDatePickedValidState(true);
    this.setState({
      showAddTestSessionPopup: false,
      roomSelectedOption: {},
      openToOgdSwitchState: false,
      startTimeHour: "",
      startTimeMinute: "",
      endTimeHour: "",
      endTimeMinute: "",
      isValidStartEndTimeCombination: true,
      isValidTimeAndDateBasedOnBookingDelay: true,
      spacesAvailable: "",
      isValidSpacesAvailable: true,
      testSkillTypeSelectedOption: [],
      testSkillSubTypeOptions: [],
      testSkillSubTypeSelectedOption: [],
      isValidForm: false,
      showTestSessionTimeOverlappingErrorPopup: false,
      showInvalidTestSessionDateAndTimeCombinationPopup: false
    });
  };

  getSelectedRoomOption = selectedOption => {
    this.setState(
      {
        roomSelectedOption: selectedOption
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  handleDateUpdates = () => {
    setTimeout(() => {
      // date is fully provided
      if (
        this.props.completeDatePicked.split("-")[0] !== "" &&
        this.props.completeDatePicked.split("-")[1] !== "" &&
        this.props.completeDatePicked.split("-")[2] !== ""
      ) {
        // triggering date validation
        this.setState({ triggerDateValidation: !this.state.triggerDateValidation }, () => {
          setTimeout(() => {
            // validating form
            this.validateForm();
          }, 100);
        });
      }
    }, 100);
  };

  getSelectedStartTimeHour = selectedOption => {
    this.setState(
      {
        startTimeHour: selectedOption
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  getSelectedStartTimeMinute = selectedOption => {
    this.setState(
      {
        startTimeMinute: selectedOption
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  getSelectedEndTimeHour = selectedOption => {
    this.setState(
      {
        endTimeHour: selectedOption
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  getSelectedEndTimeMinute = selectedOption => {
    this.setState(
      {
        endTimeMinute: selectedOption
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  updateSpacesAvailable = event => {
    const spacesAvailable = event.target.value;
    // allow maximum of 150 chars
    const regexExpression = /^([0-9]{0,5})$/;
    if (regexExpression.test(spacesAvailable)) {
      this.setState(
        {
          spacesAvailable: spacesAvailable !== "" ? parseInt(spacesAvailable) : ""
        },
        () => {
          // validating form
          this.validateForm();
        }
      );
    }
  };

  getSelectedTestSkillTypeOption = selectedOption => {
    this.setState(
      {
        testSkillTypeSelectedOption: selectedOption,
        testSkillSubTypeSelectedOption: []
      },
      () => {
        // checking if we need to populate the test skill sub type options based on the selected option
        // getting test skill sub type options
        this.populateTestSkillSubTypeOptions(selectedOption.value).then(testSkillSubTypeOptions => {
          this.setState({ testSkillSubTypeOptions: testSkillSubTypeOptions }, () => {
            // validating form
            this.validateForm();
          });
        });
      }
    );
  };

  getSelectedTestSkillSubTypeOption = selectedOption => {
    this.setState(
      {
        testSkillSubTypeSelectedOption: selectedOption
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  updateOpenToOgdSwitch = event => {
    this.setState({ openToOgdSwitchState: event });
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoadingSwitch: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoadingSwitch: false });
        }
      );
    });
  };

  validateForm = () => {
    // initializing needed variables
    let isValidForm = false;

    const isValidRoom = Object.keys(this.state.roomSelectedOption).length > 0;
    const isValidDate = this.props.completeDateValidState;
    let isValidDatePickedBasedOnClosingDate = true;
    // if valid date + non-standard sessions
    if (isValidDate && this.props.source === TEST_SESSION_DATA_SOURCE.nonStandard) {
      isValidDatePickedBasedOnClosingDate = moment(this.props.completeDatePicked).isSameOrBefore(
        moment(this.state.selectedTestSessionToViewEditOrDelete.closing_date)
      );
    }
    const isValidStartTime = this.state.startTimeHour !== "" && this.state.startTimeMinute !== "";
    const isValidEndTime = this.state.endTimeHour !== "" && this.state.endTimeMinute !== "";
    let isValidStartEndTimeCombination = true;
    let isValidTimeAndDateBasedOnBookingDelay = true;
    // start time and end time are provided
    if (isValidStartTime && isValidEndTime) {
      const formattedStartTime = moment(
        `${this.state.startTimeHour.label}:${this.state.startTimeMinute.label}`,
        "HH:mm"
      );
      const formattedEndTime = moment(
        `${this.state.endTimeHour.label}:${this.state.endTimeMinute.label}`,
        "HH:mm"
      );
      // validating times (making sure that the start date is before the end date)
      isValidStartEndTimeCombination = formattedStartTime.isBefore(formattedEndTime);
    }
    // date, start time and end time are provided + isValidStartEndTimeCombination is true + standard sessions
    if (
      isValidDate &&
      isValidStartTime &&
      isValidEndTime &&
      isValidStartEndTimeCombination &&
      this.props.source === TEST_SESSION_DATA_SOURCE.standard
    ) {
      // validating time and date (making sure that the start time + date is respecting the booking delay ==> date/time of test session >= current time + booking delay)
      // getting current time
      const currentTime = moment();
      // adding booking delay (hours) to curent time
      const currentTimePlusBookingDelay = moment(currentTime).add(
        Number(this.props.testCenterData.booking_delay),
        "hours"
      );
      // getting start date and time
      const testSessionStartDateAndTime = moment(
        `${this.props.completeDatePicked} ${this.state.startTimeHour.label}:${this.state.startTimeMinute.label}`
      );
      // checking if testSessionStartDateAndTime is same or after currentTimePlusBookingDelay
      isValidTimeAndDateBasedOnBookingDelay = testSessionStartDateAndTime.isSameOrAfter(
        currentTimePlusBookingDelay
      );
    }
    let isValidSpacesAvailable =
      this.props.source === TEST_SESSION_DATA_SOURCE.standard
        ? this.state.spacesAvailable !== ""
        : true;
    // room is selected and spaces available is provided + standard sessions
    if (
      Object.keys(this.state.roomSelectedOption).length > 0 &&
      this.state.spacesAvailable !== "" &&
      this.props.source === TEST_SESSION_DATA_SOURCE.standard
    ) {
      // getting room maximum occupancy
      const room_max_occupancy_array = this.props.testCenterData.rooms.filter(
        obj => obj.id === this.state.roomSelectedOption.value
      );

      // matching room found
      if (room_max_occupancy_array.length > 0) {
        // setting isValidSpacesAvailable
        isValidSpacesAvailable =
          this.state.spacesAvailable !== "" &&
          this.state.spacesAvailable <= room_max_occupancy_array[0].max_occupancy;
      }
      // updating isValidSpacesAvailable state
      this.setState({ isValidSpacesAvailable: isValidSpacesAvailable });
    }
    const isValidTestSkillType =
      this.props.source === TEST_SESSION_DATA_SOURCE.standard
        ? Object.keys(this.state.testSkillTypeSelectedOption).length > 0
        : true;
    let isValidTestSkillSubType = true;

    // testSkillSubTypeOptions state is defined (meaning that a test skill sub type is required) + standard sessions
    if (
      this.state.testSkillSubTypeOptions.length > 0 &&
      this.props.source === TEST_SESSION_DATA_SOURCE.standard
    ) {
      isValidTestSkillSubType = Object.keys(this.state.testSkillSubTypeSelectedOption).length > 0;
    }

    // if all mandatory fields are valid
    if (
      isValidRoom &&
      isValidDate &&
      isValidStartTime &&
      isValidEndTime &&
      isValidStartEndTimeCombination &&
      isValidTimeAndDateBasedOnBookingDelay &&
      isValidDatePickedBasedOnClosingDate &&
      isValidSpacesAvailable &&
      isValidTestSkillType &&
      isValidTestSkillSubType
    ) {
      isValidForm = true;
    }
    this.setState({
      isValidForm: isValidForm,
      isValidStartEndTimeCombination: isValidStartEndTimeCombination,
      isValidTimeAndDateBasedOnBookingDelay: isValidTimeAndDateBasedOnBookingDelay,
      isValidDatePickedBasedOnClosingDate: isValidDatePickedBasedOnClosingDate
    });
  };

  handleAddTestSession = () => {
    // converting local time to UTC before saving to DB
    const utcStartDateTime = convertUsersTimezoneDateTimeToUtc(
      `${this.props.completeDatePicked} ${this.state.startTimeHour.label}:${this.state.startTimeMinute.label}`
    );
    const utcEndDateTime = convertUsersTimezoneDateTimeToUtc(
      `${this.props.completeDatePicked} ${this.state.endTimeHour.label}:${this.state.endTimeMinute.label}`
    );

    // creating body
    const body = {
      test_center_id: this.props.testCenterData.id,
      test_center_room_id: this.props.testCenterData.rooms.filter(
        obj => obj.id === this.state.roomSelectedOption.value
      )[0].id,
      open_to_ogd: this.state.openToOgdSwitchState,
      date: this.props.completeDatePicked,
      // "yyyy-mm-dd hh:mm:ss Z"
      utc_start_time: utcStartDateTime,
      // "yyyy-mm-dd hh:mm:ss Z"
      utc_end_time: utcEndDateTime,
      spaces_available: this.state.spacesAvailable,
      test_skill_type_id: this.state.testSkillTypeSelectedOption.value,
      test_skill_sub_type_id:
        Object.keys(this.state.testSkillSubTypeSelectedOption).length > 0
          ? this.state.testSkillSubTypeSelectedOption.value
          : null
    };
    this.props.addTestSessionToTestCenter(body).then(response => {
      // successful request
      if (response.ok) {
        // populating table
        this.populateTestSessionsBasedOnActiveSearch();
        // closing popup
        this.closeAddTestSessionPopup();
        // time is conflicting with other test session(s)
      } else if (response.status === 409) {
        this.setState({ showTestSessionTimeOverlappingErrorPopup: true });
        // should never happen
      } else {
        throw new Error("An error occurred during the add test session process");
      }
    });
  };

  handleEditTestSession = () => {
    this.setState({ isLoadingEditTestSession: true }, () => {
      // converting local time to UTC before saving to DB
      const utcStartDateTime = convertUsersTimezoneDateTimeToUtc(
        `${this.props.completeDatePicked} ${this.state.startTimeHour.label}:${this.state.startTimeMinute.label}`
      );
      const utcEndDateTime = convertUsersTimezoneDateTimeToUtc(
        `${this.props.completeDatePicked} ${this.state.endTimeHour.label}:${this.state.endTimeMinute.label}`
      );
      // creating body
      const body = {
        source: this.props.source,
        id: this.state.selectedTestSessionToViewEditOrDelete.id,
        test_center_id: this.props.testCenterData.id,
        test_center_room_id:
          // STANDARD SESSIONS
          this.props.source === TEST_SESSION_DATA_SOURCE.standard
            ? this.state.selectedTestSessionToViewEditOrDelete.test_center_room_id
            : // NON-STANDARD SESSIONS
              this.props.source === TEST_SESSION_DATA_SOURCE.nonStandard
              ? this.state.roomSelectedOption.value
              : // should never happen
                null,
        open_to_ogd: this.state.openToOgdSwitchState,
        date: this.props.completeDatePicked,
        // "yyyy-mm-dd hh:mm:ss Z"
        utc_start_time: utcStartDateTime,
        // "yyyy-mm-dd hh:mm:ss Z"
        utc_end_time: utcEndDateTime,
        spaces_available: this.state.spacesAvailable
      };
      this.props.editTestCenterTestSession(body).then(response => {
        // successful request
        if (response.ok) {
          // populating table
          this.populateTestSessionsBasedOnActiveSearch();
          // show success message
          this.setState({ testSessionUpdatedSuccessfully: true, isLoadingEditTestSession: false });
          // time is conflicting with other test session(s)
        } else if (response.status === 409) {
          this.setState({
            showTestSessionTimeOverlappingErrorPopup: true,
            isLoadingEditTestSession: false
          });
          // should never happen
        } else {
          throw new Error("An error occurred during the edit test session process");
        }
      });
    });
  };

  handleEditNonStandardTestSession = () => {
    // building the finalArray that will be sent to the backend
    const finalArray = [];
    // setting loading logic
    this.setState({ isLoadingEditTestSession: true }, () => {
      // looping in testCenterNonStandardTestSessionData
      for (let i = 0; i < this.props.testCenterNonStandardTestSessionData.length; i++) {
        // converting local time to UTC before saving to DB
        const utcStartDateTime = convertUsersTimezoneDateTimeToUtc(
          `${this.props.testCenterNonStandardTestSessionData[i].date} ${this.props.testCenterNonStandardTestSessionData[i].startTimeHour.label}:${this.props.testCenterNonStandardTestSessionData[i].startTimeMinute.label}`
        );
        const utcEndDateTime = convertUsersTimezoneDateTimeToUtc(
          `${this.props.testCenterNonStandardTestSessionData[i].date} ${this.props.testCenterNonStandardTestSessionData[i].endTimeHour.label}:${this.props.testCenterNonStandardTestSessionData[i].endTimeMinute.label}`
        );

        // for second iteration and up, making sure that the start date and time is same or later than previous test session end date and time
        if (i > 0) {
          // getting previous end date time
          const previousEndDateTime = `${
            this.props.testCenterNonStandardTestSessionData[i - 1].date
          } ${this.props.testCenterNonStandardTestSessionData[i - 1].endTimeHour.label}:${
            this.props.testCenterNonStandardTestSessionData[i - 1].endTimeMinute.label
          }`;
          // getting previous start date time
          const previousStartDateTime = `${this.props.testCenterNonStandardTestSessionData[i].date} ${this.props.testCenterNonStandardTestSessionData[i].startTimeHour.label}:${this.props.testCenterNonStandardTestSessionData[i].startTimeMinute.label}`;
          const isValidTestSessionDateAndTimeCombination = moment(
            previousEndDateTime
          ).isSameOrBefore(moment(previousStartDateTime));
          // if not valid
          if (!isValidTestSessionDateAndTimeCombination) {
            // open invalid test session date and time combination popup
            this.openInvalidTestSessionDateAndTimeCombinationPopup();
            // breaking the loop/function
            return;
          }
        }

        finalArray.push({
          id: this.props.testCenterNonStandardTestSessionData[i].id,
          assessment_process_assigned_test_specs_id:
            this.props.testCenterNonStandardTestSessionData[i]
              .assessment_process_assigned_test_specs_id,
          test_center_id: this.props.testCenterData.id,
          test_center_room_id:
            this.props.testCenterNonStandardTestSessionData[i].roomSelectedOption.value,
          date: this.props.testCenterNonStandardTestSessionData[i].date,
          // "yyyy-mm-dd hh:mm:ss Z"
          utc_start_time: utcStartDateTime,
          // "yyyy-mm-dd hh:mm:ss Z"
          utc_end_time: utcEndDateTime,
          user_accommodation_file_id:
            this.props.testCenterNonStandardTestSessionData[i].user_accommodation_file_id,
          test_skill_type_id: this.props.testCenterNonStandardTestSessionData[i].test_skill_type_id,
          test_skill_sub_type_id:
            this.props.testCenterNonStandardTestSessionData[i].test_skill_sub_type_id,
          test_assessor_user_id:
            this.props.testCenterNonStandardTestSessionData[i].testAssessorSelectedOption.value
        });
      }

      this.props.editNonStandardTestCenterTestSession(finalArray).then(response => {
        // successful request
        if (response.ok) {
          // populating table
          this.populateTestSessionsBasedOnActiveSearch();
          // show success message
          this.setState({ testSessionUpdatedSuccessfully: true, isLoadingEditTestSession: false });
          // time is conflicting with other test session(s)
        } else if (response.status === 409) {
          this.setState({
            showTestSessionTimeOverlappingErrorPopup: true,
            isLoadingEditTestSession: false
          });
          // should never happen
        } else {
          throw new Error("An error occurred during the edit non standard test session process");
        }
      });
    });
  };

  closeTimeOverlappingErrorPopup = () => {
    this.setState({ showTestSessionTimeOverlappingErrorPopup: false });
  };

  openInvalidTestSessionDateAndTimeCombinationPopup = () => {
    this.setState({
      showInvalidTestSessionDateAndTimeCombinationPopup: true,
      isLoadingEditTestSession: false
    });
  };

  closeInvalidTestSessionDateAndTimeCombinationPopup = () => {
    this.setState({ showInvalidTestSessionDateAndTimeCombinationPopup: false });
  };

  openAccommodationsRequestDetailsPopup = accommodationRequestData => {
    // updating needed states
    this.setState({
      showAccommodationsRequestDetailsPopup: true,
      selectedAccommodationRequestData: accommodationRequestData
    });
  };

  closeAccommodationsRequestDetailsPopup = () => {
    this.setState({
      showAccommodationsRequestDetailsPopup: false,
      selectedAccommodationRequestData: {}
    });
  };

  // async function
  async populateTestSkillSubTypeOptions(testSkillTypeId) {
    // initializing testSkillSubTypeOptions
    const testSkillSubTypeOptions = [];
    // getting test skill sub types
    await this.props.getTestSkillSubTypes(testSkillTypeId).then(response => {
      // looping in test skill sub types
      for (let i = 0; i < response.body.length; i++) {
        // populating testSkillSubTypeOptions
        testSkillSubTypeOptions.push({
          label: response.body[i].test_skill_sub_type_text[`${this.props.currentLanguage}`][0].text,
          value: response.body[i].id,
          codename: response.body[i].codename
        });
      }
    });
    return testSkillSubTypeOptions;
  }

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
            .testSessionsData.table.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
            .testSessionsData.table.columnTwo,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
            .testSessionsData.table.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    // STANDARD SESSIONS
    if (this.props.source === TEST_SESSION_DATA_SOURCE.standard) {
      columnsDefinition.push({
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
            .testSessionsData.table.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      });
    }

    columnsDefinition.push({
      label:
        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
          .testSessionsData.table.columnFive,
      style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
    });

    const testSessionAttendeesColumnsDefinition = [
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
            .testSessionsData.viewAttendeesPopup.table.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
            .testSessionsData.viewAttendeesPopup.table.columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
            .testSessionsData.viewAttendeesPopup.table.columnThree,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
            .testSessionsData.viewAttendeesPopup.table.columnFour,
        style: COMMON_STYLE.LEFT_TEXT
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        {this.props.source === TEST_SESSION_DATA_SOURCE.standard && (
          <div>
            <CustomButton
              label={
                <>
                  <FontAwesomeIcon icon={faPlusCircle} style={styles.buttonIcon} />
                  <span>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.addTestSessionButton
                    }
                  </span>
                </>
              }
              action={this.openAddTestSessionPopup}
              buttonTheme={THEME.PRIMARY}
            />
          </div>
        )}
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"test-center-rooms"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchTestCenterTestSessionsStates}
            updatePageState={this.props.updateCurrentTestCenterTestSessionsPageState}
            paginationPageSize={Number(this.props.testCenterTestSessionsPaginationPageSize)}
            updatePaginationPageSize={this.props.updateTestCenterTestSessionsPageSizeState}
            data={this.state.testSessions}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.testCenterTestSessionsKeyword}
          />
          <div>
            <GenericTable
              classnamePrefix="test-center-rooms"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                  .topTabs.testSessionsData.table.noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <Pagination
            paginationContainerId={"test-center-rooms-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.testCenterTestSessionsPaginationPage}
            updatePaginationPageState={this.props.updateCurrentTestCenterTestSessionsPageState}
            firstTableRowId={"test-center-rooms-table-row-0"}
          />
        </div>
        <PopupBox
          show={this.state.showAddTestSessionPopup}
          handleClose={() => {}}
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
              .testSessionsData.addTestSessionPopup.title
          }
          description={
            <div>
              <p>
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                    .topTabs.testSessionsData.addTestSessionPopup.description
                }
              </p>
              <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label id="test-center-test-sessions-room-label" style={styles.inputTitles}>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.addTestSessionPopup.roomLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <DropdownSelect
                    idPrefix="test-center-test-sessions-room"
                    isValid={true}
                    ariaRequired={true}
                    ariaLabelledBy="test-center-test-sessions-room-label"
                    hasPlaceholder={true}
                    options={this.state.roomOptions}
                    onChange={this.getSelectedRoomOption}
                    defaultValue={this.state.roomSelectedOption}
                  />
                </Col>
              </Row>
              <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label
                    id="test-center-test-sessions-open-to-ogd-label"
                    style={styles.inputTitles}
                  >
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.addTestSessionPopup.openToOgdLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  {!this.state.isLoadingSwitch && (
                    <Switch
                      onChange={this.updateOpenToOgdSwitch}
                      checked={this.state.openToOgdSwitchState}
                      aria-labelledby="test-center-test-sessions-open-to-ogd-label"
                      height={this.state.switchHeight}
                      width={this.state.switchWidth}
                    />
                  )}
                </Col>
              </Row>
              <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label id="test-center-test-sessions-date-label" style={styles.inputTitles}>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.addTestSessionPopup.dateLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <DatePicker
                    dateDayFieldRef={this.testSessionDateDayFieldRef}
                    dateLabelId={"test-center-test-sessions-date-input"}
                    ariaLabelledBy={"test-center-test-sessions-date-label"}
                    onInputChange={this.handleDateUpdates}
                    customYearOptions={populateCustomFutureYearsDateOptions(1)}
                    triggerValidation={this.state.triggerDateValidation}
                    futureDateValidation={true}
                  />
                </Col>
              </Row>
              <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label id="test-center-test-sessions-start-time-label" style={styles.inputTitles}>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.addTestSessionPopup.startTimeLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumnTime.xl}
                  lg={columnSizes.secondColumnTime.lg}
                  md={columnSizes.secondColumnTime.md}
                  sm={columnSizes.secondColumnTime.sm}
                  xs={columnSizes.secondColumnTime.xs}
                >
                  <DropdownSelect
                    idPrefix="test-center-test-sessions-start-time-hour"
                    isValid={
                      this.state.isValidStartEndTimeCombination &&
                      this.state.isValidTimeAndDateBasedOnBookingDelay
                    }
                    ariaRequired={true}
                    ariaLabelledBy="test-center-test-sessions-start-time-label test-center-test-sessions-start-time-hour-label test-center-test-sessions-start-end-time-combination-error test-center-test-sessions-start-date-and-time-based-on-booking-delay-error"
                    hasPlaceholder={true}
                    customPlaceholderWording={LOCALIZE.datePicker.hourField}
                    options={this.state.hourOptions}
                    onChange={this.getSelectedStartTimeHour}
                    defaultValue={this.state.startTimeHour}
                  />
                </Col>
                <Col
                  xl={columnSizes.secondColumnTime.xl}
                  lg={columnSizes.secondColumnTime.lg}
                  md={columnSizes.secondColumnTime.md}
                  sm={columnSizes.secondColumnTime.sm}
                  xs={columnSizes.secondColumnTime.xs}
                >
                  <DropdownSelect
                    idPrefix="test-center-test-sessions-start-time-minute"
                    isValid={
                      this.state.isValidStartEndTimeCombination &&
                      this.state.isValidTimeAndDateBasedOnBookingDelay
                    }
                    ariaRequired={true}
                    ariaLabelledBy="test-center-test-sessions-start-time-label test-center-test-sessions-start-time-minute-label test-center-test-sessions-start-end-time-combination-error test-center-test-sessions-start-date-and-time-based-on-booking-delay-error"
                    hasPlaceholder={true}
                    customPlaceholderWording={LOCALIZE.datePicker.minuteField}
                    options={this.state.minuteOptions}
                    onChange={this.getSelectedStartTimeMinute}
                    defaultValue={this.state.startTimeMinute}
                  />
                </Col>
              </Row>
              <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.centerText}
                />
                <Col
                  xl={columnSizes.secondColumnTime.xl}
                  lg={columnSizes.secondColumnTime.lg}
                  md={columnSizes.secondColumnTime.md}
                  sm={columnSizes.secondColumnTime.sm}
                  xs={columnSizes.secondColumnTime.xs}
                  style={styles.centerText}
                >
                  <label id="test-center-test-sessions-start-time-hour-label">
                    {LOCALIZE.datePicker.hourField}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumnTime.xl}
                  lg={columnSizes.secondColumnTime.lg}
                  md={columnSizes.secondColumnTime.md}
                  sm={columnSizes.secondColumnTime.sm}
                  xs={columnSizes.secondColumnTime.xs}
                  style={styles.centerText}
                >
                  <label id="test-center-test-sessions-start-time-minute-label">
                    {LOCALIZE.datePicker.minuteField}
                  </label>
                </Col>
              </Row>
              <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label id="test-center-test-sessions-end-time-label" style={styles.inputTitles}>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.addTestSessionPopup.endTimeLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumnTime.xl}
                  lg={columnSizes.secondColumnTime.lg}
                  md={columnSizes.secondColumnTime.md}
                  sm={columnSizes.secondColumnTime.sm}
                  xs={columnSizes.secondColumnTime.xs}
                >
                  <DropdownSelect
                    idPrefix="test-center-test-sessions-end-time-hour"
                    isValid={
                      this.state.isValidStartEndTimeCombination &&
                      this.state.isValidTimeAndDateBasedOnBookingDelay
                    }
                    ariaRequired={true}
                    ariaLabelledBy="test-center-test-sessions-end-time-label test-center-test-sessions-end-time-hour-label test-center-test-sessions-start-end-time-combination-error test-center-test-sessions-start-date-and-time-based-on-booking-delay-error"
                    hasPlaceholder={true}
                    customPlaceholderWording={LOCALIZE.datePicker.hourField}
                    options={this.state.hourOptions}
                    onChange={this.getSelectedEndTimeHour}
                    defaultValue={this.state.endTimeHour}
                  />
                </Col>
                <Col
                  xl={columnSizes.secondColumnTime.xl}
                  lg={columnSizes.secondColumnTime.lg}
                  md={columnSizes.secondColumnTime.md}
                  sm={columnSizes.secondColumnTime.sm}
                  xs={columnSizes.secondColumnTime.xs}
                >
                  <DropdownSelect
                    idPrefix="test-center-test-sessions-end-time-minute"
                    isValid={
                      this.state.isValidStartEndTimeCombination &&
                      this.state.isValidTimeAndDateBasedOnBookingDelay
                    }
                    ariaRequired={true}
                    ariaLabelledBy="test-center-test-sessions-end-time-label test-center-test-sessions-end-time-minute-label test-center-test-sessions-start-end-time-combination-error test-center-test-sessions-start-date-and-time-based-on-booking-delay-error"
                    hasPlaceholder={true}
                    customPlaceholderWording={LOCALIZE.datePicker.minuteField}
                    options={this.state.minuteOptions}
                    onChange={this.getSelectedEndTimeMinute}
                    defaultValue={this.state.endTimeMinute}
                  />
                </Col>
              </Row>
              <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                  style={styles.centerText}
                />
                <Col
                  xl={columnSizes.secondColumnTime.xl}
                  lg={columnSizes.secondColumnTime.lg}
                  md={columnSizes.secondColumnTime.md}
                  sm={columnSizes.secondColumnTime.sm}
                  xs={columnSizes.secondColumnTime.xs}
                  style={styles.centerText}
                >
                  <label id="test-center-test-sessions-end-time-hour-label">
                    {LOCALIZE.datePicker.hourField}
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumnTime.xl}
                  lg={columnSizes.secondColumnTime.lg}
                  md={columnSizes.secondColumnTime.md}
                  sm={columnSizes.secondColumnTime.sm}
                  xs={columnSizes.secondColumnTime.xs}
                  style={styles.centerText}
                >
                  <label id="test-center-test-sessions-end-time-minute-label">
                    {LOCALIZE.datePicker.minuteField}
                  </label>
                </Col>
              </Row>
              {!this.state.isValidStartEndTimeCombination && (
                <Row
                  role="presentation"
                  className="align-items-center justify-content-end"
                  style={styles.rowContainerWithError}
                >
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label
                      id="test-center-test-sessions-start-end-time-combination-error"
                      style={styles.errorMessage}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                          .testSessions.topTabs.testSessionsData.addTestSessionPopup
                          .invalidStartEndTimeCombination
                      }
                    </label>
                  </Col>
                </Row>
              )}
              {!this.state.isValidTimeAndDateBasedOnBookingDelay && (
                <Row
                  role="presentation"
                  className="align-items-center justify-content-end"
                  style={styles.rowContainerWithError}
                >
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label
                      id="test-center-test-sessions-start-date-and-time-based-on-booking-delay-error"
                      style={styles.errorMessage}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                          .testSessions.topTabs.testSessionsData.addTestSessionPopup
                          .invalidStartDateAndTimeBasedOnBookingDelay
                      }
                    </label>
                  </Col>
                </Row>
              )}
              <Row
                role="presentation"
                className="align-items-center"
                style={
                  this.state.isValidSpacesAvailable
                    ? styles.rowContainer
                    : styles.rowContainerWithError
                }
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label
                    htmlFor="test-center-test-sessions-spaces-available-input"
                    style={styles.inputTitles}
                  >
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.addTestSessionPopup
                        .spacesAvailableLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <input
                    id="test-center-test-sessions-spaces-available-input"
                    className={this.state.isValidSpacesAvailable ? "valid-field" : "invalid-field"}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.spacesAvailable}
                    onChange={this.updateSpacesAvailable}
                  ></input>
                </Col>
              </Row>
              {!this.state.isValidSpacesAvailable && (
                <Row
                  role="presentation"
                  className="align-items-center justify-content-end"
                  style={styles.rowContainerWithError}
                >
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label
                      htmlFor="test-center-test-sessions-spaces-available-input"
                      style={styles.errorMessage}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                          .testSessions.topTabs.testSessionsData.addTestSessionPopup
                          .invalidSpacesAvailableError
                      }
                    </label>
                  </Col>
                </Row>
              )}
              <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label
                    id="test-center-test-sessions-test-skill-type-label"
                    style={styles.inputTitles}
                  >
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.addTestSessionPopup.testSkillType
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <DropdownSelect
                    idPrefix="test-center-test-sessions-test-skill-type"
                    isValid={true}
                    ariaRequired={true}
                    ariaLabelledBy="test-center-test-sessions-test-skill-type-label"
                    hasPlaceholder={true}
                    menuPlacement="top"
                    options={this.state.testSkillTypeOptions}
                    onChange={this.getSelectedTestSkillTypeOption}
                    defaultValue={this.state.testSkillTypeSelectedOption}
                  />
                </Col>
              </Row>
              {this.state.testSkillSubTypeOptions.length > 0 && (
                <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                  >
                    <label
                      id="test-center-test-sessions-test-skill-sub-type-label"
                      style={styles.inputTitles}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                          .testSessions.topTabs.testSessionsData.addTestSessionPopup
                          .testSkillSubType
                      }
                    </label>
                  </Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <DropdownSelect
                      idPrefix="test-center-test-sessions-test-skill-sub-type"
                      isValid={true}
                      ariaRequired={true}
                      ariaLabelledBy="test-center-test-sessions-test-skill-sub-type-label"
                      hasPlaceholder={true}
                      menuPlacement="top"
                      options={this.state.testSkillSubTypeOptions}
                      onChange={this.getSelectedTestSkillSubTypeOption}
                      defaultValue={this.state.testSkillSubTypeSelectedOption}
                    />
                  </Col>
                </Row>
              )}
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeAddTestSessionPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
              .testSessionsData.addTestSessionPopup.addTestSessionButton
          }
          rightButtonAction={this.handleAddTestSession}
          rightButtonIcon={faPlusCircle}
          rightButtonState={this.state.isValidForm ? BUTTON_STATE.enabled : BUTTON_STATE.disabled}
        />
        <PopupBox
          show={this.state.showEditTestSessionPopup}
          handleClose={() => {}}
          title={
            !this.state.testSessionUpdatedSuccessfully
              ? LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                  .topTabs.testSessionsData.editTestSessionPopup.title
              : LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                  .topTabs.testSessionsData.editTestSessionPopup.titleSuccess
          }
          size={!this.state.testSessionUpdatedSuccessfully ? "xl" : "lg"}
          description={
            <>
              {!this.state.testSessionUpdatedSuccessfully ? (
                <div>
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.editTestSessionPopup.description
                    }
                  </p>
                  {this.props.source === TEST_SESSION_DATA_SOURCE.standard ? (
                    <>
                      <Row
                        role="presentation"
                        className="align-items-center"
                        style={styles.rowContainer}
                      >
                        <Col
                          xl={columnSizes.firstColumn.xl}
                          lg={columnSizes.firstColumn.lg}
                          md={columnSizes.firstColumn.md}
                          sm={columnSizes.firstColumn.sm}
                          xs={columnSizes.firstColumn.xs}
                        >
                          <label
                            id="test-center-test-sessions-room-label"
                            style={styles.inputTitles}
                          >
                            {
                              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                                .testSessions.topTabs.testSessionsData.editTestSessionPopup
                                .roomLabel
                            }
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.secondColumn.xl}
                          lg={columnSizes.secondColumn.lg}
                          md={columnSizes.secondColumn.md}
                          sm={columnSizes.secondColumn.sm}
                          xs={columnSizes.secondColumn.xs}
                        >
                          <DropdownSelect
                            idPrefix="test-center-test-sessions-room"
                            isValid={true}
                            ariaRequired={true}
                            ariaLabelledBy="test-center-test-sessions-room-label"
                            hasPlaceholder={true}
                            options={
                              // NON-STANDARD SESSIONS
                              this.props.source === TEST_SESSION_DATA_SOURCE.nonStandard
                                ? this.state.roomOptions
                                : // STANDARD SESSIONS
                                  []
                            }
                            onChange={
                              // NON-STANDARD SESSIONS
                              this.props.source === TEST_SESSION_DATA_SOURCE.nonStandard
                                ? this.getSelectedRoomOption
                                : // STANDARD SESSIONS
                                  () => {}
                            }
                            isDisabled={this.props.source === TEST_SESSION_DATA_SOURCE.standard}
                            defaultValue={this.state.roomSelectedOption}
                          />
                        </Col>
                      </Row>
                      <Row
                        role="presentation"
                        className="align-items-center"
                        style={styles.rowContainer}
                      >
                        <Col
                          xl={columnSizes.firstColumn.xl}
                          lg={columnSizes.firstColumn.lg}
                          md={columnSizes.firstColumn.md}
                          sm={columnSizes.firstColumn.sm}
                          xs={columnSizes.firstColumn.xs}
                        >
                          <label
                            id="test-center-test-sessions-open-to-ogd-label"
                            style={styles.inputTitles}
                          >
                            {
                              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                                .testSessions.topTabs.testSessionsData.editTestSessionPopup
                                .openToOgdLabel
                            }
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.secondColumn.xl}
                          lg={columnSizes.secondColumn.lg}
                          md={columnSizes.secondColumn.md}
                          sm={columnSizes.secondColumn.sm}
                          xs={columnSizes.secondColumn.xs}
                        >
                          {!this.state.isLoadingSwitch && (
                            <Switch
                              onChange={this.updateOpenToOgdSwitch}
                              checked={this.state.openToOgdSwitchState}
                              aria-labelledby="test-center-test-sessions-open-to-ogd-label"
                              height={this.state.switchHeight}
                              width={this.state.switchWidth}
                            />
                          )}
                        </Col>
                      </Row>
                      <Row
                        role="presentation"
                        className="align-items-center"
                        style={styles.rowContainer}
                      >
                        <Col
                          xl={columnSizes.firstColumn.xl}
                          lg={columnSizes.firstColumn.lg}
                          md={columnSizes.firstColumn.md}
                          sm={columnSizes.firstColumn.sm}
                          xs={columnSizes.firstColumn.xs}
                        >
                          <label
                            id="test-center-test-sessions-date-label"
                            style={styles.inputTitles}
                          >
                            {
                              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                                .testSessions.topTabs.testSessionsData.editTestSessionPopup
                                .dateLabel
                            }
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.secondColumn.xl}
                          lg={columnSizes.secondColumn.lg}
                          md={columnSizes.secondColumn.md}
                          sm={columnSizes.secondColumn.sm}
                          xs={columnSizes.secondColumn.xs}
                        >
                          <DatePicker
                            dateDayFieldRef={this.testSessionDateDayFieldRef}
                            dateLabelId={"test-center-test-sessions-date-input"}
                            ariaLabelledBy={"test-center-test-sessions-date-label"}
                            initialDateDayValue={{
                              value: Number(this.props.completeDatePicked.split("-")[2]),
                              label: `${this.props.completeDatePicked.split("-")[2]}`
                            }}
                            initialDateMonthValue={{
                              value: Number(this.props.completeDatePicked.split("-")[1]),
                              label: `${this.props.completeDatePicked.split("-")[1]}`
                            }}
                            initialDateYearValue={{
                              value: Number(this.props.completeDatePicked.split("-")[0]),
                              label: `${this.props.completeDatePicked.split("-")[0]}`
                            }}
                            onInputChange={this.handleDateUpdates}
                            customYearOptions={populateCustomFutureYearsDateOptions(1)}
                            triggerValidation={this.state.triggerDateValidation}
                            futureDateValidation={true}
                            isValidCompleteDatePicked={
                              this.state.isValidDatePickedBasedOnClosingDate
                            }
                            customInvalidDateErrorMessage={
                              !this.state.isValidDatePickedBasedOnClosingDate
                                ? LOCALIZE.testCenterManager.testCenterManagement
                                    .sideNavigationItems.testSessions.topTabs.testSessionsData
                                    .addTestSessionPopup.invalidDatePickedBasedOnClosingDate
                                : undefined
                            }
                          />
                        </Col>
                      </Row>
                      <Row
                        role="presentation"
                        className="align-items-center"
                        style={styles.rowContainer}
                      >
                        <Col
                          xl={columnSizes.firstColumn.xl}
                          lg={columnSizes.firstColumn.lg}
                          md={columnSizes.firstColumn.md}
                          sm={columnSizes.firstColumn.sm}
                          xs={columnSizes.firstColumn.xs}
                        >
                          <label
                            id="test-center-test-sessions-start-time-label"
                            style={styles.inputTitles}
                          >
                            {
                              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                                .testSessions.topTabs.testSessionsData.editTestSessionPopup
                                .startTimeLabel
                            }
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.secondColumnTime.xl}
                          lg={columnSizes.secondColumnTime.lg}
                          md={columnSizes.secondColumnTime.md}
                          sm={columnSizes.secondColumnTime.sm}
                          xs={columnSizes.secondColumnTime.xs}
                        >
                          <DropdownSelect
                            idPrefix="test-center-test-sessions-start-time-hour"
                            isValid={
                              this.state.isValidStartEndTimeCombination &&
                              this.state.isValidTimeAndDateBasedOnBookingDelay
                            }
                            ariaRequired={true}
                            ariaLabelledBy="test-center-test-sessions-start-time-label test-center-test-sessions-start-time-hour-label test-center-test-sessions-start-end-time-combination-error test-center-test-sessions-start-date-and-time-based-on-booking-delay-error"
                            hasPlaceholder={true}
                            customPlaceholderWording={LOCALIZE.datePicker.hourField}
                            options={this.state.hourOptions}
                            onChange={this.getSelectedStartTimeHour}
                            defaultValue={this.state.startTimeHour}
                          />
                        </Col>
                        <Col
                          xl={columnSizes.secondColumnTime.xl}
                          lg={columnSizes.secondColumnTime.lg}
                          md={columnSizes.secondColumnTime.md}
                          sm={columnSizes.secondColumnTime.sm}
                          xs={columnSizes.secondColumnTime.xs}
                        >
                          <DropdownSelect
                            idPrefix="test-center-test-sessions-start-time-minute"
                            isValid={
                              this.state.isValidStartEndTimeCombination &&
                              this.state.isValidTimeAndDateBasedOnBookingDelay
                            }
                            ariaRequired={true}
                            ariaLabelledBy="test-center-test-sessions-start-time-label test-center-test-sessions-start-time-minute-label test-center-test-sessions-start-end-time-combination-error test-center-test-sessions-start-date-and-time-based-on-booking-delay-error"
                            hasPlaceholder={true}
                            customPlaceholderWording={LOCALIZE.datePicker.minuteField}
                            options={this.state.minuteOptions}
                            onChange={this.getSelectedStartTimeMinute}
                            defaultValue={this.state.startTimeMinute}
                          />
                        </Col>
                      </Row>
                      <Row
                        role="presentation"
                        className="align-items-center"
                        style={styles.rowContainer}
                      >
                        <Col
                          xl={columnSizes.firstColumn.xl}
                          lg={columnSizes.firstColumn.lg}
                          md={columnSizes.firstColumn.md}
                          sm={columnSizes.firstColumn.sm}
                          xs={columnSizes.firstColumn.xs}
                          style={styles.centerText}
                        />
                        <Col
                          xl={columnSizes.secondColumnTime.xl}
                          lg={columnSizes.secondColumnTime.lg}
                          md={columnSizes.secondColumnTime.md}
                          sm={columnSizes.secondColumnTime.sm}
                          xs={columnSizes.secondColumnTime.xs}
                          style={styles.centerText}
                        >
                          <label id="test-center-test-sessions-start-time-hour-label">
                            {LOCALIZE.datePicker.hourField}
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.secondColumnTime.xl}
                          lg={columnSizes.secondColumnTime.lg}
                          md={columnSizes.secondColumnTime.md}
                          sm={columnSizes.secondColumnTime.sm}
                          xs={columnSizes.secondColumnTime.xs}
                          style={styles.centerText}
                        >
                          <label id="test-center-test-sessions-start-time-minute-label">
                            {LOCALIZE.datePicker.minuteField}
                          </label>
                        </Col>
                      </Row>
                      <Row
                        role="presentation"
                        className="align-items-center"
                        style={styles.rowContainer}
                      >
                        <Col
                          xl={columnSizes.firstColumn.xl}
                          lg={columnSizes.firstColumn.lg}
                          md={columnSizes.firstColumn.md}
                          sm={columnSizes.firstColumn.sm}
                          xs={columnSizes.firstColumn.xs}
                        >
                          <label
                            id="test-center-test-sessions-end-time-label"
                            style={styles.inputTitles}
                          >
                            {
                              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                                .testSessions.topTabs.testSessionsData.editTestSessionPopup
                                .endTimeLabel
                            }
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.secondColumnTime.xl}
                          lg={columnSizes.secondColumnTime.lg}
                          md={columnSizes.secondColumnTime.md}
                          sm={columnSizes.secondColumnTime.sm}
                          xs={columnSizes.secondColumnTime.xs}
                        >
                          <DropdownSelect
                            idPrefix="test-center-test-sessions-end-time-hour"
                            isValid={
                              this.state.isValidStartEndTimeCombination &&
                              this.state.isValidTimeAndDateBasedOnBookingDelay
                            }
                            ariaRequired={true}
                            ariaLabelledBy="test-center-test-sessions-end-time-label test-center-test-sessions-end-time-hour-label test-center-test-sessions-start-end-time-combination-error test-center-test-sessions-start-date-and-time-based-on-booking-delay-error"
                            hasPlaceholder={true}
                            customPlaceholderWording={LOCALIZE.datePicker.hourField}
                            options={this.state.hourOptions}
                            onChange={this.getSelectedEndTimeHour}
                            defaultValue={this.state.endTimeHour}
                          />
                        </Col>
                        <Col
                          xl={columnSizes.secondColumnTime.xl}
                          lg={columnSizes.secondColumnTime.lg}
                          md={columnSizes.secondColumnTime.md}
                          sm={columnSizes.secondColumnTime.sm}
                          xs={columnSizes.secondColumnTime.xs}
                        >
                          <DropdownSelect
                            idPrefix="test-center-test-sessions-end-time-minute"
                            isValid={
                              this.state.isValidStartEndTimeCombination &&
                              this.state.isValidTimeAndDateBasedOnBookingDelay
                            }
                            ariaRequired={true}
                            ariaLabelledBy="test-center-test-sessions-end-time-label test-center-test-sessions-end-time-minute-label test-center-test-sessions-start-end-time-combination-error test-center-test-sessions-start-date-and-time-based-on-booking-delay-error"
                            hasPlaceholder={true}
                            customPlaceholderWording={LOCALIZE.datePicker.minuteField}
                            options={this.state.minuteOptions}
                            onChange={this.getSelectedEndTimeMinute}
                            defaultValue={this.state.endTimeMinute}
                          />
                        </Col>
                      </Row>
                      <Row
                        role="presentation"
                        className="align-items-center"
                        style={styles.rowContainer}
                      >
                        <Col
                          xl={columnSizes.firstColumn.xl}
                          lg={columnSizes.firstColumn.lg}
                          md={columnSizes.firstColumn.md}
                          sm={columnSizes.firstColumn.sm}
                          xs={columnSizes.firstColumn.xs}
                          style={styles.centerText}
                        />
                        <Col
                          xl={columnSizes.secondColumnTime.xl}
                          lg={columnSizes.secondColumnTime.lg}
                          md={columnSizes.secondColumnTime.md}
                          sm={columnSizes.secondColumnTime.sm}
                          xs={columnSizes.secondColumnTime.xs}
                          style={styles.centerText}
                        >
                          <label id="test-center-test-sessions-end-time-hour-label">
                            {LOCALIZE.datePicker.hourField}
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.secondColumnTime.xl}
                          lg={columnSizes.secondColumnTime.lg}
                          md={columnSizes.secondColumnTime.md}
                          sm={columnSizes.secondColumnTime.sm}
                          xs={columnSizes.secondColumnTime.xs}
                          style={styles.centerText}
                        >
                          <label id="test-center-test-sessions-end-time-minute-label">
                            {LOCALIZE.datePicker.minuteField}
                          </label>
                        </Col>
                      </Row>
                      {!this.state.isValidStartEndTimeCombination && (
                        <Row
                          role="presentation"
                          className="align-items-center justify-content-end"
                          style={styles.rowContainerWithError}
                        >
                          <Col
                            xl={columnSizes.secondColumn.xl}
                            lg={columnSizes.secondColumn.lg}
                            md={columnSizes.secondColumn.md}
                            sm={columnSizes.secondColumn.sm}
                            xs={columnSizes.secondColumn.xs}
                          >
                            <label
                              id="test-center-test-sessions-start-end-time-combination-error"
                              style={styles.errorMessage}
                            >
                              {
                                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                                  .testSessions.topTabs.testSessionsData.editTestSessionPopup
                                  .invalidStartEndTimeCombination
                              }
                            </label>
                          </Col>
                        </Row>
                      )}
                      {!this.state.isValidTimeAndDateBasedOnBookingDelay && (
                        <Row
                          role="presentation"
                          className="align-items-center justify-content-end"
                          style={styles.rowContainerWithError}
                        >
                          <Col
                            xl={columnSizes.secondColumn.xl}
                            lg={columnSizes.secondColumn.lg}
                            md={columnSizes.secondColumn.md}
                            sm={columnSizes.secondColumn.sm}
                            xs={columnSizes.secondColumn.xs}
                          >
                            <label
                              id="test-center-test-sessions-start-end-time-combination-error"
                              style={styles.errorMessage}
                            >
                              {
                                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                                  .testSessions.topTabs.testSessionsData.editTestSessionPopup
                                  .invalidStartDateAndTimeBasedOnBookingDelay
                              }
                            </label>
                          </Col>
                        </Row>
                      )}
                      <Row
                        role="presentation"
                        className="align-items-center"
                        style={
                          this.state.isValidSpacesAvailable
                            ? styles.rowContainer
                            : styles.rowContainerWithError
                        }
                      >
                        <Col
                          xl={columnSizes.firstColumn.xl}
                          lg={columnSizes.firstColumn.lg}
                          md={columnSizes.firstColumn.md}
                          sm={columnSizes.firstColumn.sm}
                          xs={columnSizes.firstColumn.xs}
                        >
                          <label
                            htmlFor="test-center-test-sessions-spaces-available-input"
                            style={styles.inputTitles}
                          >
                            {
                              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                                .testSessions.topTabs.testSessionsData.editTestSessionPopup
                                .spacesAvailableLabel
                            }
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.secondColumn.xl}
                          lg={columnSizes.secondColumn.lg}
                          md={columnSizes.secondColumn.md}
                          sm={columnSizes.secondColumn.sm}
                          xs={columnSizes.secondColumn.xs}
                        >
                          <input
                            id="test-center-test-sessions-spaces-available-input"
                            className={
                              this.state.isValidSpacesAvailable ? "valid-field" : "invalid-field"
                            }
                            style={{ ...styles.input, ...accommodationsStyle }}
                            type="text"
                            value={this.state.spacesAvailable}
                            onChange={this.updateSpacesAvailable}
                          ></input>
                        </Col>
                      </Row>
                      {!this.state.isValidSpacesAvailable && (
                        <Row
                          role="presentation"
                          className="align-items-center justify-content-end"
                          style={styles.rowContainerWithError}
                        >
                          <Col
                            xl={columnSizes.secondColumn.xl}
                            lg={columnSizes.secondColumn.lg}
                            md={columnSizes.secondColumn.md}
                            sm={columnSizes.secondColumn.sm}
                            xs={columnSizes.secondColumn.xs}
                          >
                            <label
                              htmlFor="test-center-test-sessions-spaces-available-input"
                              style={styles.errorMessage}
                            >
                              {
                                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                                  .testSessions.topTabs.testSessionsData.editTestSessionPopup
                                  .invalidSpacesAvailableError
                              }
                            </label>
                          </Col>
                        </Row>
                      )}

                      <Row
                        role="presentation"
                        className="align-items-center"
                        style={styles.rowContainer}
                      >
                        <Col
                          xl={columnSizes.firstColumn.xl}
                          lg={columnSizes.firstColumn.lg}
                          md={columnSizes.firstColumn.md}
                          sm={columnSizes.firstColumn.sm}
                          xs={columnSizes.firstColumn.xs}
                        >
                          <label
                            id="test-center-test-sessions-test-skill-type-label"
                            style={styles.inputTitles}
                          >
                            {
                              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                                .testSessions.topTabs.testSessionsData.editTestSessionPopup
                                .testSkillType
                            }
                          </label>
                        </Col>
                        <Col
                          xl={columnSizes.secondColumn.xl}
                          lg={columnSizes.secondColumn.lg}
                          md={columnSizes.secondColumn.md}
                          sm={columnSizes.secondColumn.sm}
                          xs={columnSizes.secondColumn.xs}
                        >
                          <DropdownSelect
                            idPrefix="test-center-test-sessions-test-skill-type"
                            isValid={true}
                            ariaRequired={true}
                            ariaLabelledBy="test-center-test-sessions-test-skill-type-label"
                            hasPlaceholder={true}
                            menuPlacement="top"
                            options={[]}
                            onChange={() => {}}
                            isDisabled={true}
                            defaultValue={this.state.testSkillTypeSelectedOption}
                          />
                        </Col>
                      </Row>
                      {this.state.testSkillSubTypeOptions.length > 0 && (
                        <Row
                          role="presentation"
                          className="align-items-center"
                          style={styles.rowContainer}
                        >
                          <Col
                            xl={columnSizes.firstColumn.xl}
                            lg={columnSizes.firstColumn.lg}
                            md={columnSizes.firstColumn.md}
                            sm={columnSizes.firstColumn.sm}
                            xs={columnSizes.firstColumn.xs}
                          >
                            <label
                              id="test-center-test-sessions-test-skill-sub-type-label"
                              style={styles.inputTitles}
                            >
                              {
                                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                                  .testSessions.topTabs.testSessionsData.editTestSessionPopup
                                  .testSkillSubType
                              }
                            </label>
                          </Col>
                          <Col
                            xl={columnSizes.secondColumn.xl}
                            lg={columnSizes.secondColumn.lg}
                            md={columnSizes.secondColumn.md}
                            sm={columnSizes.secondColumn.sm}
                            xs={columnSizes.secondColumn.xs}
                          >
                            <DropdownSelect
                              idPrefix="test-center-test-sessions-test-skill-sub-type"
                              isValid={true}
                              ariaRequired={true}
                              ariaLabelledBy="test-center-test-sessions-test-skill-sub-type-label"
                              hasPlaceholder={true}
                              menuPlacement="top"
                              options={[]}
                              onChange={() => {}}
                              isDisabled={true}
                              defaultValue={this.state.testSkillSubTypeSelectedOption}
                            />
                          </Col>
                        </Row>
                      )}
                    </>
                  ) : (
                    <div>
                      <TopTabs
                        TABS={this.state.conditionalContentTabs}
                        defaultTab={`test-session-1`}
                        customStyle={styles.contentTab}
                        containsAddTabButton={true}
                        addTabButtonAction={this.handleAddTestSessionTabPopup}
                        tabsCustomStyle={styles.tabNavigation}
                        tabClassName={"top-tabs-transparent-tab-style"}
                        tabCustomStyle={styles.tabContainer}
                      />
                    </div>
                  )}
                </div>
              ) : (
                <SystemMessage
                  messageType={MESSAGE_TYPE.success}
                  title={LOCALIZE.commons.success}
                  message={
                    // STANDARD SESSIONS
                    this.props.source === TEST_SESSION_DATA_SOURCE.standard ? (
                      <p>
                        {LOCALIZE.formatString(
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                            .testSessions.topTabs.testSessionsData.editTestSessionPopup
                            .systemMessageDescription,
                          <span style={styles.boldText}>
                            {this.state.selectedTestSessionToViewEditOrDelete.room_name}
                          </span>,
                          <span style={styles.boldText}>
                            {`${getFormattedDate(this.props.completeDatePicked)} ${
                              this.state.startTimeHour.label
                            }:${this.state.startTimeMinute.label} - ${
                              this.state.endTimeHour.label
                            }:${this.state.endTimeMinute.label}`}
                          </span>
                        )}
                      </p>
                    ) : (
                      <p>
                        {
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                            .testSessions.topTabs.testSessionsData.editTestSessionPopup
                            .systemMessageDescriptionNonStandard
                        }
                      </p>
                    )
                  }
                />
              )}
            </>
          }
          leftButtonType={
            !this.state.testSessionUpdatedSuccessfully ? BUTTON_TYPE.secondary : BUTTON_TYPE.none
          }
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeEditTestSessionPopup}
          leftButtonState={
            !this.state.isLoadingEditTestSession ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            !this.state.testSessionUpdatedSuccessfully ? (
              this.state.isLoadingEditTestSession ? (
                <div style={styles.customLoadingContainer}>
                  {/* this div is useful to get the right size of the button while loading */}
                  <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
                    <FontAwesomeIcon icon={faSave} style={styles.icon} />
                    {LOCALIZE.commons.saveButton}
                  </div>
                  <div style={styles.loadingOverlappingStyle}>
                    <label className="fa fa-spinner fa-spin">
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  </div>
                </div>
              ) : (
                LOCALIZE.commons.saveButton
              )
            ) : (
              LOCALIZE.commons.ok
            )
          }
          rightButtonAction={
            !this.state.testSessionUpdatedSuccessfully
              ? this.props.source === TEST_SESSION_DATA_SOURCE.nonStandard
                ? this.handleEditNonStandardTestSession
                : this.handleEditTestSession
              : this.closeEditTestSessionPopup
          }
          rightButtonIcon={
            !this.state.testSessionUpdatedSuccessfully
              ? !this.state.isLoadingEditTestSession
                ? faSave
                : ""
              : faTimes
          }
          rightButtonState={
            !this.state.testSessionUpdatedSuccessfully
              ? this.state.isLoadingEditTestSession
                ? BUTTON_STATE.disabled
                : this.props.source === TEST_SESSION_DATA_SOURCE.nonStandard
                  ? this.props.testCenterNonStandardTestSessionValidFormState
                    ? BUTTON_STATE.enabled
                    : BUTTON_STATE.disabled
                  : this.state.isValidForm
                    ? BUTTON_STATE.enabled
                    : BUTTON_STATE.disabled
              : BUTTON_STATE.enabled
          }
        />
        {Object.keys(this.state.selectedTestSessionToViewEditOrDelete).length > 0 && (
          <PopupBox
            show={this.state.showDeleteTestSessionPopup}
            handleClose={() => {}}
            title={
              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                .topTabs.testSessionsData.deleteTestSessionPopup.title
            }
            size="lg"
            description={
              <div>
                <SystemMessage
                  messageType={MESSAGE_TYPE.error}
                  title={LOCALIZE.commons.warning}
                  message={
                    <p>
                      {this.props.source === TEST_SESSION_DATA_SOURCE.standard
                        ? LOCALIZE.formatString(
                            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                              .testSessions.topTabs.testSessionsData.deleteTestSessionPopup
                              .systemMessageDescription,
                            <span style={styles.boldText}>
                              {this.state.selectedTestSessionToViewEditOrDelete.room_name}
                            </span>,
                            <span style={styles.boldText}>
                              {this.state.selectedTestSessionToViewEditOrDelete.formatted_date}
                            </span>
                          )
                        : LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                            .testSessions.topTabs.testSessionsData.deleteTestSessionPopup
                            .systemMessageDescriptionNonStandard}
                    </p>
                  }
                />
                <p>
                  {
                    LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                      .topTabs.testSessionsData.deleteTestSessionPopup.description
                  }
                </p>
              </div>
            }
            leftButtonType={BUTTON_TYPE.secondary}
            leftButtonTitle={LOCALIZE.commons.cancel}
            leftButtonIcon={faTimes}
            leftButtonAction={this.closeDeleteRoomPopup}
            leftButtonState={
              !this.state.isLoadingDeleteTestSession ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
            }
            rightButtonType={BUTTON_TYPE.danger}
            rightButtonTitle={
              this.state.isLoadingDeleteTestSession ? (
                <div style={styles.customLoadingContainer}>
                  {/* this div is useful to get the right size of the button while loading */}
                  <div style={{ ...styles.loadingOverlappingStyle, ...styles.transparentText }}>
                    <FontAwesomeIcon icon={faTrashAlt} style={styles.icon} />
                    {LOCALIZE.commons.deleteButton}
                  </div>
                  <div style={styles.loadingOverlappingStyle}>
                    <label className="fa fa-spinner fa-spin">
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  </div>
                </div>
              ) : (
                LOCALIZE.commons.deleteButton
              )
            }
            rightButtonAction={
              !this.state.isLoadingDeleteTestSession ? this.handleDeleteTestSession : () => {}
            }
            rightButtonIcon={!this.state.isLoadingDeleteTestSession ? faTrashAlt : ""}
            rightButtonState={
              !this.state.isLoadingDeleteTestSession ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
            }
          />
        )}
        <PopupBox
          show={this.state.showTestSessionTimeOverlappingErrorPopup}
          handleClose={() => {}}
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
              .testSessionsData.timeOverlappingErrorPopup.title
          }
          size="lg"
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.error}
                message={
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.timeOverlappingErrorPopup
                        .systemMessageDescription
                    }
                  </p>
                }
              />
              <p>
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                    .topTabs.testSessionsData.timeOverlappingErrorPopup.description
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonAction={this.closeTimeOverlappingErrorPopup}
          rightButtonIcon={faTimes}
        />
        {Object.keys(this.state.selectedTestSessionToViewEditOrDelete).length > 0 && (
          <PopupBox
            show={this.state.showTestSessionAttendeesPopup}
            handleClose={() => {}}
            title={
              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                .topTabs.testSessionsData.viewAttendeesPopup.title
            }
            description={
              <div>
                <div>
                  <ul>
                    <li>
                      {LOCALIZE.formatString(
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                          .testSessions.topTabs.testSessionsData.viewAttendeesPopup.description1,
                        <span style={styles.boldText}>
                          {this.state.selectedTestSessionToViewEditOrDelete.formatted_date}
                        </span>
                      )}
                    </li>
                    <li>
                      {LOCALIZE.formatString(
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                          .testSessions.topTabs.testSessionsData.viewAttendeesPopup.description2,
                        <span style={styles.boldText}>
                          {this.state.selectedTestSessionToViewEditOrDelete.room_name}
                        </span>
                      )}
                    </li>
                    <li>
                      {LOCALIZE.formatString(
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                          .testSessions.topTabs.testSessionsData.viewAttendeesPopup.description3,
                        <span style={styles.boldText}>
                          {this.state.selectedTestSessionToViewEditOrDelete.formatted_test_skill}
                        </span>
                      )}
                    </li>
                  </ul>
                </div>
                <div>
                  <GenericTable
                    classnamePrefix="test-session-attendees"
                    columnsDefinition={testSessionAttendeesColumnsDefinition}
                    rowsDefinition={this.state.testSessionAttendeesRowsDefinition}
                    emptyTableMessage={
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.viewAttendeesPopup.table.noData
                    }
                    currentlyLoading={this.state.testSessionAttendeesLoading}
                  />
                </div>
                {this.state.triggerReactToPrint && (
                  <ReactToPrint
                    contentRef={this.componentToPrintRef}
                    documentTitle={LOCALIZE.formatString(
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.viewAttendeesPopup
                        .printDocumentTitle,
                      this.state.selectedTestSessionToViewEditOrDelete.formatted_date
                    )}
                    content={getContentToPrint(
                      testSessionAttendeesColumnsDefinition,
                      this.state.selectedTestSessionToViewEditOrDelete,
                      this.state.testSessionAttendeesRowsDefinition
                    )}
                  />
                )}
              </div>
            }
            leftButtonType={BUTTON_TYPE.secondary}
            leftButtonTitle={
              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                .topTabs.testSessionsData.viewAttendeesPopup.printButton
            }
            leftButtonIcon={faPrint}
            leftButtonAction={this.handlePrint}
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={LOCALIZE.commons.close}
            rightButtonIcon={faTimes}
            rightButtonAction={this.closeTestSessionAttendeesPopup}
          />
        )}
        {Object.keys(this.state.selectedAccommodationRequestData).length > 0 &&
          this.state.showAccommodationsRequestDetailsPopup && (
            <AccommodationsRequestDetailsPopup
              source={ACCOMMODATION_REQUEST_DETAILS_POPUP_SOURCE.tcm}
              userAccommodationFileId={
                this.state.selectedAccommodationRequestData.user_accommodation_file_id
              }
              selectedUserAccommodationFileData={this.state.selectedAccommodationRequestData}
              showPopup={this.state.showAccommodationsRequestDetailsPopup}
              closeAccommodationsRequestDetailsPopup={this.closeAccommodationsRequestDetailsPopup}
              // at this point, the report should be completed, so no draft view for this one
              isDraft={false}
            />
          )}
        <PopupBox
          show={this.state.showDeleteTestSessionTabPopup}
          handleClose={() => {}}
          size="lg"
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
              .testSessionsData.addTestSessionPopup.deleteTestSessionTabPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.addTestSessionPopup
                        .deleteTestSessionTabPopup.systemMessageDescription
                    }
                  </p>
                }
              />
              <p>
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                    .topTabs.testSessionsData.addTestSessionPopup.deleteTestSessionTabPopup
                    .description
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeDeleteTestSessionTabPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.handleDeleteTestSessionTab}
          rightButtonIcon={faTrashAlt}
        />
        <PopupBox
          show={this.state.showInvalidTestSessionDateAndTimeCombinationPopup}
          handleClose={() => {}}
          size="lg"
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
              .testSessionsData.addTestSessionPopup.invalidTestSessionDateAndTimeCombinationPopup
              .title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testSessions.topTabs.testSessionsData.addTestSessionPopup
                        .invalidTestSessionDateAndTimeCombinationPopup.systemMessageDescription
                    }
                  </p>
                }
              />
              <p>
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions
                    .topTabs.testSessionsData.addTestSessionPopup
                    .invalidTestSessionDateAndTimeCombinationPopup.description
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonAction={this.closeInvalidTestSessionDateAndTimeCombinationPopup}
          rightButtonIcon={faTimes}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testCenterData: state.testCenter.testCenterData,
    testCenterTestSessionsPaginationPage: state.testCenter.testCenterTestSessionsPaginationPage,
    testCenterTestSessionsPaginationPageSize:
      state.testCenter.testCenterTestSessionsPaginationPageSize,
    testCenterTestSessionsKeyword: state.testCenter.testCenterTestSessionsKeyword,
    testCenterTestSessionsActiveSearch: state.testCenter.testCenterTestSessionsActiveSearch,
    completeDatePicked: state.datePicker.completeDatePicked,
    completeDateValidState: state.datePicker.completeDateValidState,
    triggerTableUpdatesState: state.testCenter.triggerTableUpdatesState,
    testCenterNonStandardTestSessionData: state.testCenter.testCenterNonStandardTestSessionData,
    testCenterNonStandardTestSessionValidFormState:
      state.testCenter.testCenterNonStandardTestSessionValidFormState
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getUsersBasedOnSpecifiedPermission,
      associateTestAdministratorsToTestCenter,
      getAssociatedTestAdministratorsToTestCenter,
      getFoundAssociatedTestAdministratorsToTestCenter,
      updateCurrentTestCenterTestSessionsPageState,
      updateTestCenterTestSessionsPageSizeState,
      updateSearchTestCenterTestSessionsStates,
      deleteTestAdministratorFromTestCenter,
      addTestSessionToTestCenter,
      getTestCenterTestSessions,
      getFoundTestCenterTestSessions,
      editTestCenterTestSession,
      deleteTestCenterTestSession,
      updateDatePicked,
      updateDatePickedValidState,
      getTestSkillTypes,
      getTestSkillSubTypes,
      getTestSessionAttendees,
      triggerTableUpdates,
      getDataOfTestCenterTestSessionToViewOrEdit,
      updateTestSessionNonStandardTestSessionData,
      editNonStandardTestCenterTestSession
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestSessionsData);
