import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import TestSessionsData, { TEST_SESSION_DATA_SOURCE } from "./TestSessionsData";
import { setTestCenterTestSessionsSelectedTab } from "../../../modules/TestCenterRedux";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  }
};

class TestSessions extends Component {
  render() {
    const TABS = [
      {
        key: "test-sessions-standard-sessions",
        tabName:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
            .testSessionsData.standardTitle,
        body: <TestSessionsData source={TEST_SESSION_DATA_SOURCE.standard} />
      },
      {
        key: "test-sessions-non-standard-sessions",
        tabName:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.topTabs
            .testSessionsData.nonStandardTitle,
        body: <TestSessionsData source={TEST_SESSION_DATA_SOURCE.nonStandard} />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>
            {LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.title}
          </h2>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey={
                  this.props.testCenterTestSessionsSelectedTab !== "" &&
                  this.props.testCenterTestSessionsSelectedTab !== null
                    ? this.props.testCenterTestSessionsSelectedTab
                    : "test-sessions-standard-sessions"
                }
                id="test-sessions-tabs"
                style={styles.tabNavigation}
                onSelect={event => this.props.setTestCenterTestSessionsSelectedTab(event)}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testCenterData: state.testCenter.testCenterData,
    testCenterTestSessionsSelectedTab: state.testCenter.testCenterTestSessionsSelectedTab
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setTestCenterTestSessionsSelectedTab
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestSessions);
