/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import ContentContainer from "../commons/ContentContainer";
import SideNavigation from "../eMIB/SideNavigation";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowAltCircleLeft,
  faCheck,
  faSpinner,
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import { history } from "../../store-index";
import { PATH } from "../commons/Constants";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import {
  resetSelectedTestCenterStates,
  setTestCenterDataSideNavState
} from "../../modules/TestCenterRedux";
import TestCenterManagement from "./testCenterManagement/TestCenterManagement";
import TestSessions from "./testSessions/TestSessions";
import AccommodationRequests from "./accommodationRequests/AccommodationRequests";
import Ola from "./ola/Ola";

const styles = {
  loadingContainer: {
    margin: "24px 0",
    textAlign: "center"
  },
  loading: {
    width: "100%",
    height: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  },
  backButtonStyle: {
    marginBottom: 24
  },
  buttonLabel: {
    marginLeft: 6
  },
  sectionContainerLabelDiv: {
    paddingLeft: 18,
    marginBottom: "-3px"
  },
  sectionContainerLabel: {
    padding: "4px 24px",
    marginBottom: 0,
    position: "relative"
  },
  tabStyleBorder: {
    borderBottom: "5px solid #00565e",
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    borderRadius: "4px 4px 0 0"
  },
  sectionContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    minHeight: 400,
    padding: "12px 0",
    marginBottom: 24
  },
  tabContainer: {
    zIndex: 1,
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent",
    overflowY: "auto"
  },
  nav: {
    marginTop: 10,
    marginLeft: 10,
    width: 206,
    textAlign: "center"
  },
  body: {
    paddingLeft: 12
  },
  sideNavBodyContent: {
    marginTop: "-12px"
  },
  appPadding: {
    padding: "15px"
  }
};

class TestCenterData extends Component {
  state = {
    isLoading: true,
    showBackToTestCenterManagerDashboardPopup: false,
    triggerRerender: false
  };

  componentDidMount = () => {
    // if testCenterData is defined
    if (
      typeof this.props.testCenterData !== "undefined" &&
      Object.keys(this.props.testCenterData).length > 0
    ) {
      this.setState({ isLoading: false });
      // a user is trying to access the test center accesses page while the props are not properly set (like trying to manually access the URL for example)
    } else {
      // waiting 3 seconds
      setTimeout(() => {
        // checking if the testCenterData is now defined
        if (
          typeof this.props.testCenterData !== "undefined" &&
          Object.keys(this.props.testCenterData).length > 0
        ) {
          this.setState({ isLoading: false });
        } else {
          // redirecting user to the Test Center Manager dashboard page
          history.push(PATH.testCenterManager);
        }
      }, 3000);
    }
  };

  componentDidUpdate = prevProps => {
    // if testCenterData gets updated
    if (prevProps.testCenterData !== this.props.testCenterData) {
      // testCenterData is defined
      if (
        typeof this.props.testCenterData !== "undefined" &&
        Object.keys(this.props.testCenterData).length > 0
      ) {
        this.setState({
          isLoading: false
        });
      }
    }
    // if triggerRerender gets updated
    if (prevProps.triggerRerender !== this.props.triggerRerender) {
      this.setState({
        triggerRerender: !this.state.triggerRerender
      });
    }
  };

  openBackToTestCenterManagerDashboardPopup = () => {
    this.setState({ showBackToTestCenterManagerDashboardPopup: true });
  };

  closeBackToTestCenterManagerPopup = () => {
    this.setState({ showBackToTestCenterManagerDashboardPopup: false });
  };

  handleBackToTestCenterManagerDashboard = () => {
    // resetting test center redux states
    this.props.resetSelectedTestCenterStates();
    // redirecting user to the Test Center Manager dashboard page
    history.push(PATH.testCenterManager);
  };

  getTestCenterManagementSections = () => {
    const sideNavArray = [
      {
        menuString:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testSessions.title,
        body: <TestSessions />
      },
      {
        menuString:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.accommodationRequests
            .title,
        body: <AccommodationRequests />
      },
      {
        menuString:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
            .title,
        body: <TestCenterManagement />
      }
    ];

    // if OLA authorized
    if (this.props.testCenterData.ola_authorized) {
      // add OLA side nav item
      sideNavArray.push({
        menuString: LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.ola.title,
        body: <Ola />
      });
    }

    return sideNavArray;
  };

  // rendu là

  render() {
    const specs = this.getTestCenterManagementSections();
    return (
      <div className="app" style={styles.appPadding}>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title className="notranslate">{LOCALIZE.titles.testCenterManager}</title>
        </Helmet>
        <ContentContainer>
          <div>
            {this.state.isLoading ? (
              <div style={styles.loadingContainer}>
                <label className="fa fa-spinner fa-spin" style={styles.loading}>
                  <FontAwesomeIcon icon={faSpinner} />
                </label>
              </div>
            ) : (
              <div>
                <div style={styles.backButtonStyle}>
                  <CustomButton
                    label={
                      <>
                        <FontAwesomeIcon icon={faArrowAltCircleLeft} />
                        <span style={styles.buttonLabel}>
                          {
                            LOCALIZE.testCenterManager.testCenterManagement
                              .backToTestCenterManagement.buttonTitle
                          }
                        </span>
                      </>
                    }
                    // only showing popup if data has been changed, but not saved yet
                    action={
                      this.props.testCenterInfoContainsChanges
                        ? this.openBackToTestCenterManagerDashboardPopup
                        : this.handleBackToTestCenterManagerDashboard
                    }
                    buttonTheme={THEME.SECONDARY}
                  />
                </div>
                <div style={styles.sectionContainerLabelDiv}>
                  <div>
                    <label style={styles.sectionContainerLabel}>
                      {LOCALIZE.formatString(
                        LOCALIZE.testCenterManager.testCenterManagement.topTabTitle,
                        this.props.testCenterData.name
                      )}
                      <span style={styles.tabStyleBorder}></span>
                    </label>
                  </div>
                </div>
                <div style={styles.sectionContainer}>
                  <section aria-label={LOCALIZE.ariaLabel.sideNavigationSection}>
                    <SideNavigation
                      specs={specs}
                      startIndex={this.props.testCenterDataSelectedSideNavItem}
                      displayNextPreviousButton={false}
                      isMain={true}
                      tabContainerStyle={styles.tabContainer}
                      tabContentStyle={styles.tabContent}
                      navStyle={styles.nav}
                      bodyContentCustomStyle={styles.sideNavBodyContent}
                      updateSelectedSideNavItem={this.props.setTestCenterDataSideNavState}
                    />
                  </section>
                </div>
              </div>
            )}
          </div>
        </ContentContainer>
        <PopupBox
          show={this.state.showBackToTestCenterManagerDashboardPopup}
          handleClose={this.closeBackToTestCenterManagerPopup}
          title={
            LOCALIZE.testCenterManager.testCenterManagement.backToTestCenterManagement.popup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.warning}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.backToTestCenterManagement
                        .popup.warningDescription
                    }
                  </p>
                }
              />
              <p>
                {
                  LOCALIZE.testCenterManager.testCenterManagement.backToTestCenterManagement.popup
                    .description
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.confirm}
          rightButtonAction={this.handleBackToTestCenterManagerDashboard}
          rightButtonIcon={faCheck}
          size="lg"
        />
      </div>
    );
  }
}

export { TestCenterData as unconnectedTestCenterData };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testCenterData: state.testCenter.testCenterData,
    triggerRerender: state.testCenter.triggerRerender,
    testCenterInfoContainsChanges: state.testCenter.testCenterInfoContainsChanges,
    testCenterDataSelectedSideNavItem: state.testCenter.testCenterDataSelectedSideNavItem
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      resetSelectedTestCenterStates,
      setTestCenterDataSideNavState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestCenterData);
