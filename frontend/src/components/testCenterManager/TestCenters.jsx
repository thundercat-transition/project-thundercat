import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import { styles as SystemAdministrationStyles } from "../etta/SystemAdministration";
import ContentContainer from "../commons/ContentContainer";
import LastLogin from "../authentication/LastLogin";
import { PATH } from "../commons/Constants";
import SearchBarWithDisplayOptions from "../commons/SearchBarWithDisplayOptions";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import Pagination from "../commons/Pagination";
import {
  updateSearchTestCentersStates,
  updateCurrentTestCentersPageState,
  updateTestCentersPageSizeState,
  getAssociatedTestCenters,
  getFoundAssociatedTestCenters,
  setTestCenterData,
  getSelectedTestCenterData
} from "../../modules/TestCenterRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars } from "@fortawesome/free-solid-svg-icons";
import { faEnvelope } from "@fortawesome/free-regular-svg-icons";
import StyledTooltip, { EFFECT, TYPE } from "../authentication/StyledTooltip";
import CustomButton from "../commons/CustomButton";
import THEME from "../commons/CustomButtonTheme";
import { history } from "../../store-index";

export const styles = {
  appPadding: {
    padding: "15px"
  },
  tableContainer: {
    padding: "12px 24px"
  },
  allUnset: {
    all: "unset"
  },
  viewIcon: {
    transform: "scale(1.2)"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  pendingRequestIcon: {
    margin: "-1px",
    transform: "scale(1.2)"
  },
  pendingRequestLabel: {
    paddingLeft: 5,
    marginTop: "-4px",
    position: "absolute",
    fontSize: "70%",
    fontWeight: "bold"
  }
};

class TestCenters extends Component {
  state = {
    isTestCenterManagerDashboard: false,
    resultsFound: 0,
    testCenters: [],
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    currentlyLoading: false,
    clearSearchTriggered: false
  };

  componentDidMount = () => {
    // to display last login if it is a Test Center Manager dashboard
    if (this.props.currentHomePage === PATH.testCenterManager) {
      this.setState({
        isTestCenterManagerDashboard: true
      });
    }
    // initialize pagination page redux state
    this.props.updateCurrentTestCentersPageState(1);
    this.populateTestCentersBasedOnActiveSearch();
  };

  componentDidUpdate = prevProps => {
    // // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateTestCentersBasedOnActiveSearch();
    }
    // // if testCentersPaginationPage gets updated
    if (prevProps.testCentersPaginationPage !== this.props.testCentersPaginationPage) {
      this.populateTestCentersBasedOnActiveSearch();
    }
    // // if testCentersPaginationPageSize get updated
    if (prevProps.testCentersPaginationPageSize !== this.props.testCentersPaginationPageSize) {
      this.populateTestCentersBasedOnActiveSearch();
    }
    // // if search testCentersKeyword gets updated
    if (prevProps.testCentersKeyword !== this.props.testCentersKeyword) {
      // if testCentersKeyword redux state is empty
      if (this.props.testCentersKeyword !== "") {
        this.populateFoundTestCenters();
      } else if (this.props.testCentersKeyword === "" && this.props.testCentersActiveSearch) {
        this.populateFoundTestCenters();
      }
    }
    // // if testCentersActiveSearch gets updated
    if (prevProps.testCentersActiveSearch !== this.props.testCentersActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.testCentersActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateTestCenters();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundTestCenters();
      }
    }
  };

  populateTestCentersBasedOnActiveSearch = () => {
    // current search
    if (this.props.testCentersActiveSearch) {
      this.populateFoundTestCenters();
      // no search
    } else {
      this.populateTestCenters();
    }
  };

  // populate Test Centers
  populateTestCenters = () => {
    this._isMounted = true;
    this.setState({ currentlyLoading: true }, () => {
      const testCentersArray = [];
      this.props
        .getAssociatedTestCenters(
          this.props.testCentersPaginationPage,
          this.props.testCentersPaginationPageSize
        )
        .then(response => {
          if (this._isMounted) {
            this.populateTestCentersObject(testCentersArray, response);
          }
        })
        .then(() => {
          if (this._isMounted) {
            if (this.state.clearSearchTriggered) {
              // go back to the first page to avoid display bugs
              this.props.updateCurrentTestCentersPageState(1);
              this.setState({ clearSearchTriggered: false });
            }
          }
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({ currentlyLoading: false });
          }
        });
    });
  };

  // populating found test centers based on a search
  populateFoundTestCenters = () => {
    // making sure that the table is not currently loading
    if (!this.state.currentlyLoading) {
      this.setState(
        { currentlyLoading: true },
        () => {
          const testCentersArray = [];
          setTimeout(() => {
            this.props
              .getFoundAssociatedTestCenters(
                this.props.testCentersKeyword,
                this.props.testCentersPaginationPage,
                this.props.testCentersPaginationPageSize
              )
              .then(response => {
                // there are no results found
                if (response[0] === "no results found") {
                  this.setState({
                    testCenters: [],
                    rowsDefinition: {},
                    numberOfPages: 1,
                    nextPageNumber: 0,
                    previousPageNumber: 0,
                    resultsFound: 0
                  });
                  // there is at least one result found
                } else {
                  this.populateTestCentersObject(testCentersArray, response);
                  this.setState({ resultsFound: response.count });
                }
              })
              .then(() => {
                this.setState(
                  {
                    currentlyLoading: false
                  },
                  () => {
                    // if there is at least one result found
                    if (testCentersArray.length > 0) {
                      // make sure that this element exsits to avoid any error
                      if (document.getElementById("test-centers-results-found")) {
                        document.getElementById("test-centers-results-found").focus();
                      }
                      // no results found
                    } else {
                      // focusing on no results found row
                      document.getElementById("test-centers-no-data").focus();
                    }
                  }
                );
              });
          });
        },
        250
      );
    }
  };

  populateTestCentersObject = (testCentersArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in testCentersArray
        testCentersArray.push({
          id: currentResult.id,
          name: currentResult.name,
          address: currentResult.test_center_address_text[this.props.currentLanguage][0].text,
          number_of_rooms: currentResult.number_of_rooms,
          number_of_tas: currentResult.number_of_tas
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult.name,
          column_2: currentResult.test_center_address_text[this.props.currentLanguage][0].text,
          column_3: currentResult.number_of_rooms,
          column_4: currentResult.number_of_tas,
          column_5:
            currentResult.pending_accommodation_requests > 0 ? (
              <>
                <FontAwesomeIcon icon={faEnvelope} style={styles.pendingRequestIcon} />

                <span
                  style={styles.pendingRequestLabel}
                >{`${currentResult.pending_accommodation_requests}`}</span>
              </>
            ) : (
              ""
            ),
          column_6: (
            <StyledTooltip
              id={`test-center-edit-row-${i}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`test-center-edit-row-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faBinoculars} style={styles.viewIcon} />
                      </>
                    }
                    action={() => {
                      this.editTestCenter(testCentersArray[i].id);
                    }}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.PRIMARY}
                    ariaLabel={LOCALIZE.formatString(
                      LOCALIZE.testCenterManager.table.viewButtonAccessibility,
                      currentResult.name
                    )}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>{LOCALIZE.testCenterManager.table.viewButtonTooltip}</p>
                </div>
              }
            />
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.LEFT_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      column_5_style: COMMON_STYLE.CENTERED_TEXT,
      column_6_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState({
      testCenters: testCentersArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(response.count / this.props.testCentersPaginationPageSize),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  editTestCenter = testCenterId => {
    // getting selected test center data
    this.props
      .getSelectedTestCenterData(testCenterId)
      .then(response => {
        this.props.setTestCenterData(response);
      })
      .then(() => {
        // redirecting user to the test center management page
        history.push(PATH.testCenterManagement);
      });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.testCenterManager.table.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.testCenterManager.table.columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label: LOCALIZE.testCenterManager.table.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testCenterManager.table.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testCenterManager.table.columnFive,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label: LOCALIZE.testCenterManager.table.columnSix,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    return (
      <div>
        <div
          style={{
            ...styles.tableContainer
          }}
        >
          <SearchBarWithDisplayOptions
            idPrefix={"test-centers"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchTestCentersStates}
            updatePageState={this.props.updateCurrentTestCentersPageState}
            paginationPageSize={Number(this.props.testCentersPaginationPageSize)}
            updatePaginationPageSize={this.props.updateTestCentersPageSizeState}
            data={this.state.testCenters}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.testCentersKeyword}
          />
          <div>
            <GenericTable
              classnamePrefix="test-centers"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={LOCALIZE.testCenterManager.table.noTestCenter}
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <Pagination
            paginationContainerId={"test-centers-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.testCentersPaginationPage}
            updatePaginationPageState={this.props.updateCurrentTestCentersPageState}
            firstTableRowId={"test-centers-table-row-0"}
          />
        </div>
      </div>
    );
  }
}

export { TestCenters as unconnectedTestCenters };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    lastLogin: state.user.lastLogin,
    currentHomePage: state.userPermissions.currentHomePage,
    testCentersPaginationPageSize: state.testCenter.testCentersPaginationPageSize,
    testCentersPaginationPage: state.testCenter.testCentersPaginationPage,
    testCentersKeyword: state.testCenter.testCentersKeyword,
    testCentersActiveSearch: state.testCenter.testCentersActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateCurrentTestCentersPageState,
      updateTestCentersPageSizeState,
      updateSearchTestCentersStates,
      getAssociatedTestCenters,
      getFoundAssociatedTestCenters,
      setTestCenterData,
      getSelectedTestCenterData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestCenters);
