import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { isTokenStillValid } from "../../modules/LoginRedux";
import { bindActionCreators } from "redux";
import ContentContainer from "../commons/ContentContainer";
import { Helmet } from "react-helmet";
import SideNavigation from "../eMIB/SideNavigation";
import AssessorsOLAUnavailability from "../scorer/ScorerOLAAssessorsUnavailability";

export const styles = {
  header: {
    marginBottom: 24
  },
  sectionContainerLabelDiv: {
    paddingLeft: 18,
    marginBottom: "-3px"
  },
  sectionContainerLabel: {
    padding: "4px 24px",
    marginBottom: 0,
    position: "relative"
  },
  tabStyleBorder: {
    borderBottom: "5px solid #00565e",
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    borderRadius: "4px 4px 0 0"
  },
  sectionContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    minHeight: 400,
    padding: "12px 0",
    marginBottom: 24
  },
  tabContainer: {
    zIndex: 1,
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent"
  },
  nav: {
    marginTop: 10,
    marginLeft: 10,
    width: 206,
    textAlign: "center"
  },
  body: {
    paddingLeft: 12
  },
  sideNavBodyContent: {
    marginTop: "-12px"
  },
  appPadding: {
    padding: "15px"
  }
};

class TestCentersOla extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // Props from Redux
    isTokenStillValid: PropTypes.func
  };

  state = {
    isLoaded: false,
    isEttaDashboard: false
  };

  componentDidMount = () => {
    this._isMounted = true;
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        this.setState({
          isLoaded: true
        });
      }
    });
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  // Returns array where each item indicates specifications related to How To Page including the title and the body
  getSuperuserSections = () => {
    return [
      {
        menuString: LOCALIZE.testCenterManager.ola.lookAhead.navTitle, // TODO in text_resources
        body: "todo" // TODO make component
      },
      {
        menuString: LOCALIZE.testCenterManager.ola.unavailability.navTitle, // TODO in text_resources
        body: (
          <div style={styles.mainContainer}>
            <div>
              <h2>{LOCALIZE.testCenterManager.ola.unavailability.title}</h2>
              <p>{LOCALIZE.testCenterManager.ola.unavailability.description}</p>
            </div>
            <div>
              <AssessorsOLAUnavailability isTestCenterManager={true} />
            </div>
          </div>
        )
      },
      {
        menuString: LOCALIZE.testCenterManager.ola.reports.navTitle, // TODO in text_resources
        body: "todo" // TODO make component
      }
    ];
  };

  render() {
    const specs = this.getSuperuserSections();
    return (
      <div>
        <section aria-label={LOCALIZE.ariaLabel.sideNavigationSection}>
          <SideNavigation
            specs={specs}
            startIndex={this.props.selectedSideNavItem}
            loadOnClick={true}
            displayNextPreviousButton={false}
            isMain={true}
            tabContainerStyle={styles.tabContainer}
            tabContentStyle={styles.tabContent}
            navStyle={styles.nav}
            bodyContentCustomStyle={styles.sideNavBodyContent}
            updateSelectedSideNavItem={this.props.setSuperUserSideNavState}
          />
        </section>
      </div>
    );
  }
}

export { TestCentersOla as unconnectedTestCentersOla };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestCentersOla);
