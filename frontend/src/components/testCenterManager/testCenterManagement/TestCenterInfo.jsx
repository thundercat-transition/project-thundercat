import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Col } from "react-bootstrap";
import DropdownSelect from "../../commons/DropdownSelect";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faSpinner, faLanguage } from "@fortawesome/free-solid-svg-icons";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import { validateEmail } from "../../../helpers/regexValidator";
import {
  getSelectedTestCenterData,
  setTestCenterData,
  updateTestCenterData,
  updateTestCenterInfoContainsChangesState,
  getGetListOfSupportedCountries
} from "../../../modules/TestCenterRedux";
import MultilingualField, { FIELD_TYPES } from "../../commons/MultilingualField";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import * as _ from "lodash";
import StyledTooltip, { EFFECT, TYPE } from "../../authentication/StyledTooltip";
import { LANGUAGES } from "../../commons/Translation";
import getSwitchTransformScale from "../../../helpers/switchTransformScale";
import Switch from "react-switch";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 10,
    sm: 10,
    md: 11,
    lg: 6,
    xl: 6
  },
  thirdColumn: {
    xs: 2,
    sm: 2,
    md: 1,
    lg: 1,
    xl: 1
  }
};

const styles = {
  mainContainer: {
    margin: "24px 96px"
  },
  rowContainer: {
    marginBottom: 12
  },
  inputTitles: {
    fontWeight: "bold"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  textArea: {
    padding: 6,
    border: "1px solid #00565E",
    borderRadius: 4,
    width: "100%",
    minHeight: 88,
    resize: "none"
  },
  applyButtonContainer: {
    textAlign: "center",
    marginTop: 36
  },
  applyButton: {
    minWidth: 200
  },
  buttonIcon: {
    marginRight: 6
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "6px 0 12px 0"
  },
  translateButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: 0,
    margin: "0 3px",
    color: "#00565e"
  }
};

const initializeTestCenterCountry = (testCenterData, language) => {
  if (language === LANGUAGES.english) {
    return {
      label: testCenterData.country_edesc,
      value: testCenterData.country_id
    };
  }
  if (language === LANGUAGES.french) {
    return {
      label: testCenterData.country_fdesc,
      value: testCenterData.country_id
    };
  }
  return {
    label: "Canada",
    value: 38
  };
};

class TestCenterInfo extends Component {
  state = {
    selectedDepartmentOption: {
      label: `${this.props.testCenterData[`dept_${this.props.currentLanguage.charAt(0)}desc`]} (${
        this.props.testCenterData[`dept_${this.props.currentLanguage.charAt(0)}abrv`]
      })`,
      value: `${this.props.testCenterData.dept_id}`
    },
    testCenterPostalCode: { content: this.props.testCenterData.postal_code },

    testCenterSecurityEmail: { content: this.props.testCenterData.security_email },
    isValidTestCenterSecurityEmail: true,
    testCenterBookingDelay: { content: this.props.testCenterData.booking_delay },
    isValidForm: true,
    dataHasBeenModified: false,
    isLoading: false,

    // Test Center Address Variables
    testCenterAddress: { content: this.props.testCenterData.test_center_address_text },
    showAddressPopup: false,

    // Test Center City Variables
    testCenterCity: { content: this.props.testCenterData.test_center_city_text },
    showCityPopup: false,

    // Test Center Province Variables
    testCenterProvince: { content: this.props.testCenterData.test_center_province_text },
    showProvincePopup: false,

    // Test Center Country Variables
    countriesList: [],
    testCenterCountry: initializeTestCenterCountry(
      this.props.testCenterData,
      this.props.currentLanguage
    ),

    // Test Center Other Details Variables
    testCenterOtherDetails: { content: this.props.testCenterData.test_center_other_details_text },
    showOtherDetailsPopup: false,

    // Test Center Accommodations Friendly Switch
    isLoadingSwitch: false,
    accommodationsFriendlySwitchState: this.props.testCenterData.accommodations_friendly
  };

  componentDidMount = () => {
    // initializing testCenterInfoContainsChanges redux state
    this.props.updateTestCenterInfoContainsChangesState(false);
    this.initializeListOfCountries();
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = prevProps => {
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
  };

  // get list of countries into our state
  initializeListOfCountries = () => {
    this.props.getGetListOfSupportedCountries().then(response => {
      const temp = [];

      response.forEach(country => {
        temp.push({
          value: country.cntry_id,
          label: this.props.currentLanguage === LANGUAGES.english ? country.edesc : country.fdesc
        });
      });

      this.setState({
        countriesList: temp
      });
    });
  };

  /*--------------------------*/
  //    Test Center Address   //
  /*--------------------------*/

  updateTestCenterAddressContent = event => {
    // Removing _name to get the language
    const language =
      event.target.name !== ""
        ? event.target.name.replace("_name", "")
        : this.props.currentLanguage;

    const testCenterAddress = event.target.value;

    // allow maximum of 255 chars
    const regexExpression = /^(.{0,255})$/;
    if (regexExpression.test(testCenterAddress)) {
      const temp = _.cloneDeep(this.state.testCenterAddress);

      // set the language's text in our address object
      temp.content[language][0].text = testCenterAddress;

      // update the state
      this.setState(
        {
          testCenterAddress: { ...temp }
        },
        () => {
          // checking if all the data is the same as in the DB
          this.checkForChangesInForm();
          // validating form
          this.validateForm();
        }
      );
    }
  };

  handleOpenAddressPopup = () => {
    this.setState({ showAddressPopup: true });
  };

  handleCloseAddressPopup = () => {
    this.setState({ showAddressPopup: false });
  };

  /*--------------------------*/
  //    Test Center City      //
  /*--------------------------*/

  updateTestCenterCityContent = event => {
    // Removing _name to get the language
    const language =
      event.target.name !== ""
        ? event.target.name.replace("_name", "")
        : this.props.currentLanguage;

    const testCenterCity = event.target.value;

    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(testCenterCity)) {
      const temp = _.cloneDeep(this.state.testCenterCity);

      // set the language's text in our city object
      temp.content[language][0].text = testCenterCity;

      // update the state
      this.setState(
        {
          testCenterCity: { ...temp }
        },
        () => {
          // checking if all the data is the same as in the DB
          this.checkForChangesInForm();
          // validating form
          this.validateForm();
        }
      );
    }
  };

  handleOpenCityPopup = () => {
    this.setState({ showCityPopup: true });
  };

  handleCloseCityPopup = () => {
    this.setState({ showCityPopup: false });
  };

  updateTestCenterPostalCodeContent = event => {
    const testCenterPostalCode = event.target.value;
    // allow maximum of 7 chars (alphanumeric with space)
    const regexExpression = /^([a-zA-z0-9 ]{0,15})$/;
    if (regexExpression.test(testCenterPostalCode)) {
      this.setState(
        {
          testCenterPostalCode: {
            content: testCenterPostalCode.toUpperCase()
          }
        },
        () => {
          // checking if all the data is the same as in the DB
          this.checkForChangesInForm();
          // validating form
          this.validateForm();
        }
      );
    }
  };

  /*--------------------------*/
  //   Test Center Province   //
  /*--------------------------*/
  updateTestCenterProvinceContent = event => {
    // Removing _name to get the language
    const language =
      event.target.name !== ""
        ? event.target.name.replace("_name", "")
        : this.props.currentLanguage;

    const testCenterProvince = event.target.value;
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(testCenterProvince)) {
      const temp = _.cloneDeep(this.state.testCenterProvince);

      // set the language's text in our province object
      temp.content[language][0].text = testCenterProvince;

      // update the state
      this.setState(
        {
          testCenterProvince: { ...temp }
        },
        () => {
          // checking if all the data is the same as in the DB
          this.checkForChangesInForm();
          // validating form
          this.validateForm();
        }
      );
    }
  };

  handleOpenProvincePopup = () => {
    this.setState({ showProvincePopup: true });
  };

  handleCloseProvincePopup = () => {
    this.setState({ showProvincePopup: false });
  };

  updateTestCenterCountryContent = event => {
    // allow maximum of 150 chars
    this.setState({ testCenterCountry: event }, () => {
      // checking if all the data is the same as in the DB
      this.checkForChangesInForm();
      // validating form
      this.validateForm();
    });
  };

  updateTestCenterSecurityEmailContent = event => {
    let testCenterSecurityEmail = event.target.value;
    // allow maximum of 254 chars
    const regexExpression = /^(.{0,254})$/;
    if (regexExpression.test(testCenterSecurityEmail)) {
      // setting testCenterSecurityEmail to null if empty string (since the model is proving "null" if there is nothing set for this email address)
      if (testCenterSecurityEmail === "") {
        testCenterSecurityEmail = null;
      }
      this.setState({ testCenterSecurityEmail: { content: testCenterSecurityEmail } }, () => {
        // checking if all the data is the same as in the DB
        this.checkForChangesInForm();
        // validating form
        this.validateForm();
      });
    }
  };

  updateTestCenterBookingDelayContent = event => {
    const testCenterBookingDelay = event.target.value;
    // allow only numeric values (0 to 5 chars)
    const regexExpression = /^([0-9]{0,5})$/;
    if (regexExpression.test(testCenterBookingDelay)) {
      this.setState(
        {
          testCenterBookingDelay: {
            content: testCenterBookingDelay !== "" ? parseInt(testCenterBookingDelay) : 0
          }
        },
        () => {
          // checking if all the data is the same as in the DB
          this.checkForChangesInForm();
          // validating form
          this.validateForm();
        }
      );
    }
  };

  /*--------------------------*/
  // Test Center Other Details//
  /*--------------------------*/
  updateTestCenterOtherDetailsContent = event => {
    // Removing _name to get the language
    const language =
      event.target.name !== ""
        ? event.target.name.replace("_name", "")
        : this.props.currentLanguage;

    const testCenterOtherDetails = event.target.value;

    const temp = _.cloneDeep(this.state.testCenterOtherDetails);

    // set the language's text in our other details object
    temp.content[language][0].text = testCenterOtherDetails;

    // update the state
    this.setState(
      {
        testCenterOtherDetails: { ...temp }
      },
      () => {
        // checking if all the data is the same as in the DB
        this.checkForChangesInForm();
        // validating form
        this.validateForm();
      }
    );
  };

  handleOpenOtherDetailsPopup = () => {
    this.setState({ showOtherDetailsPopup: true });
  };

  handleCloseOtherDetailsPopup = () => {
    this.setState({ showOtherDetailsPopup: false });
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoadingSwitch: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoadingSwitch: false });
        }
      );
    });
  };

  handleSwitchStateUpdate = event => {
    this.setState({ accommodationsFriendlySwitchState: event }, () => {
      // checking if all the data is the same as in the DB
      this.checkForChangesInForm();
      // validating form
      this.validateForm();
    });
  };

  checkForChangesInForm = () => {
    // initializing needed variables
    let dataHasBeenModified = false;
    let testCenterDepartmentModified = false;
    let testCenterAddressModified = false;
    let testCenterCityModified = false;
    let testCenterPostalCodeModified = false;
    let testCenterProvinceModified = false;
    let testCenterCountryModified = false;
    let testCenterSecurityEmailModified = false;
    let testCenterBookingDelayModified = false;
    let testCenterOtherDetailsModified = false;
    let accommodationsFriendlyModified = false;

    // checking if testCenterDepartment has been modified
    if (
      this.state.selectedDepartmentOption.value.toString() !==
      this.props.testCenterData.dept_id.toString()
    ) {
      testCenterDepartmentModified = true;
    }

    // checking if testCenterAddress has been modified
    if (
      // Using isEqual to compare 2 objects
      !_.isEqual(
        this.state.testCenterAddress.content,
        this.props.testCenterData.test_center_address_text
      )
    ) {
      testCenterAddressModified = true;
    }

    // checking if testCenterCity has been modified
    if (
      // Using isEqual to compare 2 objects
      !_.isEqual(this.state.testCenterCity.content, this.props.testCenterData.test_center_city_text)
    ) {
      testCenterCityModified = true;
    }

    // checking if testCenterPostalCode has been modified
    if (this.state.testCenterPostalCode.content !== this.props.testCenterData.postal_code) {
      testCenterPostalCodeModified = true;
    }

    // checking if testCenterProvince has been modified
    if (
      // Using isEqual to compare 2 objects
      !_.isEqual(
        this.state.testCenterProvince.content,
        this.props.testCenterData.test_center_province_text
      )
    ) {
      testCenterProvinceModified = true;
    }

    // checking if testCenterCountry has been modified
    if (this.state.testCenterCountry.value !== this.props.testCenterData.country_id) {
      testCenterCountryModified = true;
    }

    // checking if testCenterSecurityEmail has been modified
    if (this.state.testCenterSecurityEmail.content !== this.props.testCenterData.security_email) {
      testCenterSecurityEmailModified = true;
    }

    // checking if testCenterBookingDelay has been modified
    if (this.state.testCenterBookingDelay.content !== this.props.testCenterData.booking_delay) {
      testCenterBookingDelayModified = true;
    }

    // checking if testCenterOtherDetails has been modified
    if (
      // Using isEqual to compare 2 objects
      !_.isEqual(
        this.state.testCenterOtherDetails.content,
        this.props.testCenterData.test_center_other_details_text
      )
    ) {
      testCenterOtherDetailsModified = true;
    }

    // checking if accommodationsFriendlySwitchState has been modified
    if (
      this.state.accommodationsFriendlySwitchState !==
      this.props.testCenterData.accommodations_friendly
    ) {
      accommodationsFriendlyModified = true;
    }

    // if any data has been modified
    if (
      testCenterDepartmentModified ||
      testCenterAddressModified ||
      testCenterCityModified ||
      testCenterPostalCodeModified ||
      testCenterProvinceModified ||
      testCenterCountryModified ||
      testCenterSecurityEmailModified ||
      testCenterBookingDelayModified ||
      testCenterOtherDetailsModified ||
      accommodationsFriendlyModified
    ) {
      dataHasBeenModified = true;
    }

    // initializing testCenterInfoContainsChanges redux state
    this.props.updateTestCenterInfoContainsChangesState(dataHasBeenModified);

    // updating state
    this.setState({ dataHasBeenModified: dataHasBeenModified });
  };

  validateForm = () => {
    // initializing needed variables
    let isValidForm = false;
    // the only field that can be invalid is the security email, so just making sure that this is either empty or a valid email
    const isValidTestCenterSecurityEmail =
      this.state.testCenterSecurityEmail.content === null ||
      validateEmail(this.state.testCenterSecurityEmail.content);

    // if all mandatory fields are valid
    if (isValidTestCenterSecurityEmail) {
      isValidForm = true;
    }
    this.setState({
      isValidForm: isValidForm,
      isValidTestCenterSecurityEmail: isValidTestCenterSecurityEmail
    });
  };

  handleApplyChanges = () => {
    this.setState({ isLoading: true }, () => {
      // updating data in DB
      const body = {
        id: this.props.testCenterData.id,
        department_id: this.state.selectedDepartmentOption.value,
        address: this.state.testCenterAddress.content,
        city: this.state.testCenterCity.content,
        postal_code: this.state.testCenterPostalCode.content,
        province: this.state.testCenterProvince.content,
        country_id: this.state.testCenterCountry.value,
        security_email: this.state.testCenterSecurityEmail.content,
        booking_delay: this.state.testCenterBookingDelay.content,
        other_details: this.state.testCenterOtherDetails.content,
        accommodations_friendly: this.state.accommodationsFriendlySwitchState
      };
      this.props.updateTestCenterData(body).then(response => {
        if (response.ok) {
          // getting selected test center data
          this.props.getSelectedTestCenterData(this.props.testCenterData.id).then(response => {
            // making sure that the test center data redux state is up to date
            this.props.setTestCenterData(response);
            // adding small delay to make sure that the props have been updated properly
            setTimeout(() => {
              this.checkForChangesInForm();
              this.setState({ isLoading: false });
            }, 100);
          });
          // should never happen
        } else {
          throw new Error("An error occurred during the update test center data process");
        }
      });
    });
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    const addressObj = {
      ...this.state.testCenterAddress
    };

    const cityObj = {
      ...this.state.testCenterCity
    };

    const postalCodeObj = {
      ...{ content: this.props.testCenterData.postal_code },
      ...this.state.testCenterPostalCode
    };

    const provinceObj = {
      ...this.state.testCenterProvince
    };

    const countryObj = {
      ...this.state.testCenterCountry
    };

    const securityEmailObj = {
      ...{ content: this.props.testCenterData.security_email },
      ...this.state.testCenterSecurityEmail
    };

    const bookingDelayObj = {
      ...{ content: this.props.testCenterData.booking_delay },
      ...this.state.testCenterBookingDelay
    };

    const otherDetailsObj = {
      ...this.state.testCenterOtherDetails
    };

    return (
      <div style={styles.mainContainer}>
        <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label id="test-center-info-department-label" style={styles.inputTitles}>
              {
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.centerInfo.departmentLabel
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
          >
            <DropdownSelect
              idPrefix="test-center-info-department"
              isValid={true}
              ariaRequired={false}
              ariaLabelledBy="test-center-info-department-label"
              hasPlaceholder={true}
              defaultValue={this.state.selectedDepartmentOption}
              isMulti={false}
              isDisabled={true}
            />
          </Col>
        </Row>
        <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label htmlFor="test-center-info-address-input" style={styles.inputTitles}>
              {
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.centerInfo.addressLabel
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
          >
            <input
              id="test-center-info-address-input"
              className={"valid-field"}
              style={{ ...styles.input, ...accommodationsStyle }}
              type="text"
              value={addressObj.content[this.props.currentLanguage][0].text}
              onChange={this.updateTestCenterAddressContent}
            ></input>
          </Col>
          <Col
            xl={columnSizes.thirdColumn.xl}
            lg={columnSizes.thirdColumn.lg}
            md={columnSizes.thirdColumn.md}
            sm={columnSizes.thirdColumn.sm}
            xs={columnSizes.thirdColumn.xs}
            className={"text-left"}
          >
            <StyledTooltip
              id={`test-center-info-address-translations`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    className="h-100"
                    dataTip=""
                    dataFor={`test-center-info-address-translations`}
                    label={<FontAwesomeIcon icon={faLanguage} />}
                    ariaLabel={
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.centerInfo.translationsPopup.address
                        .translationsButtonAriaLabel
                    }
                    action={() => this.handleOpenAddressPopup()}
                    customStyle={{
                      ...styles.translateButton,
                      fontSize: this.props.accommodations.fontSize.replace("px", "") * 1.5
                    }}
                    buttonTheme={THEME.SECONDARY}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.centerInfo.translationsPopup.address
                        .translationsButtonTooltip
                    }
                  </p>
                </div>
              }
            />
          </Col>
        </Row>
        <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label htmlFor="test-center-info-city-input" style={styles.inputTitles}>
              {
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.centerInfo.cityLabel
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
          >
            <input
              id="test-center-info-city-input"
              className={"valid-field"}
              style={{ ...styles.input, ...accommodationsStyle }}
              type="text"
              value={cityObj.content[this.props.currentLanguage][0].text}
              onChange={this.updateTestCenterCityContent}
            ></input>
          </Col>
          <Col
            xl={columnSizes.thirdColumn.xl}
            lg={columnSizes.thirdColumn.lg}
            md={columnSizes.thirdColumn.md}
            sm={columnSizes.thirdColumn.sm}
            xs={columnSizes.thirdColumn.xs}
            className={"text-left"}
          >
            <StyledTooltip
              id={`test-center-info-city-translations`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    className="h-100"
                    dataTip=""
                    dataFor={`test-center-info-city-translations`}
                    label={<FontAwesomeIcon icon={faLanguage} />}
                    ariaLabel={
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.centerInfo.translationsPopup.city
                        .translationsButtonAriaLabel
                    }
                    action={() => this.handleOpenCityPopup()}
                    customStyle={{
                      ...styles.translateButton,
                      fontSize: this.props.accommodations.fontSize.replace("px", "") * 1.5
                    }}
                    buttonTheme={THEME.SECONDARY}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.centerInfo.translationsPopup.city
                        .translationsButtonTooltip
                    }
                  </p>
                </div>
              }
            />
          </Col>
        </Row>
        <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label htmlFor="test-center-info-postal-code-input" style={styles.inputTitles}>
              {
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.centerInfo.postalCodeLabel
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
          >
            <input
              id="test-center-info-postal-code-input"
              className={"valid-field"}
              style={{ ...styles.input, ...accommodationsStyle }}
              type="text"
              value={postalCodeObj.content}
              onChange={this.updateTestCenterPostalCodeContent}
            ></input>
          </Col>
        </Row>
        <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label htmlFor="test-center-info-province-input" style={styles.inputTitles}>
              {
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.centerInfo.provinceLabel
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
          >
            <input
              id="test-center-info-province-input"
              className={"valid-field"}
              style={{ ...styles.input, ...accommodationsStyle }}
              type="text"
              value={provinceObj.content[this.props.currentLanguage][0].text}
              onChange={this.updateTestCenterProvinceContent}
            ></input>
          </Col>
          <Col
            xl={columnSizes.thirdColumn.xl}
            lg={columnSizes.thirdColumn.lg}
            md={columnSizes.thirdColumn.md}
            sm={columnSizes.thirdColumn.sm}
            xs={columnSizes.thirdColumn.xs}
            className={"text-left"}
          >
            <StyledTooltip
              id={`test-center-info-province-translations`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    className="h-100"
                    dataTip=""
                    dataFor={`test-center-info-province-translations`}
                    label={<FontAwesomeIcon icon={faLanguage} />}
                    ariaLabel={
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.centerInfo.translationsPopup.province
                        .translationsButtonAriaLabel
                    }
                    action={() => this.handleOpenProvincePopup()}
                    customStyle={{
                      ...styles.translateButton,
                      fontSize: this.props.accommodations.fontSize.replace("px", "") * 1.5
                    }}
                    buttonTheme={THEME.SECONDARY}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.centerInfo.translationsPopup.province
                        .translationsButtonTooltip
                    }
                  </p>
                </div>
              }
            />
          </Col>
        </Row>
        <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label htmlFor="test-center-info-country-input" style={styles.inputTitles}>
              {
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.centerInfo.countryLabel
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
          >
            <DropdownSelect
              idPrefix="test-center-info-country"
              onChange={this.updateTestCenterCountryContent}
              defaultValue={countryObj}
              options={this.state.countriesList}
              ariaLabelledBy="test-center-info-country-label"
              orderByLabels={false}
              isDisabled={false}
            />
          </Col>
        </Row>
        <Row
          role="presentation"
          className="align-items-center"
          style={this.state.isValidTestCenterSecurityEmail ? styles.rowContainer : {}}
        >
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label htmlFor="test-center-info-security-email-input" style={styles.inputTitles}>
              {
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.centerInfo.securityEmailLabel
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
          >
            <input
              id="test-center-info-security-email-input"
              className={
                this.state.isValidTestCenterSecurityEmail ? "valid-field" : "invalid-field"
              }
              style={{ ...styles.input, ...accommodationsStyle }}
              type="text"
              value={securityEmailObj.content}
              onChange={this.updateTestCenterSecurityEmailContent}
            ></input>
          </Col>
        </Row>
        {!this.state.isValidTestCenterSecurityEmail && (
          <Row className="align-items-center">
            <Col
              xl={columnSizes.firstColumn.xl}
              lg={columnSizes.firstColumn.lg}
              md={columnSizes.firstColumn.md}
              sm={columnSizes.firstColumn.sm}
              xs={columnSizes.firstColumn.xs}
            ></Col>
            <Col
              xl={columnSizes.secondColumn.xl}
              lg={columnSizes.secondColumn.lg}
              md={columnSizes.secondColumn.md}
              sm={columnSizes.secondColumn.sm}
              xs={columnSizes.secondColumn.xs}
            >
              <label
                id="test-center-info-security-email-error"
                htmlFor="test-center-info-security-email-input"
                style={styles.errorMessage}
              >
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                    .testCenterManagement.topTabs.centerInfo.securityEmailErrorMessage
                }
              </label>
            </Col>
          </Row>
        )}
        <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label htmlFor="test-center-info-booking-delay-input" style={styles.inputTitles}>
              {
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.centerInfo.bookingDelayLabel
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
          >
            <input
              id="test-center-info-booking-delay-input"
              className={"valid-field"}
              style={{ ...styles.input, ...accommodationsStyle }}
              type="text"
              value={bookingDelayObj.content}
              onChange={this.updateTestCenterBookingDelayContent}
            ></input>
          </Col>
        </Row>
        <Row role="presentation" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label htmlFor="test-center-info-other-details-input" style={styles.inputTitles}>
              {
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.centerInfo.otherDetailsLabel
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
          >
            <textarea
              id={"test-center-info-other-details-input"}
              className="w-100 valid-field"
              style={styles.textArea}
              onChange={this.updateTestCenterOtherDetailsContent}
              value={otherDetailsObj.content[this.props.currentLanguage][0].text}
              maxLength="500"
            />
          </Col>
          <Col
            xl={columnSizes.thirdColumn.xl}
            lg={columnSizes.thirdColumn.lg}
            md={columnSizes.thirdColumn.md}
            sm={columnSizes.thirdColumn.sm}
            xs={columnSizes.thirdColumn.xs}
            className={"text-left"}
          >
            <StyledTooltip
              id={`test-center-info-other-details-translations`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    className="h-100"
                    dataTip=""
                    dataFor={`test-center-info-other-details-translations`}
                    label={<FontAwesomeIcon icon={faLanguage} />}
                    ariaLabel={
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.centerInfo.translationsPopup.otherDetails
                        .translationsButtonAriaLabel
                    }
                    action={() => this.handleOpenOtherDetailsPopup()}
                    customStyle={{
                      ...styles.translateButton,
                      fontSize: this.props.accommodations.fontSize.replace("px", "") * 1.5
                    }}
                    buttonTheme={THEME.SECONDARY}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.centerInfo.translationsPopup.otherDetails
                        .translationsButtonTooltip
                    }
                  </p>
                </div>
              }
            />
          </Col>
        </Row>
        <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
          <Col
            xl={columnSizes.firstColumn.xl}
            lg={columnSizes.firstColumn.lg}
            md={columnSizes.firstColumn.md}
            sm={columnSizes.firstColumn.sm}
            xs={columnSizes.firstColumn.xs}
          >
            <label
              htmlFor="test-center-info-accommodations-friendly-input"
              style={styles.inputTitles}
            >
              {
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.centerInfo.accommodationsFriendly
              }
            </label>
          </Col>
          <Col
            xl={columnSizes.secondColumn.xl}
            lg={columnSizes.secondColumn.lg}
            md={columnSizes.secondColumn.md}
            sm={columnSizes.secondColumn.sm}
            xs={columnSizes.secondColumn.xs}
          >
            {!this.state.isLoadingSwitch && (
              <Switch
                id="test-center-info-accommodations-friendly-input"
                onChange={this.handleSwitchStateUpdate}
                checked={this.state.accommodationsFriendlySwitchState}
                height={this.state.switchHeight}
                width={this.state.switchWidth}
              />
            )}
          </Col>
        </Row>
        <Row role="presentation" className="align-items-center">
          <Col xl={12} lg={12} md={12} sm={12} xs={12}>
            <div style={styles.applyButtonContainer}>
              <CustomButton
                label={
                  <>
                    {this.state.isLoading ? (
                      // eslint-disable-next-line jsx-a11y/label-has-associated-control
                      <label className="fa fa-spinner fa-spin">
                        <FontAwesomeIcon icon={faSpinner} />
                      </label>
                    ) : (
                      <>
                        <FontAwesomeIcon icon={faCheck} style={styles.buttonIcon} />
                        <span>
                          {this.state.dataHasBeenModified
                            ? LOCALIZE.commons.applyButton
                            : LOCALIZE.commons.saved}
                        </span>
                      </>
                    )}
                  </>
                }
                action={this.handleApplyChanges}
                customStyle={styles.applyButton}
                buttonTheme={this.state.dataHasBeenModified ? THEME.PRIMARY : THEME.SUCCESS}
                disabled={
                  !this.state.isValidForm || !this.state.dataHasBeenModified || this.state.isLoading
                }
              />
            </div>
          </Col>
        </Row>
        <PopupBox
          show={this.state.showAddressPopup}
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
              .topTabs.centerInfo.translationsPopup.address.title
          }
          handleClose={() => this.handleCloseAddressPopup()}
          description={
            <div>
              <MultilingualField
                handleChange={this.updateTestCenterAddressContent}
                localizePath={
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                    .testCenterManagement.topTabs.centerInfo.translationsPopup.address
                }
                fieldName={"test-center-address"}
                values={this.state.testCenterAddress.content}
                fieldType={FIELD_TYPES.INPUT}
              ></MultilingualField>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.handleCloseAddressPopup()}
        />
        <PopupBox
          show={this.state.showCityPopup}
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
              .topTabs.centerInfo.translationsPopup.city.title
          }
          handleClose={() => this.handleCloseCityPopup()}
          description={
            <div>
              <MultilingualField
                handleChange={this.updateTestCenterCityContent}
                localizePath={
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                    .testCenterManagement.topTabs.centerInfo.translationsPopup.city
                }
                fieldName={"test-center-city"}
                values={this.state.testCenterCity.content}
                fieldType={FIELD_TYPES.INPUT}
              ></MultilingualField>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.handleCloseCityPopup()}
        />
        <PopupBox
          show={this.state.showProvincePopup}
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
              .topTabs.centerInfo.translationsPopup.province.title
          }
          handleClose={() => this.handleCloseProvincePopup()}
          description={
            <div>
              <MultilingualField
                handleChange={this.updateTestCenterProvinceContent}
                localizePath={
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                    .testCenterManagement.topTabs.centerInfo.translationsPopup.province
                }
                fieldName={"test-center-province"}
                values={this.state.testCenterProvince.content}
                fieldType={FIELD_TYPES.INPUT}
              ></MultilingualField>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.handleCloseProvincePopup()}
        />
        <PopupBox
          show={this.state.showOtherDetailsPopup}
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
              .topTabs.centerInfo.translationsPopup.otherDetails.title
          }
          handleClose={() => this.handleCloseOtherDetailsPopup()}
          description={
            <div>
              <MultilingualField
                handleChange={this.updateTestCenterOtherDetailsContent}
                localizePath={
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                    .testCenterManagement.topTabs.centerInfo.translationsPopup.otherDetails
                }
                fieldName={"test-center-other-details"}
                values={this.state.testCenterOtherDetails.content}
                fieldType={FIELD_TYPES.TEXT_AREA}
              ></MultilingualField>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.handleCloseOtherDetailsPopup()}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testCenterData: state.testCenter.testCenterData
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateTestCenterData,
      getSelectedTestCenterData,
      setTestCenterData,
      updateTestCenterInfoContainsChangesState,
      getGetListOfSupportedCountries
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestCenterInfo);
