import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Col, Pagination } from "react-bootstrap";
import DropdownSelect from "../../commons/DropdownSelect";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle, faTimes, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import { getUsersBasedOnSpecifiedPermission } from "../../../modules/PermissionsRedux";
import { PERMISSION } from "../../profile/Constants";
import {
  associateTestAdministratorsToTestCenter,
  getAssociatedTestAdministratorsToTestCenter,
  getFoundAssociatedTestAdministratorsToTestCenter,
  updateCurrentTestCenterTestAdministratorsPageState,
  updateTestCenterTestAdministratorsPageSizeState,
  updateSearchTestCenterTestAdministratorsStates,
  deleteTestAdministratorFromTestCenter
} from "../../../modules/TestCenterRedux";
import StyledTooltip, { EFFECT, TYPE } from "../../authentication/StyledTooltip";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 4,
    xl: 4
  },
  secondColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 8,
    xl: 8
  }
};

const styles = {
  mainContainer: {
    margin: "24px 0"
  },
  rowContainer: {
    margin: "12px 0"
  },
  inputTitles: {
    fontWeight: "bold"
  },
  buttonIcon: {
    marginRight: 6
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  deleteIcon: {
    transform: "scale(1.2)"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  allUnset: {
    all: "unset"
  },
  boldText: {
    fontWeight: "bold"
  }
};

class TestAdministrators extends Component {
  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    resultsFound: 0,
    associatedTestAdministrators: [],
    clearSearchTriggered: false,
    showAddTestAdministratorPopup: false,
    testAdministratorOptions: [],
    selectedTestAdministrators: [],
    selectedTestAdministratorOptionsAccessibility: LOCALIZE.commons.none,
    showDeleteTestAdministratorPopup: false,
    selectedTestAdministratorToDelete: {}
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentTestCenterTestAdministratorsPageState(1);
    // populating associated test administrators
    this.populateAssociatedTestAdministratorsBasedOnActiveSearch();
  };

  componentDidUpdate = prevProps => {
    // // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateAssociatedTestAdministratorsBasedOnActiveSearch();
    }
    // // if testCenterTestAdministratorsPaginationPage gets updated
    if (
      prevProps.testCenterTestAdministratorsPaginationPage !==
      this.props.testCenterTestAdministratorsPaginationPage
    ) {
      this.populateAssociatedTestAdministratorsBasedOnActiveSearch();
    }
    // // if testCenterTestAdministratorsPaginationPageSize get updated
    if (
      prevProps.testCenterTestAdministratorsPaginationPageSize !==
      this.props.testCenterTestAdministratorsPaginationPageSize
    ) {
      this.populateAssociatedTestAdministratorsBasedOnActiveSearch();
    }
    // // if search testCenterTestAdministratorsKeyword gets updated
    if (
      prevProps.testCenterTestAdministratorsKeyword !==
      this.props.testCenterTestAdministratorsKeyword
    ) {
      // if testCenterTestAdministratorsKeyword redux state is empty
      if (this.props.testCenterTestAdministratorsKeyword !== "") {
        this.populateFoundAssociatedTestAdministrators();
      } else if (
        this.props.testCenterTestAdministratorsKeyword === "" &&
        this.props.testCenterTestAdministratorsActiveSearch
      ) {
        this.populateFoundAssociatedTestAdministrators();
      }
    }
    // // if testCenterTestAdministratorsActiveSearch gets updated
    if (
      prevProps.testCenterTestAdministratorsActiveSearch !==
      this.props.testCenterTestAdministratorsActiveSearch
    ) {
      // there is no current active search (on clear search action)
      if (!this.props.testCenterTestAdministratorsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateAssociatedTestAdministrators();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundAssociatedTestAdministrators();
      }
    }
  };

  populateAssociatedTestAdministratorsBasedOnActiveSearch = () => {
    // current search
    if (this.props.testCenterTestAdministratorsActiveSearch) {
      this.populateFoundAssociatedTestAdministrators();
      // no search
    } else {
      this.populateAssociatedTestAdministrators();
    }
  };

  // populating associated test center test administrators
  populateAssociatedTestAdministrators = () => {
    this.setState({ currentlyLoading: true }, () => {
      const associatedTestAdministratorsArray = [];
      this.props
        .getAssociatedTestAdministratorsToTestCenter(
          this.props.testCenterTestAdministratorsPaginationPage,
          this.props.testCenterTestAdministratorsPaginationPageSize,
          this.props.testCenterData.id
        )
        .then(response => {
          this.populateTestCenterTestAdministratorsObject(
            associatedTestAdministratorsArray,
            response
          );
        })
        .then(() => {
          if (this.state.clearSearchTriggered) {
            // go back to the first page to avoid display bugs
            this.props.updateCurrentTestCenterTestAdministratorsPageState(1);
            this.setState({ clearSearchTriggered: false });
          }
        })
        .then(() => {
          this.setState({ currentlyLoading: false });
          // populating test administration options
          this.populateTestAdministratorOptions();
        });
    });
  };

  // populating found associated test center test administrators
  populateFoundAssociatedTestAdministrators = () => {
    // making sure that the table is not currently loading
    if (!this.state.currentlyLoading) {
      this.setState(
        { currentlyLoading: true },
        () => {
          const associatedTestAdministratorsArray = [];
          setTimeout(() => {
            this.props
              .getFoundAssociatedTestAdministratorsToTestCenter(
                this.props.testCenterTestAdministratorsKeyword,
                this.props.testCenterTestAdministratorsPaginationPage,
                this.props.testCenterTestAdministratorsPaginationPageSize,
                this.props.testCenterData.id
              )
              .then(response => {
                // there are no results found
                if (response[0] === "no results found") {
                  this.setState({
                    associatedTestAdministrators: [],
                    rowsDefinition: {},
                    numberOfPages: 1,
                    nextPageNumber: 0,
                    previousPageNumber: 0,
                    resultsFound: 0
                  });
                  // there is at least one result found
                } else {
                  this.populateTestCenterTestAdministratorsObject(
                    associatedTestAdministratorsArray,
                    response
                  );
                  this.setState({ resultsFound: response.count });
                }
              })
              .then(() => {
                this.setState(
                  {
                    currentlyLoading: false
                  },
                  () => {
                    // populating test administration options
                    this.populateTestAdministratorOptions();
                    // make sure that this element exsits to avoid any error
                    if (document.getElementById("test-center-test-administrators-results-found")) {
                      document
                        .getElementById("test-center-test-administrators-results-found")
                        .focus();
                    }
                  }
                );
              });
          });
        },
        250
      );
    }
  };

  populateTestCenterTestAdministratorsObject = (associatedTestAdministratorsArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in associatedTestAdministratorsArray
        associatedTestAdministratorsArray.push({
          id: currentResult.id,
          user_id: currentResult.user_id,
          email: currentResult.email,
          first_name: currentResult.first_name,
          last_name: currentResult.last_name
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult.first_name,
          column_2: currentResult.last_name,
          column_3: currentResult.email,
          column_4: (
            <StyledTooltip
              id={`test-center-test-administrators-delete-row-${i}`}
              place="top"
              variant={TYPE.light}
              effect={EFFECT.solid}
              openOnClick={false}
              tooltipElement={
                <div style={styles.allUnset}>
                  <CustomButton
                    dataTip=""
                    dataFor={`test-center-test-administrators-delete-row-${i}`}
                    label={
                      <>
                        <FontAwesomeIcon icon={faTrashAlt} style={styles.deleteIcon} />
                      </>
                    }
                    action={() => {
                      this.openDeleteTestAdministratorPopup(associatedTestAdministratorsArray[i]);
                    }}
                    customStyle={styles.actionButton}
                    buttonTheme={THEME.PRIMARY}
                    ariaLabel={LOCALIZE.formatString(
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.testAdministrators.table
                        .deleteButtonAccessibility,
                      currentResult.first_name,
                      currentResult.last_name
                    )}
                  />
                </div>
              }
              tooltipContent={
                <div>
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.testAdministrators.table.deleteButtonTooltip
                    }
                  </p>
                </div>
              }
            />
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.LEFT_TEXT,
      column_3_style: COMMON_STYLE.LEFT_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState({
      associatedTestAdministrators: associatedTestAdministratorsArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(
        response.count / this.props.testCenterTestAdministratorsPaginationPageSize
      ),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  openDeleteTestAdministratorPopup = testAdministratorData => {
    this.setState({
      showDeleteTestAdministratorPopup: true,
      selectedTestAdministratorToDelete: testAdministratorData
    });
  };

  closeDeleteTestAdministratorPopup = () => {
    this.setState({
      showDeleteTestAdministratorPopup: false,
      selectedTestAdministratorToDelete: {}
    });
  };

  handleDeleteTestAdministrator = () => {
    // creating body
    const body = {
      id: this.state.selectedTestAdministratorToDelete.id,
      test_center_id: this.props.testCenterData.id
    };
    this.props.deleteTestAdministratorFromTestCenter(body).then(response => {
      // successful request
      if (response.ok) {
        // closing popup and updating table
        this.closeDeleteTestAdministratorPopup();
        this.populateAssociatedTestAdministratorsBasedOnActiveSearch();
        // should never happen
      } else {
        throw new Error("An error occurred during the delete test administrator process");
      }
    });
  };

  // populate test administration options (all users that have test administrators permission/role)
  populateTestAdministratorOptions = () => {
    const testAdministratorOptions = [];
    this.props.getUsersBasedOnSpecifiedPermission(PERMISSION.testAdministrator).then(response => {
      for (let i = 0; i < response.length; i++) {
        // making sure that the current user/TA is not already part of the associated list
        if (
          this.state.associatedTestAdministrators.filter(obj => obj.user_id === response[i].user_id)
            .length <= 0
        ) {
          // populating testAdministratorOptions
          testAdministratorOptions.push({
            label: `${response[i].last_name}, ${response[i].first_name} - ${
              response[i].pri !== null ? response[i].pri : response[i].military_nbr
            }`,
            value: response[i].user_id
          });
        }
      }
    });
    this.setState({ testAdministratorOptions: testAdministratorOptions });
  };

  openAddTestAdministratorPopup = () => {
    this.setState({ showAddTestAdministratorPopup: true });
  };

  closeAddTestAdministratorPopup = () => {
    this.setState({
      showAddTestAdministratorPopup: false,
      selectedTestAdministrators: [],
      selectedTestAdministratorOptionsAccessibility: LOCALIZE.commons.none
    });
  };

  // get selected test order number option
  getSelectedTestAdministratorOption = selectedOption => {
    let option = null;
    if (selectedOption === null) {
      option = [];
    } else {
      option = selectedOption;
    }
    this.setState({
      selectedTestAdministrators: option
    });
    this.getSelectedTestAdministratorOptionsAccessibility(option);
  };

  // getting/formatting current selected users options (for accessibility only)
  getSelectedTestAdministratorOptionsAccessibility = options => {
    let currentOptions = "";
    if (options.length > 0) {
      for (let i = 0; i < options.length; i++) {
        currentOptions += `${options[i].label}, `;
      }
    } else {
      currentOptions = LOCALIZE.commons.none;
    }
    this.setState({ selectedTestAdministratorOptionsAccessibility: currentOptions });
  };

  handleAssociateTestAdministrators = () => {
    // creating body
    const body = {
      test_center_id: this.props.testCenterData.id,
      test_administrators: this.state.selectedTestAdministrators
    };
    this.props.associateTestAdministratorsToTestCenter(body).then(response => {
      // successful request
      if (response.ok) {
        // populating table
        this.populateAssociatedTestAdministratorsBasedOnActiveSearch();
        // closing popup
        this.closeAddTestAdministratorPopup();
        // should never happen
      } else {
        throw new Error("An error occurred during the add test administrators process");
      }
    });
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
            .topTabs.testAdministrators.table.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
            .topTabs.testAdministrators.table.columnTwo,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
            .topTabs.testAdministrators.table.columnThree,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
            .topTabs.testAdministrators.table.columnFour,
        style: COMMON_STYLE.CENTERED_TEXT
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <div>
          <CustomButton
            label={
              <>
                <FontAwesomeIcon icon={faPlusCircle} style={styles.buttonIcon} />
                <span>
                  {
                    LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                      .testCenterManagement.topTabs.testAdministrators.addTestAdministratorButton
                  }
                </span>
              </>
            }
            action={this.openAddTestAdministratorPopup}
            buttonTheme={THEME.PRIMARY}
          />
        </div>
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"test-center-test-administrators"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchTestCenterTestAdministratorsStates}
            updatePageState={this.props.updateCurrentTestCenterTestAdministratorsPageState}
            paginationPageSize={Number(this.props.testCenterTestAdministratorsPaginationPageSize)}
            updatePaginationPageSize={this.props.updateTestCenterTestAdministratorsPageSizeState}
            data={this.state.associatedTestAdministrators}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.testCenterTestAdministratorsKeyword}
          />
          <div>
            <GenericTable
              classnamePrefix="test-center-test-administrators"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.testAdministrators.table.noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <Pagination
            paginationContainerId={"test-center-test-administrators-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.testCenterTestAdministratorsPaginationPage}
            updatePaginationPageState={
              this.props.updateCurrentTestCenterTestAdministratorsPageState
            }
            firstTableRowId={"test-center-test-administrators-table-row-0"}
          />
        </div>
        <PopupBox
          show={this.state.showAddTestAdministratorPopup}
          handleClose={() => {}}
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
              .topTabs.testAdministrators.addTestAdministratorPopup.title
          }
          description={
            <div>
              <p>
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                    .testCenterManagement.topTabs.testAdministrators.addTestAdministratorPopup
                    .description
                }
              </p>
              <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label
                    id="test-center-test-administrators-test-administrators-label"
                    style={styles.inputTitles}
                  >
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.testAdministrators.addTestAdministratorPopup
                        .testAdministratorsLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <DropdownSelect
                    idPrefix="test-center-test-administrators-test-administrators"
                    isValid={true}
                    ariaRequired={true}
                    ariaLabelledBy="test-center-test-administrators-test-administrators-label"
                    hasPlaceholder={true}
                    options={this.state.testAdministratorOptions}
                    onChange={this.getSelectedTestAdministratorOption}
                    defaultValue={this.state.selectedTestAdministrators}
                    isMulti={true}
                  />
                  <label
                    id="users-current-value-accessibility"
                    style={styles.hiddenText}
                  >{`${LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement.topTabs.testAdministrators.addTestAdministratorPopup.testAdministratorsCurrentValueAccessibility} ${this.state.selectedTestAdministratorOptionsAccessibility}`}</label>
                </Col>
              </Row>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeAddTestAdministratorPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
              .topTabs.testAdministrators.addTestAdministratorPopup.addTestAdministratorsButton
          }
          rightButtonAction={this.handleAssociateTestAdministrators}
          rightButtonIcon={faPlusCircle}
          rightButtonState={
            this.state.selectedTestAdministrators.length > 0
              ? BUTTON_STATE.enabled
              : BUTTON_STATE.disabled
          }
        />
        <PopupBox
          show={this.state.showDeleteTestAdministratorPopup}
          handleClose={() => {}}
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
              .topTabs.testAdministrators.deleteTestAdministratorPopup.title
          }
          size="lg"
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={LOCALIZE.commons.warning}
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.testAdministrators
                        .deleteTestAdministratorPopup.systemMessageDescription,
                      <span style={styles.boldText}>
                        {this.state.selectedTestAdministratorToDelete.first_name}
                      </span>,
                      <span style={styles.boldText}>
                        {this.state.selectedTestAdministratorToDelete.last_name}
                      </span>
                    )}
                  </p>
                }
              />
              <p>
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                    .testCenterManagement.topTabs.testAdministrators.deleteTestAdministratorPopup
                    .description
                }
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeDeleteTestAdministratorPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.handleDeleteTestAdministrator}
          rightButtonIcon={faTrashAlt}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testCenterData: state.testCenter.testCenterData,
    testCenterTestAdministratorsPaginationPage:
      state.testCenter.testCenterTestAdministratorsPaginationPage,
    testCenterTestAdministratorsPaginationPageSize:
      state.testCenter.testCenterTestAdministratorsPaginationPageSize,
    testCenterTestAdministratorsKeyword: state.testCenter.testCenterTestAdministratorsKeyword,
    testCenterTestAdministratorsActiveSearch:
      state.testCenter.testCenterTestAdministratorsActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getUsersBasedOnSpecifiedPermission,
      associateTestAdministratorsToTestCenter,
      getAssociatedTestAdministratorsToTestCenter,
      getFoundAssociatedTestAdministratorsToTestCenter,
      updateCurrentTestCenterTestAdministratorsPageState,
      updateTestCenterTestAdministratorsPageSizeState,
      updateSearchTestCenterTestAdministratorsStates,
      deleteTestAdministratorFromTestCenter
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestAdministrators);
