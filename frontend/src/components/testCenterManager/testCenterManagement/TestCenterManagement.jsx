import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import TestCenterInfo from "./TestCenterInfo";
import TestAdministrators from "./TestAdministrators";
import Rooms from "./Rooms";
import { setTestCenterManagementSelectedTopTabState } from "../../../modules/TestCenterRedux";

const styles = {
  mainContainer: {
    width: "100%"
  },
  tabNavigation: {
    borderBottom: "none"
  },
  tabContainer: {
    marginTop: 2,
    borderTop: "3px solid #00565e",
    backgroundColor: "#F8F8F8"
  }
};

class TestCenterManagement extends Component {
  render() {
    const TABS = [
      {
        key: "test-center-management-test-center-info",
        tabName:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
            .topTabs.centerInfo.title,
        body: <TestCenterInfo />
      },
      {
        key: "test-center-management-test-administrators",
        tabName:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
            .topTabs.testAdministrators.title,
        body: <TestAdministrators />
      },
      {
        key: "test-center-management-rooms",
        tabName:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
            .topTabs.rooms.title,
        body: <Rooms />
      }
    ];
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>
            {
              LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                .testCenterManagement.title
            }
          </h2>
        </div>
        <div>
          <Row>
            <Col>
              <Tabs
                defaultActiveKey={
                  this.props.testCenterManagementSelectedTopTab !== "" &&
                  this.props.testCenterManagementSelectedTopTab !== null
                    ? this.props.testCenterManagementSelectedTopTab
                    : "test-center-management-test-center-info"
                }
                id="test-center-management-tabs"
                style={styles.tabNavigation}
                onSelect={event => this.props.setTestCenterManagementSelectedTopTabState(event)}
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={styles.tabContainer}
                    >
                      {tab.body}
                    </Tab>
                  );
                })}
              </Tabs>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testCenterData: state.testCenter.testCenterData,
    testCenterManagementSelectedTopTab: state.testCenter.testCenterManagementSelectedTopTab
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setTestCenterManagementSelectedTopTabState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestCenterManagement);
