import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Col, Pagination } from "react-bootstrap";
import CustomButton from "../../commons/CustomButton";
import THEME from "../../commons/CustomButtonTheme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPencilAlt,
  faPlusCircle,
  faSave,
  faTimes,
  faTrashAlt,
  faLanguage
} from "@fortawesome/free-solid-svg-icons";
import { getLineSpacingCSS } from "../../../modules/AccommodationsRedux";
import PopupBox, { BUTTON_STATE, BUTTON_TYPE } from "../../commons/PopupBox";
import { getUsersBasedOnSpecifiedPermission } from "../../../modules/PermissionsRedux";
import {
  associateTestAdministratorsToTestCenter,
  getAssociatedTestAdministratorsToTestCenter,
  getFoundAssociatedTestAdministratorsToTestCenter,
  deleteTestAdministratorFromTestCenter,
  updateCurrentTestCenterRoomsPageState,
  updateTestCenterRoomsPageSizeState,
  updateSearchTestCenterRoomsStates,
  addRoomToTestCenter,
  getTestCenterRooms,
  getFoundTestCenterRooms,
  deleteTestCenterRoom,
  editTestCenterRoom,
  getSelectedTestCenterData,
  setTestCenterData,
  triggerTableUpdates
} from "../../../modules/TestCenterRedux";
import StyledTooltip, { EFFECT, TYPE } from "../../authentication/StyledTooltip";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import SearchBarWithDisplayOptions from "../../commons/SearchBarWithDisplayOptions";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import { validateEmail } from "../../../helpers/regexValidator";
import Switch from "react-switch";
import getSwitchTransformScale from "../../../helpers/switchTransformScale";
import MultilingualField, { FIELD_TYPES } from "../../commons/MultilingualField";
import * as _ from "lodash";

const columnSizes = {
  firstColumn: {
    xs: 12,
    sm: 12,
    md: 12,
    lg: 5,
    xl: 5
  },
  secondColumn: {
    xs: 10,
    sm: 10,
    md: 11,
    lg: 6,
    xl: 6
  },
  thirdColumn: {
    xs: 2,
    sm: 2,
    md: 1,
    lg: 1,
    xl: 1
  }
};

const styles = {
  mainContainer: {
    margin: "24px 0"
  },
  rowContainer: {
    margin: "12px 48px"
  },
  rowContainerWithError: {
    margin: "0 48px"
  },
  inputTitles: {
    fontWeight: "bold"
  },
  buttonIcon: {
    marginRight: 6
  },
  tableIcon: {
    transform: "scale(1.2)"
  },
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e"
  },
  allUnset: {
    all: "unset"
  },
  boldText: {
    fontWeight: "bold"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  textArea: {
    padding: 6,
    border: "1px solid #00565E",
    borderRadius: 4,
    width: "100%",
    minHeight: 88,
    resize: "none"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "6px 0 12px 0"
  },
  translateButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: 0,
    margin: "0 3px",
    color: "#00565e"
  }
};

class Rooms extends Component {
  state = {
    currentlyLoading: false,
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    resultsFound: 0,
    rooms: [],
    clearSearchTriggered: false,
    showAddRoomPopup: false,
    nameContent: "",
    emailContent: "",
    maximumOccupancyContent: "",
    activeSwitchState: false,
    isValidEmail: true,
    isValidForm: false,
    showEditRoomPopup: false,
    roomUpdatedSuccessfully: false,
    showDeleteRoomPopup: false,
    showRoomInUseError: false,
    selectedRoomToEditOrDelete: {},
    // default values (switch style)
    isLoadingSwitch: false,
    switchHeight: 25,
    switchWidth: 50,
    // Room Other Details Variables
    otherDetailsContent: { en: [{ text: "" }], fr: [{ text: "" }] },
    showAddOtherDetailsPopup: false,
    showModifyOtherDetailsPopup: false
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentTestCenterRoomsPageState(1);
    // populating rooms
    this.populateRoomsBasedOnActiveSearch();
    // getting updated switch dimensions
    this.getUpdatedSwitchDimensions();
  };

  componentDidUpdate = prevProps => {
    // if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateRoomsBasedOnActiveSearch();
    }
    // if testCenterRoomsPaginationPage gets updated
    if (prevProps.testCenterRoomsPaginationPage !== this.props.testCenterRoomsPaginationPage) {
      this.populateRoomsBasedOnActiveSearch();
    }
    // if testCenterRoomsPaginationPageSize get updated
    if (
      prevProps.testCenterRoomsPaginationPageSize !== this.props.testCenterRoomsPaginationPageSize
    ) {
      this.populateRoomsBasedOnActiveSearch();
    }
    // if search testCenterRoomsKeyword gets updated
    if (prevProps.testCenterRoomsKeyword !== this.props.testCenterRoomsKeyword) {
      // if testCenterRoomsKeyword redux state is empty
      if (this.props.testCenterRoomsKeyword !== "") {
        this.populateFoundRooms();
      } else if (
        this.props.testCenterRoomsKeyword === "" &&
        this.props.testCenterRoomsActiveSearch
      ) {
        this.populateFoundRooms();
      }
    }
    // if testCenterRoomsActiveSearch gets updated
    if (prevProps.testCenterRoomsActiveSearch !== this.props.testCenterRoomsActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.testCenterRoomsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateRooms();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundRooms();
      }
    }
    // if fontSize gets updated
    if (prevProps.accommodations.fontSize !== this.props.accommodations.fontSize) {
      this.getUpdatedSwitchDimensions();
    }
  };

  populateRoomsBasedOnActiveSearch = () => {
    // current search
    if (this.props.testCenterRoomsActiveSearch) {
      this.populateFoundRooms();
      // no search
    } else {
      this.populateRooms();
    }
  };

  // populating associated test center test administrators
  populateRooms = () => {
    this.setState({ currentlyLoading: true }, () => {
      const roomsArray = [];
      this.props
        .getTestCenterRooms(
          this.props.testCenterRoomsPaginationPage,
          this.props.testCenterRoomsPaginationPageSize,
          this.props.testCenterData.id
        )
        .then(response => {
          this.populateTestCenterRoomsObject(roomsArray, response);
        })
        .then(() => {
          if (this.state.clearSearchTriggered) {
            // go back to the first page to avoid display bugs
            this.props.updateCurrentTestCenterRoomsPageState(1);
            this.setState({ clearSearchTriggered: false });
          }
        })
        .then(() => {
          this.setState({ currentlyLoading: false });
        });
    });
  };

  // populating found associated test center test administrators
  populateFoundRooms = () => {
    // making sure that the table is not currently loading
    if (!this.state.currentlyLoading) {
      this.setState(
        { currentlyLoading: true },
        () => {
          const roomsArray = [];
          setTimeout(() => {
            this.props
              .getFoundTestCenterRooms(
                this.props.testCenterRoomsKeyword,
                this.props.testCenterRoomsPaginationPage,
                this.props.testCenterRoomsPaginationPageSize,
                this.props.testCenterData.id
              )
              .then(response => {
                // there are no results found
                if (response[0] === "no results found") {
                  this.setState({
                    rooms: [],
                    rowsDefinition: {},
                    numberOfPages: 1,
                    nextPageNumber: 0,
                    previousPageNumber: 0,
                    resultsFound: 0
                  });
                  // there is at least one result found
                } else {
                  this.populateTestCenterRoomsObject(roomsArray, response);
                  this.setState({ resultsFound: response.count });
                }
              })
              .then(() => {
                this.setState(
                  {
                    currentlyLoading: false
                  },
                  () => {
                    // make sure that this element exsits to avoid any error
                    if (document.getElementById("test-center-rooms-results-found")) {
                      document.getElementById("test-center-rooms-results-found").focus();
                    }
                  }
                );
              });
          });
        },
        250
      );
    }
  };

  populateTestCenterRoomsObject = (roomsArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    const data = [];
    // making sure that there are some results
    if (typeof response.results !== "undefined") {
      // looping in response given
      for (let i = 0; i < response.results.length; i++) {
        const currentResult = response.results[i];
        // pushing needed data in roomsArray
        roomsArray.push({
          id: currentResult.id,
          name: currentResult.name,
          email: currentResult.email,
          max_occupancy: currentResult.max_occupancy,
          other_details: currentResult.other_details_text,
          active: currentResult.active
        });
        // pushing needed data in data array (needed for GenericTable component)
        data.push({
          column_1: currentResult.name,
          column_2: currentResult.max_occupancy,
          column_3: currentResult.active ? LOCALIZE.commons.active : LOCALIZE.commons.inactive,
          column_4: (
            <>
              <StyledTooltip
                id={`test-center-rooms-edit-row-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`test-center-rooms-edit-row-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faPencilAlt} style={styles.tableIcon} />
                        </>
                      }
                      action={() => {
                        this.openEditRoomPopup(roomsArray[i]);
                      }}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                          .testCenterManagement.topTabs.rooms.table.editButtonAccessibility,
                        currentResult.name
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                          .testCenterManagement.topTabs.rooms.table.editButtonTooltip
                      }
                    </p>
                  </div>
                }
              />
              <StyledTooltip
                id={`test-center-rooms-delete-row-${i}`}
                place="top"
                variant={TYPE.light}
                effect={EFFECT.solid}
                openOnClick={false}
                tooltipElement={
                  <div style={styles.allUnset}>
                    <CustomButton
                      dataTip=""
                      dataFor={`test-center-rooms-delete-row-${i}`}
                      label={
                        <>
                          <FontAwesomeIcon icon={faTrashAlt} style={styles.tableIcon} />
                        </>
                      }
                      action={() => {
                        this.openDeleteRoomPopup(roomsArray[i]);
                      }}
                      customStyle={styles.actionButton}
                      buttonTheme={THEME.PRIMARY}
                      ariaLabel={LOCALIZE.formatString(
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                          .testCenterManagement.topTabs.rooms.table.deleteButtonAccessibility,
                        currentResult.name
                      )}
                    />
                  </div>
                }
                tooltipContent={
                  <div>
                    <p>
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                          .testCenterManagement.topTabs.rooms.table.deleteButtonTooltip
                      }
                    </p>
                  </div>
                }
              />
            </>
          )
        });
      }
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: COMMON_STYLE.LEFT_TEXT,
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      column_3_style: COMMON_STYLE.CENTERED_TEXT,
      column_4_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };

    // saving results in state
    this.setState({
      rooms: roomsArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(response.count / this.props.testCenterRoomsPaginationPageSize),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  openEditRoomPopup = roomData => {
    this.setState(
      {
        showEditRoomPopup: true,
        selectedRoomToEditOrDelete: roomData,
        nameContent: roomData.name,
        emailContent: roomData.email,
        maximumOccupancyContent: roomData.max_occupancy,
        otherDetailsContent: roomData.other_details,
        activeSwitchState: roomData.active
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  closeEditRoomPopup = () => {
    this.setState(
      {
        isValidForm: false,
        showEditRoomPopup: false,
        selectedRoomToEditOrDelete: {},
        nameContent: "",
        emailContent: "",
        maximumOccupancyContent: "",
        otherDetailsContent: { en: [{ text: "" }], fr: [{ text: "" }] },
        activeSwitchState: false
      },
      () => {
        // adding small delay to reset the roomUpdatedSuccessfully state to avoid closing anymation glitch
        setTimeout(() => {
          this.setState({ roomUpdatedSuccessfully: false });
        }, 200);
      }
    );
  };

  openDeleteRoomPopup = roomData => {
    this.setState({
      showDeleteRoomPopup: true,
      selectedRoomToEditOrDelete: roomData
    });
  };

  closeDeleteRoomPopup = () => {
    this.setState({
      showDeleteRoomPopup: false,
      selectedRoomToEditOrDelete: {},
      showRoomInUseError: false
    });
  };

  handleDeleteRoom = () => {
    // creating body
    const body = {
      id: this.state.selectedRoomToEditOrDelete.id,
      test_center_id: this.props.testCenterData.id
    };
    this.props.deleteTestCenterRoom(body).then(response => {
      // successful request
      if (response.ok) {
        // closing popup and updating table
        this.closeDeleteRoomPopup();
        // trigger table updates
        this.props.triggerTableUpdates();
        // populating table
        this.populateRoomsBasedOnActiveSearch();
        // getting selected test center data and setting redux states
        this.props
          .getSelectedTestCenterData(this.props.testCenterData.id)
          .then(testCenterResponse => {
            this.props.setTestCenterData(testCenterResponse);
          });
        // room is in use for current and/or future test sessions
      } else if (response.status === 409) {
        // show room in use error
        this.setState({ showRoomInUseError: true });
        // should never happen
      } else {
        throw new Error("An error occurred during the delete room process");
      }
    });
  };

  openAddRoomPopup = () => {
    this.setState({ showAddRoomPopup: true });
  };

  closeAddRoomPopup = () => {
    this.setState({
      showAddRoomPopup: false,
      nameContent: "",
      emailContent: "",
      maximumOccupancyContent: "",
      otherDetailsContent: { en: [{ text: "" }], fr: [{ text: "" }] },
      activeSwitchState: false,
      isValidEmail: true,
      isValidForm: false
    });
  };

  updateNameContent = event => {
    const nameContent = event.target.value;
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,150})$/;
    if (regexExpression.test(nameContent)) {
      this.setState({ nameContent: nameContent }, () => {
        // validating form
        this.validateForm();
      });
    }
  };

  updateEmailContent = event => {
    const emailContent = event.target.value;
    // allow maximum of 150 chars
    const regexExpression = /^(.{0,254})$/;
    if (regexExpression.test(emailContent)) {
      this.setState({ emailContent: emailContent }, () => {
        // validating form
        this.validateForm();
      });
    }
  };

  updateMaximumOccupancyContent = event => {
    const maximumOccupancyContent = event.target.value;
    // allow maximum of 150 chars
    const regexExpression = /^([0-9]{0,5})$/;
    if (regexExpression.test(maximumOccupancyContent)) {
      this.setState(
        {
          maximumOccupancyContent:
            maximumOccupancyContent !== "" ? parseInt(maximumOccupancyContent) : ""
        },
        () => {
          // validating form
          this.validateForm();
        }
      );
    }
  };

  /*--------------------------*/
  //    Room Other Details    //
  /*--------------------------*/
  updateOtherDetailsContent = event => {
    // Removing _name to get the language
    const language =
      event.target.name !== ""
        ? event.target.name.replace("_name", "")
        : this.props.currentLanguage;

    const otherDetailsContent = event.target.value;

    const temp = _.cloneDeep(this.state.otherDetailsContent);

    // set the language's text in our other details object
    temp[language][0].text = otherDetailsContent;

    // update the state
    this.setState(
      {
        otherDetailsContent: { ...temp }
      },
      () => {
        // validating form
        this.validateForm();
      }
    );
  };

  // From Create Popup
  // Open
  handleOpenAddOtherDetailsPopup = () => {
    this.setState({ showAddOtherDetailsPopup: true, showAddRoomPopup: false });
  };

  // Close
  handleCloseAddOtherDetailsPopup = () => {
    this.setState({ showAddOtherDetailsPopup: false, showAddRoomPopup: true });
  };

  // From Modify Popup
  // Open
  handleOpenModifyOtherDetailsPopup = () => {
    this.setState({ showModifyOtherDetailsPopup: true, showEditRoomPopup: false });
  };

  // Close
  handleCloseModifyOtherDetailsPopup = () => {
    this.setState({ showModifyOtherDetailsPopup: false, showEditRoomPopup: true });
  };

  updateActiveSwitch = event => {
    this.setState({ activeSwitchState: event });
  };

  // getting switch dimensions and updating states
  getUpdatedSwitchDimensions = () => {
    // need this loading state, otherwise the switch diameter is not calculated on state change
    this.setState({ isLoadingSwitch: true }, () => {
      this.setState(
        {
          switchHeight: getSwitchTransformScale(this.props.accommodations.fontSize).height,
          switchWidth: getSwitchTransformScale(this.props.accommodations.fontSize).width
        },
        () => {
          this.setState({ isLoadingSwitch: false });
        }
      );
    });
  };

  validateForm = () => {
    // initializing needed variables
    let isValidForm = false;

    const isValidName = this.state.nameContent !== "";
    const isValidEmail = validateEmail(this.state.emailContent) && this.state.emailContent !== "";
    const isValidMaximumOccupancy = this.state.maximumOccupancyContent !== "";

    // if all mandatory fields are valid
    if (isValidName && isValidEmail && isValidMaximumOccupancy) {
      isValidForm = true;
    }
    this.setState({
      isValidForm: isValidForm,
      isValidEmail: isValidEmail
    });
  };

  handleAddRoom = () => {
    // creating body
    const body = {
      test_center_id: this.props.testCenterData.id,
      name: this.state.nameContent,
      email: this.state.emailContent,
      max_occupancy: this.state.maximumOccupancyContent,
      other_details: this.state.otherDetailsContent,
      active: this.state.activeSwitchState
    };
    this.props.addRoomToTestCenter(body).then(response => {
      // successful request
      if (response.ok) {
        // trigger table updates
        this.props.triggerTableUpdates();
        // populating table
        this.populateRoomsBasedOnActiveSearch();
        // closing popup
        this.closeAddRoomPopup();
        // getting selected test center data and setting redux states
        this.props
          .getSelectedTestCenterData(this.props.testCenterData.id)
          .then(testCenterResponse => {
            this.props.setTestCenterData(testCenterResponse);
          });
        // should never happen
      } else {
        throw new Error("An error occurred during the add room process");
      }
    });
  };

  handleEditRoom = () => {
    // creating body
    const body = {
      id: this.state.selectedRoomToEditOrDelete.id,
      test_center_id: this.props.testCenterData.id,
      name: this.state.nameContent,
      email: this.state.emailContent,
      max_occupancy: this.state.maximumOccupancyContent,
      other_details: this.state.otherDetailsContent,
      active: this.state.activeSwitchState
    };
    this.props.editTestCenterRoom(body).then(response => {
      // successful request
      if (response.ok) {
        // trigger table updates
        this.props.triggerTableUpdates();
        // populating table
        this.populateRoomsBasedOnActiveSearch();
        // show success message
        this.setState({ roomUpdatedSuccessfully: true });
        // getting selected test center data and setting redux states
        this.props
          .getSelectedTestCenterData(this.props.testCenterData.id)
          .then(testCenterResponse => {
            this.props.setTestCenterData(testCenterResponse);
          });
        // should never happen
      } else {
        throw new Error("An error occurred during the edit room process");
      }
    });
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
            .topTabs.rooms.table.columnOne,
        style: COMMON_STYLE.LEFT_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
            .topTabs.rooms.table.columnTwo,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
            .topTabs.rooms.table.columnThree,
        style: COMMON_STYLE.CENTERED_TEXT
      },
      {
        label:
          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
            .topTabs.rooms.table.columnFour,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    const lineSpacingStyle = getLineSpacingCSS();
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        ...{
          lineHeight: lineSpacingStyle.lineHeight,
          letterSpacing: lineSpacingStyle.letterSpacing,
          wordSpacing: lineSpacingStyle.wordSpacing
        }
      };
    }

    return (
      <div style={styles.mainContainer}>
        <div>
          <CustomButton
            label={
              <>
                <FontAwesomeIcon icon={faPlusCircle} style={styles.buttonIcon} />
                <span>
                  {
                    LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                      .testCenterManagement.topTabs.rooms.addRoomButton
                  }
                </span>
              </>
            }
            action={this.openAddRoomPopup}
            buttonTheme={THEME.PRIMARY}
          />
        </div>
        <div>
          <SearchBarWithDisplayOptions
            idPrefix={"test-center-rooms"}
            currentlyLoading={this.state.currentlyLoading}
            updateSearchStates={this.props.updateSearchTestCenterRoomsStates}
            updatePageState={this.props.updateCurrentTestCenterRoomsPageState}
            paginationPageSize={Number(this.props.testCenterRoomsPaginationPageSize)}
            updatePaginationPageSize={this.props.updateTestCenterRoomsPageSizeState}
            data={this.state.rooms}
            resultsFound={this.state.resultsFound}
            searchBarContent={this.props.testCenterRoomsKeyword}
          />
          <div>
            <GenericTable
              classnamePrefix="test-center-rooms"
              columnsDefinition={columnsDefinition}
              rowsDefinition={this.state.rowsDefinition}
              emptyTableMessage={
                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.rooms.table.noData
              }
              currentlyLoading={this.state.currentlyLoading}
            />
          </div>
          <Pagination
            paginationContainerId={"test-center-rooms-pagination-pages-link"}
            pageCount={this.state.numberOfPages}
            paginationPage={this.props.testCenterRoomsPaginationPage}
            updatePaginationPageState={this.props.updateCurrentTestCenterRoomsPageState}
            firstTableRowId={"test-center-rooms-table-row-0"}
          />
        </div>
        <PopupBox
          show={this.state.showAddRoomPopup}
          handleClose={() => {}}
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
              .topTabs.rooms.addRoomPopup.title
          }
          description={
            <div>
              <p>
                {
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                    .testCenterManagement.topTabs.rooms.addRoomPopup.description
                }
              </p>
              <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label htmlFor="test-center-rooms-name-input" style={styles.inputTitles}>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.rooms.addRoomPopup.nameLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <input
                    id="test-center-rooms-name-input"
                    className={"valid-field"}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.nameContent}
                    onChange={this.updateNameContent}
                  ></input>
                </Col>
              </Row>
              <Row
                role="presentation"
                className="align-items-center"
                style={
                  this.state.isValidEmail && this.state.emailContent !== ""
                    ? styles.rowContainer
                    : styles.rowContainerWithError
                }
              >
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label htmlFor="test-center-rooms-email-input" style={styles.inputTitles}>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.rooms.addRoomPopup.emailLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <input
                    id="test-center-rooms-email-input"
                    className={
                      this.state.isValidEmail || this.state.emailContent === ""
                        ? "valid-field"
                        : "invalid-field"
                    }
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.emailContent}
                    onChange={this.updateEmailContent}
                  ></input>
                </Col>
              </Row>
              {!this.state.isValidEmail && this.state.emailContent !== "" && (
                <Row
                  className="align-items-center justify-content-start"
                  style={styles.rowContainerWithError}
                >
                  <Col
                    xl={columnSizes.firstColumn.xl}
                    lg={columnSizes.firstColumn.lg}
                    md={columnSizes.firstColumn.md}
                    sm={columnSizes.firstColumn.sm}
                    xs={columnSizes.firstColumn.xs}
                  ></Col>
                  <Col
                    xl={columnSizes.secondColumn.xl}
                    lg={columnSizes.secondColumn.lg}
                    md={columnSizes.secondColumn.md}
                    sm={columnSizes.secondColumn.sm}
                    xs={columnSizes.secondColumn.xs}
                  >
                    <label
                      id="test-center-rooms-email-error"
                      htmlFor="test-center-rooms-email-input"
                      style={styles.errorMessage}
                    >
                      {
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                          .testCenterManagement.topTabs.rooms.addRoomPopup.emailErrorMessage
                      }
                    </label>
                  </Col>
                </Row>
              )}
              <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label
                    htmlFor="test-center-rooms-maximum-occupancy-input"
                    style={styles.inputTitles}
                  >
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.rooms.addRoomPopup.maximumOccupancyLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <input
                    id="test-center-rooms-maximum-occupancy-input"
                    className={"valid-field"}
                    style={{ ...styles.input, ...accommodationsStyle }}
                    type="text"
                    value={this.state.maximumOccupancyContent}
                    onChange={this.updateMaximumOccupancyContent}
                  ></input>
                </Col>
              </Row>
              <Row role="presentation" className="align-items-start" style={styles.rowContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label htmlFor="test-center-rooms-other-details-input" style={styles.inputTitles}>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.rooms.addRoomPopup.otherDetailsLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  <textarea
                    id="test-center-rooms-other-details-input"
                    className={"valid-field"}
                    style={styles.textArea}
                    value={this.state.otherDetailsContent[this.props.currentLanguage][0].text}
                    onChange={this.updateOtherDetailsContent}
                    maxLength="500"
                  ></textarea>
                </Col>
                <Col
                  xl={columnSizes.thirdColumn.xl}
                  lg={columnSizes.thirdColumn.lg}
                  md={columnSizes.thirdColumn.md}
                  sm={columnSizes.thirdColumn.sm}
                  xs={columnSizes.thirdColumn.xs}
                  className={"text-left"}
                >
                  <StyledTooltip
                    id={`test-center-rooms-other-details-translations`}
                    place="top"
                    variant={TYPE.light}
                    effect={EFFECT.solid}
                    openOnClick={false}
                    tooltipElement={
                      <div style={styles.allUnset}>
                        <CustomButton
                          className="h-100"
                          dataTip=""
                          dataFor={`test-center-rooms-other-details-translations`}
                          label={<FontAwesomeIcon icon={faLanguage} />}
                          ariaLabel={
                            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                              .testCenterManagement.topTabs.rooms.translationsPopup.otherDetails
                              .translationsButtonAriaLabel
                          }
                          action={() => this.handleOpenAddOtherDetailsPopup()}
                          customStyle={{
                            ...styles.translateButton,
                            fontSize: this.props.accommodations.fontSize.replace("px", "") * 1.5
                          }}
                          buttonTheme={THEME.SECONDARY}
                        />
                      </div>
                    }
                    tooltipContent={
                      <div>
                        <p>
                          {
                            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                              .testCenterManagement.topTabs.rooms.translationsPopup.otherDetails
                              .translationsButtonTooltip
                          }
                        </p>
                      </div>
                    }
                  />
                </Col>
              </Row>
              <Row role="presentation" className="align-items-center" style={styles.rowContainer}>
                <Col
                  xl={columnSizes.firstColumn.xl}
                  lg={columnSizes.firstColumn.lg}
                  md={columnSizes.firstColumn.md}
                  sm={columnSizes.firstColumn.sm}
                  xs={columnSizes.firstColumn.xs}
                >
                  <label id="test-center-rooms-active-label" style={styles.inputTitles}>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.rooms.addRoomPopup.activeLabel
                    }
                  </label>
                </Col>
                <Col
                  xl={columnSizes.secondColumn.xl}
                  lg={columnSizes.secondColumn.lg}
                  md={columnSizes.secondColumn.md}
                  sm={columnSizes.secondColumn.sm}
                  xs={columnSizes.secondColumn.xs}
                >
                  {!this.state.isLoadingSwitch && (
                    <Switch
                      onChange={this.updateActiveSwitch}
                      checked={this.state.activeSwitchState}
                      aria-labelledby="test-center-rooms-active-label"
                      height={this.state.switchHeight}
                      width={this.state.switchWidth}
                    />
                  )}
                </Col>
              </Row>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeAddRoomPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
              .topTabs.rooms.addRoomPopup.addRoomButton
          }
          rightButtonAction={this.handleAddRoom}
          rightButtonIcon={faPlusCircle}
          rightButtonState={this.state.isValidForm ? BUTTON_STATE.enabled : BUTTON_STATE.disabled}
        />
        <PopupBox
          show={this.state.showEditRoomPopup}
          handleClose={() => {}}
          title={
            !this.state.roomUpdatedSuccessfully
              ? LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.rooms.editRoomPopup.title
              : LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.rooms.editRoomPopup.titleSuccess
          }
          size={!this.state.roomUpdatedSuccessfully ? "xl" : "lg"}
          description={
            <>
              {!this.state.roomUpdatedSuccessfully ? (
                <div>
                  <p>
                    {
                      LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                        .testCenterManagement.topTabs.rooms.editRoomPopup.description
                    }
                  </p>
                  <Row
                    role="presentation"
                    className="align-items-center"
                    style={styles.rowContainer}
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label htmlFor="test-center-rooms-name-input" style={styles.inputTitles}>
                        {
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                            .testCenterManagement.topTabs.rooms.editRoomPopup.nameLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="test-center-rooms-name-input"
                        className={"valid-field"}
                        style={{ ...styles.input, ...accommodationsStyle }}
                        type="text"
                        value={this.state.nameContent}
                        onChange={this.updateNameContent}
                      ></input>
                    </Col>
                  </Row>
                  <Row
                    role="presentation"
                    className="align-items-center"
                    style={
                      this.state.isValidEmail && this.state.emailContent !== ""
                        ? styles.rowContainer
                        : styles.rowContainerWithError
                    }
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label htmlFor="test-center-rooms-email-input" style={styles.inputTitles}>
                        {
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                            .testCenterManagement.topTabs.rooms.editRoomPopup.emailLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="test-center-rooms-email-input"
                        className={
                          this.state.isValidEmail || this.state.emailContent === ""
                            ? "valid-field"
                            : "invalid-field"
                        }
                        style={{ ...styles.input, ...accommodationsStyle }}
                        type="text"
                        value={this.state.emailContent}
                        onChange={this.updateEmailContent}
                      ></input>
                    </Col>
                  </Row>
                  {!this.state.isValidEmail && this.state.emailContent !== "" && (
                    <Row
                      className="align-items-center justify-content-start"
                      style={styles.rowContainerWithError}
                    >
                      <Col
                        xl={columnSizes.firstColumn.xl}
                        lg={columnSizes.firstColumn.lg}
                        md={columnSizes.firstColumn.md}
                        sm={columnSizes.firstColumn.sm}
                        xs={columnSizes.firstColumn.xs}
                      ></Col>
                      <Col
                        xl={columnSizes.secondColumn.xl}
                        lg={columnSizes.secondColumn.lg}
                        md={columnSizes.secondColumn.md}
                        sm={columnSizes.secondColumn.sm}
                        xs={columnSizes.secondColumn.xs}
                      >
                        <label
                          id="test-center-rooms-email-error"
                          htmlFor="test-center-rooms-email-input"
                          style={styles.errorMessage}
                        >
                          {
                            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                              .testCenterManagement.topTabs.rooms.editRoomPopup.emailErrorMessage
                          }
                        </label>
                      </Col>
                    </Row>
                  )}
                  <Row
                    role="presentation"
                    className="align-items-center"
                    style={styles.rowContainer}
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="test-center-rooms-maximum-occupancy-input"
                        style={styles.inputTitles}
                      >
                        {
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                            .testCenterManagement.topTabs.rooms.editRoomPopup.maximumOccupancyLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <input
                        id="test-center-rooms-maximum-occupancy-input"
                        className={"valid-field"}
                        style={{ ...styles.input, ...accommodationsStyle }}
                        type="text"
                        value={this.state.maximumOccupancyContent}
                        onChange={this.updateMaximumOccupancyContent}
                      ></input>
                    </Col>
                  </Row>
                  <Row
                    role="presentation"
                    className="align-items-start"
                    style={styles.rowContainer}
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label
                        htmlFor="test-center-rooms-other-details-input"
                        style={styles.inputTitles}
                      >
                        {
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                            .testCenterManagement.topTabs.rooms.editRoomPopup.otherDetailsLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      <textarea
                        id="test-center-rooms-other-details-input"
                        className={"valid-field"}
                        style={styles.textArea}
                        value={this.state.otherDetailsContent[this.props.currentLanguage][0].text}
                        onChange={this.updateOtherDetailsContent}
                        maxLength="500"
                      ></textarea>
                    </Col>
                    <Col
                      xl={columnSizes.thirdColumn.xl}
                      lg={columnSizes.thirdColumn.lg}
                      md={columnSizes.thirdColumn.md}
                      sm={columnSizes.thirdColumn.sm}
                      xs={columnSizes.thirdColumn.xs}
                      className={"text-left"}
                    >
                      <StyledTooltip
                        id={`test-center-rooms-other-details-translations`}
                        place="top"
                        variant={TYPE.light}
                        effect={EFFECT.solid}
                        openOnClick={false}
                        tooltipElement={
                          <div style={styles.allUnset}>
                            <CustomButton
                              className="h-100"
                              dataTip=""
                              dataFor={`test-center-rooms-other-details-translations`}
                              label={<FontAwesomeIcon icon={faLanguage} />}
                              ariaLabel={
                                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                                  .testCenterManagement.topTabs.rooms.translationsPopup.otherDetails
                                  .translationsButtonAriaLabel
                              }
                              action={() => this.handleOpenModifyOtherDetailsPopup()}
                              customStyle={{
                                ...styles.translateButton,
                                fontSize: this.props.accommodations.fontSize.replace("px", "") * 1.5
                              }}
                              buttonTheme={THEME.SECONDARY}
                            />
                          </div>
                        }
                        tooltipContent={
                          <div>
                            <p>
                              {
                                LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                                  .testCenterManagement.topTabs.rooms.translationsPopup.otherDetails
                                  .translationsButtonTooltip
                              }
                            </p>
                          </div>
                        }
                      />
                    </Col>
                  </Row>
                  <Row
                    role="presentation"
                    className="align-items-center"
                    style={styles.rowContainer}
                  >
                    <Col
                      xl={columnSizes.firstColumn.xl}
                      lg={columnSizes.firstColumn.lg}
                      md={columnSizes.firstColumn.md}
                      sm={columnSizes.firstColumn.sm}
                      xs={columnSizes.firstColumn.xs}
                    >
                      <label id="test-center-rooms-active-label" style={styles.inputTitles}>
                        {
                          LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                            .testCenterManagement.topTabs.rooms.editRoomPopup.activeLabel
                        }
                      </label>
                    </Col>
                    <Col
                      xl={columnSizes.secondColumn.xl}
                      lg={columnSizes.secondColumn.lg}
                      md={columnSizes.secondColumn.md}
                      sm={columnSizes.secondColumn.sm}
                      xs={columnSizes.secondColumn.xs}
                    >
                      {!this.state.isLoadingSwitch && (
                        <Switch
                          onChange={this.updateActiveSwitch}
                          checked={this.state.activeSwitchState}
                          aria-labelledby="test-center-rooms-active-label"
                          height={this.state.switchHeight}
                          width={this.state.switchWidth}
                        />
                      )}
                    </Col>
                  </Row>
                </div>
              ) : (
                <SystemMessage
                  messageType={MESSAGE_TYPE.success}
                  title={LOCALIZE.commons.success}
                  message={
                    <p>
                      {LOCALIZE.formatString(
                        LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                          .testCenterManagement.topTabs.rooms.editRoomPopup
                          .systemMessageDescription,
                        <span style={styles.boldText}>{this.state.nameContent}</span>
                      )}
                    </p>
                  }
                />
              )}
            </>
          }
          leftButtonType={
            !this.state.roomUpdatedSuccessfully ? BUTTON_TYPE.secondary : BUTTON_TYPE.none
          }
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeEditRoomPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            !this.state.roomUpdatedSuccessfully ? LOCALIZE.commons.saveButton : LOCALIZE.commons.ok
          }
          rightButtonAction={
            !this.state.roomUpdatedSuccessfully ? this.handleEditRoom : this.closeEditRoomPopup
          }
          rightButtonIcon={!this.state.roomUpdatedSuccessfully ? faSave : ""}
          rightButtonState={
            !this.state.roomUpdatedSuccessfully
              ? this.state.isValidForm
                ? BUTTON_STATE.enabled
                : BUTTON_STATE.disabled
              : BUTTON_STATE.enabled
          }
        />
        <PopupBox
          show={this.state.showAddOtherDetailsPopup}
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
              .topTabs.rooms.translationsPopup.otherDetails.title
          }
          handleClose={() => this.handleCloseAddOtherDetailsPopup()}
          description={
            <div>
              <MultilingualField
                handleChange={this.updateOtherDetailsContent}
                localizePath={
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                    .testCenterManagement.topTabs.rooms.translationsPopup.otherDetails
                }
                fieldName={"test-center-other-details"}
                values={this.state.otherDetailsContent}
                fieldType={FIELD_TYPES.TEXT_AREA}
              ></MultilingualField>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.handleCloseAddOtherDetailsPopup()}
        />
        <PopupBox
          show={this.state.showModifyOtherDetailsPopup}
          title={
            LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems.testCenterManagement
              .topTabs.rooms.translationsPopup.otherDetails.title
          }
          handleClose={() => this.handleCloseModifyOtherDetailsPopup()}
          description={
            <div>
              <MultilingualField
                handleChange={this.updateOtherDetailsContent}
                localizePath={
                  LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                    .testCenterManagement.topTabs.rooms.translationsPopup.otherDetails
                }
                fieldName={"test-center-other-details"}
                values={this.state.otherDetailsContent}
                fieldType={FIELD_TYPES.TEXT_AREA}
              ></MultilingualField>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.handleCloseModifyOtherDetailsPopup()}
        />
        <PopupBox
          show={this.state.showDeleteRoomPopup}
          handleClose={() => {}}
          title={
            !this.state.showRoomInUseError
              ? LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.rooms.deleteRoomPopup.title
              : LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                  .testCenterManagement.topTabs.rooms.deleteRoomPopup.titleWithError
          }
          size="lg"
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={
                  !this.state.showRoomInUseError ? LOCALIZE.commons.warning : LOCALIZE.commons.error
                }
                message={
                  <p>
                    {LOCALIZE.formatString(
                      !this.state.showRoomInUseError
                        ? LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                            .testCenterManagement.topTabs.rooms.deleteRoomPopup
                            .systemMessageDescription
                        : LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                            .testCenterManagement.topTabs.rooms.deleteRoomPopup
                            .systemMessageDescriptionWithError,
                      <span style={styles.boldText}>
                        {this.state.selectedRoomToEditOrDelete.name}
                      </span>
                    )}
                  </p>
                }
              />
              {!this.state.showRoomInUseError && (
                <p>
                  {
                    LOCALIZE.testCenterManager.testCenterManagement.sideNavigationItems
                      .testCenterManagement.topTabs.rooms.deleteRoomPopup.description
                  }
                </p>
              )}
            </div>
          }
          leftButtonType={!this.state.showRoomInUseError ? BUTTON_TYPE.secondary : BUTTON_TYPE.none}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeDeleteRoomPopup}
          rightButtonType={
            !this.state.showRoomInUseError ? BUTTON_TYPE.danger : BUTTON_TYPE.primary
          }
          rightButtonTitle={
            !this.state.showRoomInUseError ? LOCALIZE.commons.deleteButton : LOCALIZE.commons.ok
          }
          rightButtonAction={
            !this.state.showRoomInUseError ? this.handleDeleteRoom : this.closeDeleteRoomPopup
          }
          rightButtonIcon={!this.state.showRoomInUseError ? faTrashAlt : ""}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations,
    testCenterData: state.testCenter.testCenterData,
    testCenterRoomsPaginationPage: state.testCenter.testCenterRoomsPaginationPage,
    testCenterRoomsPaginationPageSize: state.testCenter.testCenterRoomsPaginationPageSize,
    testCenterRoomsKeyword: state.testCenter.testCenterRoomsKeyword,
    testCenterRoomsActiveSearch: state.testCenter.testCenterRoomsActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getUsersBasedOnSpecifiedPermission,
      associateTestAdministratorsToTestCenter,
      getAssociatedTestAdministratorsToTestCenter,
      getFoundAssociatedTestAdministratorsToTestCenter,
      updateCurrentTestCenterRoomsPageState,
      updateTestCenterRoomsPageSizeState,
      updateSearchTestCenterRoomsStates,
      deleteTestAdministratorFromTestCenter,
      addRoomToTestCenter,
      getTestCenterRooms,
      getFoundTestCenterRooms,
      editTestCenterRoom,
      deleteTestCenterRoom,
      getSelectedTestCenterData,
      setTestCenterData,
      triggerTableUpdates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Rooms);
