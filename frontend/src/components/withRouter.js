import { useLocation, useNavigate, useParams } from "react-router-dom";
import React from "react";

export default function withRouter(WrappedComponent) {
  function ComponentWithRouterProp(props) {
    const location = useLocation();
    const navigate = useNavigate();
    const params = useParams();

    return <WrappedComponent {...props} {...{ location, navigate, params }} />;
  }
  return ComponentWithRouterProp;
}
