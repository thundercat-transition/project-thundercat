import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action types
export const SET_USER_PROFILE_CHANGE_REQUESTS_PAGINATION_PAGE =
  "userProfileChangeRequests/SET_USER_PROFILE_CHANGE_REQUESTS_PAGINATION_PAGE";
export const SET_USER_PROFILE_CHANGE_REQUESTS_PAGINATION_PAGE_SIZE =
  "userProfileChangeRequests/SET_USER_PROFILE_CHANGE_REQUESTS_PAGINATION_PAGE_SIZE";
export const SET_USER_PROFILE_CHANGE_REQUESTS_SEARCH_PARAMETERS =
  "userProfileChangeRequests/SET_USER_PROFILE_CHANGE_REQUESTS_SEARCH_PARAMETERS";
export const RESET_STATE = "userProfileChangeRequests/RESET_STATE";

// update pagination page state (User Look-up)
const updateUserProfileChangeRequestsPageState = userProfileChangeRequestsPaginationPage => ({
  type: SET_USER_PROFILE_CHANGE_REQUESTS_PAGINATION_PAGE,
  userProfileChangeRequestsPaginationPage
});
// update pagination pageSize state (User Look-up)
const updateUserProfileChangeRequestsPageSizeState =
  userProfileChangeRequestsPaginationPageSize => ({
    type: SET_USER_PROFILE_CHANGE_REQUESTS_PAGINATION_PAGE_SIZE,
    userProfileChangeRequestsPaginationPageSize
  });
// update search keyword state (User Look-up)
const updateSearchUserProfileChangeRequestsStates = (
  userProfileChangeRequestsKeyword,
  userProfileChangeRequestsActiveSearch
) => ({
  type: SET_USER_PROFILE_CHANGE_REQUESTS_SEARCH_PARAMETERS,
  userProfileChangeRequestsKeyword,
  userProfileChangeRequestsActiveSearch
});
const resetUserProfileChangeRequestsState = () => ({ type: RESET_STATE });

const getAllUserProfileChangeRequests = (page, pageSize) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-user-profile-change-requests/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};

const getFoundUserProfileChangeRequests = (providedKeyword, page, pageSize) => {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-user-profile-change-requests/?keyword=${keyword}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};

function approveUserProfileChangeRequest(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/approve-user-profile-change-request/", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...data })
    });
    if (response.status === 200) {
      const responseObj = {
        ok: response.ok
      };
      return responseObj;
    }
    return response;
  };
}

function denyUserProfileChangeRequest(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/deny-user-profile-change-request/", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...data })
    });
    if (response.status === 200) {
      const responseObj = {
        ok: response.ok
      };
      return responseObj;
    }
    return response;
  };
}

// Initial State
const initialState = {
  userProfileChangeRequestsPaginationPage: 1,
  userProfileChangeRequestsPaginationPageSize: 25,
  userProfileChangeRequestsKeyword: null,
  userProfileChangeRequestsActiveSearch: false
};

const userProfileChangeRequests = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_PROFILE_CHANGE_REQUESTS_PAGINATION_PAGE:
      return {
        ...state,
        userProfileChangeRequestsPaginationPage: action.userProfileChangeRequestsPaginationPage
      };
    case SET_USER_PROFILE_CHANGE_REQUESTS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        userProfileChangeRequestsPaginationPageSize:
          action.userProfileChangeRequestsPaginationPageSize
      };
    case SET_USER_PROFILE_CHANGE_REQUESTS_SEARCH_PARAMETERS:
      return {
        ...state,
        userProfileChangeRequestsKeyword: action.userProfileChangeRequestsKeyword,
        userProfileChangeRequestsActiveSearch: action.userProfileChangeRequestsActiveSearch
      };
    case RESET_STATE:
      // Ensure local storage is cleaned up on logout
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default userProfileChangeRequests;
export {
  initialState,
  resetUserProfileChangeRequestsState,
  updateUserProfileChangeRequestsPageState,
  updateUserProfileChangeRequestsPageSizeState,
  updateSearchUserProfileChangeRequestsStates,
  getAllUserProfileChangeRequests,
  getFoundUserProfileChangeRequests,
  approveUserProfileChangeRequest,
  denyUserProfileChangeRequest
};
