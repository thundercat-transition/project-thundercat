import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// pass in strict api urls to update the test status
function updateCheckInRoom(testAccessCode) {
  const url = `/oec-cat/api/room-check-in/?test_access_code=${testAccessCode}`;

  return async function () {
    const tests = await fetch(url, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        cache: "default"
      }
    });
    // successful request
    if (tests.status === 200) {
      return tests;
      //  there is an error
    }
    const responseJson = await tests.json();
    return responseJson;
  };
}

export default updateCheckInRoom;
