import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

export const SET_CANDIDATE_SIDE_NAV_STATE = "candidate/SET_CANDIDATE_SIDE_NAV_STATE";
export const SET_RESERVE_TEST_SESSION_PHONE_NUMBER_STATE =
  "candidate/SET_RESERVE_TEST_SESSION_PHONE_NUMBER_STATE";
export const SET_RESERVE_TEST_SESSION_VALID_FORM_STATE =
  "candidate/SET_RESERVE_TEST_SESSION_VALID_FORM_STATE";

const setCandidateUserSideNavState = selectedSideNavItem => ({
  type: SET_CANDIDATE_SIDE_NAV_STATE,
  selectedSideNavItem
});
const setReserveTestSessionPhoneNumberState = reserveTestSessionPhoneNumberState => ({
  type: SET_RESERVE_TEST_SESSION_PHONE_NUMBER_STATE,
  reserveTestSessionPhoneNumberState
});
const setReserveTestSessionValidFormStateState = reserveTestSessionValidFormState => ({
  type: SET_RESERVE_TEST_SESSION_VALID_FORM_STATE,
  reserveTestSessionValidFormState
});
const RESET_STATE = "candidate/RESET_STATE";

// Initial State
const initialState = {
  selectedSideNavItem: 1,
  reserveTestSessionPhoneNumberState: "",
  reserveTestSessionValidFormState: false
};

// reset states
const resetCandidateStates = () => ({
  type: RESET_STATE
});

// find all sessions that are valid for the given reservation code
const verifyReservationCode = reservationCode => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/verify-reservation-code?reservation_code=${reservationCode}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
};

const reserveAndVerifyReservationCode = reservationCode => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/reserve-and-verify-reservation-code?reservation_code=${reservationCode}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
};

// find all sessions that are valid for the given reservation code
const findSessionsForReservationCode = (
  reservationCode,
  testSkillTypeCodename,
  testSkillSubTypeCodename
) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/find-sessions-for-reservation-code?reservation_code=${reservationCode}&test_skill_type_codename=${testSkillTypeCodename}&test_skill_sub_type_codename=${testSkillSubTypeCodename}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // if failed
    if (response.status !== 200) {
      return response;
    }
    // otherwise return success + status 200
    const responseJson = await response.json();
    responseJson.status = 200;
    return responseJson;
  };
};

// Common function to trigger any of the consume reservation API calls
const consumeReservationCode = (api, body) => {
  // test_session_id is first; reservation code is optional at the end
  return async function () {
    const response = await fetch(`/oec-cat/api/${api}?`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(body)
    });
    const { status } = response;
    // otherwise return success + status 200
    const responseJson = await response.json();
    responseJson.status = status;
    return responseJson;
  };
};

// book now action
const bookNowReservationCode = body => {
  return consumeReservationCode("book-reservation-code", body);
};

// withdraw from process action
const withdrawUnbookedReservationCode = body => {
  return consumeReservationCode("withdraw-reservation-code", body);
};

const withdrawBookedReservationCode = body => {
  return consumeReservationCode("withdraw-reservation-code", body);
};

// API to release the code to allow for later booking
const bookLater = (test_session_id, user_accommodation_file_id, is_ola) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/release-reservation-code?test_session_id=${test_session_id}&user_accommodation_file_id=${user_accommodation_file_id}&is_ola=${is_ola}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // get the response
    const { status } = response;
    // otherwise return success + status 200
    const responseJson = await response.json();
    responseJson.status = status;
    return responseJson;
  };
};

// API to edit the phone number linked to a test session
const editTestSessionPhoneNumber = (testSessionId, phoneNumber) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/edit-test-session-phone-number?test_session_id=${testSessionId}&phone_number=${phoneNumber}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    if (response.status !== 200) {
      const responseJson = await response.json();
      return responseJson;
    }
    return response;
  };
};

// API to edit the phone number linked to a user accommodation file
const editUserAccommodationFilePhoneNumber = (userAccommodationFileId, phoneNumber) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/edit-user-accommodation-file-phone-number?user_accommodation_file_id=${userAccommodationFileId}&phone_number=${phoneNumber}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    if (response.status !== 200) {
      const responseJson = await response.json();
      return responseJson;
    }
    return response;
  };
};

const candidate = (state = initialState, action) => {
  switch (action.type) {
    case SET_CANDIDATE_SIDE_NAV_STATE:
      return {
        ...state,
        selectedSideNavItem: action.selectedSideNavItem
      };
    case SET_RESERVE_TEST_SESSION_PHONE_NUMBER_STATE:
      return {
        ...state,
        reserveTestSessionPhoneNumberState: action.reserveTestSessionPhoneNumberState
      };
    case SET_RESERVE_TEST_SESSION_VALID_FORM_STATE:
      return {
        ...state,
        reserveTestSessionValidFormState: action.reserveTestSessionValidFormState
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

// // find all sessions that are valid for the given reservation code
const findBookedSessionsForReservationCode = () => {
  return async function () {
    const response = await fetch(`/oec-cat/api/find-booked-reservation-code`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    // if failed
    if (response.status !== 200) {
      return response;
    }
    // otherwise return success + status 200
    const responseJson = await response.json();
    responseJson.status = 200;
    return responseJson;
  };
};

// // find all requested accommodations that are valid for the given reservation code
const findRequestedAccommodationsForReservationCode = () => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/find-requested-accommodations-for-reservation-code`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // if failed
    if (response.status !== 200) {
      return response;
    }
    // otherwise return success + status 200
    const responseJson = await response.json();
    responseJson.status = 200;
    return responseJson;
  };
};

// find all booked later reservation codes for the given user
const findAllReservedCodesForUser = () => {
  return async function () {
    const response = await fetch(`/oec-cat/api/find-all-reserved-codes-for-user`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    // if failed
    if (response.status !== 200) {
      return response;
    }
    // otherwise return success + status 200
    const responseJson = await response.json();
    responseJson.status = 200;
    return responseJson;
  };
};

export default candidate;
export {
  initialState,
  setCandidateUserSideNavState,
  setReserveTestSessionPhoneNumberState,
  setReserveTestSessionValidFormStateState,
  resetCandidateStates,
  verifyReservationCode,
  reserveAndVerifyReservationCode,
  findSessionsForReservationCode,
  bookNowReservationCode,
  withdrawUnbookedReservationCode,
  withdrawBookedReservationCode,
  bookLater,
  editTestSessionPhoneNumber,
  editUserAccommodationFilePhoneNumber,
  findBookedSessionsForReservationCode,
  findAllReservedCodesForUser,
  findRequestedAccommodationsForReservationCode
};
