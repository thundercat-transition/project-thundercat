import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// getting active scored tests of the specified user
const getValidScoredTests = () => {
  return async function () {
    const tests = await fetch("/oec-cat/api/get-valid-scored-tests/", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const testsJson = await tests.json();
    return testsJson;
  };
};

// getting archived scored tests of the specified user
const getArchivedScoredTests = () => {
  return async function () {
    const tests = await fetch("/oec-cat/api/get-archived-scored-tests/", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const testsJson = await tests.json();
    return testsJson;
  };
};

// getting All tests of the specified user
const getAllTests = (userId, page, pageSize) => {
  return async function () {
    const tests = await fetch(
      `/oec-cat/api/get-all-tests-for-selected-user/?user_id=${userId}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const testsJson = await tests.json();
    return testsJson;
  };
};

// getting found tests of the specified user
const getFoundTests = (userId, providedKeyword, currentLanguage, page, pageSize) => {
  // if provided keyword is empty
  let keyword = encodeURIComponent(providedKeyword);
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }
  return async function () {
    const tests = await fetch(
      `/oec-cat/api/get-found-tests-for-selected-user/?user_id=${userId}&keyword=${keyword}&current_language=${currentLanguage}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const testsJson = await tests.json();
    return testsJson;
  };
};
export default getValidScoredTests;
export { getArchivedScoredTests, getAllTests, getFoundTests };
