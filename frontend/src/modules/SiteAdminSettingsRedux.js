import SessionStorage, { ACTION, ITEM } from "../SessionStorage";
import SiteAdminSettingType from "../components/superuser/Constants";

export const SET_ENABLE_QUALITY_OF_LIFE = "siteAdminSettings/SET_ENABLE_QUALITY_OF_LIFE";
export const RESET_STATE = "siteAdminSettings/RESET_STATE";

// set super user side nav state
const setQualityOfLife = qualityOfLife => ({
  type: SET_ENABLE_QUALITY_OF_LIFE,
  qualityOfLife
});

// Initial State
const initialState = {
  enableQualityOfLife: false
};

const siteAdminSettings = (state = initialState, action) => {
  switch (action.type) {
    case SET_ENABLE_QUALITY_OF_LIFE:
      return {
        ...state,
        qualityOfLife: action.qualityOfLife
      };
    case RESET_STATE:
      // Ensure local storage is cleaned up on logout
      return {
        ...initialState
      };
    default:
      return state;
  }
};

// get all site admin settings
const getSiteAdminSettings = () => {
  return async function () {
    const response = await fetch(`/oec-cat/api/get_site_admin_settings`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    // if failed
    if (response.status !== 200) {
      return response;
    }
    // otherwise
    const responseJson = await response.json();
    responseJson.status = 200;
    return responseJson;
  };
};

// get one specific site admin setting
const getSiteAdminSetting = codename => {
  return async function () {
    const response = await fetch(`/oec-cat/api/get_site_admin_setting?codename=${codename}`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    // if failed
    if (response.status !== 200) {
      return response;
    }
    // otherwise
    const responseJson = await response.json();
    responseJson.status = 200;
    return responseJson;
  };
};

const getLoginLock = () => {
  return getSiteAdminSetting(SiteAdminSettingType.LOCK_LOGIN);
};

const getEnableGCNotify = () => {
  return getSiteAdminSetting(SiteAdminSettingType.ENABLE_GC_NOTIFY);
};

const getEnableQualityofLife = () => {
  return async function () {
    const response = await fetch(`/oec-cat/api/get_enable_quality_of_life`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    // if failed
    if (response.status !== 200) {
      return response;
    }
    // otherwise
    const responseJson = await response.json();
    responseJson.status = 200;
    return responseJson;
  };
};

const getEnableWebex = () => {
  return getSiteAdminSetting(SiteAdminSettingType.ENABLE_WEBEX);
};

const getEnableMSTeamsAPI = () => {
  return getSiteAdminSetting(SiteAdminSettingType.ENABLE_MS_TEAMS_API);
};

// set one specific site admin setting
const setSiteAdminSetting = (codename, is_active, data) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/set_site_admin_setting?codename=${codename}&is_active=${is_active}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        },
        body: JSON.stringify(data)
      }
    );
    return response;
  };
};

const setLoginLock = is_active => {
  return setSiteAdminSetting(SiteAdminSettingType.LOCK_LOGIN, is_active, {});
};

const setQualityofLife = is_active => {
  return setSiteAdminSetting(SiteAdminSettingType.ENABLE_QUALITY_OF_LIFE, is_active, {});
};

const setEnableGCNotify = (is_active, key) => {
  const data = { key: key };
  return setSiteAdminSetting(SiteAdminSettingType.ENABLE_GC_NOTIFY, is_active, data);
};

const setEnableMSTeamsAPI = (is_active, key) => {
  const data = { key: key };
  return setSiteAdminSetting(SiteAdminSettingType.ENABLE_MS_TEAMS_API, is_active, data);
};

const setEnableWebex = (is_active, key) => {
  const data = { key: key };
  return setSiteAdminSetting(SiteAdminSettingType.ENABLE_WEBEX, is_active, data);
};

const setEnableMSTAppID = (is_active, key) => {
  const data = { key: key };
  return setSiteAdminSetting(SiteAdminSettingType.MS_TEAMS_API_APP_ID, is_active, data);
};

const setEnableMSTTenantID = (is_active, key) => {
  const data = { key: key };
  return setSiteAdminSetting(SiteAdminSettingType.MS_TEAMS_API_TENANT_ID, is_active, data);
};

const setEnableMSTSecret = (is_active, key) => {
  const data = { key: key };
  return setSiteAdminSetting(SiteAdminSettingType.MS_TEAMS_API_SECRET, is_active, data);
};

const addVirtualRoom = (name, room_user_id) => {
  const data = { name: name, room_user_id: room_user_id };
  return async function () {
    const response = await fetch(`/oec-cat/api/add-virtual-room`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });

    // if failed
    if (response.status !== 200) {
      return response;
    }
    // otherwise
    const responseJson = await response.json();
    responseJson.status = 200;
    return responseJson;
  };
};

const deleteVirtualRoom = name => {
  return async function () {
    const response = await fetch(`/oec-cat/api/delete-virtual-room?name=${name}`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    // if failed
    if (response.status !== 200) {
      return response;
    }
    // otherwise
    const responseJson = await response.json();
    responseJson.status = 200;
    return responseJson;
  };
};

const getTeamsRooms = (page, pageSize) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-teams-rooms?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );

    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
};

export default siteAdminSettings;

export {
  getSiteAdminSettings,
  getLoginLock,
  getEnableGCNotify,
  getEnableQualityofLife,
  getEnableWebex,
  getEnableMSTeamsAPI,
  setLoginLock,
  setQualityofLife,
  setEnableGCNotify,
  setEnableMSTeamsAPI,
  setEnableWebex,
  setEnableMSTAppID,
  setEnableMSTTenantID,
  setEnableMSTSecret,
  addVirtualRoom,
  deleteVirtualRoom,
  getTeamsRooms,
  setQualityOfLife
};
