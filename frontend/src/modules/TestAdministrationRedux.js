import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
export const SET_TEST_TO_ADMINISTER_SELECTED_VALUE =
  "testAdministration/SET_TEST_TO_ADMINISTER_SELECTED_VALUE";
export const SET_TEST_SESSION_OFFICERS_VALUE = "testAdministration/SET_TEST_SESSION_OFFICER_VALUE";
export const SET_GENERATE_BUTTON_DISABLED_STATE =
  "testAdministration/SET_GENERATE_BUTTON_DISABLED_STATE";
export const SET_TA_SIDE_NAV_STATE = "testAdministration/SET_TA_SIDE_NAV_STATE";
const RESET_STATE = "testAdministration/RESET_STATE";

// update test to administer state
const updateTestToAdministerState = testToAdminister => ({
  type: SET_TEST_TO_ADMINISTER_SELECTED_VALUE,
  testToAdminister
});

// update test session officer state
const updateTestSessionOfficersState = testSessionOfficers => ({
  type: SET_TEST_SESSION_OFFICERS_VALUE,
  testSessionOfficers
});

// update generate button disabled state
const updateGenerateButtonDisabledState = generateButtonDisabled => ({
  type: SET_GENERATE_BUTTON_DISABLED_STATE,
  generateButtonDisabled
});

// set TA User side nav state
const setTAUserSideNavState = selectedSideNavItem => ({
  type: SET_TA_SIDE_NAV_STATE,
  selectedSideNavItem
});

// reset states
const resetTestAdministrationStates = () => ({
  type: RESET_STATE
});

// gets randomly generated test access code
function getNewTestAccessCode(body) {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-new-test-access-code/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    return response;
  };
}

// delete test access code
function deleteTestAccessCode(testAccessCode) {
  const body = {
    test_access_code: testAccessCode
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/delete-test-access-code/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// get active test access code data for a specified TA username
function getActiveTestAccessCodesRedux(testSessionId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-active-test-access-codes/?test_session_id=${testSessionId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get assigned candidates for a specified TA username
function getTestAdministratorAssignedCandidates(testSessionId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-test-administrator-assigned-candidates/?test_session_id=${testSessionId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// updating test time (useful for TAs to allow them to customize test time for a specific candidate)
const updateTestTime = (
  assignedTestSectionId,
  testTime,
  firstIteration,
  lastIteration,
  testSessionId
) => {
  const body = {
    assigned_test_section_id: assignedTestSectionId,
    test_time: testTime
  };
  return async function () {
    const response = await fetch(
      `/oec-cat/api/update-test-time/?assigned_test_section_id=${assignedTestSectionId}&test_time=${testTime}&first_iteration=${firstIteration}&last_iteration=${lastIteration}&test_session_id=${testSessionId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        },
        body: JSON.stringify({ ...body })
      }
    );
    return response;
  };
};

// getting default test sections time
function getDefaultTestSectionsTime(testId) {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-default-test-sections-time/?test_id=${testId}`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// updating break bank (useful for TAs to allow them to add break bank for a specific candidate)
const updateBreakBank = (assignedTestId, breakBankTime, testSessionId) => {
  const body = {
    assigned_test__id: assignedTestId,
    break_bank_time: breakBankTime
  };
  return async function () {
    const response = await fetch(
      `/oec-cat/api/update-break-bank/?assigned_test_id=${assignedTestId}&break_bank_time=${breakBankTime}&test_session_id=${testSessionId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        },
        body: JSON.stringify({ ...body })
      }
    );
    return response;
  };
};

// approving candidate
const approveCandidate = (candidate_user_id, test, testSessionId) => {
  const body = {
    candidate_user_id: candidate_user_id,
    test_id: test,
    test_session_id: testSessionId
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/approve-candidate/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    const responseJson = await response.json();
    return responseJson;
  };
};

// approving all candidates
const approveAllCandidates = assignedTestIds => {
  const body = {
    assigned_test_ids: assignedTestIds
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/approve-all-candidates/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    const responseJson = await response.json();
    return responseJson;
  };
};

// locking single candidate's test
const lockCandidateTest = (candidate_user_id, test, testSectionId, testSessionId) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/lock-candidate-test/?candidate_user_id=${candidate_user_id}&test_id=${test}&test_section_id=${testSectionId}&test_session_id=${testSessionId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};

// locking all candidates test
const lockAllCandidatesTest = testSessionId => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/lock-all-candidates-test/?test_session_id=${testSessionId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};

// unlocking single candidate's test
const unlockCandidateTest = (candidate_user_id, test, testSectionId, testSessionId) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/unlock-candidate-test/?candidate_user_id=${candidate_user_id}&test_id=${test}&test_section_id=${testSectionId}&test_session_id=${testSessionId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};

// unlocking all candidates test
const unlockAllCandidatesTest = testSessionId => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/unlock-all-candidates-test/?test_session_id=${testSessionId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};

// un-assign candidate's test
const unAssignCandidate = (candidate_user_id, test, testSessionId) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/un-assign-candidate/?candidate_user_id=${candidate_user_id}&test_id=${test}&test_session_id=${testSessionId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};

// Initial State
const initialState = {
  testToAdminister: [],
  testSessionOfficers: [],
  generateButtonDisabled: true,
  selectedSideNavItem: 1
};

const testAdministration = (state = initialState, action) => {
  switch (action.type) {
    case SET_TEST_TO_ADMINISTER_SELECTED_VALUE:
      return {
        ...state,
        testToAdminister: action.testToAdminister
      };
    case SET_TEST_SESSION_OFFICERS_VALUE:
      return {
        ...state,
        testSessionOfficers: action.testSessionOfficers
      };
    case SET_GENERATE_BUTTON_DISABLED_STATE:
      return {
        ...state,
        generateButtonDisabled: action.generateButtonDisabled
      };
    case SET_TA_SIDE_NAV_STATE:
      return {
        ...state,
        selectedSideNavItem: action.selectedSideNavItem
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default testAdministration;
export {
  initialState,
  getNewTestAccessCode,
  updateTestToAdministerState,
  updateTestSessionOfficersState,
  updateGenerateButtonDisabledState,
  resetTestAdministrationStates,
  deleteTestAccessCode,
  getActiveTestAccessCodesRedux,
  getTestAdministratorAssignedCandidates,
  updateTestTime,
  getDefaultTestSectionsTime,
  approveCandidate,
  approveAllCandidates,
  lockCandidateTest,
  lockAllCandidatesTest,
  unlockCandidateTest,
  unlockAllCandidatesTest,
  unAssignCandidate,
  updateBreakBank,
  setTAUserSideNavState
};
