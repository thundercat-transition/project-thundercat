import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
const SET_ALTERNATE_TEST_REQUESTS_SELECTED_TOP_TAB_STATE =
  "alternateTestRequests/SET_ALTERNATE_TEST_REQUESTS_SELECTED_TOP_TAB_STATE";
export const SET_PENDING_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE =
  "alternateTestRequests/SET_PENDING_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE";
export const SET_PENDING_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE_SIZE =
  "alternateTestRequests/SET_PENDING_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE_SIZE";
export const SET_PENDING_ALTERNATE_TEST_REQUESTS_SEARCH_PARAMETERS =
  "alternateTestRequests/SET_PENDING_ALTERNATE_TEST_REQUESTS_SEARCH_PARAMETERS";
export const SET_COMPLETED_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE =
  "alternateTestRequests/SET_COMPLETED_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE";
export const SET_COMPLETED_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE_SIZE =
  "alternateTestRequests/SET_COMPLETED_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE_SIZE";
export const SET_COMPLETED_ALTERNATE_TEST_REQUESTS_SEARCH_PARAMETERS =
  "alternateTestRequests/SET_COMPLETED_ALTERNATE_TEST_REQUESTS_SEARCH_PARAMETERS";

// Action Creators
const setAlternateTestRequestsSelectedTopTabState = alternateTestRequestsSelectedTopTab => ({
  type: SET_ALTERNATE_TEST_REQUESTS_SELECTED_TOP_TAB_STATE,
  alternateTestRequestsSelectedTopTab
});

const updatePendingAlternateTestRequestsPageState = pendingAlternateTestRequestsPage => ({
  type: SET_PENDING_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE,
  pendingAlternateTestRequestsPage
});

const updatePendingAlternateTestRequestsPageSizeState = pendingAlternateTestRequestsPageSize => ({
  type: SET_PENDING_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE_SIZE,
  pendingAlternateTestRequestsPageSize
});

const updatePendingAlternateTestRequestsSearchParametersStates = (
  pendingAlternateTestRequestsKeyword,
  pendingAlternateTestRequestsActiveSearch
) => ({
  type: SET_PENDING_ALTERNATE_TEST_REQUESTS_SEARCH_PARAMETERS,
  pendingAlternateTestRequestsKeyword,
  pendingAlternateTestRequestsActiveSearch
});

const updateCompletedAlternateTestRequestsPageState = completedAlternateTestRequestsPage => ({
  type: SET_COMPLETED_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE,
  completedAlternateTestRequestsPage
});

const updateCompletedAlternateTestRequestsPageSizeState =
  completedAlternateTestRequestsPageSize => ({
    type: SET_COMPLETED_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE_SIZE,
    completedAlternateTestRequestsPageSize
  });

const updateCompletedAlternateTestRequestsSearchParametersStates = (
  completedAlternateTestRequestsKeyword,
  completedAlternateTestRequestsActiveSearch
) => ({
  type: SET_COMPLETED_ALTERNATE_TEST_REQUESTS_SEARCH_PARAMETERS,
  completedAlternateTestRequestsKeyword,
  completedAlternateTestRequestsActiveSearch
});

// getting all pending alternate test requests
function getAllPendingAlternateTestRequests(page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-pending-alternate-test-requests/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
}

// getting found pending alternate test requests
function getFoundPendingAlternateTestRequests(providedKeyword, currentLanguage, page, pageSize) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-pending-alternate-test-requests/?keyword=${keyword}&current_language=${currentLanguage}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
}

// submitting alternate test request
function submitPendingAlternateTestRequest(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/submit-pending-alternate-test-request", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
}

// cancelling alternate test request
function cancelPendingAlternateTestRequest(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/cancel-pending-alternate-test-request", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
}

// getting all completed alternate test requests
function getAllCompletedAlternateTestRequests(page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-completed-alternate-test-requests/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
}

// getting found completed alternate test requests
function getFoundCompletedAlternateTestRequests(providedKeyword, currentLanguage, page, pageSize) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-completed-alternate-test-requests/?keyword=${keyword}&current_language=${currentLanguage}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
}

// Initial State
const initialState = {
  alternateTestRequestsSelectedTopTab: "",
  pendingAlternateTestRequestsPage: 1,
  pendingAlternateTestRequestsPageSize: 25,
  pendingAlternateTestRequestsKeyword: "",
  pendingAlternateTestRequestsActiveSearch: false,
  completedAlternateTestRequestsPage: 1,
  completedAlternateTestRequestsPageSize: 25,
  completedAlternateTestRequestsKeyword: "",
  completedAlternateTestRequestsActiveSearch: false
};

// Reducer
const alternateTestRequests = (state = initialState, action) => {
  switch (action.type) {
    case SET_ALTERNATE_TEST_REQUESTS_SELECTED_TOP_TAB_STATE:
      return {
        ...state,
        alternateTestRequestsSelectedTopTab: action.alternateTestRequestsSelectedTopTab
      };
    case SET_PENDING_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE:
      return {
        ...state,
        pendingAlternateTestRequestsPage: action.pendingAlternateTestRequestsPage
      };
    case SET_PENDING_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        pendingAlternateTestRequestsPageSize: action.pendingAlternateTestRequestsPageSize
      };
    case SET_PENDING_ALTERNATE_TEST_REQUESTS_SEARCH_PARAMETERS:
      return {
        ...state,
        pendingAlternateTestRequestsKeyword: action.pendingAlternateTestRequestsKeyword,
        pendingAlternateTestRequestsActiveSearch: action.pendingAlternateTestRequestsActiveSearch
      };
    case SET_COMPLETED_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE:
      return {
        ...state,
        completedAlternateTestRequestsPage: action.completedAlternateTestRequestsPage
      };
    case SET_COMPLETED_ALTERNATE_TEST_REQUESTS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        completedAlternateTestRequestsPageSize: action.completedAlternateTestRequestsPageSize
      };
    case SET_COMPLETED_ALTERNATE_TEST_REQUESTS_SEARCH_PARAMETERS:
      return {
        ...state,
        completedAlternateTestRequestsKeyword: action.completedAlternateTestRequestsKeyword,
        completedAlternateTestRequestsActiveSearch:
          action.completedAlternateTestRequestsActiveSearch
      };
    default:
      return state;
  }
};

export default alternateTestRequests;
export {
  setAlternateTestRequestsSelectedTopTabState,
  updatePendingAlternateTestRequestsPageState,
  updatePendingAlternateTestRequestsPageSizeState,
  updatePendingAlternateTestRequestsSearchParametersStates,
  updateCompletedAlternateTestRequestsPageState,
  updateCompletedAlternateTestRequestsPageSizeState,
  updateCompletedAlternateTestRequestsSearchParametersStates,
  getAllPendingAlternateTestRequests,
  getFoundPendingAlternateTestRequests,
  submitPendingAlternateTestRequest,
  cancelPendingAlternateTestRequest,
  getAllCompletedAlternateTestRequests,
  getFoundCompletedAlternateTestRequests
};
