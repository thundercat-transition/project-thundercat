import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
const READ_QUESTION = "questionList/READ_QUESTION";
const MARK_QUESTION = "questionList/MARK_QUESTION";
const LOAD_ANSWERS = "questionList/LOAD_ANSWERS";
const ANSWER_QUESTION = "questionList/ANSWER_QUESTION";
const SET_START_TIME = "questionList/SET_START_TIME";
const SEEN_QUESTION_LOADED = "questionList/SEEN_QUESTION_LOADED";
const SET_PREVIOUS_QUESTION = "questionList/SET_PREVIOUS_QUESTION";
const RESET_STATE = "questionList/RESET_STATE";

const CHANGE_CURRENT_QUESTION = "questionList/CHANGE_CURRENT_QUESTION";

// Action Creators

const loadAnswers = answers => ({ type: LOAD_ANSWERS, answers });
const readQuestion = questionId => ({ type: READ_QUESTION, questionId });
const answerQuestion = (questionId, answerId) => ({ type: ANSWER_QUESTION, questionId, answerId });

const setPreviousQuestionId = questionId => ({ type: SET_PREVIOUS_QUESTION, questionId });
const setSeenQuestionLoaded = () => ({ type: SEEN_QUESTION_LOADED });
// emailIndex refers to the index of the currently visible email
const changeCurrentQuestion = questionId => ({
  type: CHANGE_CURRENT_QUESTION,
  questionId
});

// setting start time of current question
const setStartTime = startTime => ({
  type: SET_START_TIME,
  startTime
});

const markForReview = questionId => ({
  type: MARK_QUESTION,
  questionId
});
const resetQuestionListState = () => ({
  type: RESET_STATE
});

function markForReviewDB(assigned_test_id, question_id, test_section_component_id) {
  const url = `/oec-cat/api/mark-for-review?assigned_test_id=${assigned_test_id}&question_id=${question_id}&test_section_component_id=${test_section_component_id}`;

  return async function () {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        cache: "default"
      }
    });
    return response;
  };
}

function updateMultipleChoiceAnswersModifyDateDB(
  assigned_test_id,
  question_id,
  test_section_component_id
) {
  const url = `/oec-cat/api/update-multiple-choice-answers-modify-date?assigned_test_id=${assigned_test_id}&question_id=${question_id}&test_section_component_id=${test_section_component_id}`;

  return async function () {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        cache: "default"
      }
    });
    return response;
  };
}

// Initial State
// emails - represents an array of emailShape objects in the currently selected language.
// emailSummaries - represents an array of objects indicating read state of each email.
// emailActions - represents an array of arrays, each array contains actionShape objects, representing an ACTION_TYPE.
const initialState = {
  assignedQuestionIds: [],
  isTestLoadedFromDB: false,
  isContentLoaded: false,
  // new vars
  answers: [],
  readQuestions: [],
  questionSummaries: {},
  startTime: null,
  seenQuestionLoaded: false,
  previousQuestionId: null,
  currentQuestion: null,
  initialSelectedQuestion: null
};

// Reducer
const questionList = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_ANSWERS:
      const newQuestionSummaries = {};
      const { answers } = action;
      let currentQuestion = null;
      let initialSelectedQuestion = null;
      for (let i = 0; i < answers.length; i++) {
        newQuestionSummaries[answers[i].question_id] = {
          ...state.questionSummaries[answers[i].question_id],
          isRead: true,
          answer: answers[i].answer_id,
          review: answers[i].mark_for_review
        };
        // if first iteration
        if (i === 0) {
          // updating currentQuestion and initialSelectedQuestion states
          currentQuestion = answers[i].question_id;
          initialSelectedQuestion = answers[i].question_id;
        }
      }
      // preventing empty questionSummaries state
      if (answers.length > 0) {
        return {
          ...state,
          questionSummaries: newQuestionSummaries,
          currentQuestion: currentQuestion,
          initialSelectedQuestion: initialSelectedQuestion
        };
      }
      return {
        ...state
      };

    case READ_QUESTION:
      const readQuestionId = Number(action.questionId);
      const newReadQuestionSummaries = state.questionSummaries || {};
      const readQuestion = {
        ...newReadQuestionSummaries[readQuestionId],
        isRead: true
      };
      newReadQuestionSummaries[readQuestionId] = { ...readQuestion };
      return {
        ...state,
        questionSummaries: newReadQuestionSummaries
      };
    case MARK_QUESTION:
      const markQuestionId = Number(action.questionId);
      const newMarkQuestionSummaries = state.questionSummaries || {};
      const markQuestion = {
        ...newMarkQuestionSummaries[markQuestionId],
        review: newMarkQuestionSummaries[markQuestionId].review !== true
      };

      newMarkQuestionSummaries[markQuestionId] = { ...markQuestion };
      return {
        ...state,
        questionSummaries: newMarkQuestionSummaries
      };
    case ANSWER_QUESTION:
      const answerQuestionId = Number(action.questionId);
      const answerId = Number(action.answerId);

      const newAnswerQuestionSummaries = state.questionSummaries || {};

      const answerQuestion = {
        ...newAnswerQuestionSummaries[answerQuestionId],
        answer: answerId
      };
      newAnswerQuestionSummaries[answerQuestionId] = { ...answerQuestion };
      return {
        ...state,
        questionSummaries: newAnswerQuestionSummaries
      };
    case CHANGE_CURRENT_QUESTION:
      return {
        ...state,
        currentQuestion: action.questionId
      };
    case SET_START_TIME:
      return {
        ...state,
        startTime: action.startTime
      };
    case SET_PREVIOUS_QUESTION:
      return {
        ...state,
        previousQuestionId: action.questionId
      };
    case SEEN_QUESTION_LOADED:
      return {
        ...state,
        seenQuestionLoaded: true
      };
    case RESET_STATE:
      return {
        ...initialState
      };

    default:
      return state;
  }
};

export default questionList;
export {
  initialState,
  readQuestion,
  loadAnswers,
  answerQuestion,
  changeCurrentQuestion,
  markForReview,
  markForReviewDB,
  updateMultipleChoiceAnswersModifyDateDB,
  setStartTime,
  setPreviousQuestionId,
  setSeenQuestionLoaded,
  resetQuestionListState
};
