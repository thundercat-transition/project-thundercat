import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

export const SET_SUPER_USER_SIDE_NAV_STATE = "superUser/SET_SUPER_USER_SIDE_NAV_STATE";
export const RESET_STATE = "superUser/RESET_STATE";

// set super user side nav state
const setSuperUserSideNavState = selectedSideNavItem => ({
  type: SET_SUPER_USER_SIDE_NAV_STATE,
  selectedSideNavItem
});

// Initial State
const initialState = {
  selectedSideNavItem: 1
};

const superUser = (state = initialState, action) => {
  switch (action.type) {
    case SET_SUPER_USER_SIDE_NAV_STATE:
      return {
        ...state,
        selectedSideNavItem: action.selectedSideNavItem
      };
    case RESET_STATE:
      // Ensure local storage is cleaned up on logout
      return {
        ...initialState
      };
    default:
      return state;
  }
};

function resetCompletedProfileFlag() {
  return async function () {
    const response = await fetch(`/oec-cat/api/celery/reset_completed_profile_flag`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
  };
}

function handleExpiredUitProcesses() {
  return async function () {
    const response = await fetch(`/oec-cat/api/celery/handle_expired_uit_processes`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
  };
}

function removeExpiredTestPermissions() {
  return async function () {
    const response = await fetch(`/oec-cat/api/celery/remove_expired_test_permissions`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
  };
}

function updateNonSubmittedActiveAssignedTests() {
  return async function () {
    const response = await fetch(`/oec-cat/api/celery/update_non_submitted_active_assigned_tests`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
  };
}

function invalidateLockedAssignedTests() {
  return async function () {
    const response = await fetch(`/oec-cat/api/celery/invalidate_locked_assigned_tests`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
  };
}

function unassignReadyAndPreTestAssignedTests() {
  return async function () {
    const response = await fetch(`/oec-cat/api/celery/unassign_ready_and_pre_test_assigned_tests`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
  };
}

function unpauseAssignedTests() {
  return async function () {
    const response = await fetch(`/oec-cat/api/celery/unpause_assigned_tests`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
  };
}

function expireOldAccommodations() {
  return async function () {
    const response = await fetch(`/oec-cat/api/celery/expire_old_accommodations`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
  };
}

function deprovisionAfterInactivity() {
  return async function () {
    const response = await fetch(`/oec-cat/api/celery/deprovision_after_inactivity`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
  };
}

function deprovisionAdminAfterInactivityThirtyDays() {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/celery/deprovision_admin_after_inactivity_thirty_days`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
  };
}

function removeExpiredTestScorerPermissions() {
  return async function () {
    const response = await fetch(`/oec-cat/api/celery/remove_expired_test_scorer_permissions`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
  };
}

export default superUser;

export {
  resetCompletedProfileFlag,
  handleExpiredUitProcesses,
  removeExpiredTestPermissions,
  updateNonSubmittedActiveAssignedTests,
  invalidateLockedAssignedTests,
  unassignReadyAndPreTestAssignedTests,
  unpauseAssignedTests,
  expireOldAccommodations,
  deprovisionAfterInactivity,
  deprovisionAdminAfterInactivityThirtyDays,
  setSuperUserSideNavState,
  removeExpiredTestScorerPermissions
};
