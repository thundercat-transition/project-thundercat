import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action types
export const SET_TEST_CENTERS_PAGINATION_PAGE = "testCenter/SET_TEST_CENTERS_PAGINATION_PAGE";
export const SET_TEST_CENTERS_PAGINATION_PAGE_SIZE =
  "testCenter/SET_TEST_CENTERS_PAGINATION_PAGE_SIZE";
export const SET_TEST_CENTERS_SEARCH_PARAMETERS = "testCenter/SET_TEST_CENTERS_SEARCH_PARAMETERS";
export const SET_ETTA_TEST_CENTER_ADMINISTRATION_PAGINATION_PAGE =
  "testCenter/SET_ETTA_TEST_CENTER_ADMINISTRATION_PAGINATION_PAGE";
export const SET_ETTA_TEST_CENTER_ADMINISTRATION_PAGINATION_PAGE_SIZE =
  "testCenter/SET_ETTA_TEST_CENTER_ADMINISTRATION_PAGINATION_PAGE_SIZE";
export const SET_ETTA_TEST_CENTER_ADMINISTRATION_SEARCH_PARAMETERS =
  "testCenter/SET_ETTA_TEST_CENTER_ADMINISTRATION_SEARCH_PARAMETERS";
export const SET_TEST_CENTER_ASSOCIATED_TEST_ADMINISTRATORS_PAGINATION_PAGE =
  "testCenter/SET_TEST_CENTER_ASSOCIATED_TEST_ADMINISTRATORS_PAGINATION_PAGE";
export const SET_TEST_CENTER_ASSOCIATED_TEST_ADMINISTRATORS_PAGINATION_PAGE_SIZE =
  "testCenter/SET_TEST_CENTER_ASSOCIATED_TEST_ADMINISTRATORS_PAGINATION_PAGE_SIZE";
export const SET_TEST_CENTER_ASSOCIATED_TEST_ADMINISTRATORS_SEARCH_PARAMETERS =
  "testCenter/SET_TEST_CENTER_ASSOCIATED_TEST_ADMINISTRATORS_SEARCH_PARAMETERS";
export const SET_TEST_CENTER_ROOMS_PAGINATION_PAGE =
  "testCenter/SET_TEST_CENTER_ROOMS_PAGINATION_PAGE";
export const SET_TEST_CENTER_ROOMS_PAGINATION_PAGE_SIZE =
  "testCenter/SET_TEST_CENTER_ROOMS_PAGINATION_PAGE_SIZE";
export const SET_TEST_CENTER_ROOMS_SEARCH_PARAMETERS =
  "testCenter/SET_TEST_CENTER_ROOMS_SEARCH_PARAMETERS";
export const SET_TEST_CENTER_TEST_SESSIONS_SELECTED_TAB =
  "testCenter/SET_TEST_CENTER_TEST_SESSIONS_SELECTED_TAB";
export const SET_TEST_CENTER_TEST_SESSIONS_PAGINATION_PAGE =
  "testCenter/SET_TEST_CENTER_TEST_SESSIONS_PAGINATION_PAGE";
export const SET_TEST_CENTER_TEST_SESSIONS_PAGINATION_PAGE_SIZE =
  "testCenter/SET_TEST_CENTER_TEST_SESSIONS_PAGINATION_PAGE_SIZE";
export const SET_TEST_CENTER_TEST_SESSIONS_SEARCH_PARAMETERS =
  "testCenter/SET_TEST_CENTER_TEST_SESSIONS_SEARCH_PARAMETERS";
export const SET_TEST_CENTER_NON_STANDARD_TEST_SESSION_DATA =
  "testCenter/SET_TEST_CENTER_NON_STANDARD_TEST_SESSION_DATA";
export const SET_TEST_CENTER_NON_STANDARD_TEST_SESSION_VALID_FORM_STATE =
  "testCenter/SET_TEST_CENTER_NON_STANDARD_TEST_SESSION_VALID_FORM_STATE";
export const SET_TEST_CENTER_ACCOMMODATION_REQUESTS_PAGINATION_PAGE =
  "testCenter/SET_TEST_CENTER_ACCOMMODATION_REQUESTS_PAGINATION_PAGE";
export const SET_TEST_CENTER_ACCOMMODATION_REQUESTS_PAGINATION_PAGE_SIZE =
  "testCenter/SET_TEST_CENTER_ACCOMMODATION_REQUESTS_PAGINATION_PAGE_SIZE";
export const SET_TEST_CENTER_ACCOMMODATION_REQUESTS_SEARCH_PARAMETERS =
  "testCenter/SET_TEST_CENTER_ACCOMMODATION_REQUESTS_SEARCH_PARAMETERS";
export const SET_TEST_CENTER_INFO_CONTAINS_CHANGES =
  "testCenter/SET_TEST_CENTER_INFO_CONTAINS_CHANGES";
const TRIGGER_TABLE_UPDATES = "testCenter/TRIGGER_TABLE_UPDATES";
const TRIGGER_RERENDER = "testCenter/TRIGGER_RERENDER";
const RESET_SELECTED_TEST_CENTER_ETTA_STATES = "testCenter/RESET_SELECTED_TEST_CENTER_ETTA_STATES";
const SET_ETTA_TEST_CENTER_DATA = "testCenter/SET_ETTA_TEST_CENTER_DATA";
const SET_TEST_CENTER_DATA = "testCenter/SET_TEST_CENTER_DATA";
const SET_TEST_CENTER_DATA_SELECTED_SIDE_NAV_STATE =
  "testCenter/SET_TEST_CENTER_DATA_SELECTED_SIDE_NAV_STATE";
const SET_TEST_CENTER_MANAGEMENT_SELECTED_TOP_TAB_STATE =
  "testCenter/SET_TEST_CENTER_MANAGEMENT_SELECTED_TOP_TAB_STATE";
const SET_TEST_CENTER_OLA_SELECTED_TOP_TAB_STATE =
  "testCenter/SET_TEST_CENTER_OLA_SELECTED_TOP_TAB_STATE";
const RESET_SELECTED_TEST_CENTER_STATES = "testCenter/RESET_SELECTED_TEST_CENTER_STATES";
const RESET_STATE = "testCenter/RESET_STATE";

const updateCurrentTestCentersPageState = testCentersPaginationPage => ({
  type: SET_TEST_CENTERS_PAGINATION_PAGE,
  testCentersPaginationPage
});

const updateTestCentersPageSizeState = testCentersPaginationPageSize => ({
  type: SET_TEST_CENTERS_PAGINATION_PAGE_SIZE,
  testCentersPaginationPageSize
});

const updateSearchTestCentersStates = (testCentersKeyword, testCentersActiveSearch) => ({
  type: SET_TEST_CENTERS_SEARCH_PARAMETERS,
  testCentersKeyword,
  testCentersActiveSearch
});

const updateCurrentEttaTestCenterAdministrationPageState =
  ettaTestCenterAdministrationPaginationPage => ({
    type: SET_ETTA_TEST_CENTER_ADMINISTRATION_PAGINATION_PAGE,
    ettaTestCenterAdministrationPaginationPage
  });

const updateEttaTestCenterAdministrationPageSizeState =
  ettaTestCenterAdministrationPaginationPageSize => ({
    type: SET_ETTA_TEST_CENTER_ADMINISTRATION_PAGINATION_PAGE_SIZE,
    ettaTestCenterAdministrationPaginationPageSize
  });

const updateSearchEttaTestCenterAdministrationStates = (
  ettaTestCenterAdministrationKeyword,
  ettaTestCenterAdministrationActiveSearch
) => ({
  type: SET_ETTA_TEST_CENTER_ADMINISTRATION_SEARCH_PARAMETERS,
  ettaTestCenterAdministrationKeyword,
  ettaTestCenterAdministrationActiveSearch
});

const updateCurrentTestCenterTestAdministratorsPageState =
  testCenterTestAdministratorsPaginationPage => ({
    type: SET_TEST_CENTER_ASSOCIATED_TEST_ADMINISTRATORS_PAGINATION_PAGE,
    testCenterTestAdministratorsPaginationPage
  });

const updateTestCenterTestAdministratorsPageSizeState =
  testCenterTestAdministratorsPaginationPageSize => ({
    type: SET_TEST_CENTER_ASSOCIATED_TEST_ADMINISTRATORS_PAGINATION_PAGE_SIZE,
    testCenterTestAdministratorsPaginationPageSize
  });

const updateSearchTestCenterTestAdministratorsStates = (
  testCenterTestAdministratorsKeyword,
  testCenterTestAdministratorsActiveSearch
) => ({
  type: SET_TEST_CENTER_ASSOCIATED_TEST_ADMINISTRATORS_SEARCH_PARAMETERS,
  testCenterTestAdministratorsKeyword,
  testCenterTestAdministratorsActiveSearch
});

const updateCurrentTestCenterRoomsPageState = testCenterRoomsPaginationPage => ({
  type: SET_TEST_CENTER_ROOMS_PAGINATION_PAGE,
  testCenterRoomsPaginationPage
});

const updateTestCenterRoomsPageSizeState = testCenterRoomsPaginationPageSize => ({
  type: SET_TEST_CENTER_ROOMS_PAGINATION_PAGE_SIZE,
  testCenterRoomsPaginationPageSize
});

const updateSearchTestCenterRoomsStates = (
  testCenterRoomsKeyword,
  testCenterRoomsActiveSearch
) => ({
  type: SET_TEST_CENTER_ROOMS_SEARCH_PARAMETERS,
  testCenterRoomsKeyword,
  testCenterRoomsActiveSearch
});

const setTestCenterTestSessionsSelectedTab = testCenterTestSessionsSelectedTab => ({
  type: SET_TEST_CENTER_TEST_SESSIONS_SELECTED_TAB,
  testCenterTestSessionsSelectedTab
});

const updateCurrentTestCenterTestSessionsPageState = testCenterTestSessionsPaginationPage => ({
  type: SET_TEST_CENTER_TEST_SESSIONS_PAGINATION_PAGE,
  testCenterTestSessionsPaginationPage
});

const updateTestCenterTestSessionsPageSizeState = testCenterTestSessionsPaginationPageSize => ({
  type: SET_TEST_CENTER_TEST_SESSIONS_PAGINATION_PAGE_SIZE,
  testCenterTestSessionsPaginationPageSize
});

const updateSearchTestCenterTestSessionsStates = (
  testCenterTestSessionsKeyword,
  testCenterTestSessionsActiveSearch
) => ({
  type: SET_TEST_CENTER_TEST_SESSIONS_SEARCH_PARAMETERS,
  testCenterTestSessionsKeyword,
  testCenterTestSessionsActiveSearch
});

const updateTestSessionNonStandardTestSessionData = testCenterNonStandardTestSessionData => ({
  type: SET_TEST_CENTER_NON_STANDARD_TEST_SESSION_DATA,
  testCenterNonStandardTestSessionData
});

const updateTestSessionNonStandardTestSessionValidFormState =
  testCenterNonStandardTestSessionValidFormState => ({
    type: SET_TEST_CENTER_NON_STANDARD_TEST_SESSION_VALID_FORM_STATE,
    testCenterNonStandardTestSessionValidFormState
  });

const updateCurrentTestCenterAccommodationRequestsPageState =
  testCenterAccommodationRequestsPaginationPage => ({
    type: SET_TEST_CENTER_ACCOMMODATION_REQUESTS_PAGINATION_PAGE,
    testCenterAccommodationRequestsPaginationPage
  });

const updateTestCenterAccommodationRequestsPageSizeState =
  testCenterAccommodationRequestsPaginationPageSize => ({
    type: SET_TEST_CENTER_ACCOMMODATION_REQUESTS_PAGINATION_PAGE_SIZE,
    testCenterAccommodationRequestsPaginationPageSize
  });

const updateSearchTestCenterAccommodationRequestsStates = (
  testCenterAccommodationRequestsKeyword,
  testCenterAccommodationRequestsActiveSearch
) => ({
  type: SET_TEST_CENTER_ACCOMMODATION_REQUESTS_SEARCH_PARAMETERS,
  testCenterAccommodationRequestsKeyword,
  testCenterAccommodationRequestsActiveSearch
});

const updateTestCenterInfoContainsChangesState = testCenterInfoContainsChanges => ({
  type: SET_TEST_CENTER_INFO_CONTAINS_CHANGES,
  testCenterInfoContainsChanges
});

const triggerTableUpdates = () => ({
  type: TRIGGER_TABLE_UPDATES
});

const triggerRerender = () => ({
  type: TRIGGER_RERENDER
});

const resetEttaSelectedTestCenterStates = () => ({
  type: RESET_SELECTED_TEST_CENTER_ETTA_STATES
});

const setEttaTestCenterData = testCenterDataEtta => ({
  type: SET_ETTA_TEST_CENTER_DATA,
  testCenterDataEtta
});

const resetSelectedTestCenterStates = () => ({
  type: RESET_SELECTED_TEST_CENTER_STATES
});

const setTestCenterData = testCenterData => ({
  type: SET_TEST_CENTER_DATA,
  testCenterData
});

const setTestCenterDataSideNavState = testCenterDataSelectedSideNavItem => ({
  type: SET_TEST_CENTER_DATA_SELECTED_SIDE_NAV_STATE,
  testCenterDataSelectedSideNavItem
});

const setTestCenterManagementSelectedTopTabState = testCenterManagementSelectedTopTab => ({
  type: SET_TEST_CENTER_MANAGEMENT_SELECTED_TOP_TAB_STATE,
  testCenterManagementSelectedTopTab
});

const setTestCenterOlaSelectedTopTabState = testCenterOlaSelectedTopTab => ({
  type: SET_TEST_CENTER_OLA_SELECTED_TOP_TAB_STATE,
  testCenterOlaSelectedTopTab
});

const resetTestCenterStates = () => ({
  type: RESET_STATE
});

// creating new test center as ETTA
function createNewTestCenterAsEtta(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/create-new-test-center-as-etta", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
}

// get all test centers as ETTA
function getAllTestCentersAsEtta(page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-test-centers-as-etta/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get all test centers as ETTA
function getFoundTestCentersAsEtta(providedKeyword, currentLanguage, page, pageSize) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-test-centers-as-etta/?keyword=${keyword}&current_language=${currentLanguage}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get selected test center data as ETTA
function getSelectedTestCenterDataAsEtta(id) {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-selected-test-center-data-as-etta/?id=${id}`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// updating test center data
function updateTestCenterDataAsEtta(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/update-test-center-data-as-etta", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// adding test center manager for test center accesses
function addTestCenterManager(userId, testCenterId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/add-test-center-manager-accesses/?user_id=${encodeURIComponent(
        userId
      )}&test_center_id=${testCenterId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
}

// deleting test center manager for test center accesses
function deleteTestCenterManager(userId, testCenterId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/delete-test-center-manager-accesses/?user_id=${encodeURIComponent(
        userId
      )}&test_center_id=${testCenterId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
}

// get associated test centers
function getAssociatedTestCenters(page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-associated-test-centers/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get found associated test centers
function getFoundAssociatedTestCenters(providedKeyword, page, pageSize) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-associated-test-centers/?keyword=${keyword}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get selected test center data
function getSelectedTestCenterData(id) {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-selected-test-center-data/?id=${id}`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// updating test center data
function updateTestCenterData(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/update-test-center-data", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// associating test administrators to test center
function associateTestAdministratorsToTestCenter(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/associate-test-administrators-to-test-center", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// get test center associated test administrators
function getAssociatedTestAdministratorsToTestCenter(page, pageSize, testCenterId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-associated-test-administrators-to-test-center/?page=${page}&page_size=${pageSize}&test_center_id=${testCenterId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get test center associated test administrators
function getFoundAssociatedTestAdministratorsToTestCenter(
  providedKeyword,
  page,
  pageSize,
  testCenterId
) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-associated-test-administrators-to-test-center/?keyword=${keyword}&page=${page}&page_size=${pageSize}&test_center_id=${testCenterId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// deleting test administrator from test center
function deleteTestAdministratorFromTestCenter(data) {
  return async function () {
    const response = await fetch(
      "/oec-cat/api/delete-associated-test-administrator-from-test-center",
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        },
        body: JSON.stringify(data)
      }
    );
    if (response.status === 200) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// adding room to test center
function addRoomToTestCenter(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/add-room-to-test-center", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// get test center rooms
function getTestCenterRooms(page, pageSize, testCenterId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-test-center-rooms/?page=${page}&page_size=${pageSize}&test_center_id=${testCenterId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get found test center rooms
function getFoundTestCenterRooms(providedKeyword, page, pageSize, testCenterId) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-test-center-rooms/?keyword=${keyword}&page=${page}&page_size=${pageSize}&test_center_id=${testCenterId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// editing test center room
function editTestCenterRoom(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/edit-test-center-room", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// deleting test center room
function deleteTestCenterRoom(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/delete-test-center-room", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// adding test session to test center
function addTestSessionToTestCenter(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/add-test-session-to-test-center", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// adding non-standard test session to test center (test session for accommodations)
function addNonStandardTestSessionToTestCenter(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/add-non-standard-test-session-to-test-center", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// get test center test sessions
function getTestCenterTestSessions(page, pageSize, testCenterId, source) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-test-center-test-sessions/?page=${page}&page_size=${pageSize}&test_center_id=${testCenterId}&source=${source}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get found test center test sessions
function getFoundTestCenterTestSessions(
  providedKeyword,
  currentLanguage,
  page,
  pageSize,
  testCenterId,
  source
) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-test-center-test-sessions/?keyword=${keyword}&current_language=${currentLanguage}&page=${page}&page_size=${pageSize}&test_center_id=${testCenterId}&source=${source}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting test session related attendees
function getTestSessionAttendees(testSessionId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-test-session-attendees/?test_session_id=${testSessionId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting data of test center test session to edit
function getDataOfTestCenterTestSessionToViewOrEdit(testSessionId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-data-of-test-center-test-session-to-view-or-edit/?test_session_id=${testSessionId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// editing test center test session
function editTestCenterTestSession(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/edit-test-center-test-session", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}
// editing non standard test center test session
function editNonStandardTestCenterTestSession(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/edit-non-standard-test-center-test-session", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// deleting test center test session
function deleteTestCenterTestSession(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/delete-test-center-test-session", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// get TA related test center test sessions
function getTaRelatedTestCenterTestSessions() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-ta-related-test-center-test-sessions", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// get Test Session and TA related test to administer options
function getTestSessionAndTaRelatedTestToAdministerOptions(testSessionId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-test-session-and-ta-related-test-to-administer-options/?test_session_id=${testSessionId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get Test Session related test administrators
function getTestSessionRelatedTestOfficers(testSessionId, testId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-test-session-related-test-officers/?test_session_id=${testSessionId}&test_id=${testId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get all countries list
function getGetListOfCountries() {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-list-of-countries/`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// get supported countries list
function getGetListOfSupportedCountries() {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-list-of-supported-countries/`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// updating test center OLA configs data
function updateTestCenterOlaConfigsData(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/update-test-center-ola-configs-data", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// create new test center OLA vacation block
function createNewTestCenterOlaVacationBlock(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/create-new-test-center-ola-vacation-block", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// edit test center OLA vacation block
function editTestCenterOlaVacationBlock(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/edit-test-center-ola-vacation-block", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// delete test center OLA vacation block
function deleteTestCenterOlaVacationBlock(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/delete-test-center-ola-vacation-block", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// add test center OLA test assessor
function addTestCenterOlaTestAssessor(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/add-test-center-ola-test-assessor", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// edit test center OLA test assessor
function editTestCenterOlaTestAssessor(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/edit-test-center-ola-test-assessor", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// delete test center OLA test assessor
function deleteTestCenterOlaTestAssessor(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/delete-test-center-ola-test-assessor", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// get test center OLA time slot assessor availabilities
function getTestCenterOlaTimeSlotAssessorAvailabilities(
  testCenterId,
  languageId,
  testCenterOlaTimeSlotId
) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-test-center-ola-time-slot-assessor-availabilities/?test_center_id=${testCenterId}&language_id=${languageId}&test_center_ola_time_slot_id=${testCenterOlaTimeSlotId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// add test center OLA time slot
function addTestCenterOlaTimeSlot(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/add-test-center-ola-time-slot", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// edit test center OLA time slot
function editTestCenterOlaTimeSlot(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/edit-test-center-ola-time-slot", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// delete test center OLA time slot
function deleteTestCenterOlaTimeSlot(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/delete-test-center-ola-time-slot", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 409) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// Initial State
const initialState = {
  testCentersPaginationPage: 1,
  testCentersPaginationPageSize: 25,
  testCentersKeyword: "",
  testCentersActiveSearch: false,
  ettaTestCenterAdministrationPaginationPage: 1,
  ettaTestCenterAdministrationPaginationPageSize: 25,
  ettaTestCenterAdministrationKeyword: "",
  ettaTestCenterAdministrationActiveSearch: false,
  testCenterTestAdministratorsPaginationPage: 1,
  testCenterTestAdministratorsPaginationPageSize: 25,
  testCenterTestAdministratorsKeyword: "",
  testCenterTestAdministratorsActiveSearch: false,
  testCenterRoomsPaginationPage: 1,
  testCenterRoomsPaginationPageSize: 25,
  testCenterRoomsKeyword: "",
  testCenterRoomsActiveSearch: false,
  testCenterTestSessionsSelectedTab: "",
  testCenterTestSessionsPaginationPage: 1,
  testCenterTestSessionsPaginationPageSize: 25,
  testCenterTestSessionsKeyword: "",
  testCenterNonStandardTestSessionData: [],
  testCenterNonStandardTestSessionValidFormState: false,
  testCenterTestSessionsActiveSearch: false,
  testCenterAccommodationRequestsPaginationPage: 1,
  testCenterAccommodationRequestsPaginationPageSize: 25,
  testCenterAccommodationRequestsKeyword: "",
  testCenterAccommodationRequestsActiveSearch: false,
  testCenterInfoContainsChanges: false,
  triggerTableUpdatesState: false,
  triggerRerender: false,
  testCenterDataEtta: {},
  testCenterData: {},
  testCenterDataSelectedSideNavItem: 0,
  testCenterManagementSelectedTopTab: "",
  testCenterOlaSelectedTopTab: ""
};

const testCenter = (state = initialState, action) => {
  switch (action.type) {
    case SET_TEST_CENTERS_PAGINATION_PAGE:
      return {
        ...state,
        testCentersPaginationPage: action.testCentersPaginationPage
      };
    case SET_TEST_CENTERS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        testCentersPaginationPageSize: action.testCentersPaginationPageSize
      };
    case SET_TEST_CENTERS_SEARCH_PARAMETERS:
      return {
        ...state,
        testCentersKeyword: action.testCentersKeyword,
        testCentersActiveSearch: action.testCentersActiveSearch
      };
    case SET_ETTA_TEST_CENTER_ADMINISTRATION_PAGINATION_PAGE:
      return {
        ...state,
        ettaTestCenterAdministrationPaginationPage:
          action.ettaTestCenterAdministrationPaginationPage
      };
    case SET_ETTA_TEST_CENTER_ADMINISTRATION_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        ettaTestCenterAdministrationPaginationPageSize:
          action.ettaTestCenterAdministrationPaginationPageSize
      };
    case SET_ETTA_TEST_CENTER_ADMINISTRATION_SEARCH_PARAMETERS:
      return {
        ...state,
        ettaTestCenterAdministrationKeyword: action.ettaTestCenterAdministrationKeyword,
        ettaTestCenterAdministrationActiveSearch: action.ettaTestCenterAdministrationActiveSearch
      };
    case SET_TEST_CENTER_ASSOCIATED_TEST_ADMINISTRATORS_PAGINATION_PAGE:
      return {
        ...state,
        testCenterTestAdministratorsPaginationPage:
          action.testCenterTestAdministratorsPaginationPage
      };
    case SET_TEST_CENTER_ASSOCIATED_TEST_ADMINISTRATORS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        testCenterTestAdministratorsPaginationPageSize:
          action.testCenterTestAdministratorsPaginationPageSize
      };
    case SET_TEST_CENTER_ASSOCIATED_TEST_ADMINISTRATORS_SEARCH_PARAMETERS:
      return {
        ...state,
        testCenterTestAdministratorsKeyword: action.testCenterTestAdministratorsKeyword,
        testCenterTestAdministratorsActiveSearch: action.testCenterTestAdministratorsActiveSearch
      };
    case SET_TEST_CENTER_ROOMS_PAGINATION_PAGE:
      return {
        ...state,
        testCenterRoomsPaginationPage: action.testCenterRoomsPaginationPage
      };
    case SET_TEST_CENTER_ROOMS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        testCenterRoomsPaginationPageSize: action.testCenterRoomsPaginationPageSize
      };
    case SET_TEST_CENTER_ROOMS_SEARCH_PARAMETERS:
      return {
        ...state,
        testCenterRoomsKeyword: action.testCenterRoomsKeyword,
        testCenterRoomsActiveSearch: action.testCenterRoomsActiveSearch
      };
    case SET_TEST_CENTER_TEST_SESSIONS_SELECTED_TAB:
      return {
        ...state,
        testCenterTestSessionsSelectedTab: action.testCenterTestSessionsSelectedTab
      };
    case SET_TEST_CENTER_TEST_SESSIONS_PAGINATION_PAGE:
      return {
        ...state,
        testCenterTestSessionsPaginationPage: action.testCenterTestSessionsPaginationPage
      };
    case SET_TEST_CENTER_TEST_SESSIONS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        testCenterTestSessionsPaginationPageSize: action.testCenterTestSessionsPaginationPageSize
      };
    case SET_TEST_CENTER_TEST_SESSIONS_SEARCH_PARAMETERS:
      return {
        ...state,
        testCenterTestSessionsKeyword: action.testCenterTestSessionsKeyword,
        testCenterTestSessionsActiveSearch: action.testCenterTestSessionsActiveSearch
      };
    case SET_TEST_CENTER_NON_STANDARD_TEST_SESSION_DATA:
      return {
        ...state,
        testCenterNonStandardTestSessionData: action.testCenterNonStandardTestSessionData
      };
    case SET_TEST_CENTER_NON_STANDARD_TEST_SESSION_VALID_FORM_STATE:
      return {
        ...state,
        testCenterNonStandardTestSessionValidFormState:
          action.testCenterNonStandardTestSessionValidFormState
      };
    case SET_TEST_CENTER_ACCOMMODATION_REQUESTS_PAGINATION_PAGE:
      return {
        ...state,
        testCenterAccommodationRequestsPaginationPage:
          action.testCenterAccommodationRequestsPaginationPage
      };
    case SET_TEST_CENTER_ACCOMMODATION_REQUESTS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        testCenterAccommodationRequestsPaginationPageSize:
          action.testCenterAccommodationRequestsPaginationPageSize
      };
    case SET_TEST_CENTER_ACCOMMODATION_REQUESTS_SEARCH_PARAMETERS:
      return {
        ...state,
        testCenterAccommodationRequestsKeyword: action.testCenterAccommodationRequestsKeyword,
        testCenterAccommodationRequestsActiveSearch:
          action.testCenterAccommodationRequestsActiveSearch
      };
    case SET_TEST_CENTER_INFO_CONTAINS_CHANGES:
      return {
        ...state,
        testCenterInfoContainsChanges: action.testCenterInfoContainsChanges
      };
    case TRIGGER_TABLE_UPDATES:
      return {
        ...state,
        triggerTableUpdatesState: !state.triggerTableUpdatesState
      };
    case TRIGGER_RERENDER:
      return {
        ...state,
        triggerRerender: !state.triggerRerender
      };
    case RESET_SELECTED_TEST_CENTER_ETTA_STATES:
      return {
        ...state,
        testCenterDataEtta: {}
      };
    case SET_ETTA_TEST_CENTER_DATA:
      return {
        ...state,
        testCenterDataEtta: action.testCenterDataEtta
      };
    case RESET_SELECTED_TEST_CENTER_STATES:
      return {
        ...state,
        testCenterData: {},
        testCenterDataSelectedSideNavItem: 0,
        testCenterTestSessionsSelectedTab: "",
        testCenterManagementSelectedTopTab: "",
        testCenterOlaSelectedTopTab: ""
      };
    case SET_TEST_CENTER_DATA:
      return {
        ...state,
        testCenterData: action.testCenterData
      };
    case SET_TEST_CENTER_DATA_SELECTED_SIDE_NAV_STATE:
      return {
        ...state,
        testCenterDataSelectedSideNavItem: action.testCenterDataSelectedSideNavItem
      };
    case SET_TEST_CENTER_MANAGEMENT_SELECTED_TOP_TAB_STATE:
      return {
        ...state,
        testCenterManagementSelectedTopTab: action.testCenterManagementSelectedTopTab
      };
    case SET_TEST_CENTER_OLA_SELECTED_TOP_TAB_STATE:
      return {
        ...state,
        testCenterOlaSelectedTopTab: action.testCenterOlaSelectedTopTab
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default testCenter;
export {
  initialState,
  getAllTestCentersAsEtta,
  getFoundTestCentersAsEtta,
  getSelectedTestCenterDataAsEtta,
  setEttaTestCenterData,
  updateTestCenterDataAsEtta,
  addTestCenterManager,
  deleteTestCenterManager,
  getAssociatedTestCenters,
  getFoundAssociatedTestCenters,
  updateCurrentTestCentersPageState,
  updateTestCentersPageSizeState,
  updateSearchTestCentersStates,
  updateCurrentEttaTestCenterAdministrationPageState,
  updateEttaTestCenterAdministrationPageSizeState,
  updateSearchEttaTestCenterAdministrationStates,
  triggerTableUpdates,
  triggerRerender,
  resetEttaSelectedTestCenterStates,
  createNewTestCenterAsEtta,
  resetSelectedTestCenterStates,
  setTestCenterData,
  getSelectedTestCenterData,
  resetTestCenterStates,
  updateTestCenterData,
  associateTestAdministratorsToTestCenter,
  getAssociatedTestAdministratorsToTestCenter,
  getFoundAssociatedTestAdministratorsToTestCenter,
  updateCurrentTestCenterTestAdministratorsPageState,
  updateTestCenterTestAdministratorsPageSizeState,
  updateSearchTestCenterTestAdministratorsStates,
  deleteTestAdministratorFromTestCenter,
  updateCurrentTestCenterRoomsPageState,
  updateTestCenterRoomsPageSizeState,
  updateSearchTestCenterRoomsStates,
  addRoomToTestCenter,
  getTestCenterRooms,
  getFoundTestCenterRooms,
  deleteTestCenterRoom,
  editTestCenterRoom,
  updateCurrentTestCenterTestSessionsPageState,
  updateTestCenterTestSessionsPageSizeState,
  updateSearchTestCenterTestSessionsStates,
  updateTestSessionNonStandardTestSessionData,
  updateTestSessionNonStandardTestSessionValidFormState,
  updateCurrentTestCenterAccommodationRequestsPageState,
  updateTestCenterAccommodationRequestsPageSizeState,
  updateSearchTestCenterAccommodationRequestsStates,
  updateTestCenterInfoContainsChangesState,
  addTestSessionToTestCenter,
  addNonStandardTestSessionToTestCenter,
  getTestCenterTestSessions,
  getFoundTestCenterTestSessions,
  getDataOfTestCenterTestSessionToViewOrEdit,
  editTestCenterTestSession,
  editNonStandardTestCenterTestSession,
  deleteTestCenterTestSession,
  getTaRelatedTestCenterTestSessions,
  getTestSessionAndTaRelatedTestToAdministerOptions,
  getTestSessionRelatedTestOfficers,
  getTestSessionAttendees,
  getGetListOfCountries,
  getGetListOfSupportedCountries,
  setTestCenterTestSessionsSelectedTab,
  setTestCenterDataSideNavState,
  setTestCenterManagementSelectedTopTabState,
  updateTestCenterOlaConfigsData,
  createNewTestCenterOlaVacationBlock,
  editTestCenterOlaVacationBlock,
  deleteTestCenterOlaVacationBlock,
  setTestCenterOlaSelectedTopTabState,
  addTestCenterOlaTestAssessor,
  editTestCenterOlaTestAssessor,
  deleteTestCenterOlaTestAssessor,
  getTestCenterOlaTimeSlotAssessorAvailabilities,
  addTestCenterOlaTimeSlot,
  editTestCenterOlaTimeSlot,
  deleteTestCenterOlaTimeSlot
};
