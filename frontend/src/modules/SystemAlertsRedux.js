import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
export const SET_SYSTEM_ALERT_DATA = "systemAlerts/SET_SYSTEM_ALERT_DATA";
export const SET_SYSTEM_ALERT_DATA_VALID_FORM_STATE =
  "systemAlerts/SET_SYSTEM_ALERT_DATA_VALID_FORM_STATE";
export const SET_SYSTEM_ALERT_DATA_PREVIEW_VALID_STATE =
  "systemAlerts/SET_SYSTEM_ALERT_DATA_PREVIEW_VALID_STATE";
export const RESET_SYSTEM_ALERT_DATA = "systemAlerts/RESET_SYSTEM_ALERT_DATA";
export const SET_ARCHIVED_SYSTEM_ALERTS_PAGINATION_PAGE =
  "systemAlerts/SET_ARCHIVED_SYSTEM_ALERTS_PAGINATION_PAGE";
export const SET_ARCHIVED_SYSTEM_ALERTS_PAGINATION_PAGE_SIZE =
  "systemAlerts/SET_ARCHIVED_SYSTEM_ALERTS_PAGINATION_PAGE_SIZE";
export const SET_ARCHIVED_SYSTEM_ALERTS_SEARCH_PARAMETERS =
  "systemAlerts/SET_ARCHIVED_SYSTEM_ALERTS_SEARCH_PARAMETERS";
export const TRIGGER_ACTIVE_SYSTEM_ALERT_TABLE_RERENDER =
  "systemAlerts/TRIGGER_ACTIVE_SYSTEM_ALERT_TABLE_RERENDER";
export const TRIGGER_ARCHIVED_SYSTEM_ALERT_TABLE_RERENDER =
  "systemAlerts/TRIGGER_ARCHIVED_SYSTEM_ALERT_TABLE_RERENDER";

// Action Creators
// update system alert data
const updateSystemAlertData = systemAlertData => ({
  type: SET_SYSTEM_ALERT_DATA,
  systemAlertData
});
// update system alert data valid form state
const updateSystemAlertDataValidFormState = systemAlertDataValidForm => ({
  type: SET_SYSTEM_ALERT_DATA_VALID_FORM_STATE,
  systemAlertDataValidForm
});
// update system alert data preview valid state
const updateSystemAlertDataPreviewValidState = systemAlertDataPreviewValidState => ({
  type: SET_SYSTEM_ALERT_DATA_PREVIEW_VALID_STATE,
  systemAlertDataPreviewValidState
});
// reset system alert data
const resetSystemAlertData = () => ({
  type: RESET_SYSTEM_ALERT_DATA
});
// update pagination page state (archived system alerts)
const updateArchivedSystemAlertsPageState = archivedSystemAlertsPage => ({
  type: SET_ARCHIVED_SYSTEM_ALERTS_PAGINATION_PAGE,
  archivedSystemAlertsPage
});
// update pagination pageSize state (archived system alerts)
const updateArchivedSystemAlertsPageSizeState = archivedSystemAlertsPageSize => ({
  type: SET_ARCHIVED_SYSTEM_ALERTS_PAGINATION_PAGE_SIZE,
  archivedSystemAlertsPageSize
});
// update search keyword state (archived system alerts)
const updateArchivedSystemAlertsStates = (
  archivedSystemAlertsKeyword,
  archivedSystemAlertsActiveSearch
) => ({
  type: SET_ARCHIVED_SYSTEM_ALERTS_SEARCH_PARAMETERS,
  archivedSystemAlertsKeyword,
  archivedSystemAlertsActiveSearch
});
// rerender table (active system alerts)
const triggerActiveSystemAlertTableRerender = () => ({
  type: TRIGGER_ACTIVE_SYSTEM_ALERT_TABLE_RERENDER
});
// rerender table (archived system alerts)
const triggerArchivedSystemAlertTableRerender = () => ({
  type: TRIGGER_ARCHIVED_SYSTEM_ALERT_TABLE_RERENDER
});

// get active system alerts that need to be displayed on the home page
function getActiveSystemAlertsToBeDisplayedOnHomePage() {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-active-system-alerts-to-be-displayed-on-home-page`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get all archived system alerts
function getSystemAlertData(id) {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-system-alert-data/?id=${id}`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// create new system alert
function createSystemAlert(body) {
  const newObj = {
    ...body
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/create-system-alert`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    // if successful create request
    if (response.status === 201) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// modify system alert
function modifySystemAlert(body) {
  const newObj = {
    ...body
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/modify-system-alert`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    // if successful create request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// archive system alert
function archiveSystemAlert(body) {
  const newObj = {
    ...body
  };
  return async function () {
    const response = await fetch(`/oec-cat/api/archive-system-alert`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    // if successful create request
    if (response.status === 200) {
      return response;
      // else there is an error
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// get all active system alerts
function getAllActiveSystemAlerts() {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-all-active-system-alerts/`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// get all archived system alerts
function getAllArchivedSystemAlerts(page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-archived-system-alerts/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get all archived system alerts
function getFoundArchivedSystemAlerts(providedKeyword, currentLanguage, page, pageSize) {
  // if provided keyword is empty
  let keyword = encodeURIComponent(providedKeyword);
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-archived-system-alerts/?keyword=${keyword}&current_language=${currentLanguage}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get all active criticality options or specific based on provided ID
function getCriticalityOptions(id) {
  let convertedId = id;
  if (convertedId === null || typeof convertedId === "undefined") {
    convertedId = "";
  }
  return async function () {
    const response = await fetch(`/oec-cat/api/get-criticality-options/?id=${convertedId}`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// Initial State
const initialState = {
  systemAlertData: {
    title: "",
    criticality: [],
    startDate: "",
    endDate: "",
    enMessage: "",
    frMessage: ""
  },
  systemAlertDataValidForm: false,
  systemAlertDataPreviewValidState: false,
  archivedSystemAlertsPage: 1,
  archivedSystemAlertsPageSize: 25,
  archivedSystemAlertsKeyword: "",
  archivedSystemAlertsActiveSearch: false,
  triggerActiveSystemAlertTableRerender: false,
  triggerArchivedSystemAlertTableRerender: false
};

// Reducer
const systemAlerts = (state = initialState, action) => {
  switch (action.type) {
    case SET_SYSTEM_ALERT_DATA:
      return {
        ...state,
        systemAlertData: action.systemAlertData
      };
    case SET_SYSTEM_ALERT_DATA_VALID_FORM_STATE:
      return {
        ...state,
        systemAlertDataValidForm: action.systemAlertDataValidForm
      };
    case SET_SYSTEM_ALERT_DATA_PREVIEW_VALID_STATE:
      return {
        ...state,
        systemAlertDataPreviewValidState: action.systemAlertDataPreviewValidState
      };
    case RESET_SYSTEM_ALERT_DATA:
      return {
        ...state,
        systemAlertData: {
          title: initialState.systemAlertData.title,
          criticality: initialState.systemAlertData.criticality,
          startDate: initialState.systemAlertData.startDate,
          endDate: initialState.systemAlertData.endDate,
          enMessage: initialState.systemAlertData.enMessage,
          frMessage: initialState.systemAlertData.frMessage
        }
      };
    case SET_ARCHIVED_SYSTEM_ALERTS_PAGINATION_PAGE:
      return {
        ...state,
        archivedSystemAlertsPage: action.archivedSystemAlertsPage
      };
    case SET_ARCHIVED_SYSTEM_ALERTS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        archivedSystemAlertsPageSize: action.archivedSystemAlertsPageSize
      };
    case SET_ARCHIVED_SYSTEM_ALERTS_SEARCH_PARAMETERS:
      return {
        ...state,
        archivedSystemAlertsKeyword: action.archivedSystemAlertsKeyword,
        archivedSystemAlertsActiveSearch: action.archivedSystemAlertsActiveSearch
      };
    case TRIGGER_ACTIVE_SYSTEM_ALERT_TABLE_RERENDER:
      return {
        ...state,
        triggerActiveSystemAlertTableRerender: !state.triggerActiveSystemAlertTableRerender
      };
    case TRIGGER_ARCHIVED_SYSTEM_ALERT_TABLE_RERENDER:
      return {
        ...state,
        triggerArchivedSystemAlertTableRerender: !state.triggerArchivedSystemAlertTableRerender
      };
    default:
      return state;
  }
};

export default systemAlerts;
export {
  updateSystemAlertData,
  updateSystemAlertDataValidFormState,
  updateSystemAlertDataPreviewValidState,
  resetSystemAlertData,
  updateArchivedSystemAlertsPageState,
  updateArchivedSystemAlertsPageSizeState,
  updateArchivedSystemAlertsStates,
  getActiveSystemAlertsToBeDisplayedOnHomePage,
  getSystemAlertData,
  getAllActiveSystemAlerts,
  getAllArchivedSystemAlerts,
  getFoundArchivedSystemAlerts,
  getCriticalityOptions,
  createSystemAlert,
  modifySystemAlert,
  archiveSystemAlert,
  triggerActiveSystemAlertTableRerender,
  triggerArchivedSystemAlertTableRerender
};
