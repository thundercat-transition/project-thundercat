import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// check if user needs a 2fa code and send (or approve if they do not)
const send2FACodeToUser = access_token => {
  return async function () {
    const response = await fetch(`/oec-cat/api/send-2fa-code-to-user`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseObj = await response.json();
    responseObj.status = response.status;
    return responseObj;
  };
};

// check if user needs a 2fa code and send (or approve if they do not)
const reSend2FACodeToUser = user_id => {
  return async function () {
    const response = await fetch(`/oec-cat/api/resend-2fa-code-to-user?user_id=${user_id}`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseObj = await response.json();
    responseObj.status = response.status;
    return responseObj;
  };
};

// check if the submitted 2fa code is valid (or skip if the permissions do not need it)
const check2FACodeForUser = (user_id, code) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/check-2fa-code-for-user?user_id=${user_id}&code=${code}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
};

// check if the submitted 2fa code is valid (or skip if the permissions do not need it)
const get2FASettings = () => {
  return async function () {
    const response = await fetch(`/oec-cat/api/get_2fa_settings`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = response.json();
    return responseJson;
  };
};

// check if the submitted 2fa code is valid (or skip if the permissions do not need it)
const set2FA = (permission_id, is_2fa_active) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/set_2fa?permission_id=${permission_id}&is_2fa_active=${is_2fa_active}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
};

export default send2FACodeToUser;
export { check2FACodeForUser, reSend2FACodeToUser, get2FASettings, set2FA };
