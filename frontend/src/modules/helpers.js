/* eslint-disable new-cap */
import moment from "moment";
import LOCALIZE from "../text_resources";

// ref: https://stackoverflow.com/questions/46946380/fetch-api-request-timeout
AbortSignal.timeout ??= function timeout(ms) {
  const ctrl = new AbortController();
  setTimeout(() => ctrl.abort(), ms);
  return ctrl.signal;
};

// function that allows us to make sure that the backend AND DB are up and running
// customizable timeout parameter (default timeout time is 5 seconds)
async function getBackendAndDbStatus(timeout = 5000) {
  // initializing backendIsUp
  let backendIsUp = true;
  try {
    // checking if backend is up
    await fetch("/oec-cat/api/backend-status", {
      method: "GET",
      Accept: "application/json",
      "Content-Type": "application/json",
      cache: "default",
      signal: AbortSignal.timeout(timeout)
    });
    // checking if DB is up
    await fetch("/oec-cat/api/database-check", {
      method: "GET",
      Accept: "application/json",
      "Content-Type": "application/json",
      cache: "default",
      signal: AbortSignal.timeout(timeout)
    });
  } catch {
    // setting backendIsUp to false
    backendIsUp = false;
  }
  return backendIsUp;
}

const convertDateTimeToUsersTimezone = dateTime => {
  // default to NA
  let adjustedDatetime = LOCALIZE.commons.na;
  let adjustedDate = LOCALIZE.commons.na;
  let adjustedTime = LOCALIZE.commons.na;
  let adjustedDateTimeBaseOnCurrentUtcOffset = LOCALIZE.commons.na;
  let adjustedDateBaseOnCurrentUtcOffset = LOCALIZE.commons.na;
  let adjustedTimeBaseOnCurrentUtcOffset = LOCALIZE.commons.na;

  // if the date is defined, then convert from utc to the user's time
  if (dateTime !== null) {
    // get UTC offset at the time of the provided dateTime from users computer
    // need to inverse the value as a UTC offset of 180 = UTC-3 => Remove 3 hours from the UTC time
    const providedDateTimeUtcOffset = -new Date(dateTime).getTimezoneOffset();
    const currentUtcOffset = -new Date().getTimezoneOffset();

    adjustedDatetime = moment(dateTime)
      .utcOffset(providedDateTimeUtcOffset)
      .format("YYYY-MM-DDTHH:mm:ssZ");
    adjustedDate = moment(adjustedDatetime).format("YYYY-MM-DD");
    adjustedTime = moment(adjustedDatetime).format("HH:mm");
    adjustedDateTimeBaseOnCurrentUtcOffset = moment(dateTime)
      .utcOffset(currentUtcOffset)
      .format("YYYY-MM-DDTHH:mm:ssZ");
    adjustedDateBaseOnCurrentUtcOffset = moment(adjustedDateTimeBaseOnCurrentUtcOffset).format(
      "YYYY-MM-DD"
    );
    // using a "slice" here to ignore the timezone of the datetime to get the time based on the current UTC offset (otherwise, moment is converting with the wrong offset/timezone)
    adjustedTimeBaseOnCurrentUtcOffset = moment(
      adjustedDateTimeBaseOnCurrentUtcOffset.slice(0, 16)
    ).format("HH:mm");
  }

  return {
    adjustedDatetime: adjustedDatetime,
    adjustedDate: adjustedDate,
    adjustedTime: adjustedTime,
    adjustedDateTimeBaseOnCurrentUtcOffset: adjustedDateTimeBaseOnCurrentUtcOffset,
    adjustedDateBaseOnCurrentUtcOffset: adjustedDateBaseOnCurrentUtcOffset,
    adjustedTimeBaseOnCurrentUtcOffset: adjustedTimeBaseOnCurrentUtcOffset
  };
};

// converting time to user's timezone (expected format 13:15 ==> 09:15)
// note that this conversion should only be called for time without associated date only conversion (OLA time slots for example)
// otherwise, the DST might not be accurate
function convertTimeToUsersTimezone(time) {
  // getting UTC offset in minutes
  const utcOffset = new Date().getTimezoneOffset();
  // removing UTC offset from provided time
  const convertedTime = new moment(time, "HH:mm").subtract(utcOffset, "minutes").format("HH:mm");
  // returning convertedTime
  return convertedTime;
}

// converting user's timezone datetime to UTC
const convertUsersTimezoneDateTimeToUtc = dateTime => {
  return moment(dateTime).utc().format("YYYY-MM-DD HH:mm:ss.SSS Z");
};

export {
  convertDateTimeToUsersTimezone,
  convertTimeToUsersTimezone,
  convertUsersTimezoneDateTimeToUtc
};

export default getBackendAndDbStatus;
