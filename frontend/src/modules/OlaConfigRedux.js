/* eslint-disable prefer-destructuring */
import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action types
const SET_REDUX_OLA_CONFIGS = "olaConfig/SET_REDUX_OLA_CONFIGS";
const SET_REDUX_OLA_PRIORITIZATION = "olaConfig/SET_REDUX_OLA_PRIORITIZATION";
const RESET_STATE = "olaConfig/RESET_STATE";

const setReduxOlaPrioritization = olaPrioritization => ({
  type: SET_REDUX_OLA_PRIORITIZATION,
  olaPrioritization
});

const setReduxOlaConfigs = (cancellationWindow, openBookingWindow) => ({
  type: SET_REDUX_OLA_CONFIGS,
  cancellationWindow,
  openBookingWindow
});

// Initial State
const initialState = {
  olaPrioritization: [],
  cancellationWindow: 0,
  openBookingWindow: 0
};

const resetOlaConfig = () => ({
  type: RESET_STATE
});

const olaConfig = (state = initialState, action) => {
  switch (action.type) {
    case SET_REDUX_OLA_PRIORITIZATION:
      return {
        ...state,
        olaPrioritization: action.olaPrioritization
      };
    case SET_REDUX_OLA_CONFIGS:
      return {
        ...state,
        cancellationWindow: String(action.cancellationWindow),
        openBookingWindow: String(action.openBookingWindow)
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

// get ola prioritization
function getOlaPrioritization() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-ola-prioritization", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const { status } = response;
    const responseJson = await response.json();
    responseJson.status = status;
    return responseJson;
  };
}

// set all ola prioritization
function setOlaPrioritization(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/set-ola-prioritization", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    const { status } = response;
    if (status === 200) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// get ola global configs
function getOlaGlobalConfigs() {
  return async function () {
    const response = await fetch(`/oec-cat/api/get_ola_global_configs /`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const { status } = response;
    const responseJson = await response.json();
    responseJson.status = status;
    return responseJson;
  };
}

// set all ola global configs
function setOlaGlobalConfigs(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/set_ola_global_configs", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    const { status } = response;
    if (status === 200) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

export default olaConfig;
export {
  setReduxOlaPrioritization,
  setReduxOlaConfigs,
  resetOlaConfig,
  getOlaPrioritization,
  setOlaPrioritization,
  getOlaGlobalConfigs,
  setOlaGlobalConfigs
};
