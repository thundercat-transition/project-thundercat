import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action types
// Active Test Access Code Pagination
export const SET_TEST_ACCESS_CODES_PAGINATION_PAGE =
  "permissions/SET_TEST_ACCESS_CODES_PAGINATION_PAGE";
export const SET_TEST_ACCESS_CODES_PAGINATION_PAGE_SIZE =
  "permissions/SET_TEST_ACCESS_CODES_PAGINATION_PAGE_SIZE";
export const SET_TEST_ACCESS_CODES_SEARCH_PARAMETERS =
  "permissions/SET_TEST_ACCESS_CODES_SEARCH_PARAMETERS";

// update pagination page state (active test access codes)
const updateCurrentTestAccessCodesPageState = testAccessCodesPaginationPage => ({
  type: SET_TEST_ACCESS_CODES_PAGINATION_PAGE,
  testAccessCodesPaginationPage
});
// update pagination pageSize state (active test access codes)
const updateTestAccessCodesPageSizeState = testAccessCodesPaginationPageSize => ({
  type: SET_TEST_ACCESS_CODES_PAGINATION_PAGE_SIZE,
  testAccessCodesPaginationPageSize
});
// update search keyword state (active test access codes)
const updateSearchActiveTestAccessCodesStates = (
  testAccessCodeKeyword,
  testAccessCodeActiveSearch
) => ({
  type: SET_TEST_ACCESS_CODES_SEARCH_PARAMETERS,
  testAccessCodeKeyword,
  testAccessCodeActiveSearch
});

// get all active test access code data
function getAllActiveTestAccessCodesRedux(page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-active-test-access-codes/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get found active test access codes
function getFoundActiveTestAccessCodes(currentLanguage, providedKeyword, page, pageSize) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-active-test-access-codes/?current_language=${currentLanguage}&keyword=${keyword}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// Initial State
const initialState = {
  testAccessCodesPaginationPage: 1,
  testAccessCodesPaginationPageSize: 25,
  testAccessCodeKeyword: "",
  testAccessCodeActiveSearch: false
};

const testAccessCodes = (state = initialState, action) => {
  switch (action.type) {
    case SET_TEST_ACCESS_CODES_PAGINATION_PAGE:
      return {
        ...state,
        testAccessCodesPaginationPage: action.testAccessCodesPaginationPage
      };
    case SET_TEST_ACCESS_CODES_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        testAccessCodesPaginationPageSize: action.testAccessCodesPaginationPageSize
      };
    case SET_TEST_ACCESS_CODES_SEARCH_PARAMETERS:
      return {
        ...state,
        testAccessCodeKeyword: action.testAccessCodeKeyword,
        testAccessCodeActiveSearch: action.testAccessCodeActiveSearch
      };
    default:
      return state;
  }
};

export default testAccessCodes;
export {
  initialState,
  getAllActiveTestAccessCodesRedux,
  getFoundActiveTestAccessCodes,
  updateCurrentTestAccessCodesPageState,
  updateTestAccessCodesPageSizeState,
  updateSearchActiveTestAccessCodesStates
};
