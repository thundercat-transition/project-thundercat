import SessionStorage, { ACTION, ITEM } from "../SessionStorage";
// TODO this is a placeholder for when the API for the soring key is created/completed

// Scorer Assigned Tests Pagination
export const SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE =
  "scorer/SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE";
export const SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE_SIZE =
  "scorer/SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE_SIZE";
export const TOGGLE_SIDEBAR = "scorer/TOGGLE_SIDEBAR";
export const UPDATE_COMPETENCY = "scorer/UPDATE_COMPETENCY";
export const UPDATE_RATIONALE = "scorer/UPDATE_RATIONALE";
export const UPDATE_RATINGS = "scorer/UPDATE_RATINGS";
export const SET_SCORER_TEST = "scorer/SET_SCORER_TEST";
export const LOAD_SCORES = "scorer/LOAD_SCORES";

export const SET_SCORER_OLA_SELECTED_TAB = "scorer/SET_SCORER_OLA_SELECTED_TAB";

// Assessors OLA Unavailability
// Page State
export const SET_ASSESSORS_OLA_UNAVAILABILITY_PAGINATION_PAGE =
  "scorer/SET_ASSESSORS_OLA_UNAVAILABILITY_PAGINATION_PAGE";
// Page Size State
export const SET_ASSESSORS_OLA_UNAVAILABILITY_PAGINATION_PAGE_SIZE =
  "scorer/SET_ASSESSORS_OLA_UNAVAILABILITY_PAGINATION_PAGE_SIZE";
// States
export const SET_ASSESSORS_OLA_UNAVAILABILITY_SEARCH_PARAMETERS =
  "scorer/SET_ASSESSORS_OLA_UNAVAILABILITY_SEARCH_PARAMETERS";

export const RERENDER_SCORER_OLA_TABLE = "scorer/RERENDER_SCORER_OLA_TABLE";

export const RESET_SCORER_STATES = "testCenter/RESET_SCORER_STATES";

// calls
// update pagination page state (scorer assigned tests)
const updateCurrentScorerAssignedTestPageState = scorerPaginationPage => ({
  type: SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE,
  scorerPaginationPage
});
// update pagination pageSize state (scorer assigned tests)
const updateScorerAssignedTestPageSizeState = scorerPaginationPageSize => ({
  type: SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE_SIZE,
  scorerPaginationPageSize
});
export const loadScores = scores => ({
  type: LOAD_SCORES,
  scores
});
export const setScorerTest = testId => ({
  type: SET_SCORER_TEST,
  testId
});
export const updateCompetencies = scorerInfo => ({
  type: UPDATE_COMPETENCY,
  scorerInfo
});
export const updateRatings = question => ({
  type: UPDATE_RATINGS,
  question
});
export const updateQuestionScore = obj => ({
  type: UPDATE_RATIONALE,
  obj
});
// Toggle the scorer sidebar
const toggleSidebar = () => ({
  type: TOGGLE_SIDEBAR
});

const setScorerOLASelectedTab = scorerOLASelectedTab => ({
  type: SET_SCORER_OLA_SELECTED_TAB,
  scorerOLASelectedTab
});

const triggerRerenderScorerOlaTables = () => ({
  type: RERENDER_SCORER_OLA_TABLE
});

const resetScorerStates = () => ({
  type: RESET_SCORER_STATES
});

// OLA Unavailability

// Page State
const updateAssessorsOLAUnavailabilityPageState = assessorsOLAUnavailabilityPaginationPage => ({
  type: SET_ASSESSORS_OLA_UNAVAILABILITY_PAGINATION_PAGE,
  assessorsOLAUnavailabilityPaginationPage
});

// Page Size State
const updateAssessorsOLAUnavailabilityPageSizeState =
  assessorsOLAUnavailabilityPaginationPageSize => ({
    type: SET_ASSESSORS_OLA_UNAVAILABILITY_PAGINATION_PAGE_SIZE,
    assessorsOLAUnavailabilityPaginationPageSize
  });

// States
const updateAssessorsOLAUnavailabilityStates = (
  assessorsOLAUnavailabilityKeyword,
  assessorsOLAUnavailabilityActiveSearch
) => ({
  type: SET_ASSESSORS_OLA_UNAVAILABILITY_SEARCH_PARAMETERS,
  assessorsOLAUnavailabilityKeyword,
  assessorsOLAUnavailabilityActiveSearch
});

function saveScore(scoreObj) {
  return async function () {
    const response = await fetch(`/oec-cat/api/save-score/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(scoreObj)
    });
    const responseObj = {
      ok: response.ok
    };
    return responseObj;
  };
}

const getScores = assignedTestId => {
  return async function () {
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
      cache: "default"
    };
    if (SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)) {
      headers.Authorization = `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`;
    }
    const url = `/oec-cat/api/get-scores?scorer_test=${assignedTestId}`;
    const response = await fetch(url, {
      method: "GET",
      headers: headers
    });
    const testSection = await response.json();
    return testSection;
  };
};

// get scorer assigned tests (all tests assigned to this scorer)
function getAssignedTests(userId, page, pageSize) {
  return async function () {
    const response = await fetch(
      "/oec-cat/api/get_scorer_assigned_tests" +
        `?scorer_user_id=${userId}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting the test section content based on the id
const getInboxTestSection = assignedTestId => {
  return async function () {
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
      cache: "default"
    };
    if (SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)) {
      headers.Authorization = `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`;
    }
    const url = `/oec-cat/api/get-scorer-test-section?assigned_test_id=${assignedTestId}`;
    const response = await fetch(url, {
      method: "GET",
      headers: headers
    });
    const testSection = await response.json();
    return testSection;
  };
};

// checking if user is associated to a test center
function isUserAssociatedToTestCenter() {
  return async function () {
    const response = await fetch(`/oec-cat/api/is-user-associated-to-test-center/`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseObj = await response.json();
    return responseObj;
  };
}

// getting all tests to assess as a scorer OLA supervisor
const getScorerOlaTestsToAssessAsSupervisor = (dateFrom, dateTo) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/as-supervisor-get-scorer-ola-tests-to-assess/?date_from=${dateFrom}&date_to=${dateTo}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
};

// getting available test assessors as a scorer OLA supervisor
const getAvailableTestAssessorsAsSupervisor = (
  DayOfWeekId,
  startTime,
  simplifiedStartTime,
  endTime,
  simplifiedEndTime,
  languageId
) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/as-supervisor-get-available-test-assessors/?day_of_week_id=${DayOfWeekId}&start_time=${startTime}&simplified_start_time=${simplifiedStartTime}&end_time=${endTime}&simplified_end_time=${simplifiedEndTime}&language_id=${languageId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
};

// cancelling test session as a scorer OLA supervisor
function cancelTestSessionAsScorerOla(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/as-supervisor-cancel-test-session", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting test session skip options
const getScorerOlaTestSessionSkipOptions = () => {
  return async function () {
    const response = await fetch("/oec-cat/api/get-scorer-ola-test-session-skip-options", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseObj = await response.json();
    return responseObj;
  };
};

// getting highest priority candidate to assign
const getHighestPriorityCandidateToAssignAsScorerOla = (
  date,
  startTime,
  endTime,
  providedIndex,
  reasonForSkippingId
) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-highest-priority-candidate-to-assign-as-scorer-ola/?date=${date}&start_time=${startTime}&end_time=${endTime}&provided_index=${providedIndex}&reason_for_skipping_id=${reasonForSkippingId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
};

// assigning candidate as a scorer OLA
function assignCandidateAsScorerOla(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/assign-candidate-as-scorer-ola", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting all of my tests for the day
const getScorerOlaMyAssignedTests = (dateFrom, dateTo) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-scorer-ola-my-assigned-tests/?date_from=${dateFrom}&date_to=${dateTo}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
};

// getting assigned candidate details
const getScorerOlaAssignedCandidateDetails = testSessionId => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-scorer-ola-assigned-candidate-details/?test_session_id=${testSessionId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
};

// getting scorer OLA my availability data
const getScorerOlaMyAvailabilityData = () => {
  return async function () {
    const response = await fetch("/oec-cat/api/get-scorer-ola-my-availability-data/", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseObj = await response.json();
    return responseObj;
  };
};

// unassigning candidate as a scorer OLA
function unassignCandidateAsScorerOla(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/unassign-candidate-as-scorer-ola", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    if (response.status === 200 || response.status === 404) {
      return response;
    }
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting all assessors' unavailability data as a scorer OLA supervisor
const getAllAssessorsOlaUnavailabilityAsSupervisor = (page, pageSize) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-test-centers-ola-unavailability-for-test-center-supervised-by-current-user/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
};

// getting found assessors' unavailability data as a scorer OLA supervisor
const getFoundAssessorsOlaUnavailabilityAsSupervisor = (
  providedKeyword,
  currentLanguage,
  page,
  pageSize
) => {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-test-centers-ola-unavailability-for-test-center-supervised-by-current-user/?keyword=${keyword}&current_language=${currentLanguage}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
};

// getting all assessors a scorer OLA supervisor
const getAllOlaAssessorsSupervisedByCurrentUser = () => {
  return async function () {
    const response = await fetch(`/oec-cat/api/get-all-ola-assessors-supervised-by-current-user`, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseObj = await response.json();
    return responseObj;
  };
};

// getting all test centers for the selected ola assessor
const getAllOlaTestCentersForSelectedAssessor = user_id => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-ola-test-centers-for-selected-assessor?user_id=${user_id}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
};

// Creation of new assessor unavailability
const saveNewAssessorUnavailability = data => {
  return async function () {
    const response = await fetch(`/oec-cat/api/save-unavailability-days-for-specific-assessor`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
};

// Modification of assessor unavailability
const modifyAssessorUnavailability = data => {
  return async function () {
    const response = await fetch(`/oec-cat/api/modify-unavailability-days-for-specific-assessor`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
};

// Deletion of assessor unavailability
const deleteAssessorUnavailability = data => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/delete-unavailability-days-for-specific-test-center-ola-assessor-unavailability`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        },
        body: JSON.stringify(data)
      }
    );
    return response;
  };
};

// Initial State
const initialState = {
  scorerPaginationPage: 1,
  scorerPaginationPageSize: 25,
  isSidebarHidden: false,
  currentQuestionIndex: 0,
  currentCompetencyIndex: 0,
  testKey: undefined,
  currentCompetency: undefined,
  currentQuestion: undefined,
  assignedTestId: undefined,
  answerScores: [],
  scorerOLASelectedTab: "",
  triggerRerenderScorerTables: false,

  // OLA Unavailability
  assessorsOLAUnavailabilityPaginationPage: 1,
  assessorsOLAUnavailabilityPaginationPageSize: 25,
  assessorsOLAUnavailabilityActiveSearch: false,
  assessorsOLAUnavailabilityKeyword: ""
};

// Reducer
const scorer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SCORER_TEST:
      return {
        ...state,
        assignedTestId: action.testId
      };
    case SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE:
      return {
        ...state,
        scorerPaginationPage: action.scorerPaginationPage
      };
    case SET_SCORER_ASSIGNED_TEST_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        scorerPaginationPageSize: action.scorerPaginationPageSize
      };
    case SET_SCORER_OLA_SELECTED_TAB:
      return {
        ...state,
        scorerOLASelectedTab: action.scorerOLASelectedTab
      };

    // OLA Unavailability
    // Page State
    case SET_ASSESSORS_OLA_UNAVAILABILITY_PAGINATION_PAGE:
      return {
        ...state,
        assessorsOLAUnavailabilityPaginationPage: action.assessorsOLAUnavailabilityPaginationPage
      };
    // Page Size State
    case SET_ASSESSORS_OLA_UNAVAILABILITY_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        assessorsOLAUnavailabilityPaginationPageSize:
          action.assessorsOLAUnavailabilityPaginationPageSize
      };
    // States
    case SET_ASSESSORS_OLA_UNAVAILABILITY_SEARCH_PARAMETERS:
      return {
        ...state,
        assessorsOLAUnavailabilityKeyword: action.assessorsOLAUnavailabilityKeyword,
        assessorsOLAUnavailabilityActiveSearch: action.assessorsOLAUnavailabilityActiveSearch
      };
    case RERENDER_SCORER_OLA_TABLE:
      return {
        ...state,
        triggerRerenderScorerTables: !state.triggerRerenderScorerTables
      };
    case TOGGLE_SIDEBAR:
      return {
        ...state,
        isSidebarHidden: !state.isSidebarHidden
      };
    case LOAD_SCORES:
      return {
        ...state,
        answerScores: action.scores
      };
    case UPDATE_COMPETENCY:
      const answerScores = [];
      for (const q of action.scorerInfo.questions) {
        for (const c of q.email.competency_types) {
          answerScores.push({ question: q.id, competency: c, rationale: "" });
        }
      }
      return {
        ...state,
        competencyTypes: action.scorerInfo.competency_types,
        questions: action.scorerInfo.questions,
        answerScores: answerScores
      };
    case UPDATE_RATIONALE:
      let newScore = action.obj;
      let found = false;
      const newStateArray = state.answerScores.map(score => {
        if (score.question === newScore.question && score.competency === newScore.competency) {
          found = true;
          newScore = { ...score, ...newScore };
          newScore.rationale = newScore.rationale || "";
          return newScore;
        }
        return score;
      });
      if (!found) {
        newStateArray.push(newScore);
      }
      return {
        ...state,
        answerScores: newStateArray
      };

    case UPDATE_RATINGS:
      const newQuestions = state.questions.map(q => {
        if (action.question.id === q.id) {
          return { ...q, ...action.question };
        }
        return q;
      });
      return {
        ...state,
        questions: newQuestions
      };
    case RESET_SCORER_STATES:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default scorer;
export {
  getAssignedTests,
  getInboxTestSection,
  isUserAssociatedToTestCenter,
  saveScore,
  getScores,
  updateCurrentScorerAssignedTestPageState,
  updateScorerAssignedTestPageSizeState,
  toggleSidebar,
  getScorerOlaTestsToAssessAsSupervisor,
  getScorerOlaTestSessionSkipOptions,
  getHighestPriorityCandidateToAssignAsScorerOla,
  assignCandidateAsScorerOla,
  setScorerOLASelectedTab,
  resetScorerStates,
  getScorerOlaMyAssignedTests,
  getScorerOlaAssignedCandidateDetails,
  getScorerOlaMyAvailabilityData,
  unassignCandidateAsScorerOla,
  triggerRerenderScorerOlaTables,
  getAvailableTestAssessorsAsSupervisor,
  cancelTestSessionAsScorerOla,

  // OLA Unavailability
  updateAssessorsOLAUnavailabilityPageState,
  updateAssessorsOLAUnavailabilityPageSizeState,
  updateAssessorsOLAUnavailabilityStates,
  getAllAssessorsOlaUnavailabilityAsSupervisor,
  getFoundAssessorsOlaUnavailabilityAsSupervisor,
  getAllOlaAssessorsSupervisedByCurrentUser,
  getAllOlaTestCentersForSelectedAssessor,
  saveNewAssessorUnavailability,
  modifyAssessorUnavailability,
  deleteAssessorUnavailability
};
