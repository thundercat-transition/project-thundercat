import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// checking if the candidate is trying to access/book a test within the designated retest period
function isWithinTheRetestPeriod(
  testId,
  testSkillTypeId,
  testSkillSubTypeId,
  testSessionStartTime
) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/is-within-the-retest-period/?test_id=${testId}&test_skill_type_id=${testSkillTypeId}&test_skill_sub_type_id=${testSkillSubTypeId}&test_session_start_time=${testSessionStartTime}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

export default isWithinTheRetestPeriod;
