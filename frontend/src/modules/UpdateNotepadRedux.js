import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// update the notepad in the backened
function updateNotepad(assigned_test_id, notepadContent) {
  const body = {
    assigned_test_id: assigned_test_id,
    notepad_content: notepadContent
  };
  const url = "/oec-cat/api/update-notepad/";

  return async function () {
    const tests = await fetch(url, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify({ ...body })
    });
    return tests;
  };
}

// update the notepad in the backened
function getNotepad(assigned_test_id) {
  const url = `/oec-cat/api/get-notepad?assigned_test_id=${assigned_test_id}`;

  return async function () {
    const tests = await fetch(url, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const response = await tests.json();
    return response;
  };
}

export { updateNotepad, getNotepad };
