import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// creation of virtual meeting link
function createVirtualMeeting(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/create-virtual-meeting", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
}

// get of virtual meeting link
function getVirtualMeetingLink(test_session_id) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-virtual-meeting-link/?test_session_id=${test_session_id}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const { status } = response;
    // otherwise return success + status 200
    const responseJson = await response.json();
    responseJson.status = status;
    return responseJson;
  };
}

// clean-up virtual meeting link
function cleanUpVirtualMeeting(test_session_id) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/clean-up-virtual-meeting/?test_session_id=${test_session_id}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
}

export default createVirtualMeeting;
export { getVirtualMeetingLink, cleanUpVirtualMeeting };
