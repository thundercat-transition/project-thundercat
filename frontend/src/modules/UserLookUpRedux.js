import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action types
// User Look-Up Pagination
export const SET_USER_LOOK_UP_PAGINATION_PAGE = "userLookUp/SET_USER_LOOK_UP_PAGINATION_PAGE";
export const SET_USER_LOOK_UP_PAGINATION_PAGE_SIZE =
  "userLookUp/SET_USER_LOOK_UP_PAGINATION_PAGE_SIZE";
export const SET_USER_LOOK_UP_SEARCH_PARAMETERS = "userLookUp/SET_USER_LOOK_UP_SEARCH_PARAMETERS";
export const SET_USER_LOOK_UP_TOO_MANY_RESULTS_STATE =
  "userLookUp/SET_USER_LOOK_UP_TOO_MANY_RESULTS_STATE";
export const SET_USER_LOOK_UP_TESTS_PAGINATION_PAGE =
  "userLookUp/SET_USER_LOOK_UP_TESTS_PAGINATION_PAGE";
export const SET_USER_LOOK_UP_TESTS_PAGINATION_PAGE_SIZE =
  "userLookUp/SET_USER_LOOK_UP_TESTS_PAGINATION_PAGE_SIZE";
export const SET_USER_LOOK_UP_TESTS_SEARCH_PARAMETERS =
  "userLookUp/SET_USER_LOOK_UP_TESTS_SEARCH_PARAMETERS";
export const SET_SELECTED_USERNAME = "userLookUp/SET_SELECTED_USERNAME";
export const SET_SELECTED_USER_FIRSTNAME = "userLookUp/SET_SELECTED_USER_FIRSTNAME";
export const SET_SELECTED_USER_LASTNAME = "userLookUp/SET_SELECTED_USER_LASTNAME";
export const SET_BUSINESS_OPERATIONS_SIDE_NAV_STATE =
  "userLookUp/SET_BUSINESS_OPERATIONS_SIDE_NAV_STATE";
export const SET_BUSINESS_OPERATIONS_USER_LOOK_UP_SIDE_NAV_STATE =
  "userLookUp/SET_BUSINESS_OPERATIONS_USER_LOOK_UP_SIDE_NAV_STATE";
export const RESET_SELECTED_USER_STATE = "userLookUp/RESET_SELECTED_USER_STATE";
export const RESET_STATE = "userLookUp/RESET_STATE";

// update pagination page state (User Look-up)
const updateUserLookUpPageState = userLookUpPaginationPage => ({
  type: SET_USER_LOOK_UP_PAGINATION_PAGE,
  userLookUpPaginationPage
});
// update pagination pageSize state (User Look-up)
const updateUserLookUpPageSizeState = userLookUpPaginationPageSize => ({
  type: SET_USER_LOOK_UP_PAGINATION_PAGE_SIZE,
  userLookUpPaginationPageSize
});
// update search keyword state (User Look-up)
const updateSearchUserLookUpStates = (userLookUpKeyword, userLookUpActiveSearch) => ({
  type: SET_USER_LOOK_UP_SEARCH_PARAMETERS,
  userLookUpKeyword,
  userLookUpActiveSearch
});
// update too many results state (User Look-up)
const updateTooManyResultsState = userLookUpTooManyResults => ({
  type: SET_USER_LOOK_UP_TOO_MANY_RESULTS_STATE,
  userLookUpTooManyResults
});
// update pagination page state (User Look-up - Tests)
const updateUserLookUpTestsPageState = userLookUpTestsPaginationPage => ({
  type: SET_USER_LOOK_UP_TESTS_PAGINATION_PAGE,
  userLookUpTestsPaginationPage
});
// update pagination pageSize state (User Look-up - Tests)
const updateUserLookUpTestsPageSizeState = userLookUpTestsPaginationPageSize => ({
  type: SET_USER_LOOK_UP_TESTS_PAGINATION_PAGE_SIZE,
  userLookUpTestsPaginationPageSize
});
// update search keyword state (User Look-up - Tests)
const updateSearchUserLookUpTestsStates = (
  userLookUpTestsKeyword,
  userLookUpTestsActiveSearch
) => ({
  type: SET_USER_LOOK_UP_TESTS_SEARCH_PARAMETERS,
  userLookUpTestsKeyword,
  userLookUpTestsActiveSearch
});
// set selected user username
const setSelectedUserId = selectedUserId => ({
  type: SET_SELECTED_USERNAME,
  selectedUserId
});
// set selected user firstname
const setSelectedUserFirstname = selectedUserFirstname => ({
  type: SET_SELECTED_USER_FIRSTNAME,
  selectedUserFirstname
});
// set selected user lastname
const setSelectedUserLastname = selectedUserLastname => ({
  type: SET_SELECTED_USER_LASTNAME,
  selectedUserLastname
});
// set BO user side nav state
const setBOUserSideNavState = selectedSideNavItem => ({
  type: SET_BUSINESS_OPERATIONS_SIDE_NAV_STATE,
  selectedSideNavItem
});
// set BO user look-up side nav state
const setBOUserLookUpSideNavState = selectedUserLookUpSideNavItem => ({
  type: SET_BUSINESS_OPERATIONS_USER_LOOK_UP_SIDE_NAV_STATE,
  selectedUserLookUpSideNavItem
});
const resetUserLookUpSelectedUserState = () => ({ type: RESET_SELECTED_USER_STATE });
const resetUserLookUpState = () => ({ type: RESET_STATE });

// getting list of all users
const getAllUsers = (page, pageSize, maxAllowedResults) => {
  return async function () {
    const tests = await fetch(
      `/oec-cat/api/get-all-users/?page=${page}&page_size=${pageSize}&max_allowed_results=${maxAllowedResults}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const testsJson = await tests.json();
    return testsJson;
  };
};

// getting list of found users
const getAllFoundUsers = (providedKeyword, page, pageSize, maxAllowedResults) => {
  // if provided keyword is empty
  let keyword = providedKeyword;
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-users/?keyword=${keyword}&page=${page}&page_size=${pageSize}&max_allowed_results=${maxAllowedResults}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};

const getSelectedUserPersonalInfo = selectedUserId => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-selected-user-personal-info/?user_id=${selectedUserId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};

// Initial State
const initialState = {
  userLookUpPaginationPage: 1,
  userLookUpPaginationPageSize: 25,
  userLookUpKeyword: null,
  userLookUpTooManyResults: false,
  userLookUpActiveSearch: false,
  userLookUpTestsPaginationPage: 1,
  userLookUpTestsPaginationPageSize: 25,
  userLookUpTestsKeyword: null,
  userLookUpTestsActiveSearch: false,
  selectedUserId: null,
  selectedUserFirstname: null,
  selectedUserLastname: null,
  selectedSideNavItem: 1,
  selectedUserLookUpSideNavItem: 1
};

const userLookUp = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_LOOK_UP_PAGINATION_PAGE:
      return {
        ...state,
        userLookUpPaginationPage: action.userLookUpPaginationPage
      };
    case SET_USER_LOOK_UP_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        userLookUpPaginationPageSize: action.userLookUpPaginationPageSize
      };
    case SET_USER_LOOK_UP_SEARCH_PARAMETERS:
      return {
        ...state,
        userLookUpKeyword: action.userLookUpKeyword,
        userLookUpActiveSearch: action.userLookUpActiveSearch
      };
    case SET_USER_LOOK_UP_TOO_MANY_RESULTS_STATE:
      return {
        ...state,
        userLookUpTooManyResults: action.userLookUpTooManyResults
      };
    case SET_USER_LOOK_UP_TESTS_PAGINATION_PAGE:
      return {
        ...state,
        userLookUpTestsPaginationPage: action.userLookUpTestsPaginationPage
      };
    case SET_USER_LOOK_UP_TESTS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        userLookUpTestsPaginationPageSize: action.userLookUpTestsPaginationPageSize
      };
    case SET_USER_LOOK_UP_TESTS_SEARCH_PARAMETERS:
      return {
        ...state,
        userLookUpTestsKeyword: action.userLookUpTestsKeyword,
        userLookUpTestsActiveSearch: action.userLookUpTestsActiveSearch
      };
    case SET_SELECTED_USERNAME:
      return {
        ...state,
        selectedUserId: action.selectedUserId
      };
    case SET_SELECTED_USER_FIRSTNAME:
      return {
        ...state,
        selectedUserFirstname: action.selectedUserFirstname
      };
    case SET_SELECTED_USER_LASTNAME:
      return {
        ...state,
        selectedUserLastname: action.selectedUserLastname
      };
    case SET_BUSINESS_OPERATIONS_SIDE_NAV_STATE:
      return {
        ...state,
        selectedSideNavItem: action.selectedSideNavItem
      };
    case SET_BUSINESS_OPERATIONS_USER_LOOK_UP_SIDE_NAV_STATE:
      return {
        ...state,
        selectedUserLookUpSideNavItem: action.selectedUserLookUpSideNavItem
      };
    case RESET_SELECTED_USER_STATE:
      // Ensure local storage is cleaned up on logout
      return {
        ...state,
        userLookUpTestsPaginationPage: 1,
        userLookUpTestsPaginationPageSize: 25,
        userLookUpTestsKeyword: null,
        userLookUpTestsActiveSearch: false
      };
    case RESET_STATE:
      // Ensure local storage is cleaned up on logout
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default userLookUp;
export {
  initialState,
  getAllUsers,
  getAllFoundUsers,
  getSelectedUserPersonalInfo,
  updateUserLookUpPageState,
  updateTooManyResultsState,
  updateUserLookUpPageSizeState,
  updateSearchUserLookUpStates,
  updateUserLookUpTestsPageState,
  updateUserLookUpTestsPageSizeState,
  updateSearchUserLookUpTestsStates,
  setSelectedUserId,
  setSelectedUserFirstname,
  setSelectedUserLastname,
  resetUserLookUpState,
  setBOUserSideNavState,
  setBOUserLookUpSideNavState,
  resetUserLookUpSelectedUserState
};
