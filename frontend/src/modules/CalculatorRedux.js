// Action Types
const TOGGLE_CALCULATOR = "calculator/TOGGLE_CALCULATOR";
const TRIGGER_RERENDER = "calculator/TRIGGER_RERENDER";
const RESET_STATE = "calculator/RESET_STATE";

// Action Creators
const toggleCalculator = () => ({
  type: TOGGLE_CALCULATOR
});
const triggerRerender = () => ({
  type: TRIGGER_RERENDER
});
const resetCalculatorState = () => ({ type: RESET_STATE });

// Initial State
const initialState = {
  isCalculatorHidden: false,
  triggerRerender: false
};

// Reducer
const calculator = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_CALCULATOR:
      return {
        ...state,
        isCalculatorHidden: !state.isCalculatorHidden
      };
    case TRIGGER_RERENDER:
      return {
        ...state,
        triggerRerender: !state.triggerRerender
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default calculator;
export { resetCalculatorState, toggleCalculator, triggerRerender };
