import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// getting remaining time of a specific accommodation break bank
function getBreakBankRemainingTime(accommodationRequestId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-break-bank-remaining-time/?accommodation_request_id=${accommodationRequestId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// handling trigger break bank action (PAUSE/UNPAUSE)
const triggerBreakBankAction = data => {
  return async function () {
    const response = await fetch(`/oec-cat/api/trigger-break-bank-action/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
};

export default getBreakBankRemainingTime;
export { triggerBreakBankAction };
