import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action types
export const SET_BILLING_CONTACTS_PAGINATION_PAGE =
  "billingContact/SET_BILLING_CONTACTS_PAGINATION_PAGE";
export const SET_BILLING_CONTACTS_PAGINATION_PAGE_SIZE =
  "billingContact/SET_BILLING_CONTACTS_PAGINATION_PAGE_SIZE";
export const SET_BILLING_CONTACTS_SEARCH_PARAMETERS =
  "billingContact/SET_BILLING_CONTACTS_SEARCH_PARAMETERS";
const TRIGGER_TABLE_UPDATES = "billingContact/TRIGGER_TABLE_UPDATES";
const TRIGGER_RERENDER = "billingContact/TRIGGER_RERENDER";

const updateCurrentBillingContactsPageState = billingContactsPaginationPage => ({
  type: SET_BILLING_CONTACTS_PAGINATION_PAGE,
  billingContactsPaginationPage
});

const updateBillingContactsPageSizeState = billingContactsPaginationPageSize => ({
  type: SET_BILLING_CONTACTS_PAGINATION_PAGE_SIZE,
  billingContactsPaginationPageSize
});

const updateSearchBillingContactsStates = (
  billingContactsKeyword,
  billingContactsActiveSearch
) => ({
  type: SET_BILLING_CONTACTS_SEARCH_PARAMETERS,
  billingContactsKeyword,
  billingContactsActiveSearch
});

const triggerTableUpdates = () => ({
  type: TRIGGER_TABLE_UPDATES
});

const triggerRerender = () => ({
  type: TRIGGER_RERENDER
});

// creating new billing contact
function createBillingContact(data) {
  return async function () {
    try {
      const response = await fetch("/oec-cat/api/create-billing-contact", {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        },
        body: JSON.stringify(data)
      });
      return response;
    } catch (error) {
      return error;
    }
  };
}

// modify billing contact
function modifyBillingContact(data) {
  return async function () {
    try {
      const response = await fetch("/oec-cat/api/modify-billing-contact", {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        },
        body: JSON.stringify(data)
      });
      return response;
    } catch (error) {
      return error;
    }
  };
}

// delete billing contact
function deleteBillingContact(id) {
  return async function () {
    try {
      const response = await fetch(`/oec-cat/api/delete-billing-contact/?id=${id}`, {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      });
      return response;
    } catch (error) {
      return error;
    }
  };
}

// get all billing contacts for current user
function getAllBillingContacts(page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-billing-contacts/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get all found billing contacts for current user
function getFoundBillingContacts(providedKeyword, currentLanguage, page, pageSize) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-billing-contacts/?keyword=${keyword}&current_language=${currentLanguage}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// Initial State
const initialState = {
  billingContactsPaginationPage: 1,
  billingContactsPaginationPageSize: 25,
  billingContactsKeyword: "",
  billingContactsActiveSearch: false,
  triggerTableUpdates: false,
  triggerRerender: false,
  billingContactsData: {}
};

const billingContact = (state = initialState, action) => {
  switch (action.type) {
    case SET_BILLING_CONTACTS_PAGINATION_PAGE:
      return {
        ...state,
        billingContactsPaginationPage: action.billingContactsPaginationPage
      };
    case SET_BILLING_CONTACTS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        billingContactsPaginationPageSize: action.billingContactsPaginationPageSize
      };
    case SET_BILLING_CONTACTS_SEARCH_PARAMETERS:
      return {
        ...state,
        billingContactsKeyword: action.billingContactsKeyword,
        billingContactsActiveSearch: action.billingContactsActiveSearch
      };
    case TRIGGER_TABLE_UPDATES:
      return {
        ...state,
        triggerTableUpdates: !state.triggerTableUpdates
      };
    case TRIGGER_RERENDER:
      return {
        ...state,
        triggerRerender: !state.triggerRerender
      };
    default:
      return state;
  }
};

export default billingContact;
export {
  initialState,
  triggerTableUpdates,
  triggerRerender,
  updateCurrentBillingContactsPageState,
  updateBillingContactsPageSizeState,
  updateSearchBillingContactsStates,
  createBillingContact,
  modifyBillingContact,
  deleteBillingContact,
  getAllBillingContacts,
  getFoundBillingContacts
};
