import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
export const SET_UIT_ASSIGNED_TEST_ID = "assigned_tests/SET_UIT_ASSIGNED_TEST_ID";
export const RESET_STATE = "assigned_tests/RESET_STATE";

// update UIT assigned test ID
const updateAssignedTestId = (
  assignedTestId,
  testId,
  accommodationRequestId,
  userAccommodationFileId
) => ({
  type: SET_UIT_ASSIGNED_TEST_ID,
  assignedTestId,
  testId,
  accommodationRequestId,
  userAccommodationFileId
});

const resetAssignedTestState = () => ({
  type: RESET_STATE
});

// getting the assigned test of the specified user
const getAssignedTests = (currentLanguage, assignedTestId) => {
  let endpoint = "";
  if (assignedTestId) {
    endpoint = `/oec-cat/api/assigned-tests/?current_language=${currentLanguage}&assigned_test_id=${assignedTestId}`;
  } else {
    endpoint = `/oec-cat/api/assigned-tests/?current_language=${currentLanguage}`;
  }
  return async function () {
    const tests = await fetch(endpoint, {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const testsJson = await tests.json();
    return testsJson;
  };
};

const getAssignedVirtualTests = () => {
  return async function () {
    const tests = await fetch("/oec-cat/api/assigned-virtual-tests/", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const testsJson = await tests.json();
    return testsJson;
  };
};

// getting the UIT assigned test of the specified user
const getPublicTests = () => {
  return async function () {
    const response = await fetch("/oec-cat/api/public-tests/", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
};

const addAttendee = (ta_user_id, test_session_id) => {
  const body = { ta_user_id: ta_user_id, test_session_id: test_session_id };
  return async function () {
    const response = await fetch("/oec-cat/api/add-attendee", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(body)
    });
    const responseJson = await response.json();
    return responseJson;
  };
};

const removeAttendee = (ta_user_id, test_session_id) => {
  const body = { ta_user_id: ta_user_id, test_session_id: test_session_id };
  return async function () {
    const response = await fetch("/oec-cat/api/remove-attendee", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(body)
    });
    const responseJson = await response.json();
    return responseJson;
  };
};

// Initial State
const initialState = {
  assignedTestId: null,
  testId: null,
  accommodationRequestId: null,
  userAccommodationFileId: null
};

// Reducer
const assignedTest = (state = initialState, action) => {
  switch (action.type) {
    case SET_UIT_ASSIGNED_TEST_ID:
      return {
        ...state,
        assignedTestId: action.assignedTestId,
        testId: action.testId,
        accommodationRequestId: action.accommodationRequestId,
        userAccommodationFileId: action.userAccommodationFileId
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default assignedTest;
export {
  getAssignedTests,
  getAssignedVirtualTests,
  getPublicTests,
  updateAssignedTestId,
  resetAssignedTestState,
  addAttendee,
  removeAttendee
};
