// Action Types
export const SET_CONSULTATION_SERVICES_OPEN_REQUESTS_PAGINATION_PAGE =
  "testCenter/SET_CONSULTATION_SERVICES_OPEN_REQUESTS_PAGINATION_PAGE";
export const SET_CONSULTATION_SERVICES_OPEN_REQUESTS_PAGINATION_PAGE_SIZE =
  "testCenter/SET_CONSULTATION_SERVICES_OPEN_REQUESTS_PAGINATION_PAGE_SIZE";
export const SET_CONSULTATION_SERVICES_OPEN_REQUESTS_SEARCH_PARAMETERS =
  "testCenter/SET_CONSULTATION_SERVICES_OPEN_REQUESTS_SEARCH_PARAMETERS";
export const SET_CONSULTATION_SERVICES_COMPLETED_REQUESTS_PAGINATION_PAGE =
  "testCenter/SET_CONSULTATION_SERVICES_COMPLETED_REQUESTS_PAGINATION_PAGE";
export const SET_CONSULTATION_SERVICES_COMPLETED_REQUESTS_PAGINATION_PAGE_SIZE =
  "testCenter/SET_CONSULTATION_SERVICES_COMPLETED_REQUESTS_PAGINATION_PAGE_SIZE";
export const SET_CONSULTATION_SERVICES_COMPLETED_REQUESTS_SEARCH_PARAMETERS =
  "testCenter/SET_CONSULTATION_SERVICES_COMPLETED_REQUESTS_SEARCH_PARAMETERS";
export const RESET_CONSULTATION_SERVICES_STATES = "testCenter/RESET_CONSULTATION_SERVICES_STATES";

const updateCurrentConsultationServicesOpenRequestsPageState =
  consultationServicesOpenRequestsPaginationPage => ({
    type: SET_CONSULTATION_SERVICES_OPEN_REQUESTS_PAGINATION_PAGE,
    consultationServicesOpenRequestsPaginationPage
  });

const updateConsultationServicesOpenRequestsPageSizeState =
  consultationServicesOpenRequestsPaginationPageSize => ({
    type: SET_CONSULTATION_SERVICES_OPEN_REQUESTS_PAGINATION_PAGE_SIZE,
    consultationServicesOpenRequestsPaginationPageSize
  });

const updateSearchConsultationServicesOpenRequestsStates = (
  consultationServicesOpenRequestsKeyword,
  consultationServicesOpenRequestsActiveSearch
) => ({
  type: SET_CONSULTATION_SERVICES_OPEN_REQUESTS_SEARCH_PARAMETERS,
  consultationServicesOpenRequestsKeyword,
  consultationServicesOpenRequestsActiveSearch
});
const updateCurrentConsultationServicesCompletedRequestsPageState =
  consultationServicesCompletedRequestsPaginationPage => ({
    type: SET_CONSULTATION_SERVICES_COMPLETED_REQUESTS_PAGINATION_PAGE,
    consultationServicesCompletedRequestsPaginationPage
  });

const updateConsultationServicesCompletedRequestsPageSizeState =
  consultationServicesCompletedRequestsPaginationPageSize => ({
    type: SET_CONSULTATION_SERVICES_COMPLETED_REQUESTS_PAGINATION_PAGE_SIZE,
    consultationServicesCompletedRequestsPaginationPageSize
  });

const updateSearchConsultationServicesCompletedRequestsStates = (
  consultationServicesCompletedRequestsKeyword,
  consultationServicesCompletedRequestsActiveSearch
) => ({
  type: SET_CONSULTATION_SERVICES_COMPLETED_REQUESTS_SEARCH_PARAMETERS,
  consultationServicesCompletedRequestsKeyword,
  consultationServicesCompletedRequestsActiveSearch
});

const resetConsultationServicesStates = () => ({
  type: RESET_CONSULTATION_SERVICES_STATES
});

// Initial State
const initialState = {
  consultationServicesOpenRequestsPaginationPage: 1,
  consultationServicesOpenRequestsPaginationPageSize: 25,
  consultationServicesOpenRequestsKeyword: "",
  consultationServicesOpenRequestsActiveSearch: false,
  consultationServicesCompletedRequestsPaginationPage: 1,
  consultationServicesCompletedRequestsPaginationPageSize: 25,
  consultationServicesCompletedRequestsKeyword: "",
  consultationServicesCompletedRequestsActiveSearch: false
};

// Reducer
const consultationServices = (state = initialState, action) => {
  switch (action.type) {
    case SET_CONSULTATION_SERVICES_OPEN_REQUESTS_PAGINATION_PAGE:
      return {
        ...state,
        consultationServicesOpenRequestsPaginationPage:
          action.consultationServicesOpenRequestsPaginationPage
      };
    case SET_CONSULTATION_SERVICES_OPEN_REQUESTS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        consultationServicesOpenRequestsPaginationPageSize:
          action.consultationServicesOpenRequestsPaginationPageSize
      };
    case SET_CONSULTATION_SERVICES_OPEN_REQUESTS_SEARCH_PARAMETERS:
      return {
        ...state,
        consultationServicesOpenRequestsKeyword: action.consultationServicesOpenRequestsKeyword,
        consultationServicesOpenRequestsActiveSearch:
          action.consultationServicesOpenRequestsActiveSearch
      };
    case SET_CONSULTATION_SERVICES_COMPLETED_REQUESTS_PAGINATION_PAGE:
      return {
        ...state,
        consultationServicesCompletedRequestsPaginationPage:
          action.consultationServicesCompletedRequestsPaginationPage
      };
    case SET_CONSULTATION_SERVICES_COMPLETED_REQUESTS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        consultationServicesCompletedRequestsPaginationPageSize:
          action.consultationServicesCompletedRequestsPaginationPageSize
      };
    case SET_CONSULTATION_SERVICES_COMPLETED_REQUESTS_SEARCH_PARAMETERS:
      return {
        ...state,
        consultationServicesCompletedRequestsKeyword:
          action.consultationServicesCompletedRequestsKeyword,
        consultationServicesCompletedRequestsActiveSearch:
          action.consultationServicesCompletedRequestsActiveSearch
      };
    case RESET_CONSULTATION_SERVICES_STATES:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default consultationServices;
export {
  updateCurrentConsultationServicesOpenRequestsPageState,
  updateConsultationServicesOpenRequestsPageSizeState,
  updateSearchConsultationServicesOpenRequestsStates,
  updateCurrentConsultationServicesCompletedRequestsPageState,
  updateConsultationServicesCompletedRequestsPageSizeState,
  updateSearchConsultationServicesCompletedRequestsStates,
  resetConsultationServicesStates
};
