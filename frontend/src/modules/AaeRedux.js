import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
export const SET_AAE_SELECTED_SIDE_NAV = "testCenter/SET_AAE_SELECTED_SIDE_NAV";
export const SET_AAE_REQUESTS_SELECTED_TAB = "testCenter/SET_AAE_REQUESTS_SELECTED_TAB";
export const SET_AAE_OPEN_REQUESTS_PAGINATION_PAGE =
  "testCenter/SET_AAE_OPEN_REQUESTS_PAGINATION_PAGE";
export const SET_AAE_OPEN_REQUESTS_PAGINATION_PAGE_SIZE =
  "testCenter/SET_AAE_OPEN_REQUESTS_PAGINATION_PAGE_SIZE";
export const SET_AAE_OPEN_REQUESTS_SEARCH_PARAMETERS =
  "testCenter/SET_AAE_OPEN_REQUESTS_SEARCH_PARAMETERS";
export const SET_AAE_COMPLETED_REQUESTS_PAGINATION_PAGE =
  "testCenter/SET_AAE_COMPLETED_REQUESTS_PAGINATION_PAGE";
export const SET_AAE_COMPLETED_REQUESTS_PAGINATION_PAGE_SIZE =
  "testCenter/SET_AAE_COMPLETED_REQUESTS_PAGINATION_PAGE_SIZE";
export const SET_AAE_COMPLETED_REQUESTS_SEARCH_PARAMETERS =
  "testCenter/SET_AAE_COMPLETED_REQUESTS_SEARCH_PARAMETERS";
export const SET_AAE_INITIAL_SELECTED_USER_ACCOMMODATION_FILE_DATA =
  "testCenter/SET_AAE_INITIAL_SELECTED_USER_ACCOMMODATION_FILE_DATA";
export const SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_DATA =
  "testCenter/SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_DATA";
export const SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_DEFAULT_TEST_TIME =
  "testCenter/SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_DEFAULT_TEST_TIME";
export const SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_TEST_TO_ADMINISTER_VALID_STATE =
  "testCenter/SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_TEST_TO_ADMINISTER_VALID_STATE";
export const SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_TEST_CENTER_VALID_STATE =
  "testCenter/SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_TEST_CENTER_VALID_STATE";
export const SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_BREAK_AND_TIME_ADJUSTMENT_VALID_STATE =
  "testCenter/SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_BREAK_AND_TIME_ADJUSTMENT_VALID_STATE";
export const SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_FIRST_COLUMN_TAB =
  "testCenter/SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_FIRST_COLUMN_TAB";
export const SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_SECOND_COLUMN_TAB =
  "testCenter/SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_SECOND_COLUMN_TAB";
export const SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_SIDE_NAVIGATION_SELECTED_ITEM =
  "testCenter/SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_SIDE_NAVIGATION_SELECTED_ITEM";
export const TRIGGER_AAE_RENRENDER = "testCenter/TRIGGER_AAE_RENRENDER";
export const RESET_AAE_SELECTED_USER_ACCOMMODATION_FILE_DATA_STATE =
  "testCenter/RESET_AAE_SELECTED_USER_ACCOMMODATION_FILE_DATA_STATE";
export const RESET_AAE_STATES = "testCenter/RESET_AAE_STATES";

const setAaeSelectedSideNav = aaeSelectedSideNav => ({
  type: SET_AAE_SELECTED_SIDE_NAV,
  aaeSelectedSideNav
});

const setAaeRequestsSelectedTab = aaeRequestsSelectedTab => ({
  type: SET_AAE_REQUESTS_SELECTED_TAB,
  aaeRequestsSelectedTab
});

const updateCurrentAaeOpenRequestsPageState = aaeOpenRequestsPaginationPage => ({
  type: SET_AAE_OPEN_REQUESTS_PAGINATION_PAGE,
  aaeOpenRequestsPaginationPage
});

const updateAaeOpenRequestsPageSizeState = aaeOpenRequestsPaginationPageSize => ({
  type: SET_AAE_OPEN_REQUESTS_PAGINATION_PAGE_SIZE,
  aaeOpenRequestsPaginationPageSize
});

const updateSearchAaeOpenRequestsStates = (
  aaeOpenRequestsKeyword,
  aaeOpenRequestsActiveSearch
) => ({
  type: SET_AAE_OPEN_REQUESTS_SEARCH_PARAMETERS,
  aaeOpenRequestsKeyword,
  aaeOpenRequestsActiveSearch
});
const updateCurrentAaeCompletedRequestsPageState = aaeCompletedRequestsPaginationPage => ({
  type: SET_AAE_COMPLETED_REQUESTS_PAGINATION_PAGE,
  aaeCompletedRequestsPaginationPage
});

const updateAaeCompletedRequestsPageSizeState = aaeCompletedRequestsPaginationPageSize => ({
  type: SET_AAE_COMPLETED_REQUESTS_PAGINATION_PAGE_SIZE,
  aaeCompletedRequestsPaginationPageSize
});

const updateSearchAaeCompletedRequestsStates = (
  aaeCompletedRequestsKeyword,
  aaeCompletedRequestsActiveSearch
) => ({
  type: SET_AAE_COMPLETED_REQUESTS_SEARCH_PARAMETERS,
  aaeCompletedRequestsKeyword,
  aaeCompletedRequestsActiveSearch
});

const setAaeInitialSelectedUserAccommodationFileData =
  aaeInitialSelectedUserAccommodationFileData => ({
    type: SET_AAE_INITIAL_SELECTED_USER_ACCOMMODATION_FILE_DATA,
    aaeInitialSelectedUserAccommodationFileData
  });

const setAaeSelectedUserAccommodationFileData = aaeSelectedUserAccommodationFileData => ({
  type: SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_DATA,
  aaeSelectedUserAccommodationFileData
});

const setAaeSelectedUserAccommodationFileDefaultTestTime =
  aaeSelectedUserAccommodationFileDefaultTestTime => ({
    type: SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_DEFAULT_TEST_TIME,
    aaeSelectedUserAccommodationFileDefaultTestTime
  });

const setAaeSelectedUserAccommodationFileTestToAdministerValidState =
  aaeSelectedUserAccommodationFileTestToAdministerValidState => ({
    type: SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_TEST_TO_ADMINISTER_VALID_STATE,
    aaeSelectedUserAccommodationFileTestToAdministerValidState
  });

const setAaeSelectedUserAccommodationFileTestCenterValidState =
  aaeSelectedUserAccommodationFileTestCenterValidState => ({
    type: SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_TEST_CENTER_VALID_STATE,
    aaeSelectedUserAccommodationFileTestCenterValidState
  });

const setAaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState =
  aaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState => ({
    type: SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_BREAK_AND_TIME_ADJUSTMENT_VALID_STATE,
    aaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState
  });

const resetAaeSelectedUserAccommodationFileDataState = () => ({
  type: RESET_AAE_SELECTED_USER_ACCOMMODATION_FILE_DATA_STATE
});

const setAaeSelectedUserAccommodationFileFirstColumnTab =
  aaeSelectedUserAccommodationFileFirstColumnTab => ({
    type: SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_FIRST_COLUMN_TAB,
    aaeSelectedUserAccommodationFileFirstColumnTab
  });

const setAaeSelectedUserAccommodationFileSecondColumnTab =
  aaeSelectedUserAccommodationFileSecondColumnTab => ({
    type: SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_SECOND_COLUMN_TAB,
    aaeSelectedUserAccommodationFileSecondColumnTab
  });

const setAaeSelectedUserAccommodationFileSideNavigationSelectedItem =
  aaeSelectedUserAccommodationFileSideNavigationSelectedItem => ({
    type: SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_SIDE_NAVIGATION_SELECTED_ITEM,
    aaeSelectedUserAccommodationFileSideNavigationSelectedItem
  });

const triggerAaeRerender = triggerAaeRerender => ({
  type: TRIGGER_AAE_RENRENDER,
  triggerAaeRerender
});

const resetAaeStates = () => ({
  type: RESET_AAE_STATES
});

// getting test centers (for user accommodation request)
function getTestCentersForUserAccommodationRequest(testCenterId = null) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-test-centers-for-user-accommodation-request/?test_center_id=${testCenterId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

// sending accommodation request
function sendUserAccommodationRequest(body) {
  return async function () {
    try {
      const response = await fetch("/oec-cat/api/send-user-accommodation-request", {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        },
        body: JSON.stringify(body)
      });
      return response;
    } catch (error) {
      return error;
    }
  };
}

// checking if there is an ongoing user accommodation request process (non-completed request)
function checkForOngoingUserAccommodationRequestProcess(assignedTestId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/check-for-ongoing-user-accommodation-request-process/?assigned_test_id=${assignedTestId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
}

// getting all user accommodation files (specify if open or not)
function getAllUserAccommodationFiles(page, pageSize, isOpen) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-user-accommodation-files/?page=${page}&page_size=${pageSize}&is_open=${isOpen}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
}

// getting found user accommodation files (specify if open or not)
function getFoundUserAccommodationFiles(providedKeyword, currentLanguage, page, pageSize, isOpen) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-user-accommodation-files/?keyword=${keyword}&current_language=${currentLanguage}&page=${page}&page_size=${pageSize}&is_open=${isOpen}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
}

// getting selected user accommodation file data
function getSelectedUserAccommodationFileData(userAccommodationFileId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-selected-user-accommodation-file-data/?user_accommodation_file_id=${userAccommodationFileId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
}

// getting accommodation file data as a candidate (limited data only)
function getAccommodationFileDataForDetailsPopup(
  userAccommodationFileId,
  getHistoricalData,
  historyId,
  providedUserId
) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-accommodation-file-data-for-details-popup/?user_accommodation_file_id=${userAccommodationFileId}&get_historical_data=${getHistoricalData}&history_id=${historyId}&provided_user_id=${providedUserId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
}

// getting accommodation file available reports
function getAccommodationFileAvailableReports(userAccommodationFileId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-accommodation-file-available-reports/?user_accommodation_file_id=${userAccommodationFileId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
}

// creating user accommodation file comment
function createUserAccommodationFileComment(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/create-user-accommodation-file-comment", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// deleting user accommodation file comment
function deleteUserAccommodationFileComment(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/delete-user-accommodation-file-comment", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting available tests to administer for the user accommodation file
function getAvailableTestsToAdministerForUserAccommodationFile(
  testSkillTypeId,
  testSkillSubTypeId
) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-available-tests-to-administer-for-selected-user-accommodation-file/?test_skill_type_id=${testSkillTypeId}&test_skill_sub_type_id=${testSkillSubTypeId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
}

// getting user accommodation file status options
function getUserAccommodationFileStatusOptions() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-user-accommodation-file-status-options", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseObj = await response.json();
    return responseObj;
  };
}

// getting user accommodation file complexity options
function getUserAccommodationFileComplexityOptions() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-user-accommodation-file-complexity-options", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseObj = await response.json();
    return responseObj;
  };
}

// saving selected user accommodation file data
function saveSelectedUserAccommodationFileData(data) {
  return async function () {
    const response = await fetch("/oec-cat/api/save-selected-user-accommodation-file-data", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// triggering selected user accommodation file data action (REJECT/CANCEL OR COMPLETE)
function triggerSelectedUserAccommodationFileDataAction(data) {
  return async function () {
    const response = await fetch(
      "/oec-cat/api/trigger-selected-user-accommodation-file-data-action",
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        },
        body: JSON.stringify(data)
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// triggering user accommodation file status update as a candidate
function triggerUserAccommodationFileStatusUpdateAsACandidate(data) {
  return async function () {
    const response = await fetch(
      "/oec-cat/api/trigger-user-accommodation-file-status-update-as-a-candidate",
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        },
        body: JSON.stringify(data)
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting pending accommodation files status options
function getAllPendingAccommodationFilesAsATcm(page, pageSize, testCenterId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-pending-user-accommodation-files-as-a-tcm/?page=${page}&page_size=${pageSize}&test_center_id=${testCenterId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
}

// getting found pending accommodation files status options
function getFoundPendingAccommodationFilesAsATcm(
  providedKeyword,
  currentLanguage,
  page,
  pageSize,
  testCenterId
) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-pending-user-accommodation-files-as-a-tcm/?keyword=${keyword}&current_language=${currentLanguage}&page=${page}&page_size=${pageSize}&test_center_id=${testCenterId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
}

// getting number of requests received (AAE Report Data)
function getNumberOfRequestsReceivedAaeReportData(dateFrom, dateTo) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-number-of-requests-received-aae-report-data/?date_from=${dateFrom}&date_to=${dateTo}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
}

// getting number of closed requests (AAE Report Data)
function getNumberOfClosedRequestsAaeReportData(dateFrom, dateTo) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-number-of-closed-requests-aae-report-data/?date_from=${dateFrom}&date_to=${dateTo}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = await response.json();
    return responseObj;
  };
}

// Initial State
const initialState = {
  aaeSelectedSideNav: 0,
  aaeRequestsSelectedTab: "",
  aaeOpenRequestsPaginationPage: 1,
  aaeOpenRequestsPaginationPageSize: 25,
  aaeOpenRequestsKeyword: "",
  aaeOpenRequestsActiveSearch: false,
  aaeCompletedRequestsPaginationPage: 1,
  aaeCompletedRequestsPaginationPageSize: 25,
  aaeCompletedRequestsKeyword: "",
  aaeCompletedRequestsActiveSearch: false,
  aaeInitialSelectedUserAccommodationFileData: {},
  aaeSelectedUserAccommodationFileData: {},
  aaeSelectedUserAccommodationFileDefaultTestTime: null,
  aaeSelectedUserAccommodationFileTestToAdministerValidState: true,
  aaeSelectedUserAccommodationFileTestCenterValidState: true,
  aaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState: true,
  aaeSelectedUserAccommodationFileFirstColumnTab: "",
  aaeSelectedUserAccommodationFileSecondColumnTab: "",
  aaeSelectedUserAccommodationFileSideNavigationSelectedItem: "",
  triggerAaeRerender: false
};

// Reducer
const aae = (state = initialState, action) => {
  switch (action.type) {
    case SET_AAE_SELECTED_SIDE_NAV:
      return {
        ...state,
        aaeSelectedSideNav: action.aaeSelectedSideNav
      };
    case SET_AAE_REQUESTS_SELECTED_TAB:
      return {
        ...state,
        aaeRequestsSelectedTab: action.aaeRequestsSelectedTab
      };
    case SET_AAE_OPEN_REQUESTS_PAGINATION_PAGE:
      return {
        ...state,
        aaeOpenRequestsPaginationPage: action.aaeOpenRequestsPaginationPage
      };
    case SET_AAE_OPEN_REQUESTS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        aaeOpenRequestsPaginationPageSize: action.aaeOpenRequestsPaginationPageSize
      };
    case SET_AAE_OPEN_REQUESTS_SEARCH_PARAMETERS:
      return {
        ...state,
        aaeOpenRequestsKeyword: action.aaeOpenRequestsKeyword,
        aaeOpenRequestsActiveSearch: action.aaeOpenRequestsActiveSearch
      };
    case SET_AAE_COMPLETED_REQUESTS_PAGINATION_PAGE:
      return {
        ...state,
        aaeCompletedRequestsPaginationPage: action.aaeCompletedRequestsPaginationPage
      };
    case SET_AAE_COMPLETED_REQUESTS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        aaeCompletedRequestsPaginationPageSize: action.aaeCompletedRequestsPaginationPageSize
      };
    case SET_AAE_COMPLETED_REQUESTS_SEARCH_PARAMETERS:
      return {
        ...state,
        aaeCompletedRequestsKeyword: action.aaeCompletedRequestsKeyword,
        aaeCompletedRequestsActiveSearch: action.aaeCompletedRequestsActiveSearch
      };
    case SET_AAE_INITIAL_SELECTED_USER_ACCOMMODATION_FILE_DATA:
      return {
        ...state,
        aaeInitialSelectedUserAccommodationFileData:
          action.aaeInitialSelectedUserAccommodationFileData
      };
    case SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_DATA:
      return {
        ...state,
        aaeSelectedUserAccommodationFileData: action.aaeSelectedUserAccommodationFileData
      };
    case SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_DEFAULT_TEST_TIME:
      return {
        ...state,
        aaeSelectedUserAccommodationFileDefaultTestTime:
          action.aaeSelectedUserAccommodationFileDefaultTestTime
      };
    case SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_TEST_TO_ADMINISTER_VALID_STATE:
      return {
        ...state,
        aaeSelectedUserAccommodationFileTestToAdministerValidState:
          action.aaeSelectedUserAccommodationFileTestToAdministerValidState
      };
    case SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_TEST_CENTER_VALID_STATE:
      return {
        ...state,
        aaeSelectedUserAccommodationFileTestCenterValidState:
          action.aaeSelectedUserAccommodationFileTestCenterValidState
      };
    case SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_BREAK_AND_TIME_ADJUSTMENT_VALID_STATE:
      return {
        ...state,
        aaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState:
          action.aaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState
      };
    case SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_FIRST_COLUMN_TAB:
      return {
        ...state,
        aaeSelectedUserAccommodationFileFirstColumnTab:
          action.aaeSelectedUserAccommodationFileFirstColumnTab
      };
    case SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_SECOND_COLUMN_TAB:
      return {
        ...state,
        aaeSelectedUserAccommodationFileSecondColumnTab:
          action.aaeSelectedUserAccommodationFileSecondColumnTab
      };
    case SET_AAE_SELECTED_USER_ACCOMMODATION_FILE_SIDE_NAVIGATION_SELECTED_ITEM:
      return {
        ...state,
        aaeSelectedUserAccommodationFileSideNavigationSelectedItem:
          action.aaeSelectedUserAccommodationFileSideNavigationSelectedItem
      };
    case TRIGGER_AAE_RENRENDER:
      return {
        ...state,
        triggerAaeRerender: !state.triggerAaeRerender
      };
    case RESET_AAE_SELECTED_USER_ACCOMMODATION_FILE_DATA_STATE:
      return {
        ...state,
        aaeInitialSelectedUserAccommodationFileData: {},
        aaeSelectedUserAccommodationFileData: {},
        aaeSelectedUserAccommodationFileFirstColumnTab: "",
        aaeSelectedUserAccommodationFileSecondColumnTab: "",
        aaeSelectedUserAccommodationFileSideNavigationSelectedItem: ""
      };
    case RESET_AAE_STATES:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default aae;
export {
  setAaeSelectedSideNav,
  setAaeRequestsSelectedTab,
  updateCurrentAaeOpenRequestsPageState,
  updateAaeOpenRequestsPageSizeState,
  updateSearchAaeOpenRequestsStates,
  updateCurrentAaeCompletedRequestsPageState,
  updateAaeCompletedRequestsPageSizeState,
  updateSearchAaeCompletedRequestsStates,
  resetAaeStates,
  getTestCentersForUserAccommodationRequest,
  sendUserAccommodationRequest,
  checkForOngoingUserAccommodationRequestProcess,
  getAllUserAccommodationFiles,
  getFoundUserAccommodationFiles,
  getSelectedUserAccommodationFileData,
  setAaeSelectedUserAccommodationFileData,
  setAaeSelectedUserAccommodationFileFirstColumnTab,
  setAaeSelectedUserAccommodationFileSecondColumnTab,
  setAaeSelectedUserAccommodationFileSideNavigationSelectedItem,
  setAaeInitialSelectedUserAccommodationFileData,
  resetAaeSelectedUserAccommodationFileDataState,
  createUserAccommodationFileComment,
  deleteUserAccommodationFileComment,
  getAvailableTestsToAdministerForUserAccommodationFile,
  setAaeSelectedUserAccommodationFileTestToAdministerValidState,
  setAaeSelectedUserAccommodationFileTestCenterValidState,
  setAaeSelectedUserAccommodationFileBreakAndTimeAdjustmentValidState,
  saveSelectedUserAccommodationFileData,
  getAccommodationFileDataForDetailsPopup,
  getAccommodationFileAvailableReports,
  setAaeSelectedUserAccommodationFileDefaultTestTime,
  getUserAccommodationFileStatusOptions,
  getUserAccommodationFileComplexityOptions,
  triggerSelectedUserAccommodationFileDataAction,
  triggerUserAccommodationFileStatusUpdateAsACandidate,
  getAllPendingAccommodationFilesAsATcm,
  getFoundPendingAccommodationFilesAsATcm,
  triggerAaeRerender,
  getNumberOfRequestsReceivedAaeReportData,
  getNumberOfClosedRequestsAaeReportData
};
