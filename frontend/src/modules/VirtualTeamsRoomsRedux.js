// Action Types
const CHANGE_PAGE = "virtualTeamsRooms/CHANGE_PAGE";
const CHANGE_PAGE_SIZE = "virtualTeamsRooms/CHANGE_PAGE_SIZE";
const RESET_STATE = "virtualTeamsRooms/RESET_STATE";

// Action Creators
const changePage = page => ({ type: CHANGE_PAGE, page });
const changePageSize = pageSize => ({ type: CHANGE_PAGE_SIZE, pageSize });
const resetTestBuilderState = () => ({
  type: RESET_STATE
});

export const emptyState = {
  page: 1,
  pageSize: 25
};

// Reducer
const virtualTeamsRooms = (state = emptyState, action) => {
  switch (action.type) {
    case CHANGE_PAGE:
      return {
        ...state,
        page: action.page
      };
    case CHANGE_PAGE_SIZE:
      return {
        ...state,
        pageSize: action.pageSize
      };
    case RESET_STATE:
      return {
        ...emptyState
      };

    default:
      return {
        ...state
      };
  }
};

export default virtualTeamsRooms;

export { changePage, changePageSize, resetTestBuilderState };
