import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
export const SET_ASSESSMENT_PROCESS_DATA_ASSIGN_CANDIDATES =
  "assessment_process/SET_ASSESSMENT_PROCESS_DATA_ASSIGN_CANDIDATES";
export const SET_ASSESSMENT_PROCESS_DATA_ACTIVE_PROCESSES =
  "assessment_process/SET_ASSESSMENT_PROCESS_DATA_ACTIVE_PROCESSES";
export const SET_SELECTED_ASSESSMENT_PROCESS_REF_NBR_ASSIGN_CANDIDATES =
  "assessment_process/SET_SELECTED_ASSESSMENT_PROCESS_REF_NBR_ASSIGN_CANDIDATES";
export const SET_SELECTED_ASSESSMENT_PROCESS_REF_NBR_ACTIVE_PROCESSES =
  "assessment_process/SET_SELECTED_ASSESSMENT_PROCESS_REF_NBR_ACTIVE_PROCESSES";

// Process Results
export const SET_PROCESS_RESULTS_PAGINATION_PAGE =
  "processResults/SET_PROCESS_RESULTS_PAGINATION_PAGE";
export const SET_PROCESS_RESULTS_PAGINATION_PAGE_SIZE =
  "processResults/SET_PROCESS_RESULTS_PAGINATION_PAGE_SIZE";
export const SET_PROCESS_RESULTS_SEARCH_PARAMETERS =
  "processResults/SET_PROCESS_RESULTS_SEARCH_PARAMETERS";
export const TRIGGER_DATA_RERENDER = "processResults/TRIGGER_DATA_RERENDER";
export const SET_ASSESSMENT_PROCESS_LABEL = "processResults/SET_ASSESSMENT_PROCESS_LABEL";

const setAssessmentProcessDataAssignCandidates = assessmentProcessDataAssignCandidates => ({
  type: SET_ASSESSMENT_PROCESS_DATA_ASSIGN_CANDIDATES,
  assessmentProcessDataAssignCandidates
});

const setAssessmentProcessDataActiveProcesses = assessmentProcessDataActiveProcesses => ({
  type: SET_ASSESSMENT_PROCESS_DATA_ACTIVE_PROCESSES,
  assessmentProcessDataActiveProcesses
});

const setAssessmentProcessLabel = newReferenceNumber => ({
  type: SET_ASSESSMENT_PROCESS_LABEL,
  newReferenceNumber
});

const setSelectedAssessmentProcessRefNbrAssignCandidates =
  selectedAssessmentProcessRefNbrAssignCandidates => ({
    type: SET_SELECTED_ASSESSMENT_PROCESS_REF_NBR_ASSIGN_CANDIDATES,
    selectedAssessmentProcessRefNbrAssignCandidates
  });

const setSelectedAssessmentProcessRefNbrActiveProcesses =
  selectedAssessmentProcessRefNbrActiveProcesses => ({
    type: SET_SELECTED_ASSESSMENT_PROCESS_REF_NBR_ACTIVE_PROCESSES,
    selectedAssessmentProcessRefNbrActiveProcesses
  });

const updateProcessResultsPageState = processResultsPaginationPage => ({
  type: SET_PROCESS_RESULTS_PAGINATION_PAGE,
  processResultsPaginationPage
});

const updateProcessResultsPageSizeState = processResultsPaginationPageSize => ({
  type: SET_PROCESS_RESULTS_PAGINATION_PAGE_SIZE,
  processResultsPaginationPageSize
});

const updateSearchProcessResultsStates = (processResultsKeyword, processResultsActiveSearch) => ({
  type: SET_PROCESS_RESULTS_SEARCH_PARAMETERS,
  processResultsKeyword,
  processResultsActiveSearch
});

const triggerDataRerenderFunction = () => ({
  type: TRIGGER_DATA_RERENDER
});

// getting all billing contacts (associated to current HR user) ==> no pagination, so getting all results at once
function getBillingContactsList() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-all-billing-contacts", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

// getting available test sessions for a specific test to administer
function getAvailableTestSessionsForSpecificTestToAdminister(
  // mandatory parameters
  processDuration,
  testSkillTypeId,
  allowBookingExternalTc,
  // optional parameters
  testSkillSubTypeId,
  departmentId,
  assessmentProcessId
) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-available-test-sessions-for-specific-test-to-administer/?process_duration=${processDuration}&test_skill_type_id=${testSkillTypeId}&allow_booking_external_tc=${allowBookingExternalTc}&test_skill_sub_type_id=${testSkillSubTypeId}&department_id=${departmentId}&assessment_process_id=${assessmentProcessId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// creating assessment process
const createAssessmentProcess = data => {
  return async function () {
    const response = await fetch(`/oec-cat/api/create-assessment-process/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
};

// delete assessment process
function deleteAssessmentProcess(assessmentProcessId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/delete-assessment-process/?assessment_process_id=${assessmentProcessId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
}

function getAssessmentProcessReferenceNumbers(onlyGetSentRequest) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-assessment-process-reference-numbers/?only_get_sent_request=${onlyGetSentRequest}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

function getAssessmentProcessData(assessmentProcessId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-assessment-process-data/?assessment_process_id=${assessmentProcessId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// assign candidate to assessment process
const assessmentProcessAssignCandidate = data => {
  return async function () {
    const response = await fetch(`/oec-cat/api/assessment-process-assign-candidate/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
};

function getAssessmentProcessAssignedCandidates(assessmentProcessId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-assessment-process-assigned-candidates/?assessment_process_id=${assessmentProcessId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

function getAssessmentProcessResultsCandidates(assessmentProcessId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-assessment-process-results-candidates/?assessment_process_id=${assessmentProcessId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// Assessment Process Report for Candidates
function getAssessmentProcessResultsCandidatesReport(assessmentProcessId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-assessment-process-results-candidates-report/?assessment_process_id=${assessmentProcessId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// edit assigned candidate from assessment process
const editAssessmentProcessAssignedCandidate = data => {
  return async function () {
    const response = await fetch(`/oec-cat/api/edit-assessment-process-assigned-candidate/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
};

// delete assigned candidate from assessment process
const deleteAssessmentProcessAssignedCandidate = data => {
  return async function () {
    const response = await fetch(`/oec-cat/api/delete-assessment-process-assigned-candidate/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
};

// send assessment process assigned candidates request
const sendAssessmentProcessAssignedCandidatesRequest = assessmentProcessId => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/send-assessment-process-assigned-candidates-request/?assessment_process_id=${assessmentProcessId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
};

// update "non-sent" assessment process data (called from assign candidates tab)
const updateNonSentAssessmentProcessData = data => {
  return async function () {
    const response = await fetch("/oec-cat/api/update-non-sent-assessment-process-data", {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
};

// send assessment process updates request
const sendAssessmentProcessUpdatesRequest = (
  assessmentProcessId,
  duration,
  contactEmailForCandidates
) => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/send-assessment-process-updates-request/?assessment_process_id=${assessmentProcessId}&duration=${duration}&contact_email_for_candidates=${contactEmailForCandidates}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
};

// invite single assessment process candidate request
const inviteSingleAssessmentProcessCandidateRequest = data => {
  return async function () {
    const response = await fetch(
      "/oec-cat/api/invite-single-assessment-process-candidate-request/",
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        },
        body: JSON.stringify(data)
      }
    );
    return response;
  };
};

// unassign assessment process assigned test to administer
const unassignAssessmentProcessAssignedTestToAdminister = assignedTestSpecsId => {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/unassign-assessment-process-assigned-test-to-administer/?assigned_test_specs_id=${assignedTestSpecsId}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
};

// get all assessment process results for current user
function getAllAssessmentProcessResults(page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-assessment-process-results/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get found assessment process results for current user
function getFoundAssessmentProcessResults(providedKeyword, page, pageSize) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }

  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-assessment-process-results/?keyword=${keyword}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

function updateReferenceNumber(assessment_process_id, new_reference_number) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/update-reference-number/?assessment_process_id=${assessment_process_id}&new_reference_number=${new_reference_number}`,
      {
        method: "POST",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
}

// Initial State
const initialState = {
  assessmentProcessDataAssignCandidates: {},
  assessmentProcessDataActiveProcesses: {},
  selectedAssessmentProcessRefNbrAssignCandidates: {},
  selectedAssessmentProcessRefNbrActiveProcesses: {},
  // Assessment Process Results
  processResultsPaginationPage: 1,
  processResultsPaginationPageSize: 25,
  processResultsKeyword: "",
  processResultsActiveSearch: false,
  processResultsData: {},
  triggerDataRerender: false
};

// Reducer
const assessmentProcess = (state = initialState, action) => {
  switch (action.type) {
    case SET_ASSESSMENT_PROCESS_DATA_ASSIGN_CANDIDATES:
      return {
        ...state,
        assessmentProcessDataAssignCandidates: action.assessmentProcessDataAssignCandidates
      };
    case SET_ASSESSMENT_PROCESS_DATA_ACTIVE_PROCESSES:
      return {
        ...state,
        assessmentProcessDataActiveProcesses: action.assessmentProcessDataActiveProcesses
      };
    case SET_SELECTED_ASSESSMENT_PROCESS_REF_NBR_ASSIGN_CANDIDATES:
      return {
        ...state,
        selectedAssessmentProcessRefNbrAssignCandidates:
          action.selectedAssessmentProcessRefNbrAssignCandidates
      };
    case SET_SELECTED_ASSESSMENT_PROCESS_REF_NBR_ACTIVE_PROCESSES:
      return {
        ...state,
        selectedAssessmentProcessRefNbrActiveProcesses:
          action.selectedAssessmentProcessRefNbrActiveProcesses
      };
    case SET_PROCESS_RESULTS_PAGINATION_PAGE:
      return {
        ...state,
        processResultsPaginationPage: action.processResultsPaginationPage
      };
    case SET_PROCESS_RESULTS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        processResultsPaginationPageSize: action.processResultsPaginationPageSize
      };
    case SET_PROCESS_RESULTS_SEARCH_PARAMETERS:
      return {
        ...state,
        processResultsKeyword: action.processResultsKeyword,
        processResultsActiveSearch: action.processResultsActiveSearch
      };
    case TRIGGER_DATA_RERENDER:
      return {
        ...state,
        triggerDataRerender: !state.triggerDataRerender
      };
    case SET_ASSESSMENT_PROCESS_LABEL:
      const assignCandidates = state.selectedAssessmentProcessRefNbrAssignCandidates;
      assignCandidates.label = action.newReferenceNumber;
      return {
        ...state,
        selectedAssessmentProcessRefNbrAssignCandidates: assignCandidates
      };
    default:
      return state;
  }
};

export default assessmentProcess;
export {
  initialState,
  setAssessmentProcessDataAssignCandidates,
  setAssessmentProcessDataActiveProcesses,
  getBillingContactsList,
  getAvailableTestSessionsForSpecificTestToAdminister,
  createAssessmentProcess,
  deleteAssessmentProcess,
  getAssessmentProcessReferenceNumbers,
  getAssessmentProcessData,
  assessmentProcessAssignCandidate,
  getAssessmentProcessAssignedCandidates,
  getAssessmentProcessResultsCandidates,
  getAssessmentProcessResultsCandidatesReport,
  deleteAssessmentProcessAssignedCandidate,
  editAssessmentProcessAssignedCandidate,
  sendAssessmentProcessAssignedCandidatesRequest,
  updateNonSentAssessmentProcessData,
  sendAssessmentProcessUpdatesRequest,
  inviteSingleAssessmentProcessCandidateRequest,
  updateProcessResultsPageState,
  updateProcessResultsPageSizeState,
  updateSearchProcessResultsStates,
  getAllAssessmentProcessResults,
  getFoundAssessmentProcessResults,
  triggerDataRerenderFunction,
  setSelectedAssessmentProcessRefNbrAssignCandidates,
  setSelectedAssessmentProcessRefNbrActiveProcesses,
  unassignAssessmentProcessAssignedTestToAdminister,
  updateReferenceNumber,
  setAssessmentProcessLabel
};
