import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// getting all tests where "is_public" is set to false
function getActiveNonPublicTests(testSkillTypeId = null, testSkillSubTypeId = null) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-active-non-public-tests/?test_skill_type_id=${testSkillTypeId}&test_skill_sub_type_id=${testSkillSubTypeId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

function getTestData(testDefinitionId) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-test-data/?test_definition_id=${testDefinitionId}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

export default getActiveNonPublicTests;

export { getTestData };
