import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// All Active candidates pagination
export const SET_ALL_ACTIVE_CANDIDATES_PAGINATION_PAGE =
  "permissions/SET_ALL_ACTIVE_CANDIDATES_PAGINATION_PAGE";
export const SET_ALL_ACTIVE_CANDIDATES_PAGINATION_PAGE_SIZE =
  "permissions/SET_ALL_ACTIVE_CANDIDATES_PAGINATION_PAGE_SIZE";
export const SET_ALL_ACTIVE_CANDIDATES_SEARCH_PARAMETERS =
  "permissions/SET_ALL_ACTIVE_CANDIDATES_SEARCH_PARAMETERS";

// update pagination page state (all active candidates)
const updateCurrentAllActiveCandidatesPageState = allActiveCandidatesPaginationPage => ({
  type: SET_ALL_ACTIVE_CANDIDATES_PAGINATION_PAGE,
  allActiveCandidatesPaginationPage
});
// update pagination pageSize state (all active candidates)
const updateAllActiveCandidatesPageSizeState = allActiveCandidatesPaginationPageSize => ({
  type: SET_ALL_ACTIVE_CANDIDATES_PAGINATION_PAGE_SIZE,
  allActiveCandidatesPaginationPageSize
});
// update search keyword state (all active candidates)
const updateSearchAllActiveCandidateStates = (
  allActiveCandidatesKeyword,
  allActiveCandidatesActiveSearch
) => ({
  type: SET_ALL_ACTIVE_CANDIDATES_SEARCH_PARAMETERS,
  allActiveCandidatesKeyword,
  allActiveCandidatesActiveSearch
});

// get all assigned candidates
function getAllActiveTests(page, pageSize) {
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-all-active-tests/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// get found assigned candidates
function getFoundAssignedCandidates(currentLanguage, providedKeyword, page, pageSize) {
  // Encode search keyword so we can use special characters
  let keyword = encodeURIComponent(providedKeyword);

  // if provided keyword is empty
  if (keyword === "") {
    // send a space as the keyword parameter
    keyword = " ";
  }
  return async function () {
    const response = await fetch(
      `/oec-cat/api/get-found-active-tests/?current_language=${currentLanguage}&keyword=${keyword}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    const responseJson = await response.json();
    return responseJson;
  };
}

// invalidatecandidate's test as an ETTA user
const invalidateCandidateAsEtta = data => {
  return async function () {
    const response = await fetch(`/oec-cat/api/as-etta-invalidate-candidate/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
};

// validatecandidate's test as an ETTA user
const validateCandidateAsEtta = data => {
  return async function () {
    const response = await fetch(`/oec-cat/api/as-etta-validate-candidate/`, {
      method: "POST",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
};

// Initial State
const initialState = {
  allActiveCandidatesPaginationPage: 1,
  allActiveCandidatesPaginationPageSize: 25,
  allActiveCandidatesKeyword: "",
  allActiveCandidatesActiveSearch: false
};

const activeTests = (state = initialState, action) => {
  switch (action.type) {
    case SET_ALL_ACTIVE_CANDIDATES_PAGINATION_PAGE:
      return {
        ...state,
        allActiveCandidatesPaginationPage: action.allActiveCandidatesPaginationPage
      };
    case SET_ALL_ACTIVE_CANDIDATES_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        allActiveCandidatesPaginationPageSize: action.allActiveCandidatesPaginationPageSize
      };
    case SET_ALL_ACTIVE_CANDIDATES_SEARCH_PARAMETERS:
      return {
        ...state,
        allActiveCandidatesKeyword: action.allActiveCandidatesKeyword,
        allActiveCandidatesActiveSearch: action.allActiveCandidatesActiveSearch
      };
    default:
      return state;
  }
};

export default activeTests;
export {
  initialState,
  getAllActiveTests,
  getFoundAssignedCandidates,
  updateCurrentAllActiveCandidatesPageState,
  updateAllActiveCandidatesPageSizeState,
  updateSearchAllActiveCandidateStates,
  invalidateCandidateAsEtta,
  validateCandidateAsEtta
};
