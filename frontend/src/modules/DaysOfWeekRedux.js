import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// getting days of week
function getDaysOfWeek() {
  return async function () {
    const response = await fetch("/oec-cat/api/get-days-of-week", {
      method: "GET",
      headers: {
        Authorization: `JWT ${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}`,
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    const responseJson = await response.json();
    return responseJson;
  };
}

export default getDaysOfWeek;
