/* eslint-disable no-continue */
import React from "react";
import withRouter from "./components/withRouter";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import LOCALIZE from "./text_resources";
import "./css/site-nav-menu.css";
import { PATH } from "./components/commons/Constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCalendarCheck,
  faUserCircle,
  // faFile,
  // faEnvelope,
  faSignOutAlt,
  faUserClock,
  faUserShield,
  faUserCog,
  faUserEdit,
  faHammer,
  faSortDown,
  faHatWizard,
  faBuilding,
  faUserFriends,
  faUniversalAccess,
  faUserCheck
} from "@fortawesome/free-solid-svg-icons";
import { resetTestStatusState } from "./modules/TestStatusRedux";
import { resetAccommodations, getLineSpacingCSS } from "./modules/AccommodationsRedux";
import { resetNotepadState } from "./modules/NotepadRedux";
import { resetTestStatusState as resetSampleTestStatusState } from "./modules/SampleTestStatusRedux";
import { logoutAction } from "./modules/LoginRedux";
import { resetErrorStatusState } from "./modules/ErrorStatusRedux";
import { useMenuState, Menu, MenuButton, MenuItem } from "reakit/Menu";
import { resetUserState } from "./modules/UserRedux";
import { resetUserProfileStates } from "./modules/UserProfileRedux";
import { resetUserLookUpState } from "./modules/UserLookUpRedux";
import { resetAllItemBanksStates } from "./modules/ItemBankRedux";
import { resetPermissionsState } from "./modules/PermissionsRedux";
import { resetActiveItemBanksState } from "./modules/RDItemBankRedux";
import { resetTestAdministrationStates } from "./modules/TestAdministrationRedux";
import { resetTestCenterStates } from "./modules/TestCenterRedux";
import { resetAaeStates } from "./modules/AaeRedux";
import { resetConsultationServicesStates } from "./modules/ConsultationServicesRedux";
import { resetScorerStates } from "./modules/ScorerRedux";
import { history } from "./store-index";

// This file uses react hooks (a functional component feature) and classes.
// Normally these two cannot be used together (we need to for the menu package).
// I use a higher order component at the bottom to wrap the react hooks because
// they cannot be used inside a render function.
// https://infinum.com/the-capsized-eight/how-to-use-react-hooks-in-class-components

const styles = {
  menuButton: {
    marginTop: "12px",
    border: "none",
    backgroundColor: "#fff",
    color: "#00565e"
  },
  icon: {
    marginRight: 12,
    color: "white"
  },
  text: {
    color: "white"
  },
  menuItem: {
    position: "relative",
    display: "block",
    border: "none"
  },
  menu: {
    zIndex: 9999,
    backgroundColor: "#00565e",
    border: "none"
  },
  menuArrow: {
    verticalAlign: "inherit",
    marginLeft: 4
  }
};

const getItemList = (props, menu, style) => {
  const items = [];
  const className = "dropdown-item";
  const classNameActive = "dropdown-item dropdown-item-active";

  // SUPER USER
  if (props.isSuperUser) {
    items.push([
      <MenuItem
        {...menu}
        key={1}
        style={{ ...styles.menuItem, ...style }}
        onClick={() => {
          history.push(PATH.superuserDashboard);
          menu.hide();
        }}
        className={
          window.location.pathname === PATH.superuserDashboard ? classNameActive : className
        }
        id={1}
      >
        <FontAwesomeIcon style={styles.icon} icon={faHatWizard} />
        <span style={styles.text}>{LOCALIZE.menu.superuser}</span>
      </MenuItem>
    ]);
  }

  // R&D OPERATION (ETTA)
  if (props.isEtta || props.isRdOperations) {
    items.push([
      <MenuItem
        className={
          window.location.pathname === PATH.systemAdministration ||
          window.location.pathname === PATH.testCenterAccesses
            ? classNameActive
            : className
        }
        {...menu}
        key={2}
        style={{ ...styles.menuItem, ...style }}
        onClick={() => {
          history.push(PATH.systemAdministration);
          menu.hide();
        }}
        id={2}
      >
        <FontAwesomeIcon style={styles.icon} icon={faUserShield} />
        <span style={styles.text}>
          {props.isEtta && !props.isRdOperations
            ? LOCALIZE.menu.etta
            : !props.isEtta && props.isRdOperations
              ? LOCALIZE.menu.rdOperations
              : LOCALIZE.menu.operations}
        </span>
      </MenuItem>
    ]);
  }

  // TEST DEVELOPER
  if (props.isTb || props.isTd) {
    items.push([
      <MenuItem
        {...menu}
        key={3}
        style={{ ...styles.menuItem, ...style }}
        onClick={() => {
          history.push(PATH.testBuilder);
          menu.hide();
        }}
        className={
          window.location.pathname === PATH.testBuilder ||
          window.location.pathname === PATH.itemBankEditor ||
          window.location.pathname === PATH.itemEditor ||
          window.location.pathname === PATH.bundleEditor
            ? classNameActive
            : className
        }
        id={3}
      >
        <FontAwesomeIcon style={styles.icon} icon={faHammer} />
        <span style={styles.text}>{LOCALIZE.menu.testBuilder}</span>
      </MenuItem>
    ]);
  }

  // R&D
  if (props.isPpc) {
    items.push([
      <MenuItem
        {...menu}
        key={4}
        style={{ ...styles.menuItem, ...style }}
        onClick={() => {
          history.push(PATH.ppcAdministration);
          menu.hide();
        }}
        className={
          window.location.pathname === PATH.ppcAdministration ? classNameActive : className
        }
        id={4}
      >
        <FontAwesomeIcon style={styles.icon} icon={faUserCog} />
        <span style={styles.text}>{LOCALIZE.menu.ppc}</span>
      </MenuItem>
    ]);
  }

  // TEST SCORER
  if (props.isScorer) {
    items.push([
      <MenuItem
        {...menu}
        key={5}
        style={{ ...styles.menuItem, ...style }}
        onClick={() => {
          history.push(PATH.scorerBase);
          menu.hide();
        }}
        className={window.location.pathname === PATH.scorerBase ? classNameActive : className}
        id={5}
      >
        <FontAwesomeIcon style={styles.icon} icon={faUserEdit} />
        <span style={styles.text}>{LOCALIZE.menu.scorer}</span>
      </MenuItem>
    ]);
  }

  // TEST ADMINISTRATOR
  if (props.isTa) {
    items.push([
      <MenuItem
        {...menu}
        key={6}
        style={{ ...styles.menuItem, ...style }}
        onClick={() => {
          history.push(PATH.testAdministration);
          menu.hide();
        }}
        className={
          window.location.pathname === PATH.testAdministration ? classNameActive : className
        }
        id={6}
      >
        <FontAwesomeIcon style={styles.icon} icon={faUserClock} />
        <span style={styles.text}>{LOCALIZE.menu.ta}</span>
      </MenuItem>
    ]);
  }

  // TEST CENTER MANAGER
  if (props.isTcm) {
    items.push([
      <MenuItem
        {...menu}
        key={7}
        style={{ ...styles.menuItem, ...style }}
        onClick={() => {
          history.push(PATH.testCenterManager);
          menu.hide();
        }}
        className={
          window.location.pathname === PATH.testCenterManager ||
          window.location.pathname === PATH.testCenterManagement
            ? classNameActive
            : className
        }
        id={7}
      >
        <FontAwesomeIcon style={styles.icon} icon={faBuilding} />
        <span style={styles.text}>{LOCALIZE.menu.testCenterManager}</span>
      </MenuItem>
    ]);
  }

  // AAE
  if (props.isAae || props.isSuperUser) {
    items.push([
      <MenuItem
        {...menu}
        key={8}
        style={{ ...styles.menuItem, ...style }}
        onClick={() => {
          history.push(PATH.aae);
          menu.hide();
        }}
        className={
          window.location.pathname === PATH.aae ||
          window.location.pathname === PATH.aaeSelectedUserAccommodationFile
            ? classNameActive
            : className
        }
        id={8}
      >
        <FontAwesomeIcon style={styles.icon} icon={faUniversalAccess} />
        <span style={styles.text}>{LOCALIZE.menu.aae}</span>
      </MenuItem>
    ]);
  }

  // CONSULTATION SERVICES
  if (props.isConsultationServices) {
    items.push([
      <MenuItem
        {...menu}
        key={9}
        style={{ ...styles.menuItem, ...style }}
        onClick={() => {
          history.push(PATH.consultationServices);
          menu.hide();
        }}
        className={
          window.location.pathname === PATH.consultationServices ? classNameActive : className
        }
        id={9}
      >
        <FontAwesomeIcon style={styles.icon} icon={faUserCheck} />
        <span style={styles.text}>{LOCALIZE.menu.consultationServices}</span>
      </MenuItem>
    ]);
  }

  // HR COORDINATOR
  if (props.isHrCoordinator) {
    items.push([
      <MenuItem
        {...menu}
        key={10}
        style={{ ...styles.menuItem, ...style }}
        onClick={() => {
          history.push(PATH.inPersonTesting);
          menu.hide();
        }}
        className={window.location.pathname === PATH.inPersonTesting ? classNameActive : className}
        id={10}
      >
        <FontAwesomeIcon style={styles.icon} icon={faUserFriends} />
        <span style={styles.text}>{LOCALIZE.menu.inPersonTesting}</span>
      </MenuItem>
    ]);
  }

  // MY TESTS
  items.push([
    <MenuItem
      {...menu}
      key={11}
      style={{ ...styles.menuItem, ...style }}
      onClick={() => {
        history.push(PATH.dashboard);
        menu.hide();
      }}
      className={window.location.pathname === PATH.dashboard ? classNameActive : className}
      id={11}
    >
      <FontAwesomeIcon style={styles.icon} icon={faCalendarCheck} />
      <span style={styles.text}>{LOCALIZE.menu.MyTests}</span>
    </MenuItem>
  ]);

  // MY PROFILE
  items.push([
    <MenuItem
      {...menu}
      key={12}
      style={{ ...styles.menuItem, ...style }}
      onClick={() => {
        history.push(PATH.profile);
        menu.hide();
      }}
      className={window.location.pathname === PATH.profile ? classNameActive : className}
      id={12}
    >
      <FontAwesomeIcon style={styles.icon} icon={faUserCircle} />
      <span style={styles.text}>{LOCALIZE.menu.profile}</span>
    </MenuItem>
  ]);

  // LOGOUT
  items.push([
    <MenuItem
      className={"dropdown-item"}
      {...menu}
      key={13}
      style={{ ...styles.menuItem, ...style }}
      onClick={() => {
        props.resetTestStatusState();
        props.resetSampleTestStatusState();
        props.resetAccommodations();
        props.resetNotepadState();
        props.resetUserState();
        props.resetUserProfileStates();
        props.resetErrorStatusState();
        props.resetUserLookUpState();
        props.resetAllItemBanksStates();
        props.resetPermissionsState();
        props.resetActiveItemBanksState();
        props.resetTestAdministrationStates();
        props.resetTestCenterStates();
        props.resetAaeStates();
        props.resetConsultationServicesStates();
        props.resetScorerStates();
        props.logoutAction();
        history.push(PATH.login);
        menu.hide();
      }}
      id={13}
    >
      <FontAwesomeIcon style={styles.icon} icon={faSignOutAlt} />
      <span style={styles.text}>{LOCALIZE.menu.logout}</span>
    </MenuItem>
  ]);

  // items.push([
  //   <MenuItem
  //     {...menu}
  //     key={9}
  //     style={styles.menuItem}
  //     onClick={() => {
  //       history.push(PATH.incidentReport);
  //       menu.hide();
  //     }}
  //     className={window.location.pathname === PATH.incidentReport ? classNameActive : className}
  //     id={9}
  //   >
  //     <FontAwesomeIcon style={styles.icon} icon={faFile} />
  //     <span style={{ ...styles.text, ...style }}>{LOCALIZE.menu.incidentReport}</span>
  //   </MenuItem>
  // ]);

  // items.push([
  //   <MenuItem
  //     {...menu}
  //     key={11}
  //     style={styles.menuItem}
  //     onClick={() => {
  //       history.push(PATH.contactUs);
  //       menu.hide();
  //     }}
  //     className={window.location.pathname === PATH.contactUs ? classNameActive : className}
  //     id={11}
  //   >
  //     <FontAwesomeIcon style={styles.icon} icon={faEnvelope} />
  //     <span style={{ ...styles.text, ...style }}>{LOCALIZE.menu.ContactUs}</span>
  //   </MenuItem>
  // ]);

  return items;
};

const MenuHooks = props => {
  const menu = useMenuState({ loop: true });
  return function renderReakit(style) {
    return (
      <>
        <MenuButton
          {...menu}
          style={{ ...styles.menuButton, ...style, paddingBottom: 0 }}
          id="navigation-bar-main-link-id"
        >
          {LOCALIZE.mainTabs.menu}
          <FontAwesomeIcon style={styles.menuArrow} icon={faSortDown} />
        </MenuButton>
        <Menu {...menu} aria-label={LOCALIZE.mainTabs.menu} style={styles.menu}>
          {getItemList(props, menu, style).map((item, id) => (
            <div key={id}>{item}</div>
          ))}
        </Menu>
      </>
    );
  };
};

const SiteNavMenu = props => {
  // Props from Redux
  // resetInboxState: PropTypes.func
  // resetMetaDataState: PropTypes.func
  // resetTestStatusState: PropTypes.func
  // resetSampleTestStatusState: PropTypes.func
  // resetNotepadState: PropTypes.func
  // logoutAction: PropTypes.func
  // resetErrorStatusState: PropTypes.func
  // resetUserState: PropTypes.func
  // resetUserProfileStates: PropTypes.func
  // resetUserLookUpState: PropTypes.func

  let style = {
    fontFamily: props.accommodations.fontFamily,
    fontSize: props.accommodations.fontSize
  };
  if (props.accommodations.spacing) {
    style = { ...style, ...getLineSpacingCSS() };
  }

  return <>{props.myHookValue(style)}</>;
};

const mapStateToProps = (state, ownProps) => {
  return {
    isEtta: state.userPermissions.isEtta,
    isRdOperations: state.userPermissions.isRdOperations,
    isPpc: state.userPermissions.isPpc,
    isTcm: state.userPermissions.isTcm,
    isTa: state.userPermissions.isTa,
    isScorer: state.userPermissions.isScorer,
    isTb: state.userPermissions.isTb,
    isTd: state.userPermissions.isTd,
    accommodations: state.accommodations,
    isSuperUser: state.user.isSuperUser,
    isHrCoordinator: state.userPermissions.isHrCoordinator,
    isAae: state.userPermissions.isAae,
    isConsultationServices: state.userPermissions.isConsultationServices
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      logoutAction,
      resetTestStatusState,
      resetSampleTestStatusState,
      resetNotepadState,
      resetErrorStatusState,
      resetAccommodations,
      resetUserState,
      resetUserProfileStates,
      resetUserLookUpState,
      resetAllItemBanksStates,
      resetPermissionsState,
      resetActiveItemBanksState,
      resetTestAdministrationStates,
      resetTestCenterStates,
      resetAaeStates,
      resetConsultationServicesStates,
      resetScorerStates
    },
    dispatch
  );

const withMyHook = Component => {
  return function WrappedComponent(props) {
    const myHookValue = MenuHooks(props);
    return <Component {...props} myHookValue={myHookValue} />;
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(withMyHook(SiteNavMenu)));
