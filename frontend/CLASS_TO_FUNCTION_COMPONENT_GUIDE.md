# Guide to Convert a Class Component to a Function Component

## Why
Since we upgraded to React v18, some packages have been completely converted to Hooks Functions. To use Hooks, we have to use a Function Component.

## What is a "Hook" function

Hooks are functions that let you “hook into” React state and lifecycle features from function components. Hooks don’t work inside classes — they let you use React without classes.

## Differences

### Function Component:
```javascript
import React, { useState, useEffect } from 'react';

function Example() {
    // Declare a new state variable, which we'll call "count"
    const [count, setCount] = useState(0);

    // Equivalent of componentDidMount
    useEffect(() => {
        // Your code here
    }, []);

    // Equivalent of componentDidUpdate
    // -> where "yourDependency" is the variable being monitored
    useEffect(() => {
        // Your code here
    }, [yourDependency]);

    // Equivalent of componentWillUnmount
    useEffect(() => {
        return () => {
            // Your code here
        }
    }, [yourDependency]);

    return (
        <div>
        <p>You clicked {count} times</p>
        <button onClick={() => setCount(count + 1)}>
            Click me
        </button>
        </div>
    );
}
```

In this example, we use "useState" hook to create our states "count".

### Class Component:
```javascript
import React from 'react';

class Example extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        count: 0
        };
    }

    componentDidMount() {
        alert("The current component has been mounted.");
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.data !== prevProps.data) {
            alert("this.props.data has been modified");
        }
    }

    componentWillUnmount() {
        alert("The current component is about to be unmounted.");
    }

    render() {
        return (
        <div>
            <p>You clicked {this.state.count} times</p>
            <button onClick={() => this.setState({ count: this.state.count + 1 })}>
            Click me
            </button>
        </div>
        );
    }
}
```

In this example, we create the state in the constructor of the class.

## Rules of Hooks
Rules of Hooks
Hooks are JavaScript functions, but they impose two additional rules:

### Rule 1: Only call Hooks at the top level. Don’t call Hooks inside loops, conditions, or nested functions.

Functions whose names start with **use** are called Hooks in React.

Don’t call Hooks inside loops, conditions, nested functions, or try/catch/finally blocks. Instead, always use Hooks at the top level of your React function, before any early returns. You can only call Hooks while React is rendering a function component:

✅ Call them at the top level in the body of a function component.
```javascript
function Counter() {
  // ✅ Good: top-level in a function component
  const [count, setCount] = useState(0);
  // ...
}
```

✅ Call them at the top level in the body of a custom Hook.
```javascript
function useWindowWidth() {
  // ✅ Good: top-level in a custom Hook
  const [width, setWidth] = useState(window.innerWidth);
  // ...
}
```


It’s not supported to call Hooks (functions starting with use) in any other cases, for example:

🔴 Do not call Hooks inside conditions or loops.
```javascript
function Bad({ cond }) {
  if (cond) {
    // 🔴 Bad: inside a condition (to fix, move it outside!)
    const theme = useContext(ThemeContext);
  }
  // ...
}
```

```javascript
function Bad() {
  for (let i = 0; i < 10; i++) {
    // 🔴 Bad: inside a loop (to fix, move it outside!)
    const theme = useContext(ThemeContext);
  }
  // ...
}
```

🔴 Do not call Hooks after a conditional return statement.
```javascript
function Bad({ cond }) {
  if (cond) {
    return;
  }
  // 🔴 Bad: after a conditional return (to fix, move it before the return!)
  const theme = useContext(ThemeContext);
  // ...
}
```

🔴 Do not call Hooks in event handlers.
```javascript
function Bad() {
  function handleClick() {
    // 🔴 Bad: inside an event handler (to fix, move it outside!)
    const theme = useContext(ThemeContext);
  }
  // ...
}
```

🔴 Do not call Hooks in class components.
```javascript
class Bad extends React.Component {
  render() {
    // 🔴 Bad: inside a class component (to fix, write a function component instead of a class!)
    useEffect(() => {})
    // ...
  }
}
```

🔴 Do not call Hooks inside functions passed to useMemo, useReducer, or useEffect.
```javascript
function Bad() {
  const style = useMemo(() => {
    // 🔴 Bad: inside useMemo (to fix, move it outside!)
    const theme = useContext(ThemeContext);
    return createStyle(theme);
  });
  // ...
}
```

🔴 Do not call Hooks inside try/catch/finally blocks.
```javascript
function Bad() {
  try {
    // 🔴 Bad: inside try/catch/finally block (to fix, move it outside!)
    const [x, setX] = useState(0);
  } catch {
    const [x, setX] = useState(1);
  }
}
```


### Rule 2: Only call Hooks from React function components. Don’t call Hooks from regular JavaScript functions.

✅ Call Hooks from React function components.

✅ Call Hooks from custom Hooks.

```javascript
// This is a function component
function FriendList() {
  const [onlineStatus, setOnlineStatus] = useOnlineStatus(); // ✅
}

// This is a simple function
function setOnlineStatus() { // ❌ Not a component or custom Hook!
  const [onlineStatus, setOnlineStatus] = useOnlineStatus();
}
```

