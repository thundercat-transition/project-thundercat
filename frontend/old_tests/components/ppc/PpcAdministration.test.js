import React from "react";
import { shallow } from "enzyme";
import { unconnectedPpcAdministration as PpcAdministration } from "../../../PpcAdministration";
import LOCALIZE from "../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(
    <PpcAdministration firstName="John" lastName="Smith" username="john.smith@canada.ca" />,
    {
      disableLifecycleMethods: true
    }
  );

  it("renders welcome message", () => {
    const welcomeMessage = (
      <h1>{LOCALIZE.formatString(LOCALIZE.ppcAdministration.title, "John", "Smith")}</h1>
    );
    expect(wrapper.containsMatchingElement(welcomeMessage)).toEqual(true);
  });

  it("renders side nav container label", () => {
    const label = (
      <label>
        {LOCALIZE.ppcAdministration.containerLabel}
        <span></span>
      </label>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
  });
});
