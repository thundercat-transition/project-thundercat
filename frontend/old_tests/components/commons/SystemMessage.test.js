import React from "react";
import { mount } from "enzyme";
import {
  UnconnectedSystemMessage as SystemMessage,
  MESSAGE_TYPE,
  CLASS_NAME
} from "../../../components/commons/SystemMessage";
import {
  faTimesCircle,
  faExclamationCircle,
  faCheckCircle,
  faInfoCircle
} from "@fortawesome/free-solid-svg-icons";

it("renders default message (info)", () => {
  const wrapper = mount(
    <SystemMessage
      messageType={"hello world"}
      title={"title"}
      message={<p>message</p>}
      accommodations={{ fontSize: "16px" }}
    />
  );
  const warningAlertClassName = "alert-icon alert-info";
  const warningIconClassName = faInfoCircle;
  const title = <h5>title</h5>;
  const message = <p>message</p>;
  expect(warningAlertClassName === CLASS_NAME.alert).toEqual(true);
  expect(warningIconClassName === CLASS_NAME.icon).toEqual(true);
  expect(wrapper.containsMatchingElement(title)).toEqual(true);
  expect(wrapper.contains(message)).toEqual(true);
});

it("renders info message", () => {
  const wrapper = mount(
    <SystemMessage
      messageType={MESSAGE_TYPE.info}
      title={"title"}
      message={<p>message</p>}
      accommodations={{ fontSize: "16px" }}
    />
  );
  const warningAlertClassName = "alert-icon alert-info";
  const warningIconClassName = faInfoCircle;
  const title = <h5>title</h5>;
  const message = <p>message</p>;
  expect(warningAlertClassName === CLASS_NAME.alert).toEqual(true);
  expect(warningIconClassName === CLASS_NAME.icon).toEqual(true);
  expect(wrapper.containsMatchingElement(title)).toEqual(true);
  expect(wrapper.contains(message)).toEqual(true);
});

it("renders success message", () => {
  const wrapper = mount(
    <SystemMessage
      messageType={MESSAGE_TYPE.success}
      title={"title"}
      message={<p>message</p>}
      accommodations={{ fontSize: "16px" }}
    />
  );
  const warningAlertClassName = "alert-icon alert-success";
  const warningIconClassName = faCheckCircle;
  const title = <h5>title</h5>;
  const message = <p>message</p>;
  expect(warningAlertClassName === CLASS_NAME.alert).toEqual(true);
  expect(warningIconClassName === CLASS_NAME.icon).toEqual(true);
  expect(wrapper.containsMatchingElement(title)).toEqual(true);
  expect(wrapper.contains(message)).toEqual(true);
});

it("renders warning message", () => {
  const wrapper = mount(
    <SystemMessage
      messageType={MESSAGE_TYPE.warning}
      title={"title"}
      message={<p>message</p>}
      accommodations={{ fontSize: "16px" }}
    />
  );
  const warningAlertClassName = "alert-icon alert-warning";
  const warningIconClassName = faExclamationCircle;
  const title = <h5>title</h5>;
  const message = <p>message</p>;
  expect(warningAlertClassName === CLASS_NAME.alert).toEqual(true);
  expect(warningIconClassName === CLASS_NAME.icon).toEqual(true);
  expect(wrapper.containsMatchingElement(title)).toEqual(true);
  expect(wrapper.contains(message)).toEqual(true);
});

it("renders error message", () => {
  const wrapper = mount(
    <SystemMessage
      messageType={MESSAGE_TYPE.error}
      title={"title"}
      message={<p>message</p>}
      accommodations={{ fontSize: "16px" }}
    />
  );
  const errorAlertClassName = "alert-icon alert-danger";
  const errorIconClassName = faTimesCircle;
  const title = <h5>title</h5>;
  const message = <p>message</p>;
  expect(errorAlertClassName === CLASS_NAME.alert).toEqual(true);
  expect(errorIconClassName === CLASS_NAME.icon).toEqual(true);
  expect(wrapper.containsMatchingElement(title)).toEqual(true);
  expect(wrapper.contains(message)).toEqual(true);
});
