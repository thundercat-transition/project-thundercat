import React from "react";
import { shallow } from "enzyme";
import { unconnectedLockScreen as LockScreen } from "../../../components/commons/LockScreen";
import LOCALIZE from "../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(<LockScreen accommodations={{ fontSize: "16px" }} />, {
    disableLifecycleMethods: true
  });

  it("renders tab title", () => {
    const tabTitle = (
      <label>
        {LOCALIZE.candidateLockScreen.tabTitle}
        <span></span>
      </label>
    );
    expect(wrapper.containsMatchingElement(tabTitle)).toEqual(true);
  });

  it("renders tab content description", () => {
    const part1 = <p>{LOCALIZE.candidateLockScreen.description.part1}</p>;
    const part2 = <p>{LOCALIZE.candidateLockScreen.description.part2}</p>;
    const part3 = <p>{LOCALIZE.candidateLockScreen.description.part3}</p>;
    const part4 = <p>{LOCALIZE.candidateLockScreen.description.part4}</p>;
    expect(wrapper.containsMatchingElement(part1)).toEqual(true);
    expect(wrapper.containsMatchingElement(part2)).toEqual(true);
    expect(wrapper.containsMatchingElement(part3)).toEqual(true);
    expect(wrapper.containsMatchingElement(part4)).toEqual(true);
  });
});
