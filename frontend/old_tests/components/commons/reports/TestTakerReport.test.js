import React from "react";
import { shallow } from "enzyme";
import { unconnectedTestTakerReport as TestTakerReport } from "../../../../components/commons/reports/TestTakerReport";
import LOCALIZE from "../../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(<TestTakerReport />, {
    disableLifecycleMethods: true
  });

  it("renders no dropdowns at all (initial states)", () => {
    const parentCode = <label id="parent-code-label">{LOCALIZE.reports.parentCodeLabel}</label>;
    const testCode = <label id="test-code-label">{LOCALIZE.reports.testCodeLabel}</label>;
    const dateFromLabel = <label id="date-from-label">{LOCALIZE.reports.dateFromLabel}</label>;
    const dateToLabel = <label id="date-to-label">{LOCALIZE.reports.dateToLabel}</label>;
    expect(wrapper.containsMatchingElement(parentCode)).toEqual(false);
    expect(wrapper.containsMatchingElement(testCode)).toEqual(false);
    expect(wrapper.containsMatchingElement(dateFromLabel)).toEqual(false);
    expect(wrapper.containsMatchingElement(dateToLabel)).toEqual(false);
    expect(wrapper.find("#unit-test-parent-code-dropdown").exists()).toEqual(false);
    expect(wrapper.find("#unit-test-test-code-dropdown").exists()).toEqual(false);
  });

  it("renders parent code dropdown (when report type has been selected)", () => {
    // simulating that report type (option 1 - Individual Score Sheet) has been selected
    wrapper.setState({
      parentCodeVisible: true
    });

    const parentCode = <label id="parent-code-label">{LOCALIZE.reports.parentCodeLabel}</label>;
    const testCode = <label id="test-code-label">{LOCALIZE.reports.testCodeLabel}</label>;
    const dateFromLabel = <label id="date-from-label">{LOCALIZE.reports.dateFromLabel}</label>;
    const dateToLabel = <label id="date-to-label">{LOCALIZE.reports.dateToLabel}</label>;
    expect(wrapper.containsMatchingElement(parentCode)).toEqual(true);
    expect(wrapper.containsMatchingElement(testCode)).toEqual(false);
    expect(wrapper.containsMatchingElement(dateFromLabel)).toEqual(false);
    expect(wrapper.containsMatchingElement(dateToLabel)).toEqual(false);
    expect(wrapper.find("#unit-test-parent-code-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-test-code-dropdown").exists()).toEqual(false);
  });

  it("renders parent code and test code dropdowns (when parent code has been selected)", () => {
    // simulating that report type (option 1 - Individual Score Sheet) has been selected
    wrapper.setState({
      testCodeVisible: true
    });

    const parentCode = <label id="parent-code-label">{LOCALIZE.reports.parentCodeLabel}</label>;
    const testCode = <label id="test-code-label">{LOCALIZE.reports.testCodeLabel}</label>;
    const dateFromLabel = <label id="date-from-label">{LOCALIZE.reports.dateFromLabel}</label>;
    const dateToLabel = <label id="date-to-label">{LOCALIZE.reports.dateToLabel}</label>;
    expect(wrapper.containsMatchingElement(parentCode)).toEqual(true);
    expect(wrapper.containsMatchingElement(testCode)).toEqual(true);
    expect(wrapper.containsMatchingElement(dateFromLabel)).toEqual(false);
    expect(wrapper.containsMatchingElement(dateToLabel)).toEqual(false);
    expect(wrapper.find("#unit-test-parent-code-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-test-code-dropdown").exists()).toEqual(true);
  });

  it("renders parent code, test code dropdowns and date from label (when test code has been selected)", () => {
    // simulating that report type (option 1 - Individual Score Sheet) has been selected
    wrapper.setState({
      dateFromVisible: true
    });

    const parentCode = <label id="parent-code-label">{LOCALIZE.reports.parentCodeLabel}</label>;
    const testCode = <label id="test-code-label">{LOCALIZE.reports.testCodeLabel}</label>;
    const dateFromLabel = <label id="date-from-label">{LOCALIZE.reports.dateFromLabel}</label>;
    const dateToLabel = <label id="date-to-label">{LOCALIZE.reports.dateToLabel}</label>;
    expect(wrapper.containsMatchingElement(parentCode)).toEqual(true);
    expect(wrapper.containsMatchingElement(testCode)).toEqual(true);
    expect(wrapper.containsMatchingElement(dateFromLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(dateToLabel)).toEqual(false);
    expect(wrapper.find("#unit-test-parent-code-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-test-code-dropdown").exists()).toEqual(true);
  });

  it("renders parent code, test code dropdowns and date from and to labels (when date from has been selected)", () => {
    // simulating that report type (option 1 - Individual Score Sheet) has been selected
    wrapper.setState({
      dateToVisible: true
    });

    const parentCode = <label id="parent-code-label">{LOCALIZE.reports.parentCodeLabel}</label>;
    const testCode = <label id="test-code-label">{LOCALIZE.reports.testCodeLabel}</label>;
    const dateFromLabel = <label id="date-from-label">{LOCALIZE.reports.dateFromLabel}</label>;
    const dateToLabel = <label id="date-to-label">{LOCALIZE.reports.dateToLabel}</label>;
    expect(wrapper.containsMatchingElement(parentCode)).toEqual(true);
    expect(wrapper.containsMatchingElement(testCode)).toEqual(true);
    expect(wrapper.containsMatchingElement(dateFromLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(dateToLabel)).toEqual(true);
    expect(wrapper.find("#unit-test-parent-code-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-test-code-dropdown").exists()).toEqual(true);
  });
});
