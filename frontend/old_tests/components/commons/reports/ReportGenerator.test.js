import React from "react";
import { shallow } from "enzyme";
import { unconnectedReportGenerator as ReportGenerator } from "../../../../components/commons/reports/ReportGenerator";
import {
  REPORT_REQUESTOR,
  reportTypesDefinition
} from "../../../../components/commons/reports/Constants";
import LOCALIZE from "../../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(
    <ReportGenerator
      reportRequestor={REPORT_REQUESTOR.etta}
      reportTypeOptions={reportTypesDefinition()}
    />
  );

  it("renders report type label and dropdown (initial states)", () => {
    const reportTypeLabel = (
      <label id="report-type-label">{LOCALIZE.reports.reportTypeLabel}</label>
    );
    expect(wrapper.containsMatchingElement(reportTypeLabel)).toEqual(true);
    expect(wrapper.find("#unit-test-report-type-dropdown").exists()).toEqual(true);
  });
});
