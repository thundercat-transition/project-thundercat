import React from "react";
import { shallow } from "enzyme";
import { unconnectedFinancialReport as FinancialReport } from "../../../../components/commons/reports/FinancialReport";
import LOCALIZE from "../../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(<FinancialReport />, {
    disableLifecycleMethods: true
  });

  it("renders date from label (initial states)", () => {
    const dateFromLabel = <label id="date-from-label">{LOCALIZE.reports.dateFromLabel}</label>;
    const dateToLabel = <label id="date-to-label">{LOCALIZE.reports.dateToLabel}</label>;
    expect(wrapper.containsMatchingElement(dateFromLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(dateToLabel)).toEqual(false);
  });

  it("renders date from and date to labels (when date from has been selected)", () => {
    // simulating that report type (option 1 - Individual Score Sheet) has been selected
    wrapper.setState({
      dateToVisible: true
    });

    const dateFromLabel = <label id="date-from-label">{LOCALIZE.reports.dateFromLabel}</label>;
    const dateToLabel = <label id="date-to-label">{LOCALIZE.reports.dateToLabel}</label>;
    expect(wrapper.containsMatchingElement(dateFromLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(dateToLabel)).toEqual(true);
  });
});
