import React from "react";
import { shallow } from "enzyme";
import { unconnectedResultsReport as ResultsReport } from "../../../../components/commons/reports/ResultsReport";
import LOCALIZE from "../../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(
    <ResultsReport
      getTestOrderNumbers={() => {}}
      reportRequestor={"Report Tester"}
      currentlyLoading={true}
    />,
    {
      disableLifecycleMethods: true
    }
  );

  it("renders no dropdowns at all (initial states)", () => {
    const testOrderNumberLabel = (
      <label id="test-order-number-label">{LOCALIZE.reports.testOrderNumberLabel}</label>
    );
    const testLabel = <label id="test-label">{LOCALIZE.reports.testsLabel}</label>;
    const CandidateLabel = <label id="candidate-label">{LOCALIZE.reports.candidateLabel}</label>;
    // should only display report (label and dropdown), since all other labels are hidden
    expect(wrapper.containsMatchingElement(testOrderNumberLabel)).toEqual(false);
    expect(wrapper.containsMatchingElement(testLabel)).toEqual(false);
    expect(wrapper.containsMatchingElement(CandidateLabel)).toEqual(false);
    expect(wrapper.find("#unit-test-test-order-number-dropdown").exists()).toEqual(false);
    expect(wrapper.find("#unit-test-test-dropdown").exists()).toEqual(false);
    expect(wrapper.find("#unit-test-candidate-dropdown").exists()).toEqual(false);
  });

  it("renders test order number dropdown (when report type has been selected)", () => {
    // simulating that report type (option 1 - Individual Score Sheet) has been selected
    wrapper.setState({
      testOrderNumberVisible: true
    });

    const testOrderNumberLabel = (
      <label id="test-order-number-label">{LOCALIZE.reports.testOrderNumberLabel}</label>
    );
    const testLabel = <label id="test-label">{LOCALIZE.reports.testsLabel}</label>;
    const CandidateLabel = <label id="candidate-label">{LOCALIZE.reports.candidateLabel}</label>;
    expect(wrapper.containsMatchingElement(testOrderNumberLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(testLabel)).toEqual(false);
    expect(wrapper.containsMatchingElement(CandidateLabel)).toEqual(false);
    expect(wrapper.find("#unit-test-test-order-number-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-test-dropdown").exists()).toEqual(false);
    expect(wrapper.find("#unit-test-candidate-dropdown").exists()).toEqual(false);
  });

  it("renders test order number and test dropdowns (when test order number has been selected)", () => {
    // simulating that test order number has been selected
    wrapper.setState({
      testVisible: true
    });

    const testOrderNumberLabel = (
      <label id="test-order-number-label">{LOCALIZE.reports.testOrderNumberLabel}</label>
    );
    const testLabel = <label id="test-label">{LOCALIZE.reports.testsLabel}</label>;
    const CandidateLabel = <label id="candidate-label">{LOCALIZE.reports.candidateLabel}</label>;
    expect(wrapper.containsMatchingElement(testOrderNumberLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(testLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(CandidateLabel)).toEqual(false);
    expect(wrapper.find("#unit-test-test-order-number-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-test-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-candidate-dropdown").exists()).toEqual(false);
  });
});
