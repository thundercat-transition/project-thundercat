import React from "react";
import { mount } from "enzyme";
import NextPreviousButtonNav from "../../../components/commons/NextPreviousButtonNav";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight, faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
const mockStore = configureStore();

const initialState = {
  localize: {
    language: "en"
  },
  accommodations: {
    fontFamily: "Nunito Sans"
  }
};
it("renders next button", () => {
  const wrapper = mount(
    <Provider store={mockStore(initialState)}>
      <NextPreviousButtonNav
        showPrevious={false}
        showNext={true}
        onChangeToPrevious={() => {}}
        onChangeToNext={() => {}}
      />
    </Provider>
  );
  expect(wrapper.containsMatchingElement(<FontAwesomeIcon icon={faChevronRight} />)).toEqual(true);
  expect(wrapper.containsMatchingElement(<FontAwesomeIcon icon={faChevronLeft} />)).toEqual(false);
});

it("renders previous button", () => {
  const wrapper = mount(
    <Provider store={mockStore(initialState)}>
      <NextPreviousButtonNav
        showPrevious={true}
        showNext={false}
        onChangeToPrevious={() => {}}
        onChangeToNext={() => {}}
      />
    </Provider>
  );
  expect(wrapper.containsMatchingElement(<FontAwesomeIcon icon={faChevronLeft} />)).toEqual(true);
  expect(wrapper.containsMatchingElement(<FontAwesomeIcon icon={faChevronRight} />)).toEqual(false);
});
