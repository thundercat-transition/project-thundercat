import React from "react";
import { shallow } from "enzyme";
import { unconnectedPreferences as Preferences } from "../../../components/profile/Preferences";
import LOCALIZE from "../../../text_resources";

//TODO (fnormand): add more tests once the component will have more functionalities

describe("renders component content", () => {
  const wrapper = shallow(<Preferences />);

  it("renders page title", () => {
    const title = <h2>{LOCALIZE.profile.preferences.title}</h2>;
    expect(wrapper.containsMatchingElement(title)).toEqual(true);
  });

  // TODO: Uncomment when we'll put back these preferences
  // it("renders page subtitles", () => {
  //   const subtitle1 = <span>{LOCALIZE.profile.preferences.notifications.title}</span>;
  //   const subtitle2 = <span>{LOCALIZE.profile.preferences.display.title}</span>;
  //   expect(wrapper.containsMatchingElement(subtitle1)).toEqual(true);
  //   expect(wrapper.containsMatchingElement(subtitle2)).toEqual(true);
  // });
});
