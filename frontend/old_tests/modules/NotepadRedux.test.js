import notepad, { setNotepadContent, resetNotepadState } from "../../modules/NotepadRedux";

describe("Notepad", () => {
  const initialState = { notepadContent: "" };

  it("sets content", () => {
    // set the notepad
    const currentNotepadAction = setNotepadContent("emiPizzaTest");
    const currentTestState = notepad(initialState, currentNotepadAction);
    expect(currentTestState).toEqual({
      notepadContent: "emiPizzaTest"
    });
  });

  it("resets state", () => {
    // reset the state
    setNotepadContent("emiPizzaTest");
    const currentTestState = notepad(initialState, resetNotepadState);
    expect(currentTestState).toEqual({
      ...initialState
    });
  });
});
