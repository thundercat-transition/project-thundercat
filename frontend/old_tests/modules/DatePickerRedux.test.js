import datePicker, {
  initialState,
  updateDatePicked,
  updateDatePickedValidState
} from "../../modules/DatePickerRedux";

describe("testing actions", () => {
  it("gets initial states", () => {
    expect(initialState).toEqual({
      completeDatePicked: `${null}-${null}-${null}`,
      completeDateValidState: false
    });
  });

  it("updates date picked", () => {
    const action1 = updateDatePicked("2020-12-31");
    expect(datePicker(initialState, action1)).toEqual({
      completeDatePicked: "2020-12-31",
      completeDateValidState: false
    });
    const action2 = updateDatePicked("1980-01-25");
    expect(datePicker(initialState, action2)).toEqual({
      completeDatePicked: "1980-01-25",
      completeDateValidState: false
    });
  });

  it("updates date picked valid state", () => {
    const action = updateDatePickedValidState(true);
    expect(datePicker(initialState, action)).toEqual({
      completeDatePicked: "null-null-null",
      completeDateValidState: true
    });
  });
});
