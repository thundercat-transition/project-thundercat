// Jest Settings
module.exports = {
  verbose: true,
  testEnvironment: "jest-environment-jsdom",
  resetMocks: false,
  transformIgnorePatterns: [
    "[/\\\\]node_modules[/\\\\](?!(react-markdown|trim-lines|rehype-raw|zwitch|hast-util-raw|html-void-elements|hast-to-hyperscript|hast-util-to-parse5|hast-util-from-parse5|hastscript|web-namespaces|hast-util-parse-selector|vfile|vfile-message|markdown-table|unist-.*|unified|bail|is-plain-obj|trough|remark-.*|mdast-util-.*|escape-string-regexp|micromark.*|decode-named-character-reference|character-entities|property-information|hast-util-whitespace|space-separated-tokens|comma-separated-tokens|pretty-bytes|ccount)).+\\.(js|jsx|mjs|cjs|ts|tsx|png)$"
  ],
  transform: {
    "^.+\\.(js|jsx)?$": "babel-jest"
  },
  coverageDirectory: "coverage",
  collectCoverageFrom: ["src/**/*.{js,jsx}"],
  moduleNameMapper: {
    "\\.(css|less|scss|sass|xlsx)$": "identity-obj-proxy",
    "^lodash-es$": "lodash",
    "^.+\\.(png|jpg|ttf|woff|woff2|xlsx)$": "jest-transform-stub",
    "src/(.*)": "<rootDir>/src/$1"
  },
  setupFiles: ["./src/jest.setup.js"],
  setupFilesAfterEnv: ["./src/setupTests.js"],
  reporters: [
    "default",
    [
      "jest-html-reporter",
      {
        pageTitle: "Test Report",
        outputPath: "test-report.html",
        includeFailureMsg: true,
        includeConsoleLog: true
      }
    ]
  ]
};
