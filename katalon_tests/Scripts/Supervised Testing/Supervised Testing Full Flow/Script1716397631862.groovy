import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//this will run the full steps in order to get a supervised test done by a user
//make sure Ta two exists and has the testcoordinator permission
// check if they have the permision if not it will 
// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to user
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

boolean taTwoPermissionAlreadyAssociated = CustomKeywords.'database.DataValidation.userPermissionAlreadyAssociated'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, 
    GlobalVariable.TA_PERMISSION)

// if taPermissionAlreadyAssociated is false
if (!(taTwoPermissionAlreadyAssociated)) {
    WebUI.callTestCase(findTestCase('01. Accounts and Permissions Setup/1.2 Permissions/1.2.1 Send Permission Requests/1.2.1.4-Send-Permissions-TA-User-Two'), 
        [:], FailureHandling.STOP_ON_FAILURE)

    WebUI.callTestCase(findTestCase('01. Accounts and Permissions Setup/1.2 Permissions/1.2.2 Approve Permission Requests/1.2.2.4-Approve-Permissions-TA-User-Two'), 
        [:], FailureHandling.STOP_ON_FAILURE)
}

//check if the katalon test centre exists, if not make it
boolean testCentreAlreadyExists = CustomKeywords.'database.DataValidation.testCenterExists'(GlobalVariable.TEST_CENTRE_NAME)

if (!(testCentreAlreadyExists)) {
    WebUI.callTestCase(findTestCase('Supervised Testing/Create Test Centre'), [:], FailureHandling.STOP_ON_FAILURE)
	//add a coordiator to the test centre
	WebUI.callTestCase(findTestCase('Supervised Testing/Assign Test Centre Coordinator To Test Centre'), [:], FailureHandling.STOP_ON_FAILURE)
	//add the room to the test centre
	WebUI.callTestCase(findTestCase('Supervised Testing/Add Room to Test Centre'), [:], FailureHandling.STOP_ON_FAILURE)
	// assign a TA to test centre
	WebUI.callTestCase(findTestCase('Supervised Testing/Assign TA to Test Centre'), [:], FailureHandling.STOP_ON_FAILURE)
	// assign TA to be able to admin tests
	WebUI.callTestCase(findTestCase('Supervised Testing/Assign TA to the Test'), [:], FailureHandling.STOP_ON_FAILURE)
}

//create the test session 
WebUI.callTestCase(findTestCase('Supervised Testing/Create Test Session'), [:], FailureHandling.STOP_ON_FAILURE)

//check if there is a billing contact if not then make one

boolean billingContactAlreadyExists = CustomKeywords.'database.DataValidation.billingContactExists'('Katalon ','Billing')
if(!(billingContactAlreadyExists)) {
	WebUI.callTestCase(findTestCase('Supervised Testing/Add Billing Contact'), [:], FailureHandling.STOP_ON_FAILURE)
}

//create assement process
WebUI.callTestCase(findTestCase('Supervised Testing/Create Assessment Process'), [:], FailureHandling.STOP_ON_FAILURE)

//add candidate to the assement
WebUI.callTestCase(findTestCase('Supervised Testing/Add Candidate to Assessment Process'), [:], FailureHandling.STOP_ON_FAILURE)

//user reserves test
WebUI.callTestCase(findTestCase('Supervised Testing/User Reserves Test'), [:], FailureHandling.STOP_ON_FAILURE)

//TA generates test access code
WebUI.callTestCase(findTestCase('Supervised Testing/TA Generates Test Access Code'), [:], FailureHandling.STOP_ON_FAILURE)

//user checks in and writes test
WebUI.callTestCase(findTestCase('Supervised Testing/User checks in with Access code and writes test'), [:], FailureHandling.STOP_ON_FAILURE)
