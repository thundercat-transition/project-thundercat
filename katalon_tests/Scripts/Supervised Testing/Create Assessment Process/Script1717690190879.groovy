import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'basics.utils.NavigateToHomePage'()

// login
CustomKeywords.'basics.utils.login'(GlobalVariable.SUPER_USER_USERNAME, GlobalVariable.SUPER_USER_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_HR Coordinator'))

// adding a 2 seconds delay
WebUI.delay(2)
WebUI.setText(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Assessment Process Reference Number'), GlobalVariable.REFERENCE_NUMBER)

// adding a 2 seconds delay
WebUI.delay(2)
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Select_Org_Dropdown'), Keys.chord('(PSC)', 
        Keys.ENTER))

// adding a 2 seconds delay
WebUI.delay(2)
WebUI.setText(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Process Duration (calendar days)'), '5')

WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/div_concat Allow reservations at other org (1)'))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Select_Billing Contact'), Keys.chord('Katalon', 
        Keys.ENTER))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Contact Email for Candidates'), Keys.chord(
        'simulate-delivered@notification.canada.ca', Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_Test to Administer'))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Select_Organization'), Keys.chord('second', 
        Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Select_Test'), 'English - W')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Select_Test'), Keys.chord(Keys.ENTER))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Select_Reason for test'), Keys.chord('staff', 
        Keys.ENTER))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Default Required Level_create-process_d8e9d4'), 
    Keys.chord('A', Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_Add Test'))

WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_Create Process'))

WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_OK'))

//close browser
WebUI.closeBrowser()

