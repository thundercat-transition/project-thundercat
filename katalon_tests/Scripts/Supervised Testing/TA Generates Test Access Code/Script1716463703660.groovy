import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import basics.DateUtil as DateUtil
import java.util.Calendar as Calendar
import java.time.LocalDate as LocalDate
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.text.DecimalFormat as DecimalFormat

CustomKeywords.'basics.utils.NavigateToHomePage'()

// login
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Supervised Testing'))

def testDate = LocalDate.now()

//get year, month and day
def testYear = testDate.year

def testMonth = testDate.monthValue

def testDay = testDate.dayOfMonth

// use todays date to find the test session
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_react-select-12-input'), Keys.chord(
        testDay + ' ', Keys.ENTER))

//generate the test access code
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Generate Test Access Code'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_react-select-15-input'), '654')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_react-select-15-input'), Keys.chord(
        Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Generate'))

//close browser
WebUI.closeBrowser()