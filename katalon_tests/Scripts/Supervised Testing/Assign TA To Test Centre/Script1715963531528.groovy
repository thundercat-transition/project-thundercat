import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'basics.utils.NavigateToHomePage'()

// login
new basics.utils().login(GlobalVariable.SUPER_USER_USERNAME, GlobalVariable.SUPER_USER_ENCRYPTED_PASSWORD)

WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_Test Center Administration'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Search_test-centers-search-bar'), 'Katalon')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Search_test-centers-search-bar'), Keys.chord(
		Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_Public Service Commission list'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/button_Add a Test Centre Coordinator'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/div_Select'))

//pick the proper TA
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/input_Select_Test_Coordinator'), Keys.chord(
        GlobalVariable.TA_USER_LAST_NAME,", ", GlobalVariable.TA_USER_FIRST_NAME, Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/button_Save'))

//logout and close
CustomKeywords.'basics.utils.logout'()
WebUI.closeBrowser()
