import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'basics.utils.NavigateToHomePage'()

// login
CustomKeywords.'basics.utils.login'(GlobalVariable.SUPER_USER_USERNAME, GlobalVariable.SUPER_USER_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_HR Coordinator'))

WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_Manage Billing'))

WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_Add'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_First Name_first-name'), 'Katalon ')

WebUI.setText(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Last Name_last-name'), 'Billing')

WebUI.setText(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Email_email'), 'simulate-delivered@notification.canada.ca')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Select_Organization'), Keys.chord('(PSC)', 
        Keys.ENTER))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_IS - Organization Code_fis-organisation-code'), 
    Keys.chord('ORGCODE', Keys.ENTER))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_IS - Reference Code_fis-reference-code'), 
    Keys.chord('REFCODE', Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_Add_1'))

//logout and close
CustomKeywords.'basics.utils.logoutAndClose'()

