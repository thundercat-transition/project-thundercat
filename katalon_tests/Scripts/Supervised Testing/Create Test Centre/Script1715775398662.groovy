import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'basics.utils.NavigateToHomePage'()


//check if the test centre exists
boolean testCentreAlreadyExists = CustomKeywords.'database.DataValidation.testCenterExists'(GlobalVariable.TEST_CENTRE_NAME)

if(!(testCentreAlreadyExists)) {

	// login as Superuser
	new basics.utils().login(GlobalVariable.SUPER_USER_USERNAME, GlobalVariable.SUPER_USER_ENCRYPTED_PASSWORD)
	
	WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Test Center Administration'))
	
	WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/span_New Test Centre'))
	
	WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Test_Centre_Name'), GlobalVariable.TEST_CENTRE_NAME)
	
	WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Select_Test_Centre_Org'), Keys.chord('(PSC)',
			Keys.ENTER))
	
	WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_Create'))
	
	WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_OK'))
	// logout
	CustomKeywords.'basics.utils.logout'()
	// make sure the reservation time is 0
	
	// login
	new basics.utils().login(GlobalVariable.SUPER_USER_USERNAME, GlobalVariable.SUPER_USER_ENCRYPTED_PASSWORD)
	
	WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/button_Katalon Test Centre_primary undefined'))
	
	//select test centre magangment
	WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/button_Test Centre Management (1)'))
	
	WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/div_Commissionnaires Email'))
	
	WebUI.setText(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/input_Reservation deadline (hours before te_fdd5e7'),
		'0')
	
	WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/input_Reservation deadline (hours before te_fdd5e7'),
		Keys.chord(Keys.ENTER))
	
	WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/button_Apply'))
}
// closing browser
WebUI.closeBrowser()