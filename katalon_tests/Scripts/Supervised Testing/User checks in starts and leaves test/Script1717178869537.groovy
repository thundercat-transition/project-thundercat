import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'basics.utils.NavigateToHomePage'()

//get access code
String testAccessCode = CustomKeywords.'database.DataGetting.getSupervisedTestAccessCode'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

CustomKeywords.'basics.candidate_utils.candidateCheckIn'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, testAccessCode)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

WebUI.click(findTestObject('Object Repository/Page_CAT - RD Dashboard/button_My Tests'))
WebUI.delay(2)
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Start'))

WebUI.delay(2)
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/button_Return to Home Page'))

// Logout and close
CustomKeywords.'basics.utils.logoutAndClose'()
