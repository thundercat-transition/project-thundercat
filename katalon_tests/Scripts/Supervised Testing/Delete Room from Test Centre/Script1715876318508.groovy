import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//view the test centre
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/button_View_Test_centre_Actions'))

//select test centre magangment
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/button_Test Centre Management'))

//go to the rooms tab
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/a_Rooms'))

//select the delete button
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/button_View_Test_centre_Actions'))
//confirm the delete
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/button_Delete_confirmation'))

//logout and close
CustomKeywords.'basics.utils.logout'()
WebUI.closeBrowser()