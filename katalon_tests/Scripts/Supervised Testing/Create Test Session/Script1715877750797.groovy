import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import basics.DateUtil as DateUtil
import java.util.Calendar as Calendar
import java.time.LocalDate as LocalDate
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.text.DecimalFormat as DecimalFormat



//check if the test session exists
//if it does, delete the test session
//boolean testSessionAlreadyExists = CustomKeywords.'database.DataValidation.testSessionExists'(GlobalVariable.TEST_CENTRE_NAME)
//testSessionAlreadyExists =false
//if (testSessionAlreadyExists) {
//    WebUI.callTestCase(findTestCase('Supervised Testing/Delete Test Session'), [:], FailureHandling.STOP_ON_FAILURE)
//}

try {
	CustomKeywords.'basics.utils.NavigateToHomePage'()
	CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
	
	WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/button_Katalon Test Centre_primary undefined'))
	
	WebUI.verifyElementText(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/label_There are no Test Sessions'),
		'There are no Test Sessions')
}
catch (Exception e) {
	WebUI.callTestCase(findTestCase('Supervised Testing/Delete Test Session'), [:], FailureHandling.STOP_ON_FAILURE)
	CustomKeywords.'basics.utils.NavigateToHomePage'()
	// login
	CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
	
	WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/button_Katalon Test Centre_primary undefined'))
	}




WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/button_Add Test Session'))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/input_Select_ Test Centre Option'), 
    Keys.chord('Katalon', Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/div_Active_react-switch-handle'))

def testDate = LocalDate.now()

//get year, month and day
def testYear = testDate.year

def testMonth = testDate.monthValue

def testDay = testDate.dayOfMonth

//make todays date the test date
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/input_Select_Date Day'), Keys.chord(
        "$testDay", Keys.ENTER))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/input_Select_Date Month'), Keys.chord(
        "$testMonth", Keys.ENTER))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/input_Select_Date Year'), Keys.chord(
        "$testYear", Keys.ENTER))

//enter the hours for start and finish
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/input_Select_Start Hour'), Keys.chord(
        '18', Keys.ENTER))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/input_Select_Start Minute'), Keys.chord(
        '0', Keys.ENTER))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/input_Select_End Hour'), Keys.chord(
        '22', Keys.ENTER))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/input_Select_Test End Minute'), Keys.chord(
        '0', Keys.ENTER))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/input_Spaces Available'), Keys.chord(
        '2', Keys.ENTER))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/input_Select_Test Type'), Keys.chord(
        'second', Keys.ENTER))

WebUI.delay(2)

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/input_Select_Test'), Keys.chord('english - w', 
        Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Centre Coordinator/button_Save'))

WebUI.delay(2)

//close browser
WebUI.closeBrowser()

