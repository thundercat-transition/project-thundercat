import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== STANDARD USER ONE ==========
// check if Standard User one exists
boolean standardUserOneAlreadyExists = CustomKeywords.'database.DataValidation.accountAlreadyExists'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// if the user does not exist
if (!(standardUserOneAlreadyExists)) {
    // create new account
    CustomKeywords.'basics.utils.createNewAccount'(GlobalVariable.STANDARD_USER_ONE_FIRST_NAME, GlobalVariable.STANDARD_USER_ONE_LAST_NAME, 
        GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, true)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== STANDARD USER ONE (END) ==========

// closing browser
WebUI.closeBrowser()

