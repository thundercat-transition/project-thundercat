import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== STANDARD USER ONE ==========
// check if Standard User one exists
boolean standardUserOneAlreadyExists = CustomKeywords.'database.DataValidation.accountAlreadyExists'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// if the user does not exist
if (!(standardUserOneAlreadyExists)) {
    // create new account
    CustomKeywords.'basics.utils.createNewAccount'(GlobalVariable.STANDARD_USER_ONE_FIRST_NAME, GlobalVariable.STANDARD_USER_ONE_LAST_NAME, 
        GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, true)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== STANDARD USER ONE (END) ==========
// ========== STANDARD USER TWO ==========
// check if Standard User two exists
boolean standardUserTwoAlreadyExists = CustomKeywords.'database.DataValidation.accountAlreadyExists'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

// if the user does not exist
if (!(standardUserTwoAlreadyExists)) {
    // create new account
    CustomKeywords.'basics.utils.createNewAccount'(GlobalVariable.STANDARD_USER_TWO_FIRST_NAME, GlobalVariable.STANDARD_USER_TWO_LAST_NAME, 
        GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== STANDARD USER TWO (END) ==========
// ========== STANDARD USER THREE ==========
// check if Standard User three exists
boolean standardUserThreeAlreadyExists = CustomKeywords.'database.DataValidation.accountAlreadyExists'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// if the user does not exist
if (!(standardUserThreeAlreadyExists)) {
    // create new account
    CustomKeywords.'basics.utils.createNewAccount'(GlobalVariable.STANDARD_USER_THREE_FIRST_NAME, GlobalVariable.STANDARD_USER_THREE_LAST_NAME, 
        GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== STANDARD USER THREE (END) ==========
// ========== ETTA USER ==========
// check if ETTA user exists
boolean ettaUserAlreadyExists = CustomKeywords.'database.DataValidation.accountAlreadyExists'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS)

// if the user does not exist
if (!(ettaUserAlreadyExists)) {
    // create new account
    CustomKeywords.'basics.utils.createNewAccount'(GlobalVariable.ETTA_USER_FIRST_NAME, GlobalVariable.ETTA_USER_LAST_NAME, 
        GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== ETTA USER (END) ==========
// ========== PPC USER ==========
// check if PPC user exists
boolean ppcUserAlreadyExists = CustomKeywords.'database.DataValidation.accountAlreadyExists'(GlobalVariable.PPC_USER_EMAIL_ADDRESS)

// if the user does not exist
if (!(ppcUserAlreadyExists)) {
    // create new account
    CustomKeywords.'basics.utils.createNewAccount'(GlobalVariable.PPC_USER_FIRST_NAME, GlobalVariable.PPC_USER_LAST_NAME, 
        GlobalVariable.PPC_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== PPC USER (END) ==========
// ========== TA USER ==========
// check if TA user exists
boolean taUserAlreadyExists = CustomKeywords.'database.DataValidation.accountAlreadyExists'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

// if the user does not exist
if (!(taUserAlreadyExists)) {
    // create new account
    CustomKeywords.'basics.utils.createNewAccount'(GlobalVariable.TA_USER_FIRST_NAME, GlobalVariable.TA_USER_LAST_NAME, 
        GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// check if TA Two user exists
boolean taTwoUserAlreadyExists = CustomKeywords.'database.DataValidation.accountAlreadyExists'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS)

// if the user does not exist
if (!(taTwoUserAlreadyExists)) {
    // create new account
    CustomKeywords.'basics.utils.createNewAccount'(GlobalVariable.TA_USER_TWO_FIRST_NAME, GlobalVariable.TA_USER_TWO_LAST_NAME, 
        GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// closing browser
WebUI.closeBrowser()

