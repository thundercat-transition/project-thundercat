import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== APPROVE PPC PERMISSION ==========
// check if the user already has the permission
alreadyHasPpcPermission = CustomKeywords.'database.DataValidation.isUserActivePermissionExist'(GlobalVariable.PPC_USER_EMAIL_ADDRESS, 
    GlobalVariable.PPC_PERMISSION)

// if user doesn't have the permission
if (!(alreadyHasPpcPermission)) {
    // approve ppc permission
    CustomKeywords.'basics.permissions.approvePermissionRequest'(GlobalVariable.PPC_USER_EMAIL_ADDRESS, GlobalVariable.PPC_USER_FIRST_NAME, 
        GlobalVariable.PPC_USER_LAST_NAME, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// close browser
WebUI.closeBrowser()

