import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== APPROVE TA 1 PERMISSION ==========
// check if the user already has the permission
boolean alreadyHasTaPermission = CustomKeywords.'database.DataValidation.isUserActivePermissionExist'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
    GlobalVariable.TA_PERMISSION)

boolean alreadyHasAdaptionsPermission = CustomKeywords.'database.DataValidation.isUserActivePermissionExist'(GlobalVariable.TA_USER_EMAIL_ADDRESS,
	GlobalVariable.ADAPTATIONS_PERMISSION)

// if user doesn't have the permission

if (!(alreadyHasTaPermission) || !(alreadyHasAdaptionsPermission)) {
    // approve ta permission
    CustomKeywords.'basics.permissions.approvePermissionRequest'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TA_USER_FIRST_NAME, 
        GlobalVariable.TA_USER_LAST_NAME, false)
}

if (!(alreadyHasTaPermission) || !(alreadyHasAdaptionsPermission)) {
	// approve ta permission
	CustomKeywords.'basics.permissions.approvePermissionRequest'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TA_USER_FIRST_NAME,
		GlobalVariable.TA_USER_LAST_NAME, false)
}

// logout
CustomKeywords.'basics.utils.logout'()
// close browser
WebUI.closeBrowser()

