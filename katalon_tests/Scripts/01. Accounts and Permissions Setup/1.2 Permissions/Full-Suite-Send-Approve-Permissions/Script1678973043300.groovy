import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// ========== ETTA PERMISSION REQUEST ==========
// check if the permission is already requested or associated to this account
boolean ettaPermissionAlreadyAssociated = CustomKeywords.'database.DataValidation.userPermissionAlreadyAssociated'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, 
    GlobalVariable.ETTA_PERMISSION)

// if ettaPermissionAlreadyAssociated is false
if (!(ettaPermissionAlreadyAssociated)) {
    // login
    CustomKeywords.'basics.utils.login'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

    // send ETTA permission request
    CustomKeywords.'basics.permissions.sendPermissionRequest'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.ETTA_PERMISSION, 
        false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== ETTA PERMISSION REQUEST (END) ==========
// ========== PPC PERMISSION REQUEST ==========
// check if the permission is already requested or associated to this account
boolean ppcPermissionAlreadyAssociated = CustomKeywords.'database.DataValidation.userPermissionAlreadyAssociated'(GlobalVariable.PPC_USER_EMAIL_ADDRESS, 
    GlobalVariable.PPC_PERMISSION)

// if ppcPermissionAlreadyAssociated is false
if (!(ppcPermissionAlreadyAssociated)) {
    // login
    CustomKeywords.'basics.utils.login'(GlobalVariable.PPC_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

    // send PPC permission request
    CustomKeywords.'basics.permissions.sendPermissionRequest'(GlobalVariable.PPC_USER_EMAIL_ADDRESS, GlobalVariable.PPC_PERMISSION, 
        false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== PPC PERMISSION REQUEST (END) ==========
// ========== TA PERMISSION REQUEST ==========
// check if the permission is already requested or associated to this account
boolean taPermissionAlreadyAssociated = CustomKeywords.'database.DataValidation.userPermissionAlreadyAssociated'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
    GlobalVariable.TA_PERMISSION)

// if taPermissionAlreadyAssociated is false
if (!(taPermissionAlreadyAssociated)) {
    // login
    CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

    // send TA permission request
    CustomKeywords.'basics.permissions.sendPermissionRequest'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TA_PERMISSION, 
        false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// check if the permission is already requested or associated to this account
boolean taTwoPermissionAlreadyAssociated = CustomKeywords.'database.DataValidation.userPermissionAlreadyAssociated'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, 
    GlobalVariable.TA_PERMISSION)

// if taPermissionAlreadyAssociated is false
if (!(taTwoPermissionAlreadyAssociated)) {
    // login
    CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

    // send TA permission request
    CustomKeywords.'basics.permissions.sendPermissionRequest'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, GlobalVariable.TA_PERMISSION, 
        false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== TA PERMISSION REQUEST (END) ==========
// ========== APPROVE ETTA PERMISSION ==========
// check if the user already has the permission
alreadyHasEttaPermission = CustomKeywords.'database.DataValidation.isUserActivePermissionExist'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, 
    GlobalVariable.ETTA_PERMISSION)

// if user doesn't have the permission
if (!(alreadyHasEttaPermission)) {
    // approve etta permission
    CustomKeywords.'basics.permissions.approvePermissionRequest'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.ETTA_USER_FIRST_NAME, 
        GlobalVariable.ETTA_USER_LAST_NAME, true)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== APPROVE ETTA PERMISSION (END) ==========
// ========== APPROVE PPC PERMISSION ==========
// check if the user already has the permission
alreadyHasPpcPermission = CustomKeywords.'database.DataValidation.isUserActivePermissionExist'(GlobalVariable.PPC_USER_EMAIL_ADDRESS, 
    GlobalVariable.PPC_PERMISSION)

// if user doesn't have the permission
if (!(alreadyHasPpcPermission)) {
    // approve ppc permission
    CustomKeywords.'basics.permissions.approvePermissionRequest'(GlobalVariable.PPC_USER_EMAIL_ADDRESS, GlobalVariable.PPC_USER_FIRST_NAME, 
        GlobalVariable.PPC_USER_LAST_NAME, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== APPROVE PPC PERMISSION (END) ==========
// ========== APPROVE TA PERMISSION ==========
// check if the user already has the permission
alreadyHasTaPermission = CustomKeywords.'database.DataValidation.isUserActivePermissionExist'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
    GlobalVariable.TA_PERMISSION)

// if user doesn't have the permission
if (!(alreadyHasTaPermission)) {
    // approve ta permission
    CustomKeywords.'basics.permissions.approvePermissionRequest'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TA_USER_FIRST_NAME, 
        GlobalVariable.TA_USER_LAST_NAME, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// check if the user two already has the permission
alreadyHasTaTwoPermission = CustomKeywords.'database.DataValidation.isUserActivePermissionExist'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, 
    GlobalVariable.TA_PERMISSION)

// if user doesn't have the permission
if (!(alreadyHasTaTwoPermission)) {
    // approve ta permission
    CustomKeywords.'basics.permissions.approvePermissionRequest'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS, GlobalVariable.TA_USER_TWO_FIRST_NAME, 
        GlobalVariable.TA_USER_TWO_LAST_NAME, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// ========== APPROVE TA PERMISSION (END) ==========
// close browser
WebUI.closeBrowser()

