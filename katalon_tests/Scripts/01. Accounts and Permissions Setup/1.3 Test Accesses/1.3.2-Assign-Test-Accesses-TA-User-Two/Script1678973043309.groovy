import internal.GlobalVariable as GlobalVariable

// check if the specified TA Two already has a test access
boolean taTwoAlreadyHasTestAccess = CustomKeywords.'database.DataValidation.taAlreadyHasTestAccess'(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS)


// if the TA Two does already have the test access
if (!(taTwoAlreadyHasTestAccess)) {
    // open browser and navigate to login page
    CustomKeywords.'basics.utils.NavigateToHomePage'()

    // assign test access
    // TODO before  February 1st 2026: Update the expiry date of the assigned test
    CustomKeywords.'basics.permissions.assignTestAccess'(false, false, 'Katalon', 'TATwo')

    // logout and close the browser
    CustomKeywords.'basics.utils.logoutAndClose'()
}