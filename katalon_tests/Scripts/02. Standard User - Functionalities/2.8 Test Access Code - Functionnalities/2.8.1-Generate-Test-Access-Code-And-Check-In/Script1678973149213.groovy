import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

// set a testOrderNumber
String testOrderNumber = 'KATALONTEST'

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to this user
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

// make sure that there is no active tests access code related to the TA user
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

// login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// select My Tests
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Tests'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Test Results'))

// make sure that there are no tests in the table
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Home/my_tests_page_No tests have been scored yet'), 
    2)

// logout
CustomKeywords.'basics.utils.logout'()

WebUI.delay(2)

WebUI.callTestCase(findTestCase('Supervised Testing/Supervised Testing Stop before Test Access Code'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Supervised Testing/TA Generates Test Access Code'), [:], FailureHandling.STOP_ON_FAILURE)

//get access code
String testAccessCode = CustomKeywords.'database.DataGetting.getSupervisedTestAccessCode'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

CustomKeywords.'basics.utils.NavigateToHomePage'()

WebUI.callTestCase(findTestCase('Supervised Testing/User checks in and exits'), [:], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'basics.utils.NavigateToHomePage'()

// delete assigned tests data related
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

WebUI.closeBrowser()

