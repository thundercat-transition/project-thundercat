import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// make sure that the French language button is available
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Home/button_Francais'), 2)

// click Français button
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Francais'))

// make sure that the English language button is available
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_OC - Accueil/button_English'), 2)

// close the browser
WebUI.closeBrowser()