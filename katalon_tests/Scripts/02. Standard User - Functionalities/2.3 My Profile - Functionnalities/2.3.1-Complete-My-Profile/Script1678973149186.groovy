import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import org.openqa.selenium.Keys as Keys
// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// delete user extended profile data
CustomKeywords.'database.DataDeletion.deleteUserExtendedProfileData'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// delete user permissions (to make sure that the verified state is set to false)
CustomKeywords.'database.DataDeletion.deleteUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// deleting user profile changes
new database.DataDeletion().deleteAllPendingUserProfileChangeRequest(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// login
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// select My Profile
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Profile (1)'))

// update form (with invalid inputs)
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_modify_form'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_First Name_popup-new-first-name-input'), GlobalVariable.STANDARD_USER_ONE_FIRST_NAME +
	' 1')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Last Name_popup-new-last-name-input'), GlobalVariable.STANDARD_USER_ONE_LAST_NAME +
	' 1')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Select_react-select-Day'), Keys.chord('31', Keys.ENTER))


WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Select_react-select-Month'), Keys.chord('02',Keys.ENTER))


WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Select_react-select-Year'), Keys.chord('1984',Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Submit'))
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/label_Must be a valid First Name'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/label_Must be a valid Last Name'), 0)

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_CAT - Profile/label_Must be a valid Date'))


WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/span_Cancel_form_edit'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Secondary Email Address (optional)_se_a055cb'), 
    'secondary.emailemail.ca')



WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_PSRS Applicant ID (optional)_psrs-app_345856'), 
    'A123')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_PRI or Service Number (optional)_pri-_365ad8'), 
    '1234')

// click Save
WebUI.click(findTestObject('Page_CAT - Profile/button_Save_Personal_info'))

// make sure that respective error messages are displayed
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/label_Must be a valid Email Address'), 2)


WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/label_Must be a valid PSRS Applicant ID'), 
    2)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Profile/label_Must be a valid PRI'), 
    2)

// update form (with valid inputs)
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_modify_form'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_First Name_popup-new-first-name-input'), GlobalVariable.STANDARD_USER_ONE_FIRST_NAME +
	' Updated')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Last Name_popup-new-last-name-input'), GlobalVariable.STANDARD_USER_ONE_LAST_NAME +
	' Updated')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Select_react-select-Day'), Keys.chord('31', Keys.ENTER))


WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Select_react-select-Month'), Keys.chord('01',Keys.ENTER))


WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Select_react-select-Year'), Keys.chord('1984',Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Submit'))
//close
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Close'))


WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Secondary Email Address (optional)_se_a055cb'), 
    'secondary.email@email.ca')


WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_PSRS Applicant ID (optional)_psrs-app_345856'), 
    'A654321')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_PRI or Service Number (optional)_pri-_365ad8'), 
    '098834630')

WebUI.click(findTestObject('Page_CAT - Profile/input_No_aboriginal-radio-buttons'))

WebUI.click(findTestObject('Page_CAT - Profile/input_No_disability-radio-buttons'))

WebUI.click(findTestObject('Page_CAT - Profile/input_No_identify-as-woman-radio-buttons'))

WebUI.click(findTestObject('Page_CAT - Profile/input_No_visible-minority-radio-buttons'))

// click Save
WebUI.click(findTestObject('Page_CAT - Profile/button_Save_Personal_info'))

// click OK
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_OK'))

// refresh the page
WebUI.refresh()


// make sure that the personal information data has been updated successfully
// a request will have been made but the current information is not yet updated

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_modify_form'))


WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_CAT - Profile/input_First Name_popup-new-first-name-input'), 
    'value', GlobalVariable.STANDARD_USER_ONE_FIRST_NAME +
	' Updated', 0)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Page_CAT - Profile/input_Last Name_popup-new-last-name-input'), 
    'value', GlobalVariable.STANDARD_USER_ONE_LAST_NAME +
	' Updated', 0)

//verify date
WebUI.verifyElementText(findTestObject('Object Repository/Page_CAT - Profile/div_31'), '31')

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Submit'))

WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/Page_CAT - Profile/input_Secondary Email Address (optional)_se_a055cb'), 
        'value'), 'secondary.email@email.ca')


WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/Page_CAT - Profile/input_PSRS Applicant ID (optional)_psrs-app_345856'), 
        'value'), 'A654321')

WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/Page_CAT - Profile/input_PRI or Service Number (optional)_pri-_365ad8'), 
        'value'), '098834630')

// updating additional information data (selecting value = 1 for all dropdowns)
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_Please select_css-1gtu0rj-indicatorContainer (1)'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_Crown corporation or agency employee'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_Please select_css-1gtu0rj-indicatorContainer_1'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_AAA Full Name'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_NA_css-1gtu0rj-indicatorContainer'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_AA'))


//By default, NA is already selected; no need for this step right now

	//WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_NA_css-1gtu0rj-indicatorContainer_1'))	
	//WebUI.click(findTestObject('Page_CAT - Profile/div_NA_1'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_Please select_css-1gtu0rj-indicatorContainer_1_2'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_Here'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_Please select_css-1gtu0rj-indicatorContainer_1_2_3'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_Community College  CEGEP 1 year'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_Please select_css-1gtu0rj-indicatorContainer_1_2_3_4'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/div_Female'))

// click Save
WebUI.click(findTestObject('Page_CAT - Profile/button_Save_Personal_info'))

// click OK
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_OK'))

// adding small delay
WebUI.delay(2)

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()

CustomKeywords.'basics.utils.NavigateToHomePage'()

CustomKeywords.'database.DataDeletion.deleteUserExtendedProfileData'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

// delete user permissions (to make sure that the verified state is set to false)
CustomKeywords.'database.DataDeletion.deleteUserPermissions'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

// login
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// select My Profile
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Profile (1)'))

// update form (with invalid inputs)
WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_First Name_first-name-input'), GlobalVariable.STANDARD_USER_ONE_FIRST_NAME + 
    ' 1')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Last Name_last-name-input'), GlobalVariable.STANDARD_USER_ONE_LAST_NAME + 
    ' 1')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Secondary Email Address (optional)_se_a055cb'), 
    'secondary.emailemail.ca')

// update form (with valid inputs)
WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_First Name_first-name-input'), GlobalVariable.STANDARD_USER_TWO_FIRST_NAME + 
    ' Updated')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Last Name_last-name-input'), GlobalVariable.STANDARD_USER_TWO_LAST_NAME + 
    ' Updated')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Secondary Email Address (optional)_se_a055cb'), 
    'secondary.email@email.ca')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_PSRS Applicant ID (optional)_psrs-app_345856'), 
    'A654321')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_PRI or Service Number (optional)_pri-_365ad8'), 
    '098834630')

WebUI.click(findTestObject('Page_CAT - Profile/input_No_aboriginal-radio-buttons'))

WebUI.click(findTestObject('Page_CAT - Profile/input_No_disability-radio-buttons'))

WebUI.click(findTestObject('Page_CAT - Profile/input_No_identify-as-woman-radio-buttons'))

WebUI.click(findTestObject('Page_CAT - Profile/input_No_visible-minority-radio-buttons'))

// click Save
WebUI.click(findTestObject('Page_CAT - Profile/button_Save_Personal_info'))

// click OK
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_OK'))

CustomKeywords.'basics.utils.logoutAndClose'()

