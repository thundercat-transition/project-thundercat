import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

// set a testOrderNumber
String testOrderNumber = 'KATALONTEST'

// delete any data that might be presnet that will affect this test case
try {
	new database.DataDeletion().deleteTaTestAccessCodes(GlobalVariable.TA_USER_EMAIL_ADDRESS)
}
	catch (Exception e) {
		// if the data hasnt been filed out yet due to an error there will be nothing to delete
		System.out.println('There was nothing to delete from TA test access codes')
	}
try {
	new database.DataDeletion().deleteUITFromTA(GlobalVariable.TA_USER_EMAIL_ADDRESS)
	}
	catch (Exception e) {
		// if the data hasnt been filed out yet due to an error there will be nothing to delete
		System.out.println('There was nothing to delete from UIT')
	}
try {
	new database.DataDeletion().deleteUserTestPermission(GlobalVariable.TA_USER_EMAIL_ADDRESS, testOrderNumber)
	}
	catch (Exception e) {
		// if the data hasnt been filed out yet due to an error there will be nothing to delete
		System.out.println('There was nothing to delete from test permisions')
	}
	



//// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

CustomKeywords.'basics.ta_utils.assignTestAccess'( GlobalVariable.TA_USER_FIRST_NAME,  GlobalVariable.TA_USER_LAST_NAME, testOrderNumber)
CustomKeywords.'basics.utils.logout'()


//test the ta send test invite
WebUI.delay(2)
CustomKeywords.'basics.ta_utils.sendTestInvite'(GlobalVariable.TA_USER_EMAIL_ADDRESS, testOrderNumber)


CustomKeywords.'basics.utils.logout'()

// getting test access code directly from DB (since we cannot get it from the emails)
String testAccessCode = CustomKeywords.'database.DataGetting.getUITTestAccessCode'(testOrderNumber)
CustomKeywords.'basics.candidate_utils.userRunsTest'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, testAccessCode)

// logout
CustomKeywords.'basics.utils.logout'()
// close the browser
WebUI.closeBrowser()
