import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import org.openqa.selenium.Keys as Keys
import basics.DateUtil as DateUtil
import java.util.Calendar as Calendar
import java.time.LocalDate as LocalDate
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.text.DecimalFormat as DecimalFormat

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to user
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no active test access codes
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

////Generate Test access codes using TA
////Generating  test access codes using API (Last argument number represents to generate number of test access codes here it generates 1)
//String[] testAccessCode = CustomKeywords.'api.api_requests.generateTestAccessCodeUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
//    GlobalVariable.USERS_ENCRYPTED_PASSWORD, 1)
//
////candidate check in
//CustomKeywords.'api.api_requests.candidateCheckInUsingAPI'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
//    testAccessCode[0])
WebUI.callTestCase(findTestCase('Supervised Testing/Supervised Testing Stop before Test Access Code'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Supervised Testing/TA Generates Test Access Code'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Supervised Testing/User checks in and exits'), [:], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'basics.utils.NavigateToHomePage'()

//login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)


WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Supervised Testing'))

def testDate = LocalDate.now()

//get day
def testDay = testDate.dayOfMonth

// use todays date to find the test session
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_react-select-12-input'), Keys.chord(
		testDay + ' ', Keys.ENTER))

WebUI.delay(2)

//Delete the generated test-access-code
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Delete_Generated_Test_Access_Code'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Delete_Test_Access_popup'))


// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

//Click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Checked-in_secondary_approve'))

//Adding delay
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Add_Edit Break'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_increment-hours-button'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Cancel'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Add_Edit Break'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_increment-hours-button'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_increment-hours-button'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Enter'))
////Click on the lock button
//WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Lock_Test_Access'))
//
////WebUI.click(findTestObject('Object Repository/Page_CAT - TA/input_one_pause-checkbox'))
////pause test for 1 minute
////WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg__svg-inline--fa fa-sort-up fa-w-10'))
//WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Lock the Test'))

//Adding delay 
WebUI.delay(1)

// logout
CustomKeywords.'basics.utils.logout'()

//login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

WebUI.click(findTestObject('Object Repository/Page_CAT - RD Dashboard/button_My Tests'))
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Start'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/button_Start Test'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/input_Start Test_procced-to-next-section-checkbox'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/button_Start Test_1'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test 634X1E/button_Pause Test'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_CAT - Test 634X1E/p_You have paused your test. It will resume_68efad'),
	'You have paused your test. It will resume when you select Resume Test or once the timer reaches 00:00:00.')

WebUI.click(findTestObject('Object Repository/Page_CAT - Test 634X1E/button_Quit Test'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test 634X1E/input_Quit Test_checkbox-condition-0'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test 634X1E/input_I choose to quit this test_checkbox-c_66e28e'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test 634X1E/input_I understand that my test will not be_55f094'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test 634X1E/button_Quit Test_1'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test QUIT/button_Return to Home Page'))

//Verify that the test is paused
//WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Test Welcome/label_Test Paused'), 2)
//WebUI.verifyElementText(findTestObject('Object Repository/Page_CAT - Test Welcome/label_Test Paused'), 'Test Locked')

WebUI.closeBrowser()

