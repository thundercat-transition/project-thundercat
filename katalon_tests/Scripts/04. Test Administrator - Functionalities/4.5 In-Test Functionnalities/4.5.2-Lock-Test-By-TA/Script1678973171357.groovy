import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import org.openqa.selenium.Keys as Keys
import basics.DateUtil as DateUtil
import java.util.Calendar as Calendar
import java.time.LocalDate as LocalDate
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.text.DecimalFormat as DecimalFormat

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to user
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

////Generate Test access codes using TA
////Generating  test access codes using API (Last argument number represents to generate number of test access codes here it generates 1)
//String[] testAccessCode = CustomKeywords.'api.api_requests.generateTestAccessCodeUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
//    GlobalVariable.USERS_ENCRYPTED_PASSWORD, 1)
//
////candidate check in
//CustomKeywords.'api.api_requests.candidateCheckInUsingAPI'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
//    testAccessCode[0])
WebUI.callTestCase(findTestCase('Supervised Testing/Supervised Testing Stop before Test Access Code'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Supervised Testing/TA Generates Test Access Code'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Supervised Testing/User checks in and exits'), [:], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'basics.utils.NavigateToHomePage'()
//login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Supervised Testing'))

def testDate = LocalDate.now()

//get day
def testDay = testDate.dayOfMonth

// use todays date to find the test session
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_react-select-12-input'), Keys.chord(
		testDay + ' ', Keys.ENTER))

WebUI.delay(2)

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

//Click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Checked-in_secondary_approve'))

// adding a 2 seconds delay
WebUI.delay(2)

// logout
CustomKeywords.'basics.utils.logout'()

//login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

WebUI.click(findTestObject('Object Repository/Page_CAT - RD Dashboard/button_My Tests'))
//Click on Start
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Start'))

WebUI.delay(2)
// logout
//CustomKeywords.'basics.utils.logout'()
// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login (using TA credentials)
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Supervised Testing'))

// use todays date to find the test session
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_react-select-12-input'), Keys.chord(
		testDay + ' ', Keys.ENTER))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

//Click on the lock button
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Lock_Test_Access'))

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Lock the Test'))

WebUI.delay(1)

// logout
CustomKeywords.'basics.utils.logout'()

// adding 2 seconds delay
WebUI.delay(2)

// make sure that there is no channel presence related this user (this part was often failing)
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

//login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//verify that the test is locked
//WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT-Test locked/label_Test Locked'), 10)
//
//WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT-Test locked/p_Please contact your test administrator for further instructions'), 
//    2)
//
//WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT-Test locked/p_Your test has been locked by the test administrator'), 
//    2)
WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_CAT - Home/button_Start'), 0)

WebUI.closeBrowser()

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login (using TA credentials)
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)


WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Supervised Testing'))

// use todays date to find the test session
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_react-select-12-input'), Keys.chord(
		testDay + ' ', Keys.ENTER))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'),
	25)

// adding a 2 seconds delay
WebUI.delay(2)

//Delete the generated test-access-code
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Delete_Generated_Test_Access_Code'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Delete_Test_Access_popup'))


// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Lock_Test_Access'))

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Cancel'))

//Unlock the test
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Lock_Test_Access'))

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Unlock Test'))

WebUI.delay(1)

// logout
CustomKeywords.'basics.utils.logout'()

//login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

WebUI.click(findTestObject('Object Repository/Page_CAT - RD Dashboard/button_My Tests'))
// adding small delay
WebUI.delay(2)
// make sure that the candidate can now start the test
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Start'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/button_Start Test'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/input_Start Test_procced-to-next-section-checkbox'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/button_Start Test_1'))
// click on Quit Test
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Welcome/button_Quit Test'), 10)

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Welcome/button_Quit Test'))

// select the check boxes
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_Quit Test_checkbox-condition-0'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_I choose to quit this test_checkbox-condition-1'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_I understand that my test will not be scored_checkbox-condition-2'))

// click on quit test
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/button_Quit Test'))

// click on return to home page
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/button_Return to Home Page'))

// delete assigned tests data related
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// logout
CustomKeywords.'basics.utils.logoutAndClose'()

