import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to user
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no active test access codes
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

//Generate Test access codes using TA
//Generating  test access codes using API (Last argument number represents to generate number of test access codes here it generates 1)
String[] testAccessCode = CustomKeywords.'api.api_requests.generateTestAccessCodeUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
    GlobalVariable.USERS_ENCRYPTED_PASSWORD, 1)

//candidate check in
CustomKeywords.'api.api_requests.candidateCheckInUsingAPI'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
    testAccessCode[0])

//login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.delay(2)

//Delete the generated test-access-code
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/div_Delete'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete'))

//Click Active candidates
WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

//Click Approve
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Checked-in_secondary_approve'))

//Adding delay
WebUI.delay(2)

//Click on the lock button
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/btn-lock-icon'))

//WebUI.click(findTestObject('Object Repository/Page_CAT - TA/input_one_pause-checkbox'))

//pause test for 1 minute
//WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg__svg-inline--fa fa-sort-up fa-w-10'))

WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Lock the Test'))

//Adding delay 
WebUI.delay(1)

// logout
CustomKeywords.'basics.utils.logout'()

//login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//Verify that the test is paused
//WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Test Welcome/label_Test Paused'), 2)
WebUI.verifyElementText(findTestObject('Object Repository/Page_CAT - Test Welcome/label_Test Paused'), "Test Locked")

WebUI.closeBrowser()