import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import org.openqa.selenium.Keys as Keys
import basics.DateUtil as DateUtil
import java.util.Calendar as Calendar
import java.time.LocalDate as LocalDate
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.text.DecimalFormat as DecimalFormat

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// make sure that there is no current assigned tests data (even other users in case other tests failed)
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to user (even other users in case other tests failed)
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no active test access codes
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)


// ========== REMOVING TEST ACCESS BEFORE STARTING THE TEST ==========
//Generate Test access codes using TA
//Generating  test access codes using API (Last argument number represents to generate number of test access codes here it generates 1)
//String[] testAccessCode = CustomKeywords.'api.api_requests.generateTestAccessCodeUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
//    GlobalVariable.USERS_ENCRYPTED_PASSWORD, 1)
//
////candidate check in
//CustomKeywords.'api.api_requests.candidateCheckInUsingAPI'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
//    testAccessCode[0])

WebUI.callTestCase(findTestCase('Supervised Testing/Supervised Testing Stop before Test Access Code'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Supervised Testing/TA Generates Test Access Code'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Supervised Testing/User checks in and exits'), [:], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'basics.utils.NavigateToHomePage'()
//login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Supervised Testing'))

def testDate = LocalDate.now()

//get year, month and day
def testYear = testDate.year

def testMonth = testDate.monthValue

def testDay = testDate.dayOfMonth

// use todays date to find the test session
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_react-select-12-input'), Keys.chord(
		testDay + ' ', Keys.ENTER))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

//Delete access
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Delete_Access'))

//Click Cancel
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Cancel'))

//Delete access
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Delete_Access'))

//confirm
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Remove Access'))


WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Delete_Test_Access_Code'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Delete'))


// logout
CustomKeywords.'basics.utils.logout'()

// login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

WebUI.click(findTestObject('Object Repository/Page_CAT - RD Dashboard/button_My Tests'))

WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_CAT - Home/button_Start'), 0)
//WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - TA/label_You have no assigned tests'), 2)

// logout
CustomKeywords.'basics.utils.logout'()

// ========== REMOVING TEST ACCESS BEFORE STARTING THE TEST (END) ==========
// ========== REMOVING TEST ACCESS AFTER STARTING THE TEST ==========
//Generate Test access codes using TA
//Generating  test access codes using API (Last argument number represents to generate number of test access codes here it generates 1)
//String[] testAccessCode2 = CustomKeywords.'api.api_requests.generateTestAccessCodeUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
//    GlobalVariable.USERS_ENCRYPTED_PASSWORD, 1)
//
////candidate check in
//CustomKeywords.'api.api_requests.candidateCheckInUsingAPI'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
//    testAccessCode2[0])


WebUI.callTestCase(findTestCase('Supervised Testing/TA Generates Test Access Code'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Supervised Testing/User checks in and exits'), [:], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'basics.utils.NavigateToHomePage'()
//login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Supervised Testing'))

// adding a 2 seconds delay
WebUI.delay(2)

// use todays date to find the test session
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_react-select-12-input'), Keys.chord(
		testDay + ' ', Keys.ENTER))

// adding a 2 seconds delay
WebUI.delay(2)

//Delete the generated test-access-code
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Delete_Generated_Test_Access_Code'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Delete_Test_Access_popup'))


//Click Active candidates
//WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

//Click Approve
//WebUI.click(findTestObject('Object Repository/Page_CAT - TA/button_Checked-in_secondary_approve'))

// logout
CustomKeywords.'basics.utils.logout'()

//login as Candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

WebUI.click(findTestObject('Object Repository/Page_CAT - RD Dashboard/button_My Tests'))
// adding small delay
WebUI.delay(2)
// click Start (starting test)
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Start'))


// adding small delay
WebUI.delay(2)

// close the browser
WebUI.closeBrowser()

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

//login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Supervised Testing'))

// adding a 2 seconds delay
WebUI.delay(2)

// use todays date to find the test session
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_react-select-12-input'), Keys.chord(
		testDay + ' ', Keys.ENTER))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

//Remove candidate access to the test code
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Remove_Test_Access'))
//click the remove access pop up
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Remove Access'))

// adding small delay
WebUI.delay(2)

// make sure that the active candidates table is empty
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/active_candidates_table_There are no active candidates'), 
    2)

// logout
CustomKeywords.'basics.utils.logout'()

// login as candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

WebUI.click(findTestObject('Object Repository/Page_CAT - RD Dashboard/button_My Tests'))

WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_CAT - Home/button_Start'), 0)
//WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - TA/label_You have no assigned tests'), 2)

// ========== REMOVING TEST ACCESS AFTER STARTING THE TEST (END) ==========
// delete assigned tests data related
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()

