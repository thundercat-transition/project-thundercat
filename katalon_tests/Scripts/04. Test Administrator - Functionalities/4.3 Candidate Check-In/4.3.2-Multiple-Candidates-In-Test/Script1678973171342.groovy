import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import org.openqa.selenium.Keys as Keys
import basics.DateUtil as DateUtil
import java.util.Calendar as Calendar
import java.time.LocalDate as LocalDate
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.text.DecimalFormat as DecimalFormat

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to first and second candidates that we'll use in this use case
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

//TAtwo should have the roles by now
//test centre should exist by now
//billing contact exists

//need two users to be assinged to this test

//create the test session
WebUI.callTestCase(findTestCase('Supervised Testing/Create Test Session'), [:], FailureHandling.STOP_ON_FAILURE)
//create assement process
WebUI.callTestCase(findTestCase('Supervised Testing/Create Assessment Process'), [:], FailureHandling.STOP_ON_FAILURE)

//add the two users to the assement process

CustomKeywords.'basics.utils.NavigateToHomePage'()
CustomKeywords.'basics.ta_utils.assignTwoCandidatesToAssessment'(GlobalVariable.STANDARD_USER_TWO_FIRST_NAME, GlobalVariable.STANDARD_USER_TWO_LAST_NAME, GlobalVariable.REFERENCE_NUMBER,GlobalVariable.STANDARD_USER_THREE_FIRST_NAME, GlobalVariable.STANDARD_USER_THREE_LAST_NAME)

CustomKeywords.'basics.utils.NavigateToHomePage'()

//get reservation code
String testReservationCodeUserTwo = CustomKeywords.'database.DataGetting.getTestReservationCodeWithEmail'(GlobalVariable.REFERENCE_NUMBER, GlobalVariable.SIMULATE_DELIVERED_EMAIL_TWO)
String testReservationCodeUserThree = CustomKeywords.'database.DataGetting.getTestReservationCodeWithEmail'(GlobalVariable.REFERENCE_NUMBER, GlobalVariable.SIMULATE_DELIVERED_EMAIL_THREE)

CustomKeywords.'basics.utils.NavigateToHomePage'()
//users both reserve test

//get the two reservation codes
CustomKeywords.'basics.candidate_utils.userReservesTestSession'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, testReservationCodeUserTwo)
CustomKeywords.'basics.utils.logout'()
CustomKeywords.'basics.utils.NavigateToHomePage'()
CustomKeywords.'basics.candidate_utils.userReservesTestSession'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, testReservationCodeUserThree)

//TA generates test access code
WebUI.callTestCase(findTestCase('Supervised Testing/TA Generates Test Access Code'), [:], FailureHandling.STOP_ON_FAILURE)

//get access code
String testAccessCode = CustomKeywords.'database.DataGetting.getSupervisedTestAccessCode'(GlobalVariable.TA_USER_EMAIL_ADDRESS)
CustomKeywords.'basics.utils.NavigateToHomePage'()
//users check in
CustomKeywords.'basics.candidate_utils.candidateCheckIn'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, testAccessCode)
WebUI.delay(2)
CustomKeywords.'basics.utils.logout'()
CustomKeywords.'basics.utils.NavigateToHomePage'()
CustomKeywords.'basics.candidate_utils.candidateCheckIn'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, testAccessCode)
CustomKeywords.'basics.utils.logout'()
CustomKeywords.'basics.utils.NavigateToHomePage'()
WebUI.delay(2)
// login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Supervised Testing'))

def testDate = LocalDate.now()

//get year, month and day
def testYear = testDate.year

def testMonth = testDate.monthValue

def testDay = testDate.dayOfMonth

// use todays date to find the test session
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_react-select-12-input'), Keys.chord(
		testDay + ' ', Keys.ENTER))


// adding a 2 seconds delay
WebUI.delay(2)


// click Active candidates
//WebUI.click(findTestObject('Page_CAT - TA/button_Active Candidates'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

//// adding a 2 seconds delay
//WebUI.delay(2)
//
//// click Approve (first candidate)
WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg_Checked-in_svg-inline--fa fa-thumbs-up fa-w-16'))
//
//// adding a 2 seconds delay
//WebUI.delay(2)
//
//// click Approve (second candidate)
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Approve_Second_Candidate'))
// click Lock All
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Lock All'))

// click Cancel 
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Cancel'))

// click Lock All
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Lock All'))

// click Lock Tests
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Lock Tests'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// logout
CustomKeywords.'basics.utils.logout'()

// login as first candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// adding this condition, since this test was often faling in here because of the websocket connection error
// if connection error page is displayed
if (WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Websocket Connection Error/connection_error_button_Logout'), 
    5, FailureHandling.OPTIONAL)) {
    // close the browser
    WebUI.closeBrowser()

    // open browser and navigate to login page
    CustomKeywords.'basics.utils.NavigateToHomePage'()

    // delete any channel presence related to this user
    CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

    // adding 2 seconds delay
    WebUI.delay(2)

    //login as candidate
    CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
}

// make sure that the test is locked
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Welcome/label_Test Locked'), 10)

// close the browser
WebUI.closeBrowser()

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login as second candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// adding this condition, since this test was often faling in here because of the websocket connection error
// if connection error page is displayed
if (WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Websocket Connection Error/connection_error_button_Logout'), 
    5, FailureHandling.OPTIONAL)) {
    // close the browser
    WebUI.closeBrowser()

    // open browser and navigate to login page
    CustomKeywords.'basics.utils.NavigateToHomePage'()

    // delete any channel presence related to this user
    CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

    // adding 2 seconds delay
    WebUI.delay(2)

    //login as candidate
    CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
}

// make sure that the test is locked
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Welcome/label_Test Locked'), 10)

// close the browser
WebUI.closeBrowser()

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Supervised Testing'))


// use todays date to find the test session
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_react-select-12-input'), Keys.chord(
		testDay + ' ', Keys.ENTER))

// adding a 2 seconds delay
WebUI.delay(2)

//delete test access code

//Delete the generated test-access-code
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Delete_Generated_Test_Access_Code'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Delete_Test_Access_popup'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// adding a 2 seconds delay
WebUI.delay(2)

// click Unlock All
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Unlock All'))

// click Cancel
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Cancel'))

// click Unlock All
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Unlock All'))

// click Unlock All Tests
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Unlock All Tests'))

// wait for the table to be loaded
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'), 
    25)

// logout
CustomKeywords.'basics.utils.logout'()

// make sure that there is no channel presence related this user (this was failing often in headless mode)
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

// login as first candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

WebUI.click(findTestObject('Object Repository/Page_CAT - RD Dashboard/button_My Tests'))

// make sure that the candidate can now start the test
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Start'))

// adding this condition, since this test was often faling in here because of the websocket connection error
// if connection error page is displayed
if (WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Websocket Connection Error/connection_error_button_Logout'), 
    5, FailureHandling.OPTIONAL)) {
    // close the browser
    WebUI.closeBrowser()

    // open browser and navigate to login page
    CustomKeywords.'basics.utils.NavigateToHomePage'()

    // delete any channel presence related to this user
    CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

    // adding 2 seconds delay
    WebUI.delay(2)

    //login as candidate
    CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
}
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/button_Start Test'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/input_Start Test_procced-to-next-section-checkbox'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/button_Start Test_1'))

// click on Quit Test
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Welcome/button_Quit Test'), 10)

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Welcome/button_Quit Test'))

// select the check boxes
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_Quit Test_checkbox-condition-0'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_I choose to quit this test_checkbox-condition-1'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_I understand that my test will not be scored_checkbox-condition-2'))

// click on quit test
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/button_Quit Test'))

// click on return to home page
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/button_Return to Home Page'))

// logout
CustomKeywords.'basics.utils.logout'()

// make sure that there is no channel presence related this user (this was failing often in headless mode)
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// adding 2 seconds delay
WebUI.delay(2)

// login as second candidate
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

WebUI.click(findTestObject('Object Repository/Page_CAT - RD Dashboard/button_My Tests'))

// make sure that the candidate can now start the test
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Start'))

// adding this condition, since this test was often faling in here because of the websocket connection error
// if connection error page is displayed
if (WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Websocket Connection Error/connection_error_button_Logout'), 
    5, FailureHandling.OPTIONAL)) {
    // close the browser
    WebUI.closeBrowser()

    // open browser and navigate to login page
    CustomKeywords.'basics.utils.NavigateToHomePage'()

    // delete any channel presence related to this user
    CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

    // adding 2 seconds delay
    WebUI.delay(2)

    //login as candidate
    CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
}
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/button_Start Test'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/input_Start Test_procced-to-next-section-checkbox'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/button_Start Test_1'))
// click on Quit Test
WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Welcome/button_Quit Test'), 10)

WebUI.click(findTestObject('Object Repository/Page_CAT - Test Welcome/button_Quit Test'))

// select the check boxes
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_Quit Test_checkbox-condition-0'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_I choose to quit this test_checkbox-condition-1'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/input_I understand that my test will not be scored_checkbox-condition-2'))

// click on quit test
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/button_Quit Test'))

// click on return to home page
WebUI.click(findTestObject('Object Repository/Page_CAT - Quit Test/button_Return to Home Page'))

// logout
CustomKeywords.'basics.utils.logout'()

// login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Supervised Testing'))


// use todays date to find the test session
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_react-select-12-input'), Keys.chord(
		testDay + ' ', Keys.ENTER))


// adding a 2 seconds delay
WebUI.delay(2)


// make sure that there are no more active candidate in TA' active candidates table
WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/active_candidates_table_There are no active candidates'), 
    2)

// delete assigned tests data related
// first candidate
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_TWO_EMAIL_ADDRESS)

// second candidate
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()

