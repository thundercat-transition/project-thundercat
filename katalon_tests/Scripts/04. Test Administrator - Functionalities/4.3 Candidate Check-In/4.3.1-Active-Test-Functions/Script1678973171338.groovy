import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import org.openqa.selenium.Keys as Keys
import basics.DateUtil as DateUtil
import java.util.Calendar as Calendar
import java.time.LocalDate as LocalDate
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.text.DecimalFormat as DecimalFormat

// Making sure that the TA has the Test Adaptations Permission
// Verify if TA as the TA Permissions
if (!(new database.DataValidation().userPermissionAlreadyAssociated(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TA_PERMISSION))) {
    // send TA permission for our TA
    CustomKeywords.'basics.utils.NavigateToHomePage'()

    // login
    CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

    // send TA permission request
    CustomKeywords.'basics.permissions.sendPermissionRequest'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TA_PERMISSION, 
        false)

    // logout
    CustomKeywords.'basics.utils.logout'()

    // Approve permission request for our TA
    // approve ta permission
    CustomKeywords.'basics.permissions.approvePermissionRequest'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TA_USER_FIRST_NAME, 
        GlobalVariable.TA_USER_LAST_NAME, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// Verify if TA as the Adaptations Permissions
if (!(new database.DataValidation().userPermissionAlreadyAssociated(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.ADAPTATIONS_PERMISSION))) {
    // send Adaptations permission for our TA
    CustomKeywords.'basics.utils.NavigateToHomePage'()

    // login
    CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

    // send TA permission request
    CustomKeywords.'basics.permissions.sendPermissionRequest'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.ADAPTATIONS_PERMISSION, 
        false)

    // logout
    CustomKeywords.'basics.utils.logout'()

    // Approve permission request for our TA
    // approve ta permission
    CustomKeywords.'basics.permissions.approvePermissionRequest'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.TA_USER_FIRST_NAME, 
        GlobalVariable.TA_USER_LAST_NAME, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// adding a 2 seconds delay
WebUI.delay(2)

// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to user
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)


//Generate Test access code
WebUI.callTestCase(findTestCase('Supervised Testing/Supervised Testing Stop before Test Access Code'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Supervised Testing/TA Generates Test Access Code'), [:], FailureHandling.STOP_ON_FAILURE)

//get access code
String testAccessCode = CustomKeywords.'database.DataGetting.getSupervisedTestAccessCode'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()
// complete profile
CustomKeywords.'basics.candidate_utils.runCompleteProfile'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

//candidate check in
// login
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

WebUI.click(findTestObject('Object Repository/Page_CAT - RD Dashboard/button_My Tests'))

//input the test access code
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Type your Test Access Code_test-acces_5ef1cf'), Keys.chord(
        testAccessCode, Keys.ENTER))
// logout
CustomKeywords.'basics.utils.logout'()

////candidate check in Error validation
//CustomKeywords.'basics.candidate_utils.runCandidateCheckInErrorValidation'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, 
//    testAccessCode)

//login as TA
CustomKeywords.'basics.utils.login'(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Supervised Testing'))

def testDate = LocalDate.now()

//get year, month and day
def testYear = testDate.year

def testMonth = testDate.monthValue

def testDay = testDate.dayOfMonth

// use todays date to find the test session
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_react-select-12-input'), Keys.chord(
		testDay + ' ', Keys.ENTER))


// adding a 2 seconds delay
WebUI.delay(2)

//Click edit time limit
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Edit_Test_Time'))

//update time limit
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_increment-hours-button'))

//Click cancel
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Cancel'))
//Click edit time limit
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Edit_Test_Time'))

//update time limit
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_increment-hours-button'))
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_increment-hours-button'))

//Click set timer
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Set Timer'))

//Remove candidate access to the test code
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Delete_Access_Code'))

//Click cancel
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Cancel'))

//Remove candidate access to the test code
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Delete_Access_Code'))

//Click Delete on popup
WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Remove Access'))


// delete assigned tests data related
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()

