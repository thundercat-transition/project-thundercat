import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String currentProfile = RunConfiguration.getExecutionProfile()

System.out.println(currentProfile)

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login
new basics.utils().login(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// select Test Access Management side navigation item
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/button_Test Access Management'))

// current profile is local or dev
if (currentProfile.equals('local')) {
    // fill out the Test Order Number field
    WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Test Order Number_test-order-number'), 
        GlobalVariable.TEST_ACCESS_EXISTING_TEST_ORDER_NUMBER)

    // click Search
    WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/assign_test_accesses_button_Search'))

    // expect to get the unable to connect error
    WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/assign_test_accesses_Unable to connect to the ordering service. Please enter financial data manually'), 
        25)

    // click Clear All Fields
    WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/assign_test_accesses_button_Clear All Fields'))

    // click Cancel
    WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/assign_test_accesses_confirmation_popup_button_Cancel'))

    // click Clear All Fields
    WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/assign_test_accesses_button_Clear All Fields'))

    // click Clear
    WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/assign_test_accesses_confirmation_popup_button_Clear')) // current profile is dev or test
    // fill out the Test Order Number field (non existing one)
    // click Search
    // expect to get the unable to connect error
    // fill out the Test Order Number field (existing one)
    // simulate "Enter" key press
    // expect to see the Assessment Process Number field
    // click Clear All Fields
    // click Cancel
    // click Clear All Fields
    // click Clear
} else if (currentProfile.equals('dev')) {
	// Since Ordering Service is currently not working on Dev and Test, we are skipping all the testing
} else {
    WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Test Order Number_test-order-number'), 
        '000AAABBB')

    WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/assign_test_accesses_button_Search'))

    WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/assign_test_accesses_This order number cannot be found, an exact match is required'), 
        25)

    WebUI.setText(findTestObject('Object Repository/Page_CAT - Business Operations/input_Test Order Number_test-order-number'), 
        GlobalVariable.TEST_ACCESS_EXISTING_TEST_ORDER_NUMBER)

    WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Business Operations/input_Test Order Number_test-order-number'), 
        Keys.chord(Keys.ENTER))

    WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Business Operations/assign_test_accesses_Assessment Process Number'), 
        25)

    WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/assign_test_accesses_button_Clear All Fields'))

    WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/assign_test_accesses_confirmation_popup_button_Cancel'))

    WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/assign_test_accesses_button_Clear All Fields'))

    WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/assign_test_accesses_confirmation_popup_button_Clear'))
}

// make sure that the test order number field is now empty
WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/Page_CAT - Business Operations/input_Test Order Number_test-order-number'), 
        'value'), '')

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()