import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import org.openqa.selenium.Keys as Keys

// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// make sure that there is no pending or active permissions associated to this user
// deleting user permissions
new database.DataDeletion().deleteUserPermissions(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// deleting user pending permissions
new database.DataDeletion().deleteAllPendingUserPermissions(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// deleting user profile changes
new database.DataDeletion().deleteAllPendingUserProfileChangeRequest(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// deleting user assigned tests data
new database.DataDeletion().deleteCandidateAssignedTestsData(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// login (as a standard user)
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// select My Profile
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Profile'))

//// make sure that the first name, last name and DOB are editable
//// edit first name
//// edit last name
//// edit date of birth

////new form pop up still editing
//// update form (with invalid inputs)
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_modify_form'))

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_First Name_popup-new-first-name-input'), GlobalVariable.STANDARD_USER_ONE_FIRST_NAME + 
    ' Editable Fields Test')

WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Last Name_popup-new-last-name-input'), GlobalVariable.STANDARD_USER_ONE_LAST_NAME +
	' Editable Fields Test')

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Select_react-select-Day'), Keys.chord('12', Keys.ENTER))


WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Select_react-select-Month'), Keys.chord('07',Keys.ENTER))


WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Select_react-select-Year'), Keys.chord('1970',Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Submit'))

// send permission request (TA) and validate the errors
CustomKeywords.'basics.permissions.sendPermissionRequest'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.TA_PERMISSION, 
    true)

// make sure that the user' permissions table contains the new requested permission
String permissionTitle = CustomKeywords.'database.DataGetting.getUserPermissionDefinitionData'(GlobalVariable.TA_PERMISSION, 
    'en_name')

String permissionDescription = CustomKeywords.'database.DataGetting.getUserPermissionDefinitionData'(GlobalVariable.TA_PERMISSION, 
    'en_description')

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/rights_and_permissions_table_permission_name')), 
    permissionTitle)

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/rights_and_permissions_table_permission_description')), 
    permissionDescription)

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/rights_and_permissions_table_permission_status')), 
    GlobalVariable.PENDING_PERMISSION_STATUS)

// select Personal Information side navigation item
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Personal Information'))

// make sure that the first name, last name and DOB are no longer editable
// first name field
try {
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_modify_form'))
	WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_First Name_popup-new-first-name-input'), GlobalVariable.STANDARD_USER_ONE_FIRST_NAME +
		' Editable Fields Test')
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Submit'))
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Close'))

//    WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_First Name_first-name-input'), GlobalVariable.STANDARD_USER_ONE_FIRST_NAME + 
//        ' Editable Fields Test')

    // mark test as failed if it goes here
    KeywordUtil.markFailedAndStop('Should not be able to get here, since the first name field should be disabled')
}
catch (Exception e) {
    // expected result is to go here
    System.out.println('This error/exception is expected')
} 

// last name field
try {
	
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_modify_form'))
	WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Last Name_popup-new-last-name-input'), GlobalVariable.STANDARD_USER_ONE_LAST_NAME +
		' Editable Fields Test')
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Submit'))
	
//    WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Last Name_last-name-input'), GlobalVariable.STANDARD_USER_ONE_LAST_NAME + 
//        ' Editable Fields Test')

    // mark test as failed if it goes here
    KeywordUtil.markFailedAndStop('Should not be able to get here, since the last name field should be disabled')
}
catch (Exception e) {
    // expected result is to go here
    System.out.println('This error/exception is expected')
} 

// date of birth (day field) dropdown
try {
	
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_modify_form'))
	WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Select_react-select-Day field 18'), Keys.chord('12', Keys.ENTER))
	//WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Submit'))
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Close'))
	
//    WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/personal_info_dob_day_dropdown'))
//
//    WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/personal_info_dob_day_12_option'))

    // mark test as failed if it goes here
    KeywordUtil.markFailedAndStop('Should not be able to get here, since the date of birth fields should be disabled')
}
catch (Exception e) {
    // expected result is to go here
    System.out.println('This error/exception is expected')
} 
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Close'))
// logout
CustomKeywords.'basics.utils.logout'()

// login (as an ETTA user)
CustomKeywords.'basics.utils.login'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// deny permission request and validate the errors
CustomKeywords.'basics.permissions.denyPermissionRequest'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, true)

//deny profile change request
CustomKeywords.'basics.permissions.denyProfileModificationRequest'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, true)

// logout
CustomKeywords.'basics.utils.logout'()

// login (as a standard user)
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// select My Profile
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Profile (1)'))

// make sure that the first name, last name and DOB are editable again
// edit first name
// edit last name
// edit date of birth


	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_modify_form'))
	
	WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_First Name_popup-new-first-name-input'), GlobalVariable.STANDARD_USER_ONE_FIRST_NAME +
		' Editable Fields Test')
	
	WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Last Name_popup-new-last-name-input'), GlobalVariable.STANDARD_USER_ONE_LAST_NAME +
		' Editable Fields Test')
	
	WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Select_react-select-Day'), Keys.chord('12', Keys.ENTER))
	
	
	WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Select_react-select-Month'), Keys.chord('07',Keys.ENTER))
	
	
	WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Select_react-select-Year'), Keys.chord('1970',Keys.ENTER))
	
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Submit'))
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Close'))

// select Rights and Permissions side navigation item
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Rights and Permissions'))

// make sure that the permission data is no longer in the table 
WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_CAT - Profile/rights_and_permissions_table_permission_name'), 
    2)

WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_CAT - Profile/rights_and_permissions_table_permission_description'), 
    2)

WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_CAT - Profile/rights_and_permissions_table_permission_status'), 
    2)

// send permission request (PPC)
CustomKeywords.'basics.permissions.sendPermissionRequest'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.PPC_PERMISSION, 
    false)

// make sure that the user' permissions table contains the new requested permission
String permissionTitle2 = CustomKeywords.'database.DataGetting.getUserPermissionDefinitionData'(GlobalVariable.PPC_PERMISSION, 
    'en_name')

String permissionDescription2 = CustomKeywords.'database.DataGetting.getUserPermissionDefinitionData'(GlobalVariable.PPC_PERMISSION, 
    'en_description')

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/rights_and_permissions_table_permission_name')), 
    permissionTitle2)

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/rights_and_permissions_table_permission_description')), 
    permissionDescription2)

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/rights_and_permissions_table_permission_status')), 
    GlobalVariable.PENDING_PERMISSION_STATUS)

// logout
CustomKeywords.'basics.utils.logout'()

// approve permission request
CustomKeywords.'basics.permissions.approvePermissionRequest'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.STANDARD_USER_ONE_FIRST_NAME, 
    GlobalVariable.STANDARD_USER_ONE_LAST_NAME, false)

// logout
CustomKeywords.'basics.utils.logout'()

// login (as a standard user)
CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

// click MENU
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU (2)'))

// select My Profile
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_My Profile'))

// make sure that the first name, last name and DOB are no longer editable
// first name field
try {
    WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_modify_form'))
	WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_First Name_popup-new-first-name-input'), GlobalVariable.STANDARD_USER_ONE_FIRST_NAME +
		' Editable Fields Test')
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Submit'))
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Close'))

    // mark test as failed if it goes here
    KeywordUtil.markFailedAndStop('Should not be able to get here, since the first name field should be disabled')
}
catch (Exception e) {
    // expected result is to go here
    System.out.println('This error/exception is expected')
} 

// last name field
try {
    WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_modify_form'))
	WebUI.setText(findTestObject('Object Repository/Page_CAT - Profile/input_Last Name_popup-new-last-name-input'), GlobalVariable.STANDARD_USER_ONE_LAST_NAME +
		' Editable Fields Test')
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Submit'))
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Close'))

    // mark test as failed if it goes here
    KeywordUtil.markFailedAndStop('Should not be able to get here, since the last name field should be disabled')
}
catch (Exception e) {
    // expected result is to go here
    System.out.println('This error/exception is expected')
} 

// date of birth (day field) dropdown
try {
    WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_modify_form'))
	WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Profile/input_Select_react-select-Day field 18'), Keys.chord('12', Keys.ENTER))
	//WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Submit'))
	WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Close'))
	

    // mark test as failed if it goes here
    KeywordUtil.markFailedAndStop('Should not be able to get here, since the date of birth fields should be disabled')
}
catch (Exception e) {
    // expected result is to go here
    System.out.println('This error/exception is expected')
} 

WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Close'))
// select Rights and Permissions side navigation item
WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_Rights and Permissions'))

// make sure that the user' permissions table contains the new approved permission (without the pending status)
String permissionTitle3 = CustomKeywords.'database.DataGetting.getUserPermissionDefinitionData'(GlobalVariable.PPC_PERMISSION, 
    'en_name')

String permissionDescription3 = CustomKeywords.'database.DataGetting.getUserPermissionDefinitionData'(GlobalVariable.PPC_PERMISSION, 
    'en_description')

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/rights_and_permissions_table_permission_name')), 
    permissionTitle3)

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/rights_and_permissions_table_permission_description')), 
    permissionDescription3)

WebUI.verifyEqual(WebUI.getText(findTestObject('Object Repository/Page_CAT - Profile/rights_and_permissions_table_permission_status')), 
    '')

// delete user permissions
CustomKeywords.'database.DataDeletion.deleteUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// logout and close browser
CustomKeywords.'basics.utils.logoutAndClose'()