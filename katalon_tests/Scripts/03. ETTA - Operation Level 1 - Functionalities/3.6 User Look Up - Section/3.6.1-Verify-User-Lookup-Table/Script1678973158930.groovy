import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint


//delete test access codes of TA user if exists
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

// make sure that there is no current assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// make sure that there is no channel presence related to user
CustomKeywords.'database.DataDeletion.deleteCandidateChannelPresence'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS)

// making sure that the user does not have any permissions assigned to his account
CustomKeywords.'database.DataDeletion.deleteUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// making sure that the user does not have any pending permissions assigned to his account
CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.ETTA_PERMISSION)

CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.PPC_PERMISSION)

CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.TA_PERMISSION)

CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.SCORER_PERMISSION)

CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.TD_LEVEL_1_PERMISSION)

//api does not work
////Generate Test access codes using TA
////Generating  test access codes using API (Last argument number represents to generate number of test access codes here it generates 1)
//String[] testAccessCode1 = CustomKeywords.'api.api_requests.generateTestAccessCodeUsingAPI'(GlobalVariable.TA_USER_EMAIL_ADDRESS, 
//    GlobalVariable.USERS_ENCRYPTED_PASSWORD, 1)
//
////candidate check in
//CustomKeywords.'api.api_requests.candidateCheckInUsingAPI'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
//    testAccessCode1[0])

//// open browser and navigate to login page
CustomKeywords.'basics.utils.NavigateToHomePage'()

WebUI.callTestCase(findTestCase('Supervised Testing/Supervised Testing User Leaves Test'), [:], FailureHandling.STOP_ON_FAILURE)

//// send PPC Permission request for user one
//CustomKeywords.'api.api_requests.sendPermissionRequestUsingAPI'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD, 
//    GlobalVariable.PPC_PERMISSION)
// check if the permission is already requested or associated to this account
boolean ppcPermissionAlreadyAssociated = CustomKeywords.'database.DataValidation.userPermissionAlreadyAssociated'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, 
    GlobalVariable.PPC_PERMISSION)

CustomKeywords.'basics.utils.NavigateToHomePage'()
// if ppcPermissionAlreadyAssociated is false
if (!(ppcPermissionAlreadyAssociated)) {
    // login
    CustomKeywords.'basics.utils.login'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

    // send PPC permission request
    CustomKeywords.'basics.permissions.sendPermissionRequest'(GlobalVariable.STANDARD_USER_THREE_EMAIL_ADDRESS, GlobalVariable.PPC_PERMISSION, 
        false)

    // logout
    CustomKeywords.'basics.utils.logout'()

    CustomKeywords.'basics.permissions.approvePermissionRequest'(GlobalVariable.STANDARD_USER_THREE_LAST_NAME, GlobalVariable.STANDARD_USER_THREE_FIRST_NAME, 
        GlobalVariable.STANDARD_USER_THREE_LAST_NAME, false)

    // logout
    CustomKeywords.'basics.utils.logout'()
}

// Navigate to home page
CustomKeywords.'basics.utils.NavigateToHomePage'()

// login (as a ETTA user)
CustomKeywords.'basics.utils.login'(GlobalVariable.ETTA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

//Adding delay
WebUI.delay(1)

// Click on User Look-up
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_User Look-Up'))

//Adding delay
WebUI.delay(4)

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Number of results per page_react-sele_823129'), Keys.chord(
        '50',Keys.ENTER))



//Adding delay
WebUI.delay(1)

//click on next page
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/div_Next page'))

//Adding delay
WebUI.delay(1)

//click on previous page
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/div_Previous page'))

WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Number of results per page_react-sele_823129'), Keys.chord(
        '25', Keys.ENTER))


// Delay for 1 second for the dropdown
WebUI.delay(1)

// Search by email
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Search_user-look-up-search-bar'), Keys.chord(GlobalVariable.TA_USER_TWO_EMAIL_ADDRESS,Keys.ENTER))

// Add delay of 2 seconds for the results to show up
WebUI.delay(2)
WebUI.verifyElementText(findTestObject('Object Repository/Page_CAT - Home/div_katalon.tatwoemail.ca'), 'katalon.tatwo@email.ca')
// clear search
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Clear Search'))


// Search by user DOB
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Search_user-look-up-search-bar'), Keys.chord('2000-02-20',Keys.ENTER))


// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

WebUI.verifyElementText(findTestObject('Object Repository/Page_CAT - Home/label_7 result(s)'), '7 result(s)')

WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Clear Search'))

// Search by user email
WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Search_user-look-up-search-bar'), Keys.chord(GlobalVariable.STANDARD_USER_THREE_LAST_NAME, Keys.ENTER))


// Add delay of 2 seconds for the results to show up
WebUI.delay(2)

WebUI.verifyTextPresent('1 result(s)', false)

// click on view button
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_View'))

// test back button
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Back to User Look-Up results'))

WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_View'))
// delay 
WebUI.delay(2)

//click on view test details
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Tests'))
WebUI.delay(2)


//click on invalidate access
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_toggel_invalidate'))

//click cancel
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Cancel'))
WebUI.delay(2)

//click on invalidate access
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_toggel_invalidate'))

//set reason for invalidating the test
WebUI.setText(findTestObject('Object Repository/Page_CAT - Home/textarea_invalid'), 'Invalid Katalon Testing')


//click invalidate button
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Invalidate Access'))
WebUI.delay(2)

//restore access
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_toggel_invalidate'))
WebUI.delay(2)
WebUI.setText(findTestObject('Object Repository/Page_CAT - Home/textarea_restore'), 'Restore Katalon Testing')

WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Restore Acess'))

//click on invalidate access
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_toggel_invalidate'))

//set reason for invalidating the test
WebUI.setText(findTestObject('Object Repository/Page_CAT - Home/textarea_invalid'), 'Invalid Katalon Testing')


//click invalidate button
WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Invalidate Access'))
WebUI.delay(2)

//click on view test details
WebUI.click(findTestObject('Object Repository/Page_CAT - Business Operations/User Look-up/svg_Checked-in_svg-inline--fa fa-binoculars fa-w-16'))

WebUI.delay(2)


WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Rights and Permissions'))

WebUI.delay(2)
WebUI.verifyElementText(findTestObject('Object Repository/Page_CAT - Home/p_Test Admin Access'),
	'As a Test Administrator, Katalon UserThree has access to the following tests:')

// verify the proper Permissions
WebUI.verifyElementText(findTestObject('Object Repository/Page_CAT - Home/div_PPC RD Level 1'), 'PPC R&D Level 1')

WebUI.verifyElementText(findTestObject('Object Repository/Page_CAT - Home/td_The user does not have any test access'), 
    'The user does not have any test access permissions.')


//delete test access codes of TA user if exists
CustomKeywords.'database.DataDeletion.deleteTaTestAccessCodes'(GlobalVariable.TA_USER_EMAIL_ADDRESS)

// delete pending user permissions for standard user three
CustomKeywords.'database.DataDeletion.deletePendingUserPermissions'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS, GlobalVariable.PPC_PERMISSION)

// delete assigned tests data
CustomKeywords.'database.DataDeletion.deleteCandidateAssignedTestsData'(GlobalVariable.STANDARD_USER_ONE_EMAIL_ADDRESS)

// Logout and close
CustomKeywords.'basics.utils.logoutAndClose'()

