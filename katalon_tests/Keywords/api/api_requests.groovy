package api

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.util.CryptoUtil

import groovy.json.JsonSlurper
import internal.GlobalVariable

public class api_requests {

	/**
	 * Generate Test access code using API
	 */
	@Keyword
	def String[] generateTestAccessCodeUsingAPI(String username, String password, int number){
		def TestAccessCodes = [];
		//Sending request for JWT token
		ResponseObject respObj = WS.sendRequest(findTestObject('API Requests/Create JWT Token', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('username') : username
			, ('password') : CryptoUtil.decode(CryptoUtil.getDefault(password))]))

		JsonSlurper slurper1 = new JsonSlurper()
		// getting the token value from response
		Map parsedToken = slurper1.parseText(respObj.getResponseText())

		String test_id = new database.DataGetting().getFirstTestAccessDefinition('id')

		//sending the request to generate a test access code
		for(int i=0; i< number; i++){
			ResponseObject respObj2 = WS.sendRequest(findTestObject('API Requests/Generate Test Access Code', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('username') : username, ('password') : CryptoUtil.decode(
				CryptoUtil.getDefault(password)), ('authorization') : parsedToken.access, ('test_order_number') : GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER
				, ('test_id') : test_id, ('test_session_language') : GlobalVariable.TEST_ACCESS_TEST_SESSION_LANGUAGE]))

			String parsedTestAccessCode = slurper1.parseText(respObj2.getResponseText())
			TestAccessCodes.push(parsedTestAccessCode)
		}

		return TestAccessCodes
	}
	/**
	 * Candidate check in using API
	 */
	@Keyword
	def void candidateCheckInUsingAPI(String username, String password, String testAccessCode){
		//Sending request for JWT token
		ResponseObject respObj = WS.sendRequest(findTestObject('API Requests/Create JWT Token', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('username') : username
			, ('password') : CryptoUtil.decode(CryptoUtil.getDefault(password))]))

		JsonSlurper slurper1 = new JsonSlurper()
		// getting the token value from response
		Map parsedToken = slurper1.parseText(respObj.getResponseText())
		//API call for candidate check in
		WS.sendRequest(findTestObject('API Requests/Candidate Check in',[('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, , ('authorization') : parsedToken.access, ('test_access_code') : testAccessCode]))
	}

	/**
	 * send permission request using API
	 */
	@Keyword
	def void sendPermissionRequestUsingAPI(String username, String password, String requestedPermission){
		//Sending request for JWT token
		ResponseObject respObj = WS.sendRequest(findTestObject('API Requests/Create JWT Token', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('username') : username
			, ('password') : CryptoUtil.decode(CryptoUtil.getDefault(password))]))

		JsonSlurper slurper1 = new JsonSlurper()
		// getting the token value from response
		Map parsedToken = slurper1.parseText(respObj.getResponseText())

		//setting the data to send in the request
		String pri = '12345679';
		String rationale = "Katalon Tests"
		String supervisor = "Katalon Supervisor";
		String supervisor_email = 'katalon.supervisor@email.ca';

		//set the permission id based on the requested permission
		if (requestedPermission == GlobalVariable.SCORER_PERMISSION) {
			WS.sendRequest(findTestObject('API Requests/Send Permission Request', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('goc_email') : username, ('permission_requested') : 4
				, ('pri') : pri, ('rationale') : rationale, ('supervisor') : supervisor, ('supervisor_email') : supervisor_email, ('authorization') : parsedToken.access]))

		}
		else if (requestedPermission == GlobalVariable.ETTA_PERMISSION) {
			WS.sendRequest(findTestObject('API Requests/Send Permission Request', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('goc_email') : username, ('permission_requested') : 2
				, ('pri') : pri, ('rationale') : rationale, ('supervisor') : supervisor, ('supervisor_email') : supervisor_email, ('authorization') : parsedToken.access]))
		} else if (requestedPermission == GlobalVariable.PPC_PERMISSION) {
			WS.sendRequest(findTestObject('API Requests/Send Permission Request', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('goc_email') : username, ('permission_requested') : 3
				, ('pri') : pri, ('rationale') : rationale, ('supervisor') : supervisor, ('supervisor_email') : supervisor_email, ('authorization') : parsedToken.access]))
		}
		else if (requestedPermission == GlobalVariable.TA_PERMISSION) {
			WS.sendRequest(findTestObject('API Requests/Send Permission Request', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('goc_email') : username, ('permission_requested') : 1
				, ('pri') : pri, ('rationale') : rationale, ('supervisor') : supervisor, ('supervisor_email') : supervisor_email, ('authorization') : parsedToken.access]))
		}
		else if (requestedPermission == GlobalVariable.ADAPTATIONS_PERMISSION) {
			WS.sendRequest(findTestObject('API Requests/Send Permission Request', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('goc_email') : username, ('permission_requested') : 6
				, ('pri') : pri, ('rationale') : rationale, ('supervisor') : supervisor, ('supervisor_email') : supervisor_email, ('authorization') : parsedToken.access]))
		}
	}

	/**
	 * Approve permission request using API
	 */
	@Keyword
	def void approvePermissionRequestUsingAPI(String username, String permissionToBeApproved, boolean approveEttaPermission){
		ResponseObject respObj;
		if(approveEttaPermission){
			//Sending request for JWT token
			respObj = WS.sendRequest(findTestObject('API Requests/Create JWT Token', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('username') : GlobalVariable.SUPER_USER_USERNAME
				, ('password') : CryptoUtil.decode(CryptoUtil.getDefault(GlobalVariable.SUPER_USER_ENCRYPTED_PASSWORD))]))
		}else {
			//Sending request for JWT token
			respObj = WS.sendRequest(findTestObject('API Requests/Create JWT Token', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('username') : GlobalVariable.ETTA_USER_EMAIL_ADDRESS
				, ('password') : CryptoUtil.decode(CryptoUtil.getDefault(GlobalVariable.USERS_ENCRYPTED_PASSWORD))]))
		}

		JsonSlurper slurper1 = new JsonSlurper()
		// getting the token value from response
		Map parsedToken = slurper1.parseText(respObj.getResponseText())

		//set the permission id based on the permission to be approved
		if (permissionToBeApproved == GlobalVariable.SCORER_PERMISSION) {
			WS.sendRequest(findTestObject('API Requests/Approve Permission Request', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('authorization') : parsedToken.access
				, ('username') : username, ('permission_id') : 4]))
		}
		else if (permissionToBeApproved == GlobalVariable.ETTA_PERMISSION) {
			WS.sendRequest(findTestObject('API Requests/Approve Permission Request', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('authorization') : parsedToken.access
				, ('username') : username, ('permission_id') : 2]))
		} else if (permissionToBeApproved == GlobalVariable.PPC_PERMISSION) {
			WS.sendRequest(findTestObject('API Requests/Approve Permission Request', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('authorization') : parsedToken.access
				, ('username') : username, ('permission_id') : 3]))
		}
		else if (permissionToBeApproved == GlobalVariable.TA_PERMISSION) {
			WS.sendRequest(findTestObject('API Requests/Approve Permission Request', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('authorization') : parsedToken.access
				, ('username') : username, ('permission_id') : 1]))
		}
		else if (permissionToBeApproved == GlobalVariable.ADAPTATIONS_PERMISSION) {
			WS.sendRequest(findTestObject('API Requests/Approve Permission Request', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('authorization') : parsedToken.access
				, ('username') : username, ('permission_id') : 6]))
		}
	}

	/**
	 * Grant test access using API
	 */
	@Keyword
	def void assignTestAccessUsingAPI(String username){
		//Sending request for JWT token
		ResponseObject respObj = WS.sendRequest(findTestObject('API Requests/Create JWT Token', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('username') : GlobalVariable.ETTA_USER_EMAIL_ADDRESS
			, ('password') : CryptoUtil.decode(CryptoUtil.getDefault(GlobalVariable.USERS_ENCRYPTED_PASSWORD))]))

		JsonSlurper slurper1 = new JsonSlurper()
		// getting the token value from response
		Map parsedToken = slurper1.parseText(respObj.getResponseText())
		//getting test id
		String test_id = new database.DataGetting().getFirstTestAccessDefinition('id')
		//API call to grant test access
		WS.sendRequest(findTestObject('API Requests/Grant Test Access', [('THUNDERCAT_URL') : GlobalVariable.MAIN_URL, ('authorization') : parsedToken.access, ('billing_contact') : 'Katalon Contact Name'
			, ('billing_contact_info') : 'katalon.contact.info@email.ca', ('department_ministry_code') : GlobalVariable.TEST_ACCESS_ORGANIZATION_CODE, ('expiry_date') : '2026-05-13', ('is_org') : 'Org Code', ('is_ref') : 'Ref Code'
			, ('staffing_process_number') : GlobalVariable.TEST_ACCESS_STAFFING_PROCESS_NUMBER, ('test_order_number') : GlobalVariable.TEST_ACCESS_TEST_ORDER_NUMBER_2, ('test_ids') : test_id, ('usernames') : username]))
	}
}
