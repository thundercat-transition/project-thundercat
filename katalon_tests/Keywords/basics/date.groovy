package basics

import java.time.*
import java.util.Calendar

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class DateUtil {
	/**
	 * Get current date - YEAR
	 */
	@Keyword
	def int getCurrentDateYear(){
		def nowYear = Year.now().getValue();
		println "nowYear = ${nowYear}"
		return nowYear
	}

	/**
	 * Get current date - MONTH
	 */
	@Keyword
	def int getCurrentDateMonth(){
		def nowMonth = MonthDay.now().getMonthValue();
		println "nowMonth = ${nowMonth}"
		return nowMonth
	}

	/**
	 * Get current date - DAY
	 */
	@Keyword
	def int getCurrentDateDay(){
		def nowDay = MonthDay.now().getDayOfMonth();
		println "nowDay = ${nowDay}"
		return nowDay
	}

	/**
	 * Get date before today
	 */
	@Keyword
	def Calendar getDateBeforeToday(){
		Calendar dateBeforeToday = Calendar.getInstance();
		dateBeforeToday.add(Calendar.DATE, -1)

		println "dayBefore = ${dateBeforeToday.get(Calendar.DAY_OF_MONTH)}"
		println "month = ${dateBeforeToday.get(Calendar.MONTH)+1}"
		println "year = ${dateBeforeToday.get(Calendar.YEAR)}"

		return dateBeforeToday
	}
	
	/**
	 * Get date before today
	 */
	@Keyword
	def Calendar getDateAfterToday(){
		Calendar dateAfterToday = Calendar.getInstance();
		dateAfterToday.add(Calendar.DATE, +1)

		println "dayAfter = ${dateAfterToday.get(Calendar.DAY_OF_MONTH)}"
		println "month = ${dateAfterToday.get(Calendar.MONTH)+1}"
		println "year = ${dateAfterToday.get(Calendar.YEAR)}"

		return dateAfterToday
	}
}
