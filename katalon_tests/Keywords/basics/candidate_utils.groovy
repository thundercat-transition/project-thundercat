package basics

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.Keys as Keys
import internal.GlobalVariable

public class candidate_utils {
	/**
	 * Candidate check-in functionality
	 */
	@Keyword
	def void candidateCheckIn(String username, String testAccessCode){

		// login (using candidate credentials)
		(new basics.utils()).login(username, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
		// click MENU
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - RD Dashboard/button_My Tests'))

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Home/input_Type your Test Access Code_test-acces_5ef1cf'), Keys.chord(
				"$testAccessCode", Keys.ENTER))
		// adding a 2 seconds delay
		WebUI.delay(2)

	}

	/**
	 * OUTDATED
	 * Error validation for Candidate check-in functionality
	 */
	@Keyword
	def void runCandidateCheckInErrorValidation(String username, String testAccessCode){
		// candidate check-in with existing test access code
		(new basics.candidate_utils()).candidateCheckIn(username, testAccessCode)
		//verify the error message - you have already used this test access code
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - TA/label_You have already used this test access code'), 2)
		//click cancel
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/check_in_popup_button_Cancel'))
		// logout
		(new basics.utils()).logout()
		String invalidTestAccessCode = testAccessCode + "XXXX"
		//candidate check-in with invalid test access code
		(new basics.candidate_utils()).candidateCheckIn(username, invalidTestAccessCode)
		//verify the error message - Must be a valid access code
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - TA/label_Must be a valid Test Access Code'), 2)
		//click cancel
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/check_in_popup_button_Cancel'))
		// logout
		(new basics.utils()).logout()
		//Generate new Test access code for the same test
		(new basics.ta_utils()).generateTestAccessCode()
		String testAccessCodeNew = WebUI.getText(findTestObject('Object Repository/Page_CAT - TA/label_test_access_code_2'))
		// logout
		(new basics.utils()).logout()
		//candidate check in with the new test access code
		(new basics.candidate_utils()).candidateCheckIn(username, testAccessCodeNew)
		//verify the error message - you have already checked in for this test
		WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - TA/label_You havealready checked in for this test'), 2)
		//click cancel
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/check_in_popup_button_Cancel'))
		// logout
		(new basics.utils()).logout()
		// login (using TA credentials)
		(new basics.utils()).login(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
		WebUI.delay(2)
		//Click Delete on new test access code
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete_test_access_code_2'))
		//Click Delete on pop up
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Delete'))
		// logout
		(new basics.utils()).logout()
	}

	@Keyword
	def void runCompleteProfile(String username){
		// login (using candidate credentials)
		(new basics.utils()).login(username, GlobalVariable.USERS_ENCRYPTED_PASSWORD)

		// Complete Profile
		WebUI.click(findTestObject('Page_CAT - Home/button_MENU'))

		WebUI.click(findTestObject('Page_CAT - Home/button_My Profile'))

		WebUI.click(findTestObject('Page_CAT - Profile/input_No_aboriginal-radio-buttons'))

		WebUI.click(findTestObject('Page_CAT - Profile/input_No_disability-radio-buttons'))

		WebUI.click(findTestObject('Page_CAT - Profile/input_No_identify-as-woman-radio-buttons'))

		WebUI.click(findTestObject('Page_CAT - Profile/input_No_visible-minority-radio-buttons'))

		WebUI.click(findTestObject('Page_CAT - Profile/button_Save_Personal_info'))

		WebUI.click(findTestObject('Page_CAT - Profile/button_OK'))

		// Go back to Check-In page
		WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/button_MENU'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Profile/span_Check-in'))

		// logout
		(new basics.utils()).logout()
	}
	/**
	 * run a test as a user
	 * get the test access code from the database
	 * sign in as a user
	 *
	 * check into the test
	 * run the test
	 */
	@Keyword
	def void userRunsTest(String username, String testAccessCode) {

		(new basics.utils()).login(username, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
		// click MENU
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - RD Dashboard/button_My Tests'))

		// run the test
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Start'))
		//if the test is in the retest period then deal with the popup
		//		if(WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Home/h1_Reserve Test Session During Retest Waiti_db2e90'),
		//		20)) {
		//			WebUI.click(findTestObject('Object Repository/Page_CAT - Home/input_Warning_wish-to-proceed-checkbox'))
		//
		//			WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Reserve'))
		//		}
		WebUI.delay(2)
		WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/button_Start Test'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/input_Start Test_procced-to-next-section-checkbox'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/button_Start Test_1'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test 634X1E/button_Submit Test'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test 634X1E/input_Submit Test_procced-to-next-section-checkbox'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test 634X1E/button_Submit Test_1'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test FINISH/button_Return to Home Page'))
		// test is done

	}

	/**
	 * Quit a test as a user
	 * get the test access code from the database
	 * sign in as a user
	 *
	 * check into the test
	 * Quit the test
	 */
	@Keyword
	def void userQuitsTest(String username, String testAccessCode) {
		((new basics.utils().login(username, GlobalVariable.USERS_ENCRYPTED_PASSWORD)))
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Take a Test'))
		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Sample Tests/input_Test Access Code_test-access-code-field'),
				Keys.chord("$testAccessCode", Keys.ENTER))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Start'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Start'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/button_Start Test'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/input_Start Test_procced-to-next-section-checkbox'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test Instructions/button_Start Test_1'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test 634X1E/button_Quit Test'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test 634X1E/input_Quit Test_checkbox-condition-0'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test 634X1E/input_I choose to quit this test_checkbox-c_66e28e'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test 634X1E/input_I understand that my test will not be_55f094'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test 634X1E/button_Quit Test_1'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test QUIT/button_Return to Home Page'))
	}

	/**
	 * Reserve a test as a user
	 * pass in reservation code
	 * sign in as a user
	 *
	 * reserve the test session
	 */
	@Keyword
	def void userReservesTestSession(String username, String testReservationCode) {
		// login
		((new basics.utils().login(username, GlobalVariable.USERS_ENCRYPTED_PASSWORD)))

		// click MENU
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - RD Dashboard/button_My Tests'))
		//go to the reservations page
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Reservations'))


		//input the reservation code
		WebUI.setText(findTestObject('Object Repository/Page_CAT - Home/input_Type your Reservation Code that start_8a4789 (1)'),
				testReservationCode)

		//reserve the test session
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Claim Code'))
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_RES-ANUPROWYVV_primary undefined'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Session Information'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Reserve'))
		//if the test is in the retest period then deal with the popup
		
		try {
			if(WebUI.verifyElementPresent(findTestObject('Object Repository/Page_CAT - Home/h1_Reserve Test Session During Retest Waiti_db2e90'),
				20)) {
			WebUI.click(findTestObject('Object Repository/Page_CAT - Home/input_Warning_wish-to-proceed-checkbox'))
	
			WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Reserve'))
			}
			}
			catch (Exception e) {
				// there is no retest period
				System.out.println('There was no retest period')
			}
				
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_OK'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/a_Manage Reservations'))

		WebUI.verifyElementText(findTestObject('Object Repository/Page_CAT - Home/div_Katalon Test Centre'), 'Katalon Test Centre')

	}


	/**
	 * Withdraw from a reserved test as a user
	 * sign in as a user
	 *
	 * withdraw from the reserved test session
	 */
	@Keyword
	def void userWithdrawsFromReservedTestSession(String username) {
		// login
		((new basics.utils().login(username, GlobalVariable.USERS_ENCRYPTED_PASSWORD)))

		// click MENU
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - RD Dashboard/button_My Tests'))
		//go to the reservations page
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Reservations'))


		//withdraw from the reservation
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Withdraw from Reservation Process'))
		//confirm on pop up
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_Confirm'))

		//press ok on successfully  withdraw
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_popup_ok'))
	}
}
