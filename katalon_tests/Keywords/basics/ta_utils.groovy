package basics

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import basics.DateUtil
import java.util.Calendar
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.text.DecimalFormat
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import org.openqa.selenium.Keys

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class ta_utils {

	/**
	 * Generate Test access code for UIT
	 */
	@Keyword
	def void generateTestAccessCodeUIT(){
		// login (using TA credentials)
		(new basics.utils()).login(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD)
		WebUI.delay(2)

		//Click Generate Test Access Code
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Generate Test Access Code'))

		//Click Cancel
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Cancel'))

		//Click Generate Test Access Code
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/span_Generate Test Access Code'))

		//Select Test Order Number
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg_Select_css-tj5bde-Svg'))
		WebUI.delay(2)
		WebUI.click(findTestObject('Page_CAT - Test Administrator/test_order_number_dropdown_input'))

		//Select Test
		WebUI.delay(2)
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg_Select_css-tj5bde-Svg_1'))
		WebUI.sendKeys(findTestObject('Page_CAT - Test Administrator/test_dropdown_input'), Keys.chord(Keys.ENTER))

		//Select Test Session Language
		WebUI.delay(2)
		WebUI.click(findTestObject('Object Repository/Page_CAT - TA/svg_Select_css-tj5bde-Svg_1_2'))
		WebUI.sendKeys(findTestObject('Page_CAT - Test Administrator/test_session_language_dropdown_input'), Keys.chord(Keys.ENTER))

		//Click Generate
		WebUI.delay(2)
		WebUI.click(findTestObject('Page_CAT - TA/span_Generate'))
	}

	/**
	 * Delete Test with Test Access Management
	 */
	@Keyword
	def void deleteTestAccess(String testOrderNumber) {

		//login as superuser
		(new basics.utils().login(GlobalVariable.SUPER_USER_USERNAME, GlobalVariable.SUPER_USER_ENCRYPTED_PASSWORD))
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

		WebUI.click(findTestObject('Object Repository/Page_Superuser dashboard/button_Operations'))



		WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_Test Access Management'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/a_Active Test Accesses'))

		//	WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Search_active-test-accesses-search-bar'), testOrderNumber)

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Search_active-test-accesses-search-bar'), Keys.chord(testOrderNumber,
				Keys.ENTER))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_Modify'))

		WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/textarea_delete'), 'delete')

		WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_Delete'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_Delete_1'))

	}
	/**
	 * Assign Test with Test Access Management
	 */
	@Keyword
	def void assignTestAccess(String firstName, String lastName, String testOrderNumber) {
		//login as the super user
		(new basics.utils().login(GlobalVariable.SUPER_USER_USERNAME, GlobalVariable.SUPER_USER_ENCRYPTED_PASSWORD))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

		WebUI.click(findTestObject('Object Repository/Page_Superuser dashboard/button_Operations'))


		//go to test access management
		//fill out the form
		WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_Test Access Management'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_Manual Entry'))

		WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Test Order Number_test-order-number'), testOrderNumber)

		WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Assessment Process Number_staffing-pr_2da128'),
				'Process1')

		WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Organization Code_department-ministry-code'),
				'ORG1')

		WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_FIS Organization Code_is-org'), 'FISORG1')

		WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_FIS Reference Code_is-ref'), 'FISREF1')

		WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Billing Contact Name_billing-contact'), 'BILL NAME')

		WebUI.setText(findTestObject('Object Repository/Page_CAT - Operations/input_Contact Email for Candidates_billing-_40217c'),
				'simulate-delivered@notification.canada.ca')

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Operations/input_Select_react-select-21-input'), Keys.chord(
				lastName, ', ', firstName, Keys.ENTER))

		//get date in two days for a test date
		def currentDate = LocalDate.now()
		def testDate = currentDate.plusDays(2)

		//get year, month and day
		def testYear = testDate.year
		def testMonth = testDate.monthValue
		def testDay = testDate.dayOfMonth

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_Test_Expiry_Day'), Keys.chord(
				"${testDay}", Keys.ENTER))

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_Test_Expiry_Month'), Keys.chord(
				"${testMonth}", Keys.ENTER))

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_Test_Expiry_Year'), Keys.chord(
				"${testYear}", Keys.ENTER))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_208'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/input_Test Accesses_208_v11'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_Save'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_Save_1'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_OK'))
	}

	/**
	 * Send Test Invites as a TA
	 */
	@Keyword
	def void sendTestInvite(String TA, String testOrderNumber) {
		(new basics.utils().login(TA, GlobalVariable.USERS_ENCRYPTED_PASSWORD))
		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_Test_Order_Number'), Keys.chord(
				testOrderNumber, Keys.ENTER))
		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_Test'), Keys.chord(' ',Keys.ENTER))
		String listOfCandidates = RunConfiguration.getProjectDir() + '/CSV Files/List of Candidates.csv'
		listOfCandidates = listOfCandidates.replace('/', '\\')

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Browse'))
		(new basics.utils().uploadFile(listOfCandidates))


		//get date in two days for a test date
		def currentDate = LocalDate.now()
		def testDate = currentDate.plusDays(2)

		//get year, month and day
		def testYear = testDate.year
		def testMonth = testDate.monthValue
		def testDay = testDate.dayOfMonth

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_Test_Expiry_Day'), Keys.chord(
				"${testDay}", Keys.ENTER))

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_Test_Expiry_Month'), Keys.chord(
				"${testMonth}", Keys.ENTER))

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_Test_Expiry_Year'), Keys.chord(
				"${testYear}", Keys.ENTER))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Send Invitations'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Send'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_OK'))


	}

	/*
	 * Assign Candidate to assessment process
	 */
	@Keyword
	def void assignCandidateToAssessment(String user_first_name, String user_last_name, String reference_number) {
		(new basics.utils().login(GlobalVariable.SUPER_USER_USERNAME, GlobalVariable.SUPER_USER_ENCRYPTED_PASSWORD))
		// click MENU
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_HR Coordinator'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/a_Candidates'))

		//enter reference number
		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Select_Assessment Process'), Keys.chord(
				GlobalVariable.REFERENCE_NUMBER, Keys.ENTER))

		// add the candidate
		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/span_Add Candidate'))

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_First Name_assign-candidate-first-name-input'),
				Keys.chord(user_first_name, Keys.ENTER))

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Last Name_assign-candidate-last-name-input'),
				Keys.chord(user_last_name, Keys.ENTER))

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Email_assign-candidate-email-input'), Keys.chord(
				GlobalVariable.SIMULATE_DELIVERED_EMAIL, Keys.ENTER))
		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/div_Staffing  Dotation_react-switch-bg'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_Add Candidate_1'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_Send Invitations'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_Send Invitations_1'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_Close'))
	}

	/*
	 * Assign two Candidates to assessment process
	 */
	@Keyword
	def void assignTwoCandidatesToAssessment(String user_1_first_name, String user_1_last_name, String reference_number, String user_2_first_name, String user_2_last_name) {
		(new basics.utils().login(GlobalVariable.SUPER_USER_USERNAME, GlobalVariable.SUPER_USER_ENCRYPTED_PASSWORD))
		// click MENU
		WebUI.click(findTestObject('Object Repository/Page_CAT - Home/button_MENU'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Operations/button_HR Coordinator'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/a_Candidates'))

		//enter reference number
		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Select_Assessment Process'), Keys.chord(
				GlobalVariable.REFERENCE_NUMBER, Keys.ENTER))

		// add the first candidate
		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/span_Add Candidate'))

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_First Name_assign-candidate-first-name-input'),
				Keys.chord(user_1_first_name, Keys.ENTER))

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Last Name_assign-candidate-last-name-input'),
				Keys.chord(user_1_last_name, Keys.ENTER))

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Email_assign-candidate-email-input'), Keys.chord(
				GlobalVariable.SIMULATE_DELIVERED_EMAIL_TWO, Keys.ENTER))
		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/div_Staffing  Dotation_react-switch-bg'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_Add Candidate_1'))


		// add the second candidate
		WebUI.delay(2)

		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/span_Add Candidate'))


		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_First Name_assign-candidate-first-name-input'),
				Keys.chord(user_2_first_name, Keys.ENTER))

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Last Name_assign-candidate-last-name-input'),
				Keys.chord(user_2_last_name, Keys.ENTER))

		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - HR Coordinator/input_Email_assign-candidate-email-input'), Keys.chord(
				GlobalVariable.SIMULATE_DELIVERED_EMAIL_THREE, Keys.ENTER))
		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/div_Staffing  Dotation_react-switch-bg'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_Add Candidate_1'))
		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_Send Invitations'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_Send Invitations_1'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - HR Coordinator/button_Close'))

		(new basics.utils().logout())
	}

	/*
	 * TA Approves candidate to run test
	 */
	@Keyword
	def void approveCandidateTest() {
		//login as TA
		(new basics.utils().login(GlobalVariable.TA_USER_EMAIL_ADDRESS, GlobalVariable.USERS_ENCRYPTED_PASSWORD))
		WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Supervised Testing'))

		def testDate = LocalDate.now()

		//get day

		def testDay = testDate.dayOfMonth

		// use todays date to find the test session
		WebUI.sendKeys(findTestObject('Object Repository/Page_CAT - Test Administrator/input_Select_react-select-12-input'), Keys.chord(
				testDay + ' ', Keys.ENTER))

		WebUI.delay(2)

		// wait for the table to be loaded
		WebUI.waitForElementPresent(findTestObject('Object Repository/Page_CAT - Test Administrator/ta_active_candidates_first_row_candidate_name'),
				25)

		// adding a 2 seconds delay
		WebUI.delay(2)

		//Click Approve
		WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Approve All'))

		WebUI.click(findTestObject('Object Repository/Page_CAT - Test Administrator/button_Approve All_1'))
		// adding a 2 seconds delay
		WebUI.delay(2)

		// logout
		(new basics.utils().logout())
	}
}
