package database

import java.sql.*

import com.kms.katalon.core.annotation.Keyword
import basics.DateUtil as DateUtil
import java.util.Calendar as Calendar
import java.time.LocalDate as LocalDate
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.text.DecimalFormat as DecimalFormat

public class DataGetting {
	/**
	 * getting the amount of active sample tests
	 */
	@Keyword
	def Integer amountOfSampleTests(){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// get amount of active sample tests
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT COUNT(*) FROM cms_models_testdefinition WHERE is_public = 1 AND active = 1")
		resultSet.next()
		Integer amountOfSampleTests = resultSet.getString(1) as Integer
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
		return amountOfSampleTests
	}

	/**
	 * getting the first test access definition (getting the first active non public test based on parent code ascending order)
	 */
	@Keyword
	def Object getFirstTestAccessDefinition(String columnName){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// get first active non public test definition
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT TOP 1 * FROM cms_models_testdefinition WHERE is_public = 0 AND active = 1 AND archived = 0 ORDER BY parent_code, id ASC")
		resultSet.next()
		// getting index of specified column
		Integer columnIndex = resultSet.findColumn(columnName) as Integer
		// getting value of found index
		String columnValue = resultSet.getString(columnIndex) as String
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
		return columnValue
	}

	/**
	 * getting the user permission definition based on provided permission's codename
	 */
	@Keyword
	def Object getUserPermissionDefinitionData(String codename, String columnName){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// get permission based on provided codename
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT * FROM user_management_models_custompermissions WHERE codename = '${codename}'")
		resultSet.next()
		// getting index of specified column
		Integer columnIndex = resultSet.findColumn(columnName) as Integer
		// getting value of found index
		String columnValue = resultSet.getString(columnIndex) as String
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
		return columnValue
	}
	/**
	 * getting specified user password reset token
	 */
	@Keyword
	def Object getUserPasswordResetToken(String username){
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// get password reset token
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT [key] FROM django_rest_passwordreset_resetpasswordtoken WHERE user_id = (SELECT id FROM user_management_models_user WHERE username = '${username}')")
		resultSet.next()
		String passwordResetToken = resultSet.getString(1) as String
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
		return passwordResetToken
	}

	/**
	 * Get UIT test access code from database 
	 */
	@Keyword
	def Object getUITTestAccessCode(String testOrderNumber) {
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT [test_access_code] FROM custom_models_unsupervisedtestaccesscode WHERE test_order_number = '${testOrderNumber}'")
		resultSet.next()
		String testAccessCode = resultSet.getString(1) as String
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
		return testAccessCode
	}


	/**
	 * Get supervised test access code from database
	 */
	@Keyword
	def Object getSupervisedTestAccessCode(String TAUsername) {
		// open database connection
		Connection connection = (new database.DBConnection()).connection()

		// initializing userId
		Integer userId = 0
		// get specified user
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT id FROM user_management_models_user WHERE username = '${TAUsername}'")
		try {
			resultSet.next()
			userId = resultSet.getString(1) as Integer
		} catch (Exception e) { System.out.println(TAUsername + " has not been found in the database") }


		def testDate = LocalDate.now()

		//get year, month and day
		def testYear = testDate.year
		def testDay = testDate.dayOfMonth

		String test_date = "$testYear"+'%'+"$testDay"

		ResultSet resultSet2 = (new database.DBConnection()).executeQuery(connection, "SELECT [test_access_code] FROM custom_models_testaccesscode WHERE ta_user_id='${userId}' and created_date like '${test_date}' ")
		resultSet2.next()
		String testAccessCode = resultSet2.getString(1) as String
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
		return testAccessCode
	}
	/**
	 * Get reservation code from database
	 */
	@Keyword
	def Object getTestReservationCode(String refrenceNumber) {
		// open database connection
		Connection connection = (new database.DBConnection()).connection()

		ResultSet resultSet = ( new database.DBConnection()).executeQuery(connection, "SELECT [reservation_code] FROM custom_models_assessmentprocess ap JOIN custom_models_assessmentprocessassignedtestspecs apats on ap.id = apats.assessment_process_id JOIN custom_models_reservationcodes rc ON apats.id=rc.assessment_process_assigned_test_specs_id WHERE ap.reference_number= '${refrenceNumber}'")
		resultSet.next()
		String testReservationCode = resultSet.getString(1) as String
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
		return testReservationCode

	}

	/**
	 * Get reservation code from database with email
	 */
	@Keyword
	def Object getTestReservationCodeWithEmail(String refrenceNumber, String emailAddress) {
		// open database connection
		Connection connection = (new database.DBConnection()).connection()

		ResultSet resultSet = ( new database.DBConnection()).executeQuery(connection, "SELECT [reservation_code] FROM custom_models_assessmentprocess ap JOIN custom_models_assessmentprocessassignedtestspecs apats on ap.id = apats.assessment_process_id JOIN custom_models_reservationcodes rc ON apats.id=rc.assessment_process_assigned_test_specs_id WHERE ap.reference_number= '${refrenceNumber}' and apats.email ='${emailAddress}'  ")
		resultSet.next()
		String testReservationCode = resultSet.getString(1) as String
		// close database connection
		(new database.DBConnection()).closeConnection(connection)
		return testReservationCode

	}
}
