package database

import java.sql.*

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.util.CryptoUtil

import internal.GlobalVariable

public class DBConnection {
	private static Connection connection = null;

	@Keyword
	def connection(){
		connection = DriverManager.getConnection(GlobalVariable.DB_CONNECTION_URL, CryptoUtil.decode(CryptoUtil.getDefault(GlobalVariable.DB_CONNECTION_ENCRYPTED_USERNAME)), CryptoUtil.decode(CryptoUtil.getDefault(GlobalVariable.DB_CONNECTION_ENCRYPTED_PASSWORD)))
		return connection
	}

	@Keyword
	def executeQuery(Connection connection, String queryString){
		Statement stm = connection.createStatement()
		ResultSet rs = stm.executeQuery(queryString)
		return rs
	}

	@Keyword
	def executeDeleteOrUpdateQuery(Connection connection, String queryString){
		Statement stm = connection.createStatement()
		stm.executeUpdate(queryString)
	}

	@Keyword
	def getConnection(){
		if(connection != null){
			return connection
		}
		else{
			return connection()
		}
	}

	@Keyword
	def closeConnection(Connection c){
		if(c != null && !c.isClosed()){
			c.close()
		}
	}
}
