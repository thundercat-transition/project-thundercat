package database

import java.sql.*

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil

public class DataValidation {
	/**
	 * checking if a specified account already exists
	 */
	@Keyword
	def boolean accountAlreadyExists(String username){
		// initialize alreadyExists boolean
		boolean alreadyExists = false
		// open database connection
		Connection connection = (new database.DBConnection()).connection()

		Integer userId = 0
		// get specified user
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT id FROM user_management_models_user WHERE username = '${username}'")
		try {
			resultSet.next()
			userId = resultSet.getString(1) as Integer
		} catch (Exception e) { System.out.println(username + " has not been found in the database") }

		// get specified user
		ResultSet resultSet2 = (new database.DBConnection()).executeQuery(connection, "SELECT COUNT(*) FROM user_management_models_user WHERE username = '${username}'")
		resultSet2.next()
		Integer count = resultSet2.getString(1) as Integer

		// if count is greater than 0
		if (count > 0) {
			// set alreadyExists to true
			alreadyExists = true
		}

		(new database.DBConnection()).closeConnection(connection)
		// returning boolean value
		return alreadyExists
	}

	/**
	 * checking if a user has a specified permission requested or associated to his account
	 */
	@Keyword
	def boolean userPermissionAlreadyAssociated(String username, String permission){
		// initialize alreadyAssociated boolean
		boolean alreadyAssociated = false
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		Integer userId = 0
		// get specified user
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT id FROM user_management_models_user WHERE username = '${username}'")
		try {
			resultSet.next()
			userId = resultSet.getString(1) as Integer
		} catch (Exception e) { System.out.println("No user found based on specified username/email address") }

		// getting permission ID
		Integer permissionId = null
		try {
			// get permission ID
			ResultSet resultSet1 = (new database.DBConnection()).executeQuery(connection, "SELECT permission_id FROM user_management_models_custompermissions WHERE codename = '${permission}'")
			resultSet1.next()
			permissionId = resultSet1.getString(1) as Integer
		} catch (Exception e) {
			KeywordUtil.markFailedAndStop("The specified permission (" + permission + ") does not exist")
		}
		// get user permission

		ResultSet resultSet2 = (new database.DBConnection()).executeQuery(connection, "SELECT COUNT(*) FROM user_management_models_customuserpermissions WHERE user_id = '${userId}' AND permission_id = ${permissionId}")
		resultSet2.next()
		Integer count1 = resultSet2.getString(1) as Integer

		// get user pending permission
		ResultSet resultSet3 = (new database.DBConnection()).executeQuery(connection, "SELECT COUNT(*) FROM user_management_models_permissionrequest WHERE user_id = '${userId}' AND permission_requested_id = ${permissionId}")
		resultSet3.next()
		Integer count2 = resultSet3.getString(1) as Integer

		// if count1 or count2 is greater than 0
		if (count1 > 0 || count2 > 0) {
			// set alreadyAssociated to true
			alreadyAssociated = true
		}

		(new database.DBConnection()).closeConnection(connection)
		// returning boolean value
		return alreadyAssociated
	}

	/**
	 * checking if a user has a specified active permission associated to his account
	 */
	@Keyword
	def boolean isUserActivePermissionExist(String username, String permission){
		// initialize activePermissionAlreadyExists boolean
		boolean activePermissionAlreadyExists = false
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		Integer userId = 0
		// get specified user
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT id FROM user_management_models_user WHERE username = '${username}'")
		try {
			resultSet.next()
			userId = resultSet.getString(1) as Integer
		} catch (Exception e) { System.out.println("No user found based on specified username/email address") }
		// getting permission ID
		Integer permissionId = null
		try {
			// get permission ID
			ResultSet resultSet1 = (new database.DBConnection()).executeQuery(connection, "SELECT permission_id FROM user_management_models_custompermissions WHERE codename = '${permission}'")
			resultSet1.next()
			permissionId = resultSet1.getString(1) as Integer
		} catch (Exception e) {
			KeywordUtil.markFailedAndStop("The specified permission (" + permission + ") does not exist")
		}
		// get user permission
		ResultSet resultSet2 = (new database.DBConnection()).executeQuery(connection, "SELECT COUNT(*) FROM user_management_models_customuserpermissions WHERE user_id = '${userId}' AND permission_id = ${permissionId}")
		resultSet2.next()
		Integer count = resultSet2.getString(1) as Integer

		// if count1 or count2 is greater than 0
		if (count > 0) {
			// set alreadyAssociated to true
			activePermissionAlreadyExists = true
		}

		(new database.DBConnection()).closeConnection(connection)
		// returning boolean value
		return activePermissionAlreadyExists
	}

	/**
	 * checking if the TA already has a test access (test permission)
	 */
	@Keyword
	def boolean taAlreadyHasTestAccess(String username){
		// initialize alreadyHasTestAccess boolean
		boolean alreadyHasTestAccess = false
		// open database connection
		Connection connection = (new database.DBConnection()).connection()

		// initializing userId
		Integer userId = 0
		// get specified user
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT id FROM user_management_models_user WHERE username = '${username}'")
		try {
			resultSet.next()
			userId = resultSet.getString(1) as Integer
		} catch (Exception e) { System.out.println("No user found based on specified username/email address") }

		// get test access
		ResultSet resultSet2 = (new database.DBConnection()).executeQuery(connection, "SELECT COUNT(*) FROM cms_models_testpermissions WHERE user_id = '${userId}'")
		resultSet2.next()
		Integer count = resultSet2.getString(1) as Integer

		// if count1 or count2 is greater than 0
		if (count > 0) {
			// set alreadyAssociated to true
			alreadyHasTestAccess = true
		}

		(new database.DBConnection()).closeConnection(connection)
		// returning boolean value
		return alreadyHasTestAccess
	}

	/**
	 * checking if a test centre already exists
	 */
	@Keyword
	def boolean testCenterExists(String testCentreName) {
		// initialize testCentreExists boolean
		boolean testCentreExists = false
		// open database connection
		Connection connection = (new database.DBConnection()).connection()
		// get test centre
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT COUNT(*) FROM custom_models_testcenter WHERE name = '${testCentreName}'")
		resultSet.next()
		Integer count = resultSet.getString(1) as Integer

		// if count is greater than 0
		if (count > 0) {
			// set alreadyAssociated to true
			testCentreExists = true
		}

		(new database.DBConnection()).closeConnection(connection)
		// returning boolean value
		return testCentreExists

	}

	/**
	 *  checking if a test session exists in the test center
	 */
	@Keyword
	def boolean testSessionExists(String testCentreName) {
		// initialize testCSessionExists boolean
		boolean testSessionExists = false
		// open database connection
		Connection connection = (new database.DBConnection()).connection()

		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT COUNT(*) FROM custom_models_testcentertestsessions WHERE test_center_id =( SELECT id FROM custom_models_testcenter WHERE name = '${testCentreName}')")
		resultSet.next()
		Integer count = resultSet.getString(1) as Integer

		// if count is greater than 0
		if (count > 0) {
			// set alreadyAssociated to true
			testSessionExists = true
		}

		(new database.DBConnection()).closeConnection(connection)
		// returning boolean value
		return testSessionExists
	}

	/**
	 * check if billing contact exists
	 */
	def boolean billingContactExists(String firstName, String lastName) {
		// initialize alreadyHasTestAccess boolean
		boolean contactExists = false
		// open database connection
		Connection connection = (new database.DBConnection()).connection()

		//get billing contact
		ResultSet resultSet = (new database.DBConnection()).executeQuery(connection, "SELECT COUNT(*) FROM custom_models_billingcontact WHERE first_name = '${firstName}' and last_name = '${lastName}'")

		resultSet.next()
		Integer count = resultSet.getString(1) as Integer

		// if count is greater than 0
		if (count > 0) {
			// set alreadyAssociated to true
			contactExists = true
		}

		(new database.DBConnection()).closeConnection(connection)
		// returning boolean value
		return contactExists
	}
}

