{
  "keywordFileContentHash": "FD41DE9651823EDF2F79FC835CBB7FAF",
  "keywordClasses": [
    {
      "name": "basics.permissions",
      "keywordMethods": [
        {
          "name": "sendPermissionRequest",
          "parameters": [
            {
              "name": "username",
              "initialExpression": "",
              "type": {
                "fullClassName": "java.lang.String",
                "unresolvedClassName": "String",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": false,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            },
            {
              "name": "requestedPermission",
              "initialExpression": "",
              "type": {
                "fullClassName": "java.lang.String",
                "unresolvedClassName": "String",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": false,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            },
            {
              "name": "runErrorValidation",
              "initialExpression": "",
              "type": {
                "fullClassName": "boolean",
                "unresolvedClassName": "boolean",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": true,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            },
            {
              "name": "requestedSecondPermission",
              "initialExpression": "",
              "type": {
                "fullClassName": "java.lang.String",
                "unresolvedClassName": "String",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": false,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            }
          ],
          "javadoc": "/**\n\t * send permission request functionality (need to be logged in to call this keyword)\n\t * this keyword should be called for users without any permission, otherwise the requested permission might not be the right one\n\t * note that if you call this keyword with a user that has pending or active permissions, they will be deleted at the beginning of the execution \n\t */"
        },
        {
          "name": "approvePermissionRequest",
          "parameters": [
            {
              "name": "username",
              "initialExpression": "",
              "type": {
                "fullClassName": "java.lang.String",
                "unresolvedClassName": "String",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": false,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            },
            {
              "name": "firstName",
              "initialExpression": "",
              "type": {
                "fullClassName": "java.lang.String",
                "unresolvedClassName": "String",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": false,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            },
            {
              "name": "lastName",
              "initialExpression": "",
              "type": {
                "fullClassName": "java.lang.String",
                "unresolvedClassName": "String",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": false,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            },
            {
              "name": "approveEttaPermission",
              "initialExpression": "",
              "type": {
                "fullClassName": "boolean",
                "unresolvedClassName": "boolean",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": true,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            }
          ],
          "javadoc": "/**\n\t * approve permission request functionality\n\t * you need access to the super user account (if you want to approve an ETTA permission request) and/or to the ETTA account (if you want to approve any other permissions) \n\t */"
        },
        {
          "name": "denyPermissionRequest",
          "parameters": [
            {
              "name": "username",
              "initialExpression": "",
              "type": {
                "fullClassName": "java.lang.String",
                "unresolvedClassName": "String",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": false,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            },
            {
              "name": "runErrorValidation",
              "initialExpression": "",
              "type": {
                "fullClassName": "boolean",
                "unresolvedClassName": "boolean",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": true,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            }
          ],
          "javadoc": "/**\n\t * deny permission request functionality\n\t * you need access to the ETTA account to call that keyword\n\t * no more than one permission request should exist for the specified user \n\t */"
        },
        {
          "name": "denyProfileModificationRequest",
          "parameters": [
            {
              "name": "username",
              "initialExpression": "",
              "type": {
                "fullClassName": "java.lang.String",
                "unresolvedClassName": "String",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": false,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            },
            {
              "name": "runErrorValidation",
              "initialExpression": "",
              "type": {
                "fullClassName": "boolean",
                "unresolvedClassName": "boolean",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": true,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            }
          ],
          "javadoc": "/**\n\t * Deny User Profile Modification Requests functionality\n\t * you need access to the ETTA account to call that keyword\n\t * no more than one permission request should exist for the specified user\n\t */"
        },
        {
          "name": "assignTestAccess",
          "parameters": [
            {
              "name": "runErrorValidation",
              "initialExpression": "",
              "type": {
                "fullClassName": "boolean",
                "unresolvedClassName": "boolean",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": true,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            },
            {
              "name": "testAccessAlreadyAssigned",
              "initialExpression": "",
              "type": {
                "fullClassName": "boolean",
                "unresolvedClassName": "boolean",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": true,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            },
            {
              "name": "firstname",
              "initialExpression": "",
              "type": {
                "fullClassName": "java.lang.String",
                "unresolvedClassName": "String",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": false,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            },
            {
              "name": "lastname",
              "initialExpression": "",
              "type": {
                "fullClassName": "java.lang.String",
                "unresolvedClassName": "String",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": false,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            }
          ],
          "javadoc": "/**\n\t * assign test access functionality\n\t * you need access to the ETTA account to call this keyword\n\t * set \"testAccessAlreadyAssigned\" to true if the TA already has the test permission, so it will test the test permission already assigned error and assign a second one with a different test order number\n\t */"
        },
        {
          "name": "sendPermissionRequest",
          "parameters": [
            {
              "name": "username",
              "initialExpression": "",
              "type": {
                "fullClassName": "java.lang.String",
                "unresolvedClassName": "String",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": false,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            },
            {
              "name": "requestedPermission",
              "initialExpression": "",
              "type": {
                "fullClassName": "java.lang.String",
                "unresolvedClassName": "String",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": false,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            },
            {
              "name": "runErrorValidation",
              "initialExpression": "",
              "type": {
                "fullClassName": "boolean",
                "unresolvedClassName": "boolean",
                "isArray": false,
                "isGnericsPlaceHolder": false,
                "hasImmutableClassName": true,
                "genericsTypes": [],
                "errorOnParsing": false
              }
            }
          ],
          "javadoc": ""
        }
      ]
    }
  ],
  "javadocParsed": true
}