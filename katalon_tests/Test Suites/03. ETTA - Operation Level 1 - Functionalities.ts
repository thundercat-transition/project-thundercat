<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>03. ETTA - Operation Level 1 - Functionalities</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f9e27766-e528-4980-85c6-68e10c48cbce</testSuiteGuid>
   <testCaseLink>
      <guid>a3d8642e-7c35-40fb-93a3-f309818c08ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03. ETTA - Operation Level 1 - Functionalities/3.1 Access - Verifications/3.1.1-Access-BO-Side-Tab</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5bbffb36-d36f-4999-a77e-51a09e28791f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03. ETTA - Operation Level 1 - Functionalities/3.2 Rights and Permissions - Section/3.2.1-Verify-Permission-Request</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b5536f03-ba73-446a-b3d3-355d5b5ace6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03. ETTA - Operation Level 1 - Functionalities/3.2 Rights and Permissions - Section/3.2.2-Verify-Permissions-Requests-Table-Scorer</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ba957f8d-6a51-4e46-8198-35180685ca41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03. ETTA - Operation Level 1 - Functionalities/3.2 Rights and Permissions - Section/3.2.3-Verify-Active-Permissions-Table</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>85ee4973-942e-476f-b65a-43b171917775</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03. ETTA - Operation Level 1 - Functionalities/3.2 Rights and Permissions - Section/3.2.4-Verify-Active-Test-Accesses-Table</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>adf856bf-5b4f-4137-b68c-4e6696138624</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03. ETTA - Operation Level 1 - Functionalities/3.2 Rights and Permissions - Section/3.2.5-Modify-Delete-Active-Permission</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>8612a75a-b30b-4905-9f51-b31aac826a31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03. ETTA - Operation Level 1 - Functionalities/3.2 Rights and Permissions - Section/3.2.6-Modify-Delete-Test-Access</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>60000a57-ea05-4f8e-8a2c-324eaf6ccac2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03. ETTA - Operation Level 1 - Functionalities/3.3 Test Access Management - Section/3.3.1-Test-Access-Management-Search</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>8e2f5996-37a7-4916-b48f-4a009023e5c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03. ETTA - Operation Level 1 - Functionalities/3.4 Test Access Codes - Section/3.4.1-Verify-Test-Access-Codes-Table</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>96abfcab-965f-4d9a-9ccc-a9c4567552d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03. ETTA - Operation Level 1 - Functionalities/3.5 Active Tests - Section/3.5.1-Verify-Active-Tests-Table</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>eae6b94d-7452-439d-9f7e-dd863c7a5c6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/03. ETTA - Operation Level 1 - Functionalities/3.6 User Look Up - Section/3.6.1-Verify-User-Lookup-Table</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
