<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>02. Standard User - Functionalities</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b27ffa33-3e5b-4952-9e3a-40bd342902cc</testSuiteGuid>
   <testCaseLink>
      <guid>7fd42b3e-f3eb-471c-b123-e757db94c4c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02. Standard User - Functionalities/2.1 Password - Functionnalities/2.1.1-Verify-Password-Login-Field</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4c6ee154-e72a-4a68-ab00-33c65a91f754</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02. Standard User - Functionalities/2.1 Password - Functionnalities/2.1.2-Modify-Password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>8905065f-1400-4bcb-b3a0-963772f3ecbb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02. Standard User - Functionalities/2.1 Password - Functionnalities/2.1.3-Forgot-Password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1934ea53-194f-480d-86d8-79e224052e96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02. Standard User - Functionalities/2.2 Language - Functionnalities/2.2.1-Verify-Language-Switching</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>39924214-4740-4fdf-b36c-fb6e52320f2b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02. Standard User - Functionalities/2.3 My Profile - Functionnalities/2.3.1-Complete-My-Profile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2bfb05bd-b0e9-4151-a234-2dd1c935d23e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02. Standard User - Functionalities/2.4 Accessibility - Functionnalities/2.4.1-Accessibility-Settings</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>239177b7-1333-424e-9dff-c43d1e870a84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02. Standard User - Functionalities/2.4 Accessibility - Functionnalities/2.4.2-Profile-Accessibility-Settings</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a20abdce-25de-455f-91fe-50acfd679fb1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02. Standard User - Functionalities/2.5 Login - Functionnalities/2.5.1-Verify-Locked-After-Unsuccessfull-Logins</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>350a9ff7-5130-402d-9fa2-ea3c2d41d001</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02. Standard User - Functionalities/2.6 Sample Test - Functionnalities/2.6.1-Verify-Sample-Tests</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6dbc9e2e-768b-4129-b14b-90b6b4604f8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02. Standard User - Functionalities/2.7 Create Account - Functionnalities/2.7.1-Verify-Duplicate-Account</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>188da99a-4e4f-4857-99bd-93f858cf46ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/02. Standard User - Functionalities/2.8 Test Access Code - Functionnalities/2.8.1-Generate-Test-Access-Code-And-Check-In</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
