<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div</name>
   <tag></tag>
   <elementGuidId>7135055f-5638-452d-8f45-eb93a6c10d06</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='test-center-test-sessions-room-react-select-dropdown']/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>524cb4a6-de40-4644-a6d2-67a952fe189e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;test-center-test-sessions-room-react-select-dropdown&quot;)/div[@class=&quot;css-o1r4wa-control&quot;]/div[@class=&quot;css-3nk303&quot;]</value>
      <webElementGuid>cd080e46-23ec-4857-b596-fdf266c0f886</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value> css-3nk303</value>
      <webElementGuid>0e962377-0ff7-4060-be67-95dba2352087</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='test-center-test-sessions-room-react-select-dropdown']/div/div</value>
      <webElementGuid>6d840ecd-0e2a-4e88-974f-eeea74f4211f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select'])[1]/following::div[3]</value>
      <webElementGuid>e21af49c-72da-406d-bb1d-a8a45228353f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Room:'])[1]/following::div[5]</value>
      <webElementGuid>3ba60381-e104-404a-a061-c5bc44b2c81c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Open to other departments:'])[1]/preceding::div[7]</value>
      <webElementGuid>633531d7-cde9-4f1f-bd43-8d726729a1b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date:'])[1]/preceding::div[15]</value>
      <webElementGuid>541bc43b-e4f9-40fd-9c74-a7c550541569</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[2]/div/div/div/div/div[2]/div/div/div/div</value>
      <webElementGuid>150f6a37-d2a4-4817-822a-0b69c8e99bba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='test-centers-column-5-label']/div/span/div</value>
      <webElementGuid>11b28bd2-8ed5-45e4-b945-044dd41b6133</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Katalon Test Centre'])[1]/following::div[5]</value>
      <webElementGuid>6927c58c-86c3-40df-a3ec-6a425c6821cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/following::div[6]</value>
      <webElementGuid>ca477951-b316-4938-ba86-c02b71cc63bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous page'])[1]/preceding::div[4]</value>
      <webElementGuid>ad3e7f16-37b7-46d0-b109-4ced17f2fb03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next page'])[1]/preceding::div[5]</value>
      <webElementGuid>5636bf28-61ec-4070-90db-125679b51d9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/span/div</value>
      <webElementGuid>b4e09069-0d6f-4567-9b4f-9d479b67c3f7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
