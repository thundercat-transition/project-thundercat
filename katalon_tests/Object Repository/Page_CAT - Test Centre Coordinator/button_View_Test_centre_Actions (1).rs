<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_View_Test_centre_Actions (1)</name>
   <tag></tag>
   <elementGuidId>3262e534-5eda-4bd5-b7a5-4b48d9eeb953</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='test-center-test-administrators-column-4-label']/div/span/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#test-center-test-administrators-column-4-label > div.notranslate > span > div > button.primary.undefined</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>7144d2bf-8fd6-4f65-84bb-1b73556cb7e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>primary undefined</value>
      <webElementGuid>bbf51af4-73d4-4aab-89c5-a42a607f99b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-for</name>
      <type>Main</type>
      <value>test-center-test-administrators-delete-row-0</value>
      <webElementGuid>766bef5f-8aed-4825-8dcc-31f9d92d6eea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Delete Katalon TA from the list of associated test administrators.</value>
      <webElementGuid>f4b4991a-97f6-4a55-a73c-519a2f1e998e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>currentitem</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>e135c6fd-4a4d-46a0-a1bd-f0c584e5f508</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>test-center-test-administrators-delete-row-0</value>
      <webElementGuid>da885b51-8d43-4cdf-8ac0-04b001d329e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;test-center-test-administrators-column-4-label&quot;)/div[@class=&quot;notranslate&quot;]/span[1]/div[1]/button[@class=&quot;primary undefined&quot;]</value>
      <webElementGuid>d54c63e6-17e6-4d72-9616-23b8c6adfb89</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='test-center-test-administrators-column-4-label']/div/span/div/button</value>
      <webElementGuid>d986471c-aac0-4ed7-9b39-33e62cff2928</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='katalon.ta@email.ca'])[1]/following::button[1]</value>
      <webElementGuid>2748abde-e5dc-4316-b4de-c4682d070c2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TA'])[1]/following::button[1]</value>
      <webElementGuid>46637622-2adb-4317-b9b8-8fa3f7127fc7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Room'])[1]/preceding::button[1]</value>
      <webElementGuid>81248117-adcb-46cb-9289-2bb747627937</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search:'])[7]/preceding::button[2]</value>
      <webElementGuid>238fa118-0ed4-42cc-b840-25d304e7e261</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[4]/div/span/div/button</value>
      <webElementGuid>1f7ab475-6ac6-437c-8856-b659e725bfdb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='test-centers-column-5-label']/div/span/div/button</value>
      <webElementGuid>56637758-39d4-4481-93eb-9c1063273a4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Katalon Test Centre'])[1]/following::button[1]</value>
      <webElementGuid>81ae8236-0ee2-42f6-85e4-a8b3a3161edf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/following::button[1]</value>
      <webElementGuid>27280579-0501-4ce2-96f3-6d2b18b6584e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous page'])[1]/preceding::button[1]</value>
      <webElementGuid>6858ad13-bb5b-4081-bac1-1f82aca6b76a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next page'])[1]/preceding::button[1]</value>
      <webElementGuid>c42af5d3-3251-4ff6-9ddd-2ee54a43b344</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/div/button</value>
      <webElementGuid>326c50fe-3f2e-4d86-aa7f-f670c18e8053</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='test-center-rooms-column-4-label']/div/span[2]/div/button</value>
      <webElementGuid>70d335bf-e6f2-4ba5-948a-347168f735c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Active'])[1]/following::button[2]</value>
      <webElementGuid>00be659f-66b3-4ef2-9dba-1dc282288cb3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Katalon Test Room'])[1]/following::button[2]</value>
      <webElementGuid>03bb8564-d019-4329-aa87-153b8672c738</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/div/button</value>
      <webElementGuid>353d2405-f48f-48ac-83d7-5304069db714</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='test-center-accesses-column-2-label']/div/div/span/div/button</value>
      <webElementGuid>2f839acb-3d39-44bc-88b9-5841294320fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='TATwo, Katalon (katalon.tatwo@email.ca)'])[1]/following::button[1]</value>
      <webElementGuid>f2266c4f-6fd6-4f7b-882b-9035f2db7b66</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
