<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Add Room</name>
   <tag></tag>
   <elementGuidId>e8f1b286-aaac-4f40-bf2c-ef8b24c88c77</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='test-center-management-tabs-tabpane-test-center-management-rooms']/div/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#test-center-management-tabs-tabpane-test-center-management-rooms > div > div > button.primary.undefined</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>3ec6204e-4e82-4818-b0de-decbf1fb697f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>primary undefined</value>
      <webElementGuid>5c4f25b1-237d-427c-be6b-f14426733b63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add Room</value>
      <webElementGuid>84ab821d-5a68-42e4-8e0d-5fde24e7a9b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;test-center-management-tabs-tabpane-test-center-management-rooms&quot;)/div[1]/div[1]/button[@class=&quot;primary undefined&quot;]</value>
      <webElementGuid>989f27aa-c329-412e-9ecd-3b3cfd4172de</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='test-center-management-tabs-tabpane-test-center-management-rooms']/div/div/button</value>
      <webElementGuid>feb43f7f-db65-4db0-903a-5294268233c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='There are no test administrators at this test centre.'])[1]/following::button[1]</value>
      <webElementGuid>8f0f4ff9-efaa-4d78-971c-cc84ea3001aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/following::button[1]</value>
      <webElementGuid>a5b9f5ee-c689-4c1c-baa0-a3e4a036c9ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search:'])[7]/preceding::button[1]</value>
      <webElementGuid>ee67dcc4-8214-4855-85c4-334585f7ae3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/button</value>
      <webElementGuid>2818c9a7-f729-4d0b-afe8-3bac22892421</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Add Room' or . = 'Add Room')]</value>
      <webElementGuid>d41ff2ed-9537-497f-8247-9efef124084f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
