<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Add a Test Administrator</name>
   <tag></tag>
   <elementGuidId>8a7c9e64-f44d-4a43-8090-806d3f76389d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='test-center-management-tabs-tabpane-test-center-management-test-administrators']/div/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#test-center-management-tabs-tabpane-test-center-management-test-administrators > div > div > button.primary.undefined</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c7b13a8e-f89a-438b-849d-c69479f02bbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>primary undefined</value>
      <webElementGuid>d5350855-8444-4131-ae7c-26408bf7a481</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add a Test Administrator</value>
      <webElementGuid>efae0924-4a94-496a-875f-3192d06e0588</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;test-center-management-tabs-tabpane-test-center-management-test-administrators&quot;)/div[1]/div[1]/button[@class=&quot;primary undefined&quot;]</value>
      <webElementGuid>170b1151-7c06-44c4-acd4-d1fcfbcde019</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='test-center-management-tabs-tabpane-test-center-management-test-administrators']/div/div/button</value>
      <webElementGuid>6ef9739c-c1d8-4774-97f7-199e6970ceb3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Saved'])[1]/following::button[1]</value>
      <webElementGuid>f5702590-2a5b-4450-acb9-92e9f9a2639a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Other information:'])[1]/following::button[3]</value>
      <webElementGuid>cbf3225b-a269-48b2-aceb-ea729bf3348f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search:'])[4]/preceding::button[1]</value>
      <webElementGuid>a5d8abfc-1698-4b29-93ce-1b5bb9953f6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div[2]/div/div/div/div[2]/div/div/button</value>
      <webElementGuid>eb956765-0bd3-447c-8179-8741e2b82716</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Add a Test Administrator' or . = 'Add a Test Administrator')]</value>
      <webElementGuid>3440a686-2b5d-4ad7-bb56-b4f856f47045</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
