<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Rooms</name>
   <tag></tag>
   <elementGuidId>b6c763ca-16b3-4af5-a08c-9ba7aeab787e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='test-center-management-tabs-tab-test-center-management-rooms']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#test-center-management-tabs-tab-test-center-management-rooms</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>c816dd90-a473-4e92-841f-e638de9fa43c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>test-center-management-tabs-tab-test-center-management-rooms</value>
      <webElementGuid>4b5482bb-262a-4292-a05f-5d839975a3d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>8ed45638-d2ae-4fee-894f-8b05a261377a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>tab</value>
      <webElementGuid>989ef055-9dc5-40a0-a689-2dc12d3f1960</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-rb-event-key</name>
      <type>Main</type>
      <value>test-center-management-rooms</value>
      <webElementGuid>b2ce04c5-8e90-46ba-ace2-eb65a25695ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>test-center-management-tabs-tabpane-test-center-management-rooms</value>
      <webElementGuid>e3211e58-2a29-4094-b2d7-e8d7f62e4bb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-selected</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>4a303e84-c4d8-44f0-a084-08c6ad3858ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-item nav-link</value>
      <webElementGuid>c4954b08-30c9-4bfa-a994-4f7d27f87fb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Rooms</value>
      <webElementGuid>87745241-244d-4fb2-98af-148c6c7f6842</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;test-center-management-tabs-tab-test-center-management-rooms&quot;)</value>
      <webElementGuid>52593182-e5e8-42af-8cce-715b70c6a0bd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='test-center-management-tabs-tab-test-center-management-rooms']</value>
      <webElementGuid>0ef98774-46a8-4185-a958-4c5a89d5e2f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='id-vpkjag-4']/div/div/div/div[2]/div/div/nav/a[3]</value>
      <webElementGuid>970960a4-6638-45f3-ad56-6efa4645db77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Rooms')]</value>
      <webElementGuid>55eeb6cc-66b6-4d7c-a338-682e8d9f840c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Centre Management'])[2]/following::a[3]</value>
      <webElementGuid>e1f99371-6ef7-48cb-9bb9-1163bd084e60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='There are no Test Sessions'])[1]/following::a[3]</value>
      <webElementGuid>94a0d96a-9c1c-4d4f-8fd3-90c8bbb1f7a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Organization:'])[1]/preceding::a[1]</value>
      <webElementGuid>07a5e5f2-3db5-4789-bcac-1bfc7f850502</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Public Service Commission (PSC)'])[1]/preceding::a[1]</value>
      <webElementGuid>af829b01-ce9e-47ca-9957-5c8e0d6b33ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Rooms']/parent::*</value>
      <webElementGuid>c5e6c10d-a2e1-4187-9efa-69bd490e9153</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[6]</value>
      <webElementGuid>aad0d9f2-8508-42d2-b52c-741dc18ef9b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[3]</value>
      <webElementGuid>f2e270d9-a1ae-4fe5-9f84-03b91a8d810e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'test-center-management-tabs-tab-test-center-management-rooms' and @href = '#' and (text() = 'Rooms' or . = 'Rooms')]</value>
      <webElementGuid>f195ab43-70a7-4ca4-ba69-4c58c5217be4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
