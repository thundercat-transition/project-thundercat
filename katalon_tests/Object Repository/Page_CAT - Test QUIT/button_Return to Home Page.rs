<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Return to Home Page</name>
   <tag></tag>
   <elementGuidId>2df7f2de-bc54-4b1b-8774-547a01b72d84</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.primary-wide.undefined</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='unit-test-go-home-button']/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>af2691a8-4670-4bec-b1d5-b89340930a8b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>primary-wide undefined</value>
      <webElementGuid>d36cd129-f6dd-4b7a-b5de-45fed14c67ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Return to Home Page</value>
      <webElementGuid>d9e2f2f2-6570-44b0-87a1-ddfa8da81a09</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;unit-test-go-home-button&quot;)/button[@class=&quot;primary-wide undefined&quot;]</value>
      <webElementGuid>134b0973-8b24-42c6-b9c2-a09d3297d6f8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='unit-test-go-home-button']/button</value>
      <webElementGuid>35bc80aa-00e9-4733-90c8-662ec1814ee5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='You have quit the test.'])[1]/following::button[1]</value>
      <webElementGuid>8a815c30-253b-4fc7-8525-009fde0ab973</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Français'])[1]/following::button[1]</value>
      <webElementGuid>193a14af-814e-4eef-85bf-3939f95a7d8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Return to Home Page']/parent::*</value>
      <webElementGuid>99661894-c455-4f04-8426-dc4b03417e0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div[2]/button</value>
      <webElementGuid>b524505a-379e-437a-a736-f7e6bfe26f1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Return to Home Page' or . = 'Return to Home Page')]</value>
      <webElementGuid>4dc8c5c0-26e9-4307-b266-f21ae80287c0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
