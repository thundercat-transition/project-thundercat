<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Checked-in_svg-inline--fa fa-thumbs-up fa-w-16</name>
   <tag></tag>
   <elementGuidId>c3291e6f-8e1d-4934-81d2-e43915b5f27a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;active-candidates-column-6-label&quot;)/div[1]/div[1]/span[2]/div[1]/button[@class=&quot;secondary&quot;]/svg[@class=&quot;svg-inline--fa fa-thumbs-up fa-w-16&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Checked-in'])[1]/following::*[name()='svg'][3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>619d8514-6bd0-41c9-8768-cb9672168177</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>ff851ccd-ab29-4f68-b425-bdd3e531f9c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>e6ab5e46-967a-4c6e-9e4b-a4e13d495685</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-prefix</name>
      <type>Main</type>
      <value>far</value>
      <webElementGuid>71fa3826-bb5a-4d19-b74e-41d5adf4f134</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-icon</name>
      <type>Main</type>
      <value>thumbs-up</value>
      <webElementGuid>212b7ff1-faa9-4268-99a3-d8e49e32a5e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>svg-inline--fa fa-thumbs-up fa-w-16 </value>
      <webElementGuid>1f1188cc-de69-417f-9c1c-c63a0eed35f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>8fba43f3-a17d-4129-b82a-ecb4a9fdbfbb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>http://www.w3.org/2000/svg</value>
      <webElementGuid>237fff72-1914-4291-b781-a9b049e00901</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 512 512</value>
      <webElementGuid>85f63b8e-aacb-4f5b-af72-4316652c4edf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;active-candidates-column-6-label&quot;)/div[1]/div[1]/span[2]/div[1]/button[@class=&quot;secondary&quot;]/svg[@class=&quot;svg-inline--fa fa-thumbs-up fa-w-16&quot;]</value>
      <webElementGuid>10f10ddc-b502-4afa-a613-2f06c52b1058</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checked-in'])[1]/following::*[name()='svg'][2]</value>
      <webElementGuid>37476543-3d5c-42e0-be8b-3047a0abd409</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SQ F000 A1'])[1]/following::*[name()='svg'][2]</value>
      <webElementGuid>8c992766-15aa-4025-af87-0f17bd523dc0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reports'])[2]/preceding::*[name()='svg'][4]</value>
      <webElementGuid>84f0d783-6c25-4ed0-a37d-5aa3b96e3ffd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Report Type:'])[1]/preceding::*[name()='svg'][4]</value>
      <webElementGuid>f55f3db6-6028-4911-af93-08b2ec6da6fd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
