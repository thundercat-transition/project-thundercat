<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Checked-in_secondary_approve</name>
   <tag></tag>
   <elementGuidId>fa10ab15-0eb8-417f-b3e2-addfb6b72526</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Checked-in'])[1]/following::*[name()='svg'][3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5c2a0d8b-9157-47bb-8578-86ba37a1d4d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>secondary</value>
      <webElementGuid>fd86733e-32df-44b3-9a0b-443fed9d0203</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-for</name>
      <type>Main</type>
      <value>approve-candidate-tooltip-0</value>
      <webElementGuid>9c8c9d0b-4f08-4f39-bef2-87233dabafc8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Approve user Katalon UserThree (katalon.userthree@email.ca). Their date of birth is 2000-02-20. Their test form is SQ F000 A1. Their current test status is Checked-in. Their test timer is currently set to 00 hours and 45 minutes.</value>
      <webElementGuid>db8be526-e1e5-4a82-b095-372bf252c05b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>a64402b7-2f33-46f2-bc48-ef1efaf9af7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>currentitem</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>5525f0dd-d6ce-4b89-bf35-32a6085adeb5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;active-candidates-column-6-label&quot;)/div[1]/div[1]/span[2]/div[1]/button[@class=&quot;secondary&quot;]</value>
      <webElementGuid>1f410608-04c7-4a37-b798-e3fadc57b75a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='active-candidates-column-6-label']/div/div/span[2]/div/button</value>
      <webElementGuid>91d3b562-b48d-4500-9511-89d1b343c793</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checked-in'])[1]/following::button[2]</value>
      <webElementGuid>de2f50d7-e8ea-4380-96be-02ffd0ed197e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SQ F000 A1'])[1]/following::button[2]</value>
      <webElementGuid>602cab23-4029-4087-9a14-44de992657a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reports'])[2]/preceding::button[4]</value>
      <webElementGuid>3ada4f7d-ffec-4289-8f4c-fd013c5f66c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Report Type:'])[1]/preceding::button[4]</value>
      <webElementGuid>6c5fd9a7-2277-4858-a1e4-bae9a09f2885</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/div/button</value>
      <webElementGuid>f0493a66-322f-4019-ad86-d7ecbe55fa04</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
