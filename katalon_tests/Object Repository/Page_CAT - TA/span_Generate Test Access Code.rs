<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Generate Test Access Code</name>
   <tag></tag>
   <elementGuidId>a67afb98-bcfe-49a9-8024-98e54068661e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Generate Test Access Code' or . = 'Generate Test Access Code')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*/text()[normalize-space(.)='Generate Test Access Code']/parent::*</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>8e573595-7c90-40de-84c9-780b4d1f4820</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Generate Test Access Code</value>
      <webElementGuid>61fc16e1-1e63-42a6-93bc-1e6d282fa672</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;generate-test-access-code-button&quot;)/button[1]/span[2]</value>
      <webElementGuid>3c4555a4-e68a-49e7-a02c-687904b3db7e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='generate-test-access-code-button']/button/span[2]</value>
      <webElementGuid>14fb2198-eaa1-4181-85dd-3c8b822276d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/following::span[2]</value>
      <webElementGuid>97019857-fdac-460b-89e8-43fe2c2c48ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assessment Process Number'])[1]/following::span[2]</value>
      <webElementGuid>0c95eb10-22b8-4ee5-b0c5-0b6a14ef5fbb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Active Candidates'])[2]/preceding::span[1]</value>
      <webElementGuid>8acfc261-3f42-4689-8ebe-7bba48372dcb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lock All'])[1]/preceding::span[2]</value>
      <webElementGuid>0bb8ffa7-2b68-4979-8cc1-5ec6d0ff4b89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Generate Test Access Code']/parent::*</value>
      <webElementGuid>c929a86f-01d0-47d1-88b1-ea2f2ecd35e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]</value>
      <webElementGuid>a07467b9-f3b6-45e1-afd6-df6dc8daddcf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
