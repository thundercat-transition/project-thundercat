<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Delete</name>
   <tag></tag>
   <elementGuidId>d03d456a-2038-483d-b291-128bd705d0b4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='unit-test-right-btn']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#unit-test-right-btn > div > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>b09cf7dc-347d-4819-9316-6a40f24b4a96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Delete</value>
      <webElementGuid>60118343-b33c-4aa7-8297-6b329ac399b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;unit-test-right-btn&quot;)/div[1]/span[1]</value>
      <webElementGuid>38917f5a-9d4b-49de-b0d1-e97ac928703b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//button[@id='unit-test-right-btn']/div/span</value>
      <webElementGuid>88df08d1-2b0a-4a8b-adb3-d11296f51916</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::span[1]</value>
      <webElementGuid>d7b39e45-8842-47af-b97a-95685b99230f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CHVBBZ0MXB'])[2]/following::span[2]</value>
      <webElementGuid>a9cf4acd-1af9-4f71-b63e-b1a1a836bf54</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/button/div/span</value>
      <webElementGuid>e5717b21-cbd9-4109-affc-a52e0f307224</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
