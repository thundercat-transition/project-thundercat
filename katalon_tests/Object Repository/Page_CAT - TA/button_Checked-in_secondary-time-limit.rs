<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Checked-in_secondary-time-limit</name>
   <tag></tag>
   <elementGuidId>cd9f0174-8838-49ff-97fd-22edc34e62ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span/div/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;active-candidates-column-6-label&quot;)/div[1]/div[1]/span[1]/div[1]/button[@class=&quot;secondary&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span > div > button.secondary</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>875c79bd-d486-4be8-96c2-da92d8d17365</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>secondary</value>
      <webElementGuid>3abbbaa4-f32e-462e-9373-f737e9dd6ffb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-for</name>
      <type>Main</type>
      <value>update-test-time-tooltip-0</value>
      <webElementGuid>d52529fc-b48d-4e25-8f52-a55eec05b396</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Edit the test timer of user Katalon UserThree (katalon.userthree@email.ca). Their date of birth is 2000-02-20. Their test form is SQ F000 A1. Their current test status is Checked-in. Their test timer is currently set to 00 hours and 45 minutes.</value>
      <webElementGuid>47ecce9c-63c4-435d-a8a4-1b41cb6243ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>c76559a4-1d4c-4127-9daa-d581caf152ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>currentitem</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>bf18185f-ae4f-41ea-9ba1-e4af942f864d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;active-candidates-column-6-label&quot;)/div[1]/div[1]/span[1]/div[1]/button[@class=&quot;secondary&quot;]</value>
      <webElementGuid>dd7d9747-b7df-45c7-8e43-148c51220a9c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='active-candidates-column-6-label']/div/div/span/div/button</value>
      <webElementGuid>6fda1e01-8bb6-4721-9a89-f669355b4c84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checked-in'])[1]/following::button[1]</value>
      <webElementGuid>7c8619fd-2faf-405f-af77-c373327fd318</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SQ F000 A1'])[1]/following::button[1]</value>
      <webElementGuid>117043aa-a619-44af-aa70-91a17e50c626</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reports'])[2]/preceding::button[5]</value>
      <webElementGuid>ad889184-7d7e-40a7-826f-65a4a2854591</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Report Type:'])[1]/preceding::button[5]</value>
      <webElementGuid>c830d527-e5bc-42c2-99d0-7241a865a29d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/div/button</value>
      <webElementGuid>f04ba13b-6ade-4a40-9b41-e90261f85ddc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
