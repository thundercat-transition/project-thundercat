<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>active_candidates_resync_first_candidate</name>
   <tag></tag>
   <elementGuidId>fb122599-0b95-424f-a208-6068e06e243a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.react-switch-handle</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='uat-tabs-tabpane-invitations']/div/div[2]/div[2]/div/div[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;active-candidates-table-row-0&quot;)/td[@id=&quot;active-candidates-column-6-label&quot;]/div[1]/div[1]/span[5]/div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c060ed04-edbf-422a-be5d-a8fb45cd5492</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;uat-tabs-tabpane-invitations&quot;)/div[1]/div[@class=&quot;justify-content-end align-items-center row&quot;]/div[@class=&quot;col-xl-7 col-lg-6 col-md-5 col-sm-5 col-5&quot;]/div[1]/div[@class=&quot;react-switch-handle&quot;]</value>
      <webElementGuid>82d3e8fc-abb8-4eb3-ac2b-a3518b1b6621</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>react-switch-handle</value>
      <webElementGuid>9d1e1106-d3c4-466a-a3c8-1e0210c21240</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>692c2ff2-246e-4e0b-93f3-7945f4a20ca3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='uat-tabs-tabpane-invitations']/div/div[2]/div[2]/div/div[2]</value>
      <webElementGuid>c4cd8621-ec9c-40dd-a001-a92f623d70af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]</value>
      <webElementGuid>373c0123-d306-446d-87a8-f6c8b62deb32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='uat-tabs-tabpane-invitations']/div/div[2]/div[2]/div/div</value>
      <webElementGuid>4b2ad62a-e9d8-480f-ad26-17cf45f1818e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reuse Data:'])[1]/following::div[3]</value>
      <webElementGuid>977d55af-7360-473e-bba9-2b7ca4864322</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The selected test order number is:'])[1]/following::div[5]</value>
      <webElementGuid>71fdf3cf-7c9c-4b04-8e3f-8d5874087da4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Internal Reference Number:'])[1]/preceding::div[4]</value>
      <webElementGuid>05c7f1a0-628a-4906-bd23-cd0d74362dc6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Organization:'])[1]/preceding::div[7]</value>
      <webElementGuid>318fa99d-184f-4fab-8263-8c979d374e1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div[2]/div[2]/div/div</value>
      <webElementGuid>417b70cc-caa6-4a9b-b435-00937ca1a4f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='uit-test-order-number-dropdown-react-select-dropdown']/div/div[2]/div</value>
      <webElementGuid>3c8354aa-5af8-4f5e-ae63-4a080f23472c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='This1 (Process1)'])[2]/following::div[5]</value>
      <webElementGuid>0b0308cd-46f6-4b49-a97d-96010f6c9eb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select is focused ,type to refine list, press Down to open the menu,'])[1]/following::div[8]</value>
      <webElementGuid>7a18b162-3bc2-42ff-914f-5e94e1b40b69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The selected test order number is:'])[1]/preceding::div[1]</value>
      <webElementGuid>5897c234-9712-4547-8ef3-e659eb280f08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test:'])[1]/preceding::div[1]</value>
      <webElementGuid>7e8469f3-e688-4928-b969-8fe9b139130c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div/div[2]/div/div/div/div[2]/div</value>
      <webElementGuid>1521e3d4-18a6-4168-b9d5-c7780ef154b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='uit-tests-dropdown-react-select-dropdown']/div/div[2]/div</value>
      <webElementGuid>0a5701ba-077b-4b3e-8969-8ff29358b42c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select'])[2]/following::div[5]</value>
      <webElementGuid>539b7f4c-eaaf-4e37-8cc5-e028819da4a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='option 208A2E - Unsupervised Internet Test of Cognitive Ability—Level 1, selected.'])[1]/following::div[8]</value>
      <webElementGuid>2468b595-c5eb-4c17-b6fa-b9ec7a5ab783</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The selected tests are: None'])[1]/preceding::div[1]</value>
      <webElementGuid>70677831-9302-43ac-9771-5468a08e76c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='List of Candidates:'])[1]/preceding::div[1]</value>
      <webElementGuid>fd7261b8-31eb-4399-b9e9-928271ba92a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div/div[2]/div</value>
      <webElementGuid>2ea0927c-0f9f-4528-845d-fd8529000503</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='active-candidates-column-6-label']/div/div/span[5]/div</value>
      <webElementGuid>86c26409-2ecb-4ce7-b205-b889e6aab60e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checked-in'])[1]/following::div[20]</value>
      <webElementGuid>0f0429fe-0e57-44cb-aab0-492a1324e6bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SQ F639 T1N'])[1]/following::div[21]</value>
      <webElementGuid>5ddbc5db-22ad-47a2-92c3-bda6d125c463</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Two, Username (username2@email.ca)'])[1]/preceding::div[4]</value>
      <webElementGuid>361da48f-a72a-490a-924c-805dd0d14648</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SQ F639 T1N'])[2]/preceding::div[6]</value>
      <webElementGuid>5e15229a-0534-4f6a-b906-cd9a6fc0b5b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[5]/div</value>
      <webElementGuid>f3172f80-3c51-4da0-a193-06307d0940ca</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
