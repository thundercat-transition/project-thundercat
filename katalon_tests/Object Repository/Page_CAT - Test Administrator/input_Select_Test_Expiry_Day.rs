<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Select_Test_Expiry_Day</name>
   <tag></tag>
   <elementGuidId>95e54e4f-3c97-49bc-9f26-f5a8d9a46b9a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#react-select-7-input</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='react-select-7-input']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'text' and @aria-labelledby = 'uit-validity-end-date-label    day-field-selected current-day-value-intro date-picker-day-field-value-label']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>8f9580b5-b950-4392-8d0c-5c096f8390a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>e8025e03-2957-42fb-aad6-4911b88da02f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>4baa399a-d8df-4e74-9dcc-2d19c02bb49b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>89dc38fc-f3fc-4b26-847d-1ce78c542ea3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>react-select-7-input</value>
      <webElementGuid>ace1994c-4891-4d46-afae-1ab7661a9cef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>spellcheck</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>4f57df73-2c2d-45ed-babd-217a6394a699</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>44cd72c4-d6b7-403b-9b23-1fdea6c10ff9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>232cb6d6-9cd0-465f-ba5f-20cab20262e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-autocomplete</name>
      <type>Main</type>
      <value>list</value>
      <webElementGuid>54084cce-7938-4531-8e99-a3ae197f46c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>uit-validity-end-date-label    day-field-selected current-day-value-intro date-picker-day-field-value-label</value>
      <webElementGuid>b402e506-c8af-4de9-a893-0402281b4c34</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>c9ea00af-dd9a-4569-b7a2-c6f6b20acea8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-select-7-input&quot;)</value>
      <webElementGuid>28b4f1d1-9e33-4dbd-abae-e8d56dcdcb57</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='react-select-7-input']</value>
      <webElementGuid>7a736ef8-ca67-4bb5-a8dd-65d1c2a11626</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='date-picker-day-field-react-select-dropdown']/div/div/div/div/input</value>
      <webElementGuid>a291faf0-866d-4e66-aeec-79c36165c3cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/div/div/div/div/div/div/div/div/div/div/div/input</value>
      <webElementGuid>6d797ca0-b888-4e4c-9841-c4e6c23b48e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'react-select-7-input' and @type = 'text']</value>
      <webElementGuid>3c545ce9-c3ee-403d-8610-a6cd5256b189</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
