<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Generate Test Access Code</name>
   <tag></tag>
   <elementGuidId>9e87e71b-7355-4dcd-b061-222627727d39</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>label.notranslate > button.undefined.undefined</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='test-access-codes-no-data']/td/label/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>61b596e1-c0e1-40bd-b6fc-d1f275a53781</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>undefined undefined</value>
      <webElementGuid>a46b9ea9-eb93-43cf-b714-8b3bc1d3960d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Generate Test Access Code</value>
      <webElementGuid>2fdc71eb-1066-4f51-82ac-8da87469ecad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;id-j7tjea-5&quot;)/div[1]/div[1]/div[1]/div[3]/div[1]/div[@class=&quot;table-responsive&quot;]/table[@id=&quot;test-access-codes-table-id&quot;]/tbody[1]/tr[@id=&quot;test-access-codes-no-data&quot;]/td[1]/label[@class=&quot;notranslate&quot;]/button[@class=&quot;undefined undefined&quot;]</value>
      <webElementGuid>fbac7c71-66ed-400c-ba06-17562fb4e4ec</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='test-access-codes-no-data']/td/label/button</value>
      <webElementGuid>ca9fa4cc-1e29-40ef-abd4-793d748513a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[2]/following::button[1]</value>
      <webElementGuid>ca63fd55-cfb1-48a3-a79e-cecb68a8a358</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[3]/following::button[1]</value>
      <webElementGuid>7c4c4f9a-2d34-458e-97cf-84faeb8a26bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Approve All'])[1]/preceding::button[1]</value>
      <webElementGuid>e4546027-061f-4aea-9dca-df5776b19904</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label/button</value>
      <webElementGuid>008cc4f6-c5ec-48f9-98ed-7265ac562928</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Generate Test Access Code' or . = 'Generate Test Access Code')]</value>
      <webElementGuid>bcd69a4b-1ad7-4481-be6a-b3113344e360</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
