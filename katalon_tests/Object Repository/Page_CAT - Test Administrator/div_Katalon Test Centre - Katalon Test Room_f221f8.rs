<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Katalon Test Centre - Katalon Test Room_f221f8</name>
   <tag></tag>
   <elementGuidId>8ba3bc41-e38e-4758-8c62-6c5011e69d04</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='react-select-12-option-0']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#react-select-12-option-0</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a8473daf-0185-4d19-b2e9-86e24d39d566</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value> css-1xsjzd7-option</value>
      <webElementGuid>dce51c5c-1a29-4196-85a2-2c61d5ced5db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>react-select-12-option-0</value>
      <webElementGuid>2939fc69-97ca-4ddd-bc82-5f8edd26c2ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
      <webElementGuid>4c302c73-226b-45df-9aac-f098ab3a037c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Katalon Test Centre - Katalon Test Room: 2024-05-23 (17:00 - 21:00)</value>
      <webElementGuid>add197d9-4989-455a-9cb0-e521a8f31c74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-select-12-option-0&quot;)</value>
      <webElementGuid>148252ef-3adf-4a8b-bca3-1654d48a8bb8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='react-select-12-option-0']</value>
      <webElementGuid>a0affc27-b793-4c97-a200-91bf5edfb993</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'react-select-12-option-0', '&quot;', ')')])[1]/following::div[4]</value>
      <webElementGuid>bb10d413-d08f-4576-977f-45d3d3a276de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select'])[12]/following::div[18]</value>
      <webElementGuid>a79dfb26-219c-45cd-9f3d-7e0b87819752</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Katalon Test Centre - Katalon Test Room: 2024-05-26 (18:00 - 22:00)'])[1]/preceding::div[1]</value>
      <webElementGuid>7b731f04-dc85-4d54-948d-418d76a384c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Katalon Test Centre - Katalon Test Room: 2024-05-23 (17:00 - 21:00)']/parent::*</value>
      <webElementGuid>4681e6a5-a833-485f-9ecf-04b7d312be65</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div</value>
      <webElementGuid>4183abc0-e911-47a2-aeae-c0e9c9c904c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'react-select-12-option-0' and (text() = 'Katalon Test Centre - Katalon Test Room: 2024-05-23 (17:00 - 21:00)' or . = 'Katalon Test Centre - Katalon Test Room: 2024-05-23 (17:00 - 21:00)')]</value>
      <webElementGuid>17af6569-349c-4bba-b605-1d6603e8e20e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
