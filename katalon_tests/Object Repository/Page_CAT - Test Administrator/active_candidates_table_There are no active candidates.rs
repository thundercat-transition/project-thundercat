<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>active_candidates_table_There are no active candidates</name>
   <tag></tag>
   <elementGuidId>8085673e-5553-4e38-993b-5015ff15e20d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'There are no active candidates' or . = 'There are no active candidates')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td > label</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='active-candidates-no-data']/td/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>827df439-2490-44b2-9168-d26e17677dd0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>There are no active candidates</value>
      <webElementGuid>3e96f495-e114-4e54-879d-75f55df172e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;active-candidates-no-data&quot;)/td[1]/label[1]</value>
      <webElementGuid>86f44237-c00a-4aa1-99c3-f4cdc7f8fb07</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='active-candidates-no-data']/td/label</value>
      <webElementGuid>9b99c0ce-6d25-4da8-b332-bd1e37392905</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[2]/following::label[1]</value>
      <webElementGuid>99c3424d-66ad-45ea-8817-1819c22b1602</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Time Limit'])[1]/following::label[1]</value>
      <webElementGuid>732b7c1b-6931-4ddc-9d60-ac1cf528eff7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reports'])[2]/preceding::label[1]</value>
      <webElementGuid>74980b46-61b7-43c7-9735-5ca9a5c67fa4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Report Type:'])[1]/preceding::label[1]</value>
      <webElementGuid>3434bd8e-49f9-4302-adf0-92036d4ad51a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='There are no active candidates']/parent::*</value>
      <webElementGuid>12220c2b-9633-42e4-b9db-eba0d7efb28a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/label</value>
      <webElementGuid>521d6001-b050-4056-9d5b-e1de42865380</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
