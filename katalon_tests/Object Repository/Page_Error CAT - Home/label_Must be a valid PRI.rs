<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Must be a valid PRI</name>
   <tag></tag>
   <elementGuidId>6e8d88c5-3d81-42cb-95da-e8cdd17405dd</elementGuidId>
   <imagePath>Screenshots/Targets/Page_Error CAT - Home/label_Must be a valid PRI.png</imagePath>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='id-lmvt7n-5']/div/div/div/form/div[5]/label</value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value>Screenshots/Targets/Page_Error CAT - Home/label_Must be a valid PRI.png</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Must be a valid PRI' or . = 'Must be a valid PRI')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div:nth-of-type(5) > label.notranslate</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>218cbcdc-831c-4a37-b6db-0508dc929652</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>pri-field</value>
      <webElementGuid>c2bb6c54-bd0a-46b4-a68b-6f3e73b39dda</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notranslate</value>
      <webElementGuid>65aceffa-96c1-475d-b7eb-e684e3810673</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Must be a valid PRI</value>
      <webElementGuid>e7a0c795-76a4-43bc-97c2-4436407a6df8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;id-lmvt7n-5&quot;)/div[1]/div[1]/div[1]/form[1]/div[5]/label[@class=&quot;notranslate&quot;]</value>
      <webElementGuid>ad9e8b8a-f7f2-4f1f-a361-75c00da20eb3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='id-lmvt7n-5']/div/div/div/form/div[5]/label</value>
      <webElementGuid>a09e9f98-439d-42e2-91ee-55282b7f0e2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PRI (if applicable):'])[1]/following::label[1]</value>
      <webElementGuid>f484ed1b-a672-44fb-9803-7016385d54a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Valid'])[1]/following::label[2]</value>
      <webElementGuid>e16c012c-d5dc-4d17-a630-4678637950fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Service Number (if applicable):'])[1]/preceding::label[1]</value>
      <webElementGuid>7ba1d183-d3b0-4737-b653-2b3e11213dac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Valid'])[2]/preceding::label[2]</value>
      <webElementGuid>887cec2b-4816-4d8e-aaa7-472108fd5371</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Must be a valid PRI']/parent::*</value>
      <webElementGuid>0e46586c-89b0-4a58-9529-3d2f021f51ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/label</value>
      <webElementGuid>08e67fbe-fef8-41fd-8d25-c945a5d61bed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'Must be a valid PRI' or . = 'Must be a valid PRI')]</value>
      <webElementGuid>7271f9a1-48b1-4e22-842b-a19a0be63fda</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
