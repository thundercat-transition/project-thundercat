<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Quit Test</name>
   <tag></tag>
   <elementGuidId>df2e3da6-ceba-4141-a235-b856612524eb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div > button.primary-in-test.undefined</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='basic-navbar-nav']/div/div[2]/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>f7762f4d-4bb5-4a6c-bbae-972615067323</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>primary-in-test undefined</value>
      <webElementGuid>47d3d6aa-916d-4089-807d-8103ae05b7f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Quit Test</value>
      <webElementGuid>341ba851-4d87-4299-b474-3ab4e0ed53f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Quit Test</value>
      <webElementGuid>578a7fa3-f255-4b27-a72e-ec1dbf039a4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;basic-navbar-nav&quot;)/div[@class=&quot;justify-content-end w-100 row&quot;]/div[2]/div[1]/button[@class=&quot;primary-in-test undefined&quot;]</value>
      <webElementGuid>05cb028c-4f12-41f1-a35f-c048348081be</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='basic-navbar-nav']/div/div[2]/div/button</value>
      <webElementGuid>ceda3532-b0f6-4cbe-ae5d-bcccc34d88e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to main content'])[1]/following::button[3]</value>
      <webElementGuid>57e6b981-a26a-4613-9ace-044cee7724a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tools'])[1]/preceding::button[1]</value>
      <webElementGuid>938e530a-fcd8-4c59-aa92-d7609e1c32b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/button</value>
      <webElementGuid>21b1bda4-37fd-4932-b7f4-e16b4d046f7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Quit Test' or . = 'Quit Test')]</value>
      <webElementGuid>12fb4979-0d08-4212-876c-26885163974c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
