<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_I understand that my test will not be_55f094</name>
   <tag></tag>
   <elementGuidId>a8299825-4e7a-431e-96b6-d6e229bb73e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#checkbox-condition-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='checkbox-condition-2']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>4a922651-1420-4721-8a08-d16dc499d1ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>8cd1478e-0139-43ce-9b4c-a9f4fddfa3c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkbox-condition-2</value>
      <webElementGuid>f6daf769-e0b2-40c8-88be-1dfb9462d5f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkbox-condition-2&quot;)</value>
      <webElementGuid>9dcbbda1-b191-4b2b-b483-3cb1e1e163b5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='checkbox-condition-2']</value>
      <webElementGuid>844a01ee-77ab-4103-9c38-e09032434d79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modal-description']/div/div/div/div[3]/div/input</value>
      <webElementGuid>b33ad148-7a30-418f-b276-70ba8c45eee3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/input</value>
      <webElementGuid>b1139376-a8b4-4fdd-9c94-6a806f5c2ea1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'checkbox-condition-2']</value>
      <webElementGuid>ab0854b0-d836-410f-8655-56174c063aa7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div[3]/div/input</value>
      <webElementGuid>422e080e-f2a8-4e7c-ae5e-b967b5d4605d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
