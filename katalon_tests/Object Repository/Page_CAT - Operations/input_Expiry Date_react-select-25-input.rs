<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Expiry Date_react-select-25-input</name>
   <tag></tag>
   <elementGuidId>ccc9f7eb-86ec-41a7-b059-579737d1458b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'react-select-25-input' and @type = 'text']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='react-select-25-input']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#react-select-25-input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>d5f4cc2b-bbb1-4dc3-9505-4d8e0e617b5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>005be971-fd67-4c7a-8df0-a88a86b762f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>5abd038e-d4ae-4f85-a327-4f27c3b7ca9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>39c61c58-9087-49a7-9264-11ab899b7eb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>react-select-25-input</value>
      <webElementGuid>5095949b-e4ae-4de4-b48b-3b668bc931dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>spellcheck</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>42323fd4-48cb-4032-aa93-91bd9d8fa7a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>0a415113-ae94-4a5e-ad92-6f31c1a98cc8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>e569949b-878c-4059-890d-5151c2cac91b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-autocomplete</name>
      <type>Main</type>
      <value>list</value>
      <webElementGuid>daf9d9ac-006e-4557-a264-e9648b679742</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>expiry-date-label   day-field-selected current-day-value-intro  date-picker-day-field-value-label</value>
      <webElementGuid>3d844ee7-4d25-4132-bfb2-c7ce0ef933f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-select-25-input&quot;)</value>
      <webElementGuid>e4be40a0-2d5e-413b-8894-a44de3d6c415</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='react-select-25-input']</value>
      <webElementGuid>dfdbe091-bccf-4422-8051-835d413aa401</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//div[@id='date-picker-day-field-react-select-dropdown']/div/div/div[2]/div/input)[2]</value>
      <webElementGuid>18084c42-8e4f-4854-9364-b21f0316523f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[11]/div[2]/div/div/div/div/div/div/div/div/div/div[2]/div/input</value>
      <webElementGuid>38f1ad21-e13a-45de-bf83-2503a4d13136</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'react-select-25-input' and @type = 'text']</value>
      <webElementGuid>e025f1f1-8ee4-427c-ae97-e47d4d1b0ffb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
