<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_delete</name>
   <tag></tag>
   <elementGuidId>f1107a06-5512-4b6d-87da-7784157dd314</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#reason-for-modifications</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='reason-for-modifications']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>ed794718-e490-46d3-991c-5b37fcf6ecf5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>reason-for-modifications</value>
      <webElementGuid>a165880c-0b40-4f7d-93e1-f71752ebd8d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>valid-field</value>
      <webElementGuid>6c395b24-f3be-48b5-b19a-ed38051337a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>reason-for-modification-label reason-for-modifications-error</value>
      <webElementGuid>55567587-3154-4163-8acf-4b4df0fdbc4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>7be644d6-dcf3-4f9e-b225-000cc7c768e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>fe446259-c90b-4bff-bb5b-ae37885c69c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>300</value>
      <webElementGuid>e270acce-b3dd-4343-84bb-c9f7d7160c93</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>delete</value>
      <webElementGuid>c1c26aab-ea95-469a-aae7-02b42fe6e189</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;reason-for-modifications&quot;)</value>
      <webElementGuid>660e6051-eeb9-4b9f-97c5-494e4626530d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='reason-for-modifications']</value>
      <webElementGuid>b51a0f1c-4627-4e32-b34d-378db8b37e9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modal-description']/div/div/div[13]/div/textarea</value>
      <webElementGuid>86e02a27-f41e-47e1-9e1d-504af9fc0ecf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reason for modification or deletion:'])[1]/following::textarea[1]</value>
      <webElementGuid>e8d44f8e-1438-4ef4-9b4d-198f6cbda750</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The current value is:'])[6]/following::textarea[1]</value>
      <webElementGuid>1ac69a56-2d16-49e3-82cf-31de591e6f9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[1]/preceding::textarea[1]</value>
      <webElementGuid>6866f31f-5674-47a5-8edc-bda702da05b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Modify'])[27]/preceding::textarea[1]</value>
      <webElementGuid>12e91a5e-9d10-4943-8966-a299233f688c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='delete']/parent::*</value>
      <webElementGuid>fcbbd9de-0f0a-4770-8e00-c14440f43940</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>d466a1ce-fe8e-4773-b9a8-88128fe3f56c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@id = 'reason-for-modifications' and (text() = 'delete' or . = 'delete')]</value>
      <webElementGuid>c7e4509f-64be-42c6-a317-9b3f16067aeb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
