<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Close</name>
   <tag></tag>
   <elementGuidId>b33b3cc3-db5f-46ec-a4e6-adb8366d6223</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.close-button.btn.btn-secondary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='unit-test-popup']/div/div/div[2]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>c7e4c4bc-d9c6-4a25-a88d-afc714025519</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>unit-test-right-btn</value>
      <webElementGuid>5cf8d9b9-a4b5-4116-a7c7-5d4530e2e804</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>close-button btn btn-secondary</value>
      <webElementGuid>d7a88cd7-ba62-49ca-83b1-cb38a17bd57b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Close</value>
      <webElementGuid>a3d4d197-19da-4d9b-b297-ad830b6e5021</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>5ee2d156-14b0-4eb4-a012-4a2db86d108f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Close</value>
      <webElementGuid>ff3fd0db-7623-4766-be9a-4cab3d72b196</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;unit-test-popup&quot;)/div[@class=&quot;modal-header&quot;]/div[@class=&quot;align-items-center row&quot;]/div[@class=&quot;float-right&quot;]/button[@class=&quot;close-button btn btn-secondary&quot;]</value>
      <webElementGuid>9f06117f-7cdd-47fa-b0c2-21132894f6ee</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='unit-test-popup']/div/div/div[2]/button</value>
      <webElementGuid>f7445765-9230-49bc-844a-ea7439382d76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Modify or Delete User Roles'])[1]/following::button[1]</value>
      <webElementGuid>68028181-4d10-418f-a6fd-97fc66b3c8b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next page'])[2]/following::button[1]</value>
      <webElementGuid>18ba0573-0571-43cc-b27d-9f64b2b67ae3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Katalon UserOne'])[1]/preceding::button[1]</value>
      <webElementGuid>44f470f2-d894-44b8-8df6-a127805e5df3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div[2]/button</value>
      <webElementGuid>234dec0d-0c89-42b9-8559-05022e097843</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Close' or . = 'Close')]</value>
      <webElementGuid>b5103cfb-a83d-4d10-bd9a-3488dd29e144</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='unit-test-right-btn']</value>
      <webElementGuid>570265a4-1332-4ed3-9d87-6263608786cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='unit-test-popup']/div[3]/button</value>
      <webElementGuid>1263c4fd-6f3d-4cc0-894b-986e1f12e876</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Success'])[1]/following::button[1]</value>
      <webElementGuid>68defb30-b9f9-4cf4-a8d5-563e55bc7574</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Test Administrator(s)'])[1]/following::button[1]</value>
      <webElementGuid>97489df5-07f7-4eeb-95a0-c101a14baa67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Close']/parent::*</value>
      <webElementGuid>afaae53d-356d-40fe-8d99-ce623a32c525</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/div[3]/button</value>
      <webElementGuid>f0d78dd9-7a02-4857-b9af-c1a9edac9889</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'unit-test-right-btn' and (text() = 'Close' or . = 'Close')]</value>
      <webElementGuid>70c3a645-ee0f-4fa0-be55-703d0192fd5c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
