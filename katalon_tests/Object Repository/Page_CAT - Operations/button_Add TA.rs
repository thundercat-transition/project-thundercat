<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Add TA</name>
   <tag></tag>
   <elementGuidId>3fc0a9d2-4b5d-4d46-b351-4a18f829737a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xl-auto.col-lg-auto.col-md-auto.col-sm-auto.col-auto > button.primary.undefined</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='test-accesses-tabs-tabpane-ta-orderless-test-accesses']/div/div/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>940f9915-390e-4f49-abe2-c57d82c8801d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>primary undefined</value>
      <webElementGuid>414ff9d9-62c6-462f-b2e6-a5eafed1f635</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>031f0753-32c9-4a5b-a8e8-c5cd57cdcbda</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add TA</value>
      <webElementGuid>2dd861b7-336a-4def-857e-5f0e448e2157</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;test-accesses-tabs-tabpane-ta-orderless-test-accesses&quot;)/div[1]/div[@class=&quot;align-items-center justify-content-start row&quot;]/div[@class=&quot;col-xl-auto col-lg-auto col-md-auto col-sm-auto col-auto&quot;]/button[@class=&quot;primary undefined&quot;]</value>
      <webElementGuid>1dd6bbcc-ae18-4a16-b0ee-7923a0bbc54f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='test-accesses-tabs-tabpane-ta-orderless-test-accesses']/div/div/div/button</value>
      <webElementGuid>1fe35360-26bc-4943-8007-39a676b61e3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next page'])[3]/following::button[1]</value>
      <webElementGuid>7e2450b1-3e2b-4d85-b7ed-cb1e19d7fae0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='...'])[1]/following::button[1]</value>
      <webElementGuid>34a1bcea-f099-4689-abe6-658b56de568d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search:'])[10]/preceding::button[1]</value>
      <webElementGuid>3ed4cb66-1937-47b7-bc1f-e82ea7d66264</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/button</value>
      <webElementGuid>e02fb3a7-ffb1-499b-9071-fde76885580b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Add TA' or . = 'Add TA')]</value>
      <webElementGuid>7c144c96-cce0-4560-96bd-9b94e412fbd7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
