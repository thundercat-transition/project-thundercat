<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Orderless Test Accesses</name>
   <tag></tag>
   <elementGuidId>af60b6d2-cfd0-4691-a4fe-3d7a9728d005</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#test-accesses-tabs-tab-ta-orderless-test-accesses</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='test-accesses-tabs-tab-ta-orderless-test-accesses']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>3bc42dd2-ef6e-40d6-82e0-b95031b5056c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>test-accesses-tabs-tab-ta-orderless-test-accesses</value>
      <webElementGuid>47a6e7c9-e030-4016-825a-ef3216acc198</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>f73921cf-4f3f-4c15-a4de-d8f0b6a31226</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>tab</value>
      <webElementGuid>595c39e8-c38b-4f26-b01c-b3f4c0f2c203</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-rb-event-key</name>
      <type>Main</type>
      <value>ta-orderless-test-accesses</value>
      <webElementGuid>819d880e-2938-4052-9e1d-517565fc8525</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>test-accesses-tabs-tabpane-ta-orderless-test-accesses</value>
      <webElementGuid>24a06e71-6f25-416d-a705-732861aed6e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-selected</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>47178cb5-edf9-49ff-a221-29d177c96d62</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-item nav-link</value>
      <webElementGuid>9beaf4bb-9fa9-4dde-acc9-fe4e83381d7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Orderless Test Accesses</value>
      <webElementGuid>8a26162a-1886-4efa-8f10-d7a7b0545b1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;test-accesses-tabs-tab-ta-orderless-test-accesses&quot;)</value>
      <webElementGuid>747ac178-c80a-4507-a985-51da888a7d25</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='test-accesses-tabs-tab-ta-orderless-test-accesses']</value>
      <webElementGuid>7810f4ef-eba6-4529-b408-c9531d45c462</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='id-lbpbcq-14']/div/div/div/div[2]/div/div/nav/a[3]</value>
      <webElementGuid>e9fc953a-98d2-4672-9fa2-de276b3b4b90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Orderless Test Accesses')]</value>
      <webElementGuid>6f095dd6-86cc-4bd3-a032-da3b5cf50908</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Access Management'])[2]/following::a[3]</value>
      <webElementGuid>bc449113-6393-42c7-870e-5662251fa770</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next page'])[2]/following::a[3]</value>
      <webElementGuid>f81b7264-26b0-42aa-8f2d-73fdd20eca6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Order Number:'])[1]/preceding::a[1]</value>
      <webElementGuid>db0fe69b-9c32-4ff2-a88a-333216a69393</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[1]/preceding::a[1]</value>
      <webElementGuid>429052b2-2295-4aa7-b10e-c01ba375d8da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Orderless Test Accesses']/parent::*</value>
      <webElementGuid>3efb77ea-1024-474a-988e-d292d6d8c017</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[8]</value>
      <webElementGuid>994eb2c3-e899-4e08-9bca-265bf1255ec3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[3]</value>
      <webElementGuid>deea3c3f-cb52-4811-aa78-1525330423f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'test-accesses-tabs-tab-ta-orderless-test-accesses' and @href = '#' and (text() = 'Orderless Test Accesses' or . = 'Orderless Test Accesses')]</value>
      <webElementGuid>237b2e93-2653-4158-b7cc-2fd74a20ccdd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
