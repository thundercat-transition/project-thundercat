<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Manual Entry</name>
   <tag></tag>
   <elementGuidId>5cd95d43-d8ee-4466-a809-1a4e863d76ee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span:nth-of-type(2) > button.secondary.undefined</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='test-accesses-tabs-tabpane-assign-test-access']/div/div/div[2]/div[2]/span[2]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>932b967a-407e-4d76-be37-00e9fb6f6fe7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>secondary  undefined</value>
      <webElementGuid>c818b0bc-f8dd-4734-a2d8-a89cb30554fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-for</name>
      <type>Main</type>
      <value>manual-entry-button-tooltip</value>
      <webElementGuid>1eddce04-ba97-4a55-b7bd-713804911923</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>ca33df14-9bb2-43be-81e6-9d2191a10d80</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>currentitem</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>81823867-c05e-405d-9b89-f7458626ff4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>manual-entry-button-tooltip</value>
      <webElementGuid>1be98bed-08e2-4b18-a458-2c08544338aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Manual Entry</value>
      <webElementGuid>d114c3ea-b4da-41fe-af5a-25a7ef52b1ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;test-accesses-tabs-tabpane-assign-test-access&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xl-7&quot;]/div[2]/span[2]/button[@class=&quot;secondary  undefined&quot;]</value>
      <webElementGuid>f192f0c5-8fd3-449b-8bd0-c31939dc51fa</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='test-accesses-tabs-tabpane-assign-test-access']/div/div/div[2]/div[2]/span[2]/button</value>
      <webElementGuid>3330343b-9955-4ab3-b2be-a272bee17db0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[1]/following::button[1]</value>
      <webElementGuid>35975d1c-1335-4f9d-8d9e-e9733992d2e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Order Number:'])[1]/following::button[2]</value>
      <webElementGuid>4ebec119-2fec-4d31-b580-3600f286df18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assessment Process Number:'])[1]/preceding::button[1]</value>
      <webElementGuid>2048cf56-86c9-4d52-af4a-7d8f882ead90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/button</value>
      <webElementGuid>3db9708c-6add-4120-9389-265048697b5b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Manual Entry' or . = 'Manual Entry')]</value>
      <webElementGuid>029e109a-82e7-4679-a921-a8534f5d9903</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
