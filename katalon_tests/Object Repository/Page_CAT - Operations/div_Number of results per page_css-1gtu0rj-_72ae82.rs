<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Number of results per page_css-1gtu0rj-_72ae82</name>
   <tag></tag>
   <elementGuidId>7edc499b-a7b7-48e3-a99e-c6d13c51561a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='date-picker-month-field-react-select-dropdown']/div/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4fe7ac51-dd05-4eb6-98a8-124c4f0fb533</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-1g6gooi</value>
      <webElementGuid>54af016e-403d-471b-afaf-6c17ec405c17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>3146814e-b540-4fb8-855a-8dfcaba08f2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;date-picker-month-field-react-select-dropdown&quot;)/div[@class=&quot;css-o1r4wa-control&quot;]/div[@class=&quot;css-3nk303&quot;]/div[@class=&quot;css-1g6gooi&quot;]</value>
      <webElementGuid>c33b0a5c-8f64-4743-97c1-5670cb6b65d8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='date-picker-month-field-react-select-dropdown']/div/div/div[2]</value>
      <webElementGuid>f2eef7d5-f49a-4349-a766-32d86e94db99</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select'])[2]/following::div[1]</value>
      <webElementGuid>7932309e-10e4-4492-8cb2-e52091bb0689</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select'])[1]/following::div[5]</value>
      <webElementGuid>fff6b857-d293-4bc5-970c-6a21019c9bf5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Month'])[1]/preceding::div[5]</value>
      <webElementGuid>acff27bf-0a2c-4b01-9117-086011cca534</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Month field selected'])[1]/preceding::div[5]</value>
      <webElementGuid>4a66ef54-1ec3-4c33-8c98-cf4634aff749</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div[2]/div/div/div/div/div/div/div[2]</value>
      <webElementGuid>12454087-f1e9-403d-89b7-1ecb4bc38a02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='user-look-up-display-options-dropdown-react-select-dropdown']/div/div[2]/div</value>
      <webElementGuid>45a6340b-d1de-4412-abb4-ce6ca3fcce01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Number of results per page'])[3]/following::div[11]</value>
      <webElementGuid>9bc82bed-7299-4c31-9da4-4037ac9762b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display:'])[3]/following::div[11]</value>
      <webElementGuid>6c13aa02-c46c-4fe9-8437-5f73b982eff2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The selected value is: 25'])[3]/preceding::div[1]</value>
      <webElementGuid>1ad961f2-7df1-4760-8418-6c6ce6e0d945</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to list of pages'])[3]/preceding::div[1]</value>
      <webElementGuid>d755bf0c-646e-4147-9072-d550848e2cc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div/div/div/div[2]/div/div[2]/div/div/div/div[2]/div</value>
      <webElementGuid>c91d2cb2-6b32-4772-9e0b-e2e4759f4c63</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
