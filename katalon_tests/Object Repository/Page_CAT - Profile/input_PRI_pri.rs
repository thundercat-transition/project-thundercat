<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_PRI_pri</name>
   <tag></tag>
   <elementGuidId>34e65c34-e3e2-4eeb-b2f3-8ac15fa6155b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xl-6.col-lg-6.col-md-12.col-sm-12.col-12 > #pri</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='pri']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ecd10357-596d-4085-ba29-b337ffd835be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>pri</value>
      <webElementGuid>7e80b1ba-635c-4fd3-ad9a-336d3a8cd923</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>valid-field</value>
      <webElementGuid>ee073277-82bb-4200-94f9-a3ed4998d410</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>pri-title pri-error</value>
      <webElementGuid>1413dcac-99e5-41f4-b90d-1e0841582b6c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>dd364ef6-3d29-412f-8167-6f722856f205</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>c6c684b2-4a56-4b08-ab52-81c83df78317</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>5cefd6af-fd54-451c-937b-0de7ca9cd90c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>12345679</value>
      <webElementGuid>2dcd67dc-c1de-458a-a0a3-a5333080c049</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modal-description&quot;)/div[1]/div[1]/div[2]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12&quot;]/input[@id=&quot;pri&quot;]</value>
      <webElementGuid>ef4ca345-55dc-4f41-a5a1-8df0870fcbb2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='pri']</value>
      <webElementGuid>588b5454-130f-4418-be8d-17030d57f395</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modal-description']/div/div/div[2]/div[2]/div[2]/input</value>
      <webElementGuid>d3027ee9-a8ef-49bf-a84f-422739ac6f57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[2]/input</value>
      <webElementGuid>c9372451-6586-47c2-862a-aced3c5997cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'pri' and @type = 'text']</value>
      <webElementGuid>57208e6d-c4a3-440e-87b6-997ed781aa4a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
