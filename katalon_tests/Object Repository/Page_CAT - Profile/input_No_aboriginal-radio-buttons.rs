<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_No_aboriginal-radio-buttons</name>
   <tag></tag>
   <elementGuidId>beb15d79-1481-45c7-97b1-6bfed0bc5d22</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='aboriginal-prefer-not-to-answer-radio-button-id']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#aboriginal-prefer-not-to-answer-radio-button-id</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>3d2304da-d01b-4f4b-8c60-c42fda5fcdd5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>aboriginal-prefer-not-to-answer-radio-button-id</value>
      <webElementGuid>f1c28513-abbd-4f0e-beaf-ef8c30d81978</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
      <webElementGuid>c73b4f60-9cb5-4789-8ad0-50f6958b5a30</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>aboriginal-radio-buttons</value>
      <webElementGuid>19be8aef-f3d3-4a8f-a9cc-7d4f5d4e80fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>aboriginal aboriginal-prefer-not-to-answer-radio-button-label</value>
      <webElementGuid>54da8ca3-9049-4185-a3c2-275685ba3207</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;aboriginal-prefer-not-to-answer-radio-button-id&quot;)</value>
      <webElementGuid>39338855-7c8a-4922-83ba-6c298f6ea691</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='aboriginal-prefer-not-to-answer-radio-button-id']</value>
      <webElementGuid>5e473f31-81b5-4ac7-b9fd-ad0150b843da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//fieldset[@id='aboriginal-radio-buttons-group']/div[3]/div/div/input</value>
      <webElementGuid>ca77814d-a13b-4d8a-a564-5239b6b4488b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/fieldset/div[3]/div/div/input</value>
      <webElementGuid>3206a920-be81-402d-ac57-0688526e538f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'aboriginal-prefer-not-to-answer-radio-button-id' and @type = 'radio' and @name = 'aboriginal-radio-buttons']</value>
      <webElementGuid>ae8601b4-b30d-46b4-923e-83d24e45740d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
