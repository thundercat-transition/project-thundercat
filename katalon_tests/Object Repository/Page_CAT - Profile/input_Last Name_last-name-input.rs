<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Last Name_last-name-input</name>
   <tag></tag>
   <elementGuidId>d642fc57-9f9f-47c4-9238-62134f84624b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#last-name-input</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='last-name-input']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>939492cb-8a81-47e9-b1dc-d51a77999d08</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>last-name-input</value>
      <webElementGuid>2d218b37-12ea-45e5-9f7f-b9c5149dfbe7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>valid-field</value>
      <webElementGuid>e41301e8-6d4e-4e67-9b4f-f45a1675602f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>name-title last-name</value>
      <webElementGuid>2acaf599-857d-4a54-b7e6-92c5ac9600db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>3d7f1db3-fe2e-4bf6-8f09-bd25fd5f02e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>f3e7aaeb-f432-4595-bc83-4f562970eac6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>UserOne</value>
      <webElementGuid>217c346a-b20e-4dd0-a639-7dda7002b6be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;last-name-input&quot;)</value>
      <webElementGuid>da7d6fd8-4c86-4836-83dc-25ef26ce3285</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='last-name-input']</value>
      <webElementGuid>96cc7730-108b-46c1-a3b4-cb5452b54028</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='personal-info-last-name']/div[2]/input</value>
      <webElementGuid>0513627e-1820-4871-9533-f5b892911b8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/input</value>
      <webElementGuid>8dae1910-a823-4f09-b9ea-a790c174c12a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
