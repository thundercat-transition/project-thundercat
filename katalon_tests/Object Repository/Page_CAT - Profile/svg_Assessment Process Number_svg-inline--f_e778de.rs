<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Assessment Process Number_svg-inline--f_e778de</name>
   <tag></tag>
   <elementGuidId>2217ec2a-ef8a-460b-9d26-3e9f6a955974</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Welcome, Katalon UserOne.'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>3c1bbba5-958e-4580-a044-3b7e081fb79d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>e39b0f31-abc2-4d61-9ef8-8906ef5a37e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>e7991cd6-cfe6-4765-bd41-827dda708ba5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-prefix</name>
      <type>Main</type>
      <value>fas</value>
      <webElementGuid>3bce84ca-b41c-477c-a35b-a8ae17f0ffc4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-icon</name>
      <type>Main</type>
      <value>circle-user</value>
      <webElementGuid>b60f1fc7-2c0e-4f46-a771-4c60c0c6bb16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>svg-inline--fa fa-circle-user fa-w-16 </value>
      <webElementGuid>63f72788-c789-409c-a3ee-a4e45a7b7a53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>a18890c0-7cd2-4a86-a49f-614c96ff1c51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>http://www.w3.org/2000/svg</value>
      <webElementGuid>f2d3fed2-03fd-4c0f-9a44-cce31d1484b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 512 512</value>
      <webElementGuid>06cc6b75-a24e-452f-800c-b71f30696af3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navigation-items-section&quot;)/svg[@class=&quot;svg-inline--fa fa-circle-user fa-w-16&quot;]</value>
      <webElementGuid>050d91ea-6613-44f2-a6b1-5ad1afcf18a6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Welcome, Katalon UserOne.'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>c67bd3bd-711e-4bae-a51c-5a24aac7c0aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Français'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>583199ca-6499-4a04-8fa9-51035dc731d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Personal Information'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>40d427ab-f505-473d-9f88-c33d81154eb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>d8aa9b81-594c-4c12-82cd-a05acd9978e0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First Name:'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>f5b19531-b777-448d-80c2-0df11167b0c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Personal Information'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>25628a1f-4f06-45f4-9865-9a2f5231dcf7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Last Name:'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>d46a03dd-312f-49fd-b849-a547e50ef18f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username:'])[1]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>2a4d2677-eb5d-4d1a-97c9-04718b050d91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm Password:'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>9a1a0c1b-d778-48e3-a58c-41b552584d77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Minimum of 8 characters and maximum of 15'])[2]/following::*[name()='svg'][2]</value>
      <webElementGuid>a70022cc-18fe-44fe-97c9-4a31ec51a2ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save'])[2]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>e19500ab-8911-4b1c-abd6-144057b2ea63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Preferences'])[1]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>47eef976-991c-4330-8a05-8e9b6007ff11</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Minimum of 8 characters and maximum of 15'])[2]/following::*[name()='svg'][1]</value>
      <webElementGuid>c2531bb6-2c85-4776-88d5-75312840674c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='At least one special character (#?!@$%^&amp;*-)'])[2]/following::*[name()='svg'][1]</value>
      <webElementGuid>31cf5ddf-76d6-42bf-80c9-1c18596ba33a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm Password:'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>5937e31a-a834-4210-9ee7-dcb789a0af63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save'])[2]/preceding::*[name()='svg'][3]</value>
      <webElementGuid>e89686e1-5ce3-4e69-ab2f-4796df7d771d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MENU'])[1]//*[name()='svg'][1]</value>
      <webElementGuid>1fd70807-4138-4fc0-9ae3-df81a95505f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to main content'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>78973d69-2ec2-476a-9044-1bd2c6603977</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Tests'])[1]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>d38549c5-7f59-4aee-bdf3-f0e972e9dcf8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Profile'])[1]/preceding::*[name()='svg'][3]</value>
      <webElementGuid>7646c57e-c8e1-43f1-8bb8-812c13df7439</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assessment Process Number'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>6b263a2b-0156-4841-9a63-1ec68ebc1d97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order by test order number'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>69bafcd5-c303-46cf-b23c-06cc127f7515</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Order by assessment process number'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>ab8508e2-8650-4d2d-8d49-11608300e6f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expiry Date'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>54cea92e-9cc9-4e02-99cc-b419aaa99320</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
