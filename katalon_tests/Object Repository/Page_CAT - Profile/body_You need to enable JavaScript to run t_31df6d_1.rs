<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>body_You need to enable JavaScript to run t_31df6d_1</name>
   <tag></tag>
   <elementGuidId>45f7dc90-da5f-4e4b-bfc8-24b2843609ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>body</value>
      <webElementGuid>a8db0e5d-1f56-4a55-8cfa-9205fec3057d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    You need to enable JavaScript to run this app.
    Skip to main contentMENUOperationsR&amp;D DashboardTest ScorerTest AdministratorTest BuilderCheck-inMy ProfileMy TestsLogoutHomeSample TestsDisplayFrançaisWelcome, admin admin.Personal InformationPasswordPreferencesRights and PermissionsMy Personal InformationFirst Name:Last Name:Primary Email Address:Secondary Email Address (optional):Date of Birth:
  	.t308c47ad-9b3a-4904-bc56-a8a3009be40b {
	    color: #222;
	    background: #fff;
	    border: 1px solid transparent;
	    border-radius: undefinedpx;
	    padding: 8px 21px;
  	}

  	.t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-top {
        margin-top: -10px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-top::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: 2;
        width: 20px;
        height: 12px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-top::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        bottom: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(135deg);
    }

    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-bottom {
        margin-top: 10px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-bottom::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 18px;
        height: 10px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-bottom::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        top: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(45deg);
    }

    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-left {
        margin-left: -10px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-left::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-left::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        right: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(45deg);
    }

    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-right {
        margin-left: 10px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-right::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-right::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        left: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(-135deg);
    }
  Adding your date of birth will facilitate the consolidation of all your results in your test result record.Adding your date of birth will facilitate the consolidation of all your results in your test result record.0202DayDay field selectedThe current value is:0202MonthMonth field selectedThe current value is:20232023YearYear field selectedThe current value is:PSRS Applicant ID (optional):PRI or Service Number (optional):
  	.ta1728ab4-6b17-4be3-b96c-4af823de4d06 {
	    color: #222;
	    background: #fff;
	    border: 1px solid transparent;
	    border-radius: undefinedpx;
	    padding: 8px 21px;
  	}

  	.ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-top {
        margin-top: -10px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-top::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: 2;
        width: 20px;
        height: 12px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-top::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        bottom: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(135deg);
    }

    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-bottom {
        margin-top: 10px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-bottom::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 18px;
        height: 10px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-bottom::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        top: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(45deg);
    }

    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-left {
        margin-left: -10px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-left::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-left::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        right: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(45deg);
    }

    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-right {
        margin-left: 10px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-right::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-right::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        left: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(-135deg);
    }
  Adding your PRI or Service number will facilitate the consolidation of all your results in your test result recordAdding your PRI or Service number will facilitate the consolidation of all your results in your test result recordEmployment EquityThe Public Service Commission (PSC) needs your help to ensure its assessments are working as intended. The personal information you share below will be accessible exclusively to the PSC and solely used for research, analysis, and test development purposes. Participation is voluntary: you can select the option “Prefer not to say” for any questions below. The information you provide will not be shared with human resource personnel or those responsible for hiring decisions.The PSC is committed to protecting the privacy rights of individuals. Here is our Privacy Notice.Do you identify as a woman?YesNoPrefer not to sayAre you an Indigenous person?YesNoPrefer not to sayAre you a member of a visible minority group?YesNoPrefer not to sayAre you a person with a disability?YesNoPrefer not to sayAdditional Information (Optional)Current Employer:Please selectPlease selectOrganization:Please selectPlease selectGroup:AAAASubgroup:SelectN/AResidence:HereHereEducation:Secondary Less than 2 yearsSecondary Less than 2 yearsGender:Please selectoption Please select selected, 1 of 4. 4 results available. Use Up and Down to choose options, press Enter to select the currently focused option, press Escape to exit the menu, press Tab to select the option and exit the menu.Please selectSaveMy PasswordYour password was last updated on: 2023-02-02Current Password:New Password:
  	.tece2819a-1178-49b6-85bd-023bad0545f5 {
	    color: #222;
	    background: #fff;
	    border: 1px solid transparent;
	    border-radius: undefinedpx;
	    padding: 8px 21px;
  	}

  	.tece2819a-1178-49b6-85bd-023bad0545f5.place-top {
        margin-top: -10px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-top::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: 2;
        width: 20px;
        height: 12px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-top::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        bottom: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(135deg);
    }

    .tece2819a-1178-49b6-85bd-023bad0545f5.place-bottom {
        margin-top: 10px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-bottom::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 18px;
        height: 10px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-bottom::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        top: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(45deg);
    }

    .tece2819a-1178-49b6-85bd-023bad0545f5.place-left {
        margin-left: -10px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-left::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-left::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        right: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(45deg);
    }

    .tece2819a-1178-49b6-85bd-023bad0545f5.place-right {
        margin-left: 10px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-right::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-right::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        left: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(-135deg);
    }
  Your password must contain the following:At least one uppercase letterAt least one lowercase letterAt least one numberAt least one special character (#?!@$%^&amp;*-)Minimum of 8 characters and maximum of 15Your password must contain the following:At least one uppercase letterAt least one lowercase letterAt least one numberAt least one special character (#?!@$%^&amp;*-)Minimum of 8 characters and maximum of 15Confirm Password:SaveMy PreferencesModify my preferences.Accessibility:Adjust the font, font size, and text spacing to be displayed:Font Size:16px16pxFont Style:Nunito SansNunito SansText Spacing:My Rights and PermissionsYou do not need additional rights and permissions to take tests.Information about your user roleSuper UserUser has full system access and permissions
    
  

id(&quot;react-select-12-option-0&quot;)Please selectMaleFemaleOther gender</value>
      <webElementGuid>20432b33-fa40-4669-940b-d31c36e8ad0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]</value>
      <webElementGuid>11c0368b-c091-482d-bc08-2208c6fca188</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//body</value>
      <webElementGuid>6d55c6bf-adb3-46dd-86c0-f8ce46d03c3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//body[(text() = '
    You need to enable JavaScript to run this app.
    Skip to main contentMENUOperationsR&amp;D DashboardTest ScorerTest AdministratorTest BuilderCheck-inMy ProfileMy TestsLogoutHomeSample TestsDisplayFrançaisWelcome, admin admin.Personal InformationPasswordPreferencesRights and PermissionsMy Personal InformationFirst Name:Last Name:Primary Email Address:Secondary Email Address (optional):Date of Birth:
  	.t308c47ad-9b3a-4904-bc56-a8a3009be40b {
	    color: #222;
	    background: #fff;
	    border: 1px solid transparent;
	    border-radius: undefinedpx;
	    padding: 8px 21px;
  	}

  	.t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-top {
        margin-top: -10px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-top::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: 2;
        width: 20px;
        height: 12px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-top::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        bottom: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(135deg);
    }

    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-bottom {
        margin-top: 10px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-bottom::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 18px;
        height: 10px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-bottom::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        top: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(45deg);
    }

    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-left {
        margin-left: -10px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-left::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-left::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        right: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(45deg);
    }

    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-right {
        margin-left: 10px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-right::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-right::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        left: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(-135deg);
    }
  Adding your date of birth will facilitate the consolidation of all your results in your test result record.Adding your date of birth will facilitate the consolidation of all your results in your test result record.0202DayDay field selectedThe current value is:0202MonthMonth field selectedThe current value is:20232023YearYear field selectedThe current value is:PSRS Applicant ID (optional):PRI or Service Number (optional):
  	.ta1728ab4-6b17-4be3-b96c-4af823de4d06 {
	    color: #222;
	    background: #fff;
	    border: 1px solid transparent;
	    border-radius: undefinedpx;
	    padding: 8px 21px;
  	}

  	.ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-top {
        margin-top: -10px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-top::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: 2;
        width: 20px;
        height: 12px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-top::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        bottom: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(135deg);
    }

    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-bottom {
        margin-top: 10px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-bottom::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 18px;
        height: 10px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-bottom::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        top: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(45deg);
    }

    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-left {
        margin-left: -10px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-left::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-left::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        right: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(45deg);
    }

    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-right {
        margin-left: 10px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-right::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-right::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        left: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(-135deg);
    }
  Adding your PRI or Service number will facilitate the consolidation of all your results in your test result recordAdding your PRI or Service number will facilitate the consolidation of all your results in your test result recordEmployment EquityThe Public Service Commission (PSC) needs your help to ensure its assessments are working as intended. The personal information you share below will be accessible exclusively to the PSC and solely used for research, analysis, and test development purposes. Participation is voluntary: you can select the option “Prefer not to say” for any questions below. The information you provide will not be shared with human resource personnel or those responsible for hiring decisions.The PSC is committed to protecting the privacy rights of individuals. Here is our Privacy Notice.Do you identify as a woman?YesNoPrefer not to sayAre you an Indigenous person?YesNoPrefer not to sayAre you a member of a visible minority group?YesNoPrefer not to sayAre you a person with a disability?YesNoPrefer not to sayAdditional Information (Optional)Current Employer:Please selectPlease selectOrganization:Please selectPlease selectGroup:AAAASubgroup:SelectN/AResidence:HereHereEducation:Secondary Less than 2 yearsSecondary Less than 2 yearsGender:Please selectoption Please select selected, 1 of 4. 4 results available. Use Up and Down to choose options, press Enter to select the currently focused option, press Escape to exit the menu, press Tab to select the option and exit the menu.Please selectSaveMy PasswordYour password was last updated on: 2023-02-02Current Password:New Password:
  	.tece2819a-1178-49b6-85bd-023bad0545f5 {
	    color: #222;
	    background: #fff;
	    border: 1px solid transparent;
	    border-radius: undefinedpx;
	    padding: 8px 21px;
  	}

  	.tece2819a-1178-49b6-85bd-023bad0545f5.place-top {
        margin-top: -10px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-top::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: 2;
        width: 20px;
        height: 12px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-top::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        bottom: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(135deg);
    }

    .tece2819a-1178-49b6-85bd-023bad0545f5.place-bottom {
        margin-top: 10px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-bottom::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 18px;
        height: 10px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-bottom::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        top: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(45deg);
    }

    .tece2819a-1178-49b6-85bd-023bad0545f5.place-left {
        margin-left: -10px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-left::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-left::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        right: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(45deg);
    }

    .tece2819a-1178-49b6-85bd-023bad0545f5.place-right {
        margin-left: 10px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-right::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-right::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        left: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(-135deg);
    }
  Your password must contain the following:At least one uppercase letterAt least one lowercase letterAt least one numberAt least one special character (#?!@$%^&amp;*-)Minimum of 8 characters and maximum of 15Your password must contain the following:At least one uppercase letterAt least one lowercase letterAt least one numberAt least one special character (#?!@$%^&amp;*-)Minimum of 8 characters and maximum of 15Confirm Password:SaveMy PreferencesModify my preferences.Accessibility:Adjust the font, font size, and text spacing to be displayed:Font Size:16px16pxFont Style:Nunito SansNunito SansText Spacing:My Rights and PermissionsYou do not need additional rights and permissions to take tests.Information about your user roleSuper UserUser has full system access and permissions
    
  

id(&quot;react-select-12-option-0&quot;)Please selectMaleFemaleOther gender' or . = '
    You need to enable JavaScript to run this app.
    Skip to main contentMENUOperationsR&amp;D DashboardTest ScorerTest AdministratorTest BuilderCheck-inMy ProfileMy TestsLogoutHomeSample TestsDisplayFrançaisWelcome, admin admin.Personal InformationPasswordPreferencesRights and PermissionsMy Personal InformationFirst Name:Last Name:Primary Email Address:Secondary Email Address (optional):Date of Birth:
  	.t308c47ad-9b3a-4904-bc56-a8a3009be40b {
	    color: #222;
	    background: #fff;
	    border: 1px solid transparent;
	    border-radius: undefinedpx;
	    padding: 8px 21px;
  	}

  	.t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-top {
        margin-top: -10px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-top::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: 2;
        width: 20px;
        height: 12px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-top::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        bottom: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(135deg);
    }

    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-bottom {
        margin-top: 10px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-bottom::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 18px;
        height: 10px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-bottom::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        top: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(45deg);
    }

    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-left {
        margin-left: -10px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-left::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-left::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        right: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(45deg);
    }

    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-right {
        margin-left: 10px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-right::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .t308c47ad-9b3a-4904-bc56-a8a3009be40b.place-right::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        left: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(-135deg);
    }
  Adding your date of birth will facilitate the consolidation of all your results in your test result record.Adding your date of birth will facilitate the consolidation of all your results in your test result record.0202DayDay field selectedThe current value is:0202MonthMonth field selectedThe current value is:20232023YearYear field selectedThe current value is:PSRS Applicant ID (optional):PRI or Service Number (optional):
  	.ta1728ab4-6b17-4be3-b96c-4af823de4d06 {
	    color: #222;
	    background: #fff;
	    border: 1px solid transparent;
	    border-radius: undefinedpx;
	    padding: 8px 21px;
  	}

  	.ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-top {
        margin-top: -10px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-top::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: 2;
        width: 20px;
        height: 12px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-top::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        bottom: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(135deg);
    }

    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-bottom {
        margin-top: 10px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-bottom::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 18px;
        height: 10px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-bottom::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        top: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(45deg);
    }

    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-left {
        margin-left: -10px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-left::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-left::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        right: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(45deg);
    }

    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-right {
        margin-left: 10px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-right::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .ta1728ab4-6b17-4be3-b96c-4af823de4d06.place-right::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        left: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(-135deg);
    }
  Adding your PRI or Service number will facilitate the consolidation of all your results in your test result recordAdding your PRI or Service number will facilitate the consolidation of all your results in your test result recordEmployment EquityThe Public Service Commission (PSC) needs your help to ensure its assessments are working as intended. The personal information you share below will be accessible exclusively to the PSC and solely used for research, analysis, and test development purposes. Participation is voluntary: you can select the option “Prefer not to say” for any questions below. The information you provide will not be shared with human resource personnel or those responsible for hiring decisions.The PSC is committed to protecting the privacy rights of individuals. Here is our Privacy Notice.Do you identify as a woman?YesNoPrefer not to sayAre you an Indigenous person?YesNoPrefer not to sayAre you a member of a visible minority group?YesNoPrefer not to sayAre you a person with a disability?YesNoPrefer not to sayAdditional Information (Optional)Current Employer:Please selectPlease selectOrganization:Please selectPlease selectGroup:AAAASubgroup:SelectN/AResidence:HereHereEducation:Secondary Less than 2 yearsSecondary Less than 2 yearsGender:Please selectoption Please select selected, 1 of 4. 4 results available. Use Up and Down to choose options, press Enter to select the currently focused option, press Escape to exit the menu, press Tab to select the option and exit the menu.Please selectSaveMy PasswordYour password was last updated on: 2023-02-02Current Password:New Password:
  	.tece2819a-1178-49b6-85bd-023bad0545f5 {
	    color: #222;
	    background: #fff;
	    border: 1px solid transparent;
	    border-radius: undefinedpx;
	    padding: 8px 21px;
  	}

  	.tece2819a-1178-49b6-85bd-023bad0545f5.place-top {
        margin-top: -10px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-top::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: 2;
        width: 20px;
        height: 12px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-top::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        bottom: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(135deg);
    }

    .tece2819a-1178-49b6-85bd-023bad0545f5.place-bottom {
        margin-top: 10px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-bottom::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 18px;
        height: 10px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-bottom::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        top: -6px;
        left: 50%;
        margin-left: -6px;
        transform: rotate(45deg);
    }

    .tece2819a-1178-49b6-85bd-023bad0545f5.place-left {
        margin-left: -10px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-left::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-left::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        right: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(45deg);
    }

    .tece2819a-1178-49b6-85bd-023bad0545f5.place-right {
        margin-left: 10px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-right::before {
        content: &quot;&quot;;
        background-color: inherit;
        position: absolute;
        z-index: -1;
        width: 10px;
        height: 18px;
    }
    .tece2819a-1178-49b6-85bd-023bad0545f5.place-right::after {
        content: &quot;&quot;;
        position: absolute;
        width: 10px;
        height: 10px;
        border-top-right-radius: undefinedpx;
        border: 1px solid transparent;
        background-color: #fff;
        z-index: -2;
        left: -6px;
        top: 50%;
        margin-top: -6px;
        transform: rotate(-135deg);
    }
  Your password must contain the following:At least one uppercase letterAt least one lowercase letterAt least one numberAt least one special character (#?!@$%^&amp;*-)Minimum of 8 characters and maximum of 15Your password must contain the following:At least one uppercase letterAt least one lowercase letterAt least one numberAt least one special character (#?!@$%^&amp;*-)Minimum of 8 characters and maximum of 15Confirm Password:SaveMy PreferencesModify my preferences.Accessibility:Adjust the font, font size, and text spacing to be displayed:Font Size:16px16pxFont Style:Nunito SansNunito SansText Spacing:My Rights and PermissionsYou do not need additional rights and permissions to take tests.Information about your user roleSuper UserUser has full system access and permissions
    
  

id(&quot;react-select-12-option-0&quot;)Please selectMaleFemaleOther gender')]</value>
      <webElementGuid>e51576bb-d938-4522-af59-3305b6be4b30</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
