<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Confirm</name>
   <tag></tag>
   <elementGuidId>70d6e513-1afa-4d8c-8462-424b6c3236f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='unit-test-right-btn']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#unit-test-right-btn</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>84c15abf-7efc-47a1-83fa-c8f587e85bd3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>unit-test-right-btn</value>
      <webElementGuid>de4ee55d-41a1-467a-90b1-02651e255a90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>danger undefined</value>
      <webElementGuid>bf5dcd98-0e64-413c-b681-ea7ae692c2cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Confirm</value>
      <webElementGuid>17a29846-7e71-496e-8181-d49a52520726</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Confirm</value>
      <webElementGuid>b62dd480-157c-4557-8931-5e3f4df2441c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;unit-test-right-btn&quot;)</value>
      <webElementGuid>fe5d77af-b875-44f5-a251-2d79ae72d3fb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='unit-test-right-btn']</value>
      <webElementGuid>e8f7abd0-ca24-4dfa-8a9d-558f71aae279</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='unit-test-popup']/div[3]/button[2]</value>
      <webElementGuid>46a1c7eb-b473-4cc8-b06c-cb5a69cf597c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::button[1]</value>
      <webElementGuid>299187a8-269f-428a-8516-2a8050ed2a80</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Warning!'])[1]/following::button[2]</value>
      <webElementGuid>b5deeb6b-5bb9-4358-b2af-5436e9b5b546</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Confirm']/parent::*</value>
      <webElementGuid>7504f77f-99ac-4e09-9a2a-0d1e99cc6a5f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/button[2]</value>
      <webElementGuid>161d1615-6158-4ef3-870e-8bc6852e7137</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'unit-test-right-btn' and (text() = 'Confirm' or . = 'Confirm')]</value>
      <webElementGuid>73d1cc1c-5de9-4b73-afa6-b5a4a8addc54</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
