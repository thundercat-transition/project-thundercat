<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Rights and Permissions</name>
   <tag></tag>
   <elementGuidId>85e5e2c2-6ee8-4335-a4ad-f37a2f59a636</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='3']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a8a3e079-a09f-44c0-b6ac-d7ac88e2e355</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>0eada4a3-353f-48a5-b37d-a875367c1ac1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>a333c83b-cdb3-4f4f-9f3b-c158716b0220</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-selected</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>603cdfdb-d2f3-43de-badd-b9f0c079711b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>tab</value>
      <webElementGuid>7d6c7b38-dac4-4ca6-980e-947a1930882a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-xl-12 col-lg-12 col-md-12 col-sm-12</value>
      <webElementGuid>2680672b-e2b0-4aac-ac88-a0d059df719b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>id-70c16g-6</value>
      <webElementGuid>be22dc34-75ee-4d23-baf9-71cb9eaa7869</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Rights and Permissions</value>
      <webElementGuid>c80ccd23-ea1d-4e3e-bd34-3331e66e2099</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;3&quot;)</value>
      <webElementGuid>e384473c-ed81-416f-82be-955ad5b07c06</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='3']</value>
      <webElementGuid>d5130641-ece4-426c-a3bd-3efff0fc76a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navigation-items-section']/button[3]</value>
      <webElementGuid>2d2c51ee-01a0-4a67-b8f2-55a093ffe24f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tests'])[1]/following::button[1]</value>
      <webElementGuid>c8dc4255-4822-4c03-8613-f3040cd286d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Personal Information'])[1]/following::button[2]</value>
      <webElementGuid>527e8b7d-e432-49e7-bbe0-0f92a04ffa26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Personal Information'])[2]/preceding::button[1]</value>
      <webElementGuid>7eabd2ae-3253-48b6-9938-fb11f6d9e127</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First Name:'])[1]/preceding::button[1]</value>
      <webElementGuid>3cd70a62-89c4-41cf-9d6c-9c2159135066</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Rights and Permissions']/parent::*</value>
      <webElementGuid>33055ce5-f643-44ae-8743-9d4083897465</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[3]</value>
      <webElementGuid>6b9345a8-ebbe-4dc8-8895-b0300f04a328</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = '3' and (text() = 'Rights and Permissions' or . = 'Rights and Permissions')]</value>
      <webElementGuid>458c9620-985f-4c3d-aa15-207827e4053b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
