<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Reservations</name>
   <tag></tag>
   <elementGuidId>a6f5f93e-fa5b-49a4-8cbb-a977a8622607</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#navigation-items-section > #2</value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='2']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = '2' and (text() = 'Reservations' or . = 'Reservations')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9cccac5d-45b8-4ccd-b2e3-015daac445aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>419c298e-c213-4a2a-bd64-a793e9b31e45</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>44511a3c-8802-4c58-8c00-1d309b526ad3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-selected</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>e86363f9-c529-486f-a419-1563e8118d11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>tab</value>
      <webElementGuid>81b35138-9988-46b4-889b-5fac50ae3a7c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-xl-12 col-lg-12 col-md-12 col-sm-12</value>
      <webElementGuid>37c36498-21fc-4a8d-bead-1930b7d99794</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>id-28keq6-5</value>
      <webElementGuid>e5036e3f-3fa1-4979-975a-7c6ec9e9cb48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Reservations</value>
      <webElementGuid>a9ccb324-510d-4f03-8ca6-072e099ad2b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;2&quot;)</value>
      <webElementGuid>358ab348-7a0a-469e-9fd5-01801ae684da</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='2']</value>
      <webElementGuid>9646b336-7563-427e-8aa8-ae022a72e95a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navigation-items-section']/button[2]</value>
      <webElementGuid>2e32c531-97b4-4083-a31f-494a33adf581</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Take a Test'])[1]/following::button[1]</value>
      <webElementGuid>304c7f96-7d21-4b95-a92e-1403fc656be4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Welcome, Katalon UserThree.'])[1]/following::button[2]</value>
      <webElementGuid>6bc2b649-3926-410f-a78e-79aa9a21beec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Test Results'])[1]/preceding::button[1]</value>
      <webElementGuid>c6f31f6b-aa57-4107-86df-7fcb56206e0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Take a Test'])[2]/preceding::button[2]</value>
      <webElementGuid>4ec2a74a-8761-4c6d-b5a8-9fa858296bee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Reservations']/parent::*</value>
      <webElementGuid>b1d5fc0f-7ae6-454a-b3ff-0f6c535f3202</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]</value>
      <webElementGuid>421082f3-0bf2-463b-b1db-6d68277ba913</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = '2' and (text() = 'Reservations' or . = 'Reservations')]</value>
      <webElementGuid>fd2fedbc-5cef-4a54-9e54-7d21e67701f0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
