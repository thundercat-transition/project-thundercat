<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Manage Reservations</name>
   <tag></tag>
   <elementGuidId>613539e8-68b3-4806-bb2a-2b9b9e9c39cb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#reservation-tabs-tab-manageReservation</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='reservation-tabs-tab-manageReservation']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7a1ad68b-2cdf-45d9-aa10-c2309384ceff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>reservation-tabs-tab-manageReservation</value>
      <webElementGuid>9564cb17-7c79-49be-b34d-0d7f85b4d631</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>3b773a2b-0994-411c-96bb-92f927aa2fb6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>tab</value>
      <webElementGuid>b7358a3b-c812-45ec-9aa0-a177058bfa5a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-rb-event-key</name>
      <type>Main</type>
      <value>manageReservation</value>
      <webElementGuid>a38a945c-1cc3-4610-8a6f-b57d0a1c69fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>reservation-tabs-tabpane-manageReservation</value>
      <webElementGuid>74c83dfc-4f41-46ba-9933-9106b0207492</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-selected</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>b4b6450d-fe83-48c9-913c-550fd29379bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-item nav-link</value>
      <webElementGuid>e9fba260-6707-4258-91b7-fef7ea0702b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Manage Reservations</value>
      <webElementGuid>75d3bfc7-b3a7-4bbb-b30a-9847679b631e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;reservation-tabs-tab-manageReservation&quot;)</value>
      <webElementGuid>623fb21d-a37e-4d43-8e65-676e67b1b46d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='reservation-tabs-tab-manageReservation']</value>
      <webElementGuid>feb7a875-3cc5-449a-9914-b98677edabaf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='id-vs6igl-5']/div/div/div/div/div/div/nav/a[2]</value>
      <webElementGuid>7edba15a-c7a2-4468-9923-750dc3745322</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Manage Reservations')]</value>
      <webElementGuid>3049a716-6fc9-4b29-be2c-af30b764b2ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reservations'])[2]/following::a[2]</value>
      <webElementGuid>863ed48a-45ed-4ded-be9c-a6578b120960</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Check-in'])[1]/following::a[2]</value>
      <webElementGuid>c87eea17-dc80-4308-a59d-5aff014847f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reservation Code:'])[1]/preceding::a[1]</value>
      <webElementGuid>027fdb20-d914-492d-96d2-06a4836a59c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Type your Reservation Code that starts with the letters RES.'])[1]/preceding::a[1]</value>
      <webElementGuid>df58fa35-5d10-425f-8522-cd06be47c45e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Manage Reservations']/parent::*</value>
      <webElementGuid>a2f024ee-6aab-4efe-a40d-3c8f52053f50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[3]</value>
      <webElementGuid>2df9086f-1fb9-49cd-b10b-d7206aaf2cae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]</value>
      <webElementGuid>896bb37f-c860-4f21-a991-92ee530096f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'reservation-tabs-tab-manageReservation' and @href = '#' and (text() = 'Manage Reservations' or . = 'Manage Reservations')]</value>
      <webElementGuid>f9c29c02-649d-40b5-97ee-a9284c760318</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
