<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_No tests have been scored yet</name>
   <tag></tag>
   <elementGuidId>12fa3c90-5068-4a5a-bfbd-e7122411fa46</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='my-tests-no-data']/td</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#my-tests-no-data > td</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>2b5177ab-d49a-4481-8661-582df371bb8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colspan</name>
      <type>Main</type>
      <value>7</value>
      <webElementGuid>af607ab6-63fb-4f19-b2af-854e07de8e4b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>No tests have been scored yet.</value>
      <webElementGuid>2194db7c-0b72-4aa9-a0b2-f1f237509dde</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;my-tests-tabs-tabpane-valid-tests&quot;)/div[1]/div[@class=&quot;table-responsive&quot;]/table[@id=&quot;my-tests-table-id&quot;]/tbody[1]/tr[@id=&quot;my-tests-no-data&quot;]/td[1]</value>
      <webElementGuid>33e49fa7-ee00-405d-9ed2-82fdd1d84ca4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='my-tests-no-data']/td</value>
      <webElementGuid>fe8b6306-cac0-4688-b645-2acc69de051a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/following::td[1]</value>
      <webElementGuid>484b3287-27c7-4299-8568-836594309b5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Retest Date'])[1]/following::td[1]</value>
      <webElementGuid>3cce2022-26fe-4fe9-b42c-72b548cc3850</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[5]/preceding::td[1]</value>
      <webElementGuid>083210f6-209b-4e8d-abcb-777e31951a2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/table/tbody/tr/td</value>
      <webElementGuid>af666da2-8f82-41c7-a753-6998cd1a6480</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'No tests have been scored yet.' or . = 'No tests have been scored yet.')]</value>
      <webElementGuid>3c2b84e8-4694-4de8-93fb-bc1fd6e3c84d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
