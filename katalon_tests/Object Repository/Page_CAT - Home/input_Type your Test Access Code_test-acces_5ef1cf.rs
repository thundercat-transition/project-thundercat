<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Type your Test Access Code_test-acces_5ef1cf</name>
   <tag></tag>
   <elementGuidId>45b660e7-bded-4171-bdd5-64ea5c7443aa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='test-access-code-field']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#test-access-code-field</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c484df8b-79a6-4d15-acf4-ce1ba1f353a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>test-access-code-field</value>
      <webElementGuid>569fd88f-3c87-4b32-a231-6f7664e34ca1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>valid-field</value>
      <webElementGuid>bcad62b6-f015-4050-9ad4-fd6b9f69e8ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>test-access-code-title</value>
      <webElementGuid>d7b249cd-8682-442a-beb2-6ab9b982ffb2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>1a993c4a-a0d0-4fe7-9883-39fe5c8d80a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;test-access-code-field&quot;)</value>
      <webElementGuid>75db6a0d-d233-409a-b305-8d29fb62a517</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='test-access-code-field']</value>
      <webElementGuid>98f5d8be-4a8e-4b23-98b8-20578fbd4298</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='candidate-check-in']/div/div/div/div[2]/div/div/input</value>
      <webElementGuid>abf89821-9324-47f2-bc7f-9114f873fe84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>70c7eb8a-12fa-48c9-99d0-20df7ea5d12c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'test-access-code-field' and @type = 'text']</value>
      <webElementGuid>34995b52-c4e2-4e59-9efc-4aaf0fe698ef</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
