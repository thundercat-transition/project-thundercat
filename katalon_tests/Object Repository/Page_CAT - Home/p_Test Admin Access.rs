<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Test Admin Access</name>
   <tag></tag>
   <elementGuidId>23b2780a-9612-4897-93a5-eaab42cb3892</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='id-m1cmai-6']/div/div/div/div/section/div/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#id-m1cmai-6 > div > div > div > div > section > div > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>1981fe4f-00ab-4692-85f0-3369881ee4a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>As a Test Administrator, Katalon UserThree has access to the following tests:</value>
      <webElementGuid>fd89ba6c-6032-416f-abf7-18c4ad99a833</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;id-m1cmai-6&quot;)/div[1]/div[1]/div[1]/div[1]/section[1]/div[1]/p[1]</value>
      <webElementGuid>a027f6ab-2aad-44ff-b197-65fbcb21ea19</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='id-m1cmai-6']/div/div/div/div/section/div/p</value>
      <webElementGuid>045c6a20-5103-442a-97c9-2d3e36c04224</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Access Permissions'])[1]/following::p[1]</value>
      <webElementGuid>1401bd18-8201-40a4-9fe1-ad10884669b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The user does not have any rights.'])[1]/following::p[1]</value>
      <webElementGuid>224a002f-6ca4-41e9-8631-e3e2cd7e0c03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Accesses'])[1]/preceding::p[1]</value>
      <webElementGuid>23c52ca4-ccf1-4d65-bf7e-5f5662121672</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The user does not have any test access permissions.'])[1]/preceding::p[1]</value>
      <webElementGuid>3931bfed-b3b4-449f-a7d3-01501521788f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='As a Test Administrator, Katalon UserThree has access to the following tests:']/parent::*</value>
      <webElementGuid>1f9a3c99-434e-4d01-aaeb-053445d7d295</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/div/section/div/p</value>
      <webElementGuid>d86afaa1-d954-4507-b73e-e5a5ccb6b949</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'As a Test Administrator, Katalon UserThree has access to the following tests:' or . = 'As a Test Administrator, Katalon UserThree has access to the following tests:')]</value>
      <webElementGuid>5746a3c0-46e7-4a41-9dc0-13b1be06bd73</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
