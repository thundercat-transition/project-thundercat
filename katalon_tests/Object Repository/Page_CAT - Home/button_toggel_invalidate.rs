<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_toggel_invalidate</name>
   <tag></tag>
   <elementGuidId>7aef7032-5d9e-4120-9300-21199b6b2f27</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span:nth-of-type(3) > div > button.secondary.undefined</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='selected-user-all-tests-column-7-label']/div/div/span[3]/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>24938ed1-d110-4e90-9a87-459e69b389d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>499dca7e-d7ac-4624-9b50-6265fac578d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Invalidate 654X1E</value>
      <webElementGuid>3313228f-14a5-4392-92fd-07a135c371f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;selected-user-all-tests-table-row-0&quot;)/td[@id=&quot;selected-user-all-tests-column-7-label&quot;]/div[@class=&quot;notranslate&quot;]/div[@class=&quot;d-flex justify-content-center flex-nowrap&quot;]/span[3]/div[1]/button[@class=&quot;secondary undefined&quot;]</value>
      <webElementGuid>585cf0fa-9d79-4237-ad1e-6d1dad8fafe6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>secondary undefined</value>
      <webElementGuid>a9eee061-e1b2-4239-99da-5627c2192164</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-for</name>
      <type>Main</type>
      <value>selected-user-invalidate-validate-specific-test-0</value>
      <webElementGuid>819102cb-f612-4895-b3a8-57511aadbd28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>currentitem</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>97781da7-fc41-4317-9132-3abbe46cc6b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>selected-user-invalidate-validate-specific-test-0</value>
      <webElementGuid>d2c50af6-c9b6-4618-830d-bac4affb0d72</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='selected-user-all-tests-column-7-label']/div/div/span[3]/div/button</value>
      <webElementGuid>9fa744d4-3e98-4d6c-ab32-95d97cc1ba09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unassigned'])[1]/following::button[3]</value>
      <webElementGuid>246bdf16-5bac-4f19-b468-2cdb2d3db05b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Not Scored'])[1]/following::button[3]</value>
      <webElementGuid>04876571-e078-498f-8a04-58dd0c6c637d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Not Scored'])[2]/preceding::button[1]</value>
      <webElementGuid>c966db75-86b1-4937-acd5-8a0e805c18ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unassigned'])[2]/preceding::button[1]</value>
      <webElementGuid>415ad3f7-3aac-4653-8f74-5ea34a4ed798</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[3]/div/button</value>
      <webElementGuid>52191e7d-3c8f-44b0-a6a8-b71368d9a432</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='all-active-candidates-column-6-label']/div/span/div/button</value>
      <webElementGuid>6a55ff0b-b26a-418a-b92b-a3ecaf09ed00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pre-Test'])[1]/following::button[1]</value>
      <webElementGuid>a8826775-cf90-4d04-a422-e756ee38341f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='KATALON'])[1]/following::button[1]</value>
      <webElementGuid>3cb86a3c-74c5-43e1-bf6a-b9e4c42f8e8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous page'])[3]/preceding::button[1]</value>
      <webElementGuid>364dfd61-7970-4e58-9cac-ac97f6383cee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next page'])[3]/preceding::button[1]</value>
      <webElementGuid>33da8808-b878-437a-9dbb-46806affc2b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/div/button</value>
      <webElementGuid>636af222-bac4-40c4-a272-aa439cbe272e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='reserved-codes-column-3-label']/div/span[2]/div/button</value>
      <webElementGuid>65c88bc5-dc66-4299-a182-80f6dea27d89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RES-U2NVJ0ARSW'])[1]/following::button[2]</value>
      <webElementGuid>734f8274-e9f7-4b2c-a3a2-5bc87e76cb4e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Second Language Evaluation - English - Written Expression'])[1]/following::button[2]</value>
      <webElementGuid>c6de8edf-5e1a-4fc2-a3ae-68e69dbc5cc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[2]/preceding::button[1]</value>
      <webElementGuid>90faa7e6-0ebe-4d17-acf7-bcb100f6f6ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Session Date'])[1]/preceding::button[1]</value>
      <webElementGuid>5e8180a5-50df-479f-a14c-c1e99e44f330</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/div/button</value>
      <webElementGuid>7de05836-6896-4558-b75a-43a60e86f503</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='reserved-codes-column-3-label']/div/span/div/button</value>
      <webElementGuid>902a21ff-3ab3-4de0-b1c3-9b484293297e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RES-U2NVJ0ARSW'])[1]/following::button[1]</value>
      <webElementGuid>bacd1ead-96db-4396-b014-dbca25b25d52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Second Language Evaluation - English - Written Expression'])[1]/following::button[1]</value>
      <webElementGuid>9c21aaa0-565f-45a0-9dcd-dacece1cf97b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[2]/preceding::button[2]</value>
      <webElementGuid>b82d20bd-c209-4de0-aa02-e4cb5029d1d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Session Date'])[1]/preceding::button[2]</value>
      <webElementGuid>717f24d4-2c61-48e4-86ec-b2a24ef91180</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[5]</value>
      <webElementGuid>0c007706-47ce-4f47-8f8e-470bd42b292e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='id-bfa4pa-4']/div/div/div/form/div[8]/div[3]/button</value>
      <webElementGuid>d9afcbf2-7edf-4dac-a3ef-c38d99def8ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm Password:'])[1]/following::button[1]</value>
      <webElementGuid>600b40ff-a4bb-4838-b4e8-d2189edc4a82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Minimum of 8 characters and maximum of 15'])[2]/following::button[2]</value>
      <webElementGuid>858ab887-15a7-4cc6-b9e5-8bce6efa1449</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Privacy Notice'])[1]/preceding::button[1]</value>
      <webElementGuid>98d196b9-ed7a-424d-a683-8b14a1ad5512</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create account'])[1]/preceding::button[2]</value>
      <webElementGuid>0fc9021e-e246-466d-8309-2389d0289b52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div[3]/button</value>
      <webElementGuid>02a78806-363b-4b23-ab34-844826bba323</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
