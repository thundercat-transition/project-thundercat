<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Number of results per page_react-sele_823129</name>
   <tag></tag>
   <elementGuidId>8fa3c9a6-2bcd-406e-9534-98ff24facd41</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#react-select-20-input</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='react-select-20-input']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'text' and @aria-labelledby = 'user-look-up-display-option-accessibility user-look-up-display-options-dropdown-value-label']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>2fcd283a-e298-4292-a0e1-c5ab42b4c42b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>5b7c4022-0b50-4803-982c-a3b5e412ebc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>f44195b4-acb8-4d53-9bdc-cc5f8436654a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>21b94637-c83a-4623-808f-fc8536e96a2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>react-select-20-input</value>
      <webElementGuid>06f84d84-e25d-4c03-a638-24e829a538a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>spellcheck</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>c6026107-63f5-4961-9c09-2f2ab9024fa6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>558c318a-729a-4265-957a-f485131dbe0b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>b3b42a53-8b77-4fa2-917f-e107dfb084c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-autocomplete</name>
      <type>Main</type>
      <value>list</value>
      <webElementGuid>84ebb28a-7409-43ef-95ec-6f78fb831f5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>user-look-up-display-option-accessibility user-look-up-display-options-dropdown-value-label</value>
      <webElementGuid>43e02583-c7ed-4faf-8c69-9ce2a8cf25a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>5</value>
      <webElementGuid>e496cc43-8883-4340-97ed-278669dd43a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-select-20-input&quot;)</value>
      <webElementGuid>7cb04ffd-46a4-48bd-8521-ecea574f892f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='react-select-20-input']</value>
      <webElementGuid>c40b7db3-cbd6-4ce3-82b8-d089ae94e46a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='user-look-up-display-options-dropdown-react-select-dropdown']/div/div/div/div/input</value>
      <webElementGuid>5d636ea2-2411-4469-8ebb-e23cc8fc5941</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div/div/div/div[2]/div/div[2]/div/div/div/div/div/div/input</value>
      <webElementGuid>c8bced37-b03a-49fb-8a79-611702df24d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'react-select-20-input' and @type = 'text']</value>
      <webElementGuid>068db1c5-c6de-41b6-8966-60ec25a80a97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='all-active-candidates-display-options-dropdown-react-select-dropdown']/div/div/div/div/input</value>
      <webElementGuid>f711197c-2e03-4677-b533-424fa5c9e87e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/div/div[2]/div/div[2]/div/div/div/div/div/div/input</value>
      <webElementGuid>0e6dc355-8c1a-4aee-a956-6d75b005bb8a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
