<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Claim Code</name>
   <tag></tag>
   <elementGuidId>8ab40e2d-f4ba-436e-92e3-e2266dd14a59</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-xl-4.col-lg-12.col-md-12.col-sm-12.col-12 > button.primary.undefined</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='reservation-tabs-tabpane-makeReservation']/div/div/div[3]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>678afeae-e6af-46fd-87ff-5362b6615d38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>primary undefined</value>
      <webElementGuid>19f8e69c-dc25-4247-9c8e-f8c2ab7c8e6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Claim Code</value>
      <webElementGuid>f8ffa6f4-e1ba-4bfd-b58e-f37476b5a59e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;reservation-tabs-tabpane-makeReservation&quot;)/div[1]/div[@class=&quot;align-items-center align-items-start justify-content-center row&quot;]/div[@class=&quot;col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12&quot;]/button[@class=&quot;primary undefined&quot;]</value>
      <webElementGuid>015555c4-0917-458e-be7f-daf2944491ae</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='reservation-tabs-tabpane-makeReservation']/div/div/div[3]/button</value>
      <webElementGuid>98db9b64-ea74-4db0-b496-81328b63e9d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Type your Reservation Code that starts with the letters RES.'])[1]/following::button[1]</value>
      <webElementGuid>f8e9b425-f64b-4a4f-99e7-060d3c07c813</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reservation Code:'])[1]/following::button[2]</value>
      <webElementGuid>432ba773-4120-445f-ba64-8294e704694f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[1]/preceding::button[1]</value>
      <webElementGuid>c78ee72c-21bb-4144-87db-342c0771bb30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div[3]/button</value>
      <webElementGuid>d8a414fa-a2e9-45fa-90e0-a7d615d25564</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Claim Code' or . = 'Claim Code')]</value>
      <webElementGuid>729f5980-7dec-4e9e-ba23-32ad414d537c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
