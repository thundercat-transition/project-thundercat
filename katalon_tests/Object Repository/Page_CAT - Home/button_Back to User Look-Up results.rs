<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Back to User Look-Up results</name>
   <tag></tag>
   <elementGuidId>cf556dbd-5786-46d8-9c82-0425dbe2b51c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.row > button.secondary.undefined</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-content']/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>30f4d2bc-d64a-4998-805d-d6057841b3e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>secondary undefined</value>
      <webElementGuid>044bcff8-127b-41eb-9ad2-db5c94c986b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Back to User Look-Up results</value>
      <webElementGuid>ef500e44-c767-44f6-bf40-7c2ec045cb4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content&quot;)/div[@class=&quot;row&quot;]/button[@class=&quot;secondary undefined&quot;]</value>
      <webElementGuid>9dd0e563-9f9f-472b-bea0-b60c2690fff2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-content']/div/button</value>
      <webElementGuid>aea6d89f-1aa2-4111-b36e-8dcd3d238254</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Français'])[1]/following::button[1]</value>
      <webElementGuid>870b8285-23f5-49ac-9d07-a5340aafd649</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::button[2]</value>
      <webElementGuid>310ad764-77c5-41e3-b3fe-2cab5cf418b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='User Details (Katalon UserThree)'])[1]/preceding::button[1]</value>
      <webElementGuid>d230f9e6-68a4-4556-91fb-23ca6b8ff4ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/button</value>
      <webElementGuid>2f07c6e2-cdbc-4775-a56d-9349d8cd2696</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Back to User Look-Up results' or . = 'Back to User Look-Up results')]</value>
      <webElementGuid>97cafe3d-a27b-4263-b4c7-6dbbb541e4d1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
