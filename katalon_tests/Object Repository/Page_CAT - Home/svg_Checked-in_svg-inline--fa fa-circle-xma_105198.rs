<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Checked-in_svg-inline--fa fa-circle-xma_105198</name>
   <tag></tag>
   <elementGuidId>60d90771-e5ec-425d-8c5f-96fa9c9e22db</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Quit'])[1]/following::*[name()='svg'][4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>fffd804e-9479-4c1f-93ca-81b881ca68fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>09b75463-6768-4dae-8afb-8a40d8f2f300</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>0a680306-4d5e-4e9d-a806-23cac7ad6235</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-prefix</name>
      <type>Main</type>
      <value>far</value>
      <webElementGuid>99b4fcb1-4c5b-4920-9920-2d57f8c17653</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-icon</name>
      <type>Main</type>
      <value>circle-xmark</value>
      <webElementGuid>0ab98220-318e-48f8-a4bc-656253b71587</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>svg-inline--fa fa-circle-xmark fa-w-16 </value>
      <webElementGuid>e0327847-94b6-414a-9806-ee0a634c4fb2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>eaf30150-cb5a-4a6a-bb89-4e2fc77dfd91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>http://www.w3.org/2000/svg</value>
      <webElementGuid>a6f82a87-cd48-4dda-8d6d-25f39e4172f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 512 512</value>
      <webElementGuid>113ba17a-590d-45bc-8320-29a49ac98ff5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;selected-user-all-tests-table-row-0&quot;)/td[@id=&quot;selected-user-all-tests-column-7-label&quot;]/div[@class=&quot;notranslate&quot;]/div[@class=&quot;d-flex justify-content-center flex-nowrap&quot;]/span[3]/div[1]/button[@class=&quot;secondary undefined&quot;]/svg[@class=&quot;svg-inline--fa fa-circle-xmark fa-w-16&quot;]</value>
      <webElementGuid>f71c017f-107b-43d4-97f5-595f3a41e9ac</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quit'])[1]/following::*[name()='svg'][4]</value>
      <webElementGuid>c81eac7e-d74f-4727-b07f-1b3bbffbd2a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Not Scored'])[1]/following::*[name()='svg'][4]</value>
      <webElementGuid>aaf400e2-6679-4b90-87f5-38b0c2ea7277</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Not Scored'])[2]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>fcf1e1a1-fcd7-4bd1-ad14-de3980b991ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unassigned'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>cf5aff94-80ec-4cb6-8e1e-036b58d01946</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unassigned'])[2]/following::*[name()='svg'][1]</value>
      <webElementGuid>d1dc4c61-a33b-4e23-9aa7-01e87a9b9c7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Not Scored'])[2]/following::*[name()='svg'][1]</value>
      <webElementGuid>97b93e24-8695-4c5d-924c-e21373d14129</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Not Scored'])[3]/preceding::*[name()='svg'][4]</value>
      <webElementGuid>a5bcc8b1-7054-4d57-85dd-2685322f5671</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unassigned'])[3]/preceding::*[name()='svg'][4]</value>
      <webElementGuid>edf18a6e-9095-43a9-b4fa-7c3ca397fee2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Unassigned'])[3]/following::*[name()='svg'][1]</value>
      <webElementGuid>aabd95eb-8a66-45c8-b1f1-c51288e167d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Not Scored'])[3]/following::*[name()='svg'][1]</value>
      <webElementGuid>aaccb5aa-d618-4963-8737-4c194a04b2e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous page'])[1]/preceding::*[name()='svg'][4]</value>
      <webElementGuid>94dca277-86a5-43f4-bc31-669dcef6ac90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next page'])[1]/preceding::*[name()='svg'][5]</value>
      <webElementGuid>32a2a1e9-ab05-4203-a7c5-3af35be928e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checked-in'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>6f7c22d8-2141-40fd-a40c-20724b40811d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='KATALONTEST'])[1]/following::*[name()='svg'][1]</value>
      <webElementGuid>8e03d79e-c14b-43b5-8b59-23b4ca53755c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous page'])[3]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>9a6c55d5-ecaa-4e84-981c-add4b79888ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next page'])[3]/preceding::*[name()='svg'][2]</value>
      <webElementGuid>4575df68-1723-4583-b17b-fde2938be640</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
