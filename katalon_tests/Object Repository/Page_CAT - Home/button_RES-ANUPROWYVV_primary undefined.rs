<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_RES-ANUPROWYVV_primary undefined</name>
   <tag></tag>
   <elementGuidId>395f184a-ba3d-4c37-aef7-09355c82c63b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span > div > button.primary.undefined</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='reserved-codes-column-3-label']/div/span/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a69fe8fe-00b1-4eeb-87a4-41cdc2e3dbe4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>primary undefined</value>
      <webElementGuid>9c51235c-59d7-420f-a64a-e16cdc5ac8be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-for</name>
      <type>Main</type>
      <value>find-reservation-button-0</value>
      <webElementGuid>88cdf881-2e3d-4939-8825-38135d0e4d82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Todo Find Sessions help text</value>
      <webElementGuid>ef11c5ef-a6aa-45c3-9294-ebd6560e0b32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>currentitem</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>b2b87ebb-61cc-4c13-931b-b067e593c92c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>find-reservation-button-0</value>
      <webElementGuid>e3dffc61-aa6b-443b-a8c3-ab0035992d6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;reserved-codes-column-3-label&quot;)/div[@class=&quot;notranslate&quot;]/span[1]/div[1]/button[@class=&quot;primary undefined&quot;]</value>
      <webElementGuid>de600a4d-efc2-4081-82da-6fb444250c78</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='reserved-codes-column-3-label']/div/span/div/button</value>
      <webElementGuid>f44f7ee7-9471-4c39-8a4d-fc3a23e870be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='RES-ANUPROWYVV'])[1]/following::button[1]</value>
      <webElementGuid>68c044e0-ad5d-4f8a-b0c8-4b26a5e397e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Second Language Evaluation - English - Written Expression'])[1]/following::button[1]</value>
      <webElementGuid>087b5dbe-5de6-4d25-9c76-df17b96e1a44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[2]/preceding::button[2]</value>
      <webElementGuid>0c09079c-79d5-4512-ba44-da1f3e5deb0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Session Date'])[1]/preceding::button[2]</value>
      <webElementGuid>1a690562-9732-4f4a-bb4d-646d31ca038a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/div/button</value>
      <webElementGuid>a1d9bba7-d709-466b-963d-086f6d32de58</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
