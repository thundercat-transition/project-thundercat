<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Claim Code (1)</name>
   <tag></tag>
   <elementGuidId>ed89f248-7434-401f-b0c9-febcd2576025</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='reservation-tabs-tabpane-makeReservation']/div/div/div[3]/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xl-4.col-lg-12.col-md-12.col-sm-12.col-12 > button.primary.undefined</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>394c2e2a-1799-4c1d-81c8-1ff612034a3b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>primary undefined</value>
      <webElementGuid>c1e66438-39c4-4112-9f1d-2d544d1a1e39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Claim Code</value>
      <webElementGuid>10449ff4-4af7-4282-b60c-a92adbd54755</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;reservation-tabs-tabpane-makeReservation&quot;)/div[1]/div[@class=&quot;align-items-center align-items-start justify-content-center row&quot;]/div[@class=&quot;col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12&quot;]/button[@class=&quot;primary undefined&quot;]</value>
      <webElementGuid>6d87cd23-350c-48db-8198-789f694f42e4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='reservation-tabs-tabpane-makeReservation']/div/div/div[3]/button</value>
      <webElementGuid>40f6cdc2-3b28-437e-a709-58e8879e1ffd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Type your Reservation Code that starts with the letters RES.'])[1]/following::button[1]</value>
      <webElementGuid>dea36f5d-dc87-40a6-9c91-7e0d056221ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reservation Code:'])[1]/following::button[2]</value>
      <webElementGuid>ac8def0a-675d-4df6-9883-5b1aac5f459e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[1]/preceding::button[1]</value>
      <webElementGuid>17a001a1-19e4-4e8f-9d12-c721bf9e0951</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div[3]/button</value>
      <webElementGuid>d55fb81b-a3ee-45e8-83e2-fba82420e81f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Claim Code' or . = 'Claim Code')]</value>
      <webElementGuid>3d1d003f-891f-4017-8c5d-15b39b5259c2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
