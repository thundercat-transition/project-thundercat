<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Unsupervised Test of Reading Comprehension (assesses French as second official language)</name>
   <tag></tag>
   <elementGuidId>a8f1ed2b-8ef6-4774-add7-aa61765e10ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='assigned-tests-column-1-label']/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Unsupervised Test of Reading Comprehension (assesses French as second official language)' or . = 'Unsupervised Test of Reading Comprehension (assesses French as second official language)')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#assigned-tests-column-1-label > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>b8c88451-df7d-4f29-9743-d2d0c44c62a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Unsupervised Test of Reading Comprehension (assesses French as second official language)</value>
      <webElementGuid>3b27a62c-93d4-4a7f-87fd-e8d12370e756</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;assigned-tests-column-1-label&quot;)/div[1]</value>
      <webElementGuid>8123430d-57a0-4396-a56e-f3ba0d3221c9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='assigned-tests-column-1-label']/div</value>
      <webElementGuid>1310542e-60d0-4f57-b198-3f17b882c7bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/following::div[1]</value>
      <webElementGuid>a4089d75-af92-4430-b615-84e30ab2fa8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test'])[1]/following::div[1]</value>
      <webElementGuid>d149936d-9b7d-4ced-883f-6ac5a1b6fe70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Start'])[1]/preceding::div[1]</value>
      <webElementGuid>c2df2b9d-3f4c-4934-b5ef-5531442060df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::div[6]</value>
      <webElementGuid>c3b86383-bbfa-4e51-83a5-1d59e22ec2c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Unsupervised Test of Reading Comprehension (assesses French as second official language)']/parent::*</value>
      <webElementGuid>4ca3baf3-569e-45c6-826f-091519d59bcb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div</value>
      <webElementGuid>ec125abd-5bd0-4d3c-8ded-a47e3f7386f3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
