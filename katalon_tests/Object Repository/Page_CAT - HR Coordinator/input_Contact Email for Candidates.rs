<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Contact Email for Candidates</name>
   <tag></tag>
   <elementGuidId>ff4df673-4837-42d7-8fa7-0b1d904f3db4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#create-process-contact-email-for-candidates-input</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='create-process-contact-email-for-candidates-input']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ac6c9d64-499c-4c2f-af19-cbde57babce7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>create-process-contact-email-for-candidates-input</value>
      <webElementGuid>9a9caf2b-4076-41dd-a1e8-e2fe8984a9c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>valid-field</value>
      <webElementGuid>dbe9ad5e-d78e-48f3-b616-9416db26f556</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>bf83fb85-8063-4d6c-a4d8-9d4a494b8f74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>3eba7b57-ccfa-4ab1-86cf-83e2c8a831b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;create-process-contact-email-for-candidates-input&quot;)</value>
      <webElementGuid>2afaf9d3-89fd-4624-90b7-f1e5063e3b52</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='create-process-contact-email-for-candidates-input']</value>
      <webElementGuid>3fb7b7d5-8980-40dd-9f40-cce730fecd72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='assessment-processes-tabs-tabpane-create-process']/div/div/div[6]/div[2]/input</value>
      <webElementGuid>047baa06-8340-43ce-89b8-385007a489c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div[2]/input</value>
      <webElementGuid>ab9f6498-336f-40e5-bec5-4922df342f25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'create-process-contact-email-for-candidates-input' and @type = 'text']</value>
      <webElementGuid>8b360252-a16c-48a3-b0c9-e83256528081</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
