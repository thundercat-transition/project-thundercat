<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Add Test</name>
   <tag></tag>
   <elementGuidId>f161729c-e619-41e0-bac7-c70cb3cde73a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#unit-test-right-btn</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='unit-test-right-btn']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a3e64f71-ba51-48d5-b653-7f80721f66a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>unit-test-right-btn</value>
      <webElementGuid>dfa779a4-fb0a-4bcb-bd1d-debd6575130a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>primary undefined</value>
      <webElementGuid>dabb591a-8950-48c6-9f0a-705ed3bc28ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>21e3d979-61c8-45c2-916f-b0ad5ca04313</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add Test</value>
      <webElementGuid>1903ec41-0d67-40e2-acdc-183f679bef16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;unit-test-right-btn&quot;)</value>
      <webElementGuid>1cc3baaa-b83a-4c40-9810-185bda8f2b4f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='unit-test-right-btn']</value>
      <webElementGuid>04ad20c7-6d32-470d-b44f-15825e60c260</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='unit-test-popup']/div[3]/button[2]</value>
      <webElementGuid>40cb1717-6bc8-4d3b-85ec-5e514fba7ea0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::button[1]</value>
      <webElementGuid>2b8d0bc1-bd25-45bd-91cc-16fb914c6171</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Default Required Level'])[2]/following::button[2]</value>
      <webElementGuid>adabed39-9e5c-4f44-af64-9f8b66c14df3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/button[2]</value>
      <webElementGuid>dae85d3c-216c-4732-995a-16198dee8475</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'unit-test-right-btn' and (text() = 'Add Test' or . = 'Add Test')]</value>
      <webElementGuid>5e64c2e9-a75e-4d2e-bb0e-f38af78657e1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
