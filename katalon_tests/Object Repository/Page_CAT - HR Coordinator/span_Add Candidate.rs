<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Add Candidate</name>
   <tag></tag>
   <elementGuidId>939e9f47-9c0f-4a90-ae40-37c10f0e05cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#assign-candidate-add-candidate-button > button.undefined.undefined > span:nth-of-type(2)</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='assign-candidate-add-candidate-button']/button/span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>7de9fa8c-dcd0-4d36-884d-a70b7605691a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add Candidate</value>
      <webElementGuid>16c20565-baeb-4fd7-9189-ab279d0c39bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;assign-candidate-add-candidate-button&quot;)/button[@class=&quot;undefined undefined&quot;]/span[2]</value>
      <webElementGuid>30c84beb-5854-467d-a9c8-f240b5e9d436</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//td[@id='assign-candidate-add-candidate-button']/button/span[2]</value>
      <webElementGuid>d3cc928c-2798-4b02-b5cb-b5d94c231bfb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='simulate-delivered@notification.canada.ca'])[1]/following::span[4]</value>
      <webElementGuid>3bbdef74-8c98-496e-b089-eb4f22a3e2fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='UserTwo'])[1]/following::span[4]</value>
      <webElementGuid>e65b2bd8-e59a-496e-9191-1699acaeab06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Send Invitations'])[1]/preceding::span[2]</value>
      <webElementGuid>53f7c50a-24e7-4cd7-adaf-39abf1a4d702</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assessment Process / Reference Number:'])[2]/preceding::span[3]</value>
      <webElementGuid>d4d833fb-ab5f-43e3-a6b9-ad2468bd8bc0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add Candidate']/parent::*</value>
      <webElementGuid>fff75fd0-49af-4e7e-98ef-d84f35fb9127</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/button/span[2]</value>
      <webElementGuid>16657a7b-c085-4cd9-8669-b478718c5e12</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Add Candidate' or . = 'Add Candidate')]</value>
      <webElementGuid>b27b4193-d975-489e-b13a-a0b606bb0c56</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
