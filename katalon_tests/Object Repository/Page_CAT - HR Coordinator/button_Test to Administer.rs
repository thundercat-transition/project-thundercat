<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Test to Administer</name>
   <tag></tag>
   <elementGuidId>d3b22ceb-f445-47ef-ac55-270dbdc8c153</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>label.notranslate > button.undefined.undefined</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='create-process-tests-to-administer-no-data']/td/label/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>0f9c91e2-e254-48e8-8522-73d0fab2a4f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>undefined undefined</value>
      <webElementGuid>25220a2f-2460-4aac-932e-9fbfc5120cda</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Test to Administer:</value>
      <webElementGuid>db094124-b1d2-49fe-9e20-ec2570f77778</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;create-process-tests-to-administer-no-data&quot;)/td[1]/label[@class=&quot;notranslate&quot;]/button[@class=&quot;undefined undefined&quot;]</value>
      <webElementGuid>5c9ac2b2-4c96-4b88-9fc1-c766b81a0aa4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='create-process-tests-to-administer-no-data']/td/label/button</value>
      <webElementGuid>fa143b01-b138-425b-890c-ce7c92224a44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/following::button[1]</value>
      <webElementGuid>c37ebf29-5822-4a8d-bc33-81375cc68526</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Default Required Level'])[1]/following::button[1]</value>
      <webElementGuid>58447172-e5c2-41c4-af05-9aa5f9c81ec1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create Process'])[2]/preceding::button[1]</value>
      <webElementGuid>07c182e8-3ad1-4afc-a2a9-ef1b9fa4a909</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label/button</value>
      <webElementGuid>b25e5610-72a3-4a95-be27-24db2e86b377</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Test to Administer:' or . = 'Test to Administer:')]</value>
      <webElementGuid>a38537f6-eb6a-47ca-a9f4-8b4cb9b5b232</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
