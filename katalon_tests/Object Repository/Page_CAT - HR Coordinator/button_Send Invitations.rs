<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Send Invitations</name>
   <tag></tag>
   <elementGuidId>037b83fc-5bec-4b72-859c-e6f9861e64c8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div:nth-of-type(8) > button.primary.undefined</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='assessment-processes-tabs-tabpane-assign-candidates']/div/div/div[2]/div[8]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>1e6694f8-8a9b-40d3-b32b-df449badc677</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>primary undefined</value>
      <webElementGuid>44022b8a-72a9-43be-af5b-5c70b9e5e470</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>f6772ba9-cdfa-4845-a75f-90a713639e6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Send Invitations</value>
      <webElementGuid>c33fb6f8-13f3-48f9-9a59-69222be6b8a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;assessment-processes-tabs-tabpane-assign-candidates&quot;)/div[1]/div[1]/div[2]/div[8]/button[@class=&quot;primary undefined&quot;]</value>
      <webElementGuid>7a0aa41e-c98d-4222-96f5-dbc68b839e23</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='assessment-processes-tabs-tabpane-assign-candidates']/div/div/div[2]/div[8]/button</value>
      <webElementGuid>3616d6ab-484e-42b4-9d32-dd1811f8d502</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Candidate'])[1]/following::button[1]</value>
      <webElementGuid>6b7cd103-4eb1-458a-87db-99edbd6b26cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='simulate-delivered@notification.canada.ca'])[1]/following::button[4]</value>
      <webElementGuid>e5e96a70-c3ee-4ada-bbc3-426f076ce55a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assessment Process / Reference Number:'])[2]/preceding::button[1]</value>
      <webElementGuid>9e94265d-6da0-41f6-af51-6116000365fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[8]/button</value>
      <webElementGuid>4358cf97-af2b-4b9c-b7cd-43791dd35d20</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Send Invitations' or . = 'Send Invitations')]</value>
      <webElementGuid>a062742a-a6df-45eb-8f5b-25ac8281df77</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
