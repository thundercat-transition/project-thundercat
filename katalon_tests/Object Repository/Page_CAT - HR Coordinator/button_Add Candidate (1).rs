<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Add Candidate (1)</name>
   <tag></tag>
   <elementGuidId>9239b506-78c0-4060-a1a1-a03c4f14cd7b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='assign-candidate-assigned-candidates-no-data']/td/label/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#assign-candidate-assigned-candidates-no-data > td > label.notranslate > button.undefined.undefined</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>221e7322-a2c9-49e2-b716-67a8bfed2be1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>undefined undefined</value>
      <webElementGuid>bcdd86b3-b80c-44bf-9ac2-ec41f762db09</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add Candidate</value>
      <webElementGuid>4878931f-5c3d-4b67-a058-69b2bad511d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;assign-candidate-assigned-candidates-no-data&quot;)/td[1]/label[@class=&quot;notranslate&quot;]/button[@class=&quot;undefined undefined&quot;]</value>
      <webElementGuid>8c9f1643-d8ae-427f-8df2-e9dd0cc9f68a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='assign-candidate-assigned-candidates-no-data']/td/label/button</value>
      <webElementGuid>6d7641e4-d85e-443f-8112-e4c3c77e6a6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Actions'])[2]/following::button[1]</value>
      <webElementGuid>67f514e8-32e6-4fa4-86b7-c2eba3f7ca5d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tests Details (Status)'])[1]/following::button[1]</value>
      <webElementGuid>33f08bff-db2c-4a4d-8eaf-add020b70840</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Send Invitations'])[1]/preceding::button[1]</value>
      <webElementGuid>7c0b84a7-7730-48e6-82a7-f3f4f89b3110</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div[2]/div/div/table/tbody/tr/td/label/button</value>
      <webElementGuid>12cc2c86-b553-45bb-963f-70c83ca97bda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Add Candidate' or . = 'Add Candidate')]</value>
      <webElementGuid>342c4b7b-c6a6-466a-8dfc-655cdb3a246b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
