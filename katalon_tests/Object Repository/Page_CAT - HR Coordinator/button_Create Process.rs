<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Create Process</name>
   <tag></tag>
   <elementGuidId>0ac94d18-46b3-4f5e-86a2-c57372a50884</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div:nth-of-type(3) > button.primary.undefined</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='assessment-processes-tabs-tabpane-create-process']/div/div[3]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5e596512-33ba-4d34-835b-00d12fc6dd7f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>primary undefined</value>
      <webElementGuid>95f45916-e1bc-43c1-8583-2f420797b160</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>086b954f-1b65-4dca-b6e6-698f6635ddcb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create Process</value>
      <webElementGuid>d0908fae-f769-4864-86e5-91f1dc995f65</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;assessment-processes-tabs-tabpane-create-process&quot;)/div[1]/div[3]/button[@class=&quot;primary undefined&quot;]</value>
      <webElementGuid>da6855c8-a980-400e-bae4-531096d0ffcf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='assessment-processes-tabs-tabpane-create-process']/div/div[3]/button</value>
      <webElementGuid>bb3a01f6-c525-45e0-a1d8-08fe8d5b4088</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test to Administer:'])[1]/following::button[1]</value>
      <webElementGuid>2a698fd0-1a95-42f5-921d-6e52bfc722ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A'])[1]/following::button[4]</value>
      <webElementGuid>3b27ccac-5261-4189-8e9f-296d81154b5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assessment Process / Reference Number:'])[1]/preceding::button[1]</value>
      <webElementGuid>e3dcca4e-5086-4938-9bed-68ed027aa007</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div[3]/button</value>
      <webElementGuid>c6f199b9-d30c-4171-8afd-316756aef742</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Create Process' or . = 'Create Process')]</value>
      <webElementGuid>2cc7d3c2-9be3-45ef-b79a-2c9bb66d4384</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
