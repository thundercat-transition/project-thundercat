<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Add Candidate</name>
   <tag></tag>
   <elementGuidId>e1cee5da-499b-4eea-9f43-8563d2e45ec9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#assign-candidate-add-candidate-button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//tr[@id='assign-candidate-assigned-candidates-no-data']/td/label/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Add Candidate' or . = 'Add Candidate')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>123ffdab-9cdb-4b47-8ffc-94f3df81ad7d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>undefined undefined</value>
      <webElementGuid>8d684a60-eff7-4d59-acfe-4697eca170c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add Candidate</value>
      <webElementGuid>f71ded15-089e-43a5-ad28-4af3b5fa886a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;assign-candidate-assigned-candidates-no-data&quot;)/td[1]/label[@class=&quot;notranslate&quot;]/button[@class=&quot;undefined undefined&quot;]</value>
      <webElementGuid>e6159ec2-4f85-4dc1-bba1-9f5b89a9e28c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//tr[@id='assign-candidate-assigned-candidates-no-data']/td/label/button</value>
      <webElementGuid>f1ff14a6-8d38-425b-a2ec-bf4221328390</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Actions'])[2]/following::button[1]</value>
      <webElementGuid>d30563c1-9fdd-483e-a0fe-295d76ef1349</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tests Details (Status)'])[1]/following::button[1]</value>
      <webElementGuid>c3f138dd-a696-4e9e-a022-90f2d597f743</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Send Invitations'])[1]/preceding::button[1]</value>
      <webElementGuid>895c8067-358c-4d08-8e5d-355efa8bbf58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div[2]/div/div/table/tbody/tr/td/label/button</value>
      <webElementGuid>796e2258-1897-4961-a81e-d04244514c84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Add Candidate' or . = 'Add Candidate')]</value>
      <webElementGuid>91bc517b-e7e7-4762-a369-71afb31016ac</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
