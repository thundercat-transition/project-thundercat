<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>modify_test_access_confirmation_popup_button_OK</name>
   <tag></tag>
   <elementGuidId>d2179ee0-94b3-4c55-82aa-52b91fd099ee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#unit-test-right-btn</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='unit-test-right-btn']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>f089e648-42c8-4cb4-8cbb-74ac469f8b15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>unit-test-right-btn</value>
      <webElementGuid>3a0e9882-0e88-4998-ba4e-8e9c4ffb9240</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>primary</value>
      <webElementGuid>24203fad-a274-4a5b-a0ee-129463f38d1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>OK</value>
      <webElementGuid>99892b1f-3d0e-41e5-b716-25d649896fcd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>OK</value>
      <webElementGuid>d6b8cd2a-f347-4584-b0dc-7715087a8a44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;unit-test-right-btn&quot;)</value>
      <webElementGuid>cecf0458-92c9-4d9c-b71d-7f714616aea0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='unit-test-right-btn']</value>
      <webElementGuid>5c2df2ca-5fdd-4c61-ad9d-afe888595306</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='unit-test-popup']/div[3]/div/button</value>
      <webElementGuid>d6ef2dbc-a4ff-4091-9a16-48b27c6dd177</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Katalon TA'])[1]/following::button[1]</value>
      <webElementGuid>2aaac563-d8e1-43d3-b4cc-a7ed3aafe75f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::button[1]</value>
      <webElementGuid>33904c72-faca-465e-8f2d-ebb0be675031</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[1]/preceding::button[1]</value>
      <webElementGuid>56dcb618-311a-4919-968b-d8f101792181</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='OK']/parent::*</value>
      <webElementGuid>d351f9a3-3564-427b-a9f2-2af0bc1e4d39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/button</value>
      <webElementGuid>9a27c266-4a4c-4a25-824e-ff8106fc693a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
