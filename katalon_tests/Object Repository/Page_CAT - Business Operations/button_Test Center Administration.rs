<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Test Center Administration</name>
   <tag></tag>
   <elementGuidId>09aa7399-b2fa-4330-a42d-8ba99c2e15ec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@id='3'])[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#navigation-items-section > #3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>30434ba2-8628-463a-85c2-be5a6a674239</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>18fe8d4c-3139-49da-91d2-2fc97a9fead1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>3e163b18-b484-4ea4-a3e8-fd678110fc46</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-selected</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>17a1e4e0-1f5a-4455-bfeb-a153adf441e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>tab</value>
      <webElementGuid>7112e6b0-098a-4820-9805-df81710b5e44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-xl-12 col-lg-12 col-md-12 col-sm-12</value>
      <webElementGuid>b89ea621-12dc-44bd-a120-e0690d4b87ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>id-crrau5-13</value>
      <webElementGuid>f9999f78-4d41-462f-95ae-38607d6a6fe0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Test Center Administration</value>
      <webElementGuid>3a76cc7b-dab9-417d-8995-3a19c4ef191c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navigation-items-section&quot;)/button[@id=&quot;3&quot;]</value>
      <webElementGuid>ea23d450-ad70-4ac0-8673-cee4b8153b8f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@id='3'])[2]</value>
      <webElementGuid>b1d54415-3da9-4f5a-91cc-98c119da3d71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navigation-items-section']/button[3]</value>
      <webElementGuid>4d90a00f-f3f7-44ec-9bed-90e8c00a250e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Item Bank Administration'])[1]/following::button[1]</value>
      <webElementGuid>5eb99c37-f0a5-4f9f-be77-318d04a9d760</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rights and Permissions'])[1]/following::button[2]</value>
      <webElementGuid>d9474f1e-1ea3-4079-b531-fcc08b4a933c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Access Management'])[1]/preceding::button[1]</value>
      <webElementGuid>84848a08-7734-45cc-a0b6-202c9f3f2358</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test Access Codes'])[1]/preceding::button[2]</value>
      <webElementGuid>28505fee-79d0-4d67-b236-160e0d56be7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Test Center Administration']/parent::*</value>
      <webElementGuid>d5bb8558-651f-4b01-b837-5ec298799ba3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[3]</value>
      <webElementGuid>268c03ea-7e14-4cc7-8874-102e9d436604</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = '3' and (text() = 'Test Center Administration' or . = 'Test Center Administration')]</value>
      <webElementGuid>f385241d-8aa7-4ce8-8a32-162e39091332</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
