<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Start Test</name>
   <tag></tag>
   <elementGuidId>2fed6a0d-89b7-43c8-aed1-8812c6cc307a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#unit-test-submit-btn</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='unit-test-submit-btn']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>7e4c5a05-8338-410a-945b-ed8ff64626b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>unit-test-submit-btn</value>
      <webElementGuid>403506dc-ede4-4b81-a8ba-5c1858d139b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>success undefined</value>
      <webElementGuid>44042471-ae39-494a-85d0-0a85297fa9b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Start Test</value>
      <webElementGuid>e314af45-b4e2-4219-abaf-552a4849fe40</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-disabled</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>ec7e7494-3187-45cc-9943-5b957b5cce8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Start Test</value>
      <webElementGuid>7a61bc8e-e5a2-4e4c-8908-95e288dd8034</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;unit-test-submit-btn&quot;)</value>
      <webElementGuid>956e9e66-2ba4-46c0-bad0-316a6501b581</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='unit-test-submit-btn']</value>
      <webElementGuid>2a0f70c1-db4d-4583-a2dc-36a5ec78b509</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='unit-test-submit-btn-container']/button</value>
      <webElementGuid>e8b36e65-79da-4ff1-ab1e-70e2c213952e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Instructions'])[2]/following::button[1]</value>
      <webElementGuid>1e238d8b-888f-46e2-854c-b18d29b647cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terms and conditions'])[1]/following::button[1]</value>
      <webElementGuid>4c6636d5-d895-495d-8978-813e6399e995</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'unit-test-submit-btn', '&quot;', ')')])[1]/preceding::button[1]</value>
      <webElementGuid>ab5d38a9-2eee-45d2-8f7e-e846818592aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/nav/div/div/div/button</value>
      <webElementGuid>47235b10-949e-40cd-bc3f-a6b0b89e951f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'unit-test-submit-btn' and (text() = 'Start Test' or . = 'Start Test')]</value>
      <webElementGuid>995bfa62-1a15-48ed-9cfc-5eb6e6d7611f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
