<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Quit Test</name>
   <tag></tag>
   <elementGuidId>2e767b7b-f170-4292-b38b-08cf734a5f1c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='basic-navbar-nav']/div/div[2]/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div > button.primary-in-test.undefined</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>18fe11d3-d90a-4ba0-aa55-9d04f3d763ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>primary-in-test undefined</value>
      <webElementGuid>92d65d33-3d0e-41b2-9c74-36876f463b4d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Quit Test</value>
      <webElementGuid>23af5c53-775b-4201-9bd6-e526d41a40da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Quit Test</value>
      <webElementGuid>f1c4c887-42d3-4588-bc7e-38c08da27fbe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;basic-navbar-nav&quot;)/div[@class=&quot;justify-content-end w-100 row&quot;]/div[2]/div[1]/button[@class=&quot;primary-in-test undefined&quot;]</value>
      <webElementGuid>9fc80489-aa72-4b7f-8f44-aa7dde302e1c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='basic-navbar-nav']/div/div[2]/div/button</value>
      <webElementGuid>b48a82f5-2574-4007-9a79-90d22d2e4cc5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to main content'])[1]/following::button[3]</value>
      <webElementGuid>66105f45-0ed4-4528-b6c3-a6608ea5864f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tools'])[1]/preceding::button[1]</value>
      <webElementGuid>e5338d79-4d2f-4849-8cf9-7f5fe51c3223</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/button</value>
      <webElementGuid>b762e76a-1fb6-4154-a7a8-1c0e4f418dfd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Quit Test' or . = 'Quit Test')]</value>
      <webElementGuid>325f3de9-a926-4c5b-914c-7491760a1311</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
