<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Generate Test Access Code</name>
   <tag></tag>
   <elementGuidId>b6f8f310-91e8-41f8-8097-8bf2126ccb58</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\t\n  \t\&quot;test_order_number\&quot; : \&quot;${test_order_number}\&quot;,\n\t\&quot;test_session_language\&quot; : \&quot;${test_session_language}\&quot;,\n\t\&quot;test_id\&quot; : \&quot;${test_id}\&quot;,\n\t\&quot;username\&quot; : \&quot;${username}\&quot;,\n\t\&quot;password\&quot; : \&quot;${password}\&quot;,\n\t\&quot;orderless_request\&quot; : ${orderless_request}\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>cdb7f32d-208e-432c-b2e3-1b745bbb0129</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>JWT ${authorization}</value>
      <webElementGuid>c15810c4-bbaf-4dc1-90fc-5271507d6615</webElementGuid>
   </httpHeaderProperties>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${THUNDERCAT_URL}/api/get-new-test-access-code/</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>8caf04ea-cb04-4eb1-a0c7-1900044f4953</id>
      <masked>false</masked>
      <name>THUNDERCAT_URL</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>906cb96c-ea5e-4a2f-9765-59c2667dfabf</id>
      <masked>false</masked>
      <name>username</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>f335921e-e469-4344-9763-92f1169b8cb5</id>
      <masked>false</masked>
      <name>password</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>8956a660-a531-4b2d-9e40-7a6574e2c995</id>
      <masked>false</masked>
      <name>authorization</name>
   </variables>
   <variables>
      <defaultValue>'KATALON'</defaultValue>
      <description></description>
      <id>880b58a2-b32e-4697-b2cc-e15a172f61f7</id>
      <masked>false</masked>
      <name>test_order_number</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>ed8cf6f0-2ca5-4002-957f-834839a4d4fd</id>
      <masked>false</masked>
      <name>test_id</name>
   </variables>
   <variables>
      <defaultValue>'1'</defaultValue>
      <description></description>
      <id>d00c1554-b198-45d6-b5b0-647652780b8f</id>
      <masked>false</masked>
      <name>test_session_language</name>
   </variables>
   <variables>
      <defaultValue>false</defaultValue>
      <description></description>
      <id>f9527b27-4211-4a8f-9e95-51cbf4b81917</id>
      <masked>false</masked>
      <name>orderless_request</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
