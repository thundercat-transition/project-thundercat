# Deployment

## Preparing New Release Branch

1.  Check-in the "develop" branch and make sure that you are up to date.
2.  Generate the new DB Image using this script: [generate-cat-mssql-image-for-release.sh](./generate-cat-mssql-image-for-release.sh)<br> - reference: [Release versioning](./docs/Docker%20Images/scripts.md#Release-Versioning)
3.  Save the package.json file (for formatting purposes)
4.  Create a new branch.
5.  Commit and push your changes.
6.  Create a new PR to merge that branch into "develop".
7.  Merge your changes once approved properly.
8.  Create a new branch from GitLab based on "develop" called "Release-X.X.X".
9.  Once created, go protect your new branch (Gitlab - Settings - Repository - Protected Branches - Add Protected Branch)<br> - Allowed to merge: **No one**<br> - ALlowed to push and merge: **No one**
