# Database Documentation

## Server Information

| Environment | IP                       | Port | Database Name   | Username / Password        |
| ----------- | ------------------------ | ---- | --------------- | -------------------------- |
| LOCAL       | localhost (or 127.0.0.1) | 1433 | Master          | SA / someSecurePassword10! |
| DEV         | sqldevapps\sqldevapps    | 9593 | CAT             | LDAP Username / Password   |
| TEST        | sqltestapps\sqltestapps  | 9593 | CAT             | LDAP Username / Password   |
| PROD        | sqlprodapps\sqlprodapps  | 9593 | ApplicationData | LDAP Username / Password   |

<br>

## How to Connect to the Databases

### In Visual Studio Code

1. Open Visual Studio Code
2. On the left side menu, click on "SQL Server"
3. On the right of "Connections", click on the +
4. A popup will ask you for:
   - Hostname: Put the <b>IP:Port</b>; example: 192.168.7.146:1433
   - Database to connect: Put the <b>Database Name</b>
   - User name: Put your <b>ID</b>; usually, you would put your LDAP username
   - Password: Put your <b>Password</b>; usually, you would put your LDAP password
5. You should now be connected! Try a query by right clicking on the database + New Query

### In Microsoft SQL Server Management Studio

1. Open Microsoft SQL Server Management Studio
2. On the left side menu, click on "Connect Object Explorer"
3. A popup will ask you for:
   - Server Type: <b>Database Engine</b>
   - Server Name: <b>IP,Port</b>; example: sqldevapps\sqldevapps,9593
   - Authentication: <b>Windows Authentication</b>; it should detect your LDAP user
