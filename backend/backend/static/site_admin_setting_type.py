# This file is a collection of the valid status options (SiteAdminSetting Model).
# mirror of SiteAdminSettingType under ...\frontend\src\components\superuser\Constants.jsx


# ------------------------------------------------------------------------------ #
# This Static Class is being used to identify the Site Admin Settings' Codenames
# ------------------------------------------------------------------------------ #
class SiteAdminSettingType:
    LOCK_LOGIN = "locklogin"
    ENABLE_QUALITY_OF_LIFE = "enablequalityoflife"
    ENABLE_GC_NOTIFY = "enablegcnotify"
    ENABLE_MS_TEAMS_API = "enablemsteamsapi"
    ENABLE_WEBEX = "enablewebex"


class Key:
    GC_NOTIFY = "gcnotify"
    MS_TEAMS_API_APP_ID = "msteamsapiappid"
    MS_TEAMS_API_TENANT_ID = "msteamsapitenantid"
    MS_TEAMS_API_SECRET = "msteamsapisecret"
