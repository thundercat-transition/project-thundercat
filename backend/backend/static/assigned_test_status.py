# This file is a collection of all possilble candidate testing process test statuses.
from backend.custom_models.assigned_test_status import (
    AssignedTestStatus as AssignedTestStatusModel,
)


class AssignedTestStatus:
    # if you update these statuses, don't forget to also update ".../components/ta/Constants.jsx" file (frontend)
    ASSIGNED = "assigned"  # Assigned to a Candidate (deprecated)
    CHECKED_IN = "checked_in"  # Checked-in and ready to start the test
    UNASSIGNED = "unassigned"  # un-assigned by TA (user never got access to the test, meaning that he was never able to see the content)
    PRE_TEST = "pre_test"  # Pre-Test (instructions)"
    ACTIVE = "active"  # Testing (timed sections)
    TRANSITION = "transition"  # in transition after completing a timed section
    LOCKED = "locked"  # Test Locked
    PAUSED = "paused"  # Test Paused
    SUBMITTED = "submitted"  # Submitted
    QUIT = "quit"  # User Quit


def get_assigned_test_status_id(codename):
    try:
        return AssignedTestStatusModel.objects.get(codename=codename).id
    except:
        return "INVALID CODENAME"


def get_assigned_test_status_codename(status_id):
    try:
        return AssignedTestStatusModel.objects.get(id=status_id).codename
    except:
        return "INVALID STATUS ID"


# Util for Printing AssignedTestStatus - String
def get_string_assigned_test_status(status_id):
    status_codename = AssignedTestStatusModel.objects.get(id=status_id).codename
    if status_codename == AssignedTestStatus.ASSIGNED:
        return "AssignedTestStatus.ASSIGNED"
    elif status_codename == AssignedTestStatus.CHECKED_IN:
        return "AssignedTestStatus.CHECKED_IN"
    elif status_codename == AssignedTestStatus.ACTIVE:
        return "AssignedTestStatus.ACTIVE"
    elif status_codename == AssignedTestStatus.LOCKED:
        return "AssignedTestStatus.LOCKED"
    elif status_codename == AssignedTestStatus.PAUSED:
        return "AssignedTestStatus.PAUSED"
    elif status_codename == AssignedTestStatus.QUIT:
        return "AssignedTestStatus.QUIT"
    elif status_codename == AssignedTestStatus.SUBMITTED:
        return "AssignedTestStatus.SUBMITTED"
    elif status_codename == AssignedTestStatus.UNASSIGNED:
        return "AssignedTestStatus.UNASSIGNED"
    elif status_codename == AssignedTestStatus.PRE_TEST:
        return "AssignedTestStatus.PRE_TEST"
