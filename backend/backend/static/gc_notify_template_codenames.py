# This file is a collection of the valid template names. It is not cloned in the frontend


# ------------------------------------------------------------------------------ #
# This Static Class is being used to identify the gc notify template Codenames
# ------------------------------------------------------------------------------ #
class GCNotifyTemplateCodenames:
    TWO_FACTOR_AUTHENTICATION = "2fa"
    UIT_INVITE_BASIC = "uitinvitebasic"
    RESERVATION_INVITE = "reservationinvite"
    SHARE_RESULT = "shareresult"
    FAILED_SUP_DELIVERY = "failedsupdelivery"
    FAILED_UIT_DELIVERY = "faileduitdelivery"
    RESEND_ALREADY_ASSIGNED_TEST_TO_ADMINISTER = "resendalreadyassignedtesttoadminister"
    SEND_NEW_ASSIGNED_TEST_TO_ADMINISTER = "sendnewassignedtesttoadminister"
    FAILED_SEND_ASSIGNED_TEST_TO_ADMINISTER_DELIVERY = (
        "failedsendassignedtesttoadministerdelivery"
    )
    REVOKE_ASSIGNED_TEST_TO_ADMINISTER = "revokeassignedtesttoadminister"
    UPDATE_ACTIVE_ASSESSMENT_PROCESS_DATA = "updateactiveassessmentprocessdata"
    TEST_SESSION_CANCELLATION_BASED_ON_ASSESSMENT_PROCESS_DATA_UPDATES = (
        "testsessioncancellationbasedonassessmentprocessdataupdates"
    )
    TEST_SESSION_CANCELLATION_INFORM_RELATED_HR_COORDINATOR = (
        "testsessioncancellationinformrelatedhrcoordinator"
    )
    PASSWORD_RESET = "passwordreset"
    INVITE_CANDIDATE_TO_NON_STANDARD_TEST_SESSION = (
        "invitecandidatetononstandardtestsession"
    )
    UPDATE_NON_STANDARD_TEST_SESSION_INVITE = "updatenonstandardtestsessioninvite"
    DELETE_NON_STANDARD_TEST_SESSION = "deletenonstandardtestsession"
    DELETE_SPECIFIC_TEST_SESSION = "deletespecifictestsession"
    RESERVATION_CODE_WITHDRAW_NO_BOOKING = "reservationcodewithdrawnobooking"
    RESERVATION_CODE_WITHDRAW_FROM_BOOKED_SESSION = (
        "reservationcodewithdrawfrombookedsession"
    )
    RESERVATION_CODE_BOOKING = "reservationcodebooking"
    RESERVATION_CODE_BOOK_LATER = "reservationcodebooklater"
    TEST_SESSION_CANDIDATES_TO_SECURITY = "testsessioncandidatestosecurity"
    MODIFY_UIT_VALIDITY_END_DATE = "modifyuitvalidityenddate"
    DEACTIVATE_UIT_TEST_CANCELLATION = "deactivateuittestcancellation"
    FAILED_SEND_TEST_SESSION_CANDIDATES_TO_SECURITY = (
        "failedsendtestsessioncandidatestosecurity"
    )
    NEW_ACCOMODATIONS_REQUEST_HR = "newaccomodationsrequesthr"
    CANCELED_ACCOMODATIONS_REQUEST = "canceledaccomodationsrequest"
    CANCELED_ACCOMODATIONS_REQUEST_CANDIDATE = "canceledaccomodationsrequestcandidate"
    COMPLETED_ACCOMODATIONS_REQUEST = "completedaccomodationsrequest"
    RE_OPENED_ACCOMODATIONS_REQUEST = "reopenedaccomodationsrequest"
    PENDING_APPROVAL_ACCOMODATIONS_REQUEST = "pendingapprovalaccomodationsrequest"
    EXPIRED_ACCOMMODATION_REQUEST = "expiredaccommodationrequest"
    TWO_DAY_REMINDER = "twodayreminder"
    SEVEN_DAY_REMINDER = "sevendayreminder"
    CANDIDATE_WITHDRAWL_NOTIFICATION = "candidatewithdrawlnotification"
    HR_TRIGGERED_WITHDRAW = "hrtriggeredwithdraw"
    TEST_BUMPED_BY_TC = "testbumpedbytc"
    OLA_CODE_BOOKING = "olacodebooking"
    OLA_CODE_BOOK_LATER = "olacodebooklater"
    OLA_INVITE_CANDIDATE_TO_NON_STANDARD_TEST_SESSION_IN_PERSON = (
        "olainvitecandidatetononstandardtestsessioninperson"
    )
    OLA_INVITE_CANDIDATE_TO_NON_STANDARD_TEST_SESSION_VIRTUAL = (
        "olainvitecandidatetononstandardtestsessionvirtual"
    )
    OLA_UPDATE_NON_STANDARD_TEST_SESSION_INVITE_IN_PERSON = (
        "olaupdatenonstandardtestsessioninviteinperson"
    )
    OLA_UPDATE_NON_STANDARD_TEST_SESSION_INVITE_VIRTUAL = (
        "olaupdatenonstandardtestsessioninvitevirtual"
    )
