# This file is a collection of the valid question type options (NewQuestionType Model).


# ------------------------------------------------------------------------------ #
# This Static Class is being used to identify the Test Skill Types' Codenames
# ------------------------------------------------------------------------------ #
class TestSkillTypeCodename:
    NONE = "none"
    SLE = "sle"
    OCC = "occupational"


# ------------------------------------------------------------------------------ #
# This Static Class is being used to identify the Test Skill SLE Descs' Codenames
# ------------------------------------------------------------------------------ #
class TestSkillSLEDescCodename:
    READING_EN = "re"
    READING_FR = "rf"
    WRITING_EN = "we"
    WRITING_FR = "wf"
    ORAL_EN = "oe"
    ORAL_FR = "of"


# ------------------------------------------------------------------------------ #
# This Static Class is being used to identify the Test Skill Occupational Descs' Codenames
# ------------------------------------------------------------------------------ #
class TestSkillOccupationalDescCodename:
    TEST_OF_JUDGEMENT = "judgement"
    GENERAL_COMPETENCY_TEST_LEVEL_1 = "competency_test_lvl_1"
    GENERAL_COMPETENCY_TEST_LEVEL_2 = "competency_test_lvl_2"
