# This file is a collection of the valid recommendations options (UserAccommodationFileRecommendationsOption Model).
# mirror of OLA_GLOBAL_CONFIGS_CONST under ...\frontend\src\components\etta\ola_configs\Constants.jsx


# ------------------------------------------------------------------------------ #
# This Static Class is being used to identify the Ola Global Configs Codenames
# ------------------------------------------------------------------------------ #
class OlaGlobalConfigsConst:
    CANCELLATION_WINDOW = "cancellation_window"
    OPEN_BOOKING_WINDOW = "open_booking_window"
