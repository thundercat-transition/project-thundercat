# This file is a collection of the valid status options (ConsumedReservationCodeStatus Model).
# mirror of BreakBankActionsConstants under ...\frontend\src\components\candidate\Constants.jsx


# ------------------------------------------------------------------------------ #
# This Static Class is being used to identify the Consumed Reservation Code Status' Codenames
# ------------------------------------------------------------------------------ #
class StaticConsumedReservationCodeStatus:
    # reservation code is associated to a test session (displayed in Manage Reservations table)
    BOOKED = "booked"
    # candidate withdrawn from reservation code (no longer displayed in any tables)
    WITHDRAWN = "withdrawn"
    # candidate never used their reservation code, so it has been expired by the system (no longer displayed in any tables)
    EXPIRED = "expired"
    # candidate consumed their reservation code (displayed in My Reservation Codes table)
    RESERVED = "reserved"
    # reservation code has been revoked by an HR Coordinator
    REVOKED = "revoked"
    # candidate consumed their reservation code & requested an accomodation (displayed in Manage Reservations table)
    REQUESTED_ACCOMMODATION = "requested_acco"
    # candidate consumed their reservation code & is now paired with an OLA Test Assessor (displayed in Manage Reservations table)
    PAIRED_OLA = "paired_ola"
