# This file is a collection of all possilble email answer operations


class UpdateEmailAnswerType:
    # if you update these statuses, don't forget to also update
    # "UPDATE_EMAIL_ACTIONS" in ".../modules/UpdateResponseRedux.js" file (frontend)
    ADD_EMAIL = 1
    UPDATE_EMAIL = 2
    DELETE_EMAIL = 3
    ADD_TASK = 4
    UPDATE_TASK = 5
    DELETE_TASK = 6
