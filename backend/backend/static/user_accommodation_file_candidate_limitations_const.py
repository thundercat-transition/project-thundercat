# This file is a collection of the valid candidate limitations options (UserAccommodationFileCandidateLimitationsOption Model).
# mirror of USER_ACCOMMODATION_FILE_CANDIDATE_LIMITATIONS_CONST under ...\frontend\src\components\aae\Constants.jsx


# ------------------------------------------------------------------------------ #
# This Static Class is being used to identify the User Accommodation File Candidate Limitations Codenames
# ------------------------------------------------------------------------------ #
class UserAccommodationFileCandidateLimitationsConst:
    ATTENTION_DISORDERS = "attention_disorders"
    CHRONIC_MEDICAL_DISABILITIES = "chronic_medical_disabilities"
    HEARING_DISABILITIES = "hearing_disabilities"
    LEARNING_DISABILITIES = "learning_disabilities"
    MOBILITY_PHYSICAL_DISABILITIES = "mobility_physical_disabilities"
    NEUROLOGICAL_DISORDERS = "neurological_disorders"
    PSYCHIATRIC_PSYCHOLOGICAL_EMOTIONAL_DISABILITIES = (
        "psychiatric_psychological_emotional_disabilities"
    )
    SPEECH_AND_LANGUAGE_DISABILITIES = "speech_and_language_disabilities"
    TEMPORARY_CONDITIONS_AND_OTHER = "temporary_conditions_and_other"
    VISUAL_CONDITION = "visual_condition"
