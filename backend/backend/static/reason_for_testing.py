# partial copy of that file: ...\frontend\src\components\commons\Constants.jsx
class ReasonForTestingTypeCodenameConst:
    READING = "reading"
    WRITING = "writing"
    ORAL = "oral"


class ReasonForTestingCodenameConst:
    # Reading/Writing SLEs
    IMP_STAFFING = "imp_staffing"
    NON_IMP_STAFFING = "non_imp_staffing"
    FT_LANGUAGE_TRAINING_FROM_PSC = "ft_language_training_from_psc"
    PT_LANGUAGE_TRAINING_FROM_PSC = "pt_language_training_from_psc"
    FT_LANGUAGE_TRAINING_FROM_DEP_PROG = "ft_language_training_from_dep_prog"
    PT_LANGUAGE_TRAINING_FROM_DEP_PROG = "pt_language_training_from_dep_prog"
    FT_LANGUAGE_TRAINING_FROM_PRIVATE = "ft_language_training_from_private"
    PT_LANGUAGE_TRAINING_FROM_PRIVATE = "pt_language_training_from_private"
    RE_IDENTIFICATION = "re_identification"
    BILINGUALISM_BONUS = "bilingualism_bonus"
    RECORD_OR_OTHER_PURPOSES = "record_or_other_purposes"
    COVID_19_OR_ESSENTIAL_SERVICE = "covid_19_or_essential_service"
    STAFFING_DOTATION = "staffing_dotation"
    LANGUAGE_TRAINING = "language_training"
    OTHER = "other"
    # OLA SLEs
    IMP_STAFFING_OLA = "imp_staffing_ola"
    LANGUAGE_TRAINING_OLA = "language_training_ola"
    CREATION_OF_PARTIALLY_ASSESSED_POOL_OLA = "creation_of_partially_assessed_pool"
    NON_IMP_STAFFING_OLA = "non_imp_staffing_ola"
    RE_IDENTIFICATION_OLA = "re_identification_ola"
    RECORD_OR_OTHER_PURPOSES_OLA = "record_or_other_purposes_ola"
    BILINGUALISM_BONUS_OLA = "bilingualism_bonus_ola"


class ReasonForTestingPriorityCodenameConst:
    HIGH = "high"
    MEDIUM = "medium"
    LOW = "low"
