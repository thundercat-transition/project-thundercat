# This file is a collection of the valid recommendations options (UserAccommodationFileRecommendationsOption Model).
# mirror of USER_ACCOMMODATION_FILE_RECOMMENDATIONS_CONST under ...\frontend\src\components\aae\Constants.jsx


# ------------------------------------------------------------------------------ #
# This Static Class is being used to identify the User Accommodation File Recommendations Codenames
# ------------------------------------------------------------------------------ #
class UserAccommodationFileRecommendationsConst:
    ADAPTED_SCORING = "adapted_scoring"
    ADAPTIVE_TECHNOLOGY = "adaptive_technology"
    ASSESSOR_OR_TEST_ADMINISTRATOR = "assessor_or_test_administrator"
    COMMUNICATION_SUPPORT = "communication_support"
    ENVIRONMENT = "environment"
    EXTRA_TIME = "extra_time"
    FORMATS = "formats"
    INDIVIDUAL_TEST_SESSION = "individual_test_session"
    MATERIAL = "material"
    PREPARATION_TO_TEST = "preparation_to_test"
    SCHEDULING = "scheduling"
    SLE_UIT_LEVELING = "sle_uit_leveling"
    BREAKS = "breaks"
    REPETITION = "repetition"
