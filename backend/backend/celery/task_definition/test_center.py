from __future__ import absolute_import, unicode_literals
import uuid
from datetime import datetime, date
from email.mime.base import MIMEBase
from django.core.mail import EmailMessage
from django.conf import settings
from django.utils import timezone
from icalendar import Calendar, Event, vCalAddress, vText, vInt, vBoolean, vDDDTypes
from backend.celery.task_definition.utils import ConsoleMessageColor


class TestSessionEmailAction:
    ADD = "ADD"
    UPDATE = "UPDATE"
    DELETE = "DELETE"


def handle_create_test_session_email(test_session_data):
    from backend.custom_models.test_center_test_session_meeting_uuid import (
        TestCenterTestSessionMeetingUuid,
    )

    # initializing the calendar
    cal = Calendar()

    # ADD action
    if test_session_data[0]["action"] == TestSessionEmailAction.ADD:
        # generating UUID
        generated_uuid = vText(uuid.uuid4())
        # creating new entry in TestCenterTestSessionMeetingUuid table
        TestCenterTestSessionMeetingUuid.objects.create(
            uuid=generated_uuid, test_session_id=test_session_data[0]["test_session_id"]
        )
        # setting status to CONFIRMED
        status = vText("CONFIRMED")

        # test skills are provided
        if (
            "test_skill_type_en_name" in test_session_data[0]
            and "test_skill_sub_type_en_name" in test_session_data[0]
        ):
            # getting needed variable values
            test_skill_en = test_session_data[0]["test_skill_type_en_name"]
            test_skill_fr = test_session_data[0]["test_skill_type_fr_name"]
            test_sub_skill_en = test_session_data[0]["test_skill_sub_type_en_name"]
            test_sub_skill_fr = test_session_data[0]["test_skill_sub_type_fr_name"]

            # formatting test name / test skills
            formatted_test_en = "{0} - {1}".format(test_skill_en, test_sub_skill_en)
            formatted_test_fr = "{0} - {1}".format(test_skill_fr, test_sub_skill_fr)
        # test skills are not provided (meaning that there is an associated test)
        else:
            # getting needed variable values
            test_name_en = test_session_data[0]["test_name_en"]
            test_name_fr = test_session_data[0]["test_name_fr"]
            test_version = ""
            if "test_version" in test_session_data[0]:
                test_version = test_session_data[0]["test_version"]

            # formatting test name / test skills
            formatted_test_en = "{0} - v{1}".format(test_name_en, test_version)
            formatted_test_fr = "{0} - v{1}".format(test_name_fr, test_version)

        # creating email body
        # Email Subject
        email_subject = """{formatted_test_fr} / {formatted_test_en}""".format(
            formatted_test_en=formatted_test_en,
            formatted_test_fr=formatted_test_fr,
        )

        # Email Body
        email_body = """
            (English message follows)
            \n
            Cette salle est réservée pour l’administration du test suivant sur l’Outil d’évaluation des candidats.
            \n
            Salle : {room_name}
            Date du test : {date}
            Heure de début : {simplified_start_time}
            Heure de fin : {simplified_end_time}
            Test : {formatted_test_fr}
            \n
            —
            \n
            (Le français précède)
            \n
            This room is reserved for the administration of the following test on the Candidate Assessment tool.
            \n
            Room: {room_name}
            Test Date: {date}
            Test start time: {simplified_start_time}
            Test end time: {simplified_end_time}
            Test: {formatted_test_en}
            """.format(
            room_name=test_session_data[0]["room_name"],
            date=test_session_data[0]["date"],
            simplified_start_time=test_session_data[0]["simplified_start_time"],
            simplified_end_time=test_session_data[0]["simplified_end_time"],
            formatted_test_en=formatted_test_en,
            formatted_test_fr=formatted_test_fr,
        )
    # UPDATE action
    elif test_session_data[0]["action"] == TestSessionEmailAction.UPDATE:
        # getting respective UUID
        generated_uuid = TestCenterTestSessionMeetingUuid.objects.get(
            test_session_id=test_session_data[0]["test_session_id"]
        ).uuid
        # setting status to CONFIRMED
        status = vText("CONFIRMED")

        # creating email body
        # TODO: put official wording
        email_subject = "FR Meeting invitation / Meeting invitation"

        # TODO: put official wording
        email_body = "FR Email Body / Email Body (UPDATE)"
    # DELETE action
    elif test_session_data[0]["action"] == TestSessionEmailAction.DELETE:
        # getting respective UUID
        uuid_data = TestCenterTestSessionMeetingUuid.objects.get(
            test_session_id=test_session_data[0]["test_session_id"]
        )
        generated_uuid = uuid_data.uuid
        uuid_data.delete()
        # setting status to CANCELLED
        status = vText("CANCELLED")

        # creating email body
        # TODO: put official wording
        email_subject = "FR Meeting invitation / Meeting invitation"

        # TODO: put official wording
        email_body = "FR Email Body / Email Body (DELETE)"
    # unsupported action
    else:
        raise Exception("UNSUPPORTED ACTION")

    # adding cal and event parameters/settings
    cal.add("prodid", vText("-//OEC-CAT Calendar Event//"))
    cal.add("version", vText("2.0"))
    cal.add("method", vText("REQUEST"))
    cal.add("X-MS-OLK-FORCEINSPECTOROPEN", vBoolean(True))

    event = Event()
    event.add("method", vText("REQUEST"))
    event.add("class", vText("PUBLIC"))
    event.add("transp", vText("OPAQUE"))
    event.add("uid", generated_uuid)
    event.add("X-MICROSOFT-CDO-BUSYSTATUS", vText("BUSY"))
    event.add("X-MICROSOFT-CDO-IMPORTANCE", vInt(1))
    event.add("X-MICROSOFT-DISALLOW-COUNTER", vBoolean(False))
    event.add("X-MS-OLK-APPTSEQTIME", vDDDTypes(datetime.now()))
    event.add("X-MS-OLK-AUTOFILLLOCATION", vBoolean(False))
    event.add("X-MS-OLK-CONFTYPE", vInt(0))

    event.add("priority", vInt(5))
    event.add("sequence", vInt(0))
    event.add("name", vText("Awesome Meeting"))
    event.add("dtstart", test_session_data[0]["start_time"])
    event.add("dtend", test_session_data[0]["end_time"])
    event.add("status", status)

    # Add the organizer
    organizer = vCalAddress("MAILTO:{0}".format(settings.EMAIL_HOST_USER))
    organizer.params["name"] = vText("OEC/CAT")
    organizer.params["role"] = vText("Système/System")
    event["organizer"] = organizer

    # Add the attendee
    attendee = vCalAddress("MAILTO:{0}".format(test_session_data[0]["email"]))
    attendee.params["name"] = vText("{0}".format(test_session_data[0]["email"]))
    # TODO: put official wording
    attendee.params["role"] = vText("FR CAT Candidate/CAT Candidate")
    attendee.params["RSVP"] = vText("FALSE")
    event.add("attendee", attendee, encode=0)

    # Add the event to the calendar
    cal.add_component(event)

    # generate meeting invite (ics) file
    filename = "invite.ics"
    part = MIMEBase("text", "calendar", method="REQUEST", name=filename)
    part.set_payload(cal.to_ical())
    part.add_header("Content-Description", filename)
    part.add_header("Content-class", "urn:content-classes:calendarmessage")
    part.add_header("Filename", filename)
    part.add_header("Path", filename)

    # configure email to send (title, body, etc)
    email = EmailMessage(
        email_subject,
        email_body,
        settings.EMAIL_HOST_USER,
        [
            test_session_data[0]["email"],
        ],
    )
    # attach ics file to email
    email.attach(part)
    # sending email
    email.send()


def handle_test_officers_and_test_access_codes_deletion(test_session_id):
    from backend.custom_models.test_access_code_model import (
        TestAccessCode,
    )
    from backend.custom_models.test_center_test_session_officers import (
        TestCenterTestSessionOfficers,
    )

    # deleting all related test session officers
    test_center_officers = TestCenterTestSessionOfficers.objects.filter(
        test_session_id=test_session_id
    )
    for test_center_officer in test_center_officers:
        temp_test_center_officer_id = test_center_officer.id
        test_center_officer.delete()
        # printing deletion successful messasge
        print(
            "{0} \t--> Test Center Test Session Officer ID: {1} deleted successfully".format(
                ConsoleMessageColor.WHITE, temp_test_center_officer_id
            )
        )

    # deleting all related test access codes
    test_access_codes = TestAccessCode.objects.filter(test_session_id=test_session_id)
    for test_access_code in test_access_codes:
        temp_test_access_code = test_access_code.test_access_code
        test_access_code.delete()
        # printing deletion successful messasge
        print(
            "{0} \t--> Test Access Code: {1} deleted successfully".format(
                ConsoleMessageColor.WHITE, temp_test_access_code
            )
        )


def handle_past_test_sessions():
    from backend.custom_models.test_center_test_sessions import (
        TestCenterTestSessions,
    )
    from backend.custom_models.test_center_test_session_data import (
        TestCenterTestSessionData,
    )
    from backend.custom_models.test_center_test_session_data import (
        TestCenterTestSessionData,
    )
    from backend.custom_models.test_access_code_model import (
        TestAccessCode,
    )
    from backend.custom_models.test_center_test_session_officers import (
        TestCenterTestSessionOfficers,
    )
    from backend.custom_models.virtual_teams_meeting_session import (
        VirtualTeamsMeetingSession,
    )
    from backend.custom_models.user_accommodation_file_status import (
        UserAccommodationFileStatus,
    )
    from backend.static.user_accommodation_file_status_const import (
        UserAccommodationFileStatusConst,
    )
    from backend.custom_models.user_accommodation_file import UserAccommodationFile

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: HANDLE PAST TEST SESSIONS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )
    # getting current datetime
    now = timezone.now()
    # getting all past test sessions
    past_test_session_data = TestCenterTestSessionData.objects.filter(end_time__lt=now)
    # looping in past_test_session_data
    for test_session_data in past_test_session_data:
        # initializing test_session_id
        test_session_id = None

        # initializing skip_rest_of_logic
        skip_rest_of_logic = False
        try:
            # getting related test session
            test_session = TestCenterTestSessions.objects.get(
                test_session_data_id=test_session_data.id
            )

            # delete virtual meeting session if it exists; fixes bug when this celery task runs
            virtual_meeting_sessions = VirtualTeamsMeetingSession.objects.filter(
                test_session_id=test_session.id,
            )

            for session in virtual_meeting_sessions:
                session.delete()

            # if accommodation request (non-standard test session)
            if test_session.is_standard == 0:
                # getting related test sessions based on user_accommodation_file_id (ordering by test_session_order)
                related_test_sessions = TestCenterTestSessions.objects.filter(
                    user_accommodation_file_id=test_session.user_accommodation_file_id
                ).order_by("test_session_order")

                # current test session is the last one of its related sub-sessions
                if test_session.id == related_test_sessions.last().id:
                    # looping in related_test_sessions
                    for related_test_session in related_test_sessions:
                        # deleting related test officers and test access codes
                        handle_test_officers_and_test_access_codes_deletion(
                            related_test_session.id
                        )

                        # deleting related test session
                        related_test_session_id = related_test_session.id
                        related_test_session.delete()

                        # getting related test session data
                        related_test_session_data = (
                            TestCenterTestSessionData.objects.get(
                                id=related_test_session.test_session_data_id
                            )
                        )

                        # deleting respective test session data
                        related_test_session_data_id = related_test_session_data.id
                        related_test_session_data.delete()

                        # updating user accommodation file status to ADMINISTERED
                        administered_status_id = (
                            UserAccommodationFileStatus.objects.get(
                                codename=UserAccommodationFileStatusConst.ADMINISTERED
                            ).id
                        )
                        respective_user_accommodation_file = (
                            UserAccommodationFile.objects.get(
                                id=test_session.user_accommodation_file_id
                            )
                        )
                        respective_user_accommodation_file.status_id = (
                            administered_status_id
                        )
                        respective_user_accommodation_file.save()

                        # printing successful messasge
                        print(
                            "{0} ==================== TEST SESSION ID: {1} & TEST SESSION DATA ID: {2} DELETED SUCCESSFULLY ====================".format(
                                ConsoleMessageColor.CYAN,
                                related_test_session_id,
                                related_test_session_data_id,
                            )
                        )
                        # skipping the rest of the iteration logic (by setting skip_rest_of_logic to True)
                        skip_rest_of_logic = True
                # current test session is NOT the last one of its related sub-sessions
                else:
                    # skipping this iteration
                    continue

            # skip_rest_of_logic is set to False
            if not skip_rest_of_logic:
                # deleting related test officers and test access codes
                handle_test_officers_and_test_access_codes_deletion(test_session.id)

                # deleting related test session
                test_session_id = test_session.id
                test_session.delete()
        except TestCenterTestSessions.DoesNotExist:
            pass

        # skip_rest_of_logic is set to False
        if not skip_rest_of_logic:
            # deleting respective test session data
            test_session_data_id = test_session_data.id
            test_session_data.delete()
            # printing successful messasge
            print(
                "{0} ==================== TEST SESSION ID: {1} & TEST SESSION DATA ID: {2} DELETED SUCCESSFULLY ====================".format(
                    ConsoleMessageColor.CYAN, test_session_id, test_session_data_id
                )
            )


def handle_past_test_center_ola_vacation_blocks():
    from backend.custom_models.test_center_ola_vacation_block import (
        TestCenterOlaVacationBlock,
    )
    from backend.custom_models.test_center_ola_vacation_block_availability import (
        TestCenterOlaVacationBlockAvailability,
    )

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: HANDLE PAST TEST CENTER OLA VACATION BLOCKS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )
    # getting current datetime
    current_date = date.today()

    # initializing contains_data_to_delete
    contains_data_to_delete = False

    # getting all past test center OLA vacation blocks
    past_vacation_blocks = TestCenterOlaVacationBlock.objects.filter(
        date_to__lt=current_date
    )

    if past_vacation_blocks:
        contains_data_to_delete = True

    # looping in past_vacation_blocks
    for past_vacation_block in past_vacation_blocks:
        # temporarily saving ID
        temp_id_1 = past_vacation_block.id

        # getting related vacation block availability entries
        related_vacation_block_availability_entries = (
            TestCenterOlaVacationBlockAvailability.objects.filter(
                test_center_ola_vacation_block_id=past_vacation_block.id
            )
        )

        # printing starting deletion process confirmation
        print(
            "{0} \t--> Deleting vacation block availabilities related to Vacation Block ID: {1}".format(
                ConsoleMessageColor.WHITE, past_vacation_block.id
            )
        )

        # deleting entries
        for (
            related_vacation_block_availability_entry
        ) in related_vacation_block_availability_entries:
            # temporarily saving ID
            temp_id_2 = related_vacation_block_availability_entry.id
            # deleting row
            related_vacation_block_availability_entry.delete()
            # printing delete test center OLA vacation block availability successfully message
            print(
                '{0} \t\t--> Deleted Vacation Block Availability ID: "{1}"'.format(
                    ConsoleMessageColor.WHITE,
                    temp_id_2,
                )
            )

        # deleting vacation block entry
        past_vacation_block.delete()

        # printing delete test center OLA vacation block successfully message
        print(
            "{0} \t--> Deleted Vacation Block ID: {1}".format(
                ConsoleMessageColor.WHITE, temp_id_1
            )
        )

    # contains_data_to_delete is True
    if contains_data_to_delete:
        # printing successful messasge
        print(
            "{0} ==================== PAST TEST CENTER OLA VACATION BLOCKS DELETED SUCCESSFULLY ====================".format(
                ConsoleMessageColor.CYAN
            )
        )
