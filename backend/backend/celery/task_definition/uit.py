from __future__ import absolute_import, unicode_literals
from datetime import date
from backend.celery.task_definition.utils import ConsoleMessageColor


# this function is deleting the expired UIT test access codes and unassigning READY and PRE_TEST assigned tests
def handle_expired_uit_processes():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from backend.custom_models.uit_invites import UITInvites
    from backend.custom_models.unsupervised_test_access_code_model import (
        UnsupervisedTestAccessCode,
    )
    from backend.custom_models.assigned_test import AssignedTest
    from backend.custom_models.user_accommodation_file import UserAccommodationFile
    from backend.custom_models.user_accommodation_file_status import (
        UserAccommodationFileStatus,
    )
    from backend.static.user_accommodation_file_status_const import (
        UserAccommodationFileStatusConst,
    )
    from backend.static.assigned_test_status import AssignedTestStatus
    from backend.custom_models.uit_reasons_for_deletion import UITReasonsForDeletion
    from backend.static.assigned_test_status import get_assigned_test_status_id

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: DELETE EXPIRED UIT TEST ACCESS CODES ====================".format(
            ConsoleMessageColor.GREEN
        )
    )

    # getting current date
    current_date = date.today()

    # initializing variables
    assigned_tests = []

    # getting all UIT test access codes
    uit_test_access_codes = UnsupervisedTestAccessCode.objects.all()

    # getting reason for deletion
    reason_for_deletion = UITReasonsForDeletion.objects.get(en_name="Process Expired")
    reason_for_deletion = {
        "en": reason_for_deletion.en_name,
        "fr": reason_for_deletion.fr_name,
    }

    # looping in unsupservised test access codes
    for test_access_code_data in uit_test_access_codes:
        # initializing candidate_email_var
        candidate_email_var = ""
        # validity end date has past
        if current_date > test_access_code_data.validity_end_date:
            # saving candidate_email_var
            candidate_email_var = test_access_code_data.candidate_email
            # saving test access code data
            tac = test_access_code_data.test_access_code
            # delete current test access code
            test_access_code_data.delete()
            # getting related uit invite
            uit_invite = UITInvites.objects.get(id=test_access_code_data.uit_invite_id)
            # printing successful messasge
            print(
                "{0} ==================== UIT TEST ACCESS CODE: {1} (INVITED CANDIDATE EMAIL: {2}) HAS BEEN DELETED SUCCESSFULLY ====================".format(
                    ConsoleMessageColor.CYAN, tac, candidate_email_var
                )
            )

    # looping in UIT Invites where the validity end date is passed
    passed_uit_invites = UITInvites.objects.filter(validity_end_date__lt=current_date)
    for uit_invite in passed_uit_invites:
        # getting assigned tests related to current UIT invite where status is CHECKED_IN/PRE_TEST
        checked_in_status_id = get_assigned_test_status_id(
            AssignedTestStatus.CHECKED_IN
        )
        pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
        assigned_tests = AssignedTest.objects.filter(
            uit_invite_id=uit_invite.id,
            status_id__in=(checked_in_status_id, pre_test_status_id),
        )

    # removing assigned tests that have an active AAE request in progress (not cancelled, cancelled by candidate or expired)
    final_assigned_tests = []
    for assigned_test in assigned_tests:
        # there is an associated user accommodation file (AAE Request)
        if assigned_test.user_accommodation_file_id is not None:
            # getting cancelled related user accommodation file status IDs
            cancelled_related_user_accommodation_file_status_ids = (
                UserAccommodationFileStatus.objects.filter(
                    codename__in=(
                        UserAccommodationFileStatusConst.CANCELLED,
                        UserAccommodationFileStatusConst.CANCELLED_BY_CANDIDATE,
                        UserAccommodationFileStatusConst.EXPIRED,
                    )
                ).values_list("id", flat=True)
            )
            # getting related user accommodation file data
            related_user_accommodation_file_data = (
                UserAccommodationFile.objects.filter(
                    id=assigned_test.user_accommodation_file_id
                )
                .order_by("id")
                .last()
            )
            # status of related_user_accommodation_file_data is part of the cancelled_related_user_accommodation_file_status_ids
            if (
                related_user_accommodation_file_data.status_id
                in cancelled_related_user_accommodation_file_status_ids
            ):
                # adding assigned test of current iteration to final_assigned_tests array
                final_assigned_tests.append(assigned_test)

        # there is no associated user accommodation file (no AAE Request)
        else:
            # adding assigned test of current iteration to final_assigned_tests array
            final_assigned_tests.append(assigned_test)
        # looping in assigned_tests
        for assigned_test in final_assigned_tests:
            # updating status to UNASSIGNED
            unassigned_status_id = get_assigned_test_status_id(
                AssignedTestStatus.UNASSIGNED
            )
            assigned_test.status_id = unassigned_status_id
            assigned_test.save()
            # getting related historical unsupervised test access code
            historical_uit_test_access_code_data = (
                UnsupervisedTestAccessCode.history.filter(
                    uit_invite_id=uit_invite.id,
                    test_access_code=assigned_test.test_access_code,
                ).first()
            )
            # printing successful messasge
            print(
                "{0} ==================== ASSIGNED TEST ID: {1} (INVITED CANDIDATE EMAIL: {2}) HAS BEEN UNASSIGNED SUCCESSFULLY ====================".format(
                    ConsoleMessageColor.CYAN,
                    assigned_test.id,
                    historical_uit_test_access_code_data.candidate_email,
                )
            )


# this function is unassigning the non-started UIT tests
def unassign_non_started_uit_tests():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from backend.custom_models.uit_invites import UITInvites
    from backend.custom_models.assigned_test import AssignedTest
    from backend.custom_models.user_accommodation_file import UserAccommodationFile
    from backend.custom_models.user_accommodation_file_status import (
        UserAccommodationFileStatus,
    )
    from backend.static.user_accommodation_file_status_const import (
        UserAccommodationFileStatusConst,
    )
    from backend.static.assigned_test_status import AssignedTestStatus
    from backend.static.assigned_test_status import get_assigned_test_status_id

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: UNASSIGN NON-STARTED UIT TESTS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )

    # initializing at_least_one_test_unassigned
    at_least_one_test_unassigned = False

    # getting current date
    current_date = date.today()

    # getting needed assigned test status IDs
    checked_in_status_id = get_assigned_test_status_id(AssignedTestStatus.CHECKED_IN)
    pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
    unassigned_status_id = get_assigned_test_status_id(AssignedTestStatus.UNASSIGNED)

    # getting CHECKED_IN and PRE_TEST assigned UIT tests
    assigned_tests = AssignedTest.objects.filter(
        status_id__in=(checked_in_status_id, pre_test_status_id),
        uit_invite_id__isnull=False,
    )

    # removing assigned tests that have an active AAE request in progress (not cancelled, cancelled by candidate or expired)
    final_assigned_tests = []
    for assigned_test in assigned_tests:
        # there is an associated user accommodation file (AAE Request)
        if assigned_test.user_accommodation_file_id is not None:
            # getting cancelled related user accommodation file status IDs
            cancelled_related_user_accommodation_file_status_ids = (
                UserAccommodationFileStatus.objects.filter(
                    codename__in=(
                        UserAccommodationFileStatusConst.CANCELLED,
                        UserAccommodationFileStatusConst.CANCELLED_BY_CANDIDATE,
                        UserAccommodationFileStatusConst.EXPIRED,
                    )
                ).values_list("id", flat=True)
            )
            # getting related user accommodation file data
            related_user_accommodation_file_data = (
                UserAccommodationFile.objects.filter(
                    id=assigned_test.user_accommodation_file_id
                )
                .order_by("id")
                .last()
            )
            # status of related_user_accommodation_file_data is part of the cancelled_related_user_accommodation_file_status_ids
            if (
                related_user_accommodation_file_data.status_id
                in cancelled_related_user_accommodation_file_status_ids
            ):
                # adding assigned test of current iteration to final_assigned_tests array
                final_assigned_tests.append(assigned_test)

        # there is no associated user accommodation file (no AAE Request)
        else:
            # adding assigned test of current iteration to final_assigned_tests array
            final_assigned_tests.append(assigned_test)

    # looping in assigned_tests
    for assigned_test in final_assigned_tests:
        # getting related uit invite data
        related_uit_invite_data = UITInvites.objects.get(id=assigned_test.uit_invite_id)
        # we are past the validity end date
        if current_date > related_uit_invite_data.validity_end_date:
            # setting at_least_one_test_unassigned to True
            at_least_one_test_unassigned = True
            # unassigning test
            assigned_test.status_id = unassigned_status_id
            assigned_test.save()
            print(
                "{0} \t--> Assigned Test ID: {1} unassigned successfully".format(
                    ConsoleMessageColor.WHITE, assigned_test.id
                )
            )

    # at_least_one_test_unassigned has been set to True
    if at_least_one_test_unassigned:
        print(
            "{0} ==================== NON-STARTED UIT ASSIGNED TESTS HAVE BEEN UNASSIGNED SUCCESSFULLY ====================".format(
                ConsoleMessageColor.CYAN,
            )
        )


def send_uit_email(
    org_code,
    candidate_info,
    test_data,
    staffing_or_reference_number,
    billing_contact_info,
    validity_end_date,
    default_test_time,
    test_access_code,
    uit_invite_id,
):
    from django.conf import settings
    from django.core.mail import send_mail
    from backend.views.utils import (
        get_weekday_based_on_provided_date,
        get_month_as_a_word,
    )
    from backend.views.gc_notify_view import (
        is_gc_notify_active,
        send_uit_invite_basic,
    )

    site_protocol = settings.SITE_PROTOCOL
    site_domain = settings.SITE_DOMAIN

    weekday = get_weekday_based_on_provided_date(validity_end_date)
    month_as_word = get_month_as_a_word(validity_end_date.split("-")[1])

    hours = 0
    minutes = 0

    context = {
        "org_code_en": org_code["en"],
        "org_code_fr": org_code["fr"],
        "first_name": candidate_info["Prenom_First_Name"],
        "last_name": candidate_info["Nom_de_famille_Last_Name"],
        "email": candidate_info["Courriel_Email"],
        "test_en_name": test_data["en_name"],
        "test_fr_name": test_data["fr_name"],
        "staffing_process_number": staffing_or_reference_number,
        "billing_contact_info": billing_contact_info,
        "test_access_code": test_access_code,
        "deadline_day": validity_end_date.split("-")[2],
        "deadline_month_en": month_as_word["en"],
        "deadline_month_fr": month_as_word["fr"],
        "deadline_year": validity_end_date.split("-")[0],
        "deadline_weekday_en": weekday["en"],
        "deadline_weekday_fr": weekday["fr"],
        "default_test_time": default_test_time,
        "cat_url": "{}{}/oec-cat".format(site_protocol, site_domain),
        "assessment_accommodations_link": "{}".format(
            "https://www.canada.ca/en/public-service-commission/services/second-language-testing-public-service/unsupervised-test-reading-comprehension-level-b-or-c/preparing-for-the-test.html"
        ),
        "account_setup_help_link_en": "{}".format(
            "https://www.canada.ca/en/public-service-commission/services/second-language-testing-public-service/candidate-assessment-tool.html"
        ),
        "account_setup_help_link_fr": "{}".format(
            "https://www.canada.ca/fr/commission-fonction-publique/services/evaluation-langue-seconde/outil-evaluation-candidats.html"
        ),
        "hours": hours,
        "minutes": minutes,
    }

    ## If GC Notify is enabled:
    if is_gc_notify_active():
        send_uit_invite_basic(uit_invite_id, context)

    # if not GC Notify
    else:

        subject = "Invitation à passer un test – {test_fr_name} / Invitation to take a test – {test_en_name}".format(
            test_fr_name=context["test_fr_name"], test_en_name=context["test_en_name"]
        )

        email_en_part = """
            {first_name} {last_name},
            \n
            You are invited to complete the following test:
            \n
            Test: {test_en_name}
            Department or agency inviting you to take the test: {org_code_en}
            Start time: You may start the test at your convenience after having read the instructions.
            Deadline: {deadline_day}, {deadline_month_en}, {deadline_year} at 11:59 pm, Eastern time
            Location: Online
            Length: {default_test_time} minutes 
            Test access code: {test_access_code}
            \n
            Accessing your test:
                1.  Use this link to access the testing platform: {cat_url} (if clicking the link doesn’t bring you to the test website, copy and paste it into your web browser.)
                2.  Log in to, or create an account (use the same account for all tests).
                3.  We encourage you to try the sample tests and adjust your display settings before taking the test.
                4.  Use the test access code provided above to check in and start your test.
                5.  Your test only starts once you’ve begun the timed section of the test. It’s not possible to stop the timer once the test has started.
            \n
            Please allow enough time to complete the above steps, keeping the deadline in mind. You will find information to help you setup your account and use different functionalities of the testing platform at : {account_setup_help_link_en}
            \n
            Contact us during regular business hours at {billing_contact_info}, if possible, before starting the test, if: 
                •	the wrong test or test language has been assigned to you
                •	you have technical difficulties that prevent you from completing your test
                •	you’ve taken this test less than 30 days ago, in any format (online or paper).
            \n
            If you need assessment accommodation measures that haven’t been set up for you in the system, do not start your test; instead, send an email to {billing_contact_info}.
            \n
            Replies to this system-generated email will not be answered. Please contact {billing_contact_info} if you have any questions.
            """.format(
            org_code_en=context["org_code_en"],
            org_code_fr=context["org_code_fr"],
            first_name=context["first_name"],
            last_name=context["last_name"],
            test_en_name=context["test_en_name"],
            test_fr_name=context["test_fr_name"],
            staffing_process_number=context["staffing_process_number"],
            billing_contact_info=context["billing_contact_info"],
            test_access_code=context["test_access_code"],
            deadline_day=context["deadline_day"],
            deadline_month_en=context["deadline_month_en"],
            deadline_month_fr=context["deadline_month_fr"],
            deadline_year=context["deadline_year"],
            deadline_weekday_en=context["deadline_weekday_en"],
            deadline_weekday_fr=context["deadline_weekday_fr"],
            default_test_time=context["default_test_time"],
            cat_url=context["cat_url"],
            assessment_accommodations_link=context["assessment_accommodations_link"],
            account_setup_help_link_en=context["account_setup_help_link_en"],
        )

        email_fr_part = """
            {first_name} {last_name},
            \n
            Nous vous invitons à passer le test suivant :
            \n
            Test : {test_fr_name}
            Ministère ou organisme qui vous invite à faire ce test : {org_code_fr}
            Heure de début : Vous pouvez commencer le test au moment qui vous convient après avoir lu les instructions.
            Date limite : {deadline_day}, {deadline_month_fr}, {deadline_year} à 23 h 59, (HE)
            Où : En ligne
            Durée : {default_test_time} minutes 
            Code d’accès au test : {test_access_code}
            \n
            Pour accéder au test :
                1.  Utilisez le lien suivant pour accéder à la plateforme de test : {cat_url} (si le lien ne vous permet pas d’accéder au site Web du test, copiez le lien et collez-le dans votre navigateur Web).
                2.  Connectez-vous ou créez un compte (utilisez le même compte pour tous les tests).
                3.  Nous vous conseillons d’essayer les exemples de tests et de régler vos paramètres d’affichage avant de commencer le test.
                4.  Utilisez le code d’accès fourni ci-dessus pour vous inscrire et commencer le test.
                5.  Votre test débutera seulement à partir du moment où vous commencerez la section chronométrée du test. Il n’est pas possible d’arrêter le chronomètre une fois le test commencé.
            \n
            Veuillez prévoir assez de temps pour terminer les étapes indiquées ci-dessus, en tenant compte de la date limite. Vous trouverez des renseignements pour vous aider à configurer votre compte et utiliser les différentes fonctionnalités de la plateforme de test à : {account_setup_help_link_fr}
            \n
            N’hésitez pas à communiquer avec nous avant de commencer le test, si possible, durant les heures normales de bureau au {billing_contact_info} si : 
                •	on vous a attribué le mauvais test ou fait une erreur dans la langue du test.
                •	des difficultés techniques vous empêchent de passer le test.
                •	vous avez fait ce test il y a moins de 30 jours, peu importe le format (en ligne ou papier).
            \n
            Si vous avez besoin de mesures d'adaptation en matière d'évaluation qui n'ont pas été prévues pour vous dans le système, ne commencez pas le test et envoyez plutôt un courriel à {billing_contact_info}.
            \n
            Les réponses à ce courriel généré par le système ne seront pas traitées. Veuillez plutôt communiquer avec {billing_contact_info} si vous avez des questions. 
            """.format(
            org_code_en=context["org_code_en"],
            org_code_fr=context["org_code_fr"],
            first_name=context["first_name"],
            last_name=context["last_name"],
            test_en_name=context["test_en_name"],
            test_fr_name=context["test_fr_name"],
            staffing_process_number=context["staffing_process_number"],
            billing_contact_info=context["billing_contact_info"],
            test_access_code=context["test_access_code"],
            deadline_day=context["deadline_day"],
            deadline_month_en=context["deadline_month_en"],
            deadline_month_fr=context["deadline_month_fr"],
            deadline_year=context["deadline_year"],
            deadline_weekday_en=context["deadline_weekday_en"],
            deadline_weekday_fr=context["deadline_weekday_fr"],
            default_test_time=context["default_test_time"],
            cat_url=context["cat_url"],
            assessment_accommodations_link=context["assessment_accommodations_link"],
            account_setup_help_link_fr=context["account_setup_help_link_fr"],
        )

        email_plaintext_message = """
            (The English follows)
            \n
            {email_fr_part}
            \n
            \n
            {email_en_part}
            """.format(
            email_fr_part=email_fr_part, email_en_part=email_en_part
        )

        send_mail(
            # title:
            subject,
            # message:
            email_plaintext_message,
            # from (will be overridden by the real email in Dev/Test/Prod):
            settings.EMAIL_HOST_USER,
            # to:
            [context["email"]],
        )


def handle_send_uit_invite_emails(candidate_invitations_array, validity_end_date):
    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: SEND UIT INVITE EMAILS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )
    # looping in candidate_invitations_array
    for data in candidate_invitations_array:
        # sending email
        send_uit_email(
            data["org_data"],
            data["candidate_info"],
            data["test_info"],
            data["staffing_or_reference_number"],
            data["billing_contact_info"],
            validity_end_date,
            data["default_test_time"],
            data["test_access_code"],
            data["uit_invite_id"],
        )
        # printing successful messasge
        print(
            "{0} ==================== UIT INVITE EMAIL SENT: {1} ====================".format(
                ConsoleMessageColor.CYAN, data["candidate_info"]["Courriel_Email"]
            )
        )


def send_modify_uit_validity_end_date_email(
    candidate_info,
    test_data,
    staffing_or_reference_number,
    billing_contact_info,
    validity_end_date,
    total_test_time,
    uit_test_access_code,
    org_data,
):
    from django.conf import settings
    from django.core.mail import send_mail
    from backend.views.utils import (
        get_weekday_based_on_provided_date,
        get_month_as_a_word,
    )
    from backend.views.gc_notify_view import (
        is_gc_notify_active,
        modify_uit_validity_end_date,
    )

    weekday = get_weekday_based_on_provided_date(validity_end_date)
    month_as_word = get_month_as_a_word(validity_end_date.split("-")[1])

    context = {
        "first_name": candidate_info["first_name"],
        "last_name": candidate_info["last_name"],
        "email": candidate_info["email"],
        "test_en_name": test_data["en_name"],
        "test_fr_name": test_data["fr_name"],
        "staffing_process_number": staffing_or_reference_number,
        "billing_contact_info": billing_contact_info,
        "test_access_code": uit_test_access_code,
        "deadline_day": validity_end_date.split("-")[2],
        "deadline_month_en": month_as_word["en"],
        "deadline_month_fr": month_as_word["fr"],
        "deadline_year": validity_end_date.split("-")[0],
        "deadline_weekday_en": weekday["en"],
        "deadline_weekday_fr": weekday["fr"],
        "total_test_time": total_test_time,
        "org_code_en": org_data["en"],
        "org_code_fr": org_data["fr"],
    }

    # if GC Notify is active
    if is_gc_notify_active():
        modify_uit_validity_end_date(context)
    # otherwise use the old version
    else:
        subject = (
            "Avis : Changement de la date d’échéance / Notice: Change to expiry date"
        )

        email_en_part = """
            {first_name} {last_name},
            \n
            The expiry date of the following test has been changed:
            \n
            Test: {test_en_name}
            Department or agency inviting you to take the test: {org_code_en}
            Start time: You may start the test at your convenience after reading the instructions. You must complete it before the new expiry date indicated below.
            New expiry date: {deadline_day}, {deadline_month_en}, {deadline_year} at 11:59 p.m. Eastern time
            Location: Online
            Test Access Code: {test_access_code}
            \n
            Replies to this system-generated email will not be answered. Please contact {billing_contact_info} if you have any questions.
            \n
            Thank you.
            """.format(
            first_name=context["first_name"],
            last_name=context["last_name"],
            test_en_name=context["test_en_name"],
            test_fr_name=context["test_fr_name"],
            staffing_process_number=context["staffing_process_number"],
            billing_contact_info=context["billing_contact_info"],
            test_access_code=context["test_access_code"],
            deadline_day=context["deadline_day"],
            deadline_month_en=context["deadline_month_en"],
            deadline_month_fr=context["deadline_month_fr"],
            deadline_year=context["deadline_year"],
            deadline_weekday_en=context["deadline_weekday_en"],
            deadline_weekday_fr=context["deadline_weekday_fr"],
            total_test_time=context["total_test_time"],
            org_code_en=context["org_code_en"],
        )

        email_fr_part = """
            {first_name} {last_name},
            \n
            La date d’échéance pour passer le test suivant a été changée :
            \n
            Test : {test_fr_name}
            Ministère ou organisme qui vous invite à faire ce test : {org_code_fr}
            Heure de début : Vous pouvez débuter le test au moment qui vous convient le mieux après avoir lu les instructions. Vous devez terminer le test avant la nouvelle date d’échéance indiquée ci-dessous.
            Nouvelle date d’échéance : {deadline_day}, {deadline_month_fr}, {deadline_year} à 23 h 59, heure de l'Est.
            Lieu : En ligne
            Code d’accès au test : {test_access_code}
            \n
            Les réponses à ce courriel généré par le système ne seront pas traitées. Veuillez plutôt communiquer avec {billing_contact_info} si vous avez des questions.
            \n
            Merci.
            """.format(
            first_name=context["first_name"],
            last_name=context["last_name"],
            test_en_name=context["test_en_name"],
            test_fr_name=context["test_fr_name"],
            staffing_process_number=context["staffing_process_number"],
            billing_contact_info=context["billing_contact_info"],
            test_access_code=context["test_access_code"],
            deadline_day=context["deadline_day"],
            deadline_month_en=context["deadline_month_en"],
            deadline_month_fr=context["deadline_month_fr"],
            deadline_year=context["deadline_year"],
            deadline_weekday_en=context["deadline_weekday_en"],
            deadline_weekday_fr=context["deadline_weekday_fr"],
            total_test_time=context["total_test_time"],
            org_code_fr=context["org_code_fr"],
        )

        email_plaintext_message = """
            (The English follows)
            \n
            {email_fr_part}
            \n
            \n
            {email_en_part}
            """.format(
            email_en_part=email_en_part, email_fr_part=email_fr_part
        )

        send_mail(
            # title:
            subject,
            # message:
            email_plaintext_message,
            # from (will be overridden by the real email in Dev/Test/Prod):
            settings.EMAIL_HOST_USER,
            # to:
            [context["email"]],
        )


def handle_update_uit_validity_end_date_emails(candidates_to_send_email):
    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: SEND UPDATE UIT VALIDITY END DATE EMAILS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )
    # looping in candidates_to_send_email
    for data in candidates_to_send_email:
        # send modification email
        send_modify_uit_validity_end_date_email(
            data["candidate_info"],
            data["test_data"],
            data["staffing_or_reference_number"],
            data["billing_contact_info"],
            data["validity_end_date"],
            data["total_test_time"],
            data["test_access_code"],
            data["org_data"],
        )
        # printing successful messasge
        print(
            "{0} ==================== UPDATE UIT VALIDITY END DATE EMAIL SENT: {1} ====================".format(
                ConsoleMessageColor.CYAN, data["candidate_info"]["email"]
            )
        )


def send_deactivate_uit_test_cancellation_email(
    candidate_info,
    test_data,
    staffing_or_reference_number,
    billing_contact_info,
    uit_test_access_code,
    reason_for_deletion,
    org_data,
):
    from django.conf import settings
    from django.core.mail import send_mail
    from backend.views.gc_notify_view import (
        is_gc_notify_active,
        deactivate_uit_test_cancellation,
    )

    context = {
        "first_name": candidate_info["first_name"],
        "last_name": candidate_info["last_name"],
        "email": candidate_info["email"],
        "test_en_name": test_data["en_name"],
        "test_fr_name": test_data["fr_name"],
        "staffing_process_number": staffing_or_reference_number,
        "billing_contact_info": billing_contact_info,
        "test_access_code": uit_test_access_code,
        "reason_for_deletion_en": reason_for_deletion["en"],
        "reason_for_deletion_fr": reason_for_deletion["fr"],
        "org_code_en": org_data["en"],
        "org_code_fr": org_data["fr"],
    }

    # if GC Notify is active
    if is_gc_notify_active():
        deactivate_uit_test_cancellation(context)
    # otherwise use the old version
    else:
        subject = "Avis d'annulation d'un test / Test cancellation notice"

        email_en_part = """
            {first_name} {last_name},
            \n
            The following test has been canceled:
            \n
            Test: {test_en_name}
            Department or agency inviting you to take the test: {org_code_en}
            Test access code: {test_access_code}: Please note that this access code has now been deactivated and cannot be used.
            \n
            Replies to this system-generated email will not be answered. Please contact {billing_contact_info} if you have any questions.
            \n
            Thank you.
            """.format(
            first_name=context["first_name"],
            last_name=context["last_name"],
            test_en_name=context["test_en_name"],
            test_fr_name=context["test_fr_name"],
            staffing_process_number=context["staffing_process_number"],
            billing_contact_info=context["billing_contact_info"],
            test_access_code=context["test_access_code"],
            reason_for_deletion_en=context["reason_for_deletion_en"],
            reason_for_deletion_fr=context["reason_for_deletion_fr"],
            org_code_en=context["org_code_en"],
        )

        email_fr_part = """
            {first_name} {last_name},
            \n
            Le test suivant a été annulé :
            \n
            Test : {test_fr_name}
            Ministère ou organisme qui vous invite à faire ce test : {org_code_fr}
            Code d’accès au test : {test_access_code} – Veuillez noter que ce code d’accès est maintenant désactivé et ne pourra plus être utilisé.
            \n
            Les réponses à ce courriel généré par le système ne seront pas traitées. Veuillez plutôt communiquer avec {billing_contact_info} si vous avez des questions.
            \n
            Merci.
            """.format(
            first_name=context["first_name"],
            last_name=context["last_name"],
            test_en_name=context["test_en_name"],
            test_fr_name=context["test_fr_name"],
            staffing_process_number=context["staffing_process_number"],
            billing_contact_info=context["billing_contact_info"],
            test_access_code=context["test_access_code"],
            reason_for_deletion_en=context["reason_for_deletion_en"],
            reason_for_deletion_fr=context["reason_for_deletion_fr"],
            org_code_fr=context["org_code_fr"],
        )

        email_plaintext_message = """
            (The English follows)
            \n
            {email_fr_part}
            \n
            \n
            {email_en_part}
            """.format(
            email_en_part=email_en_part, email_fr_part=email_fr_part
        )

        send_mail(
            # title:
            subject,
            # message:
            email_plaintext_message,
            # from (will be overridden by the real email in Dev/Test/Prod):
            settings.EMAIL_HOST_USER,
            # to:
            [context["email"]],
        )


def handle_delete_uit_invite_emails(candidates_to_send_email):
    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: DELETE UIT INVITE EMAILS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )
    # looping in candidates_to_send_email
    for data in candidates_to_send_email:
        # send cancellation email
        send_deactivate_uit_test_cancellation_email(
            data["candidate_info"],
            data["test_data"],
            data["staffing_or_reference_number"],
            data["billing_contact_info"],
            data["test_access_code"],
            data["reason_for_deletion"],
            data["org_data"],
        )
        # printing successful messasge
        print(
            "{0} ==================== DELETE UIT INVITE EMAIL SENT: {1} ====================".format(
                ConsoleMessageColor.CYAN, data["candidate_info"]["email"]
            )
        )
