from __future__ import absolute_import, unicode_literals
from datetime import date, timedelta
from backend.celery.task_definition.utils import ConsoleMessageColor


# this function is deleting the expired test permissions
def remove_old_test_access_codes():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from backend.custom_models.test_access_code_model import TestAccessCode
    from backend.custom_models.test_center_test_session_officers import (
        TestCenterTestSessionOfficers,
    )

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: REMOVE EXPIRED TEST ACCESS CODES ====================".format(
            ConsoleMessageColor.GREEN
        )
    )

    # getting date in 30 days
    date_30_days_in_the_past = date.today() - timedelta(days=30)

    # getting all test access codes
    test_access_codes = TestAccessCode.objects.all()

    # looping in test access codes
    for test_access_code in test_access_codes:
        # created date is older than 30 days
        if test_access_code.created_date < date_30_days_in_the_past:
            # temporarily saving test access code
            temp_tac = test_access_code.test_access_code
            # deleting test center test session officer links (if needed)
            TestCenterTestSessionOfficers.objects.filter(
                test_access_code_id=test_access_code.id
            ).delete()
            # deleting test access code of current iteration
            test_access_code.delete()
            # printing successful messasge
            print(
                "{0} ==================== TEST_ACCESS_CODE: {1} HAS BEEN DELETED SUCCESSFULLY ====================".format(
                    ConsoleMessageColor.CYAN, temp_tac
                )
            )
