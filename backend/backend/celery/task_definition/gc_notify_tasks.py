import requests
import json
from datetime import datetime, timedelta
from django.conf import settings
from pytz import timezone
import pytz
from backend.views.gc_notify_view import (
    send_assigned_test_to_administer_failed_delivery,
    failed_send_test_session_candidates_to_security,
)
from backend.celery.task_definition.utils import ConsoleMessageColor

# Explanation
# Get all ids for pending emails from GC Notify in batches of 250
# keep getting them until GC notify has no more ids or all pending ids have been checked
# for each id
#   if delivered, delete the row
#   if failed, follow up with the relevant party (if any), delete the row
#   any other status, update the last checked date so it does not appear in this itteration again


# this radically cuts down on calls to GC notify: a maximum of 20 calls for 5000 emails, vs 1/email if it was driven by the application
def check_gc_notify_delivery_status():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from backend.custom_models.gc_notify_email_pending_delivery import (
        GCNotifyEmailPendingDelivery,
    )
    from backend.custom_models.gc_notify_email_failed_bulk_delivery import (
        GCNotifyEmailFailedBulkDelivery,
    )
    from backend.custom_models.keyring import Keyring
    from backend.custom_models.gc_notify_email_verified_attempted_delivery import (
        GCNotifyEmailVerifiedAttemptedDelivery,
    )
    from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames
    from backend.static.site_admin_setting_type import Key

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: CHECK GC NOTIFY DELIVERY STATUS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )

    # getting current date # TODO if its older than 7 days, there is no point in checking
    current_date = datetime.now(pytz.utc)  # getting current date
    seven_days_ago = datetime.now(pytz.utc) - timedelta(days=7)

    key = Keyring.objects.filter(codename=Key.GC_NOTIFY).last()

    # catch if GC Notify is not enabled and immedately return
    if key is None:
        return

    key = key.key

    # The template id, what GC Notify calls the unique hash for the template
    headers = {
        "Authorization": key,
        "Accept": "application/json",
        "Content-Type": "application/json",
    }
    gc_notify_email_ids = GCNotifyEmailPendingDelivery.objects.filter(
        last_checked_date__lt=current_date, sent_date__gt=seven_days_ago
    ).values_list("gc_notify_email_id", flat=True)

    older_than = ""

    # While there are unchecked emails
    while gc_notify_email_ids:
        # initializing older_than_parameter
        older_than_parameter = ""

        # if this is not the first loop, then we only want to check emails older than the last one we checked
        if older_than != "":
            # updating older_than_parameter
            older_than_parameter = "?older_than={0}".format(older_than)

        response = requests.get(
            "https://api.notification.canada.ca/v2/notifications{0}".format(
                older_than_parameter
            ),
            headers=headers,
        )

        # get the response
        json_ret = response.json()
        # if its an empty list then we've checked all emails GC Notify has record of; break and exit
        if json_ret["notifications"] == []:
            break

        # for each email notification in the return
        for notification in json_ret["notifications"]:
            # update older_than
            older_than = notification["id"]
            # if the id is in the list of unchecked ids
            if notification["id"] in gc_notify_email_ids:
                try:
                    # get the GCNotifyEmailPendingDelivery object
                    gc_notify_email_pending_delivery = (
                        GCNotifyEmailPendingDelivery.objects.filter(
                            gc_notify_email_id=notification["id"]
                        )
                    ).last()

                    # detemine if the status of each email is delivered, failed, or other and respond accordingly

                    is_delivered = True
                    status = notification["status"]
                    # Check the status and act accordingly
                    # If it is delivered, then log it
                    if status == "delivered":
                        # is_delivered is already true by default
                        # printing success
                        print(
                            "{0} ==================== CELERY TASK EXECUTION: {1} DELIVERED ====================".format(
                                ConsoleMessageColor.CYAN, notification["id"]
                            )
                        )

                    # If it failed to deliver, then handle the response (if any) based on the original template
                    elif status in [
                        "permanent-failure",
                        "temporary-failure",
                        "technical-failure",
                        "virus-scan-failed",
                    ]:
                        is_delivered = False
                        template = gc_notify_email_pending_delivery.gc_notify_template
                        if template.codename in [
                            GCNotifyTemplateCodenames.TWO_FACTOR_AUTHENTICATION,
                            GCNotifyTemplateCodenames.FAILED_SUP_DELIVERY,
                            GCNotifyTemplateCodenames.FAILED_UIT_DELIVERY,
                            GCNotifyTemplateCodenames.PASSWORD_RESET,
                            GCNotifyTemplateCodenames.RESERVATION_CODE_WITHDRAW_NO_BOOKING,
                            GCNotifyTemplateCodenames.RESERVATION_CODE_WITHDRAW_FROM_BOOKED_SESSION,
                            GCNotifyTemplateCodenames.RESERVATION_CODE_BOOKING,
                            GCNotifyTemplateCodenames.RESERVATION_CODE_BOOK_LATER,
                            GCNotifyTemplateCodenames.DELETE_SPECIFIC_TEST_SESSION,
                            GCNotifyTemplateCodenames.DEACTIVATE_UIT_TEST_CANCELLATION,
                            GCNotifyTemplateCodenames.FAILED_SEND_TEST_SESSION_CANDIDATES_TO_SECURITY,
                            GCNotifyTemplateCodenames.MODIFY_UIT_VALIDITY_END_DATE,
                        ]:
                            pass  # do nothing else if theses emails bounce
                        # handle failed emails for supervised/unsupervised test invites
                        elif template.codename in [
                            GCNotifyTemplateCodenames.RESERVATION_INVITE,
                            GCNotifyTemplateCodenames.UIT_INVITE_BASIC,
                        ]:
                            # insert into the failed delivery table; we will handle notifying the HR rep later in this function
                            GCNotifyEmailFailedBulkDelivery.objects.create(
                                gc_notify_template_id=gc_notify_email_pending_delivery.gc_notify_template_id,
                                sent_date=gc_notify_email_pending_delivery.sent_date,
                                email_to=gc_notify_email_pending_delivery.email_to,
                                email_contact_if_fail=gc_notify_email_pending_delivery.email_contact_if_fail,
                                first_name_to=gc_notify_email_pending_delivery.first_name_to,
                                last_name_to=gc_notify_email_pending_delivery.last_name_to,
                                uit_invite_id=gc_notify_email_pending_delivery.uit_invite_id,
                                assessment_process_id=gc_notify_email_pending_delivery.assessment_process_id,
                                status_date=current_date,
                                status=notification["status"],
                                email_subject=gc_notify_email_pending_delivery.email_subject,
                                email_body=gc_notify_email_pending_delivery.email_body,
                            )
                        # handle failed emails for editing active processes assigned candidates (tests to administer related logic)
                        elif template.codename in [
                            GCNotifyTemplateCodenames.SEND_NEW_ASSIGNED_TEST_TO_ADMINISTER,
                            GCNotifyTemplateCodenames.RESEND_ALREADY_ASSIGNED_TEST_TO_ADMINISTER,
                        ]:
                            # sending failed delivery email
                            personalization = {
                                "email": gc_notify_email_pending_delivery.email_contact_if_fail,
                                "process_name": gc_notify_email_pending_delivery.assessment_process.reference_number,
                                "candidate_email": gc_notify_email_pending_delivery.email_to,
                            }
                            send_assigned_test_to_administer_failed_delivery(
                                personalization
                            )
                            # delete the GCNotifyEmailPendingDelivery and make a new GCNotifyEmailVerifiedAttemptedDelivery
                            gc_notify_email_pending_delivery.delete()
                            # skip rest of the logic for this iteration
                            continue
                        elif (
                            template.codename
                            == GCNotifyTemplateCodenames.TEST_SESSION_CANDIDATES_TO_SECURITY
                        ):
                            personalization = {
                                "email": gc_notify_email_pending_delivery.email_contact_if_fail,
                                "security_email": gc_notify_email_pending_delivery.email_to,
                            }

                            failed_send_test_session_candidates_to_security(
                                personalization
                            )

                            # delete the GCNotifyEmailPendingDelivery
                            gc_notify_email_pending_delivery.delete()
                            # skip rest of the logic for this iteration
                            continue

                        # TODO add else ifs for the other templates as this is updated
                        # catch all for templates that are not handled here
                        else:
                            # this case is not handled in the code above, update the last time it was checked and do nothing
                            gc_notify_email_pending_delivery.last_checked_date = (
                                current_date
                            )
                            gc_notify_email_pending_delivery.save()
                            continue  # do not process this one further

                        # printing delivery failure
                        print(
                            "{0} ==================== CELERY TASK EXECUTION: {1} UNDELIVERABLE ====================".format(
                                ConsoleMessageColor.CYAN, notification["id"]
                            )
                        )

                    # if it's status has not been updated to delivered or failed, update the last time it was checked
                    else:
                        # printing other status
                        print(
                            "{0} ==================== CELERY TASK EXECUTION: {1} {2} ====================".format(
                                ConsoleMessageColor.CYAN, notification["id"], status
                            )
                        )
                        gc_notify_email_pending_delivery.last_checked_date = (
                            current_date
                        )
                        gc_notify_email_pending_delivery.save()
                        continue  # do not process this one further
                    # for all delivered or failed emails, do the following

                    # delete the GCNotifyEmailPendingDelivery and make a new GCNotifyEmailVerifiedAttemptedDelivery
                    gc_notify_email_pending_delivery.delete()
                    GCNotifyEmailVerifiedAttemptedDelivery.objects.create(
                        gc_notify_template_id=gc_notify_email_pending_delivery.gc_notify_template_id,
                        sent_date=gc_notify_email_pending_delivery.sent_date,
                        email_to=gc_notify_email_pending_delivery.email_to,
                        email_contact_if_fail=gc_notify_email_pending_delivery.email_contact_if_fail,
                        first_name_to=gc_notify_email_pending_delivery.first_name_to,
                        last_name_to=gc_notify_email_pending_delivery.last_name_to,
                        uit_invite_id=gc_notify_email_pending_delivery.uit_invite_id,
                        assessment_process_id=gc_notify_email_pending_delivery.assessment_process_id,
                        status_date=current_date,
                        status=notification["status"],
                        is_delivered=is_delivered,
                        email_subject=gc_notify_email_pending_delivery.email_subject,
                        email_body=gc_notify_email_pending_delivery.email_body,
                    )

                    # check to see if this was the final email of a bulk email
                    check_for_pending_bulk_failures(gc_notify_email_pending_delivery)

                except GCNotifyEmailPendingDelivery.DoesNotExist:
                    pass
        # AFTER the loop, get the new list of GCNotifyEmailPendingDelivery objects that have not been delivered or checked
        # will be used for the next itteration of the while loop
        gc_notify_email_ids = GCNotifyEmailPendingDelivery.objects.filter(
            last_checked_date__lt=current_date, sent_date__gt=seven_days_ago
        ).values_list("gc_notify_email_id", flat=True)


# celery task
def purge_expired_gc_notify_tokens():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from backend.custom_models.gc_notify_email_pending_delivery import (
        GCNotifyEmailPendingDelivery,
    )

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: PURGE EXPIRED GC NOTIFY TOKENS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )

    # getting current date
    seven_days_ago = datetime.now(pytz.utc) - timedelta(days=7)

    # get all tokens that are older in 7 days
    expired_gc_notify_tokens = GCNotifyEmailPendingDelivery.objects.filter(
        sent_date__lte=seven_days_ago
    )

    # delete these. GC Notify will not give any updates for these at this point
    # assume successful delivery
    for expired_token in expired_gc_notify_tokens:
        print(
            "{0} ==================== CELERY TASK EXECUTION: DELETE EXPIRED GC NOTIFY TOKEN {1} ====================".format(
                ConsoleMessageColor.CYAN, expired_token.gc_notify_email_id
            )
        )
        expired_token.delete()


# celery task
def check_for_orphaned_bulk_delivery_failues():
    from backend.custom_models.gc_notify_email_failed_bulk_delivery import (
        GCNotifyEmailFailedBulkDelivery,
    )

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: CHECK FOR ORPHANDED BULK DELIVERY FAILURE ====================".format(
            ConsoleMessageColor.GREEN
        )
    )

    failed_bulk_deliveries = GCNotifyEmailFailedBulkDelivery.objects.all()
    for failed_delivery in failed_bulk_deliveries:
        check_for_pending_bulk_failures(failed_delivery)


# Code to handle notification of one or more failues in a bulk delivery when all emails are delivered/failed
# prevent duplicated code: pending_object_to_check is of the following types:
#        GCNotifyEmailPendingDelivery for check_gc_notify_delivery_status
#        GCNotifyEmailFailedBulkDelivery for check_for_orphaned_bulk_delivery_failues
def check_for_pending_bulk_failures(pending_object_to_check):
    # imports need to be here to avoid errors/exceptions when starting the containers
    from backend.custom_models.gc_notify_email_pending_delivery import (
        GCNotifyEmailPendingDelivery,
    )
    from backend.custom_models.gc_notify_email_failed_bulk_delivery import (
        GCNotifyEmailFailedBulkDelivery,
    )
    from backend.custom_models.orderless_financial_data import OrderlessFinancialData
    from backend.views.gc_notify_view import (
        send_failed_supervised_invitation_delivery,
        send_failed_unsupervised_invitaion_delivery,
    )

    # check to see if this was the final pending email for a supervised test invitation
    if pending_object_to_check.assessment_process_id is not None:
        # check if there are any other emails in the same process that have not been delivered yet
        pending_emails_in_process = GCNotifyEmailPendingDelivery.objects.filter(
            assessment_process_id=pending_object_to_check.assessment_process_id
        ).exclude(id=pending_object_to_check.id)
        # if all emails in the process have responded with success or failure
        if not pending_emails_in_process:
            # get all the emails for the process that failed delivery; this email will be invcluded if it failed
            failed_emails_in_process = GCNotifyEmailFailedBulkDelivery.objects.filter(
                assessment_process_id=pending_object_to_check.assessment_process_id
            ).values_list("email_to", flat=True)
            # ensure the list is not empty
            if failed_emails_in_process:
                email_list = ", ".join(failed_emails_in_process)
                # send email of all failures
                personalization = {
                    "email": pending_object_to_check.email_contact_if_fail,
                    "process_name": pending_object_to_check.assessment_process.reference_number,
                    "email_list": email_list,
                }
                send_failed_supervised_invitation_delivery(personalization)
                # delete all GCNotifyEmailFailedBulkDelivery with this process number from the table
                GCNotifyEmailFailedBulkDelivery.objects.filter(
                    assessment_process_id=pending_object_to_check.assessment_process_id
                ).delete()
    # check to see if this was the final pending email for a unsupervised test invitation
    elif pending_object_to_check.uit_invite_id is not None:
        # check if there are any other emails in the same process that have not been delivered yet
        pending_emails_in_process = GCNotifyEmailPendingDelivery.objects.filter(
            uit_invite_id=pending_object_to_check.uit_invite_id
        ).exclude(id=pending_object_to_check.id)
        # if all emails in the process have responded with success or failure
        if not pending_emails_in_process:
            # get all the emails for the process that failed delivery; this email will be invcluded if it failed
            failed_emails_in_process = GCNotifyEmailFailedBulkDelivery.objects.filter(
                uit_invite_id=pending_object_to_check.uit_invite_id
            ).values_list("email_to", flat=True)
            # ensure the list is not empty
            if failed_emails_in_process:
                email_list = ", ".join(failed_emails_in_process)
                # send email of all failures
                staffing_process_number = None
                # check if it is an orderless test
                is_orderless = pending_object_to_check.uit_invite.orderless_request
                # in the case of an orderless test, get the process # from the orderless financial data
                if is_orderless:
                    staffing_process_number = (
                        OrderlessFinancialData.objects.filter(
                            uit_invite_id=pending_object_to_check.uit_invite_id
                        )
                        .last()
                        .reference_number
                    )
                # otherwise, get it from the TA's permissions
                else:
                    staffing_process_number = (
                        pending_object_to_check.uit_invite.ta_test_permissions.staffing_process_number
                    )

                personalization = {
                    "email": pending_object_to_check.email_contact_if_fail,
                    "process_name": staffing_process_number,
                    "email_list": email_list,
                }
                send_failed_unsupervised_invitaion_delivery(personalization)
                # delete all GCNotifyEmailFailedBulkDelivery with this process number from the table
                GCNotifyEmailFailedBulkDelivery.objects.filter(
                    uit_invite_id=pending_object_to_check.uit_invite_id
                ).delete()


# helper function for check_for_ola_reminder_timelines
def gen_personalization_for_session_reminder(session):
    # convert dates/times for readability
    local_start_time = session.start_time.astimezone(timezone(settings.TIME_ZONE))

    formatted_start_time = "{:d}:{:02d}".format(
        local_start_time.hour, local_start_time.minute
    )

    # get a string of the date
    date = session.date.strftime("%Y/%m/%d")
    return {
        "email": session.candidate_email,
        "date": date,
        "start_time": formatted_start_time,
    }


# celery task to send out reminder emails for upcoming OLA test sessions
def check_for_ola_reminder_timelines():
    from backend.views.gc_notify_view import (
        two_day_reminder,
        seven_day_reminder,
    )
    from backend.static.ola_global_configs_const import OlaGlobalConfigsConst
    from backend.custom_models.ola_global_configs import OlaGlobalConfigs

    from db_views.db_view_models.virtual_test_session_details_vw import (
        VirtualTestSessionDetailsVW,
    )
    from backend.custom_models.test_center_ola_configs import TestCenterOlaConfigs

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: CHECK FOR OLA REMINDER TIMELINES ====================".format(
            ConsoleMessageColor.GREEN
        )
    )

    # getting date in 2 days and 7 days

    two_days_ahead = datetime.now(pytz.utc) + timedelta(days=2)
    seven_days_ahead = datetime.now(pytz.utc) + timedelta(days=7)

    seven_day_reminder_sessions = VirtualTestSessionDetailsVW.objects.filter(
        date=seven_days_ahead
    )

    two_day_reminder_sessions = VirtualTestSessionDetailsVW.objects.filter(
        date=two_days_ahead
    )

    # get the global cancelation window; use this for most sessions
    cw = OlaGlobalConfigs.objects.get(
        codename=OlaGlobalConfigsConst.CANCELLATION_WINDOW
    ).value

    seven_day_count = 0

    # send out 7 day reminder spam
    for session in seven_day_reminder_sessions:
        personalize = gen_personalization_for_session_reminder(session)

        # get the start time of the test
        # go crazy I shall
        local_start_time = session.start_time.astimezone(timezone(settings.TIME_ZONE))

        # booking delay of TC and the CW is in hours, get start time, adjust by hours before

        # if they can cancel last minute; override cw with the test center's
        if session.allow_last_minute_cancellation_tc:
            # if it is assigned to a test center, get that center's booking delay
            if session.test_center_booking_delay is not None:
                cw = session.test_center_booking_delay
            else:
                # otherwise get the longest booking delay possible
                cw = (
                    TestCenterOlaConfigs.objects.filter()
                    .order_by("-booking_delay")
                    .first()
                    .booking_delay
                )

        # determine how long before the start time this is
        cancellation_time = local_start_time - timedelta(hours=cw)

        personalize["last_cancelation_date"] = cancellation_time.strftime(
            "%Y/%m/%d %H:%M"
        )
        seven_day_reminder(personalize)
        seven_day_count += 1

    two_day_count = 0

    # sedn out 2 day reminder spam
    for session in two_day_reminder_sessions:
        personalize = gen_personalization_for_session_reminder(session)
        two_day_reminder(personalize)
        two_day_count += 1

    if seven_day_count > 0 or two_day_count > 0:
        # printing summary, if anything happened
        print(
            "{0} ==================== CELERY TASK EXECUTION: Seven Day Reminders Sent: {1} Two Day Reminders Sent: {2} ====================".format(
                ConsoleMessageColor.CYAN, seven_day_count, two_day_count
            )
        )
