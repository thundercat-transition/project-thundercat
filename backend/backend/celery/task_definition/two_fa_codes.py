from __future__ import absolute_import, unicode_literals
from datetime import date
from backend.celery.task_definition.utils import ConsoleMessageColor


def handle_clear_expired_2fa_code():
    from user_management.user_management_models.user_2fa_tracking_model import (
        User2FATracking,
    )
    from django.utils import timezone

    print(
        "{0} ==================== CELERY TASK EXECUTION: CLEAR EXPIRED 2FA CODES ====================".format(
            ConsoleMessageColor.GREEN
        )
    )

    # get all expired codes
    expired_2fas = User2FATracking.objects.filter(expiry_date__lt=timezone.now())

    at_least_one_expired_2fa = False

    # delete each of them
    for expired_2fa in expired_2fas:
        user_id = expired_2fa.user_id
        expired_2fa.delete()
        # printing deletion successful messasge
        print(
            "{0} \t--> 2FA code for user {1} deleted successfully".format(
                ConsoleMessageColor.WHITE, user_id
            )
        )
        at_least_one_expired_2fa = True

    if at_least_one_expired_2fa:
        # printing successful messasge
        print(
            "{0} ==================== CELERY TASK EXECUTION: CLEAR EXPIRED 2FA CODES COMPLETED ====================".format(
                ConsoleMessageColor.CYAN
            )
        )
