from __future__ import absolute_import, unicode_literals
from django.db.models import Count
from pytz import timezone
from datetime import date, timedelta
from backend.celery.task_definition.utils import ConsoleMessageColor
from backend.views.gc_notify_view import (
    is_gc_notify_active,
    send_supervised_test_invitation_email,
)


def handle_reservation_code_email(email_data_array):
    from django.conf import settings

    site_protocol = settings.SITE_PROTOCOL
    site_domain = settings.SITE_DOMAIN

    # looping in email_data_array
    for data in email_data_array:
        context = {
            "first_name": data["assessment_process_assigned_test_specs_data"][
                "first_name"
            ],
            "last_name": data["assessment_process_assigned_test_specs_data"][
                "last_name"
            ],
            "email": data["assessment_process_assigned_test_specs_data"]["email"],
            "reservation_code": data["reservation_code"],
            "closing_date": data["assessment_process_assigned_test_specs_data"][
                "assessment_process_closing_date"
            ],
            "contact_email_for_candidates": data[
                "assessment_process_assigned_test_specs_data"
            ]["contact_email_for_candidates"],
            "test_skill_type_name_en": data[
                "assessment_process_assigned_test_specs_data"
            ]["test_skill_type_name_en"],
            "test_skill_type_name_fr": data[
                "assessment_process_assigned_test_specs_data"
            ]["test_skill_type_name_fr"],
            "test_skill_sub_type_name_en": data[
                "assessment_process_assigned_test_specs_data"
            ]["test_skill_sub_type_name_en"],
            "test_skill_sub_type_name_fr": data[
                "assessment_process_assigned_test_specs_data"
            ]["test_skill_sub_type_name_fr"],
            "assessment_process_dept_eabrv": data[
                "assessment_process_assigned_test_specs_data"
            ]["assessment_process_dept_eabrv"],
            "assessment_process_dept_fabrv": data[
                "assessment_process_assigned_test_specs_data"
            ]["assessment_process_dept_fabrv"],
            "assessment_process_dept_edesc": data[
                "assessment_process_assigned_test_specs_data"
            ]["assessment_process_dept_edesc"],
            "assessment_process_dept_fdesc": data[
                "assessment_process_assigned_test_specs_data"
            ]["assessment_process_dept_fdesc"],
            "formatted_url": "{}{}/oec-cat/login".format(site_protocol, site_domain),
        }

        # send via GC Notify
        if is_gc_notify_active():
            send_supervised_test_invitation_email(
                data["assessment_process_assigned_test_specs_data"][
                    "assessment_process_id"
                ],
                context,
            )

        # send using the internal mail server
        else:
            send_supervised_test_invitation_email_internal(context)


def send_supervised_test_invitation_email_internal(context):
    from django.conf import settings
    from django.core.mail import send_mail

    subject = "Invitation à planifier un test en personne – Invitation to schedule an in-person test"

    email_en_part = """
        Hello,
        \n
        We invite you to take the following supervised test:
        \n
        Test: {test_skill_type_name_en} - {test_skill_sub_type_name_en}
        Organization inviting you to take this test: {assessment_process_dept_edesc} ({assessment_process_dept_eabrv})
        Reservation code: {reservation_code}
        \n
        You must:
            •	Schedule your test by making your reservation as soon as possible to take the test in person
            •	Take your test no later than {closing_date}.
            •	If you have been involved in the development, administration or scoring of this test, or if you have in any other way been exposed to the contents of the test or to the scoring key, please do not complete your reservation and contact us at {contact_email_for_candidates} as soon as possible to let us know.
        \n
        Respect the 30-day waiting period if you have previously taken this test, regardless of its format (unsupervised, online or paper). If you retake this test before the end of the waiting period, your result will not be valid and a new 30-day waiting period will be set, starting from the date of your last test.
        \n
        Next steps:
            1.	Log in to the Candidate Assessment Tool ({formatted_url}) and try out the sample tests to adjust the application's display settings to your preferences. More information is available on the Candidate Assessment Tool information page (https://www.canada.ca/en/public-service-commission/services/second-language-testing-public-service/candidate-assessment-tool.html).
            2.	Select Reservations from the left-hand menu and enter the reservation code provided above.
            3.	Search for available test sessions or let us know that you:
                •	need to take your test with accommodations
                •	wish to use an existing valid result.
                •	do not wish to take this test
            4.	Once your booking is complete, you will receive a confirmation email with full details of your test session.
            5.	If you need to change your booking, you can do so in the Manage Reservation tab by {closing_date}.
        \n
        Accommodations:
        If you indicate the need to take this test with accommodations, the person who invited you to take the test will contact you to plan the next steps. You'll find more information about accommodations on the Assessment accommodations page (https://www.canada.ca/en/public-service-commission/services/assessment-accommodation-page.html).
        \n
        Test information:
        Useful information about the test you need to take is available on the following pages:
            •	Test of written expression (https://www.canada.ca/en/public-service-commission/services/second-language-testing-public-service/second-language-evaluation-writing.html)
            •	Test of reading comprehension (https://www.canada.ca/en/public-service-commission/services/second-language-testing-public-service/second-language-evaluation-reading.html)
        \n
        Existing valid results:
        If you wish to use an existing valid result and would like a copy, please complete this form: Confirmation of Previous Test Result - Canada.ca (https://www.canada.ca/en/public-service-commission/services/staffing-assessment-tools-resources/staffing-assessment-services-job-seekers-employees/confirmation-previous-test-result.html)
        \n
        Questions?
        Responses to this system-generated email will not be processed. Please contact us instead at {contact_email_for_candidates}.
        \n
        Thank you,
        """.format(
        formatted_url=context["formatted_url"],
        first_name=context["first_name"],
        last_name=context["last_name"],
        reservation_code=context["reservation_code"],
        closing_date=context["closing_date"],
        contact_email_for_candidates=context["contact_email_for_candidates"],
        test_skill_type_name_en=context["test_skill_type_name_en"],
        test_skill_sub_type_name_en=context["test_skill_sub_type_name_en"],
        assessment_process_dept_edesc=context["assessment_process_dept_edesc"],
        assessment_process_dept_eabrv=context["assessment_process_dept_eabrv"],
    )

    email_fr_part = """
        Bonjour,
        \n
        Nous vous invitons à passer le test supervisé suivant :
        \n
        Test : {test_skill_type_name_fr} - {test_skill_sub_type_name_fr}
        Organisation qui vous invite à faire ce test : {assessment_process_dept_fdesc} ({assessment_process_dept_fabrv})
        Code de réservation : {reservation_code}
        \n
        Vous devez :
            •	Planifier votre test en effectuant votre réservation dans les plus brefs délais afin de passer ce test en personne.
            •	Passer votre test au plus tard, le {closing_date}.
            •	Si vous avez participé à l’élaboration, à l’administration ou à la correction de ce test, ou si vous avez été exposé au contenu de ce test ou à la clé de correction, veuillez ne pas compléter votre réservation. Contactez-nous plutôt par courriel aussitôt que possible pour nous en aviser à : {contact_email_for_candidates}.
        \n
        Notez que vous devez respecter la période d’attente de 30 jours si vous avez fait ce test précédemment, peu importe son format (non supervisé ou en personne, en ligne ou papier). Si vous repassez ce test avant la fin de cette période d'attente, votre résultat ne sera pas valide et une nouvelle période d'attente de 30 jours sera fixée, à compter de la date de votre dernier test.
        \n
        Prochaines étapes :
            1.	Connectez-vous à l'Outil d'évaluation des candidats ({formatted_url}) et essayez les exemples de tests afin de régler les paramètres d'affichage de l’application selon vos préférences. Vous trouverez plus d’information à ce sujet sur la page d’information sur Outil d'évaluation des candidats (https://www.canada.ca/fr/commission-fonction-publique/services/evaluation-langue-seconde/outil-evaluation-candidats.html).
            2.	Sélectionnez l’option Réservations dans le menu de gauche et insérez le code de réservation fourni ci-dessus.
            3.	Recherchez les séances d’examen disponibles ou signalez-nous directement dans l’OEC que vous :
                •	devez passer votre test avec des mesures d’adaptation
                •	souhaitez utiliser un résultat valide existant.
                •	ne voulez pas passer ce test
            4.	Une fois votre réservation complétée, vous recevrez un courriel de confirmation contenant tous les détails sur votre séance d’examen.
            5.	Si vous devez modifier votre réservation, vous pouvez le faire dans l’onglet Gérer mes réservations d’ici le {closing_date}.
        \n
        Mesures d’adaptation :
        Si vous signalez le besoin de passer ce test avec des mesures d’adaptation, la personne qui vous a invité à passer le test communiquera avec vous afin de planifier les prochaines étapes. Vous trouverez plus d’information concernant les mesures d’adaptation sur la page Mesures d’adaptation en matière d’évaluation (https://www.canada.ca/fr/commission-fonction-publique/services/mesures-d-adaptation-matiere-evaluation.html).
        \n
        Renseignement sur le test :
        Des renseignements utiles sur le test que vous devez passer sont disponibles aux pages suivantes :
            •	Test d’expression écrite (https://www.canada.ca/fr/commission-fonction-publique/services/evaluation-langue-seconde/test-expression-ecrite1.html)
            •	Test de compréhension de l'écrit (https://www.canada.ca/fr/commission-fonction-publique/services/evaluation-langue-seconde/sle-test-comprehension-ecrit.html)
        \n
        Résultats valides existants :
        Si vous souhaitez utiliser un résultat valide existant et que vous désirez en obtenir une copie, veuillez remplir ce formulaire : Confirmation de résultats d'examens - Canada.ca (https://www.canada.ca/fr/commission-fonction-publique/services/outils-ressources-dotation-evaluation/chercheurs-emploi-fonctionnaires/confirmation-resultats-examens.html)
        \n
        Des questions ?
        Les réponses à ce courriel généré par le système ne seront pas traitées. Veuillez plutôt communiquer avec nous à {contact_email_for_candidates}.
        \n
        Merci,
        """.format(
        formatted_url=context["formatted_url"],
        first_name=context["first_name"],
        last_name=context["last_name"],
        reservation_code=context["reservation_code"],
        closing_date=context["closing_date"],
        contact_email_for_candidates=context["contact_email_for_candidates"],
        test_skill_type_name_fr=context["test_skill_type_name_fr"],
        test_skill_sub_type_name_fr=context["test_skill_sub_type_name_fr"],
        assessment_process_dept_fdesc=context["assessment_process_dept_fdesc"],
        assessment_process_dept_fabrv=context["assessment_process_dept_fabrv"],
    )

    email_plaintext_message = """
        (English message follows)
        \n
        {email_fr_part}
        \n
        \n
        (Le français précède)
        \n
        {email_en_part}
        """.format(
        email_en_part=email_en_part, email_fr_part=email_fr_part
    )

    send_mail(
        # title:
        subject,
        # message:
        email_plaintext_message,
        # from (will be overridden by the real email in Dev/Test/Prod):
        settings.EMAIL_HOST_USER,
        # to:
        [context["email"]],
    )


def handle_delete_specific_test_session_email(
    email_data_array,
):
    from django.conf import settings
    from django.core.mail import send_mail
    from backend.views.gc_notify_view import (
        is_gc_notify_active,
        delete_specific_test_session,
    )

    # looping in email_data_array
    for data in email_data_array:
        context = {
            "first_name": data["cat_user_data"]["first_name"],
            "last_name": data["cat_user_data"]["last_name"],
            "email": data["cat_user_data"]["email"],
            "reservation_code": data["reservation_code"],
            "closing_date": data["assessment_process_assigned_test_specs_data"][
                "assessment_process_closing_date"
            ],
            "test_skill_type_name_en": data[
                "assessment_process_assigned_test_specs_data"
            ]["test_skill_type_name_en"],
            "test_skill_type_name_fr": data[
                "assessment_process_assigned_test_specs_data"
            ]["test_skill_type_name_fr"],
            "test_skill_sub_type_name_en": data[
                "assessment_process_assigned_test_specs_data"
            ]["test_skill_sub_type_name_en"],
            "test_skill_sub_type_name_fr": data[
                "assessment_process_assigned_test_specs_data"
            ]["test_skill_sub_type_name_fr"],
            "contact_email_for_candidates": data[
                "assessment_process_assigned_test_specs_data"
            ]["contact_email_for_candidates"],
            "test_session_date": data["test_session_data"]["date"],
            "test_session_start_time": data["test_session_data"][
                "simplified_start_time"
            ],
        }

        # if GC Notify is active, use it
        if is_gc_notify_active():
            delete_specific_test_session(context)
        # otherwise, use the old way
        else:
            subject = "Annulation de votre séance de test / Test session cancellation"

            email_en_part = """
                Hello,
                \n
                Please note that the test session you booked has been cancelled by the test center. Your reservation code has been released so that you can reuse it to book a new test session.
                \n
                You must reschedule your test by making your reservation as soon as possible to take this it in-person by {closing_date} at the latest.
                \n
                Test: {test_skill_type_name_en} - {test_skill_sub_type_name_en}
                Date and time of cancelled test session: {test_session_date} {test_session_start_time}
                \n
                Reservation code: {reservation_code}
                \n
                Next steps:
                \n
                1. Log on to the Candidate Assessment Tool: https://www5.psc-cfp.gc.ca/oec-cat/login.
                2. Select the Reservations option in the left-hand menu.
                3. Search for available test sessions by selecting the search icon next to the reservation code in the My reservation codes section.
                4. Once your reservation is complete, you will receive a confirmation email containing full details of your test session.
                5. If you need to modify your reservation, you can do so in the Manage my reservations tab by {closing_date}.
                \n
                Any questions?
                \n
                Responses to this system-generated email will not be processed. Please contact the organization that invited you to take this test at {contact_email_for_candidates}.
                \n
                Thank you,
                """.format(
                first_name=context["first_name"],
                last_name=context["last_name"],
                reservation_code=context["reservation_code"],
                closing_date=context["closing_date"],
                test_skill_type_name_en=context["test_skill_type_name_en"],
                test_skill_sub_type_name_en=context["test_skill_sub_type_name_en"],
                contact_email_for_candidates=context["contact_email_for_candidates"],
                test_session_date=context["test_session_date"],
                test_session_start_time=context["test_session_start_time"],
            )

            email_fr_part = """
                Bonjour,
                \n
                Veuillez noter que la séance de test pour laquelle vous aviez fait une réservation a été annulée par le centre de test. Votre code de réservation a été libéré afin que vous puissiez le réutiliser pour réserver une nouvelle séance de test.
                \n
                Vous devez planifier votre test de nouveau en effectuant votre réservation dans les plus brefs délais afin de passer ce test en personne au plus tard le {closing_date}.
                \n
                Test: {test_skill_type_name_fr} - {test_skill_sub_type_name_fr}
                Date et heure de séance de test annulée: {test_session_date} {test_session_start_time}
                \n
                Code de réservation  : {reservation_code}
                \n
                Prochaines étapes :
                \n
                1. Connectez-vous à l'Outil d'évaluation des candidats : https://www5.psc-cfp.gc.ca/oec-cat/login.
                2. Sélectionnez l’option Réservations dans le menu de gauche.
                3. Rechercher les séances de tests disponibles en sélectionnant l’icône chercher à côté du code de réservation dans la section Mes codes de réservations.
                4. Une fois votre réservation complétée, vous recevrez un courriel de confirmation contenant tous les détails sur votre séance d’examen.
                5. Si vous devez modifier votre réservation, vous pouvez le faire dans l’onglet Gérer mes réservations d’ici le {closing_date}.
                \n
                Des questions ?
                \n
                Les réponses à ce courriel généré par le système ne seront pas traitées. Veuillez plutôt communiquer avec l’organisation qui vous a invité à passer ce test à {contact_email_for_candidates}.
                \n
                Merci,
                """.format(
                first_name=context["first_name"],
                last_name=context["last_name"],
                reservation_code=context["reservation_code"],
                closing_date=context["closing_date"],
                test_skill_type_name_fr=context["test_skill_type_name_fr"],
                test_skill_sub_type_name_fr=context["test_skill_sub_type_name_fr"],
                contact_email_for_candidates=context["contact_email_for_candidates"],
                test_session_date=context["test_session_date"],
                test_session_start_time=context["test_session_start_time"],
            )

            email_plaintext_message = """
                (English follows)
                \n
                {email_fr_part}
                \n
                \n
                {email_en_part}
                """.format(
                email_en_part=email_en_part, email_fr_part=email_fr_part
            )

            send_mail(
                # title:
                subject,
                # message:
                email_plaintext_message,
                # from (will be overridden by the real email in Dev/Test/Prod):
                settings.EMAIL_HOST_USER,
                # to:
                [context["email"]],
            )


def handle_reservation_code_withdraw_no_booking_email(
    context,
):
    from django.conf import settings
    from django.core.mail import send_mail
    from backend.views.gc_notify_view import (
        is_gc_notify_active,
        reservation_code_withdraw_no_booking,
        candidate_withdrawl_notification,
    )

    # call gc notify if it is active
    if is_gc_notify_active():
        personalization = {
            "first_name": context["first_name"],
            "last_name": context["last_name"],
            "reservation_code": context["reservation_code"],
            "assessment_process_reference_number": context[
                "assessment_process_reference_number"
            ],
            "closing_date": context["closing_date"].strftime("%Y/%m/%d"),
            "test_skill_type_name_en": context["test_skill_type_name_en"],
            "test_skill_sub_type_name_en": context["test_skill_sub_type_name_en"],
            "action_en": context["action_en"],
            "contact_email_for_candidates": context["contact_email_for_candidates"],
            "test_skill_type_name_fr": context["test_skill_type_name_fr"],
            "test_skill_sub_type_name_fr": context["test_skill_sub_type_name_fr"],
            "action_fr": context["action_fr"],
            "dept_fdesc": context["dept_fdesc"],
            "dept_fabrv": context["dept_fabrv"],
            "dept_edesc": context["dept_edesc"],
            "dept_eabrv": context["dept_eabrv"],
        }
        # email HR
        reservation_code_withdraw_no_booking(
            context["contact_email_for_candidates"], personalization
        )
        # email candidate
        candidate_withdrawl_notification(context["email"], personalization)
        # email everyone else?!
    # otherwise, call the old method
    else:
        subject = "Avis de retrait d'un processus d'évaluation  / Notice of withdrawal from an assessment process"

        email_en_part = """
            Hello,
            \n
            The following candidate has indicated that they wish to withdraw from the assessment process for which you sent them a test invitation.
                •	Candidate: {first_name} {last_name}
                •	Test: {test_skill_type_name_en} - {test_skill_sub_type_name_en}
                •	Process reference number: {assessment_process_reference_number}
            \n
            We recommend that you contact the candidate to confirm this information.
            \n
            Thank you,
            """.format(
            first_name=context["first_name"],
            last_name=context["last_name"],
            reservation_code=context["reservation_code"],
            assessment_process_reference_number=context[
                "assessment_process_reference_number"
            ],
            closing_date=context["closing_date"],
            test_skill_type_name_en=context["test_skill_type_name_en"],
            test_skill_sub_type_name_en=context["test_skill_sub_type_name_en"],
            action_en=context["action_en"],
            contact_email_for_candidates=context["contact_email_for_candidates"],
        )

        email_fr_part = """
            Bonjour,
            \n
            Le candidat suivant a signalé  qu’il désire se retirer du processus d’évaluation pour lequel vous avez envoyé une invitation à un test.
                •	Candidat : {first_name} {last_name}
                •	Test : {test_skill_type_name_fr} - {test_skill_sub_type_name_fr}
                •	Numéro de référence du processus : {assessment_process_reference_number}
            \n
            Nous vous recommandons de contacter le candidat afin de confirmer cette information.
            \n
            Merci,
            """.format(
            first_name=context["first_name"],
            last_name=context["last_name"],
            reservation_code=context["reservation_code"],
            assessment_process_reference_number=context[
                "assessment_process_reference_number"
            ],
            closing_date=context["closing_date"],
            test_skill_type_name_fr=context["test_skill_type_name_fr"],
            test_skill_sub_type_name_fr=context["test_skill_sub_type_name_fr"],
            action_fr=context["action_fr"],
            contact_email_for_candidates=context["contact_email_for_candidates"],
        )

        email_plaintext_message = """
            (English follows)
            \n
            {email_fr_part}
            \n
            \n
            (Le français précède)
            \n
            {email_en_part}
            """.format(
            email_en_part=email_en_part, email_fr_part=email_fr_part
        )

        send_mail(
            # title:
            subject,
            # message:
            email_plaintext_message,
            # from (will be overridden by the real email in Dev/Test/Prod):
            settings.EMAIL_HOST_USER,
            # to:
            [context["contact_email_for_candidates"]],
        )


def handle_reservation_code_withdraw_from_booked_session_email(
    context,
):
    from django.conf import settings
    from django.core.mail import send_mail
    from backend.views.gc_notify_view import (
        is_gc_notify_active,
        reservation_code_withdraw_from_booked_session,
    )

    # call gc notify if it is active
    if is_gc_notify_active():
        personalization = {
            "first_name": context["first_name"],
            "last_name": context["last_name"],
            "reservation_code": context["reservation_code"],
            "assessment_process_reference_number": context[
                "assessment_process_reference_number"
            ],
            "closing_date": context["closing_date"].strftime("%Y/%m/%d"),
            "test_skill_type_name_en": context["test_skill_type_name_en"],
            "test_skill_sub_type_name_en": context["test_skill_sub_type_name_en"],
            "action_en": context["action_en"],
            "contact_email_for_candidates": context["contact_email_for_candidates"],
            "test_skill_type_name_fr": context["test_skill_type_name_fr"],
            "test_skill_sub_type_name_fr": context["test_skill_sub_type_name_fr"],
            "action_fr": context["action_fr"],
            "dept_fdesc": context["dept_fdesc"],
            "dept_fabrv": context["dept_fabrv"],
            "dept_edesc": context["dept_edesc"],
            "dept_eabrv": context["dept_eabrv"],
        }
        reservation_code_withdraw_from_booked_session(
            context["contact_email_for_candidates"], personalization
        )
    # otherwise call the old method
    else:
        subject = "Avis de retrait d'un processus d'évaluation  / Notice of withdrawal from an assessment process"

        email_en_part = """
            Hello,
            \n
            The following candidate has indicated that they wish to withdraw from the assessment process for which you sent them a test invitation.
                •	Candidate: {first_name} {last_name}
                •	Test: {test_skill_type_name_en} - {test_skill_sub_type_name_en}
                •	Process reference number: {assessment_process_reference_number}
            \n
            We recommend that you contact the candidate to confirm this information.
            \n
            Thank you,
            """.format(
            first_name=context["first_name"],
            last_name=context["last_name"],
            reservation_code=context["reservation_code"],
            assessment_process_reference_number=context[
                "assessment_process_reference_number"
            ],
            closing_date=context["closing_date"],
            test_skill_type_name_en=context["test_skill_type_name_en"],
            test_skill_sub_type_name_en=context["test_skill_sub_type_name_en"],
            action_en=context["action_en"],
            contact_email_for_candidates=context["contact_email_for_candidates"],
        )

        email_fr_part = """
            Bonjour,
            \n
            Le candidat suivant a signalé  qu’il désire se retirer du processus d’évaluation pour lequel vous avez envoyé une invitation à un test.
                •	Candidat : {first_name} {last_name}
                •	Test : {test_skill_type_name_fr} - {test_skill_sub_type_name_fr}
                •	Numéro de référence du processus : {assessment_process_reference_number}
            \n
            Nous vous recommandons de contacter le candidat afin de confirmer cette information.
            \n
            Merci,
            """.format(
            first_name=context["first_name"],
            last_name=context["last_name"],
            reservation_code=context["reservation_code"],
            assessment_process_reference_number=context[
                "assessment_process_reference_number"
            ],
            closing_date=context["closing_date"],
            test_skill_type_name_fr=context["test_skill_type_name_fr"],
            test_skill_sub_type_name_fr=context["test_skill_sub_type_name_fr"],
            action_fr=context["action_fr"],
            contact_email_for_candidates=context["contact_email_for_candidates"],
        )

        email_plaintext_message = """
            (English follows)
            \n
            {email_fr_part}
            \n
            \n
            (Le français précède)
            \n
            {email_en_part}
            """.format(
            email_en_part=email_en_part, email_fr_part=email_fr_part
        )

        send_mail(
            # title:
            subject,
            # message:
            email_plaintext_message,
            # from (will be overridden by the real email in Dev/Test/Prod):
            settings.EMAIL_HOST_USER,
            # to:
            [context["contact_email_for_candidates"]],
        )


def handle_reservation_code_booking_email(context):
    from django.conf import settings
    from django.core.mail import send_mail
    from backend.views.gc_notify_view import (
        is_gc_notify_active,
        reservation_code_booking,
    )

    site_protocol = settings.SITE_PROTOCOL
    site_domain = settings.SITE_DOMAIN

    formatted_url = "{}{}/oec-cat/login".format(site_protocol, site_domain)

    # start_time conversion (Canada/Eastern)
    local_start_time = context["start_time"].astimezone(timezone(settings.TIME_ZONE))

    formatted_start_time = "{:d}:{:02d}".format(
        local_start_time.hour, local_start_time.minute
    )

    # end_time conversion (Canada/Eastern)
    local_end_time = context["end_time"].astimezone(timezone(settings.TIME_ZONE))

    formatted_end_time = "{:d}:{:02d}".format(
        local_end_time.hour, local_end_time.minute
    )
    duration = context["end_time"] - context["start_time"]
    formatted_duration = int(duration.total_seconds() / 60)

    # call gc notify if it is active
    if is_gc_notify_active():
        personalization = {
            "formatted_url": formatted_url,
            "first_name": context["first_name"],
            "last_name": context["last_name"],
            "reservation_code": context["reservation_code"],
            "closing_date": context["closing_date"].strftime("%Y/%m/%d"),
            "test_skill_type_name_en": context["test_skill_type_name_en"],
            "test_skill_sub_type_name_en": context["test_skill_sub_type_name_en"],
            "dept_edesc": context["dept_edesc"],
            "dept_eabrv": context["dept_eabrv"],
            "contact_email_for_candidates": context["contact_email_for_candidates"],
            "action_en": context["action_en"],
            "date": context["date"].strftime("%Y/%m/%d"),
            "formatted_start_time": formatted_start_time,
            "formatted_end_time": formatted_end_time,
            "formatted_duration": formatted_duration,
            "test_center_name": context["test_center_name"],
            "room_name": context["room_name"],
            # English Address
            "test_center_address_en": context["test_center_address_text"]["en"][0][
                "text"
            ],
            # French Address
            "test_center_address_fr": context["test_center_address_text"]["fr"][0][
                "text"
            ],
            # English City
            "test_center_city_en": context["test_center_city_text"]["en"][0]["text"],
            # French City
            "test_center_city_fr": context["test_center_city_text"]["fr"][0]["text"],
            # English Province
            "test_center_province_en": context["test_center_province_text"]["en"][0][
                "text"
            ],
            # French Province
            "test_center_province_fr": context["test_center_province_text"]["fr"][0][
                "text"
            ],
            "test_center_postal_code": context["test_center_postal_code"],
            # English Country
            "tc_country_eabrv": context["tc_country_eabrv"],
            "tc_country_edesc": context["tc_country_edesc"],
            # French Country
            "tc_country_fabrv": context["tc_country_fabrv"],
            "tc_country_fdesc": context["tc_country_fdesc"],
            "tc_dept_eabrv": context["tc_dept_eabrv"],
            "tc_dept_edesc": context["tc_dept_edesc"],
            "booking_delay": context["booking_delay"],
            "test_skill_type_name_fr": context["test_skill_type_name_fr"],
            "test_skill_sub_type_name_fr": context["test_skill_sub_type_name_fr"],
            "dept_fdesc": context["dept_fdesc"],
            "dept_fabrv": context["dept_fabrv"],
            "action_fr": context["action_fr"],
            "tc_dept_fabrv": context["tc_dept_fabrv"],
            "tc_dept_fdesc": context["tc_dept_fdesc"],
            "email": context["email"],
        }
        reservation_code_booking(personalization)
    # otherwise, call the old method
    else:
        subject = "Confirmation de réservation – Réservation confirmation"

        email_en_part = """
            Good day,
            \n
            We have successfully registered your reservation.
            \n
            Please read this e-mail carefully, as it contains important information that will provide you with all the information you need to take your test and fully demonstrate your abilities.
            \n
            SUMMARY:
            Organization inviting you to take this test: {dept_edesc} ({dept_eabrv})
            Test: {test_skill_type_name_en} - {test_skill_sub_type_name_en}
            Date and Time:  {date} {formatted_start_time} - {formatted_end_time}
            Test Session duration: {formatted_duration} minutes (including 20 to 30 minutes for administrative procedures)
            Location: {tc_dept_edesc} ({tc_dept_eabrv})
            {test_center_address}, {room_name}
            {test_center_city}
            {test_center_province}, {test_center_postal_code}
            \n
            Please note that you will be able to change your booking in the Candidate Assessment Tool ({formatted_url}) up to {booking_delay} hours before the test date and time. If your test is scheduled at a Public Service Commission office, the organization that invited you to take the test will incur additional charges if you do not attend the test session. Remember that you must complete your test no later than {closing_date}.
            \n
            BEFORE YOUR TEST
                •	Try out the sample tests to adjust the display settings of the Candidate Assessment Tool to your preferences. You will find information to help you use the various features of the testing platform on the Candidate Assessment Tool information page (https://www.canada.ca/en/public-service-commission/services/second-language-testing-public-service/candidate-assessment-tool.html).
                •	Make a note of your account e-mail address and password
            \n
            THE DAY OF YOUR TEST
                1.	Arrive at least 15 to 20 minutes prior to your test time to complete your registration. If you arrive late you will not be admitted to the test session and you will need to contact the organization at this email address {contact_email_for_candidates} to see if the test can be rescheduled.
                2.	Bring two pieces of valid government-issued photo ID (a copy or photo of the ID will not be accepted).
                3.	Bring your login information for the Candidate Evaluation Tool. Note that if you enter an incorrect password or email address 4 times, your account will be locked, and you will need to reset your password using your mobile device.
            \n
            QUESTIONS?
            Responses to this system-generated email will not be processed. Please contact us instead at {contact_email_for_candidates}.
            \n
            Thank you,
            """.format(
            formatted_url=formatted_url,
            first_name=context["first_name"],
            last_name=context["last_name"],
            reservation_code=context["reservation_code"],
            closing_date=context["closing_date"],
            test_skill_type_name_en=context["test_skill_type_name_en"],
            test_skill_sub_type_name_en=context["test_skill_sub_type_name_en"],
            dept_edesc=context["dept_edesc"],
            dept_eabrv=context["dept_eabrv"],
            contact_email_for_candidates=context["contact_email_for_candidates"],
            action_en=context["action_en"],
            date=context["date"],
            formatted_start_time=formatted_start_time,
            formatted_end_time=formatted_end_time,
            formatted_duration=formatted_duration,
            test_center_name=context["test_center_name"],
            room_name=context["room_name"],
            test_center_address=context["test_center_address_text"]["en"][0]["text"],
            test_center_city=context["test_center_city_text"]["en"][0]["text"],
            test_center_province=context["test_center_province_text"]["en"][0]["text"],
            test_center_postal_code=context["test_center_postal_code"],
            tc_country_eabrv=context["tc_country_eabrv"],
            tc_country_edesc=context["tc_country_edesc"],
            tc_dept_eabrv=context["tc_dept_eabrv"],
            tc_dept_edesc=context["tc_dept_edesc"],
            booking_delay=context["booking_delay"],
        )

        email_fr_part = """
            Bonjour,
            \n
            Nous avons bien enregistré votre réservation.
            \n
            Lisez bien ce courriel, il contient des renseignements importants qui vous permettront de disposer de toute l’information dont vous aurez besoin pour passer votre test et démontrer pleinement vos capacités.
            \n
            RÉCAPITULATIF :
            Organisation qui vous invite à faire ce test : {dept_fdesc} ({dept_fabrv})
            Test : {test_skill_type_name_fr} - {test_skill_sub_type_name_fr}
            Date et Heure : {date} {formatted_start_time} - {formatted_end_time}
            Durée de la séance de test : {formatted_duration} minutes (incluant 20 à 30 minutes pour les procédures administratives)
            Lieu : {tc_dept_fdesc} ({tc_dept_fabrv})
            {test_center_address}, {room_name}
            {test_center_city}
            {test_center_province}, {test_center_postal_code}
            \n
            Notez que vous pourrez modifier votre réservation dans l’Outil d’évaluation des candidats ({formatted_url}) jusqu’à {booking_delay} heures avant la date et l’heure du test. Si votre test est prévu dans un des bureaux de la Commission de la fonction publique, l’organisation qui vous a invité à faire le test devra assumer des frais supplémentaires si vous ne vous présentez pas à la session d’examen. Rappelez-vous que vous devez compléter votre test au plus tard le {closing_date}.
            \n
            AVANT VOTRE TEST
                •	Essayez les exemples de test afin de régler les paramètres d'affichage de l'Outil d’évaluation des candidats selon vos préférences. Vous trouverez des renseignements pour vous aider à utiliser les différentes fonctionnalités de la plateforme de test sur la page d'information sur l'Outil d'évaluation des candidats (https://www.canada.ca/fr/commission-fonction-publique/services/evaluation-langue-seconde/outil-evaluation-candidats.html).
                •	Prenez note de l’adresse courriel et du mot de passe de votre compte
            \n
            LE JOUR DE VOTRE EXAMEN
                1.	Arrivez au moins 15 à 20 minutes avant à l’heure de votre test afin de compléter votre inscription. Si vous arriver en retard, vous ne pourrez pas être admis à la séance de test et vous devrez communiquer avec l’organisation à cette adresse courriel {contact_email_for_candidates} pour vérifier si le test peut être reporter.
                2.	Apportez deux pièces d’identité valides avec photo émises par le gouvernement (des copies ou photos de pièces d’identité ne seront pas acceptées).
                3.	Apportez vos informations de connexion pour l’Outil d’évaluation des candidats. Notez que si vous saisissez un mot de passe ou une adresse courriel incorrect 4 fois, votre compte sera verrouillé et vous devrez réinitialiser votre mot de passe en utilisant votre appareil mobile.
            \n
            DES QUESTIONS ?
            Les réponses à ce courriel généré par le système ne seront pas traitées. Veuillez plutôt communiquer avec nous à {contact_email_for_candidates}.
            \n
            Merci,
            """.format(
            formatted_url=formatted_url,
            first_name=context["first_name"],
            last_name=context["last_name"],
            reservation_code=context["reservation_code"],
            closing_date=context["closing_date"],
            test_skill_type_name_fr=context["test_skill_type_name_fr"],
            test_skill_sub_type_name_fr=context["test_skill_sub_type_name_fr"],
            dept_fdesc=context["dept_fdesc"],
            dept_fabrv=context["dept_fabrv"],
            contact_email_for_candidates=context["contact_email_for_candidates"],
            action_fr=context["action_fr"],
            date=context["date"],
            formatted_start_time=formatted_start_time,
            formatted_end_time=formatted_end_time,
            formatted_duration=formatted_duration,
            test_center_name=context["test_center_name"],
            room_name=context["room_name"],
            test_center_address=context["test_center_address_text"]["fr"][0]["text"],
            test_center_city=context["test_center_city_text"]["fr"][0]["text"],
            test_center_province=context["test_center_province_text"]["fr"][0]["text"],
            test_center_postal_code=context["test_center_postal_code"],
            tc_country_fabrv=context["tc_country_fabrv"],
            tc_country_fdesc=context["tc_country_fdesc"],
            tc_dept_fabrv=context["tc_dept_fabrv"],
            tc_dept_fdesc=context["tc_dept_fdesc"],
            booking_delay=context["booking_delay"],
        )

        email_plaintext_message = """
            (English message follows)
            \n
            {email_fr_part}
            \n
            \n
            (Le français précède)
            \n
            {email_en_part}
            """.format(
            email_en_part=email_en_part, email_fr_part=email_fr_part
        )

        send_mail(
            # title:
            subject,
            # message:
            email_plaintext_message,
            # from (will be overridden by the real email in Dev/Test/Prod):
            settings.EMAIL_HOST_USER,
            # to:
            [context["email"]],
        )


def handle_reservation_code_book_later_email(context):
    from django.conf import settings
    from django.core.mail import send_mail
    from backend.views.gc_notify_view import (
        is_gc_notify_active,
        reservation_code_book_later,
    )

    site_protocol = settings.SITE_PROTOCOL
    site_domain = settings.SITE_DOMAIN

    formatted_url = "{}{}/oec-cat/login".format(site_protocol, site_domain)

    # call gc notify if it is active
    if is_gc_notify_active():
        personalization = {
            "formatted_url": formatted_url,
            "first_name": context["first_name"],
            "last_name": context["last_name"],
            "reservation_code": context["reservation_code"],
            "closing_date": context["closing_date"],
            "contact_email_for_candidates": context["contact_email_for_candidates"],
            "test_skill_type_name_en": context["test_skill_type_name_en"],
            "test_skill_sub_type_name_en": context["test_skill_sub_type_name_en"],
            "action_en": context["action_en"],
            "test_center_name": context["test_center_name"],
            "room_name": context["room_name"],
            # English Address
            "test_center_address_en": context["test_center_address_text"]["en"][0][
                "text"
            ],
            # French Address
            "test_center_address_fr": context["test_center_address_text"]["fr"][0][
                "text"
            ],
            # English City
            "test_center_city_en": context["test_center_city_text"]["en"][0]["text"],
            # French City
            "test_center_city_fr": context["test_center_city_text"]["fr"][0]["text"],
            # English Province
            "test_center_province_en": context["test_center_province_text"]["en"][0][
                "text"
            ],
            # French Province
            "test_center_province_fr": context["test_center_province_text"]["fr"][0][
                "text"
            ],
            "test_center_postal_code": context["test_center_postal_code"],
            # English Country
            "country_edesc": context["country_edesc"],
            "country_eabrv": context["country_eabrv"],
            # French Country
            "country_fdesc": context["country_fdesc"],
            "country_fabrv": context["country_fabrv"],
            "dept_edesc": context["dept_edesc"],
            "dept_eabrv": context["dept_eabrv"],
            "formatted_url": formatted_url,
            "test_skill_type_name_fr": context["test_skill_type_name_fr"],
            "test_skill_sub_type_name_fr": context["test_skill_sub_type_name_fr"],
            "action_fr": context["action_fr"],
            "dept_fdesc": context["dept_fdesc"],
            "dept_fabrv": context["dept_fabrv"],
            "email": context["email"],
        }
        reservation_code_book_later(personalization)
    # otherwise use the old method
    else:

        subject = "Confirmation d’annulation de réservation / Confirmation of reservation cancellation"

        email_en_part = """
            Hello,
            \n
            We have successfully registered the cancellation of your reservation. You will be able to make a new reservation by visiting the ‘Reservations’ tab in the Candidate Assessment Tool.
            \n
            Reminder
            Test: {test_skill_type_name_en} - {test_skill_sub_type_name_en}
            Organization inviting you to take this test: {dept_edesc} ({dept_eabrv})
            Reservation code: {reservation_code}
            \n
            You must:
                •	Schedule your test by making your reservation as soon as possible to take the test in person
                •	Take your test no later than {closing_date}.
                •	If you have been involved in the development, administration or scoring of this test, or if you have in any other way been exposed to the contents of the test or to the scoring key, please do not complete your reservation and contact us at {contact_email_for_candidates} as soon as possible to let us know.
            \n
            Respect the 30-day waiting period if you have previously taken this test, regardless of its format (unsupervised, online or paper). If you retake this test before the end of the waiting period, your result will not be valid and a new 30-day waiting period will be set, starting from the date of your last test.
            \n
            Next steps:
                1.	Log in to the Candidate Assessment Tool: {formatted_url}
                2.	Try out the sample tests to adjust the application's display settings to your preferences. More information is available on the following page: https://www.canada.ca/en/public-service-commission/services/second-language-testing-public-service/candidate-assessment-tool.html.
                3.	Select Reservations from the left-hand menu where you can find your tests under “My Reservation Codes”.
                4.	Search for available test sessions or let us know that you:
                    •	need to take your test with accommodations
                    •	wish to use an existing valid result.
                    •	do not wish to take this test
                5.	Once your booking is complete, you will receive a confirmation email with full details of your test session.
                6.	If you need to change your booking, you can do so in the Manage Reservation tab by {closing_date}.
            \n
            Accommodations
            If you indicate the need to take this test with accommodations, the person who invited you to take the test will contact you to plan the next steps. You'll find more information about accommodations on the following page: https://www.canada.ca/en/public-service-commission/services/assessment-accommodation-page.html.
            \n
            Test information:
            Useful information about the test you need to take is available on the following pages:
                •	https://www.canada.ca/en/public-service-commission/services/second-language-testing-public-service/second-language-evaluation-writing.html
                •	https://www.canada.ca/en/public-service-commission/services/second-language-testing-public-service/second-language-evaluation-reading.html
            \n
            Existing valid results:
            If you wish to use an existing valid result and would like a copy, please complete this form available on the following page: https://www.canada.ca/en/public-service-commission/services/staffing-assessment-tools-resources/staffing-assessment-services-job-seekers-employees/confirmation-previous-test-result.html.
            \n
            Questions?
            Responses to this system-generated email will not be processed. Please contact us instead at {contact_email_for_candidates}.
            \n
            Thank you,
            """.format(
            formatted_url=formatted_url,
            first_name=context["first_name"],
            last_name=context["last_name"],
            reservation_code=context["reservation_code"],
            closing_date=context["closing_date"],
            contact_email_for_candidates=context["contact_email_for_candidates"],
            test_skill_type_name_en=context["test_skill_type_name_en"],
            test_skill_sub_type_name_en=context["test_skill_sub_type_name_en"],
            action_en=context["action_en"],
            test_center_name=context["test_center_name"],
            room_name=context["room_name"],
            test_center_address=context["test_center_address_text"]["en"][0]["text"],
            test_center_city=context["test_center_city_text"]["en"][0]["text"],
            test_center_province=context["test_center_province_text"]["en"][0]["text"],
            test_center_postal_code=context["test_center_postal_code"],
            country_edesc=context["country_edesc"],
            country_eabrv=context["country_eabrv"],
            dept_edesc=context["dept_edesc"],
            dept_eabrv=context["dept_eabrv"],
        )

        email_fr_part = """
            Bonjour,
            \n
            Nous avons bien enregistré l’annulation de votre réservation. Vous pourrez effectuer une nouvelle réservation en consultant l'onglet "Réservations" de l'Outil d'évaluation des candidats.
            \n
            Rappel :
            Test : {test_skill_type_name_fr} - {test_skill_sub_type_name_fr}
            Organisation qui vous invite à faire ce test : {dept_fdesc} ({dept_fabrv})
            Code de réservation : {reservation_code}
            \n
            Vous devez :
                •	Planifier votre test en effectuant votre réservation dans les plus brefs délais afin de passer ce test en personne.
                •	Passer votre test au plus tard, le {closing_date}.
                •	Si vous avez participé à l’élaboration, à l’administration ou à la correction de ce test, ou si vous avez été exposé au contenu de ce test ou à la clé de correction, veuillez ne pas compléter votre réservation. Contactez-nous plutôt par courriel aussitôt que possible pour nous en aviser à : {contact_email_for_candidates}.
            \n
            Notez que vous devez respecter la période d’attente de 30 jours si vous avez fait ce test précédemment, peu importe son format (non supervisé ou en personne, en ligne ou papier). Si vous repassez ce test avant la fin de cette période d'attente, votre résultat ne sera pas valide et une nouvelle période d'attente de 30 jours sera fixée, à compter de la date de votre dernier test.
            \n
            Prochaines étapes :
                1.	Connectez-vous à l'Outil d'évaluation des candidats : {formatted_url}.
                2.	Essayez les exemples de tests afin de régler les paramètres d'affichage de l’application selon vos préférences. Vous trouverez plus d’information à ce sujet sur la page suivante : https://www.canada.ca/fr/commission-fonction-publique/services/evaluation-langue-seconde/outil-evaluation-candidats.html.
                3.	Sélectionnez l’option Réservations dans le menu de gauche et insérez le code de réservation fourni ci-dessus.
                4.	Recherchez les séances d’examen disponibles ou signalez-nous directement dans l’OEC que vous :
                    •	devez passer votre test avec des mesures d’adaptation
                    •	souhaitez utiliser un résultat valide existant.
                    •	ne voulez pas passer ce test
                5.	Une fois votre réservation complétée, vous recevrez un courriel de confirmation contenant tous les détails sur votre séance d’examen.
                6.	Si vous devez modifier votre réservation, vous pouvez le faire dans l’onglet Gérer mes réservations d’ici le {closing_date}.
            \n
            Mesures d’adaptation :
            Si vous signalez le besoin de passer ce test avec des mesures d’adaptation, la personne qui vous a invité à passer le test communiquera avec vous afin de planifier les prochaines étapes. Vous trouverez plus d’information concernant les mesures d’adaptation sur la page suivante : https://www.canada.ca/fr/commission-fonction-publique/services/mesures-d-adaptation-matiere-evaluation.html.
            \n
            Renseignement sur le test :
            Des renseignements utiles sur le test que vous devez passer sont disponibles aux pages suivantes :
                •	https://www.canada.ca/fr/commission-fonction-publique/services/evaluation-langue-seconde/test-expression-ecrite1.html
                •	https://www.canada.ca/fr/commission-fonction-publique/services/evaluation-langue-seconde/sle-test-comprehension-ecrit.html
            \n
            Résultats valides existants :
            Si vous souhaitez utiliser un résultat valide existant et que vous désirez en obtenir une copie, veuillez remplir le formulaire sur la page suivante : https://www.canada.ca/fr/commission-fonction-publique/services/outils-ressources-dotation-evaluation/chercheurs-emploi-fonctionnaires/confirmation-resultats-examens.html.
            \n
            Des questions ?
            Les réponses à ce courriel généré par le système ne seront pas traitées. Veuillez plutôt communiquer avec nous à {contact_email_for_candidates}.
            \n
            Merci,
            """.format(
            formatted_url=formatted_url,
            first_name=context["first_name"],
            last_name=context["last_name"],
            reservation_code=context["reservation_code"],
            closing_date=context["closing_date"],
            contact_email_for_candidates=context["contact_email_for_candidates"],
            test_skill_type_name_fr=context["test_skill_type_name_fr"],
            test_skill_sub_type_name_fr=context["test_skill_sub_type_name_fr"],
            action_fr=context["action_fr"],
            test_center_name=context["test_center_name"],
            room_name=context["room_name"],
            test_center_address=context["test_center_address_text"]["fr"][0]["text"],
            test_center_city=context["test_center_city_text"]["fr"][0]["text"],
            test_center_province=context["test_center_province_text"]["fr"][0]["text"],
            test_center_postal_code=context["test_center_postal_code"],
            country_fdesc=context["country_fdesc"],
            country_fabrv=context["country_fabrv"],
            dept_fdesc=context["dept_fdesc"],
            dept_fabrv=context["dept_fabrv"],
        )

        email_plaintext_message = """
            (English follows)
            \n
            {email_fr_part}
            \n
            \n
            (Le français précède)
            \n
            {email_en_part}
            """.format(
            email_en_part=email_en_part, email_fr_part=email_fr_part
        )

        send_mail(
            # title:
            subject,
            # message:
            email_plaintext_message,
            # from (will be overridden by the real email in Dev/Test/Prod):
            settings.EMAIL_HOST_USER,
            # to:
            [context["email"]],
        )


def handle_test_session_candidates_to_security_email():
    from django.conf import settings
    from django.core.mail import send_mail
    from db_views.db_view_models.test_center_test_sessions_vw import (
        TestCenterTestSessionsVW,
    )
    from db_views.db_view_models.test_centers_vw import (
        TestCentersVW,
    )
    from db_views.db_view_models.assessment_process_assigned_test_specs_vw import (
        AssessmentProcessAssignedTestSpecsVW,
    )
    from backend.custom_models.consumed_reservation_codes import (
        ConsumedReservationCodes,
    )
    from backend.custom_models.test_center_test_sessions import TestCenterTestSessions
    from user_management.user_management_models.user_models import User
    from backend.views.gc_notify_view import (
        is_gc_notify_active,
        test_session_candidates_to_security,
    )

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: SEND TEST SESSION CANDIDATES TO SECURITY ====================".format(
            ConsoleMessageColor.GREEN
        )
    )

    # getting current date
    current_date = date.today()
    # getting in two days date
    in_two_days = current_date + timedelta(days=2)

    # getting test sessions in two days + grouping by department ID
    test_sessions_grouped_by_department_id = (
        TestCenterTestSessionsVW.objects.filter(date=in_two_days)
        .values(
            "department_id",
            "dept_eabrv",
            "dept_edesc",
            "test_center_id",
            "test_center_name",
            "test_center_security_email",
        )
        .annotate(dcount=Count("department_id"))
        .order_by("department_id")
    )

    # looping in test sessions
    for test_session_grouped_by_department_id in test_sessions_grouped_by_department_id:
        # initializing candidates_list_array
        candidates_list_array = []
        # getting related test center data
        test_center_data = TestCentersVW.objects.get(
            id=test_session_grouped_by_department_id["test_center_id"]
        )
        # getting test center related test sessions
        test_sessions = TestCenterTestSessionsVW.objects.filter(
            test_center_id=test_center_data.id, date=in_two_days
        )
        # looping in test_sessions
        for test_session in test_sessions:
            # test session related consumed reservation codes
            consumed_reservation_codes = ConsumedReservationCodes.objects.filter(
                test_session_id=test_session.id
            )
            # looping in consumed_reservation_codes
            for consumed_reservation_code in consumed_reservation_codes:
                # getting consumed reservation code related assigned test specs
                assigned_test_specs = AssessmentProcessAssignedTestSpecsVW.objects.get(
                    id=consumed_reservation_code.assessment_process_assigned_test_specs_id
                )
                # formatting test skill
                formatted_test_skill_en = "{0} - {1}".format(
                    assigned_test_specs.test_skill_type_name_en,
                    assigned_test_specs.test_skill_sub_type_name_en,
                )
                formatted_test_skill_fr = "{0} - {1}".format(
                    assigned_test_specs.test_skill_type_name_fr,
                    assigned_test_specs.test_skill_sub_type_name_fr,
                )
                # getting email of user (test center manager) who created the respective test session
                his_user_id = (
                    TestCenterTestSessions.history.filter(
                        id=test_session.id, history_type="+"
                    )
                    .first()
                    .history_user_id
                )
                test_center_manager_email = User.objects.get(id=his_user_id).email
                # populating candidates_list_array
                candidates_list_array.append(
                    {
                        "first_name": assigned_test_specs.first_name,
                        "last_name": assigned_test_specs.last_name,
                        "email": assigned_test_specs.email,
                        "date": test_session.date,
                        "start_time": test_session.start_time,
                        # getting start time with the following format ==> HH:mm
                        "formatted_start_time": "{:d}:{:02d}".format(
                            test_session.start_time.hour, test_session.start_time.minute
                        ),
                        "formatted_test_skill_en": formatted_test_skill_en,
                        "formatted_test_skill_fr": formatted_test_skill_fr,
                        "test_center_manager_email": test_center_manager_email,
                    }
                )
        # re-ordering candidates_list_array by last name and start time
        candidates_list_array.sort(
            key=lambda x: (x["last_name"].lower(), x["start_time"]), reverse=False
        )

        # candidate list is not empty
        if candidates_list_array:
            # formatting candidates_list_array so it is properly displayed in the email
            formatted_candidates_list_array = "[Nom de famille/Last Name], [Prénom/Last Name] ([Test] - [Heure/Time]) ==> Contact: [email]\n\n"
            for candidate_data in candidates_list_array:
                formatted_candidates_list_array = (
                    formatted_candidates_list_array
                    + "{0}, {1} ({2}/{3} - {4}) ==> Contact: {5}\n\n".format(
                        candidate_data["last_name"],
                        candidate_data["first_name"],
                        candidate_data["formatted_test_skill_fr"],
                        candidate_data["formatted_test_skill_en"],
                        candidate_data["formatted_start_time"],
                        candidate_data["test_center_manager_email"],
                    )
                )
            test_date = candidates_list_array[0]["date"]
            test_time = candidates_list_array[0]["formatted_start_time"]
            test_center_manager_email = candidates_list_array[0][
                "test_center_manager_email"
            ]

            # security email is defined
            if (
                test_session_grouped_by_department_id["test_center_security_email"]
                is not None
            ):
                # sending email
                print(
                    "{0} \t--> Sending email to {1} ({2} - {3}) for tests that will be held in {4} on {5}".format(
                        ConsoleMessageColor.WHITE,
                        test_session_grouped_by_department_id[
                            "test_center_security_email"
                        ],
                        test_session_grouped_by_department_id["dept_edesc"],
                        test_session_grouped_by_department_id["dept_eabrv"],
                        test_session_grouped_by_department_id["test_center_name"],
                        in_two_days,
                    )
                )

                if is_gc_notify_active():
                    context = {
                        "in_two_days": in_two_days.strftime("%Y/%m/%d"),
                        "formatted_candidates_list_array": formatted_candidates_list_array,
                        "test_date": test_date.strftime("%Y/%m/%d"),
                        "test_time": test_time,
                    }
                    test_session_candidates_to_security(
                        test_session_grouped_by_department_id[
                            "test_center_security_email"
                        ],
                        context,
                        test_center_manager_email,
                    )
                else:
                    subject = "Liste de candidats attendus pour un test le {test date and time} / List of expected candidates for a test on {test date and time}"

                    email_en_part = """
                        Good day,
                        \n
                        Please find below the list of candidates attending a test on {in_two_days}.
                        \n
                        Responses to this system-generated email will not be processed. Please contact us instead using the emails provided in the list below.
                        \n
                        Thank you,
                        """.format(
                        in_two_days=in_two_days
                    )

                    email_fr_part = """
                        Bonjour,
                        \n
                        Veuillez trouver ci-dessous la liste des candidats participant à un test le {in_two_days}.
                        \n
                        Les réponses à ce courriel généré par le système ne seront pas traitées. Veuillez plutôt nous contacter en utilisant les courriels fourni dans la liste ci-dessous.
                        \n
                        Merci,
                        """.format(
                        in_two_days=in_two_days
                    )

                    email_plaintext_message = """
                        (English follows)
                        \n
                        {email_fr_part}
                        \n
                        \n
                        (Le français précède)
                        \n
                        {email_en_part}
                        \n
                        \n
                        {formatted_candidates_list_array}
                        """.format(
                        email_en_part=email_en_part,
                        email_fr_part=email_fr_part,
                        formatted_candidates_list_array=formatted_candidates_list_array,
                    )

                    send_mail(
                        # title:
                        subject,
                        # message:
                        email_plaintext_message,
                        # from (will be overridden by the real email in Dev/Test/Prod):
                        settings.EMAIL_HOST_USER,
                        # to:
                        [
                            test_session_grouped_by_department_id[
                                "test_center_security_email"
                            ]
                        ],
                    )
            else:
                print(
                    "{0} \t--> Email not sent , since no security email has been found for {1} test center ({2} - {3})".format(
                        ConsoleMessageColor.WHITE,
                        test_session_grouped_by_department_id["test_center_name"],
                        test_session_grouped_by_department_id["dept_edesc"],
                        test_session_grouped_by_department_id["dept_eabrv"],
                    )
                )
        # candidate list is empty (no email to be sent)
        else:
            print(
                "{0} \t--> Email not sent , since there no candidates for {1} test center ({2} - {3}) on {4}".format(
                    ConsoleMessageColor.WHITE,
                    test_session_grouped_by_department_id["test_center_name"],
                    test_session_grouped_by_department_id["dept_edesc"],
                    test_session_grouped_by_department_id["dept_eabrv"],
                    in_two_days,
                )
            )

    # if at least one action has been done
    if test_sessions_grouped_by_department_id:
        # printing successful messasge
        print(
            "{0} ==================== CELERY TASK EXECUTION: EMAIL FOR TEST SESSIONS HAVE BEEN SENT TO SECURITY SUCCESSFULLY ====================".format(
                ConsoleMessageColor.CYAN
            )
        )


def handle_delete_teams_event_async(virtual_user_id, meeting_id, headers):
    import requests

    # delete the meeting through teams API
    requests.delete(
        "https://graph.microsoft.com/v1.0/users/"
        + virtual_user_id
        + "/calendar/events/"
        + str(meeting_id),
        headers=headers,
    )
