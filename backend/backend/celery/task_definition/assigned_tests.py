from __future__ import absolute_import, unicode_literals
from datetime import timedelta
from datetime import date
import datetime
import pytz
from backend.celery.task_definition.utils import ConsoleMessageColor
from dateutil.relativedelta import relativedelta
from backend.views.gc_notify_view import expired_accommodation_request_send_all


# this function is scoring and updating the status of the non-submitted assigned tests
def update_non_submitted_active_assigned_tests():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from backend.custom_models.assigned_test import AssignedTest
    from backend.custom_models.assigned_test_section import AssignedTestSection
    from backend.views.uit_score_test import uit_scoring, Action
    from backend.static.assigned_test_status import AssignedTestStatus
    from cms.views.retrieve_test_section_data import (
        get_updated_time_after_lock_pause_function,
        get_total_assigned_test_time_in_seconds,
    )
    from backend.static.assigned_test_status import get_assigned_test_status_id
    from cms.cms_models.scoring_methods import ScoringMethods

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: UPDATE NON-SUBMITTED ACTIVE ASSIGNED TESTS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )
    # getting current time
    current_time = datetime.datetime.now(pytz.utc)
    # getting all active assigned tests
    active_status_id = get_assigned_test_status_id(AssignedTestStatus.ACTIVE)
    active_assigned_tests = AssignedTest.objects.filter(status_id=active_status_id)
    # looping in active_assigned_tests
    for active_assigned_test in active_assigned_tests:
        # getting new test start time considering all lock/pause times of the current assigned test
        test_data = get_updated_time_after_lock_pause_function(active_assigned_test.id)
        # test_data is not None and not equals to "no-access-times-test"
        if (
            test_data["time"] is not None
            and test_data["time"] != "no-access-times-test"
        ):
            # getting total test time
            total_test_time = get_total_assigned_test_time_in_seconds(
                active_assigned_test.id
            )
            # if current_time > test_data + total test time
            if current_time > test_data["time"] + timedelta(seconds=total_test_time):
                # ========== scoring ==========
                # getting timed_assigned_test_section_ids
                timed_assigned_test_section_ids = test_data[
                    "timed_assigned_test_section_ids"
                ]
                # looping in timed_assigned_test_section_ids
                for timed_assigned_test_section_id in timed_assigned_test_section_ids:
                    # score test section
                    parameters = {
                        "assigned_test_id": active_assigned_test.id,
                        "test_section_id": timed_assigned_test_section_id,
                    }
                    uit_scoring(parameters, Action.TEST_SECTION)
                    # set timed out flag
                    assigned_test_section = AssignedTestSection.objects.get(
                        test_section_id=timed_assigned_test_section_id,
                        assigned_test_id=active_assigned_test.id,
                    )
                    assigned_test_section.timed_out = True
                    assigned_test_section.save()
                # make sure submit_date is updated to the datetime now
                active_assigned_test.submit_date = datetime.datetime.now(pytz.utc)
                # calculating test validity based on respective test definition
                scoring_method = ScoringMethods.objects.get(
                    test_id=active_assigned_test.test_id
                )
                if scoring_method.validity_period is not None:
                    active_assigned_test.score_valid_until = (
                        datetime.date.today()
                        + relativedelta(months=+scoring_method.validity_period)
                    )
                # put the data in db
                active_assigned_test.save()
                # scoring test
                parameters = {"assigned_test_id": active_assigned_test.id}
                uit_scoring(parameters, Action.TEST)
                # ========== scoring (END) ==========
                # printing successful messasge
                print(
                    "{0} ==================== ASSIGNED_TEST_ID: {1} SCORED AND UPDATED SUCCESSFULLY TO STATUS SUBMITTED ====================".format(
                        ConsoleMessageColor.CYAN, active_assigned_test.id
                    )
                )
        # should only happen for tests from Jmeter executions
        elif test_data["time"] == "no-access-times-test":
            # update needed active_assigned_test fields
            active_assigned_test.submit_date = datetime.datetime.now(pytz.utc)
            # calculating test validity based on respective test definition
            scoring_method = ScoringMethods.objects.get(
                test_id=active_assigned_test.test_id
            )
            if scoring_method.validity_period is not None:
                active_assigned_test.score_valid_until = (
                    datetime.date.today()
                    + relativedelta(months=+scoring_method.validity_period)
                )
            active_assigned_test.save()
            # scoring test
            parameters = {"assigned_test_id": active_assigned_test.id}
            uit_scoring(parameters, Action.TEST)
            # printing warning/error message
            print(
                "{0} ==================== (SHOULD ONLY HAPPEN FOR TESTS FROM JMETER EXECUTION) ASSIGNED_TEST_ID: {1} SCORED AND UPDATED SUCCESSFULLY TO STATUS SUBMITTED ====================".format(
                    ConsoleMessageColor.RED, active_assigned_test.id
                )
            )
        # should not happen
        else:
            # TODO (fnormand): send email if that happens
            # printing error
            print(
                "{0} ==================== SOMETHING WENT WRONG RELATED TO ASSIGNED_TEST_ID: {1} ====================".format(
                    ConsoleMessageColor.RED, active_assigned_test.id
                )
            )


# this function is unpausing assigned tests that are stuck in PAUSED state
def unpause_assigned_tests():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from backend.static.assigned_test_status import AssignedTestStatus
    from backend.custom_models.assigned_test import AssignedTest
    from backend.custom_models.accommodation_request import AccommodationRequest
    from backend.custom_models.break_bank import BreakBank
    from backend.custom_models.break_bank_actions import BreakBankActions
    from backend.views.utils import get_last_accessed_test_section
    from cms.views.test_break_bank import BreakBankActionsConstants
    from backend.static.assigned_test_status import get_assigned_test_status_id

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: UNPAUSE ASSIGNED TESTS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )
    # getting current time
    current_time = datetime.datetime.now(pytz.utc)
    # getting all paused assigned tests
    paused_status_id = get_assigned_test_status_id(AssignedTestStatus.PAUSED)
    paused_assigned_tests = AssignedTest.objects.filter(status_id=paused_status_id)
    # looping in paused_assigned_tests
    for paused_assigned_test in paused_assigned_tests:
        # getting related accommodation request data
        accommodation_request_data = AccommodationRequest.objects.get(
            id=paused_assigned_test.accommodation_request_id
        )
        # getting related break bank data
        break_bank_data = BreakBank.objects.get(
            id=accommodation_request_data.break_bank_id
        )
        # getting last accessed assigned test section ID
        last_accessed_test_section_id = get_last_accessed_test_section(
            paused_assigned_test.id
        )
        # getting related break bank actions related to current test section
        break_bank_actions = BreakBankActions.objects.filter(
            break_bank_id=break_bank_data.id,
            test_section_id=last_accessed_test_section_id,
        )
        # getting most updated break bank remaining time
        # if there is a previous UNPAUSE action ==> most updated remaining time is the one from that UNPAUSE action
        # if there is no previous UNPAUSE action ==> most updated remaining time is basically the break bank time (from the break bank table)
        try:
            most_updated_remaining_time = break_bank_actions[
                len(break_bank_actions - 2)
            ].new_remaining_time
        except:
            most_updated_remaining_time = break_bank_data.break_time
        # getting last break bank action (last action should be a PAUSE, since the test is currently on PAUSE)
        last_pause_action = break_bank_actions.last()
        # making sure that the last action is in fact a PAUSE
        if last_pause_action.action_type == BreakBankActionsConstants.PAUSE:
            # validating if there is still time left on the pause action based on the provided most_updated_remaining_time and last_pause_action
            # getting time (in seconds) between current time and last pause action
            time_between_last_pause_action_and_current_time = (
                current_time - last_pause_action.modify_date
            ).total_seconds()
            # time_between_last_pause_action_and_current_time greater than the most updated remaining time
            if (
                time_between_last_pause_action_and_current_time
                > most_updated_remaining_time
            ):
                # adding new UNPAUSE action with a remaining time of 0
                BreakBankActions.objects.create(
                    action_type=BreakBankActionsConstants.UNPAUSE,
                    new_remaining_time=0,
                    break_bank_id=break_bank_data.id,
                    test_section_id=last_pause_action.test_section_id,
                )
                # update test status to ACTIVE
                active_status_id = get_assigned_test_status_id(
                    AssignedTestStatus.ACTIVE
                )
                paused_assigned_test.status_id = active_status_id
                paused_assigned_test.save()
                # printing successful messasge
                print(
                    "{0} ==================== ASSIGNED_TEST_ID: {1} UPDATED SUCCESSFULLY TO STATUS ACTIVE (UNPAUSED) ====================".format(
                        ConsoleMessageColor.CYAN, paused_assigned_test.id
                    )
                )
        # should never happen
        else:
            # TODO (fnormand): send email if that happens
            # printing error
            print(
                "{0} ==================== SOMETHING WENT WRONG RELATED TO ASSIGNED_TEST_ID: {1} ====================".format(
                    ConsoleMessageColor.RED, paused_assigned_test.id
                )
            )


# this function checks all open accommodations to see if they are expired
def expire_old_accommodations():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from db_views.db_view_models.selected_user_accommodation_file_vw import (
        SelectedUserAccommodationFileVW,
    )
    from backend.custom_models.user_accommodation_file_status import (
        UserAccommodationFileStatus,
    )
    from backend.custom_models.user_accommodation_file import UserAccommodationFile
    from backend.static.user_accommodation_file_status_const import (
        UserAccommodationFileStatusConst,
    )
    from backend.views.aae import USER_ACCOMMODATION_FILE_OPEN_STATUS_CODENAMES

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: EXPIRE OLD ACCOMMODATIONS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )

    # getting current date
    current_date = date.today()

    expired_status_id = UserAccommodationFileStatus.objects.get(
        codename=UserAccommodationFileStatusConst.EXPIRED
    ).id

    # get all accomodations that have expired (and are not completed, canceled, canceled by candidate, or already expired)

    # get all the ids of the open statuses
    open_status_ids = UserAccommodationFileStatus.objects.filter(
        codename__in=USER_ACCOMMODATION_FILE_OPEN_STATUS_CODENAMES
    ).values_list("id", flat=True)

    # getting respective user accommodation file data
    uaf_ids = SelectedUserAccommodationFileVW.objects.filter(
        status_id__in=open_status_ids,
        process_end_date__lt=current_date,
    ).values_list("id", flat=True)

    for uaf_id in uaf_ids:
        user_accommodation_file = UserAccommodationFile.objects.get(id=uaf_id)

        # updating status to EXPIRED
        user_accommodation_file.status_id = expired_status_id
        user_accommodation_file.save()

        uaf_data = SelectedUserAccommodationFileVW.objects.get(id=uaf_id)
        # printing successfully expired messasge
        print(
            "{0} ==================== USER ACCOMMODATION FILE: {1} UPDATED SUCCESSFULLY TO EXPIRED ====================".format(
                ConsoleMessageColor.CYAN, user_accommodation_file.id
            )
        )
        expired_accommodation_request_send_all(uaf_data)


# this function is invalidating assigned tests that are stuck in LOCKED state (indeterminate)
def invalidate_locked_assigned_tests():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from backend.static.assigned_test_status import AssignedTestStatus
    from backend.custom_models.assigned_test import AssignedTest
    from backend.custom_models.assigned_test_section import AssignedTestSection
    from backend.static.assigned_test_status import get_assigned_test_status_id

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: INVALIDATE LOCKED ASSIGNED TESTS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )
    # getting current time
    current_time = datetime.datetime.now(pytz.utc)
    # getting all paused assigned tests
    locked_status_id = get_assigned_test_status_id(AssignedTestStatus.LOCKED)
    locked_assigned_tests = AssignedTest.objects.filter(status_id=locked_status_id)
    # looping in locked_assigned_test
    for locked_assigned_test in locked_assigned_tests:
        # initializing total_test_time (in minutes)
        total_test_time = 0
        # getting assigned test sections based on assigned_test_id of current locked assigned test
        assigned_test_sections = AssignedTestSection.objects.filter(
            assigned_test_id=locked_assigned_test.id
        )
        # looping in assigned_test_sections
        for assigned_test_section in assigned_test_sections:
            # test_section_time is defined
            if assigned_test_section.test_section_time is not None:
                # adding test_section_time to total_test_time
                total_test_time += assigned_test_section.test_section_time
        # modify_date of current locked assigned test + total_test_time + 30 days
        calculated_time = (
            locked_assigned_test.modify_date
            + timedelta(minutes=total_test_time)
            + timedelta(days=30)
        )
        # if current_time > calculated_time
        if current_time > calculated_time:
            # update test status to Quit and set is_invalid flag to true
            quit_status_id = get_assigned_test_status_id(AssignedTestStatus.QUIT)
            locked_assigned_test.status_id = quit_status_id
            locked_assigned_test.is_invalid = True
            locked_assigned_test.save()
            # printing successful messasge
            print(
                "{0} ==================== ASSIGNED_TEST_ID: {1} UPDATED SUCCESSFULLY TO STATUS QUIT WITH IS_INVALID FLAG SET TO TRUE ====================".format(
                    ConsoleMessageColor.CYAN, locked_assigned_test.id
                )
            )


# this function is unassigning supervised assigned tests that are stuck/left in CHECKED_IN or PRE-TEST state
def unassign_checked_in_and_pre_test_supervised_assigned_tests():
    # imports need to be here to avoid errors/exceptions when starting the containers
    from backend.static.assigned_test_status import AssignedTestStatus
    from backend.custom_models.assigned_test import AssignedTest
    from backend.static.assigned_test_status import get_assigned_test_status_id

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: UNASSIGN READY AND PRE-TEST ASSIGNED TESTS ====================".format(
            ConsoleMessageColor.GREEN
        )
    )
    # getting current time
    current_time = datetime.datetime.now(pytz.utc)
    # getting all checked-in/pre test assigned tests (supervised tests only)
    checked_in_status_id = get_assigned_test_status_id(AssignedTestStatus.CHECKED_IN)
    pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
    ready_and_pre_test_assigned_tests = AssignedTest.objects.filter(
        status_id__in=(checked_in_status_id, pre_test_status_id),
        uit_invite_id=None,
    )
    # looping in ready_and_pre_test_assigned_tests
    for ready_and_pre_test_assigned_test in ready_and_pre_test_assigned_tests:
        # modify_date of current ready or pre-test assigned test + 1 hour
        calculated_time = ready_and_pre_test_assigned_test.modify_date + timedelta(
            hours=1
        )
        # if current_time > calculated_time
        if current_time > calculated_time:
            # update test status to UNASSIGNED
            unassigned_status_id = get_assigned_test_status_id(
                AssignedTestStatus.UNASSIGNED
            )
            ready_and_pre_test_assigned_test.status_id = unassigned_status_id
            ready_and_pre_test_assigned_test.save()
            # printing successful messasge
            print(
                "{0} ==================== ASSIGNED_TEST_ID: {1} UPDATED SUCCESSFULLY TO STATUS UNASSIGNED ====================".format(
                    ConsoleMessageColor.CYAN, ready_and_pre_test_assigned_test.id
                )
            )
