from __future__ import absolute_import, unicode_literals
from datetime import date
from itertools import chain
from backend.views.gc_notify_view import (
    send_test_session_cancellation_based_on_assessment_process_data_updates,
    send_test_session_cancellation_related_data_to_hr_coordinator,
    send_update_active_assessment_process_data_email,
)
from backend.celery.task_definition.utils import ConsoleMessageColor


# this function is deleting the expired reservation codes
def remove_expired_reservation_codes():
    from backend.custom_models.reservation_codes import ReservationCodes
    from backend.custom_models.assessment_process_assigned_test_specs import (
        AssessmentProcessAssignedTestSpecs,
    )
    from backend.custom_models.assessment_process import AssessmentProcess
    from backend.custom_models.consumed_reservation_codes import (
        ConsumedReservationCodes,
    )
    from backend.static.consumed_reservation_code_statuses import (
        StaticConsumedReservationCodeStatus,
    )
    from backend.custom_models.consumed_reservation_code_status import (
        ConsumedReservationCodeStatus,
    )

    # printing celery task name
    print(
        "{0} ==================== CELERY TASK EXECUTION: REMOVE EXPIRED RESERVATION CODES ====================".format(
            ConsoleMessageColor.GREEN
        )
    )

    # getting current date
    current_date = date.today()

    # initializing at_least_one_reservation_code_deleted
    at_least_one_reservation_code_deleted = False

    # getting consumed reservation code status IDs
    reserved_status_id = ConsumedReservationCodeStatus.objects.get(
        codename=StaticConsumedReservationCodeStatus.RESERVED
    ).id
    expired_status_id = ConsumedReservationCodeStatus.objects.get(
        codename=StaticConsumedReservationCodeStatus.EXPIRED
    ).id

    # getting all reservation code related assessment process assigned test specs IDs
    reservation_code_related_assigned_test_specs_ids = (
        ReservationCodes.objects.all().values_list(
            "assessment_process_assigned_test_specs_id", flat=True
        )
    )

    consumed_reservation_code_related_assigned_test_specs_ids = (
        ConsumedReservationCodes.objects.filter(
            status_id=reserved_status_id
        ).values_list("assessment_process_assigned_test_specs_id", flat=True)
    )

    # getting all related assessment process assigned test specs
    related_assessment_process_assigned_test_specs = (
        AssessmentProcessAssignedTestSpecs.objects.filter(
            id__in=(
                list(
                    chain(
                        reservation_code_related_assigned_test_specs_ids,
                        consumed_reservation_code_related_assigned_test_specs_ids,
                    )
                )
            )
        )
    )

    # looping in related_assessment_process_assigned_test_specs
    for assigned_test_specs in related_assessment_process_assigned_test_specs:
        # getting related assessment process data
        related_assessment_process_data = AssessmentProcess.objects.get(
            id=assigned_test_specs.assessment_process_id
        )

        # checking if closing date has passed
        if current_date > related_assessment_process_data.closing_date:
            # ========== RESERVATION CODES ==========
            # setting at_least_one_reservation_code_deleted to True
            at_least_one_reservation_code_deleted = True
            # deleting related reservation codes
            related_reservation_codes = ReservationCodes.objects.filter(
                assessment_process_assigned_test_specs_id=assigned_test_specs.id
            )
            for related_reservation_code in related_reservation_codes:
                print(
                    "{0} \t--> Deleting reservation code: {1}".format(
                        ConsoleMessageColor.WHITE,
                        related_reservation_code.reservation_code,
                    )
                )
                related_reservation_code.delete()
            # ========== RESERVATION CODES (END)==========

            # ========== CONSUMED RESERVATION CODES ==========
            # updating related consumed reservation codes
            related_consumed_reservation_codes = (
                ConsumedReservationCodes.objects.filter(
                    assessment_process_assigned_test_specs_id=assigned_test_specs.id,
                    status_id=reserved_status_id,
                )
            )
            for related_consumed_reservation_code in related_consumed_reservation_codes:
                print(
                    "{0} \t--> Updating consumed reservation code: {1} to status {2}".format(
                        ConsoleMessageColor.WHITE,
                        related_consumed_reservation_code.reservation_code,
                        StaticConsumedReservationCodeStatus.EXPIRED,
                    )
                )
                related_consumed_reservation_code.status_id = expired_status_id
                related_consumed_reservation_code.save()
            # ========== CONSUMED RESERVATION CODES (END)==========

    # at_least_one_reservation_code_deleted is set to True
    if at_least_one_reservation_code_deleted:
        # printing successful messasge
        print(
            "{0} ==================== CELERY TASK EXECUTION: EXPIRED RESERVATION CODES HAVE BEEN DELETED SUCCESSFULLY ====================".format(
                ConsoleMessageColor.CYAN
            )
        )

    return None


# handling active assessment process data updates
def handle_update_active_assessment_process_data(email_data_array):
    # looping in email_data_array
    for email_data in email_data_array:
        # sending email
        send_update_active_assessment_process_data_email(email_data)
    return


# handling test session cancellation based on assessment process data updates (duration and or contact for candidate email changes)
def handle_test_session_cancellation_based_on_assessment_process_data_updates(
    email_data_array,
):
    # looping in email_data_array
    for email_data in email_data_array:
        # sending email
        send_test_session_cancellation_based_on_assessment_process_data_updates(
            email_data
        )
    return


# handling test session cancellation based on assessment process data updates (duration and or contact for candidate email changes)
def handle_test_session_cancellation_inform_related_hr_coordinator(
    email_data_array,
):
    # looping in email_data_array
    for email_data in email_data_array:
        # sending email
        send_test_session_cancellation_related_data_to_hr_coordinator(email_data)
    return
