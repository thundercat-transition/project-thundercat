class ConsoleMessageColor:
    BLACK = "\033[1;30m"
    BLUE = "\033[1;34m"
    CYAN = "\033[1;36m"  # Successful Update
    GREEN = "\033[1;32m"  # Task Execution
    PURPLE = "\033[1;35m"
    RED = "\033[1;31m"  # Something Went Wrong
    WHITE = "\033[1;97m"
    YELLOW = "\033[1;33m"


class ConsoleBackgroundColor:
    BLACK = "\033[1;40m"
    BLUE = "\033[1;44m"
    CYAN = "\033[1;46m"
    GREEN = "\033[1;42m"
    PURPLE = "\033[1;45m"
    RED = "\033[1;41m"
    WHITE = "\033[1;47m"
    YELLOW = "\033[1;43m"


# See: https://stackabuse.s3.amazonaws.com/media/how-to-print-colored-text-in-python-01.jpg
class ConsoleColorCombo:
    DEFAULT_FORMAT = "\033[1;37;40m"
    BLUE_BACK_WHITE_TEXT = "\033[1;37;44m"
    GREEN_BACK_WHITE_TEXT = "\033[1;37;42m"
