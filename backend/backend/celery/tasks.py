from __future__ import absolute_import, unicode_literals

from celery import shared_task
from backend.celery.task_definition.assessment_process import (
    handle_update_active_assessment_process_data,
    handle_test_session_cancellation_based_on_assessment_process_data_updates,
    handle_test_session_cancellation_inform_related_hr_coordinator,
    remove_expired_reservation_codes,
)
from backend.celery.task_definition.test_session import (
    handle_reservation_code_email,
    handle_delete_specific_test_session_email,
    handle_reservation_code_withdraw_no_booking_email,
    handle_reservation_code_withdraw_from_booked_session_email,
    handle_reservation_code_booking_email,
    handle_reservation_code_book_later_email,
    handle_test_session_candidates_to_security_email,
    handle_delete_teams_event_async,
)
from backend.celery.task_definition.test_center import (
    handle_create_test_session_email,
    handle_past_test_sessions,
    handle_past_test_center_ola_vacation_blocks,
)
from backend.celery.task_definition.assigned_tests import (
    update_non_submitted_active_assigned_tests,
    unpause_assigned_tests,
    expire_old_accommodations,
    invalidate_locked_assigned_tests,
    unassign_checked_in_and_pre_test_supervised_assigned_tests,
)
from backend.celery.task_definition.test_permissions import (
    remove_expired_test_permissions,
)
from backend.celery.task_definition.test_access_codes import (
    remove_old_test_access_codes,
)
from backend.celery.task_definition.uit import (
    handle_expired_uit_processes,
    unassign_non_started_uit_tests,
    handle_send_uit_invite_emails,
    handle_update_uit_validity_end_date_emails,
    handle_delete_uit_invite_emails,
)

from backend.celery.task_definition.user_permissions import (
    deprovision_after_inactivity,
    deprovision_admin_after_inactivity_thirty_days,
    remove_expired_test_scorer_permissions,
    handle_send_2fa_code_email,
)
from backend.celery.task_definition.reset_completed_profile_flag import (
    reset_completed_profile_flag,
)
from backend.celery.task_definition.gc_notify_tasks import (
    check_gc_notify_delivery_status,
    purge_expired_gc_notify_tokens,
    check_for_orphaned_bulk_delivery_failues,
    check_for_ola_reminder_timelines,
)

from backend.celery.task_definition.two_fa_codes import handle_clear_expired_2fa_code


# ==================== *IMPORTANT* ====================
# if you do any update to those tasks, don't forget to restart the celery and celery-beat containers (for testing purposes)


# =============== ASSIGNED TESTS TASKS ===============
@shared_task
def run_every_60_minutes_3():
    update_non_submitted_active_assigned_tests()


@shared_task
def run_every_morning_at_03_00():
    invalidate_locked_assigned_tests()


@shared_task
def run_every_60_minutes():
    unassign_checked_in_and_pre_test_supervised_assigned_tests()


@shared_task
def run_every_15_minutes():
    unpause_assigned_tests()


# deprecated celery task (might need to re-enable it in the future)
# this task is no longer being executed at the moment
@shared_task
def run_every_morning_at_03_30():
    expire_old_accommodations()


# =============== ASSIGNED TESTS TASKS (END) ===============


# =============== TEST PERMISSIONS TASKS ===============
@shared_task
def run_every_morning_at_01_30():
    remove_expired_test_permissions()


# =============== TEST PERMISSIONS TASKS (END) ===============


# =============== TEST ACCESS CODES TASKS ===============
@shared_task
def run_every_morning_at_01_35():
    remove_old_test_access_codes()


# =============== TEST ACCESS CODES TASKS (END) ===============


# =============== USER TASKS ===============
@shared_task
def run_every_morning_at_01_10():
    reset_completed_profile_flag()


# =============== USER TASKS (END) ===============


# =============== UIT TASKS ===============
@shared_task
def run_every_morning_at_01_01():
    handle_expired_uit_processes()


@shared_task
def run_every_morning_at_01_50():
    unassign_non_started_uit_tests()


# =============== UIT TASKS (END) ===============


# ========== DEPROVISION TASKS ==========
@shared_task
def run_every_morning_at_02_30():
    deprovision_after_inactivity()


@shared_task
def run_every_morning_at_01_20():
    deprovision_admin_after_inactivity_thirty_days()


@shared_task
def run_every_morning_at_01_45():
    remove_expired_test_scorer_permissions()


# ========== DEPROVISION TASKS (END) ==========


# ========== TEST CENTER ==========
@shared_task
def run_every_60_minutes_2():
    handle_past_test_sessions()


@shared_task
def run_every_morning_at_01_15():
    handle_past_test_center_ola_vacation_blocks()


# ========== TEST CENTER (END) ==========


# ========== ASSESSMENT PROCESS ==========
@shared_task
def run_every_morning_at_01_40():
    remove_expired_reservation_codes()


# ========== ASSESSMENT PROCESS (END) ==========


# ========== TEST SESSIONS ==========
@shared_task
def run_every_morning_at_02_00():
    handle_test_session_candidates_to_security_email()


# ========== TEST SESSIONS (END) ==========


# ========== EMAIL TASKS ==========
@shared_task(bind=True)
def send_uit_emails_celery_task(self, candidate_invitations_array, validity_end_date):
    handle_send_uit_invite_emails(candidate_invitations_array, validity_end_date)


@shared_task(bind=True)
def send_update_uit_validity_end_date_email_celery_task(self, candidates_to_send_email):
    handle_update_uit_validity_end_date_emails(candidates_to_send_email)


@shared_task(bind=True)
def delete_uit_invite_email_celery_task(self, candidates_to_send_email):
    handle_delete_uit_invite_emails(candidates_to_send_email)


@shared_task(bind=True)
def create_test_session_email_celery_task(self, candidates_to_send_email):
    handle_create_test_session_email(candidates_to_send_email)


@shared_task(bind=True)
def send_reservation_code_email_celery_task(self, email_data_array):
    handle_reservation_code_email(email_data_array)


@shared_task(bind=True)
def send_delete_specific_test_session_email_celery_task(self, email_data_array):
    handle_delete_specific_test_session_email(email_data_array)


@shared_task(bind=True)
def send_reservation_code_withdraw_no_booking_email_celery_task(self, email_data_array):
    handle_reservation_code_withdraw_no_booking_email(email_data_array)


@shared_task(bind=True)
def send_reservation_code_withdraw_from_booked_session_email_celery_task(
    self, email_data_array
):
    handle_reservation_code_withdraw_from_booked_session_email(email_data_array)


@shared_task(bind=True)
def send_reservation_code_booking_email_celery_task(self, email_data_array):
    handle_reservation_code_booking_email(email_data_array)


@shared_task(bind=True)
def send_reservation_code_book_later_email_celery_task(self, email_data_array):
    handle_reservation_code_book_later_email(email_data_array)


@shared_task(bind=True)
def send_2fa_code_email_celery_task(self, email_data_array):
    handle_send_2fa_code_email(email_data_array)


@shared_task(bind=True)
def update_active_assessment_process_data(self, email_data_array):
    handle_update_active_assessment_process_data(email_data_array)


@shared_task(bind=True)
def test_session_cancellation_based_on_assessment_process_data_updates(
    self, email_data_array
):
    handle_test_session_cancellation_based_on_assessment_process_data_updates(
        email_data_array
    )


@shared_task(bind=True)
def test_session_cancellation_inform_related_hr_coordinator(self, email_data_array):
    handle_test_session_cancellation_inform_related_hr_coordinator(email_data_array)


@shared_task(bind=True)
def cancel_teams_invite(self, virtual_user_id, meeting_id, headers):
    handle_delete_teams_event_async(virtual_user_id, meeting_id, headers)


# ========== EMAIL TASKS (END) ==========


# ========== 2FA TASKS ==========
@shared_task
def run_every_60_minutes_4():
    handle_clear_expired_2fa_code()


# ========== 2FA TASKS (END) ==========


# ========== GC NOTIFY TASKS ==========
@shared_task(bind=True)
def trigger_check_gc_notify_delivery_status(self):
    check_gc_notify_delivery_status()


@shared_task(bind=True)
def trigger_clear_expired_gc_notify_tokens(self):
    purge_expired_gc_notify_tokens()


@shared_task(bind=True)
def trigger_check_for_orphaned_bulk_delivery_failues(self):
    check_for_orphaned_bulk_delivery_failues()


# ========== GC NOTIFY TASKS (END) ==========


# ========== SPAM TASKS ==========
@shared_task
def run_every_morning_at_01_55():
    check_for_ola_reminder_timelines()


# ========== SPAM TASKS (END) ==========
