from rest_framework import serializers
from backend.ref_table_views.cat_ref_gender_vw import CatRefGenderVW


class CatRefGenderSerializer(serializers.ModelSerializer):
    class Meta:
        model = CatRefGenderVW
        fields = [
            "gnd_id",
            "active_flg",
            "eabrv",
            "fabrv",
            "edesc",
            "fdesc",
            "efdt",
            "xdt",
        ]
