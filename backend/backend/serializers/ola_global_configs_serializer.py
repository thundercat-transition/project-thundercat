from rest_framework import serializers
from backend.custom_models.ola_global_configs import OlaGlobalConfigs


class OlaGlobalConfigsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OlaGlobalConfigs
        fields = "__all__"
