from rest_framework import serializers
from db_views.db_view_models.billing_contact_vw import BillingContactVW


class BillingContactVWSerializer(serializers.ModelSerializer):
    class Meta:
        model = BillingContactVW
        fields = "__all__"
