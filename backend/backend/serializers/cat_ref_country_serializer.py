from rest_framework import serializers
from backend.ref_table_views.cat_ref_country_vw import CatRefCountryVW


class CatRefCountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = CatRefCountryVW
        fields = [
            "cntry_id",
            "active_flg",
            "eabrv",
            "fabrv",
            "edesc",
            "fdesc",
            "efdt",
            "xdt",
        ]
