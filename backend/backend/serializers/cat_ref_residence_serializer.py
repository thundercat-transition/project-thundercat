from rest_framework import serializers
from backend.ref_table_views.cat_ref_residence_vw import CatRefResidenceVW


class CatRefResidenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = CatRefResidenceVW
        fields = [
            "area_res_id",
            "active_flg",
            "edesc",
            "fdesc",
            "efdt",
            "xdt",
            "presentation_order",
        ]
