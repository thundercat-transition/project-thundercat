from rest_framework import serializers
from backend.serializers.test_center_serializer import TestCenterSerializer
from backend.custom_models.test_center import TestCenter
from backend.custom_models.test_center_ola_test_assessor import (
    TestCenterOlaTestAssessor,
)
from backend.custom_models.test_center_ola_assessor_unavailability import (
    TestCenterOlaAssessorUnavailability,
)
from user_management.user_management_models.user_models import User


# Serializer for Assessor User
class UserAssessorSerializer(serializers.ModelSerializer):
    user_id = serializers.IntegerField(source="id", read_only=True)

    class Meta:
        model = User
        fields = ("user_id", "first_name", "last_name", "username", "email")


# Serializer for TestCenterOlaTestAssessor
class TestCenterOlaTestAssessorSerializer(serializers.ModelSerializer):

    class Meta:
        model = TestCenterOlaTestAssessor
        fields = "__all__"


# Serializer for TestCenterOlaAssessorUnavailability
class TestCenterOlaAssessorUnavailabilitySerializer(serializers.ModelSerializer):

    test_assessor_user = serializers.SerializerMethodField()
    test_center_ola_test_assessor = serializers.SerializerMethodField()
    test_center = serializers.SerializerMethodField()

    def get_test_assessor_user(self, request):
        test_center_ola_test_assessor = TestCenterOlaTestAssessor.objects.get(
            id=request.test_center_ola_test_assessor_id
        )

        test_assessor_user = User.objects.get(id=test_center_ola_test_assessor.user_id)
        return UserAssessorSerializer(test_assessor_user).data

    def get_test_center_ola_test_assessor(self, request):
        test_center_ola_test_assessor = TestCenterOlaTestAssessor.objects.get(
            id=request.test_center_ola_test_assessor_id
        )
        return TestCenterOlaTestAssessorSerializer(test_center_ola_test_assessor).data

    def get_test_center(self, request):
        test_center_ola_test_assessor = TestCenterOlaTestAssessor.objects.get(
            id=request.test_center_ola_test_assessor_id
        )

        test_center = TestCenter.objects.get(
            id=test_center_ola_test_assessor.test_center_id
        )
        return TestCenterSerializer(test_center).data

    class Meta:
        model = TestCenterOlaAssessorUnavailability
        fields = "__all__"
