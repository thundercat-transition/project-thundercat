from rest_framework import serializers
from backend.custom_models.virtual_test_room import VirtualTestRoom


class VirtualTestRoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = VirtualTestRoom
        fields = ["name", "user_id"]
