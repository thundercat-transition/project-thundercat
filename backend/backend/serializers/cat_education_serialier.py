from rest_framework import serializers
from backend.ref_table_views.cat_education import CatEducation


class CatEducationSerializer(serializers.ModelSerializer):
    class Meta:
        model = CatEducation
        fields = ["education_id", "edesc", "fdesc"]
