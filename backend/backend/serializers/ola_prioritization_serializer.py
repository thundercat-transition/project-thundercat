from rest_framework import serializers
from backend.static.languages import Language_id
from backend.custom_models.reason_for_testing_priority_text import (
    ReasonForTestingPriorityText,
)
from backend.custom_models.reason_for_testing_priority import ReasonForTestingPriority
from backend.custom_models.ola_prioritization import OlaPrioritization


class OlaPrioritizationSerializer(serializers.ModelSerializer):
    text_en = serializers.SerializerMethodField()
    text_fr = serializers.SerializerMethodField()

    def get_text_en(self, request):
        text_en = ""
        try:
            reason_for_testing_id = ReasonForTestingPriority.objects.get(
                codename=request.codename
            ).id
            text_en = ReasonForTestingPriorityText.objects.get(
                reason_for_testing_priority_id=reason_for_testing_id,
                language_id=Language_id.EN,
            ).text
        except:
            pass
        return text_en

    def get_text_fr(self, request):
        text_fr = ""
        try:
            reason_for_testing_id = ReasonForTestingPriority.objects.get(
                codename=request.codename
            ).id
            text_fr = ReasonForTestingPriorityText.objects.get(
                reason_for_testing_priority_id=reason_for_testing_id,
                language_id=Language_id.FR,
            ).text
        except:
            pass
        return text_fr

    class Meta:
        model = OlaPrioritization
        fields = "__all__"
