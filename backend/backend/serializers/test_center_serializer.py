from rest_framework import serializers
from backend.custom_models.test_center import TestCenter
from user_management.user_management_models.user_models import User
from backend.serializers.reports_data_serializer import UserSerializer
from backend.custom_models.test_center_associated_managers import (
    TestCenterAssociatedManagers,
)
from backend.custom_models.test_center_test_administrators import (
    TestCenterTestAdministrators,
)


class TestCenterSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestCenter
        fields = "__all__"


class TestCenterTestAdministratorsSerializer(serializers.ModelSerializer):
    user_data = serializers.SerializerMethodField()

    def get_user_data(self, request):
        user_data = User.objects.filter(id=request.user_id)
        return UserSerializer(user_data, many=True).data

    class Meta:
        model = TestCenterTestAdministrators
        fields = "__all__"


class TestCenterAssociatedManagersSerializer(serializers.ModelSerializer):
    user_data = serializers.SerializerMethodField()

    def get_user_data(self, request):
        user_data = User.objects.filter(id=request.user_id)
        return UserSerializer(user_data, many=True).data

    class Meta:
        model = TestCenterAssociatedManagers
        fields = "__all__"
