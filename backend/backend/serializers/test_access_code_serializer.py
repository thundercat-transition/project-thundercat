from rest_framework import serializers
from backend.custom_models.test_access_code_model import TestAccessCode
from cms.cms_models.test_definition import TestDefinition


class TestAccessCodeSerializer(serializers.ModelSerializer):
    en_test_name = serializers.SerializerMethodField()
    fr_test_name = serializers.SerializerMethodField()

    def get_en_test_name(self, request):
        en_test_name = TestDefinition.objects.get(id=request.test_id).en_name
        return en_test_name

    def get_fr_test_name(self, request):
        fr_test_name = TestDefinition.objects.get(id=request.test_id).fr_name
        return fr_test_name

    class Meta:
        model = TestAccessCode
        fields = "__all__"
