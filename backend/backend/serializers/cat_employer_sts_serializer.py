from rest_framework import serializers
from backend.ref_table_views.cat_ref_employer_sts_vw import CatRefEmployerStsVW


class CatEmployerStsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CatRefEmployerStsVW
        fields = ["empsts_id", "edesc", "fdesc"]
