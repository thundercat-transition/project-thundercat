from itertools import groupby
from rest_framework import serializers
from cms.cms_models.test_skill_sle_desc_text import TestSkillSLEDescText
from cms.cms_models.test_skill_sle_desc import TestSkillSLEDesc
from cms.cms_models.test_skill_occupational_desc_text import (
    TestSkillOccupationalDescText,
)
from cms.cms_models.test_skill_occupational_desc import TestSkillOccupationalDesc
from backend.custom_models.language import Language


class TestSkillSLEDescTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestSkillSLEDescText
        fields = ["id", "text", "language"]


class TestSkillOccupationalDescTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestSkillOccupationalDescText
        fields = ["id", "text", "language"]


class TestSkillSubTypeSLESerializer(serializers.ModelSerializer):
    test_skill_sub_type_text = serializers.SerializerMethodField()

    def get_test_skill_sub_type_text(self, request):
        # getting test skill sle desc text
        test_skill_sle_desc_text = TestSkillSLEDescText.objects.filter(
            test_skill_sle_desc_id=request.id
        ).order_by("language_id")

        # getting serialized data
        serialized_data = TestSkillSLEDescTextSerializer(
            test_skill_sle_desc_text, many=True
        ).data

        # getting language data
        language_data = Language.objects.all()

        # initializing final_obj
        final_obj = {}

        # grouping text by language
        for obj, inner_group in groupby(serialized_data, lambda x: x["language"]):
            for language in language_data:
                # matching language
                if obj == language.language_id:
                    # populating obj
                    final_obj["{0}".format(language.ISO_Code_1)] = list(inner_group)

        # returning final_obj
        return final_obj

    class Meta:
        model = TestSkillSLEDesc
        fields = "__all__"


class TestSkillSubTypeOccupationalSerializer(serializers.ModelSerializer):
    test_skill_sub_type_text = serializers.SerializerMethodField()

    def get_test_skill_sub_type_text(self, request):
        # getting test skill occupational desc text
        test_skill_occupational_desc_text = (
            TestSkillOccupationalDescText.objects.filter(
                test_skill_occupational_desc_id=request.id
            ).order_by("language_id")
        )

        # getting serialized data
        serialized_data = TestSkillOccupationalDescTextSerializer(
            test_skill_occupational_desc_text, many=True
        ).data

        # getting language data
        language_data = Language.objects.all()

        # initializing final_obj
        final_obj = {}

        # grouping text by language
        for obj, inner_group in groupby(serialized_data, lambda x: x["language"]):
            for language in language_data:
                # matching language
                if obj == language.language_id:
                    # populating obj
                    final_obj["{0}".format(language.ISO_Code_1)] = list(inner_group)

        # returning final_obj
        return final_obj

    class Meta:
        model = TestSkillOccupationalDesc
        fields = "__all__"
