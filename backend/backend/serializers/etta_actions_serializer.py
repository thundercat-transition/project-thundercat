from backend.custom_models.etta_actions import EttaActions
from rest_framework import serializers


class EttaActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = EttaActions
        fields = "__all__"
