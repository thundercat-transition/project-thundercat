from rest_framework import serializers
from backend.custom_models.reason_for_testing_priority import ReasonForTestingPriority
from backend.custom_models.reason_for_testing_priority_text import (
    ReasonForTestingPriorityText,
)
from backend.custom_models.reason_for_testing_priority_text import (
    ReasonForTestingPriorityText,
)
from backend.static.languages import Language_id
from backend.custom_models.reason_for_testing_text import ReasonForTestingText
from backend.custom_models.reason_for_testing import ReasonForTesting
from backend.custom_models.reason_for_testing_type import ReasonForTestingType


class ReasonForTestingSerializer(serializers.ModelSerializer):
    reason_for_testing_type_codename = serializers.SerializerMethodField()
    reason_for_testing_name_en = serializers.SerializerMethodField()
    reason_for_testing_name_fr = serializers.SerializerMethodField()
    reason_for_testing_priority_name_en = serializers.SerializerMethodField()
    reason_for_testing_priority_name_fr = serializers.SerializerMethodField()

    def get_reason_for_testing_type_codename(self, request):
        reason_for_testing_type_codename = ReasonForTestingType.objects.get(
            id=request.reason_for_testing_type_id
        ).codename
        return reason_for_testing_type_codename

    def get_reason_for_testing_name_en(self, request):
        reason_for_testing_name_en = ReasonForTestingText.objects.get(
            reason_for_testing_id=request.id, language_id=Language_id.EN
        ).text
        return reason_for_testing_name_en

    def get_reason_for_testing_name_fr(self, request):
        reason_for_testing_name_fr = ReasonForTestingText.objects.get(
            reason_for_testing_id=request.id, language_id=Language_id.FR
        ).text
        return reason_for_testing_name_fr

    def get_reason_for_testing_priority_name_en(self, request):
        reason_for_testing_priority_name_en = None
        if request.reason_for_testing_priority_id is not None:
            reason_for_testing_priority_name_en = ReasonForTestingPriorityText.objects.get(
                reason_for_testing_priority_id=request.reason_for_testing_priority_id,
                language_id=Language_id.EN,
            ).text
        return reason_for_testing_priority_name_en

    def get_reason_for_testing_priority_name_fr(self, request):
        reason_for_testing_priority_name_fr = None
        if request.reason_for_testing_priority_id is not None:
            reason_for_testing_priority_name_fr = ReasonForTestingPriorityText.objects.get(
                reason_for_testing_priority_id=request.reason_for_testing_priority_id,
                language_id=Language_id.FR,
            ).text
        return reason_for_testing_priority_name_fr

    class Meta:
        model = ReasonForTesting
        fields = "__all__"


class ReasonForTestingPrioritySerializer(serializers.ModelSerializer):
    reason_for_testing_priority_name_en = serializers.SerializerMethodField()
    reason_for_testing_priority_name_fr = serializers.SerializerMethodField()

    def get_reason_for_testing_priority_name_en(self, request):
        reason_for_testing_priority_name_en = ReasonForTestingPriorityText.objects.get(
            reason_for_testing_priority_id=request.id, language_id=Language_id.EN
        ).text
        return reason_for_testing_priority_name_en

    def get_reason_for_testing_priority_name_fr(self, request):
        reason_for_testing_priority_name_fr = ReasonForTestingPriorityText.objects.get(
            reason_for_testing_priority_id=request.id, language_id=Language_id.FR
        ).text
        return reason_for_testing_priority_name_fr

    class Meta:
        model = ReasonForTestingPriority
        fields = "__all__"
