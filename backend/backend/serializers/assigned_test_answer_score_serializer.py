from rest_framework import serializers
from backend.custom_models.assigned_test_answer_score import AssignedTestAnswerScore


class AssignedTestAnswerScoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssignedTestAnswerScore
        fields = "__all__"
