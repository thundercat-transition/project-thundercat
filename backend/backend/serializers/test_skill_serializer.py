from itertools import groupby
from rest_framework import serializers
from backend.custom_models.language import Language
from cms.cms_models.test_skill_type_text import TestSkillTypeText
from cms.cms_models.test_skill_type import TestSkillType


class TestSkillTypeTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestSkillTypeText
        fields = ["id", "text", "language"]


class TestSkillTypeSerializer(serializers.ModelSerializer):
    test_skill_type_text = serializers.SerializerMethodField()

    def get_test_skill_type_text(self, request):
        # getting test skill type text
        test_skill_type_text = TestSkillTypeText.objects.filter(
            test_skill_type_id=request.id
        ).order_by("language_id")

        # getting serialized data
        serialized_data = TestSkillTypeTextSerializer(
            test_skill_type_text, many=True
        ).data

        # getting language data
        language_data = Language.objects.all()

        # initializing final_obj
        final_obj = {}

        # grouping text by language
        for obj, inner_group in groupby(serialized_data, lambda x: x["language"]):
            for language in language_data:
                # matching language
                if obj == language.language_id:
                    # populating obj
                    final_obj["{0}".format(language.ISO_Code_1)] = list(inner_group)

        # returning final_obj
        return final_obj

    class Meta:
        model = TestSkillType
        fields = "__all__"
