from rest_framework import serializers
from backend.serializers.reason_for_testing_serializer import (
    ReasonForTestingSerializer,
)
from backend.custom_models.reason_for_testing import ReasonForTesting
from backend.custom_models.unsupervised_test_access_code_model import (
    UnsupervisedTestAccessCode,
)
from backend.custom_models.test_access_code_model import TestAccessCode
from backend.custom_models.uit_invites import UITInvites
from backend.ref_table_views.cat_ref_departments_vw import CatRefDepartmentsVW
from backend.serializers.cat_ref_departments_serializer import (
    CatRefDepartmentsSerializer,
)
from backend.custom_models.orderless_financial_data import OrderlessFinancialData
from user_management.user_management_models.user_models import User


class OrderlessFinancialDataSerializer(serializers.ModelSerializer):
    department_data = serializers.SerializerMethodField()
    reason_for_testing_data = serializers.SerializerMethodField()
    ta_username = serializers.SerializerMethodField()
    test_id = serializers.SerializerMethodField()

    def get_department_data(self, request):
        try:
            department_data = CatRefDepartmentsVW.objects.get(
                dept_id=request.department_ministry_id
            )
            serialized_data = CatRefDepartmentsSerializer(
                department_data, many=False
            ).data
            return serialized_data
        except:
            return None

    def get_reason_for_testing_data(self, request):
        try:
            reason_for_testing_data = ReasonForTesting.objects.get(
                id=request.reason_for_testing_id
            )
            serialized_data = ReasonForTestingSerializer(
                reason_for_testing_data, many=False
            ).data
            return serialized_data
        except:
            return None

    def get_ta_username(self, request):
        # uit_invite_id exists
        if request.uit_invite_id:
            ta_username = User.objects.get(
                id=UITInvites.objects.get(id=request.uit_invite_id).ta_user_id
            ).username
        else:
            ta_username = User.objects.get(
                id=TestAccessCode.history.filter(orderless_financial_data_id=request.id)
                .first()
                .ta_user_id
            ).username
        return ta_username

    def get_test_id(self, request):
        # uit_invite_id exists
        if request.uit_invite_id:
            test_id = (
                UnsupervisedTestAccessCode.history.filter(
                    uit_invite_id=request.uit_invite_id
                )
                .first()
                .test_id
            )
        else:
            test_id = (
                TestAccessCode.history.filter(orderless_financial_data_id=request.id)
                .first()
                .test_id
            )
        return test_id

    class Meta:
        model = OrderlessFinancialData
        fields = "__all__"
