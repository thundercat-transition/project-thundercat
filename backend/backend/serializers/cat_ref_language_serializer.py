from rest_framework import serializers
from backend.ref_table_views.cat_ref_language_vw import CatRefLanguageVW


class CatRefLanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = CatRefLanguageVW
        fields = ["language_id", "iso_code_1", "iso_code_2", "edesc", "fdesc"]
