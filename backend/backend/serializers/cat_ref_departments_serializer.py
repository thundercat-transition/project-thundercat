from rest_framework import serializers
from backend.ref_table_views.cat_ref_departments_vw import CatRefDepartmentsVW


class CatRefDepartmentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CatRefDepartmentsVW
        fields = [
            "dept_id",
            "active_flg",
            "eabrv",
            "fabrv",
            "edesc",
            "fdesc",
            "efdt",
            "xdt",
        ]
