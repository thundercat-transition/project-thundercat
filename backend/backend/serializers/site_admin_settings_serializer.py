from rest_framework import serializers
from backend.custom_models.site_admin_setting import SiteAdminSetting


class SiteAdminSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = SiteAdminSetting
        fields = "__all__"
