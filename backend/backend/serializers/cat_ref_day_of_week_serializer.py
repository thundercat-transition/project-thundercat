from rest_framework import serializers
from backend.ref_table_views.cat_ref_day_of_week_vw import CatRefDayOfWeekVW


class CatRefDayOfWeekSerializer(serializers.ModelSerializer):
    class Meta:
        model = CatRefDayOfWeekVW
        fields = ["id", "day_of_week_id", "text", "language_id"]
