from itertools import groupby
from rest_framework import serializers
from backend.custom_models.language_text import LanguageText
from backend.custom_models.language import Language


class LanguageTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = LanguageText
        fields = "__all__"


class LanguageSerializer(serializers.ModelSerializer):
    language_text = serializers.SerializerMethodField()

    def get_language_text(self, request):
        # getting item options and ordering by language ID
        language_text = LanguageText.objects.filter(
            language_ref_id=request.language_id
        ).order_by("language_id")

        # getting serialized data
        serialized_data = LanguageTextSerializer(language_text, many=True).data

        # getting language data
        language_data = Language.objects.all()

        # initializing final_obj
        final_obj = {}

        # grouping text by language
        for obj, inner_group in groupby(serialized_data, lambda x: x["language"]):
            for language in language_data:
                # matching language
                if obj == language.language_id:
                    # populating obj
                    final_obj["{0}".format(language.ISO_Code_1)] = list(inner_group)

        # returning final_obj
        return final_obj

    class Meta:
        model = Language
        fields = "__all__"
