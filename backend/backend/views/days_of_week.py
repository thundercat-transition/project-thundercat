from rest_framework.response import Response
from backend.serializers.cat_ref_day_of_week_serializer import (
    CatRefDayOfWeekSerializer,
)
from backend.ref_table_views.cat_ref_day_of_week_vw import CatRefDayOfWeekVW


def get_days_of_week(request):
    # getting all days of week
    day_of_week = CatRefDayOfWeekVW.objects.all()

    # serializing data
    serialized_data = CatRefDayOfWeekSerializer(day_of_week, many=True).data

    # ordering by ID
    ordered_serialized_data = sorted(
        serialized_data,
        key=lambda k: k["day_of_week_id"],
        reverse=False,
    )

    return Response(ordered_serialized_data)
