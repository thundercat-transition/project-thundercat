import datetime
import json
from urllib import parse
import urllib.parse
from operator import itemgetter
from django.db.models import Q
from django.db import transaction
from django.utils import timezone
from rest_framework.response import Response
from rest_framework import status
from backend.api_permissions.role_based_api_permissions import user_has_permission
from backend.custom_models.reason_for_testing_priority import ReasonForTestingPriority
from backend.custom_models.test_center_ola_time_slot_prioritization import (
    TestCenterOlaTimeSlotPrioritization,
)
from backend.custom_models.test_center_ola_time_slot_assessor_availability import (
    TestCenterOlaTimeSlotAssessorAvailability,
)
from backend.custom_models.scorer_ola_assigned_test_session import (
    ScorerOlaAssignedTestSession,
)
from backend.custom_models.test_center_ola_time_slot import TestCenterOlaTimeSlot
from backend.custom_models.test_center_ola_test_assessor_approved_language import (
    TestCenterOlaTestAssessorApprovedLanguage,
)
from backend.custom_models.test_center_ola_test_assessor import (
    TestCenterOlaTestAssessor,
)
from backend.custom_models.test_center_ola_vacation_block import (
    TestCenterOlaVacationBlock,
)
from backend.custom_models.test_center_ola_vacation_block_availability import (
    TestCenterOlaVacationBlockAvailability,
)
from backend.custom_models.test_center_ola_configs import TestCenterOlaConfigs
from backend.custom_models.user_accommodation_file import UserAccommodationFile
from user_management.static.permission import Permission
from backend.views.gc_notify_view import (
    delete_non_standard_test_session,
    invite_candidate_to_non_standard_test_session,
    ola_invite_candidate_to_non_standard_test_session_in_person,
    ola_invite_candidate_to_non_standard_test_session_virtual,
    ola_update_non_standard_test_session_invitation_in_person,
    ola_update_non_standard_test_session_invitation_virtual,
    update_non_standard_test_session_invitation,
)
from backend.custom_models.user_accommodation_file_test_to_administer import (
    UserAccommodationFileTestToAdminister,
)
from backend.static.languages import Language_id
from backend.static.languages import Languages
from backend.custom_models.test_center_field_translations.test_center_room_data_other_details_text import (
    TestCenterRoomDataOtherDetailsText,
)
from backend.custom_models.test_center_field_translations.test_center_city_text import (
    TestCenterCityText,
)
from backend.custom_models.test_center_field_translations.test_center_other_details_text import (
    TestCenterOtherDetailsText,
)
from backend.custom_models.test_center_field_translations.test_center_province_text import (
    TestCenterProvinceText,
)
from backend.custom_models.language import Language
from backend.custom_models.test_center_field_translations.test_center_address_text import (
    TestCenterAddressText,
)
from cms.serializers.test_definition_serializer import TestDefinitionSerializer
from cms.cms_models.test_skill_sle import TestSkillSLE
from cms.cms_models.test_skill_occupational import TestSkillOccupational
from cms.cms_models.test_skill import TestSkill
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.orderless_test_permissions_model import OrderlessTestPermissions
from cms.cms_models.test_permissions_model import TestPermissions

from cms.views.utils import get_needed_parameters, get_optional_parameters
from user_management.views.utils import CustomPagination
from user_management.user_management_models.ta_extended_profile_models import (
    TaExtendedProfile,
)
from user_management.user_management_models.user_models import User
from db_views.serializers.test_center_serializers import (
    TestCenterTestSessionsSerializer,
    TestCentersViewSerializer,
    SelectedTestCenterAsEttaDetailedViewSerializer,
    SelectedTestCenterDetailedViewSerializer,
)
from db_views.db_view_models.test_centers_vw import TestCentersVW
from db_views.db_view_models.test_center_test_administrators_vw import (
    TestCenterTestAdministratorsVW,
)
from db_views.serializers.test_center_serializers import (
    TestCenterTestAdministratorsViewSerializer,
    TestCenterRoomsViewSerializer,
    TestCenterTestSessionsViewSerializer,
)
from db_views.db_view_models.test_center_rooms_vw import TestCenterRoomsVW
from db_views.db_view_models.test_center_test_sessions_vw import (
    TestCenterTestSessionsVW,
)
from db_views.serializers.hr_coordinator_serializers import (
    AssessmentProcessAssignedTestSpecsViewSerializer,
    SimplifiedAssessmentProcessViewSerializer,
)
from db_views.db_view_models.assessment_process_assigned_test_specs_vw import (
    AssessmentProcessAssignedTestSpecsVW,
)
from db_views.db_view_models.test_session_attendees_vw import TestSessionAttendeesVW
from db_views.serializers.test_session_serializers import (
    TestSessionAttendeesViewSerializer,
)
from db_views.db_view_models.assessment_process_vw import AssessmentProcessVW

from backend.custom_models.test_center_associated_managers import (
    TestCenterAssociatedManagers,
)
from backend.views.utils import (
    TEST_SESSION_DATA_SOURCE,
    get_current_utc_offset_in_seconds,
    get_user_info_from_jwt_token,
)
from backend.custom_models.test_center import TestCenter
from backend.custom_models.test_center_test_administrators import (
    TestCenterTestAdministrators,
)
from backend.custom_models.test_center_room_data import (
    TestCenterRoomData,
)
from backend.custom_models.test_center_rooms import (
    TestCenterRooms,
)
from backend.custom_models.test_center_test_session_data import (
    TestCenterTestSessionData,
)
from backend.custom_models.test_center_test_sessions import TestCenterTestSessions
from backend.celery.tasks import (
    create_test_session_email_celery_task,
    send_delete_specific_test_session_email_celery_task,
    test_session_cancellation_inform_related_hr_coordinator,
)
from backend.celery.task_definition.test_center import TestSessionEmailAction
from backend.static.test_skill_codenames import (
    TestSkillSLEDescCodename,
    TestSkillTypeCodename,
)
from backend.serializers.test_center_serializer import (
    TestCenterTestAdministratorsSerializer,
)
from backend.custom_models.test_access_code_model import TestAccessCode
from backend.custom_models.consumed_reservation_codes import ConsumedReservationCodes
from backend.static.consumed_reservation_code_statuses import (
    StaticConsumedReservationCodeStatus,
)
from backend.custom_models.consumed_reservation_code_status import (
    ConsumedReservationCodeStatus,
)
from backend.custom_models.test_center_test_session_officers import (
    TestCenterTestSessionOfficers,
)
from backend.serializers.reports_data_serializer import UserSerializer


def create_new_test_center_as_etta(request):
    success, parameters = get_needed_parameters(["name", "department_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    name, department_id = itemgetter("name", "department_id")(parameters)

    test_center = TestCenter.objects.create(
        name=name,
        department_id=department_id,
        # default booking delay of 48 hours
        booking_delay=48,
        # all other field must remain empty (for now)
    )

    # getting languages
    languages = Language.objects.all()

    # creating empty data - with languages
    # Note: by default, the text is already set as ""
    for language in languages:
        TestCenterAddressText.objects.create(test_center=test_center, language=language)
        TestCenterCityText.objects.create(test_center=test_center, language=language)
        TestCenterProvinceText.objects.create(
            test_center=test_center, language=language
        )
        TestCenterOtherDetailsText.objects.create(
            test_center=test_center, language=language
        )

    return Response(status=status.HTTP_200_OK)


# getting all test centers as ETTA
def get_all_test_centers_as_etta(request):
    success, parameters = get_needed_parameters(["page", "page_size"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting all test centers
    test_centers = TestCentersVW.objects.all()

    # serializing data
    serialized_data = TestCentersViewSerializer(test_centers, many=True).data

    # getting sorted serialized data by name (ascending)
    ordered_test_centers = sorted(
        serialized_data,
        key=lambda k: k["name"].lower(),
        reverse=False,
    )
    # only getting data for current selected page
    new_test_centers = ordered_test_centers[
        (current_page - 1) * page_size : current_page * page_size
    ]
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_test_centers,
        len(ordered_test_centers),
        current_page,
        page_size,
    )


# getting found test centers as ETTA
def get_found_test_centers_as_etta(request):
    success, parameters = get_needed_parameters(
        ["keyword", "current_language", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, current_language, keyword = itemgetter(
        "page", "page_size", "current_language", "keyword"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        found_test_centers = TestCentersVW.objects.all()
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # search while interface is in English
        if current_language == "en":
            found_test_centers = TestCentersVW.objects.filter(
                Q(name__icontains=keyword)
                | Q(dept_eabrv__icontains=keyword)
                | Q(dept_edesc__icontains=keyword)
            )
        # search while interface is in French
        else:
            found_test_centers = TestCentersVW.objects.filter(
                Q(name__icontains=keyword)
                | Q(dept_fabrv__icontains=keyword)
                | Q(dept_fdesc__icontains=keyword)
            )

        # no results found
        if not found_test_centers:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # getting serialized active tests data
    serialized_found_test_centers = TestCentersViewSerializer(
        found_test_centers, many=True
    ).data
    # getting sorted serialized data by last_name (ascending)
    ordered_found_test_centers = sorted(
        serialized_found_test_centers,
        key=lambda k: k["name"].lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_found_test_centers = ordered_found_test_centers[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_found_test_centers,
        len(found_test_centers),
        current_page,
        page_size,
    )


# getting specific test center data as ETTA
def get_selected_test_center_data_as_etta(request):
    success, parameters = get_needed_parameters(["id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    id = itemgetter("id")(parameters)

    try:
        # getting specified test center data
        test_center_data = TestCentersVW.objects.get(id=id)

        # serializing and returning data
        serialized_data = SelectedTestCenterAsEttaDetailedViewSerializer(
            test_center_data, many=False
        ).data
        return Response(serialized_data)

    except TestCentersVW.DoesNotExist:
        return Response(
            {"error": "the specified test center does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )


def update_test_center_data_as_etta(request):
    test_center_data = json.loads(request.body)

    try:
        # getting test center
        test_center = TestCenter.objects.get(id=test_center_data["id"])

        # updating data
        test_center.name = test_center_data["name"]
        test_center.department_id = test_center_data["department_id"]
        test_center.ola_authorized = test_center_data["ola_authorized"]
        test_center.save()

        return Response(status=status.HTTP_200_OK)

    except TestCenter.DoesNotExist:
        return Response(
            {"error": "The Test Center Does Not Exist"},
            status=status.HTTP_404_NOT_FOUND,
        )


def add_test_center_manager_accesses(request):
    success, parameters = get_needed_parameters(["user_id", "test_center_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    user_id, test_center_id = itemgetter("user_id", "test_center_id")(parameters)

    # decoding username that might contain special characters
    user_id = urllib.parse.unquote(user_id)

    # making sure that the specified Test Center Manager combined with the test center ID does not already exist
    if not TestCenterAssociatedManagers.objects.filter(
        user_id=user_id, test_center_id=test_center_id
    ):
        # creating new entry in Test Center Associated Managers model
        TestCenterAssociatedManagers.objects.create(
            user_id=user_id,
            test_center_id=test_center_id,
        )
        return Response(status=status.HTTP_200_OK)
    else:
        return Response(
            {
                "error": "The specified test center manager is already associated to this test center"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


def delete_test_center_manager_accesses(request):
    success, parameters = get_needed_parameters(["user_id", "test_center_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    user_id, test_center_id = itemgetter("user_id", "test_center_id")(parameters)

    # decoding username that might contain special characters
    user_id = urllib.parse.unquote(user_id)

    # getting test center accesses for the specified user (using filter to delete all entries if needed - avoiding issues of more than one row exists)
    test_center_accesses = TestCenterAssociatedManagers.objects.filter(
        user_id=user_id, test_center_id=test_center_id
    )
    for test_center_access in test_center_accesses:
        # deleting entry
        test_center_access.delete()
    return Response(status=status.HTTP_200_OK)


# getting associated test centers
def get_associated_test_centers(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(["page", "page_size"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting associated test center IDs
    associated_test_center_ids = TestCenterAssociatedManagers.objects.filter(
        user_id=user_info["user_id"]
    ).values_list("test_center_id", flat=True)

    # getting all related test centers
    related_test_centers = TestCentersVW.objects.filter(
        id__in=associated_test_center_ids
    )

    # serializing data
    serialized_data = TestCentersViewSerializer(related_test_centers, many=True).data

    # getting sorted serialized data by name (ascending)
    ordered_related_test_centers = sorted(
        serialized_data,
        key=lambda k: k["name"].lower(),
        reverse=False,
    )
    # only getting data for current selected page
    new_related_test_centers = ordered_related_test_centers[
        (current_page - 1) * page_size : current_page * page_size
    ]
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_related_test_centers,
        len(ordered_related_test_centers),
        current_page,
        page_size,
    )


# getting found associated test centers
def get_found_associated_test_centers(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(
        ["keyword", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, keyword = itemgetter("page", "page_size", "keyword")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting associated test center IDs
    associated_test_center_ids = TestCenterAssociatedManagers.objects.filter(
        user_id=user_info["user_id"]
    ).values_list("test_center_id", flat=True)

    # if blank search
    if keyword == " ":
        # getting all related test centers
        found_test_centers = TestCentersVW.objects.filter(
            id__in=associated_test_center_ids
        )
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # getting matching test centers
        found_test_centers_ids = TestCentersVW.objects.filter(
            Q(id__in=associated_test_center_ids) & (Q(name__icontains=keyword))
        ).values_list("id", flat=True)

        # since address is translatable, we have to search in its table too
        found_test_center_addresses_ids = TestCenterAddressText.objects.filter(
            Q(test_center__in=associated_test_center_ids) & Q(text__icontains=keyword)
        ).values_list("test_center_id", flat=True)

        # get the final list of test centers
        found_test_centers = TestCentersVW.objects.filter(
            Q(id__in=found_test_centers_ids) | Q(id__in=found_test_center_addresses_ids)
        )

    # no results found
    if not found_test_centers:
        return Response({"no results found"}, status=status.HTTP_200_OK)

    # serializing data
    serialized_data = TestCentersViewSerializer(found_test_centers, many=True).data

    # getting sorted serialized data by name (ascending)
    ordered_found_test_centers = sorted(
        serialized_data,
        key=lambda k: k["name"].lower(),
        reverse=False,
    )
    # only getting data for current selected page
    new_found_test_centers = ordered_found_test_centers[
        (current_page - 1) * page_size : current_page * page_size
    ]
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_found_test_centers,
        len(found_test_centers),
        current_page,
        page_size,
    )


# getting specific test center data
def get_selected_test_center_data(request):
    success, parameters = get_needed_parameters(["id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    id = itemgetter("id")(parameters)

    try:
        # getting specified test center data
        test_center_data = TestCentersVW.objects.get(id=id)

        # serializing and returning data
        serialized_data = SelectedTestCenterDetailedViewSerializer(
            test_center_data, many=False
        ).data
        return Response(serialized_data)

    except TestCentersVW.DoesNotExist:
        return Response(
            {"error": "the specified test center does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )


def update_test_center_data(request):
    test_center_data = json.loads(request.body)

    try:
        # getting languages
        languages = Language.objects.all()

        # getting test center
        test_center = TestCenter.objects.get(id=test_center_data["id"])

        # updating data
        test_center.postal_code = test_center_data["postal_code"]
        test_center.country_id = test_center_data["country_id"]
        test_center.security_email = test_center_data["security_email"]
        test_center.booking_delay = test_center_data["booking_delay"]
        test_center.accommodations_friendly = test_center_data[
            "accommodations_friendly"
        ]
        test_center.save()

        # getting test center address text
        test_center_addresses = TestCenterAddressText.objects.filter(
            test_center_id=test_center_data["id"]
        )

        # getting test center city text
        test_center_cities = TestCenterCityText.objects.filter(
            test_center_id=test_center_data["id"]
        )

        # getting test center province text
        test_center_provinces = TestCenterProvinceText.objects.filter(
            test_center_id=test_center_data["id"]
        )

        # getting test center other details text
        test_center_other_details = TestCenterOtherDetailsText.objects.filter(
            test_center_id=test_center_data["id"]
        )

        # updating data - with languages
        for language in languages:
            # Address
            for address in test_center_addresses:
                if address.language == language:
                    # make sure we update the text field only if different
                    if (
                        test_center_data["address"]["{0}".format(language.ISO_Code_1)][
                            0
                        ]["text"]
                        != address.text
                    ):
                        address.text = test_center_data["address"][
                            "{0}".format(language.ISO_Code_1)
                        ][0]["text"]
                        address.save()
                        break
            # City
            for city in test_center_cities:
                if city.language == language:
                    # make sure we update the text field only if different
                    if (
                        test_center_data["city"]["{0}".format(language.ISO_Code_1)][0][
                            "text"
                        ]
                        != city.text
                    ):
                        city.text = test_center_data["city"][
                            "{0}".format(language.ISO_Code_1)
                        ][0]["text"]
                        city.save()
                        break
            # Province
            for province in test_center_provinces:
                if province.language == language:
                    # make sure we update the text field only if different
                    if (
                        test_center_data["province"]["{0}".format(language.ISO_Code_1)][
                            0
                        ]["text"]
                        != province.text
                    ):
                        province.text = test_center_data["province"][
                            "{0}".format(language.ISO_Code_1)
                        ][0]["text"]
                        province.save()
                        break
            # Other Details
            for other_details in test_center_other_details:
                if other_details.language == language:
                    # make sure we update the text field only if different
                    if (
                        test_center_data["other_details"][
                            "{0}".format(language.ISO_Code_1)
                        ][0]["text"]
                        != other_details.text
                    ):
                        other_details.text = test_center_data["other_details"][
                            "{0}".format(language.ISO_Code_1)
                        ][0]["text"]
                        other_details.save()
                        break

        return Response(status=status.HTTP_200_OK)

    except TestCenter.DoesNotExist:
        return Response(
            {"error": "The Test Center Does Not Exist"},
            status=status.HTTP_404_NOT_FOUND,
        )


def associate_test_administrators_to_test_center(request):
    test_administrator_data = json.loads(request.body)

    # looping in provided test administrators
    for test_admin in test_administrator_data["test_administrators"]:
        # creating new entry in TestCenterTestAdministrators model
        TestCenterTestAdministrators.objects.create(
            user_id=test_admin["value"],
            test_center_id=test_administrator_data["test_center_id"],
        )

    return Response(status=status.HTTP_200_OK)


# getting test center associated test administrators
def get_associated_test_administrators_to_test_center(request):
    success, parameters = get_needed_parameters(
        ["page", "page_size", "test_center_id"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, test_center_id = itemgetter("page", "page_size", "test_center_id")(
        parameters
    )

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting associated test administrators
    associated_test_administrators = TestCenterTestAdministratorsVW.objects.filter(
        test_center_id=test_center_id
    )

    # serializing and returning data
    serialized_data = TestCenterTestAdministratorsViewSerializer(
        associated_test_administrators, many=True
    ).data

    # getting sorted serialized data by last name (ascending)
    ordered_associated_test_administrators = sorted(
        serialized_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )
    # only getting data for current selected page
    new_associated_test_administrators = ordered_associated_test_administrators[
        (current_page - 1) * page_size : current_page * page_size
    ]
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_associated_test_administrators,
        len(ordered_associated_test_administrators),
        current_page,
        page_size,
    )


# getting found test center associated test administrators
def get_found_associated_test_administrators_to_test_center(request):
    success, parameters = get_needed_parameters(
        ["keyword", "page", "page_size", "test_center_id"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    keyword, page, page_size, test_center_id = itemgetter(
        "keyword", "page", "page_size", "test_center_id"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        # getting all associated test administrators
        found_associated_test_administrators = (
            TestCenterTestAdministratorsVW.objects.filter(test_center_id=test_center_id)
        )
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # getting keyword matches
        found_associated_test_administrators = (
            TestCenterTestAdministratorsVW.objects.filter(
                Q(test_center_id=test_center_id)
                & (
                    Q(first_name__icontains=keyword)
                    | Q(last_name__icontains=keyword)
                    | Q(email__icontains=keyword)
                )
            )
        )

        # no results found
        if not found_associated_test_administrators:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # serializing and returning data
    serialized_data = TestCenterTestAdministratorsViewSerializer(
        found_associated_test_administrators, many=True
    ).data

    # getting sorted serialized data by last name (ascending)
    ordered_found_associated_test_administrators = sorted(
        serialized_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )
    # only getting data for current selected page
    new_found_associated_test_administrators = (
        ordered_found_associated_test_administrators[
            (current_page - 1) * page_size : current_page * page_size
        ]
    )
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_found_associated_test_administrators,
        len(ordered_found_associated_test_administrators),
        current_page,
        page_size,
    )


def delete_associated_test_administrator_from_test_center(request):
    test_center_test_administrator_data = json.loads(request.body)

    # deleting respective entry
    try:
        # adding that test center ID in the filter just to make sure we're not deleting wrong data
        TestCenterTestAdministrators.objects.get(
            id=test_center_test_administrator_data["id"],
            test_center_id=test_center_test_administrator_data["test_center_id"],
        ).delete()

    except TestCenterTestAdministrators.DoesNotExist:
        return Response(
            {"error": "the specified test center test administrator does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )

    return Response(status=status.HTTP_200_OK)


def add_room_to_test_center(request):
    room_data = json.loads(request.body)

    # creating new entry in TestCenterRoomData model
    new_room_data = TestCenterRoomData.objects.create(
        name=room_data["name"],
        email=room_data["email"],
        max_occupancy=room_data["max_occupancy"],
        active=room_data["active"],
    )

    # Create English Other Details Text
    TestCenterRoomDataOtherDetailsText.objects.create(
        text=room_data["other_details"][Languages.EN][0]["text"],
        language_id=Language_id.EN,
        test_center_room_data=new_room_data,
    )
    # Create French Other Details Text
    TestCenterRoomDataOtherDetailsText.objects.create(
        text=room_data["other_details"][Languages.FR][0]["text"],
        language_id=Language_id.FR,
        test_center_room_data=new_room_data,
    )

    # creating new entry in TestCenterRooms model
    TestCenterRooms.objects.create(
        room_data_id=new_room_data.id, test_center_id=room_data["test_center_id"]
    )

    return Response(status=status.HTTP_200_OK)


# getting test center rooms
def get_test_center_rooms(request):
    success, parameters = get_needed_parameters(
        ["page", "page_size", "test_center_id"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, test_center_id = itemgetter("page", "page_size", "test_center_id")(
        parameters
    )

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting test center rooms
    rooms = TestCenterRoomsVW.objects.filter(test_center_id=test_center_id)

    # serializing and returning data
    serialized_data = TestCenterRoomsViewSerializer(rooms, many=True).data

    # getting sorted serialized data by last name (ascending)
    ordered_rooms = sorted(
        serialized_data,
        key=lambda k: k["name"].lower(),
        reverse=False,
    )
    # only getting data for current selected page
    new_rooms = ordered_rooms[(current_page - 1) * page_size : current_page * page_size]
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_rooms,
        len(ordered_rooms),
        current_page,
        page_size,
    )


# getting found test center rooms
def get_found_test_center_rooms(request):
    success, parameters = get_needed_parameters(
        ["keyword", "page", "page_size", "test_center_id"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    keyword, page, page_size, test_center_id = itemgetter(
        "keyword", "page", "page_size", "test_center_id"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        # getting all respective rooms
        found_rooms = TestCenterRoomsVW.objects.filter(test_center_id=test_center_id)
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # getting matching test center rooms
        found_rooms = TestCenterRoomsVW.objects.filter(
            Q(test_center_id=test_center_id)
            & (Q(name__icontains=keyword) | Q(max_occupancy__icontains=keyword))
        )

        # no results found
        if not found_rooms:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # serializing and returning data
    serialized_data = TestCenterRoomsViewSerializer(found_rooms, many=True).data

    # getting sorted serialized data by last name (ascending)
    ordered_found_rooms = sorted(
        serialized_data,
        key=lambda k: k["name"].lower(),
        reverse=False,
    )
    # only getting data for current selected page
    new_found_rooms = ordered_found_rooms[
        (current_page - 1) * page_size : current_page * page_size
    ]
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_found_rooms,
        len(ordered_found_rooms),
        current_page,
        page_size,
    )


def edit_test_center_room(request):
    room_data = json.loads(request.body)

    try:
        # getting related room data ID
        # adding that test center ID in the filter just to make sure we're not deleting wrong data
        related_room_data_id = TestCenterRooms.objects.get(
            id=room_data["id"], test_center_id=room_data["test_center_id"]
        ).room_data_id

        # getting related room data and saving based on provided data
        related_room_data = TestCenterRoomData.objects.get(id=related_room_data_id)
        related_room_data.name = room_data["name"]
        related_room_data.email = room_data["email"]
        related_room_data.max_occupancy = room_data["max_occupancy"]
        related_room_data.active = room_data["active"]
        related_room_data.save()

        # edit other details text
        related_room_data_other_details = (
            TestCenterRoomDataOtherDetailsText.objects.filter(
                test_center_room_data=related_room_data
            )
        )

        # get languages
        languages = Language.objects.all()

        # updating other details data - with languages
        for language in languages:
            # Other Details
            for other_details in related_room_data_other_details:
                if other_details.language == language:
                    # make sure we update the text field only if different
                    if (
                        room_data["other_details"]["{0}".format(language.ISO_Code_1)][
                            0
                        ]["text"]
                        != other_details.text
                    ):
                        other_details.text = room_data["other_details"][
                            "{0}".format(language.ISO_Code_1)
                        ][0]["text"]
                        other_details.save()
                        break

        return Response(status=status.HTTP_200_OK)

    except TestCenterRooms.DoesNotExist:
        return Response(
            {"error": "the specified test center room does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )
    except TestCenterRoomData.DoesNotExist:
        return Response(
            {"error": "the specified test center room data does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )


def delete_test_center_room(request):
    room_data = json.loads(request.body)

    # getting current datetime (UTC)
    now = timezone.now()

    # getting test sessions associated to provided room ID (only getting current and future test sessions)
    respective_test_sessions = TestCenterTestSessionsVW.objects.filter(
        test_center_id=room_data["test_center_id"],
        test_center_room_id=room_data["id"],
        end_time__gte=now,
    )

    # room is in use for current and/or future test sessions
    if respective_test_sessions:
        return Response(
            {
                "error": "the specified test center room is in use for current and/or future test sessions"
            },
            status=status.HTTP_409_CONFLICT,
        )

    try:
        # getting and deleting related room
        # adding that test center ID in the filter just to make sure we're not deleting wrong data
        related_room = TestCenterRooms.objects.get(
            id=room_data["id"], test_center_id=room_data["test_center_id"]
        )
        related_room_id = related_room.id
        # deleting related room
        related_room.delete()

        # getting related room data
        related_room_data = TestCenterRoomData.objects.get(id=related_room_id)

        # deleting all other details text
        TestCenterRoomDataOtherDetailsText.objects.filter(
            test_center_room_data=related_room_data
        ).delete()

        # deleting related room data
        related_room_data.delete()

        return Response(status=status.HTTP_200_OK)

    except TestCenterRooms.DoesNotExist:
        return Response(
            {"error": "the specified test center room does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )
    except TestCenterRoomData.DoesNotExist:
        return Response(
            {"error": "the specified test center room data does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )


def validate_test_session_overlapping_time(
    test_center_room_id, date, start_time, end_time, id=None, test_assessor_user_id=None
):
    # initializing is_valid
    is_valid = True

    # converting start_time and end_time in timestamp
    converted_start_time = datetime.datetime.timestamp(start_time)
    converted_end_time = datetime.datetime.timestamp(end_time)

    # id is provided (edit action)
    if id is not None:
        # test_assessor_user_id is defined (OLA Test)
        if test_assessor_user_id is not None:
            # getting all test sessions related to provided test_center_room_id at the provided date for the provided test_assessor_user_id
            all_test_sessions_related_to_room = TestCenterTestSessionsVW.objects.filter(
                test_center_room_id=test_center_room_id,
                date=date,
                test_assessor_user_id=test_assessor_user_id,
            ).exclude(id=id)
        # other than OLA Test
        else:
            # getting all test sessions related to provided test_center_room_id at the provided date, but excluding its own ID since this is being called for the Edit action
            all_test_sessions_related_to_room = TestCenterTestSessionsVW.objects.filter(
                test_center_room_id=test_center_room_id, date=date
            ).exclude(id=id)
    # id is NOT provided (add action)
    else:
        # test_assessor_user_id is defined (OLA Test)
        if test_assessor_user_id is not None:
            # getting all test sessions related to provided test_center_room_id at the provided date for the provided test_assessor_user_id
            all_test_sessions_related_to_room = TestCenterTestSessionsVW.objects.filter(
                test_center_room_id=test_center_room_id,
                date=date,
                test_assessor_user_id=test_assessor_user_id,
            )
        # other than OLA Test
        else:
            # getting all test sessions related to provided test_center_room_id at the provided date
            all_test_sessions_related_to_room = TestCenterTestSessionsVW.objects.filter(
                test_center_room_id=test_center_room_id, date=date
            )

    # looping in all_test_sessions_related_to_room
    for test_session in all_test_sessions_related_to_room:
        # converting start_time and end_time of current iteration in timestamp
        converted_iteration_start_time = datetime.datetime.timestamp(
            test_session.start_time
        )
        converted_iteration_end_time = datetime.datetime.timestamp(
            test_session.end_time
        )

        # time range is overlapping time of current iteration
        if (
            converted_end_time > converted_iteration_start_time
            and converted_iteration_end_time > converted_start_time
        ):
            # set is_valid to False
            is_valid = False

    # returning is_valid state
    return is_valid


def add_test_session_to_test_center(request):
    test_session_data = json.loads(request.body)

    # initializing desired date format
    date_format = "%Y-%m-%d %H:%M:%S.%f %z"

    # get and convert start time based on provided data
    utc_start_time = datetime.datetime.strptime(
        test_session_data["utc_start_time"], date_format
    )

    # get and convert end time based on provided data
    utc_end_time = datetime.datetime.strptime(
        test_session_data["utc_end_time"], date_format
    )

    # validating test session overlapping time
    is_valid_test_session_time = validate_test_session_overlapping_time(
        test_session_data["test_center_room_id"],
        test_session_data["date"],
        utc_start_time,
        utc_end_time,
    )

    # not valid
    if not is_valid_test_session_time:
        return Response(
            {
                "info": "there is an overlapping time conflict with your current room/time settings"
            },
            status=status.HTTP_409_CONFLICT,
        )

    # creating new entry in TestCenterTestSessionData model
    new_test_session_data = TestCenterTestSessionData.objects.create(
        test_center_room_id=test_session_data["test_center_room_id"],
        open_to_ogd=test_session_data["open_to_ogd"],
        date=test_session_data["date"],
        start_time=utc_start_time,
        end_time=utc_end_time,
        spaces_available=test_session_data["spaces_available"],
        test_skill_type_id=test_session_data["test_skill_type_id"],
        test_skill_sub_type_id=test_session_data["test_skill_sub_type_id"],
    )

    # creating new entry in TestCenterRooms model
    new_test_session = TestCenterTestSessions.objects.create(
        test_session_data_id=new_test_session_data.id,
        test_center_id=test_session_data["test_center_id"],
        is_standard=1,
        user_accommodation_file_id=None,
    )

    # getting respective room email
    respective_room_email = TestCenterRoomsVW.objects.get(
        id=test_session_data["test_center_room_id"]
    ).email

    # Get data from TestCenterTestSessionsVW
    test_center_test_session_data = TestCenterTestSessionsVW.objects.get(
        id=new_test_session.id
    )
    test_center_test_session_data = TestCenterTestSessionsViewSerializer(
        test_center_test_session_data, many=False
    ).data

    # sending meeting invite through email to respective recipient
    create_test_session_email_celery_task.delay(
        [
            {
                "action": TestSessionEmailAction.ADD,
                "test_session_id": test_center_test_session_data["id"],
                "email": respective_room_email,
                "date": test_center_test_session_data["date"],
                "start_time": utc_start_time,
                "end_time": utc_end_time,
                "simplified_start_time": test_center_test_session_data[
                    "simplified_start_time"
                ],
                "simplified_end_time": test_center_test_session_data[
                    "simplified_end_time"
                ],
                "test_skill_type_en_name": test_center_test_session_data[
                    "test_skill_type_en_name"
                ],
                "test_skill_type_fr_name": test_center_test_session_data[
                    "test_skill_type_fr_name"
                ],
                "test_skill_sub_type_en_name": test_center_test_session_data[
                    "test_skill_sub_type_en_name"
                ],
                "test_skill_sub_type_fr_name": test_center_test_session_data[
                    "test_skill_sub_type_fr_name"
                ],
                "room_name": test_center_test_session_data["room_name"],
            }
        ]
    )

    return Response(status=status.HTTP_200_OK)


def handle_add_room_utils(
    test_center_test_session_data,
    test_center_room_id,
    user_accommodation_file_id,
    utc_start_time,
    utc_end_time,
):
    # getting respective room email
    respective_room_email = TestCenterRoomsVW.objects.filter(id=test_center_room_id)

    # getting test related data
    user_accommodation_file_test_to_administer_data = (
        UserAccommodationFileTestToAdminister.objects.get(
            user_accommodation_file_id=user_accommodation_file_id
        )
    )

    # if there is a defined test definition
    if user_accommodation_file_test_to_administer_data.test_id is not None:
        related_test_data = TestDefinition.objects.get(
            id=user_accommodation_file_test_to_administer_data.test_id
        )
        serialized_related_test_data = TestDefinitionSerializer(
            related_test_data, many=False
        ).data
    # no defined test definition
    else:
        serialized_related_test_data = {
            "en_name": user_accommodation_file_test_to_administer_data.specified_test_description,
            "fr_name": user_accommodation_file_test_to_administer_data.specified_test_description,
        }

    # setting room_name
    room_name = ""
    if test_center_test_session_data["room_name"] is not None:
        room_name = test_center_test_session_data["room_name"]

    # if room exists
    if respective_room_email:
        # sending meeting invite through email to respective recipient
        create_test_session_email_celery_task.delay(
            [
                {
                    "action": TestSessionEmailAction.ADD,
                    "test_session_id": test_center_test_session_data["id"],
                    "email": respective_room_email.last().email,
                    "date": test_center_test_session_data["date"],
                    "start_time": utc_start_time,
                    "end_time": utc_end_time,
                    "simplified_start_time": test_center_test_session_data[
                        "simplified_start_time"
                    ],
                    "simplified_end_time": test_center_test_session_data[
                        "simplified_end_time"
                    ],
                    "test_name_en": serialized_related_test_data["en_name"],
                    "test_name_fr": serialized_related_test_data["fr_name"],
                    "room_name": room_name,
                }
            ]
        )

    return {
        "serialized_related_test_data": serialized_related_test_data,
        "room_name": room_name,
    }


def add_non_standard_test_session_to_test_center(request):
    test_session_data = json.loads(request.body)

    # initializing desired date format
    date_format = "%Y-%m-%d %H:%M:%S.%f %z"

    # initializing email_prep_array
    email_prep_array = []

    # initializing candidate_id
    candidate_id = None

    try:
        with transaction.atomic():
            # looping in the provided test_session_data
            for index, test_session in enumerate(test_session_data):
                # getting assigned test specs data (in order to get the test skill data)
                assigned_test_specs_data = (
                    AssessmentProcessAssignedTestSpecsVW.objects.get(
                        id=test_session["assessment_process_assigned_test_specs_id"]
                    )
                )

                # setting is_ola flag
                is_ola = (
                    assigned_test_specs_data.test_skill_sub_type_codename
                    == TestSkillSLEDescCodename.ORAL_EN
                    or assigned_test_specs_data.test_skill_sub_type_codename
                    == TestSkillSLEDescCodename.ORAL_FR
                )

                # get and convert start time based on provided data
                utc_start_time = datetime.datetime.strptime(
                    test_session["utc_start_time"], date_format
                )

                # get and convert end time based on provided data
                utc_end_time = datetime.datetime.strptime(
                    test_session["utc_end_time"], date_format
                )

                # initializing test_assessor_user_id and candidate_phone_number
                test_assessor_user_id = None
                candidate_phone_number = None

                # OLA Test
                if is_ola:
                    # setting test_assessor_user_id
                    test_assessor_user_id = test_session["test_assessor_user_id"]
                    # setting candidate_phone_number
                    candidate_phone_number = UserAccommodationFile.objects.get(
                        id=assigned_test_specs_data.user_accommodation_file_id
                    ).candidate_phone_number

                # validating test session overlapping time
                is_valid_test_session_time = validate_test_session_overlapping_time(
                    test_session["test_center_room_id"],
                    test_session["date"],
                    utc_start_time,
                    utc_end_time,
                    None,
                    test_assessor_user_id,
                )

                # not valid
                if not is_valid_test_session_time:
                    return Response(
                        {
                            "info": "there is an overlapping time conflict with your current room/time settings"
                        },
                        status=status.HTTP_409_CONFLICT,
                    )

                # creating new entry in TestCenterTestSessionData model
                new_test_session_data = TestCenterTestSessionData.objects.create(
                    test_center_room_id=test_session["test_center_room_id"],
                    open_to_ogd=None,
                    date=test_session["date"],
                    start_time=utc_start_time,
                    end_time=utc_end_time,
                    spaces_available=None,
                    test_skill_type_id=assigned_test_specs_data.test_skill_type_id,
                    test_skill_sub_type_id=assigned_test_specs_data.test_skill_sub_type_id,
                    test_assessor_user_id=test_session["test_assessor_user_id"],
                    candidate_phone_number=candidate_phone_number,
                )

                # creating new entry in TestCenterRooms model
                new_test_session = TestCenterTestSessions.objects.create(
                    test_session_data_id=new_test_session_data.id,
                    test_center_id=test_session["test_center_id"],
                    is_standard=0,
                    user_accommodation_file_id=test_session[
                        "user_accommodation_file_id"
                    ],
                    test_session_order=index + 1,
                    assessment_process_assigned_test_specs_id=test_session[
                        "assessment_process_assigned_test_specs_id"
                    ],
                )

                # OLA Test
                if is_ola:
                    # linking test session(s) to scorer/test assessor
                    # creating new entry in
                    ScorerOlaAssignedTestSession.objects.create(
                        test_session_id=new_test_session.id,
                        test_assessor_id=test_session["test_assessor_user_id"],
                        candidate_id=assigned_test_specs_data.candidate_user_id,
                    )

                # if first iteration
                if index == 0:
                    # addign new test session to ConsumedReservationCodes model
                    related_consumed_reservation_code_data = (
                        ConsumedReservationCodes.objects.get(
                            assessment_process_assigned_test_specs_id=test_session[
                                "assessment_process_assigned_test_specs_id"
                            ]
                        )
                    )
                    related_consumed_reservation_code_data.test_session_id = (
                        new_test_session.id
                    )

                    # setting candidate_id
                    candidate_id = related_consumed_reservation_code_data.candidate_id

                    # uppdating consumed reservation code status to BOOKED
                    booked_status_id = ConsumedReservationCodeStatus.objects.get(
                        codename=StaticConsumedReservationCodeStatus.BOOKED
                    ).id
                    related_consumed_reservation_code_data.status_id = booked_status_id

                    related_consumed_reservation_code_data.save()

                # populating email_prep_array
                email_prep_array.append(
                    {
                        "test_center_room_id": test_session["test_center_room_id"],
                        "user_accommodation_file_id": test_session[
                            "user_accommodation_file_id"
                        ],
                        "new_test_session_id": new_test_session.id,
                    }
                )

    except:
        return Response(
            {
                "error": "Something happened during the add non standard test session process"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )

    # at this point, all the data should have been created, so looping in email_prep_array in order to send the needed emails (need to loop a second time in order to allow the transaction Atomic to do his job in case of issues)
    for test_session in email_prep_array:
        # getting data from TestCenterTestSessionsVW
        test_center_test_session_data = TestCenterTestSessionsVW.objects.get(
            id=test_session["new_test_session_id"]
        )

        test_center_test_session_data = TestCenterTestSessionsViewSerializer(
            test_center_test_session_data, many=False
        ).data

        # add respective room and return needed obj
        add_room_obj = handle_add_room_utils(
            test_center_test_session_data,
            test_session["test_center_room_id"],
            test_session["user_accommodation_file_id"],
            utc_start_time,
            utc_end_time,
        )

        # getting/setting respective invitation data
        user_data = User.objects.get(id=candidate_id)

        invitation_data = {
            "email": user_data.email,
            "date": test_center_test_session_data["date"],
            "start_time": test_center_test_session_data["simplified_start_time"],
            "end_time": test_center_test_session_data["simplified_end_time"],
            "test_name_en": add_room_obj["serialized_related_test_data"]["en_name"],
            "test_name_fr": add_room_obj["serialized_related_test_data"]["fr_name"],
            "room_name": add_room_obj["room_name"],
            "test_center_address_text_en": test_center_test_session_data[
                "test_center_address_text"
            ]["en"][0]["text"],
            "test_center_address_text_fr": test_center_test_session_data[
                "test_center_address_text"
            ]["fr"][0]["text"],
            "test_center_city_text_en": test_center_test_session_data[
                "test_center_city_text"
            ]["en"][0]["text"],
            "test_center_city_text_fr": test_center_test_session_data[
                "test_center_city_text"
            ]["fr"][0]["text"],
            "test_center_postal_code": test_center_test_session_data[
                "test_center_postal_code"
            ],
            "test_center_province_text_en": test_center_test_session_data[
                "test_center_province_text"
            ]["en"][0]["text"],
            "test_center_province_text_fr": test_center_test_session_data[
                "test_center_province_text"
            ]["fr"][0]["text"],
            "country_eabrv": test_center_test_session_data["country_eabrv"],
            "country_fabrv": test_center_test_session_data["country_fabrv"],
            "country_edesc": test_center_test_session_data["country_edesc"],
            "country_fdesc": test_center_test_session_data["country_fdesc"],
            "test_center_other_details_text_en": test_center_test_session_data[
                "test_center_other_details_text"
            ]["en"][0]["text"],
            "test_center_other_details_text_fr": test_center_test_session_data[
                "test_center_other_details_text"
            ]["fr"][0]["text"],
        }

        # OLA Test
        if is_ola:
            # in-person (room_name is defined)
            if test_center_test_session_data["room_name"] is not None:
                ola_invite_candidate_to_non_standard_test_session_in_person(invitation_data)
            # virtual (room_name is NULL)
            else:
                ola_invite_candidate_to_non_standard_test_session_virtual(invitation_data)

        # Other Test
        else:
            # sending test invitation email to candidate
            invite_candidate_to_non_standard_test_session(invitation_data)

    return Response(status=status.HTTP_200_OK)


# getting test center test sessions
def get_test_center_test_sessions(request):
    success, parameters = get_needed_parameters(
        ["page", "page_size", "test_center_id", "source"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, test_center_id, source = itemgetter(
        "page", "page_size", "test_center_id", "source"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting current datetime (UTC)
    now = timezone.now()

    # setting is_standard based on provided source
    is_standard = 1
    if source == TEST_SESSION_DATA_SOURCE.NON_STANDARD:
        is_standard = 0

    # getting test center test sessions
    test_sessions = TestCenterTestSessionsVW.objects.filter(
        test_center_id=test_center_id,
        end_time__gte=now,
        is_standard=is_standard,
    )

    # serializing and returning data
    serialized_data = TestCenterTestSessionsViewSerializer(
        test_sessions, many=True
    ).data

    # getting sorted serialized data by start time (ascending)
    ordered_test_sessions = sorted(
        serialized_data,
        key=lambda k: k["start_time"],
        reverse=False,
    )

    # if non standard
    if is_standard == 0:
        # initializing new_ordered_test_sessions
        new_ordered_test_sessions = []
        # looping in test_sessions
        for test_session in ordered_test_sessions:
            # user_accommodation_file_id of current iteration does not exist in new_ordered_test_sessions
            if not any(
                x["user_accommodation_file_id"]
                == test_session["user_accommodation_file_id"]
                for x in new_ordered_test_sessions
            ):
                # populating new_ordered_test_sessions
                new_ordered_test_sessions.append(test_session)

        # updating ordered_test_sessions with new_ordered_test_sessions
        ordered_test_sessions = new_ordered_test_sessions

    # only getting data for current selected page
    new_test_sessions = ordered_test_sessions[
        (current_page - 1) * page_size : current_page * page_size
    ]
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_test_sessions,
        len(ordered_test_sessions),
        current_page,
        page_size,
    )


# getting found test center test sessions
def get_found_test_center_test_sessions(request):
    success, parameters = get_needed_parameters(
        [
            "keyword",
            "current_language",
            "page",
            "page_size",
            "test_center_id",
            "source",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    keyword, current_language, page, page_size, test_center_id, source = itemgetter(
        "keyword", "current_language", "page", "page_size", "test_center_id", "source"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting current datetime (UTC)
    now = timezone.now()

    # setting is_standard based on provided source
    is_standard = 1
    if source == TEST_SESSION_DATA_SOURCE.NON_STANDARD:
        is_standard = 0

    # if blank search
    if keyword == " ":
        # getting all respective test sessions
        found_test_sessions = TestCenterTestSessionsVW.objects.filter(
            test_center_id=test_center_id, end_time__gte=now, is_standard=is_standard
        )
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # search while interface is in English
        if current_language == "en":
            # getting matching test center test sessions
            found_test_sessions = TestCenterTestSessionsVW.objects.filter(
                (
                    Q(test_center_id=test_center_id)
                    & Q(end_time__gte=now)
                    & Q(is_standard=is_standard)
                )
                & (
                    Q(date__icontains=keyword)
                    | Q(room_name__icontains=keyword)
                    | Q(test_skill_type_en_name__icontains=keyword)
                    | Q(test_skill_sub_type_en_name__icontains=keyword)
                    | Q(spaces_available__icontains=keyword)
                )
            )
        # search while interface is in French
        else:
            # getting matching test center test sessions
            found_test_sessions = TestCenterTestSessionsVW.objects.filter(
                (
                    Q(test_center_id=test_center_id)
                    & Q(end_time__gte=now)
                    & Q(is_standard=is_standard)
                )
                & (
                    Q(date__icontains=keyword)
                    | Q(room_name__icontains=keyword)
                    | Q(test_skill_type_fr_name__icontains=keyword)
                    | Q(test_skill_sub_type_fr_name__icontains=keyword)
                    | Q(spaces_available__icontains=keyword)
                )
            )

        # no results found
        if not found_test_sessions:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # serializing and returning data
    serialized_data = TestCenterTestSessionsViewSerializer(
        found_test_sessions, many=True
    ).data

    # getting sorted serialized data by start time (ascending)
    ordered_found_test_sessions = sorted(
        serialized_data,
        key=lambda k: k["start_time"],
        reverse=False,
    )

    # if non standard
    if is_standard == 0:
        # initializing new_ordered_found_test_sessions
        new_ordered_found_test_sessions = []
        # looping in test_sessions
        for test_session in ordered_found_test_sessions:
            # user_accommodation_file_id of current iteration does not exist in new_ordered_found_test_sessions
            if not any(
                x["user_accommodation_file_id"]
                == test_session["user_accommodation_file_id"]
                for x in new_ordered_found_test_sessions
            ):
                # populating new_ordered_found_test_sessions
                new_ordered_found_test_sessions.append(test_session)

        # updating ordered_found_test_sessions with new_ordered_found_test_sessions
        ordered_found_test_sessions = new_ordered_found_test_sessions

    # only getting data for current selected page
    new_found_test_sessions = ordered_found_test_sessions[
        (current_page - 1) * page_size : current_page * page_size
    ]
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_found_test_sessions,
        len(ordered_found_test_sessions),
        current_page,
        page_size,
    )


# getting test session related attendees
def get_test_session_attendees(request):
    success, parameters = get_needed_parameters(["test_session_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_session_id = itemgetter("test_session_id")(parameters)

    # getting related attendees based on provided test session ID
    related_attendees = TestSessionAttendeesVW.objects.filter(
        test_session_id=test_session_id
    )

    # serializing data
    serialized_data = TestSessionAttendeesViewSerializer(
        related_attendees, many=True
    ).data

    return Response(serialized_data)


# getting test session related attendees
def get_data_of_test_center_test_session_to_view_or_edit(request):
    user_info = get_user_info_from_jwt_token(request)
    success, parameters = get_needed_parameters(["test_session_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_session_id = itemgetter("test_session_id")(parameters)

    # "-CRC" is part of the ID ==> accommodation request with no linked test session yet
    if "-CRC" in test_session_id:
        # returning empty array
        return Response([])

    # getting related test session based on provided test_session_id
    related_test_session = TestCenterTestSessions.objects.get(id=test_session_id)

    # user that is calling that API does not have the TCM permission
    if not user_has_permission(request, Permission.TEST_CENTER_MANAGER):
        # making sure that the user is linked to the provided test_session_id (not allowing user to look other test sessions than his)
        # if no consumed reservation code is found with matching the assessment_process_assigned_test_specs_id or test sessions ID and the user_id
        if not ConsumedReservationCodes.objects.filter(
            Q(
                assessment_process_assigned_test_specs_id=related_test_session.assessment_process_assigned_test_specs_id,
                candidate_id=user_info["user_id"],
            )
            | Q(
                test_session_id=related_test_session.id,
                candidate_id=user_info["user_id"],
            ),
        ):
            return Response(
                {"error": "you do not have the permission to make that call"},
                status=status.HTTP_401_UNAUTHORIZED,
            )

    # user accommodation file ID is defined (accommodation request)
    if related_test_session.user_accommodation_file_id is not None:
        # getting all related test sessions
        related_test_sessions = TestCenterTestSessionsVW.objects.filter(
            user_accommodation_file_id=related_test_session.user_accommodation_file_id
        )
    # user accommodation file ID is NULL (not an accommodated test)
    else:
        # formatting test_session_id
        formatted_test_session_id = str(related_test_session.id)
        # if accommodation request ("-CRC" is added to the ID in the TestCenterTestSessionsVW, so we need to add it in the filter to get a match)
        # if related_test_session.user_accommodation_file_id is not None:
        # getting related test session
        related_test_sessions = TestCenterTestSessionsVW.objects.filter(
            id=formatted_test_session_id
        )

    # serializing the data
    serialized_data = TestCenterTestSessionsViewSerializer(
        related_test_sessions, many=True
    ).data

    # getting sorted serialized data by test_session_order (ascending)
    ordered_serialized_data = sorted(
        serialized_data,
        key=lambda k: k["test_session_order"],
        reverse=False,
    )

    return Response(ordered_serialized_data)


def edit_test_center_test_session(request):
    test_session_data = json.loads(request.body)

    try:
        # getting related test session data ID
        # adding that test center ID in the filter just to make sure we're not deleting wrong data
        related_test_session = TestCenterTestSessions.objects.get(
            id=test_session_data["id"],
            test_center_id=test_session_data["test_center_id"],
        )

        # initializing desired date format
        date_format = "%Y-%m-%d %H:%M:%S.%f %z"

        utc_start_time = datetime.datetime.strptime(
            test_session_data["utc_start_time"], date_format
        )

        utc_end_time = datetime.datetime.strptime(
            test_session_data["utc_end_time"], date_format
        )

        # validating test session overlapping time
        is_valid_test_session_time = validate_test_session_overlapping_time(
            test_session_data["test_center_room_id"],
            test_session_data["date"],
            utc_start_time,
            utc_end_time,
            test_session_data["id"],
        )

        # not valid
        if not is_valid_test_session_time:
            return Response(
                {
                    "info": "there is an overlapping time conflict with your current room/time settings"
                },
                status=status.HTTP_409_CONFLICT,
            )

        # getting related test session data and saving based on provided data
        related_test_session_data = TestCenterTestSessionData.objects.get(
            id=related_test_session.test_session_data_id
        )

        # standard sessions
        if test_session_data["source"] == TEST_SESSION_DATA_SOURCE.STANDARD:
            related_test_session_data.open_to_ogd = test_session_data["open_to_ogd"]
            related_test_session_data.date = test_session_data["date"]
            related_test_session_data.start_time = utc_start_time
            related_test_session_data.end_time = utc_end_time
            related_test_session_data.spaces_available = test_session_data[
                "spaces_available"
            ]
        # non-standard sessions
        elif test_session_data["source"] == TEST_SESSION_DATA_SOURCE.NON_STANDARD:
            related_test_session_data.test_center_room_id = test_session_data[
                "test_center_room_id"
            ]
            related_test_session_data.date = test_session_data["date"]
            related_test_session_data.start_time = utc_start_time
            related_test_session_data.end_time = utc_end_time

        related_test_session_data.save()

        # getting respective room email
        respective_room_email = TestCenterRoomsVW.objects.get(
            id=test_session_data["test_center_room_id"]
        ).email

        # sending meeting invite through email to respective recipient
        create_test_session_email_celery_task.delay(
            [
                {
                    "action": TestSessionEmailAction.UPDATE,
                    "test_session_id": test_session_data["id"],
                    "email": respective_room_email,
                    "date": test_session_data["date"],
                    "start_time": utc_start_time,
                    "end_time": utc_end_time,
                }
            ]
        )

        # non-standard sessions
        if test_session_data["source"] == TEST_SESSION_DATA_SOURCE.NON_STANDARD:
            # getting/setting respective invitation data
            user_accommodation_file_data = UserAccommodationFile.objects.get(
                id=related_test_session.user_accommodation_file_id
            )
            user_data = User.objects.get(id=user_accommodation_file_data.user_id)

            test_center_test_session_data = TestCenterTestSessionsVW.objects.get(
                id=related_test_session.id
            )
            test_center_test_session_data = TestCenterTestSessionsViewSerializer(
                test_center_test_session_data, many=False
            ).data

            user_accommodation_file_test_to_administer_data = UserAccommodationFileTestToAdminister.objects.get(
                user_accommodation_file_id=related_test_session.user_accommodation_file_id
            )
            related_test_data = TestDefinition.objects.get(
                id=user_accommodation_file_test_to_administer_data.test_id
            )
            serialized_related_test_data = TestDefinitionSerializer(
                related_test_data, many=False
            ).data

            invitation_data = {
                "email": user_data.email,
                "date": test_center_test_session_data["date"],
                "start_time": test_center_test_session_data["simplified_start_time"],
                "end_time": test_center_test_session_data["simplified_end_time"],
                "test_name_en": serialized_related_test_data["en_name"],
                "test_name_fr": serialized_related_test_data["fr_name"],
                "test_version": serialized_related_test_data["version"],
                "room_name": test_center_test_session_data["room_name"],
                "test_center_address_text_en": test_center_test_session_data[
                    "test_center_address_text"
                ]["en"][0]["text"],
                "test_center_address_text_fr": test_center_test_session_data[
                    "test_center_address_text"
                ]["fr"][0]["text"],
                "test_center_city_text_en": test_center_test_session_data[
                    "test_center_city_text"
                ]["en"][0]["text"],
                "test_center_city_text_fr": test_center_test_session_data[
                    "test_center_city_text"
                ]["fr"][0]["text"],
                "test_center_postal_code": test_center_test_session_data[
                    "test_center_postal_code"
                ],
                "test_center_province_text_en": test_center_test_session_data[
                    "test_center_province_text"
                ]["en"][0]["text"],
                "test_center_province_text_fr": test_center_test_session_data[
                    "test_center_province_text"
                ]["fr"][0]["text"],
                "country_eabrv": test_center_test_session_data["country_eabrv"],
                "country_fabrv": test_center_test_session_data["country_fabrv"],
                "country_edesc": test_center_test_session_data["country_edesc"],
                "country_fdesc": test_center_test_session_data["country_fdesc"],
                "test_center_other_details_text_en": test_center_test_session_data[
                    "test_center_other_details_text"
                ]["en"][0]["text"],
                "test_center_other_details_text_fr": test_center_test_session_data[
                    "test_center_other_details_text"
                ]["fr"][0]["text"],
            }

            # OLA Test
            if (
                test_center_test_session_data["test_skill_sub_type_codename"]
                == TestSkillSLEDescCodename.ORAL_EN
                or test_center_test_session_data["test_skill_sub_type_codename"]
                == TestSkillSLEDescCodename.ORAL_FR
            ):
                # in-person (room_name is defined)
                if test_center_test_session_data["room_name"] is not None:
                    ola_update_non_standard_test_session_invitation_in_person(invitation_data)
                # virtual (room_name is NULL)
                else:
                    ola_update_non_standard_test_session_invitation_virtual(invitation_data)
            # Other Test
            else:
                # sending test invitation email to candidate
                update_non_standard_test_session_invitation(invitation_data)

        return Response(status=status.HTTP_200_OK)

    except TestCenterTestSessions.DoesNotExist:
        return Response(
            {"error": "the specified test center test session does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )
    except TestCenterTestSessionData.DoesNotExist:
        return Response(
            {"error": "the specified test center test session data does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )


def handle_delete_room_utils(test_session_id, delete_related_test_session=False):
    # initializing desired date format
    date_format = "%Y-%m-%dT%H:%M:%S%z"
    # getting respective test session data
    respective_test_session = TestCenterTestSessions.objects.get(id=test_session_id)
    serialized_test_session_data = TestCenterTestSessionsSerializer(
        respective_test_session, many=False
    ).data
    # getting respective room email
    respective_room_email = TestCenterRoomsVW.objects.filter(
        id=serialized_test_session_data["test_session_data"]["test_center_room_id"]
    )

    # if room exists
    if respective_room_email:
        # sending meeting invite through email to respective room (in order to delete it)
        create_test_session_email_celery_task.delay(
            [
                {
                    "action": TestSessionEmailAction.DELETE,
                    "test_session_id": test_session_id,
                    "email": respective_room_email.last().email,
                    "date": serialized_test_session_data["test_session_data"]["date"],
                    "start_time": datetime.datetime.strptime(
                        serialized_test_session_data["test_session_data"]["start_time"],
                        date_format,
                    ),
                    "end_time": datetime.datetime.strptime(
                        serialized_test_session_data["test_session_data"]["end_time"],
                        date_format,
                    ),
                }
            ]
        )

    # delete_related_test_session is set to True
    if delete_related_test_session:
        # getting/setting respective invitation data
        user_accommodation_file_data = UserAccommodationFile.objects.get(
            id=serialized_test_session_data["user_accommodation_file_id"]
        )
        user_data = User.objects.get(id=user_accommodation_file_data.user_id)

        user_accommodation_file_test_to_administer_data = (
            UserAccommodationFileTestToAdminister.objects.get(
                user_accommodation_file_id=serialized_test_session_data[
                    "user_accommodation_file_id"
                ]
            )
        )

        # initializing test related variables
        test_name_en = ""
        test_name_fr = ""
        test_version = ""

        # test_id is defined
        if user_accommodation_file_test_to_administer_data.test_id is not None:
            related_test_data = TestDefinition.objects.get(
                id=user_accommodation_file_test_to_administer_data.test_id
            )
            serialized_related_test_data = TestDefinitionSerializer(
                related_test_data, many=False
            ).data

            test_name_en = serialized_related_test_data["en_name"]
            test_name_fr = serialized_related_test_data["fr_name"]
            test_version = serialized_related_test_data["version"]
        # test_id is not defined (need to get name from specified_test_description field)
        else:
            test_name_en = (
                user_accommodation_file_test_to_administer_data.specified_test_description
            )
            test_name_fr = (
                user_accommodation_file_test_to_administer_data.specified_test_description
            )

        # setting room_name
        room_name = ""
        if serialized_test_session_data["room_name"] is not None:
            room_name = serialized_test_session_data["room_name"]

        invitation_data = {
            "email": user_data.email,
            "date": serialized_test_session_data["test_session_data"]["date"],
            "start_time": serialized_test_session_data["simplified_start_time"],
            "end_time": serialized_test_session_data["simplified_end_time"],
            "test_name_en": test_name_en,
            "test_name_fr": test_name_fr,
            "test_version": test_version,
            "room_name": room_name,
        }

        # sending test invitation email to candidate
        delete_non_standard_test_session(invitation_data)

        # delete respective test session / test session data
        test_session_to_delete = TestCenterTestSessions.objects.get(id=test_session_id)
        test_session_data_id_to_delete = test_session_to_delete.test_session_data_id
        test_session_to_delete.delete()
        TestCenterTestSessionData.objects.get(
            id=test_session_data_id_to_delete
        ).delete()


def edit_non_standard_test_center_test_session(request):
    test_sessions = json.loads(request.body)

    try:
        with transaction.atomic():
            # ==================== DELETING TEST SESSIONS IF NEEDED ====================
            # first, we need to check if there are some test sessions to delete
            # getting all related test session IDs (related by the user_accommodation_file_id)
            all_related_test_session_ids = (
                TestCenterTestSessions.objects.filter(
                    user_accommodation_file_id=test_sessions[0][
                        "user_accommodation_file_id"
                    ]
                )
                .order_by("test_session_order")
                .values_list("id", flat=True)
            )
            # looping in those related test session IDs
            for test_session_id in all_related_test_session_ids:
                # test session ID of current iteration is not in the provided test_sessions array
                if not any(int(x["id"]) == test_session_id for x in test_sessions):
                    # deleting respective room (including the deletion of the respective test session)
                    handle_delete_room_utils(test_session_id, True)
            # ==================== DELETING TEST SESSIONS IF NEEDED (END) ====================

            # ==================== CREATING/UPDATING TEST SESSIONS ====================
            # looping in test_sessions
            for index, test_session_data in enumerate(test_sessions):
                # initializing desired date format
                date_format = "%Y-%m-%d %H:%M:%S.%f %z"

                # converting string utc_start_time to datetime utc_start_time
                utc_start_time = datetime.datetime.strptime(
                    test_session_data["utc_start_time"], date_format
                )

                # converting string utc_end_time to datetime utc_end_time
                utc_end_time = datetime.datetime.strptime(
                    test_session_data["utc_end_time"], date_format
                )

                # getting related assessment process assigned test specs data
                related_assessment_process_assigned_test_specs_data = (
                    AssessmentProcessAssignedTestSpecsVW.objects.get(
                        id=test_session_data[
                            "assessment_process_assigned_test_specs_id"
                        ]
                    )
                )

                # initializing test_assessor_user_id
                test_assessor_user_id = None
                # initializing candidate_phone_number
                candidate_phone_number = None

                # OLA Test
                if (
                    related_assessment_process_assigned_test_specs_data.test_skill_sub_type_codename
                    == TestSkillSLEDescCodename.ORAL_EN
                    or related_assessment_process_assigned_test_specs_data.test_skill_sub_type_codename
                    == TestSkillSLEDescCodename.ORAL_FR
                ):
                    # setting test_assessor_user_id
                    test_assessor_user_id = test_session_data["test_assessor_user_id"]
                    # setting candidate_phone_number
                    candidate_phone_number = UserAccommodationFile.objects.get(
                        id=related_assessment_process_assigned_test_specs_data.user_accommodation_file_id
                    ).candidate_phone_number

                # validating test session overlapping time
                is_valid_test_session_time = validate_test_session_overlapping_time(
                    test_session_data["test_center_room_id"],
                    test_session_data["date"],
                    utc_start_time,
                    utc_end_time,
                    test_session_data["id"],
                    test_assessor_user_id,
                )

                # not valid
                if not is_valid_test_session_time:
                    return Response(
                        {
                            "info": "there is an overlapping time conflict with your current room/time settings"
                        },
                        status=status.HTTP_409_CONFLICT,
                    )

                # test session ID is provided
                if test_session_data["id"] is not None:
                    # getting related test session
                    related_test_session = TestCenterTestSessions.objects.get(
                        id=test_session_data["id"],
                    )
                    # updating test_session_order
                    related_test_session.test_session_order = index + 1
                    related_test_session.save()
                    # getting related test session data
                    related_test_session_data = TestCenterTestSessionData.objects.get(
                        id=related_test_session.test_session_data_id
                    )

                    # if there is any changes (room, date, start_time or end_time, test_assessor)
                    if (
                        test_session_data["test_center_room_id"]
                        != related_test_session_data.test_center_room_id
                        or str(test_session_data["date"])
                        != str(related_test_session_data.date)
                        or str(utc_start_time)[:19]
                        != str(related_test_session_data.start_time)[:19]
                        or str(utc_end_time)[:19]
                        != str(related_test_session_data.end_time)[:19]
                        or test_session_data["test_assessor_user_id"]
                        != related_test_session_data.test_assessor_user_id
                    ):
                        # initializing room_name
                        room_name = ""

                        # room has been updated
                        if (
                            test_session_data["test_center_room_id"]
                            != related_test_session_data.test_center_room_id
                        ):
                            # room has been deleted
                            if test_session_data["test_center_room_id"] is None:
                                # deleting respective room
                                handle_delete_room_utils(test_session_data["id"])
                            # room has been added
                            elif (
                                related_test_session_data.test_center_room_id is None
                                and test_session_data["test_center_room_id"] is not None
                            ):
                                # getting data from TestCenterTestSessionsVW
                                test_center_test_session_data = (
                                    TestCenterTestSessionsVW.objects.get(
                                        id=test_session_data["id"]
                                    )
                                )

                                test_center_test_session_data = (
                                    TestCenterTestSessionsViewSerializer(
                                        test_center_test_session_data, many=False
                                    ).data
                                )

                                # add respective room
                                handle_add_room_utils(
                                    test_center_test_session_data,
                                    test_session_data["test_center_room_id"],
                                    test_session_data["user_accommodation_file_id"],
                                    utc_start_time,
                                    utc_end_time,
                                )
                            # room has been updated
                            elif (
                                related_test_session_data.test_center_room_id
                                is not None
                                and test_session_data["test_center_room_id"] is not None
                            ):
                                # deleting respective room
                                handle_delete_room_utils(test_session_data["id"])

                                # getting data from TestCenterTestSessionsVW
                                test_center_test_session_data = (
                                    TestCenterTestSessionsVW.objects.get(
                                        id=test_session_data["id"]
                                    )
                                )

                                test_center_test_session_data = (
                                    TestCenterTestSessionsViewSerializer(
                                        test_center_test_session_data, many=False
                                    ).data
                                )

                                # add respective room
                                handle_add_room_utils(
                                    test_center_test_session_data,
                                    test_session_data["test_center_room_id"],
                                    test_session_data["user_accommodation_file_id"],
                                    utc_start_time,
                                    utc_end_time,
                                )
                            # should never happen
                            else:
                                return Response(
                                    {
                                        "error": "Unsupported room logic (should never happen)"
                                    },
                                    status=status.HTTP_405_METHOD_NOT_ALLOWED,
                                )

                        # updating related test session data
                        related_test_session_data.test_center_room_id = (
                            test_session_data["test_center_room_id"]
                        )
                        related_test_session_data.date = test_session_data["date"]
                        related_test_session_data.start_time = utc_start_time
                        related_test_session_data.end_time = utc_end_time
                        related_test_session_data.test_assessor_user_id = (
                            test_session_data["test_assessor_user_id"]
                        )
                        related_test_session_data.save()

                        # defined test_assessor_user_id (OLA Test)
                        if test_session_data["test_assessor_user_id"] is not None:
                            # making sure that the link with the scorer/test assessor is still accurate (update it if needed)
                            # getting related scorer OLA assigned test session data
                            related_scorer_ola_assigned_test_session_data = (
                                ScorerOlaAssignedTestSession.objects.get(
                                    test_session_id=test_session_data["id"],
                                )
                            )
                            # updating test_assessor_id
                            related_scorer_ola_assigned_test_session_data.test_assessor_id = test_session_data[
                                "test_assessor_user_id"
                            ]
                            related_scorer_ola_assigned_test_session_data.save()

                        # room has NOT been upated
                        if (
                            test_session_data["test_center_room_id"]
                            == related_test_session_data.test_center_room_id
                        ):
                            # getting respective room email
                            respective_room_email = TestCenterRoomsVW.objects.filter(
                                id=test_session_data["test_center_room_id"]
                            )

                            # if room exists
                            if respective_room_email:
                                # sending meeting invite through email to respective recipient
                                create_test_session_email_celery_task.delay(
                                    [
                                        {
                                            "action": TestSessionEmailAction.UPDATE,
                                            "test_session_id": test_session_data["id"],
                                            "email": respective_room_email.last().email,
                                            "date": test_session_data["date"],
                                            "start_time": utc_start_time,
                                            "end_time": utc_end_time,
                                        }
                                    ]
                                )

                        # getting/setting respective invitation data
                        user_accommodation_file_data = (
                            UserAccommodationFile.objects.get(
                                id=related_test_session.user_accommodation_file_id
                            )
                        )

                        user_data = User.objects.get(
                            id=user_accommodation_file_data.user_id
                        )

                        test_center_test_session_data = (
                            TestCenterTestSessionsVW.objects.get(
                                id=related_test_session.id
                            )
                        )

                        test_center_test_session_data = (
                            TestCenterTestSessionsViewSerializer(
                                test_center_test_session_data, many=False
                            ).data
                        )

                        # test_center_room_id is defined
                        if test_center_test_session_data["test_center_room_id"]:
                            # setting new room_name
                            room_name = test_center_test_session_data["room_name"]

                        user_accommodation_file_test_to_administer_data = UserAccommodationFileTestToAdminister.objects.get(
                            user_accommodation_file_id=related_test_session.user_accommodation_file_id
                        )

                        # initializing test related variables
                        test_name_en = ""
                        test_name_fr = ""
                        test_version = ""

                        # test_id is defined
                        if (
                            user_accommodation_file_test_to_administer_data.test_id
                            is not None
                        ):
                            related_test_data = TestDefinition.objects.get(
                                id=user_accommodation_file_test_to_administer_data.test_id
                            )

                            serialized_related_test_data = TestDefinitionSerializer(
                                related_test_data, many=False
                            ).data

                            test_name_en = serialized_related_test_data["en_name"]
                            test_name_fr = serialized_related_test_data["fr_name"]
                            test_version = serialized_related_test_data["version"]
                        # test_id is not defined (need to get name from specified_test_description field)
                        else:
                            test_name_en = (
                                user_accommodation_file_test_to_administer_data.specified_test_description
                            )
                            test_name_fr = (
                                user_accommodation_file_test_to_administer_data.specified_test_description
                            )

                        invitation_data = {
                            "email": user_data.email,
                            "date": test_center_test_session_data["date"],
                            "start_time": test_center_test_session_data[
                                "simplified_start_time"
                            ],
                            "end_time": test_center_test_session_data[
                                "simplified_end_time"
                            ],
                            "test_name_en": test_name_en,
                            "test_name_fr": test_name_fr,
                            "test_version": test_version,
                            "room_name": room_name,
                            "test_center_address_text_en": test_center_test_session_data[
                                "test_center_address_text"
                            ][
                                "en"
                            ][
                                0
                            ][
                                "text"
                            ],
                            "test_center_address_text_fr": test_center_test_session_data[
                                "test_center_address_text"
                            ][
                                "fr"
                            ][
                                0
                            ][
                                "text"
                            ],
                            "test_center_city_text_en": test_center_test_session_data[
                                "test_center_city_text"
                            ]["en"][0]["text"],
                            "test_center_city_text_fr": test_center_test_session_data[
                                "test_center_city_text"
                            ]["fr"][0]["text"],
                            "test_center_postal_code": test_center_test_session_data[
                                "test_center_postal_code"
                            ],
                            "test_center_province_text_en": test_center_test_session_data[
                                "test_center_province_text"
                            ][
                                "en"
                            ][
                                0
                            ][
                                "text"
                            ],
                            "test_center_province_text_fr": test_center_test_session_data[
                                "test_center_province_text"
                            ][
                                "fr"
                            ][
                                0
                            ][
                                "text"
                            ],
                            "country_eabrv": test_center_test_session_data[
                                "country_eabrv"
                            ],
                            "country_fabrv": test_center_test_session_data[
                                "country_fabrv"
                            ],
                            "country_edesc": test_center_test_session_data[
                                "country_edesc"
                            ],
                            "country_fdesc": test_center_test_session_data[
                                "country_fdesc"
                            ],
                            "test_center_other_details_text_en": test_center_test_session_data[
                                "test_center_other_details_text"
                            ][
                                "en"
                            ][
                                0
                            ][
                                "text"
                            ],
                            "test_center_other_details_text_fr": test_center_test_session_data[
                                "test_center_other_details_text"
                            ][
                                "fr"
                            ][
                                0
                            ][
                                "text"
                            ],
                        }

                        # OLA Test
                        if (
                            test_center_test_session_data[
                                "test_skill_sub_type_codename"
                            ]
                            == TestSkillSLEDescCodename.ORAL_EN
                            or test_center_test_session_data[
                                "test_skill_sub_type_codename"
                            ]
                            == TestSkillSLEDescCodename.ORAL_FR
                        ):
                            # in-person (room_name is defined)
                            if test_center_test_session_data["room_name"] is not None:
                                ola_update_non_standard_test_session_invitation_in_person(invitation_data)
                            # virtual (room_name is NULL)
                            else:
                                ola_update_non_standard_test_session_invitation_virtual(invitation_data)
                        # Other Test
                        else:
                            # sending test invitation email to candidate
                            update_non_standard_test_session_invitation(invitation_data)

                # test session does not exist yet (need to create a new one)
                else:
                    # creating new test session data
                    new_test_session_data = TestCenterTestSessionData.objects.create(
                        test_center_room_id=test_session_data["test_center_room_id"],
                        open_to_ogd=None,
                        date=test_session_data["date"],
                        start_time=utc_start_time,
                        end_time=utc_end_time,
                        spaces_available=None,
                        test_skill_type_id=test_session_data["test_skill_type_id"],
                        test_skill_sub_type_id=test_session_data[
                            "test_skill_sub_type_id"
                        ],
                        test_assessor_user_id=test_session_data[
                            "test_assessor_user_id"
                        ],
                        candidate_phone_number=candidate_phone_number,
                    )
                    # creating new test session
                    new_test_session = TestCenterTestSessions.objects.create(
                        test_session_data_id=new_test_session_data.id,
                        test_center_id=test_session_data["test_center_id"],
                        is_standard=0,
                        user_accommodation_file_id=test_session_data[
                            "user_accommodation_file_id"
                        ],
                        test_session_order=index + 1,
                        assessment_process_assigned_test_specs_id=test_session_data[
                            "assessment_process_assigned_test_specs_id"
                        ],
                    )

                    # OLA Test
                    if (
                        related_assessment_process_assigned_test_specs_data.test_skill_sub_type_codename
                        == TestSkillSLEDescCodename.ORAL_EN
                        or related_assessment_process_assigned_test_specs_data.test_skill_sub_type_codename
                        == TestSkillSLEDescCodename.ORAL_FR
                    ):
                        # linking test session(s) to scorer/test assessor
                        # creating new entry in
                        ScorerOlaAssignedTestSession.objects.create(
                            test_session_id=new_test_session.id,
                            test_assessor_id=test_session_data["test_assessor_user_id"],
                            candidate_id=related_assessment_process_assigned_test_specs_data.candidate_user_id,
                        )

                    # getting respective room email
                    respective_room_email = TestCenterRoomsVW.objects.filter(
                        id=test_session_data["test_center_room_id"]
                    )

                    # getting data from TestCenterTestSessionsVW
                    test_center_test_session_data = (
                        TestCenterTestSessionsVW.objects.get(id=new_test_session.id)
                    )

                    test_center_test_session_data = (
                        TestCenterTestSessionsViewSerializer(
                            test_center_test_session_data, many=False
                        ).data
                    )

                    # getting test related data
                    user_accommodation_file_test_to_administer_data = (
                        UserAccommodationFileTestToAdminister.objects.get(
                            user_accommodation_file_id=test_session_data[
                                "user_accommodation_file_id"
                            ]
                        )
                    )

                    # if there is a defined test definition
                    if (
                        user_accommodation_file_test_to_administer_data.test_id
                        is not None
                    ):
                        related_test_data = TestDefinition.objects.get(
                            id=user_accommodation_file_test_to_administer_data.test_id
                        )
                        serialized_related_test_data = TestDefinitionSerializer(
                            related_test_data, many=False
                        ).data
                    # no defined test definition
                    else:
                        serialized_related_test_data = {
                            "en_name": user_accommodation_file_test_to_administer_data.specified_test_description,
                            "fr_name": user_accommodation_file_test_to_administer_data.specified_test_description,
                        }

                    # setting room_name
                    room_name = ""
                    if test_center_test_session_data["room_name"] is not None:
                        room_name = test_center_test_session_data["room_name"]

                    # if room exists
                    if respective_room_email:
                        # sending meeting invite through email to respective recipient
                        create_test_session_email_celery_task.delay(
                            [
                                {
                                    "action": TestSessionEmailAction.ADD,
                                    "test_session_id": test_center_test_session_data[
                                        "id"
                                    ],
                                    "email": respective_room_email.last().email,
                                    "date": test_center_test_session_data["date"],
                                    "start_time": utc_start_time,
                                    "end_time": utc_end_time,
                                    "simplified_start_time": test_center_test_session_data[
                                        "simplified_start_time"
                                    ],
                                    "simplified_end_time": test_center_test_session_data[
                                        "simplified_end_time"
                                    ],
                                    "test_name_en": serialized_related_test_data[
                                        "en_name"
                                    ],
                                    "test_name_fr": serialized_related_test_data[
                                        "fr_name"
                                    ],
                                    "room_name": room_name,
                                }
                            ]
                        )

                    # getting/setting respective invitation data
                    user_data = User.objects.get(
                        id=test_center_test_session_data["user_id"]
                    )

                    invitation_data = {
                        "email": user_data.email,
                        "date": test_center_test_session_data["date"],
                        "start_time": test_center_test_session_data[
                            "simplified_start_time"
                        ],
                        "end_time": test_center_test_session_data[
                            "simplified_end_time"
                        ],
                        "test_name_en": serialized_related_test_data["en_name"],
                        "test_name_fr": serialized_related_test_data["fr_name"],
                        "room_name": room_name,
                    }

                    # sending test invitation email to candidate
                    invite_candidate_to_non_standard_test_session(invitation_data)
            # ==================== CREATING/UPDATING TEST SESSIONS (END) ====================

        return Response(status=status.HTTP_200_OK)

    except TestCenterTestSessions.DoesNotExist:
        return Response(
            {"error": "the specified test center test session does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )
    except TestCenterTestSessionData.DoesNotExist:
        return Response(
            {"error": "the specified test center test session data does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )
    except:
        return Response(
            {
                "error": "Something happened during the edit non standard test center test session process"
            },
            status=status.HTTP_404_NOT_FOUND,
        )


def delete_test_center_test_session(request):
    test_session_data = json.loads(request.body)

    try:
        # getting reservation code status IDs
        booked_status_id = ConsumedReservationCodeStatus.objects.get(
            codename=StaticConsumedReservationCodeStatus.BOOKED
        ).id
        reserved_status_id = ConsumedReservationCodeStatus.objects.get(
            codename=StaticConsumedReservationCodeStatus.RESERVED
        ).id
        requested_acco_status_id = ConsumedReservationCodeStatus.objects.get(
            codename=StaticConsumedReservationCodeStatus.REQUESTED_ACCOMMODATION
        ).id

        # standard sessions
        if test_session_data["source"] == TEST_SESSION_DATA_SOURCE.STANDARD:
            # getting respective room email
            respective_test_session_data = TestCenterTestSessionsVW.objects.get(
                id=test_session_data["id"]
            )
            respective_room_email = TestCenterRoomsVW.objects.filter(
                id=respective_test_session_data.test_center_room_id
            )
            # if room exists
            if respective_room_email:
                # sending meeting invite through email to respective room
                create_test_session_email_celery_task.delay(
                    [
                        {
                            "action": TestSessionEmailAction.DELETE,
                            "test_session_id": test_session_data["id"],
                            "email": respective_room_email.last().email,
                            "date": respective_test_session_data.date,
                            "start_time": respective_test_session_data.start_time,
                            "end_time": respective_test_session_data.end_time,
                        }
                    ]
                )

            # sending emails to candidates that have booked this test session
            # initializing needed variables
            email_data_array = []
            related_assigned_test_specs_ids = []
            # getting related consumed reservation codes
            related_consumed_reservation_codes = (
                ConsumedReservationCodes.objects.filter(
                    test_session_id=test_session_data["id"], status_id=booked_status_id
                )
            )
            # preserve invitee data for the HR email at the end; invitee is what the HR rep sees on their page, not the CAT user profile
            invitee_info = {}
            # looping in related_consumed_reservation_codes
            for consumed_reservation_code in related_consumed_reservation_codes:
                ap_test_spec = (
                    consumed_reservation_code.assessment_process_assigned_test_specs
                )
                assessment_process_id = ap_test_spec.assessment_process.id
                # populating related_assigned_test_specs_ids
                related_assigned_test_specs_ids.append(ap_test_spec.id)
                # getting respective CAT User data
                cat_user_data = UserSerializer(
                    User.objects.get(id=consumed_reservation_code.candidate_id),
                    many=False,
                ).data
                # populate the invitee info for later usage; if the key doesn't exist, add an empty list and then append it
                if assessment_process_id not in invitee_info:
                    invitee_info[assessment_process_id] = []
                invitee = {
                    "first_name": consumed_reservation_code.assessment_process_assigned_test_specs.first_name,
                    "last_name": consumed_reservation_code.assessment_process_assigned_test_specs.last_name,
                    "email": consumed_reservation_code.assessment_process_assigned_test_specs.email,
                }
                invitee_info[assessment_process_id].append(invitee)
                # getting respective assessment process assigned test specs data
                assessment_process_assigned_test_specs_data = AssessmentProcessAssignedTestSpecsVW.objects.get(
                    id=consumed_reservation_code.assessment_process_assigned_test_specs_id
                )
                # getting respective test session data
                respective_test_session_data = TestCenterTestSessionsVW.objects.get(
                    id=consumed_reservation_code.test_session_id
                )
                # populating email_data_array
                email_data_array.append(
                    {
                        "cat_user_data": cat_user_data,
                        "assessment_process_assigned_test_specs_data": AssessmentProcessAssignedTestSpecsViewSerializer(
                            assessment_process_assigned_test_specs_data, many=False
                        ).data,
                        "test_session_data": TestCenterTestSessionsViewSerializer(
                            respective_test_session_data, many=False
                        ).data,
                        "reservation_code": consumed_reservation_code.reservation_code,
                    }
                )
                # updating consumed reservation code status to RESERVED
                consumed_reservation_code.status_id = reserved_status_id
                # updating consumed reservation code test_session_id to NULL
                consumed_reservation_code.test_session_id = None
                consumed_reservation_code.save()

            # sending respective emails
            # calling celery task to handle the send reservation code emails action
            # when using this celery task to send the emails, the data creation part of the function is called and return the response 200 quiclky
            # so the emails are sent in the background while the user continues to navigate in the UI
            send_delete_specific_test_session_email_celery_task.delay(email_data_array)

            # sending emails to linked HR Coordinators
            # getting related assessment process ids based on related_assigned_test_specs_ids
            related_assessment_process_ids = (
                AssessmentProcessAssignedTestSpecsVW.objects.filter(
                    id__in=related_assigned_test_specs_ids
                ).values_list("assessment_process_id", flat=True)
            )

            # getting related HR Coordinator user ids
            related_hr_coordinator_user_ids = AssessmentProcessVW.objects.filter(
                id__in=related_assessment_process_ids
            ).values_list("user_id", flat=True)

            # removing duplicates from related_hr_coordinator_user_ids
            unique_related_hr_coordinator_user_ids = []
            for user_id in related_hr_coordinator_user_ids:
                if user_id not in unique_related_hr_coordinator_user_ids:
                    unique_related_hr_coordinator_user_ids.append(user_id)

            # building the hr_related_email_array
            hr_related_email_array = []
            # looping in unique_related_hr_coordinator_user_ids
            for user_id in unique_related_hr_coordinator_user_ids:
                # initializing considered_assessment_process_id
                considered_assessment_process_id = []
                # getting all related assessment processes for the user of the current iteration
                related_assessment_processes = AssessmentProcessVW.objects.filter(
                    user_id=user_id, id__in=related_assessment_process_ids
                )
                # can get the invitee from the related_assessment_process_ids, need to make the link and the list; this is what the HR rep saw when inviting them
                # looping in related_assessment_processes
                for assessment_process_data in related_assessment_processes:
                    # making sure that the assessment process id of the current iteration has not been considered yet (only want to send 1 email per assessment process to respective HR Coordinator)
                    if (
                        assessment_process_data.id
                        not in considered_assessment_process_id
                    ):
                        invitees = invitee_info[assessment_process_data.id]
                        invitee_list = ""
                        # NOTE
                        # this is how the invitee "list" will be formatted in GC notify. Since it is a list of N length, it needs to be built and formatted here
                        for invitee in invitees:
                            invitee_list += "* {0} {1} - {2}\n".format(
                                invitee["first_name"],
                                invitee["last_name"],
                                invitee["email"],
                            )
                        # getting assessment process serialized data and populating hr_related_email_array
                        data_map = SimplifiedAssessmentProcessViewSerializer(
                            assessment_process_data, many=False
                        ).data
                        data_map["invitee_list"] = invitee_list
                        hr_related_email_array.append(data_map)
                        # adding assessment process id to considered_assessment_process_id
                        considered_assessment_process_id.append(
                            assessment_process_data.id
                        )

            # sending emails (using celery since multiple emails can be sent in this action)
            test_session_cancellation_inform_related_hr_coordinator.delay(
                hr_related_email_array
            )

            # getting and deleting related test session officers
            related_test_session_officers = (
                TestCenterTestSessionOfficers.objects.filter(
                    test_session_id=test_session_data["id"]
                )
            )
            for test_session_officer in related_test_session_officers:
                test_session_officer.delete()

            # getting and deleting related test access codes
            related_test_access_codes = TestAccessCode.objects.filter(
                test_session_id=test_session_data["id"]
            )
            for test_access_code in related_test_access_codes:
                test_access_code.delete()

            # getting and deleting related test session
            # adding that test center ID in the filter just to make sure we're not deleting wrong data
            related_test_session = TestCenterTestSessions.objects.get(
                id=test_session_data["id"],
                test_center_id=test_session_data["test_center_id"],
            )
            related_test_session.delete()

            # getting and deleting related test session data
            related_test_session_data = TestCenterTestSessionData.objects.get(
                id=related_test_session.test_session_data_id
            )
            related_test_session_data.delete()

        # non-standard sessions
        elif test_session_data["source"] == TEST_SESSION_DATA_SOURCE.NON_STANDARD:
            # getting all related test center test sessions
            all_related_test_center_test_sessions = (
                TestCenterTestSessionsVW.objects.filter(
                    user_accommodation_file_id=test_session_data[
                        "user_accommodation_file_id"
                    ]
                )
            )

            serialized_test_center_test_session_data = (
                TestCenterTestSessionsViewSerializer(
                    all_related_test_center_test_sessions, many=True
                ).data
            )

            # looping in serialized_test_center_test_session_data
            for test_session in serialized_test_center_test_session_data:
                # deleting respective room (including the deletion of the respective test session)
                handle_delete_room_utils(test_session["id"], True)

            # getting related consumed reservation code
            related_consumed_reservation_code = ConsumedReservationCodes.objects.get(
                assessment_process_assigned_test_specs_id=serialized_test_center_test_session_data[
                    0
                ][
                    "assessment_process_assigned_test_specs_id"
                ]
            )
            # updating consumed reservation code status to REQUESTED ACCO
            related_consumed_reservation_code.status_id = requested_acco_status_id
            # updating consumed reservation code test_session_id to NULL
            related_consumed_reservation_code.test_session_id = None
            related_consumed_reservation_code.save()

        # should never happen
        else:
            return Response(
                {"error": "Unsupported source (should never happen)"},
                status=status.HTTP_405_METHOD_NOT_ALLOWED,
            )

        return Response(status=status.HTTP_200_OK)

    except TestCenterTestSessions.DoesNotExist:
        return Response(
            {"error": "the specified test center test session does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )
    except TestCenterTestSessionData.DoesNotExist:
        return Response(
            {"error": "the specified test center test session data does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )


def get_ta_related_test_center_test_sessions(request):
    user_info = get_user_info_from_jwt_token(request)

    # getting current datetime (UTC)
    now = timezone.now()

    # getting related test center IDs
    test_center_ids = TestCenterTestAdministratorsVW.objects.filter(
        user_id=user_info["user_id"]
    ).values_list("test_center_id", flat=True)

    # getting all "non-past" test sessions related to test_center_ids
    test_sessions = TestCenterTestSessionsVW.objects.filter(
        test_center_id__in=test_center_ids, end_time__gte=now
    )

    # making sure that the user has at least one valid test permission based on the test skill type and test skill sub type from the test_session respectively
    # final_test_sessions_array
    final_test_sessions_array = []

    # getting test IDs based on all TA test permissions and orderless test permissions
    combined_test_ids = get_test_ids_from_ta_test_permissions(user_info["user_id"])

    # looping in test_sessions
    for test_session_data in test_sessions:
        # ==================== SKILLS / SUB-SKILLS RELATED ====================
        if test_session_data.user_accommodation_file_id is None:
            # test session test skill type is SLE
            if test_session_data.test_skill_type_codename == TestSkillTypeCodename.SLE:
                # getting related test skill IDs
                related_test_skill_ids = TestSkillSLE.objects.filter(
                    test_skill_sle_desc_id=test_session_data.test_skill_sub_type_id
                ).values_list("test_skill_id", flat=True)
                test_skill_related_test_ids = TestSkill.objects.filter(
                    id__in=related_test_skill_ids,
                    test_skill_type_id=test_session_data.test_skill_type_id,
                ).values_list("test_definition_id", flat=True)
            # test session test skill type is Occupational
            elif (
                test_session_data.test_skill_type_codename == TestSkillTypeCodename.OCC
            ):
                # getting related test skill IDs
                related_test_skill_ids = TestSkillOccupational.objects.filter(
                    test_skill_occupational_desc_id=test_session_data.test_skill_sub_type_id
                ).values_list("test_skill_id", flat=True)
                test_skill_related_test_ids = TestSkill.objects.filter(
                    id__in=related_test_skill_ids,
                    test_skill_type_id=test_session_data.test_skill_type_id,
                ).values_list("test_definition_id", flat=True)
            # other test skill types
            else:
                # getting related test skill IDs
                test_skill_related_test_ids = TestSkill.objects.filter(
                    test_skill_type_id=test_session_data.test_skill_type_id
                ).values_list("test_definition_id", flat=True)

            # test_skill_related_test_ids is matching with at least one test IDs from combined_test_ids
            if (
                len(
                    list(
                        set(combined_test_ids).intersection(test_skill_related_test_ids)
                    )
                )
                > 0
            ):
                final_test_sessions_array.append(test_session_data)
        # ==================== SKILLS / SUB-SKILLS RELATED (END) ====================

        # ==================== ACCOMMODATION REQUESTS RELATED ====================
        else:
            # test session data related test_id is maching with at least one test IDs from combined_test_ids
            if test_session_data.test_id in combined_test_ids:
                final_test_sessions_array.append(test_session_data)
        # ==================== ACCOMMODATION REQUESTS RELATED (END) ====================

    # serializing the data
    serialized_data = TestCenterTestSessionsViewSerializer(
        final_test_sessions_array, many=True
    ).data

    # getting sorted serialized data by start time and test center name (ascending)
    ordered_test_sessions = sorted(
        serialized_data,
        key=lambda k: (k["start_time"], k["test_center_name"]),
        reverse=False,
    )

    return Response(ordered_test_sessions)


# getting all test IDs based on test permissions and ordlerss test permissions
def get_test_ids_from_ta_test_permissions(user_id):
    # getting test IDs related to user' test permissions (test permissions with test order)
    user_test_permissions_related_test_ids = TestPermissions.objects.filter(
        user_id=user_id
    ).values_list("test_id", flat=True)

    # getting test IDs related to user' orderless test permissions (test permissions without test order)
    user_orderless_test_permissions_related_test_ids = []
    related_ta_extended_profile_data = TaExtendedProfile.objects.filter(user_id=user_id)
    if related_ta_extended_profile_data:
        related_orderless_test_permissions = OrderlessTestPermissions.objects.filter(
            ta_extended_profile_id=related_ta_extended_profile_data[0].id
        )
        for orderless_test_permission_data in related_orderless_test_permissions:
            respective_test_definition_ids = TestDefinition.objects.filter(
                parent_code=orderless_test_permission_data.parent_code,
                test_code=orderless_test_permission_data.test_code,
                active=True,
            ).values_list("id", flat=True)
            user_orderless_test_permissions_related_test_ids = (
                user_orderless_test_permissions_related_test_ids
                + list(respective_test_definition_ids)
            )

    # combining user_test_permissions_related_test_ids and user_orderless_test_permissions_related_test_ids
    combined_test_ids = (
        list(user_test_permissions_related_test_ids)
        + user_orderless_test_permissions_related_test_ids
    )

    return combined_test_ids


def get_test_session_and_ta_related_test_to_administer_options(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(["test_session_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_session_id = itemgetter("test_session_id")(parameters)

    # getting test session data
    test_session_data = TestCenterTestSessionsVW.objects.get(id=test_session_id)

    # ==================== SKILLS / SUB-SKILLS RELATED ====================
    if test_session_data.user_accommodation_file_id is None:
        # getting test IDs based on all TA test permissions and orderless test permissions
        combined_test_ids = get_test_ids_from_ta_test_permissions(user_info["user_id"])

        # test session test skill type is SLE
        if test_session_data.test_skill_type_codename == TestSkillTypeCodename.SLE:
            # getting related test skill IDs
            related_test_skill_ids = TestSkillSLE.objects.filter(
                test_skill_sle_desc_id=test_session_data.test_skill_sub_type_id
            ).values_list("test_skill_id", flat=True)
            test_skill_related_test_ids = TestSkill.objects.filter(
                id__in=related_test_skill_ids,
                test_skill_type_id=test_session_data.test_skill_type_id,
            ).values_list("test_definition_id", flat=True)
        # test session test skill type is Occupational
        elif test_session_data.test_skill_type_codename == TestSkillTypeCodename.OCC:
            # getting related test skill IDs
            related_test_skill_ids = TestSkillOccupational.objects.filter(
                test_skill_occupational_desc_id=test_session_data.test_skill_sub_type_id
            ).values_list("test_skill_id", flat=True)
            test_skill_related_test_ids = TestSkill.objects.filter(
                id__in=related_test_skill_ids,
                test_skill_type_id=test_session_data.test_skill_type_id,
            ).values_list("test_definition_id", flat=True)
        # other test skill types
        else:
            # getting related test skill IDs
            test_skill_related_test_ids = TestSkill.objects.filter(
                test_skill_type_id=test_session_data.test_skill_type_id
            ).values_list("test_definition_id", flat=True)

        # getting matching test ids from combined_test_ids & test_skill_related_test_ids
        matching_test_ids = list(
            set(combined_test_ids).intersection(test_skill_related_test_ids)
        )

        # getting test ids that matches the combined_test_ids but also the test_session_data (test skill type and test skill sub-type)
        # also making sure that no sample tests are part of the list
        final_tests_array = TestDefinition.objects.filter(
            id__in=matching_test_ids, is_public=0, active=1
        )

        # serializing the data
        serialized_data = TestDefinitionSerializer(final_tests_array, many=True).data
    # ==================== SKILLS / SUB-SKILLS RELATED (END) ====================

    # ==================== ACCOMMODATION REQUESTS RELATED ====================
    else:
        # getting test id from the user accommodation file test to administer request
        # also making sure that no sample tests are part of the list
        final_tests_array = TestDefinition.objects.filter(id=test_session_data.test_id)

        # serializing the data
        serialized_data = TestDefinitionSerializer(final_tests_array, many=True).data
    # ==================== ACCOMMODATION REQUESTS RELATED (END) ====================

    return Response(serialized_data)


def get_test_session_related_test_officers(request):
    success, parameters = get_needed_parameters(["test_session_id", "test_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_session_id, test_id = itemgetter("test_session_id", "test_id")(parameters)

    # getting related test center ID
    related_test_center_id = TestCenterTestSessions.objects.get(
        id=test_session_id
    ).test_center_id

    # getting associated TAs
    associated_test_administrators = TestCenterTestAdministrators.objects.filter(
        test_center_id=related_test_center_id
    )

    # making sure that the test administrators have the needed test permissions and orderless test permissions based on the provided test ID
    final_associated_test_administrators = []
    for associated_test_administrator in associated_test_administrators:
        # getting test IDs based on all TA test permissions and orderless test permissions
        combined_test_ids = get_test_ids_from_ta_test_permissions(
            associated_test_administrator.user_id
        )
        # provided test_id is part of the combined_test_ids
        if int(test_id) in combined_test_ids:
            final_associated_test_administrators.append(associated_test_administrator)

    # serializing the data
    serialized_data = TestCenterTestAdministratorsSerializer(
        final_associated_test_administrators, many=True
    ).data

    return Response(serialized_data)


def update_test_center_ola_configs_data(request):
    test_center_ola_configs_data = json.loads(request.body)

    # checking if the respective test center already has OLA configs
    ola_configs = TestCenterOlaConfigs.objects.filter(
        test_center_id=test_center_ola_configs_data["test_center_id"]
    )

    # existing OLA configs
    if ola_configs:
        # overwriting existing data
        ola_configs = ola_configs.last()
        ola_configs.booking_delay = test_center_ola_configs_data["booking_delay"]
        ola_configs.advanced_booking_delay = test_center_ola_configs_data[
            "advanced_booking_delay"
        ]
        ola_configs.save()

        Response(status=status.HTTP_200_OK)

    else:
        # creating new entry in TestCenterOlaConfigs
        TestCenterOlaConfigs.objects.create(
            test_center_id=test_center_ola_configs_data["test_center_id"],
            booking_delay=test_center_ola_configs_data["booking_delay"],
            advanced_booking_delay=test_center_ola_configs_data[
                "advanced_booking_delay"
            ],
        )

        Response(status=status.HTTP_200_OK)

    return Response(None)


def create_new_test_center_ola_vacation_block(request):
    test_center_ola_vacation_block_data = json.loads(request.body)

    try:
        # checking if there is a matching date_from and date_to in this same test center
        matching_date_from_and_date_to = TestCenterOlaVacationBlock.objects.filter(
            test_center_id=test_center_ola_vacation_block_data["test_center_id"],
            date_from=test_center_ola_vacation_block_data["date_from"],
            date_to=test_center_ola_vacation_block_data["date_to"],
        )

        # same date_from and date_to already exists in this test center
        if matching_date_from_and_date_to:
            return Response(
                {
                    "info": "There is already an existing vacation block with the same date_from and date_to in this test center"
                },
                status=status.HTTP_409_CONFLICT,
            )

        # unique date_from and date_to in this test center
        else:
            # creating new TestCenterOlaVacationBlock entry
            new_test_center_ola_vacation_block = (
                TestCenterOlaVacationBlock.objects.create(
                    test_center_id=test_center_ola_vacation_block_data[
                        "test_center_id"
                    ],
                    date_from=test_center_ola_vacation_block_data["date_from"],
                    date_to=test_center_ola_vacation_block_data["date_to"],
                )
            )

            # looping in all languages
            for language_data in Language.objects.all():
                # EN
                if language_data.language_id == Language_id.EN:
                    # creating new TestCenterOlaVacationBlockAvailability entry
                    TestCenterOlaVacationBlockAvailability.objects.create(
                        test_center_ola_vacation_block_id=new_test_center_ola_vacation_block.id,
                        language_id=language_data.language_id,
                        availability=test_center_ola_vacation_block_data[
                            "en_availability"
                        ],
                    )
                # FR
                elif language_data.language_id == Language_id.FR:
                    # creating new TestCenterOlaVacationBlockAvailability entry
                    TestCenterOlaVacationBlockAvailability.objects.create(
                        test_center_ola_vacation_block_id=new_test_center_ola_vacation_block.id,
                        language_id=language_data.language_id,
                        availability=test_center_ola_vacation_block_data[
                            "fr_availability"
                        ],
                    )
                # unsupported language
                else:
                    pass

            return Response(status=status.HTTP_200_OK)

    # should never happen
    except:
        return Response(
            {
                "error": "Something happened during the create new test center OLA vacation block process"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


def edit_test_center_ola_vacation_block(request):
    test_center_ola_vacation_block_data = json.loads(request.body)

    try:
        # checking if there is a matching date_from and date_to in this same test center (excluding current vacation block)
        matching_date_from_and_date_to = TestCenterOlaVacationBlock.objects.filter(
            test_center_id=test_center_ola_vacation_block_data["test_center_id"],
            date_from=test_center_ola_vacation_block_data["date_from"],
            date_to=test_center_ola_vacation_block_data["date_to"],
        ).exclude(id=test_center_ola_vacation_block_data["id"])

        # same date_from and date_to already exists in this test center
        if matching_date_from_and_date_to:
            return Response(
                {
                    "info": "There is already an existing vacation block with the same date_from and date_to in this test center"
                },
                status=status.HTTP_409_CONFLICT,
            )

        # unique date_from and date_to in this test center
        else:
            # getting related test center ola vacation block
            test_center_ola_vacation_block = TestCenterOlaVacationBlock.objects.get(
                id=test_center_ola_vacation_block_data["id"]
            )

            # updating related vacation block data
            test_center_ola_vacation_block.date_from = (
                test_center_ola_vacation_block_data["date_from"]
            )
            test_center_ola_vacation_block.date_to = (
                test_center_ola_vacation_block_data["date_to"]
            )
            test_center_ola_vacation_block.save()

            # getting related test center ola vacation block availability
            test_center_ola_vacation_block_availability = (
                TestCenterOlaVacationBlockAvailability.objects.filter(
                    test_center_ola_vacation_block_id=test_center_ola_vacation_block.id
                )
            )

            # looping in test_center_ola_vacation_block_availability
            for (
                vacation_block_availability_data
            ) in test_center_ola_vacation_block_availability:
                # updating related vacation block availability data
                # EN
                if vacation_block_availability_data.language_id == Language_id.EN:
                    vacation_block_availability_data.availability = (
                        test_center_ola_vacation_block_data["en_availability"]
                    )
                    vacation_block_availability_data.save()
                # FR
                elif vacation_block_availability_data.language_id == Language_id.FR:
                    vacation_block_availability_data.availability = (
                        test_center_ola_vacation_block_data["fr_availability"]
                    )
                    vacation_block_availability_data.save()
                # unsupported language
                else:
                    pass

            return Response(status=status.HTTP_200_OK)

    except TestCenterOlaVacationBlock.DoesNotExist:
        return Response(
            {
                "error": "Test Center Vacation Block not found based on provided parameters"
            },
            status=status.HTTP_404_NOT_FOUND,
        )


def delete_test_center_ola_vacation_block(request):
    test_center_ola_vacation_block_data = json.loads(request.body)

    try:
        # getting related test center ola vacation block
        test_center_ola_vacation_block = TestCenterOlaVacationBlock.objects.get(
            id=test_center_ola_vacation_block_data["id"]
        )

        # getting related test center ola vacation block availability
        test_center_ola_vacation_block_availability = (
            TestCenterOlaVacationBlockAvailability.objects.filter(
                test_center_ola_vacation_block_id=test_center_ola_vacation_block.id
            )
        )

        # deleting test center ola vacation block availability data
        for data in test_center_ola_vacation_block_availability:
            data.delete()

        # deleting test center ola vacation block data
        test_center_ola_vacation_block.delete()

        return Response(status=status.HTTP_200_OK)

    except TestCenterOlaVacationBlock.DoesNotExist:
        return Response(
            {
                "error": "Test Center Vacation Block not found based on provided parameters"
            },
            status=status.HTTP_404_NOT_FOUND,
        )


def add_test_center_ola_test_assessor(request):
    test_center_ola_test_assessor_data = json.loads(request.body)

    try:
        with transaction.atomic():
            # making sure that the provided user_id is not already associated to the provided test center ID
            already_associated = TestCenterOlaTestAssessor.objects.filter(
                test_center_id=test_center_ola_test_assessor_data["test_center_id"],
                user_id=test_center_ola_test_assessor_data["user_id"],
            )

            # already associated (should never happen)
            if already_associated:
                return Response(
                    {
                        "error": "The provided user is already associated to this Test Center"
                    },
                    status=status.HTTP_409_CONFLICT,
                )

            # no association found (expected)
            else:
                # creating new entry in TestCenterOlaTestAssessor table
                new_test_assessor = TestCenterOlaTestAssessor.objects.create(
                    test_center_id=test_center_ola_test_assessor_data["test_center_id"],
                    user_id=test_center_ola_test_assessor_data["user_id"],
                    supervisor=test_center_ola_test_assessor_data["supervisor"],
                )

                # looping in provided language_certification_data
                for language_data in test_center_ola_test_assessor_data[
                    "language_certification_data"
                ]:
                    # creating new entry in TestCenterOlaTestAssessorApprovedLanguage table
                    TestCenterOlaTestAssessorApprovedLanguage.objects.create(
                        test_center_ola_test_assessor_id=new_test_assessor.id,
                        language_id=int(language_data["value"]),
                    )

                return Response(status=status.HTTP_200_OK)

    # should never happen
    except:
        return Response(
            {
                "error": "Something happened during the add test center OLA test assessor process"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


def edit_test_center_ola_test_assessor(request):
    test_center_ola_test_assessor_data = json.loads(request.body)

    try:
        with transaction.atomic():
            # getting related test center ola test assessor data
            related_test_assessor_data = TestCenterOlaTestAssessor.objects.get(
                id=test_center_ola_test_assessor_data["id"]
            )

            # updating respective data
            related_test_assessor_data.supervisor = test_center_ola_test_assessor_data[
                "supervisor"
            ]
            related_test_assessor_data.save()

            # getting related test center ola test assessor approved language data
            related_test_assessor_approved_language_data = (
                TestCenterOlaTestAssessorApprovedLanguage.objects.filter(
                    test_center_ola_test_assessor_id=related_test_assessor_data.id
                )
            )

            # deleting existing approved language data
            related_test_assessor_approved_language_data.delete()

            # looping in provided language_certification_data
            for language_data in test_center_ola_test_assessor_data[
                "language_certification_data"
            ]:
                # creating new entry in TestCenterOlaTestAssessorApprovedLanguage table
                TestCenterOlaTestAssessorApprovedLanguage.objects.create(
                    test_center_ola_test_assessor_id=related_test_assessor_data.id,
                    language_id=int(language_data["value"]),
                )

            return Response(status=status.HTTP_200_OK)

    except TestCenterOlaTestAssessor.DoesNotExist:
        return Response(
            {"error": "the specified test center OLA test assessor does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )


def delete_test_center_ola_test_assessor(request):
    test_center_ola_test_assessor_data = json.loads(request.body)

    try:
        with transaction.atomic():
            # getting related test center ola test assessor data
            related_test_assessor_data = TestCenterOlaTestAssessor.objects.get(
                id=test_center_ola_test_assessor_data["id"]
            )

            # getting related test center ola test assessor approved language data
            related_test_assessor_approved_language_data = (
                TestCenterOlaTestAssessorApprovedLanguage.objects.filter(
                    test_center_ola_test_assessor_id=related_test_assessor_data.id
                )
            )

            # deleting approved language data
            related_test_assessor_approved_language_data.delete()

            # deleting test assessor data
            related_test_assessor_data.delete()

            return Response(status=status.HTTP_200_OK)

    except TestCenterOlaTestAssessor.DoesNotExist:
        return Response(
            {"error": "the specified test center OLA test assessor does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )


def get_test_center_ola_time_slot_assessor_availabilities(request):
    success, parameters = get_needed_parameters(
        ["test_center_id", "language_id"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_center_id, language_id = itemgetter("test_center_id", "language_id")(
        parameters
    )

    optional_parameters = get_optional_parameters(
        ["test_center_ola_time_slot_id"], request
    )

    try:
        # getting test center related assessors
        test_center_related_assessors = TestCenterOlaTestAssessor.objects.filter(
            test_center_id=test_center_id
        )

        # initializing certified_test_center_related_assessor_ids
        certified_test_center_related_assessor_ids = []

        # looping in test_center_related_assessors in order to only keep the assessors that are certified in the provided language
        for test_center_related_assessor in test_center_related_assessors:
            # checking if assessor of current iteration is certified in provided language
            if TestCenterOlaTestAssessorApprovedLanguage.objects.filter(
                test_center_ola_test_assessor_id=test_center_related_assessor.id,
                language_id=language_id,
            ):
                # populating certified_test_center_related_assessor_ids
                certified_test_center_related_assessor_ids.append(
                    test_center_related_assessor.id
                )

        # initializing available_assessor_ids
        available_assessor_ids = []

        # test_center_ola_time_slot_id parameter is provided
        if (
            optional_parameters["test_center_ola_time_slot_id"] is not None
            and optional_parameters["test_center_ola_time_slot_id"] != ""
        ):
            # getting available assessor user IDs based on provided test_center_ola_time_slot_id
            available_assessor_ids_obj = (
                TestCenterOlaTimeSlotAssessorAvailability.objects.filter(
                    test_center_ola_time_slot_id=optional_parameters[
                        "test_center_ola_time_slot_id"
                    ]
                )
            )
            for available_assessor_id_obj in available_assessor_ids_obj:
                available_assessor_ids.append(
                    available_assessor_id_obj.test_center_ola_test_assessor_id
                )

        # initializing final_array
        final_array = []

        # looping in certified_test_center_related_assessor_ids
        for (
            certified_test_center_related_assessor_id
        ) in certified_test_center_related_assessor_ids:
            # initializing available
            available = False

            # assessor_id of current iteration exists in available_assessor_ids
            if certified_test_center_related_assessor_id in available_assessor_ids:
                # setting available to True
                available = True

            # getting respective user id
            respective_user_id = TestCenterOlaTestAssessor.objects.get(
                id=certified_test_center_related_assessor_id
            ).user_id

            # getting user data
            user_data = User.objects.get(id=respective_user_id)
            serialized_user_data = UserSerializer(user_data, many=False).data

            # populating final_array
            final_array.append(
                {
                    "user_id": user_data.id,
                    "test_center_ola_test_assessor_id": certified_test_center_related_assessor_id,
                    "user_email": serialized_user_data["email"],
                    "user_first_name": serialized_user_data["first_name"],
                    "user_last_name": serialized_user_data["last_name"],
                    "available": available,
                }
            )

            # ordering final_array by first/last name (ascending)
            ordered_final_array = sorted(
                final_array,
                key=lambda k: (
                    k["user_first_name"].lower(),
                    k["user_last_name"].lower(),
                ),
                reverse=False,
            )

        return Response(ordered_final_array)

    except User.DoesNotExist:
        return Response(
            {"error": "User not found when getting user data"},
            status=status.HTTP_404_NOT_FOUND,
        )

    except:
        return Response(
            {
                "error": "Something happened during the get test center ola time slot assessor availabilities"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


def add_test_center_ola_time_slot(request):
    test_center_ola_time_slot_data = json.loads(request.body)

    # respective date format
    date_format = "%Y-%m-%dT%H:%M:%S.%f%z"

    # get and convert start time based on provided data (UTC)
    utc_start_time = datetime.datetime.strptime(
        test_center_ola_time_slot_data["start_time"], date_format
    )
    extracted_start_time = utc_start_time.strftime("%H:%M")
    # get and convert end time based on provided data (UTC)
    utc_end_time = datetime.datetime.strptime(
        test_center_ola_time_slot_data["end_time"], date_format
    )
    extracted_end_time = utc_end_time.strftime("%H:%M")

    try:
        # overlapping time validation
        # getting potential overlapping time data
        potential_overlapping_time_slot_data = TestCenterOlaTimeSlot.objects.filter(
            test_center_id=test_center_ola_time_slot_data["test_center_id"],
            assessed_language_id=test_center_ola_time_slot_data["assessed_language_id"],
            day_of_week_id=test_center_ola_time_slot_data["day_of_week_id"],
        )
        # looping in potential_overlapping_time_slot_data
        for time_slot_data in potential_overlapping_time_slot_data:
            if (
                extracted_end_time > time_slot_data.start_time.strftime("%H:%M")
                and time_slot_data.end_time.strftime("%H:%M") > extracted_start_time
            ):
                return Response(
                    {
                        "error": "The time slot you are trying to add is overlapping with another time slot in this test center"
                    },
                    status=status.HTTP_409_CONFLICT,
                )

        # creating new entry in TestCenterOlaTimeSlot table
        new_time_slot = TestCenterOlaTimeSlot.objects.create(
            test_center_id=test_center_ola_time_slot_data["test_center_id"],
            day_of_week_id=test_center_ola_time_slot_data["day_of_week_id"],
            start_time=extracted_start_time,
            end_time=extracted_end_time,
            assessed_language_id=test_center_ola_time_slot_data["assessed_language_id"],
            availability=test_center_ola_time_slot_data["availability"],
            slots_to_prioritize=0,
            # PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
            # slots_to_prioritize=test_center_ola_time_slot_data["slots_to_prioritize"],
            utc_offset=get_current_utc_offset_in_seconds(),
        )

        # PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
        # # creating new entries in TestCenterOlaTimeSlotPrioritization
        # # looping in prioritization
        # for data in test_center_ola_time_slot_data["prioritization"]:
        #     # getting respective reason_for_testing_priority_id based on codename of current iteration
        #     respective_reason_for_testing_priority_id = (
        #         ReasonForTestingPriority.objects.get(codename=data["codename"]).id
        #     )
        #     # creating new entry in TestCenterOlaTimeSlotPrioritization
        #     TestCenterOlaTimeSlotPrioritization.objects.create(
        #         test_center_ola_time_slot_id=new_time_slot.id,
        #         reason_for_testing_priority_id=respective_reason_for_testing_priority_id,
        #         percentage=data["value"],
        #     )

        # creating new entries (if needed) in TestCenterOlaTimeSlotAssessorAvailability
        # looping in assessor_availabilities
        for assessor_availability in test_center_ola_time_slot_data[
            "assessor_availabilities"
        ]:
            # creating new entry in TestCenterOlaTimeSlotAssessorAvailability
            TestCenterOlaTimeSlotAssessorAvailability.objects.create(
                test_center_ola_time_slot_id=new_time_slot.id,
                test_center_ola_test_assessor_id=assessor_availability[
                    "test_center_ola_test_assessor_id"
                ],
            )

        return Response(status=status.HTTP_200_OK)

    # should never happen
    except:
        return Response(
            {
                "error": "Something happened during the add test center OLA time slot process"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


def edit_test_center_ola_time_slot(request):
    test_center_ola_time_slot_data = json.loads(request.body)

    try:
        # getting related test center OLA time slot data
        related_time_slot_data = TestCenterOlaTimeSlot.objects.get(
            id=test_center_ola_time_slot_data["id"]
        )

        # respective date format
        date_format = "%Y-%m-%dT%H:%M:%S.%f%z"

        # get and convert start time based on provided data (UTC)
        utc_start_time = datetime.datetime.strptime(
            test_center_ola_time_slot_data["start_time"], date_format
        )
        extracted_start_time = utc_start_time.strftime("%H:%M")
        # get and convert end time based on provided data (UTC)
        utc_end_time = datetime.datetime.strptime(
            test_center_ola_time_slot_data["end_time"], date_format
        )
        extracted_end_time = utc_end_time.strftime("%H:%M")

        # overlapping time validation
        # getting potential overlapping time data
        potential_overlapping_time_slot_data = TestCenterOlaTimeSlot.objects.filter(
            test_center_id=test_center_ola_time_slot_data["test_center_id"],
            assessed_language_id=test_center_ola_time_slot_data["assessed_language_id"],
            day_of_week_id=test_center_ola_time_slot_data["day_of_week_id"],
        ).exclude(id=related_time_slot_data.id)
        # looping in potential_overlapping_time_slot_data
        for time_slot_data in potential_overlapping_time_slot_data:
            if (
                extracted_end_time > time_slot_data.start_time.strftime("%H:%M")
                and time_slot_data.end_time.strftime("%H:%M") > extracted_start_time
            ):
                return Response(
                    {
                        "error": "The time slot you are trying to edit is overlapping with another time slot in this test center"
                    },
                    status=status.HTTP_409_CONFLICT,
                )

        # updating needed data (if there is no overlapping time found)
        # ========== TIME SLOT DATA ==========
        related_time_slot_data.day_of_week_id = test_center_ola_time_slot_data[
            "day_of_week_id"
        ]
        related_time_slot_data.start_time = extracted_start_time
        related_time_slot_data.end_time = extracted_end_time
        related_time_slot_data.assessed_language_id = test_center_ola_time_slot_data[
            "assessed_language_id"
        ]
        related_time_slot_data.availability = test_center_ola_time_slot_data[
            "availability"
        ]
        # PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
        # related_time_slot_data.slots_to_prioritize = test_center_ola_time_slot_data[
        #     "slots_to_prioritize"
        # ]
        related_time_slot_data.utc_offset = get_current_utc_offset_in_seconds()
        related_time_slot_data.save()
        # ========== TIME SLOT DATA (END) ==========

        # PRIORITIZATION IS NO LONGER USED AT THE TIME SLOT LEVEL FOR OLA TIME SLOT AVAILABILITIES AT THE MOMENT (TO BE DELETED IN THE FUTURE IF THE PRIORITIZATION OFFICIALLY STAYS IN THE BO OLA CONFIGS LEVEL)
        # # ========== TIME SLOT PRIORITIZATION DATA ==========
        # # looping in prioritization
        # for data in test_center_ola_time_slot_data["prioritization"]:
        #     # getting respective reason_for_testing_priority_id
        #     respective_reason_for_testing_priority_id = (
        #         ReasonForTestingPriority.objects.get(codename=data["codename"]).id
        #     )
        #     # checking if there is a related existing entry in TestCenterOlaTimeSlotPrioritization
        #     existing_time_slot_prioritization = TestCenterOlaTimeSlotPrioritization.objects.filter(
        #         test_center_ola_time_slot_id=related_time_slot_data.id,
        #         reason_for_testing_priority_id=respective_reason_for_testing_priority_id,
        #     )
        #     # existing entry
        #     if existing_time_slot_prioritization:
        #         # checking if provided percentage is different than the one in the DB
        #         if existing_time_slot_prioritization.last().percentage != data["value"]:
        #             # updating entry
        #             prioritization_to_update = existing_time_slot_prioritization.last()
        #             prioritization_to_update.percentage = data["value"]
        #             prioritization_to_update.save()
        #     # no entry yet
        #     else:
        #         # creating new entry in TestCenterOlaTimeSlotPrioritization
        #         TestCenterOlaTimeSlotPrioritization.objects.create(
        #             test_center_ola_time_slot_id=related_time_slot_data.id,
        #             reason_for_testing_priority_id=respective_reason_for_testing_priority_id,
        #             percentage=data["value"],
        #         )
        # # ========== TIME SLOT PRIORITIZATION DATA (END) ==========

        # ========== ASSESSOR AVAILABILITIES DATA ==========
        # building the assessor_availability_test_assessor_ids_array
        assessor_availability_test_assessor_ids_array = []
        for assessor_availability in test_center_ola_time_slot_data[
            "assessor_availabilities"
        ]:
            assessor_availability_test_assessor_ids_array.append(
                assessor_availability["test_center_ola_test_assessor_id"]
            )

        # deleting all assessor availabilities that are no longer selected (if applicable)
        TestCenterOlaTimeSlotAssessorAvailability.objects.filter(
            test_center_ola_time_slot_id=related_time_slot_data.id
        ).exclude(
            test_center_ola_test_assessor_id__in=assessor_availability_test_assessor_ids_array
        ).delete()
        # getting/building existing assessor availability assessor ids
        existing_assessor_availability_assessor_ids = []
        existing_assessor_availabilities = (
            TestCenterOlaTimeSlotAssessorAvailability.objects.filter(
                test_center_ola_time_slot_id=related_time_slot_data.id
            )
        )
        for assessor_availability in existing_assessor_availabilities:
            existing_assessor_availability_assessor_ids.append(
                assessor_availability.test_center_ola_test_assessor_id
            )
        # creating needed assessor availabilities if they don't exist yet (if applicable)
        for assessor_availability in test_center_ola_time_slot_data[
            "assessor_availabilities"
        ]:
            # test_center_ola_test_assessor_id of current iteration does not already exist
            if (
                assessor_availability["test_center_ola_test_assessor_id"]
                not in existing_assessor_availability_assessor_ids
            ):
                # creating new entry in TestCenterOlaTimeSlotAssessorAvailability
                TestCenterOlaTimeSlotAssessorAvailability.objects.create(
                    test_center_ola_time_slot_id=related_time_slot_data.id,
                    test_center_ola_test_assessor_id=assessor_availability[
                        "test_center_ola_test_assessor_id"
                    ],
                )
        # ========== ASSESSOR AVAILABILITIES DATA (END) ==========

        return Response(status=status.HTTP_200_OK)

    except TestCenterOlaTimeSlot.DoesNotExist:
        return Response(
            {"error": "the specified test center OLA time slot does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )


def delete_test_center_ola_time_slot(request):
    test_center_ola_time_slot_data = json.loads(request.body)

    try:
        # getting related test center OLA time slot data
        related_time_slot = TestCenterOlaTimeSlot.objects.get(
            id=test_center_ola_time_slot_data["id"]
        )

        # deleting related assessor availabilities
        TestCenterOlaTimeSlotAssessorAvailability.objects.filter(
            test_center_ola_time_slot_id=related_time_slot.id
        ).delete()

        # deleting related prioritization
        TestCenterOlaTimeSlotPrioritization.objects.filter(
            test_center_ola_time_slot_id=related_time_slot.id
        ).delete()

        # deleting data
        related_time_slot.delete()

        return Response(status=status.HTTP_200_OK)

    except TestCenterOlaTimeSlot.DoesNotExist:
        return Response(
            {"error": "the specified test center OLA time slot does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )
