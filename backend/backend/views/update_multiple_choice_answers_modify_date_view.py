from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.update_multiple_choice_answers_modify_date import (
    update_multiple_choice_answers_modify_date,
)


class UpdateMultipleChoiceAnswersModifyDate(APIView):
    def post(self, request):
        return update_multiple_choice_answers_modify_date(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
