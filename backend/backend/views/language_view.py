from rest_framework import permissions
from rest_framework.views import APIView
from backend.views.language import get_language_data


# getting language data
class GetLanguageData(APIView):
    def get(self, request):
        return get_language_data(request)

    def get_permissions(self):
        return [permissions.AllowAny()]
