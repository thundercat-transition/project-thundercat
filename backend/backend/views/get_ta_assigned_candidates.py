from operator import itemgetter
from rest_framework.response import Response
from rest_framework import status
from cms.cms_models.test_section import TestSection
from cms.views.utils import get_needed_parameters, get_optional_parameters
from backend.views.utils import get_user_info_from_jwt_token
from backend.static.assigned_test_status import (
    AssignedTestStatus,
    get_assigned_test_status_id,
)
from user_management.user_management_models.user_models import User
from user_management.user_management_models.custom_user_permissions_model import (
    CustomUserPermissions,
)
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.static.permission import Permission
from db_views.db_view_models.ta_assigned_candidates_vw import TaAssignedCandidatesVW
from db_views.serializers.assigned_tests_serializers import (
    TaAssignedCandidatesViewSerializer,
)


# getting test administrator assigned candidates (candidates that are checked in with current ta's test access code)
def get_ta_assigned_candidates(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(["test_session_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_session_id = itemgetter("test_session_id")(parameters)
    try:
        # getting all assigned candidates based on the specified TA (supervised tests only)
        assigned_status_id = get_assigned_test_status_id(AssignedTestStatus.ASSIGNED)
        checked_in_status_id = get_assigned_test_status_id(
            AssignedTestStatus.CHECKED_IN
        )
        locked_status_id = get_assigned_test_status_id(AssignedTestStatus.LOCKED)
        paused_status_id = get_assigned_test_status_id(AssignedTestStatus.PAUSED)
        pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
        active_status_id = get_assigned_test_status_id(AssignedTestStatus.ACTIVE)
        transition_status_id = get_assigned_test_status_id(
            AssignedTestStatus.TRANSITION
        )
        active_candidates = TaAssignedCandidatesVW.objects.filter(
            status_id__in=(
                assigned_status_id,
                checked_in_status_id,
                locked_status_id,
                paused_status_id,
                pre_test_status_id,
                active_status_id,
                transition_status_id,
            ),
            uit_invite_id=None,
            test_session_id=test_session_id,
        )

        # ordering assigned candidates by last_name
        # getting serialized assigned candidates data
        serialized_assigned_candidates_data = TaAssignedCandidatesViewSerializer(
            active_candidates, many=True
        ).data
        # getting sorted serialized data by last_name (ascending)
        ordered_assigned_candidate = sorted(
            serialized_assigned_candidates_data,
            key=lambda k: k["candidate_last_name"].lower(),
            reverse=False,
        )
        # initializing assigned candidates ids array (the assigned_candidates_ids_array order here represents the order)
        assigned_candidates_ids_array = []
        # looping in ordered assigned candidates
        for i in ordered_assigned_candidate:
            # inserting user permissions ids (ordered respectively by last name) in an array
            assigned_candidates_ids_array.insert(
                len(assigned_candidates_ids_array), i["id"]
            )
        # sorting assigned candidates queryset based on ordered (by last name) assigned candidates ids
        new_active_candidates = list(
            TaAssignedCandidatesVW.objects.filter(id__in=assigned_candidates_ids_array)
        )
        new_active_candidates.sort(
            key=lambda t: assigned_candidates_ids_array.index(t.id)
        )

        # making sure that the specified user is a TA
        # getting user' permissions
        user_permissions = CustomUserPermissions.objects.filter(
            user_id=user_info["user_id"]
        )
        # initializing is_ta flag to false
        is_ta = False
        # looping in user' permissions
        for user_permission in user_permissions:
            # specified user has TA permission
            if (
                user_permission.permission_id
                == CustomPermissions.objects.get(
                    codename=Permission.TEST_ADMINISTRATOR
                ).permission_id
            ):
                # set is_ta flag to true
                is_ta = True

        # if specified user is a super user
        if User.objects.get(id=user_info["user_id"]).is_staff:
            # set is_ta flag to true
            is_ta = True

        # specified user is a TA or a super user
        if is_ta:
            # serializing and returning data
            serializer = TaAssignedCandidatesViewSerializer(
                new_active_candidates, many=True
            )
            return Response(serializer.data)

        # specified user is NOT a TA
        else:
            return Response(
                {"error": "the specified user is not a test administrator"},
                status=status.HTTP_400_BAD_REQUEST,
            )

    except User.DoesNotExist:
        return Response(
            {"error": "the specified user does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


# getting test administrator assigned candidates (candidates that are checked in with current ta's test access code)
def get_default_test_sections_time(request):
    optional_parameters = get_optional_parameters(["test_id"], request)

    # initializing test_sections_time
    test_sections_time = []

    # getting default test sections time
    default_test_sections_time_data = TestSection.objects.filter(
        test_definition_id=optional_parameters["test_id"],
        default_time__isnull=False,
    )
    # looping in default_test_sections_time_data
    for test_section_time_data in default_test_sections_time_data:
        # populating test_sections_time array
        test_sections_time.append(
            {
                "test_section_id": test_section_time_data.id,
                "default_time": test_section_time_data.default_time,
                "title_en": test_section_time_data.en_title,
                "title_fr": test_section_time_data.fr_title,
            }
        )

    return Response(test_sections_time)
