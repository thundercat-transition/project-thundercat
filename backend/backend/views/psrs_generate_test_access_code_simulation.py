import random
import string
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework import status
from cms.cms_models.test_definition import TestDefinition
from backend.views.utils import is_undefined
from backend.custom_models.test_access_code_model import TestAccessCode


# getting a new randomly generated test access code
def generate_random_test_access_code():
    allowed_chars = string.ascii_uppercase + string.digits
    random_test_access_code = "".join(random.choice(allowed_chars) for i in range(10))
    return random_test_access_code


def access_code_already_exists(random_code):
    return TestAccessCode.objects.filter(test_access_code=random_code).count() > 0


# populating uit test access code table (MCTEST001 Test)


def populate_uit_test_access_code_table(request):
    # call the function that generates the random room number
    random_test_access_code = generate_random_test_access_code()
    while access_code_already_exists(random_test_access_code):
        random_test_access_code = generate_random_test_access_code()

    # get TD with params
    test_definition = (
        TestDefinition.objects.filter(test_code="EMIBSAMPLE001")
        .order_by("version")
        .last()
    )
    # saving the response with the needed parameters
    response = TestAccessCode.objects.create(
        test_access_code=random_test_access_code,
        # test_id=UitTestDefinition.MCTEST001,
        test=test_definition,
    )
    response.save()
    return Response(random_test_access_code)


# deleting existing uit test access code
def delete_uit_test_access_code(request):
    # making sure that we have the needed parameters
    test_access_code = request.query_params.get("test_access_code", None)
    if is_undefined(test_access_code):
        return Response(
            {"error": "no 'test_access_code' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # get the test access code id
    try:
        test_access_code_id = TestAccessCode.objects.get(
            test_access_code=test_access_code
        ).id
    except TestAccessCode.DoesNotExist:
        return Response(
            {"error": "the specified test access code does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # delete the whole row based on parameters above
    TestAccessCode.objects.get(
        id=test_access_code_id, test_access_code=test_access_code
    ).delete()
    return Response(status=status.HTTP_200_OK)


class GetNewUitTestAccessCode(APIView):
    def get(self, request):
        return populate_uit_test_access_code_table(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class DeleteUitTestAccessCode(APIView):
    def get(self, request):
        return delete_uit_test_access_code(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
