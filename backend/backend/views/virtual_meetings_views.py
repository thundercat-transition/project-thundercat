import requests
import pytz
import json
import re
from datetime import datetime
from django.db import connection
from django.conf import settings

# from urllib import parse TODO uncomment if we add a keyword parameter
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from django.db.models.functions import Coalesce
from backend.static.languages import Languages
from backend.views.utils import get_user_info_from_jwt_token
from backend.custom_models.keyring import Keyring
from backend.custom_models.site_admin_setting import SiteAdminSetting
from backend.static.site_admin_setting_type import Key, SiteAdminSettingType
from backend.custom_models.virtual_teams_meeting_session import (
    VirtualTeamsMeetingSession,
)
from backend.custom_models.virtual_test_room import VirtualTestRoom
from backend.serializers.virtual_test_room_serializer import VirtualTestRoomSerializer
from cms.views.utils import get_needed_parameters
from operator import itemgetter
from user_management.user_management_models.user_models import User
from backend.custom_models.test_center_test_session_data import (
    TestCenterTestSessionData,
)
from backend.custom_models.test_center_test_sessions import TestCenterTestSessions

from user_management.views.utils import CustomPagination
from db_views.db_view_models.virtual_test_session_details_vw import (
    VirtualTestSessionDetailsVW,
)
from db_views.serializers.virtual_test_session_details_serializers import (
    VirtualTestSessionDetailsViewSerializer,
)
from backend.celery.tasks import cancel_teams_invite


# helper function to check if ms teams is active or not
def is_ms_teams_active():
    return SiteAdminSetting.objects.get(
        codename=SiteAdminSettingType.ENABLE_MS_TEAMS_API
    ).is_active


# helper function to check if webex is active or not
def is_webex_active():
    return SiteAdminSetting.objects.get(
        codename=SiteAdminSettingType.ENABLE_WEBEX
    ).is_active


class CreateVirtualMeeting(APIView):
    def post(self, request):
        return create_virtual_meeting(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class AddAttendee(APIView):
    def post(self, request):
        return add_attendee(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class RemoveAttendee(APIView):
    def post(self, request):
        return remove_attendee(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetVirtualMeetingLink(APIView):
    def get(self, request):
        return get_virtual_meeting_link(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class CleanUpVirtualMeeting(APIView):
    def get(self, request):
        return clean_up_virtual_meeting(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class AddVirtualRoom(APIView):
    def post(self, request):
        return add_virtual_room(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class DeleteVirtualRoom(APIView):
    def get(self, request):
        return delete_virtual_room(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetTeamsRooms(APIView):
    def get(self, request):
        return get_teams_rooms(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetAssignedVirtualTests(APIView):
    def get(self, request):
        return get_assigned_virtual_tests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_access_token():  # can throw KeyError
    # get the various teams keys, send a 400 if they DNE
    app_id = check_key(Key.MS_TEAMS_API_APP_ID)
    tenant_id = check_key(Key.MS_TEAMS_API_TENANT_ID)
    secret = check_key(Key.MS_TEAMS_API_SECRET)

    body = {
        "client_id": app_id,
        "scope": "https://graph.microsoft.com/.default",
        "client_secret": secret,
        "grant_type": "client_credentials",  # THIS DOES NOT SHOW
    }
    url = "https://login.microsoftonline.com/" + tenant_id + "/oauth2/v2.0/token"
    headers = {
        # "Authorization": key,
        "Accept": "*/*",
        "Content-Type": "application/x-www-form-urlencoded",  # "application/json",
        "Cache-Control": "no-cache",
    }
    # get the response from the URL
    response = requests.post(
        url,
        headers=headers,
        data=body,
    )
    response = response.json()
    access_token = response["access_token"]
    return access_token


def date_time_to_datetime(date, time):
    return str(date) + "T" + str(time) + ":00"


def create_teams_meeting(
    subject,
    start_time,
    end_time,
    date,
    timezone,
    candidate_email,
    candidate_name,
    user_id,
    access_token,
    body_content,
):
    start = date_time_to_datetime(date, start_time)
    end = date_time_to_datetime(date, end_time)
    body = {
        "subject": subject,
        "body": {"contentType": "HTML", "content": body_content},
        "start": {"dateTime": start, "timeZone": timezone},
        "end": {"dateTime": end, "timeZone": timezone},
        "attendees": [
            {
                "emailAddress": {
                    "address": candidate_email,
                    "name": candidate_name,
                },
                "type": "required",
            },
        ],
        "isOnlineMeeting": True,
        "onlineMeetingProvider": "teamsForBusiness",
    }
    body = json.dumps(body)

    headers = {
        "Authorization": access_token,
        "Accept": "*/*",
        "Content-Type": "application/json",
    }

    # get the response from the URL
    response = requests.post(
        "https://graph.microsoft.com/v1.0/users/" + user_id + "/calendar/events",
        headers=headers,
        data=body,
    )
    response = response.json()
    meeting_id = response["id"]
    online_meeting = response["onlineMeeting"]
    join_url = None
    if online_meeting is not None:
        join_url = online_meeting["joinUrl"]

    return [meeting_id, join_url]


def create_virtual_meeting(request):
    # check if webex or teams is active
    is_active_ms_teams = is_ms_teams_active()
    is_active_webex = is_webex_active()
    if not is_active_ms_teams and not is_active_webex:
        # if nothing is active, simply return and move on
        return Response(
            {"OK": "No virtual meeting tool is active"}, status=status.HTTP_200_OK
        )

    # get relevant info
    user_info = get_user_info_from_jwt_token(request)
    user_id = user_info["user_id"]
    user = User.objects.get(id=user_info["user_id"])
    success, parameters = get_needed_parameters(
        ["start_time", "end_time", "date", "timezone", "test_session_id"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    start_time, end_time, date, timezone, test_session_id = itemgetter(
        "start_time", "end_time", "date", "timezone", "test_session_id"
    )(parameters)

    candidate_name = user_info["username"]
    candidate_email = user.email

    # respective date format
    # date_format = "%H:%M"
    date_format = "%Y-%m-%dT%H:%M:%S"

    # get and convert start time based on provided data (UTC)
    utc_start_time = datetime.strptime(
        "{0}T{1}:00".format(
            date,
            start_time,
        ),
        date_format,
    ).astimezone(pytz.utc)
    # get and convert end time based on provided data (UTC)
    utc_end_time = datetime.strptime(
        "{0}T{1}:00".format(
            date,
            end_time,
        ),
        date_format,
    ).astimezone(pytz.utc)

    if is_active_ms_teams:
        # get the room that has the least bookings for the time slot.
        # needs left outer join + on + ands to filter this properly
        with connection.cursor() as cursor:
            cursor.execute(
                """SELECT
                    vtr.id as room_id,
                    vtr.user_id as room_user_id,
                    vml.date,
                    vml.start_time,
                    vml.end_time,
                 COUNT(vml.id) count
                  FROM {db_name}..custom_models_virtualtestroom vtr
                  LEFT OUTER JOIN {db_name}..custom_models_virtualteamsmeetingsession vml on vtr.id=vml.virtual_test_room_id
                  AND vml.date='{date}' AND vml.start_time='{start_time}' and vml.start_time='{end_time}'
                 GROUP BY vtr.id, user_id, date, start_time, end_time
                 ORDER BY count
                  """.format(
                    db_name=settings.DATABASES["default"]["NAME"],
                    date=date,
                    start_time=utc_start_time,
                    end_time=utc_end_time,
                )
            )
            room_user = cursor.fetchone()

            # get the first and second values from the tuple
            room_id = room_user[0]
            room_user_id = room_user[1]

        # get the access token, or throw a 400 if it DNE
        try:
            access_token = get_access_token()
        except Exception as e:
            return Response(
                {"error": e},
                status=status.HTTP_400_BAD_REQUEST,
            )

        subject = "RÉSERVATION DE TEST/TEST RESERVATION"
        body_content = """<div>Pour vous connecter à la réunion sur MS Teams pour votre test, vous pouvez cliquer sur le lien ci-dessous ou vous connectez à l’OEC et cliquer sur le bouton « Rejoindre la réunion ». 
        <br/>
        <br/>Veuillez consulter notre site Web pour obtenir de l’information importante afin de vous préparer au test : <a href="https://www.canada.ca/fr/commission-fonction-publique/services/evaluation-langue-seconde/gestionnaires/evaluation-linguistique-oral-sle.html">Évaluation de langue seconde – Évaluation linguistique à l’oral - Canada.ca</a></div>
        <br/>
        <hr/>
        <br/>
        <div>To join the MS Teams meeting for your test, you can click on the link below or log into CAT and click on the button “Join meeting”.
        <br/>
        <br/>Please consult our website for important information on how to prepare for the test: <a href="https://www.canada.ca/en/public-service-commission/services/second-language-testing-public-service/managers/oral-language-assessment-sle.html">Second Language Evaluation – Oral Language Assessment - Canada.ca</a></div>
        """

        [meeting_id, join_url] = create_teams_meeting(
            subject,
            start_time,
            end_time,
            date,
            timezone,
            candidate_email,
            candidate_name,
            room_user_id,
            access_token,
            body_content,
        )
        virtual_teams_meeting_session = VirtualTeamsMeetingSession(
            virtual_test_room_id=room_id,
            meeting_id=meeting_id,
            meeting_link=join_url,
            candidate_id=user_id,
            date=date,
            start_time=utc_start_time,
            end_time=utc_end_time,
            test_session_id=test_session_id,
        )
        virtual_teams_meeting_session.save()
        return Response({"OK": "OK"}, status=status.HTTP_200_OK)

    if is_active_webex:
        # should not come here yet, but handle it
        return Response({"OK": "webex is not implemented"}, status=status.HTTP_200_OK)

    # should not come here, but handle it
    return Response(
        {"OK": "No virtual meeting tool is activated"}, status=status.HTTP_200_OK
    )


def check_key(codename):
    # get the key from keyring
    key = Keyring.objects.filter(codename=codename).last()

    # if it does not exist, raise an error with a message
    if key is None:
        raise KeyError(codename + " is not defined in Keyring")

    # otherwise get the actual key from it and return

    key = key.key

    return key


def get_virtual_meeting_link(request):
    # check if webex or teams is active
    is_active_ms_teams = is_ms_teams_active()
    is_active_webex = is_webex_active()
    if not is_active_ms_teams and not is_active_webex:
        # if nothing is active, simply return and move on
        return Response(
            {"OK": "No virtual meeting tool is active"},
            status=status.HTTP_204_NO_CONTENT,
        )

    user_info = get_user_info_from_jwt_token(request)
    user_id = user_info["user_id"]

    success, parameters = get_needed_parameters(
        ["test_session_id"],
        request,
    )

    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_session_id = itemgetter("test_session_id")(parameters)

    if is_active_ms_teams:
        virtual_meeting_session = VirtualTeamsMeetingSession.objects.filter(
            candidate_id=user_id,
            test_session_id=test_session_id,
        ).last()

        return Response(
            {"meeting_link": virtual_meeting_session.meeting_link},
            status=status.HTTP_200_OK,
        )

    if is_active_webex:
        # should not come here yet, but handle it
        return Response(
            {"OK": "webex is not implemented"}, status=status.HTTP_204_NO_CONTENT
        )

    # should not come here, but handle it
    return Response(
        {"OK": "No virtual meeting tool is activated"},
        status=status.HTTP_204_NO_CONTENT,
    )


def clean_up_virtual_meeting(request):
    # get relevant info
    user_info = get_user_info_from_jwt_token(request)
    user_id = user_info["user_id"]

    success, parameters = get_needed_parameters(
        ["test_session_id"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_session_id = itemgetter("test_session_id")(parameters)

    return cancel_virtual_meeting(user_id, test_session_id)


# seperate function to be used within the backend only
def cancel_virtual_meeting(user_id, test_session_id):
    # check if webex or teams is active
    is_active_ms_teams = is_ms_teams_active()
    is_active_webex = is_webex_active()

    message = None

    if is_active_ms_teams:
        # get the access token, or throw a 400 if it DNE
        try:
            access_token = get_access_token()
        except Exception as e:
            return Response(
                {"error": e},
                status=status.HTTP_400_BAD_REQUEST,
            )

        virtual_meeting_sessions = VirtualTeamsMeetingSession.objects.filter(
            candidate_id=user_id,
            test_session_id=test_session_id,
        )
        for virtual_meeting_session in virtual_meeting_sessions:
            meeting_id = virtual_meeting_session.meeting_id
            # get the MS Teams "user_id"
            virtual_user_id = virtual_meeting_session.virtual_test_room.user_id

            headers = {
                "Authorization": access_token,
                "Accept": "*/*",
                "Content-Type": "application/json",
            }

            # sending emails (using celery since multiple emails can be sent in this action)
            cancel_teams_invite.delay(virtual_user_id, meeting_id, headers)

            # there is no real need to handle the response for a delete

            virtual_meeting_session.delete()
        message = {"OK": "OK"}

    if is_active_webex:
        # should not come here yet, but handle it
        message = {"OK": "webex is not implemented"}

    # clean up other session related data
    related_test_session = TestCenterTestSessions.objects.get(id=test_session_id)
    test_session_data_id = related_test_session.test_session_data_id
    related_test_session.delete()
    TestCenterTestSessionData.objects.get(id=test_session_data_id).delete()

    if message is not None:
        return Response(message, status=status.HTTP_200_OK)

    # should not come here, but handle it
    return Response(
        {"OK": "No virtual meeting tool is activated"}, status=status.HTTP_200_OK
    )


def add_virtual_room(request):
    success, parameters = get_needed_parameters(
        ["name", "room_user_id"],
        request,
    )

    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    name, room_user_id = itemgetter("name", "room_user_id")(parameters)

    virtual_test_room = VirtualTestRoom(name=name, user_id=room_user_id)
    virtual_test_room.save()

    return Response({"OK": "OK"}, status=status.HTTP_200_OK)


def delete_virtual_room(request):
    success, parameters = get_needed_parameters(
        ["name"],
        request,
    )

    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    name = itemgetter("name")(parameters)

    virtual_test_rooms = VirtualTestRoom.objects.filter(name=name)
    for vtr in virtual_test_rooms:
        # break the link with the sessions; this is only used to prevent overbooking a givne room
        sessions = VirtualTeamsMeetingSession.objects.filter(
            virtual_test_room_id=vtr.id
        )
        for session in sessions:
            session.virtual_test_room_id = None
            session.save()
        vtr.delete()

    return Response({"OK": "OK"}, status=status.HTTP_200_OK)


def get_teams_rooms(request):
    page = request.query_params.get("page", None)
    page_size = request.query_params.get("page_size", None)

    has_pages = False
    is_a_search_request = False

    # decode keyword to get special characters NOTE there is no keyword currently, but might be added later
    keyword = "null"  # parse.unquote(request.query_params.get("keyword", None))
    # if keyword is not defined
    if keyword != "null":
        # make sure that page and page_size parameters are also defined
        if page == "null" or page_size == "null":
            return Response(
                {
                    "error": "please make sure that 'page' and 'page_size' parameters are well defined"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        else:
            is_a_search_request = True

    current_page = 0
    paginator = None

    # if page and page_size are defined
    if page != "null" and page_size != "null":
        # pagination class
        pagination_class = CustomPagination
        paginator = pagination_class()
        # initializing current_page and page_size
        current_page = int(page)
        page_size = int(page_size)
        has_pages = True

    # if this is a search call
    if is_a_search_request:
        pass  # TODO implement when we process keyword; see get_test_definition_view's GetTestDefinitionData

    # this is not a search call
    else:
        virtual_test_rooms = VirtualTestRoom.objects.all()
        # has_pages is True
        if has_pages:
            # only getting data for current selected page
            new_virtual_test_rooms = virtual_test_rooms[
                (current_page - 1) * page_size : current_page * page_size
            ]
            serialized_data = obfuscate_data(
                VirtualTestRoomSerializer(
                    new_virtual_test_rooms,
                    many=True,
                ).data
            )

            return paginator.get_paginated_response(
                serialized_data,
                virtual_test_rooms.count(),
                current_page,
                page_size,
            )
        # has_pages is False
        else:
            serialized_data = obfuscate_data(
                VirtualTestRoomSerializer(
                    virtual_test_rooms,
                    many=True,
                ).data
            )

            return Response(
                {
                    "next_page_number": None,
                    "previous_page_number": None,
                    "count": None,
                    "current_page_number": None,
                    "results": serialized_data,
                }
            )


def obfuscate_data(room_data):
    new_room_data = []
    for room in room_data:
        room["user_id"] = obfuscate_user_id(room["user_id"])
        new_room_data.append(room)
    return new_room_data


def obfuscate_user_id(user_id):
    return re.sub(r"[A-Za-z0-9](?=.{4,}$)", "*", user_id)


def get_assigned_virtual_tests(request):
    user_info = get_user_info_from_jwt_token(request)

    query_list = VirtualTestSessionDetailsVW.objects.filter(
        candidate_id=user_info["user_id"]
    ).order_by("start_time")

    serialized_data = VirtualTestSessionDetailsViewSerializer(
        query_list,
        many=True,
    ).data

    return Response(serialized_data)


def add_attendee(request):
    # if teams is not active
    if not is_ms_teams_active():
        return Response({"OK": "OK"}, status=status.HTTP_200_OK)

    success, parameters = get_needed_parameters(
        ["ta_user_id", "test_session_id"],
        request,
    )

    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    ta_user_id, test_session_id = itemgetter("ta_user_id", "test_session_id")(
        parameters
    )

    meeting_id, access_token, meeting_data, user_id, ta_email = get_attendee_info(
        test_session_id, ta_user_id
    )

    attendees = meeting_data["attendees"]

    # create a new attendee
    new_att = {
        "type": "required",
        "emailAddress": {"name": ta_email, "address": ta_email},
    }

    attendees.append(new_att)

    return update_attendee(meeting_data, attendees, meeting_id, access_token, user_id)


def remove_attendee(request):
    # if teams is not active
    if not is_ms_teams_active():
        return Response({"OK": "OK"}, status=status.HTTP_200_OK)
    success, parameters = get_needed_parameters(
        ["ta_user_id", "test_session_id"],
        request,
    )

    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    ta_user_id, test_session_id = itemgetter("ta_user_id", "test_session_id")(
        parameters
    )

    meeting_id, access_token, meeting_data, user_id, ta_email = get_attendee_info(
        test_session_id, ta_user_id
    )

    att_list = meeting_data["attendees"]

    attendees = []
    # if the attendee does not have the email address to remove, then add it to the list
    for att in att_list:
        if att["emailAddress"]["address"] != ta_email:
            attendees.append(att)

    return update_attendee(meeting_data, attendees, meeting_id, access_token, user_id)


def get_attendee_info(test_session_id, ta_user_id):

    vtms = VirtualTeamsMeetingSession.objects.filter(
        test_session_id=test_session_id
    ).first()
    virtual_test_room = vtms.virtual_test_room
    meeting_id = vtms.meeting_id

    user_id = virtual_test_room.user_id
    access_token = get_access_token()
    ta_user = User.objects.filter(id=ta_user_id).first()

    headers = {
        "Authorization": access_token,
        "Accept": "*/*",
        "Content-Type": "application/json",
    }

    # get the response from the URL
    response = requests.get(
        "https://graph.microsoft.com/v1.0/users/"
        + user_id
        + "/calendar/events/"
        + meeting_id,
        headers=headers,
    )
    meeting_data = response.json()

    return meeting_id, access_token, meeting_data, user_id, ta_user.email


def update_attendee(meeting_data, attendees, meeting_id, access_token, user_id):
    meeting_data["attendees"] = attendees
    body = json.dumps(meeting_data)

    headers = {
        "Authorization": access_token,
        "Accept": "*/*",
        "Content-Type": "application/json",
    }

    # get the response from the URL
    response = requests.patch(
        "https://graph.microsoft.com/v1.0/users/"
        + user_id
        + "/calendar/events/"  # or /users/{id}/calendars/{id}
        + meeting_id,
        headers=headers,
        data=body,
    )
    response = response.json()
    return Response({"OK": "OK"}, status=status.HTTP_200_OK)
