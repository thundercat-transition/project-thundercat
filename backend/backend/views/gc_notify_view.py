import requests
import json
from rest_framework import status
from backend.static.site_admin_setting_type import Key
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


# helper function to check if it is active or not
def is_gc_notify_active():
    # import here as these can occur within a celery task
    from backend.custom_models.site_admin_setting import SiteAdminSetting
    from backend.static.site_admin_setting_type import SiteAdminSettingType

    return SiteAdminSetting.objects.get(
        codename=SiteAdminSettingType.ENABLE_GC_NOTIFY
    ).is_active


# function to send 2fa emails
def send_2fa_email(email, personalization):
    send_gc_notify_email(
        email,
        GCNotifyTemplateCodenames.TWO_FACTOR_AUTHENTICATION,
        personalization,
        first_name=personalization["first_name"],
        last_name=personalization["last_name"],
    )


# function to send reset password email
def password_reset(personalization):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.PASSWORD_RESET,
        personalization,
    )


def invite_candidate_to_non_standard_test_session(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.INVITE_CANDIDATE_TO_NON_STANDARD_TEST_SESSION,
        personalization,
    )


def update_non_standard_test_session_invitation(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.UPDATE_NON_STANDARD_TEST_SESSION_INVITE,
        personalization,
    )


def delete_non_standard_test_session(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.DELETE_NON_STANDARD_TEST_SESSION,
        personalization,
    )


def delete_specific_test_session(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.DELETE_SPECIFIC_TEST_SESSION,
        personalization,
    )


def reservation_code_withdraw_no_booking(
    email,
    personalization,
):
    send_gc_notify_email(
        email,
        GCNotifyTemplateCodenames.RESERVATION_CODE_WITHDRAW_NO_BOOKING,
        personalization,
    )


def reservation_code_withdraw_from_booked_session(
    email,
    personalization,
):
    send_gc_notify_email(
        email,
        GCNotifyTemplateCodenames.RESERVATION_CODE_WITHDRAW_FROM_BOOKED_SESSION,
        personalization,
    )


def reservation_code_booking(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.RESERVATION_CODE_BOOKING,
        personalization,
    )


def reservation_code_book_later(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.RESERVATION_CODE_BOOK_LATER,
        personalization,
    )


def test_session_candidates_to_security(
    email, personalization, test_center_manager_email
):
    send_gc_notify_email(
        email,
        GCNotifyTemplateCodenames.TEST_SESSION_CANDIDATES_TO_SECURITY,
        personalization,
        email_contact_if_fail=test_center_manager_email,
    )


def modify_uit_validity_end_date(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.MODIFY_UIT_VALIDITY_END_DATE,
        personalization,
    )


def deactivate_uit_test_cancellation(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.DEACTIVATE_UIT_TEST_CANCELLATION,
        personalization,
    )


# function to send an invite for supervised tests
def send_supervised_test_invitation_email(assessment_process_id, personalization):
    # import here as these can occur within a celery task
    from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames

    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.RESERVATION_INVITE,
        personalization,
        email_contact_if_fail=personalization[
            "contact_email_for_candidates"
        ],  # this is the party to recieve the email of failed delivery
        first_name=personalization["first_name"],
        last_name=personalization["last_name"],
        assessment_process_id=assessment_process_id,
    )


def send_failed_supervised_invitation_delivery(personalization):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.FAILED_SUP_DELIVERY,
        personalization,
    )


def send_failed_unsupervised_invitaion_delivery(personalization):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.FAILED_UIT_DELIVERY,
        personalization,
    )


# one for each template, pass
def send_uit_invite_basic(uit_invite_id, personalization):
    # import here as these can occur within a celery task
    from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames

    send_uit_invite_all(
        uit_invite_id, personalization, GCNotifyTemplateCodenames.UIT_INVITE_BASIC
    )


def send_uit_invite_all(uit_invite_id, personalization, template):
    send_gc_notify_email(
        personalization["email"],
        template,
        personalization,
        email_contact_if_fail=personalization[
            "billing_contact_info"
        ],  # this is the party to recieve the email of failed delivery
        first_name=personalization["first_name"],
        last_name=personalization["last_name"],
        uit_invite_id=uit_invite_id,
    )


def resend_already_assigned_test_to_administer(personalization):
    # print the code. For local debugging only
    print("******")
    print(
        "Assigned Test Specs ID: ",
        personalization["id"],
    )
    print("******")
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.RESEND_ALREADY_ASSIGNED_TEST_TO_ADMINISTER,
        personalization,
        email_contact_if_fail=personalization["hr_email"],
        assessment_process_id=personalization["assessment_process_id"],
    )


def send_new_assigned_test_to_administer(personalization):
    # print the code. For local debugging only
    print("******")
    print(
        "Assigned Test Specs ID: ",
        personalization["id"],
    )
    print("******")
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.SEND_NEW_ASSIGNED_TEST_TO_ADMINISTER,
        personalization,
        email_contact_if_fail=personalization["hr_email"],
        assessment_process_id=personalization["assessment_process_id"],
    )


def send_assigned_test_to_administer_failed_delivery(personalization):
    # print the code. For local debugging only
    print("******")
    print(
        "Failed Delivery Email: ",
        personalization["candidate_email"],
    )
    print("******")
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.FAILED_SEND_ASSIGNED_TEST_TO_ADMINISTER_DELIVERY,
        personalization,
    )


def revoke_assigned_test_to_administer(personalization):
    # print the code. For local debugging only
    print("******")
    print(
        "Assigned Test Specs ID: ",
        personalization["id"],
    )
    print("******")
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.REVOKE_ASSIGNED_TEST_TO_ADMINISTER,
        personalization,
    )


def send_update_active_assessment_process_data_email(personalization):
    # print the code. For local debugging only
    print("******")
    print(
        "Assigned Test Specs ID: ",
        personalization["id"],
    )
    print("******")
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.UPDATE_ACTIVE_ASSESSMENT_PROCESS_DATA,
        personalization,
    )


def send_test_session_cancellation_based_on_assessment_process_data_updates(
    personalization,
):
    # print the code. For local debugging only
    print("******")
    print(
        "Assigned Test Specs ID: ",
        personalization["id"],
    )
    print("******")
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.TEST_SESSION_CANCELLATION_BASED_ON_ASSESSMENT_PROCESS_DATA_UPDATES,
        personalization,
    )


def send_test_session_cancellation_related_data_to_hr_coordinator(
    personalization,
):
    # print the code. For local debugging only
    print("******")
    print(
        "Assessment Process ID: ",
        personalization["id"],
    )
    print(
        "User ID: ",
        personalization["user_id"],
    )
    print("******")
    send_gc_notify_email(
        personalization["user_email"],
        GCNotifyTemplateCodenames.TEST_SESSION_CANCELLATION_INFORM_RELATED_HR_COORDINATOR,
        personalization,
    )


# send notification to Test Center Coordinator if list of candidates was not delivered to security desk
def failed_send_test_session_candidates_to_security(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.FAILED_SEND_TEST_SESSION_CANDIDATES_TO_SECURITY,
        personalization,
    )


# create a list of email address to send the message to
def send_acc_email_list(
    detail, template, send_candidate=False, send_tcm=False, send_hr_ta=False
):
    from backend.custom_models.test_center_associated_managers import (
        TestCenterAssociatedManagers,
    )
    from user_management.user_management_models.user_models import User
    from backend.views.utils import (
        get_month_as_a_word,
    )

    # this will be made up of [email, first_name, last_name]'s for each contact who needs to recieve this email
    elist = []

    if send_candidate:  # if the candidate should be notified
        elist.append(
            [
                detail.user_email,
                detail.user_first_name,
                detail.user_last_name,
            ]
        )

    if send_hr_ta:
        if (
            detail.primary_contact_user_email is not None
        ):  # if there is an hr or ta email address
            elist.append(
                [
                    detail.primary_contact_user_email,
                    detail.primary_contact_user_first_name,
                    detail.primary_contact_user_last_name,
                ]
            )

    if send_tcm:
        # get the test center managers' email(s)
        tcam_ids = TestCenterAssociatedManagers.objects.filter(
            test_center_id=detail.test_center_id
        ).values_list("user_id", flat=True)
        users = User.objects.filter(id__in=tcam_ids)
        # for each test center manager, as there can be many
        for user in users:
            elist.append([user.email, user.first_name, user.last_name])

    data = vars(detail)  # convert the detail object into a dict
    # Drop theses
    data.pop("_state")
    data.pop("modify_date")
    data.pop("user_dob")
    # dropping dept_id since it's returned as a Decimal (not allowed by the serializer)
    data.pop("dept_id")

    # convert datetimes to day/month/year variables (with fr/en months)
    created_date = data.pop("created_date")
    process_end_date = data.pop("process_end_date")
    [created_date_year, created_date_month, created_date_day] = created_date.strftime(
        "%Y-%m-%d"
    ).split("-")
    [process_end_date_year, process_end_date_month, process_end_date_day] = (
        process_end_date.strftime("%Y-%m-%d").split("-")
    )
    cdm = get_month_as_a_word(created_date_month)
    pedm = get_month_as_a_word(process_end_date_month)
    data["created_date_year"] = created_date_year
    data["created_date_month_en"] = cdm["en"]
    data["created_date_month_fr"] = cdm["fr"]
    data["created_date_day"] = created_date_day
    data["process_end_date_year"] = process_end_date_year
    data["process_end_date_month_en"] = pedm["en"]
    data["process_end_date_month_fr"] = pedm["fr"]
    data["process_end_date_day"] = process_end_date_day

    # send the email for each recipient
    for [email, first_name, last_name] in elist:
        # add first and last name to the data
        # NOTE: we do not need to copy data for each call, as this will simply be overwritten each time
        data["first_name"] = first_name
        data["last_name"] = last_name

        send_gc_notify_email(
            email,
            template,
            data,
        )


# send notification to Test Center Coordinator if list of candidates was not delivered to security desk
def new_accomodations_request_send_all(
    personalization,
):
    send_acc_email_list(
        personalization,
        GCNotifyTemplateCodenames.NEW_ACCOMODATIONS_REQUEST_HR,
        send_hr_ta=True,
    )


# send the same email to all required recipients
def canceled_accomodations_request_send_all(personalization):
    send_acc_email_list(
        personalization,
        GCNotifyTemplateCodenames.CANCELED_ACCOMODATIONS_REQUEST,
        send_candidate=True,
        send_tcm=True,
        send_hr_ta=True,  # confirm if we send to HR/TA
    )


# send the same email to all required recipients
def canceled_accomodations_request_candidate_send_all(personalization):

    send_acc_email_list(
        personalization,
        GCNotifyTemplateCodenames.CANCELED_ACCOMODATIONS_REQUEST_CANDIDATE,
        send_tcm=True,
        send_hr_ta=True,
    )


# send the same email to all required recipients
def completed_accomodations_request_send_all(personalization):
    send_acc_email_list(
        personalization,
        GCNotifyTemplateCodenames.COMPLETED_ACCOMODATIONS_REQUEST,
        send_candidate=True,
        send_tcm=True,
        send_hr_ta=True,  # confirm if we send to HR/TA
    )


# send the same email to all required recipients
def re_opened_accomodations_request_send_all(personalization):
    send_acc_email_list(
        personalization,
        GCNotifyTemplateCodenames.RE_OPENED_ACCOMODATIONS_REQUEST,
        send_candidate=True,
        send_tcm=True,
        send_hr_ta=True,  # confirm if we send to HR/TA
    )


# send the email to all required recipients
def expired_accommodation_request_send_all(personalization):
    send_acc_email_list(
        personalization,
        GCNotifyTemplateCodenames.EXPIRED_ACCOMMODATION_REQUEST,
        send_candidate=True,
        send_tcm=True,
        send_hr_ta=True,  # confirm if we send to HR/TA
    )


# send notification to candidate when the accommodations request is now pending approval (complete on AAE side)
def send_accommodations_request_pending_approval_email(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.PENDING_APPROVAL_ACCOMODATIONS_REQUEST,
        personalization,
    )


# send 2 day before reminder
def two_day_reminder(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.TWO_DAY_REMINDER,
        personalization,
    )


# send 7 day before reminder
def seven_day_reminder(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.SEVEN_DAY_REMINDER,
        personalization,
    )


# notify candidate that they withdrew from their own test
def candidate_withdrawl_notification(
    email,
    personalization,
):
    send_gc_notify_email(
        email,
        GCNotifyTemplateCodenames.CANDIDATE_WITHDRAWL_NOTIFICATION,
        personalization,
    )


# inform candidate that HR canceled their test
def hr_triggered_withdraw(
    email,
    personalization,
):
    send_gc_notify_email(
        email,
        GCNotifyTemplateCodenames.HR_TRIGGERED_WITHDRAW,
        personalization,
    )


# inform candidate that they were bumped for someone more worthy
def test_bumped_by_tc(
    email,
    personalization,
):
    send_gc_notify_email(
        email,
        GCNotifyTemplateCodenames.TEST_BUMPED_BY_TC,
        personalization,
    )


# inform candidate that they booked their ola later... cuz this is totally different from the other type....
def ola_code_book_later(
    email,
    personalization,
):
    send_gc_notify_email(
        email,
        GCNotifyTemplateCodenames.OLA_CODE_BOOK_LATER,
        personalization,
    )


# inform candidate that they booked their ola... cuz this is totally different from the other type....
def ola_code_booking(
    email,
    personalization,
):
    send_gc_notify_email(
        email,
        GCNotifyTemplateCodenames.OLA_CODE_BOOKING,
        personalization,
    )


# inform candidate that they have been assigned to an in-person accommodated OLA test session
def ola_invite_candidate_to_non_standard_test_session_in_person(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.OLA_INVITE_CANDIDATE_TO_NON_STANDARD_TEST_SESSION_IN_PERSON,
        personalization,
    )


# inform candidate that they have been assigned to a virtual accommodated OLA test session
def ola_invite_candidate_to_non_standard_test_session_virtual(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.OLA_INVITE_CANDIDATE_TO_NON_STANDARD_TEST_SESSION_VIRTUAL,
        personalization,
    )


# inform candidate that their assigned in-person accommodated OLA test session has been modified
def ola_update_non_standard_test_session_invitation_in_person(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.OLA_UPDATE_NON_STANDARD_TEST_SESSION_INVITE_IN_PERSON,
        personalization,
    )


# inform candidate that their assigned virtual accommodated OLA test session has been modified
def ola_update_non_standard_test_session_invitation_virtual(
    personalization,
):
    send_gc_notify_email(
        personalization["email"],
        GCNotifyTemplateCodenames.OLA_UPDATE_NON_STANDARD_TEST_SESSION_INVITE_VIRTUAL,
        personalization,
    )


# function to send emails via GC notify, given the email, template codename, and the personalization
# the email_contact_if_fail denotes who to contact in case of a failure and is optional
# uit_invite_id
# assessment_process_id links it back to a bulk AssessmentProcess (if applicable) and is optional
# the bulk links will be used to notiy the email_contact_if_fail of any failed deliveries AFTER all of them have attempted delivery (thus one email per bulk)
def send_gc_notify_email(
    email,
    template_codename,
    personalization,
    email_contact_if_fail=None,
    first_name="",
    last_name="",
    uit_invite_id=None,
    assessment_process_id=None,
):
    # import here as these can occur within a celery task
    from rest_framework.response import Response
    from backend.custom_models.keyring import Keyring
    from backend.custom_models.gc_notify_template import GCNotifyTemplate
    from backend.custom_models.gc_notify_email_pending_delivery import (
        GCNotifyEmailPendingDelivery,
    )
    from django.conf import settings

    key = Keyring.objects.filter(codename=Key.GC_NOTIFY).last()

    # catch if GC Notify is not enabled and immedately return
    if key is None:
        return

    key = key.key
    # the GCNotifyTemplate object
    gc_notify_template = GCNotifyTemplate.objects.get(codename=template_codename)

    # The template id, what GC Notify calls the unique hash for the template
    template_id = gc_notify_template.template_id
    body = {
        "email_address": email,
        "template_id": template_id,
        "personalisation": personalization,
    }
    body = json.dumps(body)
    headers = {
        "Authorization": key,
        "Accept": "application/json",
        "Content-Type": "application/json",
    }
    # get the response from the URL
    response = requests.post(
        "https://api.notification.canada.ca/v2/notifications/email",
        headers=headers,
        data=body,
    )
    # NOTE: keep these prints for debugging purposes and for local development
    response = response.json()
    # this will print the full error message if there is one
    if settings.IS_LOCAL:
        print("***RESPONSE FOR DEBUGGING***")
        print(response)
    content = response["content"]
    # Only print the email body if this is local, otherwise it clutters the log
    if settings.IS_LOCAL:
        print("***START EMAIL CONTENT***")
        print(email)
        print(content["from_email"])
        print(content["subject"])
        print(content["body"])
        print("***END EMAIL CONTENT***")
    email_id = response["id"]
    # create new GCNotifyEmailPendingDelivery object; these will be checked later
    GCNotifyEmailPendingDelivery.objects.create(
        gc_notify_email_id=email_id,
        gc_notify_template_id=gc_notify_template.id,
        email_to=email,
        email_contact_if_fail=email_contact_if_fail,
        first_name_to=first_name,
        last_name_to=last_name,
        uit_invite_id=uit_invite_id,
        assessment_process_id=assessment_process_id,
        email_subject=content["subject"],
        email_body=content["body"],
    )

    return Response({"OK", "OK"}, status=status.HTTP_200_OK)
