from operator import itemgetter
import datetime
import pytz
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework import status
from backend.static.test_skill_codenames import TestSkillTypeCodename
from backend.views.utils import get_user_info_from_jwt_token
from cms.views.utils import get_optional_parameters
from cms.cms_models.test_skill_type import TestSkillType
from cms.cms_models.test_skill import TestSkill
from cms.cms_models.test_skill_sle import TestSkillSLE
from cms.cms_models.test_skill_occupational import TestSkillOccupational
from db_views.db_view_models.scored_tests_vw import ScoredTestsVW


class IsWithinTheRetestPeriod(APIView):
    def get(self, request):
        user_info = get_user_info_from_jwt_token(request)

        parameters = get_optional_parameters(
            [
                "test_id",
                "test_skill_type_id",
                "test_skill_sub_type_id",
                "test_session_start_time",
            ],
            request,
        )
        test_id, test_skill_type_id, test_skill_sub_type_id, test_session_start_time = (
            itemgetter(
                "test_id",
                "test_skill_type_id",
                "test_skill_sub_type_id",
                "test_session_start_time",
            )(parameters)
        )

        # initializing is_within_the_retest_period
        is_within_the_retest_period = False

        # test_id is provided (usually called from Take a Test - Start/Resume test)
        if test_id is not None and test_id != "":
            # getting test skill type data
            test_skill_type_data = TestSkill.objects.get(test_definition_id=test_id)

            # setting custom_test_skill_type_id
            custom_test_skill_type_id = test_skill_type_data.test_skill_type_id

            # getting test skill type codename
            test_skill_type_codename = TestSkillType.objects.get(
                id=custom_test_skill_type_id
            ).codename

            # getting/setting custom_test_skill_sub_type_id
            custom_test_skill_sub_type_id = None
            # SLE test skill
            if test_skill_type_codename == TestSkillTypeCodename.SLE:
                custom_test_skill_sub_type_id = TestSkillSLE.objects.get(
                    test_skill_id=test_skill_type_data.id
                ).test_skill_sle_desc_id

            # Occupational test skill
            if test_skill_type_codename == TestSkillTypeCodename.OCC:
                custom_test_skill_sub_type_id = TestSkillOccupational.objects.get(
                    test_skill_id=test_skill_type_data.id
                ).test_skill_occupational_desc_id

        # test_skill_type_id is provided (usually called from Reservation - Book Test Session)
        elif test_skill_type_id is not None and test_skill_type_id != "":
            # setting test_skill_type_id
            custom_test_skill_type_id = test_skill_type_id
            # setting custom_test_skill_sub_type_id
            custom_test_skill_sub_type_id = test_skill_sub_type_id

        # non supported parameters (should never happen)
        else:
            return Response(
                {"error": "The provided parameters are not supported"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        # getting test skill type codename
        custom_test_skill_type_codename = TestSkillType.objects.get(
            id=custom_test_skill_type_id
        ).codename

        # test skill type is not NONE
        if custom_test_skill_type_codename != TestSkillTypeCodename.NONE:
            # getting most recent submitted/quit test with the same test_skill_type_id and test_skill_sub_type_id
            skills_related_tests = ScoredTestsVW.objects.filter(
                candidate_user_id=user_info["user_id"],
                test_skill_type_id=custom_test_skill_type_id,
                test_skill_sub_type_id=custom_test_skill_sub_type_id,
            ).order_by("submit_date", "start_date")

            # at least one matching test
            if skills_related_tests:
                # getting the calculated retest period date ("calculated" here means that the view is handling some logic on the restest period)
                calculated_retest_period_date_str = (
                    skills_related_tests.last().calculated_retest_period_date
                )
                # making sure that the calculated_retest_period_date_str is defined
                if calculated_retest_period_date_str is not None:
                    # converting date string to date
                    calculated_retest_period_date = datetime.datetime.strptime(
                        calculated_retest_period_date_str, "%Y-%m-%d"
                    ).date()
                    # called from Reservation (need to compare retest date with test session date)
                    if test_session_start_time is not None:
                        # converting date string to date
                        converted_test_session_start_time = datetime.datetime.strptime(
                            test_session_start_time, "%Y-%m-%dT%H:%M:%S%z"
                        )
                        # converting test session start time to UTC test session start date
                        utc_test_session_start_time = (
                            converted_test_session_start_time.astimezone(pytz.utc)
                        )
                        utc_test_session_start_date = utc_test_session_start_time.date()

                        # current date is less than the calculated_retest_period_date
                        if utc_test_session_start_date < calculated_retest_period_date:
                            # setting is_within_the_retest_period to True
                            is_within_the_retest_period = True
                    # called from take a test (need to compare retest date with current date)
                    else:
                        # getting current date
                        current_date = datetime.date.today()
                        # current date is less than the calculated_retest_period_date
                        if current_date < calculated_retest_period_date:
                            # setting is_within_the_retest_period to True
                            is_within_the_retest_period = True

        return Response(is_within_the_retest_period, status=status.HTTP_200_OK)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
