from rest_framework import permissions
from rest_framework.views import APIView

from backend.views.billing_contact import (
    get_all_billing_contacts,
    get_found_billing_contacts,
    create_billing_contact,
    modify_billing_contact,
    delete_billing_contact,
)

from backend.api_permissions.role_based_api_permissions import (
    HasHRCoordinatorPermission,
)


# create a billing contact for current HR Coordinator user
class CreateBillingContact(APIView):
    def post(self, request):
        return create_billing_contact(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# modify a billing contact for current HR Coordinator user
class ModifyBillingContact(APIView):
    def post(self, request):
        return modify_billing_contact(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# delete a billing contact for current HR Coordinator user
class DeleteBillingContact(APIView):
    def post(self, request):
        return delete_billing_contact(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# getting all billing contacts for current HR Coordinator user
class GetAllBillingContacts(APIView):
    def get(self, request):
        return get_all_billing_contacts(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# getting all found billing contacts for current HR Coordinator user
class GetFoundBillingContacts(APIView):
    def get(self, request):
        return get_found_billing_contacts(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]
