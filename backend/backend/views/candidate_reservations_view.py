import json
import pytz
from django.db.models import Q
from datetime import date, datetime, timedelta
from operator import itemgetter
from rest_framework import status
from rest_framework import permissions
from rest_framework.response import Response
from backend.custom_models.user_accommodation_file import UserAccommodationFile
from db_views.db_view_models.assessment_process_vw import AssessmentProcessVW
from backend.static.ola_global_configs_const import OlaGlobalConfigsConst
from backend.custom_models.ola_global_configs import OlaGlobalConfigs
from backend.static.reason_for_testing import ReasonForTestingPriorityCodenameConst
from backend.custom_models.reason_for_testing import ReasonForTesting
from backend.custom_models.reason_for_testing_priority import ReasonForTestingPriority
from db_views.db_view_models.assessment_process_assigned_test_specs_vw import (
    AssessmentProcessAssignedTestSpecsVW,
)
from backend.static.test_skill_codenames import (
    TestSkillTypeCodename,
    TestSkillSLEDescCodename,
)
from backend.custom_models.test_center_field_translations.test_center_address_text import (
    TestCenterAddressText,
)
from backend.custom_models.test_center_field_translations.test_center_city_text import (
    TestCenterCityText,
)
from backend.custom_models.test_center_field_translations.test_center_province_text import (
    TestCenterProvinceText,
)
from db_views.serializers.test_center_serializers import (
    TestCenterAddressTextSerializer,
    TestCenterCityTextSerializer,
    TestCenterOlaAvailableTimeSlotsViewSerializer,
    TestCenterProvinceTextSerializer,
)
from backend.views.utils import (
    get_user_info_from_jwt_token,
    create_language_compatible_object,
)
from rest_framework.views import APIView
from user_management.user_management_models.user_models import User
from cms.views.utils import get_needed_parameters
from cms.cms_models.test_skill_sle_desc import TestSkillSLEDesc
from db_views.db_view_models.candidate_reservations_vw import (
    CandidateReservationsVW,
)
from db_views.db_view_models.booked_reservations_vw import (
    BookedReservationsVW,
)
from db_views.db_view_models.test_session_count_vw import (
    TestSessionCountVW,
)
from db_views.db_view_models.test_center_test_sessions_vw import (
    TestCenterTestSessionsVW,
)

from db_views.serializers.test_center_serializers import (
    TestCenterTestSessionsViewSerializer,
)
from db_views.db_view_models.test_center_ola_available_time_slots_vw import (
    TestCenterOlaAvailableTimeSlotsVw,
)
from backend.custom_models.test_center_ola_configs import TestCenterOlaConfigs
from backend.custom_models.test_center_ola_time_slot import TestCenterOlaTimeSlot
from backend.custom_models.reservation_codes import ReservationCodes
from backend.custom_models.consumed_reservation_codes import ConsumedReservationCodes
from backend.custom_models.consumed_reservation_code_status import (
    ConsumedReservationCodeStatus,
)
from backend.custom_models.consumed_reservation_code_status_text import (
    ConsumedReservationCodeStatusText,
)
from backend.custom_models.test_center_test_session_data import (
    TestCenterTestSessionData,
)
from backend.static.consumed_reservation_code_statuses import (
    StaticConsumedReservationCodeStatus,
)
from backend.custom_models.test_center_rooms import TestCenterRooms
from backend.custom_models.test_center import TestCenter
from backend.custom_models.test_center_test_sessions import TestCenterTestSessions
from backend.celery.tasks import (
    send_reservation_code_withdraw_no_booking_email_celery_task,
    send_reservation_code_withdraw_from_booked_session_email_celery_task,
    send_reservation_code_booking_email_celery_task,
    send_reservation_code_book_later_email_celery_task,
)
from backend.static.languages import Language_id
from backend.views.aae import close_user_accommodation_request
from backend.views.virtual_meetings_views import cancel_virtual_meeting
from backend.views.gc_notify_view import (
    candidate_withdrawl_notification,
    reservation_code_withdraw_from_booked_session,
    ola_code_booking,
    ola_code_book_later,
)
from backend.custom_models.assessment_process_assigned_test_specs import (
    AssessmentProcessAssignedTestSpecs,
)
from backend.custom_models.assessment_process import AssessmentProcess


# reserve the actual code
class ReserveAndVerifyReservationCode(APIView):
    def post(self, request):
        return reserve_and_verify_reservation_code(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# TODO optimize this?
def reserve_and_verify_reservation_code(request):
    user_info = get_user_info_from_jwt_token(request)
    user_id = user_info["user_id"]
    success, parameters = get_needed_parameters(
        ["reservation_code"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    reservation_code = itemgetter("reservation_code")(parameters)

    # Get the reervation code
    reservation_code_obj = ReservationCodes.objects.filter(
        reservation_code=reservation_code
    ).last()

    consumed_reservation_code = ConsumedReservationCodes.objects.filter(
        reservation_code=reservation_code, candidate_id=user_id
    ).last()

    reservation_code_details = CandidateReservationsVW.objects.filter(
        reservation_code=reservation_code
    ).last()

    dne_or_expired_response = Response(
        {"error": "the specified reservation code does not exist or has expired"},
        status=status.HTTP_404_NOT_FOUND,
    )

    if not reservation_code_obj:
        # if the code does not exist in either table it is invalid or claimed by another user
        if not consumed_reservation_code:
            return dne_or_expired_response
        # if it has already been claimed by the current user
        elif consumed_reservation_code:
            return Response(
                {"error": "the user has already claimed this code"},
                status=status.HTTP_409_CONFLICT,
            )
    # if the code has not been claimed and does exist; otherwise will have returned

    # get the reserved and expired status id's
    expired_status_id = ConsumedReservationCodeStatus.objects.get(
        codename=StaticConsumedReservationCodeStatus.EXPIRED
    ).id
    reserved_status_id = ConsumedReservationCodeStatus.objects.get(
        codename=StaticConsumedReservationCodeStatus.RESERVED
    ).id

    # check if the code has expired and set the correct id
    booking_time = datetime.now(pytz.utc).date()
    status_id = reserved_status_id
    if booking_time > reservation_code_details.assessment_process_closing_date:
        status_id = expired_status_id

    # create a new Consumed ReservationCode, delete the old ReservationCode
    ConsumedReservationCodes.objects.create(
        assessment_process_assigned_test_specs_id=reservation_code_obj.assessment_process_assigned_test_specs_id,
        status_id=status_id,
        reservation_code=reservation_code,
        candidate_id=user_id,
        test_session_id=None,
        modify_date=booking_time,
    )
    # delete the reservation code object
    reservation_code_obj.delete()

    # if it was expired, return a 404 as if it had expired
    if status_id == expired_status_id:
        return dne_or_expired_response

    # otherwise return success
    return Response({"OK": "OK"}, status=status.HTTP_200_OK)


# TODO overhaul the following


# confirm the reservation code is actually valid
class VerifyReservationCode(APIView):
    def post(self, request):
        return verify_reservation_code(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def verify_reservation_code(request):
    user_info = get_user_info_from_jwt_token(request)
    user_id = user_info["user_id"]
    success, parameters = get_needed_parameters(
        ["reservation_code"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    reservation_code = itemgetter("reservation_code")(parameters)

    # check if the code has already been consumed
    already_consumed_reservation_code = ConsumedReservationCodes.objects.filter(
        reservation_code=reservation_code
    ).last()  # if booked, withdrawn, complete

    # if the code is consumed, then it is booked, withdrawn, reserved or completed.
    if already_consumed_reservation_code:
        # check if it is reserved, and get the relevant data
        reserved_status_id = ConsumedReservationCodeStatus.objects.get(
            codename=StaticConsumedReservationCodeStatus.RESERVED
        ).id

        # if it belongs to a differnent user than throw a DNE
        if already_consumed_reservation_code.candidate_id != user_id:
            return Response(
                {"error": "the specified reservation code does not exist"},
                status=status.HTTP_404_NOT_FOUND,
            )
        # if it is withdraw, booked, completed, or belongs to another user then they cannot retrieve it
        elif already_consumed_reservation_code.status_id != reserved_status_id:
            return Response(
                {"error": "the specified reservation code was already used"},
                status=status.HTTP_409_CONFLICT,
            )
        # if it comes this far, then it is reserved for the current user

    reservation_code_obj = CandidateReservationsVW.objects.filter(
        reservation_code=reservation_code
    ).last()

    # if the object does not exist (and it was not consumed), then this was never a valid code
    if not reservation_code_obj:
        return Response(
            {"error": "the specified reservation code does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )

    return Response(status=status.HTTP_200_OK)


# find all sessions available for a given reservation code
class FindSessionsForReservationCode(APIView):
    def get(self, request):
        return find_sessions_for_reservation_code(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def find_sessions_for_reservation_code(request):
    user_info = get_user_info_from_jwt_token(request)
    user_id = user_info["user_id"]
    success, parameters = get_needed_parameters(
        [
            "reservation_code",
            "test_skill_type_codename",
            "test_skill_sub_type_codename",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    reservation_code, test_skill_type_codename, test_skill_sub_type_codename = (
        itemgetter(
            "reservation_code",
            "test_skill_type_codename",
            "test_skill_sub_type_codename",
        )(parameters)
    )

    # OLA Test
    if test_skill_type_codename == TestSkillTypeCodename.SLE and (
        test_skill_sub_type_codename == TestSkillSLEDescCodename.ORAL_EN
        or test_skill_sub_type_codename == TestSkillSLEDescCodename.ORAL_FR
    ):
        # getting related reservation code data
        related_reservation_code_data = ConsumedReservationCodes.objects.get(
            reservation_code=reservation_code
        )
        # getting process validity end date
        assessment_process_assigned_test_specs_data = AssessmentProcessAssignedTestSpecsVW.objects.get(
            id=related_reservation_code_data.assessment_process_assigned_test_specs_id
        )
        # getting language of request
        language_of_request = Language_id.EN
        # skill sub-type codename is OF
        if test_skill_sub_type_codename == TestSkillSLEDescCodename.ORAL_FR:
            language_of_request = Language_id.FR

        # getting priority level codename of the respective reason for testing
        reason_for_testing_data = ReasonForTesting.objects.get(
            id=assessment_process_assigned_test_specs_data.reason_for_testing_id
        )
        priority_level_codename = ReasonForTestingPriority.objects.get(
            id=reason_for_testing_data.reason_for_testing_priority_id
        ).codename

        # ========== GETTING NEEDED DATES ==========
        # in "last minute cancellation window" hours date
        last_minute_open_booking_window_value = OlaGlobalConfigs.objects.get(
            codename=OlaGlobalConfigsConst.OPEN_BOOKING_WINDOW
        ).value
        in_last_minute_open_booking_window = date.today() + timedelta(
            days=last_minute_open_booking_window_value
        )
        # getting initial process sent date
        initial_process_sent_date = datetime.strptime(
            AssessmentProcessVW.objects.get(
                id=assessment_process_assigned_test_specs_data.assessment_process_id
            ).sent_date,
            "%Y-%m-%d",
        ).date()
        # initial process sent date + reason for testing waiting_period
        initial_process_sent_date_plus_waiting_period = (
            initial_process_sent_date
            + timedelta(days=reason_for_testing_data.waiting_period)
        )
        # initial process sent date + reason for testing minimum_process_length
        initial_process_sent_date_plus_minimum_process_length = (
            initial_process_sent_date
            + timedelta(days=reason_for_testing_data.minimum_process_length)
        )
        # ========== GETTING NEEDED DATES (END) ==========

        # initializing all_ola_available_time_slots
        all_ola_available_time_slots = []

        # HIGH PRIORITY
        if priority_level_codename == ReasonForTestingPriorityCodenameConst.HIGH:
            # getting all future OLA available time slots
            all_ola_available_time_slots = TestCenterOlaAvailableTimeSlotsVw.objects.filter(
                (
                    # date <= in_last_minute_open_booking_window
                    Q(date__lte=in_last_minute_open_booking_window)
                    # date <= initial_process_sent_date_plus_minimum_process_length
                    & Q(date__lte=initial_process_sent_date_plus_minimum_process_length)
                    # remaining_available_test_assessors_total > 0
                    & Q(remaining_available_test_assessors_total__gt=0)
                    # matching respective language ID
                    & Q(assessed_language_id=language_of_request)
                )
                | (
                    # date >= initial_process_sent_date_plus_waiting_period
                    Q(date__gte=initial_process_sent_date_plus_waiting_period)
                    # date <= initial_process_sent_date_plus_minimum_process_length
                    & Q(date__lte=initial_process_sent_date_plus_minimum_process_length)
                    # high priority only
                    & Q(remaining_available_test_assessors_per_date_high__gt=0)
                    # remaining_available_test_assessors_total > 0
                    & Q(remaining_available_test_assessors_total__gt=0)
                    # matching respective language ID
                    & Q(assessed_language_id=language_of_request)
                )
            )
        # MEDIUM PRIORITY
        elif priority_level_codename == ReasonForTestingPriorityCodenameConst.MEDIUM:
            # getting all future OLA available time slots
            all_ola_available_time_slots = TestCenterOlaAvailableTimeSlotsVw.objects.filter(
                (
                    # date <= in_last_minute_open_booking_window
                    Q(date__lte=in_last_minute_open_booking_window)
                    # date <= initial_process_sent_date_plus_minimum_process_length
                    & Q(date__lte=initial_process_sent_date_plus_minimum_process_length)
                    # remaining_available_test_assessors_total > 0
                    & Q(remaining_available_test_assessors_total__gt=0)
                    # matching respective language ID
                    & Q(assessed_language_id=language_of_request)
                )
                | (
                    # date >= initial_process_sent_date_plus_waiting_period
                    Q(date__gte=initial_process_sent_date_plus_waiting_period)
                    # date <= initial_process_sent_date_plus_minimum_process_length
                    & Q(date__lte=initial_process_sent_date_plus_minimum_process_length)
                    # medium priority only
                    & Q(remaining_available_test_assessors_per_date_medium__gt=0)
                    # remaining_available_test_assessors_total > 0
                    & Q(remaining_available_test_assessors_total__gt=0)
                    # matching respective language ID
                    & Q(assessed_language_id=language_of_request)
                )
            )
        # LOW PRIORITY
        elif priority_level_codename == ReasonForTestingPriorityCodenameConst.LOW:
            # getting all future OLA available time slots
            all_ola_available_time_slots = TestCenterOlaAvailableTimeSlotsVw.objects.filter(
                (
                    # date <= in_last_minute_open_booking_window
                    Q(date__lte=in_last_minute_open_booking_window)
                    # date <= initial_process_sent_date_plus_minimum_process_length
                    & Q(date__lte=initial_process_sent_date_plus_minimum_process_length)
                    # remaining_available_test_assessors_total > 0
                    & Q(remaining_available_test_assessors_total__gt=0)
                    # matching respective language ID
                    & Q(assessed_language_id=language_of_request)
                )
                | (
                    # date >= initial_process_sent_date_plus_waiting_period
                    Q(date__gte=initial_process_sent_date_plus_waiting_period)
                    # date <= initial_process_sent_date_plus_minimum_process_length
                    & Q(date__lte=initial_process_sent_date_plus_minimum_process_length)
                    # low priority only
                    & Q(remaining_available_test_assessors_per_date_low__gt=0)
                    # remaining_available_test_assessors_total > 0
                    & Q(remaining_available_test_assessors_total__gt=0)
                    # matching respective language ID
                    & Q(assessed_language_id=language_of_request)
                )
            )

        # serializing data
        serialized_data = TestCenterOlaAvailableTimeSlotsViewSerializer(
            all_ola_available_time_slots, many=True
        ).data

        # getting sorted serialized data by start time (ascending)
        ordered_serialized_data = sorted(
            serialized_data,
            key=lambda k: (k["date"], k["start_time"]),
            reverse=False,
        )

        final_obj = {}
        final_obj["data"] = ordered_serialized_data
        final_obj["is_ola"] = True

        return Response(final_obj)
    # Other Test
    else:
        reservation_code_obj = CandidateReservationsVW.objects.filter(
            reservation_code=reservation_code
        ).last()

        # if the object does not exist (and it was not consumed), then this was never a valid code
        if not reservation_code_obj:
            return Response(
                {"error": "the specified reservation code does not exist"},
                status=status.HTTP_404_NOT_FOUND,
            )

        booking_time = datetime.now(pytz.utc)

        valid_test_sessions = None

        # get all test sessions that have:
        # same test_skill_type_id,
        # same test_skill_sub_type_id,
        # has more than 0 unbooked spaces,
        # has not passed the booking time - booking delay
        # and session is not past the closing date

        # If the candidate is allowed to book from other test centers
        if reservation_code_obj.allow_booking_external_tc:
            valid_test_sessions = TestCenterTestSessionsVW.objects.filter(
                # the following is the same in the else
                test_skill_type_id=reservation_code_obj.test_skill_type_id,
                test_skill_sub_type_id=reservation_code_obj.test_skill_sub_type_id,
                spaces_unbooked__gt=0,
                latest_booking_modification_time__gte=booking_time,
                # adding 1 day to the comparison, since the assessment_process_closing_date is considered as the beginning of the day (should be considered at the end)
                end_time__lt=reservation_code_obj.assessment_process_closing_date
                + timedelta(days=1),
                # exclude mismatched department ids when the session is not open to OGDs
            ).exclude(~Q(department_id=reservation_code_obj.dept_id), open_to_ogd=False)

        # if the reservation code is linked to a process requiring the candidate to book within the same dept
        else:
            valid_test_sessions = TestCenterTestSessionsVW.objects.filter(
                # depatment of the session must match the one attached to the reservation code
                department_id=reservation_code_obj.dept_id,
                # the rest is the same in the if
                test_skill_type_id=reservation_code_obj.test_skill_type_id,
                test_skill_sub_type_id=reservation_code_obj.test_skill_sub_type_id,
                spaces_unbooked__gt=0,
                latest_booking_modification_time__gte=booking_time,
                # adding 1 day to the comparison, since the assessment_process_closing_date is considered as the beginning of the day (should be considered at the end)
                end_time__lt=reservation_code_obj.assessment_process_closing_date
                + timedelta(days=1),
            )
        serialized_data = TestCenterTestSessionsViewSerializer(
            valid_test_sessions, many=True
        ).data

        test_session_ids = (
            BookedReservationsVW.objects.filter(
                candidate_id=user_id,
                consumed_reservation_codename=StaticConsumedReservationCodeStatus.BOOKED,
            )
            .exclude(test_session_id=None)
            .values_list("test_session_id", flat=True)
        )

        valid_test_sessions = valid_test_sessions.exclude(id__in=test_session_ids)

        serialized_data = TestCenterTestSessionsViewSerializer(
            valid_test_sessions, many=True
        ).data

        # getting sorted serialized data by start time (ascending)
        ordered_test_sessions = sorted(
            serialized_data,
            key=lambda k: k["start_time"],
            reverse=False,
        )

        final_obj = {}
        final_obj["data"] = ordered_test_sessions
        final_obj["is_ola"] = False

        return Response(final_obj, status=status.HTTP_200_OK)


class WithdrawReservationCode(APIView):
    def post(self, request):
        return consume_reservation_code(
            request, StaticConsumedReservationCodeStatus.WITHDRAWN
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class BookReservationCode(APIView):
    def post(self, request):
        return consume_reservation_code(
            request, StaticConsumedReservationCodeStatus.BOOKED
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# handle book and withdraw actions
def consume_reservation_code(request, reservation_status):
    consumed_reservation_code_data = json.loads(request.body)
    new_test_session_id = False

    user_info = get_user_info_from_jwt_token(request)
    user_id = user_info["user_id"]
    success = True

    # if this is not a withdraw action, all params are needed;
    if not StaticConsumedReservationCodeStatus.WITHDRAWN and (
        consumed_reservation_code_data["reservation_code"] is None
        or consumed_reservation_code_data["test_session_id"] is None
    ):
        success = False

    # if it is a withdraw action, one of these two must be delared
    if (
        StaticConsumedReservationCodeStatus.WITHDRAWN
        and consumed_reservation_code_data["reservation_code"] is None
        and consumed_reservation_code_data["test_session_id"] is None
    ):
        success = False

    # if any of these combinations are missing, send a 400
    if not success:
        return Response(
            [
                consumed_reservation_code_data["reservation_code"],
                consumed_reservation_code_data["test_session_id"],
            ],
            status=status.HTTP_400_BAD_REQUEST,
        )

    test_session_id_for_email = consumed_reservation_code_data["test_session_id"]

    # try to look up the unconsumed reservation code
    reservation_code_obj = ReservationCodes.objects.filter(
        reservation_code=consumed_reservation_code_data["reservation_code"]
    ).last()

    # set these values to a default; get the date
    assessment_process_assigned_test_specs = None
    booked_date = date.today()
    current_crc_obj = (
        None  # this will be used ONLY if there is no reservation code object
    )

    # if is is not in the ReservationCodes table, look at the ConsumedReservationCodes; ONLY if it exists for the user
    if not reservation_code_obj:
        # if withdrawing from Make Reservation
        if consumed_reservation_code_data["reservation_code"] is not None:
            current_crc_obj = ConsumedReservationCodes.objects.filter(
                reservation_code=consumed_reservation_code_data["reservation_code"],
                candidate_id=user_id,
            ).last()
        # if withdrawing from Manage Reservation
        else:
            current_crc_obj = ConsumedReservationCodes.objects.filter(
                test_session_id=consumed_reservation_code_data["test_session_id"],
                candidate_id=user_id,
            ).last()

        # if withdrawing from Make Reservation with an accommodation request
        if current_crc_obj is None:
            current_crc_obj = ConsumedReservationCodes.objects.filter(
                id=consumed_reservation_code_data["test_session_id"],
                candidate_id=user_id,
            ).last()

        assessment_process_assigned_test_specs = (
            current_crc_obj.assessment_process_assigned_test_specs
        )

    # if the reservation code does exist
    else:
        assessment_process_assigned_test_specs = (
            reservation_code_obj.assessment_process_assigned_test_specs
        )

    # ensure that the current action does not violate the booking delay of the test center (if any)
    booked_time = datetime.now(pytz.utc)
    check = False
    earliest_booking = None

    # IF it is a withdraw or reserved status, we only want to check if it violates the OLD test session retest period
    if (
        reservation_status == StaticConsumedReservationCodeStatus.WITHDRAWN
        or reservation_status == StaticConsumedReservationCodeStatus.RESERVED
    ):
        # if there is only an unconsumed code, then we are done
        if reservation_code_obj:
            check = False
        # if the current session is set to reserved, then there is no delay period that can be violated
        elif (
            current_crc_obj.status.codename
            == StaticConsumedReservationCodeStatus.RESERVED
        ):
            check = False
        # otherwise check the current consumed reservation code
        # accommodation requests don't have a test date so no need to check
        if current_crc_obj.test_session_id is not None:
            # OLA Test
            if consumed_reservation_code_data["is_ola"]:
                # calling the booking delay validation
                respecting_booking_delay, greatest_booking_delay, earliest_booking = (
                    check_if_ola_booking_violates_booking_delay(
                        current_crc_obj.test_session_id
                    )
                )

                # not respecting booking delay
                if not respecting_booking_delay:
                    return Response(
                        {
                            "earliest_booking": earliest_booking
                            - timedelta(hours=greatest_booking_delay),
                            "error": "session violates booking delay",
                        },
                        status=status.HTTP_406_NOT_ACCEPTABLE,
                    )
            # Other Test
            else:
                check, earliest_booking = check_if_booking_violates_booking_delay(
                    booked_time, current_crc_obj.test_session_id
                )
        else:
            check = False
    elif current_crc_obj is not None:
        # accommodation requests don't have a test date so no need to check
        if current_crc_obj.test_session_id is not None:
            check, earliest_booking = check_if_booking_violates_booking_delay(
                booked_time, consumed_reservation_code_data["test_session_id"]
            )
        else:
            check = False
    # if this violates the booking delay of the test center (current or new), return a 406
    if check:
        return Response(
            {
                "error": "session violates booking delay",
                "earliest_booking": earliest_booking,
            },
            status=status.HTTP_406_NOT_ACCEPTABLE,
        )

    if (
        reservation_status == StaticConsumedReservationCodeStatus.RESERVED
        or reservation_status == StaticConsumedReservationCodeStatus.WITHDRAWN
    ):
        # WITHDRAWN ACTION (delete related test session data)
        if reservation_status == StaticConsumedReservationCodeStatus.WITHDRAWN:
            # if OLA test
            if consumed_reservation_code_data["is_ola"]:
                # checking if there is a related test session (standard or not)
                related_test_session = TestCenterTestSessions.objects.filter(
                    id=consumed_reservation_code_data["test_session_id"]
                )
                # if related_test_session exists
                if related_test_session:
                    # cancel the teams invitations
                    cancel_virtual_meeting(
                        user_id, consumed_reservation_code_data["test_session_id"]
                    )
            # other test
            else:
                # checking if there is a related test session (non standard session)
                related_test_session = TestCenterTestSessions.objects.filter(
                    id=consumed_reservation_code_data["test_session_id"], is_standard=0
                )
            # found related test session
            if related_test_session:
                # getting all linked test sessions (might have multiple linked test sessions)
                related_test_sessions = TestCenterTestSessions.objects.filter(
                    assessment_process_assigned_test_specs_id=related_test_session.last().assessment_process_assigned_test_specs_id
                )
                # looping in related_test_sessions
                for test_session in related_test_sessions:
                    # deleting test sessions related data
                    test_session_data_id = test_session.test_session_data_id
                    test_session.delete()
                    TestCenterTestSessionData.objects.get(
                        id=test_session_data_id
                    ).delete()
            # null out the user accommodation file ID
            current_crc_obj.user_accommodation_file_id = None
        # null out the test session if it is booked later (reserved)
        consumed_reservation_code_data["test_session_id"] = None

    # this will look up the appropriate status id for booked, withdrawn, or reserved
    code_status = ConsumedReservationCodeStatus.objects.get(codename=reservation_status)

    # Make sure there are enough seats available
    if reservation_status == StaticConsumedReservationCodeStatus.BOOKED:
        # OLA Test
        if consumed_reservation_code_data["is_ola"]:
            # respective date format
            # date_format = "%H:%M"
            date_format = "%Y-%m-%dT%H:%M:%S%z"

            # get and format provided start time
            utc_start_time = datetime.strptime(
                consumed_reservation_code_data["utc_start_time"],
                date_format,
            )
            # get and format provided end time
            utc_end_time = datetime.strptime(
                consumed_reservation_code_data["utc_end_time"],
                date_format,
            )

            # getting language of test
            test_skill_sle_desc = TestSkillSLEDesc.objects.get(
                id=consumed_reservation_code_data["test_skill_sub_type_id"]
            )
            # codename is "oe" (English test)
            if test_skill_sle_desc.codename == TestSkillSLEDescCodename.ORAL_EN:
                assessed_language_id = Language_id.EN
            # codename is "of" (French test)
            elif test_skill_sle_desc.codename == TestSkillSLEDescCodename.ORAL_FR:
                assessed_language_id = Language_id.FR
            # unsupported (should never happen)
            else:
                return Response(
                    {
                        "error": "unsupported test skill sub-type ID",
                    },
                    status=status.HTTP_406_NOT_ACCEPTABLE,
                )

            # getting assessment process assigned test specs data
            assessment_process_assigned_test_specs_data = (
                AssessmentProcessAssignedTestSpecsVW.objects.get(
                    id=current_crc_obj.assessment_process_assigned_test_specs_id
                )
            )

            # getting priority level codename of the respective reason for testing
            reason_for_testing_data = ReasonForTesting.objects.get(
                id=assessment_process_assigned_test_specs_data.reason_for_testing_id
            )
            priority_level_codename = ReasonForTestingPriority.objects.get(
                id=reason_for_testing_data.reason_for_testing_priority_id
            ).codename

            # ========== GETTING NEEDED DATES ==========
            # in "last minute cancellation window" hours date
            last_minute_open_booking_window_value = OlaGlobalConfigs.objects.get(
                codename=OlaGlobalConfigsConst.OPEN_BOOKING_WINDOW
            ).value
            in_last_minute_open_booking_window = date.today() + timedelta(
                days=last_minute_open_booking_window_value
            )
            # ========== GETTING NEEDED DATES (END) ==========

            # initializing time_slot_still_available
            time_slot_still_available = []

            # are we within the in_last_minute_open_booking_window
            if (
                datetime.strptime(
                    consumed_reservation_code_data["date"], "%Y-%m-%d"
                ).date()
                <= in_last_minute_open_booking_window
            ):
                # getting available time slot
                time_slot_still_available = TestCenterOlaAvailableTimeSlotsVw.objects.filter(
                    # matching date, start time and end time
                    date=consumed_reservation_code_data["date"],
                    start_time=utc_start_time.time(),
                    end_time=utc_end_time.time(),
                    # no priority
                    # remaining_available_test_assessors_total > 0
                    remaining_available_test_assessors_total__gt=0,
                    # matching respective language ID
                    assessed_language_id=assessed_language_id,
                )

            # HIGH PRIORITY
            elif priority_level_codename == ReasonForTestingPriorityCodenameConst.HIGH:
                # getting available time slot
                time_slot_still_available = TestCenterOlaAvailableTimeSlotsVw.objects.filter(
                    # matching date, start time and end time
                    date=consumed_reservation_code_data["date"],
                    start_time=utc_start_time.time(),
                    end_time=utc_end_time.time(),
                    # only high priority
                    remaining_available_test_assessors_per_date_high__gt=0,
                    # remaining_available_test_assessors_total > 0
                    remaining_available_test_assessors_total__gt=0,
                    # matching respective language ID
                    assessed_language_id=assessed_language_id,
                )
            # MEDIUM PRIORITY
            elif (
                priority_level_codename == ReasonForTestingPriorityCodenameConst.MEDIUM
            ):
                # getting available time slot
                time_slot_still_available = TestCenterOlaAvailableTimeSlotsVw.objects.filter(
                    # matching date, start time and end time
                    date=consumed_reservation_code_data["date"],
                    start_time=utc_start_time.time(),
                    end_time=utc_end_time.time(),
                    # only medium priority
                    remaining_available_test_assessors_per_date_medium__gt=0,
                    # remaining_available_test_assessors_total > 0
                    remaining_available_test_assessors_total__gt=0,
                    # matching respective language ID
                    assessed_language_id=assessed_language_id,
                )
            # LOW PRIORITY
            elif priority_level_codename == ReasonForTestingPriorityCodenameConst.LOW:
                # getting available time slot
                time_slot_still_available = TestCenterOlaAvailableTimeSlotsVw.objects.filter(
                    # matching date, start time and end time
                    date=consumed_reservation_code_data["date"],
                    start_time=utc_start_time.time(),
                    end_time=utc_end_time.time(),
                    # only low priority
                    remaining_available_test_assessors_per_date_low__gt=0,
                    # remaining_available_test_assessors_total > 0
                    remaining_available_test_assessors_total__gt=0,
                    # matching respective language ID
                    assessed_language_id=assessed_language_id,
                )

            # this time slot is still available
            if time_slot_still_available:
                # getting assessment process assigned test specs ID
                assessment_process_assigned_test_specs_id = (
                    ConsumedReservationCodes.objects.filter(
                        reservation_code=consumed_reservation_code_data[
                            "reservation_code"
                        ]
                    )
                    .last()
                    .assessment_process_assigned_test_specs_id
                )
                # creating new test session data
                new_test_session_data = TestCenterTestSessionData.objects.create(
                    test_center_room_id=None,
                    open_to_ogd=None,
                    date=consumed_reservation_code_data["date"],
                    start_time=utc_start_time,
                    end_time=utc_end_time,
                    spaces_available=None,
                    test_skill_type_id=consumed_reservation_code_data[
                        "test_skill_type_id"
                    ],
                    test_skill_sub_type_id=consumed_reservation_code_data[
                        "test_skill_sub_type_id"
                    ],
                    candidate_phone_number=consumed_reservation_code_data[
                        "candidate_phone_number"
                    ],
                )
                new_test_session = TestCenterTestSessions.objects.create(
                    test_session_data_id=new_test_session_data.id,
                    test_center_id=None,
                    is_standard=1,
                    user_accommodation_file_id=None,
                    assessment_process_assigned_test_specs_id=assessment_process_assigned_test_specs_id,
                )
                new_test_session_id = new_test_session.id
                consumed_reservation_code_data["test_session_id"] = new_test_session.id
            # this time slot is no longer available
            else:
                return Response(
                    {
                        "error": "this time slot is no longer available",
                    },
                    status=status.HTTP_409_CONFLICT,
                )

        # Other Test
        else:
            # make sure there are actually enough seats
            # look up the current count from the view
            test_session_count = TestSessionCountVW.objects.filter(
                id=consumed_reservation_code_data["test_session_id"]
            ).last()

            # if it is equal to or exceed the max, then delete it
            if test_session_count.count >= test_session_count.max:
                return Response(
                    {"error": "not enough seats"},
                    status=status.HTTP_409_CONFLICT,
                )

    # collect all needed info to send the email
    if not consumed_reservation_code_data["reservation_code"]:
        consumed_reservation_code_data["reservation_code"] = (
            current_crc_obj.reservation_code
        )

    candidate_reservation_code_obj = CandidateReservationsVW.objects.filter(
        reservation_code=consumed_reservation_code_data["reservation_code"]
    ).last()

    user = User.objects.get(id=user_id)
    action_en = ConsumedReservationCodeStatusText.objects.get(
        consumed_reservation_code_status_id=code_status.id, language=Language_id.EN
    ).text
    action_fr = ConsumedReservationCodeStatusText.objects.get(
        consumed_reservation_code_status_id=code_status.id, language=Language_id.FR
    ).text

    # get the test center/session details. from the view? test_session_id or current_crc_obj.test_session_id
    # delete the reservation code from the other table if existed; create a new consumed reservation code entry
    if reservation_code_obj:
        ConsumedReservationCodes.objects.create(
            assessment_process_assigned_test_specs_id=assessment_process_assigned_test_specs.id,
            status_id=code_status.id,
            reservation_code=consumed_reservation_code_data["reservation_code"],
            candidate_id=user_id,
            test_session_id=consumed_reservation_code_data["test_session_id"],
            modify_date=booked_date,
        )
        reservation_code_obj.delete()
    # if the reservation code was previously consumed and there is no need to delete the reservation code
    else:
        current_crc_obj.assessment_process_assigned_test_specs_id = (
            assessment_process_assigned_test_specs.id
        )
        current_crc_obj.status_id = code_status.id
        current_crc_obj.test_session_id = consumed_reservation_code_data[
            "test_session_id"
        ]
        current_crc_obj.modify_date = booked_date
        current_crc_obj.save()

    email_data = {
        "first_name": user.first_name,
        "last_name": user.last_name,
        "email": user.email,
        "reservation_code": consumed_reservation_code_data["reservation_code"],
        "assessment_process_reference_number": candidate_reservation_code_obj.assessment_process_reference_number,
        "closing_date": candidate_reservation_code_obj.assessment_process_closing_date,
        "test_skill_type_name_en": candidate_reservation_code_obj.test_skill_type_en_name,
        "test_skill_type_name_fr": candidate_reservation_code_obj.test_skill_type_fr_name,
        "test_skill_sub_type_name_en": candidate_reservation_code_obj.test_skill_sub_type_en_name,
        "test_skill_sub_type_name_fr": candidate_reservation_code_obj.test_skill_sub_type_fr_name,
        "dept_edesc": candidate_reservation_code_obj.dept_edesc,
        "dept_eabrv": candidate_reservation_code_obj.dept_eabrv,
        "dept_fdesc": candidate_reservation_code_obj.dept_fdesc,
        "dept_fabrv": candidate_reservation_code_obj.dept_fabrv,
        "contact_email_for_candidates": candidate_reservation_code_obj.contact_email_for_candidates,
        "action_en": action_en,
        "action_fr": action_fr,
    }

    # OLA Test
    if consumed_reservation_code_data["is_ola"]:
        personalization = {
            "first_name": email_data["first_name"],
            "last_name": email_data["last_name"],
            "reservation_code": email_data["reservation_code"],
            "assessment_process_reference_number": email_data[
                "assessment_process_reference_number"
            ],
            "closing_date": email_data["closing_date"].strftime("%Y/%m/%d"),
            "test_skill_type_name_en": email_data["test_skill_type_name_en"],
            "test_skill_sub_type_name_en": email_data["test_skill_sub_type_name_en"],
            "action_en": email_data["action_en"],
            "contact_email_for_candidates": email_data["contact_email_for_candidates"],
            "test_skill_type_name_fr": email_data["test_skill_type_name_fr"],
            "test_skill_sub_type_name_fr": email_data["test_skill_sub_type_name_fr"],
            "action_fr": email_data["action_fr"],
            "dept_fdesc": email_data["dept_fdesc"],
            "dept_fabrv": email_data["dept_fabrv"],
            "dept_edesc": email_data["dept_edesc"],
            "dept_eabrv": email_data["dept_eabrv"],
        }
        # send an email
        if reservation_status == StaticConsumedReservationCodeStatus.BOOKED:
            ola_code_booking(email_data["email"], personalization)

        # for OLA these are identical if test_session_id_for_email is None or not
        if reservation_status == StaticConsumedReservationCodeStatus.WITHDRAWN:
            # email candidate in case the forgot that they just withdrew
            candidate_withdrawl_notification(email_data["email"], personalization)
            # email hr cuz they need to knows
            reservation_code_withdraw_from_booked_session(
                personalization["contact_email_for_candidates"], personalization
            )
            # let's email everyone!

    # Other Test
    else:
        if test_session_id_for_email is not None:
            # getting test center test session data (might not be found if WITHDRAWN action)
            test_session_test_center = TestCenterTestSessionsVW.objects.filter(
                id=test_session_id_for_email
            ).last()

            # found test center test session data
            if test_session_test_center is not None:
                # ---------------------
                # Getting Address Text
                # ---------------------
                test_center_address_text = TestCenterAddressText.objects.filter(
                    test_center=test_session_test_center.test_center_id
                )

                serialized_addresses = TestCenterAddressTextSerializer(
                    test_center_address_text, many=True
                ).data

                # translated addresses final object
                final_addresses_obj = create_language_compatible_object(
                    serialized_addresses
                )

                # ---------------------
                # Getting City Text
                # ---------------------
                test_center_city_text = TestCenterCityText.objects.filter(
                    test_center=test_session_test_center.test_center_id
                )

                serialized_cities = TestCenterCityTextSerializer(
                    test_center_city_text, many=True
                ).data

                # translated cities final object
                final_cities_obj = create_language_compatible_object(serialized_cities)

                # ---------------------
                # Getting Province Text
                # ---------------------
                test_center_province_text = TestCenterProvinceText.objects.filter(
                    test_center=test_session_test_center.test_center_id
                )

                serialized_provinces = TestCenterProvinceTextSerializer(
                    test_center_province_text, many=True
                ).data

                # translated provinces final object
                final_provinces_obj = create_language_compatible_object(
                    serialized_provinces
                )

                # add to the email data IF there is test center info connected to the action
                test_center_info = {
                    "date": test_session_test_center.date,
                    "start_time": test_session_test_center.start_time,
                    "end_time": test_session_test_center.end_time,
                    "test_center_name": test_session_test_center.test_center_name,
                    "room_name": test_session_test_center.room_name,
                    "test_center_address_text": final_addresses_obj,
                    "test_center_city_text": final_cities_obj,
                    "test_center_province_text": final_provinces_obj,
                    "test_center_postal_code": test_session_test_center.test_center_postal_code,
                    "tc_country_eabrv": test_session_test_center.country_eabrv,
                    "tc_country_edesc": test_session_test_center.country_edesc,
                    "tc_country_fabrv": test_session_test_center.country_fabrv,
                    "tc_country_fdesc": test_session_test_center.country_fdesc,
                    "tc_dept_eabrv": test_session_test_center.dept_eabrv,
                    "tc_dept_edesc": test_session_test_center.dept_edesc,
                    "tc_dept_fabrv": test_session_test_center.dept_fabrv,
                    "tc_dept_fdesc": test_session_test_center.dept_fdesc,
                    "booking_delay": test_session_test_center.booking_delay,
                }
                email_data.update(test_center_info)

        # send an email
        if reservation_status == StaticConsumedReservationCodeStatus.BOOKED:
            send_reservation_code_booking_email_celery_task.delay(email_data)

        if (
            reservation_status == StaticConsumedReservationCodeStatus.WITHDRAWN
            and test_session_id_for_email is None
        ):
            send_reservation_code_withdraw_no_booking_email_celery_task.delay(
                email_data
            )

        if (
            reservation_status == StaticConsumedReservationCodeStatus.WITHDRAWN
            and test_session_id_for_email is not None
        ):
            send_reservation_code_withdraw_from_booked_session_email_celery_task.delay(
                email_data
            )

    if new_test_session_id:
        return Response(
            {"test_session_id": new_test_session_id}, status=status.HTTP_200_OK
        )
    return Response({"OK": "OK"}, status=status.HTTP_200_OK)


def check_if_ola_booking_violates_booking_delay(test_session_id):
    # initializing respecting_booking_delay
    respecting_booking_delay = True

    # getting related test session data
    related_test_session = TestCenterTestSessions.objects.get(id=test_session_id)
    related_test_session_data = TestCenterTestSessionData.objects.get(
        id=related_test_session.test_session_data_id
    )
    # get info about the assessment process; need to check if last mintute cancelations are allowed
    assessment_process_assigned_test_specs = (
        AssessmentProcessAssignedTestSpecs.objects.get(
            id=related_test_session.assessment_process_assigned_test_specs_id
        )
    )
    assessment_process = AssessmentProcess.objects.get(
        id=assessment_process_assigned_test_specs.assessment_process_id
    )

    # initializing greatest_booking_delay
    greatest_booking_delay = None

    # if last minute cancellations are allowed, then check against the test centers as before
    if assessment_process.allow_last_minute_cancellation_tc:
        # getting all test centers that have the same available time slot
        test_center_ids_with_same_time_slot = TestCenterOlaTimeSlot.objects.filter(
            start_time=related_test_session_data.start_time.time(),
            end_time=related_test_session_data.end_time.time(),
        ).values_list("test_center_id", flat=True)

        # looping in test_center_ids_with_same_time_slot
        for test_center_id in test_center_ids_with_same_time_slot:
            # getting booking delay respective test center
            booking_delay = TestCenterOlaConfigs.objects.get(
                test_center_id=test_center_id
            ).booking_delay
            # greatest_booking_delay not defined yet or booking_delay > greatest_booking_delay
            if greatest_booking_delay is None or booking_delay > greatest_booking_delay:
                # updating greatest_booking_delay with new booking_delay value
                greatest_booking_delay = booking_delay

    # if they are not allowed to use the last minute cancellations
    else:
        # get the ola global last minute cancelation window
        # get the global cancelation window; use this for most sessions
        greatest_booking_delay = OlaGlobalConfigs.objects.get(
            codename=OlaGlobalConfigsConst.CANCELLATION_WINDOW
        ).value

    # greatest_booking_delay is defined
    if greatest_booking_delay is not None:
        # getting current time (UTC)
        current_time = datetime.now(pytz.utc)

        # adding greatest_booking_delay nbr of hours to current_time
        current_time_plus_greatest_booking_delay = current_time + timedelta(
            hours=greatest_booking_delay
        )

        # current_time_plus_greatest_booking_delay > test session start time
        if (
            current_time_plus_greatest_booking_delay
            > related_test_session_data.start_time
        ):
            # setting respecting_booking_delay to False
            respecting_booking_delay = False

    # getting earliest booking
    earliest_booking = related_test_session_data.start_time

    return respecting_booking_delay, greatest_booking_delay, earliest_booking


def check_if_booking_violates_booking_delay(booked_time, test_session_id):
    test_center_test_session_data_id = TestCenterTestSessions.objects.get(
        id=test_session_id
    ).test_session_data_id
    test_center_test_session_data = TestCenterTestSessionData.objects.get(
        id=test_center_test_session_data_id
    )

    # room_id is defined
    if test_center_test_session_data.test_center_room_id is not None:
        # getting test center and test center room data
        test_center_room = TestCenterRooms.objects.get(
            id=test_center_test_session_data.test_center_room_id
        )

        # getting booking delay
        booking_delay = TestCenter.objects.get(
            id=test_center_room.test_center_id
        ).booking_delay

        # adding booking_delay nbr of hours to current_time
        booked_time_plus_booking_delay = booked_time + timedelta(hours=booking_delay)

        earliest_booking = test_center_test_session_data.start_time - timedelta(
            hours=booking_delay
        )
        return (
            booked_time_plus_booking_delay > test_center_test_session_data.start_time,
            earliest_booking,
        )
    # room_id is undefined (OLA Test)
    else:
        return False, None


class EditTestSessionPhoneNumber(APIView):
    def post(self, request):
        return edit_test_session_phone_number(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# editing phone number of provided test session
def edit_test_session_phone_number(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(
        ["test_session_id", "phone_number"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    test_session_id, phone_number = itemgetter("test_session_id", "phone_number")(
        parameters
    )

    # making sure that this request is from a rightful user (meaning that there is a related consumed reservation code linked to their user ID and to the provided test session ID)
    existing_related_consumed_reservation_code = (
        ConsumedReservationCodes.objects.filter(
            candidate_id=user_info["user_id"], test_session_id=test_session_id
        )
    )

    # rightful user
    if existing_related_consumed_reservation_code:
        try:
            # getting related test session
            related_test_session = TestCenterTestSessions.objects.get(
                id=test_session_id
            )

            # getting related test session data
            related_test_session_data = TestCenterTestSessionData.objects.get(
                id=related_test_session.test_session_data_id
            )

            # updating phone number
            related_test_session_data.candidate_phone_number = phone_number
            related_test_session_data.save()

            return Response(status=status.HTTP_200_OK)

        except TestCenterTestSessions.DoesNotExist:
            return Response(
                {
                    "error": "no test center test session found based on provided test session ID",
                },
                status=status.HTTP_404_NOT_FOUND,
            )
        except TestCenterTestSessionData.DoesNotExist:
            return Response(
                {
                    "error": "no test center test session data found based on provided test session ID",
                },
                status=status.HTTP_404_NOT_FOUND,
            )

    # should not be called from that user
    else:
        return Response(
            {
                "error": "this user it not authorized to do this call",
            },
            status=status.HTTP_401_UNAUTHORIZED,
        )


class EditUserAccommodationFilePhoneNumber(APIView):
    def post(self, request):
        return edit_user_accommodation_file_phone_number(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# editing phone number of provided test session
def edit_user_accommodation_file_phone_number(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(
        ["user_accommodation_file_id", "phone_number"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    user_accommodation_file_id, phone_number = itemgetter(
        "user_accommodation_file_id", "phone_number"
    )(parameters)

    try:
        # getting respective user accommodation file data based on provided user_accommodation_file_id (also making sure that the call is done by the rightful user)
        user_accommodation_file_data = UserAccommodationFile.objects.get(
            id=user_accommodation_file_id, user_id=user_info["user_id"]
        )

        # updating phone number
        user_accommodation_file_data.candidate_phone_number = phone_number
        user_accommodation_file_data.save()

        # checking if there are linked test sessions
        linked_test_sessions = TestCenterTestSessions.objects.filter(
            user_accommodation_file_id=user_accommodation_file_data.id
        )

        # looping in linked_test_sessions (if found linked test sessions)
        for linked_test_session in linked_test_sessions:
            # getting related test session data
            related_test_session_data = TestCenterTestSessionData.objects.get(
                id=linked_test_session.test_session_data_id
            )
            # updating phone number
            related_test_session_data.candidate_phone_number = phone_number
            related_test_session_data.save()

        return Response(status=status.HTTP_200_OK)

    except UserAccommodationFile.DoesNotExist:
        return Response(
            {
                "error": "no user accommodation file has been found based on provided user_accommodation_file_id and user_id",
            },
            status=status.HTTP_404_NOT_FOUND,
        )

    # should never happen
    except TestCenterTestSessionData.DoesNotExist:
        return Response(
            {
                "error": "no test center test session data has been found in the found linked test sessions logic",
            },
            status=status.HTTP_404_NOT_FOUND,
        )


class ReleaseReservationCode(APIView):
    def post(self, request):
        return release_reservation_code(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# releases the code for the CURRENT user to book for another session
def release_reservation_code(request):
    user_info = get_user_info_from_jwt_token(request)
    user_id = user_info["user_id"]
    success, parameters = get_needed_parameters(
        ["test_session_id", "user_accommodation_file_id", "is_ola"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    test_session_id, user_accommodation_file_id, is_ola = itemgetter(
        "test_session_id", "user_accommodation_file_id", "is_ola"
    )(parameters)

    # get the consumed reservation code
    consumed_reservation_code = ConsumedReservationCodes.objects.filter(
        test_session_id=test_session_id, candidate_id=user_id
    ).last()  # should not have more than one row, but getting last to avoid error in case of duplicate rows

    # check if accomm request (might not have a test session id yet)
    if consumed_reservation_code is None:
        consumed_reservation_code = ConsumedReservationCodes.objects.filter(
            candidate_id=user_info["user_id"],
            user_accommodation_file_id=user_accommodation_file_id,
        ).last()

    # check if the action violates the booking delay period
    modify_date = datetime.now(pytz.utc)

    # OLA Test
    if is_ola == "true":
        # calling the booking delay validation
        respecting_booking_delay, greatest_booking_delay, earliest_booking = (
            check_if_ola_booking_violates_booking_delay(test_session_id)
        )

        # not respecting booking delay
        if not respecting_booking_delay:
            return Response(
                {
                    "earliest_booking": earliest_booking
                    - timedelta(hours=greatest_booking_delay),
                    "error": "session violates booking delay",
                },
                status=status.HTTP_406_NOT_ACCEPTABLE,
            )

    # Other Test
    else:
        # check if there is a test session id -> if not then it's an accom request
        if consumed_reservation_code.test_session_id is not None:
            check, earliest_booking = check_if_booking_violates_booking_delay(
                modify_date, consumed_reservation_code.test_session_id
            )
        else:
            check = False

        # if this violates the booking delay of the booked test center, return a 406
        if check:
            return Response(
                {
                    "error": "session violates booking delay",
                    "earliest_booking": earliest_booking,
                },
                status=status.HTTP_406_NOT_ACCEPTABLE,
            )

    # update the consumed reservation code: this LOCKS the code to this user
    # but allows them to use the code again under "Reserve a Seat"
    code_status = ConsumedReservationCodeStatus.objects.get(
        codename=StaticConsumedReservationCodeStatus.RESERVED
    )
    consumed_reservation_code.modify_date = modify_date
    consumed_reservation_code.test_session_id = None
    consumed_reservation_code.status_id = code_status.id
    consumed_reservation_code.save()

    # get data for the email
    res_code = consumed_reservation_code.reservation_code
    candidate_reservation_code_obj = CandidateReservationsVW.objects.filter(
        reservation_code=res_code
    ).last()

    user = User.objects.get(id=user_id)

    action_en = ConsumedReservationCodeStatusText.objects.get(
        consumed_reservation_code_status_id=code_status.id, language=Language_id.EN
    ).text

    action_fr = ConsumedReservationCodeStatusText.objects.get(
        consumed_reservation_code_status_id=code_status.id, language=Language_id.FR
    ).text

    test_session_test_center = TestCenterTestSessionsVW.objects.filter(
        id=test_session_id
    ).last()

    # not able to find test center test session data with test_session_id ==> check with consumed reservation code id (might need improvements in the future)
    if not test_session_test_center:
        test_session_test_center = TestCenterTestSessionsVW.objects.filter(
            # adding "-CRC" to the consumed_reservation_code ID, since the TestCenterTestSessionsVW has that string as a suffix for consumed reservation code IDs
            id=(str(consumed_reservation_code.id) + "-CRC")
        ).last()

    # ---------------------
    # Getting Address Text
    # ---------------------

    test_center_address_text = TestCenterAddressText.objects.filter(
        test_center=test_session_test_center.test_center_id
    )

    serialized_addresses = TestCenterAddressTextSerializer(
        test_center_address_text, many=True
    ).data

    # translated addresses final object
    final_addresses_obj = create_language_compatible_object(serialized_addresses)

    # ---------------------
    # Getting City Text
    # ---------------------
    test_center_city_text = TestCenterCityText.objects.filter(
        test_center=test_session_test_center.test_center_id
    )

    serialized_cities = TestCenterCityTextSerializer(
        test_center_city_text, many=True
    ).data

    # translated cities final object
    final_cities_obj = create_language_compatible_object(serialized_cities)

    # ---------------------
    # Getting Province Text
    # ---------------------
    test_center_province_text = TestCenterProvinceText.objects.filter(
        test_center=test_session_test_center.test_center_id
    )

    serialized_provinces = TestCenterProvinceTextSerializer(
        test_center_province_text, many=True
    ).data

    # translated provinces final object
    final_provinces_obj = create_language_compatible_object(serialized_provinces)

    # cancel accommodation request if exists
    if consumed_reservation_code.user_accommodation_file_id is not None:
        close_user_accommodation_request(consumed_reservation_code)

    test_session_room_name = "N/A"
    if test_session_test_center.room_name is not None:
        test_session_room_name = test_session_test_center.room_name

    email_data = {
        "first_name": user.first_name,
        "last_name": user.last_name,
        "email": user.email,
        "reservation_code": res_code,
        "closing_date": candidate_reservation_code_obj.assessment_process_closing_date.strftime(
            "%Y/%m/%d"
        ),
        "contact_email_for_candidates": candidate_reservation_code_obj.contact_email_for_candidates,
        "test_skill_type_name_en": candidate_reservation_code_obj.test_skill_type_en_name,
        "test_skill_type_name_fr": candidate_reservation_code_obj.test_skill_type_fr_name,
        "test_skill_sub_type_name_en": candidate_reservation_code_obj.test_skill_sub_type_en_name,
        "test_skill_sub_type_name_fr": candidate_reservation_code_obj.test_skill_sub_type_fr_name,
        "dept_eabrv": candidate_reservation_code_obj.dept_eabrv,
        "dept_edesc": candidate_reservation_code_obj.dept_edesc,
        "dept_fabrv": candidate_reservation_code_obj.dept_fabrv,
        "dept_fdesc": candidate_reservation_code_obj.dept_fdesc,
        "action_en": action_en,
        "action_fr": action_fr,
        "test_center_name": test_session_test_center.test_center_name,
        "room_name": test_session_room_name,
        "test_center_postal_code": test_session_test_center.test_center_postal_code,
        "country_eabrv": test_session_test_center.country_eabrv,
        "country_edesc": test_session_test_center.country_edesc,
        "country_fabrv": test_session_test_center.country_fabrv,
        "country_fdesc": test_session_test_center.country_fdesc,
    }

    # OLA Test
    if is_ola == "true":
        ola_code_book_later(email_data["email"], email_data)
    # Other Test
    else:
        # these need to be included seperately, or they break ola_code_book_later as it thinks they are files/attachements
        email_data["test_center_address_text"] = final_addresses_obj
        email_data["test_center_city_text"] = final_cities_obj
        email_data["test_center_province_text"] = (final_provinces_obj,)
        send_reservation_code_book_later_email_celery_task.delay(email_data)

    return Response(
        {"OK": "OK", "reservation_code": res_code}, status=status.HTTP_200_OK
    )
