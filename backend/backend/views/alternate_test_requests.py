import json
from rest_framework.response import Response
from django.db.models import Q
from operator import itemgetter
from urllib import parse
from rest_framework import status
from backend.custom_models.consumed_reservation_code_status import (
    ConsumedReservationCodeStatus,
)
from backend.custom_models.consumed_reservation_codes import ConsumedReservationCodes
from backend.static.consumed_reservation_code_statuses import (
    StaticConsumedReservationCodeStatus,
)
from backend.static.assigned_test_status import get_assigned_test_status_id
from backend.views.aae import (
    UitTestAssociationForAccommodationOrAlternateTestRequestActions,
    get_modified_test_access_code,
    handle_assign_new_uit_test,
    handle_uit_test_association_for_accommodation_or_alternate_test_request,
)
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.user_accommodation_file_status import (
    UserAccommodationFileStatus,
)
from backend.custom_models.user_accommodation_file_test_to_administer import (
    UserAccommodationFileTestToAdminister,
)
from backend.custom_models.user_accommodation_file import UserAccommodationFile
from backend.static.languages import Languages
from cms.views.utils import get_needed_parameters
from backend.static.user_accommodation_file_status_const import (
    UserAccommodationFileStatusConst,
)
from backend.static.assigned_test_status import AssignedTestStatus
from user_management.views.utils import CustomPagination
from db_views.serializers.aae_serializers import (
    UserAccommodationFileViewSerializer,
)
from db_views.db_view_models.user_accommodation_file_vw import UserAccommodationFileVW


def get_all_pending_alternate_test_requests(request):
    success, parameters = get_needed_parameters(["page", "page_size"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting all pending alternate test requests
    pending_alternate_test_requests = UserAccommodationFileVW.objects.filter(
        is_alternate_test_request=1,
        status_codename=UserAccommodationFileStatusConst.NEW,
    )

    # serializing data
    serialized_data = UserAccommodationFileViewSerializer(
        pending_alternate_test_requests, many=True
    ).data

    # getting sorted serialized data by process_end_date (ASC) and, then, created_date (ASC)
    ordered_serialized_data = sorted(
        sorted(
            serialized_data,
            key=lambda k: k["process_end_date"],
        ),
        key=lambda k: k["created_date"],
    )

    # only getting data for current selected page
    current_pending_alternate_test_requests = ordered_serialized_data[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # final data using pagination library
    final_object = paginator.get_paginated_response(
        current_pending_alternate_test_requests,
        len(pending_alternate_test_requests),
        current_page,
        page_size,
    )

    return final_object


def get_found_pending_alternate_test_requests(request):
    success, parameters = get_needed_parameters(
        ["keyword", "current_language", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    keyword, current_language, page, page_size = itemgetter(
        "keyword", "current_language", "page", "page_size"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # Blank search
    if keyword == " ":
        # getting all pending alternate test requests
        found_pending_alternate_test_requests = UserAccommodationFileVW.objects.filter(
            is_alternate_test_request=1,
            status_codename=UserAccommodationFileStatusConst.NEW,
        )
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # English Search
        if current_language == Languages.EN:
            # getting matching pending alternate test requests
            found_pending_alternate_test_requests = (
                UserAccommodationFileVW.objects.filter(
                    (
                        Q(is_alternate_test_request=1)
                        & Q(status_codename=UserAccommodationFileStatusConst.NEW)
                    )
                    & (
                        Q(id__icontains=keyword)
                        | Q(user_id__icontains=keyword)
                        | Q(user_first_name__icontains=keyword)
                        | Q(user_last_name__icontains=keyword)
                        | Q(created_date__icontains=keyword)
                        | Q(process_end_date__icontains=keyword)
                        | Q(test_center_name__icontains=keyword)
                        | Q(test_skill_type_name_en__icontains=keyword)
                        | Q(test_skill_sub_type_name_en__icontains=keyword)
                    )
                )
            )

        # French Search
        else:
            # getting matching pending alternate test requests
            found_pending_alternate_test_requests = (
                UserAccommodationFileVW.objects.filter(
                    (
                        Q(is_alternate_test_request=1)
                        & Q(status_codename=UserAccommodationFileStatusConst.NEW)
                    )
                    & (
                        Q(id__icontains=keyword)
                        | Q(user_id__icontains=keyword)
                        | Q(user_first_name__icontains=keyword)
                        | Q(user_last_name__icontains=keyword)
                        | Q(created_date__icontains=keyword)
                        | Q(process_end_date__icontains=keyword)
                        | Q(test_center_name__icontains=keyword)
                        | Q(test_skill_type_name_fr__icontains=keyword)
                        | Q(test_skill_sub_type_name_fr__icontains=keyword)
                    )
                )
            )

    # no results found
    if not found_pending_alternate_test_requests:
        return Response({"no results found"}, status=status.HTTP_200_OK)

    # serializing data
    serialized_data = UserAccommodationFileViewSerializer(
        found_pending_alternate_test_requests, many=True
    ).data

    # getting sorted serialized data by process_end_date (ASC) and, then, created_date (ASC)
    ordered_serialized_data = sorted(
        sorted(
            serialized_data,
            key=lambda k: k["process_end_date"],
        ),
        key=lambda k: k["created_date"],
    )

    # only getting data for current selected page
    current_found_pending_alternate_test_requests = ordered_serialized_data[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # final data using pagination library
    final_object = paginator.get_paginated_response(
        current_found_pending_alternate_test_requests,
        len(found_pending_alternate_test_requests),
        current_page,
        page_size,
    )

    return final_object


def submit_pending_alternate_test_requests(request):
    user_accommodation_file_data = json.loads(request.body)

    try:
        # getting respective user accommodation file data
        user_accommodation_file = UserAccommodationFile.objects.get(
            id=user_accommodation_file_data["id"]
        )

        # checking if there is already an assigned test to administer associated to that user accommodation file
        test_to_administer = UserAccommodationFileTestToAdminister.objects.filter(
            user_accommodation_file_id=user_accommodation_file.id
        )

        # already associated test to administer
        if test_to_administer:
            # getting last entry (should always only get one entry here)
            test_to_administer = test_to_administer.last()
            # test_id is provided (no specified test description)
            if user_accommodation_file_data["test_id"] is not None:
                # updating test fields
                test_to_administer.test_id = user_accommodation_file_data["test_id"]
                test_to_administer.specified_test_description = None
            # specified test description is provided (no test_id)
            else:
                # updating test fields
                test_to_administer.test_id = None
                test_to_administer.specified_test_description = (
                    user_accommodation_file_data["specified_test_description"]
                )
            test_to_administer.save()
        # no associated test to administer yet
        else:
            # test_id is provided (no specified test description)
            if user_accommodation_file_data["test_id"] is not None:
                provided_test_id = user_accommodation_file_data["test_id"]
                provided_specified_test_description = None
            # specified test description is provided (no test_id)
            else:
                provided_test_id = None
                provided_specified_test_description = user_accommodation_file_data[
                    "specified_test_description"
                ]

            # creating new entry in UserAccommodationFileTestToAdminister
            test_to_administer = UserAccommodationFileTestToAdminister.objects.create(
                user_accommodation_file_id=user_accommodation_file.id,
                test_id=provided_test_id,
                specified_test_description=provided_specified_test_description,
            )

        # request from UIT test
        if user_accommodation_file.is_uit:
            # handling test association for this request
            handle_uit_test_association_for_accommodation_or_alternate_test_request(
                UitTestAssociationForAccommodationOrAlternateTestRequestActions.SUBMIT_SEND,
                user_accommodation_file,
            )

        # setting user accommodation file status to completed
        completed_status_id = UserAccommodationFileStatus.objects.get(
            codename=UserAccommodationFileStatusConst.COMPLETED
        ).id
        user_accommodation_file.status_id = completed_status_id

        # setting user accommodation file test_center_id
        user_accommodation_file.test_center_id = user_accommodation_file_data[
            "test_center_id"
        ]

        # setting rationale
        user_accommodation_file.rationale = user_accommodation_file_data["rationale"]

        user_accommodation_file.save()

        return Response(status=status.HTTP_200_OK)

    except UserAccommodationFile.DoesNotExist:
        return Response(
            {"error": "No user accommodation file found based on provided parameters"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def cancel_pending_alternate_test_requests(request):
    user_accommodation_file_data = json.loads(request.body)

    try:
        # getting respective user accommodation file data
        user_accommodation_file = UserAccommodationFile.objects.get(
            id=user_accommodation_file_data["id"]
        )

        # setting user accommodation file status to cancelled
        cancelled_status_id = UserAccommodationFileStatus.objects.get(
            codename=UserAccommodationFileStatusConst.CANCELLED
        ).id
        user_accommodation_file.status_id = cancelled_status_id

        # setting user accommodation file rationale
        user_accommodation_file.rationale = user_accommodation_file_data["rationale"]

        user_accommodation_file.save()

        # getting respective consusmed reservation code data
        consumed_reservation_code = ConsumedReservationCodes.objects.get(
            user_accommodation_file_id=user_accommodation_file_data["id"]
        )

        # getting reserved consumed reservation code status ID
        reserved_consumed_reservation_code_id = (
            ConsumedReservationCodeStatus.objects.get(
                codename=StaticConsumedReservationCodeStatus.RESERVED
            ).id
        )

        # updating the consumed reservation code to the needed data/state
        consumed_reservation_code.status_id = reserved_consumed_reservation_code_id
        consumed_reservation_code.assigned_test_id = None
        consumed_reservation_code.test_session_id = None
        consumed_reservation_code.user_accommodation_file_id = None
        consumed_reservation_code.save()

        # TODO: notify candidate + unclear logic that will need to be handled

        return Response(status=status.HTTP_200_OK)

    except UserAccommodationFile.DoesNotExist:
        return Response(
            {"error": "No user accommodation file found based on provided parameters"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    except ConsumedReservationCodes.DoesNotExist:
        return Response(
            {
                "error": "No consumed reservation code found based on provided parameters"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


def get_all_completed_alternate_test_requests(request):
    success, parameters = get_needed_parameters(["page", "page_size"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting all completed alternate test requests
    completed_alternate_test_requests = UserAccommodationFileVW.objects.filter(
        is_alternate_test_request=1,
        status_codename__in=(
            UserAccommodationFileStatusConst.CANCELLED,
            UserAccommodationFileStatusConst.CANCELLED_BY_CANDIDATE,
            UserAccommodationFileStatusConst.COMPLETED,
        ),
    )

    # serializing data
    serialized_data = UserAccommodationFileViewSerializer(
        completed_alternate_test_requests, many=True
    ).data

    # getting sorted serialized data by process_end_date (ASC) and, then, created_date (ASC)
    ordered_serialized_data = sorted(
        sorted(
            serialized_data,
            key=lambda k: k["process_end_date"],
        ),
        key=lambda k: k["created_date"],
    )

    # only getting data for current selected page
    current_completed_alternate_test_requests = ordered_serialized_data[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # final data using pagination library
    final_object = paginator.get_paginated_response(
        current_completed_alternate_test_requests,
        len(completed_alternate_test_requests),
        current_page,
        page_size,
    )

    return final_object


def get_found_completed_alternate_test_requests(request):
    success, parameters = get_needed_parameters(
        ["keyword", "current_language", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    keyword, current_language, page, page_size = itemgetter(
        "keyword", "current_language", "page", "page_size"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # Blank search
    if keyword == " ":
        # getting all completed alternate test requests
        found_completed_alternate_test_requests = (
            UserAccommodationFileVW.objects.filter(
                is_alternate_test_request=1,
                status_codename__in=(
                    UserAccommodationFileStatusConst.CANCELLED,
                    UserAccommodationFileStatusConst.CANCELLED_BY_CANDIDATE,
                    UserAccommodationFileStatusConst.COMPLETED,
                    UserAccommodationFileStatusConst.ADMINISTERED,
                ),
            )
        )
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # English Search
        if current_language == Languages.EN:
            # getting matching completed alternate test requests
            found_completed_alternate_test_requests = (
                UserAccommodationFileVW.objects.filter(
                    (
                        Q(is_alternate_test_request=1)
                        & Q(
                            status_codename__in=(
                                UserAccommodationFileStatusConst.CANCELLED,
                                UserAccommodationFileStatusConst.CANCELLED_BY_CANDIDATE,
                                UserAccommodationFileStatusConst.COMPLETED,
                                UserAccommodationFileStatusConst.ADMINISTERED,
                            ),
                        )
                    )
                    & (
                        Q(id__icontains=keyword)
                        | Q(user_id__icontains=keyword)
                        | Q(user_first_name__icontains=keyword)
                        | Q(user_last_name__icontains=keyword)
                        | Q(created_date__icontains=keyword)
                        | Q(process_end_date__icontains=keyword)
                        | Q(test_center_name__icontains=keyword)
                        | Q(test_skill_type_name_en__icontains=keyword)
                        | Q(test_skill_sub_type_name_en__icontains=keyword)
                    )
                )
            )

        # French Search
        else:
            # getting matching completed alternate test requests
            found_completed_alternate_test_requests = (
                UserAccommodationFileVW.objects.filter(
                    (
                        Q(is_alternate_test_request=1)
                        & Q(
                            status_codename__in=(
                                UserAccommodationFileStatusConst.CANCELLED,
                                UserAccommodationFileStatusConst.CANCELLED_BY_CANDIDATE,
                                UserAccommodationFileStatusConst.COMPLETED,
                                UserAccommodationFileStatusConst.ADMINISTERED,
                            ),
                        )
                    )
                    & (
                        Q(id__icontains=keyword)
                        | Q(user_id__icontains=keyword)
                        | Q(user_first_name__icontains=keyword)
                        | Q(user_last_name__icontains=keyword)
                        | Q(created_date__icontains=keyword)
                        | Q(process_end_date__icontains=keyword)
                        | Q(test_center_name__icontains=keyword)
                        | Q(test_skill_type_name_fr__icontains=keyword)
                        | Q(test_skill_sub_type_name_fr__icontains=keyword)
                    )
                )
            )

    # no results found
    if not found_completed_alternate_test_requests:
        return Response({"no results found"}, status=status.HTTP_200_OK)

    # serializing data
    serialized_data = UserAccommodationFileViewSerializer(
        found_completed_alternate_test_requests, many=True
    ).data

    # getting sorted serialized data by process_end_date (ASC) and, then, created_date (ASC)
    ordered_serialized_data = sorted(
        sorted(
            serialized_data,
            key=lambda k: k["process_end_date"],
        ),
        key=lambda k: k["created_date"],
    )

    # only getting data for current selected page
    current_found_completed_alternate_test_requests = ordered_serialized_data[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # final data using pagination library
    final_object = paginator.get_paginated_response(
        current_found_completed_alternate_test_requests,
        len(found_completed_alternate_test_requests),
        current_page,
        page_size,
    )

    return final_object
