from datetime import datetime
from rest_framework.response import Response
from rest_framework import status
from backend.static.user_accommodation_file_status_const import (
    UserAccommodationFileStatusConst,
)
from backend.custom_models.user_accommodation_file_status import (
    UserAccommodationFileStatus,
)
from backend.custom_models.user_accommodation_file import UserAccommodationFile
from backend.custom_models.break_bank_actions import BreakBankActions
from backend.custom_models.accommodation_request import AccommodationRequest
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.assigned_test_status import (
    AssignedTestStatus as AssignedTestStatusModel,
)
from backend.static.assigned_test_status import (
    AssignedTestStatus,
    get_assigned_test_status_id,
)
from backend.views.retrieve_notepad_view import delete_notepad
from backend.custom_models.test_scorer_assignment import TestScorerAssignment
from backend.views.utils import is_undefined, get_user_info_from_jwt_token
from cms.views.utils import get_needed_parameters
from cms.views.test_break_bank import BreakBankActionsConstants
from db_views.db_view_models.assigned_tests_vw import AssignedTestsVW


def update_test_status(test_id, new_status_id):
    test = AssignedTest.objects.get(id=test_id)
    test.status_id = new_status_id
    return save_and_return(test)


# create a new instance of assigned_test_status for this test, if it was submitted or timedout
def prepare_test_for_scoring(test_id, new_status_id):
    submitted_status_id = get_assigned_test_status_id(AssignedTestStatus.SUBMITTED)
    if new_status_id == submitted_status_id:
        check = TestScorerAssignment.objects.filter(assigned_test_id=test_id)
        if not check:
            assignment = TestScorerAssignment(assigned_test_id=test_id)
            assignment.save()


def submit_test(test_id, new_status_id):
    # getting assigned test data
    test = AssignedTest.objects.get(id=test_id)

    submitted_status_id = get_assigned_test_status_id(AssignedTestStatus.SUBMITTED)
    quit_status_id = get_assigned_test_status_id(AssignedTestStatus.QUIT)

    # checking if test is linked to an accommodation
    linked_user_accommodation_file = UserAccommodationFile.objects.filter(
        id=test.user_accommodation_file_id
    )

    # found linked accommodation
    if linked_user_accommodation_file:
        # updating user accommodation file status to ADMINISTERED
        user_accommodation_file_to_update = linked_user_accommodation_file.last()
        administered_status_id = UserAccommodationFileStatus.objects.get(
            codename=UserAccommodationFileStatusConst.ADMINISTERED
        ).id
        user_accommodation_file_to_update.status_id = administered_status_id
        user_accommodation_file_to_update.save()

    if new_status_id in (
        submitted_status_id,
        quit_status_id,
    ):
        test.submit_date = datetime.now()

    test.status_id = new_status_id

    # clean out the notepad
    delete_notepad(test_id)

    prepare_test_for_scoring(test_id, new_status_id)

    return save_and_return(test)


def save_and_return(test):
    test.save()
    return Response()


def update_candidate_test_status(request):
    user_info = get_user_info_from_jwt_token(request)
    assigned_test_id = request.query_params.get("assigned_test_id", None)
    test_status_codename = request.query_params.get("test_status_codename", None)
    previous_status_codename = request.query_params.get(
        "previous_status_codename", None
    )
    if is_undefined(assigned_test_id):
        return Response(
            {"error": "The assigned_test_id was not defined."},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(test_status_codename):
        return Response(
            {"error": "The test_status_codename was not defined."},
            status=status.HTTP_400_BAD_REQUEST,
        )
    test_status_exists = False
    # looping in AssignedTestStatus attributes
    for attr in vars(AssignedTestStatus):
        # exclude unwanted attributes (generic python attributes)
        if not attr.startswith("__"):
            if getattr(AssignedTestStatus, attr) == test_status_codename:
                test_status_exists = True
    # specified test status does not exists
    if not test_status_exists:
        return Response(
            {"error": "the specified test status does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    else:
        try:
            assigned_test = AssignedTest.objects.get(
                id=assigned_test_id, user_id=user_info["user_id"]
            )
            test_status_id = get_assigned_test_status_id(test_status_codename)
            assigned_test.status_id = test_status_id
            assigned_test.save()

            # making sure that the previous_status_codename is defined
            if previous_status_codename != "undefined":
                active_status_id = get_assigned_test_status_id(
                    AssignedTestStatus.ACTIVE
                )
                paused_status_id = get_assigned_test_status_id(
                    AssignedTestStatus.PAUSED
                )
                # previous status is provided, but is "null"
                if previous_status_codename == "null":
                    assigned_test.previous_status_id = None
                    assigned_test.save()
                # previous status is PAUSED + current status is ACTIVE (unpausing test on timer timeout action)
                elif get_assigned_test_status_id(
                    previous_status_codename
                ) == paused_status_id and (
                    get_assigned_test_status_id(test_status_codename)
                    == active_status_id
                ):
                    # if there is an accommodation request
                    if assigned_test.accommodation_request_id is not None:
                        # getting accommodation request data
                        accommodation_request_data = AccommodationRequest.objects.get(
                            id=assigned_test.accommodation_request_id
                        )
                        # if there is a defined break bank
                        if accommodation_request_data.break_bank_id is not None:
                            # getting break_bank_id
                            break_bank_id = accommodation_request_data.break_bank_id
                            # getting last break bank PAUSE action
                            last_break_bank_pause_action = (
                                BreakBankActions.objects.filter(
                                    break_bank_id=break_bank_id,
                                    action_type=BreakBankActionsConstants.PAUSE,
                                )
                                .order_by("modify_date")
                                .last()
                            )
                            # creating last UNPAUSE break bank action
                            BreakBankActions.objects.create(
                                action_type=BreakBankActionsConstants.UNPAUSE,
                                new_remaining_time=0,
                                break_bank_id=break_bank_id,
                                test_section_id=last_break_bank_pause_action.test_section_id,
                            )
            return Response(status=status.HTTP_200_OK)

        except AssignedTest.DoesNotExist:
            return Response(
                {"error": "the specified assigned test id does not exist"},
                status=status.HTTP_400_BAD_REQUEST,
            )


def get_previous_test_status(request):
    # making sure that we have the needed parameters
    success, parameters = get_needed_parameters(["assigned_test_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    try:
        assigned_test = AssignedTest.objects.get(id=parameters["assigned_test_id"])
        previous_status_codename = None
        if assigned_test.previous_status_id is not None:
            previous_status_codename = AssignedTestStatusModel.objects.get(
                id=assigned_test.previous_status_id
            ).codename
        return Response(previous_status_codename)
    except AssignedTest.DoesNotExist:
        return Response(
            {"error": "unable to find the specified assigned test"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def get_current_test_status(request):
    # making sure that we have the needed parameters
    success, parameters = get_needed_parameters(["assigned_test_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    try:
        assigned_test = AssignedTestsVW.objects.get(id=parameters["assigned_test_id"])
        return Response(
            {
                "status_id": assigned_test.status_id,
                "status_codename": assigned_test.status_codename,
                "is_invalid": assigned_test.is_invalid,
            }
        )
    except AssignedTestsVW.DoesNotExist:
        return Response(
            {"error": "unable to find the specified assigned test"},
            status=status.HTTP_400_BAD_REQUEST,
        )
