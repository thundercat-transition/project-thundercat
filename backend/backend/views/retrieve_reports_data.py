from operator import itemgetter
import datetime
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from backend.views.utils import get_user_info_from_jwt_token
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.custom_models.assigned_test_section_access_times import (
    AssignedTestSectionAccessTimes,
)
from backend.custom_models.candidate_answers import CandidateAnswers
from backend.custom_models.orderless_financial_data import OrderlessFinancialData
from backend.custom_models.candidate_multiple_choice_answers import (
    CandidateMultipleChoiceAnswers,
)
from backend.static.assigned_test_status import (
    AssignedTestStatus,
    get_assigned_test_status_codename,
    get_assigned_test_status_id,
)
from backend.serializers.reports_data_serializer import (
    TestDefinitionSerializer,
    TestContentReportDataSerializer,
    TestTakerReportDataSerializer,
)
from backend.serializers.assigned_test_serializer import AssignedTestSerializer
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_section import TestSection
from cms.cms_models.test_section_component import TestSectionComponent
from cms.static.test_section_component_type import TestSectionComponentType
from cms.cms_models.new_question import NewQuestion
from cms.views.utils import get_needed_parameters
from cms.serializers.item_bank_serializer import (
    ItemLatestVersionsDataViewSerializer,
)
from db_views.db_view_models.financial_report_vw import FinancialReportVW
from db_views.db_view_models.ta_history_report_vw import TaHistoryReportVW
from db_views.db_view_models.test_result_report_vw import TestResultReportVW
from db_views.serializers.financial_report_serializers import (
    FinancialReportDataViewSerializer,
)
from db_views.serializers.ta_history_report_serializers import (
    TaHistoryReportDataSerializer,
)
from db_views.serializers.test_result_report_serializers import (
    TestResultReportDataViewSerializer,
)
from db_views.db_view_models.test_order_and_reference_numbers_vw import (
    TestOrderAndReferenceNumbersVW,
)
from db_views.serializers.test_order_and_reference_numbers_serializers import (
    TestOrderAndReferenceNumbersViewSerializer,
)
from db_views.serializers.test_data_serializers import (
    TestDataViewSerializer,
)
from db_views.db_view_models.test_data_vw import (
    TestDataVW,
)
from db_views.db_view_models.adapted_tests_report_vw import AdaptedTestsReportVW
from db_views.serializers.adapted_tests_report_serializers import (
    AdaptedTestsReportDataViewSerializer,
)
from db_views.db_view_models.item_latest_versions_data_vw import (
    ItemLatestVersionsDataVW,
)


# retrieving TA assigned test order numbers
def retrieve_ta_assigned_test_order_numbers(request):
    user_info = get_user_info_from_jwt_token(request)

    # getting all respective test order and reference numbers
    test_order_and_reference_numbers = TestOrderAndReferenceNumbersVW.objects.filter(
        ta_user_id=user_info["user_id"]
    )
    return Response(
        TestOrderAndReferenceNumbersViewSerializer(
            test_order_and_reference_numbers, many=True
        ).data
    )


# retrieving all existing test order numbers
def retrieve_all_existing_test_order_numbers(request):
    # getting all test order numbers (not reference numbers)
    test_order_numbers = TestOrderAndReferenceNumbersVW.objects.filter(
        reference_number__isnull=True
    )

    # initializing unique_test_order_numbers
    unique_test_order_numbers = []

    # removing duplicates
    for test_order_number_data in test_order_numbers:
        if not any(
            unique_test_order_number_data.test_order_number
            == test_order_number_data.test_order_number
            and unique_test_order_number_data.staffing_process_number
            == test_order_number_data.staffing_process_number
            for unique_test_order_number_data in unique_test_order_numbers
        ):
            unique_test_order_numbers.append(test_order_number_data)

    # combining ordered and orderless requests
    combined_querysets = (
        list(
            TestOrderAndReferenceNumbersVW.objects.filter(
                test_order_number__isnull=True
            )
        )
        + unique_test_order_numbers
    )

    return Response(
        TestOrderAndReferenceNumbersViewSerializer(combined_querysets, many=True).data
    )


# retrieving tests based on provided test order number
def retrieve_tests_based_on_test_order_number(request, parameters):
    user_info = get_user_info_from_jwt_token(request)

    filters = {}

    # orderless request
    if parameters["orderless_request"] == "true":
        # requested by BO, R&D or other + providing ta_user_id
        if parameters["provided_ta_user_id"] != " ":
            # decoding username that might contain special characters
            filters["ta_user_id"] = parameters["provided_ta_user_id"]

        # requested by TA
        elif parameters["requested_by_ta"] == "true":
            # decoding username that might contain special characters
            filters["ta_user_id"] = user_info["user_id"]

        # getting related tests
        submitted_status_id = get_assigned_test_status_id(AssignedTestStatus.SUBMITTED)
        quit_status_id = get_assigned_test_status_id(AssignedTestStatus.QUIT)
        related_tests = TestDataVW.objects.filter(
            reference_number=parameters["test_order_number"],
            test_status_id__in=(submitted_status_id, quit_status_id),
            **filters,
        )

    # regular request
    else:
        # getting related tests
        submitted_status_id = get_assigned_test_status_id(AssignedTestStatus.SUBMITTED)
        quit_status_id = get_assigned_test_status_id(AssignedTestStatus.QUIT)
        related_tests = TestDataVW.objects.filter(
            test_order_number=parameters["test_order_number"],
            test_status_id__in=(submitted_status_id, quit_status_id),
        )

    # removing duplicates (we want unique combinations of test_id and test_version)
    assigned_tests_data = []
    assigned_test_ids = []
    for test in related_tests:
        if not any(
            assigned_test_data["test_id"] == test.test_id
            and assigned_test_data["test_version"] == test.test_version
            for assigned_test_data in assigned_tests_data
        ):
            assigned_tests_data.append(
                {"test_id": test.test_id, "test_version": test.test_version}
            )
            assigned_test_ids.append(test.assigned_test_id)

    # getting unique test_id and test_version combinations data
    final_queryset = TestDataVW.objects.filter(assigned_test_id__in=assigned_test_ids)

    return Response(TestDataViewSerializer(final_queryset, many=True).data)


def retrieve_candidates_base_on_selected_test(request, parameters):
    user_info = get_user_info_from_jwt_token(request)

    # initializing filters
    filters = {}

    # if requested by TA
    if parameters["requested_by_ta"] == "true":
        # decoding username that might contain special characters
        ta_user_id = user_info["user_id"]
        filters["ta_user_id"] = user_info["user_id"]
    # requested by BO or Other with provided ta username
    elif parameters["provided_ta_user_id"] != " ":
        # decoding username that might contain special characters
        ta_user_id = parameters["provided_ta_user_id"]
        filters["ta_user_id"] = parameters["provided_ta_user_id"]
    # should not happend (if so, just provided 0 (non existing user id), so the API should return nothing)
    else:
        ta_user_id = 0
        filters["ta_user_id"] = 0

    # if orderless request
    if parameters["orderless_request"] == "true":
        # getting related tests
        related_tests = TestDataVW.objects.filter(
            reference_number=parameters["test_order_or_reference_number"],
            test_id=parameters["test_id"],
            **filters,
        )

    # regular request
    else:
        # getting related tests
        temp_related_tests = TestDataVW.objects.filter(
            test_order_number=parameters["test_order_or_reference_number"],
            test_id=parameters["test_id"],
        )

        # making sure that we're only getting test data associated with allowed TA(s)
        related_tests = []
        for data in temp_related_tests:
            if data.allowed_ta_user_ids is not None and str(ta_user_id) in list(
                data.allowed_ta_user_ids.split(",")
            ):
                related_tests.append(data)

        related_tests = list(related_tests)

    # initializing needed variables
    unique_user_ids = []
    related_candidates_data = []

    # looping in related tests
    for test in related_tests:
        # getting only unique candidate usernames
        if test.candidate_user_id not in unique_user_ids:
            unique_user_ids.append(test.candidate_user_id)
            related_candidates_data.append(
                {
                    "candidate_user_id": test.candidate_user_id,
                    "candidate_username": test.candidate_username,
                    "candidate_email": test.candidate_email,
                    "candidate_first_name": test.candidate_first_name,
                    "candidate_last_name": test.candidate_last_name,
                }
            )

    return Response(related_candidates_data)


# retrieving candidate options based on provided parent and test codes
def retrieve_candidate_tests_data(parameters):
    # orderless request
    if parameters["orderless_request"] == "true":
        assigned_tests = AssignedTest.objects.filter(
            orderless_financial_data_id__in=OrderlessFinancialData.objects.filter(
                reference_number=parameters["test_order_number"]
            ),
            test_id=parameters["test_id"],
            user_id=parameters["user_id"],
        )
    # based on test permission
    else:
        # getting assigned tests based on provided test_order_number, test_id and user_id
        assigned_tests = AssignedTest.objects.filter(
            test_order_number=parameters["test_order_number"],
            test_id=parameters["test_id"],
            user_id=parameters["user_id"],
        )
    serializer = AssignedTestSerializer(assigned_tests, many=True)
    return Response(serializer.data)


# retrieving report data based on provided test order number and test (and potentially user_id)
def retrieve_results_report_data(request, parameters):
    user_id = request.query_params.get("user_id", None)
    ta_user_id = request.query_params.get("ta_user_id", None)

    # initializing needed variables
    assigned_test_ids_array = []
    filters = {}

    submitted_status_id = get_assigned_test_status_id(AssignedTestStatus.SUBMITTED)
    quit_status_id = get_assigned_test_status_id(AssignedTestStatus.QUIT)

    # orderless request
    if parameters["orderless_request"] == "true":
        # adding reference_number in the TestResultReportVW filter
        filters["reference_number"] = parameters["test_order_number"]
    # regular request
    else:
        # adding test_order_number in the TestResultReportVW filter
        filters["test_order_number"] = parameters["test_order_number"]

    # user_id parameter is provided
    if user_id is not None and user_id != "null":
        # ta_user_id is null (basically not provided)
        if ta_user_id == "null" or ta_user_id is None:
            assigned_test_ids_array = TestResultReportVW.objects.filter(
                test_id__in=parameters["test_ids"].split(","),
                test_status_id__in=(submitted_status_id, quit_status_id),
                candidate_user_id=user_id,
                **filters,
            ).values_list("assigned_test_id", flat=True)
        else:
            temp_assigned_test_ids_array = TestResultReportVW.objects.filter(
                Q(
                    test_id__in=parameters["test_ids"].split(","),
                    test_status_id__in=(
                        submitted_status_id,
                        quit_status_id,
                    ),
                    candidate_user_id=user_id,
                    **filters,
                )
                | Q(
                    test_id__in=parameters["test_ids"].split(","),
                    test_status_id__in=(
                        submitted_status_id,
                        quit_status_id,
                    ),
                    candidate_user_id=user_id,
                    # making sure that we're only getting assigned tests associated with requesting or allowed TA(s)
                    ta_user_id=ta_user_id,
                    **filters,
                )
            ).values_list("assigned_test_id", flat=True)

            # making sure that we're only getting assigned tests associated with requesting or allowed TA(s)
            assigned_test_ids_array = []
            temp_data = TestResultReportVW.objects.filter(
                assigned_test_id__in=temp_assigned_test_ids_array
            )
            for data in temp_data:
                # allowed ta user ids defined (based on test permissions) + ta_user_id is part of the array
                if data.allowed_ta_user_ids is not None and str(ta_user_id) in list(
                    data.allowed_ta_user_ids.split(",")
                ):
                    assigned_test_ids_array.append(data.assigned_test_id)
                # allowed ta user ids undefined (orderless test permissions or old data)
                else:
                    if int(data.ta_user_id) == int(ta_user_id):
                        assigned_test_ids_array.append(data.assigned_test_id)

    # user_id not provided
    else:
        # ta_user_id is null (basically not provided)
        if ta_user_id == "null" or ta_user_id is None:
            assigned_test_ids_array = TestResultReportVW.objects.filter(
                test_id__in=parameters["test_ids"].split(","),
                test_status_id__in=(submitted_status_id, quit_status_id),
                **filters,
            ).values_list("assigned_test_id", flat=True)

        else:
            temp_assigned_test_ids_array = TestResultReportVW.objects.filter(
                Q(
                    test_id__in=parameters["test_ids"].split(","),
                    test_status_id__in=(
                        submitted_status_id,
                        quit_status_id,
                    ),
                    **filters,
                )
                | Q(
                    test_id__in=parameters["test_ids"].split(","),
                    test_status_id__in=(
                        submitted_status_id,
                        quit_status_id,
                    ),
                    # making sure that we're only getting assigned tests associated with requesting or allowed TA(s)
                    ta_user_id=ta_user_id,
                    **filters,
                )
            ).values_list("assigned_test_id", flat=True)

            # making sure that we're only getting assigned tests associated with requesting or allowed TA(s)
            assigned_test_ids_array = []
            temp_data = TestResultReportVW.objects.filter(
                assigned_test_id__in=temp_assigned_test_ids_array
            )
            for data in temp_data:
                # allowed ta user ids defined (based on test permissions) + ta_user_id is part of the array
                if data.allowed_ta_user_ids is not None and str(ta_user_id) in list(
                    data.allowed_ta_user_ids.split(",")
                ):
                    assigned_test_ids_array.append(data.assigned_test_id)
                # allowed ta user ids undefined (orderless test permissions or old data)
                else:
                    if int(data.ta_user_id) == int(ta_user_id):
                        assigned_test_ids_array.append(data.assigned_test_id)

    # remove_duplicates is set to true
    if parameters["remove_duplicates"] == "true":
        # getting concerned assigned tests
        assigned_tests = TestResultReportVW.objects.filter(
            assigned_test_id__in=assigned_test_ids_array
        )

        # removing duplicates
        assigned_test_ids_without_duplicate = []
        user_ids = []

        for assigned_test in assigned_tests:
            if assigned_test.user_id not in user_ids:
                assigned_test_ids_without_duplicate.append(
                    assigned_test.assigned_test_id
                )
                user_ids.append(assigned_test.user_id)

        final_query_set = TestResultReportVW.objects.filter(
            assigned_test_id__in=assigned_test_ids_without_duplicate,
        )
    else:
        # getting concerned assigned tests
        final_query_set = TestResultReportVW.objects.filter(
            assigned_test_id__in=assigned_test_ids_array
        )

    # returning data
    data = TestResultReportDataViewSerializer(final_query_set, many=True)
    return Response(data.data)


# if you need to modify or add any field in that report, you'll need to update the view (financial_data_vw) and the FinancialReportVW model as well (reports section)
# To update the view:
#   - Create a new migration file based on 0001_create_financial_report_data_vw.py (report_models)
#   - Modify the "CREATE OR ALTER VIEW [dbo].[financial_report_vw]" part (delete the "CREATE OR ALTER FUNCTION [dbo].[status2str]" part if not needed)
# To update the model:
#   - Same as any other model, just make sure that all fields from the view are defined in the respective model
def retrieve_financial_report_data(parameters):
    submitted_status_id = get_assigned_test_status_id(AssignedTestStatus.SUBMITTED)
    quit_status_id = get_assigned_test_status_id(AssignedTestStatus.QUIT)
    # getting date parameters
    date_from = parameters["date_from"]
    date_to = parameters["date_to"]
    # generating query_set (GROUP_BY "test_id" AND "status")
    # where status is timed out, quit or submitted
    query_set = FinancialReportVW.objects.filter(
        submit_date__gte=datetime.datetime(
            int(date_from.split("-")[2]),
            int(date_from.split("-")[1]),
            int(date_from.split("-")[0]),
            0,
            0,
            0,
        ),
        submit_date__lte=datetime.datetime(
            int(date_to.split("-")[2]),
            int(date_to.split("-")[1]),
            int(date_to.split("-")[0]),
            23,
            59,
            59,
            999999,
        ),
        test_status_id__in=(
            quit_status_id,
            submitted_status_id,
        ),
    )
    # returning data
    data = FinancialReportDataViewSerializer(
        query_set.order_by(
            "order_no", "reference_number", "submit_date", "test_status_id"
        ),
        many=True,
    )
    return Response(data.data)


# getting all parent codes where there is an existing QUESTION_LIST section
#   1. No parameters provided: getting all parent codes for tests with QUESTION_LIST
#   2. parent_code parameter provided: getting all tests related to provided parameters
#   3. parent_code & test_code parameter provided: getting all test versions related to provided parameters
def retrieve_test_definition_data(request):
    # optional parameters
    parent_code = request.query_params.get("parent_code", None)
    test_code = request.query_params.get("test_code", None)
    # no parameters provided
    if not parent_code and not test_code:
        # getting test section components where component type is QUESTION_LIST or ITEM_BANK
        test_section_components = TestSectionComponent.objects.filter(
            Q(component_type=TestSectionComponentType.QUESTION_LIST)
            | Q(component_type=TestSectionComponentType.ITEM_BANK)
        ).order_by("order")
        # initializing test definition ids array
        test_definition_ids_array = []
        # looping in test section components
        for test_section_component in test_section_components:
            # getting test sections related to current test section component
            test_sections = TestSection.objects.filter(
                testsectioncomponent=test_section_component
            )
            # adding related test definition id to test_definition_ids_array
            test_definition_ids_array.append(test_sections.first().test_definition_id)
        # getting all tests based on found test_definition_ids_array
        tests = TestDefinition.objects.filter(id__in=test_definition_ids_array)
        # removing duplicates
        tests_without_duplicates = []
        parent_codes_without_duplicates = []
        for test in tests:
            if test.parent_code not in parent_codes_without_duplicates:
                parent_codes_without_duplicates.append(test.parent_code)
                tests_without_duplicates.append(test)
        data = TestDefinitionSerializer(tests_without_duplicates, many=True)
        return Response(data.data)
    # cannot provide only test_code parameter
    if not parent_code and test_code:
        return Response(
            {"error": "parent_code parameter is missing"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    # parent_code parameter provided
    if parent_code and not test_code:
        # getting test codes based on parent_code provided
        tests = TestDefinition.objects.filter(parent_code=parent_code)
        # removing duplicates
        tests_without_duplicates = []
        test_codes_without_duplicates = []
        for test in tests:
            if test.test_code not in test_codes_without_duplicates:
                test_codes_without_duplicates.append(test.test_code)
                tests_without_duplicates.append(test)
        data = TestDefinitionSerializer(tests_without_duplicates, many=True)
        return Response(data.data)
    # parent_code and test_code parameters provided
    if parent_code and test_code:
        # getting test versions based on parent_code and test_code provided
        test_versions = TestDefinition.objects.filter(
            parent_code=parent_code, test_code=test_code
        )
        data = TestDefinitionSerializer(test_versions, many=True)
        return Response(data.data)


def retrieve_test_content_report_data(parameters):
    # getting test definition based on provided parameters
    test_definition = TestDefinition.objects.get(
        parent_code=parameters["parent_code"],
        test_code=parameters["test_code"],
        version=parameters["version"],
    )
    # no test found
    if not test_definition:
        return Response(
            {"error": "the specified test does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    # initializing questions_combined
    questions_combined = []
    # getting test sections based on test definition
    test_sections = TestSection.objects.filter(test_definition_id=test_definition.id)
    # looping in test sections
    for test_section in test_sections:
        # getting test section components of type QUESTION_LIST based on test sections
        test_section_components = TestSectionComponent.objects.filter(
            Q(test_section=test_section.id)
            & (
                Q(component_type=TestSectionComponentType.QUESTION_LIST)
                | Q(component_type=TestSectionComponentType.ITEM_BANK)
            )
        ).order_by("order")
        # getting non-empty querysets
        if test_section_components:
            # getting the questions
            questions = NewQuestion.objects.filter(
                test_section_component_id=test_section_components.first().id
            )
            questions_combined.append(questions)
    # building queryset with all question sections
    queryset = []
    for questions_block in questions_combined:
        queryset += questions_block
    # returning data
    data = TestContentReportDataSerializer(
        queryset,
        many=True,
        context={
            "parent_code": parameters["parent_code"],
            "test_code": parameters["test_code"],
            "version": parameters["version"],
        },
    )
    return Response(data.data)


def retrieve_test_taker_report_data(parameters):
    submitted_status_id = get_assigned_test_status_id(AssignedTestStatus.SUBMITTED)
    quit_status_id = get_assigned_test_status_id(AssignedTestStatus.QUIT)
    # getting date parameters
    date_from = parameters["date_from"]
    date_to = parameters["date_to"]
    # getting test definition IDs based on provided parent_code and test_code
    tests = TestDefinition.objects.filter(
        parent_code=parameters["parent_code"], test_code=parameters["test_code"]
    )
    test_definition_ids = []
    for test in tests:
        test_definition_ids.append(test.id)
    # getting all scored tests (where status >= 19) and where dates are between the ones provided
    scored_tests = AssignedTest.objects.filter(
        status_id__in=(submitted_status_id, quit_status_id),
        test_id__in=test_definition_ids,
        submit_date__gte=datetime.datetime(
            int(date_from.split("-")[2]),
            int(date_from.split("-")[1]),
            int(date_from.split("-")[0]),
            0,
            0,
            0,
        ),
        submit_date__lte=datetime.datetime(
            int(date_to.split("-")[2]),
            int(date_to.split("-")[1]),
            int(date_to.split("-")[0]),
            23,
            59,
            59,
            999999,
        ),
    )
    # at least one test found
    if scored_tests:
        # returning data
        data = TestTakerReportDataSerializer(scored_tests, many=True)
        return Response(data.data)
    # no tests found
    else:
        return Response(
            {"error": "No tests found based on provided data"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def retrieve_ta_history_report_data(parameters):

    tempData = TaHistoryReportVW.objects.filter(
        Q(pri=parameters["pri"]) | Q(military_number=parameters["military_number"])
    ).order_by("test_submit_date")

    # at least one test found
    if tempData:
        # returning data
        data = TaHistoryReportDataSerializer(tempData, many=True)
        return Response(data.data)

    # no tests found
    else:
        return Response(
            {"error": "No tests found based on provided data"},
            status=status.HTTP_404_NOT_FOUND,
        )


class HistoricalDataOrigin:
    ASSIGNED_TEST = "ASSIGNED_TEST"
    ASSIGNED_TEST_SECTION_ACCESS_TIME = "ASSIGNED_TEST_SECTION_ACCESS_TIME"
    CANDIDATE_ANSWERS = "CANDIDATE_ANSWERS"
    MULTIPLE_CHOICE_ANSWER = "MULTIPLE_CHOICE_ANSWER"


def retrieve_candidate_actions_report_data(parameters):
    # getting historical assigned test data
    assigned_test_historical_data = AssignedTest.history.filter(
        id=parameters["assigned_test_id"]
    )

    # getting assigned test sections
    assigned_test_section = AssignedTestSection.history.filter(
        assigned_test_id=parameters["assigned_test_id"]
    )
    # assigned test section is defined
    if assigned_test_section:
        # only getting IDs
        assigned_test_sections_ids = assigned_test_section.values_list("id")

        # getting historical assigned test section access time data
        assigned_test_sections_historical_data = (
            AssignedTestSectionAccessTimes.history.filter(
                assigned_test_section_id__in=assigned_test_sections_ids
            )
        )
        # getting historical candidate answers data
        questions_historical_data = CandidateAnswers.history.filter(
            assigned_test_id=parameters["assigned_test_id"]
        )
        # getting candidate answer IDs
        candidate_answer_ids = CandidateAnswers.objects.filter(
            assigned_test_id=parameters["assigned_test_id"]
        ).values_list("id")
        # getting historical candidate multiple choice answers data
        candidate_multiple_choice_answers_historical_data = (
            CandidateMultipleChoiceAnswers.history.filter(
                candidate_answers_id__in=candidate_answer_ids
            )
        )
    # no assigned test section test (should only happen for tests from Jmeter execution)
    else:
        assigned_test_sections_historical_data = []
        questions_historical_data = []
        candidate_multiple_choice_answers_historical_data = []

    # creating historical_data_array
    historical_data_array = []
    for data in assigned_test_historical_data:
        data.origin = HistoricalDataOrigin.ASSIGNED_TEST
        historical_data_array.append(data)
    for data in assigned_test_sections_historical_data:
        data.origin = HistoricalDataOrigin.ASSIGNED_TEST_SECTION_ACCESS_TIME
        historical_data_array.append(data)
    for data in questions_historical_data:
        data.origin = HistoricalDataOrigin.CANDIDATE_ANSWERS
        historical_data_array.append(data)
    for data in candidate_multiple_choice_answers_historical_data:
        data.origin = HistoricalDataOrigin.MULTIPLE_CHOICE_ANSWER
        historical_data_array.append(data)

    # sorting historical_data_array by history_date
    historical_data_array.sort(key=lambda x: x.history_date, reverse=False)

    # initializing final_historical_data array
    final_historical_data = []

    # looping in historical_data_array
    for data in historical_data_array:
        # initializing needed variables
        # generic variables
        origin = ""
        history_date = data.history_date
        history_type = data.history_type
        # assigned test specific variables
        status_id = ""
        status_codename = ""
        previous_status_id = ""
        previous_status_codename = ""
        start_date = ""
        submit_date = ""
        test_access_code = ""
        total_score = ""
        ta_user_id = ""
        test_session_language_id = ""
        en_converted_score = ""
        fr_converted_score = ""
        uit_invite_id = ""
        is_invalid = ""
        # assigned test section access time specific variables
        time_type = ""
        assigned_test_section_id = ""
        # candidate answers specific variables
        candidate_answer_id = ""
        mark_for_review = ""
        question_id = ""
        selected_language_id = ""
        # candidate multiple choice answers specific variables
        answer_id = ""
        candidate_answer_id_ref = ""

        # Origin: ASSIGNED_TEST
        if data.origin == HistoricalDataOrigin.ASSIGNED_TEST:
            origin = HistoricalDataOrigin.ASSIGNED_TEST
            status_id = data.status_id
            status_codename = get_assigned_test_status_codename(data.status_id)
            previous_status_id = data.previous_status_id
            previous_status_codename = get_assigned_test_status_codename(
                data.previous_status_id
            )
            start_date = data.start_date
            submit_date = data.submit_date
            test_access_code = data.test_access_code
            total_score = data.total_score
            ta_user_id = data.ta_user_id
            test_session_language_id = data.test_session_language_id
            en_converted_score = data.en_converted_score
            fr_converted_score = data.fr_converted_score
            uit_invite_id = data.uit_invite_id
            is_invalid = data.is_invalid
        # Origin: ASSIGNED_TEST_SECTION_ACCESS_TIME
        elif data.origin == HistoricalDataOrigin.ASSIGNED_TEST_SECTION_ACCESS_TIME:
            origin = HistoricalDataOrigin.ASSIGNED_TEST_SECTION_ACCESS_TIME
            time_type = data.time_type
            assigned_test_section_id = data.assigned_test_section_id
        # Origin: CANDIDATE_ANSWERS
        elif data.origin == HistoricalDataOrigin.CANDIDATE_ANSWERS:
            origin = HistoricalDataOrigin.CANDIDATE_ANSWERS
            candidate_answer_id = data.id
            mark_for_review = data.mark_for_review

            # if we don't have the question_id, we probably have an item bank's item_id
            if data.question_id:
                question_id = data.question_id
            else:
                question_id = data.item_id

            selected_language_id = data.selected_language_id
        # Origin: MULTIPLE_CHOICE_ANSWER
        elif data.origin == HistoricalDataOrigin.MULTIPLE_CHOICE_ANSWER:
            origin = HistoricalDataOrigin.MULTIPLE_CHOICE_ANSWER

            # if we don't have the answer_id, we probably have an item bank's item_answer_id
            if data.answer_id:
                answer_id = data.answer_id
            else:
                answer_id = data.item_answer_id

            candidate_answer_id_ref = data.candidate_answers_id

        # adding data to final_historical_data array
        final_historical_data.append(
            {
                # generic
                "origin": origin,
                "history_date": history_date,
                "history_type": history_type,
                # assigned test related
                "status_id": status_id,
                "status_codename": status_codename,
                "previous_status_id": previous_status_id,
                "previous_status_codename": previous_status_codename,
                "start_date": start_date,
                "submit_date": submit_date,
                "submit_date": submit_date,
                "test_access_code": test_access_code,
                "total_score": total_score,
                "ta_user_id": ta_user_id,
                "test_session_language_id": test_session_language_id,
                "en_converted_score": en_converted_score,
                "fr_converted_score": fr_converted_score,
                "uit_invite_id": uit_invite_id,
                "is_invalid": is_invalid,
                # assigned test section access time related
                "time_type": time_type,
                "assigned_test_section_id": assigned_test_section_id,
                # candidate answers related
                "candidate_answer_id": candidate_answer_id,
                "mark_for_review": mark_for_review,
                "question_id": question_id,
                "selected_language_id": selected_language_id,
                # multiple choice answers related
                "answer_id": answer_id,
                "candidate_answer_id_ref": candidate_answer_id_ref,
            }
        )

    return Response(final_historical_data)


# if you need to modify or add any field in that report, you'll need to update the view (adapted_tests_report_vw) and the AdaptedTestsReportVW model as well (reports section)
def retrieve_adapted_tests_report_data(parameters):
    # getting date parameters
    date_from = parameters["date_from"]
    date_to = parameters["date_to"]
    # generating query_set (GROUP_BY "test_id" AND "status")
    # where status is timed out, quit or submitted
    query_set = AdaptedTestsReportVW.objects.filter(
        test_start_time__gte=datetime.datetime(
            int(date_from.split("-")[2]),
            int(date_from.split("-")[1]),
            int(date_from.split("-")[0]),
            0,
            0,
            0,
        ),
        test_start_time__lte=datetime.datetime(
            int(date_to.split("-")[2]),
            int(date_to.split("-")[1]),
            int(date_to.split("-")[0]),
            23,
            59,
            59,
            999999,
        ),
    )
    # returning data
    data = AdaptedTestsReportDataViewSerializer(
        query_set.order_by("candidate_last_name"),
        many=True,
    )
    return Response(data.data)


def retrieve_item_bank_report_data(request):
    success, parameters = get_needed_parameters(["item_bank_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    item_bank_id = itemgetter("item_bank_id")(parameters)

    # getting all realted latest version items
    latest_versions_items = ItemLatestVersionsDataVW.objects.filter(
        item_bank_id=item_bank_id
    )

    # ordering items by custom System ID (ascending)
    ordered_items = sorted(
        latest_versions_items,
        # converting second part of string (incrementing value) to int, so the ordering is respected
        key=lambda k: int(k.system_id.split("_")[1]),
        reverse=False,
    )

    # getting serialized item banks data
    serialized_ordered_items_data = ItemLatestVersionsDataViewSerializer(
        ordered_items,
        many=True,
    ).data

    # returning serialized_ordered_items_data
    return Response(serialized_ordered_items_data)
