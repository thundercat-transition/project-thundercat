from rest_framework import permissions
from rest_framework.views import APIView
from backend.api_permissions.role_based_api_permissions import (
    HasAaePermission,
    HasScorerPermission,
    HasSystemAdminPermission,
    HasTestCenterManagerPermission,
)

from backend.views.aae import (
    get_test_centers_for_user_accommodation_request,
    get_user_accommodation_file_complexity_options,
    send_user_accommodation_request,
    check_for_ongoing_user_accommodation_request_process,
    get_all_user_accommodation_files,
    get_found_user_accommodation_files,
    get_selected_user_accommodation_file_data,
    get_accommodation_file_data_for_details_popup,
    get_accommodation_file_available_reports,
    create_user_accommodation_file_comment,
    delete_user_accommodation_file_comment,
    get_available_tests_to_administer_for_selected_user_accommodation_file,
    get_user_accommodation_file_status_options,
    save_selected_user_accommodation_file_data,
    trigger_selected_user_accommodation_file_data_action,
    trigger_user_accommodation_file_status_update_as_a_candidate,
    get_all_pending_user_accommodation_files_as_a_tcm,
    get_found_pending_user_accommodation_files_as_a_tcm,
    get_number_of_requests_received_aae_report_data,
    get_number_of_closed_requests_aae_report_data,
)


class GetTestCentersForUserAccommodationRequest(APIView):
    def get(self, request):
        return get_test_centers_for_user_accommodation_request(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class SendUserAccommodationRequest(APIView):
    def post(self, request):
        return send_user_accommodation_request(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class CheckForOngoinUserAccommodationRequestProcess(APIView):
    def get(self, request):
        return check_for_ongoing_user_accommodation_request_process(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetAllUserAccommodationFiles(APIView):
    def get(self, request):
        return get_all_user_accommodation_files(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAaePermission()]


class GetFoundUserAccommodationFiles(APIView):
    def get(self, request):
        return get_found_user_accommodation_files(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAaePermission()]


class GetSelectedUserAccommodationFileData(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasAaePermission | HasSystemAdminPermission | HasScorerPermission),
    )

    def get(self, request):
        return get_selected_user_accommodation_file_data(request)


class GetAccommodationFileDataForDetailsPopup(APIView):
    def get(self, request):
        return get_accommodation_file_data_for_details_popup(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetAccommodationFileAvailableReports(APIView):
    def get(self, request):
        return get_accommodation_file_available_reports(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class CreateUserAccommodationFileComment(APIView):
    def post(self, request):
        return create_user_accommodation_file_comment(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAaePermission()]


class DeleteUserAccommodationFileComment(APIView):
    def post(self, request):
        return delete_user_accommodation_file_comment(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAaePermission()]


class GetAvailableTestsToAdministerForSelectedUserAccommodationFile(APIView):
    def get(self, request):
        return get_available_tests_to_administer_for_selected_user_accommodation_file(
            request
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAaePermission()]


class GetUserAccommodationFileStatusOptions(APIView):
    def get(self, request):
        return get_user_accommodation_file_status_options(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAaePermission()]


class GetUserAccommodationFileComplexityOptions(APIView):
    def get(self, request):
        return get_user_accommodation_file_complexity_options(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAaePermission()]


class SaveSelectedUserAccommodationFileData(APIView):
    def post(self, request):
        return save_selected_user_accommodation_file_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAaePermission()]


class TriggerSelectedUserAccommodationFileDataAction(APIView):
    def post(self, request):
        return trigger_selected_user_accommodation_file_data_action(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAaePermission()]


class TriggerUserAccommodationFileStatusUpdateAsACandidate(APIView):
    def post(self, request):
        return trigger_user_accommodation_file_status_update_as_a_candidate(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetAllPendingUserAccommodationFilesAsATcm(APIView):
    def get(self, request):
        return get_all_pending_user_accommodation_files_as_a_tcm(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestCenterManagerPermission()]


class GetFoundPendingUserAccommodationFilesAsATcm(APIView):
    def get(self, request):
        return get_found_pending_user_accommodation_files_as_a_tcm(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestCenterManagerPermission()]


class GetNumberOfRequestsReceivedAaeReportData(APIView):
    def get(self, request):
        return get_number_of_requests_received_aae_report_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAaePermission()]


class GetNumberOfClosedRequestsAaeReportData(APIView):
    def get(self, request):
        return get_number_of_closed_requests_aae_report_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAaePermission()]
