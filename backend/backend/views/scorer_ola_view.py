from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.scorer_ola import (
    get_scorer_ola_tests_to_assess_as_supervisor,
    get_available_test_assessors_as_supervisor,
    cancel_test_session_as_supervisor,
    get_scorer_ola_test_session_skip_options,
    get_highest_priority_candidate_to_assign_as_scorer_ola,
    assign_candidate_as_scorer_ola,
    unassign_candidate_as_scorer_ola,
    get_scorer_ola_my_assigned_tests,
    get_scorer_ola_assigned_candidate_details,
    get_scorer_ola_my_availability_data,
)
from backend.api_permissions.role_based_api_permissions import HasScorerPermission


class GetScorerOlaTestsToAssessAsSupervisor(APIView):
    def get(self, request):
        return get_scorer_ola_tests_to_assess_as_supervisor(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasScorerPermission()]


class GetAvailableTestAssessorsAsSupervisor(APIView):
    def get(self, request):
        return get_available_test_assessors_as_supervisor(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasScorerPermission()]


class CancelTestSessionAsSupervisor(APIView):
    def post(self, request):
        return cancel_test_session_as_supervisor(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasScorerPermission()]


class GetScorerOlaTestSessionSkipOptions(APIView):
    def get(self, request):
        return get_scorer_ola_test_session_skip_options(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasScorerPermission()]


class GetHighestPriorityCandidateToAssignAsScorerOla(APIView):
    def get(self, request):
        return get_highest_priority_candidate_to_assign_as_scorer_ola(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasScorerPermission()]


class AssignCandidateAsScorerOla(APIView):
    def post(self, request):
        return assign_candidate_as_scorer_ola(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasScorerPermission()]


class UnassignCandidateAsScorerOla(APIView):
    def post(self, request):
        return unassign_candidate_as_scorer_ola(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasScorerPermission()]


class GetScorerOlaMyAssignedTests(APIView):
    def get(self, request):
        return get_scorer_ola_my_assigned_tests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasScorerPermission()]


class GetScorerOlaAssignedCandidateDetails(APIView):
    def get(self, request):
        return get_scorer_ola_assigned_candidate_details(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasScorerPermission()]


class GetScorerOlaMyAvailabilityData(APIView):
    def get(self, request):
        return get_scorer_ola_my_availability_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasScorerPermission()]
