from rest_framework.views import APIView
from rest_framework import permissions
from backend.api_permissions.role_based_api_permissions import (
    HasSystemAdminPermission,
)
from backend.views.retrieve_all_users import (
    retrieve_all_users,
    retrieve_all_found_users,
)


class GetAllUsers(APIView):
    def get(self, request):
        return retrieve_all_users(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


class GetAllFoundUsers(APIView):
    def get(self, request):
        return retrieve_all_found_users(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]
