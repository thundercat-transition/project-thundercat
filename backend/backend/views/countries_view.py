from rest_framework.response import Response
from backend.api_permissions.role_based_api_permissions import (
    HasTestCenterManagerPermission,
)
from backend.serializers.cat_ref_country_serializer import CatRefCountrySerializer
from backend.ref_table_views.cat_ref_country_vw import CatRefCountryVW
from rest_framework.views import APIView
from rest_framework import permissions

# This is a representation of the supported list of countries
LIST_OF_SUPPORTED_COUNTRIES_EABRV = ["Canada", "USA"]


# View for a specific list of supported countries
class GetListOfSupportedCountries(APIView):
    def get(self, request):
        return get_list_of_supported_countries()

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


# Getting list of countries based on the list of supported legacy_cd
def get_list_of_supported_countries():
    supported_countries = CatRefCountryVW.objects.filter(
        eabrv__in=LIST_OF_SUPPORTED_COUNTRIES_EABRV
    )

    serialized_supported_countries = CatRefCountrySerializer(
        supported_countries, many=True
    ).data

    return Response(serialized_supported_countries)


# View for a specific list of supported countries
class GetListOfCountries(APIView):
    def get(self, request):
        return get_list_of_countries()

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


# Getting list of countries based on the list of supported legacy_cd
def get_list_of_countries():
    countries = CatRefCountryVW.objects.all()

    serialized_countries = CatRefCountrySerializer(countries, many=True).data

    return Response(serialized_countries)
