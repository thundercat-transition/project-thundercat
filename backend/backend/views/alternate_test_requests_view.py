from rest_framework import permissions
from rest_framework.views import APIView
from backend.api_permissions.role_based_api_permissions import (
    HasSystemAdminPermission,
)

from backend.views.alternate_test_requests import (
    get_all_pending_alternate_test_requests,
    get_found_pending_alternate_test_requests,
    submit_pending_alternate_test_requests,
    cancel_pending_alternate_test_requests,
    get_all_completed_alternate_test_requests,
    get_found_completed_alternate_test_requests,
)


class GetAllPendingAlternateTestRequests(APIView):
    def get(self, request):
        return get_all_pending_alternate_test_requests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


class GetFoundPendingAlternateTestRequests(APIView):
    def get(self, request):
        return get_found_pending_alternate_test_requests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


class SubmitPendingAlternateTestRequest(APIView):
    def post(self, request):
        return submit_pending_alternate_test_requests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


class CancelPendingAlternateTestRequest(APIView):
    def post(self, request):
        return cancel_pending_alternate_test_requests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


class GetAllCompletedAlternateTestRequests(APIView):
    def get(self, request):
        return get_all_completed_alternate_test_requests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


class GetFoundCompletedAlternateTestRequests(APIView):
    def get(self, request):
        return get_found_completed_alternate_test_requests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]
