from rest_framework import permissions
from rest_framework.views import APIView

from backend.views.assessment_process import (
    get_available_test_sessions_for_specific_test_to_administer,
    create_assessment_process,
    delete_assessment_process,
    get_assessment_process_reference_numbers,
    get_assessment_process_data,
    assessment_process_assign_candidate,
    get_all_assessment_process_results_data,
    get_found_assessment_process_results_data,
    get_assessment_process_assigned_candidates,
    edit_assessment_process_assigned_candidate,
    delete_assessment_process_assigned_candidate,
    send_assessment_process_assigned_candidates_request,
    update_non_sent_assessment_process_data,
    send_assessment_process_updates_request,
    invite_single_assessment_process_candidate_request,
    unassign_assessment_process_assigned_test_to_administer,
    get_assessment_process_results_candidates,
    get_assessment_process_results_candidates_report,
    update_reference_number,
)

from backend.api_permissions.role_based_api_permissions import (
    HasHRCoordinatorPermission,
)


# getting available test sessions for a specific test to administer
class GetAvailableTestSessionsForSpecificTestToAdminister(APIView):
    def get(self, request):
        return get_available_test_sessions_for_specific_test_to_administer(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# create an assessment process for current HR Coordinator user
class CreateAssessmentProcess(APIView):
    def post(self, request):
        return create_assessment_process(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# delete an assessment process for current HR Coordinator user
class DeleteAssessmentProcess(APIView):
    def post(self, request):
        return delete_assessment_process(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# getting assessment process reference numbers related to current HR Coordinator user
class GetAssessmentProcessReferenceNumbers(APIView):
    def get(self, request):
        return get_assessment_process_reference_numbers(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# getting assessment process data based on provided reference number related to current HR Coordinator user
class GetAssessmentProcessData(APIView):
    def get(self, request):
        return get_assessment_process_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# assign candidate to respective assessment process
class AssessmentProcessAssignCandidate(APIView):
    def post(self, request):
        return assessment_process_assign_candidate(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# getting all assessment process results related to current HR Coordinator user
class GetAllAssessmentProcessResults(APIView):
    def get(self, request):
        return get_all_assessment_process_results_data(request)


# getting assessment process assigned candidates
class GetAssessmentProcessAssignedCandidates(APIView):
    def get(self, request):
        return get_assessment_process_assigned_candidates(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# getting assessment process' results for all assigned candidates
class GetAssessmentProcessResultsCandidates(APIView):
    def get(self, request):
        return get_assessment_process_results_candidates(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# getting assessment process' results for candidates report
class GetAssessmentProcessResultsCandidatesReport(APIView):
    def get(self, request):
        return get_assessment_process_results_candidates_report(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# getting found assessment process results related to current HR Coordinator user
class GetFoundAssessmentProcessResults(APIView):
    def get(self, request):
        return get_found_assessment_process_results_data(request)


# edit assigned candidate from respective assessment process
class EditAssessmentProcessAssignedCandidate(APIView):
    def post(self, request):
        return edit_assessment_process_assigned_candidate(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# delete assigned candidate from respective assessment process
class DeleteAssessmentProcessAssignedCandidate(APIView):
    def post(self, request):
        return delete_assessment_process_assigned_candidate(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# send assessment process assigned candidates request
class SendAssessmentProcessAssignedCandidatesRequest(APIView):
    def post(self, request):
        return send_assessment_process_assigned_candidates_request(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# update non sent assessment process data
class UpdateNonSentAssessmentProcessData(APIView):
    def post(self, request):
        return update_non_sent_assessment_process_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# send assessment process updates request
class SendAssessmentProcessUpdatesRequest(APIView):
    def post(self, request):
        return send_assessment_process_updates_request(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# invite single assessment process candidate request
class InviteSingleAssessmentProcessCandidateRequest(APIView):
    def post(self, request):
        return invite_single_assessment_process_candidate_request(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


# unassign assessment process assigned test to administer
class UnassignAssessmentProcessAssignedTestToAdminister(APIView):
    def post(self, request):
        return unassign_assessment_process_assigned_test_to_administer(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]


class UpdateReferenceNumber(APIView):
    def post(self, request):
        return update_reference_number(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasHRCoordinatorPermission()]
