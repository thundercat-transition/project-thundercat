from rest_framework.response import Response
from cms.views.utils import get_optional_parameters
from backend.serializers.criticality_serializer import CriticalitySerializer
from backend.custom_models.criticality import Criticality


def get_criticality_options(request):
    optional_parameters = get_optional_parameters(["id"], request)

    # ID is provided
    if optional_parameters["id"] is not None:
        # getting specific active criticality option
        criticality_options = Criticality.objects.get(id=optional_parameters["id"])
        # getting serialized data
        serialized_data = CriticalitySerializer(criticality_options, many=False).data
    # ID is not provided
    else:
        # getting active criticality options
        criticality_options = Criticality.objects.filter(active=1)
        # getting serialized data
        serialized_data = CriticalitySerializer(criticality_options, many=True).data

    return Response(serialized_data)
