from operator import itemgetter
from django.utils import timezone
from rest_framework.response import Response
from rest_framework import status
from backend.custom_models.candidate_multiple_choice_answers import (
    CandidateMultipleChoiceAnswers,
)
from backend.custom_models.candidate_answers import CandidateAnswers
from cms.views.utils import get_needed_parameters


def update_multiple_choice_answers_modify_date(request):
    success, parameters = get_needed_parameters(
        ["assigned_test_id", "question_id", "test_section_component_id"], request
    )

    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    assigned_test_id, question_id, test_section_component_id = itemgetter(
        "assigned_test_id", "question_id", "test_section_component_id"
    )(parameters)

    candidate_answer = CandidateAnswers.objects.raw(
        """ SELECT id AS 'id'
            FROM custom_models_candidateanswers 
            WHERE assigned_test_id={0} AND question_id={1} AND test_section_component_id={2}
        """.format(
            assigned_test_id, question_id, test_section_component_id
        ),
    )

    # if it exists
    if candidate_answer:
        candidate_answer = candidate_answer[0]
        # getting candidate multiple choice answer based on provided parameters
        candidate_multiple_choice_answer = (
            CandidateMultipleChoiceAnswers.objects.filter(
                candidate_answers_id=candidate_answer.id
            )
        )

        # if it exists
        if candidate_multiple_choice_answer:
            # update modify date
            candidate_multiple_choice_answer = candidate_multiple_choice_answer[0]
            candidate_multiple_choice_answer.modify_date = timezone.now()
            candidate_multiple_choice_answer.save()
            # returning success
            return Response(status=status.HTTP_200_OK)

    return Response(
        {"info": "Did not find any candidate multiple choice answers to update"},
        status=status.HTTP_200_OK,
    )
