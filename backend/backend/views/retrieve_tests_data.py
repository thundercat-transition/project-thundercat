from operator import itemgetter
from rest_framework.response import Response
from django.db.models import Q
from rest_framework import status
from text_resources_backend.text_resources import TextResources
from backend.static.assigned_test_status import AssignedTestStatus
from db_views.db_view_models.user_look_up_tests_vw import UserLookUpTestsVW
from db_views.serializers.assigned_tests_serializers import (
    ScoredTestsViewSerializer,
    UserLookUpTestsViewSerializer,
)
from db_views.db_view_models.scored_tests_vw import ScoredTestsVW
from backend.views.utils import get_user_info_from_jwt_token
from cms.views.utils import get_needed_parameters
from user_management.views.utils import CustomPagination


# retrieving candidate' active scored tests
def retrieve_valid_scored_tests(request):
    user_info = get_user_info_from_jwt_token(request)
    # TODO: this will need to be updated when we'll have more test types,
    # since a submitted tests does not necessary mean that the test is scored

    # getting scored tests
    scored_tests = ScoredTestsVW.objects.filter(
        candidate_user_id=user_info["user_id"], is_most_recent_valid_test=1
    ).order_by("-start_date")

    # serializing the data
    serialized_data = ScoredTestsViewSerializer(scored_tests, many=True).data

    # returning data
    return Response(serialized_data)


# retrieving candidate' active scored tests
def retrieve_archived_scored_tests(request):
    user_info = get_user_info_from_jwt_token(request)
    # TODO: this will need to be updated when we'll have more test types,
    # since a submitted tests does not necessary mean that the test is scored

    # getting scored tests
    scored_tests = ScoredTestsVW.objects.filter(
        candidate_user_id=user_info["user_id"], is_most_recent_valid_test=0
    ).order_by("-start_date")

    # serializing the data
    serialized_data = ScoredTestsViewSerializer(scored_tests, many=True).data

    # returning data
    return Response(serialized_data)


# retrieving All Tests for a selected user
def retrieve_all_tests(request):
    success, parameters = get_needed_parameters(
        ["user_id", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    user_id, page, page_size = itemgetter("user_id", "page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting all related tests
    all_tests = UserLookUpTestsVW.objects.filter(candidate_user_id=user_id).order_by(
        "-submit_date", "-start_date"
    )

    # serializing data
    serialized_data = UserLookUpTestsViewSerializer(all_tests, many=True).data

    # only getting data for current selected page
    new_serialized_data = serialized_data[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_serialized_data,
        len(all_tests),
        current_page,
        page_size,
    )


# retrieving All Tests for a selected user
def retrieve_found_tests(request):
    success, parameters = get_needed_parameters(
        ["user_id", "keyword", "current_language", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    user_id, keyword, current_language, page, page_size = itemgetter(
        "user_id", "keyword", "current_language", "page", "page_size"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        # getting all related tests
        found_tests = UserLookUpTestsVW.objects.filter(
            candidate_user_id=user_id
        ).order_by("-submit_date", "-start_date")
    # regular search
    else:
        # initializing matching_status_codenames
        matching_status_codenames = []

        # populating matching_status_codenames array based on matching status
        if (
            keyword.lower()
            in TextResources.assigned_test_status["checkedIn"][current_language].lower()
        ):
            matching_status_codenames.append(AssignedTestStatus.CHECKED_IN)
        if (
            keyword.lower()
            in TextResources.assigned_test_status["preTest"][current_language].lower()
        ):
            matching_status_codenames.append(AssignedTestStatus.PRE_TEST)
        if (
            keyword.lower()
            in TextResources.assigned_test_status["active"][current_language].lower()
        ):
            matching_status_codenames.append(AssignedTestStatus.ACTIVE)
        if (
            keyword
            in TextResources.assigned_test_status["transition"][
                current_language
            ].lower()
        ):
            matching_status_codenames.append(AssignedTestStatus.TRANSITION)
        if (
            keyword.lower()
            in TextResources.assigned_test_status["locked"][current_language].lower()
        ):
            matching_status_codenames.append(AssignedTestStatus.LOCKED)
        if (
            keyword.lower()
            in TextResources.assigned_test_status["paused"][current_language].lower()
        ):
            matching_status_codenames.append(AssignedTestStatus.PAUSED)
        if (
            keyword.lower()
            in TextResources.assigned_test_status["quit"][current_language].lower()
        ):
            matching_status_codenames.append(AssignedTestStatus.QUIT)
        if (
            keyword.lower()
            in TextResources.assigned_test_status["submitted"][current_language].lower()
        ):
            matching_status_codenames.append(AssignedTestStatus.SUBMITTED)
        if (
            keyword
            in TextResources.assigned_test_status["unassigned"][
                current_language
            ].lower()
        ):
            matching_status_codenames.append(AssignedTestStatus.UNASSIGNED)

        # getting matching tests
        found_tests = UserLookUpTestsVW.objects.filter(
            Q(candidate_user_id=user_id)
            & (
                Q(test_code__icontains=keyword)
                | Q(simplified_start_date__icontains=keyword)
                | Q(status_codename__in=matching_status_codenames)
            )
        ).order_by("-submit_date", "-start_date")

    # no results found
    if not found_tests:
        return Response({"no results found"}, status=status.HTTP_200_OK)

    # serializing data
    serialized_data = UserLookUpTestsViewSerializer(found_tests, many=True).data

    # only getting data for current selected page
    new_serialized_data = serialized_data[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_serialized_data,
        len(found_tests),
        current_page,
        page_size,
    )
