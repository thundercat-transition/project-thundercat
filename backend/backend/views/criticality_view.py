from rest_framework import permissions
from rest_framework.views import APIView
from backend.api_permissions.role_based_api_permissions import HasSystemAdminPermission
from backend.views.criticality import get_criticality_options

# create new system alert
class GetCriticalityOptions(APIView):
    def get(self, request):
        return get_criticality_options(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]
