from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.mark_for_review import mark_for_review, Action


class MarkForReviewView(APIView):
    def post(self, request):
        needed_parameters = [
            "assigned_test_id",
            "question_id",
            "test_section_component_id",
        ]
        return mark_for_review(request, needed_parameters, Action.UPDATE)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
