from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.retrieve_tests_data import (
    retrieve_valid_scored_tests,
    retrieve_archived_scored_tests,
    retrieve_all_tests,
    retrieve_found_tests,
)
from backend.api_permissions.role_based_api_permissions import (
    HasSystemAdminPermission,
)


class GetValidScoredTests(APIView):
    def get(self, request):
        return retrieve_valid_scored_tests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetArchivedScoredTests(APIView):
    def get(self, request):
        return retrieve_archived_scored_tests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetAllTestsForSelectedUser(APIView):
    def get(self, request):
        return retrieve_all_tests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


class GetFoundTestsForSelectedUser(APIView):
    def get(self, request):
        return retrieve_found_tests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]
