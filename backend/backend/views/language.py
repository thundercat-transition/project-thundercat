from rest_framework.response import Response
from backend.serializers.language_serializer import LanguageSerializer
from backend.custom_models.language import Language


def get_language_data(request):
    # getting all language data
    language_data = Language.objects.all()

    # getting serialized data
    serialized_data = LanguageSerializer(language_data, many=True).data

    return Response(serialized_data)
