from operator import itemgetter
from django.db import transaction
from django.db.models import Q
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from backend.api_permissions.role_based_api_permissions import (
    HasScorerPermission,
    HasTestCenterManagerPermission,
)
from db_views.serializers.scorer_ola_serializers import (
    TestCenterOlaTestAssessorUnavailabilityViewSerializer,
)
from db_views.db_view_models.test_center_ola_test_assessor_unavailability_vw import (
    TestCenterOlaTestAssessorUnavailabilityVw,
)
from backend.custom_models.test_center import TestCenter
from backend.serializers.test_center_serializer import TestCenterSerializer
from backend.serializers.test_center_ola_serializer import (
    UserAssessorSerializer,
)
from backend.custom_models.test_center_ola_assessor_unavailability import (
    TestCenterOlaAssessorUnavailability,
)
from backend.custom_models.test_center_ola_test_assessor import (
    TestCenterOlaTestAssessor,
)
from user_management.user_management_models.user_models import User
from user_management.views.utils import CustomPagination
from cms.views.utils import get_needed_parameters, get_user_info_from_jwt_token


# API View -> Get all Ola Assessors' Unavailability for Test Centers Supervised by the Current User
class GetAllOlaAssessorsUnavailabilityForTestCentersSupervisedByCurrentUser(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestCenterManagerPermission | HasScorerPermission),
    )

    def get(self, request):
        return self.get_all_ola_assessors_unavailability_for_test_centers_supervised_by_current_user(
            request
        )

    # Function -> Get all Ola assessors for test centers supervised by the current user.
    def get_all_ola_assessors_unavailability_for_test_centers_supervised_by_current_user(
        self, request
    ):

        success, parameters = get_needed_parameters(["page", "page_size"], request)

        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        user_info = get_user_info_from_jwt_token(request)

        page, page_size = itemgetter("page", "page_size")(parameters)

        # initializing current_page and page_size
        current_page = int(page)
        page_size = int(page_size)

        # pagination class
        pagination_class = CustomPagination
        paginator = pagination_class()

        # get our test scorer
        user_id = user_info["user_id"]

        # getting all ola test centers that the test scorer is supervising
        test_center_ids = TestCenterOlaTestAssessor.objects.filter(
            user_id=user_id, supervisor=True
        ).values_list("test_center_id", flat=True)

        # getting all Test Center Ola Test Assessor IDs related to the Test Center IDs + own
        test_center_ola_test_assessor_unavailability_list = (
            TestCenterOlaTestAssessorUnavailabilityVw.objects.filter(
                Q(test_center_id__in=test_center_ids) | Q(assessor_id=user_id)
            )
        )

        # TestCenterOlaAssessorUnavailabilityView - Serializing Data
        serialized_data = TestCenterOlaTestAssessorUnavailabilityViewSerializer(
            test_center_ola_test_assessor_unavailability_list, many=True
        ).data

        # getting sorted serialized data by name (ascending)
        ordered_test_centers_ola_assessors_unavailability = sorted(
            serialized_data,
            key=lambda k: k["start_date"].lower(),
            reverse=False,
        )
        # only getting data for current selected page
        new_test_centers = ordered_test_centers_ola_assessors_unavailability[
            (current_page - 1) * page_size : current_page * page_size
        ]
        # retuning final data using pagination library
        return paginator.get_paginated_response(
            new_test_centers,
            len(ordered_test_centers_ola_assessors_unavailability),
            current_page,
            page_size,
        )


# API View -> Get found Ola Assessors' Unavailability for Test Centers Supervised by the Current User
class GetFoundOlaAssessorsUnavailabilityForTestCentersSupervisedByCurrentUser(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestCenterManagerPermission | HasScorerPermission),
    )

    def get(self, request):
        return self.get_found_ola_assessors_unavailability_for_test_centers_supervised_by_current_user(
            request
        )

    # Function -> Get Found Ola assessors for test centers supervised by the current user.
    def get_found_ola_assessors_unavailability_for_test_centers_supervised_by_current_user(
        self, request
    ):
        success, parameters = get_needed_parameters(
            ["keyword", "page", "page_size", "current_language"], request
        )

        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        page, page_size, keyword, current_language = itemgetter(
            "page", "page_size", "keyword", "current_language"
        )(parameters)

        user_info = get_user_info_from_jwt_token(request)

        # initializing current_page and page_size
        current_page = int(page)
        page_size = int(page_size)

        # pagination class
        pagination_class = CustomPagination
        paginator = pagination_class()

        # get our test scorer
        user_id = user_info["user_id"]

        # if blank search
        if keyword == " ":
            # getting all ola test centers that the test scorer is supervising
            test_center_ids = TestCenterOlaTestAssessor.objects.filter(
                user_id=user_id, supervisor=True
            ).values_list("test_center_id", flat=True)

            # getting all Test Center Ola Test Assessor IDs related to the Test Center IDs + own
            test_center_ola_test_assessor_unavailability_list = (
                TestCenterOlaTestAssessorUnavailabilityVw.objects.filter(
                    Q(test_center_id__in=test_center_ids) | Q(assessor_id=user_id)
                )
            )

        # regular search
        else:
            # getting all ola test centers that the test scorer is supervising
            test_center_ids = TestCenterOlaTestAssessor.objects.filter(
                user_id=user_id, supervisor=True
            ).values_list("test_center_id", flat=True)

            # search while interface is in English
            if current_language == "en":
                test_center_ola_test_assessor_unavailability_list = (
                    TestCenterOlaTestAssessorUnavailabilityVw.objects.filter(
                        (Q(test_center_id__in=test_center_ids) | Q(assessor_id=user_id))
                        & (
                            Q(first_name__icontains=keyword)
                            | Q(last_name__icontains=keyword)
                            | Q(email__icontains=keyword)
                            | Q(test_center_name__icontains=keyword)
                            | Q(test_center_city_en__icontains=keyword)
                            | Q(start_date__icontains=keyword)
                            | Q(end_date__icontains=keyword)
                        )
                    )
                )
            # search while interface is in French
            else:
                test_center_ola_test_assessor_unavailability_list = (
                    TestCenterOlaTestAssessorUnavailabilityVw.objects.filter(
                        (Q(test_center_id__in=test_center_ids) | Q(assessor_id=user_id))
                        & (
                            Q(first_name__icontains=keyword)
                            | Q(last_name__icontains=keyword)
                            | Q(email__icontains=keyword)
                            | Q(test_center_name__icontains=keyword)
                            | Q(test_center_city_fr__icontains=keyword)
                            | Q(start_date__icontains=keyword)
                            | Q(end_date__icontains=keyword)
                        )
                    )
                )

        # TestCenterOlaAssessorUnavailabilityView - Serializing Data
        serialized_data = TestCenterOlaTestAssessorUnavailabilityViewSerializer(
            test_center_ola_test_assessor_unavailability_list, many=True
        ).data

        # getting sorted serialized data by name (ascending)
        ordered_test_centers_ola_assessors_unavailability = sorted(
            serialized_data,
            key=lambda k: k["start_date"].lower(),
            reverse=False,
        )
        # only getting data for current selected page
        new_test_centers = ordered_test_centers_ola_assessors_unavailability[
            (current_page - 1) * page_size : current_page * page_size
        ]
        # retuning final data using pagination library
        return paginator.get_paginated_response(
            new_test_centers,
            len(ordered_test_centers_ola_assessors_unavailability),
            current_page,
            page_size,
        )


# API View -> Get All OLA Test Centers Supervised by The Current User
#   - Will be used for the dropdown to select test centers for when a supervisor is creating/modifying an unavailability
class GetAllOlaTestCentersSupervisedByCurrentUser(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestCenterManagerPermission | HasScorerPermission),
    )

    def get(self, request):
        return self.get_all_ola_test_centers_supervised_by_current_user(request)

    def get_all_ola_test_centers_supervised_by_current_user(self, request):
        user_info = get_user_info_from_jwt_token(request)

        # get our supervisor
        supervisor_user_id = user_info["user_id"]

        # getting all ola test centers that the supervisor is supervising
        ola_test_centers = TestCenterOlaTestAssessor.objects.filter(
            user_id=supervisor_user_id, supervisor=True
        )

        # Test Centers + Assessors By  - Serializing Data
        serialized_data = TestCenterSerializer(ola_test_centers, many=True).data

        return Response(serialized_data)


# API View -> Get All OLA Test Centers for the Selected Assessor
#   - Will be used for the dropdown to select test centers for when a supervisor is creating/modifying an unavailability
class GetAllOlaTestCentersForSelectedAssessor(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestCenterManagerPermission | HasScorerPermission),
    )

    def get(self, request):
        return self.get_all_ola_test_centers_for_selected_assessor(request)

    def get_all_ola_test_centers_for_selected_assessor(self, request):

        success, parameters = get_needed_parameters(["user_id"], request)

        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        selected_user_id = itemgetter("user_id")(parameters)

        # getting all ola test centers that the supervisor is supervising
        ola_test_center_ids = TestCenterOlaTestAssessor.objects.filter(
            user_id=selected_user_id
        ).values_list("test_center_id", flat=True)

        test_centers = TestCenter.objects.filter(id__in=ola_test_center_ids)

        # Test Centers + Assessors By  - Serializing Data
        serialized_data = TestCenterSerializer(test_centers, many=True).data

        return Response(serialized_data)


# API View -> Get all Ola Assessors for the Current Supervisor
#   - Will be used for the dropdown to select an Assessor
class GetAllOlaAssessorsSupervisedByCurrentUser(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestCenterManagerPermission | HasScorerPermission),
    )

    def get(self, request):
        return self.get_all_ola_assessors_supervised_by_current_user(request)

    def get_all_ola_assessors_supervised_by_current_user(self, request):
        user_info = get_user_info_from_jwt_token(request)

        # get our supervisor
        supervisor_user_id = user_info["user_id"]

        # getting all ola test centers that the supervisor is supervising
        ola_test_center_ids = TestCenterOlaTestAssessor.objects.filter(
            user_id=supervisor_user_id, supervisor=True
        ).values_list("test_center_id", flat=True)

        distinct_test_assessors_user_ids = (
            TestCenterOlaTestAssessor.objects.filter(
                test_center_id__in=ola_test_center_ids
            )
            .values("user_id")
            .distinct()
        )

        ola_test_assessors_users_list = User.objects.filter(
            id__in=distinct_test_assessors_user_ids,
        )

        serialized_data = UserAssessorSerializer(
            ola_test_assessors_users_list, many=True
        ).data

        return Response(serialized_data)


# API View -> Get all Ola Assessors for Selected Test Centers
#   - Will be used for the dropdown to select an Assessor given a list of test centers
class GetAllOlaAssessorsForSelectedTestCenters(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestCenterManagerPermission | HasScorerPermission),
    )

    def get(self, request):
        return self.get_all_ola_assessors_for_selected_test_centers(request)

    def get_all_ola_assessors_for_selected_test_centers(self, request):
        test_center_ids = request.data.get("test_center_ids", [])

        distinct_test_assessors_user_ids = (
            TestCenterOlaTestAssessor.objects.filter(test_center_id__in=test_center_ids)
            .values("user_id")
            .distinct()
        )

        ola_test_assessors_users_list = User.objects.filter(
            id__in=distinct_test_assessors_user_ids,
        )

        serialized_data = UserAssessorSerializer(
            ola_test_assessors_users_list, many=True
        ).data

        return Response(serialized_data)


# API View -> Save New Unavailability days
#   - Will be used to save multiple new unavailability days for a specific assessor
#   - Needs:
#       - Test Centers list
#       - Assessor ID
#       - Start Date
#       - End Date
#       - Reason
class SaveUnavailabilityDaysForSpecificAssessor(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestCenterManagerPermission | HasScorerPermission),
    )

    def post(self, request):
        return self.save_unavailability_days_for_specific_assessor(request)

    def save_unavailability_days_for_specific_assessor(self, request):
        test_center_ids = request.data.get("test_center_ids", [])
        assessor_id = request.data.get("assessor_id", None)
        start_date = request.data.get("start_date", None)
        end_date = request.data.get("end_date", None)
        reason = request.data.get("reason", None)
        try:
            with transaction.atomic():
                test_center_ola_assessors_ids = (
                    TestCenterOlaTestAssessor.objects.filter(
                        test_center_id__in=test_center_ids, user_id=assessor_id
                    ).values_list("id", flat=True)
                )

                for test_center_ola_assessors_id in test_center_ola_assessors_ids:
                    TestCenterOlaAssessorUnavailability.objects.create(
                        test_center_ola_test_assessor_id=test_center_ola_assessors_id,
                        start_date=start_date,
                        end_date=end_date,
                        reason=reason,
                    )

        except Exception as error:
            # printing error type
            print(type(error))
            return Response(
                {"error": "Something happened during the save unavailability days"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        return Response(status=status.HTTP_201_CREATED)


# API View -> Modify Unavailability days
#   - Will be used to modify a single unavailability days for a specific assessor
#   - Needs:
#       - TestCenterOlaAssessorUnavailability ID
#       - Start Date
#       - End Date
class ModifyUnavailabilityDaysForSpecificAssessor(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestCenterManagerPermission | HasScorerPermission),
    )

    def post(self, request):
        return self.modify_unavailability_days_for_specific_assessor(request)

    def modify_unavailability_days_for_specific_assessor(self, request):
        test_center_ola_assessor_unavailability_id = request.data.get(
            "test_center_ola_assessor_unavailability_id", None
        )
        new_start_date = request.data.get("start_date", None)
        new_end_date = request.data.get("end_date", None)
        new_reason = request.data.get("reason", None)
        try:
            with transaction.atomic():
                # Getting old entry
                unavailability_object = TestCenterOlaAssessorUnavailability.objects.get(
                    id=test_center_ola_assessor_unavailability_id
                )

                # Modify the dates
                unavailability_object.start_date = new_start_date
                unavailability_object.end_date = new_end_date
                unavailability_object.reason = new_reason

                # Save to database
                unavailability_object.save()

        except TestCenterOlaAssessorUnavailability.DoesNotExist:
            return Response(
                {"error": "Can't find the unavailability days to modify"},
                status=status.HTTP_404_NOT_FOUND,
            )

        except Exception as error:
            # printing error type
            print(type(error))
            return Response(
                {"error": "Something happened during the save unavailability days"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        return Response(status=status.HTTP_201_CREATED)


# API View -> Delete an Entry - Unavailability days
#   - Will be used to delete a single unavailability days for a specific assessor
#   - Needs:
#       - TestCenterOlaAssessorUnavailability ID
class DeleteUnavailabilityDaysForSpecificAssessor(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestCenterManagerPermission | HasScorerPermission),
    )

    def post(self, request):
        return self.delete_unavailability_days_for_specific_assessor(request)

    def delete_unavailability_days_for_specific_assessor(self, request):
        test_center_ola_assessor_unavailability_id = request.data.get(
            "test_center_ola_assessor_unavailability_id", None
        )

        try:
            with transaction.atomic():
                # Getting old entry
                unavailability_object = TestCenterOlaAssessorUnavailability.objects.get(
                    id=test_center_ola_assessor_unavailability_id
                )

                # Delete it
                unavailability_object.delete()

        except TestCenterOlaAssessorUnavailability.DoesNotExist:
            return Response(
                {"error": "Can't find the unavailability days to delete"},
                status=status.HTTP_404_NOT_FOUND,
            )

        except Exception as error:
            # printing error type
            print(type(error))
            return Response(
                {"error": "Something happened during the save unavailability days"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        return Response(status=status.HTTP_200_OK)
