from datetime import date
import datetime
import pytz
from rest_framework.response import Response
from backend.custom_models.user_accommodation_file import UserAccommodationFile
from db_views.db_view_models.user_accommodation_file_data_for_details_popup_vw import (
    UserAccommodationFileDataForDetailsPopupVW,
)
from backend.static.user_accommodation_file_status_const import (
    UserAccommodationFileStatusConst,
)
from backend.custom_models.user_accommodation_file_status import (
    UserAccommodationFileStatus,
)
from backend.custom_models.uit_invites import UITInvites
from backend.static.languages import Languages
from backend.custom_models.assigned_test import AssignedTest
from db_views.serializers.assigned_tests_serializers import (
    TaAssignedCandidatesViewSerializer,
)
from db_views.db_view_models.ta_assigned_candidates_vw import TaAssignedCandidatesVW
from db_views.db_view_models.test_center_test_sessions_vw import (
    TestCenterTestSessionsVW,
)
from backend.static.assigned_test_status import (
    AssignedTestStatus,
    get_assigned_test_status_id,
)
from backend.serializers.assigned_test_serializer import (
    UitAssignedTestSerializer,
)
from backend.views.utils import get_user_info_from_jwt_token


def retrieve_assigned_tests(request):
    current_language = request.query_params.get("current_language", None)
    assigned_test_id = request.query_params.get("assigned_test_id", None)
    user_info = get_user_info_from_jwt_token(request)
    if assigned_test_id:
        query_list = TaAssignedCandidatesVW.objects.filter(id=assigned_test_id)
    else:
        assigned_status_id = get_assigned_test_status_id(AssignedTestStatus.ASSIGNED)
        checked_in_status_id = get_assigned_test_status_id(
            AssignedTestStatus.CHECKED_IN
        )
        pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
        active_status_id = get_assigned_test_status_id(AssignedTestStatus.ACTIVE)
        transition_status_id = get_assigned_test_status_id(
            AssignedTestStatus.TRANSITION
        )
        locked_status_id = get_assigned_test_status_id(AssignedTestStatus.LOCKED)
        paused_status_id = get_assigned_test_status_id(AssignedTestStatus.PAUSED)
        query_list = TaAssignedCandidatesVW.objects.filter(
            candidate_user_id=user_info["user_id"],
            status_id__in=[
                assigned_status_id,
                checked_in_status_id,
                pre_test_status_id,
                active_status_id,
                transition_status_id,
                locked_status_id,
                paused_status_id,
            ],
        )

    # making sure that every single test respects the validity end date (UIT tests) and the test session end time (supervised tests)
    # initializing final_query_list
    final_query_list = []
    # getting status IDs
    checked_in_status_id = get_assigned_test_status_id(AssignedTestStatus.CHECKED_IN)
    pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
    unassigned_status_id = get_assigned_test_status_id(AssignedTestStatus.UNASSIGNED)
    # getting current date
    current_date = date.today()
    # getting current time (UTC)
    current_time = datetime.datetime.now(pytz.utc)
    # looping in query_list
    for assigned_test in query_list:
        # if status is either checked_in or pre_test
        if assigned_test.status_id in (checked_in_status_id, pre_test_status_id):
            # supervised test
            if assigned_test.uit_invite_id is None:
                # getting related test session data
                related_test_session_data = TestCenterTestSessionsVW.objects.get(
                    id=assigned_test.test_session_id
                )
                # we are past the test session end time
                if current_time > related_test_session_data.end_time:
                    # unassigning test
                    temp_assigned_test = AssignedTest.objects.get(id=assigned_test.id)
                    temp_assigned_test.status_id = unassigned_status_id
                    temp_assigned_test.save()
                # respecting the test session end time
                else:
                    final_query_list.append(assigned_test)
            # unsupervised test
            elif assigned_test.uit_invite_id is not None:
                # getting related uit invite data
                related_uit_invite_data = UITInvites.objects.get(
                    id=assigned_test.uit_invite_id
                )
                # we are past the validity end date
                if current_date > related_uit_invite_data.validity_end_date:
                    # initializing need_to_unassign_test
                    need_to_unassign_test = False
                    # assigned test of current iteration is associated to a user accommodation file
                    if assigned_test.user_accommodation_file_id is not None:
                        # getting cancelled related user accommodation file status IDs
                        cancelled_related_user_accommodation_file_status_ids = UserAccommodationFileStatus.objects.filter(
                            codename__in=(
                                UserAccommodationFileStatusConst.CANCELLED,
                                UserAccommodationFileStatusConst.CANCELLED_BY_CANDIDATE,
                                UserAccommodationFileStatusConst.EXPIRED,
                            )
                        ).values_list(
                            "id", flat=True
                        )
                        # checking if test of current iteration is associated to an active AAE Request
                        # getting related user accommodation file data
                        related_user_accommodation_file_data = (
                            UserAccommodationFile.objects.filter(
                                id=assigned_test.user_accommodation_file_id
                            )
                            .order_by("id")
                            .last()
                        )
                        # status of related_user_accommodation_file_data is part of the cancelled_related_user_accommodation_file_status_ids
                        if (
                            related_user_accommodation_file_data.status_id
                            in cancelled_related_user_accommodation_file_status_ids
                        ):
                            need_to_unassign_test = True
                    # assigned test of current iteration is not associated to a user accommodation file
                    else:
                        need_to_unassign_test = True

                    # need_to_unassign_test is set to True at this point
                    if need_to_unassign_test:
                        # unassigning test
                        temp_assigned_test = AssignedTest.objects.get(
                            id=assigned_test.id
                        )
                        temp_assigned_test.status_id = unassigned_status_id
                        temp_assigned_test.save()
                    # need_to_unassign_test is set still set to False at this point
                    else:
                        # need to populate that test in the final_query_list
                        final_query_list.append(assigned_test)
                # respecting the validity end date
                else:
                    final_query_list.append(assigned_test)
            # should never go there
            else:
                pass
        # other then checked_in and pre_test status
        else:
            final_query_list.append(assigned_test)

    serialized_data = TaAssignedCandidatesViewSerializer(
        final_query_list, many=True
    ).data

    # re-ordering serialized_data by status and name (EN or FR name depending on the provided current_language parameter)
    if current_language == Languages.EN:
        ordered_serialized_data = sorted(
            sorted(serialized_data, key=lambda k: k["test_name_en"].lower()),
            key=lambda k: k["status_id"],
            reverse=True,
        )
    else:
        ordered_serialized_data = sorted(
            sorted(serialized_data, key=lambda k: k["test_name_fr"].lower()),
            key=lambda k: k["status_id"],
            reverse=True,
        )

    # we also need to add all accommodated tests that cannot be done in CAT that are ready for approval by the candidate (for the candidate to be able to cancel or approve the AAE request)
    # getting pending_approval_status_id
    pending_approval_status_id = UserAccommodationFileStatus.objects.get(
        codename=UserAccommodationFileStatusConst.PENDING_APPROVAL
    ).id
    # getting pending approval UIT accommodation requests where the specified_test_description is not NULL (meaning that the test will be done outside of CAT)
    pending_approval_accommodation_requests = (
        UserAccommodationFileDataForDetailsPopupVW.objects.filter(
            user_id=user_info["user_id"],
            status_id=pending_approval_status_id,
            is_uit=1,
            specified_test_description__isnull=False,
        )
    )
    # looping in pending_approval_accommodation_requests
    for accommodation_request in pending_approval_accommodation_requests:
        # updating ordered_serialized_data (replicating the same object that is being returned by TaAssignedCandidatesViewSerializer)
        ordered_serialized_data.append(
            {
                "id": None,
                "assigned_test_sections": [],
                "pause_test_time": accommodation_request.break_time,
                "pause_start_date": None,
                "test_time_remaining": accommodation_request.total_adjusted_time,
                "test_time_updated": False,
                "status_id": None,
                "status_codename": "",
                "previous_status_id": None,
                "previous_status_codename": None,
                "start_date": None,
                "modify_date": None,
                "submit_date": None,
                "test_access_code": "",
                "total_score": None,
                "test_id": None,
                "parent_code": None,
                "test_code": None,
                "test_name_en": accommodation_request.specified_test_description,
                "test_name_fr": accommodation_request.specified_test_description,
                "test_section_id": None,
                "test_session_language_id": None,
                "test_order_number": None,
                "en_converted_score": None,
                "fr_converted_score": None,
                "is_invalid": False,
                "uit_invite_id": None,
                "orderless_financial_data_id": None,
                "accommodation_request_id": None,
                "test_session_id": None,
                "user_accommodation_file_id": accommodation_request.id,
                "is_alternate_test_request": accommodation_request.is_alternate_test_request,
                "candidate_user_id": accommodation_request.user_id,
                "candidate_email": accommodation_request.user_email,
                "candidate_first_name": accommodation_request.user_first_name,
                "candidate_last_name": accommodation_request.user_last_name,
                "candidate_dob": None,
                "candidate_has_accommodations_profile": True,
                "ta_user_id": accommodation_request.ta_user_id,
                "ta_username": None,
                "ta_email": accommodation_request.ta_email,
                "ta_first_name": accommodation_request.ta_first_name,
                "ta_last_name": accommodation_request.ta_last_name,
                "test_started": False,
                "timed_section_accessed": False,
                "ta_approve_action_test_section_id": None,
                "next_test_section_id_that_needs_approval": False,
                "needs_approval": False,
                # custom attribute to allow the frontend to know that this is an accommodated request for a test that is done outside of CAT
                "accommodated_test_outside_of_cat": True,
            }
        )

    return Response(ordered_serialized_data)


def retrieve_uit_assigned_tests(request):
    user_info = get_user_info_from_jwt_token(request)
    checked_in_status_id = get_assigned_test_status_id(AssignedTestStatus.CHECKED_IN)
    pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
    active_status_id = get_assigned_test_status_id(AssignedTestStatus.ACTIVE)
    transition_status_id = get_assigned_test_status_id(AssignedTestStatus.TRANSITION)
    query_list = AssignedTest.objects.filter(
        user_id=user_info["user_id"],
        status_id__in=[
            checked_in_status_id,
            pre_test_status_id,
            active_status_id,
            transition_status_id,
        ],
    )

    data = UitAssignedTestSerializer(query_list, many=True)
    return Response(data.data)
