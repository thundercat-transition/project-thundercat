from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.uit_test_actions import test_actions, Action
from backend.api_permissions.assigned_test_permissions import (
    HasAssignedTestFromAssignedTestPermission,
)


class UitSeenQuestion(APIView):
    def post(self, request):
        needed_parameters = [
            "assigned_test_id",
            "question_id",
            "test_section_component_id",
        ]
        return test_actions(request, needed_parameters, Action.SEEN_QUESTION)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasAssignedTestFromAssignedTestPermission(),
        ]


class UitSaveAnswer(APIView):
    def post(self, request):
        needed_parameters = [
            "assigned_test_id",
            "question_id",
            "answer_id",
            "test_section_component_id",
            "selected_language_id",
        ]
        return test_actions(request, needed_parameters, Action.SAVE_ANSWERS)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasAssignedTestFromAssignedTestPermission(),
        ]


class GetTestAnswers(APIView):
    def get(self, request):
        needed_parameters = ["assigned_test_id", "test_section_component_id"]
        return test_actions(request, needed_parameters, Action.GET_ANSWERS)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasAssignedTestFromAssignedTestPermission(),
        ]


class UpdateCandidateAnswerModifyDate(APIView):
    def post(self, request):
        needed_parameters = [
            "assigned_test_id",
            "question_id",
            "test_section_component_id",
        ]
        return test_actions(request, needed_parameters, Action.UPDATE_MODIFY_DATE)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasAssignedTestFromAssignedTestPermission(),
        ]
