import json
from django.db.models import Q, Sum
from django.db.models.functions import TruncMonth
from django.db import transaction
from operator import itemgetter
from urllib import parse
from rest_framework.response import Response
from rest_framework import status
from backend.custom_models.test_center_test_session_data import (
    TestCenterTestSessionData,
)
from backend.custom_models.user_accommodation_file_complexity import (
    UserAccommodationFileComplexity,
)
from backend.custom_models.user_accommodation_file_complexity_text import (
    UserAccommodationFileComplexityText,
)
from backend.custom_models.test_center_test_sessions import TestCenterTestSessions
from db_views.db_view_models.assessment_process_assigned_test_specs_vw import (
    AssessmentProcessAssignedTestSpecsVW,
)
from backend.views.gc_notify_view import (
    send_accommodations_request_pending_approval_email,
)
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.static.assigned_test_status import get_assigned_test_status_id
from backend.static.assigned_test_status import AssignedTestStatus
from backend.static.languages import Languages
from backend.custom_models.user_accommodation_file_status_text import (
    UserAccommodationFileStatusText,
)
from backend.custom_models.user_accommodation_file_other_measures import (
    UserAccommodationFileOtherMeasures,
)
from backend.custom_models.user_accommodation_file_break_bank import (
    UserAccommodationFileBreakBank,
)
from backend.custom_models.user_accommodation_file_test_to_administer import (
    UserAccommodationFileTestToAdminister,
)
from backend.custom_models.user_accommodation_file_comments import (
    UserAccommodationFileComments,
)
from backend.custom_models.consumed_reservation_codes import ConsumedReservationCodes
from backend.static.user_accommodation_file_status_const import (
    UserAccommodationFileStatusConst,
)
from user_management.user_management_models.user_models import User
from user_management.views.user_accommodations_profile import (
    get_user_accommodations_profile_for_request,
)
from user_management.views.utils import CustomPagination
from backend.views.utils import (
    create_language_compatible_object,
    get_month_as_a_word,
    get_quarter_from_date,
    get_user_info_from_jwt_token,
)
from backend.custom_models.user_accommodation_file import UserAccommodationFile
from backend.custom_models.user_accommodation_file_test_time import (
    UserAccommodationFileTestTime,
)
from backend.custom_models.user_accommodation_file_status import (
    UserAccommodationFileStatus,
)
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.user_accommodation_file_recommendations import (
    UserAccommodationFileRecommendations,
)
from backend.custom_models.user_accommodation_file_candidate_limitations import (
    UserAccommodationFileCandidateLimitations,
)
from backend.static.consumed_reservation_code_statuses import (
    StaticConsumedReservationCodeStatus,
)
from backend.custom_models.consumed_reservation_code_status import (
    ConsumedReservationCodeStatus,
)
from db_views.serializers.test_center_serializers import TestCentersViewSerializer
from db_views.db_view_models.test_centers_vw import TestCentersVW
from db_views.db_view_models.completed_supervised_user_accommodation_file_vw import (
    CompletedSupervisedUserAccommodationFileVW,
)
from db_views.db_view_models.user_accommodation_file_data_for_details_popup_vw import (
    UserAccommodationFileDataForDetailsPopupVW,
)
from cms.views.utils import get_needed_parameters, get_optional_parameters
from cms.cms_models.test_section import TestSection
from db_views.db_view_models.selected_user_accommodation_file_vw import (
    SelectedUserAccommodationFileVW,
)
from db_views.db_view_models.aae_reports_number_of_closed_requests_vw import (
    AaeReportsNumberOfClosedRequestsVw,
)
from db_views.serializers.aae_serializers import (
    AaeReportsNumberOfClosedRequestsViewSerializer,
    CompletedSupervisedUserAccommodationFileViewSerializer,
    SelectedUserAccommodationFileViewSerializer,
    UserAccommodationFileCommentsSerializer,
    UserAccommodationFileComplexitySerializer,
    UserAccommodationFileComplexityTextSerializer,
    UserAccommodationFileDataForDetailsPopupViewSerializer,
    UserAccommodationFileReportDataViewSerializer,
    UserAccommodationFileStatusSerializer,
    UserAccommodationFileStatusTextSerializer,
    UserAccommodationFileTestsToAdministerViewSerializer,
    UserAccommodationFileViewSerializer,
)
from db_views.db_view_models.user_accommodation_file_vw import UserAccommodationFileVW
from db_views.db_view_models.user_accommodation_file_tests_to_administer_vw import (
    UserAccommodationFileTestsToAdministerVW,
)
from db_views.db_view_models.user_accommodation_file_report_data_vw import (
    UserAccommodationFileReportDataVW,
)
from db_views.db_view_models.aae_reports_number_of_requests_received_vw import (
    AaeReportsNumberOfRequestsReceivedVw,
)
from backend.views.gc_notify_view import (
    new_accomodations_request_send_all,
    canceled_accomodations_request_send_all,
    canceled_accomodations_request_candidate_send_all,
    completed_accomodations_request_send_all,
    re_opened_accomodations_request_send_all,
)

USER_ACCOMMODATION_FILE_OPEN_STATUS_CODENAMES = [
    UserAccommodationFileStatusConst.NEW,
    UserAccommodationFileStatusConst.IN_PROGRESS,
    UserAccommodationFileStatusConst.ON_HOLD,
    UserAccommodationFileStatusConst.ESTIMATING_SERVICE_COST,
    UserAccommodationFileStatusConst.WAITING_FOR_FINANCIAL_CODING,
    UserAccommodationFileStatusConst.WAITING_FOR_DOCUMENTATION,
    UserAccommodationFileStatusConst.READY,
    UserAccommodationFileStatusConst.PENDING_APPROVAL,
    UserAccommodationFileStatusConst.RE_OPENED,
    UserAccommodationFileStatusConst.RE_OPENED_HOLD,
]

USER_ACCOMMODATION_FILE_COMPLETED_STATUS_CODENAMES = [
    UserAccommodationFileStatusConst.COMPLETED,
    UserAccommodationFileStatusConst.CANCELLED,
    UserAccommodationFileStatusConst.CANCELLED_BY_CANDIDATE,
    UserAccommodationFileStatusConst.EXPIRED,
    UserAccommodationFileStatusConst.ADMINISTERED,
]

USER_ACCOMMODATION_FILE_STATUSES_DROPDOWN_AVAILABLE_CODENAMES = [
    UserAccommodationFileStatusConst.NEW,
    UserAccommodationFileStatusConst.IN_PROGRESS,
    UserAccommodationFileStatusConst.ON_HOLD,
    UserAccommodationFileStatusConst.ESTIMATING_SERVICE_COST,
    UserAccommodationFileStatusConst.WAITING_FOR_FINANCIAL_CODING,
    UserAccommodationFileStatusConst.WAITING_FOR_DOCUMENTATION,
    UserAccommodationFileStatusConst.READY,
    UserAccommodationFileStatusConst.RE_OPENED_HOLD,
]


class UitTestAssociationForAccommodationOrAlternateTestRequestActions:
    SUBMIT_SEND = "submit_send"
    REJECT_CANCEL = "reject_cancel"


def get_test_centers_for_user_accommodation_request(request):
    optional_parameters = get_optional_parameters(["test_center_id"], request)

    # test_center_id parameter is provided
    if optional_parameters["test_center_id"] is not None:
        try:
            # getting specified test center data
            related_test_center = TestCentersVW.objects.get(
                id=optional_parameters["test_center_id"]
            )

            # serializing data
            serialized_data = TestCentersViewSerializer(
                related_test_center, many=False
            ).data

            return Response(serialized_data)
        except TestCentersVW.DoesNotExist:
            return Response(
                {"error": "Test center not found based on provided test center ID"},
                status=status.HTTP_404_NOT_FOUND,
            )

    # test_center_id is not provided, so return all accommodation friendly test centers
    else:
        # getting test centers that are accommodations friendly
        related_test_centers = TestCentersVW.objects.filter(accommodations_friendly=1)

        # serializing data
        serialized_data = TestCentersViewSerializer(
            related_test_centers, many=True
        ).data

        return Response(serialized_data)


def send_user_accommodation_request(request):
    user_accommodation_request_data = json.loads(request.body)
    user_info = get_user_info_from_jwt_token(request)

    # making sure that all mandatory parameters are provided
    if "is_uit" not in user_accommodation_request_data:
        return Response(
            {"error": "missing mandatory parameter(s)"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # getting respective status ID
    status_id = UserAccommodationFileStatus.objects.get(
        codename=UserAccommodationFileStatusConst.NEW
    ).id

    # setting test_center_id
    test_center_id = None
    if (
        "test_center_id" in user_accommodation_request_data
        and user_accommodation_request_data["test_center_id"] is not None
    ):
        test_center_id = user_accommodation_request_data["test_center_id"]

    # setting phone_number
    phone_number = None
    if (
        "phone_number" in user_accommodation_request_data
        and user_accommodation_request_data["phone_number"] is not None
    ):
        phone_number = user_accommodation_request_data["phone_number"]

    # creating new accommodation request
    new_user_accommodation_file = UserAccommodationFile.objects.create(
        comments=user_accommodation_request_data["comments"],
        is_uit=user_accommodation_request_data["is_uit"],
        test_center_id=test_center_id,
        status_id=status_id,
        user_id=user_info["user_id"],
        last_modified_by_user_id=user_info["user_id"],
        is_alternate_test_request=user_accommodation_request_data[
            "is_alternate_test_request"
        ],
        candidate_phone_number=phone_number,
    )

    # respectively linking new user accommodation file
    # UIT
    if user_accommodation_request_data["is_uit"]:
        # getting related assigned test data
        related_assigned_test_data = AssignedTest.objects.get(
            id=user_accommodation_request_data["assigned_test_id"]
        )
        # linking new user accommodation file
        related_assigned_test_data.user_accommodation_file_id = (
            new_user_accommodation_file.id
        )
        # saving changes
        related_assigned_test_data.save()

        # creating new entry in UserAccommodationFileTestToAdminister
        UserAccommodationFileTestToAdminister.objects.create(
            user_accommodation_file_id=new_user_accommodation_file.id,
            test_id=related_assigned_test_data.test_id,
            specified_test_description=None,
        )

        # collect data to send email to ta stating that there was a new accomodations request
        email_detail = SelectedUserAccommodationFileVW.objects.get(
            id=new_user_accommodation_file.id
        )
        new_accomodations_request_send_all(email_detail)

    # Supervised
    else:
        # getting related consumed reservation code data
        related_consumed_reservation_code_data = ConsumedReservationCodes.objects.get(
            reservation_code=user_accommodation_request_data["reservation_code"]
        )
        # linking new user accommodation file
        related_consumed_reservation_code_data.user_accommodation_file_id = (
            new_user_accommodation_file.id
        )

        # changing status to move to managed table
        related_consumed_reservation_code_data.status = (
            ConsumedReservationCodeStatus.objects.get(
                codename=StaticConsumedReservationCodeStatus.REQUESTED_ACCOMMODATION
            )
        )

        # saving changes
        related_consumed_reservation_code_data.save()

        # collect data to send email to hr stating that there was a new accomodations request
        email_detail = SelectedUserAccommodationFileVW.objects.get(
            id=new_user_accommodation_file.id
        )
        new_accomodations_request_send_all(email_detail)

    return Response(status=status.HTTP_200_OK)


def close_user_accommodation_request(request):
    try:
        # find the user accomodation file
        uaf = UserAccommodationFile.objects.get(id=request.user_accommodation_file_id)

        cancelled_by_candidate = UserAccommodationFileStatus.objects.get(
            codename=UserAccommodationFileStatusConst.CANCELLED_BY_CANDIDATE
        )

        # set the status to rejected (double check that this will still allow them to create a new request)
        uaf.status = cancelled_by_candidate

        # save
        uaf.save()

        email_detail = SelectedUserAccommodationFileVW.objects.get(id=uaf.id)

        # send cancelation by candidate email to all tcms for the test center and the hr contact who created the process
        canceled_accomodations_request_candidate_send_all(email_detail)

        # set the user_accomodation_file_id to null in the consumed_reservations_code
        consumed_reservation_code = ConsumedReservationCodes.objects.get(id=request.id)

        consumed_reservation_code.user_accommodation_file_id = None

        consumed_reservation_code.save()

    except UserAccommodationFile.DoesNotExist:
        return Response(
            {"error": "The specified user accommodation file does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def check_for_ongoing_user_accommodation_request_process(request):
    success, parameters = get_needed_parameters(
        [
            "assigned_test_id",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    assigned_test_id = itemgetter(
        "assigned_test_id",
    )(parameters)

    # initializing ongoing_accommodation_request
    ongoing_accommodation_request = False

    # getting related assigned test data
    related_assigned_test_data = AssignedTest.objects.get(id=assigned_test_id)

    # if there is an ongoing accommodation request
    if related_assigned_test_data.user_accommodation_file_id is not None:
        # getting respective user accommodation file statuses (SENT, IN_PROGRESS, ON_HOLD)
        respective_status_ids = UserAccommodationFileStatus.objects.filter(
            codename__in=(USER_ACCOMMODATION_FILE_OPEN_STATUS_CODENAMES)
        ).values_list("id", flat=True)

        # getting related user accommodation file data
        ongoin_user_accommodation_file_request = UserAccommodationFile.objects.filter(
            id=related_assigned_test_data.user_accommodation_file_id,
            status_id__in=respective_status_ids,
        )

        # if there is an ongoing user accommodation request process
        if ongoin_user_accommodation_file_request:
            # setting ongoing_accommodation_request to True
            ongoing_accommodation_request = True

    return Response(ongoing_accommodation_request)


def get_all_user_accommodation_files(request):
    # is_open is True => Get Only Open Requests
    # is_open is False => Get Only Completed Requests
    success, parameters = get_needed_parameters(
        ["page", "page_size", "is_open"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, is_open = itemgetter("page", "page_size", "is_open")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # OPEN REQUESTS
    if is_open == "true" or is_open is True:
        # Getting only the Open User Accommodation Files
        user_acc_files = UserAccommodationFileVW.objects.filter(
            status_codename__in=USER_ACCOMMODATION_FILE_OPEN_STATUS_CODENAMES,
            is_alternate_test_request=0,
        )

        serialized_user_acc_files = UserAccommodationFileViewSerializer(
            user_acc_files, many=True
        ).data

        # getting sorted serialized data by process_end_date (ASC) and, then, created_date (ASC)
        ordered_user_acc_files = sorted(
            sorted(
                serialized_user_acc_files,
                key=lambda k: k["process_end_date"],
            ),
            key=lambda k: k["created_date"],
        )

    # COMPLETED/CLOSED REQUESTS
    else:
        # Getting only the Completed User Accommodation Files
        user_acc_files = UserAccommodationFileVW.objects.filter(
            status_codename__in=USER_ACCOMMODATION_FILE_COMPLETED_STATUS_CODENAMES,
            is_alternate_test_request=0,
        )

        serialized_user_acc_files = UserAccommodationFileViewSerializer(
            user_acc_files, many=True
        ).data

        # getting sorted serialized data by created date (DESC) and user last name (ASC)
        ordered_user_acc_files = sorted(
            sorted(
                serialized_user_acc_files, key=lambda k: k["user_last_name"].lower()
            ),
            key=lambda k: k["created_date"],
            reverse=True,
        )

    # only getting data for current selected page
    current_user_acc_files = ordered_user_acc_files[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # final data using pagination library
    final_object = paginator.get_paginated_response(
        current_user_acc_files,
        len(ordered_user_acc_files),
        current_page,
        page_size,
    )

    return final_object


def get_found_user_accommodation_files(request):
    # is_open is True => Get Only Open Requests
    # is_open is False => Get Only Completed Requests
    success, parameters = get_needed_parameters(
        ["keyword", "current_language", "page", "page_size", "is_open"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    keyword, current_language, page, page_size, is_open = itemgetter(
        "keyword", "current_language", "page", "page_size", "is_open"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    if is_open == "true" or is_open is True:
        # Blank search
        if keyword == " ":
            user_acc_files = UserAccommodationFileVW.objects.filter(
                status_codename__in=USER_ACCOMMODATION_FILE_OPEN_STATUS_CODENAMES,
                is_alternate_test_request=0,
            )

        else:
            # English Search
            if current_language == Languages.EN:
                # Getting only the Open User Accommodation Files
                user_acc_files = UserAccommodationFileVW.objects.filter(
                    (
                        Q(
                            status_codename__in=USER_ACCOMMODATION_FILE_OPEN_STATUS_CODENAMES
                        )
                        & Q(is_alternate_test_request=0)
                    )
                    & (
                        Q(id__icontains=keyword)
                        | Q(user_id__icontains=keyword)
                        | Q(user_first_name__icontains=keyword)
                        | Q(user_last_name__icontains=keyword)
                        | Q(created_date__icontains=keyword)
                        | Q(process_end_date__icontains=keyword)
                        | Q(assigned_to_user_first_name__icontains=keyword)
                        | Q(assigned_to_user_last_name__icontains=keyword)
                        | Q(status_name_en__icontains=keyword)
                        | Q(test_skill_type_name_en__icontains=keyword)
                        | Q(test_skill_sub_type_name_en__icontains=keyword)
                    )
                )
            # French Search
            else:
                # Getting only the Open User Accommodation Files
                user_acc_files = UserAccommodationFileVW.objects.filter(
                    (
                        Q(
                            status_codename__in=USER_ACCOMMODATION_FILE_OPEN_STATUS_CODENAMES
                        )
                        & Q(is_alternate_test_request=0)
                    )
                    & (
                        Q(id__icontains=keyword)
                        | Q(user_id__icontains=keyword)
                        | Q(user_first_name__icontains=keyword)
                        | Q(user_last_name__icontains=keyword)
                        | Q(created_date__icontains=keyword)
                        | Q(process_end_date__icontains=keyword)
                        | Q(assigned_to_user_first_name__icontains=keyword)
                        | Q(assigned_to_user_last_name__icontains=keyword)
                        | Q(status_name_fr__icontains=keyword)
                        | Q(test_skill_type_name_fr__icontains=keyword)
                        | Q(test_skill_sub_type_name_fr__icontains=keyword)
                    )
                )

        # is_open = true
        serialized_user_acc_files = UserAccommodationFileViewSerializer(
            user_acc_files, many=True
        ).data

        # getting sorted serialized data by process_end_date (ASC) and, then, created_date (ASC)
        ordered_user_acc_files = sorted(
            sorted(
                serialized_user_acc_files,
                key=lambda k: k["process_end_date"],
            ),
            key=lambda k: k["created_date"],
        )

    else:
        # Blank search
        if keyword == " ":
            user_acc_files = UserAccommodationFileVW.objects.filter(
                status_codename__in=USER_ACCOMMODATION_FILE_COMPLETED_STATUS_CODENAMES,
                is_alternate_test_request=0,
            )
        else:
            # English Search
            if current_language == Languages.EN:
                # Getting only the Completed User Accommodation Files
                user_acc_files = UserAccommodationFileVW.objects.filter(
                    (
                        Q(
                            status_codename__in=USER_ACCOMMODATION_FILE_COMPLETED_STATUS_CODENAMES
                        )
                        & Q(is_alternate_test_request=0)
                    )
                    & (
                        Q(id__icontains=keyword)
                        | Q(user_id__icontains=keyword)
                        | Q(user_first_name__icontains=keyword)
                        | Q(user_last_name__icontains=keyword)
                        | Q(created_date__icontains=keyword)
                        | Q(process_end_date__icontains=keyword)
                        | Q(assigned_to_user_first_name__icontains=keyword)
                        | Q(assigned_to_user_last_name__icontains=keyword)
                        | Q(status_name_en__icontains=keyword)
                        | Q(test_skill_type_name_en__icontains=keyword)
                        | Q(test_skill_sub_type_name_en__icontains=keyword)
                    )
                )
            # French Search
            else:
                # Getting only the Completed User Accommodation Files
                user_acc_files = UserAccommodationFileVW.objects.filter(
                    (
                        Q(
                            status_codename__in=USER_ACCOMMODATION_FILE_COMPLETED_STATUS_CODENAMES
                        )
                        & Q(is_alternate_test_request=0)
                    )
                    & (
                        Q(id__icontains=keyword)
                        | Q(user_id__icontains=keyword)
                        | Q(user_first_name__icontains=keyword)
                        | Q(user_last_name__icontains=keyword)
                        | Q(created_date__icontains=keyword)
                        | Q(process_end_date__icontains=keyword)
                        | Q(assigned_to_user_first_name__icontains=keyword)
                        | Q(assigned_to_user_last_name__icontains=keyword)
                        | Q(status_name_fr__icontains=keyword)
                        | Q(test_skill_type_name_fr__icontains=keyword)
                        | Q(test_skill_sub_type_name_fr__icontains=keyword)
                    )
                )

        # is_open = false
        serialized_user_acc_files = UserAccommodationFileViewSerializer(
            user_acc_files, many=True
        ).data

        # getting sorted serialized data by created date (DESC) and user last name (ASC)
        ordered_user_acc_files = sorted(
            sorted(
                serialized_user_acc_files, key=lambda k: k["user_last_name"].lower()
            ),
            key=lambda k: k["created_date"],
            reverse=True,
        )

    if not user_acc_files:
        return Response({"no results found"}, status=status.HTTP_200_OK)

    # only getting data for current selected page
    current_user_acc_files = ordered_user_acc_files[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # final data using pagination library
    final_object = paginator.get_paginated_response(
        current_user_acc_files,
        len(ordered_user_acc_files),
        current_page,
        page_size,
    )

    return final_object


def get_selected_user_accommodation_file_data(request):
    success, parameters = get_needed_parameters(["user_accommodation_file_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    user_accommodation_file_id = itemgetter("user_accommodation_file_id")(parameters)

    # getting respective user accommodation file data
    user_accommodation_file_data = SelectedUserAccommodationFileVW.objects.get(
        id=user_accommodation_file_id
    )

    # serializing data
    serialized_data = SelectedUserAccommodationFileViewSerializer(
        user_accommodation_file_data, many=False
    ).data

    # Getting user accommodations profile for the User, based on the created_date of the Accommodations File Request
    serialized_data["user_accommodations_profile"] = (
        get_user_accommodations_profile_for_request(
            user_accommodation_file_data.user_id,
            user_accommodation_file_data.modify_date,
        )
    )

    return Response(serialized_data)


def get_accommodation_file_data_for_details_popup(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(
        ["user_accommodation_file_id", "get_historical_data"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    user_accommodation_file_id, get_historical_data = itemgetter(
        "user_accommodation_file_id", "get_historical_data"
    )(parameters)

    optional_parameters = get_optional_parameters(
        ["history_id", "provided_user_id"], request
    )

    # initializing user_id
    user_id = user_info["user_id"]

    # if provided_user_id is defined
    if optional_parameters["provided_user_id"] is not None:
        # updating user_id value
        user_id = optional_parameters["provided_user_id"]

    try:
        # get historical data
        if get_historical_data == "true":
            # getting respective user accommodation file data (from UserAccommodationFileReportDataVW)
            user_accommodation_file_data = (
                UserAccommodationFileReportDataVW.objects.get(
                    id=user_accommodation_file_id,
                    user_id=user_id,
                    history_id=optional_parameters["history_id"],
                )
            )

            # serializing data
            serialized_data = UserAccommodationFileReportDataViewSerializer(
                user_accommodation_file_data, many=False
            ).data

        # get current data
        else:
            # getting respective user accommodation file data (from UserAccommodationFileDataForDetailsPopupVW)
            user_accommodation_file_data = (
                UserAccommodationFileDataForDetailsPopupVW.objects.get(
                    id=user_accommodation_file_id, user_id=user_id
                )
            )

            # serializing data
            serialized_data = UserAccommodationFileDataForDetailsPopupViewSerializer(
                user_accommodation_file_data, many=False
            ).data

        return Response(serialized_data)
    except UserAccommodationFile.DoesNotExist:
        return Response(
            {
                "error": "User Accommodation File either not found or you do not have rights to access it"
            },
            status=status.HTTP_404_NOT_FOUND,
        )


def get_accommodation_file_available_reports(request):
    success, parameters = get_needed_parameters(["user_accommodation_file_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    user_accommodation_file_id = itemgetter("user_accommodation_file_id")(parameters)

    # getting all available reports for specified user accommodation file
    available_reports = UserAccommodationFileReportDataVW.objects.filter(
        id=user_accommodation_file_id
    )

    # serializing data
    serialized_data = UserAccommodationFileReportDataViewSerializer(
        available_reports, many=True
    ).data

    # ordering by history_date (DESC)
    ordered_serialized_data = sorted(
        serialized_data,
        key=lambda k: k["history_date"],
        reverse=True,
    )

    return Response(ordered_serialized_data)


def create_user_accommodation_file_comment(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(
        ["user_accommodation_file_id", "comment"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    user_accommodation_file_id, comment = itemgetter(
        "user_accommodation_file_id", "comment"
    )(parameters)

    try:
        # creating new comment
        UserAccommodationFileComments.objects.create(
            user_accommodation_file_id=user_accommodation_file_id,
            user_id=user_info["user_id"],
            comment=comment,
        )

        # getting existing comments
        existing_comments = UserAccommodationFileComments.objects.filter(
            user_accommodation_file_id=user_accommodation_file_id
        ).order_by("-modify_date")
        serialized_data = UserAccommodationFileCommentsSerializer(
            existing_comments, many=True
        ).data

        data = {"file_comments": serialized_data, "ok": True}

        return Response(data)

    except:
        return Response(
            {
                "error": "Something wrong happened during the create user accommodation file comment process"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


def delete_user_accommodation_file_comment(request):
    success, parameters = get_needed_parameters(
        ["user_accommodation_file_id", "comment_id"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    user_accommodation_file_id, comment_id = itemgetter(
        "user_accommodation_file_id", "comment_id"
    )(parameters)

    try:
        # getting specified comment
        comment = UserAccommodationFileComments.objects.get(id=comment_id)
        # deleting comment
        comment.delete()

        # getting existing comments
        existing_comments = UserAccommodationFileComments.objects.filter(
            user_accommodation_file_id=user_accommodation_file_id
        ).order_by("-modify_date")
        serialized_data = UserAccommodationFileCommentsSerializer(
            existing_comments, many=True
        ).data

        data = {"file_comments": serialized_data, "ok": True}

        return Response(data)

    except UserAccommodationFileComments.DoesNotExist:
        return Response(
            {"error": "The specified comment does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def get_available_tests_to_administer_for_selected_user_accommodation_file(request):
    success, parameters = get_needed_parameters(["test_skill_type_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    test_skill_type_id = itemgetter("test_skill_type_id")(parameters)

    optional_parameters = get_optional_parameters(["test_skill_sub_type_id"], request)

    # getting matching test definitions
    test_definitions = UserAccommodationFileTestsToAdministerVW.objects.filter(
        test_skill_type_id=test_skill_type_id,
        test_skill_sub_type_id=optional_parameters["test_skill_sub_type_id"],
    )

    # serializing data
    serialized_data = UserAccommodationFileTestsToAdministerViewSerializer(
        test_definitions, many=True
    ).data

    # ordering by test_code and version
    ordered_serialized_data = sorted(
        serialized_data,
        key=lambda k: (k["test_code"].lower(), k["version"]),
        reverse=False,
    )

    return Response(ordered_serialized_data)


def get_user_accommodation_file_status_options(request):
    # getting all existing active user accommodation file statuses that are considered "OPEN STATUSES"
    respective_statuses = UserAccommodationFileStatus.objects.filter(
        active=1,
        codename__in=USER_ACCOMMODATION_FILE_STATUSES_DROPDOWN_AVAILABLE_CODENAMES,
    )

    serialized_status_data = UserAccommodationFileStatusSerializer(
        respective_statuses, many=True
    ).data

    serialized_status_data.sort(key=lambda x: (x["order"]), reverse=False)

    temp_array = []

    for respective_status_data in serialized_status_data:
        # getting related user accommodation file status text data
        related_status_text_data = UserAccommodationFileStatusText.objects.filter(
            user_accommodation_file_status_id=respective_status_data["id"]
        )

        # serializing data
        serialized_data = UserAccommodationFileStatusTextSerializer(
            related_status_text_data, many=True
        ).data

        language_obj = create_language_compatible_object(serialized_data)

        temp_array.append(
            {
                "status_id": respective_status_data["id"],
                "status_codename": respective_status_data["codename"],
                "status_data": language_obj,
            }
        )

    return Response(temp_array)


def get_user_accommodation_file_complexity_options(request):
    # getting all existing active user accommodation file complexities
    complexity_objects = UserAccommodationFileComplexity.objects.filter(
        active=1,
    )

    serialized_complexity_data = UserAccommodationFileComplexitySerializer(
        complexity_objects, many=True
    ).data

    serialized_complexity_data.sort(key=lambda x: (x["order"]), reverse=False)

    temp_array = []

    for complexity_data in serialized_complexity_data:
        # getting related user accommodation file complexity text data
        complexity_text_data = UserAccommodationFileComplexityText.objects.filter(
            user_accommodation_file_complexity_id=complexity_data["id"]
        )

        # serializing data
        serialized_data = UserAccommodationFileComplexityTextSerializer(
            complexity_text_data, many=True
        ).data

        language_obj = create_language_compatible_object(serialized_data)

        temp_array.append(
            {
                "complexity_id": complexity_data["id"],
                "complexity_codename": complexity_data["codename"],
                "complexity_data": language_obj,
            }
        )

    return Response(temp_array)


def save_selected_user_accommodation_file_data(request):
    user_accommodation_file_data = json.loads(request.body)
    user_info = get_user_info_from_jwt_token(request)

    try:
        with transaction.atomic():
            # getting CANCELLED status ID
            cancelled_by_candidate_status_id = UserAccommodationFileStatus.objects.get(
                codename=UserAccommodationFileStatusConst.CANCELLED_BY_CANDIDATE
            ).id

            # getting related user accommodation file
            related_user_accommodation_file = UserAccommodationFile.objects.get(
                id=user_accommodation_file_data["id"]
            )

            # ==================== TEST IN PROGRESS OR CANCELLED BY CANDIDATE VALIDATION ====================
            # initializing related_test_in_active_or_end_state_or_already_cancelled
            related_test_in_active_or_end_state_or_already_cancelled = False
            # getting needed status IDs
            active_status_id = get_assigned_test_status_id(AssignedTestStatus.ACTIVE)
            locked_status_id = get_assigned_test_status_id(AssignedTestStatus.LOCKED)
            paused_status_id = get_assigned_test_status_id(AssignedTestStatus.PAUSED)
            submitted_status_id = get_assigned_test_status_id(
                AssignedTestStatus.SUBMITTED
            )
            quit_status_id = get_assigned_test_status_id(AssignedTestStatus.QUIT)
            # checking if there is a current active (active, locked, paused, submitted or quit) assigned test related to that user accommodation file
            related_assigned_tests = AssignedTest.objects.filter(
                user_accommodation_file_id=related_user_accommodation_file.id,
                status_id__in=(
                    active_status_id,
                    locked_status_id,
                    paused_status_id,
                    submitted_status_id,
                    quit_status_id,
                ),
            )
            if related_assigned_tests:
                # setting related_test_in_active_or_end_state_or_already_cancelled to True
                related_test_in_active_or_end_state_or_already_cancelled = True

            # checking if status is currently cancelled
            elif (
                related_user_accommodation_file.status_id
                == cancelled_by_candidate_status_id
            ):
                # setting related_test_in_active_or_end_state_or_already_cancelled to True
                related_test_in_active_or_end_state_or_already_cancelled = True
            # ==================== TEST IN PROGRESS OR CANCELLED BY CANDIDATE VALIDATION (END) ====================

            # ==================== TEST TO ADMINISTER ====================
            # updating test_center_id (if supervised request)
            if not related_user_accommodation_file.is_uit:
                related_user_accommodation_file.test_center_id = (
                    user_accommodation_file_data["test_center_id"]
                )
            # deleting existing entry (if needed)
            UserAccommodationFileTestToAdminister.objects.filter(
                user_accommodation_file_id=related_user_accommodation_file.id
            ).delete()
            # creating new entry based on provided parameters
            # test_id provided (existing test in CAT)
            if (
                "test" in user_accommodation_file_data["test_to_administer"]
                and user_accommodation_file_data["test_to_administer"]["test"]
                is not None
            ):
                UserAccommodationFileTestToAdminister.objects.create(
                    user_accommodation_file_id=related_user_accommodation_file.id,
                    test_id=user_accommodation_file_data["test_to_administer"]["test"],
                    specified_test_description=None,
                )
            # specified_test_description provided (test outside of CAT)
            elif (
                "specified_test_description"
                in user_accommodation_file_data["test_to_administer"]
                and user_accommodation_file_data["test_to_administer"][
                    "specified_test_description"
                ]
                is not None
            ):
                UserAccommodationFileTestToAdminister.objects.create(
                    user_accommodation_file_id=related_user_accommodation_file.id,
                    test_id=None,
                    specified_test_description=user_accommodation_file_data[
                        "test_to_administer"
                    ]["specified_test_description"],
                )
            # ==================== TEST TO ADMINISTER (END) ====================

            # ==================== BREAK BANK ====================
            # deleting existing entry (if needed)
            UserAccommodationFileBreakBank.objects.filter(
                user_accommodation_file_id=related_user_accommodation_file.id
            ).delete()

            # creating new entry based on provided parameters (if break bank is greater than 0)
            if user_accommodation_file_data["break_bank"] > 0:
                UserAccommodationFileBreakBank.objects.create(
                    user_accommodation_file_id=user_accommodation_file_data["id"],
                    break_time=user_accommodation_file_data["break_bank"],
                )
            # ==================== BREAK BANK (END) ====================

            # ==================== TEST SECTIONS TIME ====================
            # deleting existing entry (if needed)
            UserAccommodationFileTestTime.objects.filter(
                user_accommodation_file_id=related_user_accommodation_file.id
            ).delete()

            # looping in test_sections_time
            for test_section_time_data in user_accommodation_file_data[
                "test_sections_time"
            ]:
                # creating respective entry
                UserAccommodationFileTestTime.objects.create(
                    user_accommodation_file_id=user_accommodation_file_data["id"],
                    test_section_id=test_section_time_data["test_section_id"],
                    test_section_time=test_section_time_data["test_section_time"],
                )
            # ==================== TEST SECTIONS TIME (END) ====================

            # ==================== OTHER MEASURES ====================
            # deleting existing entry (if needed)
            UserAccommodationFileOtherMeasures.objects.filter(
                user_accommodation_file_id=related_user_accommodation_file.id
            ).delete()

            # if other measures have been provided (not empty string)
            if user_accommodation_file_data["other_measures"] != "":
                UserAccommodationFileOtherMeasures.objects.create(
                    user_accommodation_file_id=user_accommodation_file_data["id"],
                    other_measures=user_accommodation_file_data["other_measures"],
                )
            # ==================== OTHER MEASURES (END) ====================

            # ==================== FILE ADMINISTRATION ====================
            # setting assigned to
            related_user_accommodation_file.assigned_to_user_id = (
                user_accommodation_file_data["assigned_to_user_id"]
            )
            # setting complexity
            related_user_accommodation_file.complexity_id = (
                user_accommodation_file_data["complexity_id"]
            )
            # updating status (only if related_test_in_active_or_end_state_or_already_cancelled is set to False)
            if not related_test_in_active_or_end_state_or_already_cancelled:
                related_user_accommodation_file.status_id = (
                    user_accommodation_file_data["status_id"]
                )
            # ==================== FILE ADMINISTRATION (END) ====================

            # ==================== RATIONALE ====================
            related_user_accommodation_file.rationale = user_accommodation_file_data[
                "rationale"
            ]
            # ==================== RATIONALE (END) ====================

            # ==================== STATS - CANDIDATE LIMITATIONS ====================
            # deleting existing candidate limitations stats
            UserAccommodationFileCandidateLimitations.objects.filter(
                user_accommodation_file_id=user_accommodation_file_data["id"]
            ).delete()

            # creating new user accommodation file candidate limitations entries (if needed)
            for candidate_limitations_option_id in user_accommodation_file_data[
                "candidate_limitations_stats_selected_options"
            ]:
                UserAccommodationFileCandidateLimitations.objects.create(
                    user_accommodation_file_id=user_accommodation_file_data["id"],
                    user_accommodation_file_candidate_limitations_option_id=candidate_limitations_option_id,
                )
            # ==================== STATS - CANDIDATE LIMITATIONS (END) ====================

            # ==================== STATS - RECOMMENDATIONS ====================
            # deleting existing candidate limitations stats
            UserAccommodationFileRecommendations.objects.filter(
                user_accommodation_file_id=user_accommodation_file_data["id"]
            ).delete()

            # creating new user accommodation file candidate limitations entries (if needed)
            for recommendations_option_id in user_accommodation_file_data[
                "recommendations_stats_selected_options"
            ]:
                UserAccommodationFileRecommendations.objects.create(
                    user_accommodation_file_id=user_accommodation_file_data["id"],
                    user_accommodation_file_recommendations_option_id=recommendations_option_id,
                )
            # ==================== STATS - RECOMMENDATIONS (END) ====================

            # updating last_modified_by_user_id field (UserAccommodationFile)
            related_user_accommodation_file.last_modified_by_user_id = user_info[
                "user_id"
            ]
            related_user_accommodation_file.save()

            # getting most updated data
            most_updated_data = SelectedUserAccommodationFileVW.objects.get(
                id=related_user_accommodation_file.id
            )

            # serializing data
            serialized_data = SelectedUserAccommodationFileViewSerializer(
                most_updated_data, many=False
            ).data

            # Getting user accommodations profile for the User, based on the created_date of the Accommodations File Request
            serialized_data["user_accommodations_profile"] = (
                get_user_accommodations_profile_for_request(
                    related_user_accommodation_file.user_id,
                    related_user_accommodation_file.modify_date,
                )
            )

            serialized_data["ok"] = True

            # related tests in active or end state found
            if related_test_in_active_or_end_state_or_already_cancelled:
                # TODO: make this a better status for unit tests or better understanding of the situation
                serialized_data["status"] = 421
                return Response(
                    serialized_data,
                    # TODO: make this a better status for unit tests or better understanding of the situation
                    status=status.HTTP_421_MISDIRECTED_REQUEST,
                )
            # no related tests in active or end state found
            else:
                serialized_data["status"] = 200
                return Response(serialized_data)

    except Exception as error:
        # printing error type
        print(type(error))
        return Response(
            {
                "error": "Something happened during the save selected user accommodation file process"
            },
            status=status.HTTP_409_CONFLICT,
        )


def get_modified_test_access_code(user_accommodation_file_id, test_access_code):
    # getting number of requests with the same user accommodation file ID
    number_of_requests = len(
        AssignedTest.objects.filter(
            user_accommodation_file_id=user_accommodation_file_id
        )
    )
    # adding "_X" to old test access code
    modified_test_access_code = test_access_code + "_{0}".format(number_of_requests)
    return modified_test_access_code


def handle_assign_new_uit_test(
    related_assigned_test_data, initial_test_access_code, test_id_to_assign
):
    # getting checked in status ID
    checked_in_status_id = get_assigned_test_status_id(AssignedTestStatus.CHECKED_IN)

    # assigning new test (initial test)
    new_assigned_test = AssignedTest.objects.create(
        status_id=checked_in_status_id,
        test_id=test_id_to_assign,
        user_id=related_assigned_test_data.user_id,
        ta_user_id=related_assigned_test_data.ta_user_id,
        test_access_code=initial_test_access_code,
        test_order_number=related_assigned_test_data.test_order_number,
        test_session_language=None,
        uit_invite_id=related_assigned_test_data.uit_invite_id,
        orderless_financial_data_id=related_assigned_test_data.orderless_financial_data_id,
        accommodation_request_id=related_assigned_test_data.accommodation_request_id,
        user_accommodation_file_id=related_assigned_test_data.user_accommodation_file_id,
    )
    # creating all needed assigned test sections
    # find all test sections for this test and create an assigned test section for it
    test_sections = TestSection.objects.filter(test_definition_id=test_id_to_assign)
    for test_section in test_sections:
        # creating new assigned test section entry
        AssignedTestSection.objects.create(
            test_section_id=test_section.id,
            assigned_test_id=new_assigned_test.id,
            test_section_time=test_section.default_time,
        )


def handle_uit_test_association_for_accommodation_or_alternate_test_request(
    action, user_accommodation_file_data
):
    # getting unassigned status ID
    unassigned_status_id = get_assigned_test_status_id(AssignedTestStatus.UNASSIGNED)

    # SUBMIT_SEND ACTION
    if (
        action
        == UitTestAssociationForAccommodationOrAlternateTestRequestActions.SUBMIT_SEND
    ):
        # getting most recent related assigned test data
        related_assigned_test_data = (
            AssignedTest.objects.filter(
                user_accommodation_file_id=user_accommodation_file_data.id
            )
            .order_by("id")
            .last()
        )
        # getting user accommodation file test to administer data
        user_accommodation_file_test_to_administer_data = (
            UserAccommodationFileTestToAdminister.objects.get(
                user_accommodation_file_id=user_accommodation_file_data.id
            )
        )
        # test ID from assigned test DOES NOT match test ID from user accommodation file test to administer
        if (
            related_assigned_test_data.test_id
            != user_accommodation_file_test_to_administer_data.test_id
        ):
            # setting initial test access code
            initial_test_access_code = related_assigned_test_data.test_access_code
            # unassigning assigned test
            related_assigned_test_data.status_id = unassigned_status_id
            # test is defined in CAT
            if user_accommodation_file_test_to_administer_data.test_id is not None:
                # getting modified test access code (adding "_X" to TAC)
                modified_test_access_code = get_modified_test_access_code(
                    user_accommodation_file_data.id,
                    related_assigned_test_data.test_access_code,
                )
                # updating test access code
                related_assigned_test_data.test_access_code = modified_test_access_code
            related_assigned_test_data.save()
            # assigning new test (if defined)
            if user_accommodation_file_test_to_administer_data.test_id is not None:
                handle_assign_new_uit_test(
                    related_assigned_test_data,
                    initial_test_access_code,
                    user_accommodation_file_test_to_administer_data.test_id,
                )

    # REJECT_CANCEL ACTION
    elif (
        action
        == UitTestAssociationForAccommodationOrAlternateTestRequestActions.REJECT_CANCEL
    ):
        # getting first initial related assigned test data
        related_first_initial_assigned_test_data = (
            AssignedTest.objects.filter(
                user_accommodation_file_id=user_accommodation_file_data.id
            )
            .order_by("id")
            .first()
        )
        # getting user accommodation file test to administer data
        user_accommodation_file_test_to_administer_data = (
            UserAccommodationFileTestToAdminister.objects.get(
                user_accommodation_file_id=user_accommodation_file_data.id
            )
        )
        # test ID from first initial assigned test DOES NOT match test ID from user accommodation file test to administer
        if (
            related_first_initial_assigned_test_data.test_id
            != user_accommodation_file_test_to_administer_data.test_id
        ):
            # getting most recent assigned test data
            related_most_recent_assigned_test_data = (
                AssignedTest.objects.filter(
                    user_accommodation_file_id=user_accommodation_file_data.id
                )
                .order_by("id")
                .last()
            )
            # setting initial test access code
            initial_test_access_code = (
                related_most_recent_assigned_test_data.test_access_code
            )
            # unassigning most recent assigned test
            related_most_recent_assigned_test_data.status_id = unassigned_status_id
            # getting modified test access code (adding "_X" to TAC)
            modified_test_access_code = get_modified_test_access_code(
                user_accommodation_file_data.id,
                related_most_recent_assigned_test_data.test_access_code,
            )
            # updating test access code
            related_most_recent_assigned_test_data.test_access_code = (
                modified_test_access_code
            )
            related_most_recent_assigned_test_data.save()

            # assigning new test (initial test)
            handle_assign_new_uit_test(
                related_most_recent_assigned_test_data,
                initial_test_access_code,
                related_first_initial_assigned_test_data.test_id,
            )

    # ACTION NOT SUPPORTED
    else:
        return Response(
            {
                "error": "The specified action is not supported in handle_uit_test_association_for_accommodation_or_alternate_test_request"
            },
            status=status.HTTP_405_METHOD_NOT_ALLOWED,
        )


def trigger_selected_user_accommodation_file_data_action(request):
    success, parameters = get_needed_parameters(
        [
            "user_accommodation_file_id",
            "action",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    user_accommodation_file_id, action = itemgetter(
        "user_accommodation_file_id", "action"
    )(parameters)

    # getting RE_OPENED status ID
    re_opened_status_id = UserAccommodationFileStatus.objects.get(
        codename=UserAccommodationFileStatusConst.RE_OPENED
    ).id
    # getting CANCELLED status ID
    cancelled_status_id = UserAccommodationFileStatus.objects.get(
        codename=UserAccommodationFileStatusConst.CANCELLED
    ).id
    # getting CANCELLED_BY_CANDIDATE status ID
    cancelled_by_candidate_status_id = UserAccommodationFileStatus.objects.get(
        codename=UserAccommodationFileStatusConst.CANCELLED_BY_CANDIDATE
    ).id

    try:
        # getting respective user accommodation file data
        user_accommodation_file_data = UserAccommodationFile.objects.get(
            id=user_accommodation_file_id
        )

        # ==================== TEST IN PROGRESS OR CANCELLED BY CANDIDATE VALIDATION ====================
        # getting needed status IDs
        active_status_id = get_assigned_test_status_id(AssignedTestStatus.ACTIVE)
        locked_status_id = get_assigned_test_status_id(AssignedTestStatus.LOCKED)
        paused_status_id = get_assigned_test_status_id(AssignedTestStatus.PAUSED)
        submitted_status_id = get_assigned_test_status_id(AssignedTestStatus.SUBMITTED)
        quit_status_id = get_assigned_test_status_id(AssignedTestStatus.QUIT)
        # checking if there is a current active (active, locked, paused, submitted or quit) assigned test related to that user accommodation file
        related_assigned_tests = AssignedTest.objects.filter(
            user_accommodation_file_id=user_accommodation_file_data.id,
            status_id__in=(
                active_status_id,
                locked_status_id,
                paused_status_id,
                submitted_status_id,
                quit_status_id,
            ),
        )
        if related_assigned_tests:
            # getting and returning most updated file data
            most_updated_data = SelectedUserAccommodationFileVW.objects.get(
                id=user_accommodation_file_id
            )

            # serializing data
            serialized_data = SelectedUserAccommodationFileViewSerializer(
                most_updated_data, many=False
            ).data

            # Getting user accommodations profile for the User, based on the created_date of the Accommodations File Request
            serialized_data["user_accommodations_profile"] = (
                get_user_accommodations_profile_for_request(
                    user_accommodation_file_data.user_id,
                    user_accommodation_file_data.modify_date,
                )
            )

            serialized_data["ok"] = True
            # TODO: make this a better status for unit tests or better understanding of the situation
            serialized_data["status"] = 405
            serialized_data["error"] = (
                "You are not allowed reject/complete this request, since the candidate decided to cancel it"
            )

            return Response(
                serialized_data,
                # TODO: make this a better status for unit tests or better understanding of the situation
                status=status.HTTP_405_METHOD_NOT_ALLOWED,
            )

        # checking if status is currently cancelled
        if user_accommodation_file_data.status_id == cancelled_by_candidate_status_id:
            # getting and returning most updated file data
            most_updated_data = SelectedUserAccommodationFileVW.objects.get(
                id=user_accommodation_file_id
            )

            # serializing data
            serialized_data = SelectedUserAccommodationFileViewSerializer(
                most_updated_data, many=False
            ).data

            # Getting user accommodations profile for the User, based on the created_date of the Accommodations File Request
            serialized_data["user_accommodations_profile"] = (
                get_user_accommodations_profile_for_request(
                    user_accommodation_file_data.user_id,
                    user_accommodation_file_data.modify_date,
                )
            )

            serialized_data["ok"] = True
            # TODO: make this a better status for unit tests or better understanding of the situation
            serialized_data["status"] = 405
            serialized_data["error"] = (
                "You are not allowed reject/complete this request, since the status is already cancelled by the candidate"
            )

            return Response(
                serialized_data,
                # TODO: make this a better status for unit tests or better understanding of the situation
                status=status.HTTP_405_METHOD_NOT_ALLOWED,
            )
        # ==================== TEST IN PROGRESS OR CANCELLED BY CANDIDATE VALIDATION (END) ====================

        # REJECT/CANCEL ACTION
        if action == UserAccommodationFileStatusConst.CANCELLED:
            # request from UIT test
            if user_accommodation_file_data.is_uit:
                # handling test association for this request
                handle_uit_test_association_for_accommodation_or_alternate_test_request(
                    UitTestAssociationForAccommodationOrAlternateTestRequestActions.REJECT_CANCEL,
                    user_accommodation_file_data,
                )

            # request from supervised test
            else:
                # checking if there is a related test session
                related_test_sessions = TestCenterTestSessions.objects.filter(
                    user_accommodation_file_id=user_accommodation_file_data.id,
                )
                # looping in related_test_sessions
                for related_test_session in related_test_sessions:
                    temp_test_session_data_id = (
                        related_test_session.test_session_data_id
                    )
                    # deleting related test session
                    related_test_session.delete()
                    # deleted related test session data
                    TestCenterTestSessionData.objects.filter(
                        id=temp_test_session_data_id
                    ).delete()

                # getting related consumed reservation code data
                related_consumed_reservation_code_data = (
                    ConsumedReservationCodes.objects.get(
                        user_accommodation_file_id=user_accommodation_file_data.id
                    )
                )
                # updating status to requested_accommodation
                related_consumed_reservation_code_data.status = (
                    ConsumedReservationCodeStatus.objects.get(
                        codename=StaticConsumedReservationCodeStatus.RESERVED
                    )
                )
                related_consumed_reservation_code_data.save()

            # updating status to CANCELLED
            user_accommodation_file_data.status_id = cancelled_status_id
            user_accommodation_file_data.save()

            email_detail = SelectedUserAccommodationFileVW.objects.get(
                id=user_accommodation_file_data.id
            )

            canceled_accomodations_request_send_all(email_detail)

        # COMPLETE ACTION (PENDING APPROVAL STATE)
        elif action == UserAccommodationFileStatusConst.PENDING_APPROVAL:
            # request from UIT test
            if user_accommodation_file_data.is_uit:
                # handling test association for this request
                handle_uit_test_association_for_accommodation_or_alternate_test_request(
                    UitTestAssociationForAccommodationOrAlternateTestRequestActions.SUBMIT_SEND,
                    user_accommodation_file_data,
                )

            # getting PENDING APPROVAL status ID
            pending_approval_status_id = UserAccommodationFileStatus.objects.get(
                codename=UserAccommodationFileStatusConst.PENDING_APPROVAL
            ).id
            # updating status to PENDING APPROVAL
            user_accommodation_file_data.status_id = pending_approval_status_id
            user_accommodation_file_data.save()

            # sending notification email to candidate
            user_accommodation_file_data_from_view = (
                UserAccommodationFileVW.objects.get(id=user_accommodation_file_id)
            )
            serialized_user_accommodation_file_data_from_view = (
                UserAccommodationFileViewSerializer(
                    user_accommodation_file_data_from_view, many=False
                ).data
            )
            # adding email attribute to serialized_user_accommodation_file_data_from_view
            serialized_user_accommodation_file_data_from_view["email"] = (
                serialized_user_accommodation_file_data_from_view["user_email"]
            )

            # NULL test_skill_sub_type_name_en or test_skill_sub_type_name_fr
            if (
                serialized_user_accommodation_file_data_from_view[
                    "test_skill_sub_type_name_en"
                ]
                is None
                or serialized_user_accommodation_file_data_from_view[
                    "test_skill_sub_type_name_fr"
                ]
                is None
            ):
                # setting test_skill_sub_type_name_en and test_skill_sub_type_name_fr to ""
                serialized_user_accommodation_file_data_from_view[
                    "test_skill_sub_type_name_en"
                ] = ""
                serialized_user_accommodation_file_data_from_view[
                    "test_skill_sub_type_name_fr"
                ] = ""

            # initializing and setting reference_number
            reference_number = ""
            # supervised process
            if not user_accommodation_file_data.is_uit:
                # getting assessment_process_assigned_test_specs_id from the consumed reservation code table
                assessment_process_assigned_test_specs_id = (
                    ConsumedReservationCodes.objects.filter(
                        user_accommodation_file_id=user_accommodation_file_id
                    )
                    .last()
                    .assessment_process_assigned_test_specs_id
                )
                # getting reference number from the AssessmentProcessAssignedTestSpecsVW
                reference_number = (
                    AssessmentProcessAssignedTestSpecsVW.objects.filter(
                        id=assessment_process_assigned_test_specs_id
                    )
                    .last()
                    .assessment_process_reference_number
                )
            serialized_user_accommodation_file_data_from_view["reference_number"] = (
                reference_number
            )

            # sending email
            send_accommodations_request_pending_approval_email(
                serialized_user_accommodation_file_data_from_view
            )

        # RE-OPEN ACTION
        elif action == UserAccommodationFileStatusConst.RE_OPENED:
            # request from UIT test
            if user_accommodation_file_data.is_uit:
                # handling test association for this request
                handle_uit_test_association_for_accommodation_or_alternate_test_request(
                    UitTestAssociationForAccommodationOrAlternateTestRequestActions.REJECT_CANCEL,
                    user_accommodation_file_data,
                )

            # request from supervised test
            else:
                # checking if there is a related test session
                related_test_sessions = TestCenterTestSessions.objects.filter(
                    user_accommodation_file_id=user_accommodation_file_data.id,
                )
                # looping in related_test_sessions
                for related_test_session in related_test_sessions:
                    temp_test_session_data_id = (
                        related_test_session.test_session_data_id
                    )
                    # deleting related test session
                    related_test_session.delete()
                    # deleted related test session data
                    TestCenterTestSessionData.objects.filter(
                        id=temp_test_session_data_id
                    ).delete()

                # getting related consumed reservation code data
                related_consumed_reservation_code_data = (
                    ConsumedReservationCodes.objects.get(
                        user_accommodation_file_id=user_accommodation_file_data.id
                    )
                )
                # updating status to requested_accommodation
                related_consumed_reservation_code_data.status = ConsumedReservationCodeStatus.objects.get(
                    codename=StaticConsumedReservationCodeStatus.REQUESTED_ACCOMMODATION
                )
                related_consumed_reservation_code_data.save()

            # updating status to RE_OPENED
            user_accommodation_file_data.status_id = re_opened_status_id
            user_accommodation_file_data.save()

            email_detail = SelectedUserAccommodationFileVW.objects.get(
                id=user_accommodation_file_data.id
            )

            # send accomodation re-opemed to all tcms for the test center, the hr contact who created the process, and the candidate
            re_opened_accomodations_request_send_all(email_detail)

        # NON-SUPPORTED ACTION
        else:
            return Response(
                {"error": "This action is not supported"},
                status=status.HTTP_405_METHOD_NOT_ALLOWED,
            )

        # getting and returning most updated file data
        most_updated_data = SelectedUserAccommodationFileVW.objects.get(
            id=user_accommodation_file_id
        )

        # serializing data
        serialized_data = SelectedUserAccommodationFileViewSerializer(
            most_updated_data, many=False
        ).data

        # Getting user accommodations profile for the User, based on the created_date of the Accommodations File Request
        serialized_data["user_accommodations_profile"] = (
            get_user_accommodations_profile_for_request(
                user_accommodation_file_data.user_id,
                user_accommodation_file_data.modify_date,
            )
        )

        serialized_data["ok"] = True
        serialized_data["status"] = 200

        return Response(serialized_data)

    except UserAccommodationFile.DoesNotExist:
        return Response(
            {"error": "The specified user accommodation file does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def trigger_user_accommodation_file_status_update_as_a_candidate(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(
        ["user_accommodation_file_id", "action"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    user_accommodation_file_id, action = itemgetter(
        "user_accommodation_file_id", "action"
    )(parameters)

    try:
        # getting respective user accommodation file data
        user_accommodation_file_data = UserAccommodationFile.objects.get(
            id=user_accommodation_file_id, user_id=user_info["user_id"]
        )

        # CANCEL BY CANDIDATE ACTION
        if action == UserAccommodationFileStatusConst.CANCELLED_BY_CANDIDATE:
            # UIT test request
            if user_accommodation_file_data.is_uit:
                # handling test association for this request
                handle_uit_test_association_for_accommodation_or_alternate_test_request(
                    UitTestAssociationForAccommodationOrAlternateTestRequestActions.REJECT_CANCEL,
                    user_accommodation_file_data,
                )
            # getting CANCELLED BY CANDIDATE status ID
            cancelled_by_candidate_status_id = UserAccommodationFileStatus.objects.get(
                codename=UserAccommodationFileStatusConst.CANCELLED_BY_CANDIDATE
            ).id
            # updating status to CANCELLED
            user_accommodation_file_data.status_id = cancelled_by_candidate_status_id
            user_accommodation_file_data.save()

            # if supervised test request
            if not user_accommodation_file_data.is_uit:
                # getting related consumed reservation code data
                related_consumed_reservation_code_data = (
                    ConsumedReservationCodes.objects.get(
                        user_accommodation_file_id=user_accommodation_file_id
                    )
                )

                # updating status to reserved
                related_consumed_reservation_code_data.status = (
                    ConsumedReservationCodeStatus.objects.get(
                        codename=StaticConsumedReservationCodeStatus.RESERVED
                    )  # TODO this dies if you perform this action on a UIT
                )
                # unassigning user_accommodation_file_id and test_session_id
                related_consumed_reservation_code_data.user_accommodation_file = None
                related_consumed_reservation_code_data.test_session_id = None
                related_consumed_reservation_code_data.save()

                # checking if there is a related test session (supervised test request only)
                related_test_sessions = TestCenterTestSessions.objects.filter(
                    user_accommodation_file_id=user_accommodation_file_data.id,
                )
                # looping in related_test_sessions
                for related_test_session in related_test_sessions:
                    temp_test_session_data_id = (
                        related_test_session.test_session_data_id
                    )
                    # deleting related test session
                    related_test_session.delete()
                    # deleted related test session data
                    TestCenterTestSessionData.objects.filter(
                        id=temp_test_session_data_id
                    ).delete()

            # get the data to send the canceled by candidate email
            email_detail = SelectedUserAccommodationFileVW.objects.get(
                id=user_accommodation_file_data.id
            )

            # send cancelation by candidate email to all tcms for the test center and the hr contact who created the process
            canceled_accomodations_request_candidate_send_all(email_detail)

        # COMPLETE ACTION (Accommodation Request Approval)
        elif action == UserAccommodationFileStatusConst.COMPLETED:
            # getting COMPLETED status ID
            completed_status_id = UserAccommodationFileStatus.objects.get(
                codename=UserAccommodationFileStatusConst.COMPLETED
            ).id
            # updating status to COMPLETED
            user_accommodation_file_data.status_id = completed_status_id
            user_accommodation_file_data.save()

            email_detail = SelectedUserAccommodationFileVW.objects.get(
                id=user_accommodation_file_data.id
            )

            # send accomodation completed to all tcms for the test center, the hr contact who created the process, and the candidate
            completed_accomodations_request_send_all(email_detail)

        # NON-SUPPORTED ACTION
        else:
            return Response(
                {"error": "This action is not supported"},
                status=status.HTTP_405_METHOD_NOT_ALLOWED,
            )

        # getting and returning most updated user accommodation file data
        user_accommodation_file_data = (
            UserAccommodationFileDataForDetailsPopupVW.objects.get(
                id=user_accommodation_file_id, user_id=user_info["user_id"]
            )
        )

        # serializing data
        serialized_data = UserAccommodationFileDataForDetailsPopupViewSerializer(
            user_accommodation_file_data, many=False
        ).data

        serialized_data["ok"] = True

        return Response(serialized_data)

    except UserAccommodationFile.DoesNotExist:
        return Response(
            {"error": "The specified user accommodation file does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def get_all_pending_user_accommodation_files_as_a_tcm(request):
    success, parameters = get_needed_parameters(
        ["page", "page_size", "test_center_id"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, test_center_id = itemgetter("page", "page_size", "test_center_id")(
        parameters
    )

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting pending test center related user accommodation files (pending ==> completed from AAE)
    pending_test_center_related_user_accommodation_files = (
        CompletedSupervisedUserAccommodationFileVW.objects.filter(
            test_center_id=test_center_id,
        )
    )

    # serializing data
    serialized_data = CompletedSupervisedUserAccommodationFileViewSerializer(
        pending_test_center_related_user_accommodation_files, many=True
    ).data

    # ordering serialized data
    ordered_serialized_data = sorted(
        serialized_data,
        key=lambda k: (k["created_date"], k["user_last_name"].lower()),
        reverse=False,
    )

    # only getting data for current selected page
    pending_user_accommodation_files = ordered_serialized_data[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # returning data
    return paginator.get_paginated_response(
        pending_user_accommodation_files,
        len(ordered_serialized_data),
        current_page,
        page_size,
    )


def get_found_pending_user_accommodation_files_as_a_tcm(request):
    success, parameters = get_needed_parameters(
        ["keyword", "current_language", "page", "page_size", "test_center_id"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    keyword, current_language, page, page_size, test_center_id = itemgetter(
        "keyword", "current_language", "page", "page_size", "test_center_id"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # Blank search
    if keyword == " ":
        # getting all pending test center related user accommodation files (pending ==> completed from AAE)
        found_pending_test_center_related_user_accommodation_files = (
            CompletedSupervisedUserAccommodationFileVW.objects.filter(
                test_center_id=test_center_id,
            )
        )
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # English Search
        if current_language == Languages.EN:
            # getting matching pending test center related user accommodation files (pending ==> completed from AAE)
            found_pending_test_center_related_user_accommodation_files = (
                CompletedSupervisedUserAccommodationFileVW.objects.filter(
                    Q(test_center_id=test_center_id)
                    & (
                        Q(test_name_en__icontains=keyword)
                        | Q(version__icontains=keyword)
                        | Q(user_first_name__icontains=keyword)
                        | Q(user_last_name__icontains=keyword)
                        | Q(requesting_dept_abrv_en__icontains=keyword)
                        | Q(requesting_dept_desc_en__icontains=keyword)
                        | Q(closing_date__icontains=keyword)
                        | Q(created_date__icontains=keyword)
                    )
                )
            )
        # French Search
        else:
            # getting matching pending test center related user accommodation files (pending ==> completed from AAE)
            found_pending_test_center_related_user_accommodation_files = (
                CompletedSupervisedUserAccommodationFileVW.objects.filter(
                    Q(test_center_id=test_center_id)
                    & (
                        Q(test_name_fr__icontains=keyword)
                        | Q(version__icontains=keyword)
                        | Q(user_first_name__icontains=keyword)
                        | Q(user_last_name__icontains=keyword)
                        | Q(requesting_dept_abrv_fr__icontains=keyword)
                        | Q(requesting_dept_desc_fr__icontains=keyword)
                        | Q(closing_date__icontains=keyword)
                        | Q(created_date__icontains=keyword)
                    )
                )
            )

    # no results found
    if not found_pending_test_center_related_user_accommodation_files:
        return Response({"no results found"}, status=status.HTTP_200_OK)

    # serializing data
    serialized_data = CompletedSupervisedUserAccommodationFileViewSerializer(
        found_pending_test_center_related_user_accommodation_files, many=True
    ).data

    # ordering serialized data
    ordered_serialized_data = sorted(
        serialized_data,
        key=lambda k: (k["created_date"], k["user_last_name"].lower()),
        reverse=False,
    )

    # only getting data for current selected page
    found_pending_user_accommodation_files = ordered_serialized_data[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # returning data
    return paginator.get_paginated_response(
        found_pending_user_accommodation_files,
        len(ordered_serialized_data),
        current_page,
        page_size,
    )


def get_number_of_requests_received_aae_report_data(request):
    success, parameters = get_needed_parameters(["date_from", "date_to"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    date_from, date_to = itemgetter("date_from", "date_to")(parameters)

    # getting the number of requests per month based on provided time period
    number_of_requests_per_month = (
        # filtering based on provided date_from and date_to
        AaeReportsNumberOfRequestsReceivedVw.objects.filter(
            created_date__range=(date_from, date_to)
        )
        # grouping by month and calculating sum of number_of_requests
        .annotate(date=TruncMonth("created_date"))
        .values("date")
        .annotate(number_of_requests=Sum("number_of_requests"))
        .order_by()
    )

    # initializing final array
    final_array = []
    # loopint in number_of_requests_per_month
    for data in number_of_requests_per_month:
        # getting month as word
        month_as_word = get_month_as_a_word(data["date"].month)
        # getting quarter
        quarter = get_quarter_from_date(data["date"])

        # creating object to append to the final array
        obj = {
            "year": data["date"].year,
            "month": data["date"].month,
            "month_data": month_as_word,
            "quarter": quarter,
            "number_of_requests": data["number_of_requests"],
        }

        # populating final_array
        final_array.append(obj)

    return Response(final_array)


def get_number_of_closed_requests_aae_report_data(request):
    success, parameters = get_needed_parameters(["date_from", "date_to"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    date_from, date_to = itemgetter("date_from", "date_to")(parameters)

    # getting related closed requests
    related_closed_requests = AaeReportsNumberOfClosedRequestsVw.objects.filter(
        created_date__range=(date_from, date_to)
    )

    # serializing the data
    serialized_data = AaeReportsNumberOfClosedRequestsViewSerializer(
        related_closed_requests, many=True
    ).data

    return Response(serialized_data)
