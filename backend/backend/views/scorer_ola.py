import datetime
import json
from operator import itemgetter
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from django.conf import settings
from pytz import timezone
from backend.custom_models.test_center_ola_time_slot_assessor_availability import (
    TestCenterOlaTimeSlotAssessorAvailability,
)
from db_views.db_view_models.test_centers_vw import TestCentersVW
from backend.custom_models.test_center_ola_test_assessor_approved_language import (
    TestCenterOlaTestAssessorApprovedLanguage,
)
from backend.custom_models.test_center_test_session_data import (
    TestCenterTestSessionData,
)
from backend.custom_models.test_center_test_sessions import TestCenterTestSessions
from backend.custom_models.consumed_reservation_code_status import (
    ConsumedReservationCodeStatus,
)
from backend.custom_models.scorer_ola_assigned_test_session import (
    ScorerOlaAssignedTestSession,
)
from backend.static.consumed_reservation_code_statuses import (
    StaticConsumedReservationCodeStatus,
)
from text_resources_backend.text_resources import TextResources
from user_management.views.utils import CustomPagination
from cms.views.utils import get_needed_parameters, get_optional_parameters
from backend.custom_models.test_center_ola_time_slot import TestCenterOlaTimeSlot
from backend.custom_models.test_center_ola_test_assessor import (
    TestCenterOlaTestAssessor,
)
from backend.views.utils import get_user_info_from_jwt_token
from backend.static.languages import Languages
from backend.custom_models.scorer_ola_test_session_skip_option import (
    ScorerOlaTestSessionSkipOption,
)
from backend.custom_models.scorer_ola_test_session_skip_action import (
    ScorerOlaTestSessionSkipAction,
)
from backend.custom_models.consumed_reservation_codes import ConsumedReservationCodes
from db_views.serializers.scorer_ola_serializers import (
    ScorerOlaDetailedTestsToAssessAsSupervisorViewSerializer,
    ScorerOlaDetailedTestsToAssessViewSerializer,
    ScorerOlaTestsToAssessViewSerializer,
)
from db_views.db_view_models.scorer_ola_tests_to_assess_vw import (
    ScorerOlaTestsToAssessVw,
)
from db_views.serializers.test_center_serializers import (
    ScorerOlaTestSessionSkipOptionSerializer,
    TestCenterOlaTestAssessorSerializer,
    TestCenterOlaTestAssessorsViewSerializer,
    TestCenterTestSessionsViewSerializer,
)
from db_views.db_view_models.scorer_ola_detailed_tests_to_assess_vw import (
    ScorerOlaDetailedTestsToAssessVw,
)
from db_views.db_view_models.test_center_ola_test_assessors_vw import (
    TestCenterOlaTestAssessorsVw,
)
from db_views.db_view_models.test_center_ola_time_slots_vw import (
    TestCenterOlaTimeSlotsVw,
)
from db_views.db_view_models.test_center_test_sessions_vw import (
    TestCenterTestSessionsVW,
)
from backend.views.gc_notify_view import (
    is_gc_notify_active,
    test_bumped_by_tc,
)
from db_views.db_view_models.candidate_reservations_vw import (
    CandidateReservationsVW,
)
from backend.views.virtual_meetings_views import cancel_virtual_meeting


# getting scorer OLA tests to assess (as supervisor)
def get_scorer_ola_tests_to_assess_as_supervisor(request):
    # getting user info based on token
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(["date_from", "date_to"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    date_from, date_to = itemgetter("date_from", "date_to")(parameters)

    # converting provided date (string) to date
    date_from = datetime.datetime.strptime(date_from, "%Y-%m-%d").date()
    date_to = datetime.datetime.strptime(date_to, "%Y-%m-%d").date()

    # innitializing final_array
    final_array = []

    # getting test_assessor_related_ids_and_test_center_ids
    test_assessor_related_ids_and_test_center_ids = (
        TestCenterOlaTestAssessor.objects.filter(user_id=user_info["user_id"])
    )

    serialized_test_assessor_related_ids_and_test_center_ids = (
        TestCenterOlaTestAssessorSerializer(
            test_assessor_related_ids_and_test_center_ids, many=True
        ).data
    )

    # getting all standard detailed tests to assess between provided date_from and date_to
    detailed_tests_to_assess_for_the_day = (
        ScorerOlaDetailedTestsToAssessVw.objects.filter(
            date__gte=date_from, date__lte=date_to, is_standard=1
        )
    )

    # looping in serialized_test_assessor_related_ids_and_test_center_ids
    for test_assessor_data in serialized_test_assessor_related_ids_and_test_center_ids:
        # get test_assessor_related_time_slots
        test_to_assess_related_time_slots = TestCenterOlaTimeSlot.objects.filter(
            test_center_id=test_assessor_data["test_center_id"],
            assessed_language__in=test_assessor_data["approved_language"],
        )

        # initializing tests_to_assess_to_keep
        tests_to_assess_to_keep = []

        # looping in tests_to_assess
        for test_to_assess_data in detailed_tests_to_assess_for_the_day:
            # any of the time slot matches the test to assess data of the current iteration
            if any(
                True
                for data in test_to_assess_related_time_slots
                if data.day_of_week_id == test_to_assess_data.day_of_week_id
                and str(data.start_time)[:5]
                == str(test_to_assess_data.simplified_start_time)[:5]
                and str(data.end_time)[:5]
                == str(test_to_assess_data.simplified_end_time)[:5]
            ):
                tests_to_assess_to_keep.append(test_to_assess_data)

            # populating final_array
            for tests_to_assess in tests_to_assess_to_keep:
                # making sure that this data is not already part of the final_array
                if tests_to_assess.id not in [data.id for data in final_array]:
                    final_array.append(tests_to_assess)

    context = {"user_id": user_info["user_id"]}

    serialized_data = ScorerOlaDetailedTestsToAssessAsSupervisorViewSerializer(
        final_array, many=True, context=context
    ).data

    # ordering by start time and language
    ordered_final_serialized_data = sorted(
        serialized_data,
        key=lambda k: (
            k["date"],
            k["start_time"],
            k["language_id"],
        ),
        reverse=False,
    )

    # retuning final data using pagination library
    return Response(ordered_final_serialized_data)


# getting available test assessors (as supervisor)
def get_available_test_assessors_as_supervisor(request):
    # getting user info based on token
    user_info = get_user_info_from_jwt_token(request)

    # start_time and end_time must be provided as HH:mm (not datetime)
    success, parameters = get_needed_parameters(
        [
            "day_of_week_id",
            "start_time",
            "simplified_start_time",
            "end_time",
            "simplified_end_time",
            "language_id",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    (
        day_of_week_id,
        start_time,
        simplified_start_time,
        end_time,
        simplified_end_time,
        language_id,
    ) = itemgetter(
        "day_of_week_id",
        "start_time",
        "simplified_start_time",
        "end_time",
        "simplified_end_time",
        "language_id",
    )(
        parameters
    )

    # initializing available_test_assessors
    available_test_assessors = []

    # getting user's related test center IDs (where user is set as a supervisor)
    related_test_center_ids = TestCenterOlaTestAssessor.objects.filter(
        user_id=user_info["user_id"], supervisor=1
    ).values_list("test_center_id", flat=True)

    # getting related test center OLA time slot data
    related_time_slot_data = TestCenterOlaTimeSlotsVw.objects.filter(
        day_of_week_id=day_of_week_id,
        start_time=simplified_start_time,
        end_time=simplified_end_time,
    )

    # looping in related_test_center_ids
    for test_center_id in related_test_center_ids:
        # checking if there is a related time slot match with the test center ID of the current iteration
        if any(
            True
            for data in related_time_slot_data
            if data.test_center_id == test_center_id
        ):
            # getting test center related test assessors (excluding current user)
            test_center_related_test_assessors = (
                TestCenterOlaTestAssessorsVw.objects.filter(
                    test_center_id=test_center_id
                ).exclude(user_id=user_info["user_id"])
            )

            # looping in test_center_related_test_assessors
            for test_assessor in test_center_related_test_assessors:
                # getting test assessor related OLA assigned test sessions
                test_assessor_assigned_test_sessions = (
                    ScorerOlaDetailedTestsToAssessVw.objects.filter(
                        test_assessor_user_id=test_assessor.user_id
                    )
                )
                # provided test session time is overlapping with one of the test_assessor_assigned_test_sessions
                if any(
                    True
                    for assigned_test_session_data in test_assessor_assigned_test_sessions
                    if assigned_test_session_data.end_time
                    > datetime.datetime.strptime(start_time, "%Y-%m-%dT%H:%M:%S%z")
                    and datetime.datetime.strptime(end_time, "%Y-%m-%dT%H:%M:%S%z")
                    > assigned_test_session_data.start_time
                ):
                    pass
                else:
                    # making sure that the test assessor of the current iteration is not already part of the available_test_assessors
                    if test_assessor.user_id not in [
                        data.user_id for data in available_test_assessors
                    ]:
                        # making sure that the test assessor of current iteration is certified in the language of the respective test
                        if TestCenterOlaTestAssessorApprovedLanguage.objects.filter(
                            test_center_ola_test_assessor_id=test_assessor.id,
                            language_id=language_id,
                        ):
                            # adding test assessor of current iteration to available_test_assessors
                            available_test_assessors.append(test_assessor)

    # serializing data
    serialized_data = TestCenterOlaTestAssessorsViewSerializer(
        available_test_assessors, many=True
    ).data

    return Response(serialized_data)


# getting available test assessors (as supervisor)
def cancel_test_session_as_supervisor(request):
    test_session_data = json.loads(request.body)

    # getting user info based on token
    user_info = get_user_info_from_jwt_token(request)

    # making sure that this user is a supervisor
    if not TestCenterOlaTestAssessor.objects.filter(
        user_id=user_info["user_id"], supervisor=1
    ):
        return Response(
            {
                "error": "you are not allowed to do this call",
            },
            status=status.HTTP_401_UNAUTHORIZED,
        )

    try:
        # get most of the info for the test_bumped_by_tc email
        candidate_reservation_code_obj = CandidateReservationsVW.objects.filter(
            id=test_session_data["consumed_reservation_code_id"]
        ).last()

        closing_date = (
            candidate_reservation_code_obj.assessment_process_closing_date.strftime(
                "%Y/%m/%d"
            )
        )

        personalization = {
            "test_skill_type_name_fr": candidate_reservation_code_obj.test_skill_type_fr_name,
            "test_skill_sub_type_name_fr": candidate_reservation_code_obj.test_skill_sub_type_fr_name,
            "test_skill_type_name_en": candidate_reservation_code_obj.test_skill_type_en_name,
            "test_skill_sub_type_name_en": candidate_reservation_code_obj.test_skill_sub_type_en_name,
            "assessment_process_closing_date": closing_date,
            "contact_email_for_candidates": candidate_reservation_code_obj.contact_email_for_candidates,
            "reservation_code": candidate_reservation_code_obj.reservation_code,
        }  # populate "test_session_date" and "test_session_start_time" later

        # checking if test session is assigned to a test assessor
        assigned_test_session_data = ScorerOlaAssignedTestSession.objects.filter(
            test_session_id=test_session_data["test_session_id"]
        )
        # deleting assigned test session (if needed)
        for test_session in assigned_test_session_data:
            test_session.delete()
        # getting related test session data
        related_test_session = TestCenterTestSessions.objects.get(
            id=test_session_data["test_session_id"]
        )
        related_test_session_data = TestCenterTestSessionData.objects.get(
            id=related_test_session.test_session_data_id
        )

        # convert dates/times for readability
        local_start_time = related_test_session_data.start_time.astimezone(
            timezone(settings.TIME_ZONE)
        )

        formatted_start_time = "{:d}:{:02d}".format(
            local_start_time.hour, local_start_time.minute
        )

        # finish populating personalization
        personalization["test_session_date"] = related_test_session_data.date.strftime(
            "%Y/%m/%d"
        )
        personalization["test_session_start_time"] = formatted_start_time

        # getting reserved status ID
        reserved_status_id = ConsumedReservationCodeStatus.objects.get(
            codename=StaticConsumedReservationCodeStatus.RESERVED
        ).id
        # getting related consumed reservation code data
        related_consumed_reservation_code_data = ConsumedReservationCodes.objects.get(
            id=test_session_data["consumed_reservation_code_id"]
        )
        # updating status to RESERVED
        related_consumed_reservation_code_data.status_id = reserved_status_id
        related_consumed_reservation_code_data.save()

        # delete the virtual teams data and cancel the teams invitation
        # this is unfortunately very slow
        cancel_virtual_meeting(
            related_consumed_reservation_code_data.candidate_id,
            test_session_data["test_session_id"],
        )

        # deleting test session related data
        related_test_session.delete()
        related_test_session_data.delete()

        # send an email to the respective candidate
        if is_gc_notify_active():
            test_bumped_by_tc(
                related_consumed_reservation_code_data.candidate.email, personalization
            )

        return Response(status=status.HTTP_200_OK)

    except TestCenterTestSessions.DoesNotExist:
        return Response(
            {"error": "test session not found based on provided parameters"},
            status=status.HTTP_404_NOT_FOUND,
        )
    except TestCenterTestSessionData.DoesNotExist:
        return Response(
            {"error": "test session data not found based on provided parameters"},
            status=status.HTTP_404_NOT_FOUND,
        )
    except ConsumedReservationCodes.DoesNotExist:
        return Response(
            {
                "error": "consumed reservation code not found based on provided parameters"
            },
            status=status.HTTP_404_NOT_FOUND,
        )


# getting found scorer OLA tests to assess
def get_scorer_ola_test_session_skip_options(request):
    # getting all active test session skip options
    test_session_skip_options = ScorerOlaTestSessionSkipOption.objects.filter(active=1)

    # serializing data
    serialized_data = ScorerOlaTestSessionSkipOptionSerializer(
        test_session_skip_options, many=True
    ).data

    return Response(serialized_data)


# getting highest priority candidate to assign as Scorer OLA
def get_highest_priority_candidate_to_assign_as_scorer_ola(request):
    # getting user info based on token
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(
        ["date", "start_time", "end_time", "provided_index"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    date, start_time, end_time, provided_index = itemgetter(
        "date", "start_time", "end_time", "provided_index"
    )(parameters)

    optional_parameters = get_optional_parameters(["reason_for_skipping_id"], request)

    # getting date, start_time and end_time matching test sessions (where status is booked, not paired)
    matching_test_sessions = ScorerOlaDetailedTestsToAssessVw.objects.filter(
        date=date,
        start_time=start_time,
        end_time=end_time,
        consumed_reservation_code_status_codename=StaticConsumedReservationCodeStatus.BOOKED,
    )

    # serializing data
    serialized_data = ScorerOlaDetailedTestsToAssessViewSerializer(
        matching_test_sessions, many=True
    ).data

    # ordering by highest priority candidates
    ordered_serialized_data = sorted(
        serialized_data,
        key=lambda k: (
            k["reason_for_testing_minimum_process_length"],
            k["assessment_process_closing_date"],
            k["booked_date"],
        ),
        reverse=False,
    )

    # initializing chosen_test_session
    chosen_test_session = None

    # looping in ordered_serialized_data
    for index, data in enumerate(ordered_serialized_data):
        # index of current iteration matches the provided index
        if index == int(provided_index):
            chosen_test_session = data

    # should never happen
    if chosen_test_session is None:
        return Response(
            {
                "error": "not able to get a chosen test session based on provided parameters"
            },
            status=status.HTTP_404_NOT_FOUND,
        )

    # reason_for_skipping_id is provided
    if optional_parameters["reason_for_skipping_id"] is not None:
        # creating new entry in ScorerOlaTestSessionSkipAction model
        ScorerOlaTestSessionSkipAction.objects.create(
            test_session_id=chosen_test_session["id"],
            test_assessor_id=user_info["user_id"],
            candidate_id=chosen_test_session["candidate_user_id"],
            scorer_ola_test_session_skip_option_id=optional_parameters[
                "reason_for_skipping_id"
            ],
        )

    return Response(chosen_test_session)


# assigning candidate as Scorer OLA
def assign_candidate_as_scorer_ola(request):
    test_session_data = json.loads(request.body)

    # getting user info based on token
    user_info = get_user_info_from_jwt_token(request)

    # provided test_assessor_user_id is not the same as the user_id from the user_info (token) ==> supervisor is trying to assign another assessor
    if test_session_data["test_assessor_user_id"] != user_info["user_id"]:
        # making sure that this user is a supervisor
        if not TestCenterOlaTestAssessor.objects.filter(
            user_id=user_info["user_id"], supervisor=1
        ):
            return Response(
                {
                    "error": "you are not allowed to do this call",
                },
                status=status.HTTP_401_UNAUTHORIZED,
            )

    # checking if candidate is already paired to another OLA test assessor
    paired_ola_status_id = ConsumedReservationCodeStatus.objects.get(
        codename=StaticConsumedReservationCodeStatus.PAIRED_OLA
    ).id
    already_paired = ConsumedReservationCodes.objects.filter(
        id=test_session_data["consumed_reservation_code_id"],
        status_id=paired_ola_status_id,
    )

    # already paired
    if already_paired:
        return Response(
            {
                "status": 409,
                "error": "this candidate is already paired to another test assessor",
                # already paired
                "error_code": 1,
            },
            status=status.HTTP_409_CONFLICT,
        )
    # available candidate
    else:
        # making sure that this new session is not conflicting with the test assessor's schedule (overlapping times)
        # getting new test session data
        new_test_session_data = TestCenterTestSessionsViewSerializer(
            TestCenterTestSessionsVW.objects.get(
                id=test_session_data["test_session_id"]
            ),
            many=False,
        ).data
        # getting current assigned test sessions
        current_assigned_test_sessions = ScorerOlaAssignedTestSession.objects.filter(
            test_assessor_id=test_session_data["test_assessor_user_id"]
        )
        # looping in current_assigned_test_sessions
        for assigned_test_session_data in current_assigned_test_sessions:
            # getting related test session data
            test_session_data_of_current_iteration = (
                TestCenterTestSessionsViewSerializer(
                    TestCenterTestSessionsVW.objects.get(
                        id=assigned_test_session_data.test_session_id
                    ),
                    many=False,
                ).data
            )
            # test session data of current iteration is conflicting with the new test session
            if (
                test_session_data_of_current_iteration["date"]
                == new_test_session_data["date"]
                and new_test_session_data["end_time"]
                > test_session_data_of_current_iteration["start_time"]
                and test_session_data_of_current_iteration["end_time"]
                > new_test_session_data["start_time"]
            ):
                # return error
                return Response(
                    {
                        "status": 409,
                        "error": "this test session is conflicting with the test assessor's schedule",
                        # conflicting schedule
                        "error_code": 2,
                    },
                    status=status.HTTP_409_CONFLICT,
                )

        # creating new entry in ScorerOlaAssignedTestSession model
        ScorerOlaAssignedTestSession.objects.create(
            test_session_id=test_session_data["test_session_id"],
            test_assessor_id=test_session_data["test_assessor_user_id"],
            candidate_id=test_session_data["candidate_user_id"],
        )

        # updating the consumed reservation code status
        related_consumed_reservation_code = ConsumedReservationCodes.objects.get(
            id=test_session_data["consumed_reservation_code_id"]
        )
        related_consumed_reservation_code.status_id = paired_ola_status_id
        related_consumed_reservation_code.save()

        # getting related test session
        related_test_session = TestCenterTestSessions.objects.get(
            id=test_session_data["test_session_id"]
        )
        # getting related test session data
        related_test_session_data = TestCenterTestSessionData.objects.get(
            id=related_test_session.test_session_data_id
        )
        # updating test_assessor_user
        related_test_session_data.test_assessor_user_id = test_session_data[
            "test_assessor_user_id"
        ]
        related_test_session_data.save()

    return Response(status=status.HTTP_200_OK)


# unassigning candidate as Scorer OLA
def unassign_candidate_as_scorer_ola(request):
    test_session_data = json.loads(request.body)

    # getting user info based on token
    user_info = get_user_info_from_jwt_token(request)

    # provided test_assessor_user_id is not the same as the user_id from the user_info (token) ==> supervisor is trying to unassign another assessor
    if test_session_data["test_assessor_user_id"] != user_info["user_id"]:
        # making sure that this user is a supervisor
        if not TestCenterOlaTestAssessor.objects.filter(
            user_id=user_info["user_id"], supervisor=1
        ):
            return Response(
                {
                    "error": "you are not allowed to do this call",
                },
                status=status.HTTP_401_UNAUTHORIZED,
            )

    try:
        # getting related assigned test session data
        related_assigned_test_session_data = ScorerOlaAssignedTestSession.objects.get(
            test_assessor_id=test_session_data["test_assessor_user_id"],
            test_session_id=test_session_data["test_session_id"],
        )

        # getting related detailed test to assess data
        related_detailed_test_to_assess_data = (
            ScorerOlaDetailedTestsToAssessVw.objects.get(
                id=test_session_data["test_session_id"]
            )
        )

        # deleting respective row
        related_assigned_test_session_data.delete()

        # updating the consumed reservation code status
        booked_status_id = ConsumedReservationCodeStatus.objects.get(
            codename=StaticConsumedReservationCodeStatus.BOOKED
        ).id
        related_consumed_reservation_code_data = ConsumedReservationCodes.objects.get(
            id=related_detailed_test_to_assess_data.consumed_reservation_code_id
        )
        related_consumed_reservation_code_data.status_id = booked_status_id
        related_consumed_reservation_code_data.save()

        # getting related test session
        related_test_session = TestCenterTestSessions.objects.get(
            id=test_session_data["test_session_id"]
        )
        # getting related test session data
        related_test_session_data = TestCenterTestSessionData.objects.get(
            id=related_test_session.test_session_data_id
        )
        # updating test_assessor_user
        related_test_session_data.test_assessor_user_id = None
        related_test_session_data.save()

        return Response(status=status.HTTP_200_OK)

    except ScorerOlaAssignedTestSession.DoesNotExist:
        return Response(
            {"error": "the assigned test session does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )


# getting Scorer OLA assigned tests
def get_scorer_ola_my_assigned_tests(request):
    # getting user info based on token
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(["date_from", "date_to"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    date_from, date_to = itemgetter("date_from", "date_to")(parameters)

    # converting provided date (string) to date
    date_from = datetime.datetime.strptime(date_from, "%Y-%m-%d").date()
    date_to = datetime.datetime.strptime(date_to, "%Y-%m-%d").date()

    # getting assigned scorer OLA test sessions between provided date_from and date_to
    assigned_test_session_ids = ScorerOlaAssignedTestSession.objects.filter(
        test_assessor_id=user_info["user_id"]
    ).values_list("test_session_id", flat=True)
    assigned_test_sessions = ScorerOlaDetailedTestsToAssessVw.objects.filter(
        id__in=assigned_test_session_ids, date__gte=date_from, date__lte=date_to
    )

    # serializing data
    serialized_data = ScorerOlaDetailedTestsToAssessViewSerializer(
        assigned_test_sessions, many=True
    ).data

    # ordering by start time
    ordered_serialized_data = sorted(
        serialized_data,
        key=lambda k: (k["date"], k["start_time"]),
        reverse=False,
    )

    # retuning final data using pagination library
    return Response(ordered_serialized_data)


# getting Scorer OLA assigned candidate details
def get_scorer_ola_assigned_candidate_details(request):
    success, parameters = get_needed_parameters(["test_session_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_session_id = itemgetter("test_session_id")(parameters)

    try:
        # getting related assigned test session data
        related_assigned_test_session_data = (
            ScorerOlaDetailedTestsToAssessVw.objects.get(id=test_session_id)
        )

        # serializing data
        serialized_data = ScorerOlaDetailedTestsToAssessViewSerializer(
            related_assigned_test_session_data, many=False
        ).data

        return Response(serialized_data)

    # should never happen
    except ScorerOlaDetailedTestsToAssessVw.DoesNotExist:
        return Response(
            {"error": "the specified test session ID does not exist"},
            status=status.HTTP_404_NOT_FOUND,
        )


# getting Scorer OLA my availability data
def get_scorer_ola_my_availability_data(request):
    # getting user info based on token
    user_info = get_user_info_from_jwt_token(request)

    # initializing final_array
    final_array = []

    # getting test_center_ola_test_assessor_data based on provided user ID (assessor might be part of more than one test centers, meaning having more than one associated test center OLA assessor ID)
    test_center_ola_test_assessor_data = TestCenterOlaTestAssessor.objects.filter(
        user_id=user_info["user_id"]
    )

    # setting test_center_ola_test_assessor_ids and associated_test_center_ids
    test_center_ola_test_assessor_ids = []
    associated_test_center_ids = []
    for test_assessor_data in test_center_ola_test_assessor_data:
        # populating arrays
        test_center_ola_test_assessor_ids.append(test_assessor_data.id)
        associated_test_center_ids.append(test_assessor_data.test_center_id)

    # getting all the respective assessor availability time slot IDs
    assessor_availability_time_slot_ids = (
        TestCenterOlaTimeSlotAssessorAvailability.objects.filter(
            test_center_ola_test_assessor_id__in=test_center_ola_test_assessor_ids
        ).values_list("test_center_ola_time_slot_id", flat=True)
    )

    # looping in associated_test_center_ids
    for associated_test_center_id in associated_test_center_ids:
        # getting language certification related to test center ID of current iteration
        related_test_center_ola_test_assessor_id = (
            TestCenterOlaTestAssessor.objects.get(
                user_id=user_info["user_id"], test_center_id=associated_test_center_id
            ).id
        )
        language_certifications = TestCenterOlaTestAssessorApprovedLanguage.objects.filter(
            test_center_ola_test_assessor_id=related_test_center_ola_test_assessor_id
        ).values_list(
            "language_id", flat=True
        )
        # getting respective test center data
        respective_test_center_data = TestCentersVW.objects.get(
            id=associated_test_center_id
        )
        # getting test center related time slots for current iteration
        test_center_related_time_slots = TestCenterOlaTimeSlotsVw.objects.filter(
            test_center_id=associated_test_center_id,
            assessed_language_id__in=language_certifications,
        )

        # looping in test_center_related_time_slots
        for test_center_related_time_slot in test_center_related_time_slots:
            # initializing available
            available = False
            # time slot ID of current iteration is part of the assessor_availability_time_slot_ids array
            if test_center_related_time_slot.id in assessor_availability_time_slot_ids:
                # setting available to True
                available = True

            # populating final_array
            final_array.append(
                {
                    "test_center_ola_time_slot_id": test_center_related_time_slot.id,
                    "test_center_name": respective_test_center_data.name,
                    "test_center_dept_id": respective_test_center_data.dept_id,
                    "test_center_dept_abrv_en": respective_test_center_data.dept_eabrv,
                    "test_center_dept_abrv_fr": respective_test_center_data.dept_fabrv,
                    "test_center_dept_name_en": respective_test_center_data.dept_edesc,
                    "test_center_dept_name_fr": respective_test_center_data.dept_fdesc,
                    "start_time": test_center_related_time_slot.start_time,
                    "end_time": test_center_related_time_slot.end_time,
                    "assessed_language_id": test_center_related_time_slot.assessed_language_id,
                    "assessed_language_text_en": test_center_related_time_slot.assessed_language_text_en,
                    "assessed_language_text_fr": test_center_related_time_slot.assessed_language_text_fr,
                    "day_of_week_id": test_center_related_time_slot.day_of_week_id,
                    "available": available,
                }
            )

    # ordering by day_of_week_id, start time and assessed_language_id
    ordered_final_array = sorted(
        final_array,
        key=lambda k: (
            k["day_of_week_id"],
            k["start_time"],
            k["assessed_language_id"],
        ),
        reverse=False,
    )

    return Response(ordered_final_array)
