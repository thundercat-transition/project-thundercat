from operator import itemgetter
from rest_framework import status
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from cms.views.utils import get_needed_parameters, get_optional_parameters
from backend.custom_models.site_admin_setting import SiteAdminSetting
from backend.custom_models.keyring import Keyring
from backend.serializers.site_admin_settings_serializer import (
    SiteAdminSettingSerializer,
)

from backend.static.site_admin_setting_type import SiteAdminSettingType, Key
from user_management.views.utils import is_undefined
from backend.celery.task_definition.utils import ConsoleMessageColor


# get all site admin settings
class GetSiteAdminSettings(APIView):
    def post(self, request):
        return get_site_admin_settings(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_site_admin_settings(_):
    site_admin_settings = SiteAdminSetting.objects.all()

    # getting serialized active tests data
    serialized_site_admin_settings = SiteAdminSettingSerializer(
        site_admin_settings, many=True
    ).data
    return Response(serialized_site_admin_settings, status=status.HTTP_200_OK)


class GetEnableQualityOfLife(APIView):
    def post(self, request):
        return get_enable_quality_of_life(request)

    def get_permissions(self):
        return [permissions.AllowAny()]


def get_enable_quality_of_life(request):
    site_admin_setting = SiteAdminSetting.objects.filter(
        codename=SiteAdminSettingType.ENABLE_QUALITY_OF_LIFE
    ).last()

    if not site_admin_setting:
        return Response({"error", "no setting found"}, status=status.HTTP_404_NOT_FOUND)

    # getting serialized active tests data
    serialized_site_admin_setting = SiteAdminSettingSerializer(site_admin_setting).data
    return Response(serialized_site_admin_setting, status=status.HTTP_200_OK)


# get one specific site admin setting
class GetSiteAdminSetting(APIView):
    def post(self, request):
        return get_site_admin_setting(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_site_admin_setting(request):
    success, parameters = get_needed_parameters(["codename"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    codename = itemgetter("codename")(parameters)

    site_admin_setting = SiteAdminSetting.objects.filter(codename=codename)

    if not site_admin_setting:
        return Response({"error", "no setting found"}, status=status.HTTP_404_NOT_FOUND)

    # getting serialized active tests data
    serialized_site_admin_setting = SiteAdminSettingSerializer(site_admin_setting).data
    return Response(serialized_site_admin_setting, status=status.HTTP_200_OK)


# set one specific site admin setting
class SetSiteAdminSetting(APIView):
    def post(self, request):
        return set_site_admin_setting(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def set_site_admin_setting(request):
    success, parameters = get_needed_parameters(["codename", "is_active"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    codename, t_is_active = itemgetter("codename", "is_active")(parameters)

    # convert to T/F in python
    is_active = False
    if t_is_active == "true":
        is_active = True

    print(
        "{0} Set Site Admin Setting {1} {2}".format(
            ConsoleMessageColor.BLUE, codename, is_active
        )
    )

    # if this relates to GC Notify, we need to save the key, delete the old key, or throw an error
    if codename == SiteAdminSettingType.ENABLE_GC_NOTIFY:
        # if it is becoming active
        if is_active:
            # get the key
            optional_parameters = get_optional_parameters(["key"], request)
            key = optional_parameters["key"]
            # if this parameter is missing, throw a 400
            if is_undefined(key):
                return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
            else:
                # otherwise, create an entry in Keyring with the gc notify codename
                Keyring.objects.create(codename=Key.GC_NOTIFY, key=key)
        # if it is becoming inactive, delete it. There should only be 1
        else:
            keys = Keyring.objects.filter(codename=Key.GC_NOTIFY)
            for key in keys:
                key.delete()

    # if this relates to MS Teams API OR webex
    elif codename in [
        Key.MS_TEAMS_API_APP_ID,
        Key.MS_TEAMS_API_TENANT_ID,
        Key.MS_TEAMS_API_SECRET,
        SiteAdminSettingType.ENABLE_WEBEX,
    ]:
        # if it is becoming active
        if is_active:
            # get the key
            optional_parameters = get_optional_parameters(["key"], request)
            key = optional_parameters["key"]
            # if this parameter is missing, throw a 400
            if is_undefined(key):
                return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
            else:
                # otherwise, create an entry in Keyring with the webex bot token codename
                Keyring.objects.create(codename=codename, key=key)
        # if it is becoming inactive, delete it. There should only be 1
        else:
            keys = Keyring.objects.filter(codename=codename)
            for key in keys:
                key.delete()
            # if any of these are the values being disabled, set MS Teams API to False. I.E. NOT Webex
            if codename in [
                Key.MS_TEAMS_API_APP_ID,
                Key.MS_TEAMS_API_TENANT_ID,
                Key.MS_TEAMS_API_SECRET,
            ]:
                enable_ms_teams_api = SiteAdminSetting.objects.filter(
                    codename=SiteAdminSettingType.ENABLE_MS_TEAMS_API
                ).last()
                enable_ms_teams_api.is_active = False
                enable_ms_teams_api.save()

    # get the setting
    site_admin_setting = SiteAdminSetting.objects.filter(codename=codename).last()

    # if it DNE, throw an error. It should exist and be inactive at a minimum
    if not site_admin_setting:
        return Response({"error", "no setting found"}, status=status.HTTP_404_NOT_FOUND)

    site_admin_setting.is_active = is_active
    site_admin_setting.save()

    # return success
    return Response({"OK", "OK"}, status=status.HTTP_200_OK)
