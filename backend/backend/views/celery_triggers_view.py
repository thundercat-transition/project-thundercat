from django.http import JsonResponse

from rest_framework import permissions
from rest_framework.views import APIView

from backend.celery.task_definition.assigned_tests import (
    update_non_submitted_active_assigned_tests,
    unpause_assigned_tests,
    expire_old_accommodations,
    invalidate_locked_assigned_tests,
    unassign_checked_in_and_pre_test_supervised_assigned_tests,
)

from backend.celery.task_definition.test_permissions import (
    remove_expired_test_permissions,
)

from backend.celery.task_definition.uit import (
    handle_expired_uit_processes,
    handle_send_uit_invite_emails,
    handle_update_uit_validity_end_date_emails,
    handle_delete_uit_invite_emails,
)

from backend.celery.task_definition.user_permissions import (
    deprovision_after_inactivity,
    deprovision_admin_after_inactivity_thirty_days,
    remove_expired_test_scorer_permissions,
)

from backend.celery.task_definition.reset_completed_profile_flag import (
    reset_completed_profile_flag,
)

from backend.celery.task_definition.gc_notify_tasks import (
    check_gc_notify_delivery_status,
)

# TODO: Change all api calls in here from GET to POST, once SU dash exists.


# trigger update_non_submitted_active_assigned_tests celery task
class UpdateNonSubmittedActiveAssignedTests(APIView):
    def get(self, _):
        update_non_submitted_active_assigned_tests()
        return JsonResponse({"success": "requested celery task triggered"})

    def get_permissions(self):
        return [permissions.IsAdminUser()]


# trigger unpause_assigned_tests celery task
class UnpauseAssignedTests(APIView):
    def get(self, _):
        unpause_assigned_tests()
        return JsonResponse({"success": "requested celery task triggered"})

    def get_permissions(self):
        return [permissions.IsAdminUser()]


class ExpireOldAccommodations(APIView):
    def get(self, _):
        expire_old_accommodations()
        return JsonResponse({"success": "requested celery task triggered"})

    def get_permissions(self):
        return [permissions.IsAdminUser()]


# trigger invalidate_locked_assigned_tests celery task
class InvalidateLockedAssignedTests(APIView):
    def get(self, _):
        invalidate_locked_assigned_tests()
        return JsonResponse({"success": "requested celery task triggered"})

    def get_permissions(self):
        return [permissions.IsAdminUser()]


# trigger unassign_checked_in_and_pre_test_supervised_assigned_tests celery task
class UnassignReadyAndPreTestAssignedTests(APIView):
    def get(self, _):
        unassign_checked_in_and_pre_test_supervised_assigned_tests()
        return JsonResponse({"success": "requested celery task triggered"})

    def get_permissions(self):
        return [permissions.IsAdminUser()]


# trigger remove_expired_test_permissions celery task
class RemoveExpiredTestPermissions(APIView):
    def get(self, _):
        remove_expired_test_permissions()
        return JsonResponse({"success": "requested celery task triggered"})

    def get_permissions(self):
        return [permissions.IsAdminUser()]


# trigger handle_expired_uit_processes celery task
class HandleExpiredUitProcesses(APIView):
    def get(self, _):
        handle_expired_uit_processes()
        return JsonResponse({"success": "requested celery task triggered"})

    def get_permissions(self):
        return [permissions.IsAdminUser()]


# trigger handle_send_uit_invite_emails celery task
class HandleSendUitInviteEmails(APIView):
    def get(self, _):
        handle_send_uit_invite_emails()
        return JsonResponse({"success": "requested celery task triggered"})

    def get_permissions(self):
        return [permissions.IsAdminUser()]


# trigger handle_update_uit_validity_end_date_emails celery task
class HandleUpdateUitValidityEndDateEmails(APIView):
    def get(self, _):
        handle_update_uit_validity_end_date_emails()
        return JsonResponse({"success": "requested celery task triggered"})

    def get_permissions(self):
        return [permissions.IsAdminUser()]


# trigger handle_delete_uit_invite_emails celery task
class HandleDeleteUitInviteEmails(APIView):
    def get(self, _):
        handle_delete_uit_invite_emails()
        return JsonResponse({"success": "requested celery task triggered"})

    def get_permissions(self):
        return [permissions.IsAdminUser()]


# trigger deprovision_after_inactivity celery task
class DeprovisionAfterInactivity(APIView):
    def get(self, _):
        deprovision_after_inactivity()
        return JsonResponse({"success": "requested celery task triggered"})

    def get_permissions(self):
        return [permissions.IsAdminUser()]


# trigger deprovision_admin_after_inactivity_thirty_days celery task
class DeprovisionAdminAfterInactivityThirtyDays(APIView):
    def get(self, _):
        deprovision_admin_after_inactivity_thirty_days()
        return JsonResponse({"success": "requested celery task triggered"})

    def get_permissions(self):
        return [permissions.IsAdminUser()]


# trigger deprovision_admin_after_inactivity_thirty_days celery task
class RemoveExpiredTestScorerPermissions(APIView):
    def get(self, _):
        remove_expired_test_scorer_permissions()
        return JsonResponse({"success": "requested celery task triggered"})

    def get_permissions(self):
        return [permissions.IsAdminUser()]


# trigger reset_completed_profile_flag celery task
class ResetCompletedProfileFlag(APIView):
    def get(self, _):
        reset_completed_profile_flag()
        return JsonResponse({"success": "requested celery task triggered"})

    def get_permissions(self):
        return [permissions.IsAdminUser()]


# trigger check_gc_notify_delivery_status celery task
class CheckGCNotifyDeliveryStatus(APIView):
    def get(self, _):
        check_gc_notify_delivery_status()
        return JsonResponse({"success": "requested celery task triggered"})

    def get_permissions(self):
        return [permissions.IsAdminUser()]
