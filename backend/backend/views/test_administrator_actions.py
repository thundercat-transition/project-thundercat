from django.utils import timezone
from rest_framework.response import Response
from rest_framework import status
from rest_framework import serializers
from backend.views.utils import get_new_break_bank_remaining_time
from backend.custom_models.break_bank import BreakBank
from backend.custom_models.break_bank_actions import BreakBankActions
from backend.custom_models.accommodation_request import AccommodationRequest
from backend.custom_models.additional_time import AdditionalTime
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.custom_models.ta_actions import TaActions
from backend.custom_models.lock_test_actions import LockTestActions
from backend.static.assigned_test_status import (
    AssignedTestStatus,
    get_assigned_test_status_id,
)
from backend.views.utils import get_user_info_from_jwt_token
from cms.views.utils import get_needed_parameters, get_optional_parameters
from cms.cms_models.test_definition import TestDefinition
from cms.views.test_break_bank import BreakBankActionsConstants
from cms.cms_models.test_section import TestSection
from user_management.user_management_models.user_models import User
from db_views.db_view_models.ta_assigned_candidates_vw import TaAssignedCandidatesVW


# TA Actions Definition
class TaActionsConstants:
    UPDATE_TIME = "UPDATE_TIME"
    UPDATE_BREAK_BANK = "UPDATE_BREAK_BANK"
    APPROVE = "APPROVE"
    APPROVE_ALL = "APPROVE_ALL"
    LOCK = "LOCK"
    LOCK_ALL = "LOCK_ALL"
    UNLOCK = "UNLOCK"
    UNLOCK_ALL = "UNLOCK_ALL"
    PAUSE = "PAUSE"
    UNPAUSE = "UNPAUSE"
    UNASSIGN = "UN_ASSIGN"
    REPORT = "REPORT"


# Assigned Test Statuses Definition based on Selected Action (statuses to include in assigned test filter)
class AssignedTestStatuses:
    assigned_status_id = get_assigned_test_status_id(AssignedTestStatus.ASSIGNED)
    checked_in_status_id = get_assigned_test_status_id(AssignedTestStatus.CHECKED_IN)
    pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
    active_status_id = get_assigned_test_status_id(AssignedTestStatus.ACTIVE)
    transition_status_id = get_assigned_test_status_id(AssignedTestStatus.TRANSITION)
    locked_status_id = get_assigned_test_status_id(AssignedTestStatus.LOCKED)
    paused_status_id = get_assigned_test_status_id(AssignedTestStatus.PAUSED)

    UPDATE_TIME = [checked_in_status_id, pre_test_status_id]
    UPDATE_BREAK_BANK = [checked_in_status_id, pre_test_status_id]
    APPROVE = [
        checked_in_status_id,
        pre_test_status_id,
        active_status_id,
        locked_status_id,
        transition_status_id,
    ]
    LOCK = [
        checked_in_status_id,
        pre_test_status_id,
        active_status_id,
        transition_status_id,
        paused_status_id,
    ]
    LOCK_ALL = [
        checked_in_status_id,
        pre_test_status_id,
        active_status_id,
        transition_status_id,
        paused_status_id,
    ]
    UNLOCK = [locked_status_id]
    UNLOCK_ALL = [locked_status_id]
    UN_ASSIGN = [
        assigned_status_id,
        checked_in_status_id,
        pre_test_status_id,
    ]
    REPORT = [
        assigned_status_id,
        checked_in_status_id,
        pre_test_status_id,
        active_status_id,
        transition_status_id,
        locked_status_id,
        paused_status_id,
    ]


def handle_accommodation_request(assigned_test):
    # there is an accommodation request
    if assigned_test.accommodation_request_id is not None:
        # getting break bank ID
        break_bank_id = AccommodationRequest.objects.get(
            id=assigned_test.accommodation_request_id
        ).break_bank_id
        # existing break bank accommodation request
        if break_bank_id is not None:
            # getting break bank actions related to current test section (if they exist)
            break_bank_actions = BreakBankActions.objects.filter(
                break_bank_id=break_bank_id,
            )
            # existing break bank actions
            if break_bank_actions:
                # if last break bank action is a PAUSE
                if (
                    break_bank_actions.last().action_type
                    == BreakBankActionsConstants.PAUSE
                ):
                    # getting new break bank remaining time
                    new_remaining_time = get_new_break_bank_remaining_time(
                        assigned_test.accommodation_request_id
                    )
                    # creating new UNPAUSE action in break bank actions table
                    BreakBankActions.objects.create(
                        action_type=BreakBankActionsConstants.UNPAUSE,
                        new_remaining_time=new_remaining_time,
                        break_bank_id=break_bank_id,
                        test_section_id=break_bank_actions.last().test_section_id,
                    )


# generic function for test administrator actions (update test time, approve candidate, lock candidate and report candidate)
# needed_parameters must be in order in your request
def test_administrator_actions(request, needed_parameters, optional_parameters, action):
    # handling missing parameters
    success, params = get_needed_parameters(needed_parameters, request)
    if not success:
        return Response(params, status=status.HTTP_400_BAD_REQUEST)

    optionals = get_optional_parameters(optional_parameters, request)
    parameters = {**params, **optionals}

    unassigned_status_id = get_assigned_test_status_id(AssignedTestStatus.UNASSIGNED)
    checked_in_status_id = get_assigned_test_status_id(AssignedTestStatus.CHECKED_IN)
    pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
    active_status_id = get_assigned_test_status_id(AssignedTestStatus.ACTIVE)
    transition_status_id = get_assigned_test_status_id(AssignedTestStatus.TRANSITION)
    locked_status_id = get_assigned_test_status_id(AssignedTestStatus.LOCKED)

    try:
        if (
            action is not TaActionsConstants.UPDATE_TIME
            and action is not TaActionsConstants.UPDATE_BREAK_BANK
            and action is not TaActionsConstants.APPROVE_ALL
        ):
            # getting current assigned_test
            # using filter and first() to avoid error in case there is a user checked in to the same test twice
            assigned_test = AssignedTest.objects.get(
                # decoding username that might contain special characters
                user_id=parameters["candidate_user_id"],
                test_id=TestDefinition.objects.get(id=parameters["test_id"]),
                test_session_id=parameters["test_session_id"],
                status_id__in=(getattr(AssignedTestStatuses, action)),
            )
        # updating test status depending on the provided action
        # update time action
        if action == TaActionsConstants.UPDATE_TIME:
            # getting assigned test section data
            assigned_test_section_data = AssignedTestSection.objects.get(
                id=parameters["assigned_test_section_id"]
            )

            # getting test section data
            test_section_data = TestSection.objects.get(
                id=assigned_test_section_data.test_section_id
            )

            # getting respective assigned test data
            respective_assigned_test_data = AssignedTest.objects.get(
                id=assigned_test_section_data.assigned_test_id
            )

            # accommodation_request_id already exists
            if respective_assigned_test_data.accommodation_request_id is not None:
                # getting existing accommodation request additional time data (based on provided accommodation_request_id)
                additional_time_data = AdditionalTime.objects.filter(
                    accommodation_request_id=respective_assigned_test_data.accommodation_request_id,
                )
                # additional_time_data exists
                if additional_time_data and parameters["first_iteration"] == "true":
                    # looping in additional_time_data
                    for data in additional_time_data:
                        # deleting data
                        data.delete()
                # provided time is greater than the default test section time
                if int(parameters["test_time"]) > test_section_data.default_time:
                    # creating new entry in additional time table
                    AdditionalTime.objects.create(
                        test_section_time=parameters["test_time"],
                        accommodation_request_id=respective_assigned_test_data.accommodation_request_id,
                        test_section_id=assigned_test_section_data.test_section_id,
                    )
                else:
                    # checking if new data has been added in Additional Time table (in the last iteration)
                    if parameters["last_iteration"] == "true":
                        # no existing additional time data on the last iteration
                        if not AdditionalTime.objects.filter(
                            accommodation_request_id=respective_assigned_test_data.accommodation_request_id
                        ):
                            # break bank ID of respective accommodation request is set to None
                            respective_accommodation_request_data = AccommodationRequest.objects.get(
                                id=respective_assigned_test_data.accommodation_request_id
                            )
                            if (
                                respective_accommodation_request_data.break_bank_id
                                is None
                            ):
                                # update accommodation_request_id to None on the Assigned Test table
                                respective_assigned_test_data.accommodation_request_id = (
                                    None
                                )
                                respective_assigned_test_data.save()
                                # deleting accommodation request entry
                                respective_accommodation_request_data.delete()

            # accommodation_request_id does not exist yet
            else:
                # provided time is greater than the default test section time
                if int(parameters["test_time"]) > test_section_data.default_time:
                    # creating new entry in accommodation request table
                    new_accommodation_request = AccommodationRequest.objects.create(
                        break_bank_id=None
                    )
                    new_accommodation_request.save()

                    # associating new accommodation request to respective assigned test
                    respective_assigned_test_data.accommodation_request_id = (
                        new_accommodation_request.id
                    )
                    respective_assigned_test_data.save()

                    # creating new entry in additional time table
                    AdditionalTime.objects.create(
                        test_section_time=parameters["test_time"],
                        accommodation_request_id=new_accommodation_request.id,
                        test_section_id=assigned_test_section_data.test_section_id,
                    )

            # updating assigned test section test section time
            assigned_test_section_data.test_section_time = parameters["test_time"]
            assigned_test_section_data.save()

            # creating new entry in ta actions table
            ta_action = TaActions.objects.create(
                action_type_id=TaActionsConstants.UPDATE_TIME,
                assigned_test_id=assigned_test_section_data.assigned_test_id,
            )
            ta_action.save()
        # update break bank action
        elif action == TaActionsConstants.UPDATE_BREAK_BANK:
            # getting respective assigned test data
            respective_assigned_test_data = AssignedTest.objects.get(
                id=parameters["assigned_test_id"]
            )

            # accommodation_request_id already exists
            if respective_assigned_test_data.accommodation_request_id is not None:
                # getting existing accommodation request data (based on provided accommodation_request_id)
                accommodation_request_data = AccommodationRequest.objects.get(
                    id=respective_assigned_test_data.accommodation_request_id
                )
                # break bank ID exists
                if accommodation_request_data.break_bank_id is not None:
                    # getting existing break bank
                    existing_break_bank = BreakBank.objects.get(
                        id=accommodation_request_data.break_bank_id
                    )
                    # updating existing break bank (deleting/updating needed entries if provided break_bank_time is 0)
                    if int(parameters["break_bank_time"]) <= 0:
                        # updating break_bank_id of respective accommodation request data to None
                        accommodation_request_data.break_bank_id = None
                        accommodation_request_data.save()
                        # deleting break bank entry
                        existing_break_bank.delete()
                        # checking if there is some additional time related to that respective request
                        additional_time_data = AdditionalTime.objects.filter(
                            accommodation_request_id=accommodation_request_data.id
                        )
                        # no other accommodation settings related
                        if not additional_time_data:
                            # updating respective assigned test (accommodation_request_id ==> None)
                            respective_assigned_test_data.accommodation_request_id = (
                                None
                            )
                            respective_assigned_test_data.save()
                            # deleting related accommodation request
                            accommodation_request_data.delete()
                    else:
                        existing_break_bank.break_time = parameters["break_bank_time"]
                        existing_break_bank.save()
                # break bank ID does not exist
                else:
                    # creating new break bank entry (only if provided break_bank_time is greater than 0)
                    if int(parameters["break_bank_time"]) > 0:
                        # creating new break bank entry in Break Bank table
                        new_break_bank = BreakBank.objects.create(
                            break_time=parameters["break_bank_time"]
                        )
                        new_break_bank.save()
                        # associating new break bank to respective accommodation request
                        accommodation_request_data.break_bank_id = new_break_bank.id
                        accommodation_request_data.save()
            # accommodation_request_id does not exist yet
            else:
                # creating new break bank entry (only if provided break_bank_time is greater than 0)
                if int(parameters["break_bank_time"]) > 0:
                    # creating new break bank entry in Break Bank table
                    new_break_bank = BreakBank.objects.create(
                        break_time=parameters["break_bank_time"]
                    )
                    # creating new row in accommodation request table
                    new_accommodation_request = AccommodationRequest.objects.create(
                        break_bank_id=new_break_bank.id
                    )
                    new_accommodation_request.save()

                    # associating new accommodation request to respective assigned test
                    respective_assigned_test_data.accommodation_request_id = (
                        new_accommodation_request.id
                    )
                    respective_assigned_test_data.save()

        # approve action
        elif action == TaActionsConstants.APPROVE:
            # getting test section ID that needed an approval
            most_updated_assigned_test_data = TaAssignedCandidatesVW.objects.get(
                id=assigned_test.id
            )

            # creating new entry in ta actions table
            ta_action = TaActions.objects.create(
                action_type_id=TaActionsConstants.APPROVE,
                assigned_test_id=assigned_test.id,
                test_section_id=most_updated_assigned_test_data.next_test_section_id_that_needs_approval,
            )
            ta_action.save()
        # approve all action
        elif action == TaActionsConstants.APPROVE_ALL:
            # looping in provided assigned test ids
            for assigned_test_id in parameters["assigned_test_ids"]:
                # getting test section ID that needed an approval
                most_updated_assigned_test_data = TaAssignedCandidatesVW.objects.get(
                    id=assigned_test_id
                )

                # creating new entry in ta actions table
                ta_action = TaActions.objects.create(
                    action_type_id=TaActionsConstants.APPROVE,
                    assigned_test_id=assigned_test_id,
                    test_section_id=most_updated_assigned_test_data.next_test_section_id_that_needs_approval,
                )
                ta_action.save()
        # lock action
        elif action == TaActionsConstants.LOCK:
            # if test status is READY
            if assigned_test.status_id == checked_in_status_id:
                # update previous_status
                assigned_test.previous_status_id = checked_in_status_id
            # if test status is PRE_TEST
            elif assigned_test.status_id == pre_test_status_id:
                # update previous_status
                assigned_test.previous_status_id = pre_test_status_id
            # if test status is TRANSITION
            elif assigned_test.status_id == transition_status_id:
                # update previous_status
                assigned_test.previous_status_id = transition_status_id
            assigned_test.status_id = locked_status_id
            assigned_test.save()

            # creating new entry in ta actions table
            test_section_id = None
            if parameters["test_section_id"] != "null":
                test_section_id = parameters["test_section_id"]
            ta_action = TaActions.objects.create(
                action_type_id=TaActionsConstants.LOCK,
                assigned_test_id=assigned_test.id,
                test_section_id=test_section_id,
            )
            ta_action.save()

            # creating new entry in lock test actions table
            lock_test_action = LockTestActions.objects.create(
                lock_start_date=timezone.now(), ta_action_id=ta_action.id
            )
            lock_test_action.save()

            # handling accommodation request (pause)
            handle_accommodation_request(assigned_test)

        # unlock action
        elif action == TaActionsConstants.UNLOCK:
            # if previous_status is CHECKED_IN
            if assigned_test.previous_status_id == checked_in_status_id:
                # update status and previous_status
                assigned_test.status_id = checked_in_status_id
                assigned_test.previous_status_id = None
                assigned_test.save()
            # if previous_status is PRE_TEST
            elif assigned_test.previous_status_id == pre_test_status_id:
                # update status and previous_status
                assigned_test.status_id = pre_test_status_id
                assigned_test.previous_status_id = None
                assigned_test.save()
            # if previous_status is TRANSITION
            elif assigned_test.previous_status_id == transition_status_id:
                # update status and previous_status
                assigned_test.status_id = transition_status_id
                assigned_test.previous_status_id = None
                assigned_test.save()
            # no CHECKED_IN/PRE_TEST/TRANSITION previous_status
            else:
                assigned_test.status_id = active_status_id
                assigned_test.save()

            # creating new entry in ta actions table
            test_section_id = None
            if parameters["test_section_id"] != "null":
                test_section_id = parameters["test_section_id"]
            ta_action = TaActions.objects.create(
                action_type_id=TaActionsConstants.UNLOCK,
                assigned_test_id=assigned_test.id,
                test_section_id=test_section_id,
            )
            ta_action.save()

            # update lock_end_date field in lock test actions table
            last_lock_ta_action = TaActions.objects.filter(
                action_type_id=TaActionsConstants.LOCK,
                assigned_test_id=assigned_test.id,
            ).last()
            lock_test_action = LockTestActions.objects.get(
                ta_action_id=last_lock_ta_action.id
            )
            lock_test_action.lock_end_date = timezone.now()
            lock_test_action.save()
        # unassign action
        elif action == TaActionsConstants.UNASSIGN:
            assigned_test.status_id = unassigned_status_id
            assigned_test.save()

            # creating new entry in ta actions table
            ta_action = TaActions.objects.create(
                action_type_id=TaActionsConstants.UNASSIGN,
                assigned_test_id=assigned_test.id,
            )
            ta_action.save()

        # elif action == TaActionsConstants.REPORT:
        # no action found
        else:
            return Response(
                {"error": "'{}' action does not exist".format(action)},
                status=status.HTTP_400_BAD_REQUEST,
            )
        if (
            action is not TaActionsConstants.UPDATE_TIME
            and action is not TaActionsConstants.UPDATE_BREAK_BANK
            and action is not TaActionsConstants.APPROVE_ALL
        ):
            return Response(
                {
                    "status": status.HTTP_200_OK,
                    "test_access_code": assigned_test.test_access_code,
                }
            )
        else:
            return Response(
                {
                    "status": status.HTTP_200_OK,
                },
                status.HTTP_200_OK,
            )

    except User.DoesNotExist:
        return Response(
            {
                "error": "the specified user (candidate and/or test administrator) does not exist"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )

    except TestDefinition.DoesNotExist:
        return Response(
            {"error": "the specified test does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except AssignedTest.DoesNotExist:
        return Response(
            {"error": "no assigned test found based on provided parameters"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except TypeError:
        return Response(
            {"error": "no assigned test found based on provided parameters"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except AttributeError:
        return Response(
            {"error": "'{}' action does not exist".format(action)},
            status=status.HTTP_400_BAD_REQUEST,
        )


# locking/unlocking all candidates test (only if test status is ready or active, not assigned)
def lock_unlock_all_candidates_test(
    request, needed_parameters, optional_parameters, action
):
    # handling missing parameters
    success, params = get_needed_parameters(needed_parameters, request)
    if not success:
        return Response(params, status=status.HTTP_400_BAD_REQUEST)

    optionals = get_optional_parameters(optional_parameters, request)
    parameters = {**params, **optionals}

    checked_in_status_id = get_assigned_test_status_id(AssignedTestStatus.CHECKED_IN)
    pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
    active_status_id = get_assigned_test_status_id(AssignedTestStatus.ACTIVE)
    locked_status_id = get_assigned_test_status_id(AssignedTestStatus.LOCKED)

    try:
        # get all active supervised assigned candidates (assigned, ready, active or locked)
        active_candidates = AssignedTest.objects.filter(
            test_session_id=parameters["test_session_id"],
            status_id__in=(getattr(AssignedTestStatuses, action)),
            uit_invite_id__isnull=True,
        )

        # initializing candidates_to_update array
        candidates_to_update = []

        # for each candidate
        for candidate in active_candidates:
            # get assigned test
            assigned_test = AssignedTest.objects.get(
                user_id=candidate.user_id,
                test_id=TestDefinition.objects.get(id=candidate.test.id),
                test_session_id=parameters["test_session_id"],
                status_id__in=(getattr(AssignedTestStatuses, action)),
            )

            # save assigned_test to candidates_to_update array
            candidates_to_update.append(assigned_test)

        # for each assigned test in candidates_to_update array
        for assigned_test in candidates_to_update:
            # LOCK_ALL action
            if action == TaActionsConstants.LOCK_ALL:
                # if test status is READY
                if assigned_test.status_id == checked_in_status_id:
                    # update previous_status
                    assigned_test.previous_status_id = checked_in_status_id
                # if test status is PRE_TEST
                elif assigned_test.status_id == pre_test_status_id:
                    # update previous_status
                    assigned_test.previous_status_id = pre_test_status_id
                # updating test status to LOCKED
                assigned_test.status_id = locked_status_id

                # creating new entry in ta actions table
                # test section id is defined
                if assigned_test.test_section_id != "null":
                    ta_action = TaActions.objects.create(
                        action_type_id=TaActionsConstants.LOCK,
                        assigned_test_id=assigned_test.id,
                        test_section_id=assigned_test.test_section_id,
                    )
                # test section id is not defined, meaning that the test has been locked before the candidate even started the test
                else:
                    ta_action = TaActions.objects.create(
                        action_type_id=TaActionsConstants.LOCK,
                        assigned_test_id=assigned_test.id,
                    )
                ta_action.save()

                # creating new entry in lock test actions table
                lock_test_action = LockTestActions.objects.create(
                    lock_start_date=timezone.now(), ta_action_id=ta_action.id
                )
                lock_test_action.save()

                # handling accommodation request (pause)
                handle_accommodation_request(assigned_test)

            # UNLOCK_ALL action
            elif action == TaActionsConstants.UNLOCK_ALL:
                # previous_status is READY
                if assigned_test.previous_status_id == checked_in_status_id:
                    # update status and previous_status
                    assigned_test.status_id = checked_in_status_id
                    assigned_test.previous_status_id = None
                # previous_status is PRE_TEST
                elif assigned_test.previous_status_id == pre_test_status_id:
                    # update status and previous_status
                    assigned_test.status_id = pre_test_status_id
                    assigned_test.previous_status_id = None
                # no READY previous status
                else:
                    # update status
                    assigned_test.status_id = active_status_id

                # creating new entry in ta actions table
                # test section id is defined
                if assigned_test.test_section_id != "null":
                    ta_action = TaActions.objects.create(
                        action_type_id=TaActionsConstants.UNLOCK,
                        assigned_test_id=assigned_test.id,
                        test_section_id=assigned_test.test_section_id,
                    )
                # test section id is not defined, meaning that the test has been locked before the candidate even started the test
                else:
                    ta_action = TaActions.objects.create(
                        action_type_id=TaActionsConstants.UNLOCK,
                        assigned_test_id=assigned_test.id,
                    )
                ta_action.save()

                # update lock_end_date field in lock test actions table
                last_lock_ta_action = TaActions.objects.filter(
                    action_type_id=TaActionsConstants.LOCK,
                    assigned_test_id=assigned_test.id,
                ).last()
                lock_test_action = LockTestActions.objects.get(
                    ta_action_id=last_lock_ta_action.id
                )
                lock_test_action.lock_end_date = timezone.now()
                lock_test_action.save()
            # no action found
            else:
                return Response(
                    {"error": "'{}' action does not exist".format(action)},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            # saving new assigned_test
            assigned_test.save()
        serializer = AssignedTestsForLockUnlockAll(candidates_to_update, many=True)
        return Response({"status": status.HTTP_200_OK, "candidates": serializer.data})

    except User.DoesNotExist:
        return Response(
            {"error": "the specified test administrator does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except TestDefinition.DoesNotExist:
        return Response(
            {"error": "the specified test does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except AssignedTest.DoesNotExist:
        return Response(
            {"error": "no assigned test found based on provided parameters"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except TypeError:
        return Response(
            {
                "error": "no assigned test found based on provided parameters",
                "candidate": "{}".format(candidate.username),
            },
            status=status.HTTP_400_BAD_REQUEST,
        )

    except AttributeError:
        return Response(
            {"error": "'{}' action does not exist".format(action)},
            status=status.HTTP_400_BAD_REQUEST,
        )


class AssignedTestsForLockUnlockAll(serializers.ModelSerializer):
    class Meta:
        model = AssignedTest
        fields = "__all__"
