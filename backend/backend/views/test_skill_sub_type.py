import json
from rest_framework.response import Response
from rest_framework import status
from cms.cms_models.test_skill_occupational_desc import TestSkillOccupationalDesc
from cms.views.utils import get_needed_parameters
from cms.cms_models.test_skill_sle_desc import TestSkillSLEDesc
from backend.serializers.test_skill_sub_type_serializer import (
    TestSkillSubTypeSLESerializer,
    TestSkillSubTypeOccupationalSerializer,
)
from cms.cms_models.test_skill_type import TestSkillType
from backend.static.test_skill_codenames import (
    TestSkillSLEDescCodename,
    TestSkillTypeCodename,
)


def get_test_skill_sub_types_list(request):
    needed_parameters = [
        "test_skill_type_id",
    ]

    # handling missing parameters
    success, params = get_needed_parameters(needed_parameters, request)
    if not success:
        return Response(params, status=status.HTTP_400_BAD_REQUEST)

    try:
        # getting all test skill types
        test_skill_type = TestSkillType.objects.get(id=params["test_skill_type_id"])
    except TestSkillType.DoesNotExist:
        return Response([], status=status.HTTP_404_NOT_FOUND)

    if test_skill_type.codename == TestSkillTypeCodename.SLE:
        # get all sle sub-skills
        test_skill_type_sle = TestSkillSLEDesc.objects.all()
        #   getting serialized data
        serialized_data = TestSkillSubTypeSLESerializer(
            test_skill_type_sle, many=True
        ).data

        return Response(serialized_data)

    elif test_skill_type.codename == TestSkillTypeCodename.OCC:
        test_skill_type_occupational = TestSkillOccupationalDesc.objects.all()
        #   getting serialized data
        serialized_data = TestSkillSubTypeOccupationalSerializer(
            test_skill_type_occupational, many=True
        ).data

        return Response(serialized_data)

    # if we got TestSkillTypeCodename.NONE or non-existing TestSkillType id, we return []
    return Response([])
