from rest_framework.response import Response
from backend.static.test_skill_codenames import TestSkillTypeCodename
from backend.serializers.test_skill_serializer import TestSkillTypeSerializer
from cms.cms_models.test_skill_type import TestSkillType


def get_test_skill_types_list(request):
    # getting all test skill types
    # TEMPORARILY EXCLUDING None and Occupational
    test_skill_types = TestSkillType.objects.all()

    # getting serialized data
    serialized_data = TestSkillTypeSerializer(test_skill_types, many=True).data

    return Response(serialized_data)
