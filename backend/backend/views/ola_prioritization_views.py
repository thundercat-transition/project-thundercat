from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from backend.static.ola_prioritization_const import OlaPrioritizationConst
from backend.serializers.ola_prioritization_serializer import (
    OlaPrioritizationSerializer,
)
from backend.custom_models.ola_prioritization import OlaPrioritization
from cms.views.utils import get_needed_parameters
from operator import itemgetter
from backend.api_permissions.role_based_api_permissions import (
    HasSystemAdminPermission,
)


class GetOlaPrioritization(APIView):
    permission_classes = (permissions.IsAuthenticated & (HasSystemAdminPermission),)

    def get(self, request):
        return get_ola_prioritization(request)


def get_ola_prioritization(request):
    # getting all existing OLA prioritization values
    ola_prioritization = OlaPrioritization.objects.all()

    # serializing the data
    serialized_data = OlaPrioritizationSerializer(ola_prioritization, many=True).data

    return Response(serialized_data)


class SetOlaPrioritization(APIView):
    permission_classes = (permissions.IsAuthenticated & (HasSystemAdminPermission),)

    def post(self, request):
        return set_ola_prioritization(request)


def set_ola_prioritization(request):
    # get the codenames
    prioritization_high = OlaPrioritizationConst.HIGH
    prioritization_medium = OlaPrioritizationConst.MEDIUM
    prioritization_low = OlaPrioritizationConst.LOW
    success, parameters = get_needed_parameters(
        [prioritization_high, prioritization_medium, prioritization_low],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    prioritization_high, prioritization_medium, prioritization_low = itemgetter(
        prioritization_high, prioritization_medium, prioritization_low
    )(parameters)

    # getting OLA prioritization data
    ola_prioritization = OlaPrioritization.objects.all()

    # looping in ola_prioritization
    for prioritization_data in ola_prioritization:
        # codename of current iteration matches the HIGH codename
        if prioritization_data.codename == OlaPrioritizationConst.HIGH:
            prioritization_data.value = prioritization_high
            prioritization_data.save()
        # codename of current iteration matches the MEDIUM codename
        elif prioritization_data.codename == OlaPrioritizationConst.MEDIUM:
            prioritization_data.value = prioritization_medium
            prioritization_data.save()
        # codename of current iteration matches the LOW codename
        elif prioritization_data.codename == OlaPrioritizationConst.LOW:
            prioritization_data.value = prioritization_low
            prioritization_data.save()

    return Response(status=status.HTTP_200_OK)
