import json
import random
import string
from datetime import date, datetime, timedelta
from operator import itemgetter
from django.utils import timezone
from rest_framework import status
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from cms.views.utils import get_needed_parameters
from user_management.user_management_models.custom_user_permissions_model import (
    CustomUserPermissions,
)
from user_management.user_management_models.custom_permission_2fa import (
    CustomPermission2FA,
)
from user_management.user_management_models.user_2fa_tracking_model import (
    User2FATracking,
)
from user_management.user_management_models.user_models import User
from db_views.db_view_models.custom_permission_2fa_vw import CustomPermission2FAVW
from db_views.serializers.custom_permission_2fa_serializers import (
    CustomPermission2FAViewSerializer,
)
from backend.views.utils import get_user_info_from_jwt_token
from backend.views.gc_notify_view import (
    is_gc_notify_active,
    send_2fa_email,
)
from backend.celery.tasks import send_2fa_code_email_celery_task


# Check if the 2fa is needed; if so then send
class Send2FACodeToUser(APIView):
    def post(self, request):
        return send_2fa_code_to_user(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class ReSend2FACodeToUser(APIView):
    def post(self, request):
        return resend_2fa_code_to_user(request)

    def get_permissions(self):
        return [permissions.AllowAny()]  # this is before they are logged in


# sent form the login page
def send_2fa_code_to_user(request):
    # get the user_id from the jwt token
    user_info = get_user_info_from_jwt_token(request)
    user_id = user_info["user_id"]
    return send_2fa_code_to_user_from_user_id(user_id)


# sent from the 2FA page
def resend_2fa_code_to_user(request):
    # check that the parameters are in the request
    success, parameters = get_needed_parameters(
        ["user_id"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    (user_id) = itemgetter("user_id")(parameters)
    return send_2fa_code_to_user_from_user_id(user_id)


def send_2fa_code_to_user_from_user_id(user_id):

    # check if the user needs 2fa; otherwise return a 100
    needs_2fa = does_user_need_2fa(user_id)
    if not needs_2fa:
        return Response(
            {"skip", "2fa not needed for this account"}, status=status.HTTP_202_ACCEPTED
        )

    # find and delete any previous 2fa codes
    previous_codes = User2FATracking.objects.filter(user_id=user_id)
    for code in previous_codes:
        code.delete()

    # create the code and insert the row into User2FATracking
    allowed_chars = string.digits

    code = "".join(random.choice(allowed_chars) for i in range(6))
    expiry = timezone.now() + timedelta(minutes=15)

    User2FATracking.objects.create(
        two_factor_auth_code=code, user_id=user_id, expiry_date=expiry
    )

    # get the user info to send the email
    user = User.objects.get(id=user_id)

    if is_gc_notify_active():
        personalization = {
            "first_name": user.first_name,
            "last_name": user.last_name,
            "auth_code": code,
        }

        send_2fa_email(user.email, personalization)
    else:
        email_data = {
            "first_name": user.first_name,
            "last_name": user.last_name,
            "email": user.email,
            "auth_code": code,
        }

        send_2fa_code_email_celery_task.delay(email_data)

    # return that a code was successfully sent, but user must enter the code to get past the 2fa; also return the user_id
    return Response({"user_id": user_id}, status=status.HTTP_200_OK)


# check if the 2FA is valid
class Check2FACodeForUser(APIView):
    def post(self, request):
        return check_2fa_code_for_user(request)

    def get_permissions(self):
        return [permissions.AllowAny()]  # this is before they are logged in


def check_2fa_code_for_user(request):
    # check that the parameters are in the request
    success, parameters = get_needed_parameters(
        ["user_id", "code"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    (user_id, code) = itemgetter("user_id", "code")(parameters)

    # these are used in several places, pre-define here
    success_200_response = Response({"OK", "OK"}, status=status.HTTP_200_OK)

    # check if the user needs 2fa; otherwise return a 100
    needs_2fa = does_user_need_2fa(user_id)
    if not needs_2fa:
        return success_200_response
    # check that it exists
    user_2fa = User2FATracking.objects.filter(
        two_factor_auth_code=code, user_id=user_id
    ).last()

    # if the code exists for the user
    if user_2fa:
        # get the expiry date and delete the code; it's either expired OR being consumed if we get this far
        expiry = user_2fa.expiry_date
        user_2fa.delete()

        # if its expired, return as if it DNE
        if expiry < timezone.now():
            return Response({"error", "code expired"}, status.HTTP_408_REQUEST_TIMEOUT)
        # if it is not expired (it is still deleted), but return success
        return success_200_response

    # otherwise say the 2fa code was not foud (for this user)
    return Response({"error", "2fa code not found"}, status.HTTP_404_NOT_FOUND)


# check if the user needs 2fa based on permissions
def does_user_need_2fa(user_id):
    # get all permissions for the given user, but just a list of the ids
    user_permissions = CustomUserPermissions.objects.filter(
        user_id=user_id,
    ).values_list("permission_id", flat=True)

    if not user_permissions:
        return False

    # get all of these permissions that need 2fa
    needs_2fa = CustomPermission2FA.objects.filter(
        custom_permission_id__in=user_permissions, is_2fa_active=True
    )

    # if it returns at all,
    if needs_2fa:
        return True

    # otherwise it is unnecessry
    return False


# Get All 2FA Settings
class Get2FASettings(APIView):
    def post(self, request):
        return get_2fa_settings(request)

    def get_permissions(self):
        return [permissions.IsAdminUser()]  # only for the superuser


def get_2fa_settings(_):
    all_custom_permission_2fa = CustomPermission2FAVW.objects.all()

    serialized_data = CustomPermission2FAViewSerializer(
        all_custom_permission_2fa, many=True
    ).data

    # return the current 2fa status
    return Response(serialized_data, status=status.HTTP_200_OK)


# set 2fa for this permission group
class Set2FA(APIView):
    def post(self, request):
        return set_2fa(request)

    def get_permissions(self):
        return [permissions.IsAdminUser()]  # only for the superuser


def set_2fa(request):
    # check that the parameters are in the request
    success, parameters = get_needed_parameters(
        ["permission_id", "is_2fa_active"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    (permission_id, is_2fa_active) = itemgetter("permission_id", "is_2fa_active")(
        parameters
    )

    is_active = False
    if is_2fa_active == "true":
        is_active = True

    permission_2fa = CustomPermission2FA.objects.filter(
        custom_permission_id=permission_id
    ).last()
    permission_2fa.is_2fa_active = is_active
    permission_2fa.save()

    # return that the value was successfully updated
    return Response({"OK", "OK"}, status=status.HTTP_200_OK)
