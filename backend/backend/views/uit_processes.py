import json
from operator import or_, itemgetter
from functools import reduce
from urllib import parse
import datetime
from django.forms import ValidationError
import pytz
from django.db.models import Q
from django.db import transaction
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework import status
from backend.api_permissions.role_based_api_permissions import (
    HasHRCoordinatorPermission,
    HasSystemAdminPermission,
    HasTestAdminPermission,
    HasTestCenterManagerPermission,
)
from backend.static.languages import Language_id
from backend.custom_models.reason_for_testing_text import ReasonForTestingText
from backend.custom_models.reason_for_testing_priority import ReasonForTestingPriority
from backend.static.test_skill_codenames import (
    TestSkillSLEDescCodename,
    TestSkillTypeCodename,
)
from backend.static.reason_for_testing import ReasonForTestingTypeCodenameConst
from backend.custom_models.reason_for_testing_type import ReasonForTestingType
from backend.serializers.reason_for_testing_serializer import (
    ReasonForTestingPrioritySerializer,
    ReasonForTestingSerializer,
)
from backend.custom_models.reason_for_testing import ReasonForTesting
from backend.celery.task_definition.uit import (
    send_deactivate_uit_test_cancellation_email,
)
from backend.serializers.reports_data_serializer import TestDefinitionSerializer
from backend.celery.tasks import (
    send_uit_emails_celery_task,
    send_update_uit_validity_end_date_email_celery_task,
    delete_uit_invite_email_celery_task,
)
from backend.custom_models.orderless_financial_data import (
    OrderlessFinancialData,
)
from backend.custom_models.uit_reasons_for_modification import UITReasonsForModification
from backend.custom_models.unsupervised_test_access_code_model import (
    UnsupervisedTestAccessCode,
)
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.uit_invites import UITInvites
from backend.custom_models.uit_reasons_for_deletion import UITReasonsForDeletion
from backend.views.test_access_code import (
    generate_random_test_access_code,
    access_code_already_exists,
)
from backend.custom_models.uit_invite_related_candidates import (
    UITInviteRelatedCandidates,
)
from backend.serializers.uit_processes_serializer import (
    GetUITProcessesSerializer,
    GetSelectedUITProcessSeializer,
    UITReasonsForDeletionSerializer,
    UITReasonsForModificationSerializer,
    GetTestPermissionsSerializer,
)
from backend.ref_table_views.cat_ref_departments_vw import CatRefDepartmentsVW
from backend.static.assigned_test_status import (
    AssignedTestStatus,
    get_assigned_test_status_id,
)
from backend.custom_models.accommodation_request import AccommodationRequest
from backend.custom_models.break_bank import BreakBank
from backend.custom_models.additional_time import AdditionalTime
from backend.static.psc_department import PscDepartment
from backend.serializers.cat_ref_departments_serializer import (
    CatRefDepartmentsSerializer,
)
from cms.cms_models.test_permissions_model import TestPermissions
from cms.cms_models.test_definition import TestDefinition
from cms.views.utils import (
    get_optional_parameters,
    get_user_info_from_jwt_token,
    get_needed_parameters,
)
from cms.views.retrieve_test_section_data import get_total_test_time
from cms.cms_models.test_skill_type import TestSkillType
from cms.cms_models.test_skill_sle_desc import TestSkillSLEDesc
from user_management.views.utils import CustomPagination
from text_resources_backend.text_resources import TextResources
from db_views.db_view_models.uit_completed_processes_vw import UitCompletedProcessesVW
from db_views.serializers.uit_serializers import (
    UitCompletedProcessesViewSerializer,
    UitRelatedCandidatesViewSerializer,
)
from db_views.db_view_models.uit_related_candidates_vw import UitRelatedCandidatesVW


# getting active processes where at least one test (within the respective process) is not taken or in progress (otherwise it's a completed process)
def get_active_processes_based_on_test_statuses(active_processes):
    # building active_process_uit_invite_ids
    active_process_uit_invite_ids = []
    for active_process in active_processes:
        active_process_uit_invite_ids.append(active_process.id)
    # getting related unsupervised test access codes
    related_unsupervised_test_access_codes = UnsupervisedTestAccessCode.history.filter(
        uit_invite_id__in=active_process_uit_invite_ids, history_type="+"
    )
    # getting non-historical related unsupervised test access code UIT Invite IDs
    non_historical_related_unsupervised_test_access_code_uit_invite_ids = (
        UnsupervisedTestAccessCode.objects.filter(
            uit_invite_id__in=active_process_uit_invite_ids
        ).values_list("uit_invite_id", flat=True)
    )
    # getting related assigned tests data
    related_assigned_test_data = AssignedTest.objects.filter(
        uit_invite_id__in=active_process_uit_invite_ids
    )
    # initializing final_active_processes_array
    final_active_processes_array = []
    # for each active process, checking if all tests are invalidated and/or taken
    for active_process in active_processes:
        # in case of a non-orderless request making sure that the ta_test_permissions_id is defined, otherwise don't add it as an active process
        valid_active_process = True
        # non-orderless request + ta_test_permissions_id not defined (can be the case for older data)
        if (
            not active_process.orderless_request
            and active_process.ta_test_permissions_id is None
        ):
            valid_active_process = False
        # valid_active_process is set to True
        if valid_active_process:
            # there are still available unsupervised test access codes
            if (
                active_process.id
                in non_historical_related_unsupervised_test_access_code_uit_invite_ids
            ):
                # populating final_active_processes_array
                final_active_processes_array.append(active_process)

            # no more available unsupervised test access codes found
            else:
                # checking if there are some "in-progress" tests for this current uit process
                # getting unsupervised test access codes to consider
                unsupervised_test_access_codes_to_consider = []
                # looping in related_unsupervised_test_access_codes
                for (
                    unsupervised_test_access_code
                ) in related_unsupervised_test_access_codes:
                    if active_process.id == unsupervised_test_access_code.uit_invite_id:
                        unsupervised_test_access_codes_to_consider.append(
                            unsupervised_test_access_code.test_access_code
                        )
                # checking if there are some "in-progress" tests for this current uit process
                # looping in unsupervised_test_access_codes_to_consider
                for tac in unsupervised_test_access_codes_to_consider:
                    # looping in related_assigned_test_data
                    for assigned_test in related_assigned_test_data:
                        # matching uit_invite_id and test_access_code
                        if (
                            assigned_test.uit_invite_id == active_process.id
                            and assigned_test.test_access_code == tac
                        ):
                            # test status is not SUBMITTED/QUIT (process not completed)
                            unassigned_status_id = get_assigned_test_status_id(
                                AssignedTestStatus.UNASSIGNED
                            )
                            submitted_status_id = get_assigned_test_status_id(
                                AssignedTestStatus.SUBMITTED
                            )
                            quit_status_id = get_assigned_test_status_id(
                                AssignedTestStatus.QUIT
                            )
                            if (
                                assigned_test.status_id != submitted_status_id
                                and assigned_test.status_id != quit_status_id
                                and assigned_test.status_id != unassigned_status_id
                            ):
                                # populating final_active_processes_array
                                final_active_processes_array.append(active_process)
                                break

    # removing duplicates
    final_active_processes_array_without_duplicates = []
    for active_process in final_active_processes_array:
        if not any(
            x.id == active_process.id
            for x in final_active_processes_array_without_duplicates
        ):
            final_active_processes_array_without_duplicates.append(active_process)

    return final_active_processes_array_without_duplicates


# sending UIT invitations
class SendUitInvitations(APIView):
    def post(self, request):
        return send_uit_invitations(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


def send_uit_invitations(request):
    user_info = get_user_info_from_jwt_token(request)

    # making sure that we have the needed parameters
    success, parameters = get_needed_parameters(
        ["candidates_list", "validity_end_date", "tests", "orderless_request"], request
    )

    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    # orderless request
    if parameters["orderless_request"]:
        # optional parameters
        reference_number = request.data.get("reference_number", None)
        department_ministry_id = request.data.get("department_ministry_id", None)
        fis_organisation_code = request.data.get("fis_organisation_code", None)
        fis_reference_code = request.data.get("fis_reference_code", None)
        billing_contact_name = request.data.get("billing_contact_name", None)
        billing_contact_info = request.data.get("billing_contact_info", None)
        reason_for_testing_id = request.data.get("reason_for_testing_id", None)
        level_required = request.data.get("level_required", None)
        additional_info_provided_in_csv = request.data.get(
            "additional_info_provided_in_csv", None
        )

        # initializing needed parameters
        reason_for_testing_provided_in_csv = False
        level_required_provided_in_csv = False
        uit_invite = None

        try:
            with transaction.atomic():
                # initializing candidate_invitations_array
                candidate_invitations_array = []
                # getting test_id (orderless requests only contain 1 test)
                test_id = parameters["tests"]
                # getting test data
                test_info = TestDefinition.objects.get(id=test_id)
                default_test_time = get_total_test_time(test_id)

                staffing_or_reference_number = reference_number

                # initializing org_data
                org_data = {
                    "en": "Public Service Commission",
                    "fr": "Commission de la Fonction Publique",
                }
                # getting organization data
                try:
                    org_data_from_db = CatRefDepartmentsVW.objects.filter(
                        dept_id=department_ministry_id
                    ).first()
                    # organization data found
                    if org_data_from_db:
                        org_data = {
                            "en": org_data_from_db.edesc,
                            "fr": org_data_from_db.fdesc,
                        }
                except:
                    pass

                # creating new entry in UITInvites table
                uit_invite = UITInvites.objects.create(
                    invite_date=datetime.datetime.now(pytz.utc),
                    validity_end_date=parameters["validity_end_date"],
                    ta_test_permissions_id=None,
                    ta_user_id=user_info["user_id"],
                    orderless_request=True,
                    test_id=test_id,
                )

                # getting all reasons for testing needed data
                reading_reason_for_testing_type_id = ReasonForTestingType.objects.get(
                    codename=ReasonForTestingTypeCodenameConst.READING
                ).id
                reasons_for_testing = ReasonForTestingSerializer(
                    ReasonForTesting.objects.filter(
                        reason_for_testing_type_id=reading_reason_for_testing_type_id,
                        active=1,
                    ),
                    many=True,
                ).data

                reasons_for_testing_fr_descriptions = []
                for reason_for_testing_data in reasons_for_testing:
                    reasons_for_testing_fr_descriptions.append(
                        reason_for_testing_data["reason_for_testing_name_fr"]
                    )

                # additional_info_provided_in_csv is set to True
                if additional_info_provided_in_csv:
                    # checking if reason for testing and level required are set to "Provided in CSV"
                    if reason_for_testing_id == "CSV":
                        # setting reason_for_testing_provided_in_csv to True
                        reason_for_testing_provided_in_csv = True
                    if (
                        level_required == "CSV"
                        or level_required == ""
                        or level_required == []
                    ):
                        level_required_provided_in_csv = True

                    # initializing unique_combinations_array
                    unique_combinations_array = []

                    # looping in candidates_list
                    for candidate in parameters["candidates_list"]:
                        # updating reason_for_testing_id if provided in CSV
                        if reason_for_testing_provided_in_csv:
                            # description provided in FR (checking for FR first, since the CSV template contains the french wording first)
                            if (
                                candidate["Motif_pour_evaluation_Reason_for_Testing"]
                                in reasons_for_testing_fr_descriptions
                            ):
                                # getting index of uit invite for the current iteration
                                index_of_related_reason_for_testing = [
                                    x["reason_for_testing_name_fr"]
                                    for x in reasons_for_testing
                                ].index(
                                    candidate[
                                        "Motif_pour_evaluation_Reason_for_Testing"
                                    ]
                                )
                            # description provided in EN
                            else:
                                # getting index of uit invite for the current iteration
                                index_of_related_reason_for_testing = [
                                    x["reason_for_testing_name_en"]
                                    for x in reasons_for_testing
                                ].index(
                                    candidate[
                                        "Motif_pour_evaluation_Reason_for_Testing"
                                    ]
                                )
                            # getting reason for testing ID
                            reason_for_testing_id = reasons_for_testing[
                                index_of_related_reason_for_testing
                            ]["id"]
                        # updating level_required if provided in CSV
                        if level_required_provided_in_csv:
                            level_required = candidate["Niveau_requis_Level_Required"]
                        # current combination does not already exist
                        if (
                            "{0}-{1}".format(reason_for_testing_id, level_required)
                            not in unique_combinations_array
                        ):
                            # adding current combination to unique_combinations_array
                            unique_combinations_array.append(
                                "{0}-{1}".format(reason_for_testing_id, level_required)
                            )
                            # creating new entry in orderless_financial_data table
                            OrderlessFinancialData.objects.create(
                                reference_number=reference_number,
                                department_ministry_id=department_ministry_id,
                                fis_organisation_code=fis_organisation_code,
                                fis_reference_code=fis_reference_code,
                                billing_contact_name=billing_contact_name,
                                billing_contact_info=billing_contact_info,
                                reason_for_testing_id=reason_for_testing_id,
                                level_required=level_required,
                                uit_invite_id=uit_invite.id,
                            )

                else:
                    # creating new entry in orderless_financial_data table
                    OrderlessFinancialData.objects.create(
                        reference_number=reference_number,
                        department_ministry_id=department_ministry_id,
                        fis_organisation_code=fis_organisation_code,
                        fis_reference_code=fis_reference_code,
                        billing_contact_name=billing_contact_name,
                        billing_contact_info=billing_contact_info,
                        reason_for_testing_id=reason_for_testing_id,
                        level_required=level_required,
                        uit_invite_id=uit_invite.id,
                    )

                # looping in candidates_list
                for candidate in parameters["candidates_list"]:
                    # creating new row in uit_invite_related_candidates table
                    UITInviteRelatedCandidates.objects.create(
                        first_name=candidate["Prenom_First_Name"],
                        last_name=candidate["Nom_de_famille_Last_Name"],
                        email=candidate["Courriel_Email"],
                        uit_invite_id=uit_invite.id,
                    )

                    # call the function that generates the random room number
                    random_test_access_code = generate_random_test_access_code()
                    while access_code_already_exists(random_test_access_code):
                        random_test_access_code = generate_random_test_access_code()
                    # additional_info_provided_in_csv is set to True
                    if additional_info_provided_in_csv:
                        # if reason for testing is provided in CSV
                        if reason_for_testing_provided_in_csv:
                            # description provided in FR (checking for FR first, since the CSV template contains the french wording first)
                            if (
                                candidate["Motif_pour_evaluation_Reason_for_Testing"]
                                in reasons_for_testing_fr_descriptions
                            ):
                                # getting index of uit invite for the current iteration
                                index_of_related_reason_for_testing = [
                                    x["reason_for_testing_name_fr"]
                                    for x in reasons_for_testing
                                ].index(
                                    candidate[
                                        "Motif_pour_evaluation_Reason_for_Testing"
                                    ]
                                )
                            # description provided in EN
                            else:
                                # getting index of uit invite for the current iteration
                                index_of_related_reason_for_testing = [
                                    x["reason_for_testing_name_en"]
                                    for x in reasons_for_testing
                                ].index(
                                    candidate[
                                        "Motif_pour_evaluation_Reason_for_Testing"
                                    ]
                                )
                            # getting reason for testing ID
                            current_reason_for_testing_id = reasons_for_testing[
                                index_of_related_reason_for_testing
                            ]["id"]
                        else:
                            current_reason_for_testing_id = reason_for_testing_id
                        # if level required is provided in CSV
                        if level_required_provided_in_csv:
                            # getting current_level_required based on current level required (from candidates_list)
                            current_level_required = candidate[
                                "Niveau_requis_Level_Required"
                            ]
                        else:
                            current_level_required = level_required

                    else:
                        current_reason_for_testing_id = reason_for_testing_id
                        current_level_required = level_required

                    # getting orderless_financial_data_id
                    orderless_financial_data_id = (
                        OrderlessFinancialData.objects.filter(
                            uit_invite_id=uit_invite.id,
                            level_required=current_level_required,
                            reason_for_testing_id=current_reason_for_testing_id,
                        )
                        .first()
                        .id
                    )

                    # saving the response with the needed parameters
                    unsupervised_test_access_code = (
                        UnsupervisedTestAccessCode.objects.create(
                            test_access_code=random_test_access_code,
                            test_order_number=None,
                            candidate_email=candidate["Courriel_Email"],
                            created_date=datetime.datetime.now(pytz.utc),
                            validity_end_date=parameters["validity_end_date"],
                            ta_user_id=user_info["user_id"],
                            test=TestDefinition.objects.get(id=test_id),
                            uit_invite_id=uit_invite.id,
                            orderless_financial_data_id=orderless_financial_data_id,
                            accommodation_request_id=None,
                        )
                    )

                    test_info_data = TestDefinitionSerializer(
                        test_info, many=False
                    ).data

                    # populating candidate_invitations_array
                    candidate_invitations_array.append(
                        {
                            "candidate_info": candidate,
                            "test_info": test_info_data,
                            "staffing_or_reference_number": staffing_or_reference_number,
                            "billing_contact_info": billing_contact_info,
                            "org_data": org_data,
                            "default_test_time": default_test_time,
                            "test_access_code": unsupervised_test_access_code.test_access_code,
                            "uit_invite_id": uit_invite.id,
                        }
                    )
        except:
            return Response(
                {"error": "Something happened during the send UIT invitations process"},
                status=status.HTTP_400_BAD_REQUEST,
            )
    # request with associated test permission
    else:
        # optional parameters
        test_order_number = request.data.get("test_order_number", None)

        try:
            with transaction.atomic():
                # initializing candidate_invitations_array
                candidate_invitations_array = []
                # looping in tests
                for test_id in parameters["tests"]:
                    # getting test data
                    test_info = TestDefinition.objects.get(id=test_id)
                    default_test_time = get_total_test_time(test_id)

                    # getting test permission related to the provided test order number combined with the current test ID
                    current_test_permission = TestPermissions.objects.get(
                        test_order_number=test_order_number,
                        test_id=test_id,
                        user_id=user_info["user_id"],
                    )

                    staffing_or_reference_number = (
                        current_test_permission.staffing_process_number
                    )
                    billing_contact_info = current_test_permission.billing_contact_info

                    # initializing org_data
                    org_data = {
                        "en": "Public Service Commission",
                        "fr": "Commission de la Fonction Publique",
                    }
                    # getting organization data
                    try:
                        org_data_from_db = CatRefDepartmentsVW.objects.filter(
                            dept_id=current_test_permission.department_ministry_code
                        ).first()
                        # organization data found
                        if org_data_from_db:
                            org_data = {
                                "en": org_data_from_db.edesc,
                                "fr": org_data_from_db.fdesc,
                            }
                    except:
                        pass

                    # creating new entry in UITInvites table
                    uit_invite = UITInvites.objects.create(
                        invite_date=datetime.datetime.now(pytz.utc),
                        validity_end_date=parameters["validity_end_date"],
                        ta_test_permissions_id=current_test_permission.id,
                        ta_user_id=user_info["user_id"],
                        orderless_request=False,
                        test_id=test_id,
                    )

                    # looping in candidates_list
                    for candidate in parameters["candidates_list"]:
                        # creating new row in uit_invite_related_candidates table
                        UITInviteRelatedCandidates.objects.create(
                            first_name=candidate["Prenom_First_Name"],
                            last_name=candidate["Nom_de_famille_Last_Name"],
                            email=candidate["Courriel_Email"],
                            uit_invite_id=uit_invite.id,
                        )
                        # call the function that generates the random room number
                        random_test_access_code = generate_random_test_access_code()
                        while access_code_already_exists(random_test_access_code):
                            random_test_access_code = generate_random_test_access_code()

                        # saving the response with the needed parameters
                        unsupervised_test_access_code = (
                            UnsupervisedTestAccessCode.objects.create(
                                test_access_code=random_test_access_code,
                                test_order_number=test_order_number,
                                candidate_email=candidate["Courriel_Email"],
                                created_date=datetime.datetime.now(pytz.utc),
                                validity_end_date=parameters["validity_end_date"],
                                ta_user_id=user_info["user_id"],
                                test=TestDefinition.objects.get(id=test_id),
                                uit_invite_id=uit_invite.id,
                                orderless_financial_data_id=None,
                                accommodation_request_id=None,
                            )
                        )

                        test_info_data = TestDefinitionSerializer(
                            test_info, many=False
                        ).data

                        # populating candidate_invitations_array
                        candidate_invitations_array.append(
                            {
                                "candidate_info": candidate,
                                "test_info": test_info_data,
                                "staffing_or_reference_number": staffing_or_reference_number,
                                "billing_contact_info": billing_contact_info,
                                "org_data": org_data,
                                "default_test_time": default_test_time,
                                "test_access_code": unsupervised_test_access_code.test_access_code,
                                "uit_invite_id": uit_invite.id,
                            }
                        )

        except:
            return Response(
                {"error": "Something happened during the send UIT invitations process"},
                status=status.HTTP_400_BAD_REQUEST,
            )

    # calling celery task to handle the send UIT emails action
    # when using this celery task to send the emails, the data creation part of the function is called and return the response 200 quiclky
    # so the emails are sent in the background and the TA can still navigate in the app while the emails are being sent
    send_uit_emails_celery_task.delay(
        candidate_invitations_array, parameters["validity_end_date"]
    )

    # another way to call a function in a separate thread (same behavior as the celery task)
    # import threading
    # starting send UIT emails in new thread
    # t = threading.Thread(
    #     target=<local_function_that_sends_the_emails>,
    #     args=(<parameter_1>, <parameter_2>, ...),
    # )
    # t.setDaemon(True)
    # t.start()

    return Response(status=status.HTTP_200_OK)


# UIT invitation end date validation (maximum of 10 000 candidates ending on the same date)
class UitInvitationEndDateValidation(APIView):
    def get(self, request):
        return uit_invitation_end_date_validation(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


def uit_invitation_end_date_validation(request):
    # making sure that we have the needed parameters
    success, parameters = get_needed_parameters(
        ["end_date", "total_number_of_candidates"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    # initializing is_valid and number_of_candidates
    is_valid = True
    number_of_candidates = 0

    try:
        # getting UIT Invites that are ending on provided date
        uit_invites_ending_on_provided_date = UITInvites.objects.filter(
            validity_end_date=parameters["end_date"]
        )

        # looping in uit_invites_ending_on_provided_date
        for uit_invite in uit_invites_ending_on_provided_date:
            # getting related candidates
            related_candidates = UITInviteRelatedCandidates.objects.filter(
                uit_invite_id=uit_invite.id
            )
            # incrementing number_of_candidates
            number_of_candidates += len(related_candidates)

        # adding provided number of candidates to number_of_candidates
        number_of_candidates = number_of_candidates + int(
            parameters["total_number_of_candidates"]
        )

        # number_of_candidates > 10 000
        if number_of_candidates > 10000:
            is_valid = False

    # invalid date (ex: 31-02-2000)
    except ValidationError:
        # setting is_valid to True, since the invalid date error will be handle in the frontend
        is_valid = True

    return Response({"is_valid": is_valid})


# getting All active UIT proccesses for TA
class GetAllActiveUitProcessesForTa(APIView):
    def get(self, request):
        return get_all_active_uit_processes_for_TA(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


def get_all_active_uit_processes_for_TA(request):
    user_info = get_user_info_from_jwt_token(request)
    success, parameters = get_needed_parameters(["page", "page_size"], request)

    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # get all related uit invite IDs
    related_historical_uit_invite_ids = (
        UITInvites.history.filter(
            ta_user_id=user_info["user_id"],
            validity_end_date__gte=datetime.datetime.now(pytz.utc),
        )
        .order_by("history_date")
        .values_list("id", flat=True)
    )

    # removing duplicates
    unique_related_historical_uit_invite_ids = []
    for id in related_historical_uit_invite_ids:
        if id not in unique_related_historical_uit_invite_ids:
            unique_related_historical_uit_invite_ids.append(id)

    # getting related uit invites
    related_uit_invites = UITInvites.objects.filter(
        id__in=unique_related_historical_uit_invite_ids
    )

    # get all related historical uit invites
    related_historical_uit_invites = UITInvites.history.filter(
        id__in=unique_related_historical_uit_invite_ids,
        history_type="+",
    )

    # creating active_processes_for_TA array where ta_test_permissions_id data comes from the historical data (in case the test permission is deleted)
    active_processes_for_TA = []
    for uit_invite in related_uit_invites:
        #  getting index of historical uit invite for the current iteration
        index_of_related_historical_uit_invite = [
            x.id for x in related_historical_uit_invites
        ].index(uit_invite.id)

        # getting related historical UITInvite
        historical_uit_invite = related_historical_uit_invites[
            index_of_related_historical_uit_invite
        ]

        # updating ta_test_permissions_id (based on historical data)
        uit_invite.ta_test_permissions_id = historical_uit_invite.ta_test_permissions_id
        active_processes_for_TA.append(uit_invite)

    # getting all active processes considering test statuses
    final_active_processes_array = get_active_processes_based_on_test_statuses(
        active_processes_for_TA
    )

    # getting UIT reasons for deletion
    # delete single candidate
    reasons_for_deletion_single_candidate = UITReasonsForDeletion.objects.exclude(
        en_name="Process Expired"
    )
    reasons_for_deletion_single_candidate_serialized_data = (
        UITReasonsForDeletionSerializer(
            reasons_for_deletion_single_candidate, many=True
        ).data
    )
    # delete whole process
    reasons_for_deletion_whole_process = UITReasonsForDeletion.objects.filter(
        en_name__in=("Wrong test sent", "Other")
    )
    reasons_for_deletion_whole_process_serialized_data = (
        UITReasonsForDeletionSerializer(
            reasons_for_deletion_whole_process, many=True
        ).data
    )

    # getting UIT reasons for modification
    reasons_for_modification = UITReasonsForModification.objects.all()
    reasons_for_modification_serialized_data = UITReasonsForModificationSerializer(
        reasons_for_modification, many=True
    ).data

    # sorting final_active_processes_array by validity end date (ascending)
    final_active_processes_array = sorted(
        final_active_processes_array,
        key=lambda k: k.validity_end_date,
        reverse=False,
    )

    # only getting data for current selected page
    new_active_processes_for_TA = final_active_processes_array[
        (current_page - 1) * page_size : current_page * page_size
    ]
    # getting serialized completed processes for TA
    serializer = GetUITProcessesSerializer(new_active_processes_for_TA, many=True)

    # checking if a next page number exists
    # if not, set it to 0
    next_page_number = 0
    if len(active_processes_for_TA) - (current_page * page_size) > 0:
        next_page_number = current_page + 1

    # checking if a previous page number exists
    # if not, set it to 0
    previous_page_number = 0
    if current_page > 1:
        previous_page_number = current_page - 1

    # we didn't use get_paginated_response here since we needed some additional data (reasons_for_deletion_single_candidate, reasons_for_deletion_whole_process and reasons_for_modification)
    return Response(
        {
            "next_page_number": next_page_number,
            "previous_page_number": previous_page_number,
            "count": len(final_active_processes_array),
            "current_page_number": current_page,
            "results": serializer.data,
            "reasons_for_deletion_single_candidate": reasons_for_deletion_single_candidate_serialized_data,
            "reasons_for_deletion_whole_process": reasons_for_deletion_whole_process_serialized_data,
            "reasons_for_modification": reasons_for_modification_serialized_data,
        }
    )


# getting All Active UIT proccesses for TA
class GetFoundActiveUitProcessesForTa(APIView):
    def get(self, request):
        return get_found_active_uit_processes_for_TA(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


def get_found_active_uit_processes_for_TA(request):
    user_info = get_user_info_from_jwt_token(request)
    success, parameters = get_needed_parameters(
        ["language", "keyword", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, language, keyword = itemgetter(
        "page", "page_size", "language", "keyword"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # get test permissions related to provided TA
    historical_ta_related_test_permissions = TestPermissions.history.filter(
        user_id=user_info["user_id"]
    ).order_by("history_date")
    # removing duplicates from combined_query_lists
    ta_related_test_permissions = []
    for related_test_permission in historical_ta_related_test_permissions:
        if not any(
            test_permission_in_final_query_list.id == related_test_permission.id
            for test_permission_in_final_query_list in ta_related_test_permissions
        ):
            ta_related_test_permissions.append(related_test_permission)

    # if there are no related test permissions
    if not ta_related_test_permissions:
        # setting test_permissions to empty array
        test_permissions = []
    else:
        # get all tests permissions related to ta_related_test_permissions test order numbers and tests
        test_permissions = TestPermissions.history.filter(
            reduce(
                or_,
                [
                    Q(test_order_number=test_permission.test_order_number)
                    & Q(test_id=test_permission.test_id)
                    for test_permission in ta_related_test_permissions
                ],
            ),
            history_type="+",
        ).values_list("id", flat=True)

    # get all related uit invite IDs
    related_historical_uit_invite_ids = (
        UITInvites.history.filter(
            ta_test_permissions_id__in=test_permissions,
            validity_end_date__gte=datetime.datetime.now(pytz.utc),
        )
        .order_by("history_date")
        .values_list("id", flat=True)
    )

    # removing duplicates
    unique_related_historical_uit_invite_ids = []
    for id in related_historical_uit_invite_ids:
        if id not in unique_related_historical_uit_invite_ids:
            unique_related_historical_uit_invite_ids.append(id)

    # get all related uit invite IDs (without any defined ta_test_permission_id)
    related_historical_uit_invite_ids_without_defined_ta_test_permission_id = (
        UITInvites.history.filter(
            ta_test_permissions_id__isnull=True,
            validity_end_date__gte=datetime.datetime.now(pytz.utc),
        )
        .order_by("history_date")
        .values_list("id", flat=True)
    )

    # removing duplicates
    unique_related_historical_uit_invite_ids_without_defined_ta_test_permission_id = []
    for id in related_historical_uit_invite_ids_without_defined_ta_test_permission_id:
        if (
            id
            not in unique_related_historical_uit_invite_ids_without_defined_ta_test_permission_id
        ):
            unique_related_historical_uit_invite_ids_without_defined_ta_test_permission_id.append(
                id
            )

    # getting related uit invites
    related_uit_invites = UITInvites.objects.filter(
        id__in=unique_related_historical_uit_invite_ids
    )

    # get all related historical uit invites
    related_historical_uit_invites = UITInvites.history.filter(
        id__in=unique_related_historical_uit_invite_ids,
        history_type="+",
    )

    # creating active_processes_for_TA array where ta_test_permissions_id data comes from the historical data (in case the test permission is deleted)
    active_processes_for_TA = []
    for uit_invite in related_uit_invites:
        #  getting index of historical uit invite for the current iteration
        index_of_related_historical_uit_invite = [
            x.id for x in related_historical_uit_invites
        ].index(uit_invite.id)

        # getting related historical UITInvite
        historical_uit_invite = related_historical_uit_invites[
            index_of_related_historical_uit_invite
        ]

        # updating ta_test_permissions_id (based on historical data)
        uit_invite.ta_test_permissions_id = historical_uit_invite.ta_test_permissions_id
        active_processes_for_TA.append(uit_invite)

    # getting orderless active processes associated to respective TA (where ta_test_permissions_id is None)
    orderless_uit_invites = UITInvites.objects.filter(
        orderless_request=True,
        ta_user_id=user_info["user_id"],
        validity_end_date__gte=datetime.datetime.now(pytz.utc),
    )
    for orderless_uit_invite in orderless_uit_invites:
        # populating active_processes_for_TA
        active_processes_for_TA.append(orderless_uit_invite)

    # if blank search
    if keyword == " ":
        # getting related completed processes
        found_active_processes_for_TA = get_active_processes_based_on_test_statuses(
            active_processes_for_TA
        )
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword).lower()
        # getting related test permission IDs
        test_permission_ids = UITInvites.history.filter(
            ta_test_permissions_id__in=test_permissions,
            validity_end_date__gte=datetime.datetime.now(pytz.utc),
        ).values_list("ta_test_permissions_id", flat=True)
        # getting related active UIT Invite IDs
        active_uit_invite_ids = UITInvites.history.filter(
            id__in=unique_related_historical_uit_invite_ids,
            ta_test_permissions_id__in=test_permissions,
        ).values_list("id", flat=True)
        # getting orderless active processes associated to respective TA (where ta_test_permissions_id is None)
        orderless_uit_invites = UITInvites.objects.filter(
            id__in=unique_related_historical_uit_invite_ids_without_defined_ta_test_permission_id,
            ta_test_permissions_id__isnull=True,
            ta_user_id=user_info["user_id"],
            orderless_request=True,
        )
        # getting orderless active process IDs
        orderless_uit_invite_ids = UITInvites.objects.filter(
            id__in=unique_related_historical_uit_invite_ids_without_defined_ta_test_permission_id,
            ta_test_permissions_id__isnull=True,
            ta_user_id=user_info["user_id"],
        ).values_list("id", flat=True)

        # getting related test permissions (from historical table)
        related_historical_test_permissions = TestPermissions.history.filter(
            id__in=test_permission_ids
        ).order_by("history_date")

        # ==================== SEARCH BY STAFFING PROCESS / REFERENCE NUMBER ====================
        # initializing matching_process_numbers_permission_ids
        matching_process_numbers_permission_ids = []
        # looping in related_historical_test_permissions
        for test_permission in related_historical_test_permissions:
            # matching keyword
            if keyword in test_permission.staffing_process_number.lower():
                # populating matching_process_numbers_permission_ids
                matching_process_numbers_permission_ids.append(test_permission.id)

        # get All active proccesses
        active_processes_historical_data_matching_process_number = (
            UITInvites.history.filter(
                id__in=unique_related_historical_uit_invite_ids,
                ta_test_permissions_id__in=matching_process_numbers_permission_ids,
            ).order_by("history_date")
        )

        # creating active_processes_for_TA array where ta_test_permissions_id data comes from the historical data (in case the test permission is deleted)
        active_processes_for_TA = []
        for active_process in active_processes_historical_data_matching_process_number:
            # populating active_processes_for_TA
            active_processes_for_TA.append(active_process)

        # getting related orderless financial data
        related_orderless_financial_data = OrderlessFinancialData.objects.filter(
            uit_invite_id__in=orderless_uit_invite_ids,
            reference_number__icontains=keyword,
        )

        # looping in orderless_uit_invites
        for orderless_financial_data in related_orderless_financial_data:
            # looping in orderless_uit_invites
            for orderless_uit_invite in orderless_uit_invites:
                if orderless_uit_invite.id == orderless_financial_data.uit_invite_id:
                    # populating active_processes_for_TA
                    active_processes_for_TA.append(orderless_uit_invite)
                    break

        # getting related active processes
        search_by_process_number = get_active_processes_based_on_test_statuses(
            active_processes_for_TA
        )
        # ==================== SEARCH BY STAFFING PROCESS / REFERENCE NUMBER (END) ====================

        # ==================== SEARCH BY TEST CODE ====================
        # getting all active processes based on keyword that is matching with test code
        # initializing test_ids
        test_ids = []
        # looping in related_historical_test_permissions
        for test_permissions in related_historical_test_permissions:
            # populating test_ids
            test_ids.append(test_permissions.test_id)
        matching_test_codes = TestDefinition.objects.filter(
            id__in=test_ids, test_code__icontains=keyword
        ).values_list("id", flat=True)
        matching_test_permission_ids = []
        # looping in related_historical_test_permissions
        for test_permissions in related_historical_test_permissions:
            # matching test permission
            if test_permissions.test_id in matching_test_codes:
                # populating matching_test_permission_ids
                matching_test_permission_ids.append(test_permissions.id)

        # get All active proccesses
        active_processes_historical_data_matching_test_permissions = (
            UITInvites.history.filter(
                id__in=unique_related_historical_uit_invite_ids,
                ta_test_permissions_id__in=matching_test_permission_ids,
            ).order_by("history_date")
        )

        related_active_uit_invite_ids = (
            UITInvites.history.filter(
                id__in=unique_related_historical_uit_invite_ids,
                ta_test_permissions_id__in=matching_test_permission_ids,
            )
            .order_by("history_date")
            .values_list("id", flat=True)
        )

        # getting related unsupervised test access codes
        related_unsupervised_test_access_codes = (
            UnsupervisedTestAccessCode.history.filter(
                uit_invite_id__in=related_active_uit_invite_ids
            )
        )
        # getting related test IDs from unsupervised test access code table
        test_ids_from_orderless = UnsupervisedTestAccessCode.history.filter(
            uit_invite_id__in=orderless_uit_invite_ids
        ).values_list("test_id", flat=True)
        # getting related test IDs based on previous parameters
        matching_test_code_ids_from_orderless = TestDefinition.objects.filter(
            id__in=test_ids_from_orderless, test_code__icontains=keyword
        ).values_list("id", flat=True)

        # initializing active_processes_for_TA_matching_test_permissions
        active_processes_for_TA_matching_test_permissions = []

        # looping in matching_test_codes_from_orderless
        for (
            active_process
        ) in active_processes_historical_data_matching_test_permissions:
            # looping in related_unsupervised_test_access_codes
            for unsupervised_test_access_code in related_unsupervised_test_access_codes:
                # matching UIT Invite ID
                if active_process.id == unsupervised_test_access_code.uit_invite_id:
                    # current test_id exists in matching_test_codes
                    if unsupervised_test_access_code.test_id in matching_test_codes:
                        # populating active_processes_for_TA_matching_test_permissions
                        active_processes_for_TA_matching_test_permissions.append(
                            active_process
                        )
                        break

        # getting related unsupervised test access codes
        related_unsupervised_test_access_codes = (
            UnsupervisedTestAccessCode.history.filter(
                uit_invite_id__in=orderless_uit_invite_ids
            )
        )

        # looping in matching_test_codes_from_orderless
        for orderless_uit_invite in orderless_uit_invites:
            # looping in related_unsupervised_test_access_codes
            for unsupervised_test_access_code in related_unsupervised_test_access_codes:
                # matching UIT Invite ID
                if (
                    orderless_uit_invite.id
                    == unsupervised_test_access_code.uit_invite_id
                ):
                    # current test_id exists in matching_test_code_ids_from_orderless
                    if (
                        unsupervised_test_access_code.test_id
                        in matching_test_code_ids_from_orderless
                    ):
                        # populating active_processes_for_TA_matching_test_permissions
                        active_processes_for_TA_matching_test_permissions.append(
                            orderless_uit_invite
                        )
                        break

        # getting related active processes
        search_by_test_codes = get_active_processes_based_on_test_statuses(
            active_processes_for_TA_matching_test_permissions
        )
        # ==================== SEARCH BY TEST CODE (END) ====================

        # ==================== SEARCH BY VALIDITY END DATE ====================
        # initializing considered_uit_invites
        considered_uit_invites = []
        # looping in active_processes_historical_data
        for active_process_data in related_uit_invites:
            considered_uit_invites.append(active_process_data)
        # getting all active processes based on keyword that is matching with validity end date
        # looping in orderless_uit_invites
        for orderless_uit_invite in orderless_uit_invites:
            # if keyword is contained in validity_end_date or invite_date of current respective orderless UIT invite
            if (
                orderless_uit_invite.validity_end_date.strftime("%Y-%m-%d").find(
                    keyword
                )
                != -1
                or orderless_uit_invite.invite_date.strftime("%Y-%m-%d").find(keyword)
                != -1
            ):
                considered_uit_invites.append(orderless_uit_invite)
        # getting related active processes
        related_uit_invites_ids = [
            data.id
            for data in get_active_processes_based_on_test_statuses(
                considered_uit_invites
            )
        ]

        search_by_validity_end_date = UITInvites.objects.filter(
            id__in=related_uit_invites_ids, validity_end_date__contains=keyword
        )

        # ==================== SEARCH BY VALIDITY END DATE (END) ====================

        # ==================== SEARCH BY INVITATION DATE ====================

        search_by_invitation_date = UITInvites.objects.filter(
            id__in=related_uit_invites_ids, invite_date__contains=keyword
        )

        # ==================== SEARCH BY INVITATION DATE (END) ====================

        # ==================== LIST OF ALL ACTIVE UITINVITES =====================

        # get all history active proccesses
        active_processes_historical = UITInvites.history.filter(
            id__in=related_uit_invites_ids,
        ).order_by("history_date")

        active_processes_list = []

        # add orderless invites to the list of active processes
        for orderless_invite in orderless_uit_invites:
            active_processes_list.append(orderless_invite)
        # add active processes that aren't orderless to the list of active processes
        for invite in active_processes_historical:
            active_processes_list.append(invite)

        # getting related active processes
        full_active_processes_list = get_active_processes_based_on_test_statuses(
            active_processes_list
        )

        # ==================== LIST OF ALL ACTIVE UITINVITES (END) ==================

        # ==================== SEARCH BY REQUESTING ORGANIZATION ====================

        # getting related orderless financial data
        related_orderless_financial_data = OrderlessFinancialData.objects.filter(
            uit_invite_id__in=orderless_uit_invite_ids
        )

        # initializing search_by_requesting_org
        search_by_requesting_org = []

        for invitation in full_active_processes_list:
            # orderless
            if invitation.orderless_request:
                # looping in related_orderless_financial_data
                for (
                    related_orderless_financial_item
                ) in related_orderless_financial_data:
                    if invitation.id == related_orderless_financial_item.uit_invite_id:
                        try:
                            # getting department_ministry_code
                            department_ministry_data = CatRefDepartmentsVW.objects.get(
                                dept_id=related_orderless_financial_item.department_ministry_id
                            )
                            # building test_permission_data object (financial data object in that case)
                            department_ministry_data = CatRefDepartmentsSerializer(
                                department_ministry_data, many=False
                            ).data

                        except:
                            department_ministry_data = None

                        if department_ministry_data:
                            # English search
                            if language == "en":
                                if (
                                    keyword in department_ministry_data["edesc"].lower()
                                    or keyword
                                    in department_ministry_data["eabrv"].lower()
                                ):
                                    search_by_requesting_org.append(invitation)
                            # French search
                            else:
                                if (
                                    keyword in department_ministry_data["fdesc"].lower()
                                    or keyword
                                    in department_ministry_data["fabrv"].lower()
                                ):
                                    search_by_requesting_org.append(invitation)

            # not orderless
            else:
                if (
                    invitation.ta_test_permissions_id is not None
                    and invitation.ta_test_permissions_id
                ):
                    related_test_permissions_array = []
                    # looping in related_historical_test_permissions
                    for (
                        related_historical_test_permission
                    ) in related_historical_test_permissions:
                        # matching ta_test_permissions_id and ta_user_id
                        if (
                            related_historical_test_permission.id
                            == invitation.ta_test_permissions_id
                            and related_historical_test_permission.user_id
                            == invitation.ta_user_id
                        ):
                            # populating related_test_permissions_array
                            related_test_permissions_array.append(
                                related_historical_test_permission
                            )
                            break

                    # getting last item from related_test_permissions_array
                    test_permission_data = related_test_permissions_array[
                        len(related_test_permissions_array) - 1
                    ]

                    # getting serialized test permission data
                    test_permission_data = GetTestPermissionsSerializer(
                        test_permission_data, many=False
                    ).data

                    # organization data found
                    if test_permission_data:
                        # English search
                        if language == "en":
                            if (
                                keyword
                                in test_permission_data["department_ministry_data"][
                                    "edesc"
                                ].lower()
                                or keyword
                                in test_permission_data["department_ministry_data"][
                                    "eabrv"
                                ].lower()
                            ):
                                search_by_requesting_org.append(invitation)
                        # French search
                        else:
                            if (
                                keyword
                                in test_permission_data["department_ministry_data"][
                                    "fdesc"
                                ].lower()
                                or keyword
                                in test_permission_data["department_ministry_data"][
                                    "fabrv"
                                ].lower()
                            ):
                                search_by_requesting_org.append(invitation)

        # ==================== SEARCH BY REQUESTING ORGANIZATION (END) ====================

        # ==================== SEARCH BY INVITATION EMAIL ====================

        # getting UIT related candidates (orderless and non-orderless)
        uit_related_candidates_list = UITInviteRelatedCandidates.objects.filter(
            uit_invite_id__in=active_uit_invite_ids
        )
        uit_related_candidates_list_orderless = (
            UITInviteRelatedCandidates.objects.filter(
                uit_invite_id__in=orderless_uit_invite_ids
            )
        )

        # creating uit_related_candidates_array
        uit_related_candidates_array = []
        for candidate in uit_related_candidates_list:
            uit_related_candidates_array.append(candidate)
        for candidate in uit_related_candidates_list_orderless:
            uit_related_candidates_array.append(candidate)

        search_by_invitation_email = []

        # for each uit invite, get all the candidate emails and look if the keyword is present
        for active_process in full_active_processes_list:
            # looping in uit_related_candidates_array
            for uit_related_candidate in uit_related_candidates_array:
                # matching process
                if active_process.id == uit_related_candidate.uit_invite_id:
                    # checking if there is any match
                    if keyword in uit_related_candidate.email.lower():
                        search_by_invitation_email.append(active_process)
                        break

        # ==================== SEARCH BY INVITATION EMAIL (END) ====================

        # if at least one result has been found based on the keyword provided
        if (
            search_by_process_number
            or search_by_test_codes
            or search_by_validity_end_date
            or search_by_invitation_date
            or search_by_requesting_org
            or search_by_invitation_email
        ):
            found_active_processes_for_TA = (
                list(search_by_process_number)
                + list(search_by_test_codes)
                + list(search_by_validity_end_date)
                + list(search_by_invitation_date)
                + list(search_by_requesting_org)
                + list(search_by_invitation_email)
            )

        # there are no results found based on the keyword provided
        else:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # removing duplicates
    found_active_processes_ids = []
    for active_process in found_active_processes_for_TA:
        if active_process.id not in found_active_processes_ids:
            found_active_processes_ids.append(active_process.id)

    found_active_processes_historical_data = UITInvites.history.filter(
        id__in=found_active_processes_ids, history_type="+"
    )

    found_active_processes_historical_data_ids = UITInvites.history.filter(
        id__in=found_active_processes_ids, history_type="+"
    ).values_list("id", flat=True)

    final_related_uit_invites = UITInvites.objects.filter(
        id__in=found_active_processes_historical_data_ids
    )

    # creating found_completed_processes_array array where ta_test_permissions_id data comes from the historical data (in case the test permission is deleted)
    found_active_processes_array = []
    # looping in found_active_processes_historical_data
    for active_process in found_active_processes_historical_data:
        # looping in final_related_uit_invites
        for final_related_uit_invite in final_related_uit_invites:
            # matching uit invite id
            if active_process.id == final_related_uit_invite.id:
                # updating validity_end_date data (based on historical data)
                active_process.validity_end_date = (
                    final_related_uit_invite.validity_end_date
                )
                found_active_processes_array.append(active_process)
                break

    # sorting found_active_processes_array by validity end date (ascending)
    found_active_processes_array = sorted(
        found_active_processes_array,
        key=lambda k: k.validity_end_date,
        reverse=False,
    )

    # only getting data for current selected page
    new_active_processes_for_TA = found_active_processes_array[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # getting UIT reasons for deletion
    # delete single candidate
    reasons_for_deletion_single_candidate = UITReasonsForDeletion.objects.exclude(
        en_name="Process Expired"
    )
    reasons_for_deletion_single_candidate_serialized_data = (
        UITReasonsForDeletionSerializer(
            reasons_for_deletion_single_candidate, many=True
        ).data
    )
    # delete whole process
    reasons_for_deletion_whole_process = UITReasonsForDeletion.objects.filter(
        en_name__in=("Wrong test sent", "Other")
    )
    reasons_for_deletion_whole_process_serialized_data = (
        UITReasonsForDeletionSerializer(
            reasons_for_deletion_whole_process, many=True
        ).data
    )

    # getting UIT reasons for modification
    reasons_for_modification = UITReasonsForModification.objects.all()
    reasons_for_modification_serialized_data = UITReasonsForModificationSerializer(
        reasons_for_modification, many=True
    ).data

    # checking if a next page number exists
    # if not, set it to 0
    next_page_number = 0
    if len(found_active_processes_array) - (current_page * page_size) > 0:
        next_page_number = current_page + 1

    # checking if a previous page number exists
    # if not, set it to 0
    previous_page_number = 0
    if current_page > 1:
        previous_page_number = current_page - 1

    # getting serialized active processes for TA
    serializer = GetUITProcessesSerializer(new_active_processes_for_TA, many=True)
    return Response(
        {
            "next_page_number": next_page_number,
            "previous_page_number": previous_page_number,
            "count": len(found_active_processes_array),
            "current_page_number": current_page,
            "results": serializer.data,
            "reasons_for_deletion_single_candidate": reasons_for_deletion_single_candidate_serialized_data,
            "reasons_for_deletion_whole_process": reasons_for_deletion_whole_process_serialized_data,
            "reasons_for_modification": reasons_for_modification_serialized_data,
        }
    )


class GetSelectedUITActiveProcessesDetails(APIView):
    def get(self, request):
        return get_selected_uit_active_processes_details(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


def get_selected_uit_active_processes_details(request):
    success, parameters = get_needed_parameters(["id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    # get selected active proccesses details
    view_selected_active_process = UITInvites.objects.get(id=parameters["id"])
    # getting serialized active processes for TA
    serializer = GetSelectedUITProcessSeializer(
        view_selected_active_process, many=False
    )
    return Response(serializer.data)


class UpdateSelectedUITActiveProcessValidityEndDate(APIView):
    def post(self, request):
        return update_selected_uit_active_process_validity_end_date(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


def update_selected_uit_active_process_validity_end_date(request):
    success, parameters = get_needed_parameters(
        ["id", "validity_end_date", "reason_for_modification_id"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    # get selected active proccesses details
    selected_active_process = UITInvites.objects.get(id=parameters["id"])
    # updating needed data
    selected_active_process.validity_end_date = parameters["validity_end_date"]
    selected_active_process.reason_for_modification_id = parameters[
        "reason_for_modification_id"
    ]
    selected_active_process.save()

    # orderless request
    if selected_active_process.orderless_request:
        # getting test data based on historical unsupervised test access code data
        test_id = (
            UnsupervisedTestAccessCode.history.filter(
                uit_invite_id=selected_active_process.id
            )
            .first()
            .test_id
        )
        orderless_financial_data = OrderlessFinancialData.objects.filter(
            uit_invite_id=selected_active_process.id
        ).first()
        staffing_or_reference_number = orderless_financial_data.reference_number
        billing_contact_info = orderless_financial_data.billing_contact_info

        # initializing org_data
        org_data = {
            "en": "Public Service Commission",
            "fr": "Commission de la Fonction Publique",
        }
        # getting organization data
        try:
            org_data_from_db = CatRefDepartmentsVW.objects.filter(
                dept_id=orderless_financial_data.department_ministry_id
            ).first()
            # organization data found
            if org_data_from_db:
                org_data = {
                    "en": org_data_from_db.edesc,
                    "fr": org_data_from_db.fdesc,
                }
        except:
            pass

    # request with associated test permission
    else:
        # getting historical selected active proccesses details
        historical_selected_active_process = UITInvites.history.get(
            id=parameters["id"], history_type="+"
        )

        # getting most updated version of test permission data
        test_permission_data = (
            TestPermissions.history.filter(
                id=historical_selected_active_process.ta_test_permissions_id
            )
            .order_by("history_date")
            .last()
        )
        test_id = test_permission_data.test_id
        staffing_or_reference_number = test_permission_data.staffing_process_number
        billing_contact_info = test_permission_data.billing_contact_info

        # initializing org_data
        org_data = {
            "en": "Public Service Commission",
            "fr": "Commission de la Fonction Publique",
        }
        # getting organization data
        try:
            org_data_from_db = CatRefDepartmentsVW.objects.filter(
                dept_id=test_permission_data.department_ministry_code
            ).first()
            # organization data found
            if org_data_from_db:
                org_data = {
                    "en": org_data_from_db.edesc,
                    "fr": org_data_from_db.fdesc,
                }
        except:
            pass

    # getting related test data
    test_data = TestDefinitionSerializer(
        TestDefinition.objects.get(id=test_id), many=False
    ).data

    # getting total test time
    total_test_time = get_total_test_time(test_data["id"])

    # getting related historical test access codes data
    historical_unsupervised_test_access_codes_data = (
        UnsupervisedTestAccessCode.history.filter(
            uit_invite_id=selected_active_process.id, history_type="+"
        )
    )

    # getting related test access codes data
    unsupervised_test_access_codes_data = UnsupervisedTestAccessCode.objects.filter(
        uit_invite_id=selected_active_process.id
    )

    # getting related test access code IDs
    unsupervised_test_access_code_ids = UnsupervisedTestAccessCode.objects.filter(
        uit_invite_id=selected_active_process.id
    ).values_list("id", flat=True)

    # getting related assigned test data
    assigned_status_id = get_assigned_test_status_id(AssignedTestStatus.ASSIGNED)
    checked_in_status_id = get_assigned_test_status_id(AssignedTestStatus.CHECKED_IN)
    pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
    related_assigned_test_data = AssignedTest.objects.filter(
        uit_invite_id=selected_active_process.id,
        status_id__in=(
            assigned_status_id,
            checked_in_status_id,
            pre_test_status_id,
        ),
    )

    # getting related candidates data
    related_candidates_data = UITInviteRelatedCandidates.objects.filter(
        uit_invite_id=selected_active_process.id
    )

    # initializing candidates_to_send_email
    candidates_to_send_email = []

    # looping in historical_unsupervised_test_access_codes_data
    for (
        historical_test_access_code_data
    ) in historical_unsupervised_test_access_codes_data:
        # initializing need_to_send_email
        need_to_send_email = False
        # current historical unsupervised test access code exists in unsupervised_test_access_code_ids
        if historical_test_access_code_data.id in unsupervised_test_access_code_ids:
            # getting index of unsupervised test access code for the current iteration
            index_of_unsupervised_test_access_code = [
                x.id for x in unsupervised_test_access_codes_data
            ].index(historical_test_access_code_data.id)
            # getting current test access code data
            current_test_access_code = unsupervised_test_access_codes_data[
                index_of_unsupervised_test_access_code
            ]
            # set need_to_send_email to True
            need_to_send_email = True
            # updating validity_end_date or current test access code
            current_test_access_code.validity_end_date = parameters["validity_end_date"]
            current_test_access_code.save()

        # if there is no active test access code found, making sure that at least one candidate has not started, completed the test or did not get his test unassigned
        elif related_assigned_test_data:
            # looping in related_assigned_test_data
            for assigned_test in related_assigned_test_data:
                if (
                    assigned_test.test_access_code
                    == historical_test_access_code_data.test_access_code
                ):
                    # set need_to_send_email to True
                    need_to_send_email = True
                    break

        # if need_to_send_email is true
        if need_to_send_email:
            # getting index of related candidate for the current iteration
            index_of_related_candidate = [
                x.email for x in related_candidates_data
            ].index(historical_test_access_code_data.candidate_email)
            # getting candidate info
            candidate_info = related_candidates_data[index_of_related_candidate]
            # serializing candidate info
            candidate_info = {
                "first_name": related_candidates_data[
                    index_of_related_candidate
                ].first_name,
                "last_name": related_candidates_data[
                    index_of_related_candidate
                ].last_name,
                "email": related_candidates_data[index_of_related_candidate].email,
            }

            candidates_to_send_email.append(
                {
                    "candidate_info": candidate_info,
                    "test_data": test_data,
                    "staffing_or_reference_number": staffing_or_reference_number,
                    "billing_contact_info": billing_contact_info,
                    "validity_end_date": selected_active_process.validity_end_date,
                    "total_test_time": total_test_time,
                    "test_access_code": historical_test_access_code_data.test_access_code,
                    "org_data": org_data,
                }
            )

    # calling celery task to handle the send UIT emails action
    # when using this celery task to send the emails, the data creation part of the function is called and return the response 200 quiclky
    # so the emails are sent in the background and the TA can still navigate in the app while the emails are being sent
    send_update_uit_validity_end_date_email_celery_task.delay(candidates_to_send_email)

    return Response(status=status.HTTP_200_OK)


# getting All completed UIT proccesses for TA
class GetAllCompletedUitProcessesForTa(APIView):
    def get(self, request):
        return get_all_completed_uit_processes_for_TA(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


def get_all_completed_uit_processes_for_TA(request):
    user_info = get_user_info_from_jwt_token(request)
    success, parameters = get_needed_parameters(["page", "page_size"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting related completed processes
    completed_processes_for_TA = UitCompletedProcessesVW.objects.filter(
        ta_user_id=user_info["user_id"]
    )

    # sorting completed_processes_for_TA by validity end date (ascending)
    completed_processes_for_TA = sorted(
        completed_processes_for_TA,
        key=lambda k: k.validity_end_date,
        reverse=True,
    )

    # only getting data for current selected page
    new_completed_processes_for_TA = completed_processes_for_TA[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # getting serialized completed processes for TA
    serializer = UitCompletedProcessesViewSerializer(
        new_completed_processes_for_TA, many=True
    )
    return paginator.get_paginated_response(
        serializer.data, len(completed_processes_for_TA), current_page, page_size
    )


# getting All completed UIT proccesses for TA
class GetFoundCompletedUitProcessesForTa(APIView):
    def get(self, request):
        return get_found_completed_uit_processes_for_TA(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


def get_found_completed_uit_processes_for_TA(request):
    user_info = get_user_info_from_jwt_token(request)
    success, parameters = get_needed_parameters(
        ["language", "keyword", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, language, keyword = itemgetter(
        "page", "page_size", "language", "keyword"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        # getting related completed processes
        found_completed_processes_uit_invite_ids = (
            UitCompletedProcessesVW.objects.filter(
                ta_user_id=user_info["user_id"]
            ).values_list("id", flat=True)
        )
    # regular search
    else:
        # getting matching results for fields displayed in the table
        # English search
        if language == "en":
            found_completed_processes_uit_invite_ids = (
                UitCompletedProcessesVW.objects.filter(
                    Q(ta_user_id=user_info["user_id"])
                    & (
                        Q(assessment_process_or_reference_nbr__icontains=keyword)
                        | Q(test_code__icontains=keyword)
                        | Q(dept_eabrv__icontains=keyword)
                        | Q(dept_edesc__icontains=keyword)
                        | Q(validity_end_date__icontains=keyword)
                    )
                ).values_list("id", flat=True)
            )
        # French search
        else:
            found_completed_processes_uit_invite_ids = (
                UitCompletedProcessesVW.objects.filter(
                    Q(ta_user_id=user_info["user_id"])
                    & (
                        Q(assessment_process_or_reference_nbr__icontains=keyword)
                        | Q(test_code__icontains=keyword)
                        | Q(dept_fabrv__icontains=keyword)
                        | Q(dept_fdesc__icontains=keyword)
                        | Q(validity_end_date__icontains=keyword)
                    )
                ).values_list("id", flat=True)
            )

        # getting matching results related to emails in popups
        # getting related uit invite ids
        related_uit_invite_ids = UitCompletedProcessesVW.objects.filter(
            ta_user_id=user_info["user_id"]
        ).values_list("id", flat=True)

        # getting all related unsupervised test access codes (uit_invite_ids)
        related_unsupervised_test_access_codes_uit_invite_ids = (
            UITInviteRelatedCandidates.objects.filter(
                uit_invite_id__in=related_uit_invite_ids,
                email__icontains=keyword,
            ).values_list("uit_invite_id", flat=True)
        )

        matching_found_completed_processes_uit_invite_ids = (
            UitCompletedProcessesVW.objects.filter(
                ta_user_id=user_info["user_id"],
                id__in=related_unsupervised_test_access_codes_uit_invite_ids,
            ).values_list("id", flat=True)
        )

        # combining all results
        found_completed_processes_uit_invite_ids = list(
            set(
                list(found_completed_processes_uit_invite_ids)
                + list(matching_found_completed_processes_uit_invite_ids)
            )
        )

        # there are no results found based on the keyword provided
        if not found_completed_processes_uit_invite_ids:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    found_completed_processes_array = UitCompletedProcessesVW.objects.filter(
        id__in=found_completed_processes_uit_invite_ids
    )

    # sorting found_completed_processes_array by validity end date (ascending)
    found_completed_processes_array = sorted(
        found_completed_processes_array,
        key=lambda k: k.validity_end_date,
        reverse=True,
    )

    # only getting data for current selected page
    new_completed_processes_for_TA = found_completed_processes_array[
        (current_page - 1) * page_size : current_page * page_size
    ]
    # getting serialized active processes for TA
    serializer = UitCompletedProcessesViewSerializer(
        new_completed_processes_for_TA, many=True
    )
    return paginator.get_paginated_response(
        serializer.data,
        len(found_completed_processes_array),
        current_page,
        page_size,
    )


# getting selected completed UIT proccesses details for TA
class GetSelectedCompletedUitProcessesDetailsForTa(APIView):
    def get(self, request):
        return get_selected_completed_uit_processes_details_for_TA(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


def get_selected_completed_uit_processes_details_for_TA(request):
    success, parameters = get_needed_parameters(["uit_invite_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    # getting related uit_invite
    selected_uit_completed_process_data = UitRelatedCandidatesVW.objects.filter(
        uit_invite_id=parameters["uit_invite_id"]
    )

    # getting serialized completed processes for TA
    serialized_data = UitRelatedCandidatesViewSerializer(
        selected_uit_completed_process_data, many=True
    ).data
    return Response(serialized_data)


# deactivating single UIT test in an active process
class DeactivateSingleUitTest(APIView):
    def post(self, request):
        return deactivate_single_uit_test(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


def deactivate_single_uit_test(request):
    user_info = get_user_info_from_jwt_token(request)
    success, parameters = get_needed_parameters(
        ["uit_invite_id", "invited_candidate_email", "reason_for_deletion"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    try:
        # updating reason_for_deletion field on concerned UIT invite related candidate
        related_candidate = UITInviteRelatedCandidates.objects.get(
            uit_invite_id=parameters["uit_invite_id"],
            email=parameters["invited_candidate_email"],
        )
        related_candidate.reason_for_deletion_id = parameters["reason_for_deletion"]
        related_candidate.save()
        # getting related test access code data (from the historical table)
        historical_unsupervised_test_access_code_data = (
            UnsupervisedTestAccessCode.history.filter(
                uit_invite_id=parameters["uit_invite_id"],
                candidate_email=parameters["invited_candidate_email"],
            ).first()
        )
        # checking if the unsupervised test access code still exists in the table (real table, not the historical one)
        unsupervised_test_access_code = UnsupervisedTestAccessCode.objects.filter(
            test_access_code=historical_unsupervised_test_access_code_data.test_access_code
        ).first()
        # the unsupervised_test_access_code exists (meaning that the test has not been checked in yet)
        if unsupervised_test_access_code:
            # delete it
            unsupervised_test_access_code.delete()
        # the unsupervised_test_access_code does not exist (meaning that the test has already been checked in)
        else:
            # getting related assigned test
            assigned_test = AssignedTest.objects.get(
                uit_invite_id=parameters["uit_invite_id"],
                test_access_code=historical_unsupervised_test_access_code_data.test_access_code,
            )
            # making sure that the test status is CHECKED_IN or PRE-TEST
            checked_in_status_id = get_assigned_test_status_id(
                AssignedTestStatus.CHECKED_IN
            )
            pre_test_status_id = get_assigned_test_status_id(
                AssignedTestStatus.PRE_TEST
            )
            unassigned_status_id = get_assigned_test_status_id(
                AssignedTestStatus.UNASSIGNED
            )
            if (
                assigned_test.status_id == checked_in_status_id
                or assigned_test.status_id == pre_test_status_id
            ):
                # updating assigned test status to unassigned
                assigned_test.status_id = unassigned_status_id
                assigned_test.save()
            else:
                return Response(
                    {"error": TextResources.uitTestAlreadyStarted},
                    status=status.HTTP_400_BAD_REQUEST,
                )
    except UnsupervisedTestAccessCode.DoesNotExist:
        return Response(
            {
                "error": "the unsupervised test access code does not exist base on provided parameters"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    except AssignedTest.DoesNotExist:
        return Response(
            {"error": "the assigned test does not exist base on provided parameters"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    # getting related test data
    test_data = TestDefinitionSerializer(
        TestDefinition.objects.get(
            id=historical_unsupervised_test_access_code_data.test_id
        ),
        many=False,
    ).data
    # getting candidate info
    candidate_info = UITInviteRelatedCandidates.objects.get(
        uit_invite_id=historical_unsupervised_test_access_code_data.uit_invite_id,
        email=historical_unsupervised_test_access_code_data.candidate_email,
    )
    # serializing candidate info
    candidate_info = {
        "first_name": candidate_info.first_name,
        "last_name": candidate_info.last_name,
        "email": candidate_info.email,
    }
    # orderless request
    current_uit_invite = UITInvites.objects.get(id=parameters["uit_invite_id"])
    if current_uit_invite.orderless_request:
        orderless_financial_data = OrderlessFinancialData.objects.filter(
            uit_invite_id=current_uit_invite.id
        ).first()
        staffing_or_reference_number = orderless_financial_data.reference_number
        billing_contact_info = orderless_financial_data.billing_contact_info

        # initializing org_data
        org_data = {
            "en": "Public Service Commission",
            "fr": "Commission de la Fonction Publique",
        }
        # getting organization data
        try:
            org_data_from_db = CatRefDepartmentsVW.objects.filter(
                dept_id=orderless_financial_data.department_ministry_id
            ).first()
            # organization data found
            if org_data_from_db:
                org_data = {
                    "en": org_data_from_db.edesc,
                    "fr": org_data_from_db.fdesc,
                }
        except:
            pass

    # request with associated test permission
    else:
        # getting test permission data
        test_permission_data = (
            TestPermissions.history.filter(
                test_order_number=historical_unsupervised_test_access_code_data.test_order_number,
                test_id=historical_unsupervised_test_access_code_data.test_id,
                user_id=user_info["user_id"],
            )
            .order_by("history_date")
            .last()
        )
        staffing_or_reference_number = test_permission_data.staffing_process_number
        billing_contact_info = test_permission_data.billing_contact_info

        # initializing org_data
        org_data = {
            "en": "Public Service Commission",
            "fr": "Commission de la Fonction Publique",
        }
        # getting organization data
        try:
            org_data_from_db = CatRefDepartmentsVW.objects.filter(
                dept_id=test_permission_data.department_ministry_code
            ).first()
            # organization data found
            if org_data_from_db:
                org_data = {
                    "en": org_data_from_db.edesc,
                    "fr": org_data_from_db.fdesc,
                }
        except:
            pass
    # getting reason for deletion
    reason_for_deletion = UITReasonsForDeletion.objects.get(
        id=parameters["reason_for_deletion"]
    )
    reason_for_deletion = {
        "en": reason_for_deletion.en_name,
        "fr": reason_for_deletion.fr_name,
    }
    # send cancellation email
    send_deactivate_uit_test_cancellation_email(
        candidate_info,
        test_data,
        staffing_or_reference_number,
        billing_contact_info,
        historical_unsupervised_test_access_code_data.test_access_code,
        reason_for_deletion,
        org_data,
    )
    return Response(status=status.HTTP_200_OK)


# deactivating the whole UIT process (active processes)
class DeactivateUitProcess(APIView):
    def post(self, request):
        return deactivate_uit_process(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


def deactivate_uit_process(request):
    success, parameters = get_needed_parameters(
        ["uit_invite_id", "reason_for_deletion"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    try:
        # getting and updating UIT invite
        uit_invite = UITInvites.objects.get(id=parameters["uit_invite_id"])
        uit_invite.reason_for_deletion_id = parameters["reason_for_deletion"]
        uit_invite.save()
        # getting candidates related to the provided uit_invite_id
        related_candidates = UITInviteRelatedCandidates.objects.filter(
            uit_invite_id=parameters["uit_invite_id"]
        )
        # orderless request
        if uit_invite.orderless_request:
            # getting test data based on historical unsupervised test access code data
            test_id = (
                UnsupervisedTestAccessCode.history.filter(uit_invite_id=uit_invite.id)
                .first()
                .test_id
            )
            orderless_financial_data = OrderlessFinancialData.objects.filter(
                uit_invite_id=uit_invite.id
            ).first()
            staffing_or_reference_number = orderless_financial_data.reference_number
            billing_contact_info = orderless_financial_data.billing_contact_info

            # initializing org_data
            org_data = {
                "en": "Public Service Commission",
                "fr": "Commission de la Fonction Publique",
            }
            # getting organization data
            try:
                org_data_from_db = CatRefDepartmentsVW.objects.filter(
                    dept_id=orderless_financial_data.department_ministry_id
                ).first()
                # organization data found
                if org_data_from_db:
                    org_data = {
                        "en": org_data_from_db.edesc,
                        "fr": org_data_from_db.fdesc,
                    }
            except:
                pass

        # request with associated test permission
        else:
            # getting test permission data
            uit_invite_historical_data = UITInvites.history.get(
                id=parameters["uit_invite_id"], history_type="+"
            )
            test_permission_data = (
                TestPermissions.history.filter(
                    id=uit_invite_historical_data.ta_test_permissions_id
                )
                .order_by("history_date")
                .last()
            )
            test_id = test_permission_data.test_id
            staffing_or_reference_number = test_permission_data.staffing_process_number
            billing_contact_info = test_permission_data.billing_contact_info

            # initializing org_data
            org_data = {
                "en": "Public Service Commission",
                "fr": "Commission de la Fonction Publique",
            }
            # getting organization data
            try:
                org_data_from_db = CatRefDepartmentsVW.objects.filter(
                    dept_id=test_permission_data.department_ministry_code
                ).first()
                # organization data found
                if org_data_from_db:
                    org_data = {
                        "en": org_data_from_db.edesc,
                        "fr": org_data_from_db.fdesc,
                    }
            except:
                pass

        # getting related test data
        test_data = TestDefinitionSerializer(
            TestDefinition.objects.get(id=test_id), many=False
        ).data

        # getting related historical unsupervised test access code data
        related_historical_unsupervised_test_access_code_data = (
            UnsupervisedTestAccessCode.history.filter(
                uit_invite_id=parameters["uit_invite_id"], history_type="+"
            )
        )

        # getting related unsupervised test access code data
        related_unsupervised_test_access_code_data = (
            UnsupervisedTestAccessCode.objects.filter(
                uit_invite_id=parameters["uit_invite_id"]
            )
        )

        # getting related unsupervised test access code IDs
        related_unsupervised_test_access_code_ids = (
            UnsupervisedTestAccessCode.objects.filter(
                uit_invite_id=parameters["uit_invite_id"]
            ).values_list("id", flat=True)
        )

        # getting related assigned test data
        related_assigned_test_data = AssignedTest.objects.filter(
            uit_invite_id=parameters["uit_invite_id"]
        )

        # getting related assigned test test access codes
        related_assigned_test_test_access_codes = AssignedTest.objects.filter(
            uit_invite_id=parameters["uit_invite_id"]
        ).values_list("test_access_code", flat=True)

        # getting all reasons for deletion
        all_reasons_for_deletion = UITReasonsForDeletion.objects.all()

        # initializing candidates_to_send_email
        candidates_to_send_email = []

        # looping in related candidates
        for candidate in related_candidates:
            # initializing send_email
            send_email = False
            # getting index of historical unsupervised test access code for the current iteration
            index_of_historical_unsupervised_test_access_code = [
                x.candidate_email
                for x in related_historical_unsupervised_test_access_code_data
            ].index(candidate.email)
            # getting related test access code data (from the historical table)
            historical_unsupervised_test_access_code_data = (
                related_historical_unsupervised_test_access_code_data[
                    index_of_historical_unsupervised_test_access_code
                ]
            )
            # checking if the unsupervised test access code still exists in the table (real table, not the historical one)
            # meaning that the test has not been checked in yet
            if (
                historical_unsupervised_test_access_code_data.id
                in related_unsupervised_test_access_code_ids
            ):
                # getting index of unsupervised test access code for the current iteration
                index_of_unsupervised_test_access_code = [
                    x.id for x in related_unsupervised_test_access_code_data
                ].index(historical_unsupervised_test_access_code_data.id)
                # getting related test access code (from the real table, not the historical one)
                unsupervised_test_access_code = (
                    related_unsupervised_test_access_code_data[
                        index_of_unsupervised_test_access_code
                    ]
                )
                # delete it
                unsupervised_test_access_code.delete()
                # need to send a cancelation email
                send_email = True
            # the unsupervised_test_access_code does not exist (meaning that the test has already been checked in)
            else:
                # test access code exists in related_assigned_test_test_access_codes
                if (
                    historical_unsupervised_test_access_code_data.test_access_code
                    in related_assigned_test_test_access_codes
                ):
                    # getting index of related assigned test data for the current iteration
                    index_of_related_assigned_test_data = [
                        x.test_access_code for x in related_assigned_test_data
                    ].index(
                        historical_unsupervised_test_access_code_data.test_access_code
                    )
                    # getting assigned test
                    assigned_test = related_assigned_test_data[
                        index_of_related_assigned_test_data
                    ]
                    # if test status is CHECKED_IN or PRE-TEST
                    checked_in_status_id = get_assigned_test_status_id(
                        AssignedTestStatus.CHECKED_IN
                    )
                    pre_test_status_id = get_assigned_test_status_id(
                        AssignedTestStatus.PRE_TEST
                    )
                    unassigned_status_id = get_assigned_test_status_id(
                        AssignedTestStatus.UNASSIGNED
                    )
                    if (
                        assigned_test.status_id == checked_in_status_id
                        or assigned_test.status_id == pre_test_status_id
                    ):
                        # updating assigned test status to unassigned
                        assigned_test.status_id = unassigned_status_id
                        assigned_test.save()
                        # need to send a cancelation email
                        send_email = True

            if send_email:
                # getting index of reason for deletion for the current iteration
                index_of_reason_for_deletion = [
                    x.id for x in all_reasons_for_deletion
                ].index(parameters["reason_for_deletion"])
                # getting reason for deletion
                reason_for_deletion = all_reasons_for_deletion[
                    index_of_reason_for_deletion
                ]
                reason_for_deletion = {
                    "en": reason_for_deletion.en_name,
                    "fr": reason_for_deletion.fr_name,
                }

                # serializing candidate info
                candidate_info = {
                    "first_name": candidate.first_name,
                    "last_name": candidate.last_name,
                    "email": candidate.email,
                }

                candidates_to_send_email.append(
                    {
                        "candidate_info": candidate_info,
                        "test_data": test_data,
                        "staffing_or_reference_number": staffing_or_reference_number,
                        "billing_contact_info": billing_contact_info,
                        "test_access_code": historical_unsupervised_test_access_code_data.test_access_code,
                        "reason_for_deletion": reason_for_deletion,
                        "org_data": org_data,
                    }
                )

        # calling celery task to handle the send UIT emails action
        # when using this celery task to send the emails, the data creation part of the function is called and return the response 200 quiclky
        # so the emails are sent in the background and the TA can still navigate in the app while the emails are being sent
        delete_uit_invite_email_celery_task.delay(candidates_to_send_email)

    # these errors should never happen
    except UnsupervisedTestAccessCode.DoesNotExist:
        return Response(
            {
                "error": "the unsupervised test access code does not exist base on provided parameters"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    return Response(status=status.HTTP_200_OK)


# getting reason for testing priorities
class GetReasonForTestingPriorities(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasSystemAdminPermission | HasTestCenterManagerPermission),
    )

    def get(self, request):
        return get_reason_for_testing_priorities(request)


def get_reason_for_testing_priorities(request):
    # getting all reason for testing priorities
    reason_for_testing_priorities = ReasonForTestingPriority.objects.all()

    # serializing data
    serialized_data = ReasonForTestingPrioritySerializer(
        reason_for_testing_priorities, many=True
    ).data

    # ordering by id
    ordered_serialized_data = sorted(
        serialized_data,
        key=lambda k: k["id"],
        reverse=False,
    )

    return Response(ordered_serialized_data)


# getting reasons for testing
class GetReasonsForTesting(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (
            HasTestAdminPermission
            | HasHRCoordinatorPermission
            | HasSystemAdminPermission
        ),
    )

    def get(self, request):
        return get_reasons_for_testing(request)


def get_reasons_for_testing(request):
    optional_parameters = get_optional_parameters(
        ["test_skill_type_id", "test_skill_sub_type_id"], request
    )

    # test_skill_type_id id provided
    if (
        optional_parameters["test_skill_type_id"] is not None
        and optional_parameters["test_skill_type_id"] != ""
    ):
        # making sure that the provided parameters are int
        test_skill_type_id = int(optional_parameters["test_skill_type_id"])
        test_skill_sub_type_id = None
        if (
            optional_parameters["test_skill_sub_type_id"] != ""
            and optional_parameters["test_skill_sub_type_id"] is not None
        ):
            test_skill_sub_type_id = int(optional_parameters["test_skill_sub_type_id"])

        # getting sle test skill type ID
        sle_test_skill_type_id = TestSkillType.objects.get(
            codename=TestSkillTypeCodename.SLE
        ).id

        # SLE Test
        if test_skill_type_id == sle_test_skill_type_id:
            reading_sub_skill_ids = TestSkillSLEDesc.objects.filter(
                codename__in=(
                    TestSkillSLEDescCodename.READING_EN,
                    TestSkillSLEDescCodename.READING_FR,
                )
            ).values_list("id", flat=True)
            writing_sub_skill_ids = TestSkillSLEDesc.objects.filter(
                codename__in=(
                    TestSkillSLEDescCodename.WRITING_EN,
                    TestSkillSLEDescCodename.WRITING_FR,
                )
            ).values_list("id", flat=True)
            oral_sub_skill_ids = TestSkillSLEDesc.objects.filter(
                codename__in=(
                    TestSkillSLEDescCodename.ORAL_EN,
                    TestSkillSLEDescCodename.ORAL_FR,
                )
            ).values_list("id", flat=True)

            # Reading
            if test_skill_sub_type_id in reading_sub_skill_ids:
                # getting reason for testing type ID
                reading_reason_for_testing_type_id = ReasonForTestingType.objects.get(
                    codename=ReasonForTestingTypeCodenameConst.READING
                ).id
                # getting related active reasons for testing
                active_reasons_for_testing = ReasonForTesting.objects.filter(
                    reason_for_testing_type_id=reading_reason_for_testing_type_id,
                    active=1,
                )
            # Writing
            elif test_skill_sub_type_id in writing_sub_skill_ids:
                # getting reason for testing type ID
                writing_reason_for_testing_type_id = ReasonForTestingType.objects.get(
                    codename=ReasonForTestingTypeCodenameConst.WRITING
                ).id
                # getting related active reasons for testing
                active_reasons_for_testing = ReasonForTesting.objects.filter(
                    reason_for_testing_type_id=writing_reason_for_testing_type_id,
                    active=1,
                )
            # Oral
            elif test_skill_sub_type_id in oral_sub_skill_ids:
                # getting reason for testing type ID
                oral_reason_for_testing_type_id = ReasonForTestingType.objects.get(
                    codename=ReasonForTestingTypeCodenameConst.ORAL
                ).id
                # getting related active reasons for testing
                active_reasons_for_testing = ReasonForTesting.objects.filter(
                    reason_for_testing_type_id=oral_reason_for_testing_type_id, active=1
                )
            else:
                return Response(
                    {"error": "Unsupported test_skill_sub_type_id!"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
        # Other Test
        else:
            # getting "Reading" reasons for testing by default (might need to change that logic in the future if we need specific reasons for testing for occupational tests)
            reading_reason_for_testing_type_id = ReasonForTestingType.objects.get(
                codename=ReasonForTestingTypeCodenameConst.READING
            ).id
            # getting related active reasons for testing
            active_reasons_for_testing = ReasonForTesting.objects.filter(
                reason_for_testing_type=reading_reason_for_testing_type_id, active=1
            )

    # test_skill_type_id is not provided, so return all active reasons for testing regardless of the type
    else:
        active_reasons_for_testing = ReasonForTesting.objects.filter(active=1)

    serialized_data = ReasonForTestingSerializer(
        active_reasons_for_testing, many=True
    ).data

    return Response(serialized_data)


# getting OLA reasons for testing
class GetOlaReasonsForTesting(APIView):
    permission_classes = (permissions.IsAuthenticated & (HasSystemAdminPermission),)

    def get(self, request):
        return get_ola_reasons_for_testing(request)


def get_ola_reasons_for_testing(request):
    # getting OLA reason for testing type
    ola_reason_for_testing_type_id = ReasonForTestingType.objects.get(
        codename=ReasonForTestingTypeCodenameConst.ORAL
    ).id

    ola_reasons_for_testing = ReasonForTesting.objects.filter(
        reason_for_testing_type_id=ola_reason_for_testing_type_id
    )

    serialized_data = ReasonForTestingSerializer(
        ola_reasons_for_testing, many=True
    ).data

    return Response(serialized_data)


# editing reason for testing
class EditReasonForTesting(APIView):
    permission_classes = (permissions.IsAuthenticated & (HasSystemAdminPermission),)

    def post(self, request):
        return edit_reason_for_testing(request)


def edit_reason_for_testing(request):
    reason_for_testing_data = json.loads(request.body)

    try:
        # getting respective reason for testing data based on provided ID
        respective_reason_for_testing_data = ReasonForTesting.objects.get(
            id=reason_for_testing_data["id"]
        )

        # getting respective reason for testing text data (EN and FR)
        respective_reason_for_testing_text_data_en = ReasonForTestingText.objects.get(
            reason_for_testing_id=respective_reason_for_testing_data.id,
            language_id=Language_id.EN,
        )
        respective_reason_for_testing_text_data_fr = ReasonForTestingText.objects.get(
            reason_for_testing_id=respective_reason_for_testing_data.id,
            language_id=Language_id.FR,
        )

        # updating needed fields
        respective_reason_for_testing_text_data_en.text = reason_for_testing_data[
            "reason_for_testing_priority_name_en"
        ]
        respective_reason_for_testing_text_data_en.save()
        respective_reason_for_testing_text_data_fr.text = reason_for_testing_data[
            "reason_for_testing_priority_name_fr"
        ]
        respective_reason_for_testing_text_data_fr.save()
        respective_reason_for_testing_data.reason_for_testing_priority_id = (
            reason_for_testing_data["reason_for_testing_priority_id"]
        )
        respective_reason_for_testing_data.minimum_process_length = (
            reason_for_testing_data["minimum_process_length"]
        )
        respective_reason_for_testing_data.waiting_period = reason_for_testing_data[
            "waiting_period"
        ]
        respective_reason_for_testing_data.active = reason_for_testing_data["active"]
        respective_reason_for_testing_data.save()

        return Response(status=status.HTTP_200_OK)

    # should never happen
    except ReasonForTesting.DoesNotExist:
        return Response(
            {
                "error": "Unable to find the reason for testing based on provided parameters"
            },
            status=status.HTTP_404_NOT_FOUND,
        )

    # should never happen
    except ReasonForTestingText.DoesNotExist:
        return Response(
            {
                "error": "Unable to find the reason for testing text based on provided parameters"
            },
            status=status.HTTP_404_NOT_FOUND,
        )

    # should never happen
    except:
        return Response(
            {
                "error": "Something went wrong during the edit reason for testing process"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
