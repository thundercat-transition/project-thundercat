from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from backend.custom_models.ola_global_configs import OlaGlobalConfigs
from backend.static.ola_global_configs_const import OlaGlobalConfigsConst
from backend.serializers.ola_global_configs_serializer import OlaGlobalConfigsSerializer
from cms.views.utils import get_needed_parameters
from operator import itemgetter
from backend.api_permissions.role_based_api_permissions import (
    HasSystemAdminPermission,
)


class GetOlaGlobalConfigs(APIView):
    def get(self, request):
        return get_ola_global_configs(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class SetOlaGlobalConfigs(APIView):
    permission_classes = (permissions.IsAuthenticated & (HasSystemAdminPermission),)

    def post(self, request):
        return set_ola_global_configs(request)


def get_ola_global_configs(request):
    codenames = [
        OlaGlobalConfigsConst.CANCELLATION_WINDOW,
        OlaGlobalConfigsConst.OPEN_BOOKING_WINDOW,
    ]
    ola_global_configs = OlaGlobalConfigs.objects.filter(codename__in=codenames)
    # getting serialized active tests data
    serialized_ola_global_configs = OlaGlobalConfigsSerializer(
        ola_global_configs, many=True
    ).data
    return Response(serialized_ola_global_configs, status=status.HTTP_200_OK)


def set_ola_global_configs(request):
    # get the codenames
    cancellation_window = OlaGlobalConfigsConst.CANCELLATION_WINDOW
    open_booking_window = OlaGlobalConfigsConst.OPEN_BOOKING_WINDOW
    success, parameters = get_needed_parameters(
        [
            cancellation_window,
            open_booking_window,
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    cancellation_window_value, open_booking_window_value = itemgetter(
        cancellation_window,
        open_booking_window,
    )(parameters)

    # update the configs
    cw_config = OlaGlobalConfigs.objects.get(codename=cancellation_window)
    cw_config.value = cancellation_window_value
    cw_config.save()
    obw_config = OlaGlobalConfigs.objects.get(codename=open_booking_window)
    obw_config.value = open_booking_window_value
    obw_config.save()
    return Response({"OK": "OK"}, status=status.HTTP_200_OK)
