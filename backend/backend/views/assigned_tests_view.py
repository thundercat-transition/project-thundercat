from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.retrieve_assigned_tests_view import (
    retrieve_assigned_tests,
    retrieve_uit_assigned_tests,
)


class AssignedTestsSet(APIView):
    def get(self, request):
        return retrieve_assigned_tests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class UitAssignedTestsSet(APIView):
    def get(self, request):
        return retrieve_uit_assigned_tests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
