import requests
from django.conf import settings


class DiscoveryUrlTarget:
    PPC_ORDER_SERVICE = "eureka/apps/PPC-ORDER-SERVICE"
    OAUTH_PROVIDER = "eureka/apps/OAUTH-PROVIDER"


# getting discovery url based on target parameter
def get_discovery_url(discovery_url_target):
    try:
        # getting endpoint based on provided target
        endpoint = settings.DISCOVERY_URL + discovery_url_target
        headers = {"Accept": "application/json", "Content-Type": "application/json"}

        response = requests.get(endpoint, headers=headers)

        # converting response to json format
        data = response.json()

        # initializing instance return
        instance_url = ""
        # getting an instance where status is UP
        for instance in data["application"]["instance"]:
            if instance["status"] == "UP":
                instance_url = instance["homePageUrl"]

        # returning instance_url
        return instance_url
    # settings.DISCOVERY_URL is not found/provided
    except:
        return ""
