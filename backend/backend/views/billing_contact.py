# getting all test centers as ETTA
import json
from datetime import date
from rest_framework import status
from rest_framework.response import Response
from backend.custom_models.assessment_process_assigned_test_specs import (
    AssessmentProcessAssignedTestSpecs,
)
from backend.custom_models.assessment_process import AssessmentProcess
from backend.serializers.billing_contact_serializer import BillingContactVWSerializer
from db_views.db_view_models.billing_contact_vw import BillingContactVW
from user_management.user_management_models.user_models import User
from backend.views.utils import get_user_info_from_jwt_token
from backend.custom_models.billing_contact import BillingContact
from user_management.views.utils import CustomPagination
from cms.views.utils import get_needed_parameters, get_optional_parameters
from operator import itemgetter
from django.db.models import Q
from urllib import parse


def create_billing_contact(request):
    user_info = get_user_info_from_jwt_token(request)
    user = User.objects.get(id=user_info["user_id"])

    try:
        billing_contact_json = json.loads(request.body)

        BillingContact.objects.create(
            user=user,
            first_name=billing_contact_json["first_name"],
            last_name=billing_contact_json["last_name"],
            email=billing_contact_json["email"],
            department_id=billing_contact_json["department_id"],
            fis_organisation_code=billing_contact_json["fis_organisation_code"],
            fis_reference_code=billing_contact_json["fis_reference_code"],
        )
    # Errors:
    # - If body is not compatible with json.loads
    # - If missing parameters when calling directly the API
    except (KeyError, json.JSONDecodeError, TypeError):
        return Response(status=status.HTTP_400_BAD_REQUEST)

    return Response(status=status.HTTP_201_CREATED)


def modify_billing_contact(request):
    user_info = get_user_info_from_jwt_token(request)
    user = User.objects.get(id=user_info["user_id"])

    try:
        billing_contact_json = json.loads(request.body)

        # Getting existing billing contact
        billing_contact = BillingContact.objects.get(
            id=billing_contact_json["id"], user=user
        )

        # Updating with each parameters
        billing_contact.first_name = billing_contact_json["first_name"]
        billing_contact.last_name = billing_contact_json["last_name"]
        billing_contact.email = billing_contact_json["email"]
        billing_contact.department_id = billing_contact_json["department_id"]
        billing_contact.fis_organisation_code = billing_contact_json[
            "fis_organisation_code"
        ]
        billing_contact.fis_reference_code = billing_contact_json["fis_reference_code"]

        # Saving to database
        billing_contact.save()

    # If we can't find the billing contact to modify, we return HTTP_400_BAD_REQUEST
    except BillingContact.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    # Errors:
    # - If body is not compatible with json.loads
    # - If missing parameters when calling directly the API
    except (KeyError, json.JSONDecodeError, TypeError):
        return Response(status=status.HTTP_400_BAD_REQUEST)

    return Response(status=status.HTTP_200_OK)


def delete_billing_contact(request):
    user_info = get_user_info_from_jwt_token(request)
    user = User.objects.get(id=user_info["user_id"])

    success, parameters = get_needed_parameters(
        [
            "id",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    (id) = itemgetter(
        "id",
    )(parameters)

    try:
        BillingContact.objects.get(id=id, user=user).delete()

    # If we can't find the billing contact to delete, we return HTTP_400_BAD_REQUEST
    except BillingContact.DoesNotExist:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    return Response(status=status.HTTP_200_OK)


def get_all_billing_contacts(request):
    user_info = get_user_info_from_jwt_token(request)

    parameters = get_optional_parameters(["page", "page_size"], request)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # getting all test centers
    billing_contacts = BillingContactVW.objects.filter(user_id=user_info["user_id"])

    # serializing data
    serialized_data = BillingContactVWSerializer(billing_contacts, many=True).data

    # for each billing contact, checking if it is currently used in a non-sent/active assessment process (not expired basically)
    # initializing billing_contact_ids
    billing_contact_ids = []
    # getting current date
    current_date = date.today()
    # getting all billing_contact_ids from the non-expired assessment processes
    user_related_assessment_processes = AssessmentProcess.objects.filter(
        user_id=user_info["user_id"], closing_date__gte=current_date
    )
    for assessment_process_data in user_related_assessment_processes:
        # getting all related assessment process assigned test specs
        related_assessment_process_assigned_test_specs = (
            AssessmentProcessAssignedTestSpecs.objects.filter(
                assessment_process_id=assessment_process_data.id
            ).values_list("billing_contact_id", flat=True)
        )
        for (
            assigned_test_specs_billing_contact_id
        ) in related_assessment_process_assigned_test_specs:
            if (
                assigned_test_specs_billing_contact_id is not None
                and assigned_test_specs_billing_contact_id not in billing_contact_ids
            ):
                billing_contact_ids.append(assigned_test_specs_billing_contact_id)

    # looping in serialized_data and populating a new attribute called "exists_in_non_expired_process"
    for serialized_billing_contact_data in serialized_data:
        serialized_billing_contact_data["exists_in_non_expired_process"] = (
            serialized_billing_contact_data["id"] in billing_contact_ids
        )

    # getting sorted serialized data by name (ascending)
    ordered_billing_contacts = sorted(
        serialized_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )

    # page and page_size are provided
    if page is not None and page_size is not None:
        # initializing current_page and page_size
        current_page = int(page)
        page_size = int(page_size)

        # pagination class
        pagination_class = CustomPagination
        paginator = pagination_class()

        # only getting data for current selected page
        current_billing_contacts = ordered_billing_contacts[
            (current_page - 1) * page_size : current_page * page_size
        ]

        # final data using pagination library
        final_object = paginator.get_paginated_response(
            current_billing_contacts,
            len(ordered_billing_contacts),
            current_page,
            page_size,
        )

        return final_object

    # page and page_size are NOT provided
    else:
        return Response(ordered_billing_contacts)


def get_found_billing_contacts(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(
        ["keyword", "current_language", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, keyword, current_language = itemgetter(
        "page", "page_size", "keyword", "current_language"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        found_billing_contacts = BillingContactVW.objects.filter(
            user_id=user_info["user_id"]
        )

    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # search while interface is in English
        if current_language == "en":
            found_billing_contacts = BillingContactVW.objects.filter(
                Q(user_id=user_info["user_id"])
                & (
                    Q(first_name__icontains=keyword)
                    | Q(last_name__icontains=keyword)
                    | Q(email__icontains=keyword)
                    | Q(fis_organisation_code__icontains=keyword)
                    | Q(fis_reference_code__icontains=keyword)
                    | Q(dept_eabrv__icontains=keyword)
                    | Q(dept_edesc__icontains=keyword)
                )
            )

        # search while interface is in French
        else:
            found_billing_contacts = BillingContactVW.objects.filter(
                Q(user_id=user_info["user_id"])
                & (
                    Q(first_name__icontains=keyword)
                    | Q(last_name__icontains=keyword)
                    | Q(email__icontains=keyword)
                    | Q(fis_organisation_code__icontains=keyword)
                    | Q(fis_reference_code__icontains=keyword)
                    | Q(dept_fabrv__icontains=keyword)
                    | Q(dept_fdesc__icontains=keyword)
                )
            )

    # serializing data
    serialized_data = BillingContactVWSerializer(found_billing_contacts, many=True).data

    # getting sorted serialized data by name (ascending)
    ordered_billing_contacts = sorted(
        serialized_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )
    # only getting data for current selected page
    current_billing_contacts = ordered_billing_contacts[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # final data using pagination library
    final_object = paginator.get_paginated_response(
        current_billing_contacts,
        len(ordered_billing_contacts),
        current_page,
        page_size,
    )

    return final_object
