from rest_framework.response import Response
from rest_framework import status
from datetime import date
import datetime
import pytz
from backend.custom_models.break_bank import BreakBank
from backend.custom_models.accommodation_request import AccommodationRequest
from backend.custom_models.user_accommodation_file_break_bank import (
    UserAccommodationFileBreakBank,
)
from backend.custom_models.user_accommodation_file_test_time import (
    UserAccommodationFileTestTime,
)
from backend.custom_models.consumed_reservation_codes import ConsumedReservationCodes
from backend.custom_models.additional_time import AdditionalTime
from backend.custom_models.test_access_code_model import TestAccessCode
from backend.custom_models.unsupervised_test_access_code_model import (
    UnsupervisedTestAccessCode,
)
from backend.custom_models.assigned_test import AssignedTest
from backend.static.assigned_test_status import (
    AssignedTestStatus,
    get_assigned_test_status_id,
)
from backend.custom_models.assigned_test_section import AssignedTestSection
from cms.cms_models.test_section import TestSection
from db_views.db_view_models.test_center_test_sessions_vw import (
    TestCenterTestSessionsVW,
)


def check_into_room(test_access_code, user_id):
    # initializing unsupervised_test
    unsupervised_test = False

    assigned_status_id = get_assigned_test_status_id(AssignedTestStatus.ASSIGNED)
    checked_in_status_id = get_assigned_test_status_id(AssignedTestStatus.CHECKED_IN)
    pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
    active_status_id = get_assigned_test_status_id(AssignedTestStatus.ACTIVE)
    transition_status_id = get_assigned_test_status_id(AssignedTestStatus.TRANSITION)
    locked_status_id = get_assigned_test_status_id(AssignedTestStatus.LOCKED)
    paused_status_id = get_assigned_test_status_id(AssignedTestStatus.PAUSED)

    # check if the provided test access code exists in TestAccessCode table
    room = TestAccessCode.objects.filter(test_access_code=test_access_code).first()
    # test access code does not exist in TestAccessCode table
    if not room:
        # check if the provided test access code exists in UnsupervisedTestAccessCode table
        room = UnsupervisedTestAccessCode.objects.filter(
            test_access_code=test_access_code
        ).first()
        if room:
            unsupervised_test = True
    # no room has been found based on provided test access code
    if not room:
        return Response(
            {"error": "No room has been found based on provided test access code."},
            status=status.HTTP_400_BAD_REQUEST,
        )
    # check if room has already been checked in by the current username
    elif AssignedTest.objects.filter(
        test_access_code=test_access_code,
        user_id=user_id,
    ).first():
        return Response(
            {
                "error": "This room has already been checked in using the provided test access code."
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    # check if the test (based on the test access code) is already in an active state in assigned test table
    elif AssignedTest.objects.filter(
        user_id=user_id,
        test_id=room.test_id,
        status_id__in=(
            assigned_status_id,
            checked_in_status_id,
            pre_test_status_id,
            active_status_id,
            transition_status_id,
            locked_status_id,
            paused_status_id,
        ),
    ):
        return Response(
            {
                "error": "The test you are trying to check-in is already in an active state."
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    else:
        # getting needed data
        test_admin_id = room.ta_user_id
        test_id = room.test_id

        # don't allow a TA to take their own exam
        if user_id == test_admin_id:
            return Response(
                {
                    "error": "You cannot be a candidate in an exam you are administering."
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        # check if this rooms test has already been assigned
        if not has_test_for_test_access_code_already_been_assigned(
            test_access_code, user_id
        ):
            # supervised test
            if not unsupervised_test:
                # making sure that the code is associated to the respective test session
                consumed_reservation_code_data = (
                    ConsumedReservationCodes.objects.filter(
                        candidate_id=user_id, test_session_id=room.test_session_id
                    )
                )
                # the test session association is correct
                if consumed_reservation_code_data:
                    # making sure that the we are not past the validity end date
                    # getting current time (UTC)
                    current_time = datetime.datetime.now(pytz.utc)
                    # getting related test session data
                    related_test_session_data = TestCenterTestSessionsVW.objects.get(
                        id=room.test_session_id
                    )
                    # we are past the test session end time
                    if current_time > related_test_session_data.end_time:
                        return Response(
                            {"error": "The test session end time has past."},
                            status=status.HTTP_400_BAD_REQUEST,
                        )
                    # initializing needed variables
                    accommodation_request_id = None
                    user_accommodation_file_id = (
                        consumed_reservation_code_data.last().user_accommodation_file_id
                    )
                    # checking if break bank has been set
                    temp_break_bank = UserAccommodationFileBreakBank.objects.filter(
                        user_accommodation_file_id=user_accommodation_file_id
                    )
                    if temp_break_bank:
                        # creating needed accommodation request/break bank data
                        new_break_bank = BreakBank.objects.create(
                            # converting minutes in seconds
                            break_time=temp_break_bank.last().break_time
                            * 60
                        )
                        new_accommodation_request = AccommodationRequest.objects.create(
                            break_bank_id=new_break_bank.id
                        )
                        accommodation_request_id = new_accommodation_request.id
                    # creating new row in AssignedTest table
                    assigned_test = AssignedTest(
                        status_id=checked_in_status_id,
                        test_id=test_id,
                        user_id=user_id,
                        ta_user_id=test_admin_id,
                        test_access_code=test_access_code,
                        uit_invite_id=None,
                        orderless_financial_data_id=None,
                        accommodation_request_id=accommodation_request_id,
                        test_session_id=room.test_session_id,
                        user_accommodation_file_id=user_accommodation_file_id,
                    )
                    assigned_test.save()
                    # linking new assigned test ID to the consumed reservation code data
                    consumed_reservation_code_to_update = (
                        ConsumedReservationCodes.objects.get(
                            id=consumed_reservation_code_data.last().id
                        )
                    )
                    consumed_reservation_code_to_update.assigned_test_id = (
                        assigned_test.id
                    )
                    consumed_reservation_code_to_update.save()
                # the test session association has not been found (triggering invalid test access code error)
                else:
                    return Response(
                        {
                            "error": "This room is not linked to the test session you are trying to access."
                        },
                        status=status.HTTP_400_BAD_REQUEST,
                    )
            # unsupervised test
            else:
                # making sure that the we are not past the validity end date
                current_date = date.today()
                # validity end date has passed
                if current_date > room.validity_end_date:
                    return Response(
                        {"error": "The validity end date has past."},
                        status=status.HTTP_400_BAD_REQUEST,
                    )
                # respecting the validity end date
                else:
                    # creating new row in AssignedTest table
                    assigned_test = AssignedTest(
                        status_id=checked_in_status_id,
                        test_id=test_id,
                        user_id=user_id,
                        ta_user_id=test_admin_id,
                        test_access_code=test_access_code,
                        test_order_number=room.test_order_number,
                        test_session_language=None,
                        uit_invite_id=room.uit_invite_id,
                        orderless_financial_data_id=room.orderless_financial_data_id,
                        accommodation_request_id=room.accommodation_request_id,
                    )
                    assigned_test.save()

                    # deleting concerned row in UnsupervisedTestAccessCode table
                    unsupervised_test_access_code = (
                        UnsupervisedTestAccessCode.objects.get(
                            test_access_code=test_access_code
                        )
                    )
                    unsupervised_test_access_code.delete()

            # find all test sections for this test and create an assigned test section for it
            test_sections = TestSection.objects.filter(test_definition_id=test_id)
            for test_section in test_sections:
                # initializing test_section_time
                test_section_time = test_section.default_time

                # ==================== HANDLING ACCOMMODATION - ADDITIONAL TIME ====================
                # unsupervised test
                if unsupervised_test:
                    # contains accommodation configurations
                    if room.accommodation_request_id is not None:
                        # getting additional time configurations
                        additional_time_related_to_current_test_section = (
                            AdditionalTime.objects.filter(
                                accommodation_request_id=room.accommodation_request_id,
                                test_section_id=test_section.id,
                            )
                        )
                        # contains related additional time
                        if additional_time_related_to_current_test_section:
                            # updating test_section_time value
                            test_section_time = (
                                additional_time_related_to_current_test_section.last().test_section_time
                            )
                # supervised test
                else:
                    # associated to a user accommodation file
                    if assigned_test.user_accommodation_file_id is not None:
                        # checking if additional time has been set for current test section
                        temp_additional_time = UserAccommodationFileTestTime.objects.filter(
                            user_accommodation_file_id=assigned_test.user_accommodation_file_id,
                            test_section_id=test_section.id,
                        )
                        if temp_additional_time:
                            # setting respective time
                            test_section_time = (
                                temp_additional_time.last().test_section_time
                            )
                # ==================== HANDLING ACCOMMODATION - ADDITIONAL TIME (END) ====================

                # creating new assigned test section entry
                assigned_test_section = AssignedTestSection(
                    test_section=test_section,
                    assigned_test=assigned_test,
                    test_section_time=test_section_time,
                )
                assigned_test_section.save()

            # deleting test access code only if the specified test access code has no assigned ta username (meaning that this is a unsupervised test)
            if room.ta_user_id is None:
                TestAccessCode.objects.get(test_access_code=test_access_code).delete()

    return Response(status=status.HTTP_200_OK)


def has_test_for_test_access_code_already_been_assigned(test_access_code, user_id):
    return (
        AssignedTest.objects.filter(
            user_id=user_id,
            test_access_code=test_access_code,
        ).count()
        > 0
    )
