from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.test_access_code import (
    populate_test_access_code_table,
    delete_test_access_code_row,
    get_active_test_access_codes,
    get_all_active_test_access_codes,
    get_found_active_test_access_codes,
)
from backend.api_permissions.role_based_api_permissions import (
    HasTestAdminPermission,
    HasSystemAdminPermission,
)


class GetNewTestAccessCode(APIView):
    def post(self, request):
        return populate_test_access_code_table(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


class DeleteTestAccessCode(APIView):
    permission_classes = (
        permissions.IsAuthenticated & HasSystemAdminPermission | HasTestAdminPermission,
    )

    def post(self, request):
        return delete_test_access_code_row(request)


class GetActiveTestAccessCodes(APIView):
    def get(self, request):
        return get_active_test_access_codes(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


class GetAllActiveTestAccessCodes(APIView):
    def get(self, request):
        return get_all_active_test_access_codes(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


class GetAllFoundActiveTestAccessCodes(APIView):
    def get(self, request):
        return get_found_active_test_access_codes(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]
