from rest_framework import permissions
from rest_framework.views import APIView
from backend.views.test_skill_type import get_test_skill_types_list
from backend.views.test_skill_sub_type import get_test_skill_sub_types_list


# getting test skill types list
class GetTestSkillTypesList(APIView):
    def get(self, request):
        return get_test_skill_types_list(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# getting test skill sub-types list
class GetTestSkillSubTypesList(APIView):
    def get(self, request):
        return get_test_skill_sub_types_list(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
