from operator import and_, itemgetter
from functools import reduce
from urllib import parse
import random
import string
import datetime
import pytz
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q
from cms.cms_models.orderless_test_permissions_model import OrderlessTestPermissions
from cms.views.utils import get_needed_parameters
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_permissions_model import TestPermissions
from backend.views.utils import get_user_info_from_jwt_token
from backend.custom_models.test_access_code_model import TestAccessCode
from backend.custom_models.unsupervised_test_access_code_model import (
    UnsupervisedTestAccessCode,
)
from backend.serializers.test_access_code_serializer import (
    TestAccessCodeSerializer,
)
from backend.custom_models.test_center_test_session_officers import (
    TestCenterTestSessionOfficers,
)
from backend.custom_models.test_center_test_session_data import (
    TestCenterTestSessionData,
)
from backend.custom_models.test_center_test_sessions import TestCenterTestSessions
from user_management.views.utils import CustomPagination
from user_management.user_management_models.ta_extended_profile_models import (
    TaExtendedProfile,
)
from db_views.db_view_models.bo_test_access_codes_vw import BoTestAccessCodesVW
from db_views.serializers.bo_test_access_codes_serializers import (
    BoTestAccessCodesViewSerializer,
)


# get a new randomly generated room number
def generate_random_test_access_code():
    allowed_chars = string.ascii_uppercase + string.digits
    random_test_access_code = "".join(random.choice(allowed_chars) for i in range(10))
    return random_test_access_code


# check if test access code already exists in TestAccessCode or UnsupervisedTestAccessCode
def access_code_already_exists(random_code):
    if TestAccessCode.objects.filter(test_access_code=random_code).count() > 0:
        return True
    elif (
        UnsupervisedTestAccessCode.objects.filter(test_access_code=random_code).count()
        > 0
    ):
        return True
    else:
        return False


# populate the room number table with the needed parameters, which contains the generated room number
def populate_test_access_code_table(request):
    user_info = get_user_info_from_jwt_token(request)
    # making sure that we have the needed parameters
    success, parameters = get_needed_parameters(
        ["test_session_id", "test_id", "test_session_officers"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_session_id, test_id, test_session_officers = itemgetter(
        "test_session_id", "test_id", "test_session_officers"
    )(parameters)

    try:
        # making sure that the TA has valid test permissions at the time of the test session before generating a new TAC
        # initializing skip_validation_logic
        skip_validation_logic = False
        # getting related ta extended profile data
        related_ta_extended_profile_data = TaExtendedProfile.objects.filter(
            user_id=user_info["user_id"]
        )
        # if there is an existing TA extended profile
        if related_ta_extended_profile_data:
            # getting related test data
            related_test_data = TestDefinition.objects.get(id=test_id)
            # checking if there is an existing orderless test permission related to that user + test
            if OrderlessTestPermissions.objects.filter(
                ta_extended_profile_id=related_ta_extended_profile_data.last().id,
                parent_code=related_test_data.parent_code,
                test_code=related_test_data.test_code,
            ):
                # setting skip_validation_logic to True
                skip_validation_logic = True
        # if at this point, we still need to do some validation
        if not skip_validation_logic:
            # initializing validation_error
            validation_error = True
            # getting related test session data
            related_test_session = TestCenterTestSessions.objects.get(
                id=test_session_id
            )
            related_test_session_data = TestCenterTestSessionData.objects.get(
                id=related_test_session.test_session_data_id
            )
            # getting related test permissions
            related_test_permissions = TestPermissions.objects.filter(
                user_id=user_info["user_id"], test_id=test_id
            )
            # looping in related_test_permissions
            for test_permission in related_test_permissions:
                # checking if the expiry date of the test permission of current iteration is past the date of the respective test session (invalid)
                if test_permission.expiry_date >= related_test_session_data.date:
                    # setting validation_error to False
                    validation_error = False
                    # breaking the loop
                    break

            # if there is a validation error
            if validation_error:
                return Response(
                    {
                        "error": "end time of respective test session is past all related test permission expiry dates"
                    },
                    status=status.HTTP_405_METHOD_NOT_ALLOWED,
                )

        # call the function that generates the random room number
        random_test_access_code = generate_random_test_access_code()
        while access_code_already_exists(random_test_access_code):
            random_test_access_code = generate_random_test_access_code()

        # creating new entry in TestAccessCode table
        new_test_access_code = TestAccessCode.objects.create(
            test_access_code=random_test_access_code,
            ta_user_id=user_info["user_id"],
            test_session_id=test_session_id,
            test=TestDefinition.objects.get(id=test_id),
            created_date=datetime.datetime.now(pytz.utc),
        )

        # creating new entries in TestCenterTestSessionOfficers
        # looping in test_session_officers
        for test_session_officer_id in test_session_officers:
            TestCenterTestSessionOfficers.objects.create(
                test_session_id=test_session_id,
                test_administrator_id=test_session_officer_id,
                test_access_code_id=new_test_access_code.id,
            )

        # return test access code
        return Response(random_test_access_code)

    # should never happen
    except TestDefinition.DoesNotExist:
        return Response(
            {"error": "no test found based on provided parameters"},
            status=status.HTTP_404_NOT_FOUND,
        )
    # should never happen
    except TestCenterTestSessions.DoesNotExist:
        return Response(
            {"error": "no test session found based on provided parameters"},
            status=status.HTTP_404_NOT_FOUND,
        )
    # should never happen
    except TestCenterTestSessionData.DoesNotExist:
        return Response(
            {"error": "no test session data found based on provided parameters"},
            status=status.HTTP_404_NOT_FOUND,
        )


def delete_test_access_code_row(request):
    # making sure that we have the needed parameters
    success, parameters = get_needed_parameters(["test_access_code"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_access_code = itemgetter("test_access_code")(parameters)

    # getting test access code
    test_access_codes = TestAccessCode.objects.filter(test_access_code=test_access_code)

    # test access code found
    if test_access_codes:
        # deleting test access code
        for test_access_code_data in test_access_codes:
            # deleting related test session officers
            test_session_officers = TestCenterTestSessionOfficers.objects.filter(
                test_session_id=test_access_code_data.test_session_id
            )
            for test_session_officer in test_session_officers:
                test_session_officer.delete()
            test_access_code_data.delete()

        return Response(status=status.HTTP_200_OK)
    else:
        # getting unsupervised test access code
        unsupervised_test_access_codes = UnsupervisedTestAccessCode.objects.filter(
            test_access_code=test_access_code
        )

        # unsupervised test access code found
        if unsupervised_test_access_codes:
            # deleting unsupervised test access code
            for unsupervised_test_access_code in unsupervised_test_access_codes:
                unsupervised_test_access_code.delete()
            return Response(status=status.HTTP_200_OK)

        # no test access code (supervised/unsupervised) found at all
        else:
            return Response(
                {"error": "no test access code found"}, status=status.HTTP_404_NOT_FOUND
            )


def get_active_test_access_codes(request):
    success, parameters = get_needed_parameters(["test_session_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_session_id = itemgetter("test_session_id")(parameters)
    try:
        active_test_access_codes = TestAccessCode.objects.filter(
            test_session_id=test_session_id
        )
        serializer = TestAccessCodeSerializer(active_test_access_codes, many=True)
        return Response(serializer.data)
    # should not happen
    except TestPermissions.DoesNotExist:
        return Response(
            {
                "error": "there are no matching results between test permissions and test access codes tables"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


def get_all_active_test_access_codes(request):
    success, parameters = get_needed_parameters(["page", "page_size"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # get all test access codes
    active_test_access_codes = BoTestAccessCodesVW.objects.all()

    # getting serialized active test access codes data
    serialized_active_test_access_codes_data = BoTestAccessCodesViewSerializer(
        active_test_access_codes, many=True
    ).data

    # getting sorted serialized data by TA last_name (ascending)
    ordered_active_test_access_codes = sorted(
        serialized_active_test_access_codes_data,
        key=lambda k: k["ta_last_name"].lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_active_test_access_codes = ordered_active_test_access_codes[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_active_test_access_codes,
        len(active_test_access_codes),
        current_page,
        page_size,
    )


def get_found_active_test_access_codes(request):
    success, parameters = get_needed_parameters(
        ["keyword", "current_language", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, current_language, keyword = itemgetter(
        "page", "page_size", "current_language", "keyword"
    )(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        found_active_test_access_codes = BoTestAccessCodesVW.objects.all()
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # since there is a comma between last name and first name in active test permissions table, if the user put
        # a comma in one of the search keyword words, remove it
        keyword_without_comma = keyword.replace(",", "")
        # splitting keyword string
        split_keyword = keyword_without_comma.split()

        # if keyword contains more than one word
        if len(split_keyword) > 1:
            # search while interface is in English
            if current_language == "en":
                found_active_test_access_codes = BoTestAccessCodesVW.objects.filter(
                    reduce(
                        and_,
                        [
                            Q(test_access_code__icontains=splitted_keyword)
                            | Q(test_order_number__icontains=splitted_keyword)
                            | Q(reference_number__icontains=splitted_keyword)
                            | Q(created_date__icontains=splitted_keyword)
                            | Q(ta_first_name__icontains=splitted_keyword)
                            | Q(ta_last_name__icontains=splitted_keyword)
                            | Q(ta_goc_email__icontains=splitted_keyword)
                            | Q(en_test_name__icontains=splitted_keyword)
                            for splitted_keyword in split_keyword
                        ],
                    )
                )
            # search while interface is in French
            else:
                found_active_test_access_codes = BoTestAccessCodesVW.objects.filter(
                    reduce(
                        and_,
                        [
                            Q(test_access_code__icontains=splitted_keyword)
                            | Q(test_order_number__icontains=splitted_keyword)
                            | Q(reference_number__icontains=splitted_keyword)
                            | Q(created_date__icontains=splitted_keyword)
                            | Q(ta_first_name__icontains=splitted_keyword)
                            | Q(ta_last_name__icontains=splitted_keyword)
                            | Q(ta_goc_email__icontains=splitted_keyword)
                            | Q(fr_test_name__icontains=splitted_keyword)
                            for splitted_keyword in split_keyword
                        ],
                    )
                )
        else:
            # search while interface is in English
            if current_language == "en":
                found_active_test_access_codes = BoTestAccessCodesVW.objects.filter(
                    Q(test_access_code__icontains=keyword)
                    | Q(test_order_number__icontains=keyword)
                    | Q(reference_number__icontains=keyword)
                    | Q(created_date__icontains=keyword)
                    | Q(ta_first_name__icontains=keyword)
                    | Q(ta_last_name__icontains=keyword)
                    | Q(ta_goc_email__icontains=keyword)
                    | Q(en_test_name__icontains=keyword)
                )
            # search while interface is in French
            else:
                found_active_test_access_codes = BoTestAccessCodesVW.objects.filter(
                    Q(test_access_code__icontains=keyword)
                    | Q(test_order_number__icontains=keyword)
                    | Q(reference_number__icontains=keyword)
                    | Q(created_date__icontains=keyword)
                    | Q(ta_first_name__icontains=keyword)
                    | Q(ta_last_name__icontains=keyword)
                    | Q(ta_goc_email__icontains=keyword)
                    | Q(fr_test_name__icontains=keyword)
                )

        # no results found
        if not found_active_test_access_codes:
            return Response({"no results found"}, status=status.HTTP_200_OK)

    # getting serialized active test access codes data
    serialized_found_active_test_access_codes = BoTestAccessCodesViewSerializer(
        found_active_test_access_codes, many=True
    ).data

    # getting sorted serialized data by TA last_name (ascending)
    ordered_found_active_test_access_codes = sorted(
        serialized_found_active_test_access_codes,
        key=lambda k: k["ta_last_name"].lower(),
        reverse=False,
    )

    # only getting data for current selected page
    new_found_active_test_access_codes = ordered_found_active_test_access_codes[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_found_active_test_access_codes,
        len(found_active_test_access_codes),
        current_page,
        page_size,
    )
