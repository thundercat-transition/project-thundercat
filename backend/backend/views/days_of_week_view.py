from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.days_of_week import (
    get_days_of_week,
)


class GetDaysOfWeek(APIView):
    def get(self, request):
        return get_days_of_week(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
