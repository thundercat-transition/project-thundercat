# getting all test centers as ETTA
import json
from urllib import parse
from django.db.models import Q
from django.conf import settings
import random
import string
from datetime import date, timedelta, datetime
from rest_framework import status
from rest_framework.response import Response
from operator import itemgetter
from django.db import transaction
from backend.static.consumed_reservation_code_statuses import (
    StaticConsumedReservationCodeStatus,
)
from backend.custom_models.consumed_reservation_code_status import (
    ConsumedReservationCodeStatus,
)
from backend.views.gc_notify_view import (
    resend_already_assigned_test_to_administer,
    revoke_assigned_test_to_administer,
    send_new_assigned_test_to_administer,
    hr_triggered_withdraw,
)
from db_views.serializers.hr_coordinator_serializers import (
    AssessmentProcessResultsCandidatesReportViewSerializer,
)
from user_management.views.utils import CustomPagination
from db_views.serializers.hr_coordinator_serializers import (
    AssessmentProcessResultsViewSerializer,
)
from db_views.db_view_models.assessment_process_results_vw import (
    AssessmentProcessResultsVW,
)
from db_views.db_view_models.assessment_process_results_candidates_vw import (
    AssessmentProcessResultsCandidatesVW,
)
from db_views.db_view_models.assessment_process_results_candidates_report_vw import (
    AssessmentProcessResultsCandidatesReportVW,
)
from db_views.db_view_models.test_center_test_sessions_vw import (
    TestCenterTestSessionsVW,
)
from db_views.serializers.test_center_serializers import (
    TestCenterTestSessionsViewSerializer,
)
from user_management.user_management_models.user_models import User
from backend.views.utils import get_user_info_from_jwt_token
from backend.custom_models.assessment_process import AssessmentProcess
from backend.custom_models.reservation_codes import ReservationCodes
from backend.custom_models.assessment_process_default_test_specs import (
    AssessmentProcessDefaultTestSpecs,
)
from backend.custom_models.assessment_process_assigned_test_specs import (
    AssessmentProcessAssignedTestSpecs,
)
from backend.celery.tasks import (
    test_session_cancellation_based_on_assessment_process_data_updates,
    update_active_assessment_process_data,
    send_reservation_code_email_celery_task,
)
from backend.custom_models.consumed_reservation_codes import ConsumedReservationCodes
from cms.views.utils import get_needed_parameters, get_optional_parameters
from db_views.db_view_models.assessment_process_vw import AssessmentProcessVW
from db_views.serializers.hr_coordinator_serializers import (
    AssessmentProcessAssignedTestSpecsViewSerializer,
    AssessmentProcessViewSerializer,
    AssessmentProcessResultsCandidatesViewSerializer,
)
from db_views.db_view_models.assessment_process_assigned_test_specs_vw import (
    AssessmentProcessAssignedTestSpecsVW,
)
from backend.views.virtual_meetings_views import cancel_virtual_meeting


def get_available_test_sessions_for_specific_test_to_administer(request):
    success, parameters = get_needed_parameters(
        [
            "process_duration",
            "test_skill_type_id",
            "allow_booking_external_tc",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    (
        process_duration,
        test_skill_type_id,
        allow_booking_external_tc,
    ) = itemgetter(
        "process_duration",
        "test_skill_type_id",
        "allow_booking_external_tc",
    )(parameters)

    optional_parameters = get_optional_parameters(
        ["test_skill_sub_type_id", "department_id", "assessment_process_id"], request
    )

    # initializing comparison_date (today's date)
    comparison_date = date.today()

    # assessment_process_id is provided
    if (
        optional_parameters["assessment_process_id"] != "null"
        and optional_parameters["assessment_process_id"] is not None
    ):
        # getting sent date
        respective_sent_date = AssessmentProcessVW.objects.get(
            id=optional_parameters["assessment_process_id"]
        ).sent_date
        # making sure that the sent date is defined
        if respective_sent_date is not None:
            comparison_date = datetime.strptime(respective_sent_date, "%Y-%m-%d")

    # formatting duration as a date (current date + duration in days)
    formatted_duration_date = comparison_date + timedelta(days=int(process_duration))

    # allow booking external TC is set to false
    if allow_booking_external_tc == "false":
        # need to consider department ID for the filter
        # getting respective available test sessions
        respective_test_sessions = TestCenterTestSessionsVW.objects.filter(
            date__lte=formatted_duration_date,
            test_skill_type_id=test_skill_type_id,
            test_skill_sub_type_id=optional_parameters["test_skill_sub_type_id"],
            department_id=optional_parameters["department_id"],
        )
    # allow booking external TC is set to true
    else:
        # getting respective available test sessions (that are not for accommodations)
        respective_test_sessions = TestCenterTestSessionsVW.objects.filter(
            date__lte=formatted_duration_date,
            test_skill_type_id=test_skill_type_id,
            test_skill_sub_type_id=optional_parameters["test_skill_sub_type_id"],
            user_accommodation_file_id__isnull=True,
        )

    # serializing data
    serialized_data = TestCenterTestSessionsViewSerializer(
        respective_test_sessions, many=True
    ).data

    return Response(serialized_data)


def create_assessment_process(request):
    user_info = get_user_info_from_jwt_token(request)
    user = User.objects.get(id=user_info["user_id"])

    success, parameters = get_needed_parameters(
        [
            "reference_number",
            "department_id",
            "duration",
            "allow_booking_external_tc",
            "contact_email_for_candidates",
            "tests_to_administer",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    (
        reference_number,
        department_id,
        duration,
        allow_booking_external_tc,
        contact_email_for_candidates,
        tests_to_administer,
    ) = itemgetter(
        "reference_number",
        "department_id",
        "duration",
        "allow_booking_external_tc",
        "contact_email_for_candidates",
        "tests_to_administer",
    )(
        parameters
    )

    optional_parameters = get_optional_parameters(
        ["default_billing_contact_id"], request
    )

    try:
        with transaction.atomic():
            # creating new assessment process entry
            assessment_process = AssessmentProcess.objects.create(
                user=user,
                reference_number=reference_number,
                department_id=department_id,
                duration=duration,
                # closing date will only be defined properly when the request will be considered as "sent"
                closing_date=None,
                allow_booking_external_tc=allow_booking_external_tc,
                default_billing_contact_id=optional_parameters[
                    "default_billing_contact_id"
                ],
                contact_email_for_candidates=contact_email_for_candidates,
            )

            # looping in tests_to_administer
            for test_specs in tests_to_administer:
                # creating new assessment process default test specs entry
                AssessmentProcessDefaultTestSpecs.objects.create(
                    assessment_process_id=assessment_process.id,
                    test_skill_type_id=test_specs["test_skill_type_id"],
                    test_skill_sub_type_id=test_specs["test_skill_sub_type_id"],
                    reason_for_testing_id=test_specs["default_reason_for_testing_id"],
                    level_required=test_specs["default_level_required"],
                )

    except Exception as error:
        # printing error type
        print(type(error))
        return Response(
            {
                "error": "Something happened during the create assessment process process"
            },
            status=status.HTTP_409_CONFLICT,
        )

    return Response(status=status.HTTP_201_CREATED)


def delete_assessment_process(request):
    success, parameters = get_needed_parameters(
        [
            "assessment_process_id",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    assessment_process_id = itemgetter(
        "assessment_process_id",
    )(parameters)

    # Clean up the assessment process and everything related to it
    AssessmentProcessDefaultTestSpecs.objects.filter(
        assessment_process_id=assessment_process_id
    ).delete()

    AssessmentProcessAssignedTestSpecs.objects.filter(
        assessment_process_id=assessment_process_id
    ).delete()

    AssessmentProcess.objects.filter(id=assessment_process_id).delete()

    return Response(status=status.HTTP_200_OK)


def get_assessment_process_reference_numbers(request):
    user_info = get_user_info_from_jwt_token(request)
    user = User.objects.get(id=user_info["user_id"])

    success, parameters = get_needed_parameters(
        [
            "only_get_sent_request",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    only_get_sent_request = itemgetter(
        "only_get_sent_request",
    )(parameters)

    # only getting sent requests (active processes)
    if only_get_sent_request == "true":
        # getting current_date
        current_date = date.today()
        # getting related ids/reference numbers based on user and where the request has been sent but closing date has not passed yet
        related_reference_numbers = AssessmentProcess.objects.filter(
            user=user, request_sent=True, closing_date__gte=current_date
        ).values("id", "reference_number")
    else:
        # getting related ids/reference numbers based on user and where the request has not been sent yet
        related_reference_numbers = AssessmentProcess.objects.filter(
            user=user, request_sent=False
        ).values("id", "reference_number")

    return Response(related_reference_numbers)


def get_assessment_process_data(request):
    user_info = get_user_info_from_jwt_token(request)
    user = User.objects.get(id=user_info["user_id"])

    success, parameters = get_needed_parameters(
        [
            "assessment_process_id",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    assessment_process_id = itemgetter(
        "assessment_process_id",
    )(parameters)

    try:
        # getting respective assessment process data based on provided assessment process ID
        assessment_process_data = AssessmentProcessVW.objects.get(
            user_id=user.id, id=assessment_process_id
        )

        # getting serialized data
        serialized_data = AssessmentProcessViewSerializer(
            assessment_process_data, many=False
        ).data
        return Response(serialized_data)

    except AssessmentProcessVW.DoesNotExist:
        return Response(
            {
                "error": "No assessment process has been found based on provided assessment process ID"
            },
            status=status.HTTP_404_NOT_FOUND,
        )


def assessment_process_assign_candidate(request):
    assessment_process_array = json.loads(request.body)

    # looping in assessment_process_array
    for index, assessment_process_data in enumerate(assessment_process_array):
        # first iteration
        if index == 0:
            # check if provided email has already been used in this process
            data_found_with_same_email = (
                AssessmentProcessAssignedTestSpecs.objects.filter(
                    assessment_process_id=assessment_process_data[
                        "assessment_process_id"
                    ],
                    email=assessment_process_data["email"],
                )
            )
            # if data has been found with the same email
            if data_found_with_same_email:
                return Response(
                    {
                        "error:"
                        "The provided email has already been used in this process"
                    },
                    status=status.HTTP_409_CONFLICT,
                )
        # creating new entry in assessment process assigned test specs
        AssessmentProcessAssignedTestSpecs.objects.create(
            first_name=assessment_process_data["first_name"],
            last_name=assessment_process_data["last_name"],
            email=assessment_process_data["email"],
            assessment_process_id=assessment_process_data["assessment_process_id"],
            test_skill_type_id=assessment_process_data["test_skill_type_id"],
            test_skill_sub_type_id=assessment_process_data["test_skill_sub_type_id"],
            reason_for_testing_id=assessment_process_data["reason_for_testing_id"],
            level_required=assessment_process_data["level_required"],
            billing_contact_id=assessment_process_data["billing_contact_id"],
        )

    return Response(status=status.HTTP_200_OK)


def get_all_assessment_process_results_data(request):
    user_info = get_user_info_from_jwt_token(request)

    parameters = get_optional_parameters(["page", "page_size"], request)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # getting respective assessment process data based on provided reference number
    assessment_process_results_data = AssessmentProcessResultsVW.objects.filter(
        user_id=user_info["user_id"],
    )

    # getting serialized data
    serialized_data = AssessmentProcessResultsViewSerializer(
        assessment_process_results_data, many=True
    ).data

    # getting sorted serialized data by closing date (DESC) and reference number (ASC)
    ordered_assessment_process_results = sorted(
        sorted(serialized_data, key=lambda k: k["reference_number"]),
        key=lambda k: k["closing_date"],
        reverse=True,
    )

    # page and page_size are provided
    if page is not None and page_size is not None:
        # initializing current_page and page_size
        current_page = int(page)
        page_size = int(page_size)

        # pagination class
        pagination_class = CustomPagination
        paginator = pagination_class()

        # only getting data for current selected page
        assessment_process_results = ordered_assessment_process_results[
            (current_page - 1) * page_size : current_page * page_size
        ]

        # final data using pagination library
        final_object = paginator.get_paginated_response(
            assessment_process_results,
            len(ordered_assessment_process_results),
            current_page,
            page_size,
        )

        return final_object

    # page and page_size are NOT provided
    else:
        return Response(ordered_assessment_process_results)


def get_found_assessment_process_results_data(request):
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(
        ["keyword", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size, keyword = itemgetter("page", "page_size", "keyword")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        found_assessment_process_results = AssessmentProcessResultsVW.objects.filter(
            user_id=user_info["user_id"]
        )

    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        found_assessment_process_results = AssessmentProcessResultsVW.objects.filter(
            Q(user_id=user_info["user_id"])
            & (
                Q(reference_number__icontains=keyword)
                | Q(closing_date__icontains=keyword)
                | Q(number_of_candidates__icontains=keyword)
                | Q(number_of_tests_taken__icontains=keyword)
            )
        )

    # serializing data
    serialized_data = AssessmentProcessResultsViewSerializer(
        found_assessment_process_results, many=True
    ).data

    # getting sorted serialized data by reference_number
    ordered_assessment_process_results = sorted(
        sorted(serialized_data, key=lambda k: k["reference_number"]),
        key=lambda k: k["closing_date"],
        reverse=True,
    )
    # only getting data for current selected page
    current_assessment_process_results = ordered_assessment_process_results[
        (current_page - 1) * page_size : current_page * page_size
    ]

    # final data using pagination library
    final_object = paginator.get_paginated_response(
        current_assessment_process_results,
        len(ordered_assessment_process_results),
        current_page,
        page_size,
    )

    return final_object


def get_assessment_process_assigned_candidates(request):
    success, parameters = get_needed_parameters(
        [
            "assessment_process_id",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    assessment_process_id = itemgetter(
        "assessment_process_id",
    )(parameters)

    # getting respective assessment process assigned candidates data based on provided assessment process ID
    assessment_process_assigned_candidates_data = (
        AssessmentProcessAssignedTestSpecsVW.objects.filter(
            assessment_process_id=assessment_process_id
        )
    )

    # getting serialized data
    serialized_data = AssessmentProcessAssignedTestSpecsViewSerializer(
        assessment_process_assigned_candidates_data, many=True
    ).data

    # getting sorted serialized data by closing date (DESC) and reference number (ASC)
    ordered_assessment_process_assigned_candidates = sorted(
        serialized_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )

    return Response(ordered_assessment_process_assigned_candidates)


def get_assessment_process_results_candidates_report(request):
    success, parameters = get_needed_parameters(
        ["assessment_process_id"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    assessment_process_id = itemgetter("assessment_process_id")(parameters)

    # getting respective assessment process assigned candidates data based on provided assessment process ID
    assessment_process_results_candidates_data = (
        AssessmentProcessResultsCandidatesReportVW.objects.filter(
            assessment_process_id=assessment_process_id
        )
    )

    # getting serialized data
    serialized_data = AssessmentProcessResultsCandidatesReportViewSerializer(
        assessment_process_results_candidates_data, many=True
    ).data

    # getting sorted serialized data by last name
    ordered_assessment_process_results_candidates_report = sorted(
        serialized_data,
        key=lambda k: k["assessment_process_candidate_last_name"].lower(),
        reverse=False,
    )

    return Response(ordered_assessment_process_results_candidates_report)


def get_assessment_process_results_candidates(request):
    success, parameters = get_needed_parameters(
        ["assessment_process_id"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    assessment_process_id = itemgetter("assessment_process_id")(parameters)

    # getting respective assessment process assigned candidates data based on provided assessment process ID
    assessment_process_results_candidates_data = (
        AssessmentProcessResultsCandidatesVW.objects.filter(
            assessment_process_id=assessment_process_id
        )
    )

    # getting serialized data
    serialized_data = AssessmentProcessResultsCandidatesViewSerializer(
        assessment_process_results_candidates_data, many=True
    ).data

    # getting sorted serialized data by last name
    ordered_assessment_process_results_candidates = sorted(
        serialized_data,
        key=lambda k: k["assessment_process_candidate_last_name"].lower(),
        reverse=False,
    )

    return Response(ordered_assessment_process_results_candidates)


def edit_assessment_process_assigned_candidate(request):
    body = json.loads(request.body)

    # first step is to make sure that the email does not already exist in this assessment process
    # need to loop in assessment_process_data_array AND in already_assigned_assessment_process_data_array first
    # looping in assessment_process_data_array
    for index, assessment_process_data in enumerate(
        body["assessment_process_data_array"]
    ):
        # first iteration
        if index == 0:
            # check if provided email has already been used in this process (excluding current test specs IDs)
            data_found_with_same_email = (
                AssessmentProcessAssignedTestSpecs.objects.filter(
                    assessment_process_id=assessment_process_data[
                        "assessment_process_id"
                    ],
                    email=assessment_process_data["email"],
                ).exclude(
                    id__in=body["assessment_process_data_array"][0]["ids_to_be_updated"]
                )
            )
            # if data has been found with the same email
            if data_found_with_same_email:
                return Response(
                    {
                        "error:"
                        "The provided email has already been used in this process"
                    },
                    status=status.HTTP_409_CONFLICT,
                )

    # called from active processes
    if body["called_from_active_processes"]:
        # looping in already_assigned_assessment_process_data_array
        for index, already_assigned_assessment_process_data in enumerate(
            body["already_assigned_assessment_process_data_array"]
        ):
            # first iteration
            if index == 0:
                # check if provided email has already been used in this process (excluding current test specs IDs)
                data_found_with_same_email = (
                    AssessmentProcessAssignedTestSpecs.objects.filter(
                        assessment_process_id=already_assigned_assessment_process_data[
                            "assessment_process_id"
                        ],
                        email=already_assigned_assessment_process_data["email"],
                    ).exclude(
                        id__in=body["already_assigned_assessment_process_data_array"][
                            0
                        ]["ids_to_be_updated"]
                    )
                )
                # if data has been found with the same email
                if data_found_with_same_email:
                    return Response(
                        {
                            "error:"
                            "The provided email has already been used in this process"
                        },
                        status=status.HTTP_409_CONFLICT,
                    )

    # if we're getting there, that means that the email is unique in this specific assessment process
    # looping in assessment_process_data_array
    for index, assessment_process_data in enumerate(
        body["assessment_process_data_array"]
    ):
        try:
            # first iteration
            if index == 0:
                print("data_found_with_same_email 1: ", data_found_with_same_email)
                # if data has been found with the same email
                if data_found_with_same_email:
                    return Response(
                        {
                            "error:"
                            "The provided email has already been used in this process"
                        },
                        status=status.HTTP_409_CONFLICT,
                    )
                # no duplicate email found + not called from active processes ==> delete related existing entries
                if not body["called_from_active_processes"]:
                    # getting related assessment process assigned test specs
                    related_assessment_process_assigned_test_specs = (
                        AssessmentProcessAssignedTestSpecs.objects.filter(
                            id__in=body["assessment_process_data_array"][0][
                                "ids_to_be_updated"
                            ],
                            assessment_process_id=body["assessment_process_data_array"][
                                0
                            ]["assessment_process_id"],
                        )
                    )
                    # deleting all entries
                    for test in related_assessment_process_assigned_test_specs:
                        test.delete()

            # creating new entry in assessment process assigned test specs
            new_assessment_process_assigned_test_specs = (
                AssessmentProcessAssignedTestSpecs.objects.create(
                    first_name=assessment_process_data["first_name"],
                    last_name=assessment_process_data["last_name"],
                    email=assessment_process_data["email"],
                    assessment_process_id=assessment_process_data[
                        "assessment_process_id"
                    ],
                    test_skill_type_id=assessment_process_data["test_skill_type_id"],
                    test_skill_sub_type_id=assessment_process_data[
                        "test_skill_sub_type_id"
                    ],
                    reason_for_testing_id=assessment_process_data[
                        "reason_for_testing_id"
                    ],
                    level_required=assessment_process_data["level_required"],
                    billing_contact_id=assessment_process_data["billing_contact_id"],
                )
            )

            # called from active processes (need to send email)
            if body["called_from_active_processes"]:
                # generating new reservation code
                generated_reservation_code = generate_random_reservation_code()
                while reservation_code_already_exists(generated_reservation_code):
                    generated_reservation_code = generate_random_reservation_code()
                # creating new reservation code
                new_reservation_code_data = ReservationCodes.objects.create(
                    assessment_process_assigned_test_specs_id=new_assessment_process_assigned_test_specs.id,
                    reservation_code=generated_reservation_code,
                    created_date=date.today(),
                )
                # getting most updated assessment process assigned test specs data
                data = AssessmentProcessAssignedTestSpecsVW.objects.get(
                    id=new_assessment_process_assigned_test_specs.id
                )
                serialized_data = AssessmentProcessAssignedTestSpecsViewSerializer(
                    data, many=False
                ).data

                # adding site protocal/domain to the final object
                serialized_data["site_protocol"] = settings.SITE_PROTOCOL
                serialized_data["site_domain"] = settings.SITE_DOMAIN
                serialized_data["reservation_code"] = (
                    new_reservation_code_data.reservation_code
                )
                send_new_assigned_test_to_administer(serialized_data)

        except AssessmentProcessAssignedTestSpecs.DoesNotExist:
            return Response(
                {
                    "error": "No assessment process assigned test specs has been found based on provided data (2)"
                },
                status=status.HTTP_404_NOT_FOUND,
            )

    # called from active processes
    if body["called_from_active_processes"]:
        # looping in already_assigned_assessment_process_data_array
        for index, already_assigned_assessment_process_data in enumerate(
            body["already_assigned_assessment_process_data_array"]
        ):
            try:
                # getting respective assigned test specs data
                respective_assigned_test_specs_data = (
                    AssessmentProcessAssignedTestSpecs.objects.get(
                        id=already_assigned_assessment_process_data[
                            "assessment_process_assigned_test_specs_id"
                        ]
                    )
                )
                # updating needed data
                respective_assigned_test_specs_data.first_name = (
                    already_assigned_assessment_process_data["first_name"]
                )
                respective_assigned_test_specs_data.last_name = (
                    already_assigned_assessment_process_data["last_name"]
                )
                respective_assigned_test_specs_data.email = (
                    already_assigned_assessment_process_data["email"]
                )
                respective_assigned_test_specs_data.billing_contact_id = (
                    already_assigned_assessment_process_data["billing_contact_id"]
                )
                respective_assigned_test_specs_data.reason_for_testing_id = (
                    already_assigned_assessment_process_data["reason_for_testing_id"]
                )
                respective_assigned_test_specs_data.level_required = (
                    already_assigned_assessment_process_data["level_required"]
                )
                # saving data
                respective_assigned_test_specs_data.save()
            except AssessmentProcessAssignedTestSpecs.DoesNotExist:
                return Response(
                    {
                        "error": "No assessment process assigned test specs has been found based on provided data (1)"
                    },
                    status=status.HTTP_404_NOT_FOUND,
                )
            # email has been updated + send_email is set to True (logic handled in the frontend)
            if (
                body["email_updated"]
                and already_assigned_assessment_process_data["send_email"]
            ):
                # resend emails of initial assigned tests to administer
                # getting most updated assessment process assigned test specs data
                data = AssessmentProcessAssignedTestSpecsVW.objects.get(
                    id=already_assigned_assessment_process_data[
                        "assessment_process_assigned_test_specs_id"
                    ]
                )
                serialized_data = AssessmentProcessAssignedTestSpecsViewSerializer(
                    data, many=False
                ).data

                # adding site protocal/domain to the final object
                serialized_data["site_protocol"] = settings.SITE_PROTOCOL
                serialized_data["site_domain"] = settings.SITE_DOMAIN
                # adding reservation code to the final object
                reservation_code_data = ReservationCodes.history.filter(
                    assessment_process_assigned_test_specs_id=serialized_data["id"],
                    history_type="+",
                )
                # making sure that the reservation code exist (should always exist)
                reservation_code = ""
                if reservation_code_data:
                    reservation_code = reservation_code_data.last().reservation_code
                serialized_data["reservation_code"] = reservation_code
                resend_already_assigned_test_to_administer(serialized_data)

    return Response(status=status.HTTP_200_OK)


def delete_assessment_process_assigned_candidate(request):
    assessment_process_assigned_test_specs_data = json.loads(request.body)

    # getting respective assessment process assigned candidates data based on first name, last name and email
    assessment_process_assigned_candidates_data = (
        AssessmentProcessAssignedTestSpecs.objects.filter(
            assessment_process_id=assessment_process_assigned_test_specs_data[
                "assessment_process_id"
            ],
            first_name=assessment_process_assigned_test_specs_data["first_name"],
            last_name=assessment_process_assigned_test_specs_data["last_name"],
            email=assessment_process_assigned_test_specs_data["email"],
        )
    )

    # deleting all respective entries
    for assigned_test_specs in assessment_process_assigned_candidates_data:
        assigned_test_specs.delete()

    return Response(status=status.HTTP_200_OK)


# get a new randomly generated reservation code
def generate_random_reservation_code():
    allowed_chars = string.ascii_uppercase + string.digits
    generated_reservation_code = "RES-" + "".join(
        random.choice(allowed_chars) for i in range(10)
    )
    return generated_reservation_code


# check if reservation code already exists in ReservationCodes
def reservation_code_already_exists(generated_reservation_code):
    if (
        ReservationCodes.objects.filter(
            reservation_code=generated_reservation_code
        ).count()
        > 0
    ):
        return True
    else:
        return False


def send_assessment_process_assigned_candidates_request(request):
    success, parameters = get_needed_parameters(
        [
            "assessment_process_id",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    assessment_process_id = itemgetter(
        "assessment_process_id",
    )(parameters)

    try:
        # initializing arrays
        email_data_array = []
        reservation_codes = []

        # getting related assessment_process_assigned_test_specs
        related_assessment_process_assigned_test_specs = (
            AssessmentProcessAssignedTestSpecsVW.objects.filter(
                assessment_process_id=assessment_process_id
            )
        )
        # looping in related_assessment_process_assigned_test_specs
        for assigned_test_specs in related_assessment_process_assigned_test_specs:
            # generating new reservation code
            generated_reservation_code = generate_random_reservation_code()
            while reservation_code_already_exists(generated_reservation_code):
                generated_reservation_code = generate_random_reservation_code()

            # creating new entry in ReservationCodes table
            new_reservation_code_data = ReservationCodes.objects.create(
                assessment_process_assigned_test_specs_id=assigned_test_specs.id,
                reservation_code=generated_reservation_code,
                created_date=date.today(),
            )
            # populating email_data_array
            email_data_array.append(
                {
                    "reservation_code": new_reservation_code_data.reservation_code,
                    "assessment_process_assigned_test_specs_data": AssessmentProcessAssignedTestSpecsViewSerializer(
                        assigned_test_specs, many=False
                    ).data,
                }
            )
            reservation_codes.append(new_reservation_code_data.reservation_code)

        # getting respective assessment process
        assessment_process = AssessmentProcess.objects.get(id=assessment_process_id)
        # updating the request_sent flag + closing date
        assessment_process.request_sent = True
        # calculating closing date
        current_date = date.today()
        closing_date = current_date + timedelta(days=assessment_process.duration)
        assessment_process.closing_date = closing_date
        # saving data
        assessment_process.save()

        # sending respective emails
        # calling celery task to handle the send reservation code emails action
        # when using this celery task to send the emails, the data creation part of the function is called and return the response 200 quiclky
        # so the emails are sent in the background while the user continues to navigate in the UI
        # NOTE this functiion will eventually call either the internal email server or GC Notify
        send_reservation_code_email_celery_task.delay(email_data_array)

        return Response(
            {"reservation_codes": reservation_codes}, status=status.HTTP_200_OK
        )

    except AssessmentProcess.DoesNotExist:
        return Response(
            {"error": "No assessment process has been found based on provided ID"},
            status=status.HTTP_404_NOT_FOUND,
        )


def update_non_sent_assessment_process_data(request):
    assessment_process_data = json.loads(request.body)

    try:
        # getting related assessment process data
        related_assessment_process_data = AssessmentProcess.objects.get(
            id=assessment_process_data["id"]
        )

        # updating needed data
        related_assessment_process_data.department_id = assessment_process_data[
            "department_id"
        ]
        related_assessment_process_data.duration = assessment_process_data["duration"]
        related_assessment_process_data.allow_booking_external_tc = (
            assessment_process_data["allow_booking_external_tc"]
        )
        related_assessment_process_data.allow_last_minute_cancellation_tc = (
            assessment_process_data["allow_last_minute_cancellation_tc"]
        )
        related_assessment_process_data.contact_email_for_candidates = (
            assessment_process_data["contact_email_for_candidates"]
        )
        related_assessment_process_data.save()

        return Response(status=status.HTTP_200_OK)

    except AssessmentProcess.DoesNotExist:
        return Response(
            {"error": "No assessment process has been found based on provided ID"},
            status=status.HTTP_404_NOT_FOUND,
        )


def send_assessment_process_updates_request(request):
    success, parameters = get_needed_parameters(
        ["assessment_process_id", "duration", "contact_email_for_candidates"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    assessment_process_id, duration, contact_email_for_candidates = itemgetter(
        "assessment_process_id", "duration", "contact_email_for_candidates"
    )(parameters)

    try:
        # initializing needed variables
        cancellation_email_data_array = []
        updates_email_data_array = []

        # getting related assessment process data
        related_assessment_process_data = AssessmentProcess.objects.get(
            id=assessment_process_id
        )

        # calculating difference between the provided duration and the old one (the one in the DB)
        difference_between_durations = int(duration) - int(
            related_assessment_process_data.duration
        )

        # calculating new closing date
        calculated_new_closing_date = (
            related_assessment_process_data.closing_date
            + timedelta(days=difference_between_durations)
        )

        # getting related assessment_process_assigned_test_specs
        related_assessment_process_assigned_test_specs = (
            AssessmentProcessAssignedTestSpecsVW.objects.filter(
                assessment_process_id=assessment_process_id
            )
        )
        # looping in related_assessment_process_assigned_test_specs
        for assigned_test_specs in related_assessment_process_assigned_test_specs:
            # getting related reservation code
            related_reservation_code = ReservationCodes.history.filter(
                assessment_process_assigned_test_specs_id=assigned_test_specs.id,
                history_type="+",
            )

            # reservation code should already exists, but adding a verification here just in case of data issues (to not break the whole for loop)
            if not related_reservation_code:
                continue

            # getting latest entry in the DB
            related_reservation_code = related_reservation_code.last().reservation_code

            # getting related consumed reservation code data
            related_consumed_reservation_code_data = (
                ConsumedReservationCodes.objects.filter(
                    assessment_process_assigned_test_specs_id=assigned_test_specs.id
                )
            )

            # consumed reservation code exists AND test_session_id is defined, but no assigned test ID yet
            # if assigned_test_id is defined, that means that the candidate is currently in a test session or has already done a test
            if related_consumed_reservation_code_data and (
                related_consumed_reservation_code_data.last().test_session_id
                is not None
                and related_consumed_reservation_code_data.last().assigned_test_id
                is None
            ):
                # checking if respective test session is still valid with the provided closing date (might have been updated in this call)
                respective_test_session_data = TestCenterTestSessionsVW.objects.get(
                    id=related_consumed_reservation_code_data.last().test_session_id
                )
                # test session no longer valid (need to send an cancellation email)
                if respective_test_session_data.date > calculated_new_closing_date:
                    # building final_object
                    final_object = AssessmentProcessAssignedTestSpecsViewSerializer(
                        assigned_test_specs, many=False
                    ).data
                    final_object["reservation_code"] = related_reservation_code
                    final_object["assessment_process_closing_date"] = str(
                        calculated_new_closing_date
                    )
                    final_object["contact_email_for_candidates"] = (
                        contact_email_for_candidates
                    )
                    final_object["site_protocol"] = settings.SITE_PROTOCOL
                    final_object["site_domain"] = settings.SITE_DOMAIN
                    # adding needed data to the cancellation_email_data_array
                    cancellation_email_data_array.append(final_object)

                    # getting reserved consumed reservation code status ID
                    reserved_consumed_reservation_code_id = (
                        ConsumedReservationCodeStatus.objects.get(
                            codename=StaticConsumedReservationCodeStatus.RESERVED
                        ).id
                    )

                    # removing candidate from respective test session
                    consumed_reservation_code_to_update = (
                        related_consumed_reservation_code_data.last()
                    )
                    consumed_reservation_code_to_update.test_session_id = None
                    consumed_reservation_code_to_update.status_id = (
                        reserved_consumed_reservation_code_id
                    )
                    consumed_reservation_code_to_update.save()

            # making sure that we need to send an email for the assigned_test_specs of the current iteration
            # need to send email if status is SENT (not consumed yet), RESERVED or BOOKED
            # initializing need_to_send_email
            need_to_send_email = False
            # no existing consumed reservation code
            if not related_consumed_reservation_code_data:
                need_to_send_email = True
            # existing consumed reservation code
            else:
                # checking if status is either RESERVED or BOOKED
                reserved_consumed_reservation_code_id = (
                    ConsumedReservationCodeStatus.objects.get(
                        codename=StaticConsumedReservationCodeStatus.RESERVED
                    ).id
                )
                booked_consumed_reservation_code_id = (
                    ConsumedReservationCodeStatus.objects.get(
                        codename=StaticConsumedReservationCodeStatus.BOOKED
                    ).id
                )
                if (
                    related_consumed_reservation_code_data.last().status_id
                    == reserved_consumed_reservation_code_id
                    or related_consumed_reservation_code_data.last().status_id
                    == booked_consumed_reservation_code_id
                ):
                    need_to_send_email = True

            # if we need to send an email
            if need_to_send_email:
                # building final_object
                final_object = AssessmentProcessAssignedTestSpecsViewSerializer(
                    assigned_test_specs, many=False
                ).data
                final_object["reservation_code"] = related_reservation_code
                final_object["assessment_process_closing_date"] = str(
                    calculated_new_closing_date
                )
                final_object["contact_email_for_candidates"] = (
                    contact_email_for_candidates
                )
                final_object["site_protocol"] = settings.SITE_PROTOCOL
                final_object["site_domain"] = settings.SITE_DOMAIN

                # adding needed data to the cancellation_email_data_array
                updates_email_data_array.append(final_object)

        # updating respective assessment process data
        related_assessment_process_data.duration = duration
        related_assessment_process_data.closing_date = calculated_new_closing_date
        related_assessment_process_data.contact_email_for_candidates = (
            contact_email_for_candidates
        )
        related_assessment_process_data.save()

        # sending needed emails (using celery, since multiple emails are sent at the same time here)
        update_active_assessment_process_data.delay(updates_email_data_array)
        test_session_cancellation_based_on_assessment_process_data_updates(
            cancellation_email_data_array
        )

        # getting most updated assessment process data
        most_updated_assessment_process_data = AssessmentProcessVW.objects.get(
            id=assessment_process_id
        )

        # getting serialized data
        serialized_data = AssessmentProcessViewSerializer(
            most_updated_assessment_process_data, many=False
        ).data
        return Response(serialized_data)

    except AssessmentProcess.DoesNotExist:
        return Response(
            {"error": "No assessment process has been found based on provided ID"},
            status=status.HTTP_404_NOT_FOUND,
        )

    except ReservationCodes.DoesNotExist:
        return Response(
            {
                "error": "Something wrong happened with the reservation code call in this request"
            },
            status=status.HTTP_404_NOT_FOUND,
        )

    except:
        return Response(
            {
                "error": "Something happened during the send assessment process updates request"
            },
            status=status.HTTP_404_NOT_FOUND,
        )


def invite_single_assessment_process_candidate_request(request):
    assessment_process_array = json.loads(request.body)

    # initializing arrays
    email_data_array = []

    # looping in assessment_process_array
    for index, assessment_process_data in enumerate(assessment_process_array):
        # first iteration
        if index == 0:
            # check if provided email has already been used in this process
            data_found_with_same_email = (
                AssessmentProcessAssignedTestSpecs.objects.filter(
                    assessment_process_id=assessment_process_data[
                        "assessment_process_id"
                    ],
                    email=assessment_process_data["email"],
                )
            )
            # if data has been found with the same email
            if data_found_with_same_email:
                return Response(
                    {
                        "error:"
                        "The provided email has already been used in this process"
                    },
                    status=status.HTTP_409_CONFLICT,
                )
        # creating new entry in assessment process assigned test specs
        new_assessment_process_assigned_test_specs = (
            AssessmentProcessAssignedTestSpecs.objects.create(
                first_name=assessment_process_data["first_name"],
                last_name=assessment_process_data["last_name"],
                email=assessment_process_data["email"],
                assessment_process_id=assessment_process_data["assessment_process_id"],
                test_skill_type_id=assessment_process_data["test_skill_type_id"],
                test_skill_sub_type_id=assessment_process_data[
                    "test_skill_sub_type_id"
                ],
                reason_for_testing_id=assessment_process_data["reason_for_testing_id"],
                level_required=assessment_process_data["level_required"],
                billing_contact_id=assessment_process_data["billing_contact_id"],
            )
        )

        # getting most updated new assessment process assigned test specs data (from the view)
        most_updated_new_assessment_process_assigned_test_specs_data = (
            AssessmentProcessAssignedTestSpecsVW.objects.get(
                id=new_assessment_process_assigned_test_specs.id
            )
        )

        # generating new reservation code
        generated_reservation_code = generate_random_reservation_code()
        while reservation_code_already_exists(generated_reservation_code):
            generated_reservation_code = generate_random_reservation_code()

        # creating new entry in ReservationCodes table
        new_reservation_code_data = ReservationCodes.objects.create(
            assessment_process_assigned_test_specs_id=most_updated_new_assessment_process_assigned_test_specs_data.id,
            reservation_code=generated_reservation_code,
            created_date=date.today(),
        )
        # populating email_data_array
        email_data_array.append(
            {
                "reservation_code": new_reservation_code_data.reservation_code,
                "assessment_process_assigned_test_specs_data": AssessmentProcessAssignedTestSpecsViewSerializer(
                    most_updated_new_assessment_process_assigned_test_specs_data,
                    many=False,
                ).data,
            }
        )

    # sending respective emails
    # calling celery task to handle the send reservation code emails action
    # when using this celery task to send the emails, the data creation part of the function is called and return the response 200 quiclky
    # so the emails are sent in the background while the user continues to navigate in the UI
    # NOTE this functiion will eventually call either the internal email server or GC Notify
    send_reservation_code_email_celery_task.delay(email_data_array)

    return Response(status=status.HTTP_200_OK)


def unassign_assessment_process_assigned_test_to_administer(request):
    success, parameters = get_needed_parameters(
        ["assigned_test_specs_id"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    assigned_test_specs_id = itemgetter("assigned_test_specs_id")(parameters)

    try:
        # getting revoked consumed reservation code status ID
        revoked_status_id = ConsumedReservationCodeStatus.objects.get(
            codename=StaticConsumedReservationCodeStatus.REVOKED
        ).id

        # check if already exists in consumed reservation code model/table
        related_consumed_reservation_code_data = (
            ConsumedReservationCodes.objects.filter(
                assessment_process_assigned_test_specs_id=assigned_test_specs_id
            )
        ).last()

        # exists in consumed reservation code model/table
        if related_consumed_reservation_code_data:
            if related_consumed_reservation_code_data.test_session_id is not None:
                # cancel teams booking; this will visibly slow things down, so update the UI accordingly
                cancel_virtual_meeting(
                    related_consumed_reservation_code_data.candidate_id,
                    related_consumed_reservation_code_data.test_session_id,
                )

            # updating status to "REVOKED"

            if related_consumed_reservation_code_data.assigned_test_id is not None:
                return Response(
                    {"error": "code is already used"}, status=status.HTTP_409_CONFLICT
                )

            related_consumed_reservation_code_data.test_session_id = None
            related_consumed_reservation_code_data.status_id = revoked_status_id
            related_consumed_reservation_code_data.save()
        # does not exist in consumed reservation code model/table
        else:
            # getting needed data for the new data entry
            reservation_code_data = ReservationCodes.objects.get(
                assessment_process_assigned_test_specs_id=assigned_test_specs_id
            )
            # creating new entry in respective table
            ConsumedReservationCodes.objects.create(
                assessment_process_assigned_test_specs_id=assigned_test_specs_id,
                status_id=revoked_status_id,
                reservation_code=reservation_code_data.reservation_code,
                candidate_id=None,
                test_session_id=None,
                modify_date=datetime.now().date(),
            )

            # deleting respective reservation code
            reservation_code_data.delete()

        # sending "revoke" action email
        # getting most updated assessment process assigned test specs data
        data = AssessmentProcessAssignedTestSpecsVW.objects.get(
            id=assigned_test_specs_id
        )
        serialized_data = AssessmentProcessAssignedTestSpecsViewSerializer(
            data, many=False
        ).data
        # notify the candidate that their code was revoked by HR

        personalization = {
            "test_skill_type_name_fr": serialized_data["test_skill_type_name_fr"],
            "test_skill_sub_type_name_fr": serialized_data[
                "test_skill_sub_type_name_fr"
            ],
            "assessment_process_dept_fdesc": serialized_data[
                "assessment_process_dept_fdesc"
            ],
            "test_skill_type_name_en": serialized_data["test_skill_type_name_en"],
            "test_skill_sub_type_name_en": serialized_data[
                "test_skill_sub_type_name_en"
            ],
            "assessment_process_dept_edesc": serialized_data[
                "assessment_process_dept_edesc"
            ],
            "contact_email_for_candidates": serialized_data["billing_contact_email"],
        }

        hr_triggered_withdraw(serialized_data["email"], personalization)

        # adding site protocal/domain to the final object
        serialized_data["site_protocol"] = settings.SITE_PROTOCOL
        serialized_data["site_domain"] = settings.SITE_DOMAIN
        # adding reservation code to the final object
        reservation_code_data = ReservationCodes.history.filter(
            assessment_process_assigned_test_specs_id=serialized_data["id"],
            history_type="+",
        )
        reservation_code = ""
        if reservation_code_data:
            reservation_code = reservation_code_data.last().reservation_code
        serialized_data["reservation_code"] = reservation_code
        revoke_assigned_test_to_administer(serialized_data)

        return Response(status=status.HTTP_200_OK)

    except ConsumedReservationCodes.DoesNotExist:
        return Response(
            {
                "error:"
                "No consumed reservation code data found based on provided assigned_test_specs_id"
            },
            status=status.HTTP_404_NOT_FOUND,
        )

    except ConsumedReservationCodeStatus.DoesNotExist:
        return Response(
            {
                "error:"
                "No consumed reservation code status data found based on StaticConsumedReservationCodeStatus.REVOKED value"
            },
            status=status.HTTP_404_NOT_FOUND,
        )
    # should never happen
    except:
        return Response(
            {"error:" "Something went wrong during the unassign/revoke process"},
            status=status.HTTP_400_BAD_REQUEST,
        )


def update_reference_number(request):
    success, parameters = get_needed_parameters(
        ["assessment_process_id", "new_reference_number"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    assessment_process_id, new_reference_number = itemgetter(
        "assessment_process_id", "new_reference_number"
    )(parameters)

    assessment_process = AssessmentProcess.objects.filter(
        id=assessment_process_id
    ).first()

    assessment_process.reference_number = new_reference_number
    assessment_process.save()

    return Response(status=status.HTTP_200_OK)
