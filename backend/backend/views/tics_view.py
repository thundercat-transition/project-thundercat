from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.tics_data import get_data
from backend.api_permissions.role_based_api_permissions import HasSystemAdminPermission


class GetTicsData(APIView):
    def get(self, request):
        return get_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]
