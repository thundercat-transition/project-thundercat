from datetime import datetime
from rest_framework.response import Response
from rest_framework import status
from backend.views.utils import handleViewedQuestionsLogic
from backend.custom_models.accommodation_request import AccommodationRequest
from backend.custom_models.break_bank_actions import BreakBankActions
from backend.views.utils import (
    get_new_break_bank_remaining_time,
    get_last_accessed_test_section,
)
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.etta_actions import EttaActions
from backend.static.assigned_test_status import (
    AssignedTestStatus,
    get_assigned_test_status_id,
)
from cms.cms_models.test_definition import TestDefinition
from cms.views.test_break_bank import BreakBankActionsConstants


# ETTA Actions Definition
class EttaActionsConstants:
    # TODO: update the term "unassign" for "invalidate" (will be tricky to do, since that's used as a foreign key in ettaactiontypes)
    INVALIDATE_CANDIDATE = "UN_ASSIGN_CANDIDATE"
    VALIDATE_CANDIDATE = "VALIDATE_CANDIDATE"


# Assigned Test Statuses Definition based on Selected Action (statuses to include in assigned test filter)
class CandidateAssignedTestStatuses:
    assigned_status_id = get_assigned_test_status_id(AssignedTestStatus.ASSIGNED)
    unassigned_status_id = get_assigned_test_status_id(AssignedTestStatus.UNASSIGNED)
    checked_in_status_id = get_assigned_test_status_id(AssignedTestStatus.CHECKED_IN)
    pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
    active_status_id = get_assigned_test_status_id(AssignedTestStatus.ACTIVE)
    transition_status_id = get_assigned_test_status_id(AssignedTestStatus.TRANSITION)
    locked_status_id = get_assigned_test_status_id(AssignedTestStatus.LOCKED)
    paused_status_id = get_assigned_test_status_id(AssignedTestStatus.PAUSED)
    submitted_status_id = get_assigned_test_status_id(AssignedTestStatus.SUBMITTED)
    quit_status_id = get_assigned_test_status_id(AssignedTestStatus.QUIT)

    UN_ASSIGN_CANDIDATE = [
        submitted_status_id,
        unassigned_status_id,
        quit_status_id,
        assigned_status_id,
        checked_in_status_id,
        pre_test_status_id,
        active_status_id,
        transition_status_id,
        locked_status_id,
        paused_status_id,
    ]
    VALIDATE_CANDIDATE = [
        submitted_status_id,
        unassigned_status_id,
        quit_status_id,
    ]


# generic function for system administrator actions (terminate candidate test at any state)
# needed_parameters must be in order in your request
def system_administrator_actions(parameters, action):
    assigned_status_id = get_assigned_test_status_id(AssignedTestStatus.ASSIGNED)
    unassigned_status_id = get_assigned_test_status_id(AssignedTestStatus.UNASSIGNED)
    checked_in_status_id = get_assigned_test_status_id(AssignedTestStatus.CHECKED_IN)
    pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
    active_status_id = get_assigned_test_status_id(AssignedTestStatus.ACTIVE)
    locked_status_id = get_assigned_test_status_id(AssignedTestStatus.LOCKED)
    paused_status_id = get_assigned_test_status_id(AssignedTestStatus.PAUSED)
    submitted_status_id = get_assigned_test_status_id(AssignedTestStatus.SUBMITTED)
    quit_status_id = get_assigned_test_status_id(AssignedTestStatus.QUIT)
    try:
        # getting current assigned_test
        # using filter and first() to avoid error in case there is a user checked in to the same test twice
        assigned_test = AssignedTest.objects.get(
            id=parameters["id"],
            user_id=parameters["user_id"],
            test_id=TestDefinition.objects.get(id=parameters["test_id"]),
            status_id__in=(getattr(CandidateAssignedTestStatuses, action)),
        )

        if action == EttaActionsConstants.INVALIDATE_CANDIDATE:
            # initializing viewed_question_logic_already_called
            viewed_question_logic_already_called = False
            # test has been invalidated before
            if AssignedTest.history.filter(id=parameters["id"], is_invalid=True):
                # updating viewed_question_logic_already_called
                viewed_question_logic_already_called = True

            # if viewed_question_logic_already_called is still false
            if not viewed_question_logic_already_called:
                # test status is QUIT or SUBMIT (meaning that the logic has already been called from those actions)
                if (
                    assigned_test.status_id == quit_status_id
                    or assigned_test.status_id == submitted_status_id
                ):
                    # updating viewed_question_logic_already_called
                    viewed_question_logic_already_called = True

            # test is active
            if assigned_test.status_id == active_status_id:
                # setting submit date
                assigned_test.submit_date = datetime.now()

            # current status is ASSIGNED/READY/PRE-TEST
            if (
                assigned_test.status_id == assigned_status_id
                or assigned_test.status_id == checked_in_status_id
                or assigned_test.status_id == pre_test_status_id
            ):
                # set status to UNASSIGNED
                assigned_test.status_id = unassigned_status_id
            # current status is LOCKED
            elif assigned_test.status_id == locked_status_id:
                # previous status is READY/PRE-TEST
                if (
                    assigned_test.previous_status_id == checked_in_status_id
                    or assigned_test.previous_status_id == pre_test_status_id
                ):
                    # set status to UNASSIGNED
                    assigned_test.status_id = unassigned_status_id
                else:
                    # set status to QUIT
                    assigned_test.status_id = quit_status_id
                    # setting submit date
                    assigned_test.submit_date = datetime.now()
            # current status is PAUSED
            elif assigned_test.status_id == paused_status_id:
                # getting new break bank remaining time
                new_remaining_time = get_new_break_bank_remaining_time(
                    assigned_test.accommodation_request_id
                )
                # getting last accessed assigned test section ID
                last_accessed_test_section_id = get_last_accessed_test_section(
                    parameters["id"]
                )
                # creating new UNPAUSE action in Break Bank Actions table
                BreakBankActions.objects.create(
                    action_type=BreakBankActionsConstants.UNPAUSE,
                    new_remaining_time=new_remaining_time,
                    break_bank_id=AccommodationRequest.objects.get(
                        id=assigned_test.accommodation_request_id
                    ).break_bank_id,
                    test_section_id=last_accessed_test_section_id,
                )
                # set status to QUIT
                assigned_test.status_id = quit_status_id
                # setting submit date
                assigned_test.submit_date = datetime.now()
            # current status is SUBMITTED
            elif assigned_test.status_id == submitted_status_id:
                # make sure that the status does not change
                assigned_test.status_id = submitted_status_id
            else:
                # set status to QUIT
                assigned_test.status_id = quit_status_id

            # set is_invalid flag to true
            assigned_test.is_invalid = True
            assigned_test.save()

            # creating new entry in etta actions table
            etta_action = EttaActions.objects.create(
                action_type_id=EttaActionsConstants.INVALIDATE_CANDIDATE,
                assigned_test_id=assigned_test.id,
                action_reason=parameters["invalidate_test_reason"],
            )
            etta_action.save()

            # if viewed question logic has never been called before
            if not viewed_question_logic_already_called:
                # handling viewed questions logic
                handleViewedQuestionsLogic(assigned_test.id)
            return Response(status=status.HTTP_200_OK)

        elif action == EttaActionsConstants.VALIDATE_CANDIDATE:
            # set is_invalid flag to false
            assigned_test.is_invalid = False
            assigned_test.save()

            # creating new entry in etta actions table
            etta_action = EttaActions.objects.create(
                action_type_id=EttaActionsConstants.VALIDATE_CANDIDATE,
                assigned_test_id=assigned_test.id,
                action_reason=parameters["validate_test_reason"],
            )
            etta_action.save()
            return Response(status=status.HTTP_200_OK)

        # no action found
        else:
            return Response(
                {"error": "'{}' action does not exist".format(action)},
                status=status.HTTP_400_BAD_REQUEST,
            )

    except AssignedTest.DoesNotExist:
        return Response(
            {
                "error": "the specified Assigned Test does not exist based on provided parameters"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
