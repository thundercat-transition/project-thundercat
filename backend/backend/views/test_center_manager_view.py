from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.test_center_manager_data import (
    create_new_test_center_as_etta,
    get_all_test_centers_as_etta,
    get_selected_test_center_data_as_etta,
    update_test_center_data_as_etta,
    add_test_center_manager_accesses,
    delete_test_center_manager_accesses,
    get_found_test_centers_as_etta,
    get_associated_test_centers,
    get_found_associated_test_centers,
    get_selected_test_center_data,
    update_test_center_data,
    associate_test_administrators_to_test_center,
    get_associated_test_administrators_to_test_center,
    get_found_associated_test_administrators_to_test_center,
    delete_associated_test_administrator_from_test_center,
    add_room_to_test_center,
    get_test_center_rooms,
    get_found_test_center_rooms,
    edit_test_center_room,
    delete_test_center_room,
    add_test_session_to_test_center,
    add_non_standard_test_session_to_test_center,
    get_test_session_attendees,
    get_test_center_test_sessions,
    get_found_test_center_test_sessions,
    get_data_of_test_center_test_session_to_view_or_edit,
    edit_test_center_test_session,
    edit_non_standard_test_center_test_session,
    delete_test_center_test_session,
    get_ta_related_test_center_test_sessions,
    get_test_session_and_ta_related_test_to_administer_options,
    get_test_session_related_test_officers,
    update_test_center_ola_configs_data,
    create_new_test_center_ola_vacation_block,
    edit_test_center_ola_vacation_block,
    delete_test_center_ola_vacation_block,
    add_test_center_ola_test_assessor,
    edit_test_center_ola_test_assessor,
    delete_test_center_ola_test_assessor,
    get_test_center_ola_time_slot_assessor_availabilities,
    add_test_center_ola_time_slot,
    edit_test_center_ola_time_slot,
    delete_test_center_ola_time_slot,
)
from backend.api_permissions.role_based_api_permissions import (
    HasSystemAdminPermission,
    HasTestAdminPermission,
    HasTestCenterManagerPermission,
)


class CreateNewTestCenterAsEtta(APIView):
    def post(self, request):
        return create_new_test_center_as_etta(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasSystemAdminPermission(),
        ]


class GetAllTestCentersAsEtta(APIView):
    def get(self, request):
        return get_all_test_centers_as_etta(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasSystemAdminPermission(),
        ]


class GetFoundTestCentersAsEtta(APIView):
    def get(self, request):
        return get_found_test_centers_as_etta(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasSystemAdminPermission(),
        ]


class GetSelectedTestCenterDataAsEtta(APIView):
    def get(self, request):
        return get_selected_test_center_data_as_etta(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasSystemAdminPermission(),
        ]


class UpdateTestCenterDataAsEtta(APIView):
    def post(self, request):
        return update_test_center_data_as_etta(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasSystemAdminPermission(),
        ]


class AddTestCenterManagerAccesses(APIView):
    def post(self, request):
        return add_test_center_manager_accesses(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasSystemAdminPermission(),
        ]


class DeleteTestCenterManagerAccesses(APIView):
    def post(self, request):
        return delete_test_center_manager_accesses(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasSystemAdminPermission(),
        ]


class GetAssociatedTestCenters(APIView):
    def get(self, request):
        return get_associated_test_centers(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class GetFoundAssociatedTestCenters(APIView):
    def get(self, request):
        return get_found_associated_test_centers(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class GetSelectedTestCenterData(APIView):
    def get(self, request):
        return get_selected_test_center_data(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class UpdateTestCenterData(APIView):
    def post(self, request):
        return update_test_center_data(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class AssociateTestAdministratorsToTestCenter(APIView):
    def post(self, request):
        return associate_test_administrators_to_test_center(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class GetAssociatedTestAdministratorsToTestCenter(APIView):
    def get(self, request):
        return get_associated_test_administrators_to_test_center(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class GetFoundAssociatedTestAdministratorsToTestCenter(APIView):
    def get(self, request):
        return get_found_associated_test_administrators_to_test_center(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class DeleteAssociatedTestAdministratorFromTestCenter(APIView):
    def post(self, request):
        return delete_associated_test_administrator_from_test_center(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class AddRoomToTestCenter(APIView):
    def post(self, request):
        return add_room_to_test_center(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class GetTestCenterRooms(APIView):
    def get(self, request):
        return get_test_center_rooms(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class GetFoundTestCenterRooms(APIView):
    def get(self, request):
        return get_found_test_center_rooms(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class EditTestCenterRoom(APIView):
    def post(self, request):
        return edit_test_center_room(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class DeleteTestCenterRoom(APIView):
    def post(self, request):
        return delete_test_center_room(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class AddTestSessionToTestCenter(APIView):
    def post(self, request):
        return add_test_session_to_test_center(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class AddNonStandardTestSessionToTestCenter(APIView):
    def post(self, request):
        return add_non_standard_test_session_to_test_center(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class GetTestCenterTestSessions(APIView):
    def get(self, request):
        return get_test_center_test_sessions(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class GetFoundTestCenterTestSessions(APIView):
    def get(self, request):
        return get_found_test_center_test_sessions(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class GetTestSessionAttendees(APIView):
    permission_classes = (
        permissions.IsAuthenticated
        & (HasTestCenterManagerPermission | HasTestAdminPermission),
    )

    def get(self, request):
        return get_test_session_attendees(request)


class GetDataOfTestCenterTestSessionToViewOrEdit(APIView):
    def get(self, request):
        return get_data_of_test_center_test_session_to_view_or_edit(request)

    # allowing anyone that is authenticated to do this call, however some conditions are implemented in the logic of this API
    # if user has the TCM, he is allowed to do this call for any provided test session ID
    # if user does not have TCM, he is only allowed to do this call for his linked/related test sessions (otherwise, returns 401 UNAUTHORIZED)
    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
        ]


class EditTestCenterTestSession(APIView):
    def post(self, request):
        return edit_test_center_test_session(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class EditNonStandardTestCenterTestSession(APIView):
    def post(self, request):
        return edit_non_standard_test_center_test_session(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class DeleteTestCenterTestSession(APIView):
    def post(self, request):
        return delete_test_center_test_session(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class GetTaRelatedTestCenterTestSessions(APIView):
    def get(self, request):
        return get_ta_related_test_center_test_sessions(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestAdminPermission(),
        ]


class GetTestSessionAndTaRelatedTestToAdministerOptions(APIView):
    def get(self, request):
        return get_test_session_and_ta_related_test_to_administer_options(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestAdminPermission(),
        ]


class GetTestSessionRelatedTestOfficers(APIView):
    def get(self, request):
        return get_test_session_related_test_officers(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestAdminPermission(),
        ]


class UpdateTestCenterOlaConfigsData(APIView):
    def post(self, request):
        return update_test_center_ola_configs_data(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class CreateNewTestCenterOlaVacationBlock(APIView):
    def post(self, request):
        return create_new_test_center_ola_vacation_block(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class EditTestCenterOlaVacationBlock(APIView):
    def post(self, request):
        return edit_test_center_ola_vacation_block(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class DeleteTestCenterOlaVacationBlock(APIView):
    def post(self, request):
        return delete_test_center_ola_vacation_block(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class AddTestCenterOlaTestAssessor(APIView):
    def post(self, request):
        return add_test_center_ola_test_assessor(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class EditTestCenterOlaTestAssessor(APIView):
    def post(self, request):
        return edit_test_center_ola_test_assessor(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class DeleteTestCenterOlaTestAssessor(APIView):
    def post(self, request):
        return delete_test_center_ola_test_assessor(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class GetTestCenterOlaTimeSlotAssessorAvailabilities(APIView):
    def get(self, request):
        return get_test_center_ola_time_slot_assessor_availabilities(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class AddTestCenterOlaTimeSlot(APIView):
    def post(self, request):
        return add_test_center_ola_time_slot(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class EditTestCenterOlaTimeSlot(APIView):
    def post(self, request):
        return edit_test_center_ola_time_slot(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]


class DeleteTestCenterOlaTimeSlot(APIView):
    def post(self, request):
        return delete_test_center_ola_time_slot(request)

    def get_permissions(self):
        return [
            permissions.IsAuthenticated(),
            HasTestCenterManagerPermission(),
        ]
