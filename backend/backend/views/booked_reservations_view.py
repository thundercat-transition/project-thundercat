from itertools import chain
import json
from datetime import date, datetime, timedelta
from operator import itemgetter
from rest_framework import status
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from cms.views.utils import get_needed_parameters
from db_views.serializers.booked_reservations_serializers import (
    BookedReservationsViewSerializer,
)

from backend.views.utils import get_user_info_from_jwt_token
from db_views.db_view_models.booked_reservations_vw import (
    BookedReservationsVW,
)
from backend.custom_models.reservation_codes import ReservationCodes
from backend.custom_models.consumed_reservation_codes import ConsumedReservationCodes
from db_views.db_view_models.test_center_test_sessions_vw import (
    TestCenterTestSessionsVW,
)

from db_views.serializers.test_center_serializers import (
    ConsumedReservationCodesViewSerializer,
    TestCenterTestSessionsViewSerializer,
)
from backend.static.consumed_reservation_code_statuses import (
    StaticConsumedReservationCodeStatus,
)
from backend.custom_models.test_center_rooms import TestCenterRooms
from backend.custom_models.test_center import TestCenter
from backend.custom_models.consumed_reservation_code_status import (
    ConsumedReservationCodeStatus,
)


# find all sessions available for a given reservation code
class FindBookedReservationCode(APIView):
    def post(self, request):
        return find_booked_reservation_code(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def find_booked_reservation_code(request):
    user_info = get_user_info_from_jwt_token(request)
    user_id = user_info["user_id"]

    # get all test sessions for the given user that have status booked/paired OLA
    test_session_ids_temp = BookedReservationsVW.objects.filter(
        candidate_id=user_id,
        consumed_reservation_codename__in=(
            StaticConsumedReservationCodeStatus.BOOKED,
            StaticConsumedReservationCodeStatus.PAIRED_OLA,
        ),
        test_session_id__isnull=False,
    ).values_list("test_session_id", flat=True)

    # initializing test_session_ids
    test_session_ids = []
    for test_session_id in test_session_ids_temp:
        # populating test_session_ids with test_session_id as string (since the id in TestCenterTestSessionsVW is a CharField)
        test_session_ids.append("{0}".format(test_session_id))

    # get all reservations with accommodations requested
    requested_accommodation_ids = ConsumedReservationCodes.objects.filter(
        candidate_id=user_id,
        status=ConsumedReservationCodeStatus.objects.get(
            codename=StaticConsumedReservationCodeStatus.REQUESTED_ACCOMMODATION
        ),
    ).values_list("id", flat=True)

    # modifying consumed reservation code (requested accommodation) ids ==> adding "-CRC" to match ids in the TestCenterTestSessionsVW
    modified_requested_accommodation_ids = []
    for requested_accommodation_id in requested_accommodation_ids:
        modified_requested_accommodation_ids.append(
            str(requested_accommodation_id) + "-CRC"
        )

    # combine lists of ids
    all_ids = list(chain(test_session_ids, modified_requested_accommodation_ids))

    test_sessions = TestCenterTestSessionsVW.objects.filter(id__in=(all_ids))

    serialized_data = TestCenterTestSessionsViewSerializer(
        test_sessions, many=True
    ).data

    # getting sorted serialized data by start time (ascending) with accommodation requests at the end (null start time)
    ordered_test_sessions = sorted(
        serialized_data, key=lambda k: (k["start_time"] is None, k["start_time"])
    )

    return Response(ordered_test_sessions, status=status.HTTP_200_OK)


# find all sessions available for a given reservation code
class FindRequestedAccommodationsForReservationCode(APIView):
    def post(self, request):
        return find_requested_accommodations_for_reservation_code(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def find_requested_accommodations_for_reservation_code(request):
    user_info = get_user_info_from_jwt_token(request)
    user_id = user_info["user_id"]

    # get all reservations with accommodations requested
    requested_accommodations = ConsumedReservationCodes.objects.filter(
        candidate_id=user_id,
        status=ConsumedReservationCodeStatus.objects.get(
            codename=StaticConsumedReservationCodeStatus.REQUESTED_ACCOMMODATION
        ).id,
    )

    serialized_data = ConsumedReservationCodesViewSerializer(
        requested_accommodations, many=True
    ).data

    return Response(serialized_data, status=status.HTTP_200_OK)


# find all sessions available for a given reservation code
class FindAllReservedCodesForUser(APIView):
    def post(self, request):
        return find_all_reserved_codes_for_user(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def find_all_reserved_codes_for_user(request):
    user_info = get_user_info_from_jwt_token(request)
    user_id = user_info["user_id"]

    # get all test sessions for the given user that have status booked
    booked_reservation_codes = BookedReservationsVW.objects.filter(
        candidate_id=user_id,
        consumed_reservation_codename=StaticConsumedReservationCodeStatus.RESERVED,
    ).order_by("assessment_process_closing_date")

    serialized_data = BookedReservationsViewSerializer(
        booked_reservation_codes, many=True
    ).data

    return Response(serialized_data, status=status.HTTP_200_OK)
