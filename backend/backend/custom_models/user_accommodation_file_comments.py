from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.user_accommodation_file import UserAccommodationFile
from user_management.user_management_models.user_models import User


class UserAccommodationFileComments(models.Model):
    user_accommodation_file = models.ForeignKey(
        UserAccommodationFile,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    user = models.ForeignKey(
        User,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    modify_date = models.DateTimeField(auto_now=True)
    comment = models.CharField(max_length=255, blank=False, null=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
