from django.db import models
from backend.custom_models.test_center import TestCenter
from backend.custom_models.language import Language

# Contains time slots for a test center


class TestCenterOlaTimeSlot(models.Model):
    id = models.AutoField(primary_key=True)
    test_center = models.ForeignKey(TestCenter, on_delete=models.DO_NOTHING, null=False)
    day_of_week_id = models.IntegerField(blank=False, null=False, default=1)
    start_time = models.TimeField(blank=False, null=False)
    end_time = models.TimeField(blank=False, null=False)
    assessed_language = models.ForeignKey(
        Language,
        to_field="language_id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    availability = models.IntegerField(blank=False, null=False, default=0)
    slots_to_prioritize = models.IntegerField(blank=False, null=False, default=0)
    # need to keep track of the current UTC Offset when created/updated (in seconds) in order to manage the DST (Daylight Saving Time)
    utc_offset = models.IntegerField(blank=False, null=False, default=-18000)
