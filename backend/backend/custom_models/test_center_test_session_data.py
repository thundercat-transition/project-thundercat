from django.db import models
from simple_history.models import HistoricalRecords

from backend.custom_models.test_center_rooms import TestCenterRooms
from user_management.user_management_models.user_models import User
from cms.cms_models.test_skill_type import TestSkillType


class TestCenterTestSessionData(models.Model):
    test_center_room = models.ForeignKey(
        TestCenterRooms, to_field="id", on_delete=models.SET_NULL, null=True
    )
    open_to_ogd = models.BooleanField(default=False, blank=False, null=True)
    date = models.DateField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    spaces_available = models.IntegerField(blank=False, null=True)
    test_skill_type = models.ForeignKey(
        TestSkillType,
        to_field="id",
        blank=False,
        null=True,
        on_delete=models.DO_NOTHING,
    )
    # cannot be a foreign key because the source of the ID can be linked to different tables/models or even NULL
    test_skill_sub_type_id = models.IntegerField(blank=False, null=True)
    modify_date = models.DateTimeField(auto_now=True)
    test_assessor_user = models.ForeignKey(
        User,
        to_field="id",
        blank=False,
        null=True,
        on_delete=models.DO_NOTHING,
    )
    candidate_phone_number = models.CharField(
        max_length=10, null=True, blank=False, default=None
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
