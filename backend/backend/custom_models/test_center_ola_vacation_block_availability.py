from django.db import models
from backend.custom_models.test_center_ola_vacation_block import (
    TestCenterOlaVacationBlock,
)
from backend.custom_models.language import Language


# stores data related to the vacation block availability for a test center (as relates to an OLA)


class TestCenterOlaVacationBlockAvailability(models.Model):
    id = models.AutoField(primary_key=True)
    test_center_ola_vacation_block = models.ForeignKey(
        TestCenterOlaVacationBlock, on_delete=models.DO_NOTHING, null=False
    )
    language = models.ForeignKey(
        Language,
        to_field="language_id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    availability = models.IntegerField(blank=False, null=False, default=0)
