from django.db import models
from django.utils import timezone
from backend.custom_models.test_center import TestCenter


# stores data related to the vacation blocks for a test center (as relates to an OLA)


class TestCenterOlaVacationBlock(models.Model):
    id = models.AutoField(primary_key=True)
    test_center = models.ForeignKey(TestCenter, on_delete=models.DO_NOTHING, null=False)
    date_from = models.DateField()
    date_to = models.DateField()
