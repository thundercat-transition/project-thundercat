from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.assessment_process_assigned_test_specs import (
    AssessmentProcessAssignedTestSpecs,
)
from backend.custom_models.test_center_test_session_data import (
    TestCenterTestSessionData,
)
from backend.custom_models.user_accommodation_file import UserAccommodationFile
from backend.custom_models.test_center import TestCenter


class TestCenterTestSessions(models.Model):
    test_session_data = models.ForeignKey(
        TestCenterTestSessionData, to_field="id", on_delete=models.DO_NOTHING
    )
    test_center = models.ForeignKey(
        TestCenter, to_field="id", on_delete=models.DO_NOTHING, null=True
    )
    is_standard = models.BooleanField(default=1)
    user_accommodation_file = models.ForeignKey(
        UserAccommodationFile,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        default=None,
    )
    test_session_order = models.IntegerField(default=1)
    assessment_process_assigned_test_specs = models.ForeignKey(
        AssessmentProcessAssignedTestSpecs,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        default=None,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
