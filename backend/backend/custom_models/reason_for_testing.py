from django.db import models
from simple_history.models import HistoricalRecords

from backend.custom_models.reason_for_testing_priority import ReasonForTestingPriority
from backend.custom_models.reason_for_testing_type import ReasonForTestingType


class ReasonForTesting(models.Model):
    codename = models.CharField(max_length=50, null=False, blank=False)
    minimum_process_length = models.IntegerField(null=True, blank=False)
    waiting_period = models.IntegerField(null=True, blank=False, default=None)
    active = models.BooleanField(default=False)
    reason_for_testing_type = models.ForeignKey(
        ReasonForTestingType,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    reason_for_testing_priority = models.ForeignKey(
        ReasonForTestingPriority,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=False,
        default=None,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_reason_for_testing_codename_and_reason_for_testing_type_id_combination",
                fields=[
                    "codename",
                    "reason_for_testing_type",
                ],
            )
        ]
