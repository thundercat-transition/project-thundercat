from django.db import models
from simple_history.models import HistoricalRecords

from backend.custom_models.user_accommodation_file_recommendations_option import (
    UserAccommodationFileRecommendationsOption,
)
from backend.custom_models.language import Language


class UserAccommodationFileRecommendationsOptionText(models.Model):
    user_accommodation_file_recommendations_option = models.ForeignKey(
        UserAccommodationFileRecommendationsOption,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    text = models.CharField(max_length=150, null=False, blank=False)
    language = models.ForeignKey(
        Language,
        to_field="language_id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
