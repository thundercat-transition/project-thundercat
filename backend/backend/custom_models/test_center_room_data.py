from django.db import models
from simple_history.models import HistoricalRecords


class TestCenterRoomData(models.Model):
    name = models.CharField(max_length=150, blank=False, null=False)
    email = models.EmailField(max_length=254, blank=True, null=True)
    max_occupancy = models.IntegerField()
    active = models.BooleanField(default=False)
    modify_date = models.DateTimeField(auto_now=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
