from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.candidate_answers import CandidateAnswers


MAX_LENGTH = 25


class CandidateTaskResponseAnswers(models.Model):
    candidate_answers = models.ForeignKey(CandidateAnswers, on_delete=models.DO_NOTHING)
    answer_id = models.IntegerField()
    task = models.TextField(null=True)
    reasons_for_action = models.TextField(null=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        unique_together = (("answer_id", "candidate_answers"),)
