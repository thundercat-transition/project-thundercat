from django.db import models
from django.utils import timezone
from simple_history.models import HistoricalRecords
from cms.cms_models.test_definition import TestDefinition
from user_management.user_management_models.user_models import User
from backend.custom_models.test_center_test_sessions import TestCenterTestSessions


# Room Number Model, containing the randomly generated room numbers
# Used to make sure that only authorized candidate get their test assigned through this room number


class TestAccessCode(models.Model):
    id = models.AutoField(primary_key=True)
    test_access_code = models.CharField(max_length=10, unique=True)
    test = models.ForeignKey(TestDefinition, on_delete=models.DO_NOTHING, null=False)
    ta_user = models.ForeignKey(
        User,
        related_name="ta_user",
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
    )
    created_date = models.DateField(default=timezone.now)
    test_session = models.ForeignKey(
        TestCenterTestSessions,
        on_delete=models.SET_NULL,
        null=True,
        blank=False,
        default=None,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
