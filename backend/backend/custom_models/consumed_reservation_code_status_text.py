from django.db import models
from .consumed_reservation_code_status import ConsumedReservationCodeStatus
from .language import Language


class ConsumedReservationCodeStatusText(models.Model):
    consumed_reservation_code_status = models.ForeignKey(
        ConsumedReservationCodeStatus,
        to_field="id",
        on_delete=models.DO_NOTHING,
    )

    text = models.CharField(max_length=30, blank=False, null=False)

    language = models.ForeignKey(
        Language,
        to_field="language_id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
    )
