from django.db import models
from simple_history.models import HistoricalRecords

# ETTA Action Types Model (mapping action types data)


class EttaActionTypes(models.Model):
    action_type = models.CharField(primary_key=True, max_length=25)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
