from django.db import models
from simple_history.models import HistoricalRecords
from ..test_center import TestCenter
from ..language import Language


class TestCenterAddressText(models.Model):
    text = models.CharField(max_length=255, blank=True, null=True, default="")
    modify_date = models.DateTimeField(auto_now=True)
    language = models.ForeignKey(
        Language,
        to_field="language_id",
        on_delete=models.DO_NOTHING
    )
    test_center = models.ForeignKey(
        TestCenter,
        to_field="id",
        on_delete=models.DO_NOTHING,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Test Center Address Text"