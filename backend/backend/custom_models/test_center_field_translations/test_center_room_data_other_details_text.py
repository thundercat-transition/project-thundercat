from django.db import models
from simple_history.models import HistoricalRecords
from ..test_center_room_data import TestCenterRoomData
from ..language import Language


class TestCenterRoomDataOtherDetailsText(models.Model):
    text = models.TextField(blank=True, null=True, default="")
    modify_date = models.DateTimeField(auto_now=True)
    language = models.ForeignKey(
        Language,
        to_field="language_id",
        on_delete=models.DO_NOTHING
    )
    test_center_room_data = models.ForeignKey(
        TestCenterRoomData,
        to_field="id",
        on_delete=models.DO_NOTHING,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Test Center Room Data Other Details Text"