from django.db import models
from simple_history.models import HistoricalRecords


class OlaGlobalConfigs(models.Model):
    id = models.AutoField(primary_key=True)
    codename = models.CharField(max_length=50, null=False, blank=False)
    value = models.IntegerField(blank=False, null=False, default=0)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
