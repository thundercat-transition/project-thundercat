from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.user_accommodation_file import UserAccommodationFile


class UserAccommodationFileOtherMeasures(models.Model):
    user_accommodation_file = models.ForeignKey(
        UserAccommodationFile,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    other_measures = models.TextField(blank=False, null=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
