from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.test_section import TestSection
from backend.custom_models.user_accommodation_file import UserAccommodationFile


class UserAccommodationFileTestTime(models.Model):
    user_accommodation_file = models.ForeignKey(
        UserAccommodationFile,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    test_section = models.ForeignKey(
        TestSection,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=False,
    )
    test_section_time = models.IntegerField(blank=False, null=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
