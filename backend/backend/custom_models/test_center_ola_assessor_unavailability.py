from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.test_center_ola_test_assessor import (
    TestCenterOlaTestAssessor,
)


class TestCenterOlaAssessorUnavailability(models.Model):
    test_center_ola_test_assessor = models.ForeignKey(
        TestCenterOlaTestAssessor,
        to_field="id",
        on_delete=models.CASCADE,
    )
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    reason = models.CharField(max_length=300, null=True, blank=True, default="")

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
