from django.db import models


# stores MS Teams user id's for all the virtual rooms
class VirtualTestRoom(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40, blank=False, null=False)
    user_id = models.CharField(max_length=36, blank=False, null=False)
