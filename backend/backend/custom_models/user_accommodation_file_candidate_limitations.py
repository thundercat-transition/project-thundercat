from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.user_accommodation_file_candidate_limitations_option import (
    UserAccommodationFileCandidateLimitationsOption,
)
from backend.custom_models.user_accommodation_file import UserAccommodationFile


class UserAccommodationFileCandidateLimitations(models.Model):
    user_accommodation_file = models.ForeignKey(
        UserAccommodationFile,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    user_accommodation_file_candidate_limitations_option = models.ForeignKey(
        UserAccommodationFileCandidateLimitationsOption,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_user_accommodation_file_and_candidate_limitations_option_combination",
                fields=[
                    "user_accommodation_file",
                    "user_accommodation_file_candidate_limitations_option",
                ],
            )
        ]
