from django.db import models
from simple_history.models import HistoricalRecords


class ReasonForTestingType(models.Model):
    codename = models.CharField(max_length=25, null=False, blank=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_reason_for_testing_type_codename",
                fields=["codename"],
            )
        ]
