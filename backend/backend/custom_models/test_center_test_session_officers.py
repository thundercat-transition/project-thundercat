from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.test_access_code_model import TestAccessCode
from backend.custom_models.test_center_test_sessions import TestCenterTestSessions
from user_management.user_management_models.user_models import User


class TestCenterTestSessionOfficers(models.Model):
    test_session = models.ForeignKey(
        TestCenterTestSessions, to_field="id", on_delete=models.SET_NULL, null=True
    )
    test_administrator = models.ForeignKey(
        User, to_field="id", on_delete=models.DO_NOTHING
    )
    test_access_code = models.ForeignKey(
        TestAccessCode, to_field="id", on_delete=models.DO_NOTHING
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        constraints = [
            models.UniqueConstraint(
                name="test_session__with_test_administrator_and_test_access_code_must_be_a_unique_combination",
                fields=["test_session", "test_administrator", "test_access_code"],
            ),
        ]
