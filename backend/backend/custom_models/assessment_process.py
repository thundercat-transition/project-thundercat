from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.billing_contact import BillingContact
from user_management.user_management_models.user_models import User


class AssessmentProcess(models.Model):
    reference_number = models.CharField(max_length=30, null=False, blank=False)
    department_id = models.IntegerField(blank=False, null=False)
    duration = models.IntegerField(default=0)
    closing_date = models.DateField(null=True)
    allow_booking_external_tc = models.BooleanField()
    allow_last_minute_cancellation_tc = models.BooleanField(default=False)
    default_billing_contact = models.ForeignKey(
        BillingContact,
        to_field="id",
        on_delete=models.SET_NULL,
        null=True,
        blank=False,
    )
    contact_email_for_candidates = models.CharField(
        max_length=254, null=False, blank=False, default="temp"
    )
    user = models.ForeignKey(User, to_field="id", on_delete=models.DO_NOTHING)
    request_sent = models.BooleanField(default=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
