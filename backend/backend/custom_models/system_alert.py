from django.db import models
from django.utils import timezone
from simple_history.models import HistoricalRecords
from backend.custom_models.criticality import Criticality

# Model used for system alerts in case of service interruption


class SystemAlert(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255, null=False, blank=False)
    criticality = models.ForeignKey(
        Criticality, to_field="id", on_delete=models.DO_NOTHING
    )
    active_date = models.DateTimeField(default=timezone.now, null=False, blank=False)
    end_date = models.DateTimeField(default=timezone.now, null=False, blank=False)
    message_text_en = models.TextField(null=False, blank=False)
    message_text_fr = models.TextField(null=False, blank=False)
    archived = models.BooleanField(default=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
