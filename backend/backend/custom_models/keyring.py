from django.db import models

# Keyring to store various keys for various doors


class Keyring(models.Model):
    id = models.AutoField(primary_key=True)
    codename = models.CharField(max_length=20, null=False, blank=False)
    key = models.CharField(max_length=120, null=False, blank=False)
