from django.db import models
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_models import User


class BillingContact(models.Model):
    user = models.ForeignKey(User, to_field="id", on_delete=models.DO_NOTHING)
    first_name = models.CharField(max_length=30, null=False, blank=False)
    last_name = models.CharField(max_length=150, null=False, blank=False)
    email = models.CharField(max_length=254, null=False, blank=False)
    department_id = models.IntegerField(blank=False, null=False)
    fis_organisation_code = models.CharField(max_length=16, null=False, blank=False)
    fis_reference_code = models.CharField(max_length=20, null=False, blank=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
