from django.db import models
from backend.custom_models.reason_for_testing_priority import ReasonForTestingPriority
from backend.custom_models.test_center_ola_time_slot import TestCenterOlaTimeSlot


class TestCenterOlaTimeSlotPrioritization(models.Model):
    test_center_ola_time_slot = models.ForeignKey(
        TestCenterOlaTimeSlot,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    reason_for_testing_priority = models.ForeignKey(
        ReasonForTestingPriority,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    percentage = models.IntegerField(blank=False, null=False)
