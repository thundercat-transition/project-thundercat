from django.db import models
from simple_history.models import HistoricalRecords


class SiteAdminSetting(models.Model):
    id = models.AutoField(primary_key=True)
    en_description = models.CharField(
        default="default", max_length=75, blank=False, null=False
    )
    fr_description = models.CharField(
        default="default", max_length=75, blank=False, null=False
    )
    codename = models.CharField(max_length=25, unique=True, blank=False, null=False)
    is_active = models.BooleanField(default=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Site Admin Setting Status"
