from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.test_center_room_data import TestCenterRoomData
from backend.custom_models.test_center import TestCenter


class TestCenterRooms(models.Model):
    room_data = models.ForeignKey(
        TestCenterRoomData, to_field="id", on_delete=models.DO_NOTHING
    )
    test_center = models.ForeignKey(
        TestCenter, to_field="id", on_delete=models.DO_NOTHING
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
