from django.contrib.auth.admin import UserAdmin
from django.contrib import admin
from user_management.user_management_models.user_models import User

admin.site.register(User, UserAdmin)
