from django.db import models
from django.db.models import Q
from simple_history.models import HistoricalRecords
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.language import Language
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.items import Items


class CandidateAnswers(models.Model):
    assigned_test = models.ForeignKey(AssignedTest, on_delete=models.DO_NOTHING)
    # from test builder
    question = models.ForeignKey(NewQuestion, on_delete=models.DO_NOTHING, null=True)
    # from item bank
    item = models.ForeignKey(
        Items, on_delete=models.DO_NOTHING, null=True, default=None
    )
    test_section_component = models.ForeignKey(
        TestSectionComponent, on_delete=models.DO_NOTHING
    )
    mark_for_review = models.BooleanField(default=False)
    modify_date = models.DateTimeField(auto_now=True)
    selected_language = models.ForeignKey(
        Language,
        to_field="language_id",
        on_delete=models.DO_NOTHING,
        default=None,
        blank=True,
        null=True,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        constraints = [
            models.UniqueConstraint(
                condition=Q(question__isnull=False),
                fields=["assigned_test", "question"],
                name="combination_of_assigned_test_id_and_question_id_must_be_unique",
            ),
            models.UniqueConstraint(
                condition=Q(item__isnull=False),
                fields=["assigned_test", "item"],
                name="combination_of_assigned_test_id_and_item_id_must_be_unique",
            ),
        ]
