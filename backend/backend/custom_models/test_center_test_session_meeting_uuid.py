from django.db import models
from simple_history.models import HistoricalRecords


class TestCenterTestSessionMeetingUuid(models.Model):
    uuid = models.CharField(max_length=100, blank=False, null=False)
    test_session_id = models.IntegerField()

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
