from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.test_center import TestCenter
from user_management.user_management_models.user_models import User


class TestCenterAssociatedManagers(models.Model):
    user = models.ForeignKey(User, to_field="id", on_delete=models.DO_NOTHING)
    test_center = models.ForeignKey(
        TestCenter, to_field="id", on_delete=models.DO_NOTHING
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
