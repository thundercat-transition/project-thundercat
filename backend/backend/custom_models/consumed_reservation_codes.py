from django.db import models
from simple_history.models import HistoricalRecords


from .consumed_reservation_code_status import ConsumedReservationCodeStatus
from user_management.user_management_models.user_models import User
from backend.custom_models.assessment_process_assigned_test_specs import (
    AssessmentProcessAssignedTestSpecs,
)
from backend.custom_models.test_center_test_sessions import TestCenterTestSessions
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.user_accommodation_file import UserAccommodationFile


class ConsumedReservationCodes(models.Model):
    assessment_process_assigned_test_specs = models.ForeignKey(
        AssessmentProcessAssignedTestSpecs,
        to_field="id",
        on_delete=models.DO_NOTHING,
    )
    status = models.ForeignKey(
        ConsumedReservationCodeStatus, to_field="id", on_delete=models.DO_NOTHING
    )
    reservation_code = models.CharField(max_length=14, null=False, blank=False)
    candidate = models.ForeignKey(
        User, to_field="id", on_delete=models.DO_NOTHING, null=True, blank=False
    )
    test_session = models.ForeignKey(
        TestCenterTestSessions,
        to_field="id",
        on_delete=models.SET_NULL,
        null=True,
        blank=False,
    )
    modify_date = models.DateField()
    assigned_test = models.ForeignKey(
        AssignedTest,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        default=None,
    )
    user_accommodation_file = models.ForeignKey(
        UserAccommodationFile,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        default=None,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="consumed_reservation_must_have_a_unique_assessment_process_assigned_test_specs_id",
                fields=["assessment_process_assigned_test_specs"],
            )
        ]
