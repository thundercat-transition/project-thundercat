from django.db import migrations
from backend.static.site_admin_setting_type import SiteAdminSettingType


def create_new_uit_reasons_for_deletion(apps, _):
    # get models
    site_admin_setting = apps.get_model("custom_models", "siteadminsetting")
    # creating all needed settings
    setting_1 = site_admin_setting(
        en_description="Enable Quality of Life Mode",
        fr_description="FR Enable Quality of Life Mode",
        codename=SiteAdminSettingType.ENABLE_QUALITY_OF_LIFE,
        is_active=False,
    )
    setting_1.save()


def rollback_changes(apps, schema_editor):
    # get models
    site_admin_setting = apps.get_model("custom_models", "siteadminsetting")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the setting
    site_admin_setting.objects.using(db_alias).filter(
        codename=SiteAdminSettingType.ENABLE_QUALITY_OF_LIFE
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0208_add_ola_prioritization")]
    operations = [
        migrations.RunPython(create_new_uit_reasons_for_deletion, rollback_changes)
    ]
