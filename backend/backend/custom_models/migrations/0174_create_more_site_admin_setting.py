from django.db import migrations
from backend.static.site_admin_setting_type import SiteAdminSettingType, Key


def create_new_uit_reasons_for_deletion(apps, _):
    # get models
    site_admin_setting = apps.get_model("custom_models", "siteadminsetting")
    # creating all needed settings
    setting_1 = site_admin_setting(
        en_description="Sent Virtual Webex Meetings",
        fr_description="FR Sent Virtual Webex Meetings",
        codename=SiteAdminSettingType.ENABLE_WEBEX,
        is_active=False,
    )
    setting_1.save()
    setting_2 = site_admin_setting(
        en_description="Sent Virtual MS Teams Meetings",
        fr_description="FR Sent Virtual MS Teams Meetings",
        codename=SiteAdminSettingType.ENABLE_MS_TEAMS_API,
        is_active=False,
    )
    setting_2.save()
    setting_3 = site_admin_setting(
        en_description="Teams App ID",
        fr_description="FR Teams App ID",
        codename=Key.MS_TEAMS_API_APP_ID,
        is_active=False,
    )
    setting_3.save()
    setting_4 = site_admin_setting(
        en_description="Teams App Name",
        fr_description="FR Teams App Name",
        codename="msteamsapiappname",
        is_active=False,
    )
    setting_4.save()
    setting_5 = site_admin_setting(
        en_description="Teams Tenant ID",
        fr_description="FR Teams Tenant ID",
        codename=Key.MS_TEAMS_API_TENANT_ID,
        is_active=False,
    )
    setting_5.save()
    setting_6 = site_admin_setting(
        en_description="Teams Secret",
        fr_description="FR Teams Secret",
        codename=Key.MS_TEAMS_API_SECRET,
        is_active=False,
    )
    setting_6.save()
    setting_7 = site_admin_setting(
        en_description="Teams User ID",
        fr_description="FR Teams User ID",
        codename="msteamsapiuserid",
        is_active=False,
    )
    setting_7.save()


def rollback_changes(apps, schema_editor):
    # get models
    site_admin_setting = apps.get_model("custom_models", "siteadminsetting")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the setting
    site_admin_setting.objects.using(db_alias).filter(
        codename=SiteAdminSettingType.ENABLE_WEBEX
    ).delete()
    site_admin_setting.objects.using(db_alias).filter(
        codename=SiteAdminSettingType.ENABLE_MS_TEAMS_API
    ).delete()
    site_admin_setting.objects.using(db_alias).filter(
        codename=Key.MS_TEAMS_API_APP_ID
    ).delete()
    site_admin_setting.objects.using(db_alias).filter(
        codename="msteamsapiappname"
    ).delete()
    site_admin_setting.objects.using(db_alias).filter(
        codename=Key.MS_TEAMS_API_TENANT_ID
    ).delete()
    site_admin_setting.objects.using(db_alias).filter(
        codename=Key.MS_TEAMS_API_SECRET
    ).delete()
    site_admin_setting.objects.using(db_alias).filter(
        codename="msteamsapiuserid"
    ).delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0173_remove_assessmentprocessassignedtestspecs_reason_for_testing_old_and_more",
        )
    ]
    operations = [
        migrations.RunPython(create_new_uit_reasons_for_deletion, rollback_changes)
    ]
