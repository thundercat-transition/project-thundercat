from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def update_gc_notify_template_folders(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # update templates folder name
    template = gc_notify_template.objects.using(db_alias).get(codename="uitinvitebb")
    template.folder = "deprecated"
    template.save()
    template = gc_notify_template.objects.using(db_alias).get(codename="uitinviteet")
    template.folder = "deprecated"
    template.save()
    template = gc_notify_template.objects.using(db_alias).get(codename="uitinviteetbb")
    template.folder = "deprecated"
    template.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # update templates folder name
    template = gc_notify_template.objects.using(db_alias).get(codename="uitinvitebb")
    template.folder = "UIT"
    template.save()
    template = gc_notify_template.objects.using(db_alias).get(codename="uitinviteet")
    template.folder = "UIT"
    template.save()
    template = gc_notify_template.objects.using(db_alias).get(codename="uitinviteetbb")
    template.folder = "UIT"
    template.save()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0188_updating_user_accommodation_statuses")]
    operations = [
        migrations.RunPython(update_gc_notify_template_folders, rollback_changes)
    ]
