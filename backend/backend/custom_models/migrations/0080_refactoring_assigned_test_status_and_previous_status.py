from django.db import connection
from django.db import migrations
from django.conf import settings


def refactor_assigned_test_status_and_previous_status(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """-- declaring needed variables
                DECLARE 
                    @status_old INT,
                    @assigned INT,
                    @checked_in INT,
                    @unassigned INT,
                    @pre_test INT,
                    @active INT,
                    @transition INT,
                    @locked INT,
                    @paused INT,
                    @submitted INT,
                    @quit INT

                -- getting assigned test status ids
                SET @assigned = (SELECT id FROM custom_models_assignedteststatus WHERE codename = 'assigned')
                SET @checked_in= (SELECT id FROM custom_models_assignedteststatus WHERE codename = 'checked_in')
                SET @unassigned= (SELECT id FROM custom_models_assignedteststatus WHERE codename = 'unassigned')
                SET @pre_test = (SELECT id FROM custom_models_assignedteststatus WHERE codename = 'pre_test')
                SET @active = (SELECT id FROM custom_models_assignedteststatus WHERE codename = 'active')
                SET @transition = (SELECT id FROM custom_models_assignedteststatus WHERE codename = 'transition')
                SET @locked = (SELECT id FROM custom_models_assignedteststatus WHERE codename = 'locked')
                SET @paused = (SELECT id FROM custom_models_assignedteststatus WHERE codename = 'paused')
                SET @submitted = (SELECT id FROM custom_models_assignedteststatus WHERE codename = 'submitted')
                SET @quit = (SELECT id FROM custom_models_assignedteststatus WHERE codename = 'quit')

                -- updating assigned test model
                -- status_id
                UPDATE {db_name}..custom_models_assignedtest SET status_id = @assigned WHERE status_old = 11
                UPDATE {db_name}..custom_models_assignedtest SET status_id = @checked_in WHERE status_old = 12
                UPDATE {db_name}..custom_models_assignedtest SET status_id = @active WHERE status_old = 13
                UPDATE {db_name}..custom_models_assignedtest SET status_id = @locked WHERE status_old = 14
                UPDATE {db_name}..custom_models_assignedtest SET status_id = @paused WHERE status_old = 15
                UPDATE {db_name}..custom_models_assignedtest SET status_id = @quit WHERE status_old = 19
                UPDATE {db_name}..custom_models_assignedtest SET status_id = @submitted WHERE status_old = 20
                UPDATE {db_name}..custom_models_assignedtest SET status_id = @unassigned WHERE status_old = 21
                UPDATE {db_name}..custom_models_assignedtest SET status_id = @pre_test WHERE status_old = 22
                -- previous_status_id
                UPDATE {db_name}..custom_models_assignedtest SET previous_status_id = @checked_in WHERE previous_status_old = 12
                UPDATE {db_name}..custom_models_assignedtest SET previous_status_id = @pre_test WHERE previous_status_old = 22

                -- updating historical assigned test model
                -- status_id
                UPDATE {db_name}..custom_models_historicalassignedtest SET status_id = @assigned WHERE status_old = 11
                UPDATE {db_name}..custom_models_historicalassignedtest SET status_id = @checked_in WHERE status_old = 12
                UPDATE {db_name}..custom_models_historicalassignedtest SET status_id = @active WHERE status_old = 13
                UPDATE {db_name}..custom_models_historicalassignedtest SET status_id = @locked WHERE status_old = 14
                UPDATE {db_name}..custom_models_historicalassignedtest SET status_id = @paused WHERE status_old = 15
                UPDATE {db_name}..custom_models_historicalassignedtest SET status_id = @quit WHERE status_old = 19
                UPDATE {db_name}..custom_models_historicalassignedtest SET status_id = @submitted WHERE status_old = 20
                UPDATE {db_name}..custom_models_historicalassignedtest SET status_id = @unassigned WHERE status_old = 21
                UPDATE {db_name}..custom_models_historicalassignedtest SET status_id = @pre_test WHERE status_old = 22
                -- previous_status_id
                UPDATE {db_name}..custom_models_historicalassignedtest SET previous_status_id = @checked_in WHERE previous_status_old = 12
                UPDATE {db_name}..custom_models_historicalassignedtest SET previous_status_id = @pre_test WHERE previous_status_old = 22
            """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """UPDATE {db_name}..custom_models_assignedtest SET status_id = 1, previous_status_id = NULL
                UPDATE {db_name}..custom_models_historicalassignedtest SET status_id = 1, previous_status_id = NULL
            """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0079_assignedtest_needs_approval_and_more",
        )
    ]
    operations = [
        migrations.RunPython(
            refactor_assigned_test_status_and_previous_status, rollback_changes
        )
    ]
