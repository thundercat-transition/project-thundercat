# Generated by Django 4.2.11 on 2024-06-20 12:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('custom_models', '0131_historicaluseraccommodationfile_rationale_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicaluseraccommodationfile',
            name='accommodation_request',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='custom_models.accommodationrequest'),
        ),
        migrations.AddField(
            model_name='useraccommodationfile',
            name='accommodation_request',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='custom_models.accommodationrequest'),
        ),
    ]
