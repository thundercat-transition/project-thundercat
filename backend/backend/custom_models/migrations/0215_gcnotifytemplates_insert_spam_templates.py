from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, _):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # creating all needed settings
    tempplate_1 = gc_notify_template(
        folder="OLA",
        codename=GCNotifyTemplateCodenames.TWO_DAY_REMINDER,
        template_id="a12630c9-fd3f-405c-891a-a748f3bd4be0",
    )
    tempplate_1.save()
    tempplate_2 = gc_notify_template(
        folder="OLA",
        codename=GCNotifyTemplateCodenames.SEVEN_DAY_REMINDER,
        template_id="e4e508c0-ec32-4de9-a334-015f9aa1e203",
    )
    tempplate_2.save()
    tempplate_3 = gc_notify_template(
        folder="OLA",
        codename=GCNotifyTemplateCodenames.CANDIDATE_WITHDRAWL_NOTIFICATION,
        template_id="44ca089e-d46a-424a-8f26-fe4f42e15d10",
    )
    tempplate_3.save()
    tempplate_4 = gc_notify_template(
        folder="OLA",
        codename=GCNotifyTemplateCodenames.HR_TRIGGERED_WITHDRAW,
        template_id="a681c650-4a3e-430a-bb13-59e3dc430054",
    )
    tempplate_4.save()
    tempplate_5 = gc_notify_template(
        folder="OLA",
        codename=GCNotifyTemplateCodenames.TEST_BUMPED_BY_TC,
        template_id="1fe38fba-acbf-4fb6-b89d-7a6e3a45563e",
    )
    tempplate_5.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the setting
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.TWO_DAY_REMINDER
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.SEVEN_DAY_REMINDER
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.CANDIDATE_WITHDRAWL_NOTIFICATION
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.HR_TRIGGERED_WITHDRAW
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.TEST_BUMPED_BY_TC
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0214_remove_dayofweektext_day_of_week_and_more")]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
