from django.db import migrations
from backend.static.languages import Language_id
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames

# ==================== NEW ====================
NEW_STATUS_CODENAME_OLD_NAME = "sent"
NEW_STATUS_CODENAME_NEW_NAME = "new"

NEW_STATUS_TEXT_OLD_NAME_EN = "SENT"
NEW_STATUS_TEXT_OLD_NAME_FR = "FR SENT"
NEW_STATUS_TEXT_NEW_NAME_EN = "NEW"
NEW_STATUS_TEXT_NEW_NAME_FR = "NOUVEAU"
# ==============================================

# ==================== IN PROGRESS ====================
IN_PROGRESS_TEXT_OLD_NAME_FR = "FR IN PROGRESS"
IN_PROGRESS_TEXT_NEW_NAME_FR = "EN COURS"
# =====================================================

# ==================== ON HOLD ====================
ON_HOLD_TEXT_OLD_NAME_FR = "FR ON HOLD"
ON_HOLD_TEXT_NEW_NAME_FR = "EN ATTENTE"
# =================================================

# ==================== COMPLETED ====================
COMPLETED_TEXT_OLD_NAME_EN = "COMPLETED"
COMPLETED_TEXT_NEW_NAME_EN = "CLOSED"
COMPLETED_TEXT_OLD_NAME_FR = "FR COMPLETED"
COMPLETED_TEXT_NEW_NAME_FR = "FERMÉE"
# ===================================================

# ==================== CANCELLED ====================
CANCELLED_STATUS_CODENAME_OLD_NAME = "rejected"
CANCELLED_STATUS_CODENAME_NEW_NAME = "cancelled"

CANCELLED_STATUS_TEXT_OLD_NAME_EN = "REJECTED"
CANCELLED_STATUS_TEXT_OLD_NAME_FR = "FR REJECTED"
CANCELLED_STATUS_TEXT_NEW_NAME_EN = "CANCELLED"
CANCELLED_STATUS_TEXT_NEW_NAME_FR = "ANNULÉ"
# =================================================


def create_user_accommodation_statuses(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # updating SENT codename to NEW
    data_to_update = user_accommodation_status.objects.using(db_alias).get(
        codename=NEW_STATUS_CODENAME_OLD_NAME
    )
    data_to_update.codename = NEW_STATUS_CODENAME_NEW_NAME
    data_to_update.save()

    # updating SENT text to NEW respectively
    text_data_to_update_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=data_to_update.id, language_id=Language_id.EN
    )
    text_data_to_update_en.text = NEW_STATUS_TEXT_NEW_NAME_EN
    text_data_to_update_en.save()
    text_data_to_update_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=data_to_update.id, language_id=Language_id.FR
    )
    text_data_to_update_fr.text = NEW_STATUS_TEXT_NEW_NAME_FR
    text_data_to_update_fr.save()

    # updating IN PROGRESS text
    in_progress_status_data = user_accommodation_status.objects.using(db_alias).get(
        codename="in_progress"
    )
    in_progress_text_to_update_fr = user_accommodation_status_text.objects.using(
        db_alias
    ).get(
        user_accommodation_file_status_id=in_progress_status_data.id,
        language_id=Language_id.FR,
    )
    in_progress_text_to_update_fr.text = IN_PROGRESS_TEXT_NEW_NAME_FR
    in_progress_text_to_update_fr.save()

    # updating ON HOLD text
    on_hold_status_data = user_accommodation_status.objects.using(db_alias).get(
        codename="on_hold"
    )
    on_hold_text_to_update_fr = user_accommodation_status_text.objects.using(
        db_alias
    ).get(
        user_accommodation_file_status_id=on_hold_status_data.id,
        language_id=Language_id.FR,
    )
    on_hold_text_to_update_fr.text = ON_HOLD_TEXT_NEW_NAME_FR
    on_hold_text_to_update_fr.save()

    # updating COMPLETED text
    completed_status_data = user_accommodation_status.objects.using(db_alias).get(
        codename="completed"
    )
    completed_text_to_update_en = user_accommodation_status_text.objects.using(
        db_alias
    ).get(
        user_accommodation_file_status_id=completed_status_data.id,
        language_id=Language_id.EN,
    )
    completed_text_to_update_en.text = COMPLETED_TEXT_NEW_NAME_EN
    completed_text_to_update_en.save()
    completed_text_to_update_fr = user_accommodation_status_text.objects.using(
        db_alias
    ).get(
        user_accommodation_file_status_id=completed_status_data.id,
        language_id=Language_id.FR,
    )
    completed_text_to_update_fr.text = COMPLETED_TEXT_NEW_NAME_FR
    completed_text_to_update_fr.save()

    # updating REJECTED codename to CANCELLED
    data_to_update = user_accommodation_status.objects.using(db_alias).get(
        codename=CANCELLED_STATUS_CODENAME_OLD_NAME
    )
    data_to_update.codename = CANCELLED_STATUS_CODENAME_NEW_NAME
    data_to_update.save()

    # updating REJECTED text to CANCELLED respectively
    cancelled_text_to_update_en = user_accommodation_status_text.objects.using(
        db_alias
    ).get(
        user_accommodation_file_status_id=data_to_update.id, language_id=Language_id.EN
    )
    cancelled_text_to_update_en.text = CANCELLED_STATUS_TEXT_NEW_NAME_EN
    cancelled_text_to_update_en.save()
    cancelled_text_to_update_fr = user_accommodation_status_text.objects.using(
        db_alias
    ).get(
        user_accommodation_file_status_id=data_to_update.id, language_id=Language_id.FR
    )
    cancelled_text_to_update_fr.text = CANCELLED_STATUS_TEXT_NEW_NAME_FR
    cancelled_text_to_update_fr.save()

    # creating all needed codenames
    codename_1 = user_accommodation_status(codename="estimating_service_cost")
    codename_1.save()
    codename_2 = user_accommodation_status(codename="waiting_for_financial_coding")
    codename_2.save()
    codename_3 = user_accommodation_status(codename="waiting_for_documentation")
    codename_3.save()
    codename_4 = user_accommodation_status(codename="ready")
    codename_4.save()
    codename_5 = user_accommodation_status(codename="re_opened")
    codename_5.save()
    codename_6 = user_accommodation_status(codename="re_opened_hold")
    codename_6.save()

    # creating all needed status text
    status_text_1_en = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_1.id,
        text="ESTIMATING SERVICE COST",
        language_id=Language_id.EN,
    )
    status_text_1_en.save()
    status_text_1_fr = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_1.id,
        text="ESTIMATION DU COÛT DU SERVICE",
        language_id=Language_id.FR,
    )
    status_text_1_fr.save()

    status_text_2_en = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_2.id,
        text="WAITING FOR FINANCIAL CODING",
        language_id=Language_id.EN,
    )
    status_text_2_en.save()
    status_text_2_fr = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_2.id,
        text="EN ATTENTE DE CODES FINANCIERS",
        language_id=Language_id.FR,
    )
    status_text_2_fr.save()

    status_text_3_en = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_3.id,
        text="WAITING FOR DOCUMENTATION",
        language_id=Language_id.EN,
    )
    status_text_3_en.save()
    status_text_3_fr = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_3.id,
        text="EN ATTENTE DE DOCUMENTATION",
        language_id=Language_id.FR,
    )
    status_text_3_fr.save()

    status_text_4_en = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_4.id,
        text="READY",
        language_id=Language_id.EN,
    )
    status_text_4_en.save()
    status_text_4_fr = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_4.id,
        text="PRÊTE",
        language_id=Language_id.FR,
    )
    status_text_4_fr.save()

    status_text_5_en = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_5.id,
        text="RE-OPENED",
        language_id=Language_id.EN,
    )
    status_text_5_en.save()
    status_text_5_fr = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_5.id,
        text="RÉOUVERTE",
        language_id=Language_id.FR,
    )
    status_text_5_fr.save()

    status_text_6_en = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_6.id,
        text="RE-OPENED HOLD",
        language_id=Language_id.EN,
    )
    status_text_6_en.save()
    status_text_6_fr = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_6.id,
        text="RÉOUVERTE EN ATTENTE",
        language_id=Language_id.FR,
    )
    status_text_6_fr.save()


def rollback_changes(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # updating NEW codename to SENT
    data_to_update = user_accommodation_status.objects.using(db_alias).get(
        codename=NEW_STATUS_CODENAME_NEW_NAME
    )
    data_to_update.codename = NEW_STATUS_CODENAME_OLD_NAME
    data_to_update.save()

    # updating NEW text to SENT respectively
    text_data_to_update_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=data_to_update.id, language_id=Language_id.EN
    )
    text_data_to_update_en.text = NEW_STATUS_TEXT_OLD_NAME_EN
    text_data_to_update_en.save()
    text_data_to_update_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=data_to_update.id, language_id=Language_id.FR
    )
    text_data_to_update_fr.text = NEW_STATUS_TEXT_OLD_NAME_FR
    text_data_to_update_fr.save()

    # updating IN PROGRESS text
    in_progress_status_data = user_accommodation_status.objects.using(db_alias).get(
        codename="in_progress"
    )
    in_progress_text_to_update_fr = user_accommodation_status_text.objects.using(
        db_alias
    ).get(
        user_accommodation_file_status_id=in_progress_status_data.id,
        language_id=Language_id.FR,
    )
    in_progress_text_to_update_fr.text = IN_PROGRESS_TEXT_OLD_NAME_FR
    in_progress_text_to_update_fr.save()

    # updating ON HOLD text
    on_hold_status_data = user_accommodation_status.objects.using(db_alias).get(
        codename="on_hold"
    )
    on_hold_text_to_update_fr = user_accommodation_status_text.objects.using(
        db_alias
    ).get(
        user_accommodation_file_status_id=on_hold_status_data.id,
        language_id=Language_id.FR,
    )
    on_hold_text_to_update_fr.text = ON_HOLD_TEXT_OLD_NAME_FR
    on_hold_text_to_update_fr.save()

    # updating COMPLETED text
    completed_status_data = user_accommodation_status.objects.using(db_alias).get(
        codename="completed"
    )
    completed_text_to_update_en = user_accommodation_status_text.objects.using(
        db_alias
    ).get(
        user_accommodation_file_status_id=completed_status_data.id,
        language_id=Language_id.EN,
    )
    completed_text_to_update_en.text = COMPLETED_TEXT_OLD_NAME_EN
    completed_text_to_update_en.save()
    completed_text_to_update_fr = user_accommodation_status_text.objects.using(
        db_alias
    ).get(
        user_accommodation_file_status_id=completed_status_data.id,
        language_id=Language_id.FR,
    )
    completed_text_to_update_fr.text = COMPLETED_TEXT_OLD_NAME_FR
    completed_text_to_update_fr.save()

    # updating REJECTED codename to CANCELLED
    data_to_update = user_accommodation_status.objects.using(db_alias).get(
        codename=CANCELLED_STATUS_CODENAME_NEW_NAME
    )
    data_to_update.codename = CANCELLED_STATUS_CODENAME_OLD_NAME
    data_to_update.save()

    # updating REJECTED text to CANCELLED respectively
    text_data_to_update_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=data_to_update.id, language_id=Language_id.EN
    )
    text_data_to_update_en.text = CANCELLED_STATUS_TEXT_OLD_NAME_EN
    text_data_to_update_en.save()
    text_data_to_update_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=data_to_update.id, language_id=Language_id.FR
    )
    text_data_to_update_fr.text = CANCELLED_STATUS_TEXT_OLD_NAME_FR
    text_data_to_update_fr.save()

    # getting all respective user accommodation file status entries
    respective_user_accommodation_file_status_entries = (
        user_accommodation_status.objects.using(db_alias).filter(
            codename__in=(
                "estimating_service_cost",
                "waiting_for_financial_coding",
                "waiting_for_documentation",
                "ready",
                "re_opened",
                "re_opened_hold",
            )
        )
    )

    # looping in respective_user_accommodation_file_status_entries
    for (
        respective_user_accommodation_file_status_entry
    ) in respective_user_accommodation_file_status_entries:
        # Deleting all respective user accommodation file status text entries
        user_accommodation_status_text.objects.using(db_alias).filter(
            user_accommodation_file_status_id=respective_user_accommodation_file_status_entry.id
        ).delete()

        # Deleting respective user accommodation file status entry
        respective_user_accommodation_file_status_entry.delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0127_historicaluseraccommodationfile_reason_for_action_and_more",
        )
    ]
    operations = [
        migrations.RunPython(create_user_accommodation_statuses, rollback_changes)
    ]
