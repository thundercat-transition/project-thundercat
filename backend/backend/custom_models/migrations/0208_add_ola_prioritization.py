from django.db import migrations
from backend.static.ola_prioritization_const import OlaPrioritizationConst


def create_new_ola_prioritization(apps, schema_editor):
    # get models
    ola_prioritization = apps.get_model("custom_models", "olaprioritization")

    # adding ola prioritization codenames
    prioritization_1 = ola_prioritization(
        codename=OlaPrioritizationConst.HIGH, value=60
    )
    prioritization_1.save()

    prioritization_2 = ola_prioritization(
        codename=OlaPrioritizationConst.MEDIUM, value=25
    )
    prioritization_2.save()

    prioritization_3 = ola_prioritization(codename=OlaPrioritizationConst.LOW, value=15)
    prioritization_3.save()


def rollback_changes(apps, schema_editor):
    # get models
    ola_prioritization = apps.get_model("custom_models", "olaprioritization")

    # get db alias
    db_alias = schema_editor.connection.alias

    # deleting existing data
    ola_prioritization.objects.using(db_alias).all().delete()


class Migration(migrations.Migration):
    dependencies = [
        ("custom_models", "0207_olaprioritization_historicalolaprioritization")
    ]
    operations = [migrations.RunPython(create_new_ola_prioritization, rollback_changes)]
