from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, _):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # creating all needed templates
    tempplate_1 = gc_notify_template(
        folder="OLA",
        codename=GCNotifyTemplateCodenames.OLA_INVITE_CANDIDATE_TO_NON_STANDARD_TEST_SESSION_IN_PERSON,
        template_id="a67610ef-07ac-4c4c-afcf-551744594e9d",
    )
    tempplate_1.save()
    tempplate_2 = gc_notify_template(
        folder="OLA",
        codename=GCNotifyTemplateCodenames.OLA_INVITE_CANDIDATE_TO_NON_STANDARD_TEST_SESSION_VIRTUAL,
        template_id="a0f407fe-aaa4-49fe-b38c-da87607816a7",
    )
    tempplate_2.save()
    tempplate_3 = gc_notify_template(
        folder="OLA",
        codename=GCNotifyTemplateCodenames.OLA_UPDATE_NON_STANDARD_TEST_SESSION_INVITE_IN_PERSON,
        template_id="397ee57a-5434-4529-902b-0be51efa50e4",
    )
    tempplate_3.save()
    tempplate_4 = gc_notify_template(
        folder="OLA",
        codename=GCNotifyTemplateCodenames.OLA_UPDATE_NON_STANDARD_TEST_SESSION_INVITE_VIRTUAL,
        template_id="893b7cef-949d-4be7-93ac-d6622339f18c",
    )
    tempplate_4.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the setting
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.OLA_INVITE_CANDIDATE_TO_NON_STANDARD_TEST_SESSION_IN_PERSON
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.OLA_INVITE_CANDIDATE_TO_NON_STANDARD_TEST_SESSION_VIRTUAL
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.OLA_UPDATE_NON_STANDARD_TEST_SESSION_INVITE_IN_PERSON
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.OLA_UPDATE_NON_STANDARD_TEST_SESSION_INVITE_VIRTUAL
    ).delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0217_alter_historicaltestcenterolaassessorunavailability_end_date_and_more",
        )
    ]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
