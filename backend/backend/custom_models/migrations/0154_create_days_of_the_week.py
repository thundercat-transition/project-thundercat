from django.db import migrations
from backend.static.languages import Language_id


def create_days_of_the_week(apps, _):
    # get models
    day_of_week = apps.get_model("custom_models", "dayofweek")
    day_of_week_text = apps.get_model("custom_models", "dayofweektext")
    # creating all needed days
    monday = day_of_week(codename="monday")
    monday.save()
    tuesday = day_of_week(codename="tuesday")
    tuesday.save()
    wednesday = day_of_week(codename="wednesday")
    wednesday.save()
    thursday = day_of_week(codename="thursday")
    thursday.save()
    friday = day_of_week(codename="friday")
    friday.save()
    saturday = day_of_week(codename="saturday")
    saturday.save()
    sunday = day_of_week(codename="sunday")
    sunday.save()

    monday_en = day_of_week_text(
        day_of_week=monday, language_id=Language_id.EN, text="Monday"
    )
    monday_en.save()
    monday_fr = day_of_week_text(
        day_of_week=monday, language_id=Language_id.FR, text="Lundi"
    )
    monday_fr.save()
    tuesday_en = day_of_week_text(
        day_of_week=tuesday, language_id=Language_id.EN, text="Tuesday"
    )
    tuesday_en.save()
    tuesday_fr = day_of_week_text(
        day_of_week=tuesday, language_id=Language_id.FR, text="Mardi"
    )
    tuesday_fr.save()
    wednesday_en = day_of_week_text(
        day_of_week=wednesday, language_id=Language_id.EN, text="Wednesday"
    )
    wednesday_en.save()
    wednesday_fr = day_of_week_text(
        day_of_week=wednesday, language_id=Language_id.FR, text="Mercredi"
    )
    wednesday_fr.save()
    thursday_en = day_of_week_text(
        day_of_week=thursday, language_id=Language_id.EN, text="Thursday"
    )
    thursday_en.save()
    thursday_fr = day_of_week_text(
        day_of_week=thursday, language_id=Language_id.FR, text="Jeudi"
    )
    thursday_fr.save()
    friday_en = day_of_week_text(
        day_of_week=friday, language_id=Language_id.EN, text="Friday"
    )
    friday_en.save()
    friday_fr = day_of_week_text(
        day_of_week=friday, language_id=Language_id.FR, text="Vendredi"
    )
    friday_fr.save()
    saturday_en = day_of_week_text(
        day_of_week=saturday, language_id=Language_id.EN, text="Saturday"
    )
    saturday_en.save()
    saturday_fr = day_of_week_text(
        day_of_week=saturday, language_id=Language_id.FR, text="Samedi"
    )
    saturday_fr.save()
    sunday_en = day_of_week_text(
        day_of_week=sunday, language_id=Language_id.EN, text="Sunday"
    )
    sunday_en.save()
    sunday_fr = day_of_week_text(
        day_of_week=sunday, language_id=Language_id.FR, text="Dimanche"
    )
    sunday_fr.save()


def rollback_changes(apps, schema_editor):
    # get models
    day_of_week = apps.get_model("custom_models", "dayofweek")
    day_of_week_text = apps.get_model("custom_models", "dayofweektext")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the days
    # get the days
    monday = day_of_week.objects.using(db_alias).filter(codename="monday").last()
    tuesday = day_of_week.objects.using(db_alias).filter(codename="tuesday").last()
    wednesday = day_of_week.objects.using(db_alias).filter(codename="wednesday").last()
    thursday = day_of_week.objects.using(db_alias).filter(codename="thursday").last()
    friday = day_of_week.objects.using(db_alias).filter(codename="friday").last()
    saturday = day_of_week.objects.using(db_alias).filter(codename="saturday").last()
    sunday = day_of_week.objects.using(db_alias).filter(codename="sunday").last()

    # delete the text
    day_of_week_text.objects.using(db_alias).filter(day_of_week=monday).delete()
    day_of_week_text.objects.using(db_alias).filter(day_of_week=tuesday).delete()
    day_of_week_text.objects.using(db_alias).filter(day_of_week=wednesday).delete()
    day_of_week_text.objects.using(db_alias).filter(day_of_week=thursday).delete()
    day_of_week_text.objects.using(db_alias).filter(day_of_week=friday).delete()
    day_of_week_text.objects.using(db_alias).filter(day_of_week=saturday).delete()
    day_of_week_text.objects.using(db_alias).filter(day_of_week=sunday).delete()
    # delete the text
    monday.delete()
    tuesday.delete()
    wednesday.delete()
    thursday.delete()
    friday.delete()
    saturday.delete()
    sunday.delete()


class Migration(migrations.Migration):
    dependencies = [
        ("custom_models", "0153_alter_testcenterolatimeslot_end_time_and_more")
    ]
    operations = [migrations.RunPython(create_days_of_the_week, rollback_changes)]
