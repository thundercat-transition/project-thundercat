from django.db import connection
from django.db import migrations
from django.conf import settings


def update_data(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """
				UPDATE at
				SET at.score_valid_until = CONVERT(VARCHAR(10), DATEADD(MONTH, sm.validity_period, at.submit_date), 23)
				FROM {db_name}..custom_models_assignedtest at
				JOIN {db_name}..cms_models_scoringmethods sm on sm.test_id = at.test_id
				WHERE at.submit_date is not NULL AND at.en_converted_score is not NULL AND sm.validity_period is not NULL
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """
			UPDATE {db_name}..custom_models_assignedtest SET score_valid_until = NULL
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0087_assignedtest_score_valid_until_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_data, rollback_changes)]
