from django.db import migrations
from backend.static.user_accommodation_file_recommendations_const import (
    UserAccommodationFileRecommendationsConst,
)
from backend.static.languages import Language_id

# UserAccommodationFileRecommendationsConst.ASSESSOR_OR_TEST_ADMINISTRATOR
OLD_ASSESSOR_OR_TEST_ADMINISTRATOR = "Évaluateur ou Administrateur d'examen"
NEW_ASSESSOR_OR_TEST_ADMINISTRATOR = "Évaluateur ou administrateur de test"

# UserAccommodationFileRecommendationsConst.PREPARATION_TO_TEST
OLD_PREPARATION_TO_TEST = "Préparation à l'examen"
NEW_PREPARATION_TO_TEST = "Préparation au test"

# UserAccommodationFileRecommendationsConst.SLE_UIT_LEVELING
OLD_SLE_UIT_LEVELING = "ELS-TELNS Mise à niveau"
NEW_SLE_UIT_LEVELING = "ÉLS-TELNS mise à niveau"


def update_data(apps, schema_editor):
    # get models
    recommendations_option = apps.get_model(
        "custom_models", "useraccommodationfilerecommendationsoption"
    )
    recommendations_option_text = apps.get_model(
        "custom_models", "useraccommodationfilerecommendationsoptiontext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # ==================== RECOMMENDATIONS ====================
    # updating recommendations options text

    # UserAccommodationFileRecommendationsConst.ASSESSOR_OR_TEST_ADMINISTRATOR
    assessor_or_test_administrator = recommendations_option.objects.using(db_alias).get(
        codename=UserAccommodationFileRecommendationsConst.ASSESSOR_OR_TEST_ADMINISTRATOR
    )
    assessor_or_test_administrator_fr = recommendations_option_text.objects.using(
        db_alias
    ).get(
        user_accommodation_file_recommendations_option_id=assessor_or_test_administrator.id,
        language_id=Language_id.FR,
    )
    assessor_or_test_administrator_fr.text = NEW_ASSESSOR_OR_TEST_ADMINISTRATOR
    assessor_or_test_administrator_fr.save()

    # UserAccommodationFileRecommendationsConst.PREPARATION_TO_TEST
    preparation_to_test = recommendations_option.objects.using(db_alias).get(
        codename=UserAccommodationFileRecommendationsConst.PREPARATION_TO_TEST
    )
    preparation_to_test_fr = recommendations_option_text.objects.using(db_alias).get(
        user_accommodation_file_recommendations_option_id=preparation_to_test.id,
        language_id=Language_id.FR,
    )
    preparation_to_test_fr.text = NEW_PREPARATION_TO_TEST
    preparation_to_test_fr.save()

    # UserAccommodationFileRecommendationsConst.SLE_UIT_LEVELING
    sle_uit_leveling = recommendations_option.objects.using(db_alias).get(
        codename=UserAccommodationFileRecommendationsConst.SLE_UIT_LEVELING
    )
    sle_uit_leveling_fr = recommendations_option_text.objects.using(db_alias).get(
        user_accommodation_file_recommendations_option_id=sle_uit_leveling.id,
        language_id=Language_id.FR,
    )
    sle_uit_leveling_fr.text = NEW_SLE_UIT_LEVELING
    sle_uit_leveling_fr.save()

    # ==================== RECOMMENDATIONS (END) ====================


def rollback_changes(apps, schema_editor):
    # get models
    recommendations_option = apps.get_model(
        "custom_models", "useraccommodationfilerecommendationsoption"
    )
    recommendations_option_text = apps.get_model(
        "custom_models", "useraccommodationfilerecommendationsoptiontext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # ==================== RECOMMENDATIONS ====================
    # updating recommendations options text

    # UserAccommodationFileRecommendationsConst.ASSESSOR_OR_TEST_ADMINISTRATOR
    assessor_or_test_administrator = recommendations_option.objects.using(db_alias).get(
        codename=UserAccommodationFileRecommendationsConst.ASSESSOR_OR_TEST_ADMINISTRATOR
    )
    assessor_or_test_administrator_fr = recommendations_option_text.objects.using(
        db_alias
    ).get(
        user_accommodation_file_recommendations_option_id=assessor_or_test_administrator.id,
        language_id=Language_id.FR,
    )
    assessor_or_test_administrator_fr.text = OLD_ASSESSOR_OR_TEST_ADMINISTRATOR
    assessor_or_test_administrator_fr.save()

    # UserAccommodationFileRecommendationsConst.PREPARATION_TO_TEST
    preparation_to_test = recommendations_option.objects.using(db_alias).get(
        codename=UserAccommodationFileRecommendationsConst.PREPARATION_TO_TEST
    )
    preparation_to_test_fr = recommendations_option_text.objects.using(db_alias).get(
        user_accommodation_file_recommendations_option_id=preparation_to_test.id,
        language_id=Language_id.FR,
    )
    preparation_to_test_fr.text = OLD_PREPARATION_TO_TEST
    preparation_to_test_fr.save()

    # UserAccommodationFileRecommendationsConst.SLE_UIT_LEVELING
    sle_uit_leveling = recommendations_option.objects.using(db_alias).get(
        codename=UserAccommodationFileRecommendationsConst.SLE_UIT_LEVELING
    )
    sle_uit_leveling_fr = recommendations_option_text.objects.using(db_alias).get(
        user_accommodation_file_recommendations_option_id=sle_uit_leveling.id,
        language_id=Language_id.FR,
    )
    sle_uit_leveling_fr.text = OLD_SLE_UIT_LEVELING
    sle_uit_leveling_fr.save()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0164_rename_test_center_ola_test_assessor_id_testcenterolatestassessorapprovedlanguage_test_center_ola_te",
        ),
        (
            "db_view_models",
            "0333_create_test_center_ola_test_assessors_view"
        ),
    ]
    operations = [migrations.RunPython(update_data, rollback_changes)]
