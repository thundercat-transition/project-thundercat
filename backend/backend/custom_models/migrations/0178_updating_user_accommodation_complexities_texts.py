from django.db import migrations
from backend.static.languages import Language_id
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames

# text translations
LOW_TEXT_EN_OLD = "Low"
LOW_TEXT_FR_OLD = "Faible"
LOW_TEXT_EN_NEW = "Low"
LOW_TEXT_FR_NEW = "Basse"

MEDIUM_TEXT_EN_OLD = "Medium"
MEDIUM_TEXT_FR_OLD = "Moyenne"
MEDIUM_TEXT_EN_NEW = "Medium"
MEDIUM_TEXT_FR_NEW = "Moyenne"

HIGH_TEXT_EN_OLD = "High"
HIGH_TEXT_FR_OLD = "Haute"
HIGH_TEXT_EN_NEW = "High"
HIGH_TEXT_FR_NEW = "Élevée"


def create_user_accommodation_complexities(apps, schema_editor):
    # get models
    user_accommodation_complexity = apps.get_model(
        "custom_models", "useraccommodationfilecomplexity"
    )
    user_accommodation_complexity_text = apps.get_model(
        "custom_models", "useraccommodationfilecomplexitytext"
    )

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # update low
    option = user_accommodation_complexity.objects.using(db_alias).get(codename="Low")
    option_text_fr = user_accommodation_complexity_text.objects.using(db_alias).get(
        user_accommodation_file_complexity_id=option.id,
        language_id=Language_id.FR,
    )
    option_text_fr.text = LOW_TEXT_FR_NEW
    option_text_fr.save()

    option_text_en = user_accommodation_complexity_text.objects.using(db_alias).get(
        user_accommodation_file_complexity_id=option.id,
        language_id=Language_id.EN,
    )
    option_text_en.text = LOW_TEXT_EN_NEW
    option_text_en.save()

    # update medium
    option = user_accommodation_complexity.objects.using(db_alias).get(codename="Low")
    option_text_fr = user_accommodation_complexity_text.objects.using(db_alias).get(
        user_accommodation_file_complexity_id=option.id,
        language_id=Language_id.FR,
    )
    option_text_fr.text = MEDIUM_TEXT_FR_NEW
    option_text_fr.save()

    option_text_en = user_accommodation_complexity_text.objects.using(db_alias).get(
        user_accommodation_file_complexity_id=option.id,
        language_id=Language_id.EN,
    )
    option_text_en.text = MEDIUM_TEXT_EN_NEW
    option_text_en.save()

    # update high
    option = user_accommodation_complexity.objects.using(db_alias).get(codename="Low")
    option_text_fr = user_accommodation_complexity_text.objects.using(db_alias).get(
        user_accommodation_file_complexity_id=option.id,
        language_id=Language_id.FR,
    )
    option_text_fr.text = HIGH_TEXT_FR_NEW
    option_text_fr.save()

    option_text_en = user_accommodation_complexity_text.objects.using(db_alias).get(
        user_accommodation_file_complexity_id=option.id,
        language_id=Language_id.EN,
    )
    option_text_en.text = HIGH_TEXT_EN_NEW
    option_text_en.save()


def rollback_changes(apps, schema_editor):
    # get models
    user_accommodation_complexity = apps.get_model(
        "custom_models", "useraccommodationfilecomplexity"
    )
    user_accommodation_complexity_text = apps.get_model(
        "custom_models", "useraccommodationfilecomplexitytext"
    )

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # update low
    option = user_accommodation_complexity.objects.using(db_alias).get(codename="Low")
    option_text_fr = user_accommodation_complexity_text.objects.using(db_alias).get(
        user_accommodation_file_complexity_id=option.id,
        language_id=Language_id.FR,
    )
    option_text_fr.text = LOW_TEXT_FR_OLD
    option_text_fr.save()

    option_text_en = user_accommodation_complexity_text.objects.using(db_alias).get(
        user_accommodation_file_complexity_id=option.id,
        language_id=Language_id.EN,
    )
    option_text_en.text = LOW_TEXT_EN_OLD
    option_text_en.save()

    # update medium
    option = user_accommodation_complexity.objects.using(db_alias).get(codename="Low")
    option_text_fr = user_accommodation_complexity_text.objects.using(db_alias).get(
        user_accommodation_file_complexity_id=option.id,
        language_id=Language_id.FR,
    )
    option_text_fr.text = MEDIUM_TEXT_FR_OLD
    option_text_fr.save()

    option_text_en = user_accommodation_complexity_text.objects.using(db_alias).get(
        user_accommodation_file_complexity_id=option.id,
        language_id=Language_id.EN,
    )
    option_text_en.text = MEDIUM_TEXT_EN_OLD
    option_text_en.save()

    # update high
    option = user_accommodation_complexity.objects.using(db_alias).get(codename="Low")
    option_text_fr = user_accommodation_complexity_text.objects.using(db_alias).get(
        user_accommodation_file_complexity_id=option.id,
        language_id=Language_id.FR,
    )
    option_text_fr.text = HIGH_TEXT_FR_OLD
    option_text_fr.save()

    option_text_en = user_accommodation_complexity_text.objects.using(db_alias).get(
        user_accommodation_file_complexity_id=option.id,
        language_id=Language_id.EN,
    )
    option_text_en.text = HIGH_TEXT_EN_OLD
    option_text_en.save()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0177_virtualteamsmeetingsession_test_session",
        )
    ]
    operations = [
        migrations.RunPython(create_user_accommodation_complexities, rollback_changes)
    ]
