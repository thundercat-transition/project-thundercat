from django.db import migrations


def create_new_uit_reasons_for_modification(apps, _):
    # get models
    uit_reason_for_modification = apps.get_model(
        "custom_models", "uitreasonsformodification"
    )
    # creating all needed UIT reasons for deletion
    reason_1 = uit_reason_for_modification(
        en_name="Technical Issue", fr_name="Problème technique"
    )
    reason_1.save()
    reason_2 = uit_reason_for_modification(
        en_name="Invitation Error", fr_name="Erreur d'invitation"
    )
    reason_2.save()
    reason_3 = uit_reason_for_modification(
        en_name="Requested by client", fr_name="Demande du client"
    )
    reason_3.save()
    reason_4 = uit_reason_for_modification(en_name="Others", fr_name="Autres")
    reason_4.save()


def rollback_changes(apps, schema_editor):
    # get models
    uit_reason_for_modification = apps.get_model(
        "custom_models", "uitreasonsformodification"
    )
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the types
    uit_reason_for_modification.objects.using(db_alias).filter(
        en_name="Technical Issue"
    ).delete()
    uit_reason_for_modification.objects.using(db_alias).filter(
        en_name="Invitation Error"
    ).delete()
    uit_reason_for_modification.objects.using(db_alias).filter(
        en_name="Requested by client"
    ).delete()
    uit_reason_for_modification.objects.using(db_alias).filter(
        en_name="Others"
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0023_auto_20220110_1338")]
    operations = [
        migrations.RunPython(create_new_uit_reasons_for_modification, rollback_changes)
    ]
