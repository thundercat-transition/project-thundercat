from django.db import migrations
from backend.static.languages import Language_id


def create_user_accommodation_statuses(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )

    # adding user_accommodation_status
    user_accommodation_status_1 = user_accommodation_status(
        codename="administered", active=1, order=14
    )
    user_accommodation_status_1.save()

    # adding user_accommodation_status_text
    user_accommodation_status_text_en = user_accommodation_status_text(
        user_accommodation_file_status_id=user_accommodation_status_1.id,
        language_id=Language_id.EN,
        text="ADMINISTERED",
    )
    user_accommodation_status_text_en.save()

    user_accommodation_status_text_fr = user_accommodation_status_text(
        user_accommodation_file_status_id=user_accommodation_status_1.id,
        language_id=Language_id.FR,
        text="ADMINISTRÉE",
    )
    user_accommodation_status_text_fr.save()


def rollback_changes(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )

    # get db alias
    db_alias = schema_editor.connection.alias

    # deleting needed data
    user_accommodation_status_1 = user_accommodation_status.objects.using(db_alias).get(
        codename="administered"
    )
    user_accommodation_status_text.objects.using(db_alias).filter(
        user_accommodation_file_status_id=user_accommodation_status_1.id
    ).delete()
    user_accommodation_status_1.delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0203_create_and_update_user_accommodation_file_recommendations",
        )
    ]
    operations = [
        migrations.RunPython(create_user_accommodation_statuses, rollback_changes)
    ]
