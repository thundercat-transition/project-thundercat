from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, _):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # creating all needed settings
    tempplate_1 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.DELETE_SPECIFIC_TEST_SESSION,
        template_id="c35d701e-455e-48b3-94d9-1fb7f97ebeed",
    )
    tempplate_1.save()
    tempplate_2 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.RESERVATION_CODE_WITHDRAW_NO_BOOKING,
        template_id="7d3da5c2-7a8d-4b74-94d0-054b4e40628b",
    )
    tempplate_2.save()
    tempplate_3 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.RESERVATION_CODE_WITHDRAW_FROM_BOOKED_SESSION,
        template_id="6341fa0e-973a-4670-9a7b-1c65c989f443",
    )
    tempplate_3.save()
    tempplate_4 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.RESERVATION_CODE_BOOKING,
        template_id="02011418-e861-411e-9ae2-4648109748df",
    )
    tempplate_4.save()
    tempplate_5 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.RESERVATION_CODE_BOOK_LATER,
        template_id="6a2d3b20-84ce-4fc8-a095-fcada40d0291",
    )
    tempplate_5.save()
    tempplate_6 = gc_notify_template(
        folder="Misc",
        codename=GCNotifyTemplateCodenames.PASSWORD_RESET,
        template_id="8fc1065a-9fd3-4a58-b3f6-6eb6a0f01b85",
    )
    tempplate_6.save()
    tempplate_7 = gc_notify_template(
        folder="Misc",
        codename=GCNotifyTemplateCodenames.TEST_SESSION_CANDIDATES_TO_SECURITY,
        template_id="aae684c4-8942-4768-b304-ed9658665c72",
    )
    tempplate_7.save()
    tempplate_8 = gc_notify_template(
        folder="UIT",
        codename=GCNotifyTemplateCodenames.MODIFY_UIT_VALIDITY_END_DATE,
        template_id="461bb914-79c2-4ec3-80b5-26b104fb70a7",
    )
    tempplate_8.save()
    tempplate_9 = gc_notify_template(
        folder="UIT",
        codename=GCNotifyTemplateCodenames.DEACTIVATE_UIT_TEST_CANCELLATION,
        template_id="d847a131-4a82-4638-bcd8-a087a6bae1f2",
    )
    tempplate_9.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the setting
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.DELETE_SPECIFIC_TEST_SESSION
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.RESERVATION_CODE_WITHDRAW_NO_BOOKING
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.RESERVATION_CODE_WITHDRAW_FROM_BOOKED_SESSION
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.RESERVATION_CODE_BOOKING
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.RESERVATION_CODE_BOOK_LATER
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.PASSWORD_RESET
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.TEST_SESSION_CANDIDATES_TO_SECURITY
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.MODIFY_UIT_VALIDITY_END_DATE
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.DEACTIVATE_UIT_TEST_CANCELLATION
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0104_alter_consumedreservationcodes_candidate")]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
