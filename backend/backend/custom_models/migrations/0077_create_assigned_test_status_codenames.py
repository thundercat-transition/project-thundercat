from django.db import migrations


def create_new_codenames(apps, _):
    # get models
    assigned_test_status = apps.get_model("custom_models", "assignedteststatus")
    # creating all needed codenames
    codename_1 = assigned_test_status(codename="assigned")
    codename_1.save()

    codename_2 = assigned_test_status(codename="checked_in")
    codename_2.save()

    codename_3 = assigned_test_status(codename="unassigned")
    codename_3.save()

    codename_4 = assigned_test_status(codename="pre_test")
    codename_4.save()

    codename_5 = assigned_test_status(codename="active")
    codename_5.save()

    codename_6 = assigned_test_status(codename="transition")
    codename_6.save()

    codename_7 = assigned_test_status(codename="locked")
    codename_7.save()

    codename_8 = assigned_test_status(codename="paused")
    codename_8.save()

    codename_9 = assigned_test_status(codename="submitted")
    codename_9.save()

    codename_10 = assigned_test_status(codename="quit")
    codename_10.save()


def rollback_changes(apps, schema_editor):
    # get models
    assigned_test_status = apps.get_model("custom_models", "assignedteststatus")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete all codenames
    assigned_test_status.objects.using(db_alias).filter(codename="assigned").delete()

    assigned_test_status.objects.using(db_alias).filter(codename="checked_in").delete()

    assigned_test_status.objects.using(db_alias).filter(codename="unassigned").delete()

    assigned_test_status.objects.using(db_alias).filter(codename="pre_test").delete()

    assigned_test_status.objects.using(db_alias).filter(codename="active").delete()

    assigned_test_status.objects.using(db_alias).filter(codename="transition").delete()

    assigned_test_status.objects.using(db_alias).filter(codename="locked").delete()

    assigned_test_status.objects.using(db_alias).filter(codename="paused").delete()

    assigned_test_status.objects.using(db_alias).filter(codename="submitted").delete()

    assigned_test_status.objects.using(db_alias).filter(codename="quit").delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0076_assignedteststatus_historicalassignedteststatus_and_more",
        )
    ]
    operations = [migrations.RunPython(create_new_codenames, rollback_changes)]
