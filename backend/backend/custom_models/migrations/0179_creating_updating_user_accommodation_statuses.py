from django.db import migrations
from backend.static.languages import Language_id


NEW_OLD_NAME_EN = "NEW"
NEW_OLD_NAME_FR = "NOUVEAU"
NEW_NEW_NAME_EN = "NEW"
NEW_NEW_NAME_FR = "NOUVELLE"

WAITING_FOR_FINANCIAL_CODING_OLD_NAME_EN = "WAITING FOR FINANCIAL CODING"
WAITING_FOR_FINANCIAL_CODING_OLD_NAME_FR = "EN ATTENTE DE CODES FINANCIERS"
WAITING_FOR_FINANCIAL_CODING_NEW_NAME_EN = "WAITING FOR FINANCIAL CODING"
WAITING_FOR_FINANCIAL_CODING_NEW_NAME_FR = "EN ATTENTE DES CODES FINANCIERS"

WAITING_FOR_DOCUMENTATION_OLD_NAME_EN = "WAITING FOR DOCUMENTATION"
WAITING_FOR_DOCUMENTATION_OLD_NAME_FR = "EN ATTENTE DE DOCUMENTATION"
WAITING_FOR_DOCUMENTATION_NEW_NAME_EN = "WAITING FOR DOCUMENTATION"
WAITING_FOR_DOCUMENTATION_NEW_NAME_FR = "EN ATTENTE DE DOCUMENTATION"

READY_OLD_NAME_EN = "READY"
READY_OLD_NAME_FR = "PRÊTE"
READY_NEW_NAME_EN = "READY"
READY_NEW_NAME_FR = "PRÊTE"

IN_PROGRESS_OLD_NAME_EN = "IN PROGRESS"
IN_PROGRESS_OLD_NAME_FR = "EN COURS"
IN_PROGRESS_NEW_NAME_EN = "IN PROGRESS"
IN_PROGRESS_NEW_NAME_FR = "EN COURS"

ON_HOLD_OLD_NAME_EN = "ON HOLD"
ON_HOLD_OLD_NAME_FR = "EN ATTENTE"
ON_HOLD_NEW_NAME_EN = "ON HOLD"
ON_HOLD_NEW_NAME_FR = "EN ATTENTE"

RE_OPENED_OLD_NAME_EN = "RE-OPENED"
RE_OPENED_OLD_NAME_FR = "RÉOUVERTE"
RE_OPENED_NEW_NAME_EN = "RE-OPENED"
RE_OPENED_NEW_NAME_FR = "RÉOUVERTE"

RE_OPENED_HOLD_OLD_NAME_EN = "RE-OPENED HOLD"
RE_OPENED_HOLD_OLD_NAME_FR = "RÉOUVERTE ET EN ATTENTE"
RE_OPENED_HOLD_NEW_NAME_EN = "RE-OPENED HOLD"
RE_OPENED_HOLD_NEW_NAME_FR = "RÉOUVERTE ET EN ATTENTE"

CANCELLED_BY_SPECIALIST_OLD_NAME_EN = "CANCELLED BY SPECIALIST"
CANCELLED_BY_SPECIALIST_OLD_NAME_FR = "ANNULÉE PAR LE SPÉCIALISTE"
CANCELLED_BY_SPECIALIST_NEW_NAME_EN = "CANCELLED BY SPECIALIST"
CANCELLED_BY_SPECIALIST_NEW_NAME_FR = "ANNULÉE PAR LE SPÉCIALISTE"

COMPLETED_OLD_NAME_EN = "COMPLETED"
COMPLETED_OLD_NAME_FR = "COMPLÉTÉE"
COMPLETED_NEW_NAME_EN = "COMPLETED"
COMPLETED_NEW_NAME_FR = "COMPLÉTÉE"

CANCELLED_BY_CANDIDATE_OLD_NAME_EN = "CANCELLED BY CANDIDATE"
CANCELLED_BY_CANDIDATE_OLD_NAME_FR = "ANNULÉE PAR LE CANDIDAT"
CANCELLED_BY_CANDIDATE_NEW_NAME_EN = "CANCELLED BY CANDIDATE"
CANCELLED_BY_CANDIDATE_NEW_NAME_FR = "ANNULÉE PAR LE CANDIDAT"

EXPIRED_OLD_NAME_EN = "EXPIRED"
EXPIRED_OLD_NAME_FR = "EXPIRÉE"
EXPIRED_NEW_NAME_EN = "EXPIRED"
EXPIRED_NEW_NAME_FR = "EXPIRÉE"

PENDING_CANDIDATE_AGREEMENT_OLD_NAME_EN = "PENDING APPROVAL"
PENDING_CANDIDATE_AGREEMENT_OLD_NAME_FR = "FR PENDING APPROVAL"
PENDING_CANDIDATE_AGREEMENT_NEW_NAME_EN = "PENDING CANDIDATE AGREEMENT"
PENDING_CANDIDATE_AGREEMENT_NEW_NAME_FR = "EN ATTENTE DE L'ACCORD DU CANDIDAT"


def create_user_accommodation_statuses(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )

    # get db alias
    db_alias = schema_editor.connection.alias

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(
        codename="waiting_for_financial_coding"
    )
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = WAITING_FOR_FINANCIAL_CODING_NEW_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = WAITING_FOR_FINANCIAL_CODING_NEW_NAME_EN
    status_text_en.save()

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(
        codename="waiting_for_documentation"
    )
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = WAITING_FOR_DOCUMENTATION_NEW_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = WAITING_FOR_DOCUMENTATION_NEW_NAME_EN
    status_text_en.save()

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(codename="ready")
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = READY_NEW_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = READY_NEW_NAME_EN
    status_text_en.save()

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(
        codename="in_progress"
    )
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = IN_PROGRESS_NEW_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = IN_PROGRESS_NEW_NAME_EN
    status_text_en.save()

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(codename="on_hold")
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = ON_HOLD_NEW_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = ON_HOLD_NEW_NAME_EN
    status_text_en.save()

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(codename="re_opened")
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = RE_OPENED_NEW_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = RE_OPENED_NEW_NAME_EN
    status_text_en.save()

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(
        codename="re_opened_hold"
    )
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = RE_OPENED_HOLD_NEW_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = RE_OPENED_HOLD_NEW_NAME_EN
    status_text_en.save()

    # update cancelled by specialist
    status = user_accommodation_status.objects.using(db_alias).get(codename="cancelled")
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = CANCELLED_BY_SPECIALIST_NEW_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = CANCELLED_BY_CANDIDATE_NEW_NAME_EN
    status_text_en.save()

    # update completed
    status = user_accommodation_status.objects.using(db_alias).get(codename="completed")
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = COMPLETED_NEW_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = COMPLETED_NEW_NAME_EN
    status_text_en.save()

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(
        codename="cancelled_by_candidate"
    )
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = CANCELLED_BY_CANDIDATE_NEW_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = CANCELLED_BY_CANDIDATE_NEW_NAME_EN
    status_text_en.save()

    # update expired
    status = user_accommodation_status.objects.using(db_alias).get(codename="expired")
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = EXPIRED_NEW_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = EXPIRED_NEW_NAME_EN
    status_text_en.save()

    # update pending approval
    status = user_accommodation_status.objects.using(db_alias).get(
        codename="pending_approval"
    )
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = PENDING_CANDIDATE_AGREEMENT_NEW_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = PENDING_CANDIDATE_AGREEMENT_NEW_NAME_EN
    status_text_en.save()


def rollback_changes(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )

    # get db alias
    db_alias = schema_editor.connection.alias

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(
        codename="waiting_for_financial_coding"
    )
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = WAITING_FOR_FINANCIAL_CODING_OLD_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = WAITING_FOR_FINANCIAL_CODING_OLD_NAME_EN
    status_text_en.save()

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(
        codename="waiting_for_documentation"
    )
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = WAITING_FOR_DOCUMENTATION_OLD_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = WAITING_FOR_DOCUMENTATION_OLD_NAME_EN
    status_text_en.save()

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(codename="ready")
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = READY_OLD_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = READY_OLD_NAME_EN
    status_text_en.save()

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(
        codename="in_progress"
    )
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = IN_PROGRESS_OLD_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = IN_PROGRESS_OLD_NAME_EN
    status_text_en.save()

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(codename="on_hold")
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = ON_HOLD_OLD_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = ON_HOLD_OLD_NAME_EN
    status_text_en.save()

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(codename="re_opened")
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = RE_OPENED_OLD_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = RE_OPENED_OLD_NAME_EN
    status_text_en.save()

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(
        codename="re_opened_hold"
    )
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = RE_OPENED_HOLD_OLD_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = RE_OPENED_HOLD_OLD_NAME_EN
    status_text_en.save()

    # update cancelled by specialist
    status = user_accommodation_status.objects.using(db_alias).get(codename="cancelled")
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = CANCELLED_BY_SPECIALIST_OLD_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = CANCELLED_BY_SPECIALIST_OLD_NAME_EN
    status_text_en.save()

    # update completed
    status = user_accommodation_status.objects.using(db_alias).get(codename="completed")
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = COMPLETED_OLD_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = COMPLETED_OLD_NAME_EN
    status_text_en.save()

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(
        codename="cancelled_by_candidate"
    )
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = CANCELLED_BY_CANDIDATE_OLD_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = CANCELLED_BY_CANDIDATE_OLD_NAME_EN
    status_text_en.save()

    # update expired
    status = user_accommodation_status.objects.using(db_alias).get(codename="expired")
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = EXPIRED_OLD_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = EXPIRED_OLD_NAME_EN
    status_text_en.save()

    # update pending approval
    status = user_accommodation_status.objects.using(db_alias).get(
        codename="pending_approval"
    )
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = PENDING_CANDIDATE_AGREEMENT_OLD_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = PENDING_CANDIDATE_AGREEMENT_OLD_NAME_EN
    status_text_en.save()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0178_updating_user_accommodation_complexities_texts",
        )
    ]
    operations = [
        migrations.RunPython(create_user_accommodation_statuses, rollback_changes)
    ]
