from django.db import migrations


def create_new_and_update_reasons_for_testing(apps, schema_editor):
    # get models
    reason_for_testing = apps.get_model("custom_models", "reasonsfortesting")
    # get db alias
    db_alias = schema_editor.connection.alias

    # creating all needed reasons for testing
    reason_1 = reason_for_testing(
        description_en="Staffing / Dotation",
        description_fr="Dotation / Staffing",
        active=1,
    )
    reason_1.save()
    reason_2 = reason_for_testing(
        description_en="Language Training / Formation linguistique",
        description_fr="Formation linguistique / Language Training",
        active=1,
    )
    reason_2.save()
    reason_3 = reason_for_testing(
        description_en="Other / Autre",
        description_fr="Autre / Other",
        active=1,
    )
    reason_3.save()

    # updating existing reasons for testing
    reason_to_update_1 = reason_for_testing.objects.using(db_alias).get(
        description_en="Imperative staffing"
    )
    reason_to_update_1.active = 0
    reason_to_update_1.save()
    reason_to_update_2 = reason_for_testing.objects.using(db_alias).get(
        description_en="Non-imperative staffing"
    )
    reason_to_update_2.active = 0
    reason_to_update_2.save()
    reason_to_update_3 = reason_for_testing.objects.using(db_alias).get(
        description_en="Full-time language training from departmental program"
    )
    reason_to_update_3.active = 0
    reason_to_update_3.save()
    reason_to_update_4 = reason_for_testing.objects.using(db_alias).get(
        description_en="Part-time language training from departmental program"
    )
    reason_to_update_4.active = 0
    reason_to_update_4.save()
    reason_to_update_5 = reason_for_testing.objects.using(db_alias).get(
        description_en="Re-identification"
    )
    reason_to_update_5.active = 0
    reason_to_update_5.save()
    reason_to_update_6 = reason_for_testing.objects.using(db_alias).get(
        description_en="Bilingualism bonus"
    )
    reason_to_update_6.active = 0
    reason_to_update_6.save()
    reason_to_update_7 = reason_for_testing.objects.using(db_alias).get(
        description_en="Record or other purposes"
    )
    reason_to_update_7.active = 0
    reason_to_update_7.save()
    reason_to_update_8 = reason_for_testing.objects.using(db_alias).get(
        description_en="COVID-19 or Essential Service"
    )
    reason_to_update_8.active = 0
    reason_to_update_8.save()


def rollback_changes(apps, schema_editor):
    # get models
    reason_for_testing = apps.get_model("custom_models", "reasonsfortesting")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the reasons
    reason_for_testing.objects.using(db_alias).filter(
        description_en="Staffing / Dotation"
    ).delete()
    reason_for_testing.objects.using(db_alias).filter(
        description_en="Language Training / Formation linguistique"
    ).delete()
    reason_for_testing.objects.using(db_alias).filter(
        description_en="Other / Autre"
    ).delete()
    # update the reasons
    reason_to_update_1 = reason_for_testing.objects.using(db_alias).get(
        description_en="Imperative staffing"
    )
    reason_to_update_1.active = 1
    reason_to_update_1.save()
    reason_to_update_2 = reason_for_testing.objects.using(db_alias).get(
        description_en="Non-imperative staffing"
    )
    reason_to_update_2.active = 1
    reason_to_update_2.save()
    reason_to_update_3 = reason_for_testing.objects.using(db_alias).get(
        description_en="Full-time language training from departmental program"
    )
    reason_to_update_3.active = 1
    reason_to_update_3.save()
    reason_to_update_4 = reason_for_testing.objects.using(db_alias).get(
        description_en="Part-time language training from departmental program"
    )
    reason_to_update_4.active = 1
    reason_to_update_4.save()
    reason_to_update_5 = reason_for_testing.objects.using(db_alias).get(
        description_en="Re-identification"
    )
    reason_to_update_5.active = 1
    reason_to_update_5.save()
    reason_to_update_6 = reason_for_testing.objects.using(db_alias).get(
        description_en="Bilingualism bonus"
    )
    reason_to_update_6.active = 1
    reason_to_update_6.save()
    reason_to_update_7 = reason_for_testing.objects.using(db_alias).get(
        description_en="Record or other purposes"
    )
    reason_to_update_7.active = 1
    reason_to_update_7.save()
    reason_to_update_8 = reason_for_testing.objects.using(db_alias).get(
        description_en="COVID-19 or Essential Service"
    )
    reason_to_update_8.active = 1
    reason_to_update_8.save()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0032_auto_20220613_1020")]
    operations = [
        migrations.RunPython(
            create_new_and_update_reasons_for_testing, rollback_changes
        )
    ]
