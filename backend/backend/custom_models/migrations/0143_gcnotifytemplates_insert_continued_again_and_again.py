from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, _):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # creating all needed settings
    tempplate_1 = gc_notify_template(
        folder="Accommodations",
        codename=GCNotifyTemplateCodenames.PENDING_APPROVAL_ACCOMODATIONS_REQUEST,
        template_id="bb4891f8-6648-4d95-bd9d-29fabcd6c926",
    )
    tempplate_1.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the setting
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.PENDING_APPROVAL_ACCOMODATIONS_REQUEST
    ).delete()


class Migration(migrations.Migration):
    dependencies = [
        ("custom_models", "0142_creating_updating_user_accommodation_statuses")
    ]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
