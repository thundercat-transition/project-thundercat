# Generated by Django 4.2.11 on 2024-07-18 15:29

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import simple_history.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("custom_models", "0143_gcnotifytemplates_insert_continued_again_and_again"),
    ]

    operations = [
        migrations.CreateModel(
            name="HistoricalUserAccommodationFileComplexity",
            fields=[
                (
                    "id",
                    models.IntegerField(
                        auto_created=True, blank=True, db_index=True, verbose_name="ID"
                    ),
                ),
                ("codename", models.CharField(max_length=50)),
                ("active", models.BooleanField(default=1)),
                ("order", models.IntegerField(default=0)),
                ("history_id", models.AutoField(primary_key=True, serialize=False)),
                ("history_date", models.DateTimeField(db_index=True)),
                ("history_change_reason", models.CharField(max_length=100, null=True)),
                (
                    "history_type",
                    models.CharField(
                        choices=[("+", "Created"), ("~", "Changed"), ("-", "Deleted")],
                        max_length=1,
                    ),
                ),
            ],
            options={
                "verbose_name": "historical user accommodation file complexity",
                "verbose_name_plural": "historical user accommodation file complexitys",
                "ordering": ("-history_date", "-history_id"),
                "get_latest_by": ("history_date", "history_id"),
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name="HistoricalUserAccommodationFileComplexityText",
            fields=[
                (
                    "id",
                    models.IntegerField(
                        auto_created=True, blank=True, db_index=True, verbose_name="ID"
                    ),
                ),
                ("text", models.CharField(max_length=50)),
                ("history_id", models.AutoField(primary_key=True, serialize=False)),
                ("history_date", models.DateTimeField(db_index=True)),
                ("history_change_reason", models.CharField(max_length=100, null=True)),
                (
                    "history_type",
                    models.CharField(
                        choices=[("+", "Created"), ("~", "Changed"), ("-", "Deleted")],
                        max_length=1,
                    ),
                ),
            ],
            options={
                "verbose_name": "historical user accommodation file complexity text",
                "verbose_name_plural": "historical user accommodation file complexity texts",
                "ordering": ("-history_date", "-history_id"),
                "get_latest_by": ("history_date", "history_id"),
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name="UserAccommodationFileComplexity",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("codename", models.CharField(max_length=50)),
                ("active", models.BooleanField(default=1)),
                ("order", models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name="UserAccommodationFileComplexityText",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("text", models.CharField(max_length=50)),
                (
                    "language",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="custom_models.language",
                    ),
                ),
                (
                    "user_accommodation_file_complexity",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="custom_models.useraccommodationfilecomplexity",
                    ),
                ),
            ],
        ),
        migrations.AddConstraint(
            model_name="useraccommodationfilecomplexity",
            constraint=models.UniqueConstraint(
                fields=("codename",),
                name="must_be_a_unique_user_accommodation_file_complexity_codename",
            ),
        ),
        migrations.AddField(
            model_name="historicaluseraccommodationfilecomplexitytext",
            name="history_user",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="+",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AddField(
            model_name="historicaluseraccommodationfilecomplexitytext",
            name="language",
            field=models.ForeignKey(
                blank=True,
                db_constraint=False,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                related_name="+",
                to="custom_models.language",
            ),
        ),
        migrations.AddField(
            model_name="historicaluseraccommodationfilecomplexitytext",
            name="user_accommodation_file_complexity",
            field=models.ForeignKey(
                blank=True,
                db_constraint=False,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                related_name="+",
                to="custom_models.useraccommodationfilecomplexity",
            ),
        ),
        migrations.AddField(
            model_name="historicaluseraccommodationfilecomplexity",
            name="history_user",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="+",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AddField(
            model_name="historicaluseraccommodationfile",
            name="complexity",
            field=models.ForeignKey(
                blank=True,
                db_constraint=False,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                related_name="+",
                to="custom_models.useraccommodationfilecomplexity",
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfile",
            name="complexity",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.useraccommodationfilecomplexity",
            ),
        ),
    ]
