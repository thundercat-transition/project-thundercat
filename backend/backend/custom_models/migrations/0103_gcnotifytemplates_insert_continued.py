from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, _):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # creating all needed templates
    tempplate_1 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.REVOKE_ASSIGNED_TEST_TO_ADMINISTER,
        template_id="10ada26d-ff5f-48f0-b09c-e5167726a7b3",
    )
    tempplate_1.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the templates
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.REVOKE_ASSIGNED_TEST_TO_ADMINISTER
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0102_add_newconsumed_reservationcode_status")]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
