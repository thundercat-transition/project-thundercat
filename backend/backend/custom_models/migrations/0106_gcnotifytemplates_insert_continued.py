from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, _):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # creating all needed templates
    tempplate_1 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.UPDATE_ACTIVE_ASSESSMENT_PROCESS_DATA,
        template_id="7c2a7e86-59c3-4a20-ab72-1da8d7137590",
    )
    tempplate_1.save()
    tempplate_2 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.TEST_SESSION_CANCELLATION_BASED_ON_ASSESSMENT_PROCESS_DATA_UPDATES,
        template_id="00c415c3-5bab-4bbd-ad0a-0624f0f7bcf7",
    )
    tempplate_2.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the templates
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.UPDATE_ACTIVE_ASSESSMENT_PROCESS_DATA
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.TEST_SESSION_CANCELLATION_BASED_ON_ASSESSMENT_PROCESS_DATA_UPDATES
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0105_gcnotifytemplates_insert_continued_again")]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
