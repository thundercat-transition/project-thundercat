from django.db import migrations
from backend.static.site_admin_setting_type import SiteAdminSettingType, Key


def delete_old_admin_settings(apps, schema_editor):
    # get models
    site_admin_setting = apps.get_model("custom_models", "siteadminsetting")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete unneeded settings
    site_admin_setting.objects.using(db_alias).filter(
        codename="msteamsapiuserid"
    ).delete()
    site_admin_setting.objects.using(db_alias).filter(
        codename="msteamsapiappname"
    ).delete()


def rollback_changes(apps, _):
    # get models
    site_admin_setting = apps.get_model("custom_models", "siteadminsetting")
    # create old settings
    setting_1 = site_admin_setting(
        en_description="Teams App Name",
        fr_description="FR Teams App Name",
        codename="msteamsapiappname",
        is_active=False,
    )
    setting_1.save()
    setting_2 = site_admin_setting(
        en_description="Teams User ID",
        fr_description="FR Teams User ID",
        codename="msteamsapiuserid",
        is_active=False,
    )
    setting_2.save()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0184_updating_user_accommodation_complexities_texts",
        )
    ]
    operations = [migrations.RunPython(delete_old_admin_settings, rollback_changes)]
