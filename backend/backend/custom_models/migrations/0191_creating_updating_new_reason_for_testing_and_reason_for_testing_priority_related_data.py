from django.db import migrations
from backend.static.reason_for_testing import (
    ReasonForTestingCodenameConst,
    ReasonForTestingPriorityCodenameConst,
)
from backend.static.languages import Language_id


def update_reason_for_testing_and_reason_for_testing_priority_related_data(
    apps, schema_editor
):
    # get models
    reason_for_testing = apps.get_model("custom_models", "reasonfortesting")
    reason_for_testing_priority = apps.get_model(
        "custom_models", "reasonfortestingpriority"
    )
    reason_for_testing_priority_text = apps.get_model(
        "custom_models", "reasonfortestingprioritytext"
    )

    # get db alias
    db_alias = schema_editor.connection.alias

    # creating new reason for testing priority related data
    # ==================== REASON FOR TESTING PRIORITY RELATED DATA ====================
    # HIGH
    reason_for_testing_priority_high = reason_for_testing_priority(
        codename=ReasonForTestingPriorityCodenameConst.HIGH,
    )
    reason_for_testing_priority_high.save()

    reason_for_testing_priority_high_text = reason_for_testing_priority_text(
        reason_for_testing_priority_id=reason_for_testing_priority_high.id,
        language_id=Language_id.EN,
        text="High",
    )
    reason_for_testing_priority_high_text.save()

    reason_for_testing_priority_high_text = reason_for_testing_priority_text(
        reason_for_testing_priority_id=reason_for_testing_priority_high.id,
        language_id=Language_id.FR,
        text="FR High",
    )
    reason_for_testing_priority_high_text.save()

    # MEDIUM
    reason_for_testing_priority_medium = reason_for_testing_priority(
        codename=ReasonForTestingPriorityCodenameConst.MEDIUM,
    )
    reason_for_testing_priority_medium.save()

    reason_for_testing_priority_medium_text = reason_for_testing_priority_text(
        reason_for_testing_priority_id=reason_for_testing_priority_medium.id,
        language_id=Language_id.EN,
        text="Medium",
    )
    reason_for_testing_priority_medium_text.save()

    reason_for_testing_priority_medium_text = reason_for_testing_priority_text(
        reason_for_testing_priority_id=reason_for_testing_priority_medium.id,
        language_id=Language_id.FR,
        text="FR Medium",
    )
    reason_for_testing_priority_medium_text.save()

    # LOW
    reason_for_testing_priority_low = reason_for_testing_priority(
        codename=ReasonForTestingPriorityCodenameConst.LOW,
    )
    reason_for_testing_priority_low.save()

    reason_for_testing_priority_low_text = reason_for_testing_priority_text(
        reason_for_testing_priority_id=reason_for_testing_priority_low.id,
        language_id=Language_id.EN,
        text="Low",
    )
    reason_for_testing_priority_low_text.save()

    reason_for_testing_priority_low_text = reason_for_testing_priority_text(
        reason_for_testing_priority_id=reason_for_testing_priority_low.id,
        language_id=Language_id.FR,
        text="FR Low",
    )
    reason_for_testing_priority_low_text.save()
    # ==================== REASON FOR TESTING PRIORITY RELATED DATA (END) ====================

    # updating waiting period and reason for testing priority for OLA reasons
    # ==================== REASON FOR TESTING RELATED DATA ====================
    # imp_staffing_ola
    reason = reason_for_testing.objects.using(db_alias).get(
        codename=ReasonForTestingCodenameConst.IMP_STAFFING_OLA
    )
    reason.waiting_period = 0
    reason.reason_for_testing_priority_id = reason_for_testing_priority_high.id
    reason.save()

    # language_training_ola
    reason = reason_for_testing.objects.using(db_alias).get(
        codename=ReasonForTestingCodenameConst.LANGUAGE_TRAINING_OLA
    )
    reason.waiting_period = 15
    reason.reason_for_testing_priority_id = reason_for_testing_priority_medium.id
    reason.save()

    # creation_of_partially_assessed_pool
    reason = reason_for_testing.objects.using(db_alias).get(
        codename=ReasonForTestingCodenameConst.CREATION_OF_PARTIALLY_ASSESSED_POOL_OLA
    )
    reason.waiting_period = 30
    reason.reason_for_testing_priority_id = reason_for_testing_priority_low.id
    reason.save()

    # non_imp_staffing_ola
    reason = reason_for_testing.objects.using(db_alias).get(
        codename=ReasonForTestingCodenameConst.NON_IMP_STAFFING_OLA
    )
    reason.waiting_period = 30
    reason.reason_for_testing_priority_id = reason_for_testing_priority_low.id
    reason.save()

    # re_identification_ola
    reason = reason_for_testing.objects.using(db_alias).get(
        codename=ReasonForTestingCodenameConst.RE_IDENTIFICATION_OLA
    )
    reason.waiting_period = 30
    reason.reason_for_testing_priority_id = reason_for_testing_priority_low.id
    reason.save()

    # record_or_other_purposes_ola
    reason = reason_for_testing.objects.using(db_alias).get(
        codename=ReasonForTestingCodenameConst.RECORD_OR_OTHER_PURPOSES_OLA
    )
    reason.waiting_period = 30
    reason.reason_for_testing_priority_id = reason_for_testing_priority_low.id
    reason.save()

    # bilingualism_bonus_ola
    reason = reason_for_testing.objects.using(db_alias).get(
        codename=ReasonForTestingCodenameConst.BILINGUALISM_BONUS_OLA
    )
    reason.waiting_period = 30
    reason.reason_for_testing_priority_id = reason_for_testing_priority_low.id
    reason.save()
    # ==================== REASON FOR TESTING RELATED DATA (END) ====================


def rollback_changes(apps, schema_editor):
    # get models
    reason_for_testing = apps.get_model("custom_models", "reasonfortesting")
    reason_for_testing_priority = apps.get_model(
        "custom_models", "reasonfortestingpriority"
    )
    reason_for_testing_priority_text = apps.get_model(
        "custom_models", "reasonfortestingprioritytext"
    )

    # get db alias
    db_alias = schema_editor.connection.alias

    # put back old waiting period and reason for testing priority data (basically set them back to NULL)
    reasons = reason_for_testing.objects.using(db_alias).filter(
        codename__in=(
            ReasonForTestingCodenameConst.IMP_STAFFING_OLA,
            ReasonForTestingCodenameConst.LANGUAGE_TRAINING_OLA,
            ReasonForTestingCodenameConst.CREATION_OF_PARTIALLY_ASSESSED_POOL_OLA,
            ReasonForTestingCodenameConst.NON_IMP_STAFFING_OLA,
            ReasonForTestingCodenameConst.RE_IDENTIFICATION_OLA,
            ReasonForTestingCodenameConst.RECORD_OR_OTHER_PURPOSES_OLA,
            ReasonForTestingCodenameConst.BILINGUALISM_BONUS_OLA,
        )
    )

    for reason_for_testing in reasons:
        reason_for_testing.waiting_period = None
        reason_for_testing.reason_for_testing_priority_id = None
        reason_for_testing.save()

    # deleting all reason for testing priority related data
    reason_for_testing_priority_text.objects.using(db_alias).all().delete()
    reason_for_testing_priority.objects.using(db_alias).all().delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0190_reasonfortestingpriority_and_more",
        )
    ]
    operations = [
        migrations.RunPython(
            update_reason_for_testing_and_reason_for_testing_priority_related_data,
            rollback_changes,
        )
    ]
