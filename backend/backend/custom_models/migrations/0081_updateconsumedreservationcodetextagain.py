from django.db import migrations
from backend.static.consumed_reservation_code_statuses import (
    StaticConsumedReservationCodeStatus,
)


def update_consumed_reservation_code_status(apps, schema_editor):
    # get models
    consumed_reservation_code_status = apps.get_model(
        "custom_models", "consumedreservationcodestatus"
    )
    consumed_reservation_code_status_text = apps.get_model(
        "custom_models", "consumedreservationcodestatustext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # updating consumed reservation code status code_names
    status_1 = consumed_reservation_code_status.objects.using(db_alias).get(
        codename="booklater"
    )
    status_1.codename = StaticConsumedReservationCodeStatus.RESERVED
    status_1.save()

    # updating consumed reservation code status text
    text_1_en = consumed_reservation_code_status_text.objects.using(db_alias).get(
        consumed_reservation_code_status_id=status_1.id, language_id=1
    )
    text_1_en.text = "Reserved"
    text_1_en.save()

    text_1_fr = consumed_reservation_code_status_text.objects.using(db_alias).get(
        consumed_reservation_code_status_id=status_1.id, language_id=2
    )
    text_1_fr.text = "FR Reserved"
    text_1_fr.save()


def rollback_changes(apps, schema_editor):
    # get models
    consumed_reservation_code_status = apps.get_model(
        "custom_models", "consumedreservationcodestatus"
    )
    consumed_reservation_code_status_text = apps.get_model(
        "custom_models", "consumedreservationcodestatustext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # updating consumed reservation code status code_names
    status_1 = consumed_reservation_code_status.objects.using(db_alias).get(
        codename=StaticConsumedReservationCodeStatus.RESERVED
    )
    status_1.codename = "booklater"
    status_1.save()

    # updating consumed reservation code status text
    text_1_en = consumed_reservation_code_status_text.objects.using(db_alias).get(
        consumed_reservation_code_status_id=status_1.id, language_id=1
    )
    text_1_en.text = "Booklater"
    text_1_en.save()

    text_1_fr = consumed_reservation_code_status_text.objects.using(db_alias).get(
        consumed_reservation_code_status_id=status_1.id, language_id=2
    )
    text_1_fr.text = "FR Booklater"
    text_1_fr.save()


class Migration(migrations.Migration):
    dependencies = [
        ("custom_models", "0080_refactoring_assigned_test_status_and_previous_status")
    ]
    operations = [
        migrations.RunPython(update_consumed_reservation_code_status, rollback_changes)
    ]
