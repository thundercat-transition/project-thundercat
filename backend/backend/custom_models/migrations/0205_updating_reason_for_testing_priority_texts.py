from django.db import migrations
from backend.static.languages import Language_id

# text translations
LOW_TEXT_FR_OLD = "FR Low"
LOW_TEXT_FR_NEW = "Faible"

MEDIUM_TEXT_FR_OLD = "FR Meduim"
MEDIUM_TEXT_FR_NEW = "Moyenne"

HIGH_TEXT_FR_OLD = "FR High"
HIGH_TEXT_FR_NEW = "Haute"


def update_reason_for_testing_priority(apps, schema_editor):
    # get models
    reason_for_testing_priority = apps.get_model(
        "custom_models", "reasonfortestingpriority"
    )
    reason_for_testing_priority_text = apps.get_model(
        "custom_models", "reasonfortestingprioritytext"
    )

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # update low
    complexity = reason_for_testing_priority.objects.using(db_alias).get(codename="low")
    complexity_text_fr = reason_for_testing_priority_text.objects.using(db_alias).get(
        reason_for_testing_priority_id=complexity.id,
        language_id=Language_id.FR,
    )
    complexity_text_fr.text = LOW_TEXT_FR_NEW
    complexity_text_fr.save()

    # update medium
    complexity = reason_for_testing_priority.objects.using(db_alias).get(
        codename="medium"
    )
    complexity_text_fr = reason_for_testing_priority_text.objects.using(db_alias).get(
        reason_for_testing_priority_id=complexity.id,
        language_id=Language_id.FR,
    )
    complexity_text_fr.text = MEDIUM_TEXT_FR_NEW
    complexity_text_fr.save()

    # update high
    complexity = reason_for_testing_priority.objects.using(db_alias).get(
        codename="high"
    )
    complexity_text_fr = reason_for_testing_priority_text.objects.using(db_alias).get(
        reason_for_testing_priority_id=complexity.id,
        language_id=Language_id.FR,
    )
    complexity_text_fr.text = HIGH_TEXT_FR_NEW
    complexity_text_fr.save()


def rollback_changes(apps, schema_editor):
    # get models
    reason_for_testing_priority = apps.get_model(
        "custom_models", "reasonfortestingpriority"
    )
    reason_for_testing_priority_text = apps.get_model(
        "custom_models", "reasonfortestingprioritytext"
    )

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # update low
    complexity = reason_for_testing_priority.objects.using(db_alias).get(codename="low")
    complexity_text_fr = reason_for_testing_priority_text.objects.using(db_alias).get(
        reason_for_testing_priority_id=complexity.id,
        language_id=Language_id.FR,
    )
    complexity_text_fr.text = LOW_TEXT_FR_OLD
    complexity_text_fr.save()

    # update medium
    complexity = reason_for_testing_priority.objects.using(db_alias).get(
        codename="medium"
    )
    complexity_text_fr = reason_for_testing_priority_text.objects.using(db_alias).get(
        reason_for_testing_priority_id=complexity.id,
        language_id=Language_id.FR,
    )
    complexity_text_fr.text = MEDIUM_TEXT_FR_OLD
    complexity_text_fr.save()

    # update high
    complexity = reason_for_testing_priority.objects.using(db_alias).get(
        codename="high"
    )
    complexity_text_fr = reason_for_testing_priority_text.objects.using(db_alias).get(
        reason_for_testing_priority_id=complexity.id,
        language_id=Language_id.FR,
    )
    complexity_text_fr.text = HIGH_TEXT_FR_OLD
    complexity_text_fr.save()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0204_creating_updating_user_accommodation_statuses",
        )
    ]
    operations = [
        migrations.RunPython(update_reason_for_testing_priority, rollback_changes)
    ]
