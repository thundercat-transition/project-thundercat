from django.db import migrations
from backend.static.languages import Language_id
from backend.static.consumed_reservation_code_statuses import (
    StaticConsumedReservationCodeStatus,
)


def create_new_consumed_reservation_code_status(apps, schema_editor):
    # get models
    consumed_reservation_code_status = apps.get_model(
        "custom_models", "consumedreservationcodestatus"
    )
    consumed_reservation_code_status_text = apps.get_model(
        "custom_models", "consumedreservationcodestatustext"
    )

    # adding new reservation code status codename
    status_1 = consumed_reservation_code_status(
        codename=StaticConsumedReservationCodeStatus.PAIRED_OLA
    )
    status_1.save()

    # adding new consumed reservation code status text
    text_en = consumed_reservation_code_status_text(
        text="Paired for OLA",
        consumed_reservation_code_status_id=status_1.id,
        language_id=Language_id.EN,
    )
    text_en.save()

    text_fr = consumed_reservation_code_status_text(
        text="FR Paired for OLA",
        consumed_reservation_code_status_id=status_1.id,
        language_id=Language_id.FR,
    )
    text_fr.save()


def rollback_changes(apps, schema_editor):
    # get models
    consumed_reservation_code_status = apps.get_model(
        "custom_models", "consumedreservationcodestatus"
    )
    consumed_reservation_code_status_text = apps.get_model(
        "custom_models", "consumedreservationcodestatustext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # deleting needed data
    status_1 = consumed_reservation_code_status.objects.using(db_alias).get(
        codename=StaticConsumedReservationCodeStatus.PAIRED_OLA
    )

    consumed_reservation_code_status_text.objects.using(db_alias).get(
        consumed_reservation_code_status_id=status_1.id, language_id=Language_id.EN
    ).delete()
    consumed_reservation_code_status_text.objects.using(db_alias).get(
        consumed_reservation_code_status_id=status_1.id, language_id=Language_id.FR
    ).delete()

    status_1.delete()


class Migration(migrations.Migration):
    dependencies = [
        ("custom_models", "0168_creating_new_scorer_ola_test_session_skip_option_data")
    ]
    operations = [
        migrations.RunPython(
            create_new_consumed_reservation_code_status, rollback_changes
        )
    ]
