from django.db import migrations
from backend.static.languages import Language_id
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_user_accommodation_statuses(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # creating all needed codenames
    codename_1 = user_accommodation_status(codename="cancelled_by_candidate", order=12)
    codename_1.save()

    # creating all needed status text
    status_text_1_en = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_1.id,
        text="CANCELLED BY CANDIDATE",
        language_id=Language_id.EN,
    )
    status_text_1_en.save()
    status_text_1_fr = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_1.id,
        text="ANNULÉ PAR LE CANDIDAT",
        language_id=Language_id.FR,
    )
    status_text_1_fr.save()


def rollback_changes(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # getting all respective user accommodation file status entries
    user_accommodation_file_status_to_delete = user_accommodation_status.objects.using(
        db_alias
    ).get(codename="cancelled_by_candidate")

    # Deleting all respective user accommodation file status text entries
    user_accommodation_status_text.objects.using(db_alias).filter(
        user_accommodation_file_status_id=user_accommodation_file_status_to_delete.id
    ).delete()

    # Deleting respective user accommodation file status entry
    user_accommodation_file_status_to_delete.delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0138_create_new_user_accommodation_file_candidate_limitations_and_recommendations",
        )
    ]
    operations = [
        migrations.RunPython(create_user_accommodation_statuses, rollback_changes)
    ]
