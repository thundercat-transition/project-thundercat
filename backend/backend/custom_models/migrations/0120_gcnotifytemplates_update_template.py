from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # get the existing template
    template_1 = (
        gc_notify_template.objects.using(db_alias)
        .filter(
            codename=GCNotifyTemplateCodenames.TEST_SESSION_CANCELLATION_INFORM_RELATED_HR_COORDINATOR
        )
        .last()
    )
    # switch to point to the new teamplate id
    template_1.template_id = "39ede176-4168-4bf7-b025-b40a07168809"
    template_1.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # get the existing template
    template_1 = (
        gc_notify_template.objects.using(db_alias)
        .filter(
            codename=GCNotifyTemplateCodenames.TEST_SESSION_CANCELLATION_INFORM_RELATED_HR_COORDINATOR
        )
        .last()
    )
    # roll back to the old template id
    template_1.template_id = "582a1806-4b9e-4a82-b4b9-6eeb09ee7f2d"
    template_1.save()


class Migration(migrations.Migration):
    dependencies = [
        ("custom_models", "0119_remove_historicaltestcenter_country_and_more")
    ]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
