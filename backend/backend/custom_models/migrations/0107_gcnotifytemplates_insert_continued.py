from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, _):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # creating all needed templates
    tempplate_1 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.TEST_SESSION_CANCELLATION_INFORM_RELATED_HR_COORDINATOR,
        template_id="582a1806-4b9e-4a82-b4b9-6eeb09ee7f2d",
    )
    tempplate_1.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the templates
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.TEST_SESSION_CANCELLATION_INFORM_RELATED_HR_COORDINATOR
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0106_gcnotifytemplates_insert_continued")]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
