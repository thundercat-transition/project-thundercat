# Generated by Django 4.2.11 on 2024-09-25 10:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('custom_models', '0171_creating_new_scorer_ola_test_session_skip_option_data'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scorerolareasonforunassigningaction',
            name='test_session',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='custom_models.testcentertestsessions'),
        ),
        migrations.AlterField(
            model_name='scorerolatestsessionskipaction',
            name='test_session',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='custom_models.testcentertestsessions'),
        ),
    ]
