# Generated by Django 3.2.15 on 2022-09-22 17:10

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import simple_history.models


class Migration(migrations.Migration):

    dependencies = [
        ('cms_models', '0045_historicalorderlesstestpermissions_orderlesstestpermissions'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('custom_models', '0039_update_criticality_data'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccommodationRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='BreakBank',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('break_time', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='HistoricalBreakBankActions',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('action_type', models.CharField(max_length=15)),
                ('modify_date', models.DateTimeField(blank=True, editable=False)),
                ('new_remaining_time', models.IntegerField(null=True)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('break_bank', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='custom_models.breakbank')),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical break bank actions',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalBreakBank',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('break_time', models.IntegerField()),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical break bank',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalAdditionalTime',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('test_section_time', models.IntegerField()),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('accommodation_request', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='custom_models.accommodationrequest')),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('test_section', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='cms_models.testsection')),
            ],
            options={
                'verbose_name': 'historical additional time',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalAccommodationRequest',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('break_bank', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='custom_models.breakbank')),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical accommodation request',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.CreateModel(
            name='BreakBankActions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('action_type', models.CharField(max_length=15)),
                ('modify_date', models.DateTimeField(auto_now=True)),
                ('new_remaining_time', models.IntegerField(null=True)),
                ('break_bank', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='custom_models.breakbank')),
            ],
        ),
        migrations.CreateModel(
            name='AdditionalTime',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('test_section_time', models.IntegerField()),
                ('accommodation_request', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='custom_models.accommodationrequest')),
                ('test_section', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='cms_models.testsection')),
            ],
        ),
        migrations.AddField(
            model_name='accommodationrequest',
            name='break_bank',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='custom_models.breakbank'),
        ),
        migrations.AddField(
            model_name='assignedtest',
            name='accommodation_request',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='custom_models.accommodationrequest'),
        ),
        migrations.AddField(
            model_name='historicalassignedtest',
            name='accommodation_request',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='custom_models.accommodationrequest'),
        ),
        migrations.AddField(
            model_name='historicalunsupervisedtestaccesscode',
            name='accommodation_request',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='custom_models.accommodationrequest'),
        ),
        migrations.AddField(
            model_name='unsupervisedtestaccesscode',
            name='accommodation_request',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='custom_models.accommodationrequest'),
        ),
    ]
