from django.db import connection
from django.db import migrations
from django.conf import settings


def mapping_new_reason_for_testing_ids(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """UPDATE {db_name}..custom_models_assessmentprocessassignedtestspecs
                SET reason_for_testing_id = (CASE
                                                -- SLE READING
                                                WHEN test_skill_sub_type_id = 1 OR test_skill_sub_type_id = 2
                                                THEN (SELECT reason_for_testing_id 
                                                    FROM {db_name}..custom_models_reasonfortestingtext 
                                                    WHERE reason_for_testing_id IN (SELECT id 
                                                                                    FROM {db_name}..custom_models_reasonfortesting WHERE reason_for_testing_type_id = (SELECT id FROM custom_models_reasonfortestingtype 
                                                                                    WHERE codename = 'reading')) 
                                                                                    AND text = (SELECT description_en FROM {db_name}..custom_models_reasonsfortesting WHERE id = reason_for_testing_old_id
                                                                                    )
                                                    )
                                                -- SLE WRITING
                                                WHEN test_skill_sub_type_id = 3 OR test_skill_sub_type_id = 4
                                                THEN (SELECT reason_for_testing_id 
                                                    FROM {db_name}..custom_models_reasonfortestingtext 
                                                    WHERE reason_for_testing_id IN (SELECT id 
                                                                                    FROM {db_name}..custom_models_reasonfortesting WHERE reason_for_testing_type_id = (SELECT id FROM custom_models_reasonfortestingtype 
                                                                                    WHERE codename = 'writing')) 
                                                                                    AND text = (SELECT description_en FROM {db_name}..custom_models_reasonsfortesting WHERE id = reason_for_testing_old_id
                                                                                    )
                                                    )
                                            END
                                            )
                """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """UPDATE {db_name}..custom_models_historicalassessmentprocessassignedtestspecs
                SET reason_for_testing_id = (CASE
                                                -- SLE READING
                                                WHEN test_skill_sub_type_id = 1 OR test_skill_sub_type_id = 2
                                                THEN (SELECT reason_for_testing_id 
                                                    FROM {db_name}..custom_models_reasonfortestingtext 
                                                    WHERE reason_for_testing_id IN (SELECT id 
                                                                                    FROM {db_name}..custom_models_reasonfortesting WHERE reason_for_testing_type_id = (SELECT id FROM custom_models_reasonfortestingtype 
                                                                                    WHERE codename = 'reading')) 
                                                                                    AND text = (SELECT description_en FROM {db_name}..custom_models_reasonsfortesting WHERE id = reason_for_testing_old_id
                                                                                    )
                                                    )
                                                -- SLE WRITING
                                                WHEN test_skill_sub_type_id = 3 OR test_skill_sub_type_id = 4
                                                THEN (SELECT reason_for_testing_id 
                                                    FROM {db_name}..custom_models_reasonfortestingtext 
                                                    WHERE reason_for_testing_id IN (SELECT id 
                                                                                    FROM {db_name}..custom_models_reasonfortesting WHERE reason_for_testing_type_id = (SELECT id FROM custom_models_reasonfortestingtype 
                                                                                    WHERE codename = 'writing')) 
                                                                                    AND text = (SELECT description_en FROM {db_name}..custom_models_reasonsfortesting WHERE id = reason_for_testing_old_id
                                                                                    )
                                                    )
                                            END
                                            )
                """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """UPDATE {db_name}..custom_models_assessmentprocessdefaulttestspecs
                SET reason_for_testing_id = (CASE
                                                -- SLE READING
                                                WHEN test_skill_sub_type_id = 1 OR test_skill_sub_type_id = 2
                                                THEN (SELECT reason_for_testing_id 
                                                    FROM {db_name}..custom_models_reasonfortestingtext 
                                                    WHERE reason_for_testing_id IN (SELECT id 
                                                                                    FROM {db_name}..custom_models_reasonfortesting WHERE reason_for_testing_type_id = (SELECT id FROM custom_models_reasonfortestingtype 
                                                                                    WHERE codename = 'reading')) 
                                                                                    AND text = (SELECT description_en FROM {db_name}..custom_models_reasonsfortesting WHERE id = reason_for_testing_old_id
                                                                                    )
                                                    )
                                                -- SLE WRITING
                                                WHEN test_skill_sub_type_id = 3 OR test_skill_sub_type_id = 4
                                                THEN (SELECT reason_for_testing_id 
                                                    FROM {db_name}..custom_models_reasonfortestingtext 
                                                    WHERE reason_for_testing_id IN (SELECT id 
                                                                                    FROM {db_name}..custom_models_reasonfortesting WHERE reason_for_testing_type_id = (SELECT id FROM custom_models_reasonfortestingtype 
                                                                                    WHERE codename = 'writing')) 
                                                                                    AND text = (SELECT description_en FROM {db_name}..custom_models_reasonsfortesting WHERE id = reason_for_testing_old_id
                                                                                    )
                                                    )
                                            END
                                            )
                """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """UPDATE {db_name}..custom_models_historicalassessmentprocessdefaulttestspecs
                SET reason_for_testing_id = (CASE
                                                -- SLE READING
                                                WHEN test_skill_sub_type_id = 1 OR test_skill_sub_type_id = 2
                                                THEN (SELECT reason_for_testing_id 
                                                    FROM {db_name}..custom_models_reasonfortestingtext 
                                                    WHERE reason_for_testing_id IN (SELECT id 
                                                                                    FROM {db_name}..custom_models_reasonfortesting WHERE reason_for_testing_type_id = (SELECT id FROM custom_models_reasonfortestingtype 
                                                                                    WHERE codename = 'reading')) 
                                                                                    AND text = (SELECT description_en FROM {db_name}..custom_models_reasonsfortesting WHERE id = reason_for_testing_old_id
                                                                                    )
                                                    )
                                                -- SLE WRITING
                                                WHEN test_skill_sub_type_id = 3 OR test_skill_sub_type_id = 4
                                                THEN (SELECT reason_for_testing_id 
                                                    FROM {db_name}..custom_models_reasonfortestingtext 
                                                    WHERE reason_for_testing_id IN (SELECT id 
                                                                                    FROM {db_name}..custom_models_reasonfortesting WHERE reason_for_testing_type_id = (SELECT id FROM custom_models_reasonfortestingtype 
                                                                                    WHERE codename = 'writing')) 
                                                                                    AND text = (SELECT description_en FROM {db_name}..custom_models_reasonsfortesting WHERE id = reason_for_testing_old_id
                                                                                    )
                                                    )
                                            END
                                            )
                """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """UPDATE {db_name}..custom_models_orderlessfinancialdata
                SET reason_for_testing_id = (CASE
                                                -- SLE READING
                                                WHEN (CASE
                                                        -- SLE TEST SKILL
                                                        WHEN (SELECT test_skill_type_id 
                                                            FROM {db_name}..cms_models_testskill 
                                                            WHERE test_definition_id = (SELECT test_id 
                                                                                        FROM {db_name}..custom_models_uitinvites 
                                                                                        WHERE id = uit_invite_id
                                                                                        )
                                                            ) = 2
                                                        THEN (SELECT test_skill_sle_desc_id FROM {db_name}..cms_models_testskillsle WHERE test_skill_id = (SELECT id 
                                                                                                                                                        FROM {db_name}..cms_models_testskill 
                                                                                                                                                        WHERE test_definition_id = (SELECT test_id 
                                                                                                                                                                                    FROM {db_name}..custom_models_uitinvites 
                                                                                                                                                                                    WHERE id = uit_invite_id
                                                                                                                                                                                )
                                                                                                                                                        )
                                                            )
                                                    END
                                                    ) IN (1, 2)
                                                THEN (SELECT reason_for_testing_id 
                                                    FROM {db_name}..custom_models_reasonfortestingtext 
                                                    WHERE reason_for_testing_id IN (SELECT id 
                                                                                    FROM {db_name}..custom_models_reasonfortesting WHERE reason_for_testing_type_id = (SELECT id FROM custom_models_reasonfortestingtype 
                                                                                    WHERE codename = 'reading')) 
                                                                                    AND text = (SELECT description_en FROM {db_name}..custom_models_reasonsfortesting WHERE id = reason_for_testing_old_id
                                                                                    )
                                                    )
                                                -- SLE WRITING
                                                WHEN (CASE
                                                        -- SLE TEST SKILL
                                                        WHEN (SELECT test_skill_type_id 
                                                            FROM {db_name}..cms_models_testskill 
                                                            WHERE test_definition_id = (SELECT test_id 
                                                                                        FROM {db_name}..custom_models_uitinvites 
                                                                                        WHERE id = uit_invite_id
                                                                                        )
                                                            ) = 2
                                                        THEN (SELECT test_skill_sle_desc_id FROM {db_name}..cms_models_testskillsle WHERE test_skill_id = (SELECT id 
                                                                                                                                                        FROM {db_name}..cms_models_testskill 
                                                                                                                                                        WHERE test_definition_id = (SELECT test_id 
                                                                                                                                                                                    FROM {db_name}..custom_models_uitinvites 
                                                                                                                                                                                    WHERE id = uit_invite_id
                                                                                                                                                                                )
                                                                                                                                                        )
                                                            )
                                                    END
                                                    ) IN (3, 4)
                                                THEN (SELECT reason_for_testing_id 
                                                    FROM {db_name}..custom_models_reasonfortestingtext 
                                                    WHERE reason_for_testing_id IN (SELECT id 
                                                                                    FROM {db_name}..custom_models_reasonfortesting WHERE reason_for_testing_type_id = (SELECT id FROM custom_models_reasonfortestingtype 
                                                                                    WHERE codename = 'writing')) 
                                                                                    AND text = (SELECT description_en FROM {db_name}..custom_models_reasonsfortesting WHERE id = reason_for_testing_old_id
                                                                                    )
                                                    )
                                            END
                                        )
                """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """UPDATE {db_name}..custom_models_historicalorderlessfinancialdata
                SET reason_for_testing_id = (CASE
                                                -- SLE READING
                                                WHEN (CASE
                                                        -- SLE TEST SKILL
                                                        WHEN (SELECT test_skill_type_id 
                                                            FROM {db_name}..cms_models_testskill 
                                                            WHERE test_definition_id = (SELECT test_id 
                                                                                        FROM {db_name}..custom_models_uitinvites 
                                                                                        WHERE id = uit_invite_id
                                                                                        )
                                                            ) = 2
                                                        THEN (SELECT test_skill_sle_desc_id FROM {db_name}..cms_models_testskillsle WHERE test_skill_id = (SELECT id 
                                                                                                                                                        FROM {db_name}..cms_models_testskill 
                                                                                                                                                        WHERE test_definition_id = (SELECT test_id 
                                                                                                                                                                                    FROM {db_name}..custom_models_uitinvites 
                                                                                                                                                                                    WHERE id = uit_invite_id
                                                                                                                                                                                )
                                                                                                                                                        )
                                                            )
                                                    END
                                                    ) IN (1, 2)
                                                THEN (SELECT reason_for_testing_id 
                                                    FROM {db_name}..custom_models_reasonfortestingtext 
                                                    WHERE reason_for_testing_id IN (SELECT id 
                                                                                    FROM {db_name}..custom_models_reasonfortesting WHERE reason_for_testing_type_id = (SELECT id FROM custom_models_reasonfortestingtype 
                                                                                    WHERE codename = 'reading')) 
                                                                                    AND text = (SELECT description_en FROM {db_name}..custom_models_reasonsfortesting WHERE id = reason_for_testing_old_id
                                                                                    )
                                                    )
                                                -- SLE WRITING
                                                WHEN (CASE
                                                        -- SLE TEST SKILL
                                                        WHEN (SELECT test_skill_type_id 
                                                            FROM {db_name}..cms_models_testskill 
                                                            WHERE test_definition_id = (SELECT test_id 
                                                                                        FROM {db_name}..custom_models_uitinvites 
                                                                                        WHERE id = uit_invite_id
                                                                                        )
                                                            ) = 2
                                                        THEN (SELECT test_skill_sle_desc_id FROM {db_name}..cms_models_testskillsle WHERE test_skill_id = (SELECT id 
                                                                                                                                                        FROM {db_name}..cms_models_testskill 
                                                                                                                                                        WHERE test_definition_id = (SELECT test_id 
                                                                                                                                                                                    FROM {db_name}..custom_models_uitinvites 
                                                                                                                                                                                    WHERE id = uit_invite_id
                                                                                                                                                                                )
                                                                                                                                                        )
                                                            )
                                                    END
                                                    ) IN (3, 4)
                                                THEN (SELECT reason_for_testing_id 
                                                    FROM {db_name}..custom_models_reasonfortestingtext 
                                                    WHERE reason_for_testing_id IN (SELECT id 
                                                                                    FROM {db_name}..custom_models_reasonfortesting WHERE reason_for_testing_type_id = (SELECT id FROM custom_models_reasonfortestingtype 
                                                                                    WHERE codename = 'writing')) 
                                                                                    AND text = (SELECT description_en FROM {db_name}..custom_models_reasonsfortesting WHERE id = reason_for_testing_old_id
                                                                                    )
                                                    )
                                            END
                                        )
                """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """UPDATE {db_name}..custom_models_assessmentprocessassignedtestspecs SET reason_for_testing_id = NULL""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """UPDATE {db_name}..custom_models_historicalassessmentprocessassignedtestspecs SET reason_for_testing_id = NULL""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """UPDATE {db_name}..custom_models_assessmentprocessdefaulttestspecs SET reason_for_testing_id = NULL""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """UPDATE {db_name}..custom_models_historicalassessmentprocessdefaulttestspecs SET reason_for_testing_id = NULL""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """UPDATE {db_name}..custom_models_orderlessfinancialdata SET reason_for_testing_id = NULL""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """UPDATE {db_name}..custom_models_historicalorderlessfinancialdata SET reason_for_testing_id = NULL""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0159_assessmentprocessassignedtestspecs_reason_for_testing_and_more",
        ),
    ]
    operations = [
        migrations.RunPython(mapping_new_reason_for_testing_ids, rollback_changes)
    ]
