from django.db import migrations


def create_new_criticality_data(apps, schema_editor):
    # get models
    criticality = apps.get_model("custom_models", "criticality")

    # creating all needed criticality data
    criticality_1 = criticality(
        description_en="Danger",
        description_fr="Danger",
        active=1,
    )
    criticality_1.save()
    criticality_2 = criticality(
        description_en="Warning",
        description_fr="Avertissement",
        active=1,
    )
    criticality_2.save()
    criticality_3 = criticality(
        description_en="Information",
        description_fr="Information",
        active=1,
    )
    criticality_3.save()


def rollback_changes(apps, schema_editor):
    # get models
    criticality = apps.get_model("custom_models", "criticality")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the reasons
    criticality.objects.using(db_alias).filter(description_en="Danger").delete()
    criticality.objects.using(db_alias).filter(description_en="Warning").delete()
    criticality.objects.using(db_alias).filter(description_en="Information").delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0036_auto_20220822_1434")]
    operations = [migrations.RunPython(create_new_criticality_data, rollback_changes)]
