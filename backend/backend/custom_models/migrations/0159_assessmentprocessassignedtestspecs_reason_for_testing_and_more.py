# Generated by Django 4.2.11 on 2024-08-30 14:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        (
            "custom_models",
            "0158_drop_reason_for_testing_index_related_to_old_reference",
        ),
    ]

    operations = [
        migrations.AddField(
            model_name="assessmentprocessassignedtestspecs",
            name="reason_for_testing",
            field=models.ForeignKey(
                default=None,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.reasonfortesting",
            ),
        ),
        migrations.AddField(
            model_name="assessmentprocessdefaulttestspecs",
            name="reason_for_testing",
            field=models.ForeignKey(
                default=None,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.reasonfortesting",
            ),
        ),
        migrations.AddField(
            model_name="historicalassessmentprocessassignedtestspecs",
            name="reason_for_testing",
            field=models.ForeignKey(
                blank=True,
                db_constraint=False,
                default=None,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                related_name="+",
                to="custom_models.reasonfortesting",
            ),
        ),
        migrations.AddField(
            model_name="historicalassessmentprocessdefaulttestspecs",
            name="reason_for_testing",
            field=models.ForeignKey(
                blank=True,
                db_constraint=False,
                default=None,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                related_name="+",
                to="custom_models.reasonfortesting",
            ),
        ),
        migrations.AddField(
            model_name="historicalorderlessfinancialdata",
            name="reason_for_testing",
            field=models.ForeignKey(
                blank=True,
                db_constraint=False,
                default=None,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                related_name="+",
                to="custom_models.reasonfortesting",
            ),
        ),
        migrations.AddField(
            model_name="orderlessfinancialdata",
            name="reason_for_testing",
            field=models.ForeignKey(
                default=None,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.reasonfortesting",
            ),
        ),
    ]
