from django.db import migrations
from backend.static.scorer_ola_test_session_skip_option import (
    ScorerOlaTestSessionSkipOptionCodenameConst,
)
from backend.static.languages import Language_id

# text translations
CONFLICTS_FAMILY_EN_OLD = "Conflict of Interest - Family"
CONFLICTS_FAMILY_FR_OLD = "FR Conflict of Interest - Family"

CONFLICTS_FAMILY_EN_NEW = "Conflict of interest or acquaintance"
CONFLICTS_FAMILY_FR_NEW = "Conflit d’intérêt ou connaissance"


def create_scorer_ola_test_session_skip_option_data(apps, schema_editor):
    # get models
    scorer_ola_test_session_skip_option = apps.get_model(
        "custom_models", "scorerolatestsessionskipoption"
    )
    scorer_ola_test_session_skip_option_text = apps.get_model(
        "custom_models", "scorerolatestsessionskipoptiontext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # UPDATING EXISTING OPTION
    skip_option = scorer_ola_test_session_skip_option.objects.using(db_alias).get(
        codename=ScorerOlaTestSessionSkipOptionCodenameConst.CONFLICT_FAMILY
    )
    skip_option_en = scorer_ola_test_session_skip_option_text.objects.using(
        db_alias
    ).get(
        scorer_ola_test_session_skip_option_id=skip_option.id,
        language_id=Language_id.EN,
    )
    skip_option_en.text = CONFLICTS_FAMILY_EN_NEW
    skip_option_en.save()
    skip_option_fr = scorer_ola_test_session_skip_option_text.objects.using(
        db_alias
    ).get(
        scorer_ola_test_session_skip_option_id=skip_option.id,
        language_id=Language_id.FR,
    )
    skip_option_fr.text = CONFLICTS_FAMILY_FR_NEW
    skip_option_fr.save()

    # CREATING NEW OPTION
    scorer_ola_test_session_skip_option_var = scorer_ola_test_session_skip_option(
        codename=ScorerOlaTestSessionSkipOptionCodenameConst.TESTED_RECENTLY,
    )
    scorer_ola_test_session_skip_option_var.save()

    scorer_ola_test_session_skip_option_text_en = scorer_ola_test_session_skip_option_text(
        scorer_ola_test_session_skip_option_id=scorer_ola_test_session_skip_option_var.id,
        language_id=Language_id.EN,
        text="Tested this candidate recently",
    )
    scorer_ola_test_session_skip_option_text_en.save()

    scorer_ola_test_session_skip_option_text_fr = scorer_ola_test_session_skip_option_text(
        scorer_ola_test_session_skip_option_id=scorer_ola_test_session_skip_option_var.id,
        language_id=Language_id.FR,
        text="Déjà testé ce candidat récemment",
    )
    scorer_ola_test_session_skip_option_text_fr.save()


def rollback_changes(apps, schema_editor):
    # get models
    scorer_ola_test_session_skip_option = apps.get_model(
        "custom_models", "scorerolatestsessionskipoption"
    )
    scorer_ola_test_session_skip_option_text = apps.get_model(
        "custom_models", "scorerolatestsessionskipoptiontext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # UPDATING BACK TO ORIGINAL TEXT
    skip_option = scorer_ola_test_session_skip_option.objects.using(db_alias).get(
        codename=ScorerOlaTestSessionSkipOptionCodenameConst.CONFLICT_FAMILY
    )
    skip_option_en = scorer_ola_test_session_skip_option_text.objects.using(
        db_alias
    ).get(
        scorer_ola_test_session_skip_option_id=skip_option.id,
        language_id=Language_id.EN,
    )
    skip_option_en.text = CONFLICTS_FAMILY_EN_OLD
    skip_option_en.save()
    skip_option_fr = scorer_ola_test_session_skip_option_text.objects.using(
        db_alias
    ).get(
        scorer_ola_test_session_skip_option_id=skip_option.id,
        language_id=Language_id.FR,
    )
    skip_option_fr.text = CONFLICTS_FAMILY_FR_OLD
    skip_option_fr.save()

    # DELETING ADDED REASON FOR SKIPPING RELATED DATA
    skip_option_to_delete = scorer_ola_test_session_skip_option.objects.using(
        db_alias
    ).get(codename=ScorerOlaTestSessionSkipOptionCodenameConst.TESTED_RECENTLY)
    scorer_ola_test_session_skip_option_text.objects.using(db_alias).filter(
        scorer_ola_test_session_skip_option_id=skip_option_to_delete.id
    ).delete()
    skip_option_to_delete.delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0180_historicaltestcentertestsessiondata_test_assessor_user_and_more",
        )
    ]
    operations = [
        migrations.RunPython(
            create_scorer_ola_test_session_skip_option_data, rollback_changes
        )
    ]
