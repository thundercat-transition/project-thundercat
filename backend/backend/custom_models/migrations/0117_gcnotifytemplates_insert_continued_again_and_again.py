from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, _):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # creating all needed settings
    tempplate_1 = gc_notify_template(
        folder="Misc",
        codename=GCNotifyTemplateCodenames.FAILED_SEND_TEST_SESSION_CANDIDATES_TO_SECURITY,
        template_id="1d492ff0-0e98-43d6-bf99-35752e56c902",
    )
    tempplate_1.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the setting
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.DELETE_SPECIFIC_TEST_SESSION
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0116_new_test_id_field_in_uit_invites_table")]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
