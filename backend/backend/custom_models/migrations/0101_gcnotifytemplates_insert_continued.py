from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, _):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # creating all needed templates
    tempplate_1 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.RESEND_ALREADY_ASSIGNED_TEST_TO_ADMINISTER,
        template_id="a435c1bd-7574-415d-ae15-c70e98b14d72",
    )
    tempplate_1.save()
    tempplate_2 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.SEND_NEW_ASSIGNED_TEST_TO_ADMINISTER,
        template_id="02c8316f-bf4f-4088-ba4e-914f690a92c8",
    )
    tempplate_2.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the templates
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.RESEND_ALREADY_ASSIGNED_TEST_TO_ADMINISTER
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.SEND_NEW_ASSIGNED_TEST_TO_ADMINISTER
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0100_alter_gcnotifytemplate_codename_and_more")]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
