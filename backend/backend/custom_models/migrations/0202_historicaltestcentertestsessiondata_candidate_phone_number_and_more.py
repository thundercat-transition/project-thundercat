# Generated by Django 5.0.9 on 2025-01-24 15:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_models', '0201_remove_testcenterolatestassessor_included_in_availability'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicaltestcentertestsessiondata',
            name='candidate_phone_number',
            field=models.CharField(default=None, max_length=10, null=True),
        ),
        migrations.AddField(
            model_name='historicaluseraccommodationfile',
            name='candidate_phone_number',
            field=models.CharField(default=None, max_length=10, null=True),
        ),
        migrations.AddField(
            model_name='testcentertestsessiondata',
            name='candidate_phone_number',
            field=models.CharField(default=None, max_length=10, null=True),
        ),
        migrations.AddField(
            model_name='useraccommodationfile',
            name='candidate_phone_number',
            field=models.CharField(default=None, max_length=10, null=True),
        ),
    ]
