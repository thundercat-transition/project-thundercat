from django.db import migrations


def create_new_ta_action_types(apps, _):
    # get models
    ta_action_types = apps.get_model("custom_models", "taactiontypes")
    # creating all needed TA Action Types
    action_type_1 = ta_action_types(action_type="UPDATE_TIME")
    action_type_1.save()
    action_type_2 = ta_action_types(action_type="APPROVE")
    action_type_2.save()
    action_type_3 = ta_action_types(action_type="LOCK")
    action_type_3.save()
    action_type_4 = ta_action_types(action_type="UNLOCK")
    action_type_4.save()
    action_type_5 = ta_action_types(action_type="PAUSE")
    action_type_5.save()
    action_type_6 = ta_action_types(action_type="UNPAUSE")
    action_type_6.save()
    action_type_7 = ta_action_types(action_type="REVOKE")
    action_type_7.save()
    action_type_8 = ta_action_types(action_type="REPORT")
    action_type_8.save()


def rollback_new_ta_action_types(apps, schema_editor):
    # get models
    ta_action_types = apps.get_model("custom_models", "taactiontypes")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the types
    ta_action_types.objects.using(db_alias).filter(action_type="UPDATE_TIME").delete()
    ta_action_types.objects.using(db_alias).filter(action_type="APPROVE").delete()
    ta_action_types.objects.using(db_alias).filter(action_type="LOCK").delete()
    ta_action_types.objects.using(db_alias).filter(action_type="UNLOCK").delete()
    ta_action_types.objects.using(db_alias).filter(action_type="PAUSE").delete()
    ta_action_types.objects.using(db_alias).filter(action_type="UNPAUSE").delete()
    ta_action_types.objects.using(db_alias).filter(action_type="REVOKE").delete()
    ta_action_types.objects.using(db_alias).filter(action_type="REPORT").delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0002_create_languages")]
    operations = [
        migrations.RunPython(create_new_ta_action_types, rollback_new_ta_action_types)
    ]
