from django.db import connection
from django.db import migrations
from django.conf import settings


def update_data(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """
				-- ====================================================================
				--                            CUSTOM MODELS
				-- ====================================================================
				-- ==================== TEST ACCESS CODE ====================
				UPDATE {db_name}..custom_models_testaccesscode
				SET ta_user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = ta_username_id)

				UPDATE {db_name}..custom_models_historicaltestaccesscode
				SET ta_user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = ta_username_id)
				-- ==================== TEST ACCESS CODE (END) ====================

				-- ==================== UNSUPERVISED TEST ACCESS CODE ====================
				UPDATE {db_name}..custom_models_unsupervisedtestaccesscode
				SET ta_user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = ta_username_id)

				UPDATE {db_name}..custom_models_historicalunsupervisedtestaccesscode
				SET ta_user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = ta_username_id)
				-- ==================== UNSUPERVISED TEST ACCESS CODE (END) ====================

				-- ==================== ASSIGNED TEST ====================
				UPDATE {db_name}..custom_models_assignedtest
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)

				UPDATE {db_name}..custom_models_historicalassignedtest
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)

				UPDATE {db_name}..custom_models_assignedtest
				SET ta_user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = ta_id)

				UPDATE {db_name}..custom_models_historicalassignedtest
				SET ta_user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = ta_id)
				-- ==================== ASSIGNED TEST (END) ====================

				-- ==================== UIT INVITES ====================
				UPDATE {db_name}..custom_models_uitinvites
				SET ta_user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = ta_username_id)

				UPDATE {db_name}..custom_models_historicaluitinvites
				SET ta_user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = ta_username_id)
				-- ==================== UIT INVITES (END) ====================

				-- ==================== TEST SCORER ASSIGNMENT ====================
				UPDATE {db_name}..custom_models_testscorerassignment
				SET scorer_user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = scorer_username_id)

				UPDATE {db_name}..custom_models_historicaltestscorerassignment
				SET scorer_user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = scorer_username_id)
				-- ==================== TEST SCORER ASSIGNMENT (END) ====================
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """
				-- ====================================================================
				--                            CUSTOM MODELS
				-- ====================================================================
				-- ==================== TEST ACCESS CODE ====================
				UPDATE {db_name}..custom_models_testaccesscode
				SET ta_user_id = NULL

				UPDATE {db_name}..custom_models_historicaltestaccesscode
				SET ta_user_id = NULL
				-- ==================== TEST ACCESS CODE (END) ====================

				-- ==================== UNSUPERVISED TEST ACCESS CODE ====================
				UPDATE {db_name}..custom_models_unsupervisedtestaccesscode
				SET ta_user_id = NULL

				UPDATE {db_name}..custom_models_historicalunsupervisedtestaccesscode
				SET ta_user_id = NULL
				-- ==================== UNSUPERVISED TEST ACCESS CODE (END) ====================

				-- ==================== ASSIGNED TEST ====================
				UPDATE {db_name}..custom_models_assignedtest
				SET user_id = NULL

				UPDATE {db_name}..custom_models_historicalassignedtest
				SET user_id = NULL

				UPDATE {db_name}..custom_models_assignedtest
				SET ta_user_id = NULL

				UPDATE {db_name}..custom_models_historicalassignedtest
				SET ta_user_id = NULL
				-- ==================== ASSIGNED TEST (END) ====================

				-- ==================== UIT INVITES ====================
				UPDATE {db_name}..custom_models_uitinvites
				SET ta_user_id = NULL

				UPDATE {db_name}..custom_models_historicaluitinvites
				SET ta_user_id = NULL
				-- ==================== UIT INVITES (END) ====================

				-- ==================== TEST SCORER ASSIGNMENT ====================
				UPDATE {db_name}..custom_models_testscorerassignment
				SET scorer_user_id = NULL

				UPDATE {db_name}..custom_models_historicaltestscorerassignment
				SET scorer_user_id = NULL
				-- ==================== TEST SCORER ASSIGNMENT (END) ====================
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0083_remove_assignedtest_must_be_a_unique_status_username_tac_test_combination_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_data, rollback_changes)]
