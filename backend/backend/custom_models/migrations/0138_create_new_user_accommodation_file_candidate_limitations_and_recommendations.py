from django.db import migrations
from backend.static.user_accommodation_file_candidate_limitations_const import (
    UserAccommodationFileCandidateLimitationsConst,
)
from backend.static.user_accommodation_file_recommendations_const import (
    UserAccommodationFileRecommendationsConst,
)
from backend.static.languages import Language_id
from backend.static.consumed_reservation_code_statuses import (
    StaticConsumedReservationCodeStatus,
)


def create_new_data(apps, schema_editor):
    # get models
    candidate_limitations_option = apps.get_model(
        "custom_models", "useraccommodationfilecandidatelimitationsoption"
    )
    candidate_limitations_option_text = apps.get_model(
        "custom_models", "useraccommodationfilecandidatelimitationsoptiontext"
    )
    recommendations_option = apps.get_model(
        "custom_models", "useraccommodationfilerecommendationsoption"
    )
    recommendations_option_text = apps.get_model(
        "custom_models", "useraccommodationfilerecommendationsoptiontext"
    )

    # ==================== CANDIDATE LIMITATIONS ====================
    # adding new candidate limitations option codenames
    limitation_1 = candidate_limitations_option(
        codename=UserAccommodationFileCandidateLimitationsConst.ATTENTION_DISORDERS
    )
    limitation_1.save()
    limitation_2 = candidate_limitations_option(
        codename=UserAccommodationFileCandidateLimitationsConst.CHRONIC_MEDICAL_DISABILITIES
    )
    limitation_2.save()
    limitation_3 = candidate_limitations_option(
        codename=UserAccommodationFileCandidateLimitationsConst.HEARING_DISABILITIES
    )
    limitation_3.save()
    limitation_4 = candidate_limitations_option(
        codename=UserAccommodationFileCandidateLimitationsConst.LEARNING_DISABILITIES
    )
    limitation_4.save()
    limitation_5 = candidate_limitations_option(
        codename=UserAccommodationFileCandidateLimitationsConst.MOBILITY_PHYSICAL_DISABILITIES
    )
    limitation_5.save()
    limitation_6 = candidate_limitations_option(
        codename=UserAccommodationFileCandidateLimitationsConst.NEUROLOGICAL_DISORDERS
    )
    limitation_6.save()
    limitation_7 = candidate_limitations_option(
        codename=UserAccommodationFileCandidateLimitationsConst.PSYCHIATRIC_PSYCHOLOGICAL_EMOTIONAL_DISABILITIES
    )
    limitation_7.save()
    limitation_8 = candidate_limitations_option(
        codename=UserAccommodationFileCandidateLimitationsConst.SPEECH_AND_LANGUAGE_DISABILITIES
    )
    limitation_8.save()
    limitation_9 = candidate_limitations_option(
        codename=UserAccommodationFileCandidateLimitationsConst.TEMPORARY_CONDITIONS_AND_OTHER
    )
    limitation_9.save()
    limitation_10 = candidate_limitations_option(
        codename=UserAccommodationFileCandidateLimitationsConst.VISUAL_CONDITION
    )
    limitation_10.save()

    # adding new candidate limitations option text
    limitation_text_en = candidate_limitations_option_text(
        text="Attention Disorders",
        user_accommodation_file_candidate_limitations_option_id=limitation_1.id,
        language_id=Language_id.EN,
    )
    limitation_text_en.save()

    limitation_text_fr = candidate_limitations_option_text(
        text="Troubles de l'attention",
        user_accommodation_file_candidate_limitations_option_id=limitation_1.id,
        language_id=Language_id.FR,
    )
    limitation_text_fr.save()

    limitation_text_en = candidate_limitations_option_text(
        text="Chronic/Medical Disabilities",
        user_accommodation_file_candidate_limitations_option_id=limitation_2.id,
        language_id=Language_id.EN,
    )
    limitation_text_en.save()

    limitation_text_fr = candidate_limitations_option_text(
        text="Maladies chroniques et troubles médicaux",
        user_accommodation_file_candidate_limitations_option_id=limitation_2.id,
        language_id=Language_id.FR,
    )
    limitation_text_fr.save()

    limitation_text_en = candidate_limitations_option_text(
        text="Hearing Disabilities",
        user_accommodation_file_candidate_limitations_option_id=limitation_3.id,
        language_id=Language_id.EN,
    )
    limitation_text_en.save()

    limitation_text_fr = candidate_limitations_option_text(
        text="Handicap auditif",
        user_accommodation_file_candidate_limitations_option_id=limitation_3.id,
        language_id=Language_id.FR,
    )
    limitation_text_fr.save()

    limitation_text_en = candidate_limitations_option_text(
        text="Learning Disabilities",
        user_accommodation_file_candidate_limitations_option_id=limitation_4.id,
        language_id=Language_id.EN,
    )
    limitation_text_en.save()

    limitation_text_fr = candidate_limitations_option_text(
        text="Trouble d'apprentissage",
        user_accommodation_file_candidate_limitations_option_id=limitation_4.id,
        language_id=Language_id.FR,
    )
    limitation_text_fr.save()

    limitation_text_en = candidate_limitations_option_text(
        text="Mobility/Physical Disabilities",
        user_accommodation_file_candidate_limitations_option_id=limitation_5.id,
        language_id=Language_id.EN,
    )
    limitation_text_en.save()

    limitation_text_fr = candidate_limitations_option_text(
        text="Déficience physique ou de mobilité",
        user_accommodation_file_candidate_limitations_option_id=limitation_5.id,
        language_id=Language_id.FR,
    )
    limitation_text_fr.save()

    limitation_text_en = candidate_limitations_option_text(
        text="Neurological Disorders",
        user_accommodation_file_candidate_limitations_option_id=limitation_6.id,
        language_id=Language_id.EN,
    )
    limitation_text_en.save()

    limitation_text_fr = candidate_limitations_option_text(
        text="Troubles neurologiques",
        user_accommodation_file_candidate_limitations_option_id=limitation_6.id,
        language_id=Language_id.FR,
    )
    limitation_text_fr.save()

    limitation_text_en = candidate_limitations_option_text(
        text="Psychiatric/Psychological/Emotional Disabilities",
        user_accommodation_file_candidate_limitations_option_id=limitation_7.id,
        language_id=Language_id.EN,
    )
    limitation_text_en.save()

    limitation_text_fr = candidate_limitations_option_text(
        text="Handicap psychiatrique/psychologique/émotionnel",
        user_accommodation_file_candidate_limitations_option_id=limitation_7.id,
        language_id=Language_id.FR,
    )
    limitation_text_fr.save()

    limitation_text_en = candidate_limitations_option_text(
        text="Speech and Language Disabilities",
        user_accommodation_file_candidate_limitations_option_id=limitation_8.id,
        language_id=Language_id.EN,
    )
    limitation_text_en.save()

    limitation_text_fr = candidate_limitations_option_text(
        text="Troubles phonologiques et troubles du langage",
        user_accommodation_file_candidate_limitations_option_id=limitation_8.id,
        language_id=Language_id.FR,
    )
    limitation_text_fr.save()

    limitation_text_en = candidate_limitations_option_text(
        text="Temporary Conditions and Other",
        user_accommodation_file_candidate_limitations_option_id=limitation_9.id,
        language_id=Language_id.EN,
    )
    limitation_text_en.save()

    limitation_text_fr = candidate_limitations_option_text(
        text="Conditions temporaires et autres",
        user_accommodation_file_candidate_limitations_option_id=limitation_9.id,
        language_id=Language_id.FR,
    )
    limitation_text_fr.save()

    limitation_text_en = candidate_limitations_option_text(
        text="Visual Condition",
        user_accommodation_file_candidate_limitations_option_id=limitation_10.id,
        language_id=Language_id.EN,
    )
    limitation_text_en.save()

    limitation_text_fr = candidate_limitations_option_text(
        text="Condition visuelle",
        user_accommodation_file_candidate_limitations_option_id=limitation_10.id,
        language_id=Language_id.FR,
    )
    limitation_text_fr.save()
    # ==================== CANDIDATE LIMITATIONS (END) ====================

    # ==================== RECOMMENDATIONS ====================
    # adding new recommendations option codenames
    recommendation_1 = recommendations_option(
        codename=UserAccommodationFileRecommendationsConst.ADAPTED_SCORING
    )
    recommendation_1.save()
    recommendation_2 = recommendations_option(
        codename=UserAccommodationFileRecommendationsConst.ADAPTIVE_TECHNOLOGY
    )
    recommendation_2.save()
    recommendation_3 = recommendations_option(
        codename=UserAccommodationFileRecommendationsConst.ASSESSOR_OR_TEST_ADMINISTRATOR
    )
    recommendation_3.save()
    recommendation_4 = recommendations_option(
        codename=UserAccommodationFileRecommendationsConst.COMMUNICATION_SUPPORT
    )
    recommendation_4.save()
    recommendation_5 = recommendations_option(
        codename=UserAccommodationFileRecommendationsConst.ENVIRONMENT
    )
    recommendation_5.save()
    recommendation_6 = recommendations_option(
        codename=UserAccommodationFileRecommendationsConst.EXTRA_TIME
    )
    recommendation_6.save()
    recommendation_7 = recommendations_option(
        codename=UserAccommodationFileRecommendationsConst.FORMATS
    )
    recommendation_7.save()
    recommendation_8 = recommendations_option(
        codename=UserAccommodationFileRecommendationsConst.INDIVIDUAL_TEST_SESSION
    )
    recommendation_8.save()
    recommendation_9 = recommendations_option(
        codename=UserAccommodationFileRecommendationsConst.MATERIAL
    )
    recommendation_9.save()
    recommendation_10 = recommendations_option(
        codename=UserAccommodationFileRecommendationsConst.PREPARATION_TO_TEST
    )
    recommendation_10.save()
    recommendation_11 = recommendations_option(
        codename=UserAccommodationFileRecommendationsConst.SCHEDULING
    )
    recommendation_11.save()
    recommendation_12 = recommendations_option(
        codename=UserAccommodationFileRecommendationsConst.SLE_UIT_LEVELING
    )
    recommendation_12.save()

    # adding new recommendations option text
    recommendation_text_en = recommendations_option_text(
        text="Adapted Scoring",
        user_accommodation_file_recommendations_option_id=recommendation_1.id,
        language_id=Language_id.EN,
    )
    recommendation_text_en.save()

    recommendation_text_fr = recommendations_option_text(
        text="Correction adaptée",
        user_accommodation_file_recommendations_option_id=recommendation_1.id,
        language_id=Language_id.FR,
    )
    recommendation_text_fr.save()

    recommendation_text_en = recommendations_option_text(
        text="Adaptive Technology",
        user_accommodation_file_recommendations_option_id=recommendation_2.id,
        language_id=Language_id.EN,
    )
    recommendation_text_en.save()

    recommendation_text_fr = recommendations_option_text(
        text="Technologie adaptée",
        user_accommodation_file_recommendations_option_id=recommendation_2.id,
        language_id=Language_id.FR,
    )
    recommendation_text_fr.save()

    recommendation_text_en = recommendations_option_text(
        text="Assessor or Test Administrator",
        user_accommodation_file_recommendations_option_id=recommendation_3.id,
        language_id=Language_id.EN,
    )
    recommendation_text_en.save()

    recommendation_text_fr = recommendations_option_text(
        text="Évaluateur ou Administrateur d'examen",
        user_accommodation_file_recommendations_option_id=recommendation_3.id,
        language_id=Language_id.FR,
    )
    recommendation_text_fr.save()

    recommendation_text_en = recommendations_option_text(
        text="Communication Support",
        user_accommodation_file_recommendations_option_id=recommendation_4.id,
        language_id=Language_id.EN,
    )
    recommendation_text_en.save()

    recommendation_text_fr = recommendations_option_text(
        text="Soutien à la communication",
        user_accommodation_file_recommendations_option_id=recommendation_4.id,
        language_id=Language_id.FR,
    )
    recommendation_text_fr.save()

    recommendation_text_en = recommendations_option_text(
        text="Environment",
        user_accommodation_file_recommendations_option_id=recommendation_5.id,
        language_id=Language_id.EN,
    )
    recommendation_text_en.save()

    recommendation_text_fr = recommendations_option_text(
        text="Environnement",
        user_accommodation_file_recommendations_option_id=recommendation_5.id,
        language_id=Language_id.FR,
    )
    recommendation_text_fr.save()

    recommendation_text_en = recommendations_option_text(
        text="Extra Time",
        user_accommodation_file_recommendations_option_id=recommendation_6.id,
        language_id=Language_id.EN,
    )
    recommendation_text_en.save()

    recommendation_text_fr = recommendations_option_text(
        text="Temps additionnel",
        user_accommodation_file_recommendations_option_id=recommendation_6.id,
        language_id=Language_id.FR,
    )
    recommendation_text_fr.save()

    recommendation_text_en = recommendations_option_text(
        text="Formats",
        user_accommodation_file_recommendations_option_id=recommendation_7.id,
        language_id=Language_id.EN,
    )
    recommendation_text_en.save()

    recommendation_text_fr = recommendations_option_text(
        text="Formats",
        user_accommodation_file_recommendations_option_id=recommendation_7.id,
        language_id=Language_id.FR,
    )
    recommendation_text_fr.save()

    recommendation_text_en = recommendations_option_text(
        text="Individual Test Session",
        user_accommodation_file_recommendations_option_id=recommendation_8.id,
        language_id=Language_id.EN,
    )
    recommendation_text_en.save()

    recommendation_text_fr = recommendations_option_text(
        text="Séance d'examen individuelle",
        user_accommodation_file_recommendations_option_id=recommendation_8.id,
        language_id=Language_id.FR,
    )
    recommendation_text_fr.save()

    recommendation_text_en = recommendations_option_text(
        text="Material",
        user_accommodation_file_recommendations_option_id=recommendation_9.id,
        language_id=Language_id.EN,
    )
    recommendation_text_en.save()

    recommendation_text_fr = recommendations_option_text(
        text="Matériel",
        user_accommodation_file_recommendations_option_id=recommendation_9.id,
        language_id=Language_id.FR,
    )
    recommendation_text_fr.save()

    recommendation_text_en = recommendations_option_text(
        text="Preparation to Test",
        user_accommodation_file_recommendations_option_id=recommendation_10.id,
        language_id=Language_id.EN,
    )
    recommendation_text_en.save()

    recommendation_text_fr = recommendations_option_text(
        text="Préparation à l'examen",
        user_accommodation_file_recommendations_option_id=recommendation_10.id,
        language_id=Language_id.FR,
    )
    recommendation_text_fr.save()

    recommendation_text_en = recommendations_option_text(
        text="Scheduling",
        user_accommodation_file_recommendations_option_id=recommendation_11.id,
        language_id=Language_id.EN,
    )
    recommendation_text_en.save()

    recommendation_text_fr = recommendations_option_text(
        text="Horaire",
        user_accommodation_file_recommendations_option_id=recommendation_11.id,
        language_id=Language_id.FR,
    )
    recommendation_text_fr.save()

    recommendation_text_en = recommendations_option_text(
        text="SLE-UIT Leveling",
        user_accommodation_file_recommendations_option_id=recommendation_12.id,
        language_id=Language_id.EN,
    )
    recommendation_text_en.save()

    recommendation_text_fr = recommendations_option_text(
        text="ELS-TELNS Mise à niveau",
        user_accommodation_file_recommendations_option_id=recommendation_12.id,
        language_id=Language_id.FR,
    )
    recommendation_text_fr.save()
    # ==================== RECOMMENDATIONS (END) ====================


def rollback_changes(apps, schema_editor):
    # get models
    candidate_limitations_option = apps.get_model(
        "custom_models", "useraccommodationfilecandidatelimitationsoption"
    )
    candidate_limitations_option_text = apps.get_model(
        "custom_models", "useraccommodationfilecandidatelimitationsoptiontext"
    )
    recommendations_option = apps.get_model(
        "custom_models", "useraccommodationfilerecommendationsoption"
    )
    recommendations_option_text = apps.get_model(
        "custom_models", "useraccommodationfilerecommendationsoptiontext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # deleting needed data
    candidate_limitations_option_text.objects.using(db_alias).all().delete()
    candidate_limitations_option.objects.using(db_alias).all().delete()

    # deleting needed data
    recommendations_option_text.objects.using(db_alias).all().delete()
    recommendations_option.objects.using(db_alias).all().delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0137_historicaluseraccommodationfilecandidatelimitations_and_more",
        )
    ]
    operations = [migrations.RunPython(create_new_data, rollback_changes)]
