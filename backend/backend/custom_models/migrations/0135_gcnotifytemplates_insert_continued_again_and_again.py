from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, _):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # creating all needed settings
    tempplate_1 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.INVITE_CANDIDATE_TO_NON_STANDARD_TEST_SESSION,
        template_id="22404c9f-1ce5-4727-8d2b-554ac54b50a0",
    )
    tempplate_1.save()
    tempplate_2 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.UPDATE_NON_STANDARD_TEST_SESSION_INVITE,
        template_id="378629e7-018c-43b8-9c61-3c1507fd9d16",
    )
    tempplate_2.save()
    tempplate_3 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.DELETE_NON_STANDARD_TEST_SESSION,
        template_id="be9c24a4-23bc-44dc-b14e-b7256407d576",
    )
    tempplate_3.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the setting
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.INVITE_CANDIDATE_TO_NON_STANDARD_TEST_SESSION
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.UPDATE_NON_STANDARD_TEST_SESSION_INVITE
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.DELETE_NON_STANDARD_TEST_SESSION
    ).delete()


class Migration(migrations.Migration):
    dependencies = [
        ("custom_models", "0134_historicaltestcentertestsessions_is_standard_and_more")
    ]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
