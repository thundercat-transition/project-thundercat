from django.db import migrations
from backend.static.ola_global_configs_const import OlaGlobalConfigsConst


def create_new_ola_global_config(apps, schema_editor):
    # get models
    ola_global_configs = apps.get_model("custom_models", "olaglobalconfigs")

    # adding cancellation window codename
    config_1 = ola_global_configs(
        codename=OlaGlobalConfigsConst.CANCELLATION_WINDOW, value=96
    )
    config_1.save()


def rollback_changes(apps, schema_editor):
    # get models
    ola_global_configs = apps.get_model("custom_models", "olaglobalconfigs")

    # get db alias
    db_alias = schema_editor.connection.alias

    # deleting unneeded data
    config_1 = ola_global_configs.objects.using(db_alias).get(
        codename=OlaGlobalConfigsConst.CANCELLATION_WINDOW
    )

    config_1.delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0193_olaglobalconfigs")]
    operations = [migrations.RunPython(create_new_ola_global_config, rollback_changes)]
