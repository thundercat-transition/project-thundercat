from django.db import connection
from django.db import migrations
from django.conf import settings


def update_data(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            -- OLD DAY OF WEEK:
                -- Monday ==> 1
                -- Tuesday ==> 2
                -- Wednesday ==> 3
                -- Thursday ==> 4
                -- Friday ==> 5
                -- Saturday ==> 6
                -- Sunday ==> 7
            -- NEW DAY OF WEEK:
                -- Sunday ==> 1
                -- Monday ==> 2
                -- Tuesday ==> 3
                -- Wednesday ==> 4
                -- Thursday ==> 5
                -- Friday ==> 6
                -- Saturday ==> 7
            UPDATE {db_name}..custom_models_testcenterolatimeslot
            SET day_of_week_id = day_of_week_old_id + 1
            WHERE day_of_week_id <= 6
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
            -- OLD DAY OF WEEK:
                -- Monday ==> 1
                -- Tuesday ==> 2
                -- Wednesday ==> 3
                -- Thursday ==> 4
                -- Friday ==> 5
                -- Saturday ==> 6
                -- Sunday ==> 7
            -- NEW DAY OF WEEK:
                -- Sunday ==> 1
                -- Monday ==> 2
                -- Tuesday ==> 3
                -- Wednesday ==> 4
                -- Thursday ==> 5
                -- Friday ==> 6
                -- Saturday ==> 7
            UPDATE {db_name}..custom_models_testcenterolatimeslot
            SET day_of_week_id = 1
            WHERE day_of_week_id >= 7
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0212_testcenterolatimeslot_day_of_week_id",
        ),
    ]
    operations = [migrations.RunPython(update_data, rollback_changes)]
