from django.db import migrations
from backend.static.reason_for_testing import (
    ReasonForTestingCodenameConst,
    ReasonForTestingTypeCodenameConst,
)
from backend.static.languages import Language_id


def create_reason_for_testing_related_data(apps, schema_editor):
    # get models
    reason_for_testing_type = apps.get_model("custom_models", "reasonfortestingtype")
    reason_for_testing = apps.get_model("custom_models", "reasonfortesting")
    reason_for_testing_text = apps.get_model("custom_models", "reasonfortestingtext")

    # creating all needed reason for testing types
    # READING
    reason_for_testing_type_1 = reason_for_testing_type(
        codename=ReasonForTestingTypeCodenameConst.READING
    )
    reason_for_testing_type_1.save()

    # WRITING
    reason_for_testing_type_2 = reason_for_testing_type(
        codename=ReasonForTestingTypeCodenameConst.WRITING
    )
    reason_for_testing_type_2.save()

    # ORAL
    reason_for_testing_type_3 = reason_for_testing_type(
        codename=ReasonForTestingTypeCodenameConst.ORAL
    )
    reason_for_testing_type_3.save()

    # creating all needed reason for testing and reason for testing text
    # ==================== READING ====================
    # IMP_STAFFING
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.IMP_STAFFING,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_1.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Imperative staffing",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Dotation impérative",
    )
    reason_for_testing_text_fr.save()

    # NON_IMP_STAFFING
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.NON_IMP_STAFFING,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_1.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Non-imperative staffing",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Dotation non impérative",
    )
    reason_for_testing_text_fr.save()

    # FT_LANGUAGE_TRAINING_FROM_PSC
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.FT_LANGUAGE_TRAINING_FROM_PSC,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_1.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Full-time language training from the Canada School of Public Service",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Formation linguistique à temps plein offerte par l'École de la fonction publique du Canada",
    )
    reason_for_testing_text_fr.save()

    # PT_LANGUAGE_TRAINING_FROM_PSC
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.PT_LANGUAGE_TRAINING_FROM_PSC,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_1.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Part-time Language training from the Canada School of Public Service",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Formation linguistique à temps partiel dispensée par l'École de la fonction publique du Canada",
    )
    reason_for_testing_text_fr.save()

    # FT_LANGUAGE_TRAINING_FROM_DEP_PROG
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.FT_LANGUAGE_TRAINING_FROM_DEP_PROG,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_1.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Full-time language training from departmental program",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Formation linguistique à temps plein offerte par les ministères",
    )
    reason_for_testing_text_fr.save()

    # PT_LANGUAGE_TRAINING_FROM_DEP_PROG
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.PT_LANGUAGE_TRAINING_FROM_DEP_PROG,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_1.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Part-time language training from departmental program",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Formation linguistique à temps partiel offerte par les ministères",
    )
    reason_for_testing_text_fr.save()

    # FT_LANGUAGE_TRAINING_FROM_PRIVATE
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.FT_LANGUAGE_TRAINING_FROM_PRIVATE,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_1.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Full-time language training from private language school",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Form. ling. à temps plein disp. par un établissement ling.",
    )
    reason_for_testing_text_fr.save()

    # PT_LANGUAGE_TRAINING_FROM_PRIVATE
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.PT_LANGUAGE_TRAINING_FROM_PRIVATE,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_1.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Part-time language training from private language school",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Form. ling. à temps part. disp. par un établissement ling.",
    )
    reason_for_testing_text_fr.save()

    # RE_IDENTIFICATION
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.RE_IDENTIFICATION,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_1.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Re-identification",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Réidentification",
    )
    reason_for_testing_text_fr.save()

    # BILINGUALISM_BONUS
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.BILINGUALISM_BONUS,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_1.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Bilingualism bonus",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Prime au bilinguisme",
    )
    reason_for_testing_text_fr.save()

    # RECORD_OR_OTHER_PURPOSES
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.RECORD_OR_OTHER_PURPOSES,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_1.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Record or other purposes",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Fins documentaires ou autres",
    )
    reason_for_testing_text_fr.save()

    # COVID_19_OR_ESSENTIAL_SERVICE
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.COVID_19_OR_ESSENTIAL_SERVICE,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_1.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="COVID-19 or Essential Service",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="COVID-19 ou Service Essential",
    )
    reason_for_testing_text_fr.save()

    # STAFFING_DOTATION
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.STAFFING_DOTATION,
        minimum_process_length=None,
        active=1,
        reason_for_testing_type_id=reason_for_testing_type_1.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Staffing / Dotation",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Dotation / Staffing",
    )
    reason_for_testing_text_fr.save()

    # LANGUAGE_TRAINING
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.LANGUAGE_TRAINING,
        minimum_process_length=None,
        active=1,
        reason_for_testing_type_id=reason_for_testing_type_1.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Language Training / Formation linguistique",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Formation linguistique / Language Training",
    )
    reason_for_testing_text_fr.save()

    # OTHER
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.OTHER,
        minimum_process_length=None,
        active=1,
        reason_for_testing_type_id=reason_for_testing_type_1.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Other / Autre",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Autre / Other",
    )
    reason_for_testing_text_fr.save()
    # ==================== READING (END) ====================

    # ==================== WRITING ====================
    # IMP_STAFFING
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.IMP_STAFFING,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_2.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Imperative staffing",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Dotation impérative",
    )
    reason_for_testing_text_fr.save()

    # NON_IMP_STAFFING
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.NON_IMP_STAFFING,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_2.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Non-imperative staffing",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Dotation non impérative",
    )
    reason_for_testing_text_fr.save()

    # FT_LANGUAGE_TRAINING_FROM_PSC
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.FT_LANGUAGE_TRAINING_FROM_PSC,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_2.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Full-time language training from the Canada School of Public Service",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Formation linguistique à temps plein offerte par l'École de la fonction publique du Canada",
    )
    reason_for_testing_text_fr.save()

    # PT_LANGUAGE_TRAINING_FROM_PSC
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.PT_LANGUAGE_TRAINING_FROM_PSC,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_2.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Part-time Language training from the Canada School of Public Service",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Formation linguistique à temps partiel dispensée par l'École de la fonction publique du Canada",
    )
    reason_for_testing_text_fr.save()

    # FT_LANGUAGE_TRAINING_FROM_DEP_PROG
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.FT_LANGUAGE_TRAINING_FROM_DEP_PROG,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_2.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Full-time language training from departmental program",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Formation linguistique à temps plein offerte par les ministères",
    )
    reason_for_testing_text_fr.save()

    # PT_LANGUAGE_TRAINING_FROM_DEP_PROG
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.PT_LANGUAGE_TRAINING_FROM_DEP_PROG,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_2.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Part-time language training from departmental program",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Formation linguistique à temps partiel offerte par les ministères",
    )
    reason_for_testing_text_fr.save()

    # FT_LANGUAGE_TRAINING_FROM_PRIVATE
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.FT_LANGUAGE_TRAINING_FROM_PRIVATE,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_2.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Full-time language training from private language school",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Form. ling. à temps plein disp. par un établissement ling.",
    )
    reason_for_testing_text_fr.save()

    # PT_LANGUAGE_TRAINING_FROM_PRIVATE
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.PT_LANGUAGE_TRAINING_FROM_PRIVATE,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_2.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Part-time language training from private language school",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Form. ling. à temps part. disp. par un établissement ling.",
    )
    reason_for_testing_text_fr.save()

    # RE_IDENTIFICATION
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.RE_IDENTIFICATION,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_2.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Re-identification",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Réidentification",
    )
    reason_for_testing_text_fr.save()

    # BILINGUALISM_BONUS
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.BILINGUALISM_BONUS,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_2.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Bilingualism bonus",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Prime au bilinguisme",
    )
    reason_for_testing_text_fr.save()

    # RECORD_OR_OTHER_PURPOSES
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.RECORD_OR_OTHER_PURPOSES,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_2.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Record or other purposes",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Fins documentaires ou autres",
    )
    reason_for_testing_text_fr.save()

    # COVID_19_OR_ESSENTIAL_SERVICE
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.COVID_19_OR_ESSENTIAL_SERVICE,
        minimum_process_length=None,
        active=0,
        reason_for_testing_type_id=reason_for_testing_type_2.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="COVID-19 or Essential Service",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="COVID-19 ou Service Essential",
    )
    reason_for_testing_text_fr.save()

    # STAFFING_DOTATION
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.STAFFING_DOTATION,
        minimum_process_length=None,
        active=1,
        reason_for_testing_type_id=reason_for_testing_type_2.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Staffing / Dotation",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Dotation / Staffing",
    )
    reason_for_testing_text_fr.save()

    # LANGUAGE_TRAINING
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.LANGUAGE_TRAINING,
        minimum_process_length=None,
        active=1,
        reason_for_testing_type_id=reason_for_testing_type_2.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Language Training / Formation linguistique",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Formation linguistique / Language Training",
    )
    reason_for_testing_text_fr.save()

    # OTHER
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.OTHER,
        minimum_process_length=None,
        active=1,
        reason_for_testing_type_id=reason_for_testing_type_2.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Other / Autre",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="Autre / Other",
    )
    reason_for_testing_text_fr.save()
    # ==================== WRITING (END) ====================

    # ==================== ORAL ====================
    # IMP_STAFFING_OLA
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.IMP_STAFFING_OLA,
        minimum_process_length=15,
        active=1,
        reason_for_testing_type_id=reason_for_testing_type_3.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Imperative staffing: Impending appointment",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="FR Imperative staffing: Impending appointment",
    )
    reason_for_testing_text_fr.save()

    # LANGUAGE_TRAINING_OLA
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.LANGUAGE_TRAINING_OLA,
        minimum_process_length=30,
        active=1,
        reason_for_testing_type_id=reason_for_testing_type_3.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Language training",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="FR Language training",
    )
    reason_for_testing_text_fr.save()

    # CREATION_OF_PARTIALLY_ASSESSED_POOL_OLA
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.CREATION_OF_PARTIALLY_ASSESSED_POOL_OLA,
        minimum_process_length=60,
        active=1,
        reason_for_testing_type_id=reason_for_testing_type_3.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Creation of Partially Assessed Pool",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="FR Creation of Partially Assessed Pool",
    )
    reason_for_testing_text_fr.save()

    # NON_IMP_STAFFING_OLA
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.NON_IMP_STAFFING_OLA,
        minimum_process_length=60,
        active=1,
        reason_for_testing_type_id=reason_for_testing_type_3.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Non-imperative staffing",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="FR Non-imperative staffing",
    )
    reason_for_testing_text_fr.save()

    # RE_IDENTIFICATION_OLA
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.RE_IDENTIFICATION_OLA,
        minimum_process_length=60,
        active=1,
        reason_for_testing_type_id=reason_for_testing_type_3.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Re-identification",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="FR Re-identification",
    )
    reason_for_testing_text_fr.save()

    # RECORD_OR_OTHER_PURPOSES_OLA
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.RECORD_OR_OTHER_PURPOSES_OLA,
        minimum_process_length=60,
        active=1,
        reason_for_testing_type_id=reason_for_testing_type_3.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Record or other purposes",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="FR Record or other purposes",
    )
    reason_for_testing_text_fr.save()

    # BILINGUALISM_BONUS_OLA
    reason_for_testing_var = reason_for_testing(
        codename=ReasonForTestingCodenameConst.BILINGUALISM_BONUS_OLA,
        minimum_process_length=60,
        active=1,
        reason_for_testing_type_id=reason_for_testing_type_3.id,
    )
    reason_for_testing_var.save()

    reason_for_testing_text_en = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.EN,
        text="Bilingualism bonus",
    )
    reason_for_testing_text_en.save()

    reason_for_testing_text_fr = reason_for_testing_text(
        reason_for_testing_id=reason_for_testing_var.id,
        language_id=Language_id.FR,
        text="FR Bilingualism bonus",
    )
    reason_for_testing_text_fr.save()
    # ==================== ORAL (END) ====================


def rollback_changes(apps, schema_editor):
    # get models
    reason_for_testing_type = apps.get_model("custom_models", "reasonfortestingtype")
    reason_for_testing = apps.get_model("custom_models", "reasonfortesting")
    reason_for_testing_text = apps.get_model("custom_models", "reasonfortestingtext")
    # get db alias
    db_alias = schema_editor.connection.alias

    # deleting all reason for testing related data
    reason_for_testing_text.objects.using(db_alias).all().delete()
    reason_for_testing.objects.using(db_alias).all().delete()
    reason_for_testing_type.objects.using(db_alias).all().delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0155_historicalreasonfortesting_and_more",
        )
    ]
    operations = [
        migrations.RunPython(create_reason_for_testing_related_data, rollback_changes)
    ]
