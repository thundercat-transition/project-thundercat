from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, _):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # creating all needed settings
    tempplate_1 = gc_notify_template(
        folder="Accommodations",
        codename=GCNotifyTemplateCodenames.EXPIRED_ACCOMMODATION_REQUEST,
        template_id="1e5a05ff-885a-47b3-bd39-4421d121d8ac",
    )
    tempplate_1.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the setting
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.EXPIRED_ACCOMMODATION_REQUEST
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0146_updating_user_accomodation_statuses")]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
