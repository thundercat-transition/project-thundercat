from django.db import migrations
from backend.static.languages import Language_id
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_user_accommodation_complexities(apps, schema_editor):
    # get models
    user_accommodation_complexity = apps.get_model(
        "custom_models", "useraccommodationfilecomplexity"
    )
    user_accommodation_complexity_text = apps.get_model(
        "custom_models", "useraccommodationfilecomplexitytext"
    )

    # creating all needed codenames
    # Low
    codename_1 = user_accommodation_complexity(codename="low", order=1)
    codename_1.save()

    # Medium
    codename_2 = user_accommodation_complexity(codename="medium", order=2)
    codename_2.save()

    # High
    codename_3 = user_accommodation_complexity(codename="high", order=3)
    codename_3.save()

    # creating all needed complexity text
    # Low
    complexity_text_1_en = user_accommodation_complexity_text(
        user_accommodation_file_complexity_id=codename_1.id,
        text="Low",
        language_id=Language_id.EN,
    )
    complexity_text_1_en.save()

    complexity_text_1_fr = user_accommodation_complexity_text(
        user_accommodation_file_complexity_id=codename_1.id,
        text="Faible",
        language_id=Language_id.FR,
    )
    complexity_text_1_fr.save()

    # Medium
    complexity_text_2_en = user_accommodation_complexity_text(
        user_accommodation_file_complexity_id=codename_2.id,
        text="Medium",
        language_id=Language_id.EN,
    )
    complexity_text_2_en.save()

    complexity_text_2_fr = user_accommodation_complexity_text(
        user_accommodation_file_complexity_id=codename_2.id,
        text="Moyenne",
        language_id=Language_id.FR,
    )
    complexity_text_2_fr.save()

    # High
    complexity_text_3_en = user_accommodation_complexity_text(
        user_accommodation_file_complexity_id=codename_3.id,
        text="High",
        language_id=Language_id.EN,
    )
    complexity_text_3_en.save()

    complexity_text_3_fr = user_accommodation_complexity_text(
        user_accommodation_file_complexity_id=codename_3.id,
        text="Haute",
        language_id=Language_id.FR,
    )
    complexity_text_3_fr.save()


def rollback_changes(apps, schema_editor):
    # get models
    user_accommodation_complexity = apps.get_model(
        "custom_models", "useraccommodationfilecomplexity"
    )
    user_accommodation_complexity_text = apps.get_model(
        "custom_models", "useraccommodationfilecomplexitytext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # getting all respective user accommodation file complexity entries
    # Low
    user_accommodation_file_complexity_to_delete_1 = (
        user_accommodation_complexity.objects.using(db_alias).get(codename="low")
    )

    # Medium
    user_accommodation_file_complexity_to_delete_2 = (
        user_accommodation_complexity.objects.using(db_alias).get(codename="medium")
    )

    # High
    user_accommodation_file_complexity_to_delete_3 = (
        user_accommodation_complexity.objects.using(db_alias).get(codename="high")
    )

    # Deleting all respective user accommodation file complexity text entries
    # Low
    user_accommodation_complexity_text.objects.using(db_alias).filter(
        user_accommodation_file_complexity_id=user_accommodation_file_complexity_to_delete_1.id
    ).delete()

    # Medium
    user_accommodation_complexity_text.objects.using(db_alias).filter(
        user_accommodation_file_complexity_id=user_accommodation_file_complexity_to_delete_2.id
    ).delete()

    # High
    user_accommodation_complexity_text.objects.using(db_alias).filter(
        user_accommodation_file_complexity_id=user_accommodation_file_complexity_to_delete_3.id
    ).delete()

    # Deleting respective user accommodation file complexity entry
    # Low
    user_accommodation_file_complexity_to_delete_1.delete()

    # Medium
    user_accommodation_file_complexity_to_delete_2.delete()

    # High
    user_accommodation_file_complexity_to_delete_3.delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0144_historicaluseraccommodationfilecomplexity_and_more",
        )
    ]
    operations = [
        migrations.RunPython(create_user_accommodation_complexities, rollback_changes)
    ]
