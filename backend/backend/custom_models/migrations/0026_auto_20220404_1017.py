# Generated by Django 3.2.12 on 2022-04-04 14:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_models', '0025_auto_20220218_1250'),
    ]

    operations = [
        migrations.AddField(
            model_name='assignedtest',
            name='is_invalid',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='assignedtestsection',
            name='timed_out',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='historicalassignedtest',
            name='is_invalid',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='historicalassignedtestsection',
            name='timed_out',
            field=models.BooleanField(default=False),
        ),
    ]
