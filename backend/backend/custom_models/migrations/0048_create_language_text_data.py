from django.db import migrations


def create_new_language_data(apps, _):
    # get models
    language_text = apps.get_model("custom_models", "languagetext")

    # creating all needed language data
    language_text_1 = language_text(
        text="English",
        language_id=1,
        language_ref_id=1,
    )
    language_text_1.save()

    language_text_2 = language_text(
        text="Anglais",
        language_id=2,
        language_ref_id=1,
    )
    language_text_2.save()

    language_text_3 = language_text(
        text="French",
        language_id=1,
        language_ref_id=2,
    )
    language_text_3.save()

    language_text_4 = language_text(
        text="Français",
        language_id=2,
        language_ref_id=2,
    )
    language_text_4.save()


def rollback_changes(apps, schema_editor):
    # get models
    language_text = apps.get_model("custom_models", "languagetext")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete language data
    language_text.objects.using(db_alias).filter(language_ref_id=1).delete()
    language_text.objects.using(db_alias).filter(language_ref_id=2).delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0047_historicallanguagetext_languagetext",
        )
    ]
    operations = [migrations.RunPython(create_new_language_data, rollback_changes)]
