from django.db import migrations
from backend.static.languages import Language_id


CANCELLED_BY_SPECIALIST_OLD_NAME_EN = "CANCELLED BY CANDIDATE"
CANCELLED_BY_SPECIALIST_NEW_NAME_EN = "CANCELLED BY SPECIALIST"


def create_user_accommodation_statuses(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )

    # get db alias
    db_alias = schema_editor.connection.alias

    # update cancelled
    status = user_accommodation_status.objects.using(db_alias).get(codename="cancelled")
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = CANCELLED_BY_SPECIALIST_NEW_NAME_EN
    status_text_en.save()


def rollback_changes(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )

    # get db alias
    db_alias = schema_editor.connection.alias

    # update cancelled
    status = user_accommodation_status.objects.using(db_alias).get(codename="cancelled")
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = CANCELLED_BY_SPECIALIST_OLD_NAME_EN
    status_text_en.save()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0187_alter_virtualteamsmeetingsession_virtual_test_room",
        )
    ]
    operations = [
        migrations.RunPython(create_user_accommodation_statuses, rollback_changes)
    ]
