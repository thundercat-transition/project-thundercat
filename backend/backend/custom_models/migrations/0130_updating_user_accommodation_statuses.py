from django.db import migrations
from backend.static.languages import Language_id
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_user_accommodation_statuses(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # updating order of all existing statuses
    status = user_accommodation_status.objects.using(db_alias).get(codename="new")
    status.order = 1
    status.save()

    status = user_accommodation_status.objects.using(db_alias).get(
        codename="estimating_service_cost"
    )
    status.order = 2
    status.save()

    status = user_accommodation_status.objects.using(db_alias).get(
        codename="waiting_for_financial_coding"
    )
    status.order = 3
    status.save()

    status = user_accommodation_status.objects.using(db_alias).get(
        codename="waiting_for_documentation"
    )
    status.order = 4
    status.save()

    status = user_accommodation_status.objects.using(db_alias).get(codename="ready")
    status.order = 5
    status.save()

    status = user_accommodation_status.objects.using(db_alias).get(
        codename="in_progress"
    )
    status.order = 6
    status.save()

    status = user_accommodation_status.objects.using(db_alias).get(codename="on_hold")
    status.order = 7
    status.save()

    status = user_accommodation_status.objects.using(db_alias).get(codename="re_opened")
    status.order = 8
    status.save()

    status = user_accommodation_status.objects.using(db_alias).get(
        codename="re_opened_hold"
    )
    status.order = 9
    status.save()

    status = user_accommodation_status.objects.using(db_alias).get(codename="completed")
    status.order = 10
    status.save()

    status = user_accommodation_status.objects.using(db_alias).get(codename="cancelled")
    status.order = 11
    status.save()


def rollback_changes(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # putting back the order to 0 for all existing statuses
    all_statuses = user_accommodation_status.objects.using(db_alias).all()
    for status in all_statuses:
        status.order = 0
        status.save()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0129_historicaluseraccommodationfilestatus_active_and_more",
        )
    ]
    operations = [
        migrations.RunPython(create_user_accommodation_statuses, rollback_changes)
    ]
