from django.db import migrations


def create_new_test_room_data_translations(apps, schema_editor):
    #########################################################
    # GET MODELS
    #########################################################
    # Test Center Room Data
    test_center_room_data_model = apps.get_model("custom_models", "testcenterroomdata")
    # Other Details
    test_center_room_data_other_details_text_model = apps.get_model("custom_models", "testcenterroomdataotherdetailstext")

    # Language
    language_model = apps.get_model("custom_models", "Language")

    #########################################################
    # DB ALIAS
    #########################################################
    db_alias = schema_editor.connection.alias

    #########################################################
    # GET DATA
    #########################################################
    # get all test centers room data
    test_center_room_data = test_center_room_data_model.objects.using(db_alias).all()

    # get all languages
    languages = language_model.objects.using(db_alias).all()

    #########################################################
    # INSERT DATA
    #########################################################
    # creating all test center translations text entries
    for test_center_room in test_center_room_data:
        for language in languages:
            # Other Details
            test_center_room_data_other_details_text_object = test_center_room_data_other_details_text_model(text=test_center_room.other_details, language=language, test_center_room_data=test_center_room)
            test_center_room_data_other_details_text_object.save()


def rollback_changes(apps, schema_editor):
    #########################################################
    # GET MODELS
    #########################################################
    # Other Details
    test_center_room_data_other_details_text_model = apps.get_model("custom_models", "testcenterroomdataotherdetailstext")

    #########################################################
    # DB ALIAS
    #########################################################
    db_alias = schema_editor.connection.alias

    #########################################################
    # DELETE DATA
    #########################################################
    # delete all test center other details text
    test_center_room_data_other_details_text_model.objects.using(db_alias).all().delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0111_testcenterroomdataotherdetailstext_and_more",
        )
    ]
    operations = [migrations.RunPython(create_new_test_room_data_translations, rollback_changes)]
