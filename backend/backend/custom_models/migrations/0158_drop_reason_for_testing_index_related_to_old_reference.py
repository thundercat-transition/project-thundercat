from django.db import connection
from django.db import migrations
from django.conf import settings


def drop_indexes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """DROP INDEX [custom_models_assessmentprocessassignedtestspecs_reason_for_testing_id_b76a8945] ON {db_name}..custom_models_assessmentprocessassignedtestspecs""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """DROP INDEX [custom_models_historicalassessmentprocessassignedtestspecs_reason_for_testing_id_67731a61] ON {db_name}..custom_models_historicalassessmentprocessassignedtestspecs""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """DROP INDEX [custom_models_assessmentprocessdefaulttestspecs_reason_for_testing_id_bddf4e79] ON {db_name}..custom_models_assessmentprocessdefaulttestspecs""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """DROP INDEX [custom_models_historicalassessmentprocessdefaulttestspecs_reason_for_testing_id_121c01fc] ON {db_name}..custom_models_historicalassessmentprocessdefaulttestspecs""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """DROP INDEX [custom_models_orderlessfinancialdata_reason_for_testing_id_f5867705] ON {db_name}..custom_models_orderlessfinancialdata""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """DROP INDEX [custom_models_historicalorderlessfinancialdata_reason_for_testing_id_1a15ee69] ON {db_name}..custom_models_historicalorderlessfinancialdata""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE NONCLUSTERED INDEX [custom_models_assessmentprocessassignedtestspecs_reason_for_testing_id_b76a8945] ON {db_name}..custom_models_assessmentprocessassignedtestspecs ([reason_for_testing_old_id] ASC
                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """CREATE NONCLUSTERED INDEX [custom_models_historicalassessmentprocessassignedtestspecs_reason_for_testing_id_67731a61] ON {db_name}..custom_models_historicalassessmentprocessassignedtestspecs ([reason_for_testing_old_id] ASC
                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """CREATE NONCLUSTERED INDEX [custom_models_assessmentprocessdefaulttestspecs_reason_for_testing_id_bddf4e79] ON {db_name}..custom_models_assessmentprocessdefaulttestspecs
                ([reason_for_testing_old_id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """CREATE NONCLUSTERED INDEX [custom_models_historicalassessmentprocessdefaulttestspecs_reason_for_testing_id_121c01fc] ON {db_name}..custom_models_historicalassessmentprocessdefaulttestspecs
                ([reason_for_testing_old_id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """CREATE NONCLUSTERED INDEX [custom_models_orderlessfinancialdata_reason_for_testing_id_f5867705] ON {db_name}..custom_models_orderlessfinancialdata
                ([reason_for_testing_old_id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """CREATE NONCLUSTERED INDEX [custom_models_historicalorderlessfinancialdata_reason_for_testing_id_1a15ee69] ON {db_name}..custom_models_historicalorderlessfinancialdata
                ([reason_for_testing_old_id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0157_rename_reason_for_testing_assessmentprocessassignedtestspecs_reason_for_testing_old_and_more",
        ),
    ]
    operations = [migrations.RunPython(drop_indexes, rollback_changes)]
