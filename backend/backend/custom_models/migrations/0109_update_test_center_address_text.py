from django.db import migrations


def create_new_test_center_translations(apps, schema_editor):
    #########################################################
    # GET MODELS
    #########################################################
    # Test Center
    test_center_model = apps.get_model("custom_models", "testcenter")
    # Address
    test_center_address_text_model = apps.get_model("custom_models", "testcenteraddresstext")
    # City
    test_center_city_text_model = apps.get_model("custom_models", "testcentercitytext")
    # Province
    test_center_province_text_model = apps.get_model("custom_models", "testcenterprovincetext")
    # Other Details
    test_center_other_details_text_model = apps.get_model("custom_models", "testcenterotherdetailstext")

    # Language
    language_model = apps.get_model("custom_models", "Language")

    #########################################################
    # DB ALIAS
    #########################################################
    db_alias = schema_editor.connection.alias

    #########################################################
    # GET DATA
    #########################################################
    # get all test centers
    test_centers = test_center_model.objects.using(db_alias).all()

    # get all languages
    languages = language_model.objects.using(db_alias).all()

    #########################################################
    # INSERT DATA
    #########################################################
    # creating all test center translations text entries
    for test_center in test_centers:
        for language in languages:
            # Address
            test_center_address_text_object = test_center_address_text_model(text=test_center.address, language=language, test_center=test_center)
            test_center_address_text_object.save()

            # City
            test_center_city_text_object = test_center_city_text_model(text=test_center.city, language=language, test_center=test_center)
            test_center_city_text_object.save()

            # Province
            test_center_province_text_object = test_center_province_text_model(text=test_center.province, language=language, test_center=test_center)
            test_center_province_text_object.save()

            # Other Details
            test_center_other_details_text_object = test_center_other_details_text_model(text=test_center.other_details, language=language, test_center=test_center)
            test_center_other_details_text_object.save()


def rollback_changes(apps, schema_editor):
    #########################################################
    # GET MODELS
    #########################################################
    # Address
    test_center_address_text_model = apps.get_model("custom_models", "testcenteraddresstext")

    # City
    test_center_city_text_model = apps.get_model("custom_models", "testcentercitytext")

    # Province
    test_center_province_text_model = apps.get_model("custom_models", "testcenterprovincetext")

    # Other Details
    test_center_other_details_text_model = apps.get_model("custom_models", "testcenterotherdetailstext")

    #########################################################
    # DB ALIAS
    #########################################################
    db_alias = schema_editor.connection.alias

    #########################################################
    # DELETE DATA
    #########################################################
    # delete all test center address text
    test_center_address_text_model.objects.using(db_alias).all().delete()

    # delete all test center city text
    test_center_city_text_model.objects.using(db_alias).all().delete()

    # delete all test center province text
    test_center_province_text_model.objects.using(db_alias).all().delete()

    # delete all test center other details text
    test_center_other_details_text_model.objects.using(db_alias).all().delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0108_testcenterprovincetext_testcenterotherdetailstext_and_more",
        )
    ]
    operations = [migrations.RunPython(create_new_test_center_translations, rollback_changes)]
