from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """UPDATE {db_name}..custom_models_uitinvites
               SET test_id = (SELECT TOP 1 hutac.test_id 
			   FROM {db_name}..custom_models_historicalunsupervisedtestaccesscode hutac 
			   WHERE hutac.uit_invite_id = {db_name}..custom_models_uitinvites.id)
                """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """UPDATE {db_name}..custom_models_historicaluitinvites
               SET test_id = (SELECT TOP 1 hutac.test_id 
			   FROM {db_name}..custom_models_historicalunsupervisedtestaccesscode hutac 
			   WHERE hutac.uit_invite_id = {db_name}..custom_models_historicaluitinvites.id)
                """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """UPDATE {db_name}..custom_models_uitinvites SET test_id = NULL""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """UPDATE {db_name}..custom_models_historicaluitinvites SET test_id = NULL""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0115_historicaluitinvites_test_uitinvites_test",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
