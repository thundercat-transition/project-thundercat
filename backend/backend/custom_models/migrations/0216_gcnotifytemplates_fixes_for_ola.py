from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")

    db_alias = schema_editor.connection.alias

    # Correct the folders
    template_1 = gc_notify_template.objects.using(db_alias).get(
        codename=GCNotifyTemplateCodenames.HR_TRIGGERED_WITHDRAW
    )
    template_1.folder = "Supervised"
    template_1.save()

    template_2 = gc_notify_template.objects.using(db_alias).get(
        codename=GCNotifyTemplateCodenames.TEST_BUMPED_BY_TC
    )
    template_2.folder = "Supervised"
    template_2.save()

    # add 2 new templates

    template_3 = gc_notify_template(
        folder="OLA",
        codename=GCNotifyTemplateCodenames.OLA_CODE_BOOKING,
        template_id="40b68965-476e-45d0-8de4-c18f3f84f2c3",
    )
    template_3.save()
    template_4 = gc_notify_template(
        folder="OLA",
        codename=GCNotifyTemplateCodenames.OLA_CODE_BOOK_LATER,
        template_id="6f9a0c6d-4b78-4e29-84d7-53d4ddf5acfa",
    )
    template_4.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")

    db_alias = schema_editor.connection.alias

    # un-correct the folders
    template_1 = gc_notify_template.objects.using(db_alias).get(
        codename=GCNotifyTemplateCodenames.HR_TRIGGERED_WITHDRAW
    )
    template_1.folder = "OLA"
    template_1.save()

    template_2 = gc_notify_template.objects.using(db_alias).get(
        codename=GCNotifyTemplateCodenames.TEST_BUMPED_BY_TC
    )
    template_2.folder = "OLA"
    template_2.save()

    # delete the new templates
    gc_notify_template.objects.using(db_alias).get(
        codename=GCNotifyTemplateCodenames.OLA_CODE_BOOKING
    ).delete()

    gc_notify_template.objects.using(db_alias).get(
        codename=GCNotifyTemplateCodenames.OLA_CODE_BOOK_LATER
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0215_gcnotifytemplates_insert_spam_templates")]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
