from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    db_alias = schema_editor.connection.alias
    # creating all needed settings
    tempplate_1 = gc_notify_template(
        folder="Accomodations",
        codename=GCNotifyTemplateCodenames.NEW_ACCOMODATIONS_REQUEST_HR,
        template_id="35da1696-b79e-4a86-a5fd-4daa343c3e01",
    )
    tempplate_1.save()
    tempplate_2 = gc_notify_template(
        folder="Accomodations",
        codename=GCNotifyTemplateCodenames.CANCELED_ACCOMODATIONS_REQUEST,
        template_id="ebb4492e-19d3-4996-b023-10d604aa4472",
    )
    tempplate_2.save()
    tempplate_3 = gc_notify_template(
        folder="Accomodations",
        codename=GCNotifyTemplateCodenames.CANCELED_ACCOMODATIONS_REQUEST_CANDIDATE,
        template_id="27bae4a5-576f-45d7-bd5d-458daa0abe7b",
    )
    tempplate_3.save()
    tempplate_4 = gc_notify_template(
        folder="Accomodations",
        codename=GCNotifyTemplateCodenames.COMPLETED_ACCOMODATIONS_REQUEST,
        template_id="8af31265-dd5e-4f98-827a-298dd4baa2a2",
    )
    tempplate_4.save()
    tempplate_5 = gc_notify_template(
        folder="Accomodations",
        codename=GCNotifyTemplateCodenames.RE_OPENED_ACCOMODATIONS_REQUEST,
        template_id="896c33e2-df93-46d3-aa86-0a9c375a1762",
    )
    tempplate_5.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the setting
    gc_notify_template.objects.using(db_alias).get(
        codename=GCNotifyTemplateCodenames.NEW_ACCOMODATIONS_REQUEST_HR
    ).delete()
    gc_notify_template.objects.using(db_alias).get(
        codename=GCNotifyTemplateCodenames.CANCELED_ACCOMODATIONS_REQUEST
    ).delete()
    gc_notify_template.objects.using(db_alias).get(
        codename=GCNotifyTemplateCodenames.CANCELED_ACCOMODATIONS_REQUEST_CANDIDATE
    ).delete()
    gc_notify_template.objects.using(db_alias).get(
        codename=GCNotifyTemplateCodenames.COMPLETED_ACCOMODATIONS_REQUEST
    ).delete()
    gc_notify_template.objects.using(db_alias).get(
        codename=GCNotifyTemplateCodenames.RE_OPENED_ACCOMODATIONS_REQUEST
    ).delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0140_historicaltestcenter_accommodations_friendly_and_more",
        )
    ]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
