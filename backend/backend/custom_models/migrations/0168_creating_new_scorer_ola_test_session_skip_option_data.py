from django.db import migrations
from backend.static.scorer_ola_test_session_skip_option import (
    ScorerOlaTestSessionSkipOptionCodenameConst,
)
from backend.static.languages import Language_id


def create_scorer_ola_test_session_skip_option_data(apps, schema_editor):
    # get models
    scorer_ola_test_session_skip_option = apps.get_model(
        "custom_models", "scorerolatestsessionskipoption"
    )
    scorer_ola_test_session_skip_option_text = apps.get_model(
        "custom_models", "scorerolatestsessionskipoptiontext"
    )

    # ==================== READING ====================
    # IMP_STAFFING
    scorer_ola_test_session_skip_option_var = scorer_ola_test_session_skip_option(
        codename=ScorerOlaTestSessionSkipOptionCodenameConst.CONFLICT_FAMILY,
    )
    scorer_ola_test_session_skip_option_var.save()

    scorer_ola_test_session_skip_option_text_en = scorer_ola_test_session_skip_option_text(
        scorer_ola_test_session_skip_option_id=scorer_ola_test_session_skip_option_var.id,
        language_id=Language_id.EN,
        text="Conflict of Interest - Family",
    )
    scorer_ola_test_session_skip_option_text_en.save()

    scorer_ola_test_session_skip_option_text_fr = scorer_ola_test_session_skip_option_text(
        scorer_ola_test_session_skip_option_id=scorer_ola_test_session_skip_option_var.id,
        language_id=Language_id.FR,
        text="FR Conflict of Interest - Family",
    )
    scorer_ola_test_session_skip_option_text_fr.save()


def rollback_changes(apps, schema_editor):
    # get models
    scorer_ola_test_session_skip_option = apps.get_model(
        "custom_models", "scorerolatestsessionskipoption"
    )
    scorer_ola_test_session_skip_option_text = apps.get_model(
        "custom_models", "scorerolatestsessionskipoptiontext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # deleting all reason for testing related data
    scorer_ola_test_session_skip_option_text.objects.using(db_alias).all().delete()
    scorer_ola_test_session_skip_option.objects.using(db_alias).all().delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0167_historicalscorerolaassignedtestsession_and_more",
        )
    ]
    operations = [
        migrations.RunPython(
            create_scorer_ola_test_session_skip_option_data, rollback_changes
        )
    ]
