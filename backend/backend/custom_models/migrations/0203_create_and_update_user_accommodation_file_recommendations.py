from django.db import migrations
from backend.static.user_accommodation_file_recommendations_const import (
    UserAccommodationFileRecommendationsConst,
)
from backend.static.languages import Language_id


def create_new_data(apps, schema_editor):
    # get models
    recommendations_option = apps.get_model(
        "custom_models", "useraccommodationfilerecommendationsoption"
    )
    recommendations_option_text = apps.get_model(
        "custom_models", "useraccommodationfilerecommendationsoptiontext"
    )

    # get db alias
    db_alias = schema_editor.connection.alias

    # ==================== ADDING NEW RECOMMENDATIONS ====================
    # adding new recommendations option codenames
    recommendation_1 = recommendations_option(
        codename=UserAccommodationFileRecommendationsConst.BREAKS
    )
    recommendation_1.save()
    recommendation_2 = recommendations_option(
        codename=UserAccommodationFileRecommendationsConst.REPETITION
    )
    recommendation_2.save()

    # adding new recommendations option text
    recommendation_text_en = recommendations_option_text(
        text="Breaks",
        user_accommodation_file_recommendations_option_id=recommendation_1.id,
        language_id=Language_id.EN,
    )
    recommendation_text_en.save()

    recommendation_text_fr = recommendations_option_text(
        text="Pauses",
        user_accommodation_file_recommendations_option_id=recommendation_1.id,
        language_id=Language_id.FR,
    )
    recommendation_text_fr.save()

    recommendation_text_en = recommendations_option_text(
        text="Repetition",
        user_accommodation_file_recommendations_option_id=recommendation_2.id,
        language_id=Language_id.EN,
    )
    recommendation_text_en.save()

    recommendation_text_fr = recommendations_option_text(
        text="Répétition",
        user_accommodation_file_recommendations_option_id=recommendation_2.id,
        language_id=Language_id.FR,
    )
    recommendation_text_fr.save()
    # ==================== ADDING NEW RECOMMENDATIONS (END) ====================

    # ==================== DEACTIVATING RECOMMENDATIONS ====================
    # deactivating "ADAPTED_SCORING"
    recommendations_option_to_update = recommendations_option.objects.using(
        db_alias
    ).get(codename=UserAccommodationFileRecommendationsConst.ADAPTED_SCORING)
    recommendations_option_to_update.active = 0
    recommendations_option_to_update.save()

    # deactivating "INDIVIDUAL_TEST_SESSION"
    recommendations_option_to_update = recommendations_option.objects.using(
        db_alias
    ).get(codename=UserAccommodationFileRecommendationsConst.INDIVIDUAL_TEST_SESSION)
    recommendations_option_to_update.active = 0
    recommendations_option_to_update.save()
    # ==================== DEACTIVATING RECOMMENDATIONS (END) ====================


def rollback_changes(apps, schema_editor):
    # get models
    recommendations_option = apps.get_model(
        "custom_models", "useraccommodationfilerecommendationsoption"
    )
    recommendations_option_text = apps.get_model(
        "custom_models", "useraccommodationfilerecommendationsoptiontext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # deleting needed data
    recommendations_options_to_delete = recommendations_option.objects.using(
        db_alias
    ).filter(
        codename__in=(
            UserAccommodationFileRecommendationsConst.BREAKS,
            UserAccommodationFileRecommendationsConst.REPETITION,
        )
    )
    for recommendations_option_to_delete in recommendations_options_to_delete:
        recommendations_option_text.objects.using(db_alias).filter(
            user_accommodation_file_recommendations_option_id=recommendations_option_to_delete.id
        ).delete()
        recommendations_option_to_delete.delete()

    # ==================== REACTIVATING RECOMMENDATIONS ====================
    # reactivating "ADAPTED_SCORING"
    recommendations_option_to_update = recommendations_option.objects.using(
        db_alias
    ).get(codename=UserAccommodationFileRecommendationsConst.ADAPTED_SCORING)
    recommendations_option_to_update.active = 1
    recommendations_option_to_update.save()

    # reactivating "INDIVIDUAL_TEST_SESSION"
    recommendations_option_to_update = recommendations_option.objects.using(
        db_alias
    ).get(codename=UserAccommodationFileRecommendationsConst.INDIVIDUAL_TEST_SESSION)
    recommendations_option_to_update.active = 1
    recommendations_option_to_update.save()
    # ==================== REACTIVATING RECOMMENDATIONS (END) ====================


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0202_historicaltestcentertestsessiondata_candidate_phone_number_and_more",
        )
    ]
    operations = [migrations.RunPython(create_new_data, rollback_changes)]
