# Generated by Django 3.2.14 on 2022-08-24 17:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_models', '0037_create_new_criticality_data'),
    ]

    operations = [
        migrations.AddField(
            model_name='criticality',
            name='codename',
            field=models.CharField(default='temp', max_length=50),
        ),
        migrations.AddField(
            model_name='criticality',
            name='priority',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='historicalcriticality',
            name='codename',
            field=models.CharField(default='temp', max_length=50),
        ),
        migrations.AddField(
            model_name='historicalcriticality',
            name='priority',
            field=models.IntegerField(default=0),
        ),
    ]
