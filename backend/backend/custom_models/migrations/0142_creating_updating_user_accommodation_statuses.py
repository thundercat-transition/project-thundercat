from django.db import migrations
from backend.static.languages import Language_id

COMPLETED_STATUS_OLD_NAME_EN = "CLOSED"
COMPLETED_STATUS_OLD_NAME_FR = "FERMÉE"

COMPLETED_STATUS_NEW_NAME_EN = "COMPLETED"
COMPLETED_STATUS_NEW_NAME_FR = "COMPLÉTÉ"


def create_user_accommodation_statuses(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )

    # get db alias
    db_alias = schema_editor.connection.alias

    # updating order of statuses with order >= 10
    # completed
    codename_order_to_update = user_accommodation_status.objects.using(db_alias).get(
        codename="completed"
    )
    codename_order_to_update.order = 11
    codename_order_to_update.save()

    # cancelled
    codename_order_to_update = user_accommodation_status.objects.using(db_alias).get(
        codename="cancelled"
    )
    codename_order_to_update.order = 12
    codename_order_to_update.save()

    # cancelled_by_candidate
    codename_order_to_update = user_accommodation_status.objects.using(db_alias).get(
        codename="cancelled_by_candidate"
    )
    codename_order_to_update.order = 13
    codename_order_to_update.save()

    # creating all needed codenames
    codename_1 = user_accommodation_status(codename="pending_approval", order=10)
    codename_1.save()

    # creating all needed status text
    status_text_1_en = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_1.id,
        text="PENDING APPROVAL",
        language_id=Language_id.EN,
    )
    status_text_1_en.save()
    status_text_1_fr = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_1.id,
        text="FR PENDING APPROVAL",
        language_id=Language_id.FR,
    )
    status_text_1_fr.save()

    # updating COMPLETED status text
    data_to_update = user_accommodation_status.objects.using(db_alias).get(
        codename="completed"
    )
    text_data_to_update_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=data_to_update.id, language_id=Language_id.EN
    )
    text_data_to_update_en.text = COMPLETED_STATUS_NEW_NAME_EN
    text_data_to_update_en.save()
    text_data_to_update_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=data_to_update.id, language_id=Language_id.FR
    )
    text_data_to_update_fr.text = COMPLETED_STATUS_NEW_NAME_FR
    text_data_to_update_fr.save()


def rollback_changes(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # updating order of statuses with order >= 10
    # completed
    codename_order_to_update = user_accommodation_status.objects.using(db_alias).get(
        codename="completed"
    )
    codename_order_to_update.order = 10
    codename_order_to_update.save()

    # cancelled
    codename_order_to_update = user_accommodation_status.objects.using(db_alias).get(
        codename="cancelled"
    )
    codename_order_to_update.order = 11
    codename_order_to_update.save()

    # cancelled_by_candidate
    codename_order_to_update = user_accommodation_status.objects.using(db_alias).get(
        codename="cancelled_by_candidate"
    )
    codename_order_to_update.order = 12
    codename_order_to_update.save()

    # getting all respective user accommodation file status entries
    user_accommodation_file_status_to_delete = user_accommodation_status.objects.using(
        db_alias
    ).get(codename="pending_approval")

    # Deleting all respective user accommodation file status text entries
    user_accommodation_status_text.objects.using(db_alias).filter(
        user_accommodation_file_status_id=user_accommodation_file_status_to_delete.id
    ).delete()

    # Deleting respective user accommodation file status entry
    user_accommodation_file_status_to_delete.delete()

    # updating COMPLETED status text
    data_to_update = user_accommodation_status.objects.using(db_alias).get(
        codename="completed"
    )
    text_data_to_update_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=data_to_update.id, language_id=Language_id.EN
    )
    text_data_to_update_en.text = COMPLETED_STATUS_OLD_NAME_EN
    text_data_to_update_en.save()
    text_data_to_update_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=data_to_update.id, language_id=Language_id.FR
    )
    text_data_to_update_fr.text = COMPLETED_STATUS_OLD_NAME_FR
    text_data_to_update_fr.save()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0141_accomodations_email_templates",
        )
    ]
    operations = [
        migrations.RunPython(create_user_accommodation_statuses, rollback_changes)
    ]
