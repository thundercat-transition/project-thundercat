from django.db import migrations
from backend.static.languages import Language_id


NEW_OLD_NAME_EN = "NEW"
NEW_OLD_NAME_FR = "NOUVEAU"
NEW_NEW_NAME_EN = "NEW"
NEW_NEW_NAME_FR = "NOUVELLE"


def create_user_accommodation_statuses(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )

    # get db alias
    db_alias = schema_editor.connection.alias

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(codename="new")
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = NEW_NEW_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = NEW_NEW_NAME_EN
    status_text_en.save()


def rollback_changes(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )

    # get db alias
    db_alias = schema_editor.connection.alias

    # update cancelled by candidate
    status = user_accommodation_status.objects.using(db_alias).get(codename="new")
    status_text_fr = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=2
    )
    status_text_fr.text = NEW_OLD_NAME_FR
    status_text_fr.save()
    status_text_en = user_accommodation_status_text.objects.using(db_alias).get(
        user_accommodation_file_status_id=status.id, language_id=1
    )
    status_text_en.text = NEW_OLD_NAME_EN
    status_text_en.save()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0182_remove_historicalscorerolareasonforunassigningoption_history_user_and_more",
        )
    ]
    operations = [
        migrations.RunPython(create_user_accommodation_statuses, rollback_changes)
    ]
