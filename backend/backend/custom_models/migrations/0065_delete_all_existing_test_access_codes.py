from django.db import migrations


def delete_all_existing_test_access_codes(apps, schema_editor):
    # get models
    test_access_code = apps.get_model("custom_models", "testaccesscode")
    # get db alias
    db_alias = schema_editor.connection.alias

    # deleting all existing test access codes
    all_existing_test_access_codes = test_access_code.objects.using(db_alias).all()
    for code in all_existing_test_access_codes:
        code.delete()


def rollback_changes(apps, schema_editor):
    # get models
    test_access_code = apps.get_model("custom_models", "testaccesscode")

    # DO NOTHING


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0064_reservationcodes_historicalreservationcodes_and_more",
        )
    ]
    operations = [
        migrations.RunPython(delete_all_existing_test_access_codes, rollback_changes)
    ]
