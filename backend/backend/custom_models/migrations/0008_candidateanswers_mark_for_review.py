# Generated by Django 2.2.4 on 2020-12-17 02:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('custom_models', '0007_create_new_ta_action_types'),
    ]

    operations = [
        migrations.AddField(
            model_name='candidateanswers',
            name='mark_for_review',
            field=models.BooleanField(default=False),
        ),
    ]
