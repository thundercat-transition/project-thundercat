from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """UPDATE {db_name}..custom_models_virtualteamsmeetingsession SET test_session_id = NULL""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0196_testcenterolatimeslot_slots_to_prioritize_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
