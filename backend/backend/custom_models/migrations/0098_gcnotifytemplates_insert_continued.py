from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    db_alias = schema_editor.connection.alias
    # creating all needed settings
    tempplate_1 = gc_notify_template(
        folder="UIT",
        codename=GCNotifyTemplateCodenames.FAILED_UIT_DELIVERY,
        template_id="f7e72c80-f483-4749-a278-12d40a759ff4",
    )
    tempplate_1.save()
    tempplate_2 = gc_notify_template.objects.using(db_alias).get(
        codename="faileddelivery"
    )
    tempplate_2.codename = GCNotifyTemplateCodenames.FAILED_SUP_DELIVERY
    tempplate_2.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the setting
    gc_notify_template.objects.using(db_alias).get(
        codename=GCNotifyTemplateCodenames.FAILED_UIT_DELIVERY
    ).delete()
    tempplate = gc_notify_template.objects.using(db_alias).get(
        codename=GCNotifyTemplateCodenames.FAILED_SUP_DELIVERY
    )
    tempplate.codename = "faileddelivery"
    tempplate.save()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0097_alter_assessmentprocess_closing_date_and_more",
        )
    ]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
