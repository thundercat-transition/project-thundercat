from django.db import migrations
from backend.static.languages import Language_id
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_user_accommodation_statuses(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )

    # creating all needed codenames
    codename_1 = user_accommodation_status(codename="sent")
    codename_1.save()
    codename_2 = user_accommodation_status(codename="in_progress")
    codename_2.save()
    codename_3 = user_accommodation_status(codename="on_hold")
    codename_3.save()
    codename_4 = user_accommodation_status(codename="completed")
    codename_4.save()
    codename_5 = user_accommodation_status(codename="rejected")
    codename_5.save()

    # creating all needed status text
    status_text_1_en = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_1.id,
        text="SENT",
        language_id=Language_id.EN,
    )
    status_text_1_en.save()
    status_text_1_fr = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_1.id,
        text="FR SENT",
        language_id=Language_id.FR,
    )
    status_text_1_fr.save()

    status_text_2_en = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_2.id,
        text="IN PROGRESS",
        language_id=Language_id.EN,
    )
    status_text_2_en.save()
    status_text_2_fr = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_2.id,
        text="FR IN PROGRESS",
        language_id=Language_id.FR,
    )
    status_text_2_fr.save()

    status_text_3_en = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_3.id,
        text="ON HOLD",
        language_id=Language_id.EN,
    )
    status_text_3_en.save()
    status_text_3_fr = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_3.id,
        text="FR ON HOLD",
        language_id=Language_id.FR,
    )
    status_text_3_fr.save()

    status_text_4_en = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_4.id,
        text="COMPLETED",
        language_id=Language_id.EN,
    )
    status_text_4_en.save()
    status_text_4_fr = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_4.id,
        text="FR COMPLETED",
        language_id=Language_id.FR,
    )
    status_text_4_fr.save()

    status_text_5_en = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_5.id,
        text="REJECTED",
        language_id=Language_id.EN,
    )
    status_text_5_en.save()
    status_text_5_fr = user_accommodation_status_text(
        user_accommodation_file_status_id=codename_5.id,
        text="FR REJECTED",
        language_id=Language_id.FR,
    )
    status_text_5_fr.save()


def rollback_changes(apps, schema_editor):
    # get models
    user_accommodation_status = apps.get_model(
        "custom_models", "useraccommodationfilestatus"
    )
    user_accommodation_status_text = apps.get_model(
        "custom_models", "useraccommodationfilestatustext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # Deleting all existing user accommodation file status text entries
    user_accommodation_status_text.objects.using(db_alias).all().delete()

    # Deleting all existing user accommodation file status entries
    user_accommodation_status.objects.using(db_alias).all().delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0122_historicaluseraccommodationfile_and_more")]
    operations = [
        migrations.RunPython(create_user_accommodation_statuses, rollback_changes)
    ]
