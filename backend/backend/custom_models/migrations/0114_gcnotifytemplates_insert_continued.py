from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, _):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # creating all needed templates
    tempplate_1 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.FAILED_SEND_ASSIGNED_TEST_TO_ADMINISTER_DELIVERY,
        template_id="bbc7212d-c85c-4ccf-b26f-a52b8ce82d64",
    )
    tempplate_1.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the templates
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.FAILED_SEND_ASSIGNED_TEST_TO_ADMINISTER_DELIVERY
    ).delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "custom_models",
            "0113_remove_historicaltestcenterroomdata_other_details_and_more",
        )
    ]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
