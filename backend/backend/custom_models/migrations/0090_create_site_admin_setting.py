from django.db import migrations
from backend.static.site_admin_setting_type import SiteAdminSettingType


def create_new_uit_reasons_for_deletion(apps, _):
    # get models
    site_admin_setting = apps.get_model("custom_models", "siteadminsetting")
    # creating all needed settings
    setting_1 = site_admin_setting(
        en_description="Disable user logins",
        fr_description="FR Disable user logins",
        codename=SiteAdminSettingType.LOCK_LOGIN,
        is_active=False,
    )
    setting_1.save()
    setting_2 = site_admin_setting(
        en_description="Send emails with GC Notify",
        fr_description="FR Send emails with GC Notify",
        codename=SiteAdminSettingType.ENABLE_GC_NOTIFY,
        is_active=False,
    )
    setting_2.save()


def rollback_changes(apps, schema_editor):
    # get models
    site_admin_setting = apps.get_model("custom_models", "siteadminsetting")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the setting
    site_admin_setting.objects.using(db_alias).filter(
        codename=SiteAdminSettingType.LOCK_LOGIN
    ).delete()
    site_admin_setting.objects.using(db_alias).filter(
        codename=SiteAdminSettingType.ENABLE_GC_NOTIFY
    ).delete()


class Migration(migrations.Migration):
    dependencies = [
        ("custom_models", "0089_siteadminsetting_historicalsiteadminsetting")
    ]
    operations = [
        migrations.RunPython(create_new_uit_reasons_for_deletion, rollback_changes)
    ]
