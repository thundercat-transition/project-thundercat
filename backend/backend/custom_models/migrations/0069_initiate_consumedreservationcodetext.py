from django.db import migrations
from backend.static.consumed_reservation_code_statuses import (
    StaticConsumedReservationCodeStatus,
)


def create_new_consumed_reservation_code_status(apps, _):
    # get models
    consumed_reservation_code_status = apps.get_model(
        "custom_models", "consumedreservationcodestatus"
    )
    consumed_reservation_code_status_text = apps.get_model(
        "custom_models", "consumedreservationcodestatustext"
    )
    # creating all consumed reservation code status code_names
    reason_1 = consumed_reservation_code_status(
        codename=StaticConsumedReservationCodeStatus.BOOKED
    )
    reason_1.save()
    reason_2 = consumed_reservation_code_status(
        codename=StaticConsumedReservationCodeStatus.WITHDRAWN
    )
    reason_2.save()
    reason_3 = consumed_reservation_code_status(codename="completed")
    reason_3.save()
    reason_4 = consumed_reservation_code_status(
        codename="booklater"  # changing to remove unused status from consumed_reservation_code_statuses.py
    )
    reason_4.save()

    # creating all consumed reservation code status text
    text_1_en = consumed_reservation_code_status_text(
        consumed_reservation_code_status_id=1, language_id=1, text="Booked"
    )
    text_1_en.save()
    text_1_fr = consumed_reservation_code_status_text(
        consumed_reservation_code_status_id=1, language_id=2, text="FR Booked"
    )
    text_1_fr.save()
    text_2_en = consumed_reservation_code_status_text(
        consumed_reservation_code_status_id=2, language_id=1, text="Withdrawn"
    )
    text_2_en.save()
    text_2_fr = consumed_reservation_code_status_text(
        consumed_reservation_code_status_id=2, language_id=2, text="FR Withdrawn"
    )
    text_2_fr.save()
    text_3_en = consumed_reservation_code_status_text(
        consumed_reservation_code_status_id=3, language_id=1, text="Completed"
    )
    text_3_en.save()
    text_3_fr = consumed_reservation_code_status_text(
        consumed_reservation_code_status_id=3, language_id=2, text="FR Completed"
    )
    text_3_fr.save()
    text_4_en = consumed_reservation_code_status_text(
        consumed_reservation_code_status_id=4, language_id=1, text="Booklater"
    )
    text_4_en.save()
    text_4_fr = consumed_reservation_code_status_text(
        consumed_reservation_code_status_id=4, language_id=2, text="FR Booklater"
    )
    text_4_fr.save()


def rollback_changes(apps, schema_editor):
    # get models
    consumed_reservation_code_status = apps.get_model(
        "custom_models", "consumedreservationcodestatus"
    )
    consumed_reservation_code_status_text = apps.get_model(
        "custom_models", "consumedreservationcodestatustext"
    )
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the text
    consumed_reservation_code_status_text.objects.using(db_alias).filter(
        consumed_reservation_code_status_id=1, language_id=1
    ).delete()
    consumed_reservation_code_status_text.objects.using(db_alias).filter(
        consumed_reservation_code_status_id=1, language_id=2
    ).delete()
    consumed_reservation_code_status_text.objects.using(db_alias).filter(
        consumed_reservation_code_status_id=2, language_id=1
    ).delete()
    consumed_reservation_code_status_text.objects.using(db_alias).filter(
        consumed_reservation_code_status_id=2, language_id=2
    ).delete()
    consumed_reservation_code_status_text.objects.using(db_alias).filter(
        consumed_reservation_code_status_id=3, language_id=1
    ).delete()
    consumed_reservation_code_status_text.objects.using(db_alias).filter(
        consumed_reservation_code_status_id=3, language_id=2
    ).delete()
    consumed_reservation_code_status_text.objects.using(db_alias).filter(
        consumed_reservation_code_status_id=4, language_id=1
    ).delete()
    consumed_reservation_code_status_text.objects.using(db_alias).filter(
        consumed_reservation_code_status_id=4, language_id=2
    ).delete()
    # delete the statues
    consumed_reservation_code_status.objects.using(db_alias).filter(
        codename=StaticConsumedReservationCodeStatus.BOOKED
    ).delete()
    consumed_reservation_code_status.objects.using(db_alias).filter(
        codename=StaticConsumedReservationCodeStatus.WITHDRAWN
    ).delete()
    consumed_reservation_code_status.objects.using(db_alias).filter(
        codename="completed"
    ).delete()
    consumed_reservation_code_status.objects.using(db_alias).filter(
        codename="booklater"  # changing to remove unused status from consumed_reservation_code_statuses.py
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0068_consumedreservationcodestatus_and_more")]
    operations = [
        migrations.RunPython(
            create_new_consumed_reservation_code_status, rollback_changes
        )
    ]
