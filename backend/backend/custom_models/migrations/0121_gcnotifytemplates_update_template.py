from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames

OLD_RESERVATION_CODE_BOOKING = "02011418-e861-411e-9ae2-4648109748df"
NEW_RESERVATION_CODE_BOOKING = "e3094ad8-bc39-4231-a9d3-e1bbb848b535"

def update_gc_notify_template_reservation_code_booking(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # Change the template_id to new value
    template = gc_notify_template.objects.using(db_alias).get(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.RESERVATION_CODE_BOOKING
    )
    template.template_id = NEW_RESERVATION_CODE_BOOKING
    template.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # Change back the template_id
    template = gc_notify_template.objects.using(db_alias).get(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.RESERVATION_CODE_BOOKING
    )
    template.template_id = OLD_RESERVATION_CODE_BOOKING
    template.save()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0120_gcnotifytemplates_update_template")]
    operations = [
        migrations.RunPython(update_gc_notify_template_reservation_code_booking, rollback_changes)
    ]
