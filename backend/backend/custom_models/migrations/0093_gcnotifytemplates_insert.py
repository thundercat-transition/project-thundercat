from django.db import migrations
from backend.static.gc_notify_template_codenames import GCNotifyTemplateCodenames


def create_new_gc_notify_templates(apps, _):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # creating all needed settings
    tempplate_1 = gc_notify_template(
        folder="2fa",
        codename=GCNotifyTemplateCodenames.TWO_FACTOR_AUTHENTICATION,
        template_id="a6cd6697-3713-4513-8525-5f6c02774a7d",
    )
    tempplate_1.save()
    tempplate_2 = gc_notify_template(
        folder="UIT",
        codename=GCNotifyTemplateCodenames.UIT_INVITE_BASIC,
        template_id="35a7c396-6a15-414a-9391-2577e2e491af",
    )
    tempplate_2.save()
    tempplate_3 = gc_notify_template(
        folder="UIT",
        codename="uitinvitebb",
        template_id="7fbd1ed7-bd9e-486c-874e-57702872dd79",
    )
    tempplate_3.save()
    tempplate_4 = gc_notify_template(
        folder="UIT",
        codename="uitinviteet",
        template_id="6004dfa9-aff0-4577-a6f2-f644e6374838",
    )
    tempplate_4.save()
    tempplate_5 = gc_notify_template(
        folder="UIT",
        codename="uitinviteetbb",
        template_id="2537b795-f5c2-4f86-b816-ac7ef852cf46",
    )
    tempplate_5.save()
    tempplate_6 = gc_notify_template(
        folder="Supervised",
        codename=GCNotifyTemplateCodenames.RESERVATION_INVITE,
        template_id="f29d07df-5298-4f13-880c-a1266c1d2d0c",
    )
    tempplate_6.save()
    tempplate_7 = gc_notify_template(
        folder="Misc",
        codename=GCNotifyTemplateCodenames.SHARE_RESULT,
        template_id="f71aa6a8-2dcc-4863-8a4b-5ff2e4241055",
    )
    tempplate_7.save()


def rollback_changes(apps, schema_editor):
    # get models
    gc_notify_template = apps.get_model("custom_models", "gcnotifytemplate")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the setting
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.TWO_FACTOR_AUTHENTICATION
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.UIT_INVITE_BASIC
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(codename="uitinvitebb").delete()
    gc_notify_template.objects.using(db_alias).filter(codename="uitinviteet").delete()
    gc_notify_template.objects.using(db_alias).filter(codename="uitinviteetbb").delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.RESERVATION_INVITE
    ).delete()
    gc_notify_template.objects.using(db_alias).filter(
        codename=GCNotifyTemplateCodenames.SHARE_RESULT
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0092_gcnotifytemplate")]
    operations = [
        migrations.RunPython(create_new_gc_notify_templates, rollback_changes)
    ]
