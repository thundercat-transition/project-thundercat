from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.test_center import TestCenter


class UserAccommodationFileStatus(models.Model):
    codename = models.CharField(max_length=50, null=False, blank=False)
    active = models.BooleanField(blank=False, null=False, default=1)
    order = models.IntegerField(blank=False, null=False, default=0)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_user_accommodation_file_status_codename",
                fields=["codename"],
            )
        ]

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
