from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.scorer_ola_test_session_skip_option import (
    ScorerOlaTestSessionSkipOption,
)
from backend.custom_models.test_center_test_sessions import TestCenterTestSessions
from user_management.user_management_models.user_models import User


class ScorerOlaTestSessionSkipAction(models.Model):
    id = models.AutoField(primary_key=True)
    test_session = models.ForeignKey(
        TestCenterTestSessions, to_field="id", on_delete=models.SET_NULL, null=True
    )
    test_assessor = models.ForeignKey(
        User,
        to_field="id",
        on_delete=models.DO_NOTHING,
        related_name="sotssa_test_assessor_id",
        null=False,
        blank=False,
    )
    candidate = models.ForeignKey(
        User,
        to_field="id",
        on_delete=models.DO_NOTHING,
        related_name="sotssa_candidate_id",
        null=False,
        blank=False,
    )
    scorer_ola_test_session_skip_option = models.ForeignKey(
        ScorerOlaTestSessionSkipOption,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    modify_date = models.DateTimeField(auto_now=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
