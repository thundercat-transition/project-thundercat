from django.db import models
from simple_history.models import HistoricalRecords

from backend.custom_models.language import Language
from backend.custom_models.reason_for_testing import ReasonForTesting


class ReasonForTestingText(models.Model):
    reason_for_testing = models.ForeignKey(
        ReasonForTesting,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    text = models.CharField(max_length=150, null=False, blank=False)
    language = models.ForeignKey(
        Language,
        to_field="language_id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_reason_for_testing_id_and_language_id_combination",
                fields=[
                    "reason_for_testing",
                    "language",
                ],
            )
        ]
