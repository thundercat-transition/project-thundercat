from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.user_accommodation_file import UserAccommodationFile
from cms.cms_models.test_definition import TestDefinition


class UserAccommodationFileTestToAdminister(models.Model):
    user_accommodation_file = models.ForeignKey(
        UserAccommodationFile,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    test = models.ForeignKey(
        TestDefinition,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=False,
    )
    specified_test_description = models.CharField(max_length=175, null=True, blank=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
