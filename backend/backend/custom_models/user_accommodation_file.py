import django
from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.user_accommodation_file_complexity import (
    UserAccommodationFileComplexity,
)
from backend.custom_models.accommodation_request import AccommodationRequest
from backend.custom_models.user_accommodation_file_status import (
    UserAccommodationFileStatus,
)
from backend.custom_models.test_center import TestCenter
from user_management.user_management_models.user_models import User


class UserAccommodationFile(models.Model):
    comments = models.TextField(null=False, blank=True)
    is_uit = models.BooleanField()
    modify_date = models.DateTimeField(auto_now=True)
    created_date = models.DateField(auto_now_add=True)
    reason_for_action = models.CharField(null=True, blank=False, max_length=500)
    rationale = models.TextField(null=False, blank=True)
    is_alternate_test_request = models.BooleanField(default=0)
    test_center = models.ForeignKey(
        TestCenter,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=False,
    )
    status = models.ForeignKey(
        UserAccommodationFileStatus,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    user = models.ForeignKey(
        User,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    last_modified_by_user = models.ForeignKey(
        User,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
        related_name="uaf_last_modified_by_user",
    )
    assigned_to_user = models.ForeignKey(
        User,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=False,
        related_name="uaf_assigned_to_user",
    )
    accommodation_request = models.ForeignKey(
        AccommodationRequest,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=False,
    )
    complexity = models.ForeignKey(
        UserAccommodationFileComplexity,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=False,
    )
    candidate_phone_number = models.CharField(
        max_length=10, null=True, blank=False, default=None
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
