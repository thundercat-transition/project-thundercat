from django.db import models
from backend.custom_models.test_center import TestCenter
from user_management.user_management_models.user_models import User


# links assessor to test, adds additional details


class TestCenterOlaTestAssessor(models.Model):
    id = models.AutoField(primary_key=True)
    test_center = models.ForeignKey(TestCenter, on_delete=models.DO_NOTHING, null=False)
    user = models.ForeignKey(
        User,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    supervisor = models.BooleanField(default=False)
