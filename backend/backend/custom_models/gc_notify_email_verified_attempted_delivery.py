from django.db import models
from backend.custom_models.uit_invites import UITInvites
from backend.custom_models.assessment_process import AssessmentProcess
from .gc_notify_template import GCNotifyTemplate

# stores emails that have been sent and failed to delivery
# NOTE: this shares many columns with GCNotifyEmailPendingDelivery, but it needs to be a seperate table
# NOTE: NEVER QUERY THIS TABLE IN CODE. IT WILL GROW MASSIVE QUICKLY


class GCNotifyEmailVerifiedAttemptedDelivery(models.Model):
    id = models.AutoField(primary_key=True)
    gc_notify_template = models.ForeignKey(
        GCNotifyTemplate,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )  # local model refering to the GC Notify template
    sent_date = models.DateTimeField(blank=False, null=False)
    email_to = models.EmailField(max_length=254, blank=False, null=False)
    email_contact_if_fail = models.EmailField(
        max_length=254, blank=True, null=True
    )  # who to contact if this fails to deliver; usually just a UITInvite or AssessmentProcess
    first_name_to = models.CharField(max_length=30, null=False, blank=False)
    last_name_to = models.CharField(max_length=150, null=False, blank=False)
    uit_invite = models.ForeignKey(
        UITInvites,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
    )  # local model refering to the UITInvites model
    assessment_process = models.ForeignKey(
        AssessmentProcess,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
    )  # Link back to AssessmentProcess, if valid

    email_subject = models.TextField(null=True, blank=True)
    email_body = models.TextField(null=True, blank=True)

    # Columns not in GCNotifyEmailPendingDelivery
    status_date = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=30, null=False, blank=False)

    # column not in GCNotifyFailedBulkDelivery
    is_delivered = models.BooleanField()
