from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.assigned_test import AssignedTest
from cms.cms_models.test_section import TestSection


class AssignedQuestionsList(models.Model):
    assigned_test = models.ForeignKey(
        AssignedTest, to_field="id", on_delete=models.DO_NOTHING
    )
    test_section = models.ForeignKey(
        TestSection, to_field="id", on_delete=models.DO_NOTHING
    )
    questions_list = models.TextField(blank=False, null=False)
    from_item_bank = models.BooleanField(default=False)

    # for auditing purpose and it stores create, update, delete actions on this model in a seperate historical table
    history = HistoricalRecords()
