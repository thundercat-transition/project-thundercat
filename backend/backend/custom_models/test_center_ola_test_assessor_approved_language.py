from django.db import models
from backend.custom_models.test_center_ola_test_assessor import (
    TestCenterOlaTestAssessor,
)
from backend.custom_models.language import Language


# stores data related to the ola TA's language profiles


class TestCenterOlaTestAssessorApprovedLanguage(models.Model):
    id = models.AutoField(primary_key=True)
    test_center_ola_test_assessor = models.ForeignKey(
        TestCenterOlaTestAssessor, on_delete=models.DO_NOTHING, null=False
    )
    language = models.ForeignKey(
        Language,
        to_field="language_id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
