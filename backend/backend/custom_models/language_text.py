from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.language import Language


class LanguageText(models.Model):
    text = models.CharField(max_length=50)
    language_ref = models.ForeignKey(
        Language, on_delete=models.DO_NOTHING, related_name="language_ref"
    )
    language = models.ForeignKey(
        Language, on_delete=models.DO_NOTHING, related_name="language"
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
