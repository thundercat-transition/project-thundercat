from django.db import models
from simple_history.models import HistoricalRecords


class ScorerOlaTestSessionSkipOption(models.Model):
    id = models.AutoField(primary_key=True)
    codename = models.CharField(max_length=50, null=False, blank=False)
    active = models.BooleanField(null=False, blank=False, default=1)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_scorer_ola_test_session_skip_option_codename",
                fields=["codename"],
            )
        ]

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
