from django.db import models
from simple_history.models import HistoricalRecords


class TestCenter(models.Model):
    name = models.CharField(max_length=150, blank=False, null=False)
    postal_code = models.CharField(max_length=15, blank=True, null=False)
    # putting Canada (CAT_REF_COUNTRY_VW.CNTRY_ID = 38) as the default country
    country_id = models.IntegerField(blank=False, null=False, default=38)
    security_email = models.EmailField(max_length=254, blank=True, null=True)
    department_id = models.IntegerField(blank=False, null=False)
    booking_delay = models.IntegerField(blank=False, null=False, default=48)
    accommodations_friendly = models.BooleanField(default=False)
    modify_date = models.DateTimeField(auto_now=True)
    ola_authorized = models.BooleanField(blank=False, null=False, default=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
