from django.db import models
from simple_history.models import HistoricalRecords

# model to track the UIT invitations
class UITReasonsForDeletion(models.Model):
    en_name = models.CharField(max_length=50, default="", null=False, blank=False)
    fr_name = models.CharField(max_length=50, default="", null=False, blank=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
