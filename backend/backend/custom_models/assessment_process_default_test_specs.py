from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.test_skill_type import TestSkillType
from backend.custom_models.assessment_process import AssessmentProcess
from backend.custom_models.reasons_for_testing import ReasonsForTesting
from backend.custom_models.reason_for_testing import ReasonForTesting


class AssessmentProcessDefaultTestSpecs(models.Model):
    assessment_process = models.ForeignKey(
        AssessmentProcess, to_field="id", on_delete=models.DO_NOTHING
    )
    test_skill_type = models.ForeignKey(
        TestSkillType, to_field="id", on_delete=models.DO_NOTHING
    )
    # cannot be a foreign key because the source of the ID can be linked to different tables/models or even NULL
    test_skill_sub_type_id = models.IntegerField(blank=False, null=True)
    reason_for_testing = models.ForeignKey(
        ReasonForTesting,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=False,
        default=None,
    )
    level_required = models.CharField(max_length=30, null=True, blank=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_assessment_process_test_skill_and_test_skill_sub_type_combination",
                fields=[
                    "assessment_process",
                    "test_skill_type",
                    "test_skill_sub_type_id",
                ],
            )
        ]
