from django.db import models
from backend.custom_models.test_center import TestCenter

# Contains ola specifc settings for a test center


class TestCenterOlaConfigs(models.Model):
    id = models.AutoField(primary_key=True)
    test_center = models.ForeignKey(TestCenter, on_delete=models.DO_NOTHING, null=False)
    booking_delay = models.IntegerField(blank=False, null=False)
    advanced_booking_delay = models.IntegerField(blank=False, null=False)
