from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.language import Language
from backend.custom_models.scorer_ola_test_session_skip_option import (
    ScorerOlaTestSessionSkipOption,
)


class ScorerOlaTestSessionSkipOptionText(models.Model):
    id = models.AutoField(primary_key=True)
    scorer_ola_test_session_skip_option = models.ForeignKey(
        ScorerOlaTestSessionSkipOption,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    text = models.CharField(max_length=150, null=False, blank=False)
    language = models.ForeignKey(
        Language,
        to_field="language_id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
