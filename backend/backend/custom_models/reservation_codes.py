from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.assessment_process_assigned_test_specs import (
    AssessmentProcessAssignedTestSpecs,
)


class ReservationCodes(models.Model):
    assessment_process_assigned_test_specs = models.ForeignKey(
        AssessmentProcessAssignedTestSpecs,
        to_field="id",
        on_delete=models.DO_NOTHING,
    )
    created_date = models.DateField()
    reservation_code = models.CharField(max_length=14, null=False, blank=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_assessment_process_assigned_test_specs_id",
                fields=["assessment_process_assigned_test_specs"],
            )
        ]
