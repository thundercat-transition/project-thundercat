from django.db import models

# stores template ids for GC Notify


class GCNotifyTemplate(models.Model):
    id = models.AutoField(primary_key=True)
    folder = models.CharField(
        max_length=100, null=True, blank=True
    )  # for reference/debugging only; this won't be used by the code
    codename = models.CharField(max_length=100, null=False, blank=False)
    template_id = models.CharField(max_length=36, null=False, blank=False)
