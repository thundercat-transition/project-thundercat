from django.db import models
from backend.custom_models.uit_invites import UITInvites
from backend.custom_models.assessment_process import AssessmentProcess
from .gc_notify_template import GCNotifyTemplate
from .uit_invites import UITInvites

# stores emails that have been sent, but we have not confirmed delivery for


class GCNotifyEmailPendingDelivery(models.Model):
    id = models.AutoField(primary_key=True)
    gc_notify_email_id = models.CharField(max_length=36, null=False, blank=False)
    gc_notify_template = models.ForeignKey(
        GCNotifyTemplate,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )  # local model refering to the GC Notify template
    sent_date = models.DateTimeField(auto_now=True)
    email_to = models.EmailField(max_length=254, blank=False, null=False)
    email_contact_if_fail = models.EmailField(
        max_length=254, blank=True, null=True
    )  # who to contact if this fails to deliver; usually just a UITInvite or AssessmentProcess
    first_name_to = models.CharField(max_length=30, null=False, blank=False)
    last_name_to = models.CharField(max_length=150, null=False, blank=False)
    uit_invite = models.ForeignKey(
        UITInvites,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
    )  # local model refering to the UITInvites model
    assessment_process = models.ForeignKey(
        AssessmentProcess,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
    )  # Link back to AssessmentProcess, if valid

    email_subject = models.TextField(null=True, blank=True)
    email_body = models.TextField(null=True, blank=True)

    # Does not exist in GCNotifyEmailFailedBulkDelivery or GCNotifyEmailVerifiedAttemptedDelivery
    last_checked_date = models.DateTimeField(auto_now=True)
