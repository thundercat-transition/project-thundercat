from django.db import models


class ConsumedReservationCodeStatus(models.Model):
    # codename for internal use
    codename = models.CharField(max_length=14, null=False, blank=False)
