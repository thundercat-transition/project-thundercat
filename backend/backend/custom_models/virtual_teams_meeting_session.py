from django.db import models
from backend.custom_models.test_center_test_sessions import TestCenterTestSessions
from backend.custom_models.virtual_test_room import VirtualTestRoom

from user_management.user_management_models.user_models import User


# Used to track MS Teams meeting infromation, including the join link and the id (in case a deletion is needed)
class VirtualTeamsMeetingSession(models.Model):
    id = models.AutoField(primary_key=True)
    virtual_test_room = models.ForeignKey(
        VirtualTestRoom,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=False,
    )
    meeting_id = models.CharField(max_length=175, blank=False, null=False)
    meeting_link = models.CharField(max_length=255, blank=False, null=False)
    candidate = models.ForeignKey(
        User,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    test_administrator = models.ForeignKey(
        User,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
        related_name="test_administrator_user",
    )
    date = models.DateField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    test_session = models.ForeignKey(
        TestCenterTestSessions,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=False,
    )
