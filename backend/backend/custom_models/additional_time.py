from django.db import models
from simple_history.models import HistoricalRecords
from backend.custom_models.accommodation_request import AccommodationRequest
from cms.cms_models.test_section import TestSection


class AdditionalTime(models.Model):
    test_section_time = models.IntegerField(blank=False, null=False)
    test_section = models.ForeignKey(
        TestSection,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    accommodation_request = models.ForeignKey(
        AccommodationRequest,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
