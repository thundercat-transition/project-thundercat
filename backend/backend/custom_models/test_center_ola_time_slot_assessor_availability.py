from django.db import models
from backend.custom_models.test_center_ola_test_assessor import (
    TestCenterOlaTestAssessor,
)
from backend.custom_models.test_center_ola_time_slot import TestCenterOlaTimeSlot


class TestCenterOlaTimeSlotAssessorAvailability(models.Model):
    test_center_ola_time_slot = models.ForeignKey(
        TestCenterOlaTimeSlot,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        blank=False,
    )
    test_center_ola_test_assessor = models.ForeignKey(
        TestCenterOlaTestAssessor, on_delete=models.DO_NOTHING, null=False
    )
