from django.db import models
from simple_history.models import HistoricalRecords


class AssignedTestStatus(models.Model):
    codename = models.CharField(max_length=50, null=False, blank=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_assigned_test_status_codename",
                fields=["codename"],
            )
        ]
