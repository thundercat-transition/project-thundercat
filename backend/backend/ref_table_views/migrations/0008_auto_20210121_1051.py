# Generated by Django 2.2.4 on 2021-01-21 15:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ref_table_views', '0007_oltf_province_vw'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='catemployerstsvw',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='oltfclassificationvw',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='oltfgendervw',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='oltforganisationvw',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='oltfprovincevw',
            options={'managed': False},
        ),
    ]
