# Generated by Django 2.2.4 on 2020-12-23 16:59

import datetime
from pytz import timezone
from django.conf import settings
from django.db import migrations, models


def create_cat_aboriginal_codes(apps, schema_editor):
    if not settings.IS_LOCAL:
        return

    now = datetime.datetime.now(timezone("US/Eastern"))
    cat_aboriginal = apps.get_model("ref_table_views", "CatRefAboriginalVW")

    aboriginal1 = cat_aboriginal(
        abrg_id=1, active_flg=1, legacy_cd=1, edesc="Inuit", fdesc="Inuit", efdt=now
    )
    aboriginal1.save()

    aboriginal2 = cat_aboriginal(
        abrg_id=2, active_flg=1, legacy_cd=2, edesc="Métis", fdesc="Métis", efdt=now
    )
    aboriginal2.save()

    aboriginal3 = cat_aboriginal(
        abrg_id=3,
        active_flg=1,
        legacy_cd=3,
        edesc="North-American Indian/First Nation",
        fdesc="Indien de l'Amérique du Nord/Première nation",
        efdt=now,
    )
    aboriginal3.save()


def delete_cat_aboriginal_codes(apps, schema_editor):
    if not settings.IS_LOCAL:
        return

    db_alias = schema_editor.connection.alias
    cat_aboriginal = apps.get_model("ref_table_views", "CatRefAboriginalVW")

    aboriginal1 = (
        cat_aboriginal.objects.using(db_alias)
        .filter(abrg_id=1, active_flg=1, legacy_cd=1, edesc="Inuit", fdesc="Inuit")
        .last()
    )
    aboriginal1.delete()

    aboriginal2 = (
        cat_aboriginal.objects.using(db_alias)
        .filter(abrg_id=2, active_flg=1, legacy_cd=2, edesc="Métis", fdesc="Métis")
        .last()
    )
    aboriginal2.delete()

    aboriginal3 = (
        cat_aboriginal.objects.using(db_alias)
        .filter(
            abrg_id=3,
            active_flg=1,
            legacy_cd=3,
            edesc="North-American Indian/First Nation",
            fdesc="Indien de l'Amérique du Nord/Première nation",
        )
        .last()
    )
    aboriginal3.delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "ref_table_views",
            "0020_cat_ref_vw_data",
        )
    ]

    operations = [
        migrations.RunPython(create_cat_aboriginal_codes, delete_cat_aboriginal_codes),
    ]
