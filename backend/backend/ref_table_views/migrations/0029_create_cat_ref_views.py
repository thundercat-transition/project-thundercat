from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    # Execute only in environments
    if settings.IS_LOCAL:
        return

    with connection.cursor() as cursor:
        cursor.execute(
            """
            CREATE OR ALTER VIEW [dbo].[CAT_REF_COUNTRY_VW]
                AS
                    SELECT A.* FROM [REF].[dbo].[COUNTRY_CODES]  AS A
                    INNER JOIN [REF].[dbo].APPL_COUNTRY_CDS  AS B ON A.CNTRY_ID = B.CNTRY_ID
                    WHERE B.[APPL_ACRONYM] = 'CAT'
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    # Execute only in environments
    if settings.IS_LOCAL:
        return

    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "ref_table_views",
            "0028_deleting_unwanted_indexed_from_cat_ref_gender_vw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
