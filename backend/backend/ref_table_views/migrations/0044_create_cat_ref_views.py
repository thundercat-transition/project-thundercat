from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    # Execute only in environments
    if settings.IS_LOCAL:
        return

    with connection.cursor() as cursor:
        cursor.execute(
            """
                /*     CREATE CAT_REF_ABORIGINAL_VW  */
                CREATE OR ALTER VIEW CAT_REF_ABORIGINAL_VW AS
                SELECT ABRG_ID,ACTIVE_FLG,EABRV,FABRV,EDESC,FDESC,EFDT,XDT
                FROM COMMON_LOOKUP.dbo.APPLICATION_ABORIGINAL_VW
                WHERE APPL_ACRONYM = 'CAT' 

            """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                /*     CREATE CAT_REF_CLASSIFICATION_VW  */
                CREATE OR ALTER VIEW CAT_REF_CLASSIFICATION_VW AS
                SELECT CLASSIF_ID,ACTIVE_FLG,CLASS_GRP_CD,CLASS_SBGRP_CD,CLASS_LVL_CD,EFDT,XDT,PSC_CLASS_IND,DFLT_BUD_CD
                ,EQLZN_AMT,EQLZN_AMT_EFDT,EQLZN_AMT_XDT,OFCR_LVL_IND
                FROM COMMON_LOOKUP.dbo.APPLICATION_CLASSIFICATION_VW
                WHERE APPL_ACRONYM = 'CAT'   
                
            """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                /*     CREATE CAT_REF_COUNTRY_VW  */
                CREATE OR ALTER VIEW CAT_REF_COUNTRY_VW AS
                SELECT CNTRY_ID,ACTIVE_FLG,EABRV,FABRV,EDESC,FDESC,EFDT,XDT
                FROM COMMON_LOOKUP.dbo.APPLICATION_COUNTRY_VW
                WHERE APPL_ACRONYM = 'CAT' 
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                /*     CREATE CAT_REF_CTAB_AREA_RESIDENCES_VW  */
                CREATE OR ALTER VIEW CAT_REF_CTAB_AREA_RESIDENCES_VW AS
                SELECT AREA_RES_ID,ACTIVE_FLG,EDESC,FDESC,EFDT,XDT,PRESENTATION_ORDER
                FROM COMMON_LOOKUP.dbo.APPLICATION_AREA_RESIDENCE_VW
                WHERE APPL_ACRONYM = 'CAT' 
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                /*     CREATE CAT_REF_DAY_OF_WEEK_VW  */
                CREATE OR ALTER VIEW CAT_REF_DAY_OF_WEEK_VW AS
                SELECT [day_text_id] ID, label text, day_id day_of_week_id, language_id
                FROM [COMMON_LOOKUP].[dbo].[day_text]
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                /*     CREATE CAT_REF_DEPARTMENTS_VW  */
                CREATE OR ALTER VIEW CAT_REF_DEPARTMENTS_VW AS
                SELECT DEPT_ID,ACTIVE_FLG,EABRV,FABRV,EDESC,FDESC,EFDT,XDT
                FROM COMMON_LOOKUP.dbo.APPLICATION_DEPARTMENT_VW
                WHERE APPL_ACRONYM = 'CAT' 
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                /*     CREATE CAT_REF_DISABILITY_VW  */
                CREATE OR ALTER VIEW CAT_REF_DISABILITY_VW AS
                SELECT DSBL_ID,ACTIVE_FLG,EABRV,FABRV,EDESC,FDESC,EFDT,XDT, FULL_EDESC, FULL_FDESC
                FROM COMMON_LOOKUP.dbo.APPLICATION_DISABILITY_VW
                WHERE APPL_ACRONYM = 'CAT' 
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                /*     CREATE CAT_REF_EMPLOYER_STS_VW  */
                CREATE OR ALTER VIEW CAT_REF_EMPLOYER_STS_VW AS
                SELECT EMPSTS_ID,ACTIVE_FLG,EDESC,FDESC,EFDT,XDT
                FROM COMMON_LOOKUP.dbo.APPLICATION_EMPLOYER_TYPE_VW
                WHERE APPL_ACRONYM = 'CAT' 
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                /*     CREATE CAT_REF_GENDER_VW  */
                CREATE OR ALTER VIEW CAT_REF_GENDER_VW AS
                SELECT GND_ID,ACTIVE_FLG,EABRV,FABRV,EDESC,FDESC,EFDT,XDT
                FROM COMMON_LOOKUP.dbo.APPLICATION_GENDER_VW
                WHERE APPL_ACRONYM = 'CAT' 
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                /*     CREATE CAT_REF_LANGUAGE_VW  */
                CREATE OR ALTER VIEW CAT_REF_LANGUAGE_VW AS
                SELECT OL_ID language_id, ISO_code_1, ISO_code_2, EDESC,FDESC, EFDT date_from, xdt date_to
                FROM COMMON_LOOKUP.dbo.APPLICATION_LANGUAGE_VW
                WHERE APPL_ACRONYM = 'CAT' 
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                /*     CREATE CAT_REF_PROVINCE_VW  */
                CREATE OR ALTER VIEW CAT_REF_PROVINCE_VW AS
                SELECT PROV_ID,ACTIVE_FLG,EABRV,FABRV,EDESC,FDESC,EFDT,XDT
                FROM COMMON_LOOKUP.dbo.APPLICATION_PROVINCE_VW
                WHERE APPL_ACRONYM = 'CAT' 
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """
                /*     CREATE CAT_REF_VISIBLE_MINORITY_VW  */
                CREATE OR ALTER VIEW CAT_REF_VISIBLE_MINORITY_VW AS
                SELECT VISMIN_ID,ACTIVE_FLG,EDESC,FDESC,EFDT,XDT
                FROM COMMON_LOOKUP.dbo.APPLICATION_VISIBLE_MINORITY_VW
                WHERE APPL_ACRONYM = 'CAT' 
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    # Execute only in environments
    if settings.IS_LOCAL:
        return

    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "ref_table_views",
            "0043_create_cat_ref_day_of_week_vw_and_cat_ref_language_vw_data",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
