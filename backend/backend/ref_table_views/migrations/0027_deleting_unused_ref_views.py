from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        # LOCAL ENVIRONMENT
        if settings.IS_LOCAL:
            # ==================== OLTF VIEWS ====================
            # OLTF CLASSIFICATION
            cursor.execute("DROP TABLE IF EXISTS [dbo].[OLTF_CLASSIFICATION_VW]")

            # OLTF DEPARTMENTS
            cursor.execute("DROP TABLE IF EXISTS [dbo].[OLTF_DEPARTEMENTS_VW]")

            # OLTF GENDER
            cursor.execute("DROP TABLE IF EXISTS [dbo].[OLTF_GENDER_VW]")

            # OLTF ORGANISATION
            cursor.execute("DROP TABLE IF EXISTS [dbo].[OLTF_ORGANISATION_VW]")

            # OLTF PROVINCE
            cursor.execute("DROP TABLE IF EXISTS [dbo].[OLTF_PROVINCE_VW]")
            # ==================== OLTF VIEWS (END) ====================

            # ==================== OLD CAT VIEWS ====================
            # CAT ABORIGINAL
            cursor.execute("DROP TABLE IF EXISTS [dbo].[CAT_ABORIGINAL_VW]")

            # CAT DISABILITY
            cursor.execute("DROP TABLE IF EXISTS [dbo].[CAT_DISABILITY_VW]")

            # CAT EMPLOYER_STS
            cursor.execute("DROP TABLE IF EXISTS [dbo].[CAT_EMPLOYER_STS_VW]")

            # CAT VISIBLE_MINORITY
            cursor.execute("DROP TABLE IF EXISTS [dbo].[CAT_VISIBLE_MINORITY_VW]")

            # CAT DEPARTMENTS
            cursor.execute("DROP TABLE IF EXISTS [dbo].[CAT_DEPARTMENTS_VW]")

            # CAT GENDER
            cursor.execute("DROP TABLE IF EXISTS [dbo].[CAT_GENDER_VW]")
            # ==================== OLD CAT VIEWS (END) ====================

        # DEV/TEST/PROD ENVIRONMENT
        else:
            # ==================== OLTF VIEWS ====================
            # OLTF CLASSIFICATION
            cursor.execute("DROP VIEW IF EXISTS [dbo].[OLTF_CLASSIFICATION_VW]")

            # OLTF DEPARTMENTS
            cursor.execute("DROP VIEW IF EXISTS [dbo].[OLTF_DEPARTEMENTS_VW]")

            # OLTF GENDER
            cursor.execute("DROP VIEW IF EXISTS [dbo].[OLTF_GENDER_VW]")

            # OLTF ORGANISATION
            cursor.execute("DROP VIEW IF EXISTS [dbo].[OLTF_ORGANISATION_VW]")

            # OLTF PROVINCE
            cursor.execute("DROP VIEW IF EXISTS [dbo].[OLTF_PROVINCE_VW]")
            # ==================== OLTF VIEWS (END) ====================

            # ==================== OLD CAT VIEWS ====================
            # CAT ABORIGINAL
            cursor.execute("DROP VIEW IF EXISTS [dbo].[CAT_ABORIGINAL_VW]")

            # CAT DISABILITY
            cursor.execute("DROP VIEW IF EXISTS [dbo].[CAT_DISABILITY_VW]")

            # CAT EMPLOYER_STS
            cursor.execute("DROP VIEW IF EXISTS [dbo].[CAT_EMPLOYER_STS_VW]")

            # CAT VISIBLE_MINORITY
            cursor.execute("DROP VIEW IF EXISTS [dbo].[CAT_VISIBLE_MINORITY_VW]")

            # CAT DEPARTMENTS
            cursor.execute("DROP VIEW IF EXISTS [dbo].[CAT_DEPARTMENTS_VW]")

            # CAT GENDER
            cursor.execute("DROP VIEW IF EXISTS [dbo].[CAT_GENDER_VW]")
            # ==================== OLD CAT VIEWS (END) ====================


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "ref_table_views",
            "0026_alter_historicalcateducation_options_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
