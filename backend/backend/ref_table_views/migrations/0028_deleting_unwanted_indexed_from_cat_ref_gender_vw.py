from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            /****** Object:  Index [ref_table_views_historicalcatrefgendervw_history_date_d9393723]    Script Date: 2024-04-11 11:10:05 ******/
            DROP INDEX [ref_table_views_historicalcatrefgendervw_history_date_d9393723] ON [dbo].[ref_table_views_historicalcatrefgendervw]
            """
        )

        cursor.execute(
            """
            /****** Object:  Index [ref_table_views_historicaloltfgendervw_GND_ID_fcde7341]    Script Date: 2024-04-11 11:00:00 ******/
            DROP INDEX [ref_table_views_historicaloltfgendervw_GND_ID_fcde7341] ON [dbo].[ref_table_views_historicalcatrefgendervw]
        """
        )

        cursor.execute(
            """
            /****** Object:  Index [ref_table_views_historicaloltfgendervw_history_user_id_74ac4795]    Script Date: 2024-04-11 11:00:28 ******/
            DROP INDEX [ref_table_views_historicaloltfgendervw_history_user_id_74ac4795] ON [dbo].[ref_table_views_historicalcatrefgendervw]
        """
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """
            /****** Object:  Index [ref_table_views_historicalcatrefgendervw_history_date_d9393723]    Script Date: 2024-04-11 11:10:19 ******/
            CREATE NONCLUSTERED INDEX [ref_table_views_historicalcatrefgendervw_history_date_d9393723] ON [dbo].[ref_table_views_historicalcatrefgendervw]
            (
                [history_date] ASC
            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
        """
        )

        cursor.execute(
            """
            /****** Object:  Index [ref_table_views_historicaloltfgendervw_GND_ID_fcde7341]    Script Date: 2024-04-11 10:57:27 ******/
            CREATE NONCLUSTERED INDEX [ref_table_views_historicaloltfgendervw_GND_ID_fcde7341] ON [dbo].[ref_table_views_historicalcatrefgendervw]
            (
                [GND_ID] ASC
            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
        """
        )

        cursor.execute(
            """
            /****** Object:  Index [ref_table_views_historicaloltfgendervw_history_user_id_74ac4795]    Script Date: 2024-04-11 10:59:07 ******/
            CREATE NONCLUSTERED INDEX [ref_table_views_historicaloltfgendervw_history_user_id_74ac4795] ON [dbo].[ref_table_views_historicalcatrefgendervw]
            (
                [history_user_id] ASC
            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
        """
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "ref_table_views",
            "0027_deleting_unused_ref_views",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
