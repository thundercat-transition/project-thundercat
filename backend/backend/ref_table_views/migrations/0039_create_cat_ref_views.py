from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    # Execute only in environments
    if settings.IS_LOCAL:
        return

    with connection.cursor() as cursor:
        cursor.execute(
            """
                CREATE OR ALTER VIEW [dbo].[CAT_REF_CTAB_AREA_RESIDENCES_VW]
                    AS
                        SELECT * 
                        FROM REF.dbo.CTAB_AREA_RESIDENCES
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    # Execute only in environments
    if settings.IS_LOCAL:
        return

    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "ref_table_views",
            "0038_alter_catrefresidencevw_options",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
