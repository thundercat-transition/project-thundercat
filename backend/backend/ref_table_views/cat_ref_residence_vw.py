from django.db import models
from simple_history.models import HistoricalRecords


# ** IMPORTANT **
# If any changes are made to this model, please make sure to notify CAMM team (CAT/CAMM Relation Logic)
class CatRefResidenceVW(models.Model):
    area_res_id = models.IntegerField(primary_key=True, db_column="AREA_RES_ID")
    active_flg = models.CharField(db_column="ACTIVE_FLG", max_length=1)
    edesc = models.CharField(db_column="EDESC", max_length=200)
    fdesc = models.CharField(db_column="FDESC", max_length=200)
    efdt = models.DateTimeField(db_column="EFDT")
    xdt = models.DateTimeField(db_column="XDT", blank=True, null=True)
    presentation_order = models.IntegerField(db_column="PRESENTATION_ORDER")

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        managed = True
        db_table = "CAT_REF_CTAB_AREA_RESIDENCES_VW"
