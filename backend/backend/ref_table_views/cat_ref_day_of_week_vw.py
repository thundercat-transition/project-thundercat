from django.db import models
from simple_history.models import HistoricalRecords


class CatRefDayOfWeekVW(models.Model):
    id = models.IntegerField(primary_key=True, db_column="id")
    day_of_week_id = models.IntegerField(db_column="day_of_week_id")
    text = models.CharField(db_column="text", max_length=50)
    language_id = models.IntegerField(db_column="language_id")

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        managed = True
        db_table = "CAT_REF_DAY_OF_WEEK_VW"
