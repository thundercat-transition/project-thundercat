from django.db import models
from simple_history.models import HistoricalRecords


class CatRefCountryVW(models.Model):
    cntry_id = models.IntegerField(primary_key=True, db_column="CNTRY_ID")
    active_flg = models.CharField(db_column="ACTIVE_FLG", max_length=1)
    eabrv = models.CharField(db_column="EABRV", max_length=64, null=True, blank=True)
    fabrv = models.CharField(db_column="FABRV", max_length=64, null=True, blank=True)
    edesc = models.CharField(db_column="EDESC", max_length=200)
    fdesc = models.CharField(db_column="FDESC", max_length=200)
    efdt = models.DateTimeField(db_column="EFDT")
    xdt = models.DateTimeField(db_column="XDT", blank=True, null=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        managed = True
        db_table = "CAT_REF_COUNTRY_VW"
