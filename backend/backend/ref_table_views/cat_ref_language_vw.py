from django.db import models
from simple_history.models import HistoricalRecords


class CatRefLanguageVW(models.Model):
    language_id = models.IntegerField(primary_key=True, db_column="language_id")
    iso_code_1 = models.CharField(db_column="iso_code_1", max_length=50)
    iso_code_2 = models.CharField(db_column="iso_code_2", max_length=50)
    edesc = models.CharField(db_column="edesc", max_length=50)
    fdesc = models.CharField(db_column="fdesc", max_length=50)
    date_from = models.DateTimeField(db_column="date_from")
    date_to = models.DateTimeField(db_column="date_to", null=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        managed = True
        db_table = "CAT_REF_LANGUAGE_VW"
