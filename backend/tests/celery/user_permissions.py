from backend.ref_table_views.cat_ref_day_of_week_vw import CatRefDayOfWeekVW
from backend.custom_models.language import Language
from backend.custom_models.test_center_field_translations.test_center_room_data_other_details_text import (
    TestCenterRoomDataOtherDetailsText,
)
from backend.custom_models.test_center_test_administrators import (
    TestCenterTestAdministrators,
)
from backend.custom_models.test_center_test_session_data import (
    TestCenterTestSessionData,
)

from backend.custom_models.test_center_ola_test_assessor import (
    TestCenterOlaTestAssessor,
)
from backend.custom_models.test_center_ola_test_assessor_approved_language import (
    TestCenterOlaTestAssessorApprovedLanguage,
)
from cms.cms_models.test_skill_type import TestSkillType
from backend.custom_models.test_center_ola_assessor_unavailability import (
    TestCenterOlaAssessorUnavailability,
)
from backend.custom_models.test_center_ola_time_slot_assessor_availability import (
    TestCenterOlaTimeSlotAssessorAvailability,
)
from backend.custom_models.test_center_ola_time_slot import TestCenterOlaTimeSlot

from backend.custom_models.test_center_test_sessions import TestCenterTestSessions
from backend.custom_models.test_center_room_data import TestCenterRoomData
from backend.custom_models.test_center_rooms import TestCenterRooms
from backend.custom_models.test_center import TestCenter
from user_management.user_management_models.user_models import User
from cms.cms_models.orderless_test_permissions_model import OrderlessTestPermissions
from user_management.user_management_models.ta_extended_profile_models import (
    TaExtendedProfile,
)
from cms.cms_models.test_permissions_model import TestPermissions
from cms.cms_models.test_definition import TestDefinition
from backend.custom_models.test_access_code_model import TestAccessCode
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.user_management_models.custom_user_permissions_model import (
    CustomUserPermissions,
)
from backend.celery.task_definition.user_permissions import (
    deprovision_admin_after_inactivity_thirty_days,
    deprovision_after_inactivity,
    remove_expired_test_scorer_permissions,
)
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import pytz
from backend.celery.task_definition.utils import (
    ConsoleMessageColor,
)
from tests.Utils.util_users import (
    create_user,
)
from backend.views.utils import get_current_utc_offset_in_seconds

from tests.Utils.util_print import (
    print_task_start,
    print_task_end,
    print_sub_task_start,
    print_sub_task_end,
    print_level_1,
    print_level_2,
    print_level_3,
    print_ok_level_2,
)

FOLDER_NAME = "Celery"
FILE_NAME = "User Permissions"


def run_deprovision_admin_after_inactivity_thirty_days(self):
    print_task_start(
        FOLDER_NAME,
        FILE_NAME,
        "deprovision_admin_after_inactivity_thirty_days",
    )

    print_sub_task_start(
        "SETTING UP DATA FOR deprovision_admin_after_inactivity_thirty_days"
    )

    # Creating Users
    (
        inactive_user,
        active_user,
    ) = create_users_for_deprovision_admin_after_inactivity_thirty_days()

    # Creating Custom User Permissions Data
    create_custom_user_permissions_for_deprovision_admin_after_inactivity_thirty_days(
        inactive_user, active_user
    )

    print_sub_task_end(
        "SETTING UP DATA FOR deprovision_admin_after_inactivity_thirty_days"
    )

    print_sub_task_start(
        "EXECUTING CELERY TASK - deprovision_admin_after_inactivity_thirty_days"
    )

    # execute celery task
    deprovision_admin_after_inactivity_thirty_days()

    print_sub_task_end(
        "EXECUTING CELERY TASK - deprovision_admin_after_inactivity_thirty_days"
    )

    print_sub_task_start(
        "TESTING OF RESULTS - CELERY TASK - deprovision_admin_after_inactivity_thirty_days"
    )

    # Run Tests for deprovision_admin_after_inactivity_thirty_days
    testing_of_deprovision_admin_after_inactivity_thirty_days(self)

    print_sub_task_end(
        "TESTING OF RESULTS - CELERY TASK - deprovision_admin_after_inactivity_thirty_days"
    )

    print_task_end()


def run_remove_expired_test_scorer_permissions(self):
    print_task_start(
        FOLDER_NAME,
        FILE_NAME,
        "remove_expired_test_scorer_permissions",
    )

    print_sub_task_start("SETTING UP DATA FOR remove_expired_test_scorer_permissions")

    (inactive_test_scorer, active_test_scorer) = (
        create_users_for_remove_inactive_test_scorer()
    )

    # Creating Custom User Permissions Data
    create_custom_user_permissions_for_remove_inactive_test_scorer(
        inactive_test_scorer, active_test_scorer
    )

    create_test_center_associations_for_test_scorers(
        inactive_test_scorer, active_test_scorer
    )

    # execute celery task
    remove_expired_test_scorer_permissions()

    print_sub_task_end("EXECUTING CELERY TASK - remove_expired_test_scorer_permissions")

    print_sub_task_start(
        "TESTING OF RESULTS - CELERY TASK - remove_expired_test_scorer_permissions"
    )

    # Run Tests for deprovision_admin_after_inactivity_thirty_days
    testing_of_remove_expired_test_scorer_permissions(self)

    print_sub_task_end(
        "TESTING OF RESULTS - CELERY TASK - remove_expired_test_scorer_permissions"
    )

    print_task_end()


def run_deprovision_after_inactivity(self):
    print_task_start(FOLDER_NAME, FILE_NAME, "deprovision_after_inactivity")

    print_sub_task_start("SETTING UP DATA FOR deprovision_after_inactivity")

    # Creating Users
    (
        inactive_user,
        active_user,
    ) = create_users_for_deprovision_after_inactivity()

    # Creating Custom User Permissions Data
    create_custom_user_permissions_for_deprovision_after_inactivity(
        inactive_user, active_user
    )

    # Creating Test Definition Data
    (
        test_definition_1,
        test_definition_2,
    ) = create_test_definition_data_for_deprovision_after_inactivity()

    # Creating Test Access Code Data
    create_test_access_code_data_for_deprovision_after_inactivity(
        inactive_user,
        active_user,
        test_definition_1,
    )

    # Creating Test Permission Data
    (
        inactive_ta_extended_profile_1,
        inactive_ta_extended_profile_2,
        active_ta_extended_profile_1,
        active_ta_extended_profile_2,
    ) = create_test_permission_data_for_deprovision_after_inactivity(
        inactive_user,
        active_user,
        test_definition_1,
        test_definition_2,
    )

    print_sub_task_end("SETTING UP DATA FOR deprovision_after_inactivity")

    print_sub_task_start("EXECUTING CELERY TASK - deprovision_after_inactivity")

    # execute celery task
    deprovision_after_inactivity()

    print_sub_task_end("EXECUTING CELERY TASK - deprovision_after_inactivity")

    print_sub_task_start(
        "TESTING OF RESULTS - CELERY TASK - deprovision_after_inactivity"
    )

    testing_of_run_deprovision_after_inactivity(
        self,
        inactive_ta_extended_profile_1,
        inactive_ta_extended_profile_2,
        active_ta_extended_profile_1,
        active_ta_extended_profile_2,
    )

    print_sub_task_end(
        "TESTING OF RESULTS - CELERY TASK - deprovision_after_inactivity"
    )

    print_task_end()


def create_users_for_deprovision_admin_after_inactivity_thirty_days():
    # required user variables
    # inactive user
    inactive_username = "inactive_celery_user@email.ca"
    inactive_last_login = datetime.now(tz=pytz.UTC) - timedelta(days=31)

    # active user
    active_username = "active_celery_user@email.ca"
    active_last_login = datetime.now(tz=pytz.UTC)

    # inactive user
    print_level_1("Creating Inactive User with 31 days last_login")
    inactive_user = create_user(
        username=inactive_username, last_login=inactive_last_login
    )

    # inactive user
    print_level_1("Creating Active User with today last_login")
    active_user = create_user(username=active_username, last_login=active_last_login)

    return inactive_user, active_user


def create_users_for_remove_inactive_test_scorer():
    # required user variables
    # inactive user
    inactive_username = "inactive_celery_test_scorer@email.ca"
    inactive_last_login = datetime.now(tz=pytz.UTC) - timedelta(days=600)

    # active user
    active_username = "active_celery_test_scorer@email.ca"
    active_last_login = datetime.now(tz=pytz.UTC)

    # inactive user
    print_level_1("Creating Inactive User with 600 days last_login")
    inactive_test_scorer = create_user(
        username=inactive_username, last_login=inactive_last_login
    )

    # inactive user
    print_level_1("Creating Active User with today last_login")
    active_test_scorer = create_user(
        username=active_username, last_login=active_last_login
    )

    return inactive_test_scorer, active_test_scorer


def create_custom_user_permissions_for_deprovision_admin_after_inactivity_thirty_days(
    inactive_user, active_user
):
    # common variables
    email = "email@email.ca"
    pri = 123456789
    name = "name"

    # create custom user permissions data
    print_level_1("Creating Custom User Permissions' Data")

    print_level_2("Assigning to inactive user:")
    for permission in CustomPermissions.objects.all():
        print_level_3(permission.en_name)
        CustomUserPermissions.objects.create(
            user_id=User.objects.get(username=inactive_user.username).id,
            permission=permission,
            goc_email=email,
            pri=pri,
            rationale=name,
            supervisor=name,
            supervisor_email=email,
        )

    print_level_2("Assigning to active user:")

    for permission in CustomPermissions.objects.all():
        print_level_3(permission.en_name)
        CustomUserPermissions.objects.create(
            user_id=User.objects.get(username=active_user.username).id,
            permission=permission,
            goc_email=email,
            pri=pri,
            rationale=name,
            supervisor=name,
            supervisor_email=email,
        )


def create_custom_user_permissions_for_remove_inactive_test_scorer(
    inactive_test_scorer, active_test_scorer
):
    # common variables
    email = "email@email.ca"
    pri = 123456789
    name = "name"

    # create custom user permissions data
    print_level_1("Creating Custom User Permissions' Data")

    print_level_2("Assigning to inactive test scorer:")
    for permission in CustomPermissions.objects.all():

        print_level_3(permission.en_name)
        CustomUserPermissions.objects.create(
            user_id=User.objects.get(username=inactive_test_scorer.username).id,
            permission=permission,
            goc_email=email,
            pri=pri,
            rationale=name,
            supervisor=name,
            supervisor_email=email,
        )

    print_level_2("Assigning to active test scorer:")
    for permission in CustomPermissions.objects.all():

        print_level_3(permission.en_name)
        CustomUserPermissions.objects.create(
            user_id=User.objects.get(username=active_test_scorer.username).id,
            permission=permission,
            goc_email=email,
            pri=pri,
            rationale=name,
            supervisor=name,
            supervisor_email=email,
        )


def create_test_center_associations_for_test_scorers(
    inactive_test_scorer, active_test_scorer
):

    test_center = TestCenter.objects.create(
        name="Test Center 000",
        postal_code="A1A 1A1",
        country_id=1,
        security_email="Secondary Email",
        department_id=1,
        booking_delay=0,
        accommodations_friendly=True,
        modify_date=datetime.now(tz=pytz.UTC),
        ola_authorized=True,
    )

    # create custom user permissions data
    print_level_1("Creating Test Center association")

    print_level_2("Assigning to inactive test scorer:")

    inactive_test_scorer_association = TestCenterOlaTestAssessor.objects.create(
        supervisor=0,
        test_center_id=test_center.id,
        user_id=User.objects.get(username=inactive_test_scorer.username).id,
    )

    print_level_2("Assigning to active test scorer: ")

    active_test_scorer_association = TestCenterOlaTestAssessor.objects.create(
        supervisor=0,
        test_center_id=test_center.id,
        user_id=User.objects.get(username=active_test_scorer.username).id,
    )

    print_level_1("Assigning Approved Langauges to both users")

    languages = Language.objects.all()

    for language in languages:

        TestCenterOlaTestAssessorApprovedLanguage.objects.create(
            test_center_ola_test_assessor=inactive_test_scorer_association,
            language=language,
        )

        TestCenterOlaTestAssessorApprovedLanguage.objects.create(
            test_center_ola_test_assessor=active_test_scorer_association,
            language=language,
        )

    print_level_1("Assigning future test session to both users")

    date = "2026-01-28"
    start_time = "2026-01-28 14:00:00.0000000 +00:00"
    end_time = "2026-01-28 15:00:00.0000000 +00:00"
    modify_date = datetime.now(tz=pytz.UTC)

    test_session_data_inactive = TestCenterTestSessionData.objects.create(
        open_to_ogd=True,
        date=date,
        start_time=start_time,
        end_time=end_time,
        spaces_available=10,
        test_skill_sub_type_id=None,
        modify_date=modify_date,
        test_center_room_id=None,
        test_skill_type_id=None,
        test_assessor_user_id=User.objects.get(
            username=inactive_test_scorer.username
        ).id,
    )

    test_session_data_active = TestCenterTestSessionData.objects.create(
        open_to_ogd=True,
        date=date,
        start_time=start_time,
        end_time=end_time,
        spaces_available=10,
        test_skill_sub_type_id=None,
        modify_date=modify_date,
        test_center_room_id=None,
        test_skill_type_id=None,
        test_assessor_user_id=User.objects.get(username=active_test_scorer.username).id,
    )

    print_level_1("Assigning availabilities to both users")

    langs = Language.objects.all()
    lang = langs[0]

    days = CatRefDayOfWeekVW.objects.all()
    day = days[0]

    time_slot = TestCenterOlaTimeSlot.objects.create(
        test_center_id=test_center.id,
        day_of_week_id=day.id,
        start_time="10:00",
        end_time="12:00",
        assessed_language_id=lang.language_id,
        availability=0,
        slots_to_prioritize=0,
        utc_offset=get_current_utc_offset_in_seconds(),
    )

    TestCenterOlaTimeSlotAssessorAvailability.objects.create(
        test_center_ola_test_assessor=inactive_test_scorer_association,
        test_center_ola_time_slot=time_slot,
    )

    TestCenterOlaTimeSlotAssessorAvailability.objects.create(
        test_center_ola_test_assessor=active_test_scorer_association,
        test_center_ola_time_slot=time_slot,
    )

    print_level_1("Assigning unavailabilities to both users")

    TestCenterOlaAssessorUnavailability.objects.create(
        test_center_ola_test_assessor=inactive_test_scorer_association,
        start_date="2026-02-18",
        end_date="2026-02-20",
    )

    TestCenterOlaAssessorUnavailability.objects.create(
        test_center_ola_test_assessor=active_test_scorer_association,
        start_date="2026-02-20",
        end_date="2026-02-22",
    )


def testing_of_deprovision_admin_after_inactivity_thirty_days(self):
    # INACTIVE USER
    print_level_1("Inactive User - Verify that only 2 Custom User Permissions remain")
    # verify that all permissions but TA, Test Adaptations & Test Scorer have been deleted
    self.assertEqual(
        CustomUserPermissions.objects.filter(
            user_id=User.objects.get(username="inactive_celery_user@email.ca").id
        ).count(),
        3,
    )
    print_ok_level_2()

    print_level_1(
        "Inactive User - Verify that the Custom User Permission remaining is Test Administrator"
    )

    # verify that all permissions but TA have been deleted
    self.assertEqual(
        CustomUserPermissions.objects.filter(
            user_id=User.objects.get(username="inactive_celery_user@email.ca").id
        )
        .first()
        .permission.en_name,
        "Test Administrator",
    )
    print_ok_level_2()

    # ACTIVE USER
    total_count_user_permissions = CustomPermissions.objects.all().count()

    print_level_1(
        "Active User - Verify that {0} Custom User Permissions remain".format(
            total_count_user_permissions
        )
    )

    # verify that all permissions but TA have been deleted
    self.assertEqual(
        CustomUserPermissions.objects.filter(
            user_id=User.objects.get(username="active_celery_user@email.ca").id
        ).count(),
        total_count_user_permissions,
    )
    print_ok_level_2()


def testing_of_remove_expired_test_scorer_permissions(self):
    # TODO - add checks for:
    # Remove ScorerOlaAssignedTestSession
    # Remove TestScorerAssignment

    # Get all custom permissions types
    total_count_user_permissions = CustomPermissions.objects.all().count()

    # INACTIVE - verify that all permissions but Test Scorer have been deleted
    print_level_1(
        "Inactive User - Verify that the Custom User Test Scorer Permission has been removed"
    )
    self.assertEqual(
        CustomUserPermissions.objects.filter(
            user_id=User.objects.get(username="inactive_celery_test_scorer@email.ca").id
        ).count(),
        total_count_user_permissions - 1,
    )
    print_ok_level_2()

    # INACTIVE - verify test center associations have been removed
    print_level_1(
        "Inactive User - Verify that test center test associations have been removed"
    )
    test_center_ola_test_assessors = TestCenterOlaTestAssessor.objects.filter(
        user_id=User.objects.get(username="inactive_celery_test_scorer@email.ca").id
    )
    self.assertEqual(test_center_ola_test_assessors.count(), 0)
    print_ok_level_2()

    # INACTIVE - verify the associated langs have been removed
    print_level_1("Inactive User - Verify that associated languages have been removed")
    for test_center_ola_test_assessor in test_center_ola_test_assessors:
        self.assertEqual(
            TestCenterOlaTestAssessorApprovedLanguage.objects.filter(
                test_center_ola_test_assessor=test_center_ola_test_assessor.id
            ).count(),
            0,
        )
    print_ok_level_2()

    # INACTIVE - verify they have been removed from future sessions (set back to null)
    print_level_1(
        "Inactive User - verify they have been removed from future sessions (set back to null)"
    )
    for test_center_ola_test_assessor in test_center_ola_test_assessors:
        # Verify test scorer is not in future test sessions
        self.assertEquals(
            # Verify the test session - test scorer id is now null
            TestCenterTestSessionData.objects.filter(
                test_assessor_user_id=test_center_ola_test_assessor.user_id
            ).count(),
            0,
        )
    print_ok_level_2()

    # INACTIVE - verify the unavailabilities have been removed
    print_level_1("Inactive User - verify the unavailabilities have been removed")
    for test_center_ola_test_assessor in test_center_ola_test_assessors:
        self.assertEquals(
            # Verify the test session - test scorer id is now null
            TestCenterOlaAssessorUnavailability.objects.filter(
                test_center_ola_test_assessor=test_center_ola_test_assessor.id
            ).count(),
            0,
        )
    print_ok_level_2()

    # INACTIVE - verify the availabilities have been removed
    print_level_1("Inactive User - verify the availabilities have been removed")
    for test_center_ola_test_assessor in test_center_ola_test_assessors:
        self.assertEquals(
            # Verify the test session - test scorer id is now null
            TestCenterOlaTimeSlotAssessorAvailability.objects.filter(
                test_center_ola_test_assessor=test_center_ola_test_assessor.id
            ).count(),
            0,
        )
    print_ok_level_2()

    # ACTIVE USER

    # INACTIVE - verify that no permissions have been deleted
    print_level_1(
        "active User - Verify that the Custom User Test Scorer Permission has not been removed"
    )
    self.assertEqual(
        CustomUserPermissions.objects.filter(
            user_id=User.objects.get(username="active_celery_test_scorer@email.ca").id
        ).count(),
        total_count_user_permissions,
    )
    print_ok_level_2()

    # ACTIVE - Verify that the test center association has not been removed
    print_level_1(
        "active User - Verify that the test center association has not been removed"
    )
    test_center_ola_test_assessors = TestCenterOlaTestAssessor.objects.filter(
        user_id=User.objects.get(username="active_celery_test_scorer@email.ca").id
    )
    self.assertTrue(test_center_ola_test_assessors.count() > 0)
    print_ok_level_2()

    # ACTIVE - Verify that the approved languages still exist
    print_level_1("active User - Verify that the approved languages still exist")
    count = 0
    for test_center_ola_test_assessor in test_center_ola_test_assessors:
        count += TestCenterOlaTestAssessorApprovedLanguage.objects.filter(
            test_center_ola_test_assessor=test_center_ola_test_assessor.id
        ).count()
    self.assertTrue(count > 0)
    print_ok_level_2()

    # ACTIVE - verify they have not been removed from future sessions
    print_level_1(
        "Active User - verify they have not been removed from future sessions"
    )
    for test_center_ola_test_assessor in test_center_ola_test_assessors:
        # Verify test scorer is not in future test sessions
        self.assertTrue(
            # Verify the test session - test scorer id is now null
            TestCenterTestSessionData.objects.filter(
                test_assessor_user_id=test_center_ola_test_assessor.user_id
            ).count()
            > 0
        )
    print_ok_level_2()

    # ACTIVE - verify the unavailabilities have not been removed
    print_level_1("Active User - verify the unavailabilities have not been removed")
    for test_center_ola_test_assessor in test_center_ola_test_assessors:
        self.assertTrue(
            # Verify the test session - test scorer id is now null
            TestCenterOlaAssessorUnavailability.objects.filter(
                test_center_ola_test_assessor=test_center_ola_test_assessor.id
            ).count()
            > 0
        )
    print_ok_level_2()

    # ACTIVE - verify the availabilities have not been removed
    print_level_1("Active User - verify the availabilities have not been removed")
    for test_center_ola_test_assessor in test_center_ola_test_assessors:
        self.assertTrue(
            # Verify the test session - test scorer id is now null
            TestCenterOlaTimeSlotAssessorAvailability.objects.filter(
                test_center_ola_test_assessor=test_center_ola_test_assessor.id
            ).count()
            > 0
        )
    print_ok_level_2()


def create_users_for_deprovision_after_inactivity():
    # Required user variables
    # Variables - inactive user
    inactive_username = "inactive_celery_user@email.ca"
    inactive_last_login = datetime.now(tz=pytz.UTC) - (
        relativedelta(years=1) + timedelta(days=1)
    )

    # Variables - active user
    active_username = "active_celery_user@email.ca"
    active_last_login = datetime.now(tz=pytz.UTC)

    # Create - inactive user
    print(
        "{0} \t--> Creating Inactive User with 1 year last_login".format(
            ConsoleMessageColor.WHITE
        )
    )
    inactive_user = create_user(
        username=inactive_username, last_login=inactive_last_login
    )

    # Create - inactive user
    print(
        "{0} \t--> Creating Active User with today last_login".format(
            ConsoleMessageColor.WHITE
        )
    )
    active_user = create_user(username=active_username, last_login=active_last_login)

    return inactive_user, active_user


def create_custom_user_permissions_for_deprovision_after_inactivity(
    inactive_user, active_user
):
    # Common variables
    email = "email@email.ca"
    pri = 123456789
    name = "name"

    # Create custom user permissions data
    print_level_1("Creating Custom User Permissions' Data")

    # Create custom user permissions - inactive user
    print_level_2("Assigning Custom User Permission to Inactive User")
    for permission in CustomPermissions.objects.all():
        print_level_3(permission.en_name)
        CustomUserPermissions.objects.create(
            user_id=User.objects.get(username=inactive_user.username).id,
            permission=permission,
            goc_email=email,
            pri=pri,
            rationale=name,
            supervisor=name,
            supervisor_email=email,
        )

    # Create custom user permissions - active user
    print_level_2("Assigning Custom User Permission to Active User")
    for permission in CustomPermissions.objects.all():
        print_level_3(permission.en_name)
        CustomUserPermissions.objects.create(
            user_id=User.objects.get(username=active_user.username).id,
            permission=permission,
            goc_email=email,
            pri=pri,
            rationale=name,
            supervisor=name,
            supervisor_email=email,
        )


def create_test_definition_data_for_deprovision_after_inactivity():
    # Test Definition Data
    print_level_1("Creating Test Definition Data")

    # Create - Fake Test Definition Data
    test_definition_1 = TestDefinition.objects.create(
        parent_code="UNIT_TEST_1",
        test_code="UNIT_TEST_1",
        version=1,
        en_name="Unit Test - Test Definition",
        fr_name="FR Unit Test - Test Definition",
    )

    print_level_2(
        "New Test Definition Created: ID={0} TEST_CODE={1} NAME={2}".format(
            test_definition_1.id,
            test_definition_1.test_code,
            test_definition_1.en_name,
        )
    )

    test_definition_2 = TestDefinition.objects.create(
        parent_code="UNIT_TEST_2",
        test_code="UNIT_TEST_2",
        version=1,
        en_name="Unit Test - Test Definition",
        fr_name="FR Unit Test - Test Definition",
    )

    print_level_2(
        "New Test Definition Created: ID={0} TEST_CODE={1} NAME={2}".format(
            test_definition_2.id,
            test_definition_2.test_code,
            test_definition_2.en_name,
        )
    )

    return test_definition_1, test_definition_2


def create_test_access_code_data_for_deprovision_after_inactivity(
    inactive_user,
    active_user,
    test_definition_1,
):
    # Test Center Required Data
    print_level_1("Creating Test Center Required Data")

    # Test Center
    print_level_2("Creating Test Center Data")
    new_test_center = TestCenter.objects.create(
        name="Unit Test Test Center",
        department_id=4,
        # default booking delay of 48 hours
        booking_delay=48,
        # all other field must remain empty (for now)
    )

    # Test Center Associated TAs
    print_level_2("Associating TAs to Test Center")
    ta_user_id = User.objects.get(username=inactive_user).id
    TestCenterTestAdministrators.objects.create(
        user_id=ta_user_id, test_center_id=new_test_center.id
    )

    # Test Center Room Data
    print_level_2("Creating Test Center Room Data")
    new_test_center_room_data = TestCenterRoomData.objects.create(
        name="Room #1",
        email="unit.test@email.ca",
        max_occupancy=5,
        active=True,
    )
    languages = Language.objects.all()

    # Create all translated test center's room's other details
    for language in languages:
        TestCenterRoomDataOtherDetailsText.objects.create(
            language=language, test_center_room_data=new_test_center_room_data
        )

    # Test Center Room
    print_level_2("Creating Test Center Room")
    new_test_center_room = TestCenterRooms.objects.create(
        room_data_id=new_test_center_room_data.id, test_center_id=new_test_center.id
    )

    # Test Center Test Session Data
    print_level_2("Creating Test Center Test Session Data")
    new_test_center_test_session_data = TestCenterTestSessionData.objects.create(
        test_center_room_id=new_test_center_room.id,
        open_to_ogd=False,
        date=datetime.today() + timedelta(days=3),
        start_time=datetime.now(tz=pytz.UTC) + timedelta(hours=1),
        end_time=datetime.now(tz=pytz.UTC) + timedelta(hours=3),
        spaces_available=5,
        test_skill_type_id=1,
        test_skill_sub_type_id=None,
    )

    # Test Center Test Session
    print_level_2("Creating Test Center Test Session")
    new_test_center_test_session = TestCenterTestSessions.objects.create(
        test_session_data_id=new_test_center_test_session_data.id,
        test_center_id=new_test_center.id,
        is_standard=1,
        user_accommodation_file_id=None,
    )

    # Test Access Code Data
    print_level_1("Creating Test Access Code Data")
    # Test Access Code Data - Inactive User
    print_level_2("Assigning Test Access Code to inactive user:")
    for i in range(0, 5):
        test_access_code_str = "{0}{1}".format("UNITTEST_", i)

        print_level_3("TEST_ACCESS_CODE: {0}".format(test_access_code_str))
        TestAccessCode.objects.create(
            test_access_code=test_access_code_str,
            test_session_id=new_test_center_test_session.id,
            test=test_definition_1,
            ta_user_id=User.objects.get(username=inactive_user).id,
            created_date=datetime.now(pytz.utc),
        )

    # Test Access Code Data - Active User
    print_level_2("Assigning Test Access Code to active user:")
    for i in range(5, 10):
        test_access_code_str = "{0}{1}".format("UNITTEST_", i)

        print_level_3("TEST_ACCESS_CODE: {0}".format(test_access_code_str))
        TestAccessCode.objects.create(
            test_access_code=test_access_code_str,
            test_session_id=new_test_center_test_session.id,
            test=test_definition_1,
            ta_user_id=User.objects.get(username=active_user).id,
            created_date=datetime.now(pytz.utc),
        )


def create_test_permission_data_for_deprovision_after_inactivity(
    inactive_user,
    active_user,
    test_definition_1,
    test_definition_2,
):
    # Test Permission Data
    print_level_1("Creating Test Permission Data")

    # Test Permission Data - Inactive User
    print_level_2("Assigning Test Permissions to inactive user:")
    # Test Permission that would expire later
    test_permission_temp = TestPermissions.objects.create(
        user_id=User.objects.get(username=inactive_user).id,
        test=test_definition_1,
        expiry_date="2024-01-01",
        test_order_number="UNIT_TEST",
        staffing_process_number="UNIT_TEST",
        department_ministry_code="UNIT_TEST",
        is_org="UNIT_TEST",
        is_ref="UNIT_TEST",
        billing_contact="UNIT_TEST",
        billing_contact_info="UNIT_TEST",
        reason_for_modif_or_del=None,
    )
    print_level_3(
        "TEST: {0} Expiry Date: {1}".format(
            test_permission_temp.test.test_code, test_permission_temp.expiry_date
        )
    )
    # Test Permission that is expired
    test_permission_temp = TestPermissions.objects.create(
        user_id=User.objects.get(username=inactive_user).id,
        test=test_definition_2,
        expiry_date="2020-01-01",
        test_order_number="UNIT_TEST",
        staffing_process_number="UNIT_TEST",
        department_ministry_code="UNIT_TEST",
        is_org="UNIT_TEST",
        is_ref="UNIT_TEST",
        billing_contact="UNIT_TEST",
        billing_contact_info="UNIT_TEST",
        reason_for_modif_or_del=None,
    )
    print_level_3(
        "TEST: {0} Expiry Date: {1}".format(
            test_permission_temp.test.test_code, test_permission_temp.expiry_date
        )
    )

    # Test Permission Data - Active User
    print_level_2("Assigning Test Permissions to active user:")
    # Test Permission that would expire later
    test_permission_temp = TestPermissions.objects.create(
        user_id=User.objects.get(username=active_user).id,
        test=test_definition_1,
        expiry_date="2024-01-01",
        test_order_number="UNIT_TEST",
        staffing_process_number="UNIT_TEST",
        department_ministry_code="UNIT_TEST",
        is_org="UNIT_TEST",
        is_ref="UNIT_TEST",
        billing_contact="UNIT_TEST",
        billing_contact_info="UNIT_TEST",
        reason_for_modif_or_del=None,
    )
    print_level_3(
        "TEST: {0} Expiry Date: {1}".format(
            test_permission_temp.test.test_code, test_permission_temp.expiry_date
        )
    )
    # Test Permission that is expired
    test_permission_temp = TestPermissions.objects.create(
        user_id=User.objects.get(username=active_user).id,
        test=test_definition_2,
        expiry_date="2020-01-01",
        test_order_number="UNIT_TEST",
        staffing_process_number="UNIT_TEST",
        department_ministry_code="UNIT_TEST",
        is_org="UNIT_TEST",
        is_ref="UNIT_TEST",
        billing_contact="UNIT_TEST",
        billing_contact_info="UNIT_TEST",
        reason_for_modif_or_del=None,
    )
    print_level_3(
        "TEST: {0} Expiry Date: {1}".format(
            test_permission_temp.test.test_code, test_permission_temp.expiry_date
        )
    )

    # ORDERLESS TEST PERMISSIONS
    print_level_1("Creating Orderless Test Permission Data")

    # Create - TaExtendedProfile - Inactive User
    print_level_2("Creating TaExtendedProfiles - Inactive User")

    # We want to make sure that the system doesn't break in a rare case where we would have 2 TaExtendedProfiles for our user
    inactive_ta_extended_profile_1 = TaExtendedProfile.objects.create(
        user_id=User.objects.get(username=inactive_user).id, department_id=1
    )
    print_level_3(
        "First TaExtendedProfile has a department_id={0}".format(
            inactive_ta_extended_profile_1.department_id
        )
    )
    inactive_ta_extended_profile_2 = TaExtendedProfile.objects.create(
        user_id=User.objects.get(username=inactive_user).id, department_id=2
    )
    print_level_3(
        "Second TaExtendedProfile has a department_id={0}".format(
            inactive_ta_extended_profile_2.department_id
        )
    )

    # Create - TaExtendedProfile - Active User
    print_level_2("Creating TaExtendedProfiles - Active User")

    # We want to make sure that the system doesn't break in a rare case where we would have 2 TaExtendedProfiles for our user
    active_ta_extended_profile_1 = TaExtendedProfile.objects.create(
        user_id=User.objects.get(username=active_user).id, department_id=1
    )
    print_level_3(
        "First TaExtendedProfile has a department_id={0}".format(
            active_ta_extended_profile_1.department_id
        )
    )
    active_ta_extended_profile_2 = TaExtendedProfile.objects.create(
        user_id=User.objects.get(username=active_user).id, department_id=2
    )
    print_level_3(
        "Second TaExtendedProfile has a department_id={0}".format(
            active_ta_extended_profile_2.department_id
        )
    )

    # Create - OrderlessTestPermissions - Inactive User
    print_level_2("Creating OrderlessTestPermissions - Inactive User")

    # First OrderlessTestPermissions for the inactive_ta_extended_profile_1
    inactive_orderless_test_permissions_1 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=inactive_ta_extended_profile_1,
        parent_code=test_definition_1.parent_code,
        test_code=test_definition_1.test_code,
    )
    print_level_3(
        "First OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            inactive_orderless_test_permissions_1.ta_extended_profile.pk,
        )
    )

    # Second OrderlessTestPermissions for the inactive_ta_extended_profile_1
    inactive_orderless_test_permissions_2 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=inactive_ta_extended_profile_1,
        parent_code=test_definition_2.parent_code,
        test_code=test_definition_2.test_code,
    )
    print_level_3(
        "Second OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            inactive_orderless_test_permissions_2.ta_extended_profile.pk,
        )
    )

    # First OrderlessTestPermissions for the inactive_ta_extended_profile_2
    inactive_orderless_test_permissions_3 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=inactive_ta_extended_profile_2,
        parent_code=test_definition_1.parent_code,
        test_code=test_definition_1.test_code,
    )
    print_level_3(
        "First OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            inactive_orderless_test_permissions_3.ta_extended_profile.pk,
        )
    )

    # Second OrderlessTestPermissions for the inactive_ta_extended_profile_2
    inactive_orderless_test_permissions_4 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=inactive_ta_extended_profile_2,
        parent_code=test_definition_2.parent_code,
        test_code=test_definition_2.test_code,
    )
    print_level_3(
        "Second OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            inactive_orderless_test_permissions_4.ta_extended_profile.pk,
        )
    )

    # Create - OrderlessTestPermissions - Active User
    print_level_2("Creating OrderlessTestPermissions - Active User")

    # First OrderlessTestPermissions for the active_ta_extended_profile_1
    active_orderless_test_permissions_1 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=active_ta_extended_profile_1,
        parent_code=test_definition_1.parent_code,
        test_code=test_definition_1.test_code,
    )
    print_level_3(
        "First OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            active_orderless_test_permissions_1.ta_extended_profile.pk,
        )
    )

    # Second OrderlessTestPermissions for the active_ta_extended_profile_1
    active_orderless_test_permissions_2 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=active_ta_extended_profile_1,
        parent_code=test_definition_2.parent_code,
        test_code=test_definition_2.test_code,
    )
    print_level_3(
        "Second OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            active_orderless_test_permissions_2.ta_extended_profile.pk,
        )
    )

    # First OrderlessTestPermissions for the active_ta_extended_profile_2
    active_orderless_test_permissions_3 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=active_ta_extended_profile_2,
        parent_code=test_definition_1.parent_code,
        test_code=test_definition_1.test_code,
    )
    print_level_3(
        "First OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            active_orderless_test_permissions_3.ta_extended_profile.pk,
        )
    )

    # Second OrderlessTestPermissions for the active_ta_extended_profile_2
    active_orderless_test_permissions_4 = OrderlessTestPermissions.objects.create(
        ta_extended_profile=active_ta_extended_profile_2,
        parent_code=test_definition_2.parent_code,
        test_code=test_definition_2.test_code,
    )
    print_level_3(
        "Second OrderlessTestPermissions - TaExtendedProfile_pk={0}".format(
            active_orderless_test_permissions_4.ta_extended_profile.pk,
        )
    )

    return (
        inactive_ta_extended_profile_1,
        inactive_ta_extended_profile_2,
        active_ta_extended_profile_1,
        active_ta_extended_profile_2,
    )


def testing_of_run_deprovision_after_inactivity(
    self,
    inactive_ta_extended_profile_1,
    inactive_ta_extended_profile_2,
    active_ta_extended_profile_1,
    active_ta_extended_profile_2,
):
    # INACTIVE USER
    # TEST ACCESS CODES
    print_level_1("Inactive User - Verify that all Test Access Codes have been deleted")

    # verify that all test access codes have been deleted
    self.assertEqual(
        TestAccessCode.objects.filter(
            ta_user_id=User.objects.get(username="inactive_celery_user@email.ca").id
        ).count(),
        0,
    )
    print_ok_level_2()

    # TEST PERMISSIONS
    print_level_1("Inactive User - Verify that all Test Permissions have been deleted")

    self.assertEqual(
        TestPermissions.objects.filter(
            user_id=User.objects.get(username="inactive_celery_user@email.ca").id
        ).count(),
        0,
    )
    print_ok_level_2()

    # CUSTOM USER PERMISSIONS
    print_level_1(
        "Inactive User - Verify that all Custom User Permission have been deleted"
    )

    # verify that all permissions but TA have been deleted
    self.assertEqual(
        CustomUserPermissions.objects.filter(
            user_id=User.objects.get(username="inactive_celery_user@email.ca").id
        ).count(),
        0,
    )
    print_ok_level_2()

    # ORDERLESS TEST PERMISSIONS
    print_level_1(
        "Inactive User - Verify that all Orderless Test Permissions have been deleted"
    )

    # verify that all permissions but TA have been deleted
    self.assertEqual(
        OrderlessTestPermissions.objects.filter(
            ta_extended_profile__in=[
                inactive_ta_extended_profile_1,
                inactive_ta_extended_profile_2,
            ]
        ).count(),
        0,
    )
    print_ok_level_2()

    # ACTIVE USER
    # TEST ACCESS CODES
    print_level_1("Active User - Verify that all Test Access Codes remain")

    total_count_test_access_code = TestAccessCode.objects.all().count()
    # verify that all test access codes remain
    self.assertEqual(
        TestAccessCode.objects.filter(
            ta_user_id=User.objects.get(username="active_celery_user@email.ca").id
        ).count(),
        total_count_test_access_code,
    )
    print_ok_level_2()

    # TEST PERMISSIONS
    print_level_1("Active User - Verify that all Test Permissions remain")

    # verify that all test permissions remain
    self.assertEqual(
        TestPermissions.objects.filter(
            user_id=User.objects.get(username="active_celery_user@email.ca").id
        ).count(),
        TestPermissions.objects.all().count(),
    )
    print_ok_level_2()

    # CUSTOM USER PERMISSIONS
    total_count_user_permissions = CustomPermissions.objects.all().count()

    print_level_1(
        "Active User - Verify that {0} Custom User Permissions remain".format(
            total_count_user_permissions
        )
    )

    # verify that all permissions but TA have been deleted
    self.assertEqual(
        CustomUserPermissions.objects.filter(
            user_id=User.objects.get(username="active_celery_user@email.ca").id
        ).count(),
        total_count_user_permissions,
    )
    print_ok_level_2()

    # ORDERLESS TEST PERMISSIONS
    print_level_1("Active User - Verify that all Orderless Test Permissions remain")

    # verify that all permissions but TA have been deleted
    self.assertEqual(
        OrderlessTestPermissions.objects.filter(
            ta_extended_profile__in=[
                active_ta_extended_profile_1,
                active_ta_extended_profile_2,
            ]
        ).count(),
        OrderlessTestPermissions.objects.all().count(),
    )
    print_ok_level_2()
