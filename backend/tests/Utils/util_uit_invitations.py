import json
from rest_framework import status
from tests.Utils.util_endpoint import get_header_for_endpoint_call
from tests.Utils.util_print import (
    print_level_1,
    print_level_2,
    print_ok_level_3,
)
from tests.Utils.util_users import create_access_token
from datetime import datetime
from dateutil.relativedelta import relativedelta

DEFAULT_VALIDITY_END_DATE = ((datetime.today()) + (relativedelta(years=1))).strftime(
    "%Y-%m-%d"
)
DEFAULT_ORDERLESS_REQUEST = False

# -------------------------------------------------------------------------------------------------#
# Send UIT Invitation
# -------------------------------------------------------------------------------------------------#
# Function Used to Send UIT invitations
# -------------------------------------------------------------------------------------------------#
def send_uit_invitation(
    self, ta_user, tester_users, new_test_definition, test_section, test_permission
):
    print_level_1("send_uit_invitation - Creating Headers:")

    # Creation of the headers
    headers = create_uit_invitation_headers(self, ta_user)

    # Creation of the URL
    print_level_1("send_uit_invitation - Creating Request")

    data = json.dumps(
        {
            "candidates_list": create_uit_candidates_list(test_section, tester_users),
            "validity_end_date": create_uit_validity_end_date(),
            "test_order_number": create_uit_test_order_number(test_permission),
            "tests": create_uit_tests(new_test_definition),
            "orderless_request": create_uit_oderless_request(),
        },
        separators=(",", ":"),
    )

    url = "/oec-cat/api/send-uit-invitations/"

    print_level_1("send_uit_invitation - Sending Request")

    response = self.client.post(
        url, data=data, content_type="application/json", HTTP_AUTHORIZATION=headers
    )

    # Verify that the UIT invitations have been sent successfully
    self.assertEqual(response.status_code, status.HTTP_200_OK)
    print_level_2("Response: {0}".format(response.status_code))
    print_ok_level_3()

    return response


# -------------------------------------------------------------------------------------------------#
# Create UIT Invitation Header
# -------------------------------------------------------------------------------------------------#
# Function Used to Create and Print the Header Used to Access UIT Invitation Post Enpoint
# -------------------------------------------------------------------------------------------------#
def create_uit_invitation_headers(self, ta_user):
    token = create_access_token(self, ta_user.username)
    headers = get_header_for_endpoint_call(token)

    print_level_2("{0}".format(headers))

    return "JWT {0}".format(token)


# -------------------------------------------------------------------------------------------------#
# Create UIT Candidate List
# -------------------------------------------------------------------------------------------------#
# Function used to create uit invitation - candidates_list
#   Arguments:
#       - test_section
#       - list of users
# -------------------------------------------------------------------------------------------------#
def create_uit_candidates_list(test_section, tester_users):
    candidate_list_json = []

    for tester_user in tester_users:
        candidate_list_json.append(
            {
                "Prenom_First_Name": tester_user["first_name"],
                "Nom_de_famille_Last_Name": tester_user["last_name"],
                "Courriel_Email": tester_user["email"],
                "Motif_pour_evaluation_Reason_for_Testing": "Formation linguistique / Language Training",
                "Niveau_requis_Level_Required": "A",
                "": "FR",
                "test_sections": [
                    {
                        "id": test_section.id,
                        "order": test_section.order,
                        "section_type": test_section.section_type,
                        "default_time": test_section.default_time,
                        "en_title": test_section.en_title,
                        "fr_title": test_section.fr_title,
                        "next_section_button_type": test_section.next_section_button_type,
                        "scoring_type": test_section.scoring_type,
                        "minimum_score": test_section.minimum_score,
                        "uses_notepad": test_section.uses_notepad,
                        "uses_calculator": test_section.uses_calculator,
                        "block_cheating": test_section.block_cheating,
                        "default_tab": test_section.default_tab,
                        "item_exposure": test_section.item_exposure,
                        "test_definition": test_section.test_definition.id,
                        "time": 1,
                    }
                ],
                "total_time": "00 : 01",
                "break_bank": 0,
            },
        )

    return candidate_list_json


# -------------------------------------------------------------------------------------------------#
# Create UIT Validity End Date
# -------------------------------------------------------------------------------------------------#
# Function used to create uit invitation - validity_end_date
# -------------------------------------------------------------------------------------------------#
def create_uit_validity_end_date(validity_end_date=DEFAULT_VALIDITY_END_DATE):
    return validity_end_date


# -------------------------------------------------------------------------------------------------#
# Create UIT Test Order Number
# -------------------------------------------------------------------------------------------------#
# Function used to create uit invitation - test_order_number
# -------------------------------------------------------------------------------------------------#
def create_uit_test_order_number(test_permission):
    return test_permission.test_order_number


# -------------------------------------------------------------------------------------------------#
# Create UIT Tests
# -------------------------------------------------------------------------------------------------#
# Function used to create uit invitation - tests
# -------------------------------------------------------------------------------------------------#
def create_uit_tests(new_test_definiton):
    return [new_test_definiton.id]


# -------------------------------------------------------------------------------------------------#
# Create UIT Orderless Request
# -------------------------------------------------------------------------------------------------#
# Function used to create uit invitation - oderless_request
# -------------------------------------------------------------------------------------------------#
def create_uit_oderless_request(oderless_request=DEFAULT_ORDERLESS_REQUEST):
    return oderless_request
