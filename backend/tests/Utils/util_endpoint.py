# -------------------------------------------------------------------------------------------------#
# Get Header for Get Endpoint
# -------------------------------------------------------------------------------------------------#
# Returns the header used for a get endpoint
# -------------------------------------------------------------------------------------------------#
def get_header_for_endpoint_call(access_token):
    return {
        "HTTP_AUTHORIZATION": "JWT {0}".format(access_token),
        "Accept": "application/json",
        "Content-Type": "application/json",
        "cache": "default",
    }
