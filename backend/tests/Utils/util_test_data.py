import json
from tests.Utils.util_print import print_level_1
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_section import TestSection
from cms.cms_models.test_section_component import TestSectionComponent
from cms.views.try_test_section import try_test_section, Action

# -------------------------------------------------------------------------------------------------#
# Upload Test - JSON
# -------------------------------------------------------------------------------------------------#
# Uploads a JSON Test File to Create Test Data
# -------------------------------------------------------------------------------------------------#
def upload_test(json_file_name):
    print_level_1("Uploading Json File - {0}".format(json_file_name))
    json_data = None
    with open(
        "/backend/tests/Utils/json_test_files/{0}".format(json_file_name), "r"
    ) as f:
        data = f.read()
        json_data = json.loads(data)

    # Returns the HTTP Response
    return try_test_section(0, json_data["new_test"], "en", Action.UPLOAD)


# TEST DEFINITION
DEFAULT_PARENT_CODE = "PARENT_CODE_UNIT_TEST_"
DEFAULT_TEST_CODE = "TEST_CODE_UNIT_TEST_"
DEFAULT_VERSION = 1
DEFAULT_VERSION_NOTES = ""
DEFAULT_EN_NAME = "Unit Test - Test Definition"
DEFAULT_FR_NAME = "FR Unit Test - Test Definition"
DEFAULT_IS_UIT = False
DEFAULT_IS_PUBLIC = False
DEFAULT_COUNT_UP = False
DEFAULT_ACTIVE = False
DEFAULT_RETEST_PERIOD = 0
DEFAULT_ARCHIVED = False

# -------------------------------------------------------------------------------------------------#
# Create Test Definition
# -------------------------------------------------------------------------------------------------#
# Function used to create a new TestDefinition row
# -------------------------------------------------------------------------------------------------#
def create_test_definition(
    parent_code_nbr=None,
    parent_code=DEFAULT_PARENT_CODE,
    test_code_nbr=None,
    test_code=DEFAULT_TEST_CODE,
    version=DEFAULT_VERSION,
    version_notes=DEFAULT_VERSION_NOTES,
    en_name=DEFAULT_EN_NAME,
    fr_name=DEFAULT_FR_NAME,
    is_uit=DEFAULT_IS_UIT,
    is_public=DEFAULT_IS_PUBLIC,
    count_up=DEFAULT_COUNT_UP,
    active=DEFAULT_ACTIVE,
    retest_period=DEFAULT_RETEST_PERIOD,
    archived=DEFAULT_ARCHIVED,
):

    # Get last TestDefinition pk so we can simply name the code nbr that value + 1
    last_test_definition_pk = TestDefinition.objects.last().pk

    if parent_code_nbr is None:
        parent_code_nbr = last_test_definition_pk + 1
    if test_code_nbr is None:
        test_code_nbr = last_test_definition_pk + 1

    new_test_definition = TestDefinition.objects.create(
        parent_code="{0}{1}".format(parent_code, parent_code_nbr),
        test_code="{0}{1}".format(test_code, test_code_nbr),
        version=version,
        version_notes=version_notes,
        en_name=en_name,
        fr_name=fr_name,
        is_uit=is_uit,
        is_public=is_public,
        count_up=count_up,
        active=active,
        retest_period=retest_period,
        archived=archived,
    )

    return new_test_definition


# TEST SECTION
DEFAULT_ORDER = 1
DEFAULT_SECTION_TYPE = 1
DEFAULT_TIME = None
DEFAULT_EN_TITLE = "Unit Test - Test Section - Title"
DEFAULT_FR_TITLE = "FR Unit Test - Test Section - Title"
DEFAULT_NEXT_SECTION_BTN_TYPE = 1
DEFAULT_SCORING_TYPE = 1
DEFAULT_MINIMUM_SCORE = 0
DEFAULT_USES_NOTEPAD = False
DEFAULT_USES_CALCULATOR = False
DEFAULT_BLOCK_CHEATING = True
DEFAULT_TAB = True
DEFAULT_ITEM_EXPOSURE = False

# -------------------------------------------------------------------------------------------------#
# Create Test Section
# -------------------------------------------------------------------------------------------------#
# Function used to create a new TestSection row
# -------------------------------------------------------------------------------------------------#
def create_test_section(
    test_definition,
    order=DEFAULT_ORDER,
    section_type=DEFAULT_SECTION_TYPE,
    default_time=DEFAULT_TIME,
    en_title=DEFAULT_EN_TITLE,
    fr_title=DEFAULT_FR_TITLE,
    next_section_button_type=DEFAULT_NEXT_SECTION_BTN_TYPE,
    scoring_type=DEFAULT_SCORING_TYPE,
    minimum_score=DEFAULT_MINIMUM_SCORE,
    uses_notepad=DEFAULT_USES_NOTEPAD,
    uses_calculator=DEFAULT_USES_CALCULATOR,
    block_cheating=DEFAULT_BLOCK_CHEATING,
    default_tab=DEFAULT_TAB,
    item_exposure=DEFAULT_ITEM_EXPOSURE,
):

    new_test_section = TestSection.objects.create(
        test_definition=test_definition,
        order=order,
        section_type=section_type,
        default_time=default_time,
        en_title=en_title,
        fr_title=fr_title,
        next_section_button_type=next_section_button_type,
        scoring_type=scoring_type,
        minimum_score=minimum_score,
        uses_notepad=uses_notepad,
        uses_calculator=uses_calculator,
        block_cheating=block_cheating,
        default_tab=default_tab,
        item_exposure=item_exposure,
    )

    return new_test_section


# TEST SECTION COMPONENT
DEFAULT_SCTN_CMPNNT_ORDER = 1
DEFAULT_COMPONENT_TYPE = 1
DEFAULT_SCTN_CMPNNT_EN_TITLE = "Unit Test - Test Section Component - Title"
DEFAULT_SCTN_CMPNNT_FR_TITLE = "FR Unit Test - Test Section Component - Title"
DEFAULT_LANGUAGE_ID = 1
DEFAULT_SHUFFLE_ALL_QUESTIONS = False
DEFAULT_SHUFFLE_QUESTION_BLOCKS = False

# -------------------------------------------------------------------------------------------------#
# Create Test Section Component
# -------------------------------------------------------------------------------------------------#
# Function used to create a new TestSectionComponent row
# -------------------------------------------------------------------------------------------------#
def create_test_section_component(
    test_section,
    order=DEFAULT_SCTN_CMPNNT_ORDER,
    component_type=DEFAULT_COMPONENT_TYPE,
    en_title=DEFAULT_SCTN_CMPNNT_EN_TITLE,
    fr_title=DEFAULT_SCTN_CMPNNT_FR_TITLE,
    language=DEFAULT_LANGUAGE_ID,
    shuffle_all_questions=DEFAULT_SHUFFLE_ALL_QUESTIONS,
    shuffle_question_blocks=DEFAULT_SHUFFLE_QUESTION_BLOCKS,
):

    new_test_section_component = TestSectionComponent.objects.create(
        test_section=test_section,
        order=order,
        component_type=component_type,
        en_title=en_title,
        fr_title=fr_title,
        language=language,
        shuffle_all_questions=shuffle_all_questions,
        shuffle_question_blocks=shuffle_question_blocks,
    )

    return new_test_section_component
