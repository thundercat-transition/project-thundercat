from cms.cms_models.test_definition import TestDefinition
from tests.Utils.util_print import (
    print_task_start,
    print_sub_task_start,
    print_sub_task_end,
    print_task_end,
    print_ok_level_2,
    print_ok_level_3,
    print_ok_level_4,
    print_level_1,
    print_level_2,
    print_level_3,
)
from tests.Utils.util_test_data import upload_test
from rest_framework import status

FOLDER_NAME = "Backend"
FILE_NAME = "Upload_Test"

JSON_FILE_EMPTY = "empty.json"
JSON_FILE_NAME_1 = "test_1.json"


def run_upload_test(self):
    print_task_start(
        folder_name=FOLDER_NAME,
        file_name=FILE_NAME,
        function_name="run_upload_test",
    )

    print_sub_task_start("UPLOADING JSON DATA")

    response_test_empty = upload_test(JSON_FILE_EMPTY)
    response_test_1 = upload_test(JSON_FILE_NAME_1)

    print_sub_task_end("UPLOADING JSON DATA")

    print_sub_task_start("EXECUTING ENDPOINT TESTS - run_upload_test")

    execute_tests_for_upload_test_empty(self, response_test_empty)

    execute_tests_for_upload_test_1(self, response_test_1)

    print_sub_task_end("EXECUTING ENDPOINT TESTS - run_upload_test")

    print_task_end()


# Sub-Function - Execute test for upload_test - empty
def execute_tests_for_upload_test_empty(self, response):

    # Uploading first json file
    print_level_1(
        "Verify that the json file, {0}, is getting HTTP_400_BAD_REQUEST".format(
            JSON_FILE_EMPTY
        )
    )

    # Verify that the json file has been uploaded successfully
    self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    print_ok_level_2()


# Sub-Function - Execute test for upload_test - test 1
def execute_tests_for_upload_test_1(self, response):

    # Uploading first json file
    print_level_1(
        "Verify that the json file, {0}, has been uploaded successfully:".format(
            JSON_FILE_NAME_1
        )
    )

    print_level_2("Reponse is HTTP_200_OK")
    # Verify that we received a response HTTP_200_OK
    self.assertEqual(response.status_code, status.HTTP_200_OK)
    print_ok_level_3()

    print_level_2("New Test Definition has been created")

    # Verify that the new TestDefinition exists
    new_test_definiton = TestDefinition.objects.get(
        parent_code="8877", test_code="SQ F000 A1", version=1
    )
    print_level_3("Parent Code: 8877")
    self.assertEqual(
        new_test_definiton.parent_code,
        "8877",
    )
    print_ok_level_4()

    print_level_3("Test Code: SQ F000 A1")
    self.assertEqual(
        new_test_definiton.test_code,
        "SQ F000 A1",
    )
    print_ok_level_4()

    # Comment: even if we specify the version in the json file (in this case 6)
    # the system will put the next version instead of the specified verison (in this case 1)
    print_level_3("Test Version: 1")
    self.assertEqual(
        new_test_definiton.version,
        1,
    )
    print_ok_level_4()
