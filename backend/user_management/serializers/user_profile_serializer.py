from rest_framework import serializers
from user_management.user_management_models.user_profile_change_request import (
    UserProfileChangeRequest,
)
from user_management.user_management_models.user_models import User


class UserProfileSerializer(serializers.ModelSerializer):
    last_updated_date = serializers.SerializerMethodField()

    def get_last_updated_date(self, request):
        # initializing last_updated_date
        last_updated_date = None
        # getting historical user profile change request data where reason_for_deny is blank (meaning last approved change request)
        historical_approved_user_profile_change_request_data = (
            UserProfileChangeRequest.history.filter(
                user_id=request.id, history_type="-"
            )
            .exclude(reason_for_deny__gt="")
            .order_by("history_date")
        )
        # historical_approved_user_profile_change_request_data contains data
        if historical_approved_user_profile_change_request_data:
            # updating last_updated_date with latest modify_date
            last_updated_date = (
                historical_approved_user_profile_change_request_data.last().modify_date
            )

        return last_updated_date

    class Meta:
        model = User
        fields = [
            "first_name",
            "last_name",
            "birth_date",
            "email",
            "username",
            "secondary_email",
            "pri",
            "military_nbr",
            "last_password_change",
            "is_staff",
            "psrs_applicant_id",
            "last_login",
            "date_joined",
            "last_updated_date",
        ]


class UserProfileChangeRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfileChangeRequest
        fields = "__all__"
