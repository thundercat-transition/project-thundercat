from itertools import groupby
from rest_framework import serializers
from user_management.user_management_models.user_accommodations_profile_repeated_oral_questions import (
    UserAccommodationsProfileRepeatedOralQuestions,
)
from user_management.user_management_models.user_accommodations_profile_other_needs_oral import (
    UserAccommodationsProfileOtherNeedsOral,
)
from user_management.user_management_models.user_accommodations_profile_scheduling_adaptation import (
    UserAccommodationsProfileSchedulingAdaptation,
)
from user_management.user_management_models.user_accommodations_profile_breaks import (
    UserAccommodationsProfileBreaks,
)
from user_management.user_management_models.user_accommodations_profile_extra_time import (
    UserAccommodationsProfileExtraTime,
)
from user_management.user_management_models.user_accommodations_profile_built_in_accessbility import (
    UserAccommodationsProfileBuiltInAccessibility,
)
from user_management.user_management_models.user_accommodations_profile_other_needs_written import (
    UserAccommodationsProfileOtherNeedsWritten,
)
from user_management.user_management_models.user_accommodations_profile_access_assistance import (
    UserAccommodationsProfileAccessAssistance,
)
from user_management.user_management_models.user_accommodations_profile_environment import (
    UserAccommodationsProfileEnvironment,
)
from user_management.user_management_models.user_accommodations_profile_previously_accommodated import (
    UserAccommodationsProfilePreviouslyAccommodated,
)
from user_management.user_management_models.user_accommodations_profile_ergonomic import (
    UserAccommodationsProfileErgonomic,
)
from user_management.user_management_models.user_accommodations_profile_adaptive_tech import (
    UserAccommodationsProfileAdaptiveTech,
)
from user_management.user_management_models.user_accommodations_profile import (
    UserAccommodationsProfile,
)


class UserAccommodationsProfileSerializer(serializers.ModelSerializer):
    built_in_accessibility = serializers.SerializerMethodField()
    extra_time = serializers.SerializerMethodField()
    breaks = serializers.SerializerMethodField()
    adaptive_tech = serializers.SerializerMethodField()
    ergonomic = serializers.SerializerMethodField()
    access_assistance = serializers.SerializerMethodField()
    environment = serializers.SerializerMethodField()
    scheduling_adaptation = serializers.SerializerMethodField()
    other_needs_written = serializers.SerializerMethodField()
    other_needs_oral = serializers.SerializerMethodField()
    repeated_oral_questions = serializers.SerializerMethodField()
    previous_accommodation = serializers.SerializerMethodField()

    def get_built_in_accessibility(self, request):
        if request.has_built_in_accessibility:
            # get adaptive tech description
            try:
                built_in_accessibility = (
                    UserAccommodationsProfileBuiltInAccessibility.objects.get(
                        user_accommodations_profile_id=request.id
                    )
                )
                serialized_data = (
                    UserAccommodationsProfileBuiltInAccessibilitySerializer(
                        built_in_accessibility, many=False
                    ).data
                )
                return serialized_data
            except UserAccommodationsProfileBuiltInAccessibility.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_extra_time(self, request):
        if request.needs_extra_time:
            # get adaptive tech description
            try:
                extra_time = UserAccommodationsProfileExtraTime.objects.get(
                    user_accommodations_profile_id=request.id
                )
                serialized_data = UserAccommodationsProfileExtraTimeSerializer(
                    extra_time, many=False
                ).data
                return serialized_data
            except UserAccommodationsProfileExtraTime.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_breaks(self, request):
        if request.needs_breaks:
            # get breaks description
            try:
                breaks = UserAccommodationsProfileBreaks.objects.get(
                    user_accommodations_profile_id=request.id
                )
                serialized_data = UserAccommodationsProfileBreaksSerializer(
                    breaks, many=False
                ).data
                return serialized_data
            except UserAccommodationsProfileBreaks.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_adaptive_tech(self, request):
        if request.needs_adaptive_tech:
            # get adaptive tech description
            try:
                adaptive_tech = UserAccommodationsProfileAdaptiveTech.objects.get(
                    user_accommodations_profile_id=request.id
                )
                serialized_additional_time_options = (
                    UserAccommodationsProfileAdaptiveTechSerializer(
                        adaptive_tech, many=False
                    ).data
                )
                return serialized_additional_time_options
            except UserAccommodationsProfileAdaptiveTech.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_ergonomic(self, request):
        if request.needs_ergonomic:
            # get ergonomic description
            try:
                adaptive_tech = UserAccommodationsProfileErgonomic.objects.get(
                    user_accommodations_profile_id=request.id
                )
                serialized_additional_time_options = (
                    UserAccommodationsProfileErgonomicSerializer(
                        adaptive_tech, many=False
                    ).data
                )
                return serialized_additional_time_options
            except UserAccommodationsProfileErgonomic.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_access_assistance(self, request):
        if request.needs_access_assistance:
            # get access assistance description
            try:
                adaptive_tech = UserAccommodationsProfileAccessAssistance.objects.get(
                    user_accommodations_profile_id=request.id
                )
                serialized_additional_time_options = (
                    UserAccommodationsProfileAccessAssistanceSerializer(
                        adaptive_tech, many=False
                    ).data
                )
                return serialized_additional_time_options
            except UserAccommodationsProfileAccessAssistance.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_environment(self, request):
        if request.needs_environment:
            # get environment description
            try:
                adaptive_tech = UserAccommodationsProfileEnvironment.objects.get(
                    user_accommodations_profile_id=request.id
                )
                serialized_additional_time_options = (
                    UserAccommodationsProfileEnvironmentSerializer(
                        adaptive_tech, many=False
                    ).data
                )
                return serialized_additional_time_options
            except UserAccommodationsProfileEnvironment.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_scheduling_adaptation(self, request):
        if request.needs_scheduling_adaptation:
            # get paper version description
            try:
                scheduling_adaptation = (
                    UserAccommodationsProfileSchedulingAdaptation.objects.get(
                        user_accommodations_profile_id=request.id
                    )
                )
                serialized_data = (
                    UserAccommodationsProfileSchedulingAdaptationSerializer(
                        scheduling_adaptation, many=False
                    ).data
                )
                return serialized_data
            except UserAccommodationsProfileSchedulingAdaptation.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_other_needs_written(self, request):
        if request.has_other_needs_written:
            # get other needs written description
            try:
                has_other_needs_written = (
                    UserAccommodationsProfileOtherNeedsWritten.objects.get(
                        user_accommodations_profile_id=request.id
                    )
                )
                serialized_data = UserAccommodationsProfileOtherNeedsWrittenSerializer(
                    has_other_needs_written, many=False
                ).data
                return serialized_data
            except UserAccommodationsProfileOtherNeedsWritten.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_other_needs_oral(self, request):
        if request.has_other_needs_oral:
            # get other needs oral description
            try:
                has_other_needs_oral = (
                    UserAccommodationsProfileOtherNeedsOral.objects.get(
                        user_accommodations_profile_id=request.id
                    )
                )
                serialized_data = UserAccommodationsProfileOtherNeedsOralSerializer(
                    has_other_needs_oral, many=False
                ).data
                return serialized_data
            except UserAccommodationsProfileOtherNeedsOral.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_repeated_oral_questions(self, request):
        if request.needs_repeated_oral_questions:
            # get needs repeated oral questions description
            try:
                needs_repeated_oral_questions = (
                    UserAccommodationsProfileRepeatedOralQuestions.objects.get(
                        user_accommodations_profile_id=request.id
                    )
                )
                serialized_data = (
                    UserAccommodationsProfileRepeatedOralQuestionsSerializer(
                        needs_repeated_oral_questions, many=False
                    ).data
                )
                return serialized_data
            except UserAccommodationsProfileRepeatedOralQuestions.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_previous_accommodation(self, request):
        if request.previously_accommodated:
            # get previous accommodation description
            try:
                adaptive_tech = (
                    UserAccommodationsProfilePreviouslyAccommodated.objects.get(
                        user_accommodations_profile_id=request.id
                    )
                )
                serialized_additional_time_options = (
                    UserAccommodationsProfilePreviouslyAccommodatedSerializer(
                        adaptive_tech, many=False
                    ).data
                )
                return serialized_additional_time_options
            except UserAccommodationsProfilePreviouslyAccommodated.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    class Meta:
        model = UserAccommodationsProfile
        fields = "__all__"


class UserAccommodationsProfileForRequestSerializer(serializers.ModelSerializer):
    built_in_accessibility = serializers.SerializerMethodField()
    extra_time = serializers.SerializerMethodField()
    breaks = serializers.SerializerMethodField()
    adaptive_tech = serializers.SerializerMethodField()
    ergonomic = serializers.SerializerMethodField()
    access_assistance = serializers.SerializerMethodField()
    environment = serializers.SerializerMethodField()
    scheduling_adaptation = serializers.SerializerMethodField()
    other_needs_written = serializers.SerializerMethodField()
    other_needs_oral = serializers.SerializerMethodField()
    repeated_oral_questions = serializers.SerializerMethodField()
    previous_accommodation = serializers.SerializerMethodField()

    def get_built_in_accessibility(self, request):
        if request.has_built_in_accessibility:
            # get adaptive tech description
            try:
                built_in_accessibility = (
                    UserAccommodationsProfileBuiltInAccessibility.history.filter(
                        user_accommodations_profile_id=request.id,
                        history_date__gte=request.history_date,
                    )
                    .order_by("history_date")
                    .first()
                )
                serialized_data = (
                    UserAccommodationsProfileBuiltInAccessibilitySerializer(
                        built_in_accessibility, many=False
                    ).data
                )
                return serialized_data
            except UserAccommodationsProfileBuiltInAccessibility.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_extra_time(self, request):
        if request.needs_extra_time:
            try:
                # Since the Extra Time is being saved few milliseconds after saving the UserAccommodationsProfile (technical delay when writing to the database),
                # we have to get the AdditionalTime that has been modified directly after the UserAccommodationsProfile's save (modify date)
                extra_time = (
                    UserAccommodationsProfileExtraTime.history.filter(
                        user_accommodations_profile_id=request.id,
                        history_date__gte=request.history_date,
                    )
                    .order_by("history_date")
                    .first()
                )

                serialized_data = (
                    UserAccommodationsProfileExtraTimeSerializer(extra_time, many=False)
                ).data

                return serialized_data

            except UserAccommodationsProfileExtraTime.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_breaks(self, request):
        if request.needs_breaks:
            try:
                # Since the Extra Time is being saved few milliseconds after saving the UserAccommodationsProfile (technical delay when writing to the database),
                # we have to get the AdditionalTime that has been modified directly after the UserAccommodationsProfile's save (modify date)
                extra_time = (
                    UserAccommodationsProfileBreaks.history.filter(
                        user_accommodations_profile_id=request.id,
                        history_date__gte=request.history_date,
                    )
                    .order_by("history_date")
                    .first()
                )

                serialized_data = (
                    UserAccommodationsProfileBreaksSerializer(extra_time, many=False)
                ).data

                return serialized_data

            except UserAccommodationsProfileBreaks.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_adaptive_tech(self, request):
        if request.needs_adaptive_tech:
            # get adaptive tech description
            try:
                # Since the Adaptive Tech is being saved few milliseconds after saving the UserAccommodationsProfile (technical delay when writing to the database),
                # we have to get the AdaptiveTech that has been modified directly after the UserAccommodationsProfile's save (modify date)
                adaptive_tech = (
                    UserAccommodationsProfileAdaptiveTech.history.filter(
                        user_accommodations_profile_id=request.id,
                        history_date__gte=request.history_date,
                    )
                    .order_by("history_date")
                    .first()
                )

                serialized_data = UserAccommodationsProfileAdaptiveTechSerializer(
                    adaptive_tech, many=False
                ).data
                return serialized_data

            except UserAccommodationsProfileAdaptiveTech.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_ergonomic(self, request):
        if request.needs_ergonomic:
            # get ergonomic description
            try:
                # Since the Ergonomic Tech is being saved few milliseconds after saving the UserAccommodationsProfile (technical delay when writing to the database),
                # we have to get the Ergonomic that has been modified directly after the UserAccommodationsProfile's save (modify date)
                ergonomic = (
                    UserAccommodationsProfileErgonomic.history.filter(
                        user_accommodations_profile_id=request.id,
                        history_date__gte=request.history_date,
                    )
                    .order_by("history_date")
                    .first()
                )

                serialized_data = UserAccommodationsProfileErgonomicSerializer(
                    ergonomic, many=False
                ).data
                return serialized_data

            except UserAccommodationsProfileErgonomic.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_access_assistance(self, request):
        if request.needs_access_assistance:
            # get access assistance description
            try:
                # Since the Access Assistance Tech is being saved few milliseconds after saving the UserAccommodationsProfile (technical delay when writing to the database),
                # we have to get the AccessAssistance that has been modified directly after the UserAccommodationsProfile's save (modify date)
                access_assistance = (
                    UserAccommodationsProfileAccessAssistance.history.filter(
                        user_accommodations_profile_id=request.id,
                        history_date__gte=request.history_date,
                    )
                    .order_by("history_date")
                    .first()
                )

                serialized_data = UserAccommodationsProfileAccessAssistanceSerializer(
                    access_assistance, many=False
                ).data
                return serialized_data
            except UserAccommodationsProfileAccessAssistance.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_environment(self, request):
        if request.needs_environment:
            # get environment description
            try:
                # Since the Environment Tech is being saved few milliseconds after saving the UserAccommodationsProfile (technical delay when writing to the database),
                # we have to get the Environment that has been modified directly after the UserAccommodationsProfile's save (modify date)
                environment = (
                    UserAccommodationsProfileEnvironment.history.filter(
                        user_accommodations_profile_id=request.id,
                        history_date__gte=request.history_date,
                    )
                    .order_by("history_date")
                    .first()
                )

                serialized_data = UserAccommodationsProfileEnvironmentSerializer(
                    environment, many=False
                ).data
                return serialized_data

            except UserAccommodationsProfileEnvironment.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_scheduling_adaptation(self, request):
        if request.needs_scheduling_adaptation:
            # get paper version description
            try:
                # Since the Paper Version Tech is being saved few milliseconds after saving the UserAccommodationsProfile (technical delay when writing to the database),
                # we have to get the scheduling adaptation that has been modified directly after the UserAccommodationsProfile's save (modify date)
                scheduling_adaptation = (
                    UserAccommodationsProfileSchedulingAdaptation.history.filter(
                        user_accommodations_profile_id=request.id,
                        history_date__gte=request.history_date,
                    )
                    .order_by("history_date")
                    .first()
                )

                serialized_data = (
                    UserAccommodationsProfileSchedulingAdaptationSerializer(
                        scheduling_adaptation, many=False
                    ).data
                )
                return serialized_data
            except UserAccommodationsProfileSchedulingAdaptation.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_other_needs_written(self, request):
        if request.has_other_needs_written:
            # get other needs written description
            try:
                # Since the Other Needs Tech is being saved few milliseconds after saving the UserAccommodationsProfile (technical delay when writing to the database),
                # we have to get the other needs written that has been modified directly after the UserAccommodationsProfile's save (modify date)
                other_needs_written = (
                    UserAccommodationsProfileOtherNeedsWritten.history.filter(
                        user_accommodations_profile_id=request.id,
                        history_date__gte=request.history_date,
                    )
                    .order_by("history_date")
                    .first()
                )

                serialized_data = UserAccommodationsProfileOtherNeedsWrittenSerializer(
                    other_needs_written, many=False
                ).data
                return serialized_data

            except UserAccommodationsProfileOtherNeedsWritten.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_other_needs_oral(self, request):
        if request.has_other_needs_oral:
            # get other needs oral description
            try:
                # Since the Other Needs Tech is being saved few milliseconds after saving the UserAccommodationsProfile (technical delay when writing to the database),
                # we have to get the other needs oral that has been modified directly after the UserAccommodationsProfile's save (modify date)
                other_needs_oral = (
                    UserAccommodationsProfileOtherNeedsOral.history.filter(
                        user_accommodations_profile_id=request.id,
                        history_date__gte=request.history_date,
                    )
                    .order_by("history_date")
                    .first()
                )

                serialized_data = UserAccommodationsProfileOtherNeedsOralSerializer(
                    other_needs_oral, many=False
                ).data
                return serialized_data

            except UserAccommodationsProfileOtherNeedsOral.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_repeated_oral_questions(self, request):
        if request.needs_repeated_oral_questions:
            # get needs repeated oral questions description
            try:
                needs_repeated_oral_questions = (
                    UserAccommodationsProfileRepeatedOralQuestions.history.filter(
                        user_accommodations_profile_id=request.id,
                        history_date__gte=request.history_date,
                    )
                    .order_by("history_date")
                    .first()
                )

                serialized_data = (
                    UserAccommodationsProfileRepeatedOralQuestionsSerializer(
                        needs_repeated_oral_questions, many=False
                    ).data
                )
                return serialized_data
            except UserAccommodationsProfileRepeatedOralQuestions.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    def get_previous_accommodation(self, request):
        if request.previously_accommodated:
            # get previous accommodation description
            try:
                # Since the Previously Accommodated Tech is being saved few milliseconds after saving the UserAccommodationsProfile (technical delay when writing to the database),
                # we have to get the PreviouslyAccommodated that has been modified directly after the UserAccommodationsProfile's save (modify date)
                previous_accommodation = (
                    UserAccommodationsProfilePreviouslyAccommodated.history.filter(
                        user_accommodations_profile_id=request.id,
                        history_date__gte=request.history_date,
                    )
                    .order_by("history_date")
                    .first()
                )

                serialized_previous_accommodation = (
                    UserAccommodationsProfilePreviouslyAccommodatedSerializer(
                        previous_accommodation, many=False
                    ).data
                )
                return serialized_previous_accommodation
            except UserAccommodationsProfilePreviouslyAccommodated.DoesNotExist:
                return {"description": ""}
        return {"description": ""}

    class Meta:
        model = UserAccommodationsProfile
        fields = "__all__"


class UserAccommodationsProfileBuiltInAccessibilitySerializer(
    serializers.ModelSerializer
):
    class Meta:
        model = UserAccommodationsProfileBuiltInAccessibility
        fields = [
            "user_accommodations_profile_id",
            "description",
        ]


class UserAccommodationsProfileExtraTimeSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccommodationsProfileExtraTime
        fields = [
            "user_accommodations_profile_id",
            "description",
        ]


class UserAccommodationsProfileBreaksSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccommodationsProfileBreaks
        fields = [
            "user_accommodations_profile_id",
            "description",
        ]


class UserAccommodationsProfileAdaptiveTechSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccommodationsProfileAdaptiveTech
        fields = [
            "user_accommodations_profile_id",
            "description",
        ]


class UserAccommodationsProfileErgonomicSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccommodationsProfileErgonomic
        fields = [
            "user_accommodations_profile_id",
            "description",
        ]


class UserAccommodationsProfileAccessAssistanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccommodationsProfileAccessAssistance
        fields = [
            "user_accommodations_profile_id",
            "description",
        ]


class UserAccommodationsProfileEnvironmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccommodationsProfileEnvironment
        fields = [
            "user_accommodations_profile_id",
            "description",
        ]


class UserAccommodationsProfileSchedulingAdaptationSerializer(
    serializers.ModelSerializer
):
    class Meta:
        model = UserAccommodationsProfileSchedulingAdaptation
        fields = [
            "user_accommodations_profile_id",
            "description",
        ]


class UserAccommodationsProfileOtherNeedsWrittenSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccommodationsProfileOtherNeedsWritten
        fields = [
            "user_accommodations_profile_id",
            "description",
        ]


class UserAccommodationsProfileOtherNeedsOralSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccommodationsProfileOtherNeedsOral
        fields = [
            "user_accommodations_profile_id",
            "description",
        ]


class UserAccommodationsProfileRepeatedOralQuestionsSerializer(
    serializers.ModelSerializer
):
    class Meta:
        model = UserAccommodationsProfileRepeatedOralQuestions
        fields = [
            "user_accommodations_profile_id",
            "description",
        ]


class UserAccommodationsProfilePreviouslyAccommodatedSerializer(
    serializers.ModelSerializer
):
    class Meta:
        model = UserAccommodationsProfilePreviouslyAccommodated
        fields = [
            "user_accommodations_profile_id",
            "description",
        ]
