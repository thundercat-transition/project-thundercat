from rest_framework import status
from rest_framework.response import Response
from user_management.user_management_models.accommodations import Accommodations
from user_management.user_management_models.user_models import User
from user_management.serializers.accommodations_serializer import (
    AccommodationsSerializer,
)
from user_management.views.utils import is_undefined


def get_user_accommodations(user):
    accomms = Accommodations.objects.filter(
        user=User.objects.get(id=user["user_id"])
    ).first()
    # return nothing so we don't override in the front end
    if not accomms:
        return {"empty": True}

    return AccommodationsSerializer(accomms, many=False).data


def save_user_accommodations(user, request):
    accomms = Accommodations.objects.filter(
        user=User.objects.get(id=user["user_id"])
    ).first()

    # get the default values for the model attributes
    def_ff = Accommodations._meta.get_field("font_family").get_default()
    def_fs = Accommodations._meta.get_field("font_size").get_default()
    def_s = Accommodations._meta.get_field("spacing").get_default()

    # determine if parameters are undefined/replace with default
    fs = request.data.get("fontSize", def_fs)
    if is_undefined(fs):
        fs = def_fs

    ff = request.data.get("fontFamily", def_fs)
    if is_undefined(ff):
        ff = def_ff

    spacing = request.data.get("spacing", def_s)
    if is_undefined(spacing):
        spacing = def_s

    # if accomms don't exist, create them
    if not accomms:
        accomms = Accommodations.objects.create(
            user=User.objects.get(id=user["user_id"]),
            font_family=ff,
            font_size=fs,
            spacing=spacing,
        )
    # accomm does exist so update it
    else:
        accomms = Accommodations(
            user=User.objects.get(id=user["user_id"]),
            font_family=request.data.get("fontFamily", accomms.font_family or def_ff),
            font_size=request.data.get("fontSize", accomms.font_size or def_fs),
            spacing=request.data.get("spacing", accomms.font_size or def_s),
        )
        accomms.save()

    return Response(
        {"message": "the accommodations have been saved"}, status=status.HTTP_200_OK
    )
