from django.forms import ValidationError
from rest_framework import status
from rest_framework.response import Response
from user_management.user_management_models.user_accommodations_profile_other_needs_oral import (
    UserAccommodationsProfileOtherNeedsOral,
)
from user_management.user_management_models.user_accommodations_profile_built_in_accessbility import (
    UserAccommodationsProfileBuiltInAccessibility,
)
from user_management.user_management_models.user_accommodations_profile_repeated_oral_questions import (
    UserAccommodationsProfileRepeatedOralQuestions,
)
from user_management.user_management_models.user_accommodations_profile_scheduling_adaptation import (
    UserAccommodationsProfileSchedulingAdaptation,
)
from user_management.user_management_models.user_accommodations_profile_breaks import (
    UserAccommodationsProfileBreaks,
)
from user_management.user_management_models.user_accommodations_profile_extra_time import (
    UserAccommodationsProfileExtraTime,
)
from user_management.user_management_models.user_accommodations_profile_previously_accommodated import (
    UserAccommodationsProfilePreviouslyAccommodated,
)
from user_management.user_management_models.user_accommodations_profile_other_needs_written import (
    UserAccommodationsProfileOtherNeedsWritten,
)
from user_management.user_management_models.user_accommodations_profile_environment import (
    UserAccommodationsProfileEnvironment,
)
from user_management.user_management_models.user_accommodations_profile_access_assistance import (
    UserAccommodationsProfileAccessAssistance,
)
from user_management.user_management_models.user_accommodations_profile_ergonomic import (
    UserAccommodationsProfileErgonomic,
)
from user_management.user_management_models.user_accommodations_profile_adaptive_tech import (
    UserAccommodationsProfileAdaptiveTech,
)
from user_management.serializers.user_accommodations_profile_serializers import (
    UserAccommodationsProfileSerializer,
    UserAccommodationsProfileForRequestSerializer,
)
from user_management.user_management_models.user_accommodations_profile import (
    UserAccommodationsProfile,
)
from user_management.user_management_models.user_models import User


def get_user_accommodations_profile(user):
    # Get existing user accommodations profile
    try:
        user_accommodations_profile = UserAccommodationsProfile.objects.get(
            user=User.objects.get(id=user["user_id"])
        )

        return UserAccommodationsProfileSerializer(
            user_accommodations_profile, many=False
        ).data

    # If no existing user accommodations profile
    # return an object that can be used to put the data
    except UserAccommodationsProfile.DoesNotExist:
        # initial object
        initial_object = {
            "has_built_in_accessibility": False,
            "built_in_accessibility": {"description": ""},
            "needs_extra_time": False,
            "extra_time": {"description": ""},
            "needs_breaks": False,
            "breaks": {"description": ""},
            "needs_adaptive_tech": False,
            "adaptive_tech": {"description": ""},
            "needs_ergonomic": False,
            "ergonomic": {"description": ""},
            "needs_access_assistance": False,
            "access_assistance": {"description": ""},
            "needs_environment": False,
            "environment": {"description": ""},
            "needs_scheduling_adaptation": False,
            "scheduling_adaptation": {"description": ""},
            "has_other_needs_written": False,
            "other_needs_written": {"description": ""},
            "has_other_needs_oral": False,
            "other_needs_oral": {"description": ""},
            "needs_repeated_oral_questions": False,
            "repeated_oral_questions": {"description": ""},
            "previously_accommodated": False,
            "previous_accommodation": {"description": ""},
        }

        return initial_object

    # If missing argument
    except TypeError as e:
        return Response({"error": e}, status=status.HTTP_400_BAD_REQUEST)


def get_user_accommodations_profile_for_request(user_id, request_date):
    # Get existing user accommodations profile
    try:
        # Get the most up to date data based on a date that is lower than the request date
        user_accommodations_profile = (
            UserAccommodationsProfile.history.filter(
                user_id=user_id,
                history_date__lte=request_date,
            )
            .order_by("-history_date")
            .first()
        )
        return UserAccommodationsProfileForRequestSerializer(
            user_accommodations_profile, many=False
        ).data

    # If no existing user accommodations profile
    # return an object that can be used to put the data
    except UserAccommodationsProfile.DoesNotExist:
        # initial object
        initial_object = {
            "has_built_in_accessibility": False,
            "built_in_accessibility": {"description": ""},
            "needs_extra_time": False,
            "extra_time": {"description": ""},
            "needs_breaks": False,
            "breaks": {"description": ""},
            "needs_adaptive_tech": False,
            "adaptive_tech": {"description": ""},
            "needs_ergonomic": False,
            "ergonomic": {"description": ""},
            "needs_access_assistance": False,
            "access_assistance": {"description": ""},
            "needs_environment": False,
            "environment": {"description": ""},
            "needs_scheduling_adaptation": False,
            "scheduling_adaptation": {"description": ""},
            "has_other_needs_written": False,
            "other_needs_written": {"description": ""},
            "has_other_needs_oral": False,
            "other_needs_oral": {"description": ""},
            "needs_repeated_oral_questions": False,
            "repeated_oral_questions": {"description": ""},
            "previously_accommodated": False,
            "previous_accommodation": {"description": ""},
        }

        return initial_object

    # If missing argument
    except TypeError as e:
        return Response({"error": e}, status=status.HTTP_400_BAD_REQUEST)


def save_user_accommodations_profile(user, request):
    # Case where we have already an accommodations profile
    try:
        user_accommodations_profile = UserAccommodationsProfile.objects.get(
            user=User.objects.get(id=user["user_id"])
        )

        # Make modifications to UserAccommodationsProfile with given data
        user_accommodations_profile.has_built_in_accessibility = request[
            "has_built_in_accessibility"
        ]
        user_accommodations_profile.needs_extra_time = request["needs_extra_time"]
        user_accommodations_profile.needs_breaks = request["needs_breaks"]
        user_accommodations_profile.needs_adaptive_tech = request["needs_adaptive_tech"]
        user_accommodations_profile.needs_ergonomic = request["needs_ergonomic"]
        user_accommodations_profile.needs_access_assistance = request[
            "needs_access_assistance"
        ]
        user_accommodations_profile.needs_environment = request["needs_environment"]
        user_accommodations_profile.needs_scheduling_adaptation = request[
            "needs_scheduling_adaptation"
        ]
        user_accommodations_profile.has_other_needs_written = request[
            "has_other_needs_written"
        ]
        user_accommodations_profile.has_other_needs_oral = request[
            "has_other_needs_oral"
        ]
        user_accommodations_profile.needs_repeated_oral_questions = request[
            "needs_repeated_oral_questions"
        ]
        user_accommodations_profile.previously_accommodated = request[
            "previously_accommodated"
        ]

        user_accommodations_profile.save()

    # If no existing user accommodations profile
    # creates everything from scratch
    except UserAccommodationsProfile.DoesNotExist:
        user_accommodations_profile = UserAccommodationsProfile.objects.create(
            user_id=user["user_id"],
            has_built_in_accessibility=request["has_built_in_accessibility"],
            needs_extra_time=request["needs_extra_time"],
            needs_breaks=request["needs_breaks"],
            needs_adaptive_tech=request["needs_adaptive_tech"],
            needs_ergonomic=request["needs_ergonomic"],
            needs_access_assistance=request["needs_access_assistance"],
            needs_environment=request["needs_environment"],
            needs_scheduling_adaptation=request["needs_scheduling_adaptation"],
            has_other_needs_written=request["has_other_needs_written"],
            has_other_needs_oral=request["has_other_needs_oral"],
            needs_repeated_oral_questions=request["needs_repeated_oral_questions"],
            previously_accommodated=request["previously_accommodated"],
        )
    # If missing argument
    except (TypeError, KeyError) as e:
        return Response(
            {"error": "missing argument: {0}".format(e)},
            status=status.HTTP_400_BAD_REQUEST,
        )
    # If wrong value format
    except (ValueError, ValidationError) as e:
        return Response(
            {"error": "wrong value format: {0}".format(e)},
            status=status.HTTP_400_BAD_REQUEST,
        )

    try:
        # Take care of built-in accessibility data
        create_or_modify_built_in_accessibility_data(
            user_accommodations_profile, request
        )

        # Take care of extra time data
        create_or_modify_extra_time_data(user_accommodations_profile, request)

        # Take care of break time data
        create_or_modify_breaks_data(user_accommodations_profile, request)

        # Take care of adaptive tech data
        create_or_modify_adaptive_tech_data(user_accommodations_profile, request)

        # Take care of ergonomic data
        create_or_modify_ergonomic_data(user_accommodations_profile, request)

        # Take care of access assistance data
        create_or_modify_access_assistance_data(user_accommodations_profile, request)

        # Take care of environment data
        create_or_modify_environment_data(user_accommodations_profile, request)

        # Take care of scheduling adaptation data
        create_or_modify_scheduling_adaptation_data(
            user_accommodations_profile, request
        )

        # Take care of other needs written data
        create_or_modify_other_needs_written_data(user_accommodations_profile, request)

        # Take care of other needs oral data
        create_or_modify_other_needs_oral_data(user_accommodations_profile, request)

        # Take care of repeated oral questions data
        create_or_modify_repeated_oral_questions_data(
            user_accommodations_profile, request
        )

        # Take care of previously accommodated data
        create_or_modify_previously_accommodated_data(
            user_accommodations_profile, request
        )

    # If missing argument
    except (TypeError, KeyError) as e:
        return Response(
            {"error": "missing argument: {0}".format(e)},
            status=status.HTTP_400_BAD_REQUEST,
        )
    # If wrong value format
    except (ValueError, ValidationError) as e:
        return Response(
            {"error": "wrong value format: {0}".format(e)},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # Success
    return Response(
        {"message": "the user accommodations profile has been saved"},
        status=status.HTTP_200_OK,
    )


def create_or_modify_built_in_accessibility_data(user_accommodations_profile, request):
    # Built-in Accessibility
    # Look if currently have a row for built-in-accessibility
    try:
        user_accommodation_profile_built_in_accessibility = (
            UserAccommodationsProfileBuiltInAccessibility.objects.get(
                user_accommodations_profile=user_accommodations_profile
            )
        )

        # if we need built-in-accessibility
        if request["has_built_in_accessibility"]:
            user_accommodation_profile_built_in_accessibility.description = request[
                "built_in_accessibility"
            ]["description"]

            user_accommodation_profile_built_in_accessibility.save()

        # if we no longer need built-in-accessibility, delete the row
        else:
            user_accommodation_profile_built_in_accessibility.delete()

    # If no existing user accommodations profile built-in-accessibility
    # creates everything from scratch (if needed)
    except UserAccommodationsProfileBuiltInAccessibility.DoesNotExist:
        if request["has_built_in_accessibility"]:
            UserAccommodationsProfileBuiltInAccessibility.objects.create(
                user_accommodations_profile=user_accommodations_profile,
                description=request["built_in_accessibility"]["description"],
            )
    # If missing argument / wrong type of value
    except (TypeError, KeyError, ValueError, ValidationError) as e:
        # raise sends the error to this function's caller
        raise e


def create_or_modify_extra_time_data(user_accommodations_profile, request):
    # Extra Time
    # Look if currently have a row for extra time
    try:
        user_accommodation_profile_extra_time = (
            UserAccommodationsProfileExtraTime.objects.get(
                user_accommodations_profile=user_accommodations_profile
            )
        )

        # if we need extra time
        if request["needs_extra_time"]:
            user_accommodation_profile_extra_time.description = request["extra_time"][
                "description"
            ]

            user_accommodation_profile_extra_time.save()

        # if we no longer need extra time, delete the row
        else:
            user_accommodation_profile_extra_time.delete()

    # If no existing user accommodations profile extra time
    # creates everything from scratch (if needed)
    except UserAccommodationsProfileExtraTime.DoesNotExist:
        if request["needs_extra_time"]:
            UserAccommodationsProfileExtraTime.objects.create(
                user_accommodations_profile=user_accommodations_profile,
                description=request["extra_time"]["description"],
            )
    # If missing argument / wrong type of value
    except (TypeError, KeyError, ValueError, ValidationError) as e:
        # raise sends the error to this function's caller
        raise e


def create_or_modify_breaks_data(user_accommodations_profile, request):
    # Breaks
    # Look if currently have a row for breaks
    try:
        user_accommodation_profile_breaks = UserAccommodationsProfileBreaks.objects.get(
            user_accommodations_profile=user_accommodations_profile
        )

        # if we need breaks
        if request["needs_breaks"]:
            user_accommodation_profile_breaks.description = request["breaks"][
                "description"
            ]

            user_accommodation_profile_breaks.save()

        # if we no longer need breaks, delete the row
        else:
            user_accommodation_profile_breaks.delete()

    # If no existing user accommodations profile breaks
    # creates everything from scratch (if needed)
    except UserAccommodationsProfileBreaks.DoesNotExist:
        if request["needs_breaks"]:
            UserAccommodationsProfileBreaks.objects.create(
                user_accommodations_profile=user_accommodations_profile,
                description=request["breaks"]["description"],
            )
    # If missing argument / wrong type of value
    except (TypeError, KeyError, ValueError, ValidationError) as e:
        # raise sends the error to this function's caller
        raise e


def create_or_modify_adaptive_tech_data(user_accommodations_profile, request):
    # Adaptive Tech
    # Look if currently have a row for adaptive tech
    try:
        user_accommodation_profile_adaptive_tech = (
            UserAccommodationsProfileAdaptiveTech.objects.get(
                user_accommodations_profile=user_accommodations_profile
            )
        )

        # if we need adaptive tech
        if request["needs_adaptive_tech"]:
            user_accommodation_profile_adaptive_tech.description = request[
                "adaptive_tech"
            ]["description"]

            user_accommodation_profile_adaptive_tech.save()

        # if we no longer need adaptive tech, delete the row
        else:
            user_accommodation_profile_adaptive_tech.delete()

    # If no existing user accommodations profile adaptive tech
    # creates everything from scratch (if needed)
    except UserAccommodationsProfileAdaptiveTech.DoesNotExist:
        if request["needs_adaptive_tech"]:
            UserAccommodationsProfileAdaptiveTech.objects.create(
                user_accommodations_profile=user_accommodations_profile,
                description=request["adaptive_tech"]["description"],
            )
    # If missing argument / wrong type of value
    except (TypeError, KeyError, ValueError, ValidationError) as e:
        # raise sends the error to this function's caller
        raise e


def create_or_modify_ergonomic_data(user_accommodations_profile, request):
    # Ergonomic
    # Look if currently have a row for ergonomic
    try:
        user_accommodation_profile_ergonomic = (
            UserAccommodationsProfileErgonomic.objects.get(
                user_accommodations_profile=user_accommodations_profile
            )
        )

        # if we need ergonomic
        if request["needs_ergonomic"]:
            user_accommodation_profile_ergonomic.description = request["ergonomic"][
                "description"
            ]

            user_accommodation_profile_ergonomic.save()

        # if we no longer need ergonomic, delete the row
        else:
            user_accommodation_profile_ergonomic.delete()

    # If no existing user accommodations profile ergonomic
    # creates everything from scratch (if needed)
    except UserAccommodationsProfileErgonomic.DoesNotExist:
        if request["needs_ergonomic"]:
            UserAccommodationsProfileErgonomic.objects.create(
                user_accommodations_profile=user_accommodations_profile,
                description=request["ergonomic"]["description"],
            )
    # If missing argument / wrong type of value
    except (TypeError, KeyError, ValueError, ValidationError) as e:
        # raise sends the error to this function's caller
        raise e


def create_or_modify_access_assistance_data(user_accommodations_profile, request):
    # Access Assistance
    # Look if currently have a row for ergonomic
    try:
        user_accommodation_profile_access_assistance = (
            UserAccommodationsProfileAccessAssistance.objects.get(
                user_accommodations_profile=user_accommodations_profile
            )
        )

        # if we need ergonomic
        if request["needs_access_assistance"]:
            user_accommodation_profile_access_assistance.description = request[
                "access_assistance"
            ]["description"]

            user_accommodation_profile_access_assistance.save()

        # if we no longer need access_assistance, delete the row
        else:
            user_accommodation_profile_access_assistance.delete()

    # If no existing user accommodations profile access_assistance
    # creates everything from scratch (if needed)
    except UserAccommodationsProfileAccessAssistance.DoesNotExist:
        if request["needs_access_assistance"]:
            UserAccommodationsProfileAccessAssistance.objects.create(
                user_accommodations_profile=user_accommodations_profile,
                description=request["access_assistance"]["description"],
            )
    # If missing argument / wrong type of value
    except (TypeError, KeyError, ValueError, ValidationError) as e:
        # raise sends the error to this function's caller
        raise e


def create_or_modify_environment_data(user_accommodations_profile, request):
    # Environment
    # Look if currently have a row for ergonomic
    try:
        user_accommodation_profile_environment = (
            UserAccommodationsProfileEnvironment.objects.get(
                user_accommodations_profile=user_accommodations_profile
            )
        )

        # if we need ergonomic
        if request["needs_environment"]:
            user_accommodation_profile_environment.description = request["environment"][
                "description"
            ]

            user_accommodation_profile_environment.save()

        # if we no longer need environment, delete the row
        else:
            user_accommodation_profile_environment.delete()

    # If no existing user accommodations profile environment
    # creates everything from scratch (if needed)
    except UserAccommodationsProfileEnvironment.DoesNotExist:
        if request["needs_environment"]:
            UserAccommodationsProfileEnvironment.objects.create(
                user_accommodations_profile=user_accommodations_profile,
                description=request["environment"]["description"],
            )
    # If missing argument / wrong type of value
    except (TypeError, KeyError, ValueError, ValidationError) as e:
        # raise sends the error to this function's caller
        raise e


def create_or_modify_scheduling_adaptation_data(user_accommodations_profile, request):
    # Scheduling Adaptation
    # Look if currently have a row for scheduling adaptation
    try:
        user_accommodation_profile_scheduling_adaptation = (
            UserAccommodationsProfileSchedulingAdaptation.objects.get(
                user_accommodations_profile=user_accommodations_profile
            )
        )

        # if we need scheduling adaptation
        if request["needs_scheduling_adaptation"]:
            user_accommodation_profile_scheduling_adaptation.description = request[
                "scheduling_adaptation"
            ]["description"]

            user_accommodation_profile_scheduling_adaptation.save()

        # if we no longer need scheduling adaptation, delete the row
        else:
            user_accommodation_profile_scheduling_adaptation.delete()

    # If no existing user accommodations profile scheduling adaptation
    # creates everything from scratch (if needed)
    except UserAccommodationsProfileSchedulingAdaptation.DoesNotExist:
        if request["needs_scheduling_adaptation"]:
            UserAccommodationsProfileSchedulingAdaptation.objects.create(
                user_accommodations_profile=user_accommodations_profile,
                description=request["scheduling_adaptation"]["description"],
            )
    # If missing argument / wrong type of value
    except (TypeError, KeyError, ValueError, ValidationError) as e:
        # raise sends the error to this function's caller
        raise e


def create_or_modify_other_needs_written_data(user_accommodations_profile, request):
    # Other Needs Written
    # Look if currently have a row for other needs written
    try:
        user_accommodation_profile_other_needs_written = (
            UserAccommodationsProfileOtherNeedsWritten.objects.get(
                user_accommodations_profile=user_accommodations_profile
            )
        )

        # if we need other needs written
        if request["has_other_needs_written"]:
            user_accommodation_profile_other_needs_written.description = request[
                "other_needs_written"
            ]["description"]

            user_accommodation_profile_other_needs_written.save()

        # if we no longer need other needs written, delete the row
        else:
            user_accommodation_profile_other_needs_written.delete()

    # If no existing user accommodations profile other needs written
    # creates everything from scratch (if needed)
    except UserAccommodationsProfileOtherNeedsWritten.DoesNotExist:
        if request["has_other_needs_written"]:
            UserAccommodationsProfileOtherNeedsWritten.objects.create(
                user_accommodations_profile=user_accommodations_profile,
                description=request["other_needs_written"]["description"],
            )
    # If missing argument / wrong type of value
    except (TypeError, KeyError, ValueError, ValidationError) as e:
        # raise sends the error to this function's caller
        raise e


def create_or_modify_other_needs_oral_data(user_accommodations_profile, request):
    # Other Needs Oral
    # Look if currently have a row for other needs oral
    try:
        user_accommodation_profile_other_needs_oral = (
            UserAccommodationsProfileOtherNeedsOral.objects.get(
                user_accommodations_profile=user_accommodations_profile
            )
        )

        # if we need other needs oral
        if request["has_other_needs_oral"]:
            user_accommodation_profile_other_needs_oral.description = request[
                "other_needs_oral"
            ]["description"]

            user_accommodation_profile_other_needs_oral.save()

        # if we no longer need other needs oral, delete the row
        else:
            user_accommodation_profile_other_needs_oral.delete()

    # If no existing user accommodations profile other needs oral
    # creates everything from scratch (if needed)
    except UserAccommodationsProfileOtherNeedsOral.DoesNotExist:
        if request["has_other_needs_oral"]:
            UserAccommodationsProfileOtherNeedsOral.objects.create(
                user_accommodations_profile=user_accommodations_profile,
                description=request["other_needs_oral"]["description"],
            )
    # If missing argument / wrong type of value
    except (TypeError, KeyError, ValueError, ValidationError) as e:
        # raise sends the error to this function's caller
        raise e


def create_or_modify_repeated_oral_questions_data(user_accommodations_profile, request):
    # Repeated Oral Questions
    # Look if currently have a row for repeated oral questions
    try:
        user_accommodation_profile_repeated_oral_questions = (
            UserAccommodationsProfileRepeatedOralQuestions.objects.get(
                user_accommodations_profile=user_accommodations_profile
            )
        )

        # if we need repeated oral questions
        if request["needs_repeated_oral_questions"]:
            user_accommodation_profile_repeated_oral_questions.description = request[
                "repeated_oral_questions"
            ]["description"]

            user_accommodation_profile_repeated_oral_questions.save()

        # if we no longer need repeated oral questions, delete the row
        else:
            user_accommodation_profile_repeated_oral_questions.delete()

    # If no existing user accommodations profile repeated oral questions
    # creates everything from scratch (if needed)
    except UserAccommodationsProfileRepeatedOralQuestions.DoesNotExist:
        if request["needs_repeated_oral_questions"]:
            UserAccommodationsProfileRepeatedOralQuestions.objects.create(
                user_accommodations_profile=user_accommodations_profile,
                description=request["repeated_oral_questions"]["description"],
            )
    # If missing argument / wrong type of value
    except (TypeError, KeyError, ValueError, ValidationError) as e:
        # raise sends the error to this function's caller
        raise e


def create_or_modify_previously_accommodated_data(user_accommodations_profile, request):
    # Previously Accommodated
    # Look if currently have a row for ergonomic
    try:
        user_accommodation_profile_previously_accommodated = (
            UserAccommodationsProfilePreviouslyAccommodated.objects.get(
                user_accommodations_profile=user_accommodations_profile
            )
        )

        # if we need ergonomic
        if request["previously_accommodated"]:
            user_accommodation_profile_previously_accommodated.description = request[
                "previous_accommodation"
            ]["description"]

            user_accommodation_profile_previously_accommodated.save()

        # if we no longer need previously_accommodated, delete the row
        else:
            user_accommodation_profile_previously_accommodated.delete()

    # If no existing user accommodations profile previously_accommodated
    # creates everything from scratch (if needed)
    except UserAccommodationsProfilePreviouslyAccommodated.DoesNotExist:
        if request["previously_accommodated"]:
            UserAccommodationsProfilePreviouslyAccommodated.objects.create(
                user_accommodations_profile=user_accommodations_profile,
                description=request["previous_accommodation"]["description"],
            )
    # If missing argument / wrong type of value
    except (TypeError, KeyError, ValueError, ValidationError) as e:
        # raise sends the error to this function's caller
        raise e
