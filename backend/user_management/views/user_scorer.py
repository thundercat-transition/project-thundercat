from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from user_management.views.utils import get_user_info_from_jwt_token
from backend.api_permissions.role_based_api_permissions import HasScorerPermission
from backend.custom_models.test_center_ola_test_assessor import (
    TestCenterOlaTestAssessor,
)


class IsUserAssociatedToTestCenter(APIView):
    def get(self, request):
        return is_user_associated_to_test_center(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasScorerPermission()]


# Function that returns true if user_id is linked to a test center
def is_user_associated_to_test_center(request):
    # getting user info
    user_info = get_user_info_from_jwt_token(request)

    # initializing needed attributes
    is_ola_test_assessor = False
    is_ola_supervisor = False

    test_center_associations = TestCenterOlaTestAssessor.objects.filter(
        user_id=user_info["user_id"]
    )

    # test_center_associations found
    if test_center_associations:
        # setting is_ola_test_assessor to True
        is_ola_test_assessor = True

        # looping in test_center_associations
        for test_center_association in test_center_associations:
            # checking if the user is a supervisor
            if test_center_association.supervisor:
                # setting is_ola_supervisor to True
                is_ola_supervisor = True
                # breaking the loop
                break

    # returning attributes
    return Response(
        {
            "is_ola_test_assessor": is_ola_test_assessor,
            "is_ola_supervisor": is_ola_supervisor,
        }
    )
