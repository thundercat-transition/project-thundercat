from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from cms.views.utils import get_user_info_from_jwt_token
from user_management.views.user_accommodations_profile import (
    get_user_accommodations_profile,
    save_user_accommodations_profile,
    get_user_accommodations_profile_for_request,
)


class GetUserAccommodationsProfile(APIView):
    def get(self, request):
        user_info = get_user_info_from_jwt_token(request)

        return Response(get_user_accommodations_profile(user_info))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# Get UserAccommodationsProfile for Specific Accommodations File Request
class GetUserAccommodationsProfileForRequest(APIView):
    def get(self, request):
        user_info = get_user_info_from_jwt_token(request)

        return Response(
            get_user_accommodations_profile_for_request(
                user_info["user_id"], request.data["request_date"]
            )
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class SaveUserAccommodationsProfile(APIView):
    def post(self, request):
        user_info = get_user_info_from_jwt_token(request)

        return save_user_accommodations_profile(user_info, request.data)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
