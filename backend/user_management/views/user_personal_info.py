import datetime
import urllib.parse
from urllib import parse
import json
from operator import itemgetter
from django.db.models import Q
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django_rest_passwordreset.views import ResetPasswordToken
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions, status
from db_views.serializers.user_profile_change_request_serializers import (
    UserProfileChangeRequestViewSerializer,
)
from db_views.db_view_models.user_profile_change_request_vw import (
    UserProfileChangeRequestVW,
)
from cms.views.utils import get_needed_parameters, get_optional_parameters
from user_management.serializers.user_profile_serializer import (
    UserProfileSerializer,
    UserProfileChangeRequestSerializer,
)
from user_management.views.utils import CustomPagination
from user_management.views.utils import get_user_info_from_jwt_token
from user_management.user_management_models.user_models import User
from user_management.user_management_models.user_profile_change_request import (
    UserProfileChangeRequest,
)
from backend.api_permissions.role_based_api_permissions import HasSystemAdminPermission


def update_user_personal_info(request):
    success, parameters = get_needed_parameters(
        ["user_id", "username", "email"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    optional_parameters = get_optional_parameters(
        ["phone_number", "secondary_email", "pri", "military_nbr", "psrs_app_id"],
        request,
    )

    # secondary email validation (useful only for manual entry in the DB, not for the Personal Info page, since there is frontend validation first)
    try:
        # if secondary_email string is not empty, validate it
        if optional_parameters["secondary_email"] != "":
            validate_email(optional_parameters["secondary_email"])
    except ValidationError:
        return Response(
            {"error": "secondary email is invalid"}, status=status.HTTP_400_BAD_REQUEST
        )
    # getting current user
    user_info = get_user_info_from_jwt_token(request)

    if user_info["user_id"] != parameters["user_id"]:
        return Response(
            {
                "error": "you do not have permission to access someone else's personal information."
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
    current_user = User.objects.filter(id=parameters["user_id"])

    # checking if username is associated to another account
    if User.objects.filter(username=parameters["username"]).exclude(
        id=parameters["user_id"]
    ):
        return Response(
            {
                "error_code": 1,
                "error": "the provided username is already associated to another account",
            },
            status=status.HTTP_409_CONFLICT,
        )

    # checking if email is associated to another account
    if User.objects.filter(email=parameters["email"]).exclude(id=parameters["user_id"]):
        return Response(
            {
                "error_code": 2,
                "error": "the provided email is already associated to another account",
            },
            status=status.HTTP_409_CONFLICT,
        )

    # checking if username is associated to another account's email
    if User.objects.filter(email=parameters["username"]).exclude(
        id=parameters["user_id"]
    ):
        return Response(
            {
                "error_code": 3,
                "error": "the provided username is already used as the email of another account",
            },
            status=status.HTTP_409_CONFLICT,
        )

    # checking if email is associated to another account's username
    if User.objects.filter(username=parameters["email"]).exclude(
        id=parameters["user_id"]
    ):
        return Response(
            {
                "error_code": 4,
                "error": "the provided email is already used as the username of another account",
            },
            status=status.HTTP_409_CONFLICT,
        )

    # updating user's personal info
    current_user.update(
        username=parameters["username"],
        email=parameters["email"],
        secondary_email=optional_parameters["secondary_email"],
        phone_number=optional_parameters["phone_number"],
        pri=optional_parameters["pri"],
        military_nbr=optional_parameters["military_nbr"],
        psrs_applicant_id=optional_parameters["psrs_app_id"],
    )
    return Response(
        UserProfileSerializer(current_user[0], many=False).data,
        status=status.HTTP_200_OK,
    )


class UpdateUserPersonalInfo(APIView):
    def post(self, request):
        return update_user_personal_info(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def update_last_password_change_time(request):
    user_info = get_user_info_from_jwt_token(request)
    # getting current user
    current_user = User.objects.filter(id=user_info["user_id"])
    # updating user's last password change time
    current_user.update(last_password_change=datetime.date.today())
    return status.HTTP_200_OK


def send_user_profile_change_request(request):
    # getting current user
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(
        [
            "current_first_name",
            "current_last_name",
            "current_birth_date",
            "new_first_name",
            "new_last_name",
            "new_birth_date",
            "comments",
        ],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

    # getting user_id
    user_id = User.objects.get(id=user_info["user_id"]).id

    # making sure that there is no existing request at the moment (if so, delete them)
    existing_profile_change_requests = UserProfileChangeRequest.objects.filter(
        user_id=user_id
    )
    if existing_profile_change_requests:
        for existing_profile_change_request in existing_profile_change_requests:
            existing_profile_change_request.delete()

    # checking if new DOB has been provided
    new_birth_date = None
    if parameters["new_birth_date"] != "null-null-null":
        # setting new_birth_date with provided parameter
        new_birth_date = parameters["new_birth_date"]

    # creating new user profile change request
    UserProfileChangeRequest.objects.create(
        user_id=user_id,
        current_first_name=parameters["current_first_name"],
        current_last_name=parameters["current_last_name"],
        current_birth_date=parameters["current_birth_date"],
        new_first_name=parameters["new_first_name"],
        new_last_name=parameters["new_last_name"],
        new_birth_date=new_birth_date,
        comments=parameters["comments"],
        reason_for_deny="",
    )

    return Response(status=status.HTTP_200_OK)


class SendUserProfileChangeRequest(APIView):
    def post(self, request):
        return send_user_profile_change_request(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_user_profile_change_request(request):
    # getting current user
    user_info = get_user_info_from_jwt_token(request)

    # getting user_id
    user_id = User.objects.get(id=user_info["user_id"]).id

    # checking if there is an existing request
    existing_profile_change_requests = UserProfileChangeRequest.objects.filter(
        user_id=user_id
    )

    # existing profile change request
    if existing_profile_change_requests:
        serialized_data = UserProfileChangeRequestSerializer(
            existing_profile_change_requests.last(), many=False
        ).data
        return Response(serialized_data)

    # no profile change request
    else:
        return Response(
            {
                "info": "User ID: {0} ({1}) has no existing user profile change request".format(
                    user_info["user_id"], user_info["username"]
                )
            },
            status=status.HTTP_200_OK,
        )


class GetUserProfileChangeRequest(APIView):
    def get(self, request):
        return get_user_profile_change_request(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def delete_user_profile_change_request(request):
    # getting current user
    user_info = get_user_info_from_jwt_token(request)

    # getting user_id
    user_id = User.objects.get(id=user_info["user_id"]).id

    # get existing request(s) and delete needed data
    existing_profile_change_requests = UserProfileChangeRequest.objects.filter(
        user_id=user_id
    )
    for existing_profile_change_request in existing_profile_change_requests:
        existing_profile_change_request.delete()

    return Response(status=status.HTTP_200_OK)


class DeleteUserProfileChangeRequest(APIView):
    def post(self, request):
        return delete_user_profile_change_request(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_all_user_profile_change_requests(request):
    # getting current user
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(["page", "page_size"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    page, page_size = itemgetter("page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # getting all user profile change requests (excluding current user)
    user_profile_change_requests = UserProfileChangeRequestVW.objects.all().exclude(
        user_id=user_info["user_id"]
    )

    # getting serialized data
    serialized_data = UserProfileChangeRequestViewSerializer(
        user_profile_change_requests, many=True
    ).data

    # getting sorted serialized data by last_name (ascending)
    ordered_user_profile_change_requests = sorted(
        serialized_data,
        key=lambda k: k["current_last_name"].lower(),
        reverse=False,
    )
    # only getting data for current selected page
    new_user_profile_change_requests = ordered_user_profile_change_requests[
        (current_page - 1) * page_size : current_page * page_size
    ]
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_user_profile_change_requests, len(serialized_data), current_page, page_size
    )


class GetAllUserProfileChangeRequests(APIView):
    def get(self, request):
        return get_all_user_profile_change_requests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


def get_found_user_profile_change_requests(request):
    # getting current user
    user_info = get_user_info_from_jwt_token(request)

    success, parameters = get_needed_parameters(
        ["keyword", "page", "page_size"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    keyword, page, page_size = itemgetter("keyword", "page", "page_size")(parameters)

    # initializing current_page and page_size
    current_page = int(page)
    page_size = int(page_size)

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # if blank search
    if keyword == " ":
        # getting all user profile change requests (excluding current user)
        found_user_profile_change_requests = (
            UserProfileChangeRequestVW.objects.all().exclude(
                user_id=user_info["user_id"]
            )
        )
    # regular search
    else:
        # decode keyword to get special characters
        keyword = parse.unquote(keyword)

        # getting found user profile change requests (excluding current user)
        found_user_profile_change_requests = UserProfileChangeRequestVW.objects.filter(
            Q(email__icontains=keyword)
            | Q(current_first_name__icontains=keyword)
            | Q(current_last_name__icontains=keyword)
            | Q(current_birth_date__icontains=keyword)
        ).exclude(user_id=user_info["user_id"])

    # no results found
    if not found_user_profile_change_requests:
        return Response({"no results found"}, status=status.HTTP_200_OK)

    # getting serialized data
    serialized_data = UserProfileChangeRequestViewSerializer(
        found_user_profile_change_requests, many=True
    ).data

    # getting sorted serialized data by last_name (ascending)
    ordered_found_user_profile_change_requests = sorted(
        serialized_data,
        key=lambda k: k["current_last_name"].lower(),
        reverse=False,
    )
    # only getting data for current selected page
    new_found_user_profile_change_requests = ordered_found_user_profile_change_requests[
        (current_page - 1) * page_size : current_page * page_size
    ]
    # retuning final data using pagination library
    return paginator.get_paginated_response(
        new_found_user_profile_change_requests,
        len(serialized_data),
        current_page,
        page_size,
    )


class GetFoundUserProfileChangeRequests(APIView):
    def get(self, request):
        return get_found_user_profile_change_requests(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


def approve_user_profile_change_request(request):
    user_profile_change_request_data = json.loads(request.body)

    # getting user data
    user_data = User.objects.get(id=user_profile_change_request_data["user_id"])

    # setting first_name
    first_name = user_profile_change_request_data["current_first_name"]
    if user_profile_change_request_data["new_first_name"] != "":
        first_name = user_profile_change_request_data["new_first_name"]

    # setting last_name
    last_name = user_profile_change_request_data["current_last_name"]
    if user_profile_change_request_data["new_last_name"] != "":
        last_name = user_profile_change_request_data["new_last_name"]

    # setting dob
    dob = user_profile_change_request_data["current_birth_date"]
    if user_profile_change_request_data["new_birth_date"] != None:
        dob = user_profile_change_request_data["new_birth_date"]

    # updating needed fields
    user_data.first_name = first_name
    user_data.last_name = last_name
    user_data.birth_date = dob
    user_data.save()

    # deleting respective user profile change request
    UserProfileChangeRequest.objects.get(
        id=user_profile_change_request_data["id"]
    ).delete()
    # making sure that there are no more requests associated to that user
    duplicate_user_profile_change_requests = UserProfileChangeRequest.objects.filter(
        user_id=user_profile_change_request_data["user_id"]
    )
    for duplicate_user_profile_change_request in duplicate_user_profile_change_requests:
        duplicate_user_profile_change_request.delete()

    return Response(status=status.HTTP_200_OK)


class ApproveUserProfileChangeRequest(APIView):
    def post(self, request):
        return approve_user_profile_change_request(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


def deny_user_profile_change_request(request):
    user_profile_change_request_data = json.loads(request.body)

    # getting respective user profile change request
    respective_user_profile_change_request = UserProfileChangeRequest.objects.get(
        id=user_profile_change_request_data["id"]
    )
    # updating reason_for_deny field
    respective_user_profile_change_request.reason_for_deny = (
        user_profile_change_request_data["reason_for_deny"]
    )
    # deleting request
    respective_user_profile_change_request.delete()
    # making sure that there are no more requests associated to that user
    duplicate_user_profile_change_requests = UserProfileChangeRequest.objects.filter(
        user_id=user_profile_change_request_data["user_id"]
    )
    for duplicate_user_profile_change_request in duplicate_user_profile_change_requests:
        duplicate_user_profile_change_request.delete()

    return Response(status=status.HTTP_200_OK)


class DenyUserProfileChangeRequest(APIView):
    def post(self, request):
        return deny_user_profile_change_request(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


class UpdateUserLastPasswordChangeTime(APIView):
    def post(self, request):
        return Response(update_last_password_change_time(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class ChangeUserPassword(APIView):
    def post(self, request):
        success, parameters = get_needed_parameters(
            ["new_password", "confirm_password", "username"], request
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return change_password(parameters)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


def change_password(parameters):
    if parameters["new_password"] != parameters["confirm_password"]:
        return Response(
            {"error": "passwords don't match"}, status=status.HTTP_400_BAD_REQUEST
        )

    user = User.objects.filter(id=parameters["user_id"]).first()
    if not user:
        return Response(
            {"error": "user doesn't exist"}, status=status.HTTP_400_BAD_REQUEST
        )

    user.set_password(parameters["new_password"])
    user.save()
    return Response(status=status.HTTP_200_OK)


# check if a specified user account (active) exsits
def email_exists(parameters):
    # getting user
    # decoding email that might contain special characters
    user = User.objects.filter(
        email=urllib.parse.unquote(parameters["email"]), is_active=1
    )
    # if it does not exist
    if not user:
        # return false
        return Response(False, status=status.HTTP_200_OK)
    # if it exists
    else:
        # return true
        return Response(True, status=status.HTTP_200_OK)


class DoesThisEmailExist(APIView):
    def get(self, request):
        success, parameters = get_needed_parameters(["email"], request)
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return email_exists(parameters)

    def get_permissions(self):
        return [permissions.IsAuthenticatedOrReadOnly()]


# check if a specified user account has an active reset password session
def has_active_password_reset_session(parameters):
    # getting password reset row
    password_reset_rorw = ResetPasswordToken.objects.filter(
        key=parameters["password_reset_token"]
    )
    # if it does not exist
    if not password_reset_rorw:
        # return false
        return Response(False, status=status.HTTP_200_OK)
    # if it exists
    else:
        # return true
        return Response(True, status=status.HTTP_200_OK)


class GetSelectedUserPersonalInfo(APIView):
    def get(self, request):
        return get_selected_user_personal_info(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


def get_selected_user_personal_info(request):
    user_id = request.query_params.get("user_id", None)

    # getting user
    user = User.objects.filter(id=user_id)
    # returning data
    data = UserProfileSerializer(user, many=True)
    return Response(data.data)
