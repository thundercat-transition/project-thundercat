from django.db import models
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_models import User


class UserAccommodationsProfile(models.Model):
    user = models.OneToOneField(
        User,
        #  Delete entry when user is deleted
        on_delete=models.CASCADE,
    )
    has_built_in_accessibility = models.BooleanField(default=False)
    needs_extra_time = models.BooleanField(default=False)
    needs_breaks = models.BooleanField(default=False)
    needs_adaptive_tech = models.BooleanField(default=False)
    needs_ergonomic = models.BooleanField(default=False)
    needs_access_assistance = models.BooleanField(default=False)
    needs_environment = models.BooleanField(default=False)
    needs_scheduling_adaptation = models.BooleanField(default=False)
    has_other_needs_written = models.BooleanField(default=False)
    has_other_needs_oral = models.BooleanField(default=False)
    needs_repeated_oral_questions = models.BooleanField(default=False)
    previously_accommodated = models.BooleanField(default=False)
    modify_date = models.DateTimeField(auto_now=True)
    created_date = models.DateField(auto_now_add=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "(user_id: {0}) has_built_in_accessibility: {1}, needs_extra_time: {2}, needs_breaks: {3}, needs_adaptive_tech: {4}, needs_ergonomic: {5}, needs_access_assistance: {6}, needs_environment: {7}, needs_scheduling_adaptation: {8}, has_other_needs_written: {9}, has_other_needs_oral: {10}, needs_repeated_oral_questions: {11}, previously_accommodated: {12}, modify_date: {13}, created_date: {14}".format(
            self.user.id,
            self.has_built_in_accessibility,
            self.needs_extra_time,
            self.needs_breaks,
            self.needs_adaptive_tech,
            self.needs_ergonomic,
            self.needs_access_assistance,
            self.needs_environment,
            self.needs_scheduling_adaptation,
            self.has_other_needs_written,
            self.has_other_needs_oral,
            self.needs_repeated_oral_questions,
            self.previously_accommodated,
            self.modify_date,
            self.created_date,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "User Accommodations Profile"
