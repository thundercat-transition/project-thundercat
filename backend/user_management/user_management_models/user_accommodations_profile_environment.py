from django.db import models
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_accommodations_profile import (
    UserAccommodationsProfile,
)


class UserAccommodationsProfileEnvironment(models.Model):
    user_accommodations_profile = models.OneToOneField(
        UserAccommodationsProfile,
        # Delete entry if the user_accommodations_profile has been deleted
        on_delete=models.CASCADE,
    )
    description = models.TextField(blank=False, null=False)
    modify_date = models.DateTimeField(auto_now=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "user_accommodations_profile_id: {0}, description: {1}, modify_date: {2}".format(
            self.user_accommodations_profile.id,
            self.description,
            self.modify_date,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "User Accommodations Profile Environment"
