from django.db import connection
from django.db import migrations
from django.conf import settings


def update_data(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """
				-- ====================================================================
				--                        USER MANAGEMENT MODELS
				-- ====================================================================
				-- ==================== CUSTOM USER PERMISSION ====================
				UPDATE {db_name}..user_management_models_customuserpermissions
				SET username_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = user_id)

				UPDATE {db_name}..user_management_models_historicalcustomuserpermissions
				SET username_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = user_id)
				-- ==================== CUSTOM USER PERMISSION (END) ====================

				-- ==================== PERMISSION REQUEST ====================
				UPDATE {db_name}..user_management_models_permissionrequest
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)

				UPDATE {db_name}..user_management_models_historicalpermissionrequest
				SET user_id  = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)
				-- ==================== PERMISSION REQUEST (END) ====================

				-- ==================== ITEM OPTION DRAFTS ====================
				UPDATE {db_name}..user_management_models_taextendedprofile
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)

				UPDATE {db_name}..user_management_models_historicaltaextendedprofile
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)
				-- ==================== ITEM OPTION DRAFTS (END) ====================
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """
				-- ====================================================================
				--                        USER MANAGEMENT MODELS
				-- ====================================================================
				-- ==================== CUSTOM USER PERMISSION ====================
				UPDATE {db_name}..user_management_models_customuserpermissions
				SET username_id = NULL

				UPDATE {db_name}..user_management_models_historicalcustomuserpermissions
				SET username_id = NULL
				-- ==================== CUSTOM USER PERMISSION (END) ====================

				-- ==================== PERMISSION REQUEST ====================
				UPDATE {db_name}..user_management_models_permissionrequest
				SET user_id = NULL

				UPDATE {db_name}..user_management_models_historicalpermissionrequest
				SET user_id  = NULL
				-- ==================== PERMISSION REQUEST (END) ====================

				-- ==================== ITEM OPTION DRAFTS ====================
				UPDATE {db_name}..user_management_models_taextendedprofile
				SET user_id = NULL

				UPDATE {db_name}..user_management_models_historicaltaextendedprofile
				SET user_id = NULL
				-- ==================== ITEM OPTION DRAFTS (END) ====================
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "user_management_models",
            "0065_customuserpermissions_username_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_data, rollback_changes)]
