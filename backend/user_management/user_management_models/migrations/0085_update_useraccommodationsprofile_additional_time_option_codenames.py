# Created by Michael Cherry to set the initial 2fa settings to 0

from django.db import migrations

from backend.static.languages import Language_id

# text translations
EXTRA_TIME_33_TEXT_OLD_EN = "33% extra time"
EXTRA_TIME_33_TEXT_OLD_FR = "FR 33% extra time"
EXTRA_TIME_33_TEXT_NEW_EN = "33% more time"
EXTRA_TIME_33_TEXT_NEW_FR = "33% de temps additionnel"

EXTRA_TIME_50_TEXT_OLD_EN = "50% extra time"
EXTRA_TIME_50_TEXT_OLD_FR = "FR 50% extra time"
EXTRA_TIME_50_TEXT_NEW_EN = "50% more time"
EXTRA_TIME_50_TEXT_NEW_FR = "50% de temps additionnel"

EXTRA_TIME_OTHER_TEXT_OLD_EN = "Other"
EXTRA_TIME_OTHER_TEXT_OLD_FR = "FR Other"
EXTRA_TIME_OTHER_TEXT_NEW_EN = "Other"
EXTRA_TIME_OTHER_TEXT_NEW_FR = "Autre"


def create_useraccommodationsprofile_additional_time_option_codenames(
    apps, schema_editor
):

    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # UserAccommodationsProfileAdditionalTimeOption
    user_accommodations_profile_additional_time_option = apps.get_model(
        "user_management_models", "UserAccommodationsProfileAdditionalTimeOption"
    )

    # UserAccommodationsProfileAdditionalTimeOptionText
    user_accommodations_profile_additional_time_option_text = apps.get_model(
        "user_management_models", "UserAccommodationsProfileAdditionalTimeOptionText"
    )

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # update extra 50
    option = user_accommodations_profile_additional_time_option.objects.using(
        db_alias
    ).get(codename="extra_time_50")
    option_text_fr = (
        user_accommodations_profile_additional_time_option_text.objects.using(
            db_alias
        ).get(
            additional_time_option_id=option.id,
            language_id=Language_id.FR,
        )
    )
    option_text_fr.text = EXTRA_TIME_50_TEXT_NEW_FR
    option_text_fr.save()
    option_text_en = (
        user_accommodations_profile_additional_time_option_text.objects.using(
            db_alias
        ).get(
            additional_time_option_id=option.id,
            language_id=Language_id.EN,
        )
    )
    option_text_en.text = EXTRA_TIME_50_TEXT_NEW_EN
    option_text_en.save()


def rollback_updates(apps, schema_editor):
    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # UserAccommodationsProfileAdditionalTimeOption
    user_accommodations_profile_additional_time_option = apps.get_model(
        "user_management_models", "UserAccommodationsProfileAdditionalTimeOption"
    )

    # UserAccommodationsProfileAdditionalTimeOptionText
    user_accommodations_profile_additional_time_option_text = apps.get_model(
        "user_management_models", "UserAccommodationsProfileAdditionalTimeOptionText"
    )

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # update extra 50
    option = user_accommodations_profile_additional_time_option.objects.using(
        db_alias
    ).get(codename="extra_time_50")
    option_text_fr = (
        user_accommodations_profile_additional_time_option_text.objects.using(
            db_alias
        ).get(
            additional_time_option_id=option.id,
            language_id=Language_id.FR,
        )
    )
    option_text_fr.text = EXTRA_TIME_50_TEXT_OLD_FR
    option_text_fr.save()
    option_text_en = (
        user_accommodations_profile_additional_time_option_text.objects.using(
            db_alias
        ).get(
            additional_time_option_id=option.id,
            language_id=Language_id.EN,
        )
    )
    option_text_en.text = EXTRA_TIME_33_TEXT_OLD_EN
    option_text_en.save()


class Migration(migrations.Migration):
    dependencies = [
        (
            "user_management_models",
            "0084_update_useraccommodationsprofile_break_time_option_codenames",
        )
    ]

    operations = [
        migrations.RunPython(
            create_useraccommodationsprofile_additional_time_option_codenames,
            rollback_updates,
        )
    ]
