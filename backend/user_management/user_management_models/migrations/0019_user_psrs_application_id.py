# Generated by Django 2.2.3 on 2020-11-09 13:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_management_models', '0018_accommodations_spacing'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='psrs_applicant_id',
            field=models.CharField(blank=True, max_length=8, null=True),
        ),
    ]
