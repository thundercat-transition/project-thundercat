# Created by Michael Cherry to set the initial 2fa settings to 0

from django.db import migrations

from backend.static.languages import Languages, Language_id

# text translations
EXTRA_5_PER_30_MIN_TEXT_EN_OLD = "5 minutes per 30 minutes of assessment"
EXTRA_5_PER_30_MIN_TEXT_FR_OLD = "FR 5 minutes per 30 minutes of assessment"
EXTRA_5_PER_30_MIN_TEXT_EN_NEW = "5 minutes per 30 minutes of assessment"
EXTRA_5_PER_30_MIN_TEXT_FR_NEW = "5 minutes par tranche de 30 minutes d'évaluation"

EXTRA_10_PER_30_MIN_TEXT_EN_OLD = "10 minutes per 30 minutes of assessment"
EXTRA_10_PER_30_MIN_TEXT_FR_OLD = "FR 10 minutes per 30 minutes of assessment"
EXTRA_10_PER_30_MIN_TEXT_EN_NEW = "10 minutes per 30 minutes of assessment"
EXTRA_10_PER_30_MIN_TEXT_FR_NEW = "10 minutes par tranche de 30 minutes d'évaluation"


EXTRA_15_PER_30_MIN_TEXT_EN_OLD = "15 minutes per 30 minutes of assessment"
EXTRA_15_PER_30_MIN_TEXT_FR_OLD = "FR 15 minutes per 30 minutes of assessment"
EXTRA_15_PER_30_MIN_TEXT_EN_NEW = "15 minutes per 30 minutes of assessment"
EXTRA_15_PER_30_MIN_TEXT_FR_NEW = "15 minutes par tranche de 30 minutes d'évaluation"


EXTRA_TIME_OTHER_TEXT_EN_OLD = "Other"
EXTRA_TIME_OTHER_TEXT_FR_OLD = "FR Other"
EXTRA_TIME_OTHER_TEXT_EN_NEW = "Other"
EXTRA_TIME_OTHER_TEXT_FR_NEW = "Autre"


def create_useraccommodationsprofile_break_time_option_codenames(apps, schema_editor):
    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # UserAccommodationsProfileBreakTimeOption
    user_accommodations_profile_break_time_option = apps.get_model(
        "user_management_models", "UserAccommodationsProfileBreakTimeOption"
    )

    # UserAccommodationsProfileBreakTimeOptionText
    user_accommodations_profile_break_time_option_text = apps.get_model(
        "user_management_models", "UserAccommodationsProfileBreakTimeOptionText"
    )

    # Language
    language = apps.get_model("custom_models", "language")

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # --------------------------------------------------------------------------------- #
    # Language Data - Getting the 2 Official Languages
    # --------------------------------------------------------------------------------- #
    # 1. English
    language_en = language.objects.using(db_alias).get(ISO_Code_1=Languages.EN)

    # 2. French
    language_fr = language.objects.using(db_alias).get(ISO_Code_1=Languages.FR)

    # update extra 5
    option = user_accommodations_profile_break_time_option.objects.using(db_alias).get(
        codename="extra_5_per_30_min"
    )
    option_text_fr = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.FR,
    )
    option_text_fr.text = EXTRA_5_PER_30_MIN_TEXT_FR_NEW
    option_text_fr.save()
    option_text_en = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.EN,
    )
    option_text_en.text = EXTRA_5_PER_30_MIN_TEXT_EN_NEW
    option_text_en.save()

    # update extra 10
    option = user_accommodations_profile_break_time_option.objects.using(db_alias).get(
        codename="extra_10_per_30_min"
    )
    option_text_fr = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.FR,
    )
    option_text_fr.text = EXTRA_10_PER_30_MIN_TEXT_FR_NEW
    option_text_fr.save()
    option_text_en = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.EN,
    )
    option_text_en.text = EXTRA_10_PER_30_MIN_TEXT_EN_NEW
    option_text_en.save()

    # update extra 15
    option = user_accommodations_profile_break_time_option.objects.using(db_alias).get(
        codename="extra_15_per_30_min"
    )
    option_text_fr = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.FR,
    )
    option_text_fr.text = EXTRA_15_PER_30_MIN_TEXT_FR_NEW
    option_text_fr.save()
    option_text_en = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.EN,
    )
    option_text_en.text = EXTRA_15_PER_30_MIN_TEXT_EN_NEW
    option_text_en.save()

    # update other
    option = user_accommodations_profile_break_time_option.objects.using(db_alias).get(
        codename="other"
    )
    option_text_fr = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.FR,
    )
    option_text_fr.text = EXTRA_TIME_OTHER_TEXT_FR_NEW
    option_text_fr.save()
    option_text_en = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.EN,
    )
    option_text_en.text = EXTRA_TIME_OTHER_TEXT_EN_NEW
    option_text_en.save()


def rollback_updates(apps, schema_editor):
    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # UserAccommodationsProfileBreakTimeOption
    user_accommodations_profile_break_time_option = apps.get_model(
        "user_management_models", "UserAccommodationsProfileBreakTimeOption"
    )

    # UserAccommodationsProfileBreakTimeOptionText
    user_accommodations_profile_break_time_option_text = apps.get_model(
        "user_management_models", "UserAccommodationsProfileBreakTimeOptionText"
    )

    # Language
    language = apps.get_model("custom_models", "language")

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # --------------------------------------------------------------------------------- #
    # Language Data - Getting the 2 Official Languages
    # --------------------------------------------------------------------------------- #
    # 1. English
    language_en = language.objects.using(db_alias).get(ISO_Code_1=Languages.EN)

    # 2. French
    language_fr = language.objects.using(db_alias).get(ISO_Code_1=Languages.FR)

    # update extra 5
    option = user_accommodations_profile_break_time_option.objects.using(db_alias).get(
        codename="extra_5_per_30_min"
    )
    option_text_fr = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.FR,
    )
    option_text_fr.text = EXTRA_5_PER_30_MIN_TEXT_FR_OLD
    option_text_fr.save()
    option_text_en = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.EN,
    )
    option_text_en.text = EXTRA_5_PER_30_MIN_TEXT_EN_OLD
    option_text_en.save()

    # update extra 10
    option = user_accommodations_profile_break_time_option.objects.using(db_alias).get(
        codename="extra_10_per_30_min"
    )
    option_text_fr = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.FR,
    )
    option_text_fr.text = EXTRA_10_PER_30_MIN_TEXT_FR_OLD
    option_text_fr.save()
    option_text_en = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.EN,
    )
    option_text_en.text = EXTRA_10_PER_30_MIN_TEXT_EN_OLD
    option_text_en.save()

    # update extra 15
    option = user_accommodations_profile_break_time_option.objects.using(db_alias).get(
        codename="extra_15_per_30_min"
    )
    option_text_fr = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.FR,
    )
    option_text_fr.text = EXTRA_15_PER_30_MIN_TEXT_FR_OLD
    option_text_fr.save()
    option_text_en = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.EN,
    )
    option_text_en.text = EXTRA_15_PER_30_MIN_TEXT_EN_OLD
    option_text_en.save()

    # update other
    option = user_accommodations_profile_break_time_option.objects.using(db_alias).get(
        codename="other"
    )
    option_text_fr = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.FR,
    )
    option_text_fr.text = EXTRA_TIME_OTHER_TEXT_FR_OLD
    option_text_fr.save()
    option_text_en = user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).get(
        break_time_option_id=option.id,
        language_id=Language_id.EN,
    )
    option_text_en.text = EXTRA_TIME_OTHER_TEXT_EN_OLD
    option_text_en.save()


class Migration(migrations.Migration):
    dependencies = [
        (
            "user_management_models",
            "0083_update_useraccommodationsprofile_additional_time_option_codenames",
        )
    ]

    operations = [
        migrations.RunPython(
            create_useraccommodationsprofile_break_time_option_codenames,
            rollback_updates,
        )
    ]
