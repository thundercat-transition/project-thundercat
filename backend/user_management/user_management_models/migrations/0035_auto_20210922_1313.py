# Generated by Django 3.0.14 on 2021-09-22 17:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("user_management_models", "0034_auto_20210920_1048")]

    operations = [
        migrations.AlterModelOptions(name="user", options={}),
        migrations.AddIndex(
            model_name="user",
            index=models.Index(
                fields=["first_name", "last_name", "email", "birth_date"],
                name="user_manage_first_n_3d7ad5_idx",
            ),
        ),
    ]
