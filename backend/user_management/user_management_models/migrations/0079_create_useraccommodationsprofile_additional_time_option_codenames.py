# Created by Michael Cherry to set the initial 2fa settings to 0

from django.db import migrations

from backend.static.languages import Languages

# text translations
EXTRA_TIME_33_TEXT_EN = "33% extra time"
EXTRA_TIME_33_TEXT_FR = "FR 33% extra time"

EXTRA_TIME_50_TEXT_EN = "50% extra time"
EXTRA_TIME_50_TEXT_FR = "FR 50% extra time"

EXTRA_TIME_OTHER_TEXT_EN = "Other"
EXTRA_TIME_OTHER_TEXT_FR = "FR Other"


def create_useraccommodationsprofile_additional_time_option_codenames(
    apps, schema_editor
):
    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # UserAccommodationsProfileAdditionalTimeOption
    user_accommodations_profile_additional_time_option = apps.get_model(
        "user_management_models", "UserAccommodationsProfileAdditionalTimeOption"
    )

    # UserAccommodationsProfileAdditionalTimeOptionText
    user_accommodations_profile_additional_time_option_text = apps.get_model(
        "user_management_models", "UserAccommodationsProfileAdditionalTimeOptionText"
    )

    # Language
    language = apps.get_model("custom_models", "language")

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # --------------------------------------------------------------------------------- #
    # Language Data - Getting the 2 Official Languages
    # --------------------------------------------------------------------------------- #
    # 1. English
    language_en = language.objects.using(db_alias).get(ISO_Code_1=Languages.EN)

    # 2. French
    language_fr = language.objects.using(db_alias).get(ISO_Code_1=Languages.FR)

    # --------------------------------------------------------------------------------- #
    # UserAccommodationsProfileAdditionalTimeOption Data - Adding Codenames
    # --------------------------------------------------------------------------------- #
    # 33 Extra Time
    extra_time_33 = user_accommodations_profile_additional_time_option(
        codename="extra_time_33"
    )
    extra_time_33.save()

    # 50 Extra Time
    extra_time_50 = user_accommodations_profile_additional_time_option(
        codename="extra_time_50"
    )
    extra_time_50.save()

    # Other
    extra_time_other = user_accommodations_profile_additional_time_option(
        codename="other"
    )
    extra_time_other.save()

    # --------------------------------------------------------------------------------- #
    # UserAccommodationsProfileAdditionalTimeOptionText Data - Adding Translations
    # --------------------------------------------------------------------------------- #

    # EXTRA TIME 33
    extra_time_33_text_en = user_accommodations_profile_additional_time_option_text(
        additional_time_option=extra_time_33,
        language=language_en,
        text=EXTRA_TIME_33_TEXT_EN,
    )
    extra_time_33_text_en.save()

    extra_time_33_text_fr = user_accommodations_profile_additional_time_option_text(
        additional_time_option=extra_time_33,
        language=language_fr,
        text=EXTRA_TIME_33_TEXT_FR,
    )
    extra_time_33_text_fr.save()

    # EXTRA TIME 50
    extra_time_50_text_en = user_accommodations_profile_additional_time_option_text(
        additional_time_option=extra_time_50,
        language=language_en,
        text=EXTRA_TIME_50_TEXT_EN,
    )
    extra_time_50_text_en.save()

    extra_time_50_text_fr = user_accommodations_profile_additional_time_option_text(
        additional_time_option=extra_time_50,
        language=language_fr,
        text=EXTRA_TIME_50_TEXT_FR,
    )
    extra_time_50_text_fr.save()

    # EXTRA TIME OTHER
    extra_time_other_text_en = user_accommodations_profile_additional_time_option_text(
        additional_time_option=extra_time_other,
        language=language_en,
        text=EXTRA_TIME_OTHER_TEXT_EN,
    )
    extra_time_other_text_en.save()

    extra_time_other_text_fr = user_accommodations_profile_additional_time_option_text(
        additional_time_option=extra_time_other,
        language=language_fr,
        text=EXTRA_TIME_OTHER_TEXT_FR,
    )
    extra_time_other_text_fr.save()


def rollback_updates(apps, schema_editor):
    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # UserAccommodationsProfileAdditionalTimeOption
    user_accommodations_profile_additional_time_option = apps.get_model(
        "user_management_models", "UserAccommodationsProfileAdditionalTimeOption"
    )

    # UserAccommodationsProfileAdditionalTimeOptionText
    user_accommodations_profile_additional_time_option_text = apps.get_model(
        "user_management_models", "UserAccommodationsProfileAdditionalTimeOptionText"
    )

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # --------------------------------------------------------------------------------- #
    # UserAccommodationsProfileAdditionalTimeOptionText Data - Deleting Translations
    # --------------------------------------------------------------------------------- #
    user_accommodations_profile_additional_time_option_text.objects.using(
        db_alias
    ).all().delete()

    # --------------------------------------------------------------------------------- #
    # UserAccommodationsProfileAdditionalTimeOption Data - Deleting Codenames
    # --------------------------------------------------------------------------------- #
    user_accommodations_profile_additional_time_option.objects.using(
        db_alias
    ).all().delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "user_management_models",
            "0078_useraccommodationsprofile_and_more",
        )
    ]

    operations = [
        migrations.RunPython(
            create_useraccommodationsprofile_additional_time_option_codenames,
            rollback_updates,
        )
    ]
