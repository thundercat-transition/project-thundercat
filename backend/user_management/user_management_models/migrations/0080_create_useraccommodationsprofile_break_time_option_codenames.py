# Created by Michael Cherry to set the initial 2fa settings to 0

from django.db import migrations

from backend.static.languages import Languages

# text translations
EXTRA_5_PER_30_MIN_TEXT_EN = "5 minutes per 30 minutes of assessment"
EXTRA_5_PER_30_MIN_TEXT_FR = "FR 5 minutes per 30 minutes of assessment"

EXTRA_10_PER_30_MIN_TEXT_EN = "10 minutes per 30 minutes of assessment"
EXTRA_10_PER_30_MIN_TEXT_FR = "FR 10 minutes per 30 minutes of assessment"

EXTRA_15_PER_30_MIN_TEXT_EN = "15 minutes per 30 minutes of assessment"
EXTRA_15_PER_30_MIN_TEXT_FR = "FR 15 minutes per 30 minutes of assessment"

EXTRA_TIME_OTHER_TEXT_EN = "Other"
EXTRA_TIME_OTHER_TEXT_FR = "FR Other"


def create_useraccommodationsprofile_break_time_option_codenames(apps, schema_editor):
    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # UserAccommodationsProfileBreakTimeOption
    user_accommodations_profile_break_time_option = apps.get_model(
        "user_management_models", "UserAccommodationsProfileBreakTimeOption"
    )

    # UserAccommodationsProfileBreakTimeOptionText
    user_accommodations_profile_break_time_option_text = apps.get_model(
        "user_management_models", "UserAccommodationsProfileBreakTimeOptionText"
    )

    # Language
    language = apps.get_model("custom_models", "language")

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # --------------------------------------------------------------------------------- #
    # Language Data - Getting the 2 Official Languages
    # --------------------------------------------------------------------------------- #
    # 1. English
    language_en = language.objects.using(db_alias).get(ISO_Code_1=Languages.EN)

    # 2. French
    language_fr = language.objects.using(db_alias).get(ISO_Code_1=Languages.FR)

    # --------------------------------------------------------------------------------- #
    # UserAccommodationsProfileBreakTimeOption Data - Adding Codenames
    # --------------------------------------------------------------------------------- #
    # Extra 5 min per 30 min
    extra_5_per_30_min = user_accommodations_profile_break_time_option(
        codename="extra_5_per_30_min"
    )
    extra_5_per_30_min.save()

    # Extra 10 min per 30 min
    extra_10_per_30_min = user_accommodations_profile_break_time_option(
        codename="extra_10_per_30_min"
    )
    extra_10_per_30_min.save()

    # Extra 15 min per 30 min
    extra_15_per_30_min = user_accommodations_profile_break_time_option(
        codename="extra_15_per_30_min"
    )
    extra_15_per_30_min.save()

    # Other
    extra_time_other = user_accommodations_profile_break_time_option(codename="other")
    extra_time_other.save()

    # --------------------------------------------------------------------------------- #
    # UserAccommodationsProfileBreakTimeOptionText Data - Adding Translations
    # --------------------------------------------------------------------------------- #

    # Extra 5 min per 30 min
    extra_5_per_30_min_text_en = user_accommodations_profile_break_time_option_text(
        break_time_option=extra_5_per_30_min,
        language=language_en,
        text=EXTRA_5_PER_30_MIN_TEXT_EN,
    )
    extra_5_per_30_min_text_en.save()

    extra_5_per_30_min_text_fr = user_accommodations_profile_break_time_option_text(
        break_time_option=extra_5_per_30_min,
        language=language_fr,
        text=EXTRA_5_PER_30_MIN_TEXT_FR,
    )
    extra_5_per_30_min_text_fr.save()

    # Extra 10 min per 30 min
    extra_10_per_30_min_text_en = user_accommodations_profile_break_time_option_text(
        break_time_option=extra_10_per_30_min,
        language=language_en,
        text=EXTRA_10_PER_30_MIN_TEXT_EN,
    )
    extra_10_per_30_min_text_en.save()

    extra_10_per_30_min_text_fr = user_accommodations_profile_break_time_option_text(
        break_time_option=extra_10_per_30_min,
        language=language_fr,
        text=EXTRA_10_PER_30_MIN_TEXT_FR,
    )
    extra_10_per_30_min_text_fr.save()

    # Extra 15 min per 30 min
    extra_15_per_30_min_text_en = user_accommodations_profile_break_time_option_text(
        break_time_option=extra_15_per_30_min,
        language=language_en,
        text=EXTRA_15_PER_30_MIN_TEXT_EN,
    )
    extra_15_per_30_min_text_en.save()

    extra_15_per_30_min_text_fr = user_accommodations_profile_break_time_option_text(
        break_time_option=extra_15_per_30_min,
        language=language_fr,
        text=EXTRA_15_PER_30_MIN_TEXT_FR,
    )
    extra_15_per_30_min_text_fr.save()

    # Extra Other
    extra_time_other_text_en = user_accommodations_profile_break_time_option_text(
        break_time_option=extra_time_other,
        language=language_en,
        text=EXTRA_TIME_OTHER_TEXT_EN,
    )
    extra_time_other_text_en.save()

    extra_time_other_text_fr = user_accommodations_profile_break_time_option_text(
        break_time_option=extra_time_other,
        language=language_fr,
        text=EXTRA_TIME_OTHER_TEXT_FR,
    )
    extra_time_other_text_fr.save()


def rollback_updates(apps, schema_editor):
    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # UserAccommodationsProfileBreakTimeOption
    user_accommodations_profile_break_time_option = apps.get_model(
        "user_management_models", "UserAccommodationsProfileBreakTimeOption"
    )

    # UserAccommodationsProfileBreakTimeOptionText
    user_accommodations_profile_break_time_option_text = apps.get_model(
        "user_management_models", "UserAccommodationsProfileBreakTimeOptionText"
    )

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # --------------------------------------------------------------------------------- #
    # UserAccommodationsProfileBreakTimeOptionText Data - Deleting Translations
    # --------------------------------------------------------------------------------- #
    user_accommodations_profile_break_time_option_text.objects.using(
        db_alias
    ).all().delete()

    # --------------------------------------------------------------------------------- #
    # UserAccommodationsProfileBreakTimeOption Data - Deleting Codenames
    # --------------------------------------------------------------------------------- #
    user_accommodations_profile_break_time_option.objects.using(db_alias).all().delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "user_management_models",
            "0079_create_useraccommodationsprofile_additional_time_option_codenames",
        )
    ]

    operations = [
        migrations.RunPython(
            create_useraccommodationsprofile_break_time_option_codenames,
            rollback_updates,
        )
    ]
