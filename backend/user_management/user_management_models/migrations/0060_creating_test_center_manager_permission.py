from django.db import migrations


def create_new_permissions(apps, schema_editor):
    # get models
    custom_permissions = apps.get_model("user_management_models", "CustomPermissions")
    content_type = apps.get_model("contenttypes", "ContentType")
    # get db alias
    db_alias = schema_editor.connection.alias

    try:
        # get custom permissions content type model
        custom_permissions_content_type_id = (
            content_type.objects.using(db_alias)
            .filter(model="custompermissions")
            .last()
            .id
        )

        # creating new permission
        new_permission = custom_permissions(
            en_name="Test Center Manager",
            fr_name="FR Test Center Manager",
            en_description="Test Center Manager Description...",
            fr_description="FR Test Center Manager Description...",
            codename="is_tcm",
            content_type_id=custom_permissions_content_type_id,
        )
        new_permission.save()

    # should only happen while testing (backend tests)
    except Exception as e:
        print("Exception (should only be displayed while testing): ", e)


def rollback_changes(apps, schema_editor):
    # get models
    custom_permissions = apps.get_model("user_management_models", "CustomPermissions")
    # get db alias
    db_alias = schema_editor.connection.alias

    # get permission object
    permission = (
        custom_permissions.objects.using(db_alias).filter(codename="is_tcm").last()
    )
    if permission:
        permission.delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "user_management_models",
            "0059_creating_hr_coordinator_permission",
        )
    ]

    operations = [migrations.RunPython(create_new_permissions, rollback_changes)]
