# Created by Michael Cherry to set the initial 2fa settings to 0

from django.db import migrations


def create_inactive_2fa(apps, schema_editor):
    # get models
    custom_permissions = apps.get_model("user_management_models", "custompermissions")
    custom_permission_2fa = apps.get_model(
        "user_management_models", "custompermission2fa"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    # get all permission ids
    permission_ids = custom_permissions.objects.using(db_alias).values_list(
        "permission_id", flat=True
    )

    for p_id in permission_ids:
        permission_2fa = custom_permission_2fa(
            custom_permission_id=p_id, is_2fa_active=0
        )
        permission_2fa.save()


def rollback_updates(apps, schema_editor):
    # get models
    custom_permission_2fa = apps.get_model(
        "user_management_models", "custompermission2fa"
    )
    # get db alias
    db_alias = schema_editor.connection.alias

    custom_permission_2fa.objects.using(db_alias).all().delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "user_management_models",
            "0074_alter_historicaluser_email_alter_user_email",
        )
    ]

    operations = [migrations.RunPython(create_inactive_2fa, rollback_updates)]
