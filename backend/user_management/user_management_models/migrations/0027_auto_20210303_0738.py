# Generated by Django 2.2.4 on 2021-03-03 12:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("user_management_models", "0026_auto_20210126_1255")]

    operations = [
        migrations.AlterField(
            model_name="custompermissions",
            name="en_description",
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name="custompermissions",
            name="fr_description",
            field=models.TextField(),
        ),
    ]
