# Generated by Django 2.2.4 on 2021-03-29 15:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user_management_models', '0029_update_permission_names_and_descriptions'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userextendedprofile',
            name='minority_id',
        ),
        migrations.CreateModel(
            name='UserExtendedProfileMinority',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('minority_id', models.IntegerField()),
                ('user_extended_profile', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='user_management_models.UserExtendedProfile')),
            ],
            options={
                'verbose_name_plural': 'User Extended Profile (Minority)',
            },
        ),
    ]
