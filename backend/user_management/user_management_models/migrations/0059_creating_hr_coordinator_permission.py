from django.db import migrations


def create_hr_coordinator_permissions(apps, schema_editor):
    # get models
    custom_permissions = apps.get_model("user_management_models", "CustomPermissions")
    content_type = apps.get_model("contenttypes", "ContentType")
    # get db alias
    db_alias = schema_editor.connection.alias

    try:
        # get custom permissions content type model
        custom_permissions_content_type_id = (
            content_type.objects.using(db_alias)
            .filter(model="custompermissions")
            .last()
            .id
        )

        # creating TA permission
        accommodations_specialized_permission = custom_permissions(
            en_name="HR Coordinator",
            fr_name="Coordinateur en RH",
            en_description="Responsible for sending in-person (supervised) invite requests to candidates.",
            fr_description="FR Responsible for sending in-person (supervised) invite requests to candidates.",
            codename="is_hr_coordinator",
            content_type_id=custom_permissions_content_type_id,
        )
        accommodations_specialized_permission.save()

    # should only happen while testing (backend tests)
    except Exception as e:
        print("Exception (should only be displayed while testing): ", e)


def rollback_changes(apps, schema_editor):
    # get models
    custom_permissions = apps.get_model("user_management_models", "CustomPermissions")
    # get db alias
    db_alias = schema_editor.connection.alias

    # get TA permission object
    accommodations_specialized_permission = (
        custom_permissions.objects.using(db_alias)
        .filter(codename="is_hr_coordinator")
        .last()
    )
    if accommodations_specialized_permission:
        accommodations_specialized_permission.delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "user_management_models",
            "0058_customuserpermissions_pri_and_military_nbr_at_least_1_is_not_empty_in_user_permissions",
        )
    ]

    operations = [
        migrations.RunPython(create_hr_coordinator_permissions, rollback_changes)
    ]
