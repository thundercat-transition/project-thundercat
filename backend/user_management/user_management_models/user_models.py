from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from django.db.models import Q
from simple_history.models import HistoricalRecords

##################################################################################
# USER MODELS
##################################################################################

# number of login attempts allowed
NUM_OF_ATTEMPTS = 5


class LOCK_OUT_TIME:
    hours = 4
    minutes = 0


class ACCOUNT_CREATION_ERRORS:
    USERNAME_ALREADY_EXISTS = "A user with that username already exists."
    EMAIL_ALREADY_EXISTS = "user with this email already exists."
    DUPLICATED_EMAIL_OR_USERNAME = "You probably already have another account."


# customize user manager by allowing the user to log in by email or username
class CustomUserManager(UserManager):
    # Login with either Username or Email
    def get_by_natural_key(self, username):
        return self.get(
            Q(**{self.model.USERNAME_FIELD: username})
            | Q(**{self.model.EMAIL_FIELD: username})
        )

    # Account Creation Validation
    def create_user(self, username, email=None, password=None, **extra_fields):
        # Verify if the username already exist
        existing_username = User.objects.filter(username=username)

        # Verify if the email already exist
        existing_email = User.objects.filter(email=email)

        # If the email/username are not already used
        if not existing_username and not existing_email:
            # Verify if:
            #   - Username has already been used as an Email
            existing_username_in_email = User.objects.filter(email=username)

            # Verify if:
            #   - Email has already been used as a Username
            existing_email_in_username = User.objects.filter(username=email)

            # If we don't have anything matching in the DB, create the new User
            if not existing_username_in_email and not existing_email_in_username:
                user = super().create_user(username, email, password, **extra_fields)

                return user

            else:
                # if only username exists as an email
                if existing_username_in_email and not existing_email_in_username:
                    return User(
                        username=ACCOUNT_CREATION_ERRORS.DUPLICATED_EMAIL_OR_USERNAME
                    )

                # if only email exists as a username
                elif not existing_username_in_email and existing_email_in_username:
                    return User(
                        email=ACCOUNT_CREATION_ERRORS.DUPLICATED_EMAIL_OR_USERNAME
                    )

                # if username exists as an email AND email exists as a username
                else:
                    return User(
                        username=ACCOUNT_CREATION_ERRORS.DUPLICATED_EMAIL_OR_USERNAME,
                        email=ACCOUNT_CREATION_ERRORS.DUPLICATED_EMAIL_OR_USERNAME,
                    )
        # Case where username already exist
        elif existing_username and not existing_email:
            return User(username=[ACCOUNT_CREATION_ERRORS.USERNAME_ALREADY_EXISTS])

        # Case where email already exist
        elif not existing_username and existing_email:
            return User(email=[ACCOUNT_CREATION_ERRORS.EMAIL_ALREADY_EXISTS])

        # Case where username and email already exist
        elif existing_username and existing_email:
            return User(
                email=[ACCOUNT_CREATION_ERRORS.EMAIL_ALREADY_EXISTS],
                username=[ACCOUNT_CREATION_ERRORS.USERNAME_ALREADY_EXISTS],
            )


# ** IMPORTANT **
# If any changes are made to this model, please make sure to notify CAMM team (CAT/CAMM Relation Logic)
class User(AbstractUser):
    objects = CustomUserManager()
    email = models.EmailField(max_length=254, blank=False, null=False, unique=True)
    secondary_email = models.EmailField(max_length=254, blank=True, null=True)
    first_name = models.CharField(max_length=30, null=False, blank=False)
    last_name = models.CharField(max_length=150, null=False, blank=False)
    birth_date = models.DateField(blank=False, null=False)
    phone_number = models.CharField(max_length=10, null=False, blank=True, default="")
    pri = models.CharField(max_length=10, null=False, blank=True, default="")
    military_nbr = models.CharField(max_length=9, null=False, blank=True, default="")
    last_password_change = models.DateField(blank=True, null=True)
    psrs_applicant_id = models.CharField(max_length=8, null=True, blank=True)
    last_login = models.DateTimeField(blank=True, null=True)
    last_login_attempt = models.DateTimeField(blank=True, null=True)
    login_attempts = models.IntegerField(default=0)
    locked = models.BooleanField(default=False)
    locked_until = models.DateTimeField(blank=True, null=True)
    is_profile_complete = models.BooleanField(default=False)
    last_profile_update = models.DateTimeField(blank=True, null=True)
    REQUIRED_FIELDS = [
        "first_name",
        "last_name",
        "birth_date",
        "email",
        "secondary_email",
        "phone_number",
        "pri",
        "military_nbr",
        "last_password_change",
        "is_staff",
        "psrs_applicant_id",
        "last_login",
        "is_profile_complete",
    ]

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # Adding indexes to make search faster
        indexes = [
            models.Index(fields=["first_name", "last_name", "email", "birth_date"])
        ]
