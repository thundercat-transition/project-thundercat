from django.db import models
from django.db.models import Q
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_models import User
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)


##################################################################################
# CUSTOM USER PERMISSIONS MODEL
# what approved roles does a user have
##################################################################################


class CustomUserPermissions(models.Model):
    user_permission_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(
        User,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        related_name="username_id",
    )
    permission = models.ForeignKey(
        CustomPermissions,
        to_field="permission_id",
        on_delete=models.DO_NOTHING,
        null=False,
    )
    # same fields as in permission_request_model
    goc_email = models.CharField(
        max_length=254, default="default", blank=False, null=False
    )
    pri = models.CharField(max_length=10, default="", null=False, blank=True)
    military_nbr = models.CharField(max_length=9, default="", null=False, blank=True)
    supervisor = models.CharField(
        max_length=180, default="default", blank=False, null=False
    )
    supervisor_email = models.CharField(
        max_length=254, default="default", blank=False, null=False
    )
    rationale = models.CharField(
        max_length=300, default="default", blank=False, null=False
    )
    modify_date = models.DateTimeField(auto_now=True)
    reason_for_modif_or_del = models.CharField(
        max_length=300, default=None, null=True, blank=False
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "{0} ({1})".format(self.user, self.permission)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "User Permissions"
        constraints = [
            models.UniqueConstraint(
                name="one_user_id_per_permission_id_instance",
                fields=["user", "permission"],
            ),
            # contraint on getting at least 1 of the 2 fields (pri / military_nbr) with a value different than ""
            models.CheckConstraint(
                name="pri_and_military_nbr_at_least_1_is_not_empty_in_user_permissions",
                check=Q(pri__gt="", military_nbr__exact="")
                | Q(pri__exact="", military_nbr__gt="")
                | Q(pri__gt="", military_nbr__gt=""),
            ),
        ]
