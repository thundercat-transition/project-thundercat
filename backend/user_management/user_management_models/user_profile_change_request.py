from django.db import models
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_models import User


class UserProfileChangeRequest(models.Model):
    user = models.ForeignKey(User, to_field="id", on_delete=models.DO_NOTHING)
    current_first_name = models.CharField(max_length=30, null=False, blank=False)
    current_last_name = models.CharField(max_length=150, null=False, blank=False)
    current_birth_date = models.DateField(blank=False, null=False)
    new_first_name = models.CharField(max_length=30, null=True, blank=True)
    new_last_name = models.CharField(max_length=150, null=True, blank=True)
    new_birth_date = models.DateField(blank=True, null=True)
    modify_date = models.DateTimeField(auto_now=True)
    comments = models.TextField(blank=True, null=True)
    reason_for_deny = models.CharField(max_length=255, null=True, blank=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "User Profile Change Request"
