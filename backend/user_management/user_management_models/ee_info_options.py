from django.db import models
from simple_history.models import HistoricalRecords


class EEInfoOptions(models.Model):
    opt_id = models.IntegerField(primary_key=True)
    opt_value = models.CharField(
        max_length=25,
        null=False,
        blank=False,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "Option ID: {0} | Option Value: {1}".format(
            self.opt_id,
            self.opt_value,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "EE Info Options"
