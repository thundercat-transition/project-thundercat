from django.db import models
from django.db.models import Q
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_models import User
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)


##################################################################################
# CUSTOM PERMISSIONS MODEL
##################################################################################


class PermissionRequest(models.Model):
    permission_request_id = models.AutoField(primary_key=True)
    goc_email = models.CharField(max_length=254, blank=False, null=False)
    pri = models.CharField(max_length=10, null=False, blank=True, default="")
    military_nbr = models.CharField(max_length=9, null=False, blank=True, default="")
    supervisor = models.CharField(max_length=180, blank=False, null=False)
    supervisor_email = models.CharField(max_length=254, blank=False, null=False)
    rationale = models.CharField(max_length=300, blank=False, null=False)
    permission_requested = models.ForeignKey(
        CustomPermissions,
        to_field="permission_id",
        on_delete=models.DO_NOTHING,
        null=False,
    )
    request_date = models.DateTimeField(auto_now_add=True, blank=False, null=False)
    user = models.ForeignKey(
        User,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        related_name="user_id",
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical model
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Permission Request"
        constraints = [
            models.UniqueConstraint(
                name="one_user_id_per_permission_requested_instance",
                fields=["user", "permission_requested"],
            ),
            # contraint on getting at least 1 of the 2 fields (pri / military_nbr) with a value different than ""
            models.CheckConstraint(
                name="pri_and_military_nbr_at_least_1_is_not_empty_in_permission_request",
                check=Q(pri__gt="", military_nbr__exact="")
                | Q(pri__exact="", military_nbr__gt="")
                | Q(pri__gt="", military_nbr__gt=""),
            ),
        ]
