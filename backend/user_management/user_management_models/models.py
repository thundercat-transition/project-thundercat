from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.user_management_models.user_models import User
from user_management.user_management_models.user_extended_profile import (
    UserExtendedProfile,
)
from user_management.user_management_models.user_extended_profile_aboriginal import (
    UserExtendedProfileAboriginal,
)
from user_management.user_management_models.user_extended_profile_visible_minority import (
    UserExtendedProfileVisibleMinority,
)
from user_management.user_management_models.user_extended_profile_disability import (
    UserExtendedProfileDisability,
)
from user_management.user_management_models.permission_request_model import (
    PermissionRequest,
)
from user_management.user_management_models.accommodations import Accommodations
from user_management.views.password_reset import (
    password_reset_token_created,
    post_password_reset_actions,
)
from user_management.user_management_models.user_password_reset_tracking_model import (
    UserPasswordResetTracking,
)
from user_management.user_management_models.ta_extended_profile_models import (
    TaExtendedProfile,
)
from user_management.user_management_models.ee_info_options import (
    EEInfoOptions,
)
from user_management.user_management_models.user_profile_change_request import (
    UserProfileChangeRequest,
)

from user_management.user_management_models.custom_permission_2fa import (
    CustomPermission2FA,
)

from user_management.user_management_models.user_2fa_tracking_model import (
    User2FATracking,
)
from user_management.user_management_models.user_accommodations_profile import (
    UserAccommodationsProfile,
)
from user_management.user_management_models.user_accommodations_profile_built_in_accessbility import (
    UserAccommodationsProfileBuiltInAccessibility,
)
from user_management.user_management_models.user_accommodations_profile_extra_time import (
    UserAccommodationsProfileExtraTime,
)
from user_management.user_management_models.user_accommodations_profile_breaks import (
    UserAccommodationsProfileBreaks,
)
from user_management.user_management_models.user_accommodations_profile_access_assistance import (
    UserAccommodationsProfileAccessAssistance,
)
from user_management.user_management_models.user_accommodations_profile_adaptive_tech import (
    UserAccommodationsProfileAdaptiveTech,
)
from user_management.user_management_models.user_accommodations_profile_scheduling_adaptation import (
    UserAccommodationsProfileSchedulingAdaptation,
)
from user_management.user_management_models.user_accommodations_profile_environment import (
    UserAccommodationsProfileEnvironment,
)
from user_management.user_management_models.user_accommodations_profile_ergonomic import (
    UserAccommodationsProfileErgonomic,
)
from user_management.user_management_models.user_accommodations_profile_other_needs_written import (
    UserAccommodationsProfileOtherNeedsWritten,
)
from user_management.user_management_models.user_accommodations_profile_other_needs_oral import (
    UserAccommodationsProfileOtherNeedsOral,
)
from user_management.user_management_models.user_accommodations_profile_previously_accommodated import (
    UserAccommodationsProfilePreviouslyAccommodated,
)
from user_management.user_management_models.user_accommodations_profile_repeated_oral_questions import (
    UserAccommodationsProfileRepeatedOralQuestions,
)

# These imports help to auto discover the models
