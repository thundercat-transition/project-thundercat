from django.db import models
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_models import User


class User2FATracking(models.Model):
    id = models.AutoField(primary_key=True)
    two_factor_auth_code = models.CharField(max_length=6, unique=True)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    expiry_date = models.DateTimeField(blank=False, null=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "{0}".format(self.user.username)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "User 2 Factor Authentication Tracking"
