from django.contrib.auth.models import UserManager
from django.db import models
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_models import User

##################################################################################
# USER MODELS
##################################################################################


class TaExtendedProfile(models.Model):
    user = models.ForeignKey(
        User,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        related_name="tep_user",
    )
    department_id = models.IntegerField(null=False, blank=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords(use_base_model_db=False)

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "TA Extended Profile"
