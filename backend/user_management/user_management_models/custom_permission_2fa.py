from datetime import datetime
from django.utils import timezone
from django.db import models
from simple_history.models import HistoricalRecords
from user_management.user_management_models.user_models import User
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)


class CustomPermission2FA(models.Model):
    id = models.AutoField(primary_key=True)
    custom_permission = models.ForeignKey(
        CustomPermissions,
        to_field="permission_id",
        on_delete=models.DO_NOTHING,
        null=False,
    )
    is_2fa_active = models.BooleanField(default=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "User 2 Factor Authentication Tracking"
