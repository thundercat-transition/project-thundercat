from django.db import models


class TestCenterOlaTestAssessorsVw(models.Model):
    id = models.IntegerField(db_column="id", primary_key=True)
    test_center_id = models.IntegerField(db_column="test_center_id")
    user_id = models.IntegerField(db_column="user_id")
    user_first_name = models.CharField(db_column="user_first_name", max_length=30)
    user_last_name = models.CharField(db_column="user_last_name", max_length=150)
    user_email = models.EmailField(db_column="user_email", max_length=254)
    supervisor = models.BooleanField(db_column="supervisor")
    certified_language_ids = models.CharField(
        db_column="certified_language_ids", max_length=100
    )

    class Meta:
        managed = False
        db_table = "test_center_ola_test_assessors_vw"
