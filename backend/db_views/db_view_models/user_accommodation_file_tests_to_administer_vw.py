from django.db import models


class UserAccommodationFileTestsToAdministerVW(models.Model):
    id = models.IntegerField(db_column="id", primary_key=True)
    parent_code = models.CharField(db_column="parent_code", max_length=25)
    test_code = models.CharField(db_column="test_code", max_length=25)
    version = models.IntegerField(db_column="version")
    name_en = models.CharField(db_column="name_en", max_length=175)
    name_fr = models.CharField(db_column="name_fr", max_length=175)
    active = models.BooleanField(db_column="active")
    version_notes = models.CharField(db_column="version_notes", max_length=500)
    is_uit = models.BooleanField(db_column="is_uit")
    test_skill_type_id = models.IntegerField(db_column="test_skill_type_id")
    test_skill_type_codename = models.CharField(
        db_column="test_skill_type_codename", max_length=25
    )
    test_skill_type_name_en = models.CharField(
        db_column="test_skill_type_name_en", max_length=150
    )
    test_skill_type_name_fr = models.CharField(
        db_column="test_skill_type_name_fr", max_length=150
    )
    test_skill_sub_type_id = models.IntegerField(db_column="test_skill_sub_type_id")
    test_skill_sub_type_codename = models.CharField(
        db_column="test_skill_sub_type_codename", max_length=25
    )
    test_skill_sub_type_name_en = models.CharField(
        db_column="test_skill_sub_type_name_en", max_length=150
    )
    test_skill_sub_type_name_fr = models.CharField(
        db_column="test_skill_sub_type_name_fr", max_length=150
    )

    class Meta:
        managed = False
        db_table = "user_accommodation_file_tests_to_administer_vw"
