from django.db import models


class TestResultReportVW(models.Model):
    assigned_test_id = models.IntegerField(
        db_column="ASSIGNED_TEST_ID", primary_key=True
    )
    candidate_user_id = models.IntegerField(db_column="CANDIDATE_USER_ID", default=1)
    candidate_username = models.EmailField(
        db_column="CANDIDATE_USERNAME", max_length=254, default="temp"
    )
    candidate_email = models.EmailField(db_column="CANDIDATE_EMAIL", max_length=254)
    uit_candidate_email = models.EmailField(
        db_column="UIT_CANDIDATE_EMAIL", max_length=254
    )
    candidate_first_name = models.EmailField(
        db_column="CANDIDATE_FIRST_NAME", max_length=30
    )
    candidate_last_name = models.EmailField(
        db_column="CANDIDATE_LAST_NAME", max_length=150
    )
    candidate_pri = models.CharField(
        db_column="CANDIDATE_PRI", max_length=10, default=""
    )
    candidate_dob = models.DateField(db_column="CANDIDATE_DOB", default="1900-01-01")
    candidate_military_nbr = models.CharField(
        db_column="CANDIDATE_MILITARY_NBR", max_length=9, default=""
    )
    ta_user_id = models.IntegerField(db_column="TA_USER_ID", default=1)
    ta_username = models.CharField(
        db_column="TA_USERNAME", max_length=254, default="temp"
    )
    ta_email = models.EmailField(db_column="TA_EMAIL", max_length=254, default="temp")
    test_order_number = models.CharField(db_column="TEST_ORDER_NUMBER", max_length=12)
    process_number = models.CharField(
        db_column="PROCESS_NUMBER",
        max_length=50,
    )
    reference_number = models.CharField(db_column="REFERENCE_NUMBER", max_length=25)
    td_test_code = models.CharField(db_column="TD_TEST_CODE", max_length=25)
    td_en_name = models.CharField(db_column="TD_EN_NAME", max_length=175)
    td_fr_name = models.CharField(db_column="TD_FR_NAME", max_length=175)
    submit_date = models.DateTimeField(db_column="SUBMIT_DATE")
    test_score = models.IntegerField(db_column="TEST_SCORE")
    level_en = models.CharField(db_column="LEVEL_EN", max_length=50)
    level_fr = models.CharField(db_column="LEVEL_FR", max_length=50)
    test_status_id = models.IntegerField(db_column="TEST_STATUS_ID", default=1)
    test_status_codename = models.CharField(
        db_column="TEST_STATUS_CODENAME", max_length=50, default="temp"
    )
    test_id = models.IntegerField(db_column="TEST_ID")
    allowed_ta_user_ids = models.TextField(
        db_column="ALLOWED_TA_USER_IDS", default=None
    )

    class Meta:
        managed = False
        db_table = "test_result_vw"
