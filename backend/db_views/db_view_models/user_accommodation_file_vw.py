from django.db import models


class UserAccommodationFileVW(models.Model):
    id = models.IntegerField(db_column="id", primary_key=True)
    comments = models.TextField(db_column="comments")
    is_uit = models.BooleanField(db_column="is_uit")
    modify_date = models.DateTimeField(db_column="modify_date")
    created_date = models.DateField(db_column="created_date")
    process_end_date = models.DateField(
        db_column="process_end_date", default="1900-01-01"
    )
    last_modified_by_user_id = models.IntegerField(
        db_column="last_modified_by_user_id", default=1
    )
    is_alternate_test_request = models.BooleanField(
        db_column="is_alternate_test_request", default=0
    )
    status_id = models.IntegerField(db_column="status_id")
    status_codename = models.CharField(db_column="status_codename", max_length=50)
    status_name_en = models.CharField(
        db_column="status_name_en", max_length=50, default="temp"
    )
    status_name_fr = models.CharField(
        db_column="status_name_fr", max_length=50, default="temp"
    )
    test_center_id = models.IntegerField(db_column="test_center_id")
    test_center_name = models.CharField(
        db_column="test_center_name", max_length=150, default=None
    )
    user_id = models.IntegerField(db_column="user_id")
    user_username = models.CharField(db_column="user_username", max_length=254)
    user_email = models.CharField(db_column="user_email", max_length=254)
    user_first_name = models.CharField(db_column="user_first_name", max_length=30)
    user_last_name = models.CharField(db_column="user_last_name", max_length=150)
    assigned_to_user_id = models.IntegerField(
        db_column="assigned_to_user_id", default=1
    )
    assigned_to_user_email = models.CharField(
        db_column="assigned_to_user_email", max_length=254, default="temp"
    )
    assigned_to_user_first_name = models.CharField(
        db_column="assigned_to_user_first_name", max_length=30, default="temp"
    )
    assigned_to_user_last_name = models.CharField(
        db_column="assigned_to_user_last_name", max_length=150, default="temp"
    )
    test_skill_type_id = models.IntegerField(db_column="test_skill_type_id", default=1)
    test_skill_type_codename = models.CharField(
        db_column="test_skill_type_codename", max_length=25, default="temp"
    )
    test_skill_type_name_en = models.CharField(
        db_column="test_skill_type_name_en", max_length=150, default="temp"
    )
    test_skill_type_name_fr = models.CharField(
        db_column="test_skill_type_name_fr", max_length=150, default="temp"
    )
    test_skill_sub_type_id = models.IntegerField(
        db_column="test_skill_sub_type_id", default=1
    )
    test_skill_sub_type_codename = models.CharField(
        db_column="test_skill_sub_type_codename", max_length=25, default="temp"
    )
    test_skill_sub_type_name_en = models.CharField(
        db_column="test_skill_sub_type_name_en", max_length=150, default="temp"
    )
    test_skill_sub_type_name_fr = models.CharField(
        db_column="test_skill_sub_type_name_fr", max_length=150, default="temp"
    )

    class Meta:
        managed = False
        db_table = "user_accommodation_file_vw"
