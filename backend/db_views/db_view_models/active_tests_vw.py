from django.db import models


class ActiveTestsVW(models.Model):
    assigned_test_id = models.IntegerField(
        db_column="ASSIGNED_TEST_ID", primary_key=True
    )
    candidate_user_id = models.IntegerField(db_column="CANDIDATE_USER_ID", default=1)
    candidate_username = models.CharField(
        db_column="CANDIDATE_USERNAME", max_length=254
    )
    candidate_email = models.EmailField(db_column="CANDIDATE_EMAIL", max_length=254)
    candidate_first_name = models.CharField(
        db_column="CANDIDATE_FIRST_NAME", max_length=30
    )
    candidate_last_name = models.CharField(
        db_column="CANDIDATE_LAST_NAME", max_length=150
    )
    ta_user_id = models.IntegerField(db_column="TA_USER_ID", default=1)
    ta_username = models.CharField(
        db_column="TA_USERNAME", max_length=254, default="temp"
    )
    ta_email = models.EmailField(db_column="TA_EMAIL", max_length=254, default="temp")
    ta_first_name = models.CharField(db_column="TA_FIRST_NAME", max_length=30)
    ta_last_name = models.CharField(db_column="TA_LAST_NAME", max_length=150)
    test_status_id = models.IntegerField(db_column="TEST_STATUS_ID", default=1)
    test_status_codename = models.CharField(
        db_column="TEST_STATUS_CODENAME", max_length=50, default="temp"
    )
    test_order_reference_number = models.CharField(
        db_column="TEST_ORDER_REFERENCE_NUMBER", max_length=30, default="temp"
    )
    test_id = models.IntegerField(db_column="TEST_ID")
    test_code = models.CharField(db_column="TEST_CODE", max_length=25)

    class Meta:
        managed = False
        db_table = "active_tests_vw"
