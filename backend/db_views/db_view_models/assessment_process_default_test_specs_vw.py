from django.db import models


class AssessmentProcessDefaultTestSpecsVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    assessment_process_id = models.IntegerField(db_column="ASSESSMENT_PROCESS_ID")
    test_skill_type_id = models.IntegerField(db_column="TEST_SKILL_TYPE_ID")
    test_skill_type_codename = models.CharField(
        db_column="TEST_SKILL_TYPE_CODENAME", max_length=25
    )
    test_skill_type_name_en = models.CharField(
        db_column="TEST_SKILL_TYPE_NAME_EN", max_length=150
    )
    test_skill_type_name_fr = models.CharField(
        db_column="TEST_SKILL_TYPE_NAME_FR", max_length=150
    )
    test_skill_sub_type_id = models.IntegerField(db_column="TEST_SKILL_SUB_TYPE_ID")
    test_skill_sub_type_codename = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_CODENAME", max_length=25
    )
    test_skill_sub_type_name_en = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_NAME_EN", max_length=150
    )
    test_skill_sub_type_name_fr = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_NAME_FR", max_length=150
    )
    reason_for_testing_id = models.IntegerField(db_column="REASON_FOR_TESTING_ID")
    reason_for_testing_name_en = models.CharField(
        db_column="REASON_FOR_TESTING_NAME_EN", max_length=255
    )
    reason_for_testing_name_fr = models.CharField(
        db_column="REASON_FOR_TESTING_NAME_FR", max_length=255
    )
    level_required = models.CharField(db_column="LEVEL_REQUIRED", max_length=30)

    class Meta:
        managed = False
        db_table = "assessment_process_default_test_specs_vw"
