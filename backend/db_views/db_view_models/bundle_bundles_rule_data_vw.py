from django.db import models


class BundleBundlesRuleDataVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    bundle_id = models.IntegerField(db_column="BUNDLE_ID")
    bundle_bundles_rule_id = models.IntegerField(db_column="BUNDLE_BUNDLES_RULE_ID")
    number_of_bundles = models.IntegerField(db_column="NUMBER_OF_BUNDLES")
    shuffle_bundles = models.BooleanField(db_column="SHUFFLE_BUNDLES")
    keep_items_together = models.BooleanField(db_column="KEEP_ITEMS_TOGETHER")
    bundle_bundles_rule_display_order = models.IntegerField(
        db_column="BUNDLE_BUNDLES_RULE_DISPLAY_ORDER"
    )
    bundle_bundles_rule_associations_id = models.IntegerField(
        db_column="BUNDLE_BUNDLES_RULE_ASSOCIATIONS_ID"
    )
    bundle_bundles_rule_associations_bundle_id = models.IntegerField(
        db_column="BUNDLE_BUNDLES_RULE_ASSOCIATIONS_BUNDLE_ID"
    )
    display_order = models.IntegerField(db_column="DISPLAY_ORDER")

    class Meta:
        managed = False
        db_table = "bundle_bundles_rule_data_vw"
