from django.db import models


class TestCenterOlaTestAssessorUnavailabilityVw(models.Model):
    id = models.IntegerField(primary_key=True, db_column="id")
    test_center_ola_test_assessor_id = models.IntegerField(
        db_column="test_center_ola_test_assessor_id"
    )
    assessor_id = models.IntegerField(db_column="assessor_id")
    is_supervisor = models.BooleanField(db_column="is_supervisor")
    first_name = models.CharField(max_length=30, db_column="first_name")
    last_name = models.CharField(max_length=150, db_column="last_name")
    email = models.EmailField(db_column="email")
    test_center_id = models.IntegerField(db_column="test_center_id")
    test_center_name = models.CharField(max_length=150, db_column="test_center_name")
    test_center_city_en = models.CharField(
        max_length=150, db_column="test_center_city_en"
    )
    test_center_city_fr = models.CharField(
        max_length=150, db_column="test_center_city_fr"
    )
    start_date = models.DateTimeField(db_column="start_date")
    end_date = models.DateTimeField(db_column="end_date")
    reason = models.CharField(max_length=300, db_column="reason")

    class Meta:
        managed = False  # No migrations will be created for this model
        db_table = "test_center_ola_test_assessor_unavailability_vw"
