from django.db import models


class AssignedTestsVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    status_id = models.IntegerField(db_column="STATUS_ID")
    status_codename = models.CharField(db_column="STATUS_CODENAME", max_length=50)
    previous_status_id = models.IntegerField(db_column="PREVIOUS_STATUS_ID")
    previous_status_codename = models.CharField(
        db_column="PREVIOUS_STATUS_CODENAME", max_length=50
    )
    start_date = models.DateTimeField(db_column="START_DATE")
    modify_date = models.DateTimeField(db_column="MODIFY_DATE")
    submit_date = models.DateTimeField(db_column="SUBMIT_DATE")
    test_access_code = models.CharField(db_column="TEST_ACCESS_CODE", max_length=10)
    test_id = models.IntegerField(db_column="TEST_ID")
    total_score = models.IntegerField(db_column="TOTAL_SCORE")
    ta_user_id = models.IntegerField(db_column="TA_USER_ID", default=1)
    ta_username = models.CharField(
        db_column="TA_USERNAME", max_length=254, default="temp"
    )
    test_section_id = models.IntegerField(db_column="TEST_SECTION_ID")
    test_session_language_id = models.IntegerField(db_column="TEST_SESSION_LANGUAGE_ID")
    user_id = models.IntegerField(db_column="USER_ID", default=1)
    username = models.CharField(db_column="USERNAME", max_length=254, default="temp")
    test_order_number = models.CharField(db_column="TEST_ORDER_NUMBER", max_length=12)
    en_converted_score = models.CharField(db_column="EN_CONVERTED_SCORE", max_length=50)
    fr_converted_score = models.CharField(db_column="FR_CONVERTED_SCORE", max_length=50)
    is_invalid = models.BooleanField(db_column="IS_INVALID")
    uit_invite_id = models.IntegerField(db_column="UIT_INVITE_ID")
    orderless_financial_data_id = models.IntegerField(
        db_column="ORDERLESS_FINANCIAL_DATA_ID"
    )
    accommodation_request_id = models.IntegerField(db_column="ACCOMMODATION_REQUEST_ID")
    test_session_id = models.IntegerField(db_column="TEST_SESSION_ID")

    class Meta:
        managed = False
        db_table = "assigned_tests_vw"
