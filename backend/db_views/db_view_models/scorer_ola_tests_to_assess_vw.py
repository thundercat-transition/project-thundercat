from django.db import models


class ScorerOlaTestsToAssessVw(models.Model):
    id = models.IntegerField(db_column="id", primary_key=True)
    date = models.DateField(db_column="date")
    day_of_week_id = models.IntegerField(db_column="day_of_week_id")
    start_time = models.DateTimeField(db_column="start_time")
    simplified_start_time = models.TimeField(db_column="simplified_start_time")
    end_time = models.DateTimeField(db_column="end_time")
    simplified_end_time = models.TimeField(db_column="simplified_end_time")
    language_id = models.IntegerField(db_column="language_id")
    is_standard = models.BooleanField(db_column="is_standard", default=1)
    nbr_of_sessions = models.IntegerField(db_column="nbr_of_sessions")

    class Meta:
        managed = False
        db_table = "scorer_ola_tests_to_assess_vw"
