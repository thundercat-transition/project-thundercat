from django.db import models


class TestCenterOlaVacationBlockVw(models.Model):
    id = models.IntegerField(db_column="id", primary_key=True)
    date_from = models.DateField(db_column="date_from")
    date_to = models.DateField(db_column="date_to")
    test_center_id = models.IntegerField(db_column="test_center_id")
    availability_en = models.IntegerField(db_column="availability_en")
    availability_fr = models.IntegerField(db_column="availability_fr")

    class Meta:
        managed = False
        db_table = "test_center_ola_vacation_block_vw"
