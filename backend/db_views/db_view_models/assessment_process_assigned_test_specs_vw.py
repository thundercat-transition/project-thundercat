from django.db import models


class AssessmentProcessAssignedTestSpecsVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    first_name = models.CharField(db_column="FIRST_NAME", max_length=30)
    last_name = models.CharField(db_column="LAST_NAME", max_length=150)
    email = models.EmailField(db_column="EMAIL", max_length=254)
    assessment_process_id = models.IntegerField(db_column="ASSESSMENT_PROCESS_ID")
    assessment_process_reference_number = models.CharField(
        db_column="ASSESSMENT_PROCESS_REFERENCE_NUMBER", max_length=30
    )
    assessment_process_closing_date = models.DateField(
        db_column="ASSESSMENT_PROCESS_CLOSING_DATE"
    )
    assessment_process_allow_booking_external_tc = models.BooleanField(
        db_column="ASSESSMENT_PROCESS_ALLOW_BOOKING_EXTERNAL_TC"
    )
    contact_email_for_candidates = models.CharField(
        db_column="CONTACT_EMAIL_FOR_CANDIDATES", max_length=254, default="temp"
    )
    hr_user_id = models.IntegerField(db_column="HR_USER_ID", default=1)
    hr_email = models.EmailField(db_column="HR_EMAIL", max_length=254, default="temps")
    hr_first_name = models.CharField(
        db_column="HR_FIRST_NAME", max_length=30, default="temps"
    )
    hr_last_name = models.CharField(
        db_column="HR_LAST_NAME", max_length=150, default="temps"
    )
    assessment_process_dept_id = models.IntegerField(
        db_column="ASSESSMENT_PROCESS_DEPT_ID"
    )
    assessment_process_dept_eabrv = models.CharField(
        db_column="ASSESSMENT_PROCESS_DEPT_EABRV", max_length=10
    )
    assessment_process_dept_fabrv = models.CharField(
        db_column="ASSESSMENT_PROCESS_DEPT_FABRV", max_length=10
    )
    assessment_process_dept_edesc = models.CharField(
        db_column="ASSESSMENT_PROCESS_DEPT_EDESC", max_length=200
    )
    assessment_process_dept_fdesc = models.CharField(
        db_column="ASSESSMENT_PROCESS_DEPT_FDESC", max_length=200
    )
    billing_contact_id = models.IntegerField(db_column="BILLING_CONTACT_ID")
    billing_contact_first_name = models.CharField(
        db_column="BILLING_CONTACT_FIRST_NAME", max_length=30
    )
    billing_contact_last_name = models.CharField(
        db_column="BILLING_CONTACT_LAST_NAME", max_length=150
    )
    billing_contact_email = models.EmailField(
        db_column="BILLING_CONTACT_EMAIL", max_length=254
    )
    test_skill_type_id = models.IntegerField(db_column="TEST_SKILL_TYPE_ID")
    test_skill_type_codename = models.CharField(
        db_column="TEST_SKILL_TYPE_CODENAME", max_length=25
    )
    test_skill_type_name_en = models.CharField(
        db_column="TEST_SKILL_TYPE_NAME_EN", max_length=150
    )
    test_skill_type_name_fr = models.CharField(
        db_column="TEST_SKILL_TYPE_NAME_FR", max_length=150
    )
    test_skill_sub_type_id = models.IntegerField(db_column="TEST_SKILL_SUB_TYPE_ID")
    test_skill_sub_type_codename = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_CODENAME", max_length=25
    )
    test_skill_sub_type_name_en = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_NAME_EN", max_length=150
    )
    test_skill_sub_type_name_fr = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_NAME_FR", max_length=150
    )
    reason_for_testing_id = models.IntegerField(db_column="REASON_FOR_TESTING_ID")
    reason_for_testing_codename = models.CharField(
        max_length=50, db_column="REASON_FOR_TESTING_CODENAME"
    )
    reason_for_testing_minimum_process_length = models.IntegerField(
        db_column="REASON_FOR_TESTING_MINIMUM_PROCESS_LENGTH", default=None
    )
    reason_for_testing_name_en = models.CharField(
        db_column="REASON_FOR_TESTING_NAME_EN", max_length=255
    )
    reason_for_testing_name_fr = models.CharField(
        db_column="REASON_FOR_TESTING_NAME_FR", max_length=255
    )
    level_required = models.CharField(db_column="LEVEL_REQUIRED", max_length=30)
    request_sent = models.BooleanField(db_column="REQUEST_SENT")
    consumed_reservation_codes_status_id = models.IntegerField(
        db_column="consumed_reservation_codes_status_id", default=1
    )
    consumed_reservation_codes_status_codename = models.CharField(
        db_column="consumed_reservation_codes_status_codename",
        max_length=14,
        default="temp",
    )
    test_session_id = models.IntegerField(db_column="test_session_id", default=1)
    assigned_test_id = models.IntegerField(db_column="assigned_test_id", default=1)
    assigned_test_status_id = models.IntegerField(
        db_column="assigned_test_status_id", default=1
    )
    assigned_test_status_codename = models.CharField(
        db_column="assigned_test_status_codename", max_length=50, default="temp"
    )
    user_accommodation_file_id = models.IntegerField(
        db_column="user_accommodation_file_id", default=1
    )
    is_alternate_test_request = models.BooleanField(
        db_column="is_alternate_test_request", default=0
    )
    test_id = models.IntegerField(db_column="test_id", default=1)
    parent_code = models.CharField(
        db_column="parent_code", max_length=25, default="temps"
    )
    test_code = models.CharField(db_column="test_code", max_length=25, default="temps")
    version = models.IntegerField(db_column="version", default=1)
    test_name_en = models.CharField(
        db_column="test_name_en", max_length=175, default="temps"
    )
    test_name_fr = models.CharField(
        db_column="test_name_fr", max_length=175, default="temps"
    )
    candidate_user_id = models.IntegerField(db_column="candidate_user_id", default=1)
    candidate_username = models.CharField(
        db_column="candidate_username", max_length=254, default="temps"
    )
    candidate_email = models.CharField(
        db_column="candidate_email", max_length=254, default="temps"
    )
    candidate_first_name = models.CharField(
        db_column="candidate_first_name", max_length=30, default="temps"
    )
    candidate_last_name = models.CharField(
        db_column="candidate_last_name", max_length=150, default="temps"
    )

    class Meta:
        managed = False
        db_table = "assessment_process_assigned_test_specs_vw"
