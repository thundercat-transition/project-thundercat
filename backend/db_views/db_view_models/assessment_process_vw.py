from django.db import models


class AssessmentProcessVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    user_id = models.IntegerField(db_column="USER_ID")
    user_email = models.CharField(
        db_column="USER_EMAIL", max_length=254, default="temp"
    )
    reference_number = models.CharField(db_column="REFERENCE_NUMBER", max_length=30)
    duration = models.IntegerField(db_column="DURATION", default=0)
    closing_date = models.DateField(db_column="CLOSING_DATE")
    sent_date = models.DateField(db_column="SENT_DATE", default="2000-01-01")
    allow_booking_external_tc = models.BooleanField(
        db_column="ALLOW_BOOKING_EXTERNAL_TC"
    )
    allow_last_minute_cancellation_tc = models.BooleanField(
        db_column="ALLOW_LAST_MINUTE_CANCELLATION_TC", default=False
    )
    request_sent = models.BooleanField(db_column="REQUEST_SENT", default=False)
    dept_id = models.IntegerField(db_column="DEPT_ID")
    dept_eabrv = models.CharField(db_column="DEPT_EABRV", max_length=10)
    dept_fabrv = models.CharField(db_column="DEPT_FABRV", max_length=10)
    dept_edesc = models.CharField(db_column="DEPT_EDESC", max_length=200)
    dept_fdesc = models.CharField(db_column="DEPT_FDESC", max_length=200)
    default_billing_contact_id = models.IntegerField(
        db_column="DEFAULT_BILLING_CONTACT_ID", null=True
    )
    default_billing_contact_first_name = models.CharField(
        db_column="DEFAULT_BILLING_CONTACT_FIRST_NAME", max_length=30, null=True
    )
    default_billing_contact_last_name = models.CharField(
        db_column="DEFAULT_BILLING_CONTACT_LAST_NAME", max_length=150, null=True
    )
    default_billing_contact_email = models.EmailField(
        db_column="DEFAULT_BILLING_CONTACT_EMAIL", max_length=254, null=True
    )
    default_billing_contact_fis_organisation_code = models.CharField(
        db_column="DEFAULT_BILLING_CONTACT_FIS_ORGANISATION_CODE",
        max_length=16,
        null=True,
    )
    default_billing_contact_fis_reference_code = models.CharField(
        db_column="DEFAULT_BILLING_CONTACT_FIS_REFERENCE_CODE", max_length=20, null=True
    )
    default_billing_contact_dept_id = models.IntegerField(
        db_column="DEFAULT_BILLING_CONTACT_DEPT_ID", null=True
    )
    default_billing_contact_dept_eabrv = models.CharField(
        db_column="DEFAULT_BILLING_CONTACT_DEPT_EABRV", max_length=10, null=True
    )
    default_billing_contact_dept_fabrv = models.CharField(
        db_column="DEFAULT_BILLING_CONTACT_DEPT_FABRV", max_length=10, null=True
    )
    default_billing_contact_dept_edesc = models.CharField(
        db_column="DEFAULT_BILLING_CONTACT_DEPT_EDESC", max_length=200, null=True
    )
    default_billing_contact_dept_fdesc = models.CharField(
        db_column="DEFAULT_BILLING_CONTACT_DEPT_FDESC", max_length=200, null=True
    )
    contact_email_for_candidates = models.CharField(
        db_column="CONTACT_EMAIL_FOR_CANDIDATES", max_length=254, default="temp"
    )

    class Meta:
        managed = False
        db_table = "assessment_process_vw"
