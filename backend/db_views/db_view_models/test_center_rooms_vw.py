from django.db import models


class TestCenterRoomsVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    test_center_id = models.IntegerField(db_column="TEST_CENTER_ID")
    name = models.CharField(db_column="NAME", max_length=150)
    email = models.EmailField(db_column="EMAIL", max_length=254)
    max_occupancy = models.IntegerField(db_column="MAX_OCCUPANCY")
    active = models.BooleanField(db_column="ACTIVE")

    class Meta:
        managed = False
        db_table = "test_center_rooms_vw"
