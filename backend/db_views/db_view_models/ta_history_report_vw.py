from django.db import models


class TaHistoryReportVW(models.Model):

    assigned_test_id = models.IntegerField(
        db_column="assigned_test_id", primary_key=True
    )
    is_invalid = models.BooleanField(db_column="is_invalid", default="1")
    ta_first_name = models.CharField(db_column="ta_first_name", max_length=91)
    ta_last_name = models.CharField(db_column="ta_last_name", max_length=91)
    pri = models.CharField(db_column="pri", max_length=10)
    military_number = models.CharField(db_column="military_number", max_length=7)
    ta_user_id = models.IntegerField(db_column="ta_user_id", default=1)
    ta_email = models.CharField(db_column="ta_email", max_length=254, default="temp")
    goc_email = models.CharField(db_column="goc_email", max_length=254)
    requesting_organization = models.CharField(
        db_column="requesting_organization", max_length=447
    )
    test_code = models.CharField(db_column="test", max_length=25)
    test_submit_date = models.DateField(db_column="test_submit_date")
    candidate_name = models.CharField(db_column="candidate_name", max_length=182)
    candidate_user_id = models.IntegerField(db_column="candidate_user_id", default=1)
    candidate_username = models.CharField(
        db_column="candidate_username", max_length=150
    )

    class Meta:
        managed = False
        db_table = "ta_history_vw"
