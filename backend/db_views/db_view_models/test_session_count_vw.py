# TODO For deletion? duplicates functionality of test_center_test_sessions_vw
from django.db import models


class TestSessionCountVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    count = models.IntegerField(db_column="COUNT")
    max = models.IntegerField(db_column="MAX")

    class Meta:
        managed = False
        db_table = "test_session_count_vw"
