from .item_bank_attribute_values_vw import ItemBankAttributeValuesVW
from .financial_report_vw import FinancialReportVW
from .ta_history_report_vw import TaHistoryReportVW
from .test_result_report_vw import TestResultReportVW
from .active_tests_vw import ActiveTestsVW
from .user_look_up_vw import UserLookUpVW
from .bo_test_access_codes_vw import BoTestAccessCodesVW
from .test_order_and_reference_numbers_vw import TestOrderAndReferenceNumbersVW
from .test_data_vw import TestDataVW
from .all_items_data_vw import AllItemsDataVW
from .item_latest_versions_data_vw import ItemLatestVersionsDataVW
from .item_drafts_vw import ItemDraftsVW
from .item_content_vw import ItemContentVW
from .item_option_vw import ItemOptionVW
from .item_content_drafts_vw import ItemContentDraftsVW
from .item_option_drafts_vw import ItemOptionDraftsVW
from .bundle_association_vw import BundleAssociationVW
from .bundle_data_vw import BundleDataVW
from .bundle_bundles_rule_data_vw import BundleBundlesRuleDataVW
from .user_profile_change_request_vw import UserProfileChangeRequestVW
from .test_centers_vw import TestCentersVW
from .test_center_test_administrators_vw import TestCenterTestAdministratorsVW
from .test_center_rooms_vw import TestCenterRoomsVW
from .test_center_test_sessions_vw import TestCenterTestSessionsVW
from .assessment_process_vw import AssessmentProcessVW
from .assessment_process_results_vw import AssessmentProcessResultsVW
from .assessment_process_default_test_specs_vw import (
    AssessmentProcessDefaultTestSpecsVW,
)
from .candidate_reservations_vw import CandidateReservationsVW
from .booked_reservations_vw import BookedReservationsVW
from .test_session_count_vw import TestSessionCountVW
from .test_session_attendees_vw import TestSessionAttendeesVW
from .billing_contact_vw import BillingContactVW
from .assessment_process_results_candidates_report_vw import (
    AssessmentProcessResultsCandidatesReportVW,
)
from .assessment_process_results_candidates_vw import (
    AssessmentProcessResultsCandidatesVW,
)
from .ta_assigned_candidates_vw import (
    TaAssignedCandidatesVW,
)
from .assigned_tests_vw import (
    AssignedTestsVW,
)
from .adapted_tests_report_vw import (
    AdaptedTestsReportVW,
)
from .custom_permission_2fa_vw import CustomPermission2FAVW
from .scored_tests_vw import ScoredTestsVW
from .user_look_up_tests_vw import UserLookUpTestsVW
from .uit_completed_processes_vw import UitCompletedProcessesVW
from .uit_related_candidates_vw import UitRelatedCandidatesVW
from .ta_extended_profile_vw import TaExtendedProfileVW
from .user_accommodation_file_vw import UserAccommodationFileVW
from .selected_user_accommodation_file_vw import SelectedUserAccommodationFileVW
from .user_accommodation_file_tests_to_administer_vw import (
    UserAccommodationFileTestsToAdministerVW,
)
from .completed_supervised_user_accommodation_file_vw import (
    CompletedSupervisedUserAccommodationFileVW,
)
from .user_accommodation_file_data_for_details_popup_vw import (
    UserAccommodationFileDataForDetailsPopupVW,
)
from .user_accommodation_file_report_data_vw import (
    UserAccommodationFileReportDataVW,
)
from .test_center_ola_vacation_block_vw import (
    TestCenterOlaVacationBlockVw,
)
from .test_center_ola_test_assessors_vw import (
    TestCenterOlaTestAssessorsVw,
)
from .test_center_ola_time_slots_vw import (
    TestCenterOlaTimeSlotsVw,
)
from .test_center_ola_available_time_slots_vw import (
    TestCenterOlaAvailableTimeSlotsVw,
)
from .scorer_ola_tests_to_assess_vw import (
    ScorerOlaTestsToAssessVw,
)
from .scorer_ola_detailed_tests_to_assess_vw import (
    ScorerOlaDetailedTestsToAssessVw,
)
from .aae_reports_number_of_requests_received_vw import (
    AaeReportsNumberOfRequestsReceivedVw,
)
from .aae_reports_number_of_closed_requests_vw import (
    AaeReportsNumberOfClosedRequestsVw,
)

from .test_center_ola_test_assessor_unavailability_vw import (
    TestCenterOlaTestAssessorUnavailabilityVw,
)

from .virtual_meeting_room_availability_vw import VirtualMeetingRoomAvailabilityVW
from .virtual_test_session_details_vw import VirtualTestSessionDetailsVW

# from .assessment_process_assigned_test_specs_vw import (
#     AssessmentProcessAssignedTestSpecsVW,
# )

# These imports help to auto discover the models
