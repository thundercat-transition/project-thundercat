from django.db import models


class AaeReportsNumberOfRequestsReceivedVw(models.Model):
    id = models.IntegerField(db_column="id", primary_key=True)
    created_date = models.DateField(db_column="created_date")
    number_of_requests = models.IntegerField(db_column="number_of_requests")

    class Meta:
        managed = False
        db_table = "aae_reports_number_of_requests_received_vw"
