from django.db import models


class AssessmentProcessResultsCandidatesVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    assessment_process_candidate_first_name = models.CharField(
        db_column="ASSESSMENT_PROCESS_CANDIDATE_FIRST_NAME",
        max_length=30,
        default="default",
    )
    assessment_process_candidate_last_name = models.CharField(
        db_column="ASSESSMENT_PROCESS_CANDIDATE_LAST_NAME",
        max_length=150,
        default="default",
    )
    assessment_process_candidate_email = models.EmailField(
        db_column="ASSESSMENT_PROCESS_CANDIDATE_EMAIL",
        max_length=254,
        default="default",
    )
    assessment_process_id = models.IntegerField(db_column="ASSESSMENT_PROCESS_ID")
    assessment_process_reference_number = models.CharField(
        db_column="ASSESSMENT_PROCESS_REFERENCE_NUMBER", max_length=30
    )
    assessment_process_closing_date = models.DateField(
        db_column="ASSESSMENT_PROCESS_CLOSING_DATE"
    )
    assessment_process_allow_booking_external_tc = models.BooleanField(
        db_column="ASSESSMENT_PROCESS_ALLOW_BOOKING_EXTERNAL_TC"
    )
    assessment_process_dept_id = models.IntegerField(
        db_column="ASSESSMENT_PROCESS_DEPT_ID"
    )
    assessment_process_dept_eabrv = models.CharField(
        db_column="ASSESSMENT_PROCESS_DEPT_EABRV", max_length=10
    )
    assessment_process_dept_fabrv = models.CharField(
        db_column="ASSESSMENT_PROCESS_DEPT_FABRV", max_length=10
    )
    assessment_process_dept_edesc = models.CharField(
        db_column="ASSESSMENT_PROCESS_DEPT_EDESC", max_length=200
    )
    assessment_process_dept_fdesc = models.CharField(
        db_column="ASSESSMENT_PROCESS_DEPT_FDESC", max_length=200
    )
    billing_contact_id = models.IntegerField(db_column="BILLING_CONTACT_ID")
    billing_contact_first_name = models.CharField(
        db_column="BILLING_CONTACT_FIRST_NAME", max_length=30
    )
    billing_contact_last_name = models.CharField(
        db_column="BILLING_CONTACT_LAST_NAME", max_length=150
    )
    billing_contact_email = models.EmailField(
        db_column="BILLING_CONTACT_EMAIL", max_length=254
    )
    test_skill_type_id = models.IntegerField(db_column="TEST_SKILL_TYPE_ID")
    test_skill_type_codename = models.CharField(
        db_column="TEST_SKILL_TYPE_CODENAME", max_length=25
    )
    test_skill_type_name_en = models.CharField(
        db_column="TEST_SKILL_TYPE_NAME_EN", max_length=150
    )
    test_skill_type_name_fr = models.CharField(
        db_column="TEST_SKILL_TYPE_NAME_FR", max_length=150
    )
    test_skill_sub_type_id = models.IntegerField(db_column="TEST_SKILL_SUB_TYPE_ID")
    test_skill_sub_type_codename = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_CODENAME", max_length=25
    )
    test_skill_sub_type_name_en = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_NAME_EN", max_length=150
    )
    test_skill_sub_type_name_fr = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_NAME_FR", max_length=150
    )
    reason_for_testing_id = models.IntegerField(db_column="REASON_FOR_TESTING_ID")
    reason_for_testing_name_en = models.CharField(
        db_column="REASON_FOR_TESTING_NAME_EN", max_length=255
    )
    reason_for_testing_name_fr = models.CharField(
        db_column="REASON_FOR_TESTING_NAME_FR", max_length=255
    )
    level_required = models.CharField(db_column="LEVEL_REQUIRED", max_length=30)
    request_sent = models.BooleanField(db_column="REQUEST_SENT")
    reservation_code = models.CharField(db_column="RESERVATION_CODE", max_length=14)
    consumed_reservation_code_status_id = models.IntegerField(
        db_column="CONSUMED_RESERVATION_CODE_STATUS_ID"
    )
    test_session_id = models.IntegerField(db_column="TEST_SESSION_ID")
    candidate_id = models.IntegerField(db_column="CANDIDATE_ID")
    assigned_test_id = models.IntegerField(db_column="ASSIGNED_TEST_ID")
    assigned_test_status_id = models.IntegerField(
        db_column="ASSIGNED_TEST_STATUS_ID", default=1
    )
    assigned_test_status_codename = models.CharField(
        db_column="ASSIGNED_TEST_STATUS_CODENAME", max_length=50, default="temp"
    )
    en_converted_score = models.CharField(db_column="EN_CONVERTED_SCORE", max_length=50)
    fr_converted_score = models.CharField(db_column="FR_CONVERTED_SCORE", max_length=50)
    submit_date = models.DateTimeField(db_column="SUBMIT_DATE", default="default")
    total_score = models.IntegerField(db_column="TOTAL_SCORE", default="default")
    user_accommodation_file_id = models.IntegerField(
        db_column="USER_ACCOMMODATION_FILE_ID", default=None
    )
    is_alternate_test_request = models.BooleanField(
        db_column="IS_ALTERNATE_TEST_REQUEST", default=0
    )
    user_username = models.CharField(
        db_column="USER_USERNAME", max_length=254, default="default"
    )
    user_email = models.EmailField(
        db_column="USER_EMAIL", max_length=254, default="temp"
    )
    user_pri = models.CharField(max_length=10, db_column="USER_PRI", default="default")
    user_military_nbr = models.CharField(
        max_length=9, db_column="USER_MILITARY_NBR", default="default"
    )
    user_first_name = models.CharField(
        max_length=30, db_column="USER_FIRST_NAME", default="default"
    )
    user_last_name = models.CharField(
        max_length=150, db_column="USER_LAST_NAME", default="default"
    )
    test_definition_test_code = models.CharField(
        max_length=25, db_column="TEST_DEFINITION_TEST_CODE", default="default"
    )
    test_definition_en_name = models.CharField(
        max_length=175, db_column="TEST_DEFINITION_EN_NAME", default="default"
    )
    test_definition_fr_name = models.CharField(
        max_length=175, db_column="TEST_DEFINITION_FR_NAME", default="default"
    )

    class Meta:
        managed = False
        db_table = "assessment_process_results_candidates_vw"
