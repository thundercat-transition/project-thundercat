from django.db import models


class TaExtendedProfileVW(models.Model):
    id = models.IntegerField(db_column="id", primary_key=True)
    user_id = models.IntegerField(db_column="user_id")
    first_name = models.CharField(db_column="first_name", max_length=30)
    last_name = models.CharField(db_column="last_name", max_length=150)
    email = models.CharField(db_column="email", max_length=254)
    dept_id = models.IntegerField(db_column="dept_id")
    dept_eabrv = models.CharField(db_column="dept_eabrv", max_length=10)
    dept_fabrv = models.CharField(db_column="dept_fabrv", max_length=10)
    dept_edesc = models.CharField(db_column="dept_edesc", max_length=200)
    dept_fdesc = models.CharField(db_column="dept_fdesc", max_length=200)

    class Meta:
        managed = False
        db_table = "ta_extended_profile_vw"
