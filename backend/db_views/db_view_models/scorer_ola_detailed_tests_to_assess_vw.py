from django.db import models


class ScorerOlaDetailedTestsToAssessVw(models.Model):
    id = models.IntegerField(db_column="id", primary_key=True)
    date = models.DateField(db_column="date")
    day_of_week_id = models.IntegerField(db_column="day_of_week_id")
    start_time = models.DateTimeField(db_column="start_time")
    simplified_start_time = models.TimeField(db_column="simplified_start_time")
    end_time = models.DateTimeField(db_column="end_time")
    simplified_end_time = models.TimeField(db_column="simplified_end_time")
    language_id = models.IntegerField(db_column="language_id")
    is_standard = models.BooleanField(db_column="is_standard", default=0)
    user_accommodation_file_id = models.IntegerField(
        db_column="user_accommodation_file_id", default=1
    )
    candidate_user_id = models.IntegerField(db_column="candidate_user_id")
    candidate_user_email = models.EmailField(
        db_column="candidate_user_email", max_length=254
    )
    candidate_user_first_name = models.CharField(
        max_length=30, db_column="candidate_user_first_name"
    )
    candidate_user_last_name = models.CharField(
        max_length=30, db_column="candidate_user_last_name"
    )
    candidate_pri = models.CharField(max_length=10, db_column="candidate_pri")
    candidate_phone_number = models.CharField(
        max_length=10, db_column="candidate_phone_number", default=""
    )
    consumed_reservation_code_id = models.IntegerField(
        db_column="consumed_reservation_code_id"
    )
    consumed_reservation_code_status_id = models.IntegerField(
        db_column="consumed_reservation_code_status_id"
    )
    consumed_reservation_code_status_codename = models.CharField(
        max_length=14, db_column="consumed_reservation_code_status_codename"
    )
    assessment_process_assigned_test_specs_id = models.IntegerField(
        db_column="assessment_process_assigned_test_specs_id"
    )
    assessment_process_reference_number = models.CharField(
        db_column="assessment_process_reference_number", max_length=30
    )
    booked_date = models.DateTimeField(db_column="booked_date")
    assessment_process_closing_date = models.DateField(
        db_column="assessment_process_closing_date"
    )
    assessment_process_dept_id = models.IntegerField(
        db_column="assessment_process_dept_id"
    )
    assessment_process_dept_eabrv = models.CharField(
        db_column="assessment_process_dept_eabrv", max_length=10
    )
    assessment_process_dept_edesc = models.CharField(
        db_column="assessment_process_dept_edesc", max_length=200
    )
    assessment_process_dept_fabrv = models.CharField(
        db_column="assessment_process_dept_fabrv", max_length=10
    )
    assessment_process_dept_fdesc = models.CharField(
        db_column="assessment_process_dept_fdesc", max_length=200
    )
    hr_user_id = models.IntegerField(db_column="hr_user_id")
    hr_email = models.EmailField(db_column="hr_email", max_length=254)
    hr_first_name = models.CharField(max_length=30, db_column="hr_first_name")
    hr_last_name = models.CharField(max_length=30, db_column="hr_last_name")
    reason_for_testing_id = models.IntegerField(db_column="reason_for_testing_id")
    reason_for_testing_minimum_process_length = models.IntegerField(
        db_column="reason_for_testing_minimum_process_length"
    )
    reason_for_testing_name_en = models.CharField(
        db_column="reason_for_testing_name_en", max_length=255
    )
    reason_for_testing_name_fr = models.CharField(
        db_column="reason_for_testing_name_fr", max_length=255
    )
    level_required = models.CharField(db_column="level_required", max_length=30)
    test_assessor_user_id = models.IntegerField(db_column="test_assessor_user_id")
    test_assessor_user_email = models.EmailField(
        db_column="test_assessor_user_email", max_length=254
    )
    test_assessor_user_first_name = models.CharField(
        max_length=30, db_column="test_assessor_user_first_name"
    )
    test_assessor_user_last_name = models.CharField(
        max_length=30, db_column="test_assessor_user_last_name"
    )
    meeting_link = models.CharField(max_length=255, blank=False, null=False, default="")

    class Meta:
        managed = False
        db_table = "scorer_ola_detailed_tests_to_assess_vw"
