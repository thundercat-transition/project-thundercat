from django.db import models


class UserProfileChangeRequestVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    user_id = models.IntegerField(db_column="USER_ID")
    current_first_name = models.CharField(db_column="CURRENT_FIRST_NAME", max_length=30)
    current_last_name = models.CharField(db_column="CURRENT_LAST_NAME", max_length=150)
    current_birth_date = models.DateField(db_column="CURRENT_BIRTH_DATE")
    new_first_name = models.CharField(db_column="NEW_FIRST_NAME", max_length=30)
    new_last_name = models.CharField(db_column="NEW_LAST_NAME", max_length=150)
    new_birth_date = models.DateField(db_column="NEW_BIRTH_DATE")
    modify_date = models.DateTimeField(db_column="MODIFY_DATE")
    comments = models.TextField(db_column="COMMENTS")
    reason_for_deny = models.CharField(db_column="REASON_FOR_DENY", max_length=255)
    username = models.EmailField(db_column="USERNAME", max_length=254)
    email = models.EmailField(db_column="EMAIL", max_length=254)
    pri = models.CharField(db_column="PRI", max_length=10)
    military_nbr = models.CharField(db_column="MILITARY_NBR", max_length=9)

    class Meta:
        managed = False
        db_table = "user_profile_change_request_vw"
