from django.db import models


class UitRelatedCandidatesVW(models.Model):
    id = models.IntegerField(db_column="id", primary_key=True)
    user_id = models.IntegerField(db_column="user_id", default=None)
    first_name = models.CharField(db_column="first_name", max_length=30)
    last_name = models.CharField(db_column="last_name", max_length=150)
    email = models.CharField(db_column="email", max_length=254)
    reason_for_deletion_id = models.IntegerField(db_column="reason_for_deletion_id")
    uit_invite_id = models.IntegerField(db_column="uit_invite_id")
    invite_date = models.DateField(db_column="invite_date")
    validity_end_date = models.DateField(db_column="validity_end_date")
    orderless_request = models.BooleanField(db_column="orderless_request")
    ta_user_id = models.IntegerField(db_column="ta_user_id")
    test_id = models.IntegerField(db_column="test_id")
    assigned_test_id = models.IntegerField(db_column="assigned_test_id")
    test_access_code = models.CharField(db_column="test_access_code", max_length=10)
    start_date = models.DateTimeField(db_column="start_date")
    modify_date = models.DateTimeField(db_column="modify_date")
    submit_date = models.DateTimeField(db_column="submit_date")
    status_id = models.IntegerField(db_column="status_id")
    status_codename = models.CharField(db_column="status_codename", max_length=50)
    user_accommodation_file_id = models.IntegerField(
        db_column="user_accommodation_file_id", default=None
    )
    is_alternate_test_request = models.BooleanField(
        db_column="is_alternate_test_request", default=0
    )

    class Meta:
        managed = False
        db_table = "uit_related_candidates_vw"
