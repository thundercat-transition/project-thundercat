from django.db import models


class ItemBankAttributeValuesVW(models.Model):
    item_bank_attribute_value_id = models.IntegerField(
        db_column="ITEM_BANK_ATTRIBUTE_VALUE_ID", primary_key=True
    )
    attribute_value_order = models.IntegerField(db_column="ATTRIBUTE_VALUE_ORDER")
    item_bank_attribute_id = models.IntegerField(db_column="ITEM_BANK_ATTRIBUTE_ID")
    attribute_text_en = models.TextField(db_column="ATTRIBUTE_TEXT_EN", default="")
    attribute_text_fr = models.TextField(db_column="ATTRIBUTE_TEXT_FR", default="")
    attribute_value_type = models.IntegerField(db_column="ATTRIBUTE_VALUE_TYPE")
    item_bank_attribute_value_text_id = models.IntegerField(
        db_column="ITEM_BANK_ATTRIBUTE_VALUES_TEXT_ID"
    )
    text = models.TextField(db_column="TEXT", default="")

    class Meta:
        managed = False
        db_table = "item_bank_attribute_values_vw"
