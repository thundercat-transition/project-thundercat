from django.db import models


class BookedReservationsVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    reservation_code = models.CharField(max_length=14, null=False, blank=False)
    modify_date = models.DateField(db_column="modify_date")
    candidate_id = models.IntegerField(db_column="CANDIDATE_ID")
    candidate_has_accommodations_profile = models.BooleanField(
        db_column="CANDIDATE_HAS_ACCOMMODATIONS_PROFILE", default=0
    )
    user_accommodation_file_id = models.IntegerField(
        db_column="USER_ACCOMMODATION_FILE_ID", default=1
    )
    assessment_process_closing_date = models.DateField(
        db_column="ASSESSMENT_PROCESS_CLOSING_DATE"
    )
    consumed_reservation_code_status_id = models.IntegerField(default=1)
    consumed_reservation_codename = models.CharField(
        max_length=14, null=False, blank=False
    )
    consumed_reservation_code_en_status = models.CharField(
        max_length=30, blank=False, null=False
    )
    consumed_reservation_code_fr_status = models.CharField(
        max_length=30, blank=False, null=False
    )
    test_skill_type_id = models.IntegerField(db_column="TEST_SKILL_TYPE_ID")
    test_skill_type_codename = models.CharField(
        db_column="TEST_SKILL_TYPE_CODENAME", max_length=25, default=""
    )
    test_skill_type_en_name = models.CharField(
        db_column="TEST_SKILL_TYPE_EN_NAME", max_length=150
    )
    test_skill_type_fr_name = models.CharField(
        db_column="TEST_SKILL_TYPE_FR_NAME", max_length=150
    )
    test_skill_sub_type_id = models.IntegerField(db_column="TEST_SKILL_SUB_TYPE_ID")
    test_skill_sub_type_codename = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_CODENAME", max_length=25, default=""
    )
    test_skill_sub_type_en_name = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_EN_NAME", max_length=150
    )
    test_skill_sub_type_fr_name = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_FR_NAME", max_length=150
    )
    request_sent = models.BooleanField(db_column="REQUEST_SENT", default=False)
    test_session_id = models.IntegerField(db_column="TEST_SESSION_ID", default=1)
    date = models.DateField(db_column="DATE")
    start_time = models.DateTimeField(db_column="START_TIME")
    end_time = models.DateTimeField(db_column="END_TIME")
    test_center_name = models.CharField(max_length=150, blank=False, null=False)
    test_center_postal_code = models.CharField(max_length=7, blank=True, null=False)
    country_id = models.IntegerField(db_column="COUNTRY_ID", default=38)
    country_eabrv = models.CharField(
        db_column="COUNTRY_EABRV", max_length=64, default="temp"
    )
    country_fabrv = models.CharField(
        db_column="COUNTRY_FABRV", max_length=64, default="temp"
    )
    country_edesc = models.CharField(
        db_column="COUNTRY_EDESC", max_length=200, default="temp"
    )
    country_fdesc = models.CharField(
        db_column="COUNTRY_FDESC", max_length=200, default="temp"
    )

    class Meta:
        managed = False
        db_table = "booked_reservations_vw"
