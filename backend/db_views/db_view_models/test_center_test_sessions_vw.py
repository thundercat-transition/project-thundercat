from django.db import models


class TestCenterTestSessionsVW(models.Model):
    # this ID needs to be a CharField because this view is created from a UNION with test center test sessions and consumed reservation codes, so the ID of the consumed reservation code is manipulated with a suffix (-CRC)
    id = models.CharField(db_column="ID", primary_key=True, max_length=254)
    test_center_id = models.IntegerField(db_column="TEST_CENTER_ID")
    test_center_name = models.CharField(
        db_column="TEST_CENTER_NAME", max_length=150, default="temp"
    )
    test_center_postal_code = models.CharField(
        db_column="TEST_CENTER_POSTAL_CODE", max_length=7
    )
    test_session_order = models.IntegerField(db_column="TEST_SESSION_ORDER", default=1)
    assessment_process_assigned_test_specs_id = models.IntegerField(
        db_column="ASSESSMENT_PROCESS_ASSIGNED_TEST_SPECS_ID", default=1
    )
    test_center_security_email = models.CharField(
        db_column="TEST_CENTER_SECURITY_EMAIL", max_length=254, default="temp"
    )
    test_center_room_id = models.IntegerField(db_column="TEST_CENTER_ROOM_ID")
    room_name = models.CharField(db_column="ROOM_NAME", max_length=150)
    spaces_booked = models.IntegerField(db_column="SPACES_BOOKED")
    spaces_unbooked = models.IntegerField(db_column="SPACES_UNBOOKED")
    session_spaces_available = models.IntegerField(db_column="SESSION_SPACES_AVAILABLE")
    room_max_occupancy = models.IntegerField(db_column="ROOM_MAX_OCCUPANCY")
    open_to_ogd = models.BooleanField(db_column="OPEN_TO_OGD")
    country_id = models.IntegerField(db_column="COUNTRY_ID", default=1)
    country_eabrv = models.CharField(
        db_column="COUNTRY_EABRV", max_length=64, default="temp"
    )
    country_fabrv = models.CharField(
        db_column="COUNTRY_FABRV", max_length=64, default="temp"
    )
    country_edesc = models.CharField(
        db_column="COUNTRY_EDESC", max_length=200, default="temp"
    )
    country_fdesc = models.CharField(
        db_column="COUNTRY_FDESC", max_length=200, default="temp"
    )
    department_id = models.IntegerField(db_column="DEPARTMENT_ID")
    dept_eabrv = models.CharField(db_column="DEPT_EABRV", max_length=10, default="temp")
    dept_fabrv = models.CharField(db_column="DEPT_FABRV", max_length=10, default="temp")
    dept_edesc = models.CharField(
        db_column="DEPT_EDESC", max_length=200, default="temp"
    )
    dept_fdesc = models.CharField(
        db_column="DEPT_FDESC", max_length=200, default="temp"
    )
    test_center_test_session_data_id = models.IntegerField(
        db_column="TEST_CENTER_TEST_SESSION_DATA_ID"
    )
    latest_booking_modification_time = models.DateTimeField(
        db_column="LATEST_BOOKING_MODIFICATION_TIME"
    )
    booking_delay = models.IntegerField(db_column="BOOKING_DELAY")
    date = models.DateField(db_column="DATE")
    start_time = models.DateTimeField(db_column="START_TIME")
    end_time = models.DateTimeField(db_column="END_TIME")
    spaces_available = models.IntegerField(db_column="SPACES_AVAILABLE")
    test_assessor_user_id = models.IntegerField(
        db_column="TEST_ASSESSOR_USER_ID", default=1
    )
    test_assessor_user_email = models.EmailField(
        db_column="TEST_ASSESSOR_USER_EMAIL", max_length=254, default="temp"
    )
    test_assessor_user_first_name = models.EmailField(
        db_column="TEST_ASSESSOR_USER_FIRST_NAME", max_length=30, default="temp"
    )
    test_assessor_user_last_name = models.EmailField(
        db_column="TEST_ASSESSOR_USER_LAST_NAME", max_length=150, default="temp"
    )
    candidate_phone_number = models.CharField(
        db_column="candidate_phone_number", max_length=10, default=None
    )
    test_skill_type_id = models.IntegerField(db_column="TEST_SKILL_TYPE_ID")
    test_skill_type_codename = models.CharField(
        db_column="TEST_SKILL_TYPE_CODENAME", max_length=25
    )
    test_skill_type_en_name = models.CharField(
        db_column="TEST_SKILL_TYPE_EN_NAME", max_length=150
    )
    test_skill_type_fr_name = models.CharField(
        db_column="TEST_SKILL_TYPE_FR_NAME", max_length=150
    )
    test_skill_sub_type_id = models.IntegerField(db_column="TEST_SKILL_SUB_TYPE_ID")
    test_skill_sub_type_codename = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_CODENAME", max_length=25, default=""
    )
    test_skill_sub_type_en_name = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_EN_NAME", max_length=150
    )
    test_skill_sub_type_fr_name = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_FR_NAME", max_length=150
    )
    language_id = models.IntegerField(db_column="LANGUAGE_ID", default=1)
    is_standard = models.BooleanField(db_column="IS_STANDARD", default=1)
    user_accommodation_file_id = models.IntegerField(
        db_column="USER_ACCOMMODATION_FILE_ID", default=1
    )
    is_alternate_test_request = models.BooleanField(
        db_column="IS_ALTERNATE_TEST_REQUEST", default=0
    )
    user_id = models.IntegerField(db_column="USER_ID", default=1)
    user_username = models.CharField(
        db_column="USER_USERNAME", max_length=254, default="temp"
    )
    user_email = models.CharField(
        db_column="USER_EMAIL", max_length=254, default="temp"
    )
    user_first_name = models.CharField(
        db_column="USER_FIRST_NAME", max_length=30, default="temp"
    )
    user_last_name = models.CharField(
        db_column="USER_LAST_NAME", max_length=150, default="temp"
    )
    closing_date = models.DateField(db_column="CLOSING_DATE", default="1900-01-01")
    test_id = models.IntegerField(db_column="TEST_ID", default=1)
    parent_code = models.CharField(
        db_column="PARENT_CODE", max_length=25, default="temp"
    )
    test_code = models.CharField(db_column="TEST_CODE", max_length=25, default="temp")
    test_name_en = models.CharField(
        db_column="TEST_NAME_EN", max_length=175, default="temp"
    )
    test_name_fr = models.CharField(
        db_column="TEST_NAME_FR", max_length=175, default="temp"
    )
    version = models.IntegerField(db_column="VERSION", default=1)
    specified_test_description = models.CharField(
        db_column="specified_test_description", max_length=175, default=None
    )
    meeting_link = models.CharField(max_length=255, blank=False, null=False)

    class Meta:
        managed = False
        db_table = "test_center_test_sessions_vw"
