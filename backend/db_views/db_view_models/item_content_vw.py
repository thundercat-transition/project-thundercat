from django.db import models


class ItemContentVW(models.Model):
    item_content_id = models.IntegerField(db_column="ITEM_CONTENT_ID", primary_key=True)
    content_type = models.IntegerField(db_column="CONTENT_TYPE")
    content_order = models.IntegerField(db_column="CONTENT_ORDER")
    item_content_modify_date = models.DateTimeField(
        db_column="ITEM_CONTENT_MODIFY_DATE"
    )
    item_id = models.IntegerField(db_column="ITEM_ID")
    item_content_text_id = models.IntegerField(db_column="ITEM_CONTENT_TEXT_ID")
    text = models.TextField()
    item_content_text_modify_date = models.DateTimeField(
        db_column="ITEM_CONTENT_TEXT_MODIFY_DATE"
    )
    language_id = models.IntegerField(db_column="LANGUAGE_ID")

    class Meta:
        managed = False
        db_table = "item_content_vw"
