from django.db import models
from cms.static.items_utils import ItemTypeCodenames


class AllItemsDataVW(models.Model):
    item_id = models.IntegerField(db_column="ITEM_ID", primary_key=True)
    system_id = models.CharField(db_column="SYSTEM_ID", max_length=15)
    version = models.IntegerField(db_column="VERSION")
    number_of_versions = models.IntegerField(db_column="NUMBER_OF_VERSIONS")
    item_bank_id = models.IntegerField(db_column="ITEM_BANK_ID")
    modify_date = models.DateTimeField(db_column="MODIFY_DATE")
    last_modified_by_user_id = models.EmailField(
        db_column="LAST_MODIFIED_BY_USER_ID", max_length=254, default=1
    )
    active_status = models.BooleanField(db_column="ACTIVE_STATUS", default=0)
    shuffle_options = models.BooleanField(db_column="SHUFFLE_OPTIONS", default=0)
    development_status_id = models.IntegerField(db_column="DEVELOPMENT_STATUS_ID")
    development_status_codename = models.CharField(
        db_column="DEVELOPMENT_STATUS_CODENAME", max_length=50
    )
    development_status_name_en = models.CharField(
        db_column="DEVELOPMENT_STATUS_NAME_EN", max_length=25
    )
    development_status_name_fr = models.CharField(
        db_column="DEVELOPMENT_STATUS_NAME_FR", max_length=25
    )
    response_format_id = models.IntegerField(db_column="RESPONSE_FORMAT_ID")
    response_format_codename = models.CharField(
        db_column="RESPONSE_FORMAT_CODENAME", max_length=50
    )
    response_format_name_en = models.CharField(
        db_column="RESPONSE_FORMAT_NAME_EN", max_length=25
    )
    response_format_name_fr = models.CharField(
        db_column="RESPONSE_FORMAT_NAME_FR", max_length=25
    )
    historical_id = models.CharField(db_column="HISTORICAL_ID", max_length=50)

    item_type_id = models.IntegerField(db_column="ITEM_TYPE_ID", default=1)

    item_type_codename = models.CharField(
        db_column="ITEM_TYPE_CODENAME",
        max_length=25,
        default=ItemTypeCodenames.QUESTION,
    )

    class Meta:
        managed = False
        db_table = "all_items_data_vw"
