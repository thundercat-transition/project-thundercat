from django.db import models


class CustomPermission2FAVW(models.Model):
    custom_permission2fa_id = models.IntegerField(
        db_column="CUSTOM_PERMISSION2FA_ID", primary_key=True
    )
    is_2fa_active = models.BooleanField(db_column="IS_2FA_ACTIVE")
    permission_id = models.IntegerField(db_column="PERMISSION_ID")
    en_name = models.CharField(max_length=75, db_column="EN_NAME")
    fr_name = models.CharField(max_length=75, db_column="FR_NAME")
    en_description = models.TextField(db_column="EN_DESCRIPTION")
    fr_description = models.TextField(db_column="FR_DESCRIPTION")

    class Meta:
        managed = False
        db_table = "custom_permission_2fa_vw"
