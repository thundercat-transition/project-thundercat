from django.db import models


class ScoredTestsVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    start_date = models.DateTimeField(db_column="start_date")
    simplified_start_date = models.DateField(db_column="simplified_start_date")
    modify_date = models.DateTimeField(db_column="modify_date")
    submit_date = models.DateTimeField(db_column="submit_date")
    test_access_code = models.CharField(db_column="test_access_code", max_length=10)
    total_score = models.IntegerField(db_column="total_score")
    test_id = models.IntegerField(db_column="test_id")
    parent_code = models.CharField(db_column="parent_code", max_length=25)
    test_code = models.CharField(db_column="test_code", max_length=25)
    test_name_en = models.CharField(db_column="test_name_en", max_length=175)
    test_name_fr = models.CharField(db_column="test_name_fr", max_length=175)
    retest_period = models.IntegerField(db_column="retest_period")
    show_score = models.BooleanField(db_column="show_score")
    show_result = models.BooleanField(db_column="show_result")
    test_skill_type_id = models.IntegerField(db_column="test_skill_type_id", default=1)
    test_skill_type_name_en = models.CharField(
        db_column="test_skill_type_name_en", max_length=150, default="temp"
    )
    test_skill_type_name_fr = models.CharField(
        db_column="test_skill_type_name_fr", max_length=150, default="temp"
    )
    test_skill_sub_type_id = models.IntegerField(
        db_column="test_skill_sub_type_id", default=1
    )
    test_skill_sub_type_name_en = models.CharField(
        db_column="test_skill_sub_type_name_en", max_length=150, default="temp"
    )
    test_skill_sub_type_name_fr = models.CharField(
        db_column="test_skill_sub_type_name_fr", max_length=150, default="temp"
    )
    test_session_language_id = models.IntegerField(db_column="test_session_language_id")
    test_order_number = models.CharField(db_column="test_order_number", max_length=12)
    en_converted_score = models.CharField(db_column="en_converted_score", max_length=50)
    fr_converted_score = models.CharField(db_column="fr_converted_score", max_length=50)
    is_invalid = models.BooleanField(db_column="is_invalid")
    uit_invite_id = models.IntegerField(db_column="uit_invite_id")
    orderless_financial_data_id = models.IntegerField(
        db_column="orderless_financial_data_id"
    )
    accommodation_request_id = models.IntegerField(db_column="accommodation_request_id")
    test_session_id = models.IntegerField(db_column="test_session_id")
    status_id = models.IntegerField(db_column="status_id")
    status_codename = models.CharField(db_column="status_codename", max_length=50)
    ta_user_id = models.IntegerField(db_column="ta_user_id")
    candidate_user_id = models.IntegerField(db_column="candidate_user_id")
    candidate_email = models.CharField(db_column="candidate_email", max_length=254)
    candidate_first_name = models.CharField(
        db_column="candidate_first_name", max_length=30
    )
    candidate_last_name = models.CharField(
        db_column="candidate_last_name", max_length=150
    )
    candidate_dob = models.DateField(db_column="candidate_dob")
    validity_period = models.IntegerField(db_column="validity_period")
    result_valid_indefinitely = models.BooleanField(
        db_column="result_valid_indefinitely", default=0
    )
    score_valid_until = models.DateField(db_column="score_valid_until")
    calculated_score_valid_until = models.DateField(
        db_column="calculated_score_valid_until", default="temp"
    )
    is_most_recent_valid_test = models.BooleanField(
        db_column="is_most_recent_valid_test", default=0
    )
    most_updated_retest_period = models.IntegerField(
        db_column="most_updated_retest_period", default=1
    )
    calculated_retest_period_date = models.DateField(
        db_column="calculated_retest_period_date", default="temp"
    )
    pending_score = models.BooleanField(db_column="pending_score")

    class Meta:
        managed = False
        db_table = "scored_tests_vw"
