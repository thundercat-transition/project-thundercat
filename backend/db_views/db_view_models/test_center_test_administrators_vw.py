from django.db import models


class TestCenterTestAdministratorsVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    test_center_id = models.IntegerField(db_column="TEST_CENTER_ID")
    user_id = models.IntegerField(db_column="USER_ID")
    email = models.EmailField(db_column="EMAIL", max_length=254)
    first_name = models.CharField(db_column="FIRST_NAME", max_length=30)
    last_name = models.CharField(db_column="LAST_NAME", max_length=150)

    class Meta:
        managed = False
        db_table = "test_center_test_administrators_vw"
