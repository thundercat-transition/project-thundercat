from django.db import models


class TaAssignedCandidatesVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    status_id = models.IntegerField(db_column="STATUS_ID")
    status_codename = models.CharField(db_column="STATUS_CODENAME", max_length=50)
    previous_status_id = models.IntegerField(db_column="PREVIOUS_STATUS_ID")
    previous_status_codename = models.CharField(
        db_column="PREVIOUS_STATUS_CODENAME", max_length=50
    )
    start_date = models.DateTimeField(db_column="START_DATE")
    modify_date = models.DateTimeField(db_column="MODIFY_DATE")
    submit_date = models.DateTimeField(db_column="SUBMIT_DATE")
    test_access_code = models.CharField(db_column="TEST_ACCESS_CODE", max_length=10)
    total_score = models.IntegerField(db_column="TOTAL_SCORE")
    test_id = models.IntegerField(db_column="TEST_ID")
    parent_code = models.CharField(db_column="PARENT_CODE", max_length=25)
    test_code = models.CharField(db_column="TEST_CODE", max_length=25)
    test_name_en = models.CharField(db_column="TEST_NAME_EN", max_length=175)
    test_name_fr = models.CharField(db_column="TEST_NAME_FR", max_length=175)
    test_section_id = models.IntegerField(db_column="TEST_SECTION_ID")
    test_session_language_id = models.IntegerField(db_column="TEST_SESSION_LANGUAGE_ID")
    test_order_number = models.CharField(db_column="TEST_ORDER_NUMBER", max_length=12)
    en_converted_score = models.CharField(db_column="EN_CONVERTED_SCORE", max_length=50)
    fr_converted_score = models.CharField(db_column="FR_CONVERTED_SCORE", max_length=50)
    is_invalid = models.BooleanField(db_column="IS_INVALID")
    uit_invite_id = models.IntegerField(db_column="UIT_INVITE_ID")
    orderless_financial_data_id = models.IntegerField(
        db_column="ORDERLESS_FINANCIAL_DATA_ID"
    )
    accommodation_request_id = models.IntegerField(db_column="ACCOMMODATION_REQUEST_ID")
    test_session_id = models.IntegerField(db_column="TEST_SESSION_ID")
    user_accommodation_file_id = models.IntegerField(
        db_column="USER_ACCOMMODATION_FILE_ID", default=None
    )
    is_alternate_test_request = models.BooleanField(
        db_column="IS_ALTERNATE_TEST_REQUEST", default=0
    )
    candidate_user_id = models.IntegerField(db_column="CANDIDATE_USER_ID", default=1)
    candidate_email = models.CharField(db_column="CANDIDATE_EMAIL", max_length=254)
    candidate_first_name = models.CharField(
        db_column="CANDIDATE_FIRST_NAME", max_length=30
    )
    candidate_last_name = models.CharField(
        db_column="CANDIDATE_LAST_NAME", max_length=150
    )
    candidate_dob = models.DateField(db_column="CANDIDATE_DOB")
    candidate_has_accommodations_profile = models.BooleanField(
        db_column="CANDIDATE_HAS_ACCOMMODATIONS_PROFILE", default=0
    )
    ta_user_id = models.IntegerField(db_column="TA_USER_ID", default=1)
    ta_username = models.CharField(
        db_column="TA_USERNAME", max_length=254, default="temp"
    )
    ta_email = models.CharField(db_column="TA_EMAIL", max_length=254, default="temp")
    ta_first_name = models.CharField(db_column="TA_FIRST_NAME", max_length=30)
    ta_last_name = models.CharField(db_column="TA_LAST_NAME", max_length=150)
    test_started = models.BooleanField(db_column="TEST_STARTED", default=False)
    timed_section_accessed = models.BooleanField(
        db_column="TIMED_SECTION_ACCESSED", default=False
    )
    ta_approve_action_test_section_id = models.BooleanField(
        db_column="TA_APPROVE_ACTION_TEST_SECTION_ID"
    )
    next_test_section_id_that_needs_approval = models.BooleanField(
        db_column="NEXT_TEST_SECTION_ID_THAT_NEEDS_APPROVAL"
    )
    needs_approval = models.BooleanField(db_column="NEEDS_APPROVAL")

    class Meta:
        managed = False
        db_table = "ta_assigned_candidates_vw"
