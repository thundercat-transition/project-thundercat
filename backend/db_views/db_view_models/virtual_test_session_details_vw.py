from django.db import models
from user_management.user_management_models.user_models import User


class VirtualTestSessionDetailsVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    candidate_id = models.IntegerField(db_column="CANDIDATE_ID")
    start_time = models.DateTimeField(db_column="START_TIME")
    date = models.DateField(db_column="DATE")
    end_time = models.DateTimeField(db_column="END_TIME")
    meeting_link = models.CharField(
        db_column="MEETING_LINK", max_length=255, blank=False, null=False
    )
    test_skill_type_en_name = models.CharField(
        db_column="TEST_SKILL_TYPE_EN_NAME", max_length=150, blank=False, null=False
    )
    test_skill_type_fr_name = models.CharField(
        db_column="TEST_SKILL_TYPE_FR_NAME", max_length=150, blank=False, null=False
    )
    test_skill_sub_type_en_name = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_EN_NAME", max_length=150, blank=False, null=False
    )
    test_skill_sub_type_fr_name = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_FR_NAME", max_length=150, blank=False, null=False
    )
    candidate_email = models.EmailField(db_column="CANDIDATE_EMAIL")
    allow_last_minute_cancellation_tc = models.BooleanField(
        db_column="ALLOW_LAST_MINUTE_CANCELLATION_TC"
    )
    test_center_booking_delay = models.IntegerField(
        db_column="TEST_CENTER_BOOKING_DELAY"
    )

    class Meta:
        managed = False
        db_table = "virtual_test_session_details_vw"
