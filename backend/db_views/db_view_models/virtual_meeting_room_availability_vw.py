from django.db import models


class VirtualMeetingRoomAvailabilityVW(models.Model):
    room_id = models.AutoField(primary_key=True)
    room_user_id = models.CharField(max_length=36, blank=False, null=False)
    date = models.DateField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    count = models.IntegerField(db_column="COUNT")

    class Meta:
        managed = False
        db_table = "virtual_meeting_room_availability_vw"
