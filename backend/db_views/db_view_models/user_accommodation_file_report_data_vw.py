from django.db import models


class UserAccommodationFileReportDataVW(models.Model):
    id = models.IntegerField(db_column="id", primary_key=True)
    history_id = models.IntegerField(db_column="history_id", default=1)
    history_date = models.DateTimeField(db_column="history_date")
    is_uit = models.BooleanField(db_column="is_uit")
    created_date = models.DateField(db_column="created_date")
    request_completed_by_aae_date = models.DateField(
        db_column="request_completed_by_aae_date"
    )
    status_id = models.IntegerField(db_column="status_id")
    status_codename = models.CharField(db_column="status_codename", max_length=50)
    status_name_en = models.CharField(
        db_column="status_name_en", max_length=50, default="temp"
    )
    status_name_fr = models.CharField(
        db_column="status_name_fr", max_length=50, default="temp"
    )
    user_id = models.IntegerField(db_column="user_id")
    user_username = models.CharField(db_column="user_username", max_length=254)
    user_email = models.CharField(db_column="user_email", max_length=254)
    user_first_name = models.CharField(db_column="user_first_name", max_length=30)
    user_last_name = models.CharField(db_column="user_last_name", max_length=150)
    assigned_to_user_id = models.IntegerField(db_column="assigned_to_user_id")
    assigned_to_user_email = models.CharField(
        db_column="assigned_to_user_email", max_length=254
    )
    assigned_to_user_first_name = models.CharField(
        db_column="assigned_to_user_first_name", max_length=30
    )
    assigned_to_user_last_name = models.CharField(
        db_column="assigned_to_user_last_name", max_length=150
    )
    assigned_to_phone_number = models.CharField(
        db_column="assigned_to_phone_number", max_length=10, default=None
    )
    hr_user_id = models.IntegerField(db_column="hr_user_id", default=1)
    hr_email = models.EmailField(db_column="hr_email", max_length=254, default="temps")
    hr_first_name = models.CharField(
        db_column="hr_first_name", max_length=30, default="temps"
    )
    hr_last_name = models.CharField(
        db_column="hr_last_name", max_length=150, default="temps"
    )
    hr_phone_number = models.CharField(
        db_column="hr_phone_number", max_length=10, default=None
    )
    ta_user_id = models.IntegerField(db_column="ta_user_id", default=1)
    ta_email = models.EmailField(db_column="ta_email", max_length=254, default="temps")
    ta_first_name = models.CharField(
        db_column="ta_first_name", max_length=30, default="temps"
    )
    ta_last_name = models.CharField(
        db_column="ta_last_name", max_length=150, default="temps"
    )
    ta_phone_number = models.CharField(
        db_column="ta_phone_number", max_length=10, default=None
    )
    other_measures = models.TextField(db_column="other_measures")
    test_skill_type_id = models.IntegerField(db_column="test_skill_type_id")
    test_skill_type_codename = models.CharField(
        db_column="test_skill_type_codename", max_length=25
    )
    test_skill_type_name_en = models.CharField(
        db_column="test_skill_type_name_en", max_length=150
    )
    test_skill_type_name_fr = models.CharField(
        db_column="test_skill_type_name_fr", max_length=150
    )
    test_skill_sub_type_id = models.IntegerField(db_column="test_skill_sub_type_id")
    test_skill_sub_type_codename = models.CharField(
        db_column="test_skill_sub_type_codename", max_length=25
    )
    test_skill_sub_type_name_en = models.CharField(
        db_column="test_skill_sub_type_name_en", max_length=150
    )
    test_skill_sub_type_name_fr = models.CharField(
        db_column="test_skill_sub_type_name_fr", max_length=150
    )
    requesting_dept_id = models.IntegerField(db_column="requesting_dept_id", default=1)
    requesting_dept_abrv_en = models.CharField(
        db_column="requesting_dept_abrv_en", max_length=10, default="temps"
    )
    requesting_dept_abrv_fr = models.CharField(
        db_column="requesting_dept_abrv_fr", max_length=10, default="temps"
    )
    requesting_dept_desc_en = models.CharField(
        db_column="requesting_dept_desc_en", max_length=200, default="temps"
    )
    requesting_dept_desc_fr = models.CharField(
        db_column="requesting_dept_desc_fr", max_length=200, default="temps"
    )
    test_center_name = models.CharField(
        db_column="test_center_name", max_length=150, default="temps"
    )
    test_id = models.IntegerField(db_column="test_id")
    parent_code = models.CharField(db_column="parent_code", max_length=25)
    test_code = models.CharField(db_column="test_code", max_length=25)
    version = models.IntegerField(db_column="version")
    test_name_en = models.CharField(db_column="test_name_en", max_length=175)
    test_name_fr = models.CharField(db_column="test_name_fr", max_length=175)
    specified_test_description = models.CharField(
        db_column="specified_test_description", max_length=175, default=""
    )
    total_default_time = models.IntegerField(db_column="total_default_time")
    total_adjusted_time = models.IntegerField(db_column="total_adjusted_time")
    break_time = models.IntegerField(db_column="break_time")

    class Meta:
        managed = False
        db_table = "user_accommodation_file_report_data_vw"
