from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[scorer_ola_detailed_tests_to_assess_vw] AS
                SELECT
                    TOP 100 PERCENT
                    ts.id,
                    tsd.date,
                    DATEPART(dw, tsd.date) as 'day_of_week_id',
                    tsd.start_time,
                    CONVERT(VARCHAR(8), tsd.start_time, 108) as 'simplified_start_time',
                    tsd.end_time,
                    CONVERT(VARCHAR(8), tsd.end_time, 108) as 'simplified_end_time',
                    IIF(tsd.test_skill_sub_type_id = (SELECT id FROM {db_name}..cms_models_testskillsledesc tssle WHERE tssle.codename = 'oe'),  1, 2) as 'language_id',
                    ts.is_standard,
                    ts.user_accommodation_file_id,
					u.id as 'candidate_user_id',
                    u.email as 'candidate_user_email',
                    u.first_name as 'candidate_user_first_name',
                    u.last_name 'candidate_user_last_name',
                    u.pri as 'candidate_pri',
                    tsd.candidate_phone_number,
                    crc.id as 'consumed_reservation_code_id',
                    crcs.id as 'consumed_reservation_code_status_id',
                    crcs.codename as 'consumed_reservation_code_status_codename',
                    apatsv.id as 'assessment_process_assigned_test_specs_id',
                    apatsv.assessment_process_reference_number,
                    booked_date.history_date as 'booked_date',
                    apatsv.assessment_process_closing_date,
                    apatsv.assessment_process_dept_id,
                    apatsv.assessment_process_dept_eabrv,
                    apatsv.assessment_process_dept_edesc,
                    apatsv.assessment_process_dept_fabrv,
                    apatsv.assessment_process_dept_fdesc,
                    apatsv.hr_user_id,
                    apatsv.hr_email,
                    apatsv.hr_first_name,
                    apatsv.hr_last_name,
                    apatsv.reason_for_testing_id,
                    apatsv.reason_for_testing_minimum_process_length,
                    apatsv.reason_for_testing_name_en,
                    apatsv.reason_for_testing_name_fr,
                    apatsv.level_required,
                    assessor_u.id as 'test_assessor_user_id',
                    assessor_u.email as 'test_assessor_user_email',
                    assessor_u.first_name as 'test_assessor_user_first_name',
                    assessor_u.last_name 'test_assessor_user_last_name',
					vtms.meeting_link 'meeting_link'
                FROM {db_name}..custom_models_testcentertestsessions ts
                JOIN {db_name}..custom_models_testcentertestsessiondata tsd ON tsd.id = ts.test_session_data_id
                LEFT JOIN {db_name}..custom_models_consumedreservationcodes crc ON crc.test_session_id = ts.id
                LEFT JOIN {db_name}..custom_models_consumedreservationcodestatus crcs ON crcs.id = crc.status_id
                LEFT JOIN {db_name}..assessment_process_assigned_test_specs_vw apatsv ON apatsv.id = ts.assessment_process_assigned_test_specs_id
				OUTER APPLY (SELECT crc_2.candidate_id FROM {db_name}..custom_models_consumedreservationcodes crc_2 WHERE crc_2.assessment_process_assigned_test_specs_id = ts.assessment_process_assigned_test_specs_id) cdd_id_from_consumed_reservation_code
				LEFT JOIN {db_name}..user_management_models_user u ON u.id = cdd_id_from_consumed_reservation_code.candidate_id
                LEFT JOIN {db_name}..custom_models_scorerolaassignedtestsession soats ON soats.test_session_id = ts.id
                LEFT JOIN {db_name}..user_management_models_user assessor_u ON assessor_u.id = soats.test_assessor_id
				LEFT JOIN {db_name}..custom_models_virtualteamsmeetingsession vtms ON ts.id = vtms.test_session_id
                OUTER APPLY (SELECT TOP 1 hcrc.history_date FROM {db_name}..custom_models_historicalconsumedreservationcodes hcrc WHERE hcrc.id = crc.id AND hcrc.status_id = (SELECT crcs.id FROM {db_name}..custom_models_consumedreservationcodestatus crcs WHERE crcs.codename = 'booked') ORDER BY hcrc.history_date DESC) booked_date
                -- ONLY GETTING OLA TEST SESSIONS THAT ARE BOOKED/PAIRED OR MULTI-SESSIONS SESSIONS (MULTI-SESSIONS MEANS THAT THIS IS COMING FROM A SCHEDULED ACCOMMODATION)
                WHERE tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype tst WHERE tst.codename = 'sle')
                AND tsd.test_skill_sub_type_id IN (SELECT id FROM {db_name}..cms_models_testskillsledesc tssle WHERE tssle.codename IN ('oe', 'of'))
                AND (crcs.codename IN ('booked', 'paired_ola') OR (SELECT COUNT(*) FROM {db_name}..custom_models_testcentertestsessions ts_2 WHERE ts_2.assessment_process_assigned_test_specs_id = ts.assessment_process_assigned_test_specs_id) > 1)
                -- ORDERING BY HIGHEST PRIORITY (REASON FOR TESTING MINIMUM PROCESS LENGTH FOLLOWED BY ASSESSMENT PROCESS CLOSING DATE FOLLOWED BY BOOKED DATE)
                ORDER BY apatsv.reason_for_testing_minimum_process_length, apatsv.assessment_process_closing_date, booked_date.history_date
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0421_update_scorer_ola_tests_to_assess_view",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
