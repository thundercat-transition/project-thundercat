from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_data_vw] AS
				SELECT 
					at.id as 'assigned_test_id',
					at.status_id as 'test_status_id',
					atst.codename as 'test_status_codename',
					IIF (hta.id is not NULL, hta.id, ta.id) as 'ta_user_id',
					IIF (hta.id is not NULL, hta.username, ta.username) as 'ta_username',
					IIF (hta.id is not NULL, hta.email, ta.email) as 'ta_email',
					IIF (hu.id is not NULL, hu.id, u.id) as 'candidate_user_id',
					IIF (hu.id is not NULL, hu.username, u.username) as 'candidate_username',
					IIF (hu.id is not NULL, hu.email, u.email) as 'candidate_email',
					IIF (hu.id is not NULL, hu.first_name, u.first_name) as 'candidate_first_name',
					IIF (hu.id is not NULL, hu.last_name, u.last_name) as 'candidate_last_name',
					td.id as 'test_id',
					td.en_name as 'en_test_name',
					td.fr_name as 'fr_test_name',
					td.version as 'test_version',
					IIF (htp.id is not NULL, htp.test_order_number, htp2.test_order_number) as 'test_order_number',
					IIF (htp.id is not NULL, htp.staffing_process_number, htp2.staffing_process_number) as 'staffing_process_number',
					htp3.ta_user_ids as 'allowed_ta_user_ids',
					ofd.reference_number as 'reference_number'
				FROM {db_name}..custom_models_assignedtest at
				JOIN {db_name}..custom_models_assignedteststatus atst on atst.id = at.status_id
				OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.id = at.ta_user_id AND u1.history_date < at.submit_date ORDER BY u1.history_date DESC) hta
				JOIN {db_name}..user_management_models_user ta on ta.id = at.ta_user_id
				OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u2 WHERE u2.id = at.user_id AND u2.history_date < at.submit_date ORDER BY u2.history_date DESC) hu
				JOIN {db_name}..user_management_models_user u on u.id = at.user_id
				JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
				OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions htp1 WHERE htp1.test_order_number = at.test_order_number AND htp1.history_date < at.submit_date ORDER BY htp1.history_date DESC) htp
				OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions htp1 WHERE htp1.test_order_number = at.test_order_number ORDER BY htp1.history_date DESC) htp2
				OUTER APPLY (SELECT string_agg(ta_user_id_list.user_id, ',') within group (ORDER BY ta_user_id_list.user_id) as 'ta_user_ids' FROM (SELECT DISTINCT user_id FROM {db_name}..cms_models_historicaltestpermissions htp WHERE htp.history_type = '+' AND htp.test_order_number = at.test_order_number) ta_user_id_list) htp3
				LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """ALTER VIEW [dbo].[test_data_vw] AS


# 			SELECT
# 				at.id as 'assigned_test_id',
# 				at.status_id as 'test_status_id',
# 				atst.codename as 'test_status_codename',
# 				IIF (hta.id is not NULL, hta.id, ta.id) as 'ta_user_id',
# 				IIF (hta.id is not NULL, hta.username, ta.username) as 'ta_username',
# 				IIF (hta.id is not NULL, hta.email, ta.email) as 'ta_email',
# 				IIF (hu.id is not NULL, hu.id, u.id) as 'candidate_user_id',
# 				IIF (hu.id is not NULL, hu.username, u.username) as 'candidate_username',
# 				IIF (hu.id is not NULL, hu.email, u.email) as 'candidate_email',
# 				IIF (hu.id is not NULL, hu.first_name, u.first_name) as 'candidate_first_name',
# 				IIF (hu.id is not NULL, hu.last_name, u.last_name) as 'candidate_last_name',
# 				td.id as 'test_id',
# 				td.en_name as 'en_test_name',
# 				td.fr_name as 'fr_test_name',
# 				td.version as 'test_version',
# 				IIF (htp.id is not NULL, htp.test_order_number, htp2.test_order_number) as 'test_order_number',
# 				IIF (htp.id is not NULL, htp.staffing_process_number, htp2.staffing_process_number) as 'staffing_process_number',
# 				htp3.ta_user_ids as 'allowed_ta_user_ids',
# 				ofd.reference_number as 'reference_number'
# 			FROM {db_name}..custom_models_assignedtest at
# 			JOIN {db_name}..custom_models_assignedteststatus atst on atst.id = at.status_id
# 			OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = at.ta_id AND u1.history_date < at.submit_date ORDER BY u1.history_date DESC) hta
# 			JOIN {db_name}..user_management_models_user ta on ta.username = at.ta_id
# 			OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u2 WHERE u2.username = at.username_id AND u2.history_date < at.submit_date ORDER BY u2.history_date DESC) hu
# 			JOIN {db_name}..user_management_models_user u on u.username = at.username_id
# 			JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
# 			OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions htp1 WHERE htp1.test_order_number = at.test_order_number AND htp1.history_date < at.submit_date ORDER BY htp1.history_date DESC) htp
# 			OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions htp1 WHERE htp1.test_order_number = at.test_order_number ORDER BY htp1.history_date DESC) htp2
# 			OUTER APPLY (SELECT string_agg(ta_user_id_list.user_id, ',') within group (ORDER BY ta_user_id_list.user_id) as 'ta_user_ids' FROM (SELECT DISTINCT user_id FROM {db_name}..cms_models_historicaltestpermissions htp WHERE htp.history_type = '+' AND htp.test_order_number = at.test_order_number) ta_user_id_list) htp3
# 			LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
#     """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0181_update_test_result_report_vw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
