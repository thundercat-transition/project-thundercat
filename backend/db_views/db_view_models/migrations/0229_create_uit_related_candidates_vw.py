from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[uit_related_candidates_vw] AS
                SELECT
                    uirc.id,
                    uirc.first_name,
                    uirc.last_name,
                    uirc.email,
                    uirc.reason_for_deletion_id,
                    ui.id as 'uit_invite_id',
                    ui.invite_date,
                    ui.validity_end_date,
                    ui.orderless_request,
                    ui.ta_user_id,
                    ui.test_id,
                    at.id as 'assigned_test_id',
                    at.test_access_code,
                    at.start_date,
                    at.modify_date,
                    at.submit_date,
                    ats.id as 'status_id',
                    ats.codename as 'status_codename'
                FROM {db_name}..custom_models_uitinviterelatedcandidates uirc
                JOIN {db_name}..custom_models_uitinvites ui on ui.id = uirc.uit_invite_id
                JOIN {db_name}..custom_models_historicalunsupervisedtestaccesscode hutac on hutac.candidate_email = uirc.email AND hutac.uit_invite_id = uirc.uit_invite_id AND hutac.history_type = '+'
                LEFT JOIN {db_name}..custom_models_assignedtest at on at.test_id = ui.test_id AND at.uit_invite_id = uirc.uit_invite_id AND at.test_access_code = hutac.test_access_code
                LEFT JOIN {db_name}..custom_models_assignedteststatus ats on ats.id = at.status_id
                """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0228_uitcompletedprocessesvw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
