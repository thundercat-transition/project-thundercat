# Generated by Django 4.2.11 on 2024-05-02 12:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("db_view_models", "0216_update_assessment_process_vw"),
    ]

    operations = [
        migrations.AddField(
            model_name="assessmentprocessvw",
            name="user_email",
            field=models.CharField(
                db_column="USER_EMAIL", default="temp", max_length=254
            ),
        ),
    ]
