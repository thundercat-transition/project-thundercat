from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[assessment_process_vw] AS
                SELECT
                    ap.id,
                    ap.user_id,
                    ap.reference_number,
                    ap.closing_date,
                    ap.allow_booking_external_tc,
                    ap.request_sent,
                    d1.dept_id,
                    d1.eabrv as 'dept_eabrv',
                    d1.fabrv as 'dept_fabrv',
                    d1.edesc as 'dept_edesc',
                    d1.fdesc as 'dept_fdesc',
                    IIF(bc.id IS NOT NULL, bc.id, NULL) as 'default_billing_contact_id',
                    IIF(bc.id IS NOT NULL, bc.first_name, NULL) as 'default_billing_contact_first_name',
                    IIF(bc.id IS NOT NULL, bc.last_name, NULL) as 'default_billing_contact_last_name',
                    IIF(bc.id IS NOT NULL, bc.email, NULL) as 'default_billing_contact_email',
                    IIF(bc.id IS NOT NULL, bc.fis_organisation_code, NULL) as 'default_billing_contact_fis_organisation_code',
                    IIF(bc.id IS NOT NULL, bc.fis_reference_code, NULL) as 'default_billing_contact_fis_reference_code',
                    IIF(bc.id IS NOT NULL, d2.dept_id, NULL) as 'default_billing_contact_dept_id',
                    IIF(bc.id IS NOT NULL, d2.eabrv, NULL) as 'default_billing_contact_dept_eabrv',
                    IIF(bc.id IS NOT NULL, d2.fabrv, NULL) as 'default_billing_contact_dept_fabrv',
                    IIF(bc.id IS NOT NULL, d2.edesc, NULL) as 'default_billing_contact_dept_edesc',
                    IIF(bc.id IS NOT NULL, d2.fdesc, NULL) as 'default_billing_contact_dept_fdesc'
                FROM {db_name}..custom_models_assessmentprocess ap
                JOIN {db_name}..CAT_REF_DEPARTMENTS_VW d1 on d1.dept_id = ap.department_id
                LEFT JOIN {db_name}..custom_models_billingcontact bc on bc.id = ap.default_billing_contact_id
                LEFT JOIN {db_name}..CAT_REF_DEPARTMENTS_VW d2 on d2.dept_id = bc.department_id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        ("db_view_models", "0082_assessmentprocessdefaulttestspecsvw"),
        ("ref_table_views", "0019_auto_20230719_1432"),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
