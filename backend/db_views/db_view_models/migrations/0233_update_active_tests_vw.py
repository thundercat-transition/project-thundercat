from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[active_tests_vw] AS
				SELECT
                    at.id as 'assigned_test_id',
                    u.id as 'candidate_user_id',
                    u.username as 'candidate_username',
                    u.email as 'candidate_email',
                    u.first_name as 'candidate_first_name',
                    u.last_name as 'candidate_last_name',
                    ta.id as 'ta_user_id',
                    ta.username as 'ta_username',
                    ta.email as 'ta_email',
                    ta.first_name as 'ta_first_name',
                    ta.last_name as 'ta_last_name',
                    at.status_id as 'test_status_id',
                    atst.codename as 'test_status_codename',
                    CASE
                        WHEN tp.test_order_number is not NULL THEN tp.test_order_number
                        WHEN ofd.reference_number is not NULL THEN ofd.reference_number
                        WHEN apatsv.assessment_process_reference_number is not NULL THEN apatsv.assessment_process_reference_number
                        ELSE NULL
                    END as 'test_order_reference_number',
                    td.id as 'test_id',
                    td.test_code
                FROM {db_name}..custom_models_assignedtest at
                JOIN {db_name}..custom_models_assignedteststatus atst on atst.id = at.status_id
                JOIN {db_name}..user_management_models_user u on u.id = at.user_id
                JOIN {db_name}..user_management_models_user ta on ta.id = at.ta_user_id
                LEFT JOIN {db_name}..cms_models_testpermissions tp on tp.test_order_number = at.test_order_number AND tp.user_id = at.ta_user_id
                LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
                LEFT JOIN {db_name}..custom_models_consumedreservationcodes crc on crc.assigned_test_id = at.id
                LEFT JOIN {db_name}..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = crc.assessment_process_assigned_test_specs_id
                LEFT JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
                WHERE at.status_id in (SELECT id FROM {db_name}..custom_models_assignedteststatus WHERE codename in ('assigned', 'checked_in', 'active', 'transition', 'locked', 'paused', 'pre_test'))
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0232_taextendedprofilevw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
