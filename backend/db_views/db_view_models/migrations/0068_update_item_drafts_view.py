from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """ALTER VIEW [dbo].[item_drafts_vw] AS


# 			SELECT
# 				id.item_id as 'item_id',
# 				id.system_id,
# 				id.item_bank_id,
# 				id.modify_date,
# 				id.username_id,
# 				id.active_status,
#                 id.shuffle_options,
# 				id.item_type_id,
# 				ds.id as 'development_status_id',
# 				ds.codename as 'development_status_codename',
# 				ds.en_name as 'development_status_name_en',
# 				ds.fr_name as 'development_status_name_fr',
# 				IIF (rf.id is not NULL, rf.id, NULL) as 'response_format_id',
# 				IIF (rf.codename is not NULL, rf.codename, NULL) as 'response_format_codename',
# 				IIF (rf.en_name is not NULL, rf.en_name, NULL) as 'response_format_name_en',
# 				IIF (rf.fr_name is not NULL, rf.fr_name, NULL) as 'response_format_name_fr',
# 				id.historical_id as 'historical_id'
# 			FROM {db_name}..cms_models_itemdrafts id
# 			JOIN {db_name}..cms_models_itemdevelopmentstatuses ds on ds.id = id.development_status_id
# 			LEFT JOIN {db_name}..cms_models_itemresponseformats rf on rf.id = id.response_format_id
#     """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


def rollback_changes(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """ALTER VIEW [dbo].[item_drafts_vw] AS


# 			SELECT
# 				id.item_id as 'item_id',
# 				id.system_id,
# 				id.item_bank_id,
# 				id.modify_date,
# 				id.username_id,
# 				id.active_status,
#                 id.shuffle_options,
# 				ds.id as 'development_status_id',
# 				ds.codename as 'development_status_codename',
# 				ds.en_name as 'development_status_name_en',
# 				ds.fr_name as 'development_status_name_fr',
# 				rf.id as 'response_format_id',
# 				rf.codename as 'response_format_codename',
# 				rf.en_name as 'response_format_name_en',
# 				rf.fr_name as 'response_format_name_fr',
# 				id.historical_id as 'historical_id'
# 			FROM {db_name}..cms_models_itemdrafts id
# 			JOIN {db_name}..cms_models_itemdevelopmentstatuses ds on ds.id = id.development_status_id
# 			JOIN {db_name}..cms_models_itemresponseformats rf on rf.id = id.response_format_id
#     """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0067_itemlatestversionsdatavw_item_type_codename_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
