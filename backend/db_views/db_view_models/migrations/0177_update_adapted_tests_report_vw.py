from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[adapted_tests_report_vw] AS
				SELECT
                    t1.id as 'assigned_test_id',
                    cdd.last_name as 'candidate_last_name',
                    cdd.first_name as 'candidate_first_name',
                    cdd.email as 'candidate_email',
                    CASE
                        WHEN ordered_test_org_data.DEPT_ID is not NULL THEN ordered_test_org_data.DEPT_ID
                        WHEN orderless_test_org_data.DEPT_ID is not NULL THEN orderless_test_org_data.DEPT_ID
                        WHEN supervised_test_org_data.[assessment_process_dept_id ] is not NULL THEN supervised_test_org_data.assessment_process_dept_id
                    END as 'requesting_organization_dept_id',
                    CASE
                        WHEN ordered_test_org_data.DEPT_ID is not NULL THEN ordered_test_org_data.EDESC
                        WHEN orderless_test_org_data.DEPT_ID is not NULL THEN orderless_test_org_data.EDESC
                        WHEN supervised_test_org_data.[assessment_process_dept_id ] is not NULL THEN supervised_test_org_data.assessment_process_dept_edesc
                    END as 'requesting_organization_dept_edesc',
                    CASE
                        WHEN ordered_test_org_data.DEPT_ID is not NULL THEN ordered_test_org_data.FDESC
                        WHEN orderless_test_org_data.DEPT_ID is not NULL THEN orderless_test_org_data.FDESC
                        WHEN supervised_test_org_data.[assessment_process_dept_id ] is not NULL THEN supervised_test_org_data.assessment_process_dept_fdesc
                    END as 'requesting_organization_dept_fdesc',
                    CASE
                        WHEN ordered_test_org_data.DEPT_ID is not NULL THEN ordered_test_org_data.EABRV
                        WHEN orderless_test_org_data.DEPT_ID is not NULL THEN orderless_test_org_data.EABRV
                        WHEN supervised_test_org_data.[assessment_process_dept_id ] is not NULL THEN supervised_test_org_data.assessment_process_dept_eabrv
                    END as 'requesting_organization_dept_eabrv',
                    CASE
                        WHEN ordered_test_org_data.DEPT_ID is not NULL THEN ordered_test_org_data.FABRV
                        WHEN orderless_test_org_data.DEPT_ID is not NULL THEN orderless_test_org_data.FABRV
                        WHEN supervised_test_org_data.[assessment_process_dept_id ] is not NULL THEN supervised_test_org_data.assessment_process_dept_fabrv
                    END as 'requesting_organization_dept_fabrv',
                    t1.ta_user_id as 'test_administrator_user_id',
					ta_historical_user_data.username as 'test_administrator_username',
                    ta_historical_user_data.last_name as 'test_administrator_last_name', --getting historical user data based on test access code creation date
                    ta_historical_user_data.first_name as 'test_administrator_first_name', --getting historical user data based on test access code creation date
                    ta_historical_permission_data.goc_email as 'test_administrator_goc_email',
                    ta_org_data.DEPT_ID as 'test_administrator_dept_id',
                    ta_org_data.EDESC as 'test_administrator_dept_edesc',
                    ta_org_data.FDESC as 'test_administrator_dept_fdesc',
                    ta_org_data.EABRV as 'test_administrator_dept_eabrv',
                    ta_org_data.FABRV as 'test_administrator_dept_fabrv',
                    t1.test_order_number as 'test_order_number',
	                ofd.reference_number as 'reference_number',
                    ts1.id as 'test_status_id',
                    ts1.codename as 'test_status_codename',
                    t1.is_invalid as 'is_invalid',
                    -- Test start date and time (time when the timed section was actually started ... might get complicated with multi-section tests)
                    calculated_start_time.start_time as 'test_start_time',
                    -- Test Submit date and time (time when the timed section was actually submitted ... might show strange results for users that x-out)
                    calculated_end_time.end_time as 'test_end_time',
                    td.test_code as 'test_code',
                    td.en_name as 'test_description_en',
                    td.fr_name as 'test_description_fr',
                    -- Total time alotted 
                    total_time_allotted_calculations.total_time_seconds as 'total_time_allotted_in_minutes', 
                    -- Time Used / Spent in test
                    ROUND((total_time_used_calculations.total_time_seconds
                        -
                        IIF(locked_data.total_time_in_seconds is NULL, 0, locked_data.total_time_in_seconds)  
                        -
                        IIF((bb.break_time - break_bank_data.new_remaining_time) is null, 0, (bb.break_time - break_bank_data.new_remaining_time))
                    )/CAST(60 as FLOAT), 2) as 'total_time_used_in_minutes',
                    -- Break Bank alotted
                    IIF(ar.break_bank_id is null, 0 , bb.break_time/60) as 'break_bank_allotted_in_minutes',
                    -- Break bank used
                    IIF((bb.break_time - break_bank_data.new_remaining_time) is null, 0, ROUND((bb.break_time - break_bank_data.new_remaining_time)/CAST(60 as FLOAT), 2)) as 'break_bank_used_in_minutes'
                FROM {db_name}..custom_models_assignedtest t1
                JOIN {db_name}..custom_models_assignedteststatus ts1 ON ts1.id = t1.status_id
                JOIN {db_name}..user_management_models_user cdd ON t1.user_id = cdd.id
                OUTER APPLY (SELECT TOP 1 * 
                            FROM {db_name}..user_management_models_historicaluser hu 
                            WHERE hu.id = t1.ta_user_id 
                            AND hu.history_date < IIF(t1.uit_invite_id is not NULL, 
                                                    (SELECT huitac.history_date
                                                    FROM {db_name}..custom_models_historicalunsupervisedtestaccesscode huitac 
                                                    WHERE huitac.uit_invite_id = t1.uit_invite_id 
                                                    AND huitac.test_access_code = t1.test_access_code 
                                                    AND huitac.history_type = '+'
                                                    ),
                                                    (SELECT htac.history_date 
                                                        FROM {db_name}..custom_models_historicaltestaccesscode htac 
                                                        WHERE htac.test_id = t1.test_id 
                                                        AND htac.test_access_code = t1.test_access_code 
                                                        AND htac.history_type = '+'
                                                        )
                            ) ORDER BY hu.history_date DESC
                            ) ta_historical_user_data
                OUTER APPLY (SELECT TOP 1 hup.goc_email 
                            FROM {db_name}..user_management_models_historicalcustomuserpermissions hup 
                            WHERE hup.permission_id = (SELECT cp.permission_id 
                                                        FROM {db_name}..user_management_models_custompermissions cp 
                                                        WHERE cp.codename = 'is_test_administrator')
                                                        AND hup.history_type = '+' 
                                                        AND hup.history_date < IIF(t1.uit_invite_id is not NULL, 
                                                                                (SELECT huitac.history_date
                                                                                FROM {db_name}..custom_models_historicalunsupervisedtestaccesscode huitac 
                                                                                WHERE huitac.uit_invite_id = t1.uit_invite_id 
                                                                                AND huitac.test_access_code = t1.test_access_code 
                                                                                AND huitac.history_type = '+'
                                                                                ),
                                                                                (SELECT htac.history_date 
                                                                                    FROM {db_name}..custom_models_historicaltestaccesscode htac 
                                                                                    WHERE htac.test_id = t1.test_id 
                                                                                    AND htac.test_access_code = t1.test_access_code 
                                                                                    AND htac.history_type = '+'
                                                                                    )
                                                                                )
                            AND hup.user_id = t1.ta_user_id
                            ORDER BY hup.history_date DESC
                            ) ta_historical_permission_data
                OUTER APPLY (SELECT dv.DEPT_ID, dv.EDESC, dv.FDESC, dv.EABRV, dv.FABRV 
                            FROM {db_name}..CAT_REF_DEPARTMENTS_VW dv 
                            WHERE dv.DEPT_ID = (SELECT TOP 1 htep.department_id 
                                                FROM {db_name}..user_management_models_historicaltaextendedprofile htep 
                                                WHERE htep.history_date < IIF(t1.uit_invite_id is not NULL, 
                                                                            (SELECT huitac.history_date
                                                                            FROM {db_name}..custom_models_historicalunsupervisedtestaccesscode huitac 
                                                                            WHERE huitac.uit_invite_id = t1.uit_invite_id 
                                                                            AND huitac.test_access_code = t1.test_access_code 
                                                                            AND huitac.history_type = '+'
                                                                            ),
                                                                            (SELECT htac.history_date 
                                                                            FROM {db_name}..custom_models_historicaltestaccesscode htac 
                                                                            WHERE htac.test_id = t1.test_id 
                                                                            AND htac.test_access_code = t1.test_access_code 
                                                                            AND htac.history_type = '+'
                                                                            )
                                                                            )
                                                AND htep.id = t1.ta_user_id
                                                ORDER BY htep.history_date DESC
                                                )
                            ) ta_org_data
                JOIN {db_name}..cms_models_testdefinition td ON t1.test_id = td.id
                JOIN {db_name}..custom_models_accommodationrequest ar ON t1.accommodation_request_id = ar.id
                LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd ON t1.orderless_financial_data_id = ofd.id
                OUTER APPLY (SELECT TOP 1 atsat.time as 'start_time' 
                                FROM {db_name}..custom_models_assignedtestsectionaccesstimes atsat 
                                WHERE atsat.time_type = 1 
                                AND atsat.assigned_test_section_id = (SELECT TOP 1 ats.id 
                                                                    FROM {db_name}..custom_models_assignedtestsection ats 
                                                                    WHERE ats.assigned_test_id = t1.id AND ats.test_section_time is not NULL
                                                                    ) ORDER BY atsat.time
                                ) calculated_start_time
                OUTER APPLY (SELECT TOP 1 atsat.time as 'end_time' 
                                FROM {db_name}..custom_models_assignedtestsectionaccesstimes atsat 
                                WHERE atsat.time_type = 2 
                                AND atsat.assigned_test_section_id = (SELECT TOP 1 ats.id 
                                                                    FROM {db_name}..custom_models_assignedtestsection ats 
                                                                    WHERE ats.assigned_test_id = t1.id AND ats.test_section_time is not NULL 
                                                                    ORDER BY ats.id DESC
                                ) ORDER BY atsat.time DESC
                                ) calculated_end_time
                OUTER APPLY (SELECT SUM(test_section_time) as total_time_seconds 
                                FROM (SELECT temp_ats2.assigned_test_id, temp_ats2.test_section_time as test_section_time
                                    FROM (SELECT temp_ats1.id, temp_ats1.assigned_test_id , temp_ats1.test_section_time
                                            FROM {db_name}..custom_models_assignedtestsection temp_ats1 
                                            WHERE temp_ats1.assigned_test_id = t1.id AND temp_ats1.test_section_time is not NULL
                                    ) temp_ats2
                                ) x 
                                GROUP BY x.assigned_test_id) total_time_allotted_calculations
                OUTER APPLY (SELECT SUM(diff_time) as total_time_seconds 
                                FROM (SELECT temp_ats2.assigned_test_id, DATEDIFF(SECOND, atsat1.time, atsat2.time) as diff_time 
                                    FROM (SELECT temp_ats1.id, temp_ats1.assigned_test_id 
                                            FROM {db_name}..custom_models_assignedtestsection temp_ats1 
                                            WHERE temp_ats1.assigned_test_id = t1.id AND temp_ats1.test_section_time is not NULL) temp_ats2
                                    CROSS APPLY (SELECT TOP 1 * FROM custom_models_assignedtestsectionaccesstimes a WHERE temp_ats2.id = a.assigned_test_section_id AND a.time_type = 1 ORDER BY a.time ASC) atsat1 
                                    CROSS APPLY (SELECT TOP 1 * FROM custom_models_assignedtestsectionaccesstimes a WHERE temp_ats2.id = a.assigned_test_section_id AND a.time_type = 2 ORDER BY a.time ASC) atsat2
                            ) x 
                            GROUP BY x.assigned_test_id) total_time_used_calculations
                OUTER APPLY (SELECT SUM(DATEDIFF(second, lta.lock_start_date, lta.lock_end_date)) as 'total_time_in_seconds'
                            FROM {db_name}..custom_models_taactions ta
                            LEFT JOIN {db_name}..custom_models_locktestactions lta ON ta.id = lta.ta_action_id 
                            OUTER APPLY (SELECT id as 'timed_test_section_id' FROM {db_name}..cms_models_testsection ts WHERE ts.id in (SELECT ats.test_section_id FROM {db_name}..custom_models_assignedtestsection ats WHERE ats.assigned_test_id = t1.id AND ats.test_section_time is not NULL)) timed_test_section_ids
                            WHERE ta.assigned_test_id = t1.id AND ta.action_type_id in ('LOCK', 'UNLOCK') AND test_section_id in (timed_test_section_ids.timed_test_section_id)) locked_data
                LEFT JOIN {db_name}..custom_models_breakbank bb ON ar.break_bank_id = bb.id
                OUTER APPLY (SELECT TOP 1 bba.new_remaining_time as 'new_remaining_time'
                            FROM {db_name}..custom_models_breakbank sbb
                            LEFT JOIN {db_name}..custom_models_breakbankactions bba ON bba.break_bank_id = sbb.id
                            WHERE sbb.id = bb.id
                            ORDER BY bba.id DESC) break_bank_data
                -- ordered requesting org (old supervised + unsupervised)
                OUTER APPLY (SELECT dv.DEPT_ID, dv.EDESC, dv.FDESC, dv.EABRV, dv.FABRV 
                            FROM {db_name}..CAT_REF_DEPARTMENTS_VW dv 
                            WHERE dv.DEPT_ID = (SELECT TOP 1 IIF(TRY_CAST(htp.department_ministry_code as NUMERIC) is not NULL,
                                                                IIF((SELECT dv1.DEPT_ID FROM {db_name}..CAT_REF_DEPARTMENTS_VW dv1 WHERE dv1.DEPT_ID = CAST(htp.department_ministry_code as NUMERIC)) is not NULL,
                                                                    (SELECT dv1.DEPT_ID FROM {db_name}..CAT_REF_DEPARTMENTS_VW dv1 WHERE dv1.DEPT_ID = CAST(htp.department_ministry_code as NUMERIC)),
                                                                    104 -- defaulting to PSC
                                                                ),
                                                                104) -- defaulting to PSC
                                                FROM cms_models_historicaltestpermissions htp WHERE htp.test_order_number = t1.test_order_number AND htp.history_type = '+')) ordered_test_org_data
                -- orderless org data
                OUTER APPLY (SELECT dv.DEPT_ID, dv.EDESC, dv.FDESC, dv.EABRV, dv.FABRV FROM {db_name}..CAT_REF_DEPARTMENTS_VW dv WHERE dv.DEPT_ID = ofd.department_ministry_id) orderless_test_org_data
                -- supervised requesting org
                OUTER APPLY (SELECT apatpv.[assessment_process_dept_id ], apatpv.assessment_process_dept_edesc, apatpv.assessment_process_dept_fdesc, apatpv.assessment_process_dept_eabrv, apatpv.assessment_process_dept_fabrv FROM {db_name}..assessment_process_assigned_test_specs_vw apatpv WHERE id = (SELECT TOP 1 crc.assessment_process_assigned_test_specs_id FROM {db_name}..custom_models_historicalconsumedreservationcodes crc WHERE crc.assigned_test_id = t1.id ORDER BY crc.history_date ASC)) supervised_test_org_data
                WHERE t1.accommodation_request_id is not null
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """ALTER VIEW [dbo].[adapted_tests_report_vw] AS


# 			SELECT
#                 t1.id as 'assigned_test_id',
#                 cdd.last_name as 'candidate_last_name',
#                 cdd.first_name as 'candidate_first_name',
#                 cdd.email as 'candidate_email',
#                 CASE
#                     WHEN ordered_test_org_data.DEPT_ID is not NULL THEN ordered_test_org_data.DEPT_ID
#                     WHEN orderless_test_org_data.DEPT_ID is not NULL THEN orderless_test_org_data.DEPT_ID
#                     WHEN supervised_test_org_data.[assessment_process_dept_id ] is not NULL THEN supervised_test_org_data.assessment_process_dept_id
#                 END as 'requesting_organization_dept_id',
#                 CASE
#                     WHEN ordered_test_org_data.DEPT_ID is not NULL THEN ordered_test_org_data.EDESC
#                     WHEN orderless_test_org_data.DEPT_ID is not NULL THEN orderless_test_org_data.EDESC
#                     WHEN supervised_test_org_data.[assessment_process_dept_id ] is not NULL THEN supervised_test_org_data.assessment_process_dept_edesc
#                 END as 'requesting_organization_dept_edesc',
#                 CASE
#                     WHEN ordered_test_org_data.DEPT_ID is not NULL THEN ordered_test_org_data.FDESC
#                     WHEN orderless_test_org_data.DEPT_ID is not NULL THEN orderless_test_org_data.FDESC
#                     WHEN supervised_test_org_data.[assessment_process_dept_id ] is not NULL THEN supervised_test_org_data.assessment_process_dept_fdesc
#                 END as 'requesting_organization_dept_fdesc',
#                 CASE
#                     WHEN ordered_test_org_data.DEPT_ID is not NULL THEN ordered_test_org_data.EABRV
#                     WHEN orderless_test_org_data.DEPT_ID is not NULL THEN orderless_test_org_data.EABRV
#                     WHEN supervised_test_org_data.[assessment_process_dept_id ] is not NULL THEN supervised_test_org_data.assessment_process_dept_eabrv
#                 END as 'requesting_organization_dept_eabrv',
#                 CASE
#                     WHEN ordered_test_org_data.DEPT_ID is not NULL THEN ordered_test_org_data.FABRV
#                     WHEN orderless_test_org_data.DEPT_ID is not NULL THEN orderless_test_org_data.FABRV
#                     WHEN supervised_test_org_data.[assessment_process_dept_id ] is not NULL THEN supervised_test_org_data.assessment_process_dept_fabrv
#                 END as 'requesting_organization_dept_fabrv',
#                 t1.ta_id as 'test_administrator_username',
#                 ta_historical_user_data.last_name as 'test_administrator_last_name', --getting historical user data based on test access code creation date
#                 ta_historical_user_data.first_name as 'test_administrator_first_name', --getting historical user data based on test access code creation date
#                 ta_historical_permission_data.goc_email as 'test_administrator_goc_email',
#                 ta_org_data.DEPT_ID as 'test_administrator_dept_id',
#                 ta_org_data.EDESC as 'test_administrator_dept_edesc',
#                 ta_org_data.FDESC as 'test_administrator_dept_fdesc',
#                 ta_org_data.EABRV as 'test_administrator_dept_eabrv',
#                 ta_org_data.FABRV as 'test_administrator_dept_fabrv',
#                 t1.test_order_number as 'test_order_number',
#                 ofd.reference_number as 'reference_number',
#                 ts1.id as 'test_status_id',
#                 ts1.codename as 'test_status_codename',
#                 t1.is_invalid as 'is_invalid',
#                 -- Test start date and time (time when the timed section was actually started ... might get complicated with multi-section tests)
#                 calculated_start_time.start_time as 'test_start_time',
#                 -- Test Submit date and time (time when the timed section was actually submitted ... might show strange results for users that x-out)
#                 calculated_end_time.end_time as 'test_end_time',
#                 td.test_code as 'test_code',
#                 td.en_name as 'test_description_en',
#                 td.fr_name as 'test_description_fr',
#                 -- Total time alotted
#                 total_time_allotted_calculations.total_time_seconds as 'total_time_allotted_in_minutes',
#                 -- Time Used / Spent in test
#                 ROUND((total_time_used_calculations.total_time_seconds
#                     -
#                     IIF(locked_data.total_time_in_seconds is NULL, 0, locked_data.total_time_in_seconds)
#                     -
#                     IIF((bb.break_time - break_bank_data.new_remaining_time) is null, 0, (bb.break_time - break_bank_data.new_remaining_time))
#                 )/CAST(60 as FLOAT), 2) as 'total_time_used_in_minutes',
#                 -- Break Bank alotted
#                 IIF(ar.break_bank_id is null, 0 , bb.break_time/60) as 'break_bank_allotted_in_minutes',
#                 -- Break bank used
#                 IIF((bb.break_time - break_bank_data.new_remaining_time) is null, 0, ROUND((bb.break_time - break_bank_data.new_remaining_time)/CAST(60 as FLOAT), 2)) as 'break_bank_used_in_minutes'
#             FROM {db_name}..custom_models_assignedtest t1
#             JOIN {db_name}..custom_models_assignedteststatus ts1 ON ts1.id = t1.status_id
#             JOIN {db_name}..user_management_models_user cdd ON t1.username_id = cdd.username
#             OUTER APPLY (SELECT TOP 1 *
#                         FROM {db_name}..user_management_models_historicaluser hu
#                         WHERE hu.username = t1.ta_id
#                         AND hu.history_date < IIF(t1.uit_invite_id is not NULL,
#                                                 (SELECT huitac.history_date
#                                                 FROM {db_name}..custom_models_historicalunsupervisedtestaccesscode huitac
#                                                 WHERE huitac.uit_invite_id = t1.uit_invite_id
#                                                 AND huitac.test_access_code = t1.test_access_code
#                                                 AND huitac.history_type = '+'
#                                                 ),
#                                                 (SELECT htac.history_date
#                                                     FROM {db_name}..custom_models_historicaltestaccesscode htac
#                                                     WHERE htac.test_id = t1.test_id
#                                                     AND htac.test_access_code = t1.test_access_code
#                                                     AND htac.history_type = '+'
#                                                     )
#                         ) ORDER BY hu.history_date DESC
#                         ) ta_historical_user_data
#             OUTER APPLY (SELECT TOP 1 hup.goc_email
#                         FROM {db_name}..user_management_models_historicalcustomuserpermissions hup
#                         WHERE hup.permission_id = (SELECT cp.permission_id
#                                                     FROM {db_name}..user_management_models_custompermissions cp
#                                                     WHERE cp.codename = 'is_test_administrator')
#                                                     AND hup.history_type = '+'
#                                                     AND hup.history_date < IIF(t1.uit_invite_id is not NULL,
#                                                                             (SELECT huitac.history_date
#                                                                             FROM {db_name}..custom_models_historicalunsupervisedtestaccesscode huitac
#                                                                             WHERE huitac.uit_invite_id = t1.uit_invite_id
#                                                                             AND huitac.test_access_code = t1.test_access_code
#                                                                             AND huitac.history_type = '+'
#                                                                             ),
#                                                                             (SELECT htac.history_date
#                                                                                 FROM {db_name}..custom_models_historicaltestaccesscode htac
#                                                                                 WHERE htac.test_id = t1.test_id
#                                                                                 AND htac.test_access_code = t1.test_access_code
#                                                                                 AND htac.history_type = '+'
#                                                                                 )
#                                                                             )
#                         AND hup.user_id = t1.ta_id
#                         ORDER BY hup.history_date DESC
#                         ) ta_historical_permission_data
#             OUTER APPLY (SELECT dv.DEPT_ID, dv.EDESC, dv.FDESC, dv.EABRV, dv.FABRV
#                         FROM {db_name}..CAT_REF_DEPARTMENTS_VW dv
#                         WHERE dv.DEPT_ID = (SELECT TOP 1 htep.department_id
#                                             FROM {db_name}..user_management_models_historicaltaextendedprofile htep
#                                             WHERE htep.history_date < IIF(t1.uit_invite_id is not NULL,
#                                                                         (SELECT huitac.history_date
#                                                                         FROM {db_name}..custom_models_historicalunsupervisedtestaccesscode huitac
#                                                                         WHERE huitac.uit_invite_id = t1.uit_invite_id
#                                                                         AND huitac.test_access_code = t1.test_access_code
#                                                                         AND huitac.history_type = '+'
#                                                                         ),
#                                                                         (SELECT htac.history_date
#                                                                         FROM {db_name}..custom_models_historicaltestaccesscode htac
#                                                                         WHERE htac.test_id = t1.test_id
#                                                                         AND htac.test_access_code = t1.test_access_code
#                                                                         AND htac.history_type = '+'
#                                                                         )
#                                                                         )
#                                             AND htep.username_id = t1.ta_id
#                                             ORDER BY htep.history_date DESC
#                                             )
#                         ) ta_org_data
#             JOIN {db_name}..cms_models_testdefinition td ON t1.test_id = td.id
#             JOIN {db_name}..custom_models_accommodationrequest ar ON t1.accommodation_request_id = ar.id
#             LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd ON t1.orderless_financial_data_id = ofd.id
#             OUTER APPLY (SELECT TOP 1 atsat.time as 'start_time'
#                             FROM {db_name}..custom_models_assignedtestsectionaccesstimes atsat
#                             WHERE atsat.time_type = 1
#                             AND atsat.assigned_test_section_id = (SELECT TOP 1 ats.id
#                                                                 FROM {db_name}..custom_models_assignedtestsection ats
#                                                                 WHERE ats.assigned_test_id = t1.id AND ats.test_section_time is not NULL
#                                                                 ) ORDER BY atsat.time
#                             ) calculated_start_time
#             OUTER APPLY (SELECT TOP 1 atsat.time as 'end_time'
#                             FROM {db_name}..custom_models_assignedtestsectionaccesstimes atsat
#                             WHERE atsat.time_type = 2
#                             AND atsat.assigned_test_section_id = (SELECT TOP 1 ats.id
#                                                                 FROM {db_name}..custom_models_assignedtestsection ats
#                                                                 WHERE ats.assigned_test_id = t1.id AND ats.test_section_time is not NULL
#                                                                 ORDER BY ats.id DESC
#                             ) ORDER BY atsat.time DESC
#                             ) calculated_end_time
#             OUTER APPLY (SELECT SUM(test_section_time) as total_time_seconds
#                             FROM (SELECT temp_ats2.assigned_test_id, temp_ats2.test_section_time as test_section_time
#                                 FROM (SELECT temp_ats1.id, temp_ats1.assigned_test_id , temp_ats1.test_section_time
#                                         FROM {db_name}..custom_models_assignedtestsection temp_ats1
#                                         WHERE temp_ats1.assigned_test_id = t1.id AND temp_ats1.test_section_time is not NULL
#                                 ) temp_ats2
#                             ) x
#                             GROUP BY x.assigned_test_id) total_time_allotted_calculations
#             OUTER APPLY (SELECT SUM(diff_time) as total_time_seconds
#                             FROM (SELECT temp_ats2.assigned_test_id, DATEDIFF(SECOND, atsat1.time, atsat2.time) as diff_time
#                                 FROM (SELECT temp_ats1.id, temp_ats1.assigned_test_id
#                                         FROM {db_name}..custom_models_assignedtestsection temp_ats1
#                                         WHERE temp_ats1.assigned_test_id = t1.id AND temp_ats1.test_section_time is not NULL) temp_ats2
#                                 CROSS APPLY (SELECT TOP 1 * FROM custom_models_assignedtestsectionaccesstimes a WHERE temp_ats2.id = a.assigned_test_section_id AND a.time_type = 1 ORDER BY a.time ASC) atsat1
#                                 CROSS APPLY (SELECT TOP 1 * FROM custom_models_assignedtestsectionaccesstimes a WHERE temp_ats2.id = a.assigned_test_section_id AND a.time_type = 2 ORDER BY a.time ASC) atsat2
#                         ) x
#                         GROUP BY x.assigned_test_id) total_time_used_calculations
#             OUTER APPLY (SELECT SUM(DATEDIFF(second, lta.lock_start_date, lta.lock_end_date)) as 'total_time_in_seconds'
#                         FROM {db_name}..custom_models_taactions ta
#                         LEFT JOIN {db_name}..custom_models_locktestactions lta ON ta.id = lta.ta_action_id
#                         OUTER APPLY (SELECT id as 'timed_test_section_id' FROM {db_name}..cms_models_testsection ts WHERE ts.id in (SELECT ats.test_section_id FROM {db_name}..custom_models_assignedtestsection ats WHERE ats.assigned_test_id = t1.id AND ats.test_section_time is not NULL)) timed_test_section_ids
#                         WHERE ta.assigned_test_id = t1.id AND ta.action_type_id in ('LOCK', 'UNLOCK') AND test_section_id in (timed_test_section_ids.timed_test_section_id)) locked_data
#             LEFT JOIN {db_name}..custom_models_breakbank bb ON ar.break_bank_id = bb.id
#             OUTER APPLY (SELECT TOP 1 bba.new_remaining_time as 'new_remaining_time'
#                         FROM {db_name}..custom_models_breakbank sbb
#                         LEFT JOIN {db_name}..custom_models_breakbankactions bba ON bba.break_bank_id = sbb.id
#                         WHERE sbb.id = bb.id
#                         ORDER BY bba.id DESC) break_bank_data
#             -- ordered requesting org (old supervised + unsupervised)
#             OUTER APPLY (SELECT dv.DEPT_ID, dv.EDESC, dv.FDESC, dv.EABRV, dv.FABRV
#                         FROM {db_name}..CAT_REF_DEPARTMENTS_VW dv
#                         WHERE dv.DEPT_ID = (SELECT TOP 1 IIF(TRY_CAST(htp.department_ministry_code as NUMERIC) is not NULL,
#                                                             IIF((SELECT dv1.DEPT_ID FROM {db_name}..CAT_REF_DEPARTMENTS_VW dv1 WHERE dv1.DEPT_ID = CAST(htp.department_ministry_code as NUMERIC)) is not NULL,
#                                                                 (SELECT dv1.DEPT_ID FROM {db_name}..CAT_REF_DEPARTMENTS_VW dv1 WHERE dv1.DEPT_ID = CAST(htp.department_ministry_code as NUMERIC)),
#                                                                 104 -- defaulting to PSC
#                                                             ),
#                                                             104) -- defaulting to PSC
#                                             FROM cms_models_historicaltestpermissions htp WHERE htp.test_order_number = t1.test_order_number AND htp.history_type = '+')) ordered_test_org_data
#             -- orderless org data
#             OUTER APPLY (SELECT dv.DEPT_ID, dv.EDESC, dv.FDESC, dv.EABRV, dv.FABRV FROM {db_name}..CAT_REF_DEPARTMENTS_VW dv WHERE dv.DEPT_ID = ofd.department_ministry_id) orderless_test_org_data
#             -- supervised requesting org
#             OUTER APPLY (SELECT apatpv.[assessment_process_dept_id ], apatpv.assessment_process_dept_edesc, apatpv.assessment_process_dept_fdesc, apatpv.assessment_process_dept_eabrv, apatpv.assessment_process_dept_fabrv FROM {db_name}..assessment_process_assigned_test_specs_vw apatpv WHERE id = (SELECT TOP 1 crc.assessment_process_assigned_test_specs_id FROM {db_name}..custom_models_historicalconsumedreservationcodes crc WHERE crc.assigned_test_id = t1.id ORDER BY crc.history_date ASC)) supervised_test_org_data
#             WHERE t1.accommodation_request_id is not null
#     """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0176_tahistoryreportvw_candidate_user_id_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
