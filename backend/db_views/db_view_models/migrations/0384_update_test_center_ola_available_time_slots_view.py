from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER FUNCTION get_nbr_of_reserved_test_assessors_by_date_and_time_fn (@date date, @start_time time, @end_time time, @language_id int, @reason_for_testing_priority_codename varchar(50))
                RETURNS TABLE
                AS RETURN
                SELECT
                    TOP 100 PERCENT
                    COUNT(ts.id) as 'nbr_of_reserved_assessors'
                FROM {db_name}..custom_models_testcentertestsessions ts
                JOIN {db_name}..custom_models_testcentertestsessiondata tsd on tsd.id = ts.test_session_data_id
				JOIN {db_name}..cms_models_testskillsledesc sle_desc on sle_desc.id = tsd.test_skill_sub_type_id
				JOIN {db_name}..custom_models_assessmentprocessassignedtestspecs apats on apats.id = ts.assessment_process_assigned_test_specs_id
				JOIN {db_name}..custom_models_reasonfortesting rft on rft.id = apats.reason_for_testing_id
				JOIN {db_name}..custom_models_reasonfortestingpriority rftp on rftp.id = rft.reason_for_testing_priority_id
                WHERE tsd.date = @date AND tsd.test_skill_sub_type_id = IIF(@language_id = 1, (SELECT id FROM {db_name}..cms_models_testskillsledesc WHERE codename = 'oe'), (SELECT id FROM {db_name}..cms_models_testskillsledesc WHERE codename = 'of'))
                AND CAST(tsd.start_time as time) = @start_time 
                AND CAST(tsd.end_time as time) = @end_time
				AND rftp.codename = @reason_for_testing_priority_codename
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """CREATE OR ALTER FUNCTION get_test_assessor_availabilities_by_date_fn (@date date, @test_center_id int)
                RETURNS TABLE
                AS RETURN
                SELECT
                    TOP 100 PERCENT
                    tcts.start_time,
                    tcts.end_time,
                    tcts.day_of_week_id,
                    tcts.assessed_language_id,
                    SUM(IIF(nbr_of_available_test_assessors_high_priority.id IS NULL, 0, nbr_of_available_test_assessors_high_priority.count)) as 'available_test_assessors_high_priority',
					SUM(IIF(nbr_of_available_test_assessors_medium_priority.id IS NULL, 0, nbr_of_available_test_assessors_medium_priority.count)) as 'available_test_assessors_medium_priority',
					SUM(IIF(nbr_of_available_test_assessors_low_priority.id IS NULL, 0, nbr_of_available_test_assessors_low_priority.count)) as 'available_test_assessors_low_priority',
					SUM(IIF(nbr_of_available_test_assessors_high_priority.id IS NULL, 0, nbr_of_available_test_assessors_high_priority.count))
					+ SUM(IIF(nbr_of_available_test_assessors_medium_priority.id IS NULL, 0, nbr_of_available_test_assessors_medium_priority.count)) 
					+ SUM(IIF(nbr_of_available_test_assessors_low_priority.id IS NULL, 0, nbr_of_available_test_assessors_low_priority.count)) 
					as 'available_test_assessors_total'
                FROM {db_name}..custom_models_testcenterolatimeslot tcts
                JOIN {db_name}..custom_models_testcenter tc on tc.id = tcts.test_center_id
                -- GETTING NUMBER OF AVAILABLE ASSESSORS FOR HIGH PRIORITY
                OUTER APPLY (SELECT tcotsp.id, FLOOR(tcotsp.percentage * tcts.slots_to_prioritize / 100) as 'count' 
                            FROM {db_name}..custom_models_testcenterolatimeslotprioritization tcotsp
                            WHERE tcotsp.test_center_ola_time_slot_id = tcts.id
							AND tcotsp.reason_for_testing_priority_id = (SELECT rftp.id FROM {db_name}..custom_models_reasonfortestingpriority rftp WHERE rftp.codename = 'high')
                            ) nbr_of_available_test_assessors_high_priority
                -- GETTING NUMBER OF AVAILABLE ASSESSORS FOR MEDIUM PRIORITY
                OUTER APPLY (SELECT tcotsp.id, FLOOR(tcotsp.percentage * tcts.slots_to_prioritize / 100) as 'count' 
                            FROM {db_name}..custom_models_testcenterolatimeslotprioritization tcotsp
                            WHERE tcotsp.test_center_ola_time_slot_id = tcts.id
							AND tcotsp.reason_for_testing_priority_id = (SELECT rftp.id FROM {db_name}..custom_models_reasonfortestingpriority rftp WHERE rftp.codename = 'medium')
                            ) nbr_of_available_test_assessors_medium_priority
                -- GETTING NUMBER OF AVAILABLE ASSESSORS FOR LOW PRIORITY
                OUTER APPLY (SELECT tcotsp.id, FLOOR(tcotsp.percentage * tcts.slots_to_prioritize / 100) as 'count' 
                            FROM {db_name}..custom_models_testcenterolatimeslotprioritization tcotsp
                            WHERE tcotsp.test_center_ola_time_slot_id = tcts.id
							AND tcotsp.reason_for_testing_priority_id = (SELECT rftp.id FROM {db_name}..custom_models_reasonfortestingpriority rftp WHERE rftp.codename = 'low')
                            ) nbr_of_available_test_assessors_low_priority
                -- ONLY CONSIDERING RESPECTIVE AUTHORIZED OLA TEST CENTERS WHERE DAY OF WEEK MATCHES THE PROVIDED DATE
                WHERE tc.ola_authorized = 1 AND tcts.test_center_id = @test_center_id AND tcts.day_of_week_id = DATEPART(dw, @date) - 1
                GROUP BY tcts.start_time, tcts.end_time, tcts.day_of_week_id, tcts.assessed_language_id
                ORDER BY tcts.day_of_week_id
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """CREATE OR ALTER FUNCTION get_ola_available_time_slots_fn ()
                RETURNS @ola_available_time_slots table(id int Primary Key IDENTITY(1,1), date date, start_time time, end_time time, day_of_week_id int, assessed_language_id int, available_test_assessors_high_priority int, available_test_assessors_medium_priority int, available_test_assessors_low_priority int, available_test_assessors_total int)
                AS
                BEGIN
                    -- DECLARING ALL NEEDED TABLES/VARIABLES
                    -- TABLES
                    DECLARE @test_center_availability table(id int Primary Key IDENTITY(1,1), test_center_id int, nbr_of_days int)
                    -- VARIABLES
                    DECLARE @i int
                    DECLARE @test_center_availability_id int
                    DECLARE @nbr_of_days int
                    DECLARE @current_date date
                    DECLARE @test_center_id int

                    -- CREATING @test_center_availability TABLE
                    INSERT INTO @test_center_availability 
                    SELECT tcoc.test_center_id, tcoc.advanced_booking_delay 
                    FROM {db_name}..custom_models_testcenterolaconfigs tcoc

                    -- INFINITE LOOP
                    WHILE 1 = 1
                    BEGIN
                        -- SETTING NEEDED VARIABLES
                        SET @i = 0
                        SET @current_date = GETDATE()
                        SET @test_center_availability_id = NULL
                    
                        -- SETTING @test_center_availability_id, @test_center_id AND @nbr_of_days BASED ON @test_center_availability TABLE
                        SELECT TOP 1 @test_center_availability_id = id, @test_center_id = test_center_id, @nbr_of_days = nbr_of_days 
                        FROM @test_center_availability 
                        ORDER BY id

                        -- NO MORE DATA
                        IF @test_center_availability_id IS NULL
                            -- BREAKING THE LOOP
                            BREAK

                        -- LOOPING FROM 0 TO PROVIDED NUMBER OF DAYS (TEST CENTER OLA CONFIGS - ADVANCED BOOKING DELAY)
                        WHILE @i < @nbr_of_days
                        BEGIN
                            -- POPULATING @ola_available_time_slots TABLE
                            INSERT INTO @ola_available_time_slots
                            SELECT 
                                @current_date,
                                start_time,
                                end_time,
                                day_of_week_id,
                                assessed_language_id,
								(SELECT available_test_assessors FROM {db_name}..handle_vacation_block_availability_by_date_fn(@test_center_id, @current_date, assessed_language_id, available_test_assessors_high_priority)) as 'available_test_assessors_high_priority',
								(SELECT available_test_assessors FROM {db_name}..handle_vacation_block_availability_by_date_fn(@test_center_id, @current_date, assessed_language_id, available_test_assessors_medium_priority)) as 'available_test_assessors_medium_priority',
								(SELECT available_test_assessors FROM {db_name}..handle_vacation_block_availability_by_date_fn(@test_center_id, @current_date, assessed_language_id, available_test_assessors_low_priority)) as 'available_test_assessors_low_priority',
								(SELECT available_test_assessors FROM {db_name}..handle_vacation_block_availability_by_date_fn(@test_center_id, @current_date, assessed_language_id, available_test_assessors_total)) as 'available_test_assessors_total'
                            FROM {db_name}..get_test_assessor_availabilities_by_date_fn(@current_date, @test_center_id)

                            -- ADDING 1 TO @i VARIABLE
                            SET @i = @i + 1
                            -- ADDING 1 DAY TO @current_date VARIABLE
                            SET @current_date = DATEADD(DAY, 1, @current_date)
                        END

                        -- DELETING RESPECTIVE ROW FROM @test_center_availability TABLE
                        DELETE FROM @test_center_availability WHERE id = @test_center_availability_id
                    END
                    RETURN
                END
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_center_ola_available_time_slots_vw] AS
                SELECT
                    TOP 100 PERCENT
                    -- generating ID by converting the datetime in timestamp
                    DATEDIFF(SECOND, '1970-01-01', (CONCAT(CONCAT(fn.date, ' '), fn.start_time))) as 'id',
                    fn.date,
                    fn.start_time, 
                    fn.end_time,
                    fn.day_of_week_id,
                    fn.assessed_language_id,
                    SUM(fn.available_test_assessors_high_priority) as 'available_test_assessors_high_priority',
					nbr_of_reserved_assessors_high_priority.count as 'nbr_of_reserved_assessors_high_priority',
					SUM(fn.available_test_assessors_high_priority) - nbr_of_reserved_assessors_high_priority.count as 'remaining_available_test_assessors_high_priority',
					SUM(fn.available_test_assessors_medium_priority) as 'available_test_assessors_medium_priority',
					nbr_of_reserved_assessors_medium_priority.count 'nbr_of_reserved_assessors_medium_priority',
					SUM(available_test_assessors_medium_priority) - SUM(nbr_of_reserved_assessors_medium_priority.count) as 'remaining_available_test_assessors_medium_priority',
					SUM(fn.available_test_assessors_low_priority) as 'available_test_assessors_low_priority',
					nbr_of_reserved_assessors_low_priority.count as 'nbr_of_reserved_assessors_low_priority',
					SUM(fn.available_test_assessors_low_priority) - SUM(nbr_of_reserved_assessors_low_priority.count) as 'remaining_available_test_assessors_low_priority',
					SUM(fn.available_test_assessors_total) as 'available_test_assessors_total',
					SUM(fn.available_test_assessors_total) - nbr_of_reserved_assessors_high_priority.count - nbr_of_reserved_assessors_medium_priority.count - nbr_of_reserved_assessors_low_priority.count as 'remaining_available_test_assessors_total'
                FROM {db_name}..get_ola_available_time_slots_fn() fn
                OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM {db_name}..get_nbr_of_reserved_test_assessors_by_date_and_time_fn(date, start_time, end_time, fn.assessed_language_id, 'high')) nbr_of_reserved_assessors_high_priority
				OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM {db_name}..get_nbr_of_reserved_test_assessors_by_date_and_time_fn(date, start_time, end_time, fn.assessed_language_id, 'medium')) nbr_of_reserved_assessors_medium_priority
				OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM {db_name}..get_nbr_of_reserved_test_assessors_by_date_and_time_fn(date, start_time, end_time, fn.assessed_language_id, 'low')) nbr_of_reserved_assessors_low_priority
                OUTER APPLY (SELECT MAX(tcoc.booking_delay) as 'booking_delay' FROM {db_name}..custom_models_testcenterolaconfigs tcoc WHERE tcoc.test_center_id in (SELECT tcots.test_center_id FROM {db_name}..custom_models_testcenterolatimeslot tcots WHERE tcots.start_time = fn.start_time AND tcots.end_time = fn.end_time AND tcots.day_of_week_id = fn.day_of_week_id)) greatest_booking_delay_found
                -- slots that have/had at least one available test assessor (one available spot)
                WHERE fn.available_test_assessors_total > 0 
                /* KNOWN ISSUE:
                    - depending on the timezone, the date might be wrong ==> 2024-09-24 - 02:00-03:00 UTC | 2024-09-24 - 22:00-00:00 -4 !== 2024-09-24 - 01:00-02:00 -1 (should have been the 25th - 01:00-02:00)
                    - if booking delay is less than 24 hours and the candidate tries to get late sessions on the same day (01:00-02:00 UTC for example), the logic might filter it out because the date is not converted in UTC, just the start time and end time
                */
                -- start_time_as_datetime (ex: CAST('2024-09-18 19:00:00' as datetime)) >= current UTC datetime + greatest_booking_delay_found
                AND CAST(CONCAT(CONCAT(fn.date, ' '), LEFT (fn.start_time, 8)) as datetime) >= DATEADD(HOUR, greatest_booking_delay_found.booking_delay, GETUTCDATE())
                GROUP BY date, start_time, end_time, day_of_week_id, assessed_language_id, nbr_of_reserved_assessors_high_priority.count, nbr_of_reserved_assessors_medium_priority.count, nbr_of_reserved_assessors_low_priority.count, greatest_booking_delay_found.booking_delay
                ORDER BY fn.date, fn.start_time ASC

    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0383_testcenterolatimeslotsvw_number_of_available_assessors_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
