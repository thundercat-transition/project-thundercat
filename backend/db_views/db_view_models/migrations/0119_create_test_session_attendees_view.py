from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_session_attendees_vw] AS
                SELECT
                    crc.id,
                    crc.reservation_code,
                    crc.status_id,
                    crc.test_session_id,
                    crc.assigned_test_id,
                    crc.assessment_process_assigned_test_specs_id,
                    atsv.first_name,
                    atsv.last_name,
                    atsv.email,
                    atsv.assessment_process_id,
                    u.id as 'cat_user_id',
                    u.first_name as 'cat_user_first_name',
                    u.last_name as 'cat_user_last_name',
                    u.email 'cat_user_email'
                FROM {db_name}..custom_models_consumedreservationcodes crc
                JOIN {db_name}..assessment_process_assigned_test_specs_vw atsv on atsv.id = crc.assessment_process_assigned_test_specs_id
                JOIN {db_name}..user_management_models_user u on u.id = crc.candidate_id
                WHERE status_id = 1
            """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0118_assessmentprocessresultscandidatesvw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
