from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[scored_tests_vw] AS
    #             SELECT
    #                 at.id,
    #                 at.start_date,
    #                 CONVERT(VARCHAR(10), at.start_date, 23) as 'simplified_start_date',
    #                 at.modify_date,
    #                 at.submit_date,
    #                 at.test_access_code,
    #                 at.total_score,
    #                 td.id as 'test_id',
    #                 td.parent_code,
    #                 td.test_code,
    #                 td.en_name as 'test_name_en',
    #                 td.fr_name as 'test_name_fr',
    #                 td.retest_period,
    #                 td.show_score,


#                 td.show_result,
#                 at.test_session_language_id,
#                 at.test_order_number,
#                 at.en_converted_score,
#                 at.fr_converted_score,
#                 at.is_invalid,
#                 at.uit_invite_id,
#                 at.orderless_financial_data_id,
#                 at.accommodation_request_id,
#                 at.test_session_id,
#                 ats.id as 'status_id',
#                 ats.codename as 'status_codename',
#                 at.ta_user_id,
#                 u.id as 'candidate_user_id',
#                 u.email as 'candidate_email',
#                 u.first_name as 'candidate_first_name',
#                 u.last_name as 'candidate_last_name',
#                 u.birth_date as 'candidate_dob',
#                 sm.validity_period,
#                 at.score_valid_until,
#                 CASE
#                     -- test has been quit
# 	                WHEN at.status_id = (SELECT id FROM {db_name}..custom_models_assignedteststatus WHERE codename = 'quit') THEN 0
#                     -- if supervised test ==> test must have been submitted at least two days ago to be displayed
#                     WHEN at.uit_invite_id is NULL AND at.submit_date <= DATEADD(DAY, -2, GETUTCDATE()) THEN 0
#                     -- if unsupervised test ==> current date (UTC) must be greater than two days after the validity end date of respective uit invite
#                     WHEN at.uit_invite_id is not NULL AND CONVERT(VARCHAR(10), GETUTCDATE(), 23) > (SELECT DATEADD(DAY, 2, validity_end_date) FROM {db_name}..custom_models_uitinvites WHERE id = at.uit_invite_id) THEN 0
#                 ELSE
#                     1
#                 END as 'pending_score'
#             FROM {db_name}..custom_models_assignedtest at
#             JOIN {db_name}..custom_models_assignedteststatus ats on ats.id = at.status_id
#             JOIN {db_name}..user_management_models_user u on u.id = at.user_id
#             JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
#             JOIN {db_name}..cms_models_scoringmethods sm on sm.test_id = at.test_id
#             -- status must be submitted or quit
#             WHERE at.status_id in (SELECT id
#                                 FROM {db_name}..custom_models_assignedteststatus
#                                 WHERE codename in ('submitted', 'quit'))
# """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0185_custompermission2favw",
        ),
        (
            "cms_models",
            "0097_historicaltestdefinition_show_result_and_more",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
