from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[aae_email_details_vw]")


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[aae_email_details_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0314_testcentertestsessionsvw_specified_test_description",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
