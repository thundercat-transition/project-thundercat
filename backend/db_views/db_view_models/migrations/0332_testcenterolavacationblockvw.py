# Generated by Django 4.2.11 on 2024-09-06 11:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('db_view_models', '0331_create_test_center_ola_vacation_block_view'),
    ]

    operations = [
        migrations.CreateModel(
            name='TestCenterOlaVacationBlockVw',
            fields=[
                ('id', models.IntegerField(db_column='id', primary_key=True, serialize=False)),
                ('date_from', models.DateField(db_column='date_from')),
                ('date_to', models.DateField(db_column='date_to')),
                ('test_center_id', models.IntegerField(db_column='test_center_id')),
                ('availability_en', models.IntegerField(db_column='availability_en')),
                ('availability_fr', models.IntegerField(db_column='availability_fr')),
            ],
            options={
                'db_table': 'test_center_ola_vacation_block_vw',
                'managed': False,
            },
        ),
    ]
