from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[uit_completed_processes_vw] AS
                SELECT
                    hui.id,
                    hui.invite_date,
                    hui.validity_end_date,
                    hui.orderless_request,
                    u.id as 'ta_user_id',
                    u.email as 'ta_email',
                    u.first_name as 'ta_first_name',
                    u.last_name as 'ta_last_name',
                    htp.id as 'ta_test_permissions_id',
                    IIF (hui.ta_test_permissions_id is not NULL, htp.staffing_process_number, orderless_financial_data.reference_number) as 'assessment_process_or_reference_nbr',
                    -- ==================== DEPARTMENT DATA ====================
                    IIF (hui.ta_test_permissions_id is not NULL,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN test_permission_dep.DEPT_ID is not NULL THEN test_permission_dep.DEPT_ID
                            -- PSC DEPT_ID
                            ELSE 104
                        END,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN orderless_dep.DEPT_ID is not NULL THEN orderless_dep.DEPT_ID
                            -- PSC DEPT_ID
                            ELSE 104
                        END
                        ) as 'dept_id',
                    IIF (hui.ta_test_permissions_id is not NULL,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN test_permission_dep.DEPT_ID is not NULL THEN test_permission_dep.EABRV
                            -- PSC DEPT_ID
                            ELSE 'PSC'
                        END,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN orderless_dep.DEPT_ID is not NULL THEN orderless_dep.EABRV
                            -- PSC DEPT_ID
                            ELSE 'PSC'
                        END
                        ) as 'dept_eabrv',
                    IIF (hui.ta_test_permissions_id is not NULL,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN test_permission_dep.DEPT_ID is not NULL THEN test_permission_dep.FABRV
                            -- PSC DEPT_ID
                            ELSE 'CFP'
                        END,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN orderless_dep.DEPT_ID is not NULL THEN orderless_dep.FABRV
                            -- PSC DEPT_ID
                            ELSE 'CFP'
                        END
                        ) as 'dept_fabrv',
                    IIF (hui.ta_test_permissions_id is not NULL,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN test_permission_dep.DEPT_ID is not NULL THEN test_permission_dep.EDESC
                            -- PSC DEPT_ID
                            ELSE 'Public Service Commission'
                        END,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN orderless_dep.DEPT_ID is not NULL THEN orderless_dep.EDESC
                            -- PSC DEPT_ID
                            ELSE 'Public Service Commission'
                        END
                        ) as 'dept_edesc',
                    IIF (hui.ta_test_permissions_id is not NULL,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN test_permission_dep.DEPT_ID is not NULL THEN test_permission_dep.FDESC
                            -- PSC DEPT_ID
                            ELSE 'Commission de la fonction publique'
                        END,
                        CASE
                            -- MATCHING DEPT_ID FOUND
                            WHEN orderless_dep.DEPT_ID is not NULL THEN orderless_dep.FDESC
                            -- PSC DEPT_ID
                            ELSE 'Commission de la fonction publique'
                        END
                        ) as 'dept_fdesc',
                    -- ==================== DEPARTMENT DATA (END) ====================
                    td.id as 'test_id',
                    td.test_code,
                    td.version,
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    total_number_of_utac.count as 'total_number_of_utac',
                    number_of_remaining_utac.count as 'number_of_remaining_utac',
                    number_of_active_tests.count as 'number_of_active_tests',
                    number_of_tests_taken.count as 'number_of_tests_taken'
                FROM {db_name}..custom_models_historicaluitinvites hui
                JOIN {db_name}..user_management_models_user u on u.id = hui.ta_user_id
                LEFT JOIN {db_name}..cms_models_historicaltestpermissions htp on htp.id = hui.ta_test_permissions_id AND htp.history_type = '+'
                LEFT JOIN {db_name}..cms_models_testdefinition td on td.id = hui.test_id
                -- GETTING ORDERLESS FINANCIAL DATA (ONE TO MANY, SO ONLY GETTING THE FIRST RESULT)
                OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_orderlessfinancialdata ofd WHERE ofd.uit_invite_id = hui.id) orderless_financial_data
                LEFT JOIN {db_name}..CAT_REF_DEPARTMENTS_VW orderless_dep on orderless_dep.DEPT_ID = orderless_financial_data.department_ministry_id
                LEFT JOIN {db_name}..CAT_REF_DEPARTMENTS_VW test_permission_dep on CAST(orderless_dep.DEPT_ID AS VARCHAR) = htp.department_ministry_code
                -- GETTING TOTAL NUMBER OF UNSUPERVISED TEST ACCESS CODE FOR RESPECTIVE UIT_INVITE_ID
                OUTER APPLY (SELECT COUNT(*) as 'count' 
                            FROM {db_name}..custom_models_historicalunsupervisedtestaccesscode hutac 
                            WHERE hutac.uit_invite_id = hui.id AND hutac.history_type = '+') total_number_of_utac
                -- GETTING NUMBER OF REMAINING UNSUPERVISED TEST ACCESS CODE FOR RESPECTIVE UIT_INVITE_ID
                OUTER APPLY (SELECT COUNT(*) as 'count' 
                            FROM {db_name}..custom_models_unsupervisedtestaccesscode utac 
                            WHERE utac.uit_invite_id = hui.id) number_of_remaining_utac
                -- GETTING NUMBER OF ACTIVE TESTS (CHECKED_IN, PRE_TEST, ACTIVE OR TRANSITION) FOR RESPECTIVE UIT_INVITE_ID
                OUTER APPLY (SELECT COUNT(*) as 'count' 
                            FROM {db_name}..custom_models_assignedtest at 
                            WHERE at.uit_invite_id = hui.id 
                            AND at.status_id in (SELECT id 
                                                FROM {db_name}..custom_models_assignedteststatus ats 
                                                WHERE ats.codename in ('checked_in', 'pre_test', 'active', 'transition')
                                                )
                            ) number_of_active_tests
                -- GETTING NUMBER OF TESTS TAKEN
                OUTER APPLY (SELECT COUNT(*) as 'count' 
                            FROM {db_name}..custom_models_assignedtest at 
                            WHERE at.uit_invite_id = hui.id 
                            AND at.status_id in (SELECT id 
                                                FROM {db_name}..custom_models_assignedteststatus ats 
                                                WHERE ats.codename in ('submitted', 'quit')
                                                )
                            ) number_of_tests_taken
                -- HISTORY_TYPE +
                -- AND
                -- VALIDITY_END_DATE IS IN THE PAST
                -- OR
                -- NO MORE REMAINING UNSUPERVISED TEST ACCESS CODE AND NO MORE ACTIVE TESTS
                -- AND
                -- REMOVING NULL TA_TEST_PERMISSION_ID FOR NON-ORDERLESS TEST (OLDER DATA)
                WHERE hui.history_type = '+' 
                AND (hui.validity_end_date < CONVERT(VARCHAR(10), GETDATE(), 23)
                    OR
                    number_of_remaining_utac.count <= 0 AND number_of_active_tests.count <= 0
                    )
                AND (hui.orderless_request = 0 AND hui.ta_test_permissions_id is not NULL OR hui.orderless_request = 1)
                """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0226_assessmentprocessassignedtestspecsvw_hr_email_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
