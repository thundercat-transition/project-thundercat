# Generated by Django 4.2.10 on 2024-04-09 19:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("db_view_models", "0202_remove_tahistoryreportvw_ta_username_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="assessmentprocessresultscandidatesvw",
            name="user_email",
            field=models.EmailField(
                db_column="USER_EMAIL", default="temp", max_length=254
            ),
        ),
    ]
