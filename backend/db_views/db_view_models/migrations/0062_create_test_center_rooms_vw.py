from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_center_rooms_vw] AS
                SELECT
                    r.id,
                    r.test_center_id,
                    rd.name,
                    rd.email,
                    rd.max_occupancy,
                    rd.other_details,
                    rd.active
                FROM {db_name}..custom_models_testcenterrooms r
                JOIN {db_name}..custom_models_testcenterroomdata rd on rd.id = r.room_data_id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0061_testcentertestadministratorsvw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
