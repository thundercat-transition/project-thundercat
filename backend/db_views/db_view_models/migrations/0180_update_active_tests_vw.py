from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[active_tests_vw] AS


# 			SELECT
#                 at.id as 'assigned_test_id',
#                 u.id as 'candidate_user_id',
#                 u.username as 'candidate_username',
#                 u.email as 'candidate_email',
#                 u.first_name as 'candidate_first_name',
#                 u.last_name as 'candidate_last_name',
#                 ta.id as 'ta_user_id',
#                 ta.username as 'ta_username',
#                 ta.email as 'ta_email',
#                 ta.first_name as 'ta_first_name',
#                 ta.last_name as 'ta_last_name',
#                 at.status_id as 'test_status_id',
#                 atst.codename as 'test_status_codename',
#                 tp.test_order_number,
#                 ofd.reference_number,
#                 ats.time_limit,
#                 td.id as 'test_id',
#                 td.test_code
#             FROM {db_name}..custom_models_assignedtest at
#             JOIN {db_name}..custom_models_assignedteststatus atst on atst.id = at.status_id
#             OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.id = at.user_id ORDER BY u1.history_date DESC) u
#             OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.id = at.ta_user_id ORDER BY u1.history_date DESC) ta
#             OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions tp1 WHERE tp1.test_order_number = at.test_order_number AND tp1.user_id = at.ta_user_id ORDER BY tp1.history_date DESC) tp
#             LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
#             OUTER APPLY (SELECT SUM(ats1.test_section_time) as 'time_limit' FROM {db_name}..custom_models_assignedtestsection ats1 WHERE ats1.assigned_test_id = at.id) ats
#             LEFT JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
#             WHERE at.status_id in (SELECT id FROM {db_name}..custom_models_assignedteststatus WHERE codename in ('assigned', 'checked_in', 'active', 'transition', 'locked', 'paused', 'pre_test'))
#     """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


def rollback_changes(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """ALTER VIEW [dbo].[active_tests_vw] AS


# 			SELECT
#                 at.id as 'assigned_test_id',
#                 u.id as 'candidate_user_id',
#                 u.username as 'candidate_username',
#                 u.email as 'candidate_email',
#                 u.first_name as 'candidate_first_name',
#                 u.last_name as 'candidate_last_name',
#                 ta.id as 'ta_user_id',
#                 ta.username as 'ta_username',
#                 ta.email as 'ta_email',
#                 ta.first_name as 'ta_first_name',
#                 ta.last_name as 'ta_last_name',
#                 at.status_id as 'test_status_id',
#                 atst.codename as 'test_status_codename',
#                 tp.test_order_number,
#                 ofd.reference_number,
#                 ats.time_limit,
#                 td.id as 'test_id',
#                 td.test_code
#             FROM {db_name}..custom_models_assignedtest at
#             JOIN {db_name}..custom_models_assignedteststatus atst on atst.id = at.status_id
#             OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.id = at.user_id ORDER BY u1.history_date DESC) u
#             OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.id = at.ta_user_id ORDER BY u1.history_date DESC) ta
#             OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions tp1 WHERE tp1.test_order_number = at.test_order_number AND tp1.username_id = at.ta_id ORDER BY tp1.history_date DESC) tp
#             LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
#             OUTER APPLY (SELECT SUM(ats1.test_section_time) as 'time_limit' FROM {db_name}..custom_models_assignedtestsection ats1 WHERE ats1.assigned_test_id = at.id) ats
#             LEFT JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
#             WHERE at.status_id in (SELECT id FROM {db_name}..custom_models_assignedteststatus WHERE codename in ('assigned', 'checked_in', 'active', 'transition', 'locked', 'paused', 'pre_test'))
#     """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0179_update_bo_test_access_codes_vw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
