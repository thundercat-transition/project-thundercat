from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[assessment_process_assigned_test_specs_vw] AS
    #             SELECT
    #                 ats.id,
    #                 ats.first_name,
    #                 ats.last_name,
    #                 ats.email,
    #                 apv.id as 'assessment_process_id',
    #                 apv.reference_number as 'assessment_process_reference_number',
    #                 apv.closing_date as 'assessment_process_closing_date',
    #                 apv.allow_booking_external_tc as 'assessment_process_allow_booking_external_tc',
    #                 apv.contact_email_for_candidates,
    #                 apv.dept_id as 'assessment_process_dept_id ',
    #                 apv.dept_eabrv as 'assessment_process_dept_eabrv',
    #                 apv.dept_fabrv as 'assessment_process_dept_fabrv',
    #                 apv.dept_edesc as 'assessment_process_dept_edesc',
    #                 apv.dept_fdesc as 'assessment_process_dept_fdesc',
    #                 IIF(bc.id is not NULL, bc.id, NULL) as 'billing_contact_id',
    #                 IIF(bc.id is not NULL, bc.first_name, NULL) as 'billing_contact_first_name',
    #                 IIF(bc.id is not NULL, bc.last_name, NULL) as 'billing_contact_last_name ',
    #                 IIF(bc.id is not NULL, bc.email, NULL) as 'billing_contact_email',
    #                 tst.id as 'test_skill_type_id',
    #                 tst.codename as 'test_skill_type_codename',
    #                 type_e.text as 'test_skill_type_name_en',
    #                 type_f.text as 'test_skill_type_name_fr',
    #                 ats.test_skill_sub_type_id as 'test_skill_sub_type_id',
    #                 sled.codename as 'test_skill_sub_type_codename',
    #                 CASE
    #                     WHEN tst.id = 2 THEN sle_sub_type_e.text
    #                     WHEN tst.id = 3 THEN occ_sub_type_e.text
    #                     ELSE
    #                         NULL
    #                 END AS 'test_skill_sub_type_name_en',
    #                 CASE
    #                     WHEN tst.id = 2 THEN sle_sub_type_f.text
    #                     WHEN tst.id = 3 THEN occ_sub_type_e.text
    #                     ELSE
    #                         NULL
    #                 END AS 'test_skill_sub_type_name_fr',
    #                 ats.reason_for_testing_id,
    #                 rft.description_en as 'reason_for_testing_name_en',
    #                 rft.description_fr as 'reason_for_testing_name_fr',
    #                 ats.level_required,
    #                 apv.request_sent
    #             FROM {db_name}..custom_models_assessmentprocessassignedtestspecs ats
    #             LEFT JOIN {db_name}..custom_models_billingcontact bc on bc.id = ats.billing_contact_id
    #             JOIN {db_name}..assessment_process_vw apv on apv.id = ats.assessment_process_id
    #             JOIN {db_name}..cms_models_testskilltype tst on tst.id = ats.test_skill_type_id
    #             OUTER APPLY (SELECT TOP 1 tstte.text FROM {db_name}..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
    #             OUTER APPLY (SELECT TOP 1 tsttf.text FROM {db_name}..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
    #             LEFT JOIN {db_name}..cms_models_testskillsledesc sled on sled.id = ats.test_skill_sub_type_id
    #             OUTER APPLY (SELECT TOP 1 sledte.text FROM {db_name}..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
    #             OUTER APPLY (SELECT TOP 1 sledtf.text FROM {db_name}..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
    #             LEFT JOIN {db_name}..cms_models_testskilloccupationaldesc occd on occd.id = ats.test_skill_sub_type_id
    #             OUTER APPLY (SELECT TOP 1 occdte.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = sled.id AND occdte.language_id = 1) occ_sub_type_e
    #             OUTER APPLY (SELECT TOP 1 occdtf.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = sled.id AND occdtf.language_id = 2) occ_sub_type_f
    #             JOIN {db_name}..custom_models_reasonsfortesting rft on rft.id = ats.reason_for_testing_id
    #             """.format(
    #             db_name=settings.DATABASES["default"]["NAME"]
    #         )
    #     )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0192_update_booked_reservations_view",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
