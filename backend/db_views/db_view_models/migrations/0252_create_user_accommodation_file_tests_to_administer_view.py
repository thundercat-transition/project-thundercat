from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[user_accommodation_file_tests_to_administer_vw] AS
                SELECT
                    td.id,
                    td.parent_code,
                    td.test_code,
                    td.version,
                    td.en_name as 'name_en',
	                td.fr_name as 'name_fr',
                    td.active,
                    td.version_notes,
                    td.is_uit,
                    tst.id as 'test_skill_type_id',
                    tst.codename as 'test_skill_type_codename',
                    type_e.text as 'test_skill_type_name_en',
                    type_f.text as 'test_skill_type_name_fr',
                    CASE
                        WHEN tst.id = 2 THEN sled.id
                        WHEN tst.id = 3 THEN occd.id
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_id',
                    CASE
                        WHEN tst.id = 2 THEN sled.codename
                        WHEN tst.id = 3 THEN occd.codename
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_codename',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_e.text
                        WHEN tst.id = 3 THEN occ_sub_type_e.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_en',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_f.text
                        WHEN tst.id = 3 THEN occ_sub_type_f.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_fr'
                FROM {db_name}..cms_models_testdefinition td
                JOIN {db_name}..cms_models_testskill ts on ts.test_definition_id = td.id
                LEFT JOIN {db_name}..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM {db_name}..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM {db_name}..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN {db_name}..cms_models_testskillsle tssle on tssle.test_skill_id = ts.id
                LEFT JOIN {db_name}..cms_models_testskillsledesc sled on sled.id = tssle.test_skill_sle_desc_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM {db_name}..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM {db_name}..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                LEFT JOIN {db_name}..cms_models_testskilloccupational tsocc on tsocc.test_skill_id = ts.id
                LEFT JOIN {db_name}..cms_models_testskilloccupationaldesc occd on occd.id = tsocc.test_skill_occupational_desc_id
                OUTER APPLY (SELECT TOP 1 occdte.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = occd.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = occd.id AND occdtf.language_id = 2) occ_sub_type_f
                -- Active, non-public and non-archived tests
                WHERE td.active = 1 AND td.is_public = 0 AND td.archived = 0
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0251_selecteduseraccommodationfilevw_test_skill_sub_type_codename_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
