from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[user_profile_change_request_vw] AS
                SELECT
                    cr.id,
                    cr.user_id,
                    cr.current_first_name,
                    cr.current_last_name,
                    cr.current_birth_date,
                    cr.new_first_name,
                    cr.new_last_name,
                    cr.new_birth_date,
                    cr.modify_date,
                    cr.comments,
                    cr.reason_for_deny,
                    u.username,
                    u.email,
                    u.pri,
                    u.military_nbr
                FROM {db_name}..user_management_models_userprofilechangerequest cr
                JOIN {db_name}..user_management_models_user u ON u.id = cr.user_id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0054_update_test_result_vw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
