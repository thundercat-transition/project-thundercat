from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("db_view_models", "0106_simplify_candidate_reservation_view"),
    ]

    operations = [
        migrations.AddField(
            model_name="CandidateReservationsVW",
            name="allow_booking_external_tc",
            field=models.BooleanField(db_column="ALLOW_BOOKING_EXTERNAL_TC"),
        ),
        migrations.AddField(
            model_name="CandidateReservationsVW",
            name="dept_id",
            field=models.IntegerField(db_column="DEPT_ID"),
        ),
        migrations.AddField(
            model_name="CandidateReservationsVW",
            name="dept_eabrv",
            field=models.CharField(db_column="DEPT_EABRV", max_length=10),
        ),
        migrations.AddField(
            model_name="CandidateReservationsVW",
            name="dept_fabrv",
            field=models.CharField(db_column="DEPT_FABRV", max_length=10),
        ),
        migrations.AddField(
            model_name="CandidateReservationsVW",
            name="dept_edesc",
            field=models.CharField(db_column="DEPT_EDESC", max_length=200),
        ),
        migrations.AddField(
            model_name="CandidateReservationsVW",
            name="dept_fdesc",
            field=models.CharField(db_column="DEPT_FDESC", max_length=200),
        ),
        migrations.RemoveField(
            model_name="CandidateReservationsVW",
            name="test_center_test_session_data_id",
        ),
        migrations.RemoveField(
            model_name="CandidateReservationsVW",
            name="date",
        ),
        migrations.RemoveField(
            model_name="CandidateReservationsVW",
            name="start_time",
        ),
        migrations.RemoveField(
            model_name="CandidateReservationsVW",
            name="end_time",
        ),
        migrations.RemoveField(
            model_name="CandidateReservationsVW",
            name="test_center_name",
        ),
        migrations.RemoveField(
            model_name="CandidateReservationsVW",
            name="test_center_address",
        ),
        migrations.RemoveField(
            model_name="CandidateReservationsVW",
            name="test_center_city",
        ),
        migrations.RemoveField(
            model_name="CandidateReservationsVW",
            name="test_center_postal_code",
        ),
        migrations.RemoveField(
            model_name="CandidateReservationsVW",
            name="test_center_province",
        ),
        migrations.RemoveField(
            model_name="CandidateReservationsVW",
            name="test_center_country",
        ),
    ]
