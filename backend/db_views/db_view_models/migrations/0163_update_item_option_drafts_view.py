from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[item_option_drafts_vw] AS
				SELECT 
					o.id as 'item_option_id',
					o.system_id,
					o.option_type,
					o.modify_date as 'item_option_modify_date',
					o.option_order,
					o.exclude_from_shuffle,
					o.historical_option_id,
					o.rationale,
					o.score,
					o.item_id,
					o.user_id,
					u.username,
					ot.id as 'item_option_text_id',
					ot.text,
					ot.modify_date as 'item_option_text_modify_date',
					ot.language_id
				FROM {db_name}..cms_models_itemoptiondrafts o
				JOIN {db_name}..user_management_models_user u on u.id = o.user_id
				JOIN {db_name}..cms_models_itemoptiontextdrafts ot on ot.item_option_id = o.id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[item_option_drafts_vw] AS


# 			SELECT
# 				o.id as 'item_option_id',
# 				o.system_id,
# 				o.option_type,
# 				o.modify_date as 'item_option_modify_date',
# 				o.option_order,
# 				o.exclude_from_shuffle,
# 				o.historical_option_id,
# 				o.rationale,
# 				o.score,
# 				o.item_id,
# 				o.username_id,
# 				ot.id as 'item_option_text_id',
# 				ot.text,
# 				ot.modify_date as 'item_option_text_modify_date',
# 				ot.language_id
# 			FROM {db_name}..cms_models_itemoptiondrafts o
# 			JOIN {db_name}..cms_models_itemoptiontextdrafts ot on ot.item_option_id = o.id
#     """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0162_remove_itemcontentdraftsvw_username_id_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
