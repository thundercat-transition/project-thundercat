from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_center_test_administrators_vw] AS
                SELECT 
                    ta.id,
                    ta.test_center_id,
                    u.id as 'user_id',
                    u.email,
                    u.first_name,
                    u.last_name
                FROM {db_name}..custom_models_testcentertestadministrators ta
                JOIN {db_name}..user_management_models_user u on ta.user_id = u.id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0059_testcentersvw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
