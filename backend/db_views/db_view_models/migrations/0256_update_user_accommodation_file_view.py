from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[user_accommodation_file_vw] AS
                SELECT
                    uaf.id,
                    uaf.comments,
                    uaf.is_uit,
                    uaf.modify_date,
                    uaf.created_date,
                    uaf.last_modified_by_user_id,
                    uafs.id as 'status_id',
                    uafs.codename as 'status_codename',
                    uafst_en.text as 'status_name_en',
                    uafst_fr.text as 'status_name_fr',
                    uaf.test_center_id,
                    ummu.id as 'user_id',
                    ummu.username as 'user_username',
                    ummu.email as 'user_email',
                    ummu.first_name as 'user_first_name',
                    ummu.last_name as 'user_last_name',
                    at_u.id as 'assigned_to_user_id',
                    at_u.email as 'assigned_to_user_email',
                    at_u.first_name as 'assigned_to_user_first_name',
                    at_u.last_name 'assigned_to_user_last_name'
                FROM {db_name}..custom_models_useraccommodationfile uaf
                JOIN {db_name}..user_management_models_user ummu ON ummu.id = uaf.user_id
                JOIN {db_name}..custom_models_useraccommodationfilestatus uafs ON uafs.id = uaf.status_id
                JOIN {db_name}..custom_models_useraccommodationfilestatustext uafst_en on uafst_en.user_accommodation_file_status_id = uafs.id AND uafst_en.language_id = 1
                JOIN {db_name}..custom_models_useraccommodationfilestatustext uafst_fr on uafst_fr.user_accommodation_file_status_id = uafs.id AND uafst_fr.language_id = 2
                LEFT JOIN {db_name}..user_management_models_user at_u on at_u.id = uaf.assigned_to_user_id
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0255_selecteduseraccommodationfilevw_assigned_to_user_email_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
