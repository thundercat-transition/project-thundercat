# Generated by Django 4.2.11 on 2024-06-26 18:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        (
            "db_view_models",
            "0266_create_completed_supervised_user_accommodation_file_view",
        ),
    ]

    operations = [
        migrations.CreateModel(
            name="CompletedSupervisedUserAccommodationFileVW",
            fields=[
                (
                    "id",
                    models.IntegerField(
                        db_column="id", primary_key=True, serialize=False
                    ),
                ),
                ("comments", models.TextField(db_column="comments")),
                ("is_uit", models.BooleanField(db_column="is_uit")),
                ("modify_date", models.DateTimeField(db_column="modify_date")),
                ("created_date", models.DateField(db_column="created_date")),
                ("status_id", models.IntegerField(db_column="status_id")),
                (
                    "status_codename",
                    models.CharField(db_column="status_codename", max_length=50),
                ),
                (
                    "status_name_en",
                    models.CharField(db_column="status_name_en", max_length=50),
                ),
                (
                    "status_name_fr",
                    models.CharField(db_column="status_name_fr", max_length=50),
                ),
                ("test_center_id", models.IntegerField(db_column="test_center_id")),
                ("user_id", models.IntegerField(db_column="user_id")),
                (
                    "user_username",
                    models.CharField(db_column="user_username", max_length=254),
                ),
                (
                    "user_email",
                    models.CharField(db_column="user_email", max_length=254),
                ),
                (
                    "user_first_name",
                    models.CharField(db_column="user_first_name", max_length=30),
                ),
                (
                    "user_last_name",
                    models.CharField(db_column="user_last_name", max_length=150),
                ),
                (
                    "assigned_to_user_id",
                    models.IntegerField(db_column="assigned_to_user_id"),
                ),
                (
                    "assigned_to_user_email",
                    models.CharField(
                        db_column="assigned_to_user_email", max_length=254
                    ),
                ),
                (
                    "assigned_to_user_first_name",
                    models.CharField(
                        db_column="assigned_to_user_first_name", max_length=30
                    ),
                ),
                (
                    "assigned_to_user_last_name",
                    models.CharField(
                        db_column="assigned_to_user_last_name", max_length=150
                    ),
                ),
                (
                    "consumed_reservation_code_id",
                    models.IntegerField(db_column="consumed_reservation_code_id"),
                ),
                (
                    "assessment_process_assigned_test_specs_id",
                    models.IntegerField(
                        db_column="assessment_process_assigned_test_specs_id"
                    ),
                ),
                ("closing_date", models.DateField(db_column="closing_date")),
                (
                    "requesting_dept_id",
                    models.IntegerField(db_column="requesting_dept_id"),
                ),
                (
                    "requesting_dept_abrv_en",
                    models.CharField(
                        db_column="requesting_dept_abrv_en", max_length=10
                    ),
                ),
                (
                    "requesting_dept_abrv_fr",
                    models.CharField(
                        db_column="requesting_dept_abrv_fr", max_length=10
                    ),
                ),
                (
                    "requesting_dept_desc_en",
                    models.CharField(
                        db_column="requesting_dept_desc_en", max_length=200
                    ),
                ),
                (
                    "requesting_dept_desc_fr",
                    models.CharField(
                        db_column="requesting_dept_desc_fr", max_length=200
                    ),
                ),
                ("test_id", models.IntegerField(db_column="test_id")),
                (
                    "parent_code",
                    models.CharField(db_column="parent_code", max_length=25),
                ),
                ("test_code", models.CharField(db_column="test_code", max_length=25)),
                (
                    "test_name_en",
                    models.CharField(db_column="test_name_en", max_length=175),
                ),
                (
                    "test_name_fr",
                    models.CharField(db_column="test_name_fr", max_length=175),
                ),
                ("version", models.IntegerField(db_column="version")),
            ],
            options={
                "db_table": "completed_supervised_user_accommodation_file_vw",
                "managed": False,
            },
        ),
    ]
