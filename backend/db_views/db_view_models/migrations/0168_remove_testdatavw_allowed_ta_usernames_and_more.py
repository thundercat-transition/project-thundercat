# Generated by Django 4.2.7 on 2024-03-01 14:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("db_view_models", "0167_update_test_data_vw"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="testdatavw",
            name="allowed_ta_usernames",
        ),
        migrations.AddField(
            model_name="testdatavw",
            name="allowed_ta_user_ids",
            field=models.TextField(db_column="ALLOWED_TA_USER_IDS", default="temp"),
        ),
        migrations.AddField(
            model_name="testdatavw",
            name="candidate_user_id",
            field=models.IntegerField(db_column="CANDIDATE_USER_ID", default=1),
        ),
        migrations.AddField(
            model_name="testdatavw",
            name="ta_user_id",
            field=models.IntegerField(db_column="TA_USER_ID", default=1),
        ),
        migrations.AlterField(
            model_name="testdatavw",
            name="candidate_username",
            field=models.CharField(
                db_column="CANDIDATE_USERNAME", default="temp", max_length=254
            ),
        ),
        migrations.AlterField(
            model_name="testdatavw",
            name="ta_username",
            field=models.CharField(
                db_column="TA_USERNAME", default="temp", max_length=254
            ),
        ),
    ]
