# Generated by Django 4.2.11 on 2024-07-11 12:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("db_view_models", "0276_update_user_accommodation_file_view"),
    ]

    operations = [
        migrations.AddField(
            model_name="useraccommodationfilevw",
            name="test_skill_sub_type_codename",
            field=models.CharField(
                db_column="test_skill_sub_type_codename", default="temp", max_length=25
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfilevw",
            name="test_skill_sub_type_id",
            field=models.IntegerField(db_column="test_skill_sub_type_id", default=1),
        ),
        migrations.AddField(
            model_name="useraccommodationfilevw",
            name="test_skill_sub_type_name_en",
            field=models.CharField(
                db_column="test_skill_sub_type_name_en", default="temp", max_length=150
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfilevw",
            name="test_skill_sub_type_name_fr",
            field=models.CharField(
                db_column="test_skill_sub_type_name_fr", default="temp", max_length=150
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfilevw",
            name="test_skill_type_codename",
            field=models.CharField(
                db_column="test_skill_type_codename", default="temp", max_length=25
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfilevw",
            name="test_skill_type_id",
            field=models.IntegerField(db_column="test_skill_type_id", default=1),
        ),
        migrations.AddField(
            model_name="useraccommodationfilevw",
            name="test_skill_type_name_en",
            field=models.CharField(
                db_column="test_skill_type_name_en", default="temp", max_length=150
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfilevw",
            name="test_skill_type_name_fr",
            field=models.CharField(
                db_column="test_skill_type_name_fr", default="temp", max_length=150
            ),
        ),
    ]
