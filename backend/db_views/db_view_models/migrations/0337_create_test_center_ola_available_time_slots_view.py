from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER FUNCTION get_test_assessor_availabilities_by_date_fn (@date date, @test_center_id int)
                RETURNS TABLE
                AS RETURN
                SELECT
                    TOP 100 PERCENT
                    tcts.start_time,
                    tcts.end_time,
                    tcts.day_of_week_id,
                    tcts.assessed_language_id,
                    SUM(nbr_of_available_test_assessors.count) as 'available_test_assessors'
                FROM {db_name}..custom_models_testcenterolatimeslot tcts
                JOIN {db_name}..custom_models_testcenter tc on tc.id = tcts.test_center_id
                -- GETTING NUMBER OF ASSESSORS THAT ARE INCLUDED IN AVAILABILITY BASED ON PROVIDED TEST CENTER ID AND RESPECTIVE ASSESSED LANGUAGE ID
                OUTER APPLY (SELECT COUNT(*) * tcts.availability/100 as 'count' 
                            FROM {db_name}..custom_models_testcenterolatestassessor tcta 
                            JOIN {db_name}..custom_models_testcenterolatestassessorapprovedlanguage tctaal 
                            ON tctaal.test_center_ola_test_assessor_id = tcta.id 
                            WHERE tctaal.language_id = tcts.assessed_language_id 
                            AND tcta.included_in_availability = 1 
                            AND tcta.test_center_id = @test_center_id
                            ) nbr_of_available_test_assessors
                -- ONLY CONSIDERING AUTHORIZED OLA TEST CENTERS WHERE DAY OF WEEK MATCHES THE PROVIDED DATE
                WHERE tc.ola_authorized = 1 AND tcts.test_center_id = @test_center_id AND tcts.day_of_week_id = DATEPART(dw, @date) - 1
                GROUP BY tcts.start_time, tcts.end_time, tcts.day_of_week_id, tcts.assessed_language_id
                ORDER BY tcts.day_of_week_id
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """CREATE OR ALTER FUNCTION handle_vacation_block_availability_by_date_fn (@test_center_id int, @date date, @assessed_language_id int, @available_test_assessors int)
                RETURNS TABLE
                AS RETURN
                SELECT TOP 1 IIF(
                                -- NO VACATION BLOCK DATA FOUND
                                vacation_block_availability_data.availability is NULL,
                                -- RETURN PROVIDED AVAILABLE TEST ASSESSORS (SAME DATA AS BEFORE - NO CHANGES)
                                @available_test_assessors,
                                -- RETURN PROVIDED AVAILABLE TEST ASSESSORS X VACATION BLOCK AVAILABILITY (%)
                                @available_test_assessors * vacation_block_availability_data.availability/100
                                ) as 'available_test_assessors'
                FROM custom_models_testcenter tc
                -- GETTING VACATION BLOCK AVAILABILITY DATA (IF EXISTS) BASED ON PROVIDED TEST CENTER ID, ASSESSED LANGUAGE ID AND DATE
                OUTER APPLY (SELECT TOP 1 availability as 'availability'
                            FROM {db_name}..custom_models_testcenterolavacationblockavailability vba 
                            WHERE vba.test_center_ola_vacation_block_id IN (SELECT id 
                                                                            FROM {db_name}..custom_models_testcenterolavacationblock vb 
                                                                            WHERE vb.test_center_id = tc.id
                                                                            AND vba.language_id = @assessed_language_id
                                                                            AND @date >= vb.date_from 
                                                                            AND @date <= vb.date_to
                                                                            )
                            
                            ORDER BY vba.availability ASC
                            ) vacation_block_availability_data
                -- GETTING DATA BASED ON PROVIDED TEST CENTER ID
                WHERE tc.id = @test_center_id 

    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """CREATE OR ALTER FUNCTION get_nbr_of_reserved_test_assessors_by_date_and_time_fn (@date date, @start_time time, @end_time time)
                RETURNS TABLE
                AS RETURN
                SELECT
                    TOP 100 PERCENT
                    COUNT(ts.id) as 'nbr_of_reserved_assessors'
                FROM {db_name}..custom_models_testcentertestsessions ts
                JOIN {db_name}..custom_models_testcentertestsessiondata tsd on tsd.id = ts.test_session_data_id
                WHERE tsd.date = @date 
                AND CAST(tsd.start_time as time) = @start_time 
                AND CAST(tsd.end_time as time) = @end_time
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """CREATE OR ALTER FUNCTION get_ola_available_time_slots_fn ()
                RETURNS @ola_available_time_slots table(id int Primary Key IDENTITY(1,1), date date, start_time time, end_time time, day_of_week_id int, assessed_language_id int, available_test_assessors int)
                AS
                BEGIN
                    -- DECLARING ALL NEEDED TABLES/VARIABLES
                    -- TABLES
                    DECLARE @test_center_availability table(id int Primary Key IDENTITY(1,1), test_center_id int, nbr_of_days int)
                    -- VARIABLES
                    DECLARE @i int
                    DECLARE @test_center_availability_id int
                    DECLARE @nbr_of_days int
                    DECLARE @current_date date
                    DECLARE @test_center_id int

                    -- CREATING @test_center_availability TABLE
                    INSERT INTO @test_center_availability 
                    SELECT tcoc.test_center_id, tcoc.advanced_booking_delay 
                    FROM {db_name}..custom_models_testcenterolaconfigs tcoc

                    -- INFINITE LOOP
                    WHILE 1 = 1
                    BEGIN
                        -- SETTING NEEDED VARIABLES
                        SET @i = 0
                        SET @current_date = GETDATE()
                        SET @test_center_availability_id = NULL
                    
                        -- SETTING @test_center_availability_id, @test_center_id AND @nbr_of_days BASED ON @test_center_availability TABLE
                        SELECT TOP 1 @test_center_availability_id = id, @test_center_id = test_center_id, @nbr_of_days = nbr_of_days 
                        FROM @test_center_availability 
                        ORDER BY id

                        -- NO MORE DATA
                        IF @test_center_availability_id IS NULL
                            -- BREAKING THE LOOP
                            BREAK

                        -- LOOPING FROM 0 TO PROVIDED NUMBER OF DAYS (TEST CENTER OLA CONFIGS - ADVANCED BOOKING DELAY)
                        WHILE @i < @nbr_of_days
                        BEGIN
                            -- POPULATING @ola_available_time_slots TABLE
                            INSERT INTO @ola_available_time_slots
                            SELECT 
                                @current_date,
                                start_time,
                                end_time,
                                day_of_week_id,
                                assessed_language_id ,
                                available_test_assessors = (SELECT available_test_assessors 
                                                            FROM {db_name}..handle_vacation_block_availability_by_date_fn(@test_center_id, @current_date, assessed_language_id, available_test_assessors
														   )
                                )
                            FROM {db_name}..get_test_assessor_availabilities_by_date_fn(@current_date, @test_center_id)

                            -- ADDING 1 TO @i VARIABLE
                            SET @i = @i + 1
                            -- ADDING 1 DAY TO @current_date VARIABLE
                            SET @current_date = DATEADD(DAY, 1, @current_date)
                        END

                        -- DELETING RESPECTIVE ROW FROM @test_center_availability TABLE
                        DELETE FROM @test_center_availability WHERE id = @test_center_availability_id
                    END
                    RETURN
                END

    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_center_ola_available_time_slots_vw] AS
                SELECT
                    TOP 100 PERCENT
                    -- generating ID by converting the datetime in timestamp
                    DATEDIFF(SECOND, '1970-01-01', (CONCAT(CONCAT(fn.date, ' '), fn.start_time))) as 'id',
                    fn.date,
                    fn.start_time, 
                    fn.end_time,
                    fn.day_of_week_id,
                    fn.assessed_language_id,
                    SUM(available_test_assessors) as 'total_available_test_assessors',
                    nbr_of_reserved_assessors.count as 'nbr_of_reserved_assessors',
                    SUM(available_test_assessors) - nbr_of_reserved_assessors.count as 'remaining_available_test_assessors'
                FROM {db_name}..get_ola_available_time_slots_fn() fn
                OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM {db_name}..get_nbr_of_reserved_test_assessors_by_date_and_time_fn(date, start_time, end_time)) nbr_of_reserved_assessors
                OUTER APPLY (SELECT MAX(tcoc.booking_delay) as 'booking_delay' FROM {db_name}..custom_models_testcenterolaconfigs tcoc WHERE tcoc.test_center_id in (SELECT tcots.test_center_id FROM {db_name}..custom_models_testcenterolatimeslot tcots WHERE tcots.start_time = fn.start_time AND tcots.end_time = fn.end_time AND tcots.day_of_week_id = fn.day_of_week_id)) greatest_booking_delay_found
                -- still have available spots
                WHERE fn.available_test_assessors > 0 
                /* KNOWN ISSUE:
                    - depending on the timezone, the date might be wrong ==> 2024-09-24 - 02:00-03:00 UTC | 2024-09-24 - 22:00-00:00 -4 !== 2024-09-24 - 01:00-02:00 -1 (should have been the 25th - 01:00-02:00)
                    - if booking delay is less than 24 hours and the candidate tries to get late sessions on the same day (01:00-02:00 UTC for example), the logic might filter it out because the date is not converted in UTC, just the start time and end time
                */
                -- start_time_as_datetime (ex: CAST('2024-09-18 19:00:00' as datetime)) >= current UTC datetime + greatest_booking_delay_found
                AND CAST(CONCAT(CONCAT(fn.date, ' '), LEFT (fn.start_time, 8)) as datetime) >= DATEADD(HOUR, greatest_booking_delay_found.booking_delay, GETUTCDATE())
                GROUP BY date, start_time, end_time, day_of_week_id, assessed_language_id, nbr_of_reserved_assessors.count, greatest_booking_delay_found.booking_delay
                ORDER BY fn.date, fn.start_time ASC

    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            "DROP FUNCTION IF EXISTS [dbo].[get_test_assessor_availabilities_by_date_fn]"
        )
        cursor.execute(
            "DROP FUNCTION IF EXISTS [dbo].[handle_vacation_block_availability_by_date_fn]"
        )
        cursor.execute(
            "DROP FUNCTION IF EXISTS [dbo].[get_ola_available_time_slots_fn]"
        )
        cursor.execute(
            "DROP VIEW IF EXISTS [dbo].[test_center_ola_available_time_slots_vw]"
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0336_testcenterolatimeslotsvw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
