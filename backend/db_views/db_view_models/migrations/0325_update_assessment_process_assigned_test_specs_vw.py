from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[assessment_process_assigned_test_specs_vw] AS
                SELECT
                    ats.id,
                    ats.first_name,
                    ats.last_name,
                    ats.email,
                    apv.id as 'assessment_process_id',
                    apv.reference_number as 'assessment_process_reference_number',
                    apv.closing_date as 'assessment_process_closing_date',
                    apv.allow_booking_external_tc as 'assessment_process_allow_booking_external_tc',
                    apv.contact_email_for_candidates,
                    u.id as 'hr_user_id',
                    u.email as 'hr_email',
                    u.first_name as 'hr_first_name',
                    u.last_name as 'hr_last_name',
                    apv.dept_id as 'assessment_process_dept_id ',
                    apv.dept_eabrv as 'assessment_process_dept_eabrv',
                    apv.dept_fabrv as 'assessment_process_dept_fabrv',
                    apv.dept_edesc as 'assessment_process_dept_edesc',
                    apv.dept_fdesc as 'assessment_process_dept_fdesc',
                    IIF(bc.id is not NULL, bc.id, NULL) as 'billing_contact_id',
                    IIF(bc.id is not NULL, bc.first_name, NULL) as 'billing_contact_first_name',
                    IIF(bc.id is not NULL, bc.last_name, NULL) as 'billing_contact_last_name ',
                    IIF(bc.id is not NULL, bc.email, NULL) as 'billing_contact_email',
                    tst.id as 'test_skill_type_id',
                    tst.codename as 'test_skill_type_codename',
                    type_e.text as 'test_skill_type_name_en',
                    type_f.text as 'test_skill_type_name_fr',
                    ats.test_skill_sub_type_id as 'test_skill_sub_type_id',
                    CASE
                        WHEN tst.id = 2 THEN sled.codename
                        WHEN tst.id = 3 THEN occd.codename
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_codename',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_e.text
                        WHEN tst.id = 3 THEN occ_sub_type_e.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_en',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_f.text
                        WHEN tst.id = 3 THEN occ_sub_type_f.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_fr',
                    rft.id as 'reason_for_testing_id',
                    reason_for_testing_e.text as 'reason_for_testing_name_en',
                    reason_for_testing_f.text as 'reason_for_testing_name_fr',
                    ats.level_required,
                    apv.request_sent,
                    crcs.id as 'consumed_reservation_codes_status_id',
                    crcs.codename 'consumed_reservation_codes_status_codename',
                    crc.test_session_id,
                    at.id as 'assigned_test_id',
                    atsts.id as 'assigned_test_status_id',
                    atsts.codename as 'assigned_test_status_codename',
                    uaf.id as 'user_accommodation_file_id',
                    uaf.is_alternate_test_request,
                    td.id as 'test_id',
                    td.parent_code as 'parent_code',
                    td.test_code as 'test_code',
                    td.version as 'version',
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    c_u.id as 'candidate_user_id',
                    c_u.username as 'candidate_username',
                    c_u.email as 'candidate_email',
                    c_u.first_name as 'candidate_first_name',
                    c_u.last_name as 'candidate_last_name'
                FROM {db_name}..custom_models_assessmentprocessassignedtestspecs ats
                LEFT JOIN {db_name}..custom_models_billingcontact bc on bc.id = ats.billing_contact_id
                JOIN {db_name}..assessment_process_vw apv on apv.id = ats.assessment_process_id
                JOIN {db_name}..user_management_models_user u on u.id = apv.user_id
                JOIN {db_name}..cms_models_testskilltype tst on tst.id = ats.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM {db_name}..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM {db_name}..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN {db_name}..cms_models_testskillsledesc sled on sled.id = ats.test_skill_sub_type_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM {db_name}..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM {db_name}..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                LEFT JOIN {db_name}..cms_models_testskilloccupationaldesc occd on occd.id = ats.test_skill_sub_type_id
                OUTER APPLY (SELECT TOP 1 occdte.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = occd.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = occd.id AND occdtf.language_id = 2) occ_sub_type_f
                JOIN {db_name}..custom_models_reasonfortesting rft on rft.id = ats.reason_for_testing_id
                OUTER APPLY (SELECT TOP 1 text FROM {db_name}..custom_models_reasonfortestingtext rftt WHERE rftt.reason_for_testing_id = rft.id AND language_id = 1) reason_for_testing_e
                OUTER APPLY (SELECT TOP 1 text FROM {db_name}..custom_models_reasonfortestingtext rftt WHERE rftt.reason_for_testing_id = rft.id AND language_id = 2) reason_for_testing_f
                LEFT JOIN {db_name}..custom_models_consumedreservationcodes crc on crc.assessment_process_assigned_test_specs_id = ats.id
                LEFT JOIN {db_name}..custom_models_consumedreservationcodestatus crcs on crcs.id = crc.status_id
                LEFT JOIN {db_name}..custom_models_assignedtest at on at.id = crc.assigned_test_id
                LEFT JOIN {db_name}..custom_models_assignedteststatus atsts on atsts.id = at.status_id
                LEFT JOIN {db_name}..custom_models_useraccommodationfile uaf on uaf.id = crc.user_accommodation_file_id
                LEFT JOIN {db_name}..custom_models_useraccommodationfiletesttoadminister uaftta on uaftta.user_accommodation_file_id = uaf.id
                LEFT JOIN {db_name}..user_management_models_user c_u on c_u.id = uaf.user_id
                LEFT JOIN {db_name}..cms_models_testdefinition td on td.id = uaftta.test_id

                """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0324_update_test_centers_vw",
        ),
        (
            "custom_models",
            "0155_historicalreasonfortesting_and_more",
        )
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
