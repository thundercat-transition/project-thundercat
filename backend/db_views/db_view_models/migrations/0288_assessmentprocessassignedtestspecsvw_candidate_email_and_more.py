# Generated by Django 4.2.11 on 2024-07-17 15:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("db_view_models", "0287_update_assessment_process_assigned_test_specs_vw"),
    ]

    operations = [
        migrations.AddField(
            model_name="assessmentprocessassignedtestspecsvw",
            name="candidate_email",
            field=models.CharField(
                db_column="candidate_email", default="temps", max_length=254
            ),
        ),
        migrations.AddField(
            model_name="assessmentprocessassignedtestspecsvw",
            name="candidate_first_name",
            field=models.CharField(
                db_column="candidate_first_name", default="temps", max_length=30
            ),
        ),
        migrations.AddField(
            model_name="assessmentprocessassignedtestspecsvw",
            name="candidate_last_name",
            field=models.CharField(
                db_column="candidate_last_name", default="temps", max_length=150
            ),
        ),
        migrations.AddField(
            model_name="assessmentprocessassignedtestspecsvw",
            name="candidate_user_id",
            field=models.IntegerField(db_column="candidate_user_id", default=1),
        ),
        migrations.AddField(
            model_name="assessmentprocessassignedtestspecsvw",
            name="candidate_username",
            field=models.CharField(
                db_column="candidate_username", default="temps", max_length=254
            ),
        ),
        migrations.AddField(
            model_name="assessmentprocessassignedtestspecsvw",
            name="parent_code",
            field=models.CharField(
                db_column="parent_code", default="temps", max_length=25
            ),
        ),
        migrations.AddField(
            model_name="assessmentprocessassignedtestspecsvw",
            name="test_code",
            field=models.CharField(
                db_column="test_code", default="temps", max_length=25
            ),
        ),
        migrations.AddField(
            model_name="assessmentprocessassignedtestspecsvw",
            name="test_id",
            field=models.IntegerField(db_column="test_id", default=1),
        ),
        migrations.AddField(
            model_name="assessmentprocessassignedtestspecsvw",
            name="test_name_en",
            field=models.CharField(
                db_column="test_name_en", default="temps", max_length=175
            ),
        ),
        migrations.AddField(
            model_name="assessmentprocessassignedtestspecsvw",
            name="test_name_fr",
            field=models.CharField(
                db_column="test_name_fr", default="temps", max_length=175
            ),
        ),
        migrations.AddField(
            model_name="assessmentprocessassignedtestspecsvw",
            name="user_accommodation_file_id",
            field=models.IntegerField(
                db_column="user_accommodation_file_id", default=1
            ),
        ),
        migrations.AddField(
            model_name="assessmentprocessassignedtestspecsvw",
            name="version",
            field=models.IntegerField(db_column="version", default=1),
        ),
    ]
