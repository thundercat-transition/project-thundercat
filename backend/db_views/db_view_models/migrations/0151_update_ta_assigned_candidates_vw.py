from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[ta_assigned_candidates_vw] AS
    #             SELECT
    #                 at.id,
    #                 at.status_id,
    #                 atst.codename as 'status_codename',
    #                 at.previous_status_id,
    #                 atst2.codename as 'previous_status_codename',
    #                 at.start_date,
    #                 at.modify_date,
    #                 at.submit_date,
    #                 at.test_access_code,
    #                 at.total_score,
    #                 at.test_id,
    #                 td.parent_code,
    #                 td.test_code,
    #                 td.en_name as  'test_name_en',
    #                 td.fr_name as  'test_name_fr',
    #                 at.test_section_id,
    #                 at.test_session_language_id,
    #                 at.test_order_number,
    #                 at.en_converted_score,
    #                 at.fr_converted_score,
    #                 at.is_invalid,
    #                 at.uit_invite_id,
    #                 at.orderless_financial_data_id,
    #                 at.accommodation_request_id,
    #                 at.test_session_id,
    #                 u.id as 'candidate_user_id',
    #                 u.email as 'candidate_email',
    #                 u.first_name as 'candidate_first_name',
    #                 u.last_name as 'candidate_last_name',
    #                 u.birth_date as 'candidate_dob',
    #                 tau.id as 'ta_user_id',
    #                 tau.username as 'ta_username',
    #                 tau.email as 'ta_email',
    #                 tau.first_name as 'ta_first_name',
    #                 tau.last_name as 'ta_last_name',
    #                 ta_approve_action_test_section_id,
    #                 CASE
    #                     WHEN test_started_id is NULL then 0
    #                     ELSE 1
    #                 END as 'test_started',
    #                 CASE
    #                     WHEN timed_section_has_been_accessed is NULL then 0
    #                     ELSE 1
    #                 END as 'timed_section_accessed',
    #                 CASE
    #                     WHEN test_started_id is NULL THEN non_started_test_next_test_section_id_that_needs_approval
    #                     ELSE started_test_next_test_section_id_that_needs_approval
    #                 END as 'next_test_section_id_that_needs_approval',
    #                 CASE
    #                     -- THERE IS NO TEST SECTION TO BE APPROVED (TEST STARTED OR NOT)
    #                     WHEN (test_started_id is NULL AND non_started_test_next_test_section_id_that_needs_approval IS NULL) OR (test_started_id is not NULL AND started_test_next_test_section_id_that_needs_approval is NULL) THEN 0
    #                     -- TEST NOT STARTED + LATEST APPROVED TEST SECTION ID (BY TA) IS THE SAME AS THE NEXT TEST SECTION ID THAT NEEDS APPROVAL
    #                     WHEN test_started_id is NULL AND ta_approve_action_test_section_id = non_started_test_next_test_section_id_that_needs_approval AND ta_approve_action_test_section_id is not NULL THEN 0
    #                     -- TEST STARTED + LATEST APPROVED TEST SECTION ID (BY TA) IS THE SAME AS THE NEXT TEST SECTION ID THAT NEEDS APPROVAL
    #                     WHEN test_started_id is not NULL AND ta_approve_action_test_section_id = started_test_next_test_section_id_that_needs_approval THEN 0
    #                     -- TEST STARTED (NO MORE SECTION TO BE APPROVED)
    #                     WHEN test_started_id is not NULL AND started_test_next_test_section_id_that_needs_approval is NULL THEN 0
    #                     -- NEEDS APPROVAL
    #                     ELSE 1
    #                 END as 'needs_approval'
    #             FROM {db_name}..custom_models_assignedtest at
    #             JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
    #             JOIN {db_name}..user_management_models_user u on u.id= at.user_id
    #             JOIN {db_name}..user_management_models_user tau on tau.id = at.ta_user_id
    #             LEFT JOIN {db_name}..custom_models_assignedteststatus atst on atst.id = at.status_id
    #             LEFT JOIN {db_name}..custom_models_assignedteststatus atst2 on atst2.id = at.previous_status_id
    #             -- CHECKING IF TEST HAS BEEN STARTED BASED ON ASSIGNED TEST SECTION ACCESS TIMES ENTRIES
    #             OUTER APPLY (SELECT TOP 1 id as 'test_started_id' FROM {db_name}..custom_models_assignedtestsectionaccesstimes atsat1 WHERE atsat1.assigned_test_section_id in (SELECT id FROM {db_name}..custom_models_assignedtestsection ats1 WHERE ats1.assigned_test_id = at.id) AND time_type = 1 ORDER BY atsat1.id DESC) test_started_id
    #             -- CHECKING IF TIMED SECTION HAS BEEN ACCESSED
    #             OUTER APPLY (SELECT TOP 1 id as 'timed_section_has_been_accessed' FROM {db_name}..custom_models_assignedtestsectionaccesstimes atsat1 WHERE atsat1.assigned_test_section_id in (SELECT id FROM {db_name}..custom_models_assignedtestsection ats1 WHERE ats1.assigned_test_id = at.id AND ats1.test_section_time is not NULL) AND time_type = 1 ORDER BY atsat1.id DESC) timed_section_has_been_accessed
    #             -- GETTING THE NEXT TEST SECTION ID THAT NEEDS APPROVAL IF THE TEST HAS NOT BEEN STARTED YET (BASICALLY GETTING THE FIRST TEST SECTION THAT NEEDS APPROVAL)
    #             OUTER APPLY (SELECT TOP 1 id as 'non_started_test_next_test_section_id_that_needs_approval' FROM {db_name}..cms_models_testsection ts1 WHERE ts1.id in (SELECT test_section_id FROM {db_name}..custom_models_assignedtestsection ats3 WHERE ats3.assigned_test_id = at.id AND ts1.needs_approval = 1) ORDER BY ts1.[order]) non_started_test_next_test_section_id_that_needs_approval
    #             -- GETTING LATEST ACCESSED ASSIGNED TEST SECTION ID (NEEDED TO ONLY GET CURRENT/FUTURE ASSIGNED TEST SECTIONS)
    #             OUTER APPLY (SELECT TOP 1 assigned_test_section_id as 'latest_assigned_test_section_id' FROM {db_name}..custom_models_assignedtestsectionaccesstimes atsat2 WHERE atsat2.assigned_test_section_id in (SELECT id FROM {db_name}..custom_models_assignedtestsection ats2 WHERE ats2.assigned_test_id = at.id) AND atsat2.time_type = 1 ORDER BY atsat2.time DESC) latest_assigned_test_section_id
    #             -- GETTING THE NEXT TEST SECTION ID THAT NEEDS APPROVAL IF THE TEST HAS BEEN STARTED (USING THE 'latest_assigned_test_section_id' TO MAKE SURE WE ARE NOT GETTING A PAST TEST SESSION)
    #             OUTER APPLY (SELECT TOP 1 id as 'started_test_next_test_section_id_that_needs_approval' FROM {db_name}..cms_models_testsection ts2 WHERE ts2.id in (SELECT test_section_id FROM {db_name}..custom_models_assignedtestsection ats4 WHERE ats4.assigned_test_id = at.id AND ats4.id >= latest_assigned_test_section_id AND ts2.needs_approval = 1) ORDER BY ts2.[ORDER]) started_test_next_test_section_id_that_needs_approval
    #             -- GETTING TEST SECTION ID FROM THE LATEST APPROVE TA ACTION (IF IT EXISTS)
    #             OUTER APPLY (SELECT TOP 1 test_section_id as 'ta_approve_action_test_section_id' FROM {db_name}..custom_models_taactions ta WHERE ta.assigned_test_id = at.id AND ta.action_type_id = 'APPROVE' ORDER BY ta.modify_date DESC) ta_approve_action_test_section_id
    #             WHERE at.status_id in (SELECT id FROM {db_name}..custom_models_assignedteststatus WHERE codename in ('checked_in', 'pre_test', 'active', 'transition', 'locked', 'paused'))
    #         """.format(
    #             db_name=settings.DATABASES["default"]["NAME"]
    #         )
    #     )


def rollback_changes(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[ta_assigned_candidates_vw] AS
    #             SELECT
    #                 at.id,
    #                 at.status_id,
    #                 atst.codename as 'status_codename',
    #                 at.previous_status_id,
    #                 atst2.codename as 'previous_status_codename',
    #                 at.start_date,
    #                 at.modify_date,
    #                 at.submit_date,
    #                 at.test_access_code,
    #                 at.total_score,
    #                 at.ta_id,
    #                 at.test_id,
    #                 td.parent_code,
    #                 td.test_code,
    #                 td.en_name as  'test_name_en',
    #                 td.fr_name as  'test_name_fr',
    #                 at.test_section_id,
    #                 at.test_session_language_id,
    #                 at.username_id,
    #                 at.test_order_number,
    #                 at.en_converted_score,
    #                 at.fr_converted_score,
    #                 at.is_invalid,
    #                 at.uit_invite_id,
    #                 at.orderless_financial_data_id,
    #                 at.accommodation_request_id,
    #                 at.test_session_id,
    #                 u.id as 'candidate_id',
    #                 u.email as 'candidate_email',
    #                 u.first_name as 'candidate_first_name',
    #                 u.last_name as 'candidate_last_name',
    #                 u.birth_date as 'candidate_dob',
    #                 tau.first_name as 'ta_first_name',
    #                 tau.last_name as 'ta_last_name',
    #                 ta_approve_action_test_section_id,
    #                 CASE
    #                     WHEN test_started_id is NULL then 0
    #                     ELSE 1
    #                 END as 'test_started',
    #                 CASE
    #                     WHEN timed_section_has_been_accessed is NULL then 0
    #                     ELSE 1
    #                 END as 'timed_section_accessed',
    #                 CASE
    #                     WHEN test_started_id is NULL THEN non_started_test_next_test_section_id_that_needs_approval
    #                     ELSE started_test_next_test_section_id_that_needs_approval
    #                 END as 'next_test_section_id_that_needs_approval',
    #                 CASE
    #                     -- THERE IS NO TEST SECTION TO BE APPROVED (TEST STARTED OR NOT)
    #                     WHEN (test_started_id is NULL AND non_started_test_next_test_section_id_that_needs_approval IS NULL) OR (test_started_id is not NULL AND started_test_next_test_section_id_that_needs_approval is NULL) THEN 0
    #                     -- TEST NOT STARTED + LATEST APPROVED TEST SECTION ID (BY TA) IS THE SAME AS THE NEXT TEST SECTION ID THAT NEEDS APPROVAL
    #                     WHEN test_started_id is NULL AND ta_approve_action_test_section_id = non_started_test_next_test_section_id_that_needs_approval AND ta_approve_action_test_section_id is not NULL THEN 0
    #                     -- TEST STARTED + LATEST APPROVED TEST SECTION ID (BY TA) IS THE SAME AS THE NEXT TEST SECTION ID THAT NEEDS APPROVAL
    #                     WHEN test_started_id is not NULL AND ta_approve_action_test_section_id = started_test_next_test_section_id_that_needs_approval THEN 0
    #                     -- TEST STARTED (NO MORE SECTION TO BE APPROVED)
    #                     WHEN test_started_id is not NULL AND started_test_next_test_section_id_that_needs_approval is NULL THEN 0
    #                     -- NEEDS APPROVAL
    #                     ELSE 1
    #                 END as 'needs_approval'
    #             FROM {db_name}..custom_models_assignedtest at
    #             JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
    #             JOIN {db_name}..user_management_models_user u on u.username = at.username_id
    #             JOIN {db_name}..user_management_models_user tau on tau.username = at.ta_id
    #             LEFT JOIN {db_name}..custom_models_assignedteststatus atst on atst.id = at.status_id
    #             LEFT JOIN {db_name}..custom_models_assignedteststatus atst2 on atst2.id = at.previous_status_id
    #             -- CHECKING IF TEST HAS BEEN STARTED BASED ON ASSIGNED TEST SECTION ACCESS TIMES ENTRIES
    #             OUTER APPLY (SELECT TOP 1 id as 'test_started_id' FROM {db_name}..custom_models_assignedtestsectionaccesstimes atsat1 WHERE atsat1.assigned_test_section_id in (SELECT id FROM {db_name}..custom_models_assignedtestsection ats1 WHERE ats1.assigned_test_id = at.id) AND time_type = 1 ORDER BY atsat1.id DESC) test_started_id
    #             -- CHECKING IF TIMED SECTION HAS BEEN ACCESSED
    #             OUTER APPLY (SELECT TOP 1 id as 'timed_section_has_been_accessed' FROM {db_name}..custom_models_assignedtestsectionaccesstimes atsat1 WHERE atsat1.assigned_test_section_id in (SELECT id FROM {db_name}..custom_models_assignedtestsection ats1 WHERE ats1.assigned_test_id = at.id AND ats1.test_section_time is not NULL) AND time_type = 1 ORDER BY atsat1.id DESC) timed_section_has_been_accessed
    #             -- GETTING THE NEXT TEST SECTION ID THAT NEEDS APPROVAL IF THE TEST HAS NOT BEEN STARTED YET (BASICALLY GETTING THE FIRST TEST SECTION THAT NEEDS APPROVAL)
    #             OUTER APPLY (SELECT TOP 1 id as 'non_started_test_next_test_section_id_that_needs_approval' FROM {db_name}..cms_models_testsection ts1 WHERE ts1.id in (SELECT test_section_id FROM {db_name}..custom_models_assignedtestsection ats3 WHERE ats3.assigned_test_id = at.id AND ts1.needs_approval = 1) ORDER BY ts1.[order]) non_started_test_next_test_section_id_that_needs_approval
    #             -- GETTING LATEST ACCESSED ASSIGNED TEST SECTION ID (NEEDED TO ONLY GET CURRENT/FUTURE ASSIGNED TEST SECTIONS)
    #             OUTER APPLY (SELECT TOP 1 assigned_test_section_id as 'latest_assigned_test_section_id' FROM {db_name}..custom_models_assignedtestsectionaccesstimes atsat2 WHERE atsat2.assigned_test_section_id in (SELECT id FROM {db_name}..custom_models_assignedtestsection ats2 WHERE ats2.assigned_test_id = at.id) AND atsat2.time_type = 1 ORDER BY atsat2.time DESC) latest_assigned_test_section_id
    #             -- GETTING THE NEXT TEST SECTION ID THAT NEEDS APPROVAL IF THE TEST HAS BEEN STARTED (USING THE 'latest_assigned_test_section_id' TO MAKE SURE WE ARE NOT GETTING A PAST TEST SESSION)
    #             OUTER APPLY (SELECT TOP 1 id as 'started_test_next_test_section_id_that_needs_approval' FROM {db_name}..cms_models_testsection ts2 WHERE ts2.id in (SELECT test_section_id FROM {db_name}..custom_models_assignedtestsection ats4 WHERE ats4.assigned_test_id = at.id AND ats4.id >= latest_assigned_test_section_id AND ts2.needs_approval = 1) ORDER BY ts2.[ORDER]) started_test_next_test_section_id_that_needs_approval
    #             -- GETTING TEST SECTION ID FROM THE LATEST APPROVE TA ACTION (IF IT EXISTS)
    #             OUTER APPLY (SELECT TOP 1 test_section_id as 'ta_approve_action_test_section_id' FROM {db_name}..custom_models_taactions ta WHERE ta.assigned_test_id = at.id AND ta.action_type_id = 'APPROVE' ORDER BY ta.modify_date DESC) ta_approve_action_test_section_id
    #             WHERE at.status_id in (SELECT id FROM {db_name}..custom_models_assignedteststatus WHERE codename in ('checked_in', 'pre_test', 'active', 'transition', 'locked', 'paused'))
    #         """.format(
    #             db_name=settings.DATABASES["default"]["NAME"]
    #         )
    #     )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0150_adaptedtestsreportvw",
        ),
        (
            "custom_models",
            "0083_remove_assignedtest_must_be_a_unique_status_username_tac_test_combination_and_more",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
