from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[ta_extended_profile_vw] AS
                SELECT
                    tep.id,
                    u.id as 'user_id',
                    u.first_name,
                    u.last_name,
                    u.email,
                    dep.DEPT_ID as 'dept_id',
                    dep.EABRV as 'dept_eabrv',
                    dep.FABRV as 'dept_fabrv',
                    dep.EDESC as 'dept_edesc',
                    dep.FDESC as 'dept_fdesc'
                FROM {db_name}..user_management_models_taextendedprofile tep
                JOIN {db_name}..user_management_models_user u on u.id = tep.user_id
                LEFT JOIN {db_name}..CAT_REF_DEPARTMENTS_VW dep on dep.DEPT_ID = tep.department_id
                """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0230_uitrelatedcandidatesvw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
