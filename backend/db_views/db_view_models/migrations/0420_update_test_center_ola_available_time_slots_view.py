from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER FUNCTION get_test_assessor_availabilities_by_date_fn (@date date, @test_center_id int)
                RETURNS TABLE
                AS RETURN
                SELECT
                    TOP 100 PERCENT
                    tcts.start_time,
                    tcts.end_time,
                    tcts.day_of_week_id,
                    tcts.assessed_language_id,
					SUM(available_assessors.count) as 'nbr_of_available_assessors'
                FROM {db_name}..custom_models_testcenterolatimeslot tcts
                JOIN {db_name}..custom_models_testcenter tc on tc.id = tcts.test_center_id
				OUTER APPLY (SELECT COUNT(*) as 'count' FROM {db_name}..custom_models_testcenterolatimeslotassessoravailability tcoaa WHERE tcoaa.test_center_ola_time_slot_id = tcts.id) available_assessors
                -- ONLY CONSIDERING RESPECTIVE AUTHORIZED OLA TEST CENTERS WHERE DAY OF WEEK MATCHES THE PROVIDED DATE
                WHERE tc.ola_authorized = 1 AND tcts.test_center_id = @test_center_id AND tcts.day_of_week_id = DATEPART(dw, @date)
                GROUP BY tcts.start_time, tcts.end_time, tcts.day_of_week_id, tcts.assessed_language_id
                ORDER BY tcts.day_of_week_id
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0419_remove_testcenterolatimeslotsvw_day_of_week_codename",
        ),
        ("custom_models", "0208_add_ola_prioritization"),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
