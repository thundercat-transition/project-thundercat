from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_center_test_sessions_vw] AS
                SELECT
                    CAST(ts.id as varchar) as 'id',
                    ts.test_center_id,
                    tc.name as 'test_center_name',
                    tc.postal_code as 'test_center_postal_code',
                    ts.test_session_order,
                    ts.assessment_process_assigned_test_specs_id,
                    tc.security_email as 'test_center_security_email',
                    tsd.test_center_room_id,
                    rv.name as 'room_name',
                    IIF(ts.is_standard = 1, crc.count, NULL) as 'spaces_booked',
                    IIF(ts.is_standard = 1, (tsd.spaces_available-crc.count), NULL) as 'spaces_unbooked',
                    tsd.spaces_available as 'session_spaces_available',
                    rv.max_occupancy as 'room_max_occupancy',
                    tsd.id as 'test_center_test_session_data_id',
                    tsd.open_to_ogd,
                    tc.country_id,
                    c.EABRV as 'country_eabrv',
                    c.EDESC as 'country_edesc',
                    c.FABRV as 'country_fabrv',
                    c.FDESC as 'country_fdesc',
                    tc.department_id,
                    dep.EABRV as 'dept_eabrv',
                    dep.EDESC as 'dept_edesc',
                    dep.FABRV as 'dept_fabrv',
                    dep.FDESC as 'dept_fdesc',
                    DATEADD(hour, -tc.booking_delay, tsd.start_time) as 'latest_booking_modification_time',
                    tc.booking_delay as 'booking_delay',
                    tsd.date,
                    tsd.start_time,
                    tsd.end_time,
                    tsd.spaces_available,
                    ta_u.id as 'test_assessor_user_id',
                    ta_u.email as 'test_assessor_user_email',
                    ta_u.first_name as 'test_assessor_user_first_name',
                    ta_u.last_name as 'test_assessor_user_last_name',
                    tsd.test_skill_type_id,
                    tst.codename as 'test_skill_type_codename',
                    stte.text as 'test_skill_type_en_name',
                    sttf.text as 'test_skill_type_fr_name',
                    tsd.test_skill_sub_type_id,
                    apatsv.test_skill_sub_type_codename,
                    CASE
                        -- SLE Sub Type
                        WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'sle') THEN (SELECT text FROM {db_name}..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = tsd.test_skill_sub_type_id AND language_id = 1)
                        ELSE
                            -- OCCUPATIONAL Sub Type
                            CASE
                                WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM {db_name}..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = tsd.test_skill_sub_type_id AND language_id = 1)
                            ELSE
                                NULL
                            END
                    END AS 'test_skill_sub_type_en_name',
                    CASE
                        -- SLE Sub Type
                        WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'SLE') THEN (SELECT text FROM {db_name}..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = tsd.test_skill_sub_type_id AND language_id = 2)
                        ELSE
                            -- OCCUPATIONAL Sub Type
                            CASE
                                WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM {db_name}..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = tsd.test_skill_sub_type_id AND language_id = 2)
                            ELSE
                                NULL
                            END
                    END AS 'test_skill_sub_type_fr_name',
                    CASE
                        WHEN tst.codename = 'sle' AND apatsv.test_skill_sub_type_codename in ('oe', 're', 'we') THEN 1
                        WHEN tst.codename = 'sle' AND apatsv.test_skill_sub_type_codename in ('of', 'rf', 'wf') THEN 2
                        ELSE NULL
                    END AS 'language_id',
                    ts.is_standard,
                    ts.user_accommodation_file_id,
                    -- is_alternate_test_request
                    0 as 'is_alternate_test_request',
                    -- user_id
                    NULL as 'user_id',
                    -- user_username
                    NULL as 'user_username',
                    -- user_email
                    NULL as 'user_email',
                    -- user_first_name
                    NULL as 'user_first_name',
                    -- user_last_name 
                    NULL as 'user_last_name',
                    apatsv.assessment_process_closing_date as 'closing_date',
                    td.id as 'test_id',
                    td.parent_code as 'parent_code',
                    td.test_code as 'test_code',
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    td.version as 'version',
                    uaftta.specified_test_description
                FROM {db_name}..custom_models_testcentertestsessions ts
                LEFT JOIN {db_name}..custom_models_testcenter tc on tc.id = ts.test_center_id
                JOIN {db_name}..custom_models_testcentertestsessiondata tsd on tsd.id = ts.test_session_data_id
                LEFT JOIN {db_name}..user_management_models_user ta_u on ta_u.id = tsd.test_assessor_user_id
                LEFT JOIN {db_name}..CAT_REF_DEPARTMENTS_VW dep on dep.DEPT_ID = tc.department_id
                LEFT JOIN {db_name}..CAT_REF_COUNTRY_VW c on c.CNTRY_ID = tc.country_id
                LEFT JOIN {db_name}..test_center_rooms_vw rv on rv.id = tsd.test_center_room_id
                JOIN {db_name}..cms_models_testskilltype tst on tst.id = tsd.test_skill_type_id
                JOIN {db_name}..cms_models_testskilltypetext stte on stte.test_skill_type_id = tsd.test_skill_type_id AND stte.language_id = 1
                JOIN {db_name}..cms_models_testskilltypetext sttf on sttf.test_skill_type_id = tsd.test_skill_type_id AND sttf.language_id = 2
                LEFT JOIN {db_name}..custom_models_useraccommodationfiletesttoadminister uaftta on uaftta.user_accommodation_file_id = ts.user_accommodation_file_id
                LEFT JOIN {db_name}..cms_models_testdefinition td on td.id = uaftta.test_id
                LEFT JOIN {db_name}..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = ts.assessment_process_assigned_test_specs_id
                OUTER APPLY (SELECT COUNT(*) as count FROM {db_name}..custom_models_consumedreservationcodes WHERE test_session_id=ts.id) crc
                WHERE ts.is_standard = 1

                UNION

                -- Accommodation Requests
                SELECT
                    -- Show testsession ID if exists, otherwise show consumed reservation code ID with "-CRC" as suffix (this is fixing potential duplicated IDs which might cause candidates to have unrelated sessions/accommodation requests associated to their manage reservations table)
                    IIF(crc.test_session_id is not NULL, CAST(ts.id as varchar), CAST(crc.id as varchar) + '-CRC') as 'id',
                    tc.id,
                    tc.name as 'test_center_name',
                    tc.postal_code as 'test_center_postal_code',
                    ts.test_session_order,
                    ts.assessment_process_assigned_test_specs_id,
                    tc.security_email as 'test_center_security_email',
                    IIF(crc.test_session_id is not NULL, tsd.test_center_room_id, NULL) as 'test_center_room_id',
                    IIF(crc.test_session_id is not NULL, rv.name , NULL) as 'room_name',
                    -- spaced_booked
                    NULL,
                    -- spaces_unbooked
                    NULL,
                    -- session_spaces_available
                    NULL,
                    -- room_max_occupancy
                    NULL,
                    IIF(crc.test_session_id is not NULL, tsd.id, NULL) as 'test_center_test_session_data_id',
                    -- open_to_ogd
                    NULL,
                    tc.country_id,
                    c.EABRV as 'country_eabrv',
                    c.EDESC as 'country_edesc',
                    c.FABRV as 'country_fabrv',
                    c.FDESC as 'country_fdesc',
                    tc.department_id,
                    dep.EABRV as 'dept_eabrv',
                    dep.EDESC as 'dept_edesc',
                    dep.FABRV as 'dept_fabrv',
                    dep.FDESC as 'dept_fdesc',
                    --DATEADD(hour, -tc.booking_delay, tsd.start_time) as 'latest_booking_modification_time',
                    NULL,
                    tc.booking_delay as 'booking_delay',
                    IIF(crc.test_session_id is not NULL, tsd.date, NULL) as 'date',
                    IIF(crc.test_session_id is not NULL, tsd.start_time, NULL) as 'start_time',
                    IIF(crc.test_session_id is not NULL, tsd.end_time, NULL) as 'end_time',
                    --tsd.spaces_available,
                    NULL,
                    ta_u.id as 'test_assessor_user_id',
                    ta_u.email as 'test_assessor_user_email',
                    ta_u.first_name as 'test_assessor_user_first_name',
                    ta_u.last_name as 'test_assessor_user_last_name',
                    apats.test_skill_type_id,
                    tst.codename as 'test_skill_type_codename',
                    stte.text as 'test_skill_type_en_name',
                    sttf.text as 'test_skill_type_fr_name',
                    apats.test_skill_sub_type_id,
                    apatsv.test_skill_sub_type_codename,
                    CASE
                        -- SLE Sub Type
                        WHEN apats.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'sle') THEN (SELECT text FROM {db_name}..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = apats.test_skill_sub_type_id AND language_id = 1)
                        ELSE
                            -- OCCUPATIONAL Sub Type
                            CASE
                                WHEN apats.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM {db_name}..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = apats.test_skill_sub_type_id AND language_id = 1)
                            ELSE
                                NULL
                            END
                    END AS 'test_skill_sub_type_en_name',
                    CASE
                        -- SLE Sub Type
                        WHEN apats.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'SLE') THEN (SELECT text FROM {db_name}..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = apats.test_skill_sub_type_id AND language_id = 2)
                        ELSE
                            -- OCCUPATIONAL Sub Type
                            CASE
                                WHEN apats.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM {db_name}..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = apats.test_skill_sub_type_id AND language_id = 2)
                            ELSE
                                NULL
                            END
                    END AS 'test_skill_sub_type_fr_name',
                    CASE
                        WHEN tst.codename = 'sle' AND apatsv.test_skill_sub_type_codename in ('oe', 're', 'we') THEN 1
                        WHEN tst.codename = 'sle' AND apatsv.test_skill_sub_type_codename in ('of', 'rf', 'wf') THEN 2
                        ELSE NULL
                    END AS 'language_id',
                    0 as 'is_standard',
                    uaf.id as 'user_accommodation_file_id',
                    uaf.is_alternate_test_request,
                    u.id as 'user_id',
                    u.username as 'user_username',
                    u.email as 'user_email',
                    u.first_name as 'user_first_name',
                    u.last_name as 'user_last_name ',
                    apatsv.assessment_process_closing_date as 'closing_date',
                    td.id as 'test_id',
                    td.parent_code as 'parent_code',
                    td.test_code as 'test_code',
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    td.version as 'version',
                    uaftta.specified_test_description
                FROM {db_name}..custom_models_consumedreservationcodes crc
                JOIN {db_name}..custom_models_useraccommodationfile uaf on crc.user_accommodation_file_id = uaf.id
                JOIN {db_name}..user_management_models_user u on u.id = uaf.user_id
                JOIN {db_name}..custom_models_testcenter tc on tc.id = uaf.test_center_id
                JOIN {db_name}..CAT_REF_DEPARTMENTS_VW dep on dep.DEPT_ID = tc.department_id
                JOIN {db_name}..CAT_REF_COUNTRY_VW c on c.CNTRY_ID = tc.country_id
                JOIN {db_name}..custom_models_assessmentprocessassignedtestspecs apats on apats.id = crc.assessment_process_assigned_test_specs_id
                JOIN {db_name}..cms_models_testskilltype tst on tst.id = apats.test_skill_type_id
                JOIN {db_name}..cms_models_testskilltypetext stte on stte.test_skill_type_id = apats.test_skill_type_id AND stte.language_id = 1
                JOIN {db_name}..cms_models_testskilltypetext sttf on sttf.test_skill_type_id = apats.test_skill_type_id AND sttf.language_id = 2
                LEFT JOIN {db_name}..custom_models_testcentertestsessions ts on ts.assessment_process_assigned_test_specs_id = crc.assessment_process_assigned_test_specs_id
                LEFT JOIN {db_name}..custom_models_testcentertestsessiondata tsd on tsd.id = ts.test_session_data_id
                LEFT JOIN {db_name}..user_management_models_user ta_u on ta_u.id = tsd.test_assessor_user_id
                LEFT JOIN {db_name}..test_center_rooms_vw rv on rv.id = tsd.test_center_room_id
                LEFT JOIN {db_name}..custom_models_useraccommodationfiletesttoadminister uaftta on uaftta.user_accommodation_file_id = uaf.id
                LEFT JOIN {db_name}..cms_models_testdefinition td on td.id = uaftta.test_id
                LEFT JOIN {db_name}..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = crc.assessment_process_assigned_test_specs_id
                """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0366_update_user_accommodation_file_report_data_view",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
