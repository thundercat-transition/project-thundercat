from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[user_accommodation_file_vw] AS
                SELECT
                    uaf.id,
                    uaf.comments,
                    uaf.is_uit,
                    uaf.modify_date,
                    uaf.created_date,
                    uaf.last_modified_by_user_id,
                    uafs.id AS 'status_id',
                    uafs.codename AS 'status_codename',
                    uaf.test_center_id,
                    ummu.id AS 'user_id',
                    ummu.username AS 'user_username',
                    ummu.email AS 'user_email',
                    ummu.first_name AS 'user_first_name',
                    ummu.last_name AS 'user_last_name'
                FROM {db_name}..custom_models_useraccommodationfile uaf
                JOIN {db_name}..user_management_models_user ummu ON ummu.id = uaf.user_id
                JOIN {db_name}..custom_models_useraccommodationfilestatus uafs ON uafs.id = uaf.status_id
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0244_bookedreservationsvw_user_accommodation_file_id",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
