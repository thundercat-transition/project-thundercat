from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[ta_history_vw] AS


# 			SELECT t1.id as [assigned_test_id],
# 				t3.first_name as [ta_first_name],
# 				t3.last_name as [ta_last_name],
#                 cup.pri as [pri],
#                 cup.military_nbr [military_number],
#                 t1.ta_id as [ta_username],
#                 cup.goc_email [goc_email],
#                 -- Need to add requesting org from uitinvite and/or HR request (supervised), Extract based on interface language.
#                 CONCAT(IIF(t1.uit_invite_id is null, CONCAT(apvw.dept_eabrv, ' - ',apvw.dept_edesc), CONCAT(depvw.eabrv,' - ', depvw.EDESC)),'/',IIF(t1.uit_invite_id is null, CONCAT(apvw.dept_fabrv, ' - ',apvw.dept_fdesc), CONCAT(depvw.fabrv,' - ', depvw.FDESC)) ) [requesting_organization],
#                 --IIF(t1.uit_invite_id is null, CONCAT(apvw.dept_fabrv, ' - ',apvw.dept_fdesc), CONCAT(depvw.fabrv,' - ', depvw.FDESC))
#                 td.test_code as [test],
#                 CAST(t1.submit_date as date) [test_submit_date],
#                 -- cdd.id [CAT User ID],
#                 CONCAT(cdd.last_name, ', ', cdd.first_name) [candidate_name],
#                 t1.username_id as [candidate_username]

#             FROM {db_name}..custom_models_assignedtest t1
#                 JOIN {db_name}..user_management_models_user cdd on t1.username_id = cdd.username
#                 CROSS APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicalcustomuserpermissions hcup WHERE hcup.permission_id = 1 AND hcup.user_id = t1.ta_id AND history_type = '+' AND hcup.history_date < t1.start_date) cup
#                 JOIN {db_name}..cms_models_testdefinition td on td.id = t1.test_id
#                 JOIN {db_name}..user_management_models_user t3 on t3.username = t1.ta_id
#                 LEFT JOIN {db_name}..custom_models_uitinvites uit on uit.id = t1.uit_invite_id
#                 LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on t1.orderless_financial_data_id = ofd.id
#                 LEFT JOIN {db_name}..CAT_REF_DEPARTMENTS_VW depvw on depvw.DEPT_ID = ofd.department_ministry_id
#                 LEFT JOIN {db_name}..custom_models_consumedreservationcodes crc on crc.assigned_test_id = t1.id
#                 LEFT JOIN {db_name}..custom_models_assessmentprocessassignedtestspecs apts on apts.id = crc.assessment_process_assigned_test_specs_id
#                 LEFT JOIN {db_name}..assessment_process_vw apvw on apvw.id = apts.assessment_process_id
#     """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0146_remove_assessmentprocessresultscandidatesvw_email_and_more",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
