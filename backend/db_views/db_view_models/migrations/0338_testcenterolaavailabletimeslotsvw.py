# Generated by Django 4.2.11 on 2024-09-17 13:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("db_view_models", "0337_create_test_center_ola_available_time_slots_view"),
    ]

    operations = [
        migrations.CreateModel(
            name="TestCenterOlaAvailableTimeSlotsVw",
            fields=[
                (
                    "id",
                    models.IntegerField(
                        db_column="id", primary_key=True, serialize=False
                    ),
                ),
                ("date", models.DateField(db_column="date")),
                ("start_time", models.TimeField(db_column="start_time")),
                ("end_time", models.TimeField(db_column="end_time")),
                ("day_of_week_id", models.IntegerField(db_column="day_of_week_id")),
                (
                    "assessed_language_id",
                    models.IntegerField(db_column="assessed_language_id"),
                ),
                (
                    "total_available_test_assessors",
                    models.IntegerField(db_column="total_available_test_assessors"),
                ),
                (
                    "nbr_of_reserved_assessors",
                    models.IntegerField(db_column="nbr_of_reserved_assessors"),
                ),
                (
                    "remaining_available_test_assessors",
                    models.IntegerField(db_column="remaining_available_test_assessors"),
                ),
            ],
            options={
                "db_table": "test_center_ola_available_time_slots_vw",
                "managed": False,
            },
        ),
    ]
