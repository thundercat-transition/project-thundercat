from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[item_content_drafts_vw] AS
				SELECT 
					c.id as 'item_content_id',
					c.system_id,
					c.content_type,
					c.modify_date as 'item_content_modify_date',
					c.content_order,
					c.item_id,
					c.user_id,
					u.username,
					ct.id as 'item_content_text_id',
					ct.text,
					ct.modify_date as 'item_content_text_modify_date',
					ct.language_id
				FROM {db_name}..cms_models_itemcontentdrafts c
				JOIN {db_name}..user_management_models_user u on u.id = c.user_id
				JOIN {db_name}..cms_models_itemcontenttextdrafts ct on ct.item_content_id = c.id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[item_content_drafts_vw] AS


# 			SELECT
# 				c.id as 'item_content_id',
# 				c.system_id,
# 				c.content_type,
# 				c.modify_date as 'item_content_modify_date',
# 				c.content_order,
# 				c.item_id,
# 				c.username_id,
# 				ct.id as 'item_content_text_id',
# 				ct.text,
# 				ct.modify_date as 'item_content_text_modify_date',
# 				ct.language_id
# 			FROM {db_name}..cms_models_itemcontentdrafts c
# 			JOIN {db_name}..cms_models_itemcontenttextdrafts ct on ct.item_content_id = c.id
#     """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0160_remove_itemdraftsvw_username_id_itemdraftsvw_user_id_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
