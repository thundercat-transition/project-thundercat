from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[assessment_process_results_vw] AS
                SELECT
                    id,
                    user_id,
                    reference_number,
                    closing_date,
                    (SELECT COUNT(DISTINCT email) FROM {db_name}..custom_models_assessmentprocessassignedtestspecs WHERE assessment_process_id = ap.id) AS "number_of_candidates",
                    0 AS "number_of_tests_taken"
                FROM {db_name}..custom_models_assessmentprocess ap
                WHERE request_sent = 'TRUE'
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0088_candidatereservationsvw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
