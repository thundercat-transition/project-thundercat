from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[assigned_tests_vw] AS
                SELECT
					at.id,
					ats1.id as 'status_id',
					ats1.codename as 'status_codename',
					ats2.id as 'previous_status_id',
					ats2.codename as 'previous_status_codename',
					at.start_date,
					at.modify_date,
					at.submit_date,
					at.test_access_code,
					at.total_score,
					at.test_id,
					at.ta_user_id,
					ta.username as 'ta_username',
					at.test_section_id,
					at.test_session_language_id,
					at.user_id,
					u.username,
					at.test_order_number,
					at.en_converted_score,
					at.fr_converted_score,
					at.is_invalid,
					at.uit_invite_id,
					at.orderless_financial_data_id,
					at.accommodation_request_id,
					at.test_session_id
				FROM {db_name}..custom_models_assignedtest at
				JOIN {db_name}..user_management_models_user u on u.id = at.user_id
				JOIN {db_name}..user_management_models_user ta on ta.id = at.ta_user_id
				LEFT JOIN {db_name}..custom_models_assignedteststatus ats1 on ats1.id = at.status_id
				LEFT JOIN {db_name}..custom_models_assignedteststatus ats2 on ats2.id = at.previous_status_id
            """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[assigned_tests_vw] AS
    #             SELECT


# 				at.id,
# 				ats1.id as 'status_id',
# 				ats1.codename as 'status_codename',
# 				ats2.id as 'previous_status_id',
# 				ats2.codename as 'previous_status_codename',
# 				at.start_date,
# 				at.modify_date,
# 				at.submit_date,
# 				at.test_access_code,
# 				at.total_score,
# 				at.test_id,
# 				at.ta_id,
# 				at.test_section_id,
# 				at.test_session_language_id,
# 				at.username_id,
# 				at.test_order_number,
# 				at.en_converted_score,
# 				at.fr_converted_score,
# 				at.is_invalid,
# 				at.uit_invite_id,
# 				at.orderless_financial_data_id,
# 				at.accommodation_request_id,
# 				at.test_session_id
# 			FROM {db_name}..custom_models_assignedtest at
# 			LEFT JOIN {db_name}..custom_models_assignedteststatus ats1 on ats1.id = at.status_id
# 			LEFT JOIN {db_name}..custom_models_assignedteststatus ats2 on ats2.id = at.previous_status_id
#         """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0168_remove_testdatavw_allowed_ta_usernames_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
