from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[aae_reports_number_of_closed_requests_vw] AS
                SELECT
                    uaf.id,
                    uaf.created_date,
                    uafs.id as 'status_id',
                    uafs.codename as 'status_codename',
                    first_completed_by_aae_date.history_date as 'first_completed_by_aae_date',
                    first_approved_by_candidate_date.history_date as 'first_approved_by_candidate_date',
                    tta.test_id,
                    -- TEST SKILL TYPE ID
                    ts.test_skill_type_id,
                    tst.codename as 'test_skill_type_codename',
                    type_e.text as 'test_skill_type_name_en',
                    type_f.text as 'test_skill_type_name_fr',
                    -- TEST SKILL SUB-TYPE ID
                    CASE
                        WHEN ts.test_skill_type_id = 2 THEN sled.id
                        WHEN ts.test_skill_type_id = 3 THEN occd.id
                        WHEN ts.test_skill_type_id = 1 THEN NULL
                    END as 'test_skill_sub_type_id',
                    -- TEST SKILL SUB-TYPE CODENAME
                    CASE
                        WHEN ts.test_skill_type_id = 2 THEN sled.codename
                        WHEN ts.test_skill_type_id = 3 THEN occd.codename
                        WHEN ts.test_skill_type_id = 1 THEN NULL
                    END as 'test_skill_sub_type_codename',
                    -- TEST SKILL SUB-TYPE NAME (EN)
                    CASE
                        WHEN ts.test_skill_type_id = 2 THEN sle_sub_type_e.text
                        WHEN ts.test_skill_type_id = 3 THEN occ_sub_type_e.text
                        WHEN ts.test_skill_type_id = 1 THEN NULL
                    END as 'test_skill_sub_type_name_en',
                    -- TEST SKILL SUB-TYPE NAME (FR)
                    CASE
                        WHEN ts.test_skill_type_id = 2 THEN sle_sub_type_f.text
                        WHEN ts.test_skill_type_id = 3 THEN occ_sub_type_f.text
                        WHEN ts.test_skill_type_id = 1 THEN NULL
                    END as 'test_skill_sub_type_name_fr'
                FROM {db_name}..custom_models_useraccommodationfile uaf
                JOIN {db_name}..custom_models_useraccommodationfilestatus uafs on uafs.id = uaf.status_id
                JOIN {db_name}..custom_models_useraccommodationfiletesttoadminister tta on tta.user_accommodation_file_id = uaf.id
                -- GETTING FIRST COMPLETED BY AAE DATE
                OUTER APPLY (SELECT TOP 1 huaf.history_date 
                            FROM {db_name}..custom_models_historicaluseraccommodationfile huaf 
                            WHERE huaf.id = uaf.id AND huaf.status_id = (SELECT uafs_2.id 
                                                                        FROM {db_name}..custom_models_useraccommodationfilestatus uafs_2 
                                                                        WHERE uafs_2.codename = 'pending_approval'
                                                                        ) 
                            ORDER BY huaf.history_date ASC
                            ) first_completed_by_aae_date
                -- GETTING FIRST APPROVED DATE
                OUTER APPLY (SELECT TOP 1 huaf.history_date 
                            FROM {db_name}..custom_models_historicaluseraccommodationfile huaf 
                            WHERE huaf.id = uaf.id AND huaf.status_id = (SELECT uafs_2.id 
                                                                        FROM {db_name}..custom_models_useraccommodationfilestatus uafs_2 
                                                                        WHERE uafs_2.codename = 'completed'
                                                                        ) 
                            ORDER BY huaf.history_date ASC
                            ) first_approved_by_candidate_date
                LEFT JOIN {db_name}..cms_models_testskill ts on ts.test_definition_id = tta.test_id
                LEFT JOIN {db_name}..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM {db_name}..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM {db_name}..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN {db_name}..cms_models_testskillsle tssle on tssle.test_skill_id = ts.id
                LEFT JOIN {db_name}..cms_models_testskillsledesc sled on sled.id = tssle.test_skill_sle_desc_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM {db_name}..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM {db_name}..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                LEFT JOIN {db_name}..cms_models_testskilloccupational tsocc on tsocc.test_skill_id = ts.id
                LEFT JOIN {db_name}..cms_models_testskilloccupationaldesc occd on occd.id = tsocc.test_skill_occupational_desc_id
                OUTER APPLY (SELECT TOP 1 occdte.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = occd.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = occd.id AND occdtf.language_id = 2) occ_sub_type_f
                -- ONLY GETTING COMPLETED REQUESTS
                WHERE uafs.codename = 'completed'
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            "DROP VIEW IF EXISTS [dbo].[aae_reports_number_of_closed_requests_vw]"
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0351_aaereportsnumberofrequestsreceivedvw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
