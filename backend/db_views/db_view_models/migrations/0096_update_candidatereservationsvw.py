from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("db_view_models", "0095_update_candidate_reservation_view"),
    ]

    operations = [
        migrations.AddField(
            model_name="CandidateReservationsVW",
            name="test_center_test_session_data_id",
            field=models.IntegerField(db_column="TEST_CENTER_TEST_SESSION_DATA_ID"),
        ),
    ]
