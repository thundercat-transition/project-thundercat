from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[candidate_reservations_vw] AS
    #             SELECT


# 				rc.id as 'id',
# 				rc.reservation_code as 'reservation_code',
# 				apv.closing_date as 'assessment_process_closing_date',
# 				apv.allow_booking_external_tc as 'allow_booking_external_tc',
# 				apv.dept_id as 'dept_id',
# 				apv.dept_eabrv as 'dept_eabrv',
# 				apv.dept_fabrv as 'dept_fabrv',
# 				apv.dept_edesc as 'dept_edesc',
# 				apv.dept_fdesc as 'dept_fdesc',
# 				tst.id as 'test_skill_type_id',
# 				type_e.text as 'test_skill_type_en_name',
# 				type_f.text as 'test_skill_type_fr_name',
# 				ats.test_skill_sub_type_id as 'test_skill_sub_type_id',
# 				CASE
# 					WHEN tst.id = 2 THEN sub_type_e.text
# 					ELSE
# 						NULL
# 				END AS 'test_skill_sub_type_en_name',
# 				CASE
# 					WHEN tst.id = 2 THEN sub_type_f.text
# 					ELSE
# 						NULL
# 				END AS 'test_skill_sub_type_fr_name',
# 				apv.request_sent
# 			FROM (SELECT id, reservation_code, assessment_process_assigned_test_specs_id FROM {db_name}..custom_models_reservationcodes
#                     UNION SELECT id, reservation_code, assessment_process_assigned_test_specs_id FROM {db_name}..custom_models_ConsumedReservationCodes) rc
# 			JOIN {db_name}..custom_models_assessmentprocessassignedtestspecs ats ON rc.assessment_process_assigned_test_specs_id=ats.id
# 			JOIN {db_name}..assessment_process_vw apv ON apv.id = ats.assessment_process_id
# 			JOIN {db_name}..cms_models_testskilltype tst ON tst.id = ats.test_skill_type_id
# 			OUTER APPLY (SELECT TOP 1 tstte.text FROM {db_name}..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
# 			OUTER APPLY (SELECT TOP 1 tsttf.text FROM {db_name}..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
# 			LEFT JOIN {db_name}..cms_models_testskillsledesc sled ON sled.id = ats.test_skill_sub_type_id
# 			OUTER APPLY (SELECT TOP 1 sledte.text FROM {db_name}..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sub_type_e
# 			OUTER APPLY (SELECT TOP 1 sledtf.text FROM {db_name}..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sub_type_f
#     """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


def rollback_changes(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[candidate_reservations_vw] AS
    #             SELECT


# 				rc.id as "id",
# 				rc.reservation_code as 'reservation_code',
# 				apv.closing_date as 'assessment_process_closing_date',
# 				tst.id as 'test_skill_type_id',
# 				type_e.text as 'test_skill_type_en_name',
# 				type_f.text as 'test_skill_type_fr_name',
# 				ats.test_skill_sub_type_id as 'test_skill_sub_type_id',
# 				CASE
# 					WHEN tst.id = 2 THEN sub_type_e.text
# 					ELSE
# 						NULL
# 				END AS 'test_skill_sub_type_en_name',
# 				CASE
# 					WHEN tst.id = 2 THEN sub_type_f.text
# 					ELSE
# 						NULL
# 				END AS 'test_skill_sub_type_fr_name',
# 				apv.request_sent,
#                 tctsd.id as 'test_center_test_session_data_id',
# 				tctsd.date,
# 				tctsd.start_time,
# 				tctsd.end_time,
# 				tc.name as 'test_center_name',
# 				tc.address as 'test_center_address',
# 				tc.city as 'test_center_city',
# 				tc.postal_code as 'test_center_postal_code',
# 				tc.province as 'test_center_province',
# 				tc.country as 'test_center_country'
# 			FROM (SELECT id, reservation_code, assessment_process_assigned_test_specs_id FROM {db_name}..custom_models_reservationcodes
#                     UNION SELECT id, reservation_code, assessment_process_assigned_test_specs_id FROM {db_name}..custom_models_ConsumedReservationCodes) rc
# 			JOIN {db_name}..custom_models_assessmentprocessassignedtestspecs ats ON rc.assessment_process_assigned_test_specs_id=ats.id
# 			JOIN {db_name}..assessment_process_vw apv ON apv.id = ats.assessment_process_id
# 			JOIN {db_name}..cms_models_testskilltype tst ON tst.id = ats.test_skill_type_id
# 			OUTER APPLY (SELECT TOP 1 tstte.text FROM {db_name}..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
# 			OUTER APPLY (SELECT TOP 1 tsttf.text FROM {db_name}..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
# 			LEFT JOIN {db_name}..cms_models_testskillsledesc sled ON sled.id = ats.test_skill_sub_type_id
# 			OUTER APPLY (SELECT TOP 1 sledte.text FROM {db_name}..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sub_type_e
# 			OUTER APPLY (SELECT TOP 1 sledtf.text FROM {db_name}..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sub_type_f
# 			RIGHT JOIN {db_name}..custom_models_testcentertestsessiondata tctsd ON tctsd.test_skill_type_id=tst.id AND tctsd.test_skill_sub_type_id=ats.test_skill_sub_type_id AND apv.closing_date >= tctsd.date
# 			LEFT JOIN {db_name}..custom_models_testcentertestsessions tcts ON tctsd.id=tcts.test_session_data_id
# 			LEFT JOIN {db_name}..custom_models_testcenter tc ON tc.id=tcts.test_center_id
#     """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0105_update_testcentertestsessionsvw_other_details",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
