from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("db_view_models", "0375_update_test_center_test_sessions_vw"),
    ]

    operations = [
        migrations.AddField(
            model_name="testcentertestsessionsvw",
            name="meeting_link",
            field=models.CharField(
                db_column="MEETING_LINK", max_length=255, blank=False, null=False
            ),
        )
    ]
