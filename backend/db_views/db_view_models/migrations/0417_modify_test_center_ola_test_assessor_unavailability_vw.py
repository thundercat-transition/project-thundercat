from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_center_ola_test_assessor_unavailability_vw] AS
                SELECT
                    tcoau.id as 'id',
                    tcoau.test_center_ola_test_assessor_id as 'test_center_ola_test_assessor_id',
                    tcota.user_id as 'assessor_id',
                    tcota.supervisor as 'is_supervisor',
                    u.first_name as 'first_name',
                    u.last_name as 'last_name',
                    u.email as 'email',
                    tcota.test_center_id as 'test_center_id',
                    tc.name as 'test_center_name',
                    tccte.text as 'test_center_city_en',
                    tcctf.text as 'test_center_city_fr',
                    tcoau.start_date as 'start_date',
                    tcoau.end_date as 'end_date',
                    tcoau.reason as 'reason'

                    FROM {db_name}..custom_models_testcenterolaassessorunavailability tcoau
                    LEFT JOIN {db_name}..custom_models_testcenterolatestassessor tcota on tcota.id = tcoau.test_center_ola_test_assessor_id
                    LEFT JOIN {db_name}..user_management_models_user u on u.id = tcota.user_id
                    LEFT JOIN {db_name}..custom_models_testcenter tc on tc.id = tcota.test_center_id
                    LEFT JOIN {db_name}..custom_models_testcentercitytext tccte on tcota.test_center_id = tccte.test_center_id AND tccte.language_id = 1
                    LEFT JOIN {db_name}..custom_models_testcentercitytext tcctf on tcota.test_center_id = tcctf.test_center_id AND tcctf.language_id = 2
                """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0416_testcenterolatestassessorunavailabilityvw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
