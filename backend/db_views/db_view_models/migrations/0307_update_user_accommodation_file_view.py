from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[user_accommodation_file_vw] AS
                SELECT
                    uaf.id,
                    uaf.comments,
                    uaf.is_uit,
                    uaf.modify_date,
                    uaf.created_date,
                    CASE
                        -- Supervised Test (getting reference number from assessment process data)
                        WHEN uaf.is_uit = 0 THEN apatsv.assessment_process_closing_date
                        -- UIT Test
                        ELSE ui.validity_end_date
                    END as 'process_end_date',
                    uaf.last_modified_by_user_id,
                    uaf.is_alternate_test_request,
                    uafs.id as 'status_id',
                    uafs.codename as 'status_codename',
                    uafst_en.text as 'status_name_en',
                    uafst_fr.text as 'status_name_fr',
                    uaf.test_center_id,
                    ummu.id as 'user_id',
                    ummu.username as 'user_username',
                    ummu.email as 'user_email',
                    ummu.first_name as 'user_first_name',
                    ummu.last_name as 'user_last_name',
                    at_u.id as 'assigned_to_user_id',
                    at_u.email as 'assigned_to_user_email',
                    at_u.first_name as 'assigned_to_user_first_name',
                    at_u.last_name 'assigned_to_user_last_name',
                    -- TEST SKILL TYPE ID
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN ts.test_skill_type_id
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_id
                    END as 'test_skill_type_id',
                    -- TEST SKILL TYPE CODENAME
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN tst.codename
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_codename
                    END as 'test_skill_type_codename',
                    -- TEST SKILL TYPE NAME (EN)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN type_e.text
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_name_en
                    END as 'test_skill_type_name_en',
                    -- TEST SKILL TYPE NAME (FR)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 THEN type_f.text
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_name_fr
                    END as 'test_skill_type_name_fr',
                    -- TEST SKILL SUB-TYPE ID
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sled.id
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occd.id
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_id
                    END as 'test_skill_sub_type_id',
                    -- TEST SKILL SUB-TYPE CODENAME
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sled.codename
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occd.codename
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_codename
                    END as 'test_skill_sub_type_codename',
                    -- TEST SKILL SUB-TYPE NAME (EN)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sle_sub_type_e.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occ_sub_type_e.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_name_en
                    END as 'test_skill_sub_type_name_en',
                    -- TEST SKILL SUB-TYPE NAME (FR)
                    CASE
                        -- UIT Test
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sle_sub_type_f.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occ_sub_type_f.text
                        WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_name_fr
                    END as 'test_skill_sub_type_name_fr'
                FROM {db_name}..custom_models_useraccommodationfile uaf
                JOIN {db_name}..user_management_models_user ummu ON ummu.id = uaf.user_id
                JOIN {db_name}..custom_models_useraccommodationfilestatus uafs ON uafs.id = uaf.status_id
                JOIN {db_name}..custom_models_useraccommodationfilestatustext uafst_en on uafst_en.user_accommodation_file_status_id = uafs.id AND uafst_en.language_id = 1
                JOIN {db_name}..custom_models_useraccommodationfilestatustext uafst_fr on uafst_fr.user_accommodation_file_status_id = uafs.id AND uafst_fr.language_id = 2
                LEFT JOIN {db_name}..user_management_models_user at_u on at_u.id = uaf.assigned_to_user_id
                -- TEST SKILL TYPE / SUB-TYPE DATA (UIT TESTS)
                OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_assignedtest at_1 WHERE at_1.user_accommodation_file_id = uaf.id ORDER BY at_1.id DESC) at
                LEFT JOIN {db_name}..cms_models_testskill ts on ts.test_definition_id = at.test_id
                LEFT JOIN {db_name}..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM {db_name}..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM {db_name}..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN {db_name}..cms_models_testskillsle tssle on tssle.test_skill_id = ts.id
                LEFT JOIN {db_name}..cms_models_testskillsledesc sled on sled.id = tssle.test_skill_sle_desc_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM {db_name}..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM {db_name}..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                LEFT JOIN {db_name}..cms_models_testskilloccupational tsocc on tsocc.test_skill_id = ts.id
                LEFT JOIN {db_name}..cms_models_testskilloccupationaldesc occd on occd.id = tsocc.test_skill_occupational_desc_id
                OUTER APPLY (SELECT TOP 1 occdte.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = occd.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = occd.id AND occdtf.language_id = 2) occ_sub_type_f
                -- TEST SKILL TYPE / SUB-TYPE DATA (SUPERVISED TESTS)
                OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_historicalconsumedreservationcodes hcrc_1 WHERE hcrc_1.user_accommodation_file_id = uaf.id ORDER BY hcrc_1.history_date DESC) hcrc
                LEFT JOIN {db_name}..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = hcrc.assessment_process_assigned_test_specs_id
                LEFT JOIN {db_name}..custom_models_uitinvites ui on ui.id = at.uit_invite_id
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0306_selecteduseraccommodationfilevw_is_alternate_test_request",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
