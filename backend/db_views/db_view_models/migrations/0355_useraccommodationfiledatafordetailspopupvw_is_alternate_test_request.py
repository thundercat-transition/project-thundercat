# Generated by Django 5.0.9 on 2024-10-10 15:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        (
            "db_view_models",
            "0354_update_user_accommodation_file_data_for_details_popup_view",
        ),
    ]

    operations = [
        migrations.AddField(
            model_name="useraccommodationfiledatafordetailspopupvw",
            name="is_alternate_test_request",
            field=models.BooleanField(db_column="is_alternate_test_request", default=0),
        ),
    ]
