from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[completed_supervised_user_accommodation_file_vw] AS
                SELECT
                    uaf.id,
                    uaf.comments,
                    uaf.is_uit,
                    uaf.modify_date,
                    uaf.created_date,
                    uaf.last_modified_by_user_id,
                    uaf.is_alternate_test_request,
                    uafs.id as 'status_id',
                    uafs.codename as 'status_codename',
                    uafst_en.text as 'status_name_en',
                    uafst_fr.text as 'status_name_fr',
                    uaf.test_center_id,
                    ummu.id as 'user_id',
                    ummu.username as 'user_username',
                    ummu.email as 'user_email',
                    ummu.first_name as 'user_first_name',
                    ummu.last_name as 'user_last_name',
                    at_u.id as 'assigned_to_user_id',
                    at_u.email as 'assigned_to_user_email',
                    at_u.first_name as 'assigned_to_user_first_name',
                    at_u.last_name 'assigned_to_user_last_name',
                    crc.id as 'consumed_reservation_code_id',
                    apatsv.id as 'assessment_process_assigned_test_specs_id',
                    apatsv.assessment_process_closing_date as 'closing_date',
                    apatsv.assessment_process_dept_id as 'requesting_dept_id',
                    apatsv.assessment_process_dept_eabrv as 'requesting_dept_abrv_en',
                    apatsv.assessment_process_dept_fabrv as 'requesting_dept_abrv_fr',
                    apatsv.assessment_process_dept_edesc as 'requesting_dept_desc_en',
                    apatsv.assessment_process_dept_fdesc as 'requesting_dept_desc_fr',
                    apatsv.test_skill_type_id,
                    apatsv.test_skill_type_codename,
                    apatsv.test_skill_type_name_en,
                    apatsv.test_skill_type_name_fr,
                    apatsv.test_skill_sub_type_id,
                    apatsv.test_skill_sub_type_codename,
                    apatsv.test_skill_sub_type_name_en,
                    apatsv.test_skill_sub_type_name_fr,
                    td.id as 'test_id',
                    td.parent_code as 'parent_code',
	                td.test_code as 'test_code',
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    td.version as 'version',
                    uaftta.specified_test_description
                FROM {db_name}..custom_models_useraccommodationfile uaf
                JOIN {db_name}..user_management_models_user ummu ON ummu.id = uaf.user_id
                JOIN {db_name}..custom_models_useraccommodationfilestatus uafs ON uafs.id = uaf.status_id
                JOIN {db_name}..custom_models_useraccommodationfilestatustext uafst_en on uafst_en.user_accommodation_file_status_id = uafs.id AND uafst_en.language_id = 1
                JOIN {db_name}..custom_models_useraccommodationfilestatustext uafst_fr on uafst_fr.user_accommodation_file_status_id = uafs.id AND uafst_fr.language_id = 2
                JOIN {db_name}..custom_models_consumedreservationcodes crc on crc.user_accommodation_file_id = uaf.id
                JOIN {db_name}..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = crc.assessment_process_assigned_test_specs_id
                JOIN {db_name}..custom_models_useraccommodationfiletesttoadminister uaftta on uaftta.user_accommodation_file_id = crc.user_accommodation_file_id
                LEFT JOIN {db_name}..cms_models_testdefinition td on td.id = uaftta.test_id
                LEFT JOIN {db_name}..user_management_models_user at_u on at_u.id = uaf.assigned_to_user_id
                LEFT JOIN {db_name}..custom_models_testcentertestsessions tcts on tcts.user_accommodation_file_id = uaf.id
                -- GETTING ONLY UITS THAT ARE COMPLETED WHERE THERE IS NO ASSOCIATED TEST SESSION YET
                WHERE uaf.is_uit = 0 AND uafs.codename = 'completed' AND tcts.user_accommodation_file_id is NULL
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0359_testcentertestsessionsvw_assessment_process_assigned_test_specs_id_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
