from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_center_ola_time_slots_vw] AS
                SELECT
                    ts.id,
                    ts.start_time,
                    ts.end_time,
                    ts.availability,
                    l.language_id as 'assessed_language_id',
                    assessed_language_text_en.text as 'assessed_language_text_en',
                    assessed_language_text_fr.text as 'assessed_language_text_fr',
                    dow.id as 'day_of_week_id',
                    dow.codename as 'day_of_week_codename',
                    day_of_week_text_en.text as 'day_of_week_text_en',
                    day_of_week_text_fr.text as 'day_of_week_text_fr',
                    ts.test_center_id
                FROM {db_name}..custom_models_testcenterolatimeslot ts
                JOIN {db_name}..custom_models_language l on l.language_id = ts.assessed_language_id
                OUTER APPLY (SELECT lt1.text FROM {db_name}..custom_models_languagetext lt1 WHERE lt1.language_ref_id = l.language_id AND lt1.language_id = 1) assessed_language_text_en
                OUTER APPLY (SELECT lt2.text FROM {db_name}..custom_models_languagetext lt2 WHERE lt2.language_ref_id = l.language_id AND lt2.language_id = 2) assessed_language_text_fr
                JOIN {db_name}..custom_models_dayofweek dow on dow.id = ts.day_of_week_id
                OUTER APPLY (SELECT dowt1.text FROM {db_name}..custom_models_dayofweektext dowt1 WHERE dowt1.day_of_week_id = dow.id AND dowt1.language_id = 1) day_of_week_text_en
                OUTER APPLY (SELECT dowt2.text FROM {db_name}..custom_models_dayofweektext dowt2 WHERE dowt2.day_of_week_id = dow.id AND dowt2.language_id = 2) day_of_week_text_fr
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[test_center_ola_time_slots_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0334_testcenterolatestassessorsvw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
