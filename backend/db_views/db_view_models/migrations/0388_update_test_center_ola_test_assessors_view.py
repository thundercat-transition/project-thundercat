from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_center_ola_test_assessors_vw] AS
                SELECT
                    ta.id,
                    ta.test_center_id,
                    u.id as 'user_id',
                    u.first_name as 'user_first_name',
                    u.last_name as 'user_last_name',
                    u.email as 'user_email',
                    ta.supervisor,
                    lang.certified_language_ids
                FROM {db_name}..custom_models_testcenterolatestassessor ta
                JOIN {db_name}..user_management_models_user u on u.id = ta.user_id
                OUTER APPLY (SELECT string_agg(language_certifications.language_id, ',') within group (ORDER BY language_certifications.language_id) as 'certified_language_ids' FROM (SELECT DISTINCT taal.language_id FROM {db_name}..custom_models_testcenterolatestassessorapprovedlanguage taal WHERE taal.test_center_ola_test_assessor_id = ta.id) language_certifications) lang
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0387_update_test_center_test_sessions_vw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
