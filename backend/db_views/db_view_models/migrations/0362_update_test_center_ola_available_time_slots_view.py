from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER FUNCTION get_nbr_of_reserved_test_assessors_by_date_and_time_fn (@date date, @start_time time, @end_time time, @language_id int)
                RETURNS TABLE
                AS RETURN
                SELECT
                    TOP 100 PERCENT
                    COUNT(ts.id) as 'nbr_of_reserved_assessors'
                FROM {db_name}..custom_models_testcentertestsessions ts
                JOIN {db_name}..custom_models_testcentertestsessiondata tsd on tsd.id = ts.test_session_data_id
				JOIN {db_name}..cms_models_testskillsledesc sle_desc on sle_desc.id = tsd.test_skill_sub_type_id
                WHERE tsd.date = @date AND tsd.test_skill_sub_type_id = IIF(@language_id = 1, (SELECT id FROM {db_name}..cms_models_testskillsledesc WHERE codename = 'oe'), (SELECT id FROM {db_name}..cms_models_testskillsledesc WHERE codename = 'of'))
                AND CAST(tsd.start_time as time) = @start_time 
                AND CAST(tsd.end_time as time) = @end_time
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_center_ola_available_time_slots_vw] AS
                SELECT
                    TOP 100 PERCENT
                    -- generating ID by converting the datetime in timestamp
                    DATEDIFF(SECOND, '1970-01-01', (CONCAT(CONCAT(fn.date, ' '), fn.start_time))) as 'id',
                    fn.date,
                    fn.start_time, 
                    fn.end_time,
                    fn.day_of_week_id,
                    fn.assessed_language_id,
                    SUM(available_test_assessors) as 'total_available_test_assessors',
                    nbr_of_reserved_assessors.count as 'nbr_of_reserved_assessors',
                    SUM(available_test_assessors) - nbr_of_reserved_assessors.count as 'remaining_available_test_assessors'
                FROM {db_name}..get_ola_available_time_slots_fn() fn
                OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM {db_name}..get_nbr_of_reserved_test_assessors_by_date_and_time_fn(date, start_time, end_time, fn.assessed_language_id)) nbr_of_reserved_assessors
                OUTER APPLY (SELECT MAX(tcoc.booking_delay) as 'booking_delay' FROM {db_name}..custom_models_testcenterolaconfigs tcoc WHERE tcoc.test_center_id in (SELECT tcots.test_center_id FROM {db_name}..custom_models_testcenterolatimeslot tcots WHERE tcots.start_time = fn.start_time AND tcots.end_time = fn.end_time AND tcots.day_of_week_id = fn.day_of_week_id)) greatest_booking_delay_found
                -- still have available spots
                WHERE fn.available_test_assessors > 0 
                /* KNOWN ISSUE:
                    - depending on the timezone, the date might be wrong ==> 2024-09-24 - 02:00-03:00 UTC | 2024-09-24 - 22:00-00:00 -4 !== 2024-09-24 - 01:00-02:00 -1 (should have been the 25th - 01:00-02:00)
                    - if booking delay is less than 24 hours and the candidate tries to get late sessions on the same day (01:00-02:00 UTC for example), the logic might filter it out because the date is not converted in UTC, just the start time and end time
                */
                -- start_time_as_datetime (ex: CAST('2024-09-18 19:00:00' as datetime)) >= current UTC datetime + greatest_booking_delay_found
                AND CAST(CONCAT(CONCAT(fn.date, ' '), LEFT (fn.start_time, 8)) as datetime) >= DATEADD(HOUR, greatest_booking_delay_found.booking_delay, GETUTCDATE())
                GROUP BY date, start_time, end_time, day_of_week_id, assessed_language_id, nbr_of_reserved_assessors.count, greatest_booking_delay_found.booking_delay
                ORDER BY fn.date, fn.start_time ASC

    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0361_completedsuperviseduseraccommodationfilevw_test_skill_sub_type_codename_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
