from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[camm_integration_vw] AS
    #             SELECT
    #                 t1.id,
    #                 td.test_code,
    #                 td.parent_code,
    #                 -- language, based on test skill/sub-skill type
    #                 CASE
    #                     WHEN tstsle.test_skill_sle_desc_id = 1 THEN 1
    #                     WHEN tstsle.test_skill_sle_desc_id = 2 THEN 2
    #                     WHEN tstsle.test_skill_sle_desc_id = 3 THEN 1
    #                     WHEN tstsle.test_skill_sle_desc_id = 4 THEN 2
    #                     WHEN tstsle.test_skill_sle_desc_id = 5 THEN 1
    #                     WHEN tstsle.test_skill_sle_desc_id = 6 THEN 2
    #                 END [language_id],
    #                 -- SLE Test Type Code, based on test skill/sub-skill type
    #                 CASE
    #                     WHEN tstsle.test_skill_sle_desc_id = 1 THEN 1
    #                     WHEN tstsle.test_skill_sle_desc_id = 2 THEN 1
    #                     WHEN tstsle.test_skill_sle_desc_id = 3 THEN 2
    #                     WHEN tstsle.test_skill_sle_desc_id = 4 THEN 2
    #                     WHEN tstsle.test_skill_sle_desc_id = 5 THEN 4
    #                     WHEN tstsle.test_skill_sle_desc_id = 6 THEN 4
    #                 END [sle_test_type_code],
    #                 t1.submit_date,
    #                 t1.modify_date,
    #                 t1.status,
    #                 t1.total_score,
    #                 t1.en_converted_score,
    #                 IIF(t1.orderless_financial_data_id is null , apats.level_required, ofd.level_required) [required_converted_score],
    #                 IIF(t1.orderless_financial_data_id is null, apats.reason_for_testing_id, ofd.reason_for_testing_id) [reason_for_testing_id],
    #                 IIF(t1.orderless_financial_data_id is null, ap.department_id, ofd.department_ministry_id) [requesting_department_id],
    #                 -- validity period non-existant so set to 60
    #                 '60' [validity_period],
    #                 td.retest_period [retest_period],
    #                 CASE
    #                     WHEN t1.is_invalid = 1 THEN 0
    #                     WHEN t1.is_invalid = 0 THEN 1
    #                 END [is_valid],
    #                 cdd.id [cat_user_id],
    #                 cdd.first_name [first_name],
    #                 cdd.last_name [last_name],
    #                 cdd.username [username],
    #                 cdd.email [email_address],
    #                 cdd.pri [pri],
    #                 cdd.military_nbr [military_number],
    #                 cdd.birth_date [date_of_birth]
    #             FROM {db_name}..custom_models_assignedtest t1
    #             JOIN {db_name}..cms_models_testdefinition td on td.id  = t1.test_id
    #             JOIN {db_name}..user_management_models_user cdd on cdd.username = t1.username_id
    #             JOIN {db_name}..cms_models_testskill ts on ts.test_definition_id = td.id  AND ts.test_skill_type_id = 2
    #             LEFT JOIN {db_name}..cms_models_testskillsle tstsle on tstsle.test_skill_id = ts.id
    #             LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = t1.orderless_financial_data_id
    #             LEFT JOIN {db_name}..custom_models_consumedreservationcodes crc on crc.assigned_test_id = t1.id
    #             LEFT JOIN {db_name}..custom_models_assessmentprocessassignedtestspecs apats on apats.id = crc.assessment_process_assigned_test_specs_id AND crc.assigned_test_id = t1.id
    #             LEFT JOIN {db_name}..custom_models_assessmentprocess ap on ap.id = apats.assessment_process_id
    #         """.format(
    #             db_name=settings.DATABASES["default"]["NAME"]
    #         )
    #     )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0121_update_assessment_process_results_vw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
