from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("db_view_models", "0104_test_center_test_sessions_vw_center_and_room_details"),
    ]

    operations = [
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="session_spaces_available",
            field=models.IntegerField(db_column="SESSION_SPACES_AVAILABLE"),
        ),
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="test_center_address",
            field=models.CharField(db_column="TEST_CENTER_ADDRESS", max_length=255),
        ),
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="test_center_city",
            field=models.CharField(db_column="TEST_CENTER_CITY", max_length=150),
        ),
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="test_center_postal_code",
            field=models.CharField(db_column="TEST_CENTER_POSTAL_CODE", max_length=7),
        ),
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="test_center_province",
            field=models.CharField(db_column="TEST_CENTER_PROVINCE", max_length=150),
        ),
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="test_center_country",
            field=models.CharField(db_column="TEST_CENTER_COUNTRY", max_length=150),
        ),
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="department_id",
            field=models.IntegerField(db_column="DEPARTMENT_ID"),
        ),
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="test_center_test_session_data_id",
            field=models.IntegerField(db_column="TEST_CENTER_TEST_SESSION_DATA_ID"),
        ),
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="test_center_en_other_details",
            field=models.TextField(db_column="TEST_CENTER_EN_OTHER_DETAILS"),
        ),
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="test_center_fr_other_details",
            field=models.TextField(db_column="TEST_CENTER_FR_OTHER_DETAILS"),
        ),
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="test_room_en_other_details",
            field=models.TextField(db_column="TEST_ROOM_EN_OTHER_DETAILS"),
        ),
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="test_room_fr_other_details",
            field=models.TextField(db_column="TEST_ROOM_FR_OTHER_DETAILS"),
        ),
    ]
