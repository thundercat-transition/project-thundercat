from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[test_center_test_sessions_vw] AS
    #             SELECT
    #                 ts.id,
    #                 ts.test_center_id,
    #                 tc.name as 'test_center_name',
    #                 tc.address as 'test_center_address',
    #                 tc.city as 'test_center_city',
    #                 tc.province as 'test_center_province',
    #                 tc.postal_code as 'test_center_postal_code',
    #                 tc.country as 'test_center_country',
    #                 tsd.test_center_room_id,
    #                 rv.name as 'room_name',
    #                 --crc.count as 'spaces_booked',
    #                 --(tsd.spaces_available-crc.count) as 'spaces_unbooked',
    #                 tsd.spaces_available as 'session_spaces_available',
    #                 rv.max_occupancy as 'room_max_occupancy',
    #                 tsd.id as 'test_center_test_session_data_id',
    #                 tsd.open_to_ogd,
    #                 tc.department_id,
    #                 DATEADD(hour, -tc.booking_delay, tsd.start_time) as 'latest_booking_modification_time',
    #                 tc.booking_delay as 'booking_delay',


# 				tc.other_details as 'test_center_en_other_details',
# 				tc.other_details as 'test_center_fr_other_details',
# 				rv.other_details as 'test_room_en_other_details',
# 				rv.other_details as 'test_room_fr_other_details',
#                 tsd.date,
#                 tsd.start_time,
#                 tsd.end_time,
#                 tsd.spaces_available,
#                 tsd.test_skill_type_id,
#                 tst.codename as 'test_skill_type_codename',
#                 stte.text as 'test_skill_type_en_name',
#                 sttf.text as 'test_skill_type_fr_name',
#                 tsd.test_skill_sub_type_id,
#                 CASE
#                     -- SLE Sub Type
#                     WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'sle') THEN (SELECT text FROM {db_name}..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = tsd.test_skill_sub_type_id AND language_id = 1)
#                     ELSE
#                         -- OCCUPATIONAL Sub Type
#                         CASE
#                             WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM {db_name}..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = tsd.test_skill_sub_type_id AND language_id = 1)
#                         ELSE
#                             NULL
#                         END
#                 END AS 'test_skill_sub_type_en_name',
#                 CASE
#                     -- SLE Sub Type
#                     WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'SLE') THEN (SELECT text FROM {db_name}..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = tsd.test_skill_sub_type_id AND language_id = 2)
#                     ELSE
#                         -- OCCUPATIONAL Sub Type
#                         CASE
#                             WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM {db_name}..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = tsd.test_skill_sub_type_id AND language_id = 2)
#                         ELSE
#                             NULL
#                         END
#                 END AS 'test_skill_sub_type_fr_name'
#             FROM {db_name}..custom_models_testcentertestsessions ts
#             JOIN {db_name}..custom_models_testcenter tc on tc.id = ts.test_center_id
#             JOIN {db_name}..custom_models_testcentertestsessiondata tsd on tsd.id = ts.test_session_data_id
#             JOIN {db_name}..test_center_rooms_vw rv on rv.id = tsd.test_center_room_id
#             JOIN {db_name}..cms_models_testskilltype tst on tst.id = tsd.test_skill_type_id
#             JOIN {db_name}..cms_models_testskilltypetext stte on stte.test_skill_type_id = tsd.test_skill_type_id AND stte.language_id = 1
#             JOIN {db_name}..cms_models_testskilltypetext sttf on sttf.test_skill_type_id = tsd.test_skill_type_id AND sttf.language_id = 2
#             --OUTER APPLY (SELECT COUNT(*) as count FROM {db_name}..custom_models_consumedreservationcodes WHERE test_center_test_session_data_id=tsd.id) crc
#             """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0103_update_testcentertestsessionsvw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
