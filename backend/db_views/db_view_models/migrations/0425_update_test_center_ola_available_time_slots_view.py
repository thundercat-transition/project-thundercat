from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER FUNCTION get_test_assessor_availabilities_by_date_fn (@date date, @test_center_id int)
                RETURNS TABLE
                AS RETURN
                SELECT
					TOP 100 PERCENT
					tcts.start_time,
					tcts.end_time,
					tcts.day_of_week_id,
					tcts.assessed_language_id,
                    -- AVAILABLE ASSESSORS - UNAVAILABLE ASSESSORS - UNAVAILABLE ASSESSORS DUE TO ACCOMMODATIONS
					SUM(available_assessors.count) - SUM(unavailable_assessors.count) - SUM(unavailable_assessors_due_to_accommodations.count) as 'nbr_of_available_assessors'
				FROM {db_name}..custom_models_testcenterolatimeslot tcts
				JOIN {db_name}..custom_models_testcenter tc on tc.id = tcts.test_center_id
				-- GETTING NUMBER OF AVAILABLE ASSESSORS
				OUTER APPLY (SELECT COUNT(*) as 'count'
							 FROM {db_name}..custom_models_testcenterolatimeslotassessoravailability tcoaa
							 WHERE tcoaa.test_center_ola_time_slot_id = tcts.id
							) available_assessors
				-- GETTING NUMBER OF UNAVAILABLE ASSESSORS
				OUTER APPLY (SELECT COUNT(* ) as 'count'
							 FROM {db_name}..custom_models_testcenterolaassessorunavailability tcoau
							 WHERE tcoau.test_center_ola_test_assessor_id IN (SELECT tcoaa_2.test_center_ola_test_assessor_id
																			  FROM {db_name}..custom_models_testcenterolatimeslotassessoravailability tcoaa_2
																			  WHERE tcoaa_2.test_center_ola_time_slot_id = tcts.id
																			 )
							 -- Unavailability start_date (DateTime) < Formatted DateTime based on Test Session End Time
							 AND tcoau.start_date < CAST(CONCAT(@date, ' ', CAST(tcts.end_time AS TIME), ' +00:00') AS DATETIMEOFFSET)
							 -- Unavailability end_date (DateTime) > Formatted DateTime based on Test Session Start Time
							 AND tcoau.end_date > CAST(CONCAT(@date, ' ', CAST(tcts.start_time AS TIME), ' +00:00') AS DATETIMEOFFSET)
							 ) unavailable_assessors
				-- GETTING NUMBER OF UNAVAILABLE ASSESSORS DUE TO ACCOMMODATIONS
				OUTER APPLY (SELECT COUNT(*) as 'count'
							 FROM {db_name}..custom_models_testcentertestsessions ts
							 JOIN {db_name}..custom_models_testcentertestsessiondata tsd
							 ON tsd.id = ts.test_session_data_id
							 WHERE ts.is_standard = 0
							 -- tsd.test_assessor_user_id IS A USER_ID NOT A test_center_ola_test_assessor_id
							 AND tsd.test_assessor_user_id IN (SELECT tcota.user_id
															   FROM {db_name}..custom_models_testcenterolatestassessor tcota
															   WHERE id IN (SELECT tcoaa_3.test_center_ola_test_assessor_id
																			FROM {db_name}..custom_models_testcenterolatimeslotassessoravailability tcoaa_3
																			WHERE tcoaa_3.test_center_ola_time_slot_id = tcts.id
																		   )
															  )
							 AND (CAST(tsd.start_time as TIME) < tcts.end_time AND CAST(tsd.end_time as TIME) > tcts.start_time)
							) unavailable_assessors_due_to_accommodations
				-- ONLY CONSIDERING RESPECTIVE AUTHORIZED OLA TEST CENTERS WHERE DAY OF WEEK MATCHES THE PROVIDED DATE
				WHERE tc.ola_authorized = 1 AND tcts.test_center_id = @test_center_id AND tcts.day_of_week_id = DATEPART(dw, @date)
				GROUP BY tcts.start_time, tcts.end_time, tcts.day_of_week_id, tcts.assessed_language_id
				ORDER BY tcts.day_of_week_id
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """CREATE OR ALTER FUNCTION get_ola_prioritization_value_by_codename_fn (@codename char(50))
                RETURNS TABLE
                AS RETURN
                SELECT
                    CONVERT(DECIMAL(10, 2),op.value) as 'value'
                FROM {db_name}..custom_models_olaprioritization op
                WHERE op.codename = @codename
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_center_ola_available_time_slots_vw] AS
                SELECT
                    TOP 100 PERCENT
                    -- generating ID by converting the datetime in timestamp
                    DATEDIFF(SECOND, '1970-01-01', (CONCAT(CONCAT(fn.date, ' '), fn.start_time))) as 'id',
                    fn.date,
                    fn.start_time,
                    fn.end_time,
                    fn.day_of_week_id,
                    fn.assessed_language_id,
					nbr_of_reserved_assessors_high_priority.count as 'nbr_of_reserved_assessors_high_priority',
					SUM(nbr_of_reserved_assessors_per_day_high_priority.count) as 'nbr_of_reserved_assessors_per_date_high_priority',
					-- available_test_assessors_total_per_date (high) - nbr_of_reserved_assessors_per_day_high_priority
					CONVERT(INT, (available_test_assessors_total_per_date.count * (SELECT * FROM {db_name}..get_ola_prioritization_value_by_codename_fn('high')) / 100) - SUM(nbr_of_reserved_assessors_per_day_high_priority.count)) as 'remaining_available_test_assessors_per_date_high',
					nbr_of_reserved_assessors_medium_priority.count 'nbr_of_reserved_assessors_medium_priority',
					SUM(nbr_of_reserved_assessors_per_day_medium_priority.count) as 'nbr_of_reserved_assessors_per_date_medium_priority',
					-- available_test_assessors_total_per_date (medium) - nbr_of_reserved_assessors_per_day_medium_priority
					CONVERT(INT, (available_test_assessors_total_per_date.count * (SELECT * FROM {db_name}..get_ola_prioritization_value_by_codename_fn('medium')) / 100) - SUM(nbr_of_reserved_assessors_per_day_medium_priority.count)) as 'remaining_available_test_assessors_per_date_medium',
					nbr_of_reserved_assessors_low_priority.count as 'nbr_of_reserved_assessors_low_priority',
					SUM(nbr_of_reserved_assessors_per_day_low_priority.count) as 'nbr_of_reserved_assessors_per_date_low_priority',
					-- available_test_assessors_total_per_date (low) - nbr_of_reserved_assessors_per_day_low_priority
					CONVERT(INT, (available_test_assessors_total_per_date.count * (SELECT * FROM {db_name}..get_ola_prioritization_value_by_codename_fn('low')) / 100) - SUM(nbr_of_reserved_assessors_per_day_low_priority.count)) as 'remaining_available_test_assessors_per_date_low',
					-- number of assessors selected in the app for respective time slot
					SUM(fn.available_test_assessors_total) as 'available_test_assessors_total',
					-- available_test_assessors_total_per_date (high + medium + low)
					CONVERT(INT, ((available_test_assessors_total_per_date.count * (SELECT * FROM {db_name}..get_ola_prioritization_value_by_codename_fn('high')) / 100) + (available_test_assessors_total_per_date.count * (SELECT * FROM {db_name}..get_ola_prioritization_value_by_codename_fn('medium')) / 100) + (available_test_assessors_total_per_date.count * (SELECT * FROM {db_name}..get_ola_prioritization_value_by_codename_fn('low')) / 100))) as 'reservable_available_test_assessors_per_date',
					-- available_test_assessors_total - nbr_of_reserved_assessors_per_day_high_priority - nbr_of_reserved_assessors_per_day_medium_priority - nbr_of_reserved_assessors_per_day_low_priority
					CONVERT(INT, (SUM(fn.available_test_assessors_total) * (SELECT * FROM {db_name}..get_ola_prioritization_value_by_codename_fn('high')) / 100) + (SUM(fn.available_test_assessors_total) * (SELECT * FROM {db_name}..get_ola_prioritization_value_by_codename_fn('medium')) / 100) + (SUM(fn.available_test_assessors_total) * (SELECT * FROM {db_name}..get_ola_prioritization_value_by_codename_fn('low')) / 100) - nbr_of_reserved_assessors_high_priority.count - nbr_of_reserved_assessors_medium_priority.count - nbr_of_reserved_assessors_low_priority.count) as 'remaining_available_test_assessors_total'
				FROM {db_name}..get_ola_available_time_slots_fn() fn
                OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM {db_name}..get_nbr_of_reserved_test_assessors_by_date_and_time_fn(date, start_time, end_time, fn.assessed_language_id, 'high')) nbr_of_reserved_assessors_high_priority
				OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM {db_name}..get_nbr_of_reserved_test_assessors_by_date_fn(date, fn.assessed_language_id, 'high')) nbr_of_reserved_assessors_per_day_high_priority
				OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM {db_name}..get_nbr_of_reserved_test_assessors_by_date_and_time_fn(date, start_time, end_time, fn.assessed_language_id, 'medium')) nbr_of_reserved_assessors_medium_priority
				OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM {db_name}..get_nbr_of_reserved_test_assessors_by_date_fn(date, fn.assessed_language_id, 'medium')) nbr_of_reserved_assessors_per_day_medium_priority
				OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM {db_name}..get_nbr_of_reserved_test_assessors_by_date_and_time_fn(date, start_time, end_time, fn.assessed_language_id, 'low')) nbr_of_reserved_assessors_low_priority
                OUTER APPLY (SELECT nbr_of_reserved_assessors as 'count' FROM {db_name}..get_nbr_of_reserved_test_assessors_by_date_fn(date, fn.assessed_language_id, 'low')) nbr_of_reserved_assessors_per_day_low_priority
				OUTER APPLY (SELECT MAX(tcoc.booking_delay) as 'booking_delay' FROM {db_name}..custom_models_testcenterolaconfigs tcoc WHERE tcoc.test_center_id in (SELECT tcots.test_center_id FROM {db_name}..custom_models_testcenterolatimeslot tcots WHERE tcots.start_time = fn.start_time AND tcots.end_time = fn.end_time AND tcots.day_of_week_id = fn.day_of_week_id)) greatest_booking_delay_found
				OUTER APPLY (SELECT fn2.date, fn2.assessed_language_id, SUM(fn2.available_test_assessors_total) as 'count' FROM {db_name}..get_ola_available_time_slots_fn() fn2 WHERE fn2.date = fn.date AND fn2.assessed_language_id = fn.assessed_language_id GROUP BY date, assessed_language_id) available_test_assessors_total_per_date
				-- slots that have/had at least one available test assessor (one available spot)
                WHERE fn.available_test_assessors_total > 0
                /* KNOWN ISSUE:
                    - depending on the timezone, the date might be wrong ==> 2024-09-24 - 02:00-03:00 UTC | 2024-09-24 - 22:00-00:00 -4 !== 2024-09-24 - 01:00-02:00 -1 (should have been the 25th - 01:00-02:00)
                    - if booking delay is less than 24 hours and the candidate tries to get late sessions on the same day (01:00-02:00 UTC for example), the logic might filter it out because the date is not converted in UTC, just the start time and end time
                */
                -- start_time_as_datetime (ex: CAST('2024-09-18 19:00:00' as datetime)) >= current UTC datetime + greatest_booking_delay_found
                AND CAST(CONCAT(CONCAT(fn.date, ' '), LEFT (fn.start_time, 8)) as datetime) >= DATEADD(HOUR, greatest_booking_delay_found.booking_delay, GETUTCDATE())
				GROUP BY fn.date, fn.start_time, fn.end_time, fn.day_of_week_id, fn.assessed_language_id, nbr_of_reserved_assessors_high_priority.count, nbr_of_reserved_assessors_medium_priority.count, nbr_of_reserved_assessors_low_priority.count, greatest_booking_delay_found.booking_delay, available_test_assessors_total_per_date.count
                ORDER BY fn.date, fn.start_time ASC
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0424_update_virtual_test_session_details_vw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
