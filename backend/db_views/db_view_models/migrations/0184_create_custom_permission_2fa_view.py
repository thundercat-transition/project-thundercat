from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[custom_permission_2fa_vw] AS
                SELECT
                    cp2fa.id as 'custom_permission2fa_id',
                    cp2fa.is_2fa_active as 'is_2fa_active',
                    cp.permission_id as 'permission_id',
                    cp.en_name as 'en_name',
                    cp.fr_name as 'fr_name',
                    cp.en_description as 'en_description',
                    cp.fr_description as 'fr_description'
                FROM {db_name}..[user_management_models_custompermission2fa] cp2fa
                JOIN {db_name}..[user_management_models_custompermissions] cp on cp.permission_id=cp2fa.custom_permission_id""".format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0183_update_financial_report_vw",
        ),
        (
            "user_management_models",
            "0071_user2fatracking_historicaluser2fatracking_and_more",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
