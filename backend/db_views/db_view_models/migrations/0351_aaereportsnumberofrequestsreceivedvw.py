# Generated by Django 5.0.9 on 2024-10-04 11:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('db_view_models', '0350_create_aae_reports_number_of_requests_received_view'),
    ]

    operations = [
        migrations.CreateModel(
            name='AaeReportsNumberOfRequestsReceivedVw',
            fields=[
                ('id', models.IntegerField(db_column='id', primary_key=True, serialize=False)),
                ('created_date', models.DateField(db_column='created_date')),
                ('number_of_requests', models.IntegerField(db_column='number_of_requests')),
            ],
            options={
                'db_table': 'aae_reports_number_of_requests_received_vw',
                'managed': False,
            },
        ),
    ]
