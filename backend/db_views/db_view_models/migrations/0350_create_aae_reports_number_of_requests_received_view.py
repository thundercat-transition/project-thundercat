from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[aae_reports_number_of_requests_received_vw] AS
                SELECT
                    -- generating ID by converting the created_date in timestamp
                    DATEDIFF(SECOND, '1970-01-01', uaf.created_date) as 'id',
                    uaf.created_date,
                    COUNT(uaf.id) as 'number_of_requests'
                FROM {db_name}..custom_models_useraccommodationfile uaf
                GROUP BY uaf.created_date
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            "DROP VIEW IF EXISTS [dbo].[aae_reports_number_of_requests_received_vw]"
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0349_testcentertestsessionsvw_language_id",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
