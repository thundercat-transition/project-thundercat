# Generated by Django 4.2.10 on 2024-03-18 16:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("db_view_models", "0188_create_scored_tests_view"),
    ]

    operations = [
        migrations.AddField(
            model_name="scoredtestsvw",
            name="calculated_retest_period_date",
            field=models.DateField(
                db_column="calculated_retest_period_date", default="temp"
            ),
        ),
        migrations.AddField(
            model_name="scoredtestsvw",
            name="calculated_score_valid_until",
            field=models.DateField(
                db_column="calculated_score_valid_until", default="temp"
            ),
        ),
        migrations.AddField(
            model_name="scoredtestsvw",
            name="is_most_recent_valid_test",
            field=models.BooleanField(db_column="is_most_recent_valid_test", default=0),
        ),
        migrations.AddField(
            model_name="scoredtestsvw",
            name="most_updated_retest_period",
            field=models.IntegerField(
                db_column="most_updated_retest_period", default=1
            ),
        ),
        migrations.AddField(
            model_name="scoredtestsvw",
            name="result_valid_indefinitely",
            field=models.BooleanField(db_column="result_valid_indefinitely", default=0),
        ),
    ]
