# Generated by Django 4.2.10 on 2024-03-05 20:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("db_view_models", "0169_update_assigned_tests_vw"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="assignedtestsvw",
            name="ta_id",
        ),
        migrations.RemoveField(
            model_name="assignedtestsvw",
            name="username_id",
        ),
        migrations.AddField(
            model_name="assignedtestsvw",
            name="ta_user_id",
            field=models.IntegerField(db_column="TA_USER_ID", default=1),
        ),
        migrations.AddField(
            model_name="assignedtestsvw",
            name="ta_username",
            field=models.CharField(
                db_column="TA_USERNAME", default="temp", max_length=254
            ),
        ),
        migrations.AddField(
            model_name="assignedtestsvw",
            name="user_id",
            field=models.IntegerField(db_column="USER_ID", default=1),
        ),
        migrations.AddField(
            model_name="assignedtestsvw",
            name="username",
            field=models.CharField(
                db_column="USERNAME", default="temp", max_length=254
            ),
        ),
    ]
