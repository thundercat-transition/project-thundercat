# Generated by Django 4.2.7 on 2024-02-29 16:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("db_view_models", "0161_update_item_content_drafts_view"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="itemcontentdraftsvw",
            name="username_id",
        ),
        migrations.AddField(
            model_name="itemcontentdraftsvw",
            name="user_id",
            field=models.IntegerField(db_column="USER_ID", default=1),
        ),
        migrations.AddField(
            model_name="itemcontentdraftsvw",
            name="username",
            field=models.CharField(
                db_column="USERNAME", default="temp", max_length=254
            ),
        ),
    ]
