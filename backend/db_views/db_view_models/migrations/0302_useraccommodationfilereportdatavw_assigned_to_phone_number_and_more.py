# Generated by Django 4.2.11 on 2024-07-23 18:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("db_view_models", "0301_update_user_accommodation_file_report_data_view"),
    ]

    operations = [
        migrations.AddField(
            model_name="useraccommodationfilereportdatavw",
            name="assigned_to_phone_number",
            field=models.CharField(
                db_column="assigned_to_phone_number", default=None, max_length=10
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfilereportdatavw",
            name="hr_phone_number",
            field=models.CharField(
                db_column="hr_phone_number", default=None, max_length=10
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfilereportdatavw",
            name="requesting_dept_abrv_en",
            field=models.CharField(
                db_column="requesting_dept_abrv_en", default="temps", max_length=10
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfilereportdatavw",
            name="requesting_dept_abrv_fr",
            field=models.CharField(
                db_column="requesting_dept_abrv_fr", default="temps", max_length=10
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfilereportdatavw",
            name="requesting_dept_desc_en",
            field=models.CharField(
                db_column="requesting_dept_desc_en", default="temps", max_length=200
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfilereportdatavw",
            name="requesting_dept_desc_fr",
            field=models.CharField(
                db_column="requesting_dept_desc_fr", default="temps", max_length=200
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfilereportdatavw",
            name="requesting_dept_id",
            field=models.IntegerField(db_column="requesting_dept_id", default=1),
        ),
        migrations.AddField(
            model_name="useraccommodationfilereportdatavw",
            name="ta_email",
            field=models.EmailField(
                db_column="ta_email", default="temps", max_length=254
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfilereportdatavw",
            name="ta_first_name",
            field=models.CharField(
                db_column="ta_first_name", default="temps", max_length=30
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfilereportdatavw",
            name="ta_last_name",
            field=models.CharField(
                db_column="ta_last_name", default="temps", max_length=150
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfilereportdatavw",
            name="ta_phone_number",
            field=models.CharField(
                db_column="ta_phone_number", default=None, max_length=10
            ),
        ),
        migrations.AddField(
            model_name="useraccommodationfilereportdatavw",
            name="ta_user_id",
            field=models.IntegerField(db_column="ta_user_id", default=1),
        ),
        migrations.AddField(
            model_name="useraccommodationfilereportdatavw",
            name="test_center_name",
            field=models.CharField(
                db_column="test_center_name", default="temps", max_length=150
            ),
        ),
    ]
