from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_centers_vw] AS
                SELECT
                    tc.id,
                    tc.name,
                    tc.postal_code,
                    tc.security_email,
                    tc.booking_delay,
                    tc.accommodations_friendly,
                    tc.modify_date,
                    tc.ola_authorized,
                    tcta.number_of_tas,
                    tcr.number_of_rooms,
                    c.CNTRY_ID as 'country_id',
                    c.EABRV as 'country_eabrv',
                    c.FABRV as 'country_fabrv',
                    c.EDESC as 'country_edesc',
                    c.FDESC as 'country_fdesc',
                    d.DEPT_ID as 'dept_id',
                    d.EABRV as 'dept_eabrv',
                    d.FABRV as 'dept_fabrv',
                    d.EDESC as 'dept_edesc',
                    d.FDESC as 'dept_fdesc'
                FROM {db_name}..custom_models_testcenter tc
                OUTER APPLY (SELECT COUNT(id) as number_of_tas FROM {db_name}..custom_models_testcentertestadministrators WHERE test_center_id = tc.id) tcta
                OUTER APPLY (SELECT COUNT(id) as number_of_rooms FROM {db_name}..custom_models_testcenterrooms WHERE test_center_id = tc.id) tcr
                LEFT JOIN CAT_REF_DEPARTMENTS_VW d on d.DEPT_ID = tc.department_id
                LEFT JOIN CAT_REF_COUNTRY_VW c on c.CNTRY_ID = tc.country_id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0323_testcentersvw_ola_authorized",
        ),
        (
            "custom_models",
            "0140_historicaltestcenter_accommodations_friendly_and_more",
        ),
        (
            "custom_models",
            "0149_historicaltestcenter_ola_authorized_and_more",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
