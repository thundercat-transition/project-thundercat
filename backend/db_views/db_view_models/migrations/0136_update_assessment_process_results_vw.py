from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[assessment_process_results_vw] AS
                SELECT
                    id,
                    user_id,
                    reference_number,
                    closing_date,
                    (SELECT COUNT(DISTINCT email) FROM {db_name}..custom_models_assessmentprocessassignedtestspecs WHERE assessment_process_id = ap.id) AS "number_of_candidates",
                    (SELECT COUNT(*) 
                        FROM {db_name}..custom_models_assignedtest at 
                        JOIN {db_name}..custom_models_consumedreservationcodes crc on crc.assigned_test_id = at.id 
                        JOIN {db_name}..custom_models_assessmentprocessassignedtestspecs ats on ats.id = crc.assessment_process_assigned_test_specs_id
                        JOIN {db_name}..custom_models_assessmentprocess ap2 on ap2.id = ats.assessment_process_id
                        WHERE at.status_id in (SELECT id FROM custom_models_assignedteststatus WHERE codename in ('quit', 'submitted')) AND ap2.id = ap.id ) AS "number_of_tests_taken"
                FROM {db_name}..custom_models_assessmentprocess ap
                WHERE request_sent = 'TRUE'
            """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0135_remove_activetestsvw_test_status_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
