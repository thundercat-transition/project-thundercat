from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[assessment_process_default_test_specs_vw] AS
                SELECT
                    ts.id,
                    ts.assessment_process_id,
                    tst.id as 'test_skill_type_id',
                    tst.codename as 'test_skill_type_codename',
                    type_e.text as 'test_skill_type_name_en',
                    type_f.text as 'test_skill_type_name_fr',
                    ts.test_skill_sub_type_id as 'test_skill_sub_type_id',
                    sled.codename as 'test_skill_sub_type_codename',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_e.text
                        WHEN tst.id = 3 THEN occ_sub_type_e.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_en',
                    CASE
                        WHEN tst.id = 2 THEN sle_sub_type_f.text
                        WHEN tst.id = 3 THEN occ_sub_type_f.text
                        ELSE
                            NULL
                    END AS 'test_skill_sub_type_name_fr',
                    rft.id as 'reason_for_testing_id',
                    reason_for_testing_e.text as 'reason_for_testing_name_en',
                    reason_for_testing_f.text as 'reason_for_testing_name_fr',
                    IIF(ts.level_required != '', ts.level_required, NULL) as 'level_required'
                FROM {db_name}..custom_models_assessmentprocessdefaulttestspecs ts
                JOIN {db_name}..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM {db_name}..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM {db_name}..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN {db_name}..cms_models_testskillsledesc sled on sled.id = ts.test_skill_sub_type_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM {db_name}..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM {db_name}..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                LEFT JOIN {db_name}..cms_models_testskilloccupationaldesc occd on occd.id = ts.test_skill_sub_type_id
                OUTER APPLY (SELECT TOP 1 occdte.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = sled.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = sled.id AND occdtf.language_id = 2) occ_sub_type_f
                LEFT JOIN {db_name}..custom_models_reasonfortesting rft on rft.id = ts.reason_for_testing_id
                OUTER APPLY (SELECT TOP 1 text FROM {db_name}..custom_models_reasonfortestingtext rftt WHERE rftt.reason_for_testing_id = rft.id AND language_id = 1) reason_for_testing_e
                OUTER APPLY (SELECT TOP 1 text FROM {db_name}..custom_models_reasonfortestingtext rftt WHERE rftt.reason_for_testing_id = rft.id AND language_id = 2) reason_for_testing_f
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0325_update_assessment_process_assigned_test_specs_vw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
