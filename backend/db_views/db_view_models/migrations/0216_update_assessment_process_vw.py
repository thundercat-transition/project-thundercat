from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[assessment_process_vw] AS
                SELECT
                    ap.id,
                    u.id as 'user_id',
                    u.email as 'user_email',
                    ap.reference_number,
                    ap.duration,
                    CASE
                        -- request has been sent (closing date should be defined)
                        WHEN ap.request_sent = 1 THEN ap.closing_date
                        -- request has not been sent yet (adding "duration" amount of days to today's date)
                        ELSE CONVERT(VARCHAR(10), DATEADD(DAY, ap.duration, GETDATE()), 23)
                    END as 'closing_date',
                    CASE
                        WHEN ap.request_sent = 1 THEN sent_assessment_process_data.calculated_initial_date
                        ELSE NULL
                    END as 'sent_date',
                    ap.allow_booking_external_tc,
                    ap.contact_email_for_candidates,
                    ap.request_sent,
                    d1.dept_id,
                    d1.eabrv as 'dept_eabrv',
                    d1.fabrv as 'dept_fabrv',
                    d1.edesc as 'dept_edesc',
                    d1.fdesc as 'dept_fdesc',
                    IIF(bc.id IS NOT NULL, bc.id, NULL) as 'default_billing_contact_id',
                    IIF(bc.id IS NOT NULL, bc.first_name, NULL) as 'default_billing_contact_first_name',
                    IIF(bc.id IS NOT NULL, bc.last_name, NULL) as 'default_billing_contact_last_name',
                    IIF(bc.id IS NOT NULL, bc.email, NULL) as 'default_billing_contact_email',
                    IIF(bc.id IS NOT NULL, bc.fis_organisation_code, NULL) as 'default_billing_contact_fis_organisation_code',
                    IIF(bc.id IS NOT NULL, bc.fis_reference_code, NULL) as 'default_billing_contact_fis_reference_code',
                    IIF(bc.id IS NOT NULL, d2.dept_id, NULL) as 'default_billing_contact_dept_id',
                    IIF(bc.id IS NOT NULL, d2.eabrv, NULL) as 'default_billing_contact_dept_eabrv',
                    IIF(bc.id IS NOT NULL, d2.fabrv, NULL) as 'default_billing_contact_dept_fabrv',
                    IIF(bc.id IS NOT NULL, d2.edesc, NULL) as 'default_billing_contact_dept_edesc',
                    IIF(bc.id IS NOT NULL, d2.fdesc, NULL) as 'default_billing_contact_dept_fdesc'
                FROM {db_name}..custom_models_assessmentprocess ap
                JOIN {db_name}..user_management_models_user u on u.id = ap.user_id
                JOIN {db_name}..CAT_REF_DEPARTMENTS_VW d1 on d1.dept_id = ap.department_id
                LEFT JOIN {db_name}..custom_models_billingcontact bc on bc.id = ap.default_billing_contact_id
                LEFT JOIN {db_name}..CAT_REF_DEPARTMENTS_VW d2 on d2.dept_id = bc.department_id
                OUTER APPLY (SELECT TOP 1 CONVERT(VARCHAR(10), DATEADD(DAY, -ap2.duration, ap2.closing_date), 23) as 'calculated_initial_date' FROM {db_name}..custom_models_assessmentprocess ap2 WHERE ap2.id = ap.id AND closing_date is not NULL) sent_assessment_process_data
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0215_assessmentprocessassignedtestspecsvw_assigned_test_id_and_more",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
