from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[test_center_ola_time_slots_vw] AS
    #             SELECT
    #                 ts.id,
    #                 ts.start_time,
    #                 ts.end_time,
    #                 ts.utc_offset,
    #                 ts.availability,


# 				ts.slots_to_prioritize,
#                 l.language_id as 'assessed_language_id',
#                 assessed_language_text_en.text as 'assessed_language_text_en',
#                 assessed_language_text_fr.text as 'assessed_language_text_fr',
#                 dow.id as 'day_of_week_id',
#                 dow.codename as 'day_of_week_codename',
#                 day_of_week_text_en.text as 'day_of_week_text_en',
#                 day_of_week_text_fr.text as 'day_of_week_text_fr',
#                 ts.test_center_id,
#                 number_of_selected_assessors.count as 'number_of_selected_assessors',
# 				-- available slots (high + medium + low)
# 				(number_of_selected_assessors.count * (SELECT * FROM {db_name}..get_ola_prioritization_value_by_codename_fn('high')) / 100) + (number_of_selected_assessors.count * (SELECT * FROM {db_name}..get_ola_prioritization_value_by_codename_fn('medium')) / 100) + (number_of_selected_assessors.count * (SELECT * FROM {db_name}..get_ola_prioritization_value_by_codename_fn('low')) / 100) as 'number_of_available_slots',
# 				-- NO LONGER USED AT THE MOMENT, SINCE PRIORITIZATION FROM BO OLA CONFIGS IS NOW USED INSTEAD
# 				IIF(prioritization_high.percentage is not NULL, prioritization_high.percentage, 0) as 'prioritization_high',
# 				IIF(prioritization_medium.percentage is not NULL, prioritization_medium.percentage, 0) as 'prioritization_medium',
# 				IIF(prioritization_low.percentage is not NULL, prioritization_low.percentage, 0) as 'prioritization_low'
#             FROM {db_name}..custom_models_testcenterolatimeslot ts
#             JOIN {db_name}..custom_models_language l on l.language_id = ts.assessed_language_id
#             OUTER APPLY (SELECT lt1.text FROM {db_name}..custom_models_languagetext lt1 WHERE lt1.language_ref_id = l.language_id AND lt1.language_id = 1) assessed_language_text_en
#             OUTER APPLY (SELECT lt2.text FROM {db_name}..custom_models_languagetext lt2 WHERE lt2.language_ref_id = l.language_id AND lt2.language_id = 2) assessed_language_text_fr
#             JOIN {db_name}..custom_models_dayofweek dow on dow.id = ts.day_of_week_id
#             OUTER APPLY (SELECT dowt1.text FROM {db_name}..custom_models_dayofweektext dowt1 WHERE dowt1.day_of_week_id = dow.id AND dowt1.language_id = 1) day_of_week_text_en
#             OUTER APPLY (SELECT dowt2.text FROM {db_name}..custom_models_dayofweektext dowt2 WHERE dowt2.day_of_week_id = dow.id AND dowt2.language_id = 2) day_of_week_text_fr
# 			OUTER APPLY (SELECT COUNT(*) as 'count' FROM {db_name}..custom_models_testcenterolatimeslotassessoravailability tcotsaa WHERE tcotsaa.test_center_ola_time_slot_id = ts.id) number_of_selected_assessors
#             OUTER APPLY (SELECT tctsp1.percentage FROM {db_name}..custom_models_testcenterolatimeslotprioritization tctsp1 WHERE tctsp1.test_center_ola_time_slot_id = ts.id AND tctsp1.reason_for_testing_priority_id = (SELECT rftp1.id FROM {db_name}..custom_models_reasonfortestingpriority rftp1 WHERE rftp1.codename = 'high')) prioritization_high
# 			OUTER APPLY (SELECT tctsp2.percentage FROM {db_name}..custom_models_testcenterolatimeslotprioritization tctsp2 WHERE tctsp2.test_center_ola_time_slot_id = ts.id AND tctsp2.reason_for_testing_priority_id = (SELECT rftp2.id FROM {db_name}..custom_models_reasonfortestingpriority rftp2 WHERE rftp2.codename = 'medium')) prioritization_medium
# 			OUTER APPLY (SELECT tctsp3.percentage FROM {db_name}..custom_models_testcenterolatimeslotprioritization tctsp3 WHERE tctsp3.test_center_ola_time_slot_id = ts.id AND tctsp3.reason_for_testing_priority_id = (SELECT rftp3.id FROM {db_name}..custom_models_reasonfortestingpriority rftp3 WHERE rftp3.codename = 'low')) prioritization_low
# """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0412_remove_testcenterolaavailabletimeslotsvw_available_test_assessors_high_priority_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
