from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[user_accommodation_file_report_data_vw] AS
                SELECT
                    huaf.id,
                    huaf.history_id,
                    huaf.history_date,
                    huaf.is_uit,
                    huaf.created_date,
                    CASE
                        WHEN uafs.codename = 'pending_approval' THEN CONVERT(VARCHAR(10), latest_completed_file_data.history_date, 23)
                        ELSE NULL
                    END as 'request_completed_by_aae_date',
                    uafs.id as 'status_id',
                    uafs.codename as 'status_codename',
                    uafst_en.text as 'status_name_en',
                    uafst_fr.text as 'status_name_fr',
                    ummu.id as 'user_id',
                    ummu.username as 'user_username',
                    ummu.email as 'user_email',
                    ummu.first_name as 'user_first_name',
                    ummu.last_name as 'user_last_name',
                    at_u.id as 'assigned_to_user_id',
                    at_u.email as 'assigned_to_user_email',
                    at_u_email_data.goc_email as 'assigned_to_user_goc_email',
                    at_u.first_name as 'assigned_to_user_first_name',
                    at_u.last_name 'assigned_to_user_last_name',
                    apatsv.hr_user_id,
                    apatsv.hr_email,
                    apatsv.hr_first_name,
                    apatsv.hr_last_name,
                    IIF(huafom.history_type = '+', huafom.other_measures, NULL) as 'other_measures',
                    -- TEST SKILL TYPE ID
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 THEN ts.test_skill_type_id
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_id
                    END as 'test_skill_type_id',
                    -- TEST SKILL TYPE CODENAME
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 THEN tst.codename
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_codename
                    END as 'test_skill_type_codename',
                    -- TEST SKILL TYPE NAME (EN)
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 THEN type_e.text
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_name_en
                    END as 'test_skill_type_name_en',
                    -- TEST SKILL TYPE NAME (FR)
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 THEN type_f.text
                        -- Supervised Test
                        ELSE apatsv.test_skill_type_name_fr
                    END as 'test_skill_type_name_fr',
                    -- TEST SKILL SUB-TYPE ID
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sled.id
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occd.id
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_id
                    END as 'test_skill_sub_type_id',
                    -- TEST SKILL SUB-TYPE CODENAME
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sled.codename
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occd.codename
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_codename
                    END as 'test_skill_sub_type_codename',
                    -- TEST SKILL SUB-TYPE NAME (EN)
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sle_sub_type_e.text
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occ_sub_type_e.text
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_name_en
                    END as 'test_skill_sub_type_name_en',
                    -- TEST SKILL SUB-TYPE NAME (FR)
                    CASE
                        -- UIT Test
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sle_sub_type_f.text
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occ_sub_type_f.text
                        WHEN huaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
                        -- Supervised Test
                        ELSE apatsv.test_skill_sub_type_name_fr
                    END as 'test_skill_sub_type_name_fr',
                    td.id as 'test_id',
                    td.parent_code,
                    td.test_code,
                    td.version,
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    huaftta.specified_test_description,
                    default_times.sum as 'total_default_time',
                    -- if adjusted_times.sum is not defined ==> go get the most updated data from the real table (not the historical one)
                    IIF(adjusted_times.sum is not NULL, adjusted_times.sum, default_times.sum) as 'total_adjusted_time',
                    -- if last entry of break bank is "+" ==> break bank defined
                    -- if last entry of break bank is "-" ==> no break bank defined
                    IIF(huafbb.history_type = '+', huafbb.break_time, NULL) as 'break_time'
                FROM {db_name}..custom_models_historicaluseraccommodationfile huaf
                -- PREVIOUS COMPLETED USER ACCOMMODATION FILE
                OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_historicaluseraccommodationfile phuaf_2 WHERE phuaf_2.id = huaf.id AND phuaf_2.history_date < huaf.history_date ORDER BY phuaf_2.history_date DESC) previous_huaf
                -- CANDIDATE USER DATA
                JOIN {db_name}..user_management_models_user ummu ON ummu.id = huaf.user_id
                -- FILE STATUS DATA
                JOIN {db_name}..custom_models_useraccommodationfilestatus uafs ON uafs.id = huaf.status_id
                JOIN {db_name}..custom_models_useraccommodationfilestatustext uafst_en on uafst_en.user_accommodation_file_status_id = uafs.id AND uafst_en.language_id = 1
                JOIN {db_name}..custom_models_useraccommodationfilestatustext uafst_fr on uafst_fr.user_accommodation_file_status_id = uafs.id AND uafst_fr.language_id = 2
                -- ASSIGNED AAE USER DATA
                LEFT JOIN {db_name}..user_management_models_user at_u on at_u.id = huaf.assigned_to_user_id
                -- GOC EMAIL OF ASSIGNED AAE (FROM HISTORICAL CUSTOM USER PERMISSIONS)
                OUTER APPLY (SELECT TOP 1 hcup.goc_email FROM {db_name}..user_management_models_historicalcustomuserpermissions hcup WHERE hcup.user_id = at_u.id AND hcup.history_type = '+' AND hcup.permission_id = (SELECT cp.permission_id FROM {db_name}..user_management_models_custompermissions cp WHERE cp.codename = 'is_aae') ORDER BY hcup.history_date DESC) at_u_email_data
                -- REQUEST COMPLETED DATE (FROM HISTORICAL TABLE)
                OUTER APPLY (SELECT TOP 1 huaf.history_date FROM {db_name}..custom_models_historicaluseraccommodationfile huaf WHERE huaf.id = huaf.id AND huaf.status_id = (SELECT uafs_2.id FROM {db_name}..custom_models_useraccommodationfilestatus uafs_2 WHERE uafs_2.codename = 'completed') ORDER BY huaf.history_date DESC) latest_completed_file_data
                -- OTHER MEASURES DATA
                OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_historicaluseraccommodationfileothermeasures huafom_2 WHERE huafom_2.user_accommodation_file_id = huaf.id AND huafom_2.history_date < huaf.history_date ORDER BY huafom_2.history_date DESC) huafom
                -- TEST SKILL TYPE / SUB-TYPE DATA (UIT TESTS)
                OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_assignedtest at_1 WHERE at_1.user_accommodation_file_id = huaf.id ORDER BY at_1.id DESC) at
                LEFT JOIN {db_name}..cms_models_testskill ts on ts.test_definition_id = at.test_id
                LEFT JOIN {db_name}..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM {db_name}..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM {db_name}..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN {db_name}..cms_models_testskillsle tssle on tssle.test_skill_id = ts.id
                LEFT JOIN {db_name}..cms_models_testskillsledesc sled on sled.id = tssle.test_skill_sle_desc_id
                OUTER APPLY (SELECT TOP 1 sledte.text FROM {db_name}..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
                OUTER APPLY (SELECT TOP 1 sledtf.text FROM {db_name}..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
                LEFT JOIN {db_name}..cms_models_testskilloccupational tsocc on tsocc.test_skill_id = ts.id
                LEFT JOIN {db_name}..cms_models_testskilloccupationaldesc occd on occd.id = tsocc.test_skill_occupational_desc_id
                OUTER APPLY (SELECT TOP 1 occdte.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = occd.id AND occdte.language_id = 1) occ_sub_type_e
                OUTER APPLY (SELECT TOP 1 occdtf.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = occd.id AND occdtf.language_id = 2) occ_sub_type_f
                -- TEST SKILL TYPE / SUB-TYPE DATA (SUPERVISED TESTS)
                OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_historicalconsumedreservationcodes hcrc_1 WHERE hcrc_1.user_accommodation_file_id = huaf.id AND hcrc_1.history_date < huaf.history_date ORDER BY hcrc_1.history_date DESC) hcrc
                LEFT JOIN {db_name}..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = hcrc.assessment_process_assigned_test_specs_id
                --  TEST TO ADMINISTER
                OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_historicaluseraccommodationfiletesttoadminister huaftta_2 WHERE huaftta_2.user_accommodation_file_id = huaf.id AND huaftta_2.history_date < huaf.history_date ORDER BY huaftta_2.history_date DESC) huaftta
                LEFT JOIN {db_name}..cms_models_testdefinition td on td.id = huaftta.test_id
                -- BREAK TIME DATA
                OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_historicaluseraccommodationfilebreakbank huafbb_2 WHERE huafbb_2.user_accommodation_file_id = huaf.id AND huafbb_2.history_date < huaf.history_date ORDER BY huafbb_2.history_date DESC) huafbb
                -- TOTAL DEFAULT TEST TIME
                OUTER APPLY (SELECT SUM(tsec.default_time) as 'sum' FROM {db_name}..cms_models_testsection tsec WHERE tsec.test_definition_id = td.id AND tsec.default_time is not NULL) default_times
                -- TOTAL ADJUSTED TIME
                OUTER APPLY (SELECT SUM(huaftt.test_section_time) AS 'sum' FROM {db_name}..custom_models_historicaluseraccommodationfiletesttime huaftt WHERE huaftt.user_accommodation_file_id = huaf.id AND huaftt.history_date < huaf.history_date AND DATEDIFF(second, huaftt.history_date, previous_huaf.history_date) < 1 AND huaftt.history_type = '+') adjusted_times
                -- ONLY COMPLETED USER ACCOMMODATION FILES
                WHERE huaf.status_id = (SELECT uafs_2.id FROM {db_name}..custom_models_useraccommodationfilestatus uafs_2 WHERE uafs_2.codename = 'pending_approval')
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0294_useraccommodationfiledatafordetailspopupvw_specified_test_description",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
