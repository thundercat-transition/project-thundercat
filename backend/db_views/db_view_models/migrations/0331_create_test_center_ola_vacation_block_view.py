from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_center_ola_vacation_block_vw] AS
                SELECT 
                    vb.id,
                    vb.date_from,
                    vb.date_to,
                    vb.test_center_id,
                    vacation_block_availability_en.availability as 'availability_en',
                    vacation_block_availability_fr.availability as 'availability_fr'
                FROM {db_name}..custom_models_testcenterolavacationblock vb
                OUTER APPLY (SELECT vba1.availability as 'availability' FROM {db_name}..custom_models_testcenterolavacationblockavailability vba1 WHERE vba1.test_center_ola_vacation_block_id = vb.id AND vba1.language_id = 1) vacation_block_availability_en
                OUTER APPLY (SELECT vba2.availability as 'availability' FROM {db_name}..custom_models_testcenterolavacationblockavailability vba2 WHERE vba2.test_center_ola_vacation_block_id = vb.id AND vba2.language_id = 2) vacation_block_availability_fr
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[test_center_ola_vacation_block_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0330_assessmentprocessassignedtestspecsvw_reason_for_testing_minimum_process_length",
        ),
        (
            "custom_models",
            "0163_alter_testcenterolatimeslot_availability_and_more",
        )
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
