from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[selected_user_accommodation_file_vw] AS
    #             SELECT
    #                 uaf.id,
    #                 uaf.comments,
    #                 uaf.is_uit,
    #                 uaf.modify_date,
    #                 uaf.created_date,
    #                 uaf.last_modified_by_user_id,
    #                 uafs.id AS 'status_id',
    #                 uafs.codename AS 'status_codename',
    #                 uaf.test_center_id,
    #                 ummu.id AS 'user_id',
    #                 ummu.username AS 'user_username',
    #                 ummu.email AS 'user_email',
    #                 ummu.secondary_email AS 'user_secondary_email',
    #                 ummu.first_name AS 'user_first_name',
    #                 ummu.last_name AS 'user_last_name',
    #                 ummu.birth_date AS 'user_dob',
    #                 ummu.psrs_applicant_id AS 'user_psrs_applicant_id',
    #                 ummu.pri AS 'user_pri',
    #                 ummu.military_nbr AS 'user_military_nbr',
    #                 ummu.phone_number AS 'user_phone_number',
    #                 CASE
    #                     -- Supervised Test (getting reference number from assessment process data)
    #                     WHEN uaf.is_uit = 0 THEN apatsv.assessment_process_reference_number
    #                     -- UIT Test
    #                     ELSE
    #                         CASE
    #                             -- Orderless Test (getting reference number from orderless financial data)
    #                             WHEN ofd.id is not NULL THEN ofd.reference_number
    #                             -- Ordered Test (getting reference number from historical test permission)
    #                             WHEN htp.id is not NULL THEN htp.staffing_process_number
    #                             -- Undefined reference number
    #                             ELSE NULL
    #                         END
    #                 END as 'reference_number',
    #                 tcv.name as 'test_center_name',
    #                 -- DEPT ID
    #                 CASE
    #                     -- Supervised Test (getting department data from user accommodation file request)
    #                     WHEN uaf.is_uit = 0 THEN tcv.dept_id
    #                     -- UIT Test
    #                     ELSE
    #                         CASE
    #                             -- Orderless Test (getting department data from orderless financial data)
    #                             WHEN ofd.id is not NULL THEN dept_ref_from_ofd.DEPT_ID
    #                             -- Ordered Test (getting department data from historical test permission)
    #                             WHEN htp.id is not NULL THEN
    #                                 CASE
    #                                     -- MATCHING DEPT_ID FOUND
    #                                     WHEN (SELECT COUNT(*) FROM {db_name}..CAT_REF_DEPARTMENTS_VW WHERE DEPT_ID = htp.department_ministry_code) > 0 THEN dep_ref_from_htp.DEPT_ID
    #                                     -- PSC DEPT_ID
    #                                     ELSE 104
    #                                 END
    #                             -- Undefined department
    #                             ELSE NULL
    #                         END
    #                 END as 'dept_id',
    #                 -- DEPT ABRV (EN)
    #                 CASE
    #                     -- Supervised Test (getting department data from user accommodation file request)
    #                     WHEN uaf.is_uit = 0 THEN tcv.dept_eabrv
    #                     -- UIT Test
    #                     ELSE
    #                         CASE
    #                             -- Orderless Test (getting department data from orderless financial data)
    #                             WHEN ofd.id is not NULL THEN dept_ref_from_ofd.EABRV
    #                             -- Ordered Test (getting department data from historical test permission)
    #                             WHEN htp.id is not NULL THEN
    #                                 CASE
    #                                     -- MATCHING DEPT_ID FOUND
    #                                     WHEN (SELECT COUNT(*) FROM {db_name}..CAT_REF_DEPARTMENTS_VW WHERE DEPT_ID = htp.department_ministry_code) > 0 THEN dep_ref_from_htp.EABRV
    #                                     -- PSC DEPT_EABRV
    #                                     ELSE 'PSC'
    #                                 END
    #                             -- Undefined department
    #                             ELSE NULL
    #                         END
    #                 END as 'dept_eabrv',
    #                 -- DEPT ABRV (FR)
    #                 CASE
    #                     -- Supervised Test (getting department data from user accommodation file request)
    #                     WHEN uaf.is_uit = 0 THEN tcv.dept_fabrv
    #                     -- UIT Test
    #                     ELSE
    #                         CASE
    #                             -- Orderless Test (getting department data from orderless financial data)
    #                             WHEN ofd.id is not NULL THEN dept_ref_from_ofd.FABRV
    #                             -- Ordered Test (getting department data from historical test permission)
    #                             WHEN htp.id is not NULL THEN
    #                                 CASE
    #                                     -- MATCHING DEPT_ID FOUND
    #                                     WHEN (SELECT COUNT(*) FROM {db_name}..CAT_REF_DEPARTMENTS_VW WHERE DEPT_ID = htp.department_ministry_code) > 0 THEN dep_ref_from_htp.FABRV
    #                                     -- PSC DEPT_FABRV
    #                                     ELSE 'CFP'
    #                                 END
    #                             -- Undefined department
    #                             ELSE NULL
    #                         END
    #                 END as 'dept_fabrv',
    #                 -- DEPT DESCRIPTION (EN)
    #                 CASE
    #                     -- Supervised Test (getting department data from user accommodation file request)
    #                     WHEN uaf.is_uit = 0 THEN tcv.dept_edesc
    #                     -- UIT Test
    #                     ELSE
    #                         CASE
    #                             -- Orderless Test (getting department data from orderless financial data)
    #                             WHEN ofd.id is not NULL THEN dept_ref_from_ofd.EDESC
    #                             -- Ordered Test (getting department data from historical test permission)
    #                             WHEN htp.id is not NULL THEN
    #                                 CASE
    #                                     -- MATCHING DEPT_ID FOUND
    #                                     WHEN (SELECT COUNT(*) FROM {db_name}..CAT_REF_DEPARTMENTS_VW WHERE DEPT_ID = htp.department_ministry_code) > 0 THEN dep_ref_from_htp.EDESC
    #                                     -- PSC DEPT_EDESC
    #                                     ELSE 'Public Service Commission'
    #                                 END
    #                             -- Undefined department
    #                             ELSE NULL
    #                         END
    #                 END as 'dept_edesc',
    #                 -- DEPT DESCRIPTION (FR)
    #                 CASE
    #                     -- Supervised Test (getting department data from user accommodation file request)
    #                     WHEN uaf.is_uit = 0 THEN tcv.dept_fdesc
    #                     -- UIT Test
    #                     ELSE
    #                         CASE
    #                             -- Orderless Test (getting department data from orderless financial data)
    #                             WHEN ofd.id is not NULL THEN dept_ref_from_ofd.FDESC
    #                             -- Ordered Test (getting department data from historical test permission)
    #                             WHEN htp.id is not NULL THEN
    #                                 CASE
    #                                     -- MATCHING DEPT_ID FOUND
    #                                     WHEN (SELECT COUNT(*) FROM {db_name}..CAT_REF_DEPARTMENTS_VW WHERE DEPT_ID = htp.department_ministry_code) > 0 THEN dep_ref_from_htp.FDESC
    #                                     -- PSC DEPT_FDESC
    #                                     ELSE 'Commission de la fonction publique'
    #                                 END
    #                             -- Undefined department
    #                             ELSE NULL
    #                         END
    #                 END as 'dept_fdesc',
    #                 -- PRIMARY CONTACT USER ID
    #                 CASE
    #                     -- Supervised Test (HR Contact)
    #                     WHEN hru.id is not NULL THEN  hru.id
    #                     -- UIT Test (TA Contact)
    #                     ELSE tau.id
    #                 END as 'primary_contact_user_id',
    #                 -- PRIMARY CONTACT USER EMAIL
    #                 CASE
    #                     -- Supervised Test (HR Contact)
    #                     WHEN hru.id is not NULL THEN  hru.email
    #                     -- UIT Test (TA Contact)
    #                     ELSE tau.email
    #                 END as 'primary_contact_user_email',
    #                 -- PRIMARY CONTACT USER FIRST NAME
    #                 CASE
    #                     -- Supervised Test (HR Contact)
    #                     WHEN hru.id is not NULL THEN  hru.first_name
    #                     -- UIT Test (TA Contact)
    #                     ELSE tau.first_name
    #                 END as 'primary_contact_user_first_name',
    #                 -- PRIMARY CONTACT USER LAST NAME
    #                 CASE
    #                     -- Supervised Test (HR Contact)
    #                     WHEN hru.id is not NULL THEN  hru.last_name
    #                     -- UIT Test (TA Contact)
    #                     ELSE tau.last_name
    #                 END as 'primary_contact_user_last_name',
    #                 -- TEST SKILL TYPE ID
    #                 CASE
    #                     -- UIT Test
    #                     WHEN uaf.is_uit = 1 THEN ts.test_skill_type_id
    #                     -- Supervised Test
    #                     ELSE apatsv.test_skill_type_id
    #                 END as 'test_skill_type_id',
    #                 -- TEST SKILL TYPE CODENAME
    #                 CASE
    #                     -- UIT Test
    #                     WHEN uaf.is_uit = 1 THEN tst.codename
    #                     -- Supervised Test
    #                     ELSE apatsv.test_skill_type_codename
    #                 END as 'test_skill_type_codename',
    #                 -- TEST SKILL TYPE NAME (EN)
    #                 CASE
    #                     -- UIT Test
    #                     WHEN uaf.is_uit = 1 THEN type_e.text
    #                     -- Supervised Test
    #                     ELSE apatsv.test_skill_type_name_en
    #                 END as 'test_skill_type_name_en',
    #                 -- TEST SKILL TYPE NAME (FR)
    #                 CASE
    #                     -- UIT Test
    #                     WHEN uaf.is_uit = 1 THEN type_f.text
    #                     -- Supervised Test
    #                     ELSE apatsv.test_skill_type_name_fr
    #                 END as 'test_skill_type_name_fr',
    #                 -- TEST SKILL SUB-TYPE ID
    #                 CASE
    #                     -- UIT Test
    #                     WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sled.id
    #                     WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occd.id
    #                     WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
    #                     -- Supervised Test
    #                     ELSE apatsv.test_skill_sub_type_id
    #                 END as 'test_skill_sub_type_id',
    #                 -- TEST SKILL SUB-TYPE CODENAME
    #                 CASE
    #                     -- UIT Test
    #                     WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sled.codename
    #                     WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occd.codename
    #                     WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
    #                     -- Supervised Test
    #                     ELSE apatsv.test_skill_sub_type_codename
    #                 END as 'test_skill_sub_type_codename',
    #                 -- TEST SKILL SUB-TYPE NAME (EN)
    #                 CASE
    #                     -- UIT Test
    #                     WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sle_sub_type_e.text
    #                     WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occ_sub_type_e.text
    #                     WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
    #                     -- Supervised Test
    #                     ELSE apatsv.test_skill_sub_type_name_en
    #                 END as 'test_skill_sub_type_name_en',
    #                 -- TEST SKILL SUB-TYPE NAME (FR)
    #                 CASE
    #                     -- UIT Test
    #                     WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 2 THEN sle_sub_type_f.text
    #                     WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 3 THEN occ_sub_type_f.text
    #                     WHEN uaf.is_uit = 1 AND ts.test_skill_type_id = 1 THEN NULL
    #                     -- Supervised Test
    #                     ELSE apatsv.test_skill_sub_type_name_fr
    #                 END as 'test_skill_sub_type_name_fr'
    #             FROM {db_name}..custom_models_useraccommodationfile uaf
    #             JOIN {db_name}..user_management_models_user ummu ON ummu.id = uaf.user_id
    #             JOIN {db_name}..custom_models_useraccommodationfilestatus uafs ON uafs.id = uaf.status_id
    #             -- using assigned test to get data (if UIT)
    #             LEFT JOIN {db_name}..custom_models_assignedtest at on at.user_accommodation_file_id = uaf.id
    #             LEFT JOIN {db_name}..user_management_models_user tau on tau.id = at.ta_user_id
    #             LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
    #             LEFT JOIN {db_name}..CAT_REF_DEPARTMENTS_VW dept_ref_from_ofd on dept_ref_from_ofd.DEPT_ID = ofd.department_ministry_id
    #             LEFT JOIN {db_name}..cms_models_historicaltestpermissions htp on htp.test_order_number = at.test_order_number
    #             LEFT JOIN {db_name}..CAT_REF_DEPARTMENTS_VW dep_ref_from_htp on dep_ref_from_htp.DEPT_ID = htp.department_ministry_code
    #             LEFT JOIN {db_name}..cms_models_testskill ts on ts.test_definition_id = at.test_id
    #             LEFT JOIN {db_name}..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
    #             OUTER APPLY (SELECT TOP 1 tstte.text FROM {db_name}..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
    #             OUTER APPLY (SELECT TOP 1 tsttf.text FROM {db_name}..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
    #             LEFT JOIN {db_name}..cms_models_testskillsle tssle on tssle.test_skill_id = ts.id
    #             LEFT JOIN {db_name}..cms_models_testskillsledesc sled on sled.id = tssle.test_skill_sle_desc_id
    #             OUTER APPLY (SELECT TOP 1 sledte.text FROM {db_name}..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sle_sub_type_e
    #             OUTER APPLY (SELECT TOP 1 sledtf.text FROM {db_name}..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sle_sub_type_f
    #             LEFT JOIN {db_name}..cms_models_testskilloccupational tsocc on tsocc.test_skill_id = ts.id
    #             LEFT JOIN {db_name}..cms_models_testskilloccupationaldesc occd on occd.id = tsocc.test_skill_occupational_desc_id
    #             OUTER APPLY (SELECT TOP 1 occdte.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdte WHERE occdte.test_skill_occupational_desc_id = occd.id AND occdte.language_id = 1) occ_sub_type_e
    #             OUTER APPLY (SELECT TOP 1 occdtf.text FROM {db_name}..cms_models_testskilloccupationaldesctext occdtf WHERE occdtf.test_skill_occupational_desc_id = occd.id AND occdtf.language_id = 2) occ_sub_type_f
    #             -- ----------------------------------------------------
    #             -- using consumed reservation codes to get data (is Supervised)
    #             LEFT JOIN {db_name}..custom_models_consumedreservationcodes crc on crc.user_accommodation_file_id = uaf.id
    #             LEFT JOIN {db_name}..assessment_process_assigned_test_specs_vw apatsv on apatsv.id = crc.assessment_process_assigned_test_specs_id
    #             LEFT JOIN {db_name}..custom_models_assessmentprocess ap on ap.id = apatsv.assessment_process_id
    #             LEFT JOIN {db_name}..user_management_models_user hru on hru.id = ap.user_id
    #             -- ------------------------------------------------------------------------
    #             LEFT JOIN {db_name}..test_centers_vw tcv on tcv.id = uaf.test_center_id
    # """.format(
    #             db_name=settings.DATABASES["default"]["NAME"]
    #         )
    #     )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0249_update_assessment_process_assigned_test_specs_vw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
