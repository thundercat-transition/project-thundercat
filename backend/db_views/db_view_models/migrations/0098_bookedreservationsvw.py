# Generated by Django 4.2.7 on 2024-01-13 01:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('db_view_models', '0097_create_booked_reservations_view'),
    ]

    operations = [
        migrations.CreateModel(
            name='BookedReservationsVW',
            fields=[
                ('id', models.IntegerField(db_column='ID', primary_key=True, serialize=False)),
                ('reservation_code', models.CharField(max_length=14)),
                ('modify_date', models.DateField(db_column='modify_date')),
                ('candidate_id', models.IntegerField(db_column='CANDIDATE_ID')),
                ('assessment_process_closing_date', models.DateField(db_column='ASSESSMENT_PROCESS_CLOSING_DATE')),
                ('test_skill_type_id', models.IntegerField(db_column='TEST_SKILL_TYPE_ID')),
                ('consumed_reservation_codename', models.CharField(max_length=14)),
                ('consumed_reservation_code_en_status', models.CharField(max_length=30)),
                ('consumed_reservation_code_fr_status', models.CharField(max_length=30)),
                ('test_skill_type_en_name', models.CharField(db_column='TEST_SKILL_TYPE_EN_NAME', max_length=150)),
                ('test_skill_type_fr_name', models.CharField(db_column='TEST_SKILL_TYPE_FR_NAME', max_length=150)),
                ('test_skill_sub_type_id', models.IntegerField(db_column='TEST_SKILL_SUB_TYPE_ID')),
                ('test_skill_sub_type_en_name', models.CharField(db_column='TEST_SKILL_SUB_TYPE_EN_NAME', max_length=150)),
                ('test_skill_sub_type_fr_name', models.CharField(db_column='TEST_SKILL_SUB_TYPE_FR_NAME', max_length=150)),
                ('request_sent', models.BooleanField(db_column='REQUEST_SENT', default=False)),
                ('test_center_test_session_data_id', models.IntegerField(db_column='TEST_CENTER_TEST_SESSION_DATA_ID')),
                ('date', models.DateField(db_column='DATE')),
                ('start_time', models.DateTimeField(db_column='START_TIME')),
                ('end_time', models.DateTimeField(db_column='END_TIME')),
                ('test_center_name', models.CharField(max_length=150)),
                ('test_center_address', models.CharField(blank=True, max_length=255)),
                ('test_center_city', models.CharField(blank=True, max_length=150)),
                ('test_center_postal_code', models.CharField(blank=True, max_length=7)),
                ('test_center_province', models.CharField(blank=True, max_length=150)),
                ('test_center_country', models.CharField(blank=True, max_length=150)),
            ],
            options={
                'db_table': 'booked_reservations_vw',
                'managed': False,
            },
        ),
    ]
