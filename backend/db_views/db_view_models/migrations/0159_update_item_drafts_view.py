from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[item_drafts_vw] AS
				SELECT
					id.item_id as 'item_id',
					id.system_id,
					id.item_bank_id,
					id.modify_date,
					id.user_id,
					u.username,
					id.active_status,
					id.shuffle_options,
					id.item_type_id,
					it.codename as 'item_type_codename',
					ds.id as 'development_status_id',
					ds.codename as 'development_status_codename',
					ds.en_name as 'development_status_name_en',
					ds.fr_name as 'development_status_name_fr',
					IIF (rf.id is not NULL, rf.id, NULL) as 'response_format_id',
					IIF (rf.codename is not NULL, rf.codename, NULL) as 'response_format_codename',
					IIF (rf.en_name is not NULL, rf.en_name, NULL) as 'response_format_name_en',
					IIF (rf.fr_name is not NULL, rf.fr_name, NULL) as 'response_format_name_fr',
					id.historical_id as 'historical_id'
				FROM {db_name}..cms_models_itemdrafts id
				JOIN {db_name}..user_management_models_user u on u.id = id.user_id
				JOIN {db_name}..cms_models_itemtype it on it.id = id.item_type_id
				JOIN {db_name}..cms_models_itemdevelopmentstatuses ds on ds.id = id.development_status_id
				LEFT JOIN {db_name}..cms_models_itemresponseformats rf on rf.id = id.response_format_id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """ALTER VIEW [dbo].[item_drafts_vw] AS


# 			SELECT
# 				id.item_id as 'item_id',
# 				id.system_id,
# 				id.item_bank_id,
# 				id.modify_date,
# 				id.username_id,
# 				id.active_status,
# 				id.shuffle_options,
# 				id.item_type_id,
# 				it.codename as 'item_type_codename',
# 				ds.id as 'development_status_id',
# 				ds.codename as 'development_status_codename',
# 				ds.en_name as 'development_status_name_en',
# 				ds.fr_name as 'development_status_name_fr',
# 				IIF (rf.id is not NULL, rf.id, NULL) as 'response_format_id',
# 				IIF (rf.codename is not NULL, rf.codename, NULL) as 'response_format_codename',
# 				IIF (rf.en_name is not NULL, rf.en_name, NULL) as 'response_format_name_en',
# 				IIF (rf.fr_name is not NULL, rf.fr_name, NULL) as 'response_format_name_fr',
# 				id.historical_id as 'historical_id'
# 			FROM {db_name}..cms_models_itemdrafts id
# 			JOIN {db_name}..cms_models_itemtype it on it.id = id.item_type_id
# 			JOIN {db_name}..cms_models_itemdevelopmentstatuses ds on ds.id = id.development_status_id
# 			LEFT JOIN {db_name}..cms_models_itemresponseformats rf on rf.id = id.response_format_id
#     """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0158_remove_testresultreportvw_allowed_ta_usernames_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
