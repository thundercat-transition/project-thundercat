from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[billing_contact_vw] AS
                SELECT
                bc.id,
                bc.first_name,
                bc.last_name,
                bc.email,
                bc.fis_organisation_code,
                bc.fis_reference_code,
                bc.user_id,
                bc.department_id as "dept_id",
                odv.EDESC as "dept_edesc",
                odv.FDESC as "dept_fdesc",
                odv.EABRV as "dept_eabrv",
                odv.FABRV as "dept_fabrv"
                FROM {db_name}..custom_models_billingcontact bc
                LEFT JOIN {db_name}..CAT_REF_DEPARTMENTS_VW odv ON bc.department_id = odv.DEPT_ID
            """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0122_create_camm_integration_vw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
