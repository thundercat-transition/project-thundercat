from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[aae_email_details_vw] AS
                SELECT
                    uaf.id,
                    suaf.user_email as candidate_email,
                    suaf.user_first_name as candidate_first_name,
                    suaf.user_last_name as candidate_last_name,
                    suaf.primary_contact_user_email as contact_email,
                    suaf.primary_contact_user_first_name as contact_first_name,
                    suaf.primary_contact_user_last_name as contact_last_name,
                    apats.email as hr_invite_email,
                    apats.first_name as hr_invite_first_name,
                    apats.last_name as hr_invite_last_name,
                    suaf.reference_number as reference_number,
                    u.email as tcm_email
                    -- the names and email the ta used to invite them??
                FROM {db_name}..custom_models_useraccommodationfile uaf
                LEFT JOIN {db_name}..user_management_models_user cand 
                ON uaf.user_id=cand.id
                LEFT JOIN {db_name}..custom_models_testcenterassociatedmanagers tcam
                ON uaf.test_center_id=tcam.test_center_id
                LEFT JOIN {db_name}..user_management_models_user u 
                ON tcam.user_id=u.id
                LEFT JOIN {db_name}..custom_models_consumedreservationcodes crc
                ON crc.user_accommodation_file_id=uaf.id
                LEFT JOIN {db_name}..custom_models_assessmentprocessassignedtestspecs apats
                ON crc.assessment_process_assigned_test_specs_id=apats.id
                LEFT JOIN {db_name}..[selected_user_accommodation_file_vw] suaf
                ON suaf.id=uaf.id;
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[aae_email_details_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0288_assessmentprocessassignedtestspecsvw_candidate_email_and_more",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
