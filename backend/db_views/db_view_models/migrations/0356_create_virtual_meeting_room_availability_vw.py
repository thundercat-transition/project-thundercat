from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[virtual_meeting_room_availability_vw] AS
                SELECT
                    vtr.id as room_id,
                    vtr.user_id as room_user_id,
                    vml.date,
                    vml.start_time,
                    vml.end_time,
                COUNT(vml.id) count
                FROM {db_name}..custom_models_virtualtestroom vtr
                LEFT JOIN {db_name}..custom_models_virtualteamsmeetingsession vml on vtr.id=vml.virtual_test_room_id
                GROUP BY vtr.id, user_id, date, start_time, end_time
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            "DROP VIEW IF EXISTS [dbo].[virtual_meeting_room_availability_vw]"
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0355_useraccommodationfiledatafordetailspopupvw_is_alternate_test_request",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
