from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[assessment_process_vw] AS
    #             SELECT
    #                 ap.id,
    #                 ap.user_id,
    #                 ap.reference_number,
    #                 ap.closing_date,
    #                 ap.allow_booking_external_tc,
    #                 d1.dept_id,
    #                 d1.eabrv as 'dept_eabrv',
    #                 d1.fabrv as 'dept_fabrv',
    #                 d1.edesc as 'dept_edesc',
    #                 d1.fdesc as 'dept_fdesc',
    #                 bc.id as 'default_billing_contact_id',
    #                 bc.first_name as 'default_billing_contact_first_name',
    #                 bc.last_name as 'default_billing_contact_last_name',
    #                 bc.email as 'default_billing_contact_email',
    #                 bc.fis_organisation_code as 'default_billing_contact_fis_organisation_code',
    #                 bc.fis_reference_code as 'default_billing_contact_fis_reference_code',
    #                 d2.dept_id as 'default_billing_contact_dept_id',
    #                 d2.eabrv as 'default_billing_contact_dept_eabrv',
    #                 d2.fabrv as 'default_billing_contact_dept_fabrv',
    #                 d2.edesc as 'default_billing_contact_dept_edesc',
    #                 d2.fdesc as 'default_billing_contact_dept_fdesc'
    #             FROM {db_name}..custom_models_assessmentprocess ap
    #             JOIN {db_name}..CAT_REF_DEPARTMENTS_VW d1 on d1.dept_id = ap.department_id
    #             LEFT JOIN {db_name}..custom_models_billingcontact bc on bc.id = ap.default_billing_contact_id
    #             JOIN {db_name}..CAT_REF_DEPARTMENTS_VW d2 on d2.dept_id = bc.department_id
    #     """.format(
    #             db_name=settings.DATABASES["default"]["NAME"]
    #         )
    #     )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0076_itemdraftsvw_item_type_codename_and_more",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
