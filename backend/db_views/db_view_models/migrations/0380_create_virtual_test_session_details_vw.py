from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[virtual_test_session_details_vw] AS
                SELECT
                    vtms.id,
                    vtms.candidate_id,
                    vtms.start_time,
                    vtms.date,
                    vtms.end_time,
                    vtms.meeting_link,
                    stte.text as 'test_skill_type_en_name',
                    sttf.text as 'test_skill_type_fr_name',
	                CASE
                        -- SLE Sub Type
                        WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'sle') THEN (SELECT text FROM {db_name}..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = tsd.test_skill_sub_type_id AND language_id = 1)
                        ELSE
                            -- OCCUPATIONAL Sub Type
                            CASE
                                WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM {db_name}..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = tsd.test_skill_sub_type_id AND language_id = 1)
                            ELSE
                                NULL
                            END
                    END AS 'test_skill_sub_type_en_name',
                    CASE
                        -- SLE Sub Type
                        WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'SLE') THEN (SELECT text FROM {db_name}..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = tsd.test_skill_sub_type_id AND language_id = 2)
                        ELSE
                            -- OCCUPATIONAL Sub Type
                            CASE
                                WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM {db_name}..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = tsd.test_skill_sub_type_id AND language_id = 2)
                            ELSE
                                NULL
                            END
                    END AS 'test_skill_sub_type_fr_name'
                FROM {db_name}..custom_models_virtualteamsmeetingsession vtms
                JOIN {db_name}..custom_models_testcentertestsessiondata tsd on tsd.id=vtms.test_session_id
                JOIN {db_name}..cms_models_testskilltypetext stte on stte.test_skill_type_id = tsd.test_skill_type_id AND stte.language_id = 1
                JOIN {db_name}..cms_models_testskilltypetext sttf on sttf.test_skill_type_id = tsd.test_skill_type_id AND sttf.language_id = 2
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0379_update_test_center_test_sessions_vw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
