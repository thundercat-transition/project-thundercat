from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_result_vw] AS
				SELECT
                    assigned_test_id,
                    candidate_user_id,
                    candidate_username,
                    candidate_email,
                    uit_candidate_email,
                    user_firstname AS 'candidate_first_name',
                    user_lastname AS 'candidate_last_name',
                    candidate_pri,
                    candidate_military_nbr,
                    ta_user_id,
                    ta_username,
                    ta_email,
                    reference_number,
                    test_order_number,
                    process_number,
                    history_id,
                    td_test_code,
                    td_fr_name,
                    td_en_name,
                    submit_date,
                    test_score,
                    CASE
                        WHEN test_is_invalid = 1 THEN 'Invalide'
                        ELSE
                            CASE
                                WHEN test_score IS NULL THEN '-'
                                WHEN test_fr_converted_score IS NULL THEN 'Conversion invalide du résultat'
                            ELSE
                                test_fr_converted_score
                            END
                    END AS 'level_fr',
                    CASE
                        WHEN test_is_invalid = 1 THEN 'Invalid'
                        ELSE
                            CASE
                                WHEN test_score IS NULL THEN '-'
                                WHEN test_en_converted_score IS NULL THEN 'Invalid Score Conversion'
                            ELSE
                                test_en_converted_score
                            END
                    END AS 'level_en',
                    dbo.status2str('fr', test_status_id) AS test_status_fr,
                    dbo.status2str('en', test_status_id) AS test_status_en,
                    test_status_id,
                    test_status_codename,
                    test_id,
                    allowed_ta_user_ids

                FROM
                (
                    SELECT
                        at.id AS 'assigned_test_id',
                        at.test_id,
                        at.ta_user_id AS 'ta_user_id',
                        ta.username AS 'ta_username',
                        ta.email AS 'ta_email',
                        ofd.reference_number AS 'reference_number',
                        at.test_order_number AS 'test_order_number',
                        at.submit_date AS 'submit_date',
                        at.total_score AS 'test_score',
                        at.en_converted_score AS 'test_en_converted_score',
                        at.fr_converted_score AS 'test_fr_converted_score',
                        at.status_id AS 'test_status_id',
                        atst.codename AS 'test_status_codename',
                        at.is_invalid AS 'test_is_invalid',

                        IIF(u.id is not null, u.id, v.id) AS 'candidate_user_id',
                        IIF(u.id is not null, u.username, v.username) AS 'candidate_username',
                        t.candidate_email AS 'uit_candidate_email',
                        IIF(u.id is not null, u.email, v.email) AS 'candidate_email',
                        IIF(u.id is not null, u.first_name, v.first_name) AS 'user_firstname',
                        IIF(u.id is not null, u.last_name, v.last_name) AS 'user_lastname',
                        IIF(u.id is not null, u.pri, v.pri) AS 'candidate_pri',
                        IIF(u.id is not null, u.military_nbr, v.military_nbr) AS 'candidate_military_nbr',

                        p.staffing_process_number AS 'process_number',
                        p.history_id AS 'history_id',

                        td.test_code AS 'td_test_code',
                        td.fr_name AS 'td_fr_name',
                        td.en_name AS 'td_en_name',

                        htp3.ta_user_ids as 'allowed_ta_user_ids'

                    FROM {db_name}..custom_models_assignedtest at
                    JOIN {db_name}..custom_models_assignedteststatus atst on atst.id = at.status_id
                    OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.id = at.user_id AND u1.history_date < at.submit_date ORDER BY u1.history_date DESC) u
                    OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_user u2 WHERE u2.id = at.ta_user_id) ta
                    JOIN {db_name}..user_management_models_user v on v.id = at.user_id
                    OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions p1 WHERE p1.test_order_number = at.test_order_number AND p1.user_id = at.ta_user_id AND p1.history_date < at.submit_date ORDER BY p1.history_date DESC) p
                    JOIN {db_name}..cms_models_testdefinition td ON td.id = at.test_id
                    LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on at.orderless_financial_data_id = ofd.id
                    OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_historicalunsupervisedtestaccesscode t1 WHERE t1.test_access_code = at.test_access_code AND t1.uit_invite_id = at.uit_invite_id ORDER BY t1.history_date DESC) t
                    OUTER APPLY (SELECT string_agg(ta_username_list.user_id, ',') within group (ORDER BY ta_username_list.user_id) as 'ta_user_ids' FROM (SELECT DISTINCT user_id FROM {db_name}..cms_models_historicaltestpermissions htp WHERE htp.history_type = '+' AND htp.test_order_number = at.test_order_number) ta_username_list) htp3
                ) r
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """ALTER VIEW [dbo].[test_result_vw] AS


# 			SELECT
#                 assigned_test_id,
#                 candidate_user_id,
#                 candidate_username,
#                 candidate_email,
#                 uit_candidate_email,
#                 user_firstname AS 'candidate_first_name',
#                 user_lastname AS 'candidate_last_name',
#                 candidate_pri,
#                 candidate_military_nbr,
#                 ta_user_id,
#                 ta_username,
#                 ta_email,
#                 reference_number,
#                 test_order_number,
#                 process_number,
#                 history_id,
#                 td_test_code,
#                 td_fr_name,
#                 td_en_name,
#                 submit_date,
#                 test_score,
#                 CASE
#                     WHEN test_is_invalid = 1 THEN 'Invalide'
#                     ELSE
#                         CASE
#                             WHEN test_score IS NULL THEN '-'
#                             WHEN test_fr_converted_score IS NULL THEN 'Conversion invalide du résultat'
#                         ELSE
#                             test_fr_converted_score
#                         END
#                 END AS 'level_fr',
#                 CASE
#                     WHEN test_is_invalid = 1 THEN 'Invalid'
#                     ELSE
#                         CASE
#                             WHEN test_score IS NULL THEN '-'
#                             WHEN test_en_converted_score IS NULL THEN 'Invalid Score Conversion'
#                         ELSE
#                             test_en_converted_score
#                         END
#                 END AS 'level_en',
#                 dbo.status2str('fr', test_status_id) AS test_status_fr,
#                 dbo.status2str('en', test_status_id) AS test_status_en,
#                 test_status_id,
#                 test_status_codename,
#                 test_id,
#                 allowed_ta_user_ids

#             FROM
#             (
#                 SELECT
#                     at.id AS 'assigned_test_id',
#                     at.test_id,
#                     at.ta_user_id AS 'ta_user_id',
#                     ta.username AS 'ta_username',
#                     ta.email AS 'ta_email',
#                     ofd.reference_number AS 'reference_number',
#                     at.test_order_number AS 'test_order_number',
#                     at.submit_date AS 'submit_date',
#                     at.total_score AS 'test_score',
#                     at.en_converted_score AS 'test_en_converted_score',
#                     at.fr_converted_score AS 'test_fr_converted_score',
#                     at.status_id AS 'test_status_id',
#                     atst.codename AS 'test_status_codename',
#                     at.is_invalid AS 'test_is_invalid',

#                     IIF(u.id is not null, u.id, v.id) AS 'candidate_user_id',
#                     IIF(u.id is not null, u.username, v.username) AS 'candidate_username',
#                     t.candidate_email AS 'uit_candidate_email',
#                     IIF(u.id is not null, u.email, v.email) AS 'candidate_email',
#                     IIF(u.id is not null, u.first_name, v.first_name) AS 'user_firstname',
#                     IIF(u.id is not null, u.last_name, v.last_name) AS 'user_lastname',
#                     IIF(u.id is not null, u.pri, v.pri) AS 'candidate_pri',
#                     IIF(u.id is not null, u.military_nbr, v.military_nbr) AS 'candidate_military_nbr',

#                     p.staffing_process_number AS 'process_number',
#                     p.history_id AS 'history_id',

#                     td.test_code AS 'td_test_code',
#                     td.fr_name AS 'td_fr_name',
#                     td.en_name AS 'td_en_name',

#                     htp3.ta_user_ids as 'allowed_ta_user_ids'

#                 FROM {db_name}..custom_models_assignedtest at
#                 JOIN {db_name}..custom_models_assignedteststatus atst on atst.id = at.status_id
#                 OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.id = at.user_id AND u1.history_date < at.submit_date ORDER BY u1.history_date DESC) u
#                 OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_user u2 WHERE u2.id = at.ta_user_id) ta
#                 JOIN {db_name}..user_management_models_user v on v.id = at.user_id
#                 OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions p1 WHERE p1.test_order_number = at.test_order_number AND p1.username_id = at.ta_id AND p1.history_date < at.submit_date ORDER BY p1.history_date DESC) p
#                 JOIN {db_name}..cms_models_testdefinition td ON td.id = at.test_id
#                 LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on at.orderless_financial_data_id = ofd.id
#                 OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_historicalunsupervisedtestaccesscode t1 WHERE t1.test_access_code = at.test_access_code AND t1.uit_invite_id = at.uit_invite_id ORDER BY t1.history_date DESC) t
#                 OUTER APPLY (SELECT string_agg(ta_username_list.user_id, ',') within group (ORDER BY ta_username_list.user_id) as 'ta_user_ids' FROM (SELECT DISTINCT user_id FROM {db_name}..cms_models_historicaltestpermissions htp WHERE htp.history_type = '+' AND htp.test_order_number = at.test_order_number) ta_username_list) htp3
#             ) r
#     """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0180_update_active_tests_vw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
