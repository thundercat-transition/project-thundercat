from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[scorer_ola_tests_to_assess_vw] AS
                SELECT
                    TOP 100 PERCENT
                    DATEDIFF(SECOND, '1970-01-01', tsd.start_time) as 'id',
                    tsd.date,
                    DATEPART(dw, tsd.date) - 1 as 'day_of_week_id',
                    tsd.start_time,
                    CONVERT(VARCHAR(8), tsd.start_time, 108) as 'simplified_start_time',
                    tsd.end_time,
                    CONVERT(VARCHAR(8), tsd.end_time, 108) as 'simplified_end_time',
                    IIF(tsd.test_skill_sub_type_id = (SELECT id FROM {db_name}..cms_models_testskillsledesc tssle WHERE tssle.codename = 'oe'),  1, 2) as 'language_id',
                    count(*) as 'nbr_of_sessions'
                FROM {db_name}..custom_models_testcentertestsessions ts
                JOIN {db_name}..custom_models_testcentertestsessiondata tsd on tsd.id = ts.test_session_data_id
                -- ONLY GETTING OLA TEST SESSIONS
                WHERE tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype tst WHERE tst.codename = 'sle')
                AND tsd.test_skill_sub_type_id IN (SELECT id FROM {db_name}..cms_models_testskillsledesc tssle WHERE tssle.codename IN ('oe', 'of'))
                GROUP BY tsd.date, tsd.start_time, tsd.end_time, tsd.test_skill_sub_type_id
                ORDER BY tsd.date, tsd.start_time
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute("DROP VIEW IF EXISTS [dbo].[scorer_ola_tests_to_assess_vw]")


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0342_testcentertestsessionsvw_test_skill_sub_type_codename",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
