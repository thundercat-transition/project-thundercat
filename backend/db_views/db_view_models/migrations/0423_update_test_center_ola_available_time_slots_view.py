from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER FUNCTION get_test_assessor_availabilities_by_date_fn (@date date, @test_center_id int)
                RETURNS TABLE
                AS RETURN
                SELECT
					TOP 100 PERCENT
					tcts.start_time,
					tcts.end_time,
					tcts.day_of_week_id,
					tcts.assessed_language_id,
                    -- AVAILABLE ASSESSORS - UNAVAILABLE ASSESSORS - UNAVAILABLE ASSESSORS DUE TO ACCOMMODATIONS
					SUM(available_assessors.count) - SUM(unavailable_assessors.count) - SUM(unavailable_assessors_due_to_accommodations.count) as 'nbr_of_available_assessors'
				FROM {db_name}..custom_models_testcenterolatimeslot tcts
				JOIN {db_name}..custom_models_testcenter tc on tc.id = tcts.test_center_id
				-- GETTING NUMBER OF AVAILABLE ASSESSORS
				OUTER APPLY (SELECT COUNT(*) as 'count' 
							 FROM {db_name}..custom_models_testcenterolatimeslotassessoravailability tcoaa 
							 WHERE tcoaa.test_center_ola_time_slot_id = tcts.id
							) available_assessors
				-- GETTING NUMBER OF UNAVAILABLE ASSESSORS
				OUTER APPLY (SELECT COUNT(* ) as 'count'
							 FROM {db_name}..custom_models_testcenterolaassessorunavailability tcoau 
							 WHERE tcoau.test_center_ola_test_assessor_id IN (SELECT tcoaa_2.test_center_ola_test_assessor_id 
																			  FROM {db_name}..custom_models_testcenterolatimeslotassessoravailability tcoaa_2 
																			  WHERE tcoaa_2.test_center_ola_time_slot_id = tcts.id
																			 )
							 AND tcoau.start_date <= @date
							 AND tcoau.end_date >= @date
							 ) unavailable_assessors
				-- GETTING NUMBER OF UNAVAILABLE ASSESSORS DUE TO ACCOMMODATIONS
				OUTER APPLY (SELECT COUNT(*) as 'count' 
							 FROM {db_name}..custom_models_testcentertestsessions ts
							 JOIN {db_name}..custom_models_testcentertestsessiondata tsd
							 ON tsd.id = ts.test_session_data_id
							 WHERE ts.is_standard = 0
							 -- tsd.test_assessor_user_id IS A USER_ID NOT A test_center_ola_test_assessor_id
							 AND tsd.test_assessor_user_id IN (SELECT tcota.user_id
															   FROM {db_name}..custom_models_testcenterolatestassessor tcota
															   WHERE id IN (SELECT tcoaa_3.test_center_ola_test_assessor_id 
																			FROM {db_name}..custom_models_testcenterolatimeslotassessoravailability tcoaa_3 
																			WHERE tcoaa_3.test_center_ola_time_slot_id = tcts.id
																		   )
															  )
							 AND (CAST(tsd.start_time as TIME) < tcts.end_time AND CAST(tsd.end_time as TIME) > tcts.start_time)
							) unavailable_assessors_due_to_accommodations
				-- ONLY CONSIDERING RESPECTIVE AUTHORIZED OLA TEST CENTERS WHERE DAY OF WEEK MATCHES THE PROVIDED DATE
				WHERE tc.ola_authorized = 1 AND tcts.test_center_id = @test_center_id AND tcts.day_of_week_id = DATEPART(dw, @date)
				GROUP BY tcts.start_time, tcts.end_time, tcts.day_of_week_id, tcts.assessed_language_id
				ORDER BY tcts.day_of_week_id
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )

        cursor.execute(
            """CREATE OR ALTER FUNCTION get_nbr_of_reserved_test_assessors_by_date_fn (@date date, @language_id int, @reason_for_testing_priority_codename varchar(50))
                RETURNS TABLE
                AS RETURN
                SELECT
                    TOP 100 PERCENT
                    COUNT(ts.id) as 'nbr_of_reserved_assessors'
                FROM {db_name}..custom_models_testcentertestsessions ts
                JOIN {db_name}..custom_models_testcentertestsessiondata tsd on tsd.id = ts.test_session_data_id
				JOIN {db_name}..cms_models_testskillsledesc sle_desc on sle_desc.id = tsd.test_skill_sub_type_id
				JOIN {db_name}..custom_models_assessmentprocessassignedtestspecs apats on apats.id = ts.assessment_process_assigned_test_specs_id
				JOIN {db_name}..custom_models_reasonfortesting rft on rft.id = apats.reason_for_testing_id
				JOIN {db_name}..custom_models_reasonfortestingpriority rftp on rftp.id = rft.reason_for_testing_priority_id
                -- MATCHING PROVIDED DATE
				WHERE tsd.date = @date 
				-- MATCHING TEST SKILL SUB TYPE ID
				AND tsd.test_skill_sub_type_id = IIF(@language_id = 1, (SELECT id FROM {db_name}..cms_models_testskillsledesc WHERE codename = 'oe'), (SELECT id FROM {db_name}..cms_models_testskillsledesc WHERE codename = 'of'))
				-- MATCHING PROVIDED RESON FOR TESTING
				AND rftp.codename = @reason_for_testing_priority_codename
				-- ONLY CONSIDERING STANDARD SESSIONS
				AND ts.is_standard = 1
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0422_update_scorer_ola_detailed_tests_to_assess_view",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
