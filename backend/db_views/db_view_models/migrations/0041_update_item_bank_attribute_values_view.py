from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """ALTER VIEW [dbo].[item_bank_attribute_values_vw] AS
                SELECT 
                    av.id as 'item_bank_attribute_value_id', 
                    av.attribute_value_order,
                    av.item_bank_attribute_id,
                    at_e.text as 'attribute_text_en',
	                at_f.text as 'attribute_text_fr',
                    av.attribute_value_type,
                    avt.id as 'item_bank_attribute_values_text_id',
                    avt.text
                FROM {db_name}..cms_models_itembankattributevalues av 
                JOIN {db_name}..cms_models_itembankattributevaluestext avt ON avt.item_bank_attribute_values_id = av.id
                JOIN {db_name}..cms_models_itembankattributestext at_e on at_e.item_bank_attribute_id = av.item_bank_attribute_id AND at_e.language_id = 1
                JOIN {db_name}..cms_models_itembankattributestext at_f on at_f.item_bank_attribute_id = av.item_bank_attribute_id AND at_f.language_id = 2
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """ALTER VIEW [dbo].[item_bank_attribute_values_vw] AS
                SELECT 
                    av.id as 'item_bank_attribute_value_id', 
                    av.attribute_value_order,
                    av.item_bank_attribute_id,
                    av.attribute_value_type,
                    avt.id as 'item_bank_attribute_values_text_id',
                    avt.text
                FROM {db_name}..cms_models_itembankattributevalues av 
                JOIN {db_name}..cms_models_itembankattributevaluestext avt ON avt.item_bank_attribute_values_id = av.id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0040_itembankattributevaluesvw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
