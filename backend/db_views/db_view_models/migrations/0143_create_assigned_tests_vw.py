from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[assigned_tests_vw] AS
    #             SELECT
    #                 at.id,
    #                 ats1.id as 'status_id',
    #                 ats1.codename as 'status_codename',
    #                 ats2.id as 'previous_status_id',
    #                 ats2.codename as 'previous_status_codename',
    #                 at.start_date,
    #                 at.modify_date,
    #                 at.submit_date,
    #                 at.test_access_code,
    #                 at.total_score,
    #                 at.test_id,
    #                 at.ta_id,
    #                 at.test_section_id,
    #                 at.test_session_language_id,
    #                 at.username_id,
    #                 at.test_order_number,
    #                 at.en_converted_score,
    #                 at.fr_converted_score,
    #                 at.is_invalid,
    #                 at.uit_invite_id,
    #                 at.orderless_financial_data_id,
    #                 at.accommodation_request_id,
    #                 at.test_session_id
    #             FROM {db_name}..custom_models_assignedtest at
    #             LEFT JOIN {db_name}..custom_models_assignedteststatus ats1 on ats1.id = at.status_id
    #             LEFT JOIN {db_name}..custom_models_assignedteststatus ats2 on ats2.id = at.previous_status_id
    #         """.format(
    #             db_name=settings.DATABASES["default"]["NAME"]
    #         )
    #     )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0142_update_active_tests_vw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
