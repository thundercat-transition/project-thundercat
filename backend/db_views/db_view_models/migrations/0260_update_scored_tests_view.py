from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[scored_tests_vw] AS
                SELECT
                    at.id,
                    at.start_date,
                    CONVERT(VARCHAR(10), at.start_date, 23) as 'simplified_start_date',
                    at.modify_date,
                    at.submit_date,
                    at.test_access_code,
                    at.total_score,
                    td.id as 'test_id',
                    td.parent_code,
                    td.test_code,
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    td.retest_period,
                    td.show_score,
                    td.show_result,
                    ts.test_skill_type_id,
                    type_e.text as 'test_skill_type_name_en',
                    type_f.text as 'test_skill_type_name_fr',
                    test_skill_sub_type_data.id as 'test_skill_sub_type_id',
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return NULL
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'sle')
                                                    -- Getting sle description text (EN)
                                                    THEN (SELECT text 
                                                            FROM {db_name}..cms_models_testskillsledesctext 
                                                            WHERE language_id = 1 AND test_skill_sle_desc_id = (SELECT id 
                                                                                                                FROM {db_name}..cms_models_testskillsledesc 
                                                                                                                WHERE id = tssle.test_skill_sle_desc_id))
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'occupational')
                                                    -- Getting occupational description text (EN)
                                                    THEN (SELECT text 
                                                            FROM {db_name}..cms_models_testskilloccupationaldesctext
                                                            WHERE language_id = 1 AND test_skill_occupational_desc_id = (SELECT id 
                                                                                                                        FROM {db_name}..cms_models_testskilloccupationaldesc 
                                                                                                                        WHERE id = tsocc.test_skill_occupational_desc_id))
                    END as 'test_skill_sub_type_name_en',
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return None
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'sle')
                                                    -- Getting sle description text (FR)
                                                    THEN (SELECT text 
                                                            FROM {db_name}..cms_models_testskillsledesctext 
                                                            WHERE language_id = 2 AND test_skill_sle_desc_id = (SELECT id 
                                                                                                                FROM {db_name}..cms_models_testskillsledesc 
                                                                                                                WHERE id = tssle.test_skill_sle_desc_id))
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    -- Getting occupational description text (FR)
                                                    THEN (SELECT text 
                                                            FROM {db_name}..cms_models_testskilloccupationaldesctext
                                                            WHERE language_id = 2 AND test_skill_occupational_desc_id = (SELECT id 
                                                                                                                        FROM {db_name}..cms_models_testskilloccupationaldesc 
                                                                                                                        WHERE id = tsocc.test_skill_occupational_desc_id))
                    END as 'test_skill_sub_type_name_fr',
                    at.test_session_language_id,
                    at.test_order_number,
                    at.en_converted_score,
                    at.fr_converted_score,
                    at.is_invalid,
                    at.uit_invite_id,
                    at.orderless_financial_data_id,
                    at.accommodation_request_id,
                    at.test_session_id,
                    ats.id as 'status_id',
                    ats.codename as 'status_codename',
                    at.ta_user_id,
                    u.id as 'candidate_user_id',
                    u.email as 'candidate_email',
                    u.first_name as 'candidate_first_name',
                    u.last_name as 'candidate_last_name',
                    u.birth_date as 'candidate_dob',
                    sm.validity_period,
                    CASE
                        -- Scoring Method NONE ==> return 0
                        WHEN sm.method_type_id = (SELECT id FROM {db_name}..cms_models_scoringmethodtypes WHERE method_type_codename = 'NONE') THEN 0
                        -- Scoring method RAW ==> retuning respective boolean value from table
                        WHEN sm.method_type_id = (SELECT id FROM {db_name}..cms_models_scoringmethodtypes WHERE method_type_codename = 'RAW') THEN (SELECT result_valid_indefinitely FROM {db_name}..cms_models_scoringraw WHERE scoring_method_id = sm.id)
                        -- Scoring method PASS/FAIL ==> retuning respective boolean value from table
                        WHEN sm.method_type_id = (SELECT id FROM {db_name}..cms_models_scoringmethodtypes WHERE method_type_codename = 'PASS_FAIL') THEN (SELECT result_valid_indefinitely FROM {db_name}..cms_models_scoringpassfail WHERE scoring_method_id = sm.id)
                        -- Scoring method THRESHOLD ==> retuning respective boolean value from table
                        WHEN sm.method_type_id = (SELECT id FROM {db_name}..cms_models_scoringmethodtypes WHERE method_type_codename = 'THRESHOLD') THEN (SELECT result_valid_indefinitely FROM {db_name}..cms_models_scoringthreshold WHERE scoring_method_id = sm.id AND en_conversion_value = at.en_converted_score)
                    END as 'result_valid_indefinitely',
                    at.score_valid_until,
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return NULL
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'sle') 
                                                    -- return respective score_valid_until (if defined)
                                                    THEN IIF(calculated_sle_tests_score_validity.id = at.id, calculated_sle_tests_score_validity.score_valid_until, NULL)
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    -- return respective score_valid_until (if defined)
                                                    THEN IIF(calculated_occ_tests_score_validity.id = at.id, calculated_occ_tests_score_validity.score_valid_until, NULL)
                    END as 'calculated_score_valid_until',
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return NULL
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'sle') 
                                                    -- Return 1 if calculated_sle_tests_score_validity is defined, otherwise return 0
                                                    THEN IIF(calculated_sle_tests_score_validity.id = at.id, 1, 0)
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    -- Return 1 if calculated_occ_tests_score_validity is defined, otherwise return 0
                                                    THEN IIF(calculated_occ_tests_score_validity.id = at.id, 1, 0)
                    END as 'is_most_recent_valid_test',
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return NULL
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'sle') 
                                                    -- Return respective retest_period
                                                    THEN calculated_sle_tests_retest_data.retest_period
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    -- Return respective retest_period
                                                    THEN calculated_occ_tests_retest_data.retest_period
                    END as 'most_updated_retest_period',
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return NULL
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'sle') 
                                                    -- Return submit_date + respective retest_period (in days) (YYYY-MM-DD)
                                                    THEN CONVERT(VARCHAR(10), DATEADD(DAY, calculated_sle_tests_retest_data.retest_period, calculated_sle_tests_retest_data.submit_date), 23)
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    -- Return submit_date + respective retest_period (in days) (YYYY-MM-DD)
                                                    THEN CONVERT(VARCHAR(10), DATEADD(DAY, calculated_occ_tests_retest_data.retest_period, calculated_occ_tests_retest_data.submit_date), 23)
                    END as 'calculated_retest_period_date',
                    CASE
                        -- test has been quit
                        WHEN at.status_id = (SELECT id FROM {db_name}..custom_models_assignedteststatus WHERE codename = 'quit') THEN 0
                        -- if supervised test ==> test must have been submitted at least two days ago to be displayed
                        WHEN at.uit_invite_id is NULL AND at.submit_date <= DATEADD(DAY, -2, GETUTCDATE()) THEN 0
                        -- if unsupervised test ==> current date (UTC) must be greater than two days after the validity end date of respective uit invite
                        WHEN at.uit_invite_id is not NULL AND CONVERT(VARCHAR(10), GETUTCDATE(), 23) > (SELECT DATEADD(DAY, 2, validity_end_date) FROM {db_name}..custom_models_uitinvites WHERE id = at.uit_invite_id) THEN 0
                    ELSE
                        1
                    END as 'pending_score'
                FROM {db_name}..custom_models_assignedtest at
                JOIN {db_name}..custom_models_assignedteststatus ats on ats.id = at.status_id
                JOIN {db_name}..user_management_models_user u on u.id = at.user_id
                JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
                JOIN {db_name}..cms_models_scoringmethods sm on sm.test_id = at.test_id
                JOIN {db_name}..cms_models_testskill ts on ts.test_definition_id = at.test_id
                JOIN {db_name}..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM {db_name}..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM {db_name}..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN {db_name}..cms_models_testskillsle tssle on tssle.test_skill_id = ts.id
                LEFT JOIN {db_name}..cms_models_testskilloccupational tsocc on tsocc.test_skill_id = ts.id
                OUTER APPLY (SELECT (CASE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    THEN NULL
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'sle') 
                                                    THEN tssle.test_skill_sle_desc_id
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    THEN tsocc.test_skill_occupational_desc_id
                    END) as 'id') test_skill_sub_type_data
                -- calculated_sle_tests_score_validity
                OUTER APPLY (SELECT TOP 1 at2.id, at2.score_valid_until
                            FROM custom_models_assignedtest at2
                            WHERE at2.is_invalid = 0
                            AND at2.user_id = u.id AND at2.test_id in (SELECT test_definition_id 
                                                                        FROM cms_models_testskill ts2 
                                                                        WHERE ts2.id in (SELECT tssle2.test_skill_id 
                                                                                        FROM cms_models_testskillsle tssle2 
                                                                                        WHERE tssle2.test_skill_id in (SELECT ts3.id 
                                                                                                                        FROM cms_models_testskill ts3
                                                                                                                        WHERE ts3.test_skill_type_id = ts.test_skill_type_id) 
                                                                                            AND tssle2.test_skill_sle_desc_id = test_skill_sub_type_data.id)) ORDER BY at2.submit_date DESC) calculated_sle_tests_score_validity
                -- calculated_occ_tests_score_validity
                OUTER APPLY (SELECT TOP 1 at2.id, at2.score_valid_until
                            FROM custom_models_assignedtest at2
                            WHERE at2.is_invalid = 0
                            AND at2.user_id = u.id AND at2.test_id in (SELECT test_definition_id 
                                                                        FROM cms_models_testskill ts2 
                                                                        WHERE ts2.id in (SELECT tsocc2.test_skill_id 
                                                                                        FROM cms_models_testskilloccupational tsocc2 
                                                                                        WHERE tsocc2.test_skill_id in (SELECT ts3.id 
                                                                                                                        FROM cms_models_testskill ts3
                                                                                                                        WHERE ts3.test_skill_type_id = ts.test_skill_type_id) 
                                                                                            AND tsocc2.test_skill_occupational_desc_id = test_skill_sub_type_data.id)) ORDER BY at2.submit_date DESC) calculated_occ_tests_score_validity
                -- calculated_sle_tests_retest_data
                OUTER APPLY (SELECT TOP 1 td2.retest_period, at2.submit_date
                            FROM custom_models_assignedtest at2
                            JOIN {db_name}..cms_models_testdefinition td2 on td2.id = at2.test_id
                            WHERE at2.user_id = u.id AND at2.test_id in (SELECT test_definition_id 
                                                                        FROM cms_models_testskill ts2 
                                                                        WHERE ts2.id in (SELECT tssle2.test_skill_id 
                                                                                        FROM cms_models_testskillsle tssle2 
                                                                                        WHERE tssle2.test_skill_id in (SELECT ts3.id 
                                                                                                                        FROM cms_models_testskill ts3
                                                                                                                        WHERE ts3.test_skill_type_id = ts.test_skill_type_id) 
                                                                                            AND tssle2.test_skill_sle_desc_id = test_skill_sub_type_data.id)) ORDER BY at2.submit_date DESC) calculated_sle_tests_retest_data
                -- calculated_occ_tests_retest_data
                OUTER APPLY (SELECT TOP 1 td2.retest_period, at2.submit_date
                            FROM custom_models_assignedtest at2
                            JOIN {db_name}..cms_models_testdefinition td2 on td2.id = at2.test_id
                            WHERE at2.user_id = u.id AND at2.test_id in (SELECT test_definition_id 
                                                                        FROM cms_models_testskill ts2 
                                                                        WHERE ts2.id in (SELECT tsocc2.test_skill_id 
                                                                                        FROM cms_models_testskilloccupational tsocc2 
                                                                                        WHERE tsocc2.test_skill_id in (SELECT ts3.id 
                                                                                                                        FROM cms_models_testskill ts3
                                                                                                                        WHERE ts3.test_skill_type_id = ts.test_skill_type_id) 
                                                                                            AND tsocc2.test_skill_occupational_desc_id = test_skill_sub_type_data.id)) ORDER BY at2.submit_date DESC) calculated_occ_tests_retest_data
                -- status must be submitted or quit
                WHERE at.status_id in (SELECT id 
                                    FROM {db_name}..custom_models_assignedteststatus 
                                    WHERE codename in ('submitted', 'quit'))
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[scored_tests_vw] AS
                SELECT
                    at.id,
                    at.start_date,
                    CONVERT(VARCHAR(10), at.start_date, 23) as 'simplified_start_date',
                    at.modify_date,
                    at.submit_date,
                    at.test_access_code,
                    at.total_score,
                    td.id as 'test_id',
                    td.parent_code,
                    td.test_code,
                    td.en_name as 'test_name_en',
                    td.fr_name as 'test_name_fr',
                    td.retest_period,
                    td.show_score,
                    td.show_result,
                    ts.test_skill_type_id,
                    type_e.text as 'test_skill_type_name_en',
                    type_f.text as 'test_skill_type_name_fr',
                    test_skill_sub_type_data.id as 'test_skill_sub_type_id',
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return NULL
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'sle')
                                                    -- Getting sle description text (EN)
                                                    THEN (SELECT text 
                                                            FROM {db_name}..cms_models_testskillsledesctext 
                                                            WHERE language_id = 1 AND test_skill_sle_desc_id = (SELECT id 
                                                                                                                FROM {db_name}..cms_models_testskillsledesc 
                                                                                                                WHERE id = tssle.test_skill_sle_desc_id))
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'occupational')
                                                    -- Getting occupational description text (EN)
                                                    THEN (SELECT text 
                                                            FROM {db_name}..cms_models_testskilloccupationaldesctext
                                                            WHERE language_id = 1 AND test_skill_occupational_desc_id = (SELECT id 
                                                                                                                        FROM {db_name}..cms_models_testskilloccupationaldesc 
                                                                                                                        WHERE id = tsocc.test_skill_occupational_desc_id))
                    END as 'test_skill_sub_type_name_en',
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return None
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'sle')
                                                    -- Getting sle description text (FR)
                                                    THEN (SELECT text 
                                                            FROM {db_name}..cms_models_testskillsledesctext 
                                                            WHERE language_id = 2 AND test_skill_sle_desc_id = (SELECT id 
                                                                                                                FROM {db_name}..cms_models_testskillsledesc 
                                                                                                                WHERE id = tssle.test_skill_sle_desc_id))
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    -- Getting occupational description text (FR)
                                                    THEN (SELECT text 
                                                            FROM {db_name}..cms_models_testskilloccupationaldesctext
                                                            WHERE language_id = 2 AND test_skill_occupational_desc_id = (SELECT id 
                                                                                                                        FROM {db_name}..cms_models_testskilloccupationaldesc 
                                                                                                                        WHERE id = tsocc.test_skill_occupational_desc_id))
                    END as 'test_skill_sub_type_name_fr',
                    at.test_session_language_id,
                    at.test_order_number,
                    at.en_converted_score,
                    at.fr_converted_score,
                    at.is_invalid,
                    at.uit_invite_id,
                    at.orderless_financial_data_id,
                    at.accommodation_request_id,
                    at.test_session_id,
                    ats.id as 'status_id',
                    ats.codename as 'status_codename',
                    at.ta_user_id,
                    u.id as 'candidate_user_id',
                    u.email as 'candidate_email',
                    u.first_name as 'candidate_first_name',
                    u.last_name as 'candidate_last_name',
                    u.birth_date as 'candidate_dob',
                    sm.validity_period,
                    CASE
                        -- Scoring Method NONE ==> return 0
                        WHEN sm.method_type_id = (SELECT id FROM {db_name}..cms_models_scoringmethodtypes WHERE method_type_codename = 'NONE') THEN 0
                        -- Scoring method RAW ==> retuning respective boolean value from table
                        WHEN sm.method_type_id = (SELECT id FROM {db_name}..cms_models_scoringmethodtypes WHERE method_type_codename = 'RAW') THEN (SELECT result_valid_indefinitely FROM {db_name}..cms_models_scoringraw WHERE scoring_method_id = sm.id)
                        -- Scoring method PASS/FAIL ==> retuning respective boolean value from table
                        WHEN sm.method_type_id = (SELECT id FROM {db_name}..cms_models_scoringmethodtypes WHERE method_type_codename = 'PASS_FAIL') THEN (SELECT result_valid_indefinitely FROM {db_name}..cms_models_scoringpassfail WHERE scoring_method_id = sm.id)
                        -- Scoring method THRESHOLD ==> retuning respective boolean value from table
                        WHEN sm.method_type_id = (SELECT id FROM {db_name}..cms_models_scoringmethodtypes WHERE method_type_codename = 'THRESHOLD') THEN (SELECT result_valid_indefinitely FROM {db_name}..cms_models_scoringthreshold WHERE scoring_method_id = sm.id AND en_conversion_value = at.en_converted_score)
                    END as 'result_valid_indefinitely',
                    at.score_valid_until,
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return NULL
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'sle') 
                                                    -- return respective score_valid_until (if defined)
                                                    THEN IIF(calculated_sle_tests_score_validity.id = at.id, calculated_sle_tests_score_validity.score_valid_until, NULL)
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    -- return respective score_valid_until (if defined)
                                                    THEN IIF(calculated_occ_tests_score_validity.id = at.id, calculated_occ_tests_score_validity.score_valid_until, NULL)
                    END as 'calculated_score_valid_until',
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return NULL
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'sle') 
                                                    -- Return 1 if calculated_sle_tests_score_validity is defined, otherwise return 0
                                                    THEN IIF(calculated_sle_tests_score_validity.id = at.id, 1, 0)
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    -- Return 1 if calculated_occ_tests_score_validity is defined, otherwise return 0
                                                    THEN IIF(calculated_occ_tests_score_validity.id = at.id, 1, 0)
                    END as 'is_most_recent_valid_test',
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return NULL
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'sle') 
                                                    -- Return respective retest_period
                                                    THEN calculated_sle_tests_retest_data.retest_period
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    -- Return respective retest_period
                                                    THEN calculated_occ_tests_retest_data.retest_period
                    END as 'most_updated_retest_period',
                    CASE
                        -- Skill Type: None
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    -- Return NULL
                                                    THEN NULL
                        -- Skill Type: SLE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'sle') 
                                                    -- Return submit_date + respective retest_period (in days) + 1 (YYYY-MM-DD)
                                                    THEN CONVERT(VARCHAR(10), DATEADD(DAY, calculated_sle_tests_retest_data.retest_period + 1, calculated_sle_tests_retest_data.submit_date), 23)
                        -- Skill Type: Occupational
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    -- Return submit_date + respective retest_period (in days) + 1 (YYYY-MM-DD)
                                                    THEN CONVERT(VARCHAR(10), DATEADD(DAY, calculated_occ_tests_retest_data.retest_period + 1, calculated_occ_tests_retest_data.submit_date), 23)
                    END as 'calculated_retest_period_date',
                    CASE
                        -- test has been quit
                        WHEN at.status_id = (SELECT id FROM {db_name}..custom_models_assignedteststatus WHERE codename = 'quit') THEN 0
                        -- if supervised test ==> test must have been submitted at least two days ago to be displayed
                        WHEN at.uit_invite_id is NULL AND at.submit_date <= DATEADD(DAY, -2, GETUTCDATE()) THEN 0
                        -- if unsupervised test ==> current date (UTC) must be greater than two days after the validity end date of respective uit invite
                        WHEN at.uit_invite_id is not NULL AND CONVERT(VARCHAR(10), GETUTCDATE(), 23) > (SELECT DATEADD(DAY, 2, validity_end_date) FROM {db_name}..custom_models_uitinvites WHERE id = at.uit_invite_id) THEN 0
                    ELSE
                        1
                    END as 'pending_score'
                FROM {db_name}..custom_models_assignedtest at
                JOIN {db_name}..custom_models_assignedteststatus ats on ats.id = at.status_id
                JOIN {db_name}..user_management_models_user u on u.id = at.user_id
                JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
                JOIN {db_name}..cms_models_scoringmethods sm on sm.test_id = at.test_id
                JOIN {db_name}..cms_models_testskill ts on ts.test_definition_id = at.test_id
                JOIN {db_name}..cms_models_testskilltype tst on tst.id = ts.test_skill_type_id
                OUTER APPLY (SELECT TOP 1 tstte.text FROM {db_name}..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
                OUTER APPLY (SELECT TOP 1 tsttf.text FROM {db_name}..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
                LEFT JOIN {db_name}..cms_models_testskillsle tssle on tssle.test_skill_id = ts.id
                LEFT JOIN {db_name}..cms_models_testskilloccupational tsocc on tsocc.test_skill_id = ts.id
                OUTER APPLY (SELECT (CASE
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'none') 
                                                    THEN NULL
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'sle') 
                                                    THEN tssle.test_skill_sle_desc_id
                        WHEN ts.test_skill_type_id = (SELECT id 
                                                    FROM {db_name}..cms_models_testskilltype 
                                                    WHERE codename = 'occupational') 
                                                    THEN tsocc.test_skill_occupational_desc_id
                    END) as 'id') test_skill_sub_type_data
                -- calculated_sle_tests_score_validity
                OUTER APPLY (SELECT TOP 1 at2.id, at2.score_valid_until
                            FROM custom_models_assignedtest at2
                            WHERE at2.is_invalid = 0
                            AND at2.user_id = u.id AND at2.test_id in (SELECT test_definition_id 
                                                                        FROM cms_models_testskill ts2 
                                                                        WHERE ts2.id in (SELECT tssle2.test_skill_id 
                                                                                        FROM cms_models_testskillsle tssle2 
                                                                                        WHERE tssle2.test_skill_id in (SELECT ts3.id 
                                                                                                                        FROM cms_models_testskill ts3
                                                                                                                        WHERE ts3.test_skill_type_id = ts.test_skill_type_id) 
                                                                                            AND tssle2.test_skill_sle_desc_id = test_skill_sub_type_data.id)) ORDER BY at2.submit_date DESC) calculated_sle_tests_score_validity
                -- calculated_occ_tests_score_validity
                OUTER APPLY (SELECT TOP 1 at2.id, at2.score_valid_until
                            FROM custom_models_assignedtest at2
                            WHERE at2.is_invalid = 0
                            AND at2.user_id = u.id AND at2.test_id in (SELECT test_definition_id 
                                                                        FROM cms_models_testskill ts2 
                                                                        WHERE ts2.id in (SELECT tsocc2.test_skill_id 
                                                                                        FROM cms_models_testskilloccupational tsocc2 
                                                                                        WHERE tsocc2.test_skill_id in (SELECT ts3.id 
                                                                                                                        FROM cms_models_testskill ts3
                                                                                                                        WHERE ts3.test_skill_type_id = ts.test_skill_type_id) 
                                                                                            AND tsocc2.test_skill_occupational_desc_id = test_skill_sub_type_data.id)) ORDER BY at2.submit_date DESC) calculated_occ_tests_score_validity
                -- calculated_sle_tests_retest_data
                OUTER APPLY (SELECT TOP 1 td2.retest_period, at2.submit_date
                            FROM custom_models_assignedtest at2
                            JOIN {db_name}..cms_models_testdefinition td2 on td2.id = at2.test_id
                            WHERE at2.user_id = u.id AND at2.test_id in (SELECT test_definition_id 
                                                                        FROM cms_models_testskill ts2 
                                                                        WHERE ts2.id in (SELECT tssle2.test_skill_id 
                                                                                        FROM cms_models_testskillsle tssle2 
                                                                                        WHERE tssle2.test_skill_id in (SELECT ts3.id 
                                                                                                                        FROM cms_models_testskill ts3
                                                                                                                        WHERE ts3.test_skill_type_id = ts.test_skill_type_id) 
                                                                                            AND tssle2.test_skill_sle_desc_id = test_skill_sub_type_data.id)) ORDER BY at2.submit_date DESC) calculated_sle_tests_retest_data
                -- calculated_occ_tests_retest_data
                OUTER APPLY (SELECT TOP 1 td2.retest_period, at2.submit_date
                            FROM custom_models_assignedtest at2
                            JOIN {db_name}..cms_models_testdefinition td2 on td2.id = at2.test_id
                            WHERE at2.user_id = u.id AND at2.test_id in (SELECT test_definition_id 
                                                                        FROM cms_models_testskill ts2 
                                                                        WHERE ts2.id in (SELECT tsocc2.test_skill_id 
                                                                                        FROM cms_models_testskilloccupational tsocc2 
                                                                                        WHERE tsocc2.test_skill_id in (SELECT ts3.id 
                                                                                                                        FROM cms_models_testskill ts3
                                                                                                                        WHERE ts3.test_skill_type_id = ts.test_skill_type_id) 
                                                                                            AND tsocc2.test_skill_occupational_desc_id = test_skill_sub_type_data.id)) ORDER BY at2.submit_date DESC) calculated_occ_tests_retest_data
                -- status must be submitted or quit
                WHERE at.status_id in (SELECT id 
                                    FROM {db_name}..custom_models_assignedteststatus 
                                    WHERE codename in ('submitted', 'quit'))
    """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0259_selecteduseraccommodationfilevw_rationale",
        )
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
