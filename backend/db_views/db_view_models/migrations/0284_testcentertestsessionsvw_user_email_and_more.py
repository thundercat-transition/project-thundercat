# Generated by Django 4.2.11 on 2024-07-17 14:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("db_view_models", "0283_update_test_center_test_sessions_vw"),
    ]

    operations = [
        migrations.AddField(
            model_name="testcentertestsessionsvw",
            name="user_email",
            field=models.CharField(
                db_column="USER_EMAIL", default="temp", max_length=254
            ),
        ),
        migrations.AddField(
            model_name="testcentertestsessionsvw",
            name="user_first_name",
            field=models.CharField(
                db_column="USER_FIRST_NAME", default="temp", max_length=30
            ),
        ),
        migrations.AddField(
            model_name="testcentertestsessionsvw",
            name="user_id",
            field=models.IntegerField(db_column="USER_ID", default=1),
        ),
        migrations.AddField(
            model_name="testcentertestsessionsvw",
            name="user_last_name",
            field=models.CharField(
                db_column="USER_LAST_NAME", default="temp", max_length=150
            ),
        ),
        migrations.AddField(
            model_name="testcentertestsessionsvw",
            name="user_username",
            field=models.CharField(
                db_column="USER_USERNAME", default="temp", max_length=254
            ),
        ),
    ]
