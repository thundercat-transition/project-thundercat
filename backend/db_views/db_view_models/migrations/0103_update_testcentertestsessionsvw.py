from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("db_view_models", "0102_test_center_test_sessions_vw_extra_columns"),
    ]

    operations = [
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="spaces_booked",
            field=models.IntegerField(db_column="SPACES_BOOKED"),
        ),
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="spaces_unbooked",
            field=models.IntegerField(db_column="SPACES_UNBOOKED"),
        ),
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="latest_booking_modification_time",
            field=models.IntegerField(db_column="LATEST_BOOKING_MODIFICATION_TIME"),
        ),
        migrations.AddField(
            model_name="TestCenterTestSessionsVW",
            name="booking_delay",
            field=models.IntegerField(db_column="BOOKING_DELAY"),
        ),
    ]
