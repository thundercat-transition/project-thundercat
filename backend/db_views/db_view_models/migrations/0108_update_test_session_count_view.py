from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_session_count_vw] AS
                SELECT
                    tcts.id,
                    (SELECT COUNT(*) FROM {db_name}..custom_models_consumedreservationcodes WHERE test_session_id=tcts.id) as count,
                    tctsd.spaces_available as max
                    FROM {db_name}..custom_models_testcentertestsessions tcts
                    JOIN {db_name}..custom_models_testcentertestsessiondata tctsd on tctsd.id = tcts.test_session_data_id
            """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #             """CREATE OR ALTER VIEW [dbo].[test_session_count_vw] AS
    #                 SELECT

    #         tctsd.id,
    #         (SELECT COUNT(*) FROM {db_name}..custom_models_consumedreservationcodes WHERE test_center_test_session_data_id=tctsd.id) as count,
    #         tctsd.spaces_available as max
    #         FROM {db_name}..custom_models_testcentertestsessiondata tctsd""".format(
    #         db_name=settings.DATABASES["default"]["NAME"]
    #     )
    # )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0107_simplify_candidatereservationsvw",
        ),
        (
            "custom_models",
            "0071_remove_consumedreservationcodes_test_center_test_session_data_and_more",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
