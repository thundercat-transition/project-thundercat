from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """ALTER VIEW [dbo].[item_latest_versions_data_vw] AS


# 			SELECT
# 				i.id as 'item_id',
# 				i.system_id,
# 				i.version,
# 				v.number_of_versions as 'number_of_versions',
# 				i.item_bank_id,
# 				i.modify_date,
# 				i.last_modified_by_username_id,
# 				i.active_status,
# 				i.shuffle_options,
# 				i.item_type_id,
#                 it.codename as 'item_type_codename',
# 				ds.id as 'development_status_id',
# 				ds.codename as 'development_status_codename',
# 				ds.en_name as 'development_status_name_en',
# 				ds.fr_name as 'development_status_name_fr',
#                 IIF (i.response_format_id is not NULL, rf.id, NULL) as 'response_format_id',
# 				IIF (i.response_format_id is not NULL, rf.codename, NULL) as 'response_format_codename',
# 				IIF (i.response_format_id is not NULL, rf.en_name, NULL) as 'response_format_name_en',
# 				IIF (i.response_format_id is not NULL, rf.fr_name, NULL) as 'response_format_name_fr',
# 				IIF (hi.id is not NULL, hi.historical_id, NULL) as 'historical_id'
# 			FROM {db_name}..cms_models_items i
#             JOIN {db_name}..cms_models_itemtype it on it.id = i.item_type_id
# 			JOIN {db_name}..cms_models_itemdevelopmentstatuses ds on ds.id = i.development_status_id
# 			LEFT JOIN {db_name}..cms_models_itemresponseformats rf on rf.id = i.response_format_id
# 			LEFT JOIN {db_name}..cms_models_itemhistoricalids hi on hi.system_id = i.system_id
# 			OUTER APPLY (SELECT MAX(version) as 'number_of_versions' FROM {db_name}..cms_models_items it WHERE it.system_id = i.system_id) v
# 			WHERE i.id in (SELECT MAX(it.id) FROM {db_name}..cms_models_items it GROUP BY it.system_id)
#     """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


def rollback_changes(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """ALTER VIEW [dbo].[item_latest_versions_data_vw] AS


# 			SELECT
# 				i.id as 'item_id',
# 				i.system_id,
# 				i.version,
# 				v.number_of_versions as 'number_of_versions',
# 				i.item_bank_id,
# 				i.modify_date,
# 				i.last_modified_by_username_id,
# 				i.active_status,
# 				i.shuffle_options,
# 				ds.id as 'development_status_id',
# 				ds.codename as 'development_status_codename',
# 				ds.en_name as 'development_status_name_en',
# 				ds.fr_name as 'development_status_name_fr',
# 				rf.id as 'response_format_id',
# 				rf.codename as 'response_format_codename',
# 				rf.en_name as 'response_format_name_en',
# 				rf.fr_name as 'response_format_name_fr',
# 				IIF (hi.id is not NULL, hi.historical_id, NULL) as 'historical_id'
# 			FROM {db_name}..cms_models_items i
# 			JOIN {db_name}..cms_models_itemdevelopmentstatuses ds on ds.id = i.development_status_id
# 			JOIN {db_name}..cms_models_itemresponseformats rf on rf.id = i.response_format_id
# 			LEFT JOIN {db_name}..cms_models_itemhistoricalids hi on hi.system_id = i.system_id
# 			OUTER APPLY (SELECT MAX(version) as 'number_of_versions' FROM {db_name}..cms_models_items it WHERE it.system_id = i.system_id) v
# 			WHERE i.id in (SELECT MAX(it.id) FROM {db_name}..cms_models_items it GROUP BY it.system_id)
#     """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0065_testcentertestsessionsvw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
