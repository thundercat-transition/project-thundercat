from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[user_look_up_tests_vw] AS
    #             SELECT


# 				at.id,
# 				ats1.id as 'status_id',
# 				ats1.codename as 'status_codename',
# 				ats2.id as 'previous_status_id',
# 				ats2.codename as 'previous_status_codename',
# 				at.start_date,
# 				CONVERT(VARCHAR(10), at.start_date, 23) as 'simplified_start_date',
# 				at.modify_date,
# 				at.submit_date,
# 				CONVERT(VARCHAR(10), at.submit_date, 23) as 'simplified_submit_date',
# 				at.test_access_code,
# 				at.total_score,
# 				td.id as 'test_id',
# 				td.version as 'test_version',
# 				td.parent_code,
# 				td.test_code,
# 				td.en_name as 'test_name_en',
# 				td.fr_name as 'test_name_fr',
# 				td.retest_period,
# 				at.ta_user_id,
# 				ta.username as 'ta_username',
# 				ta.email as 'ta_email',
# 				IIF(historical_permission_data.goc_email IS NOT NULL, historical_permission_data.goc_email, permission_data.goc_email) as 'ta_goc_email',
# 				ta.first_name as 'ta_first_name',
# 				ta.last_name as 'ta_last_name',
# 				at.test_section_id,
# 				at.test_session_language_id,
# 				u.id as 'candidate_user_id',
# 				u.username as 'candidate_username',
# 				u.email as 'candidate_email',
# 				u.first_name as 'candidate_first_name',
# 				u.last_name as 'candidate_last_name',
# 				at.test_order_number,
# 				at.en_converted_score,
# 				at.fr_converted_score,
# 				at.is_invalid,
# 				IIF(at.is_invalid = 1, etta_invalidate_action.reason, NULL) as 'invalidate_test_reason',
# 				at.uit_invite_id,
# 				at.orderless_financial_data_id,
# 				at.accommodation_request_id,
# 				at.test_session_id,
# 				CASE
# 					WHEN at.orderless_financial_data_id IS NOT NULL THEN ofd.reference_number
# 					WHEN at.test_order_number IS NOT NULL THEN historical_test_permission_data.staffing_process_number
# 				ELSE
# 					NULL
# 				END as 'assessment_process_reference_nbr'
# 			FROM {db_name}..custom_models_assignedtest at
# 			JOIN {db_name}..user_management_models_user u on u.id = at.user_id
# 			JOIN {db_name}..user_management_models_user ta on ta.id = at.ta_user_id
# 			JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
# 			LEFT JOIN {db_name}..custom_models_assignedteststatus ats1 on ats1.id = at.status_id
# 			LEFT JOIN {db_name}..custom_models_assignedteststatus ats2 on ats2.id = at.previous_status_id
# 			OUTER APPLY (SELECT TOP 1 EA.action_reason AS 'reason' FROM {db_name}..custom_models_ettaactions ea WHERE ea.assigned_test_id = at.id AND ea.action_type_id = 'UN_ASSIGN_CANDIDATE' ORDER BY ea.modify_date DESC) etta_invalidate_action
# 			LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
# 			OUTER APPLY (SELECT TOP 1 htp.staffing_process_number FROM {db_name}..cms_models_historicaltestpermissions htp WHERE htp.user_id = ta.id AND htp.test_id = td.id AND htp.test_order_number = at.test_order_number AND htp.history_type = '+' ORDER BY htp.history_date DESC) historical_test_permission_data
# 			OUTER APPLY (SELECT TOP 1 hcup.goc_email FROM {db_name}..user_management_models_historicalcustomuserpermissions hcup WHERE hcup.user_id = ta.id AND hcup.permission_id = (SELECT cp.permission_id FROM {db_name}..user_management_models_custompermissions cp WHERE cp.codename = 'is_test_administrator') AND hcup.history_date < at.start_date AND hcup.history_type = '+' ORDER BY hcup.history_date DESC) historical_permission_data
# 			OUTER APPLY (SELECT TOP 1 cup.goc_email FROM {db_name}..user_management_models_customuserpermissions cup WHERE cup.user_id = ta.id AND cup.permission_id = (SELECT cp.permission_id FROM {db_name}..user_management_models_custompermissions cp WHERE cp.codename = 'is_test_administrator')) permission_data
#         """.format(
#             db_name=settings.DATABASES["default"]["NAME"]
#         )
#     )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0198_userlookuptestsvw",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
