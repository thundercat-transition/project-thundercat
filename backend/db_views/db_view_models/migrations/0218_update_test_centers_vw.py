from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE OR ALTER VIEW [dbo].[test_centers_vw] AS
                SELECT
                    tc.id,
                    tc.name,
                    tc.postal_code,
                    tc.country,
                    tc.security_email,
                    tc.booking_delay,
                    tc.modify_date,
                    tcta.number_of_tas,
                    tcr.number_of_rooms,
                    d.DEPT_ID as 'dept_id',
                    d.EABRV as 'dept_eabrv',
                    d.FABRV as 'dept_fabrv',
                    d.EDESC as 'dept_edesc',
                    d.FDESC as 'dept_fdesc'
                FROM {db_name}..custom_models_testcenter tc
                OUTER APPLY (SELECT COUNT(id) as number_of_tas FROM {db_name}..custom_models_testcentertestadministrators WHERE test_center_id = tc.id) tcta
                OUTER APPLY (SELECT COUNT(id) as number_of_rooms FROM {db_name}..custom_models_testcenterrooms WHERE test_center_id = tc.id) tcr
                LEFT JOIN {db_name}..CAT_REF_DEPARTMENTS_VW d on d.DEPT_ID = tc.department_id
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0217_assessmentprocessvw_user_email",
        ),
        ("custom_models", "0110_remove_historicaltestcenter_address_and_more"),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
