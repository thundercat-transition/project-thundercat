from django.db import connection
from django.db import migrations
from django.conf import settings


def update_vw(apps, schema_editor):
    with connection.cursor() as cursor:
        # cursor.execute(
        #     """CREATE OR ALTER VIEW [dbo].[active_tests_vw] AS
        #         SELECT
        #             at.id as 'assigned_test_id',
        #             u.first_name as 'candidate_first_name',
        #             u.last_name as 'candidate_last_name',
        #             u.username as 'candidate_username',
        #             u.email as 'candidate_email',
        #             ta.email as 'ta_id',
        #             ta.first_name as 'ta_first_name',
        #             ta.last_name as 'ta_last_name',
        #             at.status_id as 'test_status_id',
        #             atst.codename as 'test_status_codename',
        #             tp.test_order_number,
        #             ofd.reference_number,
        #             ats.time_limit,
        #             td.id as 'test_id',
        #             td.test_code
        #         FROM {db_name}..custom_models_assignedtest at
        #         JOIN {db_name}..custom_models_assignedteststatus atst on atst.id = at.status_id
        #         OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = at.username_id ORDER BY u1.history_date DESC) u
        #         OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = at.ta_id ORDER BY u1.history_date DESC) ta
        #         OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions tp1 WHERE tp1.test_order_number = at.test_order_number AND tp1.username_id = at.ta_id ORDER BY tp1.history_date DESC) tp
        #         LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
        #         OUTER APPLY (SELECT SUM(ats1.test_section_time) as 'time_limit' FROM {db_name}..custom_models_assignedtestsection ats1 WHERE ats1.assigned_test_id = at.id) ats
        #         LEFT JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
        #         WHERE at.status_id in (SELECT id FROM {db_name}..custom_models_assignedteststatus WHERE codename in ('assigned', 'checked_in', 'active', 'locked', 'paused', 'pre_test'))
        #     """.format(
        #         db_name=settings.DATABASES["default"]["NAME"]
        #     )
        # )
        # cursor.execute(
        #     """CREATE OR ALTER VIEW [dbo].[assessment_process_results_candidates_vw] AS
        #         SELECT
        #             ats.id,
        #             ats.first_name,
        #             ats.last_name,
        #             ats.email,
        #             apv.id as 'assessment_process_id',
        #             apv.reference_number as 'assessment_process_reference_number',
        #             apv.closing_date as 'assessment_process_closing_date',
        #             apv.allow_booking_external_tc as 'assessment_process_allow_booking_external_tc',
        #             apv.dept_id as 'assessment_process_dept_id ',
        #             apv.dept_eabrv as 'assessment_process_dept_eabrv',
        #             apv.dept_fabrv as 'assessment_process_dept_fabrv',
        #             apv.dept_edesc as 'assessment_process_dept_edesc',
        #             apv.dept_fdesc as 'assessment_process_dept_fdesc',
        #             bc.id as 'billing_contact_id',
        #             bc.first_name as 'billing_contact_first_name',
        #             bc.last_name as 'billing_contact_last_name ',
        #             bc.email as 'billing_contact_email',
        #             tst.id as 'test_skill_type_id',
        #             tst.codename as 'test_skill_type_codename',
        #             type_e.text as 'test_skill_type_name_en',
        #             type_f.text as 'test_skill_type_name_fr',
        #             ats.test_skill_sub_type_id as 'test_skill_sub_type_id',
        #             sled.codename as 'test_skill_sub_type_codename',
        #             CASE
        #                 WHEN tst.id = 2 THEN sub_type_e.text
        #                 ELSE
        #                     NULL
        #             END AS 'test_skill_sub_type_name_en',
        #             CASE
        #                 WHEN tst.id = 2 THEN sub_type_f.text
        #                 ELSE
        #                     NULL
        #             END AS 'test_skill_sub_type_name_fr',
        #             ats.reason_for_testing_id,
        #             rft.description_en as 'reason_for_testing_name_en',
        #             rft.description_fr as 'reason_for_testing_name_fr',
        #             ats.level_required,
        #             apv.request_sent,
        #             crc.reservation_code,
        #             crc.status_id as 'consumed_reservation_code_status_id',
        #             crc.test_session_id,
        #             crc.candidate_id,
        #             crc.assigned_test_id,
        #             at.status_id as 'assigned_test_status_id',
        #             atst.codename as 'assigned_test_status_codename',
        #             at.en_converted_score,
        #             at.fr_converted_score
        #         FROM {db_name}..custom_models_assessmentprocessassignedtestspecs ats
        #         LEFT JOIN {db_name}..custom_models_billingcontact bc on bc.id = ats.billing_contact_id
        #         JOIN {db_name}..assessment_process_vw apv on apv.id = ats.assessment_process_id
        #         JOIN {db_name}..cms_models_testskilltype tst on tst.id = ats.test_skill_type_id
        #         OUTER APPLY (SELECT TOP 1 tstte.text FROM {db_name}..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
        #         OUTER APPLY (SELECT TOP 1 tsttf.text FROM {db_name}..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
        #         LEFT JOIN {db_name}..cms_models_testskillsledesc sled on sled.id = ats.test_skill_sub_type_id
        #         OUTER APPLY (SELECT TOP 1 sledte.text FROM {db_name}..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sub_type_e
        #         OUTER APPLY (SELECT TOP 1 sledtf.text FROM {db_name}..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sub_type_f
        #         JOIN {db_name}..custom_models_reasonsfortesting rft on rft.id = ats.reason_for_testing_id
        #         LEFT JOIN {db_name}..custom_models_consumedreservationcodes crc on crc.assessment_process_assigned_test_specs_id = ats.id
        #         LEFT JOIN {db_name}..custom_models_assignedtest at on at.id = crc.assigned_test_id
        #         LEFT JOIN {db_name}..custom_models_assignedteststatus atst on atst.id = at.status_id
        #     """.format(
        #         db_name=settings.DATABASES["default"]["NAME"]
        #     )
        # )
        cursor.execute(
            """CREATE OR ALTER FUNCTION [dbo].[status2str] 
                ( 
                    @lang_code varchar(2),
                    @status_id numeric
                )
                RETURNS varchar(100)
                AS 
                BEGIN
                    DECLARE @Result		varchar(100)
                    IF (@lang_code = 'fr')
                        BEGIN
                            SELECT @Result	= CASE 
                            WHEN @status_id = 1 THEN 'ATTRIBUÉ'
                            WHEN @status_id = 2 THEN 'ENREGISTRÉ'
                            WHEN @status_id = 3 THEN 'NON ATTRIBUÉ'
                            WHEN @status_id = 4 THEN 'PRÉ_TEST'
                            WHEN @status_id = 5 THEN 'ACTIF'
                            WHEN @status_id = 6 THEN 'TRANSITION'
                            WHEN @status_id = 7 THEN 'VERROUILLÉ'
                            WHEN @status_id = 8 THEN 'EN PAUSE'
                            WHEN @status_id = 9 THEN 'SOUMIS'
                            WHEN @status_id = 10 THEN 'ABANDONNÉ'
                            ELSE CAST(@status_id AS varchar(4))
                            END
                        END
                    ELSE
                        BEGIN
                            SELECT @Result	= CASE 
                            WHEN @status_id = 1 THEN 'ASSIGNED'
                            WHEN @status_id = 2 THEN 'CHECKED_IN'
                            WHEN @status_id = 3 THEN 'UNASSIGNED'
                            WHEN @status_id = 4 THEN 'PRE_TEST'
                            WHEN @status_id = 5 THEN 'ACTIVE'
                            WHEN @status_id = 6 THEN 'TRANSITION'
                            WHEN @status_id = 7 THEN 'LOCKED'
                            WHEN @status_id = 8 THEN 'PAUSED'
                            WHEN @status_id = 9 THEN 'SUBMITTED'
                            WHEN @status_id = 10 THEN 'QUIT'

                            ELSE CAST(@status_id AS varchar(4))
                            END
                        END
                    RETURN @Result
                END"""
        )
        # cursor.execute(
        #     """CREATE OR ALTER VIEW [dbo].[financial_report_vw] AS
        #         SELECT TOP 100 percent
        #             IIF (u.id is null, v.last_name, u.last_name) as "candidate_last_name",
        #             IIF (u.id is null, v.first_name, u.first_name) as "candidate_first_name",
        #             IIF (u.id is null, v.pri, u.pri) as "candidate_pri",
        #             IIF (u.id is null, v.military_nbr, u.military_nbr) as "candidate_military_nbr",
        #             ta.goc_email as "ta_email",
        #             de.edesc as "ta_org_en",
        #             de.fdesc as "ta_org_fr",
        #             IIF (dp.edesc is null and a.orderless_financial_data_id is not null, odp.edesc, IIF(dp.edesc is null, 'Public Service Commission', dp.edesc)) as "requesting_dep_en",
        #             IIF (dp.fdesc is null and a.orderless_financial_data_id is not null, odp.fdesc, IIF(dp.fdesc is null, 'Public Service Commission', dp.fdesc)) as "requesting_dep_fr",
        #             a.test_order_number as "order_no",
        #             p.staffing_process_number as "assessment_process",
        #             o.reference_number as "reference_number",
        #             CASE
        #                 WHEN a.orderless_financial_data_id is not null THEN o.department_ministry_id
        #                 WHEN ISNUMERIC(p.department_ministry_code) = 1 THEN (p.department_ministry_code) ELSE (104)
        #             END as "org_code",
        #             IIF (a.orderless_financial_data_id is not null, o.fis_organisation_code , p.is_org) as "fis_org_code",
        #             IIF (a.orderless_financial_data_id is not null, o.fis_reference_code, p.is_ref) as "fis_ref_code",
        #             IIF (a.orderless_financial_data_id is not null, o.billing_contact_name, p.billing_contact) as "billing_contact_name",
        #             dbo.status2str('fr', a.status_id) AS "test_status_fr",
        #             dbo.status2str('en', a.status_id) AS "test_status_en",
        #             a.is_invalid as "is_invalid",
        #             cast(a.submit_date as date) as "submit_date",
        #             d.test_code as "test_code",
        #             d.fr_name as "test_description_fr",
        #             d.en_name as "test_description_en",
        #             a.orderless_financial_data_id,
        #             a.status_id as "test_status_id",
        #             atst.codename as "test_status_codename",
        #             a.test_id as "test_id",
        #             a.id as "assigned_test_id"
        #             FROM
        #             {db_name}..custom_models_assignedtest a
        #             JOIN {db_name}..custom_models_assignedteststatus atst on atst.id = a.status_id
        #             OUTER APPLY (SELECT TOP 1 id, last_name, first_name, pri, military_nbr FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = a.username_id AND u1.history_date < a.submit_date ORDER BY u1.history_date DESC) u
        #             OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicalpermissionrequest pr WHERE pr.username_id = a.ta_id and pr.history_type = '+' and pr.permission_requested_id = 1 and pr.history_date < a.submit_date order by history_date desc) ta
        #             JOIN {db_name}..user_management_models_user v on v.username = a.username_id
        #             LEFT JOIN {db_name}..user_management_models_taextendedprofile e ON e.username_id = a.ta_id
        #             LEFT JOIN {db_name}..OLTF_DEPARTEMENTS_VW de ON de.DEPT_ID = e.department_id
        #             OUTER APPLY (SELECT TOP 1 staffing_process_number, department_ministry_code, is_org, is_ref, billing_contact FROM {db_name}..cms_models_historicaltestpermissions h WHERE h.test_order_number = a.test_order_number AND h.username_id = a.ta_id AND h.history_date < a.submit_date AND h.history_type = '+' ORDER BY h.history_date DESC) p
        #             LEFT JOIN {db_name}..OLTF_DEPARTEMENTS_VW dp ON cast(dp.DEPT_ID as nvarchar) = p.department_ministry_code
        #             JOIN {db_name}..cms_models_testdefinition d ON d.id = a.test_id
        #             LEFT JOIN {db_name}..custom_models_orderlessfinancialdata o ON o.id = a.orderless_financial_data_id
        #             LEFT JOIN {db_name}..OLTF_DEPARTEMENTS_VW odp ON cast(odp.DEPT_ID as nvarchar) = o.department_ministry_id

        #             WHERE
        #             a.status_id IN  (SELECT id FROM {db_name}..custom_models_assignedteststatus WHERE codename in ('submitted', 'quit'))

        #             ORDER BY
        #             a.test_order_number, a.test_id, a.status_id, a.id DESC
        #     """.format(
        #         db_name=settings.DATABASES["default"]["NAME"]
        #     )
        # )
        # cursor.execute(
        #     """CREATE OR ALTER VIEW [dbo].[test_data_vw] AS
        #         SELECT
        #             at.id as 'assigned_test_id',
        #             at.status_id as 'test_status_id',
        #             atst.codename as 'test_status_codename',
        #             IIF (hta.id is not NULL, hta.username, ta.username) as 'ta_username',
        #             IIF (hta.id is not NULL, hta.email, ta.email) as 'ta_email',
        #             IIF (hu.id is not NULL, hu.username, u.username) as 'candidate_username',
        #             IIF (hu.id is not NULL, hu.email, u.email) as 'candidate_email',
        #             IIF (hu.id is not NULL, hu.first_name, u.first_name) as 'candidate_first_name',
        #             IIF (hu.id is not NULL, hu.last_name, u.last_name) as 'candidate_last_name',
        #             td.id as 'test_id',
        #             td.en_name as 'en_test_name',
        #             td.fr_name as 'fr_test_name',
        #             td.version as 'test_version',
        #             IIF (htp.id is not NULL, htp.test_order_number, htp2.test_order_number) as 'test_order_number',
        #             IIF (htp.id is not NULL, htp.staffing_process_number, htp2.staffing_process_number) as 'staffing_process_number',
        #             htp3.ta_usernames as 'allowed_ta_usernames',
        #             ofd.reference_number as 'reference_number'
        #         FROM {db_name}..custom_models_assignedtest at
        #         JOIN {db_name}..custom_models_assignedteststatus atst on atst.id = at.status_id
        #         OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = at.ta_id AND u1.history_date < at.submit_date ORDER BY u1.history_date DESC) hta
        #         JOIN {db_name}..user_management_models_user ta on ta.username = at.ta_id
        #         OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u2 WHERE u2.username = at.username_id AND u2.history_date < at.submit_date ORDER BY u2.history_date DESC) hu
        #         JOIN {db_name}..user_management_models_user u on u.username = at.username_id
        #         JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
        #         OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions htp1 WHERE htp1.test_order_number = at.test_order_number AND htp1.history_date < at.submit_date ORDER BY htp1.history_date DESC) htp
        #         OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions htp1 WHERE htp1.test_order_number = at.test_order_number ORDER BY htp1.history_date DESC) htp2
        #         OUTER APPLY (SELECT string_agg(ta_username_list.username_id, ',') within group (ORDER BY ta_username_list.username_id) as 'ta_usernames' FROM (SELECT DISTINCT username_id FROM {db_name}..cms_models_historicaltestpermissions htp WHERE htp.history_type = '+' AND htp.test_order_number = at.test_order_number) ta_username_list) htp3
        #         LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
        #     """.format(
        #         db_name=settings.DATABASES["default"]["NAME"]
        #     )
        # )
        # cursor.execute(
        #     """CREATE OR ALTER VIEW [dbo].[test_result_vw] AS
        #         SELECT
        #             assigned_test_id,
        #             user_id,
        #             user_firstname AS 'candidate_first_name',
        #             user_lastname AS 'candidate_last_name',
        #             username,
        #             uit_candidate_email,
        #             candidate_email,
        #             candidate_pri,
        #             candidate_military_nbr,
        #             ta_id,
        #             reference_number,
        #             test_order_number,
        #             process_number,
        #             history_id,
        #             td_test_code,
        #             td_fr_name,
        #             td_en_name,
        #             submit_date,
        #             test_score,
        #             CASE
        #                 WHEN test_is_invalid = 1 THEN 'Invalide'
        #                 ELSE
        #                     CASE
        #                         WHEN test_score IS NULL THEN '-'
        #                         WHEN test_fr_converted_score IS NULL THEN 'Conversion invalide du résultat'
        #                     ELSE
        #                         test_fr_converted_score
        #                     END
        #             END AS 'level_fr',
        #             CASE
        #                 WHEN test_is_invalid = 1 THEN 'Invalid'
        #                 ELSE
        #                     CASE
        #                         WHEN test_score IS NULL THEN '-'
        #                         WHEN test_en_converted_score IS NULL THEN 'Invalid Score Conversion'
        #                     ELSE
        #                         test_en_converted_score
        #                     END
        #             END AS 'level_en',
        #             dbo.status2str('fr', test_status_id) AS test_status_fr,
        #             dbo.status2str('en', test_status_id) AS test_status_en,
        #             test_status_id,
        #             test_status_codename,
        #             test_id,
        #             allowed_ta_usernames

        #         FROM
        #         (
        #             SELECT
        #                 at.id AS 'assigned_test_id',
        #                 at.test_id,
        #                 at.ta_id AS 'ta_id',
        #                 ofd.reference_number AS 'reference_number',
        #                 at.test_order_number AS 'test_order_number',
        #                 at.submit_date AS 'submit_date',
        #                 at.total_score AS 'test_score',
        #                 at.en_converted_score AS 'test_en_converted_score',
        #                 at.fr_converted_score AS 'test_fr_converted_score',
        #                 at.status_id AS 'test_status_id',
        #                 atst.codename AS 'test_status_codename',
        #                 at.is_invalid AS 'test_is_invalid',

        #                 IIF(u.id is not null, u.id, v.id) AS 'user_id',
        #                 IIF(u.id is not null, u.username, v.username) AS 'username',
        #                 t.candidate_email AS 'uit_candidate_email',
        #                 IIF(u.id is not null, u.email, v.email) AS 'candidate_email',
        #                 IIF(u.id is not null, u.first_name, v.first_name) AS 'user_firstname',
        #                 IIF(u.id is not null, u.last_name, v.last_name) AS 'user_lastname',
        #                 IIF(u.id is not null, u.pri, v.pri) AS 'candidate_pri',
        #                 IIF(u.id is not null, u.military_nbr, v.military_nbr) AS 'candidate_military_nbr',

        #                 p.staffing_process_number AS 'process_number',
        #                 p.history_id AS 'history_id',

        #                 td.test_code AS 'td_test_code',
        #                 td.fr_name AS 'td_fr_name',
        #                 td.en_name AS 'td_en_name',

        #                 htp3.ta_usernames as 'allowed_ta_usernames'

        #             FROM {db_name}..custom_models_assignedtest at
        #             JOIN {db_name}..custom_models_assignedteststatus atst on atst.id = at.status_id
        #             OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = at.username_id AND u1.history_date < at.submit_date ORDER BY u1.history_date DESC) u
        #             JOIN {db_name}..user_management_models_user v on v.username = at.username_id
        #             OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions p1 WHERE p1.test_order_number = at.test_order_number AND p1.username_id = at.ta_id AND p1.history_date < at.submit_date ORDER BY p1.history_date DESC) p
        #             JOIN {db_name}..cms_models_testdefinition td ON td.id = at.test_id
        #             LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on at.orderless_financial_data_id = ofd.id
        #             OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_historicalunsupervisedtestaccesscode t1 WHERE t1.test_access_code = at.test_access_code AND t1.uit_invite_id = at.uit_invite_id ORDER BY t1.history_date DESC) t
        #             OUTER APPLY (SELECT string_agg(ta_username_list.username_id, ',') within group (ORDER BY ta_username_list.username_id) as 'ta_usernames' FROM (SELECT DISTINCT username_id FROM {db_name}..cms_models_historicaltestpermissions htp WHERE htp.history_type = '+' AND htp.test_order_number = at.test_order_number) ta_username_list) htp3
        #         ) r
        #     """.format(
        #         db_name=settings.DATABASES["default"]["NAME"]
        #     )
        # )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        # cursor.execute(
        #     """CREATE OR ALTER VIEW [dbo].[active_tests_vw] AS
        #         SELECT
        # 			at.id as 'assigned_test_id',
        # 			u.first_name as 'candidate_first_name',
        # 			u.last_name as 'candidate_last_name',
        #             u.username as 'candidate_username',
        # 			u.email as 'candidate_email',
        # 			ta.email as 'ta_id',
        # 			ta.first_name as 'ta_first_name',
        # 			ta.last_name as 'ta_last_name',
        # 			at.status as 'test_status',
        # 			tp.test_order_number,
        # 			ofd.reference_number,
        # 			ats.time_limit,
        #             td.id as 'test_id',
        #             td.test_code
        # 		FROM {db_name}..custom_models_assignedtest at
        # 		OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = at.username_id ORDER BY u1.history_date DESC) u
        #         OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = at.ta_id ORDER BY u1.history_date DESC) ta
        # 		OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions tp1 WHERE tp1.test_order_number = at.test_order_number AND tp1.username_id = at.ta_id ORDER BY tp1.history_date DESC) tp
        # 		LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
        # 		OUTER APPLY (SELECT SUM(ats1.test_section_time) as 'time_limit' FROM {db_name}..custom_models_assignedtestsection ats1 WHERE ats1.assigned_test_id = at.id) ats
        #         LEFT JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
        # 		WHERE at.status in (11, 12, 13, 14, 15, 22)
        #     """.format(
        #         db_name=settings.DATABASES["default"]["NAME"]
        #     )
        # )
        # cursor.execute(
        #     """CREATE OR ALTER VIEW [dbo].[assessment_process_results_candidates_vw] AS
        #         SELECT
        #             ats.id,
        #             ats.first_name,
        #             ats.last_name,
        #             ats.email,
        #             apv.id as 'assessment_process_id',
        #             apv.reference_number as 'assessment_process_reference_number',
        #             apv.closing_date as 'assessment_process_closing_date',
        #             apv.allow_booking_external_tc as 'assessment_process_allow_booking_external_tc',
        #             apv.dept_id as 'assessment_process_dept_id ',
        #             apv.dept_eabrv as 'assessment_process_dept_eabrv',
        #             apv.dept_fabrv as 'assessment_process_dept_fabrv',
        #             apv.dept_edesc as 'assessment_process_dept_edesc',
        #             apv.dept_fdesc as 'assessment_process_dept_fdesc',
        #             bc.id as 'billing_contact_id',
        #             bc.first_name as 'billing_contact_first_name',
        #             bc.last_name as 'billing_contact_last_name ',
        #             bc.email as 'billing_contact_email',
        #             tst.id as 'test_skill_type_id',
        #             tst.codename as 'test_skill_type_codename',
        #             type_e.text as 'test_skill_type_name_en',
        #             type_f.text as 'test_skill_type_name_fr',
        #             ats.test_skill_sub_type_id as 'test_skill_sub_type_id',
        #             sled.codename as 'test_skill_sub_type_codename',
        #             CASE
        #                 WHEN tst.id = 2 THEN sub_type_e.text
        #                 ELSE
        #                     NULL
        #             END AS 'test_skill_sub_type_name_en',
        #             CASE
        #                 WHEN tst.id = 2 THEN sub_type_f.text
        #                 ELSE
        #                     NULL
        #             END AS 'test_skill_sub_type_name_fr',
        #             ats.reason_for_testing_id,
        #             rft.description_en as 'reason_for_testing_name_en',
        #             rft.description_fr as 'reason_for_testing_name_fr',
        #             ats.level_required,
        #             apv.request_sent,
        #             crc.reservation_code,
        #             crc.status_id as 'consumed_reservation_code_status_id',
        #             crc.test_session_id,
        #             crc.candidate_id,
        #             crc.assigned_test_id,
        #             at.status as 'assigned_test_status',
        #             at.en_converted_score,
        #             at.fr_converted_score
        #         FROM {db_name}..custom_models_assessmentprocessassignedtestspecs ats
        #         LEFT JOIN {db_name}..custom_models_billingcontact bc on bc.id = ats.billing_contact_id
        #         JOIN {db_name}..assessment_process_vw apv on apv.id = ats.assessment_process_id
        #         JOIN {db_name}..cms_models_testskilltype tst on tst.id = ats.test_skill_type_id
        #         OUTER APPLY (SELECT TOP 1 tstte.text FROM {db_name}..cms_models_testskilltypetext tstte WHERE tstte.test_skill_type_id = tst.id AND tstte.language_id = 1) type_e
        #         OUTER APPLY (SELECT TOP 1 tsttf.text FROM {db_name}..cms_models_testskilltypetext tsttf WHERE tsttf.test_skill_type_id = tst.id AND tsttf.language_id = 2) type_f
        #         LEFT JOIN {db_name}..cms_models_testskillsledesc sled on sled.id = ats.test_skill_sub_type_id
        #         OUTER APPLY (SELECT TOP 1 sledte.text FROM {db_name}..cms_models_testskillsledesctext sledte WHERE sledte.test_skill_sle_desc_id = sled.id AND sledte.language_id = 1) sub_type_e
        #         OUTER APPLY (SELECT TOP 1 sledtf.text FROM {db_name}..cms_models_testskillsledesctext sledtf WHERE sledtf.test_skill_sle_desc_id = sled.id AND sledtf.language_id = 2) sub_type_f
        #         JOIN {db_name}..custom_models_reasonsfortesting rft on rft.id = ats.reason_for_testing_id
        #         LEFT JOIN {db_name}..custom_models_consumedreservationcodes crc on crc.assessment_process_assigned_test_specs_id = ats.id
        #         LEFT JOIN {db_name}..custom_models_assignedtest at on at.id = crc.assigned_test_id
        #     """.format(
        #         db_name=settings.DATABASES["default"]["NAME"]
        #     )
        # )
        cursor.execute(
            """CREATE OR ALTER FUNCTION [dbo].[status2str] 
                ( 
                    @lang_code varchar(2),
                    @status_id numeric
                )
                RETURNS varchar(100)
                AS 
                BEGIN
                    DECLARE @Result		varchar(100)
                    IF (@lang_code = 'fr')
                        BEGIN
                            SELECT @Result	= CASE 
                            WHEN @status_id = 11 THEN 'ATTRIBUÉ'
                            WHEN @status_id = 12 THEN 'PRÊT'
                            WHEN @status_id = 13 THEN 'ACTIF'
                            WHEN @status_id = 14 THEN 'VERROUILLÉ'
                            WHEN @status_id = 15 THEN 'EN PAUSE'			
                            WHEN @status_id = 19 THEN 'ABANDONNÉ'
                            WHEN @status_id = 20 THEN 'SOUMIS'
                            WHEN @status_id = 21 THEN 'NON ATTRIBUÉ'
                            WHEN @status_id = 22 THEN 'PRÉ_TEST'
                            ELSE CAST(@status_id AS varchar(4))
                            END
                        END
                    ELSE
                        BEGIN
                            SELECT @Result	= CASE 
                            WHEN @status_id = 11 THEN 'ASSIGNED'
                            WHEN @status_id = 12 THEN 'READY'
                            WHEN @status_id = 13 THEN 'ACTIVE'
                            WHEN @status_id = 14 THEN 'LOCKED'
                            WHEN @status_id = 15 THEN 'PAUSED'			
                            WHEN @status_id = 19 THEN 'QUIT'
                            WHEN @status_id = 20 THEN 'SUBMITTED'
                            WHEN @status_id = 21 THEN 'UNASSIGNED'
                            WHEN @status_id = 22 THEN 'PRE_TEST'
                            ELSE CAST(@status_id AS varchar(4))
                            END
                        END
                    RETURN @Result
                END"""
        )
        # cursor.execute(
        #     """CREATE OR ALTER VIEW [dbo].[financial_report_vw] AS
        #         SELECT TOP 100 percent
        #             IIF (u.id is null, v.last_name, u.last_name) as "candidate_last_name",
        #             IIF (u.id is null, v.first_name, u.first_name) as "candidate_first_name",
        #             IIF (u.id is null, v.pri, u.pri) as "candidate_pri",
        #             IIF (u.id is null, v.military_nbr, u.military_nbr) as "candidate_military_nbr",
        #             ta.goc_email as "ta_email",
        #             de.edesc as "ta_org_en",
        #             de.fdesc as "ta_org_fr",
        #             IIF (dp.edesc is null and a.orderless_financial_data_id is not null, odp.edesc, IIF(dp.edesc is null, 'Public Service Commission', dp.edesc)) as "requesting_dep_en",
        #             IIF (dp.fdesc is null and a.orderless_financial_data_id is not null, odp.fdesc, IIF(dp.fdesc is null, 'Public Service Commission', dp.fdesc)) as "requesting_dep_fr",
        #             a.test_order_number as "order_no",
        #             p.staffing_process_number as "assessment_process",
        #             o.reference_number as "reference_number",
        #             CASE
        #                 WHEN a.orderless_financial_data_id is not null THEN o.department_ministry_id
        #                 WHEN ISNUMERIC(p.department_ministry_code) = 1 THEN (p.department_ministry_code) ELSE (104)
        #             END as "org_code",
        #             IIF (a.orderless_financial_data_id is not null, o.fis_organisation_code , p.is_org) as "fis_org_code",
        #             IIF (a.orderless_financial_data_id is not null, o.fis_reference_code, p.is_ref) as "fis_ref_code",
        #             IIF (a.orderless_financial_data_id is not null, o.billing_contact_name, p.billing_contact) as "billing_contact_name",
        #             dbo.status2str('fr', a.status) AS "test_status_fr",
        #             dbo.status2str('en', a.status) AS "test_status_en",
        #             a.is_invalid as "is_invalid",
        #             cast(a.submit_date as date) as "submit_date",
        #             d.test_code as "test_code",
        #             d.fr_name as "test_description_fr",
        #             d.en_name as "test_description_en",
        #             a.orderless_financial_data_id,
        #             a.status as "test_status",
        #             a.test_id as "test_id",
        #             a.id as "assigned_test_id"
        #         FROM
        #             {db_name}..custom_models_assignedtest a
        #             OUTER APPLY (SELECT TOP 1 id, last_name, first_name, pri, military_nbr FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = a.username_id AND u1.history_date < a.submit_date ORDER BY u1.history_date DESC) u
        #             OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicalpermissionrequest pr WHERE pr.username_id = a.ta_id and pr.history_type = '+' and pr.permission_requested_id = 1 and pr.history_date < a.submit_date order by history_date desc) ta
        #             JOIN {db_name}..user_management_models_user v on v.username = a.username_id
        #             LEFT JOIN {db_name}..user_management_models_taextendedprofile e ON e.username_id = a.ta_id
        #             LEFT JOIN {db_name}..OLTF_DEPARTEMENTS_VW de ON de.DEPT_ID = e.department_id
        #             OUTER APPLY (SELECT TOP 1 staffing_process_number, department_ministry_code, is_org, is_ref, billing_contact FROM {db_name}..cms_models_historicaltestpermissions h WHERE h.test_order_number = a.test_order_number AND h.username_id = a.ta_id AND h.history_date < a.submit_date AND h.history_type = '+' ORDER BY h.history_date DESC) p
        #             LEFT JOIN {db_name}..OLTF_DEPARTEMENTS_VW dp ON cast(dp.DEPT_ID as nvarchar) = p.department_ministry_code
        #             JOIN {db_name}..cms_models_testdefinition d ON d.id = a.test_id
        #             LEFT JOIN {db_name}..custom_models_orderlessfinancialdata o ON o.id = a.orderless_financial_data_id
        #             LEFT JOIN {db_name}..OLTF_DEPARTEMENTS_VW odp ON cast(odp.DEPT_ID as nvarchar) = o.department_ministry_id

        #         WHERE
        #             a.status IN (19, 20)

        #         ORDER BY
        #             a.test_order_number, a.test_id, a.status, a.id DESC
        #     """.format(
        #         db_name=settings.DATABASES["default"]["NAME"]
        #     )
        # )
        # cursor.execute(
        #     """CREATE OR ALTER VIEW [dbo].[test_data_vw] AS
        #         SELECT
        #             at.id as 'assigned_test_id',
        #             at.status as 'test_status',
        #             IIF (hta.id is not NULL, hta.username, ta.username) as 'ta_username',
        #             IIF (hta.id is not NULL, hta.email, ta.email) as 'ta_email',
        #             IIF (hu.id is not NULL, hu.username, u.username) as 'candidate_username',
        #             IIF (hu.id is not NULL, hu.email, u.email) as 'candidate_email',
        #             IIF (hu.id is not NULL, hu.first_name, u.first_name) as 'candidate_first_name',
        #             IIF (hu.id is not NULL, hu.last_name, u.last_name) as 'candidate_last_name',
        #             td.id as 'test_id',
        #             td.en_name as 'en_test_name',
        #             td.fr_name as 'fr_test_name',
        #             td.version as 'test_version',
        #             IIF (htp.id is not NULL, htp.test_order_number, htp2.test_order_number) as 'test_order_number',
        #             IIF (htp.id is not NULL, htp.staffing_process_number, htp2.staffing_process_number) as 'staffing_process_number',
        #             htp3.ta_usernames as 'allowed_ta_usernames',
        #             ofd.reference_number as 'reference_number'
        #         FROM {db_name}..custom_models_assignedtest at
        #         OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = at.ta_id AND u1.history_date < at.submit_date ORDER BY u1.history_date DESC) hta
        #         JOIN {db_name}..user_management_models_user ta on ta.username = at.ta_id
        #         OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u2 WHERE u2.username = at.username_id AND u2.history_date < at.submit_date ORDER BY u2.history_date DESC) hu
        #         JOIN {db_name}..user_management_models_user u on u.username = at.username_id
        #         JOIN {db_name}..cms_models_testdefinition td on td.id = at.test_id
        #         OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions htp1 WHERE htp1.test_order_number = at.test_order_number AND htp1.history_date < at.submit_date ORDER BY htp1.history_date DESC) htp
        #         OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions htp1 WHERE htp1.test_order_number = at.test_order_number ORDER BY htp1.history_date DESC) htp2
        #         OUTER APPLY (SELECT string_agg(ta_username_list.username_id, ',') within group (ORDER BY ta_username_list.username_id) as 'ta_usernames' FROM (SELECT DISTINCT username_id FROM {db_name}..cms_models_historicaltestpermissions htp WHERE htp.history_type = '+' AND htp.test_order_number = at.test_order_number) ta_username_list) htp3
        #         LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on ofd.id = at.orderless_financial_data_id
        #     """.format(
        #         db_name=settings.DATABASES["default"]["NAME"]
        #     )
        # )
        # cursor.execute(
        #     """CREATE OR ALTER VIEW [dbo].[test_result_vw] AS
        #         SELECT
        #             assigned_test_id,
        #             user_id,
        #             user_firstname AS 'candidate_first_name',
        #             user_lastname AS 'candidate_last_name',
        #             username,
        #             uit_candidate_email,
        #             candidate_email,
        #             candidate_pri,
        #             candidate_military_nbr,
        #             ta_id,
        #             reference_number,
        #             test_order_number,
        #             process_number,
        #             history_id,
        #             td_test_code,
        #             td_fr_name,
        #             td_en_name,
        #             submit_date,
        #             test_score,
        #             CASE
        #                 WHEN test_is_invalid = 1 THEN 'Invalide'
        #                 ELSE
        #                     CASE
        #                         WHEN test_score IS NULL THEN '-'
        #                         WHEN test_fr_converted_score IS NULL THEN 'Conversion invalide du résultat'
        #                     ELSE
        #                         test_fr_converted_score
        #                     END
        #             END AS 'level_fr',
        #             CASE
        #                 WHEN test_is_invalid = 1 THEN 'Invalid'
        #                 ELSE
        #                     CASE
        #                         WHEN test_score IS NULL THEN '-'
        #                         WHEN test_en_converted_score IS NULL THEN 'Invalid Score Conversion'
        #                     ELSE
        #                         test_en_converted_score
        #                     END
        #             END AS 'level_en',
        #             dbo.status2str('fr', test_status) AS test_status_fr,
        #             dbo.status2str('en', test_status) AS test_status_en,
        #             test_status,
        #             test_id,
        #             allowed_ta_usernames

        #         FROM
        #         (
        #             SELECT
        #                 at.id AS 'assigned_test_id',
        #                 at.test_id,
        #                 at.ta_id AS 'ta_id',
        #                 ofd.reference_number AS 'reference_number',
        #                 at.test_order_number AS 'test_order_number',
        #                 at.submit_date AS 'submit_date',
        #                 at.total_score AS 'test_score',
        #                 at.en_converted_score AS 'test_en_converted_score',
        #                 at.fr_converted_score AS 'test_fr_converted_score',
        #                 at.status AS 'test_status',
        #                 at.is_invalid AS 'test_is_invalid',

        #                 IIF(u.id is not null, u.id, v.id) AS 'user_id',
        #                 IIF(u.id is not null, u.username, v.username) AS 'username',
        #                 t.candidate_email AS 'uit_candidate_email',
        #                 IIF(u.id is not null, u.email, v.email) AS 'candidate_email',
        #                 IIF(u.id is not null, u.first_name, v.first_name) AS 'user_firstname',
        #                 IIF(u.id is not null, u.last_name, v.last_name) AS 'user_lastname',
        #                 IIF(u.id is not null, u.pri, v.pri) AS 'candidate_pri',
        #                 IIF(u.id is not null, u.military_nbr, v.military_nbr) AS 'candidate_military_nbr',

        #                 p.staffing_process_number AS 'process_number',
        #                 p.history_id AS 'history_id',

        #                 td.test_code AS 'td_test_code',
        #                 td.fr_name AS 'td_fr_name',
        #                 td.en_name AS 'td_en_name',

        #                 htp3.ta_usernames as 'allowed_ta_usernames'

        #             FROM {db_name}..custom_models_assignedtest at
        #             OUTER APPLY (SELECT TOP 1 * FROM {db_name}..user_management_models_historicaluser u1 WHERE u1.username = at.username_id AND u1.history_date < at.submit_date ORDER BY u1.history_date DESC) u
        #             JOIN {db_name}..user_management_models_user v on v.username = at.username_id
        #             OUTER APPLY (SELECT TOP 1 * FROM {db_name}..cms_models_historicaltestpermissions p1 WHERE p1.test_order_number = at.test_order_number AND p1.username_id = at.ta_id AND p1.history_date < at.submit_date ORDER BY p1.history_date DESC) p
        #             JOIN {db_name}..cms_models_testdefinition td ON td.id = at.test_id
        #             LEFT JOIN {db_name}..custom_models_orderlessfinancialdata ofd on at.orderless_financial_data_id = ofd.id
        #             OUTER APPLY (SELECT TOP 1 * FROM {db_name}..custom_models_historicalunsupervisedtestaccesscode t1 WHERE t1.test_access_code = at.test_access_code AND t1.uit_invite_id = at.uit_invite_id ORDER BY t1.history_date DESC) t
        #             OUTER APPLY (SELECT string_agg(ta_username_list.username_id, ',') within group (ORDER BY ta_username_list.username_id) as 'ta_usernames' FROM (SELECT DISTINCT username_id FROM {db_name}..cms_models_historicaltestpermissions htp WHERE htp.history_type = '+' AND htp.test_order_number = at.test_order_number) ta_username_list) htp3
        #         ) r
        #     """.format(
        #         db_name=settings.DATABASES["default"]["NAME"]
        #     )
        # )


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0133_update_camm_integration_vw",
        ),
        (
            "custom_models",
            "0080_refactoring_assigned_test_status_and_previous_status",
        ),
    ]
    operations = [migrations.RunPython(update_vw, rollback_changes)]
