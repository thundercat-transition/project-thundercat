# Generated by Django 5.0.9 on 2024-12-23 19:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("db_view_models", "0377_update_scorer_ola_detailed_tests_to_assess_view"),
    ]

    operations = [
        migrations.AddField(
            model_name="scoreroladetailedteststoassessvw",
            name="is_standard",
            field=models.BooleanField(db_column="is_standard", default=0),
        ),
        migrations.AddField(
            model_name="scoreroladetailedteststoassessvw",
            name="meeting_link",
            field=models.CharField(default="", max_length=255),
        ),
        migrations.AddField(
            model_name="scoreroladetailedteststoassessvw",
            name="user_accommodation_file_id",
            field=models.IntegerField(
                db_column="user_accommodation_file_id", default=1
            ),
        ),
    ]
