from django.db import connection
from django.db import migrations
from django.conf import settings


def create_vw(apps, schema_editor):
    pass
    # with connection.cursor() as cursor:
    #     cursor.execute(
    #         """CREATE OR ALTER VIEW [dbo].[test_center_test_sessions_vw] AS
    #             SELECT
    #                 ts.id,
    #                 ts.test_center_id,
    #                 tsd.test_center_room_id,
    #                 rv.name as 'room_name',
    #                 rv.max_occupancy as 'room_max_occupancy',
    #                 tsd.open_to_ogd,
    #                 tsd.date,
    #                 tsd.start_time,
    #                 tsd.end_time,
    #                 tsd.spaces_available,
    #                 tsd.test_skill_type_id,
    #                 tst.codename as 'test_skill_type_codename',
    #                 stte.text as 'test_skill_type_en_name',
    #                 sttf.text as 'test_skill_type_fr_name',
    #                 tsd.test_skill_sub_type_id,
    #                 CASE
    #                     -- SLE Sub Type
    #                     WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'sle') THEN (SELECT text FROM {db_name}..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = tsd.test_skill_sub_type_id AND language_id = 1)
    #                     ELSE
    #                         -- OCCUPATIONAL Sub Type
    #                         CASE
    #                             WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM {db_name}..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = tsd.test_skill_sub_type_id AND language_id = 1)
    #                         ELSE
    #                             NULL
    #                         END
    #                 END AS 'test_skill_sub_type_en_name',
    #                 CASE
    #                     -- SLE Sub Type
    #                     WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'SLE') THEN (SELECT text FROM {db_name}..cms_models_testskillsledesctext WHERE test_skill_sle_desc_id = tsd.test_skill_sub_type_id AND language_id = 2)
    #                     ELSE
    #                         -- OCCUPATIONAL Sub Type
    #                         CASE
    #                             WHEN tsd.test_skill_type_id = (SELECT id FROM {db_name}..cms_models_testskilltype WHERE codename = 'occupational') THEN (SELECT text FROM {db_name}..cms_models_testskilloccupationaldesctext WHERE test_skill_occupational_desc_id = tsd.test_skill_sub_type_id AND language_id = 2)
    #                         ELSE
    #                             NULL
    #                         END
    #                 END AS 'test_skill_sub_type_fr_name'
    #             FROM {db_name}..custom_models_testcentertestsessions ts
    #             JOIN {db_name}..custom_models_testcentertestsessiondata tsd on tsd.id = ts.test_session_data_id
    #             JOIN {db_name}..test_center_rooms_vw rv on rv.id = tsd.test_center_room_id
    #             JOIN {db_name}..cms_models_testskilltype tst on tst.id = tsd.test_skill_type_id
    #             JOIN {db_name}..cms_models_testskilltypetext stte on stte.test_skill_type_id = tsd.test_skill_type_id AND stte.language_id = 1
    #             JOIN {db_name}..cms_models_testskilltypetext sttf on sttf.test_skill_type_id = tsd.test_skill_type_id AND sttf.language_id = 2
    #     """.format(
    #             db_name=settings.DATABASES["default"]["NAME"]
    #         )
    #     )


def rollback_changes(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        (
            "db_view_models",
            "0063_testcenterroomsvw",
        ),
    ]
    operations = [migrations.RunPython(create_vw, rollback_changes)]
