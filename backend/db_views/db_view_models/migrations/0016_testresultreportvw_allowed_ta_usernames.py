# Generated by Django 3.2.18 on 2023-03-20 18:29

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("db_view_models", "0015_update_test_result_vw"),
    ]

    operations = [
        migrations.AddField(
            model_name="testresultreportvw",
            name="allowed_ta_usernames",
            field=models.TextField(db_column="ALLOWED_TA_USERNAMES", default=None),
        ),
    ]
