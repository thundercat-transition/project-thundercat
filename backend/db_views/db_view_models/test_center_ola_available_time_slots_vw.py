from django.db import models


class TestCenterOlaAvailableTimeSlotsVw(models.Model):
    id = models.IntegerField(db_column="id", primary_key=True)
    date = models.DateField(db_column="date")
    start_time = models.TimeField(db_column="start_time")
    end_time = models.TimeField(db_column="end_time")
    day_of_week_id = models.IntegerField(db_column="day_of_week_id")
    assessed_language_id = models.IntegerField(db_column="assessed_language_id")
    nbr_of_reserved_assessors_high_priority = models.IntegerField(
        db_column="nbr_of_reserved_assessors_high_priority", default=0
    )
    nbr_of_reserved_assessors_per_date_high_priority = models.IntegerField(
        db_column="nbr_of_reserved_assessors_per_date_high_priority", default=0
    )
    remaining_available_test_assessors_per_date_high = models.IntegerField(
        db_column="remaining_available_test_assessors_per_date_high", default=0
    )
    nbr_of_reserved_assessors_medium_priority = models.IntegerField(
        db_column="nbr_of_reserved_assessors_medium_priority", default=0
    )
    nbr_of_reserved_assessors_per_date_medium_priority = models.IntegerField(
        db_column="nbr_of_reserved_assessors_per_date_medium_priority", default=0
    )
    remaining_available_test_assessors_per_date_medium = models.IntegerField(
        db_column="remaining_available_test_assessors_per_date_medium", default=0
    )
    nbr_of_reserved_assessors_low_priority = models.IntegerField(
        db_column="nbr_of_reserved_assessors_low_priority", default=0
    )
    nbr_of_reserved_assessors_per_date_low_priority = models.IntegerField(
        db_column="nbr_of_reserved_assessors_per_date_low_priority", default=0
    )
    remaining_available_test_assessors_per_date_low = models.IntegerField(
        db_column="remaining_available_test_assessors_per_date_low", default=0
    )
    available_test_assessors_total = models.IntegerField(
        db_column="available_test_assessors_total", default=0
    )
    reservable_available_test_assessors_per_date = models.IntegerField(
        db_column="reservable_available_test_assessors_per_date", default=0
    )
    remaining_available_test_assessors_total = models.IntegerField(
        db_column="remaining_available_test_assessors_total", default=0
    )

    class Meta:
        managed = False
        db_table = "test_center_ola_available_time_slots_vw"
