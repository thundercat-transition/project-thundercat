from django.db import models


class AssessmentProcessResultsVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    user_id = models.IntegerField(db_column="USER_ID")
    reference_number = models.CharField(db_column="REFERENCE_NUMBER", max_length=30)
    closing_date = models.DateField(db_column="CLOSING_DATE")
    number_of_candidates = models.IntegerField(db_column="NUMBER_OF_CANDIDATES")
    number_of_tests_taken = models.IntegerField(db_column="NUMBER_OF_TESTS_TAKEN")

    class Meta:
        managed = False
        db_table = "assessment_process_results_vw"
