from django.db import models


class AaeReportsNumberOfClosedRequestsVw(models.Model):
    id = models.IntegerField(db_column="id", primary_key=True)
    created_date = models.DateField(db_column="created_date")
    status_id = models.IntegerField(db_column="status_id")
    status_codename = models.CharField(db_column="status_codename", max_length=50)
    first_completed_by_aae_date = models.DateTimeField(
        db_column="first_completed_by_aae_date"
    )
    first_approved_by_candidate_date = models.DateTimeField(
        db_column="first_approved_by_candidate_date"
    )
    test_id = models.IntegerField(db_column="test_id")
    test_skill_type_id = models.IntegerField(db_column="test_skill_type_id")
    test_skill_type_codename = models.CharField(
        db_column="test_skill_type_codename", max_length=25
    )
    test_skill_type_name_en = models.CharField(
        db_column="test_skill_type_name_en", max_length=150
    )
    test_skill_type_name_fr = models.CharField(
        db_column="test_skill_type_name_fr", max_length=150
    )
    test_skill_sub_type_id = models.IntegerField(db_column="test_skill_sub_type_id")
    test_skill_sub_type_codename = models.CharField(
        db_column="test_skill_sub_type_codename", max_length=25
    )
    test_skill_sub_type_name_en = models.CharField(
        db_column="test_skill_sub_type_name_en", max_length=150
    )
    test_skill_sub_type_name_fr = models.CharField(
        db_column="test_skill_sub_type_name_fr", max_length=150
    )

    class Meta:
        managed = False
        db_table = "aae_reports_number_of_closed_requests_vw"
