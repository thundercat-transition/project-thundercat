from django.db import models


class TestCentersVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    name = models.CharField(db_column="NAME", max_length=150)
    postal_code = models.CharField(db_column="POSTAL_CODE", max_length=7)
    security_email = models.EmailField(db_column="SECURITY_EMAIL", max_length=254)
    booking_delay = models.IntegerField(db_column="BOOKING_DELAY", default=48)
    accommodations_friendly = models.BooleanField(
        db_column="ACCOMMODATIONS_FRIENDLY", default=False
    )
    modify_date = models.DateTimeField(db_column="MODIFY_DATE")
    number_of_tas = models.IntegerField(db_column="NUMBER_OF_TAS")
    number_of_rooms = models.IntegerField(db_column="NUMBER_OF_ROOMS")
    country_id = models.IntegerField(db_column="COUNTRY_ID", default=1)
    country_eabrv = models.CharField(
        db_column="COUNTRY_EABRV", max_length=64, default=""
    )
    country_fabrv = models.CharField(
        db_column="COUNTRY_FABRV", max_length=64, default=""
    )
    country_edesc = models.CharField(
        db_column="COUNTRY_EDESC", max_length=200, default=""
    )
    country_fdesc = models.CharField(
        db_column="COUNTRY_FDESC", max_length=200, default=""
    )
    dept_id = models.IntegerField(db_column="DEPT_ID")
    dept_eabrv = models.CharField(db_column="DEPT_EABRV", max_length=10)
    dept_fabrv = models.CharField(db_column="DEPT_FABRV", max_length=10)
    dept_edesc = models.CharField(db_column="DEPT_EDESC", max_length=200)
    dept_fdesc = models.CharField(db_column="DEPT_FDESC", max_length=200)
    ola_authorized = models.BooleanField(db_column="OLA_AUTHORIZED", default=False)
    pending_accommodation_requests = models.IntegerField(
        db_column="PENDING_ACCOMMODATION_REQUESTS", default=0
    )

    class Meta:
        managed = False
        db_table = "test_centers_vw"
