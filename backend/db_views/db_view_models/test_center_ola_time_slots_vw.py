from django.db import models


class TestCenterOlaTimeSlotsVw(models.Model):
    id = models.IntegerField(db_column="id", primary_key=True)
    start_time = models.TimeField(db_column="start_time")
    end_time = models.TimeField(db_column="end_time")
    utc_offset = models.IntegerField(db_column="utc_offset", default=-18000)
    availability = models.IntegerField(db_column="availability")
    slots_to_prioritize = models.IntegerField(
        db_column="slots_to_prioritize", default=0
    )
    assessed_language_id = models.IntegerField(db_column="assessed_language_id")
    assessed_language_text_en = models.CharField(
        db_column="assessed_language_text_en", max_length=50
    )
    assessed_language_text_fr = models.CharField(
        db_column="assessed_language_text_fr", max_length=50
    )
    day_of_week_id = models.IntegerField(db_column="day_of_week_id")
    day_of_week_text_en = models.CharField(
        db_column="day_of_week_text_en", max_length=20
    )
    day_of_week_text_fr = models.CharField(
        db_column="day_of_week_text_fr", max_length=20
    )
    test_center_id = models.IntegerField(db_column="test_center_id")
    number_of_selected_assessors = models.IntegerField(
        db_column="number_of_selected_assessors", default=0
    )
    number_of_available_slots = models.IntegerField(
        db_column="number_of_available_slots", default=0
    )
    prioritization_high = models.IntegerField(
        db_column="prioritization_high", default=0
    )
    prioritization_medium = models.IntegerField(
        db_column="prioritization_medium", default=0
    )
    prioritization_low = models.IntegerField(db_column="prioritization_low", default=0)

    class Meta:
        managed = False
        db_table = "test_center_ola_time_slots_vw"
