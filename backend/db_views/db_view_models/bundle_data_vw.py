from django.db import models


class BundleDataVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    name = models.CharField(db_column="NAME", max_length=50)
    version_text = models.CharField(db_column="VERSION_TEXT", max_length=50)
    shuffle_items = models.BooleanField(db_column="SHUFFLE_ITEMS")
    shuffle_bundles = models.BooleanField(db_column="SHUFFLE_BUNDLES")
    shuffle_between_bundles = models.BooleanField(
        db_column="SHUFFLE_BETWEEN_BUNDLES", default=0
    )
    active = models.BooleanField(db_column="ACTIVE")
    item_bank_id = models.IntegerField(db_column="ITEM_BANK_ID")
    item_count = models.IntegerField(db_column="ITEM_COUNT")
    bundle_count = models.IntegerField(db_column="BUNDLE_COUNT")

    class Meta:
        managed = False
        db_table = "bundle_data_vw"
