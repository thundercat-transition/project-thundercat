from django.db import models


class SelectedUserAccommodationFileVW(models.Model):
    id = models.IntegerField(db_column="id", primary_key=True)
    comments = models.TextField(db_column="comments")
    is_uit = models.BooleanField(db_column="is_uit")
    modify_date = models.DateTimeField(db_column="modify_date")
    created_date = models.DateField(db_column="created_date")
    rationale = models.TextField(db_column="rationale", default="")
    is_alternate_test_request = models.BooleanField(
        db_column="is_alternate_test_request", default=0
    )
    last_modified_by_user_id = models.IntegerField(
        db_column="last_modified_by_user_id", default=1
    )
    last_modified_by_user_email = models.CharField(
        db_column="last_modified_by_user_email", max_length=254, default="temp"
    )
    last_modified_by_user_first_name = models.CharField(
        db_column="last_modified_by_user_first_name", max_length=30, default="temp"
    )
    last_modified_by_user_last_name = models.CharField(
        db_column="last_modified_by_user_last_name", max_length=150, default="temp"
    )
    assigned_to_user_id = models.IntegerField(
        db_column="assigned_to_user_id", default=1
    )
    assigned_to_user_email = models.CharField(
        db_column="assigned_to_user_email", max_length=254, default="temp"
    )
    assigned_to_user_first_name = models.CharField(
        db_column="assigned_to_user_first_name", max_length=30, default="temp"
    )
    assigned_to_user_last_name = models.CharField(
        db_column="assigned_to_user_last_name", max_length=150, default="temp"
    )
    status_id = models.IntegerField(db_column="status_id")
    status_codename = models.CharField(db_column="status_codename", max_length=50)
    complexity_id = models.IntegerField(db_column="complexity_id", default=1)
    complexity_codename = models.CharField(
        db_column="complexity_codename", max_length=50, default="temp"
    )
    test_center_id = models.IntegerField(db_column="test_center_id")
    user_id = models.IntegerField(db_column="user_id")
    user_username = models.CharField(db_column="user_username", max_length=254)
    user_email = models.CharField(db_column="user_email", max_length=254)
    user_secondary_email = models.CharField(
        db_column="user_secondary_email", max_length=254
    )
    user_first_name = models.CharField(db_column="user_first_name", max_length=30)
    user_last_name = models.CharField(db_column="user_last_name", max_length=150)
    user_dob = models.DateField(db_column="user_dob")
    user_psrs_applicant_id = models.CharField(
        db_column="user_psrs_applicant_id", max_length=8
    )
    user_pri = models.CharField(db_column="user_pri", max_length=10)
    user_military_nbr = models.CharField(db_column="user_military_nbr", max_length=9)
    user_phone_number = models.CharField(db_column="user_phone_number", max_length=10)
    ola_phone_number = models.CharField(
        db_column="ola_phone_number", max_length=10, default=""
    )
    process_end_date = models.DateField(
        db_column="process_end_date", default="1900-01-01"
    )
    reference_number = models.CharField(db_column="reference_number", max_length=30)
    test_center_name = models.CharField(max_length=150, blank=False, null=False)
    dept_id = models.IntegerField(db_column="dept_id")
    dept_eabrv = models.CharField(db_column="dept_eabrv", max_length=10)
    dept_fabrv = models.CharField(db_column="dept_fabrv", max_length=10)
    dept_edesc = models.CharField(db_column="dept_edesc", max_length=200)
    dept_fdesc = models.CharField(db_column="dept_fdesc", max_length=200)
    primary_contact_user_id = models.IntegerField(db_column="primary_contact_user_id")
    primary_contact_user_email = models.CharField(
        db_column="primary_contact_user_email", max_length=254
    )
    primary_contact_user_first_name = models.CharField(
        db_column="primary_contact_user_first_name", max_length=30
    )
    primary_contact_user_last_name = models.CharField(
        db_column="primary_contact_user_last_name", max_length=150
    )
    test_skill_type_id = models.IntegerField(db_column="TEST_SKILL_TYPE_ID", default=1)
    test_skill_type_codename = models.CharField(
        db_column="TEST_SKILL_TYPE_CODENAME", max_length=25, default="temp"
    )
    test_skill_type_name_en = models.CharField(
        db_column="TEST_SKILL_TYPE_NAME_EN", max_length=150, default="temp"
    )
    test_skill_type_name_fr = models.CharField(
        db_column="TEST_SKILL_TYPE_NAME_FR", max_length=150, default="temp"
    )
    test_skill_sub_type_id = models.IntegerField(
        db_column="TEST_SKILL_SUB_TYPE_ID", default=1
    )
    test_skill_sub_type_codename = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_CODENAME", max_length=25, default="temp"
    )
    test_skill_sub_type_name_en = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_NAME_EN", max_length=150, default="temp"
    )
    test_skill_sub_type_name_fr = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_NAME_FR", max_length=150, default="temp"
    )

    class Meta:
        managed = False
        db_table = "selected_user_accommodation_file_vw"
