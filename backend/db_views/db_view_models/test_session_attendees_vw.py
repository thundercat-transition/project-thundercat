from django.db import models


class TestSessionAttendeesVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    reservation_code = models.CharField(db_column="RESERVATION_CODE", max_length=14)
    status_id = models.IntegerField(db_column="STATUS_ID")
    test_session_id = models.IntegerField(db_column="TEST_SESSION_ID")
    assigned_test_id = models.IntegerField(db_column="ASSIGNED_TEST_ID")
    assessment_process_assigned_test_specs_id = models.IntegerField(
        db_column="ASSESSMENT_PROCESS_ASSIGNED_TEST_SPECS_ID"
    )
    first_name = models.CharField(db_column="FIRST_NAME", max_length=30)
    last_name = models.CharField(db_column="LAST_NAME", max_length=150)
    email = models.EmailField(db_column="EMAIL", max_length=254)
    assessment_process_id = models.IntegerField(db_column="ASSESSMENT_PROCESS_ID")
    cat_user_id = models.IntegerField(db_column="CAT_USER_ID")
    cat_user_first_name = models.CharField(
        db_column="CAT_USER_FIRST_NAME", max_length=30
    )
    cat_user_last_name = models.CharField(
        db_column="CAT_USER_LAST_NAME", max_length=150
    )
    cat_user_email = models.EmailField(db_column="CAT_USER_EMAIL", max_length=254)

    class Meta:
        managed = False
        db_table = "test_session_attendees_vw"
