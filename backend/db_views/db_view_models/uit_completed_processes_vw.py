from django.db import models


class UitCompletedProcessesVW(models.Model):
    id = models.IntegerField(db_column="id", primary_key=True)
    invite_date = models.DateField(db_column="invite_date")
    validity_end_date = models.DateField(db_column="validity_end_date")
    orderless_request = models.BooleanField(db_column="orderless_request")
    ta_user_id = models.IntegerField(db_column="ta_user_id")
    ta_email = models.CharField(db_column="ta_email", max_length=254)
    ta_first_name = models.CharField(db_column="ta_first_name", max_length=30)
    ta_last_name = models.CharField(db_column="ta_last_name", max_length=150)
    ta_test_permissions_id = models.IntegerField(db_column="ta_test_permissions_id")
    assessment_process_or_reference_nbr = models.CharField(
        db_column="assessment_process_or_reference_nbr", max_length=50
    )
    dept_id = models.IntegerField(db_column="dept_id")
    dept_eabrv = models.CharField(db_column="dept_eabrv", max_length=10)
    dept_fabrv = models.CharField(db_column="dept_fabrv", max_length=10)
    dept_edesc = models.CharField(db_column="dept_edesc", max_length=200)
    dept_fdesc = models.CharField(db_column="dept_fdesc", max_length=200)
    test_id = models.IntegerField(db_column="test_id")
    test_code = models.CharField(db_column="test_code", max_length=25)
    version = models.IntegerField(db_column="version")
    test_name_en = models.CharField(db_column="test_name_en", max_length=175)
    test_name_fr = models.CharField(db_column="test_name_fr", max_length=175)
    total_number_of_utac = models.IntegerField(db_column="total_number_of_utac")
    number_of_remaining_utac = models.IntegerField(db_column="number_of_remaining_utac")
    number_of_active_tests = models.IntegerField(db_column="number_of_active_tests")
    number_of_tests_taken = models.IntegerField(db_column="number_of_tests_taken")

    class Meta:
        managed = False
        db_table = "uit_completed_processes_vw"
