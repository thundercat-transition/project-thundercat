from django.db import models


class BillingContactVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    first_name = models.CharField(db_column="FIRST_NAME", max_length=30)
    last_name = models.CharField(db_column="LAST_NAME", max_length=150)
    email = models.EmailField(db_column="EMAIL", max_length=254)
    fis_organisation_code = models.CharField(
        db_column="FIS_ORGANISATION_CODE", max_length=16
    )
    fis_reference_code = models.CharField(db_column="FIS_REFERENCE_CODE", max_length=20)
    user_id = models.IntegerField(db_column="USER_ID")
    dept_id = models.IntegerField(db_column="DEPT_ID")
    dept_eabrv = models.CharField(db_column="DEPT_EABRV", max_length=10)
    dept_fabrv = models.CharField(db_column="DEPT_FABRV", max_length=10)
    dept_edesc = models.CharField(db_column="DEPT_EDESC", max_length=200)
    dept_fdesc = models.CharField(db_column="DEPT_FDESC", max_length=200)

    class Meta:
        managed = False
        db_table = "billing_contact_vw"
