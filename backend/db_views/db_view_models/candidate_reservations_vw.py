from django.db import models


class CandidateReservationsVW(models.Model):
    id = models.IntegerField(db_column="ID", primary_key=True)
    reservation_code = models.CharField(max_length=14, null=False, blank=False)
    assessment_process_reference_number = models.CharField(
        db_column="ASSESSMENT_PROCESS_REFERENCE_NUMBER", max_length=30, default="temp"
    )
    assessment_process_closing_date = models.DateField(
        db_column="ASSESSMENT_PROCESS_CLOSING_DATE"
    )
    allow_booking_external_tc = models.BooleanField(
        db_column="ALLOW_BOOKING_EXTERNAL_TC"
    )
    dept_id = models.IntegerField(db_column="DEPT_ID")
    dept_eabrv = models.CharField(db_column="DEPT_EABRV", max_length=10)
    dept_fabrv = models.CharField(db_column="DEPT_FABRV", max_length=10)
    dept_edesc = models.CharField(db_column="DEPT_EDESC", max_length=200)
    dept_fdesc = models.CharField(db_column="DEPT_FDESC", max_length=200)
    contact_email_for_candidates = models.CharField(
        db_column="CONTACT_EMAIL_FOR_CANDIDATES", max_length=254, default="temp"
    )
    test_skill_type_id = models.IntegerField(db_column="TEST_SKILL_TYPE_ID")
    test_skill_type_en_name = models.CharField(
        db_column="TEST_SKILL_TYPE_EN_NAME", max_length=150
    )
    test_skill_type_fr_name = models.CharField(
        db_column="TEST_SKILL_TYPE_FR_NAME", max_length=150
    )
    test_skill_sub_type_id = models.IntegerField(db_column="TEST_SKILL_SUB_TYPE_ID")
    test_skill_sub_type_en_name = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_EN_NAME", max_length=150
    )
    test_skill_sub_type_fr_name = models.CharField(
        db_column="TEST_SKILL_SUB_TYPE_FR_NAME", max_length=150
    )
    request_sent = models.BooleanField(db_column="REQUEST_SENT", default=False)

    class Meta:
        managed = False
        db_table = "candidate_reservations_vw"
