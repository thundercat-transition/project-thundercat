from rest_framework import serializers
from db_views.db_view_models.adapted_tests_report_vw import AdaptedTestsReportVW


class AdaptedTestsReportDataViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdaptedTestsReportVW
        fields = "__all__"
