from db_views.db_view_models.user_profile_change_request_vw import (
    UserProfileChangeRequestVW,
)
from rest_framework import serializers


class UserProfileChangeRequestViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfileChangeRequestVW
        fields = "__all__"
