from db_views.db_view_models.ta_extended_profile_vw import TaExtendedProfileVW
from rest_framework import serializers


class TaExtendedProfileViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaExtendedProfileVW
        fields = "__all__"
