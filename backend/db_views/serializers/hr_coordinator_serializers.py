from db_views.db_view_models.assessment_process_results_candidates_report_vw import (
    AssessmentProcessResultsCandidatesReportVW,
)
from db_views.db_view_models.assessment_process_results_vw import (
    AssessmentProcessResultsVW,
)
from db_views.db_view_models.assessment_process_results_candidates_vw import (
    AssessmentProcessResultsCandidatesVW,
)
from db_views.db_view_models.assessment_process_vw import AssessmentProcessVW
from rest_framework import serializers
from db_views.db_view_models.assessment_process_assigned_test_specs_vw import (
    AssessmentProcessAssignedTestSpecsVW,
)
from db_views.db_view_models.assessment_process_default_test_specs_vw import (
    AssessmentProcessDefaultTestSpecsVW,
)
from db_views.db_view_models.assessment_process_vw import AssessmentProcessVW


class AssessmentProcessDefaultTestSpecsViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssessmentProcessDefaultTestSpecsVW
        fields = "__all__"


class SimplifiedAssessmentProcessViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssessmentProcessVW
        fields = "__all__"


class AssessmentProcessViewSerializer(serializers.ModelSerializer):
    tests_to_administer = serializers.SerializerMethodField()

    def get_tests_to_administer(self, request):
        tests_to_administer = AssessmentProcessDefaultTestSpecsVW.objects.filter(
            assessment_process_id=request.id
        )
        return AssessmentProcessDefaultTestSpecsViewSerializer(
            tests_to_administer, many=True
        ).data

    class Meta:
        model = AssessmentProcessVW
        fields = "__all__"


class AssessmentProcessResultsViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssessmentProcessResultsVW
        fields = "__all__"


class AssessmentProcessAssignedTestSpecsViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssessmentProcessAssignedTestSpecsVW
        fields = "__all__"


class AssessmentProcessResultsCandidatesViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssessmentProcessResultsCandidatesVW
        fields = "__all__"


class AssessmentProcessResultsCandidatesReportViewSerializer(
    serializers.ModelSerializer
):
    class Meta:
        model = AssessmentProcessResultsCandidatesReportVW
        fields = "__all__"
