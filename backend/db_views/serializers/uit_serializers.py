from backend.static.assigned_test_status import AssignedTestStatus
from backend.views.utils import UIT_TEST_STATUS
from db_views.db_view_models.uit_related_candidates_vw import UitRelatedCandidatesVW
from db_views.db_view_models.uit_completed_processes_vw import UitCompletedProcessesVW
from rest_framework import serializers


class UitCompletedProcessesViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = UitCompletedProcessesVW
        fields = "__all__"


class UitRelatedCandidatesViewSerializer(serializers.ModelSerializer):
    uit_test_status = serializers.SerializerMethodField()

    def get_uit_test_status(self, request):
        # initializing uit_test_status to NOT_TAKEN
        uit_test_status = UIT_TEST_STATUS.NOT_TAKEN

        # DEACTIVATED
        if (
            request.status_codename is None
            and request.reason_for_deletion_id is not None
        ):
            uit_test_status = UIT_TEST_STATUS.DEACTIVATED
        # ACTIVE
        elif request.status_codename == AssignedTestStatus.ACTIVE:
            uit_test_status = UIT_TEST_STATUS.IN_PROGRESS
        # SUBMITTED/QUIT
        elif (
            request.status_codename == AssignedTestStatus.SUBMITTED
            or request.status_codename == AssignedTestStatus.QUIT
        ):
            uit_test_status = UIT_TEST_STATUS.TAKEN
        # UNASSIGNED
        elif request.status_codename == AssignedTestStatus.UNASSIGNED:
            uit_test_status = UIT_TEST_STATUS.UNASSIGNED
        return uit_test_status

    class Meta:
        model = UitRelatedCandidatesVW
        fields = "__all__"
