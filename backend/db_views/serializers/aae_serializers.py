from rest_framework import serializers
from cms.cms_models.test_section import TestSection
from cms.cms_models.test_definition import TestDefinition
from backend.views.utils import create_language_compatible_object
from backend.custom_models.user_accommodation_file import UserAccommodationFile
from backend.custom_models.user_accommodation_file_test_to_administer import (
    UserAccommodationFileTestToAdminister,
)
from backend.custom_models.user_accommodation_file_status import (
    UserAccommodationFileStatus,
)
from backend.custom_models.user_accommodation_file_status_text import (
    UserAccommodationFileStatusText,
)
from backend.custom_models.user_accommodation_file_complexity import (
    UserAccommodationFileComplexity,
)
from backend.custom_models.user_accommodation_file_complexity_text import (
    UserAccommodationFileComplexityText,
)
from backend.custom_models.user_accommodation_file_comments import (
    UserAccommodationFileComments,
)
from backend.custom_models.user_accommodation_file_test_time import (
    UserAccommodationFileTestTime,
)
from backend.custom_models.user_accommodation_file_break_bank import (
    UserAccommodationFileBreakBank,
)
from backend.custom_models.user_accommodation_file_other_measures import (
    UserAccommodationFileOtherMeasures,
)
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.user_accommodation_file_candidate_limitations import (
    UserAccommodationFileCandidateLimitations,
)
from backend.custom_models.user_accommodation_file_candidate_limitations_option import (
    UserAccommodationFileCandidateLimitationsOption,
)
from backend.custom_models.user_accommodation_file_candidate_limitations_option_text import (
    UserAccommodationFileCandidateLimitationsOptionText,
)
from backend.custom_models.user_accommodation_file_recommendations_option_text import (
    UserAccommodationFileRecommendationsOptionText,
)
from backend.custom_models.user_accommodation_file_recommendations_option import (
    UserAccommodationFileRecommendationsOption,
)
from backend.custom_models.user_accommodation_file_recommendations import (
    UserAccommodationFileRecommendations,
)
from db_views.db_view_models.user_accommodation_file_tests_to_administer_vw import (
    UserAccommodationFileTestsToAdministerVW,
)
from db_views.db_view_models.selected_user_accommodation_file_vw import (
    SelectedUserAccommodationFileVW,
)
from db_views.db_view_models.user_accommodation_file_vw import UserAccommodationFileVW
from db_views.db_view_models.completed_supervised_user_accommodation_file_vw import (
    CompletedSupervisedUserAccommodationFileVW,
)
from db_views.db_view_models.user_accommodation_file_data_for_details_popup_vw import (
    UserAccommodationFileDataForDetailsPopupVW,
)
from db_views.db_view_models.user_accommodation_file_report_data_vw import (
    UserAccommodationFileReportDataVW,
)
from db_views.db_view_models.aae_reports_number_of_closed_requests_vw import (
    AaeReportsNumberOfClosedRequestsVw,
)
from user_management.user_management_models.user_models import User


class UserAccommodationFileViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccommodationFileVW
        fields = "__all__"


class UserAccommodationFileCommentsSerializer(serializers.ModelSerializer):
    user_first_name = serializers.SerializerMethodField()
    user_last_name = serializers.SerializerMethodField()

    def get_user_first_name(self, request):
        user_first_name = User.objects.get(id=request.user_id).first_name
        return user_first_name

    def get_user_last_name(self, request):
        user_last_name = User.objects.get(id=request.user_id).last_name
        return user_last_name

    class Meta:
        model = UserAccommodationFileComments
        fields = "__all__"


class UserAccommodationFileTestToAdministerSerializer(serializers.ModelSerializer):
    test_code = serializers.SerializerMethodField()
    test_version = serializers.SerializerMethodField()
    test_name_en = serializers.SerializerMethodField()
    test_name_fr = serializers.SerializerMethodField()

    def get_test_code(self, request):
        test_code = None
        test_data = TestDefinition.objects.filter(id=request.test_id)
        if test_data:
            test_code = test_data.last().test_code
        return test_code

    def get_test_version(self, request):
        test_version = None
        test_data = TestDefinition.objects.filter(id=request.test_id)
        if test_data:
            test_version = test_data.last().version
        return test_version

    def get_test_name_en(self, request):
        test_name_en = None
        test_data = TestDefinition.objects.filter(id=request.test_id)
        if test_data:
            test_name_en = test_data.last().en_name
        return test_name_en

    def get_test_name_fr(self, request):
        test_name_fr = None
        test_data = TestDefinition.objects.filter(id=request.test_id)
        if test_data:
            test_name_fr = test_data.last().fr_name
        return test_name_fr

    class Meta:
        model = UserAccommodationFileTestToAdminister
        fields = "__all__"


class UserAccommodationFileStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccommodationFileStatus
        fields = "__all__"


class UserAccommodationFileStatusTextSerializer(serializers.ModelSerializer):
    user_accommodation_file_status_codename = serializers.SerializerMethodField()

    def get_user_accommodation_file_status_codename(self, request):
        user_accommodation_file_status_codename = (
            UserAccommodationFileStatus.objects.get(
                id=request.user_accommodation_file_status_id
            ).codename
        )
        return user_accommodation_file_status_codename

    class Meta:
        model = UserAccommodationFileStatusText
        fields = [
            "user_accommodation_file_status_id",
            "user_accommodation_file_status_codename",
            "text",
            "language_id",
        ]


class UserAccommodationFileComplexitySerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccommodationFileComplexity
        fields = "__all__"


class UserAccommodationFileComplexityTextSerializer(serializers.ModelSerializer):
    user_accommodation_file_complexity_codename = serializers.SerializerMethodField()

    def get_user_accommodation_file_complexity_codename(self, request):
        user_accommodation_file_complexity_codename = (
            UserAccommodationFileComplexity.objects.get(
                id=request.user_accommodation_file_complexity_id
            ).codename
        )
        return user_accommodation_file_complexity_codename

    class Meta:
        model = UserAccommodationFileComplexityText
        fields = [
            "user_accommodation_file_complexity_id",
            "user_accommodation_file_complexity_codename",
            "text",
            "language_id",
        ]


class UserAccommodationFileCandidateLimitationsOptionTextSerializer(
    serializers.ModelSerializer
):
    user_accommodation_file_candidate_limitations_option_codename = (
        serializers.SerializerMethodField()
    )

    def get_user_accommodation_file_candidate_limitations_option_codename(
        self, request
    ):
        user_accommodation_file_candidate_limitations_option_codename = (
            UserAccommodationFileCandidateLimitationsOption.objects.get(
                id=request.user_accommodation_file_candidate_limitations_option_id
            ).codename
        )
        return user_accommodation_file_candidate_limitations_option_codename

    class Meta:
        model = UserAccommodationFileCandidateLimitationsOptionText
        fields = [
            "user_accommodation_file_candidate_limitations_option_id",
            "user_accommodation_file_candidate_limitations_option_codename",
            "text",
            "language_id",
        ]


class UserAccommodationFileRecommendationsOptionTextSerializer(
    serializers.ModelSerializer
):
    user_accommodation_file_recommendations_option_codename = (
        serializers.SerializerMethodField()
    )

    def get_user_accommodation_file_recommendations_option_codename(self, request):
        user_accommodation_file_recommendations_option_codename = (
            UserAccommodationFileRecommendationsOption.objects.get(
                id=request.user_accommodation_file_recommendations_option_id
            ).codename
        )
        return user_accommodation_file_recommendations_option_codename

    class Meta:
        model = UserAccommodationFileRecommendationsOptionText
        fields = [
            "user_accommodation_file_recommendations_option_id",
            "user_accommodation_file_recommendations_option_codename",
            "text",
            "language_id",
        ]


class UserAccommodationFileUserSideSerializer(serializers.ModelSerializer):
    status_data = serializers.SerializerMethodField()

    def get_status_data(self, request):
        # initializing status_data
        status_data = {}

        # getting related user accommodation file status text data
        related_status_text_data = UserAccommodationFileStatusText.objects.filter(
            user_accommodation_file_status_id=request.status_id
        )

        # status defined
        if related_status_text_data:
            # serializing data
            serialized_data = UserAccommodationFileStatusTextSerializer(
                related_status_text_data, many=True
            ).data

            status_data = create_language_compatible_object(serialized_data)

        return status_data

    class Meta:
        model = UserAccommodationFile
        fields = ["id", "created_date", "status_data"]


class SelectedUserAccommodationFileViewSerializer(serializers.ModelSerializer):
    test_to_administer = serializers.SerializerMethodField()
    break_bank = serializers.SerializerMethodField()
    test_sections_time = serializers.SerializerMethodField()
    default_test_sections_time = serializers.SerializerMethodField()
    other_measures = serializers.SerializerMethodField()
    file_comments = serializers.SerializerMethodField()
    status_data = serializers.SerializerMethodField()
    complexity_data = serializers.SerializerMethodField()
    candidate_limitations_stats_options = serializers.SerializerMethodField()
    candidate_limitations_stats_selected_options = serializers.SerializerMethodField()
    recommendations_stats_options = serializers.SerializerMethodField()
    recommendations_stats_selected_options = serializers.SerializerMethodField()

    def get_test_to_administer(self, request):
        test_to_administer = {}
        test_to_administer_data = UserAccommodationFileTestToAdminister.objects.filter(
            user_accommodation_file_id=request.id
        )
        if test_to_administer_data:
            test_to_administer = UserAccommodationFileTestToAdministerSerializer(
                test_to_administer_data.last(), many=False
            ).data
        return test_to_administer

    def get_break_bank(self, request):
        break_bank = 0
        break_bank_data = UserAccommodationFileBreakBank.objects.filter(
            user_accommodation_file_id=request.id
        )
        if break_bank_data:
            break_bank = break_bank_data.last().break_time
        return break_bank

    def get_test_sections_time(self, request):
        test_sections_time = []
        # checking if there is an entry in User Accommodation File Test Time model
        user_accommodation_file_test_time = (
            UserAccommodationFileTestTime.objects.filter(
                user_accommodation_file_id=request.id
            )
        )
        # match found
        if user_accommodation_file_test_time:
            # looping in found entries
            for test_time_data in user_accommodation_file_test_time:
                # test section ID is defined
                if test_time_data.test_section_id is not None:
                    # getting respective test section data
                    respective_test_section_data = TestSection.objects.get(
                        id=test_time_data.test_section_id
                    )
                    # populating test_sections_time array
                    test_sections_time.append(
                        {
                            "test_section_id": test_time_data.test_section_id,
                            "test_section_time": test_time_data.test_section_time,
                            "title_en": respective_test_section_data.en_title,
                            "title_fr": respective_test_section_data.fr_title,
                        }
                    )
                # test section ID is NULL
                else:
                    # populating test_sections_time array
                    test_sections_time.append(
                        {
                            "test_section_id": None,
                            "test_section_time": test_time_data.test_section_time,
                        }
                    )
        # no match found
        else:
            # checking if this is a request from a UIT test (that means that we have an assigned test ID)
            if request.is_uit:
                # getting respective assigned test data
                respective_assigned_test_data = AssignedTest.objects.filter(
                    user_accommodation_file_id=request.id
                ).last()
                # getting default test sections time
                default_test_sections_time_data = TestSection.objects.filter(
                    test_definition_id=respective_assigned_test_data.test_id,
                    default_time__isnull=False,
                )
                # looping in default_test_sections_time_data
                for test_section_time_data in default_test_sections_time_data:
                    # getting respective test section data
                    respective_test_section_data = TestSection.objects.get(
                        id=test_section_time_data.id
                    )
                    # populating test_sections_time array
                    test_sections_time.append(
                        {
                            "test_section_id": test_section_time_data.id,
                            "test_section_time": test_section_time_data.default_time,
                            "title_en": respective_test_section_data.en_title,
                            "title_fr": respective_test_section_data.fr_title,
                        }
                    )

        return test_sections_time

    def get_default_test_sections_time(self, request):
        default_test_sections_time = []

        # checking if there is an entry in User Accommodation File Test Time model
        user_accommodation_file_test_time = (
            UserAccommodationFileTestTime.objects.filter(
                user_accommodation_file_id=request.id
            )
        )

        # match found
        if user_accommodation_file_test_time:
            # looping in found entries
            for test_time_data in user_accommodation_file_test_time:
                # test section ID is defined
                if test_time_data.test_section_id is not None:
                    # getting respective test section data
                    respective_test_section_data = TestSection.objects.get(
                        id=test_time_data.test_section_id
                    )

                    # populating default_test_time
                    default_test_sections_time.append(
                        {
                            "test_section_id": respective_test_section_data.id,
                            "default_time": respective_test_section_data.default_time,
                        }
                    )
                # test section ID is NULL
                else:
                    # populating test_sections_time array
                    default_test_sections_time.append(
                        {
                            "test_section_id": None,
                            "default_time": 0,
                        }
                    )

        # no match found
        else:
            # checking if this is a request from a UIT test (that means that we have an assigned test ID)
            if request.is_uit:
                # getting respective assigned test data
                respective_assigned_test_data = AssignedTest.objects.filter(
                    user_accommodation_file_id=request.id
                ).last()
                # getting default test sections time
                default_test_sections_time_data = TestSection.objects.filter(
                    test_definition_id=respective_assigned_test_data.test_id,
                    default_time__isnull=False,
                )
                # looping in default_test_sections_time_data
                for test_section_time_data in default_test_sections_time_data:
                    # populating default_test_time
                    default_test_sections_time.append(
                        {
                            "test_section_id": test_section_time_data.id,
                            "default_time": test_section_time_data.default_time,
                        }
                    )

        return default_test_sections_time

    def get_other_measures(self, request):
        other_measures = ""
        # getting other measures data
        other_measures_data = UserAccommodationFileOtherMeasures.objects.filter(
            user_accommodation_file_id=request.id
        )
        # match found
        if other_measures_data:
            other_measures = other_measures_data.last().other_measures
        return other_measures

    def get_file_comments(self, request):
        file_comments = UserAccommodationFileComments.objects.filter(
            user_accommodation_file_id=request.id
        )
        serialized_data = UserAccommodationFileCommentsSerializer(
            file_comments, many=True
        ).data

        ordered_serialized_data = sorted(
            serialized_data,
            key=lambda k: k["modify_date"],
            reverse=True,
        )
        return ordered_serialized_data

    def get_status_data(self, request):
        # initializing status_data
        status_data = {}

        # getting related user accommodation file status text data
        related_status_text_data = UserAccommodationFileStatusText.objects.filter(
            user_accommodation_file_status_id=request.status_id
        )

        # status defined
        if related_status_text_data:
            # serializing data
            serialized_data = UserAccommodationFileStatusTextSerializer(
                related_status_text_data, many=True
            ).data

            status_data = create_language_compatible_object(serialized_data)

        return status_data

    def get_complexity_data(self, request):
        # initializing complexity_data
        complexity_data = {}

        # getting related user accommodation file complexity text data
        related_complexity_text_data = (
            UserAccommodationFileComplexityText.objects.filter(
                user_accommodation_file_complexity_id=request.complexity_id
            )
        )

        # complexity defined
        if related_complexity_text_data:
            # serializing data
            serialized_data = UserAccommodationFileComplexityTextSerializer(
                related_complexity_text_data, many=True
            ).data

            complexity_data = create_language_compatible_object(serialized_data)

        return complexity_data

    def get_candidate_limitations_stats_options(self, request):
        candidate_limitations_stats_options = []
        active_candidate_limitations_ids = (
            UserAccommodationFileCandidateLimitationsOption.objects.filter(
                active=1
            ).values_list("id", flat=True)
        )
        for active_candidate_limitations_id in active_candidate_limitations_ids:
            candidate_limitations = UserAccommodationFileCandidateLimitationsOptionText.objects.filter(
                user_accommodation_file_candidate_limitations_option_id=active_candidate_limitations_id
            )
            serialized_data = (
                UserAccommodationFileCandidateLimitationsOptionTextSerializer(
                    candidate_limitations, many=True
                ).data
            )
            candidate_limitations_stats_options.append(
                create_language_compatible_object(serialized_data)
            )

        return candidate_limitations_stats_options

    def get_candidate_limitations_stats_selected_options(self, request):
        candidate_limitations_stats_selected_options = (
            UserAccommodationFileCandidateLimitations.objects.filter(
                user_accommodation_file_id=request.id
            )
            .values_list(
                "user_accommodation_file_candidate_limitations_option_id", flat=True
            )
            .order_by("user_accommodation_file_candidate_limitations_option_id")
        )
        return candidate_limitations_stats_selected_options

    def get_recommendations_stats_options(self, request):
        recommendations_stats_options = []
        active_recommendations_ids = (
            UserAccommodationFileRecommendationsOption.objects.filter(
                active=1
            ).values_list("id", flat=True)
        )
        for active_recommendations_id in active_recommendations_ids:
            recommendations = UserAccommodationFileRecommendationsOptionText.objects.filter(
                user_accommodation_file_recommendations_option_id=active_recommendations_id
            )
            serialized_data = UserAccommodationFileRecommendationsOptionTextSerializer(
                recommendations, many=True
            ).data
            recommendations_stats_options.append(
                create_language_compatible_object(serialized_data)
            )

        return recommendations_stats_options

    def get_recommendations_stats_selected_options(self, request):
        recommendations_stats_selected_options = (
            UserAccommodationFileRecommendations.objects.filter(
                user_accommodation_file_id=request.id
            )
            .values_list("user_accommodation_file_recommendations_option_id", flat=True)
            .order_by("user_accommodation_file_recommendations_option_id")
        )
        return recommendations_stats_selected_options

    class Meta:
        model = SelectedUserAccommodationFileVW
        fields = "__all__"


class UserAccommodationFileTestsToAdministerViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccommodationFileTestsToAdministerVW
        fields = "__all__"


class CompletedSupervisedUserAccommodationFileViewSerializer(
    serializers.ModelSerializer
):
    class Meta:
        model = CompletedSupervisedUserAccommodationFileVW
        fields = "__all__"


class UserAccommodationFileDataForDetailsPopupViewSerializer(
    serializers.ModelSerializer
):
    status_data = serializers.SerializerMethodField()

    def get_status_data(self, request):
        # initializing status_data
        status_data = {}

        # getting related user accommodation file status text data
        related_status_text_data = UserAccommodationFileStatusText.objects.filter(
            user_accommodation_file_status_id=request.status_id
        )

        # status defined
        if related_status_text_data:
            # serializing data
            serialized_data = UserAccommodationFileStatusTextSerializer(
                related_status_text_data, many=True
            ).data

            status_data = create_language_compatible_object(serialized_data)

        return status_data

    class Meta:
        model = UserAccommodationFileDataForDetailsPopupVW
        fields = "__all__"


class UserAccommodationFileReportDataViewSerializer(serializers.ModelSerializer):
    status_data = serializers.SerializerMethodField()

    def get_status_data(self, request):
        # initializing status_data
        status_data = {}

        # getting related user accommodation file status text data
        related_status_text_data = UserAccommodationFileStatusText.objects.filter(
            user_accommodation_file_status_id=request.status_id
        )

        # status defined
        if related_status_text_data:
            # serializing data
            serialized_data = UserAccommodationFileStatusTextSerializer(
                related_status_text_data, many=True
            ).data

            status_data = create_language_compatible_object(serialized_data)

        return status_data

    class Meta:
        model = UserAccommodationFileReportDataVW
        fields = "__all__"


class AaeReportsNumberOfClosedRequestsViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = AaeReportsNumberOfClosedRequestsVw
        fields = "__all__"
