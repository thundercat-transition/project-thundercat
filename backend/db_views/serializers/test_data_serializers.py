from db_views.db_view_models.test_data_vw import (
    TestDataVW,
)
from rest_framework import serializers


class TestDataViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDataVW
        fields = "__all__"
