from itertools import groupby
from rest_framework import serializers
from backend.custom_models.test_center_field_translations.test_center_other_details_text import TestCenterOtherDetailsText
from backend.custom_models.test_center_field_translations.test_center_province_text import TestCenterProvinceText
from backend.custom_models.test_center_field_translations.test_center_city_text import TestCenterCityText
from backend.custom_models.language import Language
from backend.custom_models.test_center_field_translations.test_center_address_text import TestCenterAddressText
from db_views.serializers.test_center_serializers import TestCenterOtherDetailsTextSerializer
from db_views.serializers.test_center_serializers import TestCenterProvinceTextSerializer
from db_views.serializers.test_center_serializers import TestCenterCityTextSerializer
from db_views.serializers.test_center_serializers import TestCenterAddressTextSerializer
from db_views.db_view_models.booked_reservations_vw import (
    BookedReservationsVW,
)
from backend.views.utils import create_language_compatible_object

class BookedReservationsViewSerializer(serializers.ModelSerializer):
    test_center_address_text = serializers.SerializerMethodField()
    test_center_city_text = serializers.SerializerMethodField()
    test_center_province_text = serializers.SerializerMethodField()
    test_center_other_details_text = serializers.SerializerMethodField()

    def get_test_center_address_text(self, request):
        test_center_address_text = TestCenterAddressText.objects.filter(test_center_id=request.id).order_by("language_id")

        serialized_data = TestCenterAddressTextSerializer(
            test_center_address_text, many=True
        ).data

        return create_language_compatible_object(serialized_data)

    def get_test_center_city_text(self, request):
        test_center_city_text = TestCenterCityText.objects.filter(test_center_id=request.id).order_by("language_id")

        serialized_data = TestCenterCityTextSerializer(
            test_center_city_text, many=True
        ).data

        return create_language_compatible_object(serialized_data)

    def get_test_center_province_text(self, request):
        test_center_province_text = TestCenterProvinceText.objects.filter(test_center_id=request.id).order_by("language_id")

        serialized_data = TestCenterProvinceTextSerializer(
            test_center_province_text, many=True
        ).data

        return create_language_compatible_object(serialized_data)

    def get_test_center_other_details_text(self, request):
        test_center_other_details_text = TestCenterOtherDetailsText.objects.filter(test_center_id=request.id).order_by("language_id")

        serialized_data = TestCenterOtherDetailsTextSerializer(
            test_center_other_details_text, many=True
        ).data

        return create_language_compatible_object(serialized_data)
    class Meta:
        model = BookedReservationsVW
        fields = "__all__"
