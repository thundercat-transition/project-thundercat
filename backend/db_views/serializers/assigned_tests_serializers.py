from django.utils import timezone
from datetime import timedelta
import datetime
import pytz
from db_views.db_view_models.user_look_up_tests_vw import UserLookUpTestsVW
from db_views.db_view_models.scored_tests_vw import ScoredTestsVW
from db_views.db_view_models.assigned_tests_vw import AssignedTestsVW
from db_views.db_view_models.ta_assigned_candidates_vw import TaAssignedCandidatesVW
from db_views.db_view_models.active_tests_vw import ActiveTestsVW
from rest_framework import serializers
from backend.custom_models.additional_time import AdditionalTime
from backend.views.utils import get_last_accessed_test_section
from backend.custom_models.break_bank import BreakBank
from backend.custom_models.break_bank_actions import BreakBankActions
from backend.custom_models.accommodation_request import AccommodationRequest
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.custom_models.ta_actions import TaActions
from backend.custom_models.lock_test_actions import LockTestActions
from backend.views.test_administrator_actions import TaActionsConstants
from backend.static.assigned_test_status import (
    AssignedTestStatus,
    get_assigned_test_status_id,
)
from backend.custom_models.assigned_test_section_access_times import (
    AssignedTestSectionAccessTimes,
)
from backend.static.assigned_test_section_access_time_type import (
    AssignedTestSectionAccessTimeType,
)
from cms.cms_models.test_section import TestSection
from cms.serializers.test_section_serializer import AssignedTestSectionSerializer
from cms.views.test_break_bank import BreakBankActionsConstants
from cms.views.retrieve_test_section_data import (
    get_total_assigned_test_time_in_seconds,
    get_updated_time_during_lock,
)


class ActiveTestsViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActiveTestsVW
        fields = "__all__"


class TaAssignedCandidatesViewSerializer(serializers.ModelSerializer):
    assigned_test_sections = serializers.SerializerMethodField()

    pause_test_time = serializers.SerializerMethodField()
    pause_start_date = serializers.SerializerMethodField()
    test_time_remaining = serializers.SerializerMethodField()
    test_time_updated = serializers.SerializerMethodField()

    def get_assigned_test_sections(self, assigned_test):
        assigned_test_sections = AssignedTestSection.objects.filter(
            assigned_test_id=assigned_test.id
        )
        return AssignedTestSectionSerializer(assigned_test_sections, many=True).data

    def get_pause_test_time(self, assigned_test):
        # defined accommodation request
        if assigned_test.accommodation_request_id is not None:
            # getting break bank ID
            break_bank_id = AccommodationRequest.objects.get(
                id=assigned_test.accommodation_request_id
            ).break_bank_id
            # existing break bank accommodation request
            if break_bank_id is not None:
                # getting break bank actions related to current test section where action_type is "UNPAUSE" (if they exist)
                unpause_break_bank_actions = BreakBankActions.objects.filter(
                    break_bank_id=break_bank_id,
                    action_type=BreakBankActionsConstants.UNPAUSE,
                ).order_by("modify_date")

                # unpause break bank actions contains data
                if unpause_break_bank_actions:
                    pause_test_time = (
                        # divided by 60 to get the time in minutes (provided in seconds)
                        unpause_break_bank_actions.last().new_remaining_time
                        / 60
                    )

                # no unpause break bank actions
                else:
                    # getting total time of the break bank
                    # divided by 60 to get the time in minutes (provided in seconds)
                    pause_test_time = (
                        BreakBank.objects.get(id=break_bank_id).break_time / 60
                    )
            # non-existing break bank accommodation request
            else:
                pause_test_time = None
        # undefined accommodation request
        else:
            pause_test_time = None
        return pause_test_time

    def get_pause_start_date(self, assigned_test):
        # defined accommodation request
        if assigned_test.accommodation_request_id is not None:
            # getting break bank ID
            break_bank_id = AccommodationRequest.objects.get(
                id=assigned_test.accommodation_request_id
            ).break_bank_id
            # existing break bank accommodation request
            if break_bank_id is not None:
                # getting last accessed assigned test section ID
                last_accessed_test_section_id = get_last_accessed_test_section(
                    assigned_test.id
                )
                # getting break bank actions related to current test section where action_type is "PAUSE" (if they exist)
                pause_break_bank_actions = BreakBankActions.objects.filter(
                    break_bank_id=break_bank_id,
                    action_type=BreakBankActionsConstants.PAUSE,
                    test_section_id=last_accessed_test_section_id,
                ).order_by("modify_date")
                # unpause break bank actions contains data
                if pause_break_bank_actions:
                    pause_start_date = pause_break_bank_actions.last().modify_date
                else:
                    pause_start_date = None
            # non-existing break bank accommodation request
            else:
                pause_start_date = None
        # undefined accommodation request
        else:
            pause_start_date = None

        return pause_start_date

    # TODO: fix the logic for multi sections tests
    def get_test_time_remaining(self, assigned_test):
        # initialize the variables
        test_section_start_time = 0
        test_section_updated_start_time = 0
        test_section_default_time = 0
        lock_start_date = 0
        consumed_pause_time = 0
        current_time = datetime.datetime.now(pytz.utc)
        test_time_elapsed = 0
        test_time_remaining = 0

        if assigned_test.test_section_id:
            # get assigned test section
            assigned_test_section = AssignedTestSection.objects.filter(
                assigned_test_id=assigned_test.id,
                test_section_id=assigned_test.test_section_id,
            )

            # assigned test section is defined
            if assigned_test_section:
                # get the start time of the test
                assigned_test_section_start_time = AssignedTestSectionAccessTimes.objects.filter(
                    # first assigned test section
                    assigned_test_section_id=assigned_test_section.first().id,
                    time_type=AssignedTestSectionAccessTimeType.START,
                ).first()

                if assigned_test_section_start_time is not None:
                    test_section_start_time = assigned_test_section_start_time.time

                # test section default time
                test_section_default_time = (
                    assigned_test_section.first().test_section_time
                )

            # to get lock start date of test
            try:
                ta_action_id_2 = (
                    TaActions.objects.filter(
                        assigned_test_id=assigned_test.id,
                        action_type_id=TaActionsConstants.LOCK,
                    )
                    .last()
                    .id
                )

                lock_start_date = LockTestActions.objects.get(
                    ta_action_id=ta_action_id_2
                ).lock_start_date

            except:
                lock_start_date = 0

            # existing accommodation request
            if assigned_test.accommodation_request_id is not None:
                # getting accommodation request
                accommodation_request_data = AccommodationRequest.objects.get(
                    id=assigned_test.accommodation_request_id
                )
                # getting break bank ID
                break_bank_id = accommodation_request_data.break_bank_id
                # break bank ID exists
                if break_bank_id:
                    # getting last accessed assigned test section ID
                    last_accessed_test_section_id = get_last_accessed_test_section(
                        assigned_test.id
                    )
                    # getting break bank actions related to current test section (if they exist)
                    break_bank_actions = BreakBankActions.objects.filter(
                        break_bank_id=break_bank_id,
                        test_section_id=last_accessed_test_section_id,
                    ).order_by("modify_date")
                    # existing break bank actions
                    if break_bank_actions:
                        # getting total break bank time
                        total_break_bank_time = BreakBank.objects.get(
                            id=break_bank_id
                        ).break_time
                        # if last break bank action is a PAUSE
                        if (
                            break_bank_actions.last().action_type
                            == BreakBankActionsConstants.PAUSE
                        ):
                            # checking for a previous UNPAUSE action
                            try:
                                previous_unpause_action = break_bank_actions[
                                    len(break_bank_actions) - 2
                                ]
                            except:
                                previous_unpause_action = None
                                pass
                            # calculating time between now and last PAUSE
                            time_between_now_and_last_pause_action = (
                                timezone.now() - break_bank_actions.last().modify_date
                            ).total_seconds()

                            # there is a previous unpause action
                            if previous_unpause_action is not None:
                                # calculating remaining time
                                remaining_time = datetime.timedelta(
                                    seconds=previous_unpause_action.new_remaining_time
                                ) - datetime.timedelta(
                                    seconds=time_between_now_and_last_pause_action
                                )

                                # updating consumed_pause_time
                                consumed_pause_time = (
                                    total_break_bank_time
                                    - remaining_time.total_seconds()
                                )

                            # there is no previous unpause action
                            else:
                                # calculating remaining time
                                remaining_time = datetime.timedelta(
                                    seconds=total_break_bank_time
                                ) - datetime.timedelta(
                                    seconds=time_between_now_and_last_pause_action
                                )

                                # updating consumed_pause_time
                                consumed_pause_time = (
                                    total_break_bank_time
                                    - remaining_time.total_seconds()
                                )
                        # if last break bank action is an UNPAUSE
                        else:
                            # caucliating consumed pause time (total break time - last UNPAUSE new remaining time)
                            consumed_pause_time = (
                                total_break_bank_time
                                - break_bank_actions.last().new_remaining_time
                            )

            # test is active or paused or locked and previous status is not checked-in or pre-test
            active_status_id = get_assigned_test_status_id(AssignedTestStatus.ACTIVE)
            locked_status_id = get_assigned_test_status_id(AssignedTestStatus.LOCKED)
            paused_status_id = get_assigned_test_status_id(AssignedTestStatus.PAUSED)
            checked_in_status_id = get_assigned_test_status_id(
                AssignedTestStatus.CHECKED_IN
            )
            pre_test_status_id = get_assigned_test_status_id(
                AssignedTestStatus.PRE_TEST
            )
            if (
                (
                    assigned_test.status_id == active_status_id
                    or assigned_test.status_id == locked_status_id
                    or assigned_test.status_id == paused_status_id
                )
                and assigned_test.previous_status_id != checked_in_status_id
                and assigned_test.previous_status_id != pre_test_status_id
            ):
                # get updated time of the test to check previous pause/lock actions
                if consumed_pause_time != 0 or lock_start_date != 0:
                    test_section_updated_start_time = get_updated_time_during_lock(
                        assigned_test.id, assigned_test.test_section_id
                    )
                    # if the time is updated since the beginning of the test then get the updated time
                    if test_section_updated_start_time is not None:
                        test_section_start_time = test_section_updated_start_time

                if (
                    current_time >= test_section_start_time
                    and test_section_default_time is not None
                ):
                    test_section_default_time_delta = timedelta(
                        minutes=test_section_default_time
                    ).total_seconds()

                    # if test status is active or paused
                    if (
                        assigned_test.status_id == active_status_id
                        or assigned_test.status_id == paused_status_id
                    ):
                        # getting elapsed time of the test
                        test_time_elapsed = current_time - test_section_start_time
                        # calculating time remaining
                        test_time_remaining = datetime.timedelta(
                            seconds=test_section_default_time_delta
                        ) - (
                            test_time_elapsed
                            - datetime.timedelta(seconds=consumed_pause_time)
                        )

                    # if test status is Locked
                    elif assigned_test.status_id == locked_status_id:
                        # getting elapsed time of the test
                        test_time_elapsed = lock_start_date - test_section_start_time
                        # calculating time remaining
                        test_time_remaining = datetime.timedelta(
                            seconds=test_section_default_time_delta
                        ) - (
                            test_time_elapsed
                            - datetime.timedelta(seconds=consumed_pause_time)
                        )
            # return total time of the test
            else:
                test_time_remaining = get_total_assigned_test_time_in_seconds(
                    assigned_test.id
                )
        else:
            test_time_remaining = get_total_assigned_test_time_in_seconds(
                assigned_test.id
            )

        return test_time_remaining

    def get_test_time_updated(self, assigned_test):
        # there is no accommodation request associated to this assigned test
        if assigned_test.accommodation_request_id is None:
            return False
        # accommodation request associated to this assigned test
        else:
            # getting related test section IDs
            test_section_ids = AssignedTestSection.objects.filter(
                assigned_test_id=assigned_test.id
            ).values_list("test_section_id", flat=True)
            # getting test sections data (where default time is defined)
            test_sections = TestSection.objects.filter(
                id__in=test_section_ids, default_time__isnull=False
            )
            # looping in test_sections
            for test_section in test_sections:
                # checking if there is some additional time set based on the current test section and the accommodation request data
                if AdditionalTime.objects.filter(
                    accommodation_request_id=assigned_test.accommodation_request_id,
                    test_section_id=test_section.id,
                ):
                    return True
            return False

    class Meta:
        model = TaAssignedCandidatesVW
        fields = "__all__"


class AssignedTestsViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssignedTestsVW
        fields = "__all__"


class ScoredTestsViewSerializer(serializers.ModelSerializer):
    utc_submit_date = serializers.SerializerMethodField()
    utc_retest_period_date = serializers.SerializerMethodField()

    def get_utc_submit_date(self, request):
        if request.submit_date is not None:
            utc_submit_date = request.submit_date.astimezone(pytz.utc).strftime(
                "%Y-%m-%d %H:%M:%S%z"
            )
            return utc_submit_date
        else:
            return None

    def get_utc_retest_period_date(self, request):
        if request.submit_date is not None:
            utc_retest_period_date = (
                request.submit_date.astimezone(pytz.utc)
                + datetime.timedelta(days=request.retest_period)
            ).strftime("%Y-%m-%d %H:%M:%S%z")
            return utc_retest_period_date
        else:
            return None

    class Meta:
        model = ScoredTestsVW
        fields = "__all__"


class UserLookUpTestsViewSerializer(serializers.ModelSerializer):

    utc_submit_date = serializers.SerializerMethodField()

    def get_utc_submit_date(self, request):
        if request.submit_date is not None:
            utc_submit_date = request.submit_date.astimezone(pytz.utc).strftime(
                "%Y-%m-%d %H:%M:%S%z"
            )
            return utc_submit_date
        else:
            return None

    class Meta:
        model = UserLookUpTestsVW
        fields = "__all__"
