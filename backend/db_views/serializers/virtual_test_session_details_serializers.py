from db_views.db_view_models.virtual_test_session_details_vw import (
    VirtualTestSessionDetailsVW,
)
from rest_framework import serializers


class VirtualTestSessionDetailsViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = VirtualTestSessionDetailsVW
        fields = "__all__"
