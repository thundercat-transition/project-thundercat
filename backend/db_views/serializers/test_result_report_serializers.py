from db_views.db_view_models.test_result_report_vw import TestResultReportVW
from rest_framework import serializers


class TestResultReportDataViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestResultReportVW
        fields = "__all__"
