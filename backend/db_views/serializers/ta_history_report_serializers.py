from db_views.db_view_models.ta_history_report_vw import TaHistoryReportVW
from rest_framework import serializers


class TaHistoryReportDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaHistoryReportVW
        fields = "__all__"
