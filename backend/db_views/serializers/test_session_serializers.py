from django.utils import timezone
from rest_framework import serializers
from db_views.db_view_models.test_session_attendees_vw import TestSessionAttendeesVW


class TestSessionAttendeesViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestSessionAttendeesVW
        fields = "__all__"
