from db_views.db_view_models.test_order_and_reference_numbers_vw import (
    TestOrderAndReferenceNumbersVW,
)
from rest_framework import serializers


class TestOrderAndReferenceNumbersViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestOrderAndReferenceNumbersVW
        fields = "__all__"
