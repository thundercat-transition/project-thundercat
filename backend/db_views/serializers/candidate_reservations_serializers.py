from rest_framework import serializers
from db_views.db_view_models.candidate_reservations_vw import (
    CandidateReservationsVW,
)


class CandidateReservationsViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = CandidateReservationsVW
        fields = "__all__"
