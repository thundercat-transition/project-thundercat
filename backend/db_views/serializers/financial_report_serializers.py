from db_views.db_view_models.financial_report_vw import FinancialReportVW
from rest_framework import serializers


class FinancialReportDataViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = FinancialReportVW
        fields = "__all__"
