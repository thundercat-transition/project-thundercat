from rest_framework import serializers
from db_views.db_view_models.custom_permission_2fa_vw import (
    CustomPermission2FAVW,
)


class CustomPermission2FAViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomPermission2FAVW
        fields = "__all__"
