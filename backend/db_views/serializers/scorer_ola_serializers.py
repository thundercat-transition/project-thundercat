from rest_framework import serializers
from backend.custom_models.test_center_ola_test_assessor import (
    TestCenterOlaTestAssessor,
)
from backend.custom_models.test_center_ola_time_slot import TestCenterOlaTimeSlot

from db_views.db_view_models.test_center_ola_test_assessor_unavailability_vw import (
    TestCenterOlaTestAssessorUnavailabilityVw,
)
from db_views.db_view_models.scorer_ola_detailed_tests_to_assess_vw import (
    ScorerOlaDetailedTestsToAssessVw,
)
from db_views.db_view_models.scorer_ola_tests_to_assess_vw import (
    ScorerOlaTestsToAssessVw,
)


class ScorerOlaTestsToAssessViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScorerOlaTestsToAssessVw
        fields = "__all__"


class ScorerOlaDetailedTestsToAssessViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScorerOlaDetailedTestsToAssessVw

        fields = "__all__"


class TestCenterOlaTestAssessorUnavailabilityViewSerializer(
    serializers.ModelSerializer
):
    class Meta:
        model = TestCenterOlaTestAssessorUnavailabilityVw

        fields = "__all__"


class ScorerOlaDetailedTestsToAssessAsSupervisorViewSerializer(
    serializers.ModelSerializer
):
    is_supervisor = serializers.SerializerMethodField()

    def get_is_supervisor(self, request):
        # initializing is_supervisor
        is_supervisor = False

        # getting user_id from provided context
        user_id = self.context.get("user_id", None)

        # getting user's related test center IDs (where user is set as a supervisor)
        related_test_center_ids = TestCenterOlaTestAssessor.objects.filter(
            user_id=user_id, supervisor=1
        ).values_list("test_center_id", flat=True)

        # looping in related_test_center_ids
        for test_center_id in related_test_center_ids:
            if TestCenterOlaTimeSlot.objects.filter(
                test_center_id=test_center_id,
                day_of_week_id=request.day_of_week_id,
                start_time=request.start_time,
                end_time=request.end_time,
            ):
                # setting is_supervisor to True
                is_supervisor = True
                # breaking the loop
                break

        return is_supervisor

    class Meta:
        model = ScorerOlaDetailedTestsToAssessVw
        fields = "__all__"
