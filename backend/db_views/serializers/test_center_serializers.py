from django.utils import timezone
import datetime
from rest_framework import serializers
from backend.custom_models.test_center_room_data import TestCenterRoomData
from backend.custom_models.test_center_test_sessions import TestCenterTestSessions
from backend.custom_models.test_center_test_session_data import (
    TestCenterTestSessionData,
)
from backend.custom_models.scorer_ola_test_session_skip_option_text import (
    ScorerOlaTestSessionSkipOptionText,
)
from backend.custom_models.scorer_ola_test_session_skip_option import (
    ScorerOlaTestSessionSkipOption,
)
from backend.custom_models.test_center_ola_test_assessor_approved_language import (
    TestCenterOlaTestAssessorApprovedLanguage,
)
from backend.custom_models.test_center_ola_test_assessor import (
    TestCenterOlaTestAssessor,
)
from backend.custom_models.test_center_ola_configs import TestCenterOlaConfigs
from backend.views.utils import (
    create_language_compatible_object,
    get_current_utc_offset_in_seconds,
)
from backend.custom_models.test_center_rooms import TestCenterRooms
from backend.custom_models.test_center_field_translations.test_center_room_data_other_details_text import (
    TestCenterRoomDataOtherDetailsText,
)
from backend.custom_models.test_center_field_translations.test_center_address_text import (
    TestCenterAddressText,
)
from backend.custom_models.test_center_field_translations.test_center_city_text import (
    TestCenterCityText,
)
from backend.custom_models.test_center_field_translations.test_center_province_text import (
    TestCenterProvinceText,
)
from backend.custom_models.test_center_field_translations.test_center_other_details_text import (
    TestCenterOtherDetailsText,
)
from backend.custom_models.consumed_reservation_codes import (
    ConsumedReservationCodes,
)

from db_views.db_view_models.test_center_test_sessions_vw import (
    TestCenterTestSessionsVW,
)
from db_views.db_view_models.test_center_rooms_vw import TestCenterRoomsVW
from db_views.db_view_models.test_center_test_administrators_vw import (
    TestCenterTestAdministratorsVW,
)
from db_views.db_view_models.test_centers_vw import TestCentersVW
from db_views.db_view_models.test_center_ola_vacation_block_vw import (
    TestCenterOlaVacationBlockVw,
)
from db_views.db_view_models.test_center_ola_test_assessors_vw import (
    TestCenterOlaTestAssessorsVw,
)
from db_views.db_view_models.test_center_ola_time_slots_vw import (
    TestCenterOlaTimeSlotsVw,
)
from db_views.db_view_models.test_center_ola_available_time_slots_vw import (
    TestCenterOlaAvailableTimeSlotsVw,
)
from backend.custom_models.test_center_associated_managers import (
    TestCenterAssociatedManagers,
)
from backend.serializers.test_center_serializer import (
    TestCenterAssociatedManagersSerializer,
)
import pytz


# Function used to get Test Center Address Text
def get_test_center_address_text(test_center_id):
    test_center_address_text = TestCenterAddressText.objects.filter(
        test_center_id=test_center_id
    ).order_by("language_id")

    serialized_data = TestCenterAddressTextSerializer(
        test_center_address_text, many=True
    ).data

    return create_language_compatible_object(serialized_data)


# Function used to get Test Center City Text
def get_test_center_city_text(test_center_id):
    test_center_city_text = TestCenterCityText.objects.filter(
        test_center_id=test_center_id
    ).order_by("language_id")

    serialized_data = TestCenterCityTextSerializer(
        test_center_city_text, many=True
    ).data

    return create_language_compatible_object(serialized_data)


# Function used to get Test Center Province Text
def get_test_center_province_text(test_center_id):
    test_center_province_text = TestCenterProvinceText.objects.filter(
        test_center_id=test_center_id
    ).order_by("language_id")

    serialized_data = TestCenterProvinceTextSerializer(
        test_center_province_text, many=True
    ).data

    return create_language_compatible_object(serialized_data)


# Function used to get Test Center Other Details Text
def get_test_center_other_details_text(test_center_id):
    test_center_other_details_text = TestCenterOtherDetailsText.objects.filter(
        test_center_id=test_center_id
    ).order_by("language_id")

    serialized_data = TestCenterOtherDetailsTextSerializer(
        test_center_other_details_text, many=True
    ).data

    return create_language_compatible_object(serialized_data)


# Function used to get Test Room Other Details Text
def get_test_room_other_details_text(test_center_room_id):
    # Get Test Center Rooms with the ID Provided so we can have the room data id
    test_center_room = TestCenterRooms.objects.get(id=test_center_room_id)

    # Get all other details text linked to our room data id
    test_center_other_details_text = TestCenterRoomDataOtherDetailsText.objects.filter(
        test_center_room_data=test_center_room.room_data
    ).order_by("language_id")

    serialized_data = TestCenterRoomDataOtherDetailsTextSerializer(
        test_center_other_details_text, many=True
    ).data

    return create_language_compatible_object(serialized_data)


class TestCentersViewSerializer(serializers.ModelSerializer):
    test_center_address_text = serializers.SerializerMethodField()
    test_center_city_text = serializers.SerializerMethodField()
    test_center_province_text = serializers.SerializerMethodField()
    test_center_other_details_text = serializers.SerializerMethodField()

    def get_test_center_address_text(self, request):
        return get_test_center_address_text(request.id)

    def get_test_center_city_text(self, request):
        return get_test_center_city_text(request.id)

    def get_test_center_province_text(self, request):
        return get_test_center_province_text(request.id)

    def get_test_center_other_details_text(self, request):
        return get_test_center_other_details_text(request.id)

    class Meta:
        model = TestCentersVW
        fields = "__all__"


class SelectedTestCenterAsEttaDetailedViewSerializer(serializers.ModelSerializer):
    test_center_accesses = serializers.SerializerMethodField()
    test_center_address_text = serializers.SerializerMethodField()
    test_center_city_text = serializers.SerializerMethodField()
    test_center_province_text = serializers.SerializerMethodField()
    test_center_other_details_text = serializers.SerializerMethodField()

    def get_test_center_accesses(self, request):
        test_center_accesses = TestCenterAssociatedManagers.objects.filter(
            test_center_id=request.id
        )
        return TestCenterAssociatedManagersSerializer(
            test_center_accesses, many=True
        ).data

    def get_test_center_address_text(self, request):
        return get_test_center_address_text(request.id)

    def get_test_center_city_text(self, request):
        return get_test_center_city_text(request.id)

    def get_test_center_province_text(self, request):
        return get_test_center_province_text(request.id)

    def get_test_center_other_details_text(self, request):
        return get_test_center_other_details_text(request.id)

    class Meta:
        model = TestCentersVW
        fields = "__all__"


class TestCenterOlaConfigsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestCenterOlaConfigs
        fields = "__all__"


class TestCenterOlaVacationBlockViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestCenterOlaVacationBlockVw
        fields = "__all__"


class TestCenterOlaTestAssessorsViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestCenterOlaTestAssessorsVw
        fields = "__all__"


class TestCenterOlaTimeSlotsViewSerializer(serializers.ModelSerializer):
    start_time = serializers.SerializerMethodField()
    end_time = serializers.SerializerMethodField()

    def get_start_time(self, request):
        # getting current UTC Offset
        current_utc_offset = get_current_utc_offset_in_seconds()

        # there is a difference between the current_utc_offset and the utc_offset of the time slot
        if request.utc_offset != current_utc_offset:
            # initializing offset difference
            offset_difference_in_seconds = int(request.utc_offset - current_utc_offset)
            # adjusting start time based on the difference between those two utc offsets
            adjusted_start_time = (
                datetime.datetime.combine(datetime.date.today(), request.start_time)
                + datetime.timedelta(seconds=offset_difference_in_seconds)
            ).time()
            return adjusted_start_time

        # no difference between the utc offsets
        else:
            # returning start time without adjustment
            return request.start_time

    def get_end_time(self, request):
        # getting current UTC Offset
        current_utc_offset = get_current_utc_offset_in_seconds()

        # there is a difference between the current_utc_offset and the utc_offset of the time slot
        if request.utc_offset != current_utc_offset:
            # initializing offset difference
            offset_difference_in_seconds = int(request.utc_offset - current_utc_offset)
            # adjusting start time based on the difference between those two utc offsets
            adjusted_end_time = (
                datetime.datetime.combine(datetime.date.today(), request.end_time)
                + datetime.timedelta(seconds=offset_difference_in_seconds)
            ).time()
            return adjusted_end_time

        # no difference between the utc offsets
        else:
            # returning start time without adjustment
            return request.end_time

    class Meta:
        model = TestCenterOlaTimeSlotsVw
        fields = "__all__"


class TestCenterOlaAvailableTimeSlotsViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestCenterOlaAvailableTimeSlotsVw
        fields = "__all__"


class SelectedTestCenterDetailedViewSerializer(serializers.ModelSerializer):
    test_administrators = serializers.SerializerMethodField()
    rooms = serializers.SerializerMethodField()
    test_center_address_text = serializers.SerializerMethodField()
    test_center_city_text = serializers.SerializerMethodField()
    test_center_province_text = serializers.SerializerMethodField()
    test_center_other_details_text = serializers.SerializerMethodField()
    test_center_ola_configs = serializers.SerializerMethodField()
    test_center_ola_vacation_blocks = serializers.SerializerMethodField()
    test_center_ola_test_assessors = serializers.SerializerMethodField()
    test_center_ola_time_slots = serializers.SerializerMethodField()

    def get_test_administrators(self, request):
        test_administrators = TestCenterTestAdministratorsVW.objects.filter(
            test_center_id=request.id
        )
        return TestCenterTestAdministratorsViewSerializer(
            test_administrators, many=True
        ).data

    def get_rooms(self, request):
        rooms = TestCenterRoomsVW.objects.filter(test_center_id=request.id)
        return TestCenterRoomsViewSerializer(rooms, many=True).data

    def get_test_center_address_text(self, request):
        return get_test_center_address_text(request.id)

    def get_test_center_city_text(self, request):
        return get_test_center_city_text(request.id)

    def get_test_center_province_text(self, request):
        return get_test_center_province_text(request.id)

    def get_test_center_other_details_text(self, request):
        return get_test_center_other_details_text(request.id)

    def get_test_center_ola_configs(self, request):
        test_center_ola_config = TestCenterOlaConfigs.objects.filter(
            test_center_id=request.id
        )

        if test_center_ola_config:
            test_center_ola_config = test_center_ola_config.last()
            return TestCenterOlaConfigsSerializer(
                test_center_ola_config, many=False
            ).data
        else:
            return None

    def get_test_center_ola_vacation_blocks(self, request):
        test_center_ola_vacation_blocks = TestCenterOlaVacationBlockVw.objects.filter(
            test_center_id=request.id
        )

        if test_center_ola_vacation_blocks:
            serialized_data = TestCenterOlaVacationBlockViewSerializer(
                test_center_ola_vacation_blocks, many=True
            ).data

            # ordering serialized data by date_from & date_to
            ordered_serialized_data = sorted(
                serialized_data,
                key=lambda k: (k["date_from"], k["date_to"]),
                reverse=False,
            )

            return ordered_serialized_data

        else:
            return []

    def get_test_center_ola_test_assessors(self, request):
        test_center_ola_test_assessors = TestCenterOlaTestAssessorsVw.objects.filter(
            test_center_id=request.id
        )

        if test_center_ola_test_assessors:
            serialized_data = TestCenterOlaTestAssessorsViewSerializer(
                test_center_ola_test_assessors, many=True
            ).data

            # ordering serialized data by last name & first name
            ordered_serialized_data = sorted(
                serialized_data,
                key=lambda k: (k["user_last_name"], k["user_first_name"]),
                reverse=False,
            )

            return ordered_serialized_data

        else:
            return []

    def get_test_center_ola_time_slots(self, request):
        test_center_ola_time_slots = TestCenterOlaTimeSlotsVw.objects.filter(
            test_center_id=request.id
        )

        if test_center_ola_time_slots:
            serialized_data = TestCenterOlaTimeSlotsViewSerializer(
                test_center_ola_time_slots, many=True
            ).data

            # ordering serialized data by assessed_language, day of week ID & start time
            ordered_serialized_data = sorted(
                serialized_data,
                key=lambda k: (
                    k["assessed_language_id"],
                    k["day_of_week_id"],
                    k["start_time"],
                ),
                reverse=False,
            )

            return ordered_serialized_data

        else:
            return []

    class Meta:
        model = TestCentersVW
        fields = "__all__"


class TestCenterTestAdministratorsViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestCenterTestAdministratorsVW
        fields = "__all__"


class TestCenterRoomsViewSerializer(serializers.ModelSerializer):
    other_details_text = serializers.SerializerMethodField()

    def get_other_details_text(self, request):
        return get_test_room_other_details_text(request.id)

    class Meta:
        model = TestCenterRoomsVW
        fields = "__all__"


class TestCenterTestSessionDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestCenterTestSessionData
        fields = [
            "id",
            "open_to_ogd",
            "date",
            "start_time",
            "end_time",
            "spaces_available",
            "test_skill_sub_type_id",
            "modify_date",
            "test_center_room_id",
            "test_skill_type_id",
        ]


class TestCenterTestSessionsSerializer(serializers.ModelSerializer):
    test_session_data = serializers.SerializerMethodField()
    simplified_start_time = serializers.SerializerMethodField()
    simplified_end_time = serializers.SerializerMethodField()
    utc_start_time = serializers.SerializerMethodField()
    utc_end_time = serializers.SerializerMethodField()
    room_name = serializers.SerializerMethodField()

    def get_test_session_data(self, request):
        test_session_data = TestCenterTestSessionData.objects.get(
            id=request.test_session_data_id
        )
        serialized_data = TestCenterTestSessionDataSerializer(
            test_session_data, many=False
        ).data
        return serialized_data

    def get_simplified_start_time(self, request):
        start_time = TestCenterTestSessionData.objects.get(
            id=request.test_session_data_id
        ).start_time
        if start_time is not None:
            # only returning in "hh:mm" format
            simplified_start_time = start_time.astimezone(
                timezone.get_current_timezone()
            ).strftime("%H:%M")
            return simplified_start_time
        else:
            return None

    def get_simplified_end_time(self, request):
        end_time = TestCenterTestSessionData.objects.get(
            id=request.test_session_data_id
        ).end_time
        if end_time is not None:
            # only returning in "hh:mm" format
            simplified_end_time = end_time.astimezone(
                timezone.get_current_timezone()
            ).strftime("%H:%M")
            return simplified_end_time
        else:
            return None

    def get_utc_start_time(self, request):
        start_time = TestCenterTestSessionData.objects.get(
            id=request.test_session_data_id
        ).start_time
        if start_time is not None:
            utc_start_time = start_time.astimezone(pytz.utc).strftime(
                "%Y-%m-%d %H:%M:%S%z"
            )
            return utc_start_time
        else:
            return None

    def get_utc_end_time(self, request):
        end_time = TestCenterTestSessionData.objects.get(
            id=request.test_session_data_id
        ).end_time
        if end_time is not None:
            utc_end_time = end_time.astimezone(pytz.utc).strftime("%Y-%m-%d %H:%M:%S%z")
            return utc_end_time
        else:
            return None

    def get_room_name(self, request):
        try:
            test_center_room_id = TestCenterTestSessionData.objects.get(
                id=request.test_session_data_id
            ).test_center_room_id
            room_data_id = TestCenterRooms.objects.get(
                id=test_center_room_id
            ).room_data_id
            room_name = TestCenterRoomData.objects.get(id=room_data_id).name
        except:
            room_name = ""
        return room_name

    class Meta:
        model = TestCenterTestSessions
        fields = [
            "id",
            "test_session_data",
            "simplified_start_time",
            "simplified_end_time",
            "utc_start_time",
            "utc_end_time",
            "room_name",
            "is_standard",
            "test_session_order",
            "test_center_id",
            "user_accommodation_file_id",
            "assessment_process_assigned_test_specs_id",
        ]


class TestCenterTestSessionsViewSerializer(serializers.ModelSerializer):
    simplified_start_time = serializers.SerializerMethodField()
    simplified_end_time = serializers.SerializerMethodField()
    utc_start_time = serializers.SerializerMethodField()
    utc_end_time = serializers.SerializerMethodField()
    test_center_address_text = serializers.SerializerMethodField()
    test_center_city_text = serializers.SerializerMethodField()
    test_center_province_text = serializers.SerializerMethodField()
    test_center_other_details_text = serializers.SerializerMethodField()
    test_room_other_details_text = serializers.SerializerMethodField()

    def get_simplified_start_time(self, request):
        if request.start_time is not None:
            # only returning in "hh:mm" format
            simplified_start_time = request.start_time.astimezone(
                timezone.get_current_timezone()
            ).strftime("%H:%M")
            return simplified_start_time
        else:
            return None

    def get_simplified_end_time(self, request):
        if request.end_time is not None:
            # only returning in "hh:mm" format
            simplified_end_time = request.end_time.astimezone(
                timezone.get_current_timezone()
            ).strftime("%H:%M")
            return simplified_end_time
        else:
            return None

    def get_utc_start_time(self, request):
        if request.start_time is not None:
            utc_start_time = request.start_time.astimezone(pytz.utc).strftime(
                "%Y-%m-%d %H:%M:%S%z"
            )
            return utc_start_time
        else:
            return None

    def get_utc_end_time(self, request):
        if request.end_time is not None:
            utc_end_time = request.end_time.astimezone(pytz.utc).strftime(
                "%Y-%m-%d %H:%M:%S%z"
            )
            return utc_end_time
        else:
            return None

    def get_test_center_address_text(self, request):
        return get_test_center_address_text(request.test_center_id)

    def get_test_center_city_text(self, request):
        return get_test_center_city_text(request.test_center_id)

    def get_test_center_province_text(self, request):
        return get_test_center_province_text(request.test_center_id)

    def get_test_center_other_details_text(self, request):
        return get_test_center_other_details_text(request.test_center_id)

    def get_test_room_other_details_text(self, request):
        if request.test_center_room_id is not None:
            return get_test_room_other_details_text(request.test_center_room_id)
        else:
            return None

    class Meta:
        model = TestCenterTestSessionsVW
        fields = "__all__"


class ConsumedReservationCodesViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConsumedReservationCodes
        fields = "__all__"


class TestCenterAddressTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestCenterAddressText
        fields = ["id", "text", "test_center_id", "language_id"]


class TestCenterCityTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestCenterCityText
        fields = ["id", "text", "test_center_id", "language_id"]


class TestCenterProvinceTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestCenterProvinceText
        fields = ["id", "text", "test_center_id", "language_id"]


class TestCenterOtherDetailsTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestCenterOtherDetailsText
        fields = ["id", "text", "test_center_id", "language_id"]


class TestCenterRoomDataOtherDetailsTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestCenterRoomDataOtherDetailsText
        fields = ["id", "text", "test_center_room_data_id", "language_id"]


class TestCenterOlaTestAssessorSerializer(serializers.ModelSerializer):
    approved_language = serializers.SerializerMethodField()

    def get_approved_language(self, request):
        final_array = []
        approved_language_ids = (
            TestCenterOlaTestAssessorApprovedLanguage.objects.filter(
                test_center_ola_test_assessor_id=request.id
            ).values_list("language", flat=True)
        )

        for approved_language_id in approved_language_ids:
            final_array.append(approved_language_id)
        return final_array

    class Meta:
        model = TestCenterOlaTestAssessor
        fields = ["id", "test_center_id", "approved_language"]


class ScorerOlaTestSessionSkipOptionTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScorerOlaTestSessionSkipOptionText
        fields = ["id", "text", "scorer_ola_test_session_skip_option_id", "language_id"]


class ScorerOlaTestSessionSkipOptionSerializer(serializers.ModelSerializer):
    scorer_ola_test_session_skip_option_text = serializers.SerializerMethodField()

    def get_scorer_ola_test_session_skip_option_text(self, request):
        scorer_ola_test_session_skip_option_text = (
            ScorerOlaTestSessionSkipOptionText.objects.filter(
                scorer_ola_test_session_skip_option_id=request.id
            ).order_by("language_id")
        )

        serialized_data = ScorerOlaTestSessionSkipOptionTextSerializer(
            scorer_ola_test_session_skip_option_text, many=True
        ).data

        return create_language_compatible_object(serialized_data)

    class Meta:
        model = ScorerOlaTestSessionSkipOption
        fields = "__all__"
