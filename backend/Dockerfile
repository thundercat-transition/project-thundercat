# Pull base image from https://hub.docker.com/_/python/
FROM python:3.11.2

# Set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV TZ America/Toronto

# Set work directory
RUN mkdir /backend
WORKDIR /backend

# Install dependencies
ADD requirements.txt /backend/

# new for ms sql set up
RUN apt-get update \
    && apt-get install -y --no-install-recommends g++ unixodbc-dev curl \
    && rm -rf /var/lib/apt/lists/* \
    && pip install -r requirements.txt \
    && apt-get purge -y --auto-remove g++

# pyodbc needs gcc and g++ to compile, and unixodbc-dev to run. g++ is installed and removed after the build, but unixodbc is kept on the image

# apt-get and system utilities
RUN apt-get update && apt-get install -y \
    curl apt-utils apt-transport-https debconf-utils gcc build-essential g++\
    && rm -rf /var/lib/apt/lists/*

# adding custom MS repository
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list

# install SQL Server drivers
RUN apt-get update && ACCEPT_EULA=Y apt-get -y install msodbcsql17
# unixODBC dev headers
RUN apt-get -y install unixodbc-dev

# install GraphViz & PyGraphViz
RUN apt install --assume-yes graphviz graphviz-dev
RUN pip install --no-cache-dir pygraphviz

# Copy project
ADD . /backend/

CMD ["/bin/sh", "entrypoint.sh"]
EXPOSE 8000
