#!bin/bash

# faking all migrations (since the image should already contain all the needed tables and static data)
python manage.py migrate --fake

# showing migrations (for debugging purposes)
python manage.py showmigrations

# starting web server
python manage.py runserver 0.0.0.0:8000
