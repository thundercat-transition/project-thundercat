import os
import django
from channels.routing import get_default_application

from config.environment import SETTINGS_MODULE

# will be needed for PROD

os.environ.setdefault("DJANGO_SETTINGS_MODULE", SETTINGS_MODULE)
django.setup()
application = get_default_application()
