from .base import *
import os
from datetime import timedelta
from backend.celery import tasks

DEBUG = True

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "mssql",
        "NAME": "master",
        "USER": "SA",
        "PASSWORD": "someSecurePassword10!",
        "HOST": "db",  # set in docker-compose.yml
        "PORT": 1433,  # ms sql port,
        # odbc driver installed
        "OPTIONS": {"driver": "ODBC Driver 17 for SQL Server"},
    }
}

# INSTALLED_APPS.append("debug_toolbar")

# MIDDLEWARE.append("debug_toolbar.middleware.DebugToolbarMiddleware")

STATIC_URL = "/static/"
STATIC_ROOT = os.path.join(BASE_DIR, "static/")

MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media/")
IS_LOCAL = True

SECRET_KEY = "9t7j9n-@yf!8(xtohmf3k85f&qdateph(j7-_0pn-8iwu$l)xj"

# celery
CELERY_BROKER_URL = "redis://project-thundercat-redis-1:6379/0"
CELERY_RESULT_BACKEND = "redis://project-thundercat-redis-1:6379/0"
CELERY_ACCEPT_CONTENT = ["json"]
CELERY_TASK_SERIALIZER = "json"
CELERY_TIMEZONE = "UTC"

# Redis
REDIS_HOST = "redis"
REDIS_PORT = 6379
REDIS_DB = 0

# TODO (fnormand): setup this setting once django v3.1 and higher will be deployed
# validity period (in seconds)
# PASSWORD_RESET_TIMEOUT = 5
# time in hours
DJANGO_REST_MULTITOKENAUTH_RESET_TOKEN_EXPIRY_TIME = 1

# console email (comment if testing with code above)
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
EMAIL_HOST_USER = "noreply@somehost.local"

SITE_PROTOCOL = "http://"
SITE_DOMAIN = "localhost:81"

CELERY_BEAT_SCHEDULE = {
    # ========== ASSIGNED TESTS TASKS ==========
    # running this one every minute locally for testing purposes
    "run_every_60_minutes_3": {
        "task": "backend.celery.tasks.run_every_60_minutes_3",
        "schedule": timedelta(minutes=1),
    },
    # running this one every minute locally for testing purposes
    "run_every_morning_at_03_00": {
        "task": "backend.celery.tasks.run_every_morning_at_03_00",
        "schedule": timedelta(minutes=1),
    },
    # running this one every minute locally for testing purposes
    "run_every_60_minutes": {
        "task": "backend.celery.tasks.run_every_60_minutes",
        "schedule": timedelta(minutes=1),
    },
    # running this one every minute locally for testing purposes
    "run_every_15_minutes": {
        "task": "backend.celery.tasks.run_every_15_minutes",
        "schedule": timedelta(minutes=1),
    },
    # deprecated celery task execution (might need to re-enable it in the future)
    # running this one every minute locally for testing purposes
    # "run_every_morning_at_03_30": {
    #     "task": "backend.celery.tasks.run_every_morning_at_03_30",
    #     "schedule": timedelta(minutes=1),
    # },
    # ========== ASSIGNED TESTS TASKS (END) ==========
    # ========== TEST PERMISSIONS TASKS ==========
    # running this one every minute locally for testing purposes
    "run_every_morning_at_01_30": {
        "task": "backend.celery.tasks.run_every_morning_at_01_30",
        "schedule": timedelta(minutes=1),
    },
    # ========== TEST PERMISSIONS TASKS (END) ==========
    # ========== TEST ACCESS CODES TASKS ==========
    # running this one every minute locally for testing purposes
    "run_every_morning_at_01_35": {
        "task": "backend.celery.tasks.run_every_morning_at_01_35",
        "schedule": timedelta(minutes=1),
    },
    # ========== TEST ACCESS CODES TASKS (END) ==========
    # ========== UIT TASKS ==========
    # running this one every minute locally for testing purposes
    "run_every_morning_at_01_01": {
        "task": "backend.celery.tasks.run_every_morning_at_01_01",
        "schedule": timedelta(minutes=1),
    },
    # running this one every minute locally for testing purposes
    "run_every_morning_at_01_50": {
        "task": "backend.celery.tasks.run_every_morning_at_01_50",
        "schedule": timedelta(minutes=1),
    },
    # ========== UIT TASKS (END) ==========
    # ========== USER UPDATE TASKS ==========
    "run_every_morning_at_01_10": {
        "task": "backend.celery.tasks.run_every_morning_at_01_10",
        "schedule": timedelta(minutes=1),
    },
    # ========== USER UPDATE TASKS (END) ==========
    # ========== DEPROVISION TASKS ==========
    # running this one every minute locally for testing purposes
    "run_every_morning_at_02_30": {
        "task": "backend.celery.tasks.run_every_morning_at_02_30",
        "schedule": timedelta(minutes=1),
    },
    #
    # Commented so it doesn't affect accounts that we don't use often locally
    #
    # "run_every_morning_at_01_20": {
    #     "task": "backend.celery.tasks.run_every_morning_at_01_20",
    #     "schedule": timedelta(minutes=1),
    # },
    # running this one every morning at 01:15AM (5:15AM UTC)
    "run_every_morning_at_01_15": {
        "task": "backend.celery.tasks.trigger_clear_expired_gc_notify_tokens",
        # UTC time (5:10AM)
        "schedule": timedelta(minutes=1),
    },
    # ========== DEPROVISION TASKS (END) ==========
    # ========== TEST CENTER TASKS ==========
    # running this one every minute locally for testing purposes
    "run_every_60_minutes_2": {
        "task": "backend.celery.tasks.run_every_60_minutes_2",
        "schedule": timedelta(minutes=1),
    },
    # running this one every minute locally for testing purposes
    "run_every_morning_at_01_15": {
        "task": "backend.celery.tasks.run_every_morning_at_01_15",
        "schedule": timedelta(minutes=1),
    },
    # running this one every morning at 01:45AM (5:45AM UTC)
    "run_every_morning_at_01_45": {
        "task": "backend.celery.tasks.run_every_morning_at_01_45",
        "schedule": timedelta(minutes=1),
    },
    # ========== TEST CENTER TASKS (END) ==========
    # ========== ASSESSMENT PROCESS TASKS ==========
    # running this one every minute locally for testing purposes
    "run_every_morning_at_01_40": {
        "task": "backend.celery.tasks.run_every_morning_at_01_40",
        "schedule": timedelta(minutes=1),
    },
    # ========== ASSESSMENT PROCESS TASKS (END) ==========
    # ========== TEST SESSIONS TASKS ==========
    # running this one every minute locally for testing purposes
    # Commented so it doesn't always spam the console (uncomment for testing)
    # "run_every_morning_at_02_00": {
    #     "task": "backend.celery.tasks.run_every_morning_at_02_00",
    #     "schedule": timedelta(minutes=1),
    # },
    # ========== TEST SESSIONS TASKS (END) ==========
    # ========== 2FA TASKS ==========
    "run_every_60_minutes_4": {
        "task": "backend.celery.tasks.run_every_60_minutes_4",
        "schedule": timedelta(minutes=1),
    },
    # ========== 2FA TASKS (END) ==========
    # ========== GC NOTIFY TASKS ==========
    # running this one every minute locally for testing purposes
    "trigger_check_gc_notify_delivery_status": {
        "task": "backend.celery.tasks.trigger_check_gc_notify_delivery_status",
        "schedule": timedelta(minutes=1),
    },
    # running this one every 2 mins for local testing purposes
    "trigger_check_for_orphaned_bulk_delivery_failues": {
        "task": "backend.celery.tasks.trigger_check_for_orphaned_bulk_delivery_failues",
        "schedule": timedelta(minutes=2),
    },
    # ========== GC NOTIFY TASKS (END) ==========
    # ========== SPAM TASKS ==========
    "run_every_morning_at_01_55": {
        "task": "backend.celery.tasks.run_every_morning_at_01_55",
        "schedule": timedelta(minutes=1),
    },
    # ========== SPAM TASKS (END) ==========
}
