import os

SETTINGS_MODULE = "config.settings.local"

if os.environ.get("ENVIRONMENT") == "kube":
    SETTINGS_MODULE = "config.settings.kube"

if os.environ.get("ENVIRONMENT") == "legacy":
    SETTINGS_MODULE = "config.settings.legacy"
