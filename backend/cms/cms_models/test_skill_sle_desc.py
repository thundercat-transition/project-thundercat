from django.db import models
from simple_history.models import HistoricalRecords


# ------------------------------------------------------------------------------ #
# This Model is being used to create a Test Skill SLE Desc by attributing a codename
#
# Example: We have a Test Skill SLE Type of "WE" (Writing English)
# ------------------------------------------------------------------------------ #
class TestSkillSLEDesc(models.Model):
    codename = models.CharField(max_length=25, unique=True, blank=False, null=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    def __str__(self):
        ret = "ID: {0} Test Skill SLE Desc Codename: {1}".format(self.id, self.codename)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Test Skill - SLE Description's Codename"
