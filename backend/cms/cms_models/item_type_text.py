from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.item_type import ItemType
from backend.custom_models.language import Language


class ItemTypeText(models.Model):

    # FK to item type
    item_type = models.ForeignKey(ItemType, to_field="id", on_delete=models.DO_NOTHING)

    text = models.CharField(max_length=30, blank=False, null=False)

    language = models.ForeignKey(
        Language,
        to_field="language_id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
    )

    # For tracking
    modify_date = models.DateTimeField(auto_now=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "item type id: {0}, text: {1}".format(self.item_type_id, self.text)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "ItemTypeText"
