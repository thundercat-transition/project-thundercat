from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.item_development_statuses import ItemDevelopmentStatuses
from cms.cms_models.item_response_formats import ItemResponseFormats
from cms.cms_models.item_bank import ItemBank
from user_management.user_management_models.user_models import User
from cms.cms_models.item_type import ItemType


class Items(models.Model):
    system_id = models.CharField(max_length=15, blank=False, null=False)
    version = models.IntegerField(blank=False, null=False)
    modify_date = models.DateTimeField(auto_now=True)
    item_bank = models.ForeignKey(
        ItemBank, to_field="id", on_delete=models.DO_NOTHING, default=1
    )
    active_status = models.BooleanField(default=0)
    development_status = models.ForeignKey(
        ItemDevelopmentStatuses, to_field="id", on_delete=models.DO_NOTHING
    )
    response_format = models.ForeignKey(
        ItemResponseFormats, to_field="id", on_delete=models.DO_NOTHING, null=True
    )
    shuffle_options = models.BooleanField(default=0)
    last_modified_by_user = models.ForeignKey(
        User,
        to_field="id",
        on_delete=models.DO_NOTHING,
        null=False,
        related_name="i_last_modified_by_user",
    )

    item_type = models.ForeignKey(
        ItemType, to_field="id", on_delete=models.DO_NOTHING, default=1
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}, system ID: {1}, version: {2}".format(
            self.id, self.system_id, self.version
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Items"
        constraints = [
            models.UniqueConstraint(
                name="system_id_combined_with_version_must_be_unique_in_item_bank",
                fields=["system_id", "version", "item_bank"],
            )
        ]
