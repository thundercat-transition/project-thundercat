from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.item_bank_attribute_values import ItemBankAttributeValues
from cms.cms_models.items import Items


class ItemAttributeValue(models.Model):
    item = models.ForeignKey(Items, to_field="id", on_delete=models.DO_NOTHING)
    item_bank_attribute_value = models.ForeignKey(
        ItemBankAttributeValues, to_field="id", on_delete=models.DO_NOTHING
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "item_id: {0}, item_bank_attribute_value_id: {1}".format(
            self.item, self.item_bank_attribute_value
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Attribute Values"
