from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.bundles import Bundles
from cms.cms_models.item_bank import ItemBank
from cms.cms_models.test_section_component import TestSectionComponent
from backend.custom_models.language import Language
from cms.cms_models.content_type import ContentType


class ContentTypeText(models.Model):
    # FK to content type
    content_type = models.ForeignKey(
        ContentType, to_field="id", on_delete=models.DO_NOTHING
    )

    text = models.CharField(max_length=30, default="temp")

    language = models.ForeignKey(
        Language, to_field="language_id", on_delete=models.DO_NOTHING
    )

    # For tracking
    modify_date = models.DateTimeField(auto_now=True)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate history table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0} -> {1}  ({2})".format(self.id, self.text, self.content_type)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Content Type Text"
