from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.item_bank import ItemBank


class ItemBankAttributes(models.Model):
    attribute_order = models.IntegerField(blank=False, null=False)
    item_bank = models.ForeignKey(ItemBank, to_field="id", on_delete=models.DO_NOTHING)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}".format(self.id)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Bank Attributes"
