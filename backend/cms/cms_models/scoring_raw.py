from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.scoring_methods import ScoringMethods


class ScoringRaw(models.Model):
    scoring_method = models.ForeignKey(ScoringMethods, on_delete=models.DO_NOTHING)
    result_valid_indefinitely = models.BooleanField(
        null=False, blank=False, default=False
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate history table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0} scoring method id: {1} result valid indefinitely: {2}".format(
            self.id, self.scoring_method, self.result_valid_indefinitely
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Scoring Raw"
