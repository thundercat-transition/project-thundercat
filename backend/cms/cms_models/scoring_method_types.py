from django.db import models
from simple_history.models import HistoricalRecords

MAX_CHAR_LEN = 100


class ScoringMethodTypes(models.Model):
    method_type_codename = models.CharField(max_length=50, null=False, blank=False)
    en_name = models.TextField()
    fr_name = models.TextField()

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate history table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0} method type: {1}".format(self.id, self.en_name)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Scoring Method Types"
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_scoring_method_type_codename",
                fields=["method_type_codename"],
            )
        ]
