from django.db import models
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_skill_type import TestSkillType
from simple_history.models import HistoricalRecords


# ------------------------------------------------------------------------ #
# This Model is being used to link a Test Skill to a Test Definition
#
# Example: A specific test definition has a test skill of "SLE" (Codename)
# ------------------------------------------------------------------------ #
class TestSkill(models.Model):
    test_skill_type = models.ForeignKey(TestSkillType, on_delete=models.DO_NOTHING)
    test_definition = models.ForeignKey(TestDefinition, on_delete=models.DO_NOTHING)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    def __str__(self):
        ret = "ID: {0} Test Definition: {1} Test Skill Type: {2}".format(
            self.id, self.test_definition, self.test_skill_type
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Test Skill"
