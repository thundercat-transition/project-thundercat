from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.test_skill_sle_desc import TestSkillSLEDesc
from backend.custom_models.language import Language


# ------------------------------------------------------------------------------ #
# This Model is being used to create the Test Skill SLE Types Text
#
# Example: We have a Test Skill SLE Type of "WE" and it is being displayed
# in the dropdown of the app in English as "Writing English"
# ------------------------------------------------------------------------------ #
class TestSkillSLEDescText(models.Model):
    text = models.CharField(max_length=150, blank=False, null=False)
    language = models.ForeignKey(
        Language, to_field="language_id", on_delete=models.DO_NOTHING
    )
    test_skill_sle_desc = models.ForeignKey(
        TestSkillSLEDesc, on_delete=models.DO_NOTHING
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    def __str__(self):
        ret = "ID: {0} Text: {1} Language: {2} Test Skill SLE Desc: {3}".format(
            self.id, self.text, self.language, self.test_skill_sle_desc
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Test Skill - SLE Description's Text"
