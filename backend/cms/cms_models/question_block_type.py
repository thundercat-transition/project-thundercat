from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.test_definition import TestDefinition


MAX_CHAR_LEN = 150

# this model replaces the old question model


class QuestionBlockType(models.Model):
    name = models.CharField(max_length=MAX_CHAR_LEN)
    test_definition = models.ForeignKey(TestDefinition, on_delete=models.DO_NOTHING)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate history table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0} type name: {1}".format(self.id, self.name)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Question Block Type"
