from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.page_section_type_survey_link import PageSectionTypeSurveyLink
from cms.cms_models.page_section import PageSection
from backend.custom_models.language import Language


class PageSectionTypeSurveyLinkText(models.Model):
    text = models.CharField(max_length=150, null=False, blank=False)
    page_section_type_survey_link = models.ForeignKey(
        PageSectionTypeSurveyLink,
        to_field="id",
        null=False,
        blank=False,
        on_delete=models.DO_NOTHING,
    )
    language = models.ForeignKey(
        Language,
        to_field="language_id",
        null=False,
        blank=False,
        on_delete=models.DO_NOTHING,
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console

    def __str__(self):
        ret = "({0}) [{1}] [{2}] [{3}]".format(
            self.id, self.text, self.page_section_type_survey_link, self.language
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Page Section Type Survey Link Text"
