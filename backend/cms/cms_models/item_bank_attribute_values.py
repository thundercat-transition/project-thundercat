from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.item_bank_attributes import ItemBankAttributes
from cms.static.item_bank_utils import AttributeValueType


class ItemBankAttributeValues(models.Model):
    attribute_value_order = models.IntegerField(blank=False, null=False)
    item_bank_attribute = models.ForeignKey(
        ItemBankAttributes, to_field="id", on_delete=models.DO_NOTHING
    )
    attribute_value_type = models.IntegerField(default=AttributeValueType.TEXT)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}, attribute value type: {1}".format(
            self.id, self.attribute_value_type
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Bank Attribute Values"
