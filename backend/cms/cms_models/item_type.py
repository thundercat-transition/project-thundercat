from django.db import models
from simple_history.models import HistoricalRecords


class ItemType(models.Model):
    codename = models.CharField(max_length=25, unique=True, blank=False, null=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "codename: {0}".format(
            self.codename
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "ItemType"
