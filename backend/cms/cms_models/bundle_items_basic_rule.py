from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.bundle_rule import BundleRule


class BundleItemsBasicRule(models.Model):
    number_of_items = models.IntegerField(null=False, blank=False)
    bundle_rule = models.ForeignKey(
        BundleRule, to_field="id", on_delete=models.DO_NOTHING
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}, number of items: {1}, bundle rule ID: {2}".format(
            self.id, self.number_of_items, self.bundle_rule
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Bundle Items Basic Rule"
