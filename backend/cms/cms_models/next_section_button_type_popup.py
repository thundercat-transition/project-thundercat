from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.test_section import TestSection
from backend.custom_models.language import Language


MAX_BTN_TXT_LEN = 50
MAX_TITLE_LEN = 150
MAX_POPUP_CONTENT_LEN = 500


class NextSectionButtonTypePopup(models.Model):
    test_section = models.ForeignKey(TestSection, on_delete=models.DO_NOTHING)
    language = models.ForeignKey(
        Language, on_delete=models.DO_NOTHING, to_field="ISO_Code_1"
    )
    content = models.TextField(max_length=MAX_POPUP_CONTENT_LEN)
    title = models.TextField(max_length=MAX_TITLE_LEN)
    button_text = models.CharField(max_length=MAX_BTN_TXT_LEN)
    confirm_proceed = models.BooleanField(default=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console

    def __str__(self):
        ret = "{0}: {1} ({2}) {3}".format(
            self.test_section.test_definition.test_code,
            self.test_section.en_title,
            self.language,
            self.content,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Next Section Button Type Popup"
