from django.db import models
from cms.cms_models.test_skill import TestSkill
from cms.cms_models.test_skill_sle_desc import TestSkillSLEDesc
from simple_history.models import HistoricalRecords


# ------------------------------------------------------------------------ #
# This Model is being used to link a Test Skill to a Test Skill SLE Desc
#
# When we have SLE as the Test Skill Type for our Test Definition, we can get
# the Sub-Test Skill SLE type
#
# Example: Test Skill Type SLE with a Test Skill SLE Desc of Codename: WE (Writing English)
# ------------------------------------------------------------------------ #
class TestSkillSLE(models.Model):
    test_skill = models.ForeignKey(TestSkill, on_delete=models.DO_NOTHING)
    test_skill_sle_desc = models.ForeignKey(
        TestSkillSLEDesc, on_delete=models.DO_NOTHING
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    def __str__(self):
        ret = "ID: {0} Test Skill: {1} Test Skill SLE Desc: {2}".format(
            self.id, self.test_skill, self.test_skill_sle_desc
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Test Skill - SLE"
