from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.new_question import NewQuestion


class Answer(models.Model):
    question = models.ForeignKey(NewQuestion, on_delete=models.DO_NOTHING)
    ppc_answer_id = models.CharField(max_length=25, default="")
    scoring_value = models.IntegerField(default=0, blank=False, null=False)
    order = models.IntegerField(default=0)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console

    def __str__(self):
        ret = "question id: {0}, ppc_answer_id: {1} answerid: {2}, score: {3}, order: {4}".format(
            self.question.id,
            self.ppc_answer_id,
            self.id,
            self.scoring_value,
            self.order,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Answer"
