from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.page_section import PageSection
from cms.cms_models.items import Items
from backend.custom_models.language import Language


class PageSectionTypeItemBankInstruction(models.Model):
    system_id = models.CharField(max_length=15, blank=False, null=False, default="TEMP")
    page_section = models.ForeignKey(PageSection, on_delete=models.DO_NOTHING)
    content_type_id = models.IntegerField()

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console

    def __str__(self):
        ret = "({0}) [{1}] [{2}]".format(
            self.page_section.section_component_page,
            self.system_id,
            self.content_type_id,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Page Section Type Item Bank Instruction"
        constraints = [
            models.UniqueConstraint(
                fields=["system_id", "page_section", "content_type_id"],
                name="no duplicate page section entries",
            )
        ]
