from django.db import models
from simple_history.models import HistoricalRecords

MAX_CHAR_LEN = 20


class ReducerControl(models.Model):
    reducer = models.CharField(primary_key=True, max_length=MAX_CHAR_LEN)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()
