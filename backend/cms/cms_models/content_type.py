from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.bundles import Bundles
from cms.cms_models.item_bank import ItemBank
from cms.cms_models.test_section_component import TestSectionComponent


# these are types of content from the item bank


class ContentType(models.Model):
    codename = models.CharField(max_length=25, unique=True, blank=False, null=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate history table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0} codename: {1}".format(self.id, self.codename)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Content Type"
