from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.page_section import PageSection


class PageSectionTypeSurveyLink(models.Model):
    link_value = models.CharField(max_length=300, null=False, blank=False)
    page_section = models.ForeignKey(PageSection, on_delete=models.DO_NOTHING)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console

    def __str__(self):
        ret = "({0}) [{1}]".format(
            self.page_section.section_component_page, self.link_value
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Page Section Type Survey Link"
