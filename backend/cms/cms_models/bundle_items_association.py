from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.items import Items
from cms.cms_models.bundles import Bundles


class BundleItemsAssociation(models.Model):
    bundle_source = models.ForeignKey(
        Bundles, to_field="id", on_delete=models.DO_NOTHING
    )
    system_id_link = models.CharField(max_length=15, blank=False, null=False)
    version = models.IntegerField(blank=True, null=True)
    display_order = models.IntegerField()

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Bundles Items Association"
