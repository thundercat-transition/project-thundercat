from django.db import models
from simple_history.models import HistoricalRecords
from cms.cms_models.item_content import ItemContent
from backend.custom_models.language import Language


class ItemContentText(models.Model):
    text = models.TextField(blank=True, null=True)
    modify_date = models.DateTimeField(auto_now=True)
    language = models.ForeignKey(
        Language, to_field="language_id", on_delete=models.DO_NOTHING
    )
    item_content = models.ForeignKey(
        ItemContent, to_field="id", on_delete=models.DO_NOTHING
    )

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Content Text"
