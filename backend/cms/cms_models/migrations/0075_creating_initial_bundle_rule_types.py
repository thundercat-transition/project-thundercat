from django.db import migrations


def create_initial_bundle_rules(apps, _):
    # get models
    bundle_rule_type = apps.get_model("cms_models", "bundleruletype")
    # creating all needed bundle rule type data
    bundle_rule_type_1 = bundle_rule_type(
        codename="bundle_items_basic_rule",
    )
    bundle_rule_type_1.save()

    bundle_rule_type_2 = bundle_rule_type(
        codename="bundle_bundles_rule",
    )
    bundle_rule_type_2.save()


def rollback_changes(apps, schema_editor):
    # get models
    bundle_rule_type = apps.get_model("cms_models", "bundleruletype")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the needed data
    bundle_rule_type.objects.using(db_alias).get(
        codename="bundle_items_basic_rule"
    ).delete()

    bundle_rule_type.objects.using(db_alias).get(
        codename="bundle_bundles_rule"
    ).delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "cms_models",
            "0074_auto_20230706_1322",
        )
    ]
    operations = [migrations.RunPython(create_initial_bundle_rules, rollback_changes)]
