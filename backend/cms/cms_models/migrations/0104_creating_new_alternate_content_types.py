from django.db import migrations
from backend.static.languages import Language_id
from datetime import date


def create_new_alternate_content_types_and_texts(apps, schema_editor):
    # get models
    content_types = apps.get_model("cms_models", "contenttype")
    content_type_text = apps.get_model("cms_models", "contenttypetext")

    # creating all needed content types
    content_type_1 = content_types(
        codename="is_standard",
    )
    content_type_1.save()

    content_type_2 = content_types(
        codename="is_screen_reader",
    )
    content_type_2.save()

    # creating all needed content type texts
    content_type_text_1 = content_type_text(
        text="Standard",
        modify_date=date.today(),
        content_type_id=content_type_1.id,
        language_id=Language_id.EN,
    )
    content_type_text_1.save()

    content_type_text_2 = content_type_text(
        text="Screen Reader",
        modify_date=date.today(),
        content_type_id=content_type_2.id,
        language_id=Language_id.EN,
    )
    content_type_text_2.save()

    content_type_text_3 = content_type_text(
        text="FR Standard",
        modify_date=date.today(),
        content_type_id=content_type_1.id,
        language_id=Language_id.FR,
    )
    content_type_text_3.save()

    content_type_text_4 = content_type_text(
        text="Lecteur d'écran",
        modify_date=date.today(),
        content_type_id=content_type_2.id,
        language_id=Language_id.FR,
    )
    content_type_text_4.save()


def rollback_changes(apps, schema_editor):
    # get models
    content_type_texts = apps.get_model("cms_models", "contenttypetext")
    content_types = apps.get_model("cms_models", "contenttype")
    # get db alias
    db_alias = schema_editor.connection.alias

    # delete the content types texts
    content_type_texts.objects.using(db_alias).filter(text="Standard").delete()
    content_type_texts.objects.using(db_alias).filter(text="Screen Reader").delete()
    content_type_texts.objects.using(db_alias).filter(text="FR Standard").delete()
    content_type_texts.objects.using(db_alias).filter(text="Lecteur d'écran").delete()

    # delete the content types
    content_types.objects.using(db_alias).filter(codename="is_standard").delete()
    content_types.objects.using(db_alias).filter(codename="is_screen_reader").delete()


class Migration(migrations.Migration):
    dependencies = [
        ("cms_models", "0103_contenttype_historicalcontenttypetext_and_more")
    ]
    operations = [
        migrations.RunPython(
            create_new_alternate_content_types_and_texts, rollback_changes
        )
    ]
