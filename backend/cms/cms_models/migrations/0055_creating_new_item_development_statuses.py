from django.db import migrations


def create_new_item_development_statuses(apps, _):
    # get models
    item_development_statuses = apps.get_model("cms_models", "itemdevelopmentstatuses")
    # creating all needed item development statuses
    development_status_1 = item_development_statuses(
        codename="is_pilot",
        en_name="PILOT",
        fr_name="FR PILOT",
    )
    development_status_1.save()

    development_status_2 = item_development_statuses(
        codename="is_regular",
        en_name="REGULAR",
        fr_name="FR REGULAR",
    )
    development_status_2.save()

    development_status_3 = item_development_statuses(
        codename="is_retired",
        en_name="RETIRED",
        fr_name="FR RETIRED",
    )
    development_status_3.save()

    development_status_4 = item_development_statuses(
        codename="is_review_needed",
        en_name="REVIEW NEEDED",
        fr_name="FR REVIEW NEEDED",
    )
    development_status_4.save()


def rollback_changes(apps, schema_editor):
    # get models
    item_development_statuses = apps.get_model("cms_models", "itemdevelopmentstatuses")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the item development statuses
    item_development_statuses.objects.using(db_alias).filter(
        codename="is_pilot"
    ).delete()
    item_development_statuses.objects.using(db_alias).filter(
        codename="is_regular"
    ).delete()
    item_development_statuses.objects.using(db_alias).filter(
        codename="is_retired"
    ).delete()
    item_development_statuses.objects.using(db_alias).filter(
        codename="is_review_needed"
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("cms_models", "0054_creating_new_item_response_formats")]
    operations = [
        migrations.RunPython(create_new_item_development_statuses, rollback_changes)
    ]
