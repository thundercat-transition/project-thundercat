# Generated by Django 3.2.18 on 2023-04-21 13:50

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("cms_models", "0066_auto_20230420_1353"),
    ]

    operations = [
        migrations.AddField(
            model_name="historicalitemdrafts",
            name="shuffle_options",
            field=models.BooleanField(default=0),
        ),
        migrations.AddField(
            model_name="historicalitems",
            name="shuffle_options",
            field=models.BooleanField(default=0),
        ),
        migrations.AddField(
            model_name="itemdrafts",
            name="shuffle_options",
            field=models.BooleanField(default=0),
        ),
        migrations.AddField(
            model_name="items",
            name="shuffle_options",
            field=models.BooleanField(default=0),
        ),
        migrations.AlterField(
            model_name="historicalitemdrafts",
            name="active_status",
            field=models.BooleanField(default=0),
        ),
        migrations.AlterField(
            model_name="historicalitems",
            name="active_status",
            field=models.BooleanField(default=0),
        ),
        migrations.AlterField(
            model_name="itemdrafts",
            name="active_status",
            field=models.BooleanField(default=0),
        ),
        migrations.AlterField(
            model_name="items",
            name="active_status",
            field=models.BooleanField(default=0),
        ),
        migrations.DeleteModel(
            name="HistoricalItemActiveStatuses",
        ),
        migrations.DeleteModel(
            name="ItemActiveStatuses",
        ),
    ]
