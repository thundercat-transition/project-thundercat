# Generated by Django 2.2.3 on 2020-11-20 15:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms_models', '0011_auto_20201119_1034'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testdefinition',
            name='en_name',
            field=models.CharField(max_length=500),
        ),
        migrations.AlterField(
            model_name='testdefinition',
            name='fr_name',
            field=models.CharField(max_length=500),
        ),
        migrations.AlterField(
            model_name='testdefinition',
            name='parent_code',
            field=models.CharField(default='change-me', max_length=500),
        ),
        migrations.AlterField(
            model_name='testdefinition',
            name='test_code',
            field=models.CharField(max_length=500),
        ),
    ]
