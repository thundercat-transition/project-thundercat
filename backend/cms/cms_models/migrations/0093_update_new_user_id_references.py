from django.db import connection
from django.db import migrations
from django.conf import settings


def update_data(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """
				-- ====================================================================
				--                               CMS MODELS
				-- ====================================================================
				-- ==================== TEST PERMISSION ====================
				UPDATE {db_name}..cms_models_testpermissions
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)

				UPDATE {db_name}..cms_models_historicaltestpermissions
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)
				-- ==================== TEST PERMISSION (END) ====================

				-- ==================== ITEM ATTRIBUTE VALUE DRAFTS ====================
				UPDATE {db_name}..cms_models_itemattributevaluedrafts
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)

				UPDATE {db_name}..cms_models_historicalitemattributevaluedrafts
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)
				-- ==================== ITEM ATTRIBUTE VALUE DRAFTS (END) ====================

				-- ==================== ITEM BANK PERMISSIONS ====================
				UPDATE {db_name}..cms_models_itembankpermissions
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)

				UPDATE {db_name}..cms_models_historicalitembankpermissions
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)
				-- ==================== ITEM BANK PERMISSIONS (END) ====================

				-- ==================== ITEM COMMENTS ====================
				UPDATE {db_name}..cms_models_itemcomments
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)

				UPDATE {db_name}..cms_models_historicalitemcomments
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)
				-- ==================== ITEM COMMENTS (END) ====================

				-- ==================== ITEM CONTENT DRAFTS ====================
				UPDATE {db_name}..cms_models_itemcontentdrafts
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)

				UPDATE {db_name}..cms_models_historicalitemcontentdrafts
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)
				-- ==================== ITEM CONTENT DRAFTS (END) ====================

				-- ==================== ITEM DRAFTS ====================
				UPDATE {db_name}..cms_models_itemdrafts
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)

				UPDATE {db_name}..cms_models_historicalitemdrafts
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)
				-- ==================== ITEM DRAFTS (END) ====================

				-- ==================== ITEM OPTION DRAFTS ====================
				UPDATE {db_name}..cms_models_itemoptiondrafts
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)

				UPDATE {db_name}..cms_models_historicalitemoptiondrafts
				SET user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = username_id)
				-- ==================== ITEM OPTION DRAFTS (END) ====================

				-- ==================== ITEMS ====================
				UPDATE {db_name}..cms_models_items
				SET last_modified_by_user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = last_modified_by_username_id)

				UPDATE {db_name}..cms_models_historicalitems
				SET last_modified_by_user_id = (SELECT u.id 
								FROM {db_name}..user_management_models_user u
								WHERE u.username = last_modified_by_username_id)
				-- ==================== ITEMS (END) ====================
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """
				-- ====================================================================
				--                               CMS MODELS
				-- ====================================================================
				-- ==================== TEST PERMISSION ====================
				UPDATE {db_name}..cms_models_testpermissions
				SET user_id = NULL

				UPDATE {db_name}..cms_models_historicaltestpermissions
				SET user_id = NULL
				-- ==================== TEST PERMISSION (END) ====================

				-- ==================== ITEM ATTRIBUTE VALUE DRAFTS ====================
				UPDATE {db_name}..cms_models_itemattributevaluedrafts
				SET user_id = NULL

				UPDATE {db_name}..cms_models_historicalitemattributevaluedrafts
				SET user_id = NULL
				-- ==================== ITEM ATTRIBUTE VALUE DRAFTS (END) ====================

				-- ==================== ITEM BANK PERMISSIONS ====================
				UPDATE {db_name}..cms_models_itembankpermissions
				SET user_id = NULL

				UPDATE {db_name}..cms_models_historicalitembankpermissions
				SET user_id = NULL
				-- ==================== ITEM BANK PERMISSIONS (END) ====================

				-- ==================== ITEM COMMENTS ====================
				UPDATE {db_name}..cms_models_itemcomments
				SET user_id = NULL

				UPDATE {db_name}..cms_models_historicalitemcomments
				SET user_id = NULL
				-- ==================== ITEM COMMENTS (END) ====================

				-- ==================== ITEM CONTENT DRAFTS ====================
				UPDATE {db_name}..cms_models_itemcontentdrafts
				SET user_id = NULL

				UPDATE {db_name}..cms_models_historicalitemcontentdrafts
				SET user_id = NULL
				-- ==================== ITEM CONTENT DRAFTS (END) ====================

				-- ==================== ITEM DRAFTS ====================
				UPDATE {db_name}..cms_models_itemdrafts
				SET user_id = NULL

				UPDATE {db_name}..cms_models_historicalitemdrafts
				SET user_id = NULL
				-- ==================== ITEM DRAFTS (END) ====================

				-- ==================== ITEM OPTION DRAFTS ====================
				UPDATE {db_name}..cms_models_itemoptiondrafts
				SET user_id = NULL

				UPDATE {db_name}..cms_models_historicalitemoptiondrafts
				SET user_id = NULL
				-- ==================== ITEM OPTION DRAFTS (END) ====================

				-- ==================== ITEMS ====================
				UPDATE {db_name}..cms_models_items
				SET last_modified_by_user_id = NULL

				UPDATE {db_name}..cms_models_historicalitems
				SET last_modified_by_user_id = NULL
				-- ==================== ITEMS (END) ====================
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "cms_models",
            "0092_historicalitemattributevaluedrafts_user_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_data, rollback_changes)]
