from django.db import connection
from django.db import migrations
from django.conf import settings


def create_new_entries(apps, schema_editor):
    # get models
    scoring_methods = apps.get_model("cms_models", "scoringmethods")
    scoring_method_types = apps.get_model("cms_models", "scoringmethodtypes")
    scoring_raw = apps.get_model("cms_models", "scoringraw")

    # get db alias
    db_alias = schema_editor.connection.alias

    # getting RAW scoring method type ID
    raw_scoring_method_type_id = (
        scoring_method_types.objects.using(db_alias).get(method_type_codename="RAW").id
    )

    # getting raw scoring methods
    raw_scoring_methods = scoring_methods.objects.using(db_alias).filter(
        method_type_id=raw_scoring_method_type_id
    )

    # looping in raw_scoring_methods
    for raw_scoring_method in raw_scoring_methods:
        # creating new entry in scoring_raw model
        scoring_raw_1 = scoring_raw(
            scoring_method_id=raw_scoring_method.id,
            result_valid_indefinitely=0,
        )
        scoring_raw_1.save()

    # setting validity_period to 60 for all scoring methods except None method types
    with connection.cursor() as cursor:
        cursor.execute(
            """
			UPDATE {db_name}..cms_models_scoringmethods SET validity_period = 60
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """
			UPDATE {db_name}..cms_models_scoringmethods 
            SET validity_period = NULL 
            WHERE method_type_id = (SELECT id 
                                    FROM {db_name}..cms_models_scoringmethodtypes 
                                    WHERE method_type_codename = 'NONE')
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    # get models
    scoring_raw = apps.get_model("cms_models", "scoringraw")

    # delete data
    for data in scoring_raw.objects.all():
        data.delete()

    # setting validity_period back to NULL for all scoring methods
    with connection.cursor() as cursor:
        cursor.execute(
            """
			UPDATE {db_name}..cms_models_scoringmethods SET validity_period = NULL
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        ("cms_models", "0095_historicalscoringmethods_validity_period_and_more")
    ]
    operations = [migrations.RunPython(create_new_entries, rollback_changes)]
