from django.db import migrations

def create_new_item_types(apps, schema_editor):
    # get models
    item_types = apps.get_model("cms_models", "itemtype")

    # creating all needed item bank access types
    item_type_1 = item_types(
        codename="is_question",
    )
    item_type_1.save()

    item_type_2 = item_types(
        codename="is_instruction",
    )
    item_type_2.save()

def rollback_changes(apps, schema_editor):
    # get models
    item_types = apps.get_model("cms_models", "itemtype") 
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the types
    item_types.objects.using(db_alias).filter(codename="is_question").delete()
    item_types.objects.using(db_alias).filter(codename="is_instruction").delete()

class Migration(migrations.Migration):
    dependencies = [("cms_models", "0085_itemtype_alter_itemdrafts_response_format_and_more")]
    operations = [
        migrations.RunPython(create_new_item_types, rollback_changes)
    ]
