from django.db import migrations
from datetime import date
from backend.custom_models.language import Language
from cms.cms_models.item_type import ItemType


def create_new_item_type_texts(apps, schema_editor):
    # get models
    item_type_texts = apps.get_model("cms_models", "itemtypetext")
    language = apps.get_model("custom_models", "language")
    item_types = apps.get_model("cms_models", "itemtype")

    # get db alias
    db_alias = schema_editor.connection.alias
    
    # creating all needed item type texts
    item_type_1 = item_type_texts(
        text="Questions",
        modify_date=date.today(),
        item_type_id=item_types.objects.using(db_alias).get(codename="is_question").id,
        language_id=language.objects.using(db_alias).get(language_id=1).language_id
    )
    item_type_1.save()

    item_type_2 = item_type_texts(
        text="Instructions",
        modify_date=date.today(),
        item_type_id=item_types.objects.using(db_alias).get(codename="is_instruction").id,
        language_id=language.objects.using(db_alias).get(language_id=1).language_id
    )
    item_type_2.save()

    item_type_3 = item_type_texts(
        text="FR Questions",
        modify_date=date.today(),
        item_type_id=item_types.objects.using(db_alias).get(codename="is_question").id,
        language_id=language.objects.using(db_alias).get(language_id=2).language_id
    )
    item_type_3.save()

    item_type_4 = item_type_texts(
        text="FR Instructions",
        modify_date=date.today(),
        item_type_id=item_types.objects.using(db_alias).get(codename="is_Instruction").id,
        language_id=language.objects.using(db_alias).get(language_id=2).language_id
    )
    item_type_4.save()

def rollback_changes(apps, schema_editor):
    # get models
    item_type_texts = apps.get_model("cms_models", "itemtypetext") 
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the types
    item_type_texts.objects.using(db_alias).filter(text="Questions").delete()
    item_type_texts.objects.using(db_alias).filter(text="Instructions").delete()
    item_type_texts.objects.using(db_alias).filter(text="FR Questions").delete()
    item_type_texts.objects.using(db_alias).filter(text="FR Instructions").delete()


class Migration(migrations.Migration):
    dependencies = [("cms_models", "0087_itemtype_alter_itemdrafts_response_format_and_more_2")]
    operations = [
        migrations.RunPython(create_new_item_type_texts, rollback_changes)
    ]
