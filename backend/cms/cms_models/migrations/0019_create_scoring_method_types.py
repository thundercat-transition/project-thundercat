from django.db import migrations


def create_new_scoring_method_types(apps, _):
    # get models
    scoring_method_types = apps.get_model("cms_models", "scoringmethodtypes")
    # creating all needed TA Action Types
    method_type_1 = scoring_method_types(
        method_type_codename="THRESHOLD", en_name="Threshold", fr_name="FR Threshold"
    )
    method_type_1.save()
    method_type_2 = scoring_method_types(
        method_type_codename="PASS_FAIL", en_name="Pass/Fail", fr_name="FR Pass/Fail"
    )
    method_type_2.save()
    method_type_3 = scoring_method_types(
        method_type_codename="RAW", en_name="Raw", fr_name="FR Raw"
    )
    method_type_3.save()
    method_type_4 = scoring_method_types(
        method_type_codename="PERCENTAGE", en_name="Percentage", fr_name="FR Percentage"
    )
    method_type_4.save()


def rollback_new_scoring_method_types(apps, schema_editor):
    # get models
    scoring_method_types = apps.get_model("cms_models", "scoringmethodtypes")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the types
    scoring_method_types.objects.using(db_alias).filter(
        method_type_codename="THRESHOLD"
    ).delete()
    scoring_method_types.objects.using(db_alias).filter(
        method_type_codename="PASS_FAIL"
    ).delete()
    scoring_method_types.objects.using(db_alias).filter(
        method_type_codename="RAW"
    ).delete()
    scoring_method_types.objects.using(db_alias).filter(
        method_type_codename="PERCENTAGE"
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("cms_models", "0018_auto_20210603_0851")]
    operations = [
        migrations.RunPython(
            create_new_scoring_method_types, rollback_new_scoring_method_types
        )
    ]
