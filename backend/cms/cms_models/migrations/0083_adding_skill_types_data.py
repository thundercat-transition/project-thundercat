from django.db import migrations
from backend.static.languages import Languages
from backend.static.test_skill_codenames import (
    TestSkillTypeCodename,
    TestSkillSLEDescCodename,
)


def insert_test_skill_types_data(apps, schema_editor):
    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # TestSkillType
    TestSkillType = apps.get_model("cms_models", "TestSkillType")

    # TestSkillTypeText
    TestSkillTypeText = apps.get_model("cms_models", "TestSkillTypeText")

    # TestSkillSLEDesc
    TestSkillSLEDesc = apps.get_model("cms_models", "TestSkillSLEDesc")

    # TestSkillSLEDescText
    TestSkillSLEDescText = apps.get_model("cms_models", "TestSkillSLEDescText")

    # Language
    language = apps.get_model("custom_models", "language")

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # --------------------------------------------------------------------------------- #
    # Language Data - Getting the 2 Official Languages
    # --------------------------------------------------------------------------------- #
    # 1. English
    language_en = language.objects.using(db_alias).get(ISO_Code_1=Languages.EN)

    # 2. French
    language_fr = language.objects.using(db_alias).get(ISO_Code_1=Languages.FR)

    # --------------------------------------------------------------------------------- #
    # TestSkillType Data - creating all needed skill types' codenames
    # --------------------------------------------------------------------------------- #
    # 1. None
    test_skill_type_1 = TestSkillType(codename=TestSkillTypeCodename.NONE)
    test_skill_type_1.save()

    # 2. SLE
    test_skill_type_2 = TestSkillType(codename=TestSkillTypeCodename.SLE)
    test_skill_type_2.save()

    # 3. Occupational
    test_skill_type_3 = TestSkillType(codename=TestSkillTypeCodename.OCC)
    test_skill_type_3.save()

    # --------------------------------------------------------------------------------- #
    # TestSkillTypeText Data - creating all needed skill types' text
    # --------------------------------------------------------------------------------- #
    # 1. None - English
    test_skill_type_text_1 = TestSkillTypeText(
        text="None",
        language=language_en,
        test_skill_type=test_skill_type_1,
    )
    test_skill_type_text_1.save()

    # 2. None - French
    test_skill_type_text_2 = TestSkillTypeText(
        text="FR None",
        language=language_fr,
        test_skill_type=test_skill_type_1,
    )
    test_skill_type_text_2.save()

    # 3. SLE - English
    test_skill_type_text_3 = TestSkillTypeText(
        text="Second Language Evaluation",
        language=language_en,
        test_skill_type=test_skill_type_2,
    )
    test_skill_type_text_3.save()

    # 4. SLE - French
    test_skill_type_text_4 = TestSkillTypeText(
        text="FR Second Language Evaluation",
        language=language_fr,
        test_skill_type=test_skill_type_2,
    )
    test_skill_type_text_4.save()

    # 5. Occupational - English
    test_skill_type_text_5 = TestSkillTypeText(
        text="Occupational",
        language=language_en,
        test_skill_type=test_skill_type_3,
    )
    test_skill_type_text_5.save()

    # 6. Occupational - French
    test_skill_type_text_6 = TestSkillTypeText(
        text="FR Occupational",
        language=language_fr,
        test_skill_type=test_skill_type_3,
    )
    test_skill_type_text_6.save()

    # --------------------------------------------------------------------------------- #
    # TestSkillSLEDesc Data - creating all needed SLE Skill Descriptions' Codenames
    # --------------------------------------------------------------------------------- #
    # 1. Reading English
    test_skill_sle_desc_1 = TestSkillSLEDesc(
        codename=TestSkillSLEDescCodename.READING_EN
    )
    test_skill_sle_desc_1.save()

    # 2. Reading French
    test_skill_sle_desc_2 = TestSkillSLEDesc(
        codename=TestSkillSLEDescCodename.READING_FR
    )
    test_skill_sle_desc_2.save()

    # 3. Writing English
    test_skill_sle_desc_3 = TestSkillSLEDesc(
        codename=TestSkillSLEDescCodename.WRITING_EN
    )
    test_skill_sle_desc_3.save()

    # 4. Writing French
    test_skill_sle_desc_4 = TestSkillSLEDesc(
        codename=TestSkillSLEDescCodename.WRITING_FR
    )
    test_skill_sle_desc_4.save()

    # 5. Oral English
    test_skill_sle_desc_5 = TestSkillSLEDesc(codename=TestSkillSLEDescCodename.ORAL_EN)
    test_skill_sle_desc_5.save()

    # 6. Oral French
    test_skill_sle_desc_6 = TestSkillSLEDesc(codename=TestSkillSLEDescCodename.ORAL_FR)
    test_skill_sle_desc_6.save()

    # --------------------------------------------------------------------------------- #
    # TestSkillSLEDescText Data - creating all needed SLE Skill Descriptions' Text
    # --------------------------------------------------------------------------------- #
    # 1. English Reading - English
    test_skill_sle_desc_text_1 = TestSkillSLEDescText(
        text="English Reading",
        language=language_en,
        test_skill_sle_desc=test_skill_sle_desc_1,
    )
    test_skill_sle_desc_text_1.save()

    # 2. English Reading - French
    test_skill_sle_desc_text_2 = TestSkillSLEDescText(
        text="FR English Reading",
        language=language_fr,
        test_skill_sle_desc=test_skill_sle_desc_1,
    )
    test_skill_sle_desc_text_2.save()

    # 3. French Reading - English
    test_skill_sle_desc_text_3 = TestSkillSLEDescText(
        text="French Reading",
        language=language_en,
        test_skill_sle_desc=test_skill_sle_desc_2,
    )
    test_skill_sle_desc_text_3.save()

    # 4. French Reading - French
    test_skill_sle_desc_text_4 = TestSkillSLEDescText(
        text="FR French Reading",
        language=language_fr,
        test_skill_sle_desc=test_skill_sle_desc_2,
    )
    test_skill_sle_desc_text_4.save()

    # 5. English Writing - English
    test_skill_sle_desc_text_5 = TestSkillSLEDescText(
        text="English Writing",
        language=language_en,
        test_skill_sle_desc=test_skill_sle_desc_3,
    )
    test_skill_sle_desc_text_5.save()

    # 6. English Writing - French
    test_skill_sle_desc_text_6 = TestSkillSLEDescText(
        text="FR English Writing",
        language=language_fr,
        test_skill_sle_desc=test_skill_sle_desc_3,
    )
    test_skill_sle_desc_text_6.save()

    # 7. French Writing - English
    test_skill_sle_desc_text_7 = TestSkillSLEDescText(
        text="French Writing",
        language=language_en,
        test_skill_sle_desc=test_skill_sle_desc_4,
    )
    test_skill_sle_desc_text_7.save()

    # 8. French Writing - French
    test_skill_sle_desc_text_8 = TestSkillSLEDescText(
        text="FR French Writing",
        language=language_fr,
        test_skill_sle_desc=test_skill_sle_desc_4,
    )
    test_skill_sle_desc_text_8.save()

    # 9. English Oral - English
    test_skill_sle_desc_text_9 = TestSkillSLEDescText(
        text="English Oral",
        language=language_en,
        test_skill_sle_desc=test_skill_sle_desc_5,
    )
    test_skill_sle_desc_text_9.save()

    # 10. English Oral - French
    test_skill_sle_desc_text_10 = TestSkillSLEDescText(
        text="FR English Oral",
        language=language_fr,
        test_skill_sle_desc=test_skill_sle_desc_5,
    )
    test_skill_sle_desc_text_10.save()

    # 11. French Oral - English
    test_skill_sle_desc_text_11 = TestSkillSLEDescText(
        text="French Oral",
        language=language_en,
        test_skill_sle_desc=test_skill_sle_desc_6,
    )
    test_skill_sle_desc_text_11.save()

    # 12. French Oral - French
    test_skill_sle_desc_text_12 = TestSkillSLEDescText(
        text="FR French Oral",
        language=language_fr,
        test_skill_sle_desc=test_skill_sle_desc_6,
    )
    test_skill_sle_desc_text_12.save()


def rollback_changes(apps, schema_editor):
    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # TestSkillType
    TestSkillType = apps.get_model("cms_models", "TestSkillType")

    # TestSkillTypeText
    TestSkillTypeText = apps.get_model("cms_models", "TestSkillTypeText")

    # TestSkillSLEDesc
    TestSkillSLEDesc = apps.get_model("cms_models", "TestSkillSLEDesc")

    # TestSkillSLEDescText
    TestSkillSLEDescText = apps.get_model("cms_models", "TestSkillSLEDescText")

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # --------------------------------------------------------------------------------- #
    # TestSkillSLEDescText Data - Delete All The SLE Skill Descriptions' Text
    # --------------------------------------------------------------------------------- #
    # 1. English Reading - English
    TestSkillSLEDescText.objects.using(db_alias).filter(text="English Reading").delete()

    # 2. English Reading - French
    TestSkillSLEDescText.objects.using(db_alias).filter(
        text="FR English Reading"
    ).delete()

    # 3. French Reading - English
    TestSkillSLEDescText.objects.using(db_alias).filter(text="French Reading").delete()

    # 4. French Reading - French
    TestSkillSLEDescText.objects.using(db_alias).filter(
        text="FR French Reading"
    ).delete()

    # 5. English Writing - English
    TestSkillSLEDescText.objects.using(db_alias).filter(text="English Writing").delete()

    # 6. English Writing - French
    TestSkillSLEDescText.objects.using(db_alias).filter(
        text="FR English Writing"
    ).delete()

    # 7. French Writing - English
    TestSkillSLEDescText.objects.using(db_alias).filter(text="French Writing").delete()

    # 8. French Writing - French
    TestSkillSLEDescText.objects.using(db_alias).filter(
        text="FR French Writing"
    ).delete()

    # 9. English Oral - English
    TestSkillSLEDescText.objects.using(db_alias).filter(text="English Oral").delete()

    # 10. English Oral - French
    TestSkillSLEDescText.objects.using(db_alias).filter(text="FR English Oral").delete()

    # 11. French Oral - English
    TestSkillSLEDescText.objects.using(db_alias).filter(text="French Oral").delete()

    # 12. French Oral - French
    TestSkillSLEDescText.objects.using(db_alias).filter(text="FR French Oral").delete()

    # --------------------------------------------------------------------------------- #
    # TestSkillSLEDesc - Delete All The SLE Skill Descriptions' Codenames
    # --------------------------------------------------------------------------------- #
    # 1. Reading English
    TestSkillSLEDesc.objects.using(db_alias).filter(
        codename=TestSkillSLEDescCodename.READING_EN
    ).delete()

    # 2. Reading French
    TestSkillSLEDesc.objects.using(db_alias).filter(
        codename=TestSkillSLEDescCodename.READING_FR
    ).delete()

    # 3. Writing English
    TestSkillSLEDesc.objects.using(db_alias).filter(
        codename=TestSkillSLEDescCodename.WRITING_EN
    ).delete()

    # 4. Writing French
    TestSkillSLEDesc.objects.using(db_alias).filter(
        codename=TestSkillSLEDescCodename.WRITING_FR
    ).delete()

    # 5. Oral English
    TestSkillSLEDesc.objects.using(db_alias).filter(
        codename=TestSkillSLEDescCodename.ORAL_EN
    ).delete()

    # 6. Oral French
    TestSkillSLEDesc.objects.using(db_alias).filter(
        codename=TestSkillSLEDescCodename.ORAL_FR
    ).delete()

    # --------------------------------------------------------------------------------- #
    # TestSkillTypeText Data - Delete All Skill Types' Text
    # --------------------------------------------------------------------------------- #
    # 1. None - English
    TestSkillTypeText.objects.using(db_alias).filter(text="None").delete()

    # 2. None - French
    TestSkillTypeText.objects.using(db_alias).filter(text="FR None").delete()

    # 3. SLE - English
    TestSkillTypeText.objects.using(db_alias).filter(
        text="Second Language Evaluation"
    ).delete()

    # 4. SLE - French
    TestSkillTypeText.objects.using(db_alias).filter(
        text="FR Second Language Evaluation"
    ).delete()

    # 5. Occupational - English
    TestSkillTypeText.objects.using(db_alias).filter(text="Occupational").delete()

    # 6. Occupational - French
    TestSkillTypeText.objects.using(db_alias).filter(text="FR Occupational").delete()

    # --------------------------------------------------------------------------------- #
    # TestSkillType Data - Delete All The Test Skill Type Data
    # --------------------------------------------------------------------------------- #
    # 1. None
    TestSkillType.objects.using(db_alias).filter(
        codename=TestSkillTypeCodename.NONE
    ).delete()

    # 2. SLE
    TestSkillType.objects.using(db_alias).filter(
        codename=TestSkillTypeCodename.SLE
    ).delete()

    # 3. Occupational
    TestSkillType.objects.using(db_alias).filter(
        codename=TestSkillTypeCodename.OCC
    ).delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "cms_models",
            "0082_testskill_testskilloccupationaldesc_testskillsledesc_and_more",
        )
    ]
    operations = [migrations.RunPython(insert_test_skill_types_data, rollback_changes)]
