from django.db import connection
from django.db import migrations
from django.conf import settings


def update_data(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """
				UPDATE {db_name}..cms_models_pagesectiontypeitembankinstruction
				SET system_id = (SELECT i.system_id 
				FROM {db_name}..cms_models_items i
				WHERE i.id = item_id)
                
                UPDATE {db_name}..cms_models_historicalpagesectiontypeitembankinstruction
				SET system_id = (SELECT i.system_id 
				FROM {db_name}..cms_models_items i
				WHERE i.id = item_id)
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """
				UPDATE {db_name}..cms_models_pagesectiontypeitembankinstruction
				SET system_id = 'temp'
                
                UPDATE {db_name}..cms_models_historicalpagesectiontypeitembankinstruction
				SET system_id = 'temp'
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "cms_models",
            "0098_remove_historicalpagesectiontypeitembankinstruction_item_and_more",
        ),
    ]
    operations = [migrations.RunPython(update_data, rollback_changes)]
