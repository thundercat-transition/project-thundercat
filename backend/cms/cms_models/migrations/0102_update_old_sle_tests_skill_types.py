from django.db import connection
from django.db import migrations
from django.conf import settings


def update_data(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """
				-- ==================== ENGLISH READING ====================
                -- declaring needed variables
                DECLARE @test_skill_id INT

                -- looping in respective test skills
                DECLARE db_cursor CURSOR FOR
                SELECT id 
                FROM {db_name}..cms_models_testskill
                WHERE test_definition_id in (SELECT td.id 
                                            FROM {db_name}..cms_models_testdefinition td
                                            WHERE td.is_public = 0
                                            AND td.parent_code in ('634','639')
                                            AND td.test_code like 'E%')
                AND test_skill_type_id = (SELECT id 
                                        FROM {db_name}..cms_models_testskilltype 
                                        WHERE codename = 'none')

                -- initializing loop
                OPEN db_cursor
                FETCH NEXT FROM db_cursor INTO @test_skill_id
                WHILE @@FETCH_STATUS = 0
                BEGIN
                    -- updating respective test skill to SLE
                    UPDATE {db_name}..cms_models_testskill 
                    SET test_skill_type_id = (SELECT id 
                                            FROM {db_name}..cms_models_testskilltype 
                                            WHERE codename = 'sle')
                    WHERE id = @test_skill_id

                    -- creating new test skill sle entry
                    INSERT INTO {db_name}..cms_models_testskillsle (test_skill_id, test_skill_sle_desc_id)
                    VALUES (@test_skill_id,
                            (SELECT id 
                            FROM {db_name}..cms_models_testskillsledesc 
                            WHERE codename = 're')
                            )

                    FETCH NEXT FROM db_cursor INTO @test_skill_id
                END

                CLOSE db_cursor
                DEALLOCATE db_cursor
                -- ==================== ENGLISH READING (END) ====================
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """
                -- ==================== FRENCH READING ====================
                -- declaring needed variables
                DECLARE @test_skill_id_2 INT

                -- looping in respective test skills
                DECLARE db_cursor_2 CURSOR FOR
                SELECT id 
                FROM {db_name}..cms_models_testskill
                WHERE test_definition_id in (SELECT td.id 
                                            FROM {db_name}..cms_models_testdefinition td
                                            WHERE td.is_public = 0
                                            AND td.parent_code in ('634','639')
                                            AND td.test_code like 'F%')
                AND test_skill_type_id = (SELECT id 
                                        FROM {db_name}..cms_models_testskilltype 
                                        WHERE codename = 'none')

                -- initializing loop
                OPEN db_cursor_2
                FETCH NEXT FROM db_cursor_2 INTO @test_skill_id_2
                WHILE @@FETCH_STATUS = 0
                BEGIN
                    -- updating respective test skill to SLE
                    UPDATE {db_name}..cms_models_testskill 
                    SET test_skill_type_id = (SELECT id 
                                            FROM {db_name}..cms_models_testskilltype 
                                            WHERE codename = 'sle')
                    WHERE id = @test_skill_id_2

                    -- creating new test skill sle entry
                    INSERT INTO {db_name}..cms_models_testskillsle (test_skill_id, test_skill_sle_desc_id)
                    VALUES (@test_skill_id_2,
                            (SELECT id 
                            FROM {db_name}..cms_models_testskillsledesc 
                            WHERE codename = 'rf')
                            )

                    FETCH NEXT FROM db_cursor_2 INTO @test_skill_id_2
                END

                CLOSE db_cursor_2
                DEALLOCATE db_cursor_2
                -- ==================== FRENCH READING (END) ====================
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """
                -- ==================== ENGLISH WRITING ====================
                -- declaring needed variables
                DECLARE @test_skill_id_3 INT

                -- looping in respective test skills
                DECLARE db_cursor_3 CURSOR FOR
                SELECT id 
                FROM {db_name}..cms_models_testskill
                WHERE test_definition_id in (SELECT td.id 
                                            FROM {db_name}..cms_models_testdefinition td
                                            WHERE td.is_public = 0
                                            AND td.parent_code in ('654','659')
                                            AND td.test_code like 'E%')
                AND test_skill_type_id = (SELECT id 
                                        FROM {db_name}..cms_models_testskilltype 
                                        WHERE codename = 'none')

                -- initializing loop
                OPEN db_cursor_3
                FETCH NEXT FROM db_cursor_3 INTO @test_skill_id_3
                WHILE @@FETCH_STATUS = 0
                BEGIN
                    -- updating respective test skill to SLE
                    UPDATE {db_name}..cms_models_testskill 
                    SET test_skill_type_id = (SELECT id 
                                            FROM {db_name}..cms_models_testskilltype 
                                            WHERE codename = 'sle')
                    WHERE id = @test_skill_id_3

                    -- creating new test skill sle entry
                    INSERT INTO {db_name}..cms_models_testskillsle (test_skill_id, test_skill_sle_desc_id)
                    VALUES (@test_skill_id_3,
                            (SELECT id 
                            FROM {db_name}..cms_models_testskillsledesc 
                            WHERE codename = 'we')
                            )

                    FETCH NEXT FROM db_cursor_3 INTO @test_skill_id_3
                END

                CLOSE db_cursor_3
                DEALLOCATE db_cursor_3
                -- ==================== ENGLISH WRITING (END) ====================
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """
                -- ==================== FRENCH WRITING ====================
                -- declaring needed variables
                DECLARE @test_skill_id_4 INT

                -- looping in respective test skills
                DECLARE db_cursor_4 CURSOR FOR
                SELECT id 
                FROM {db_name}..cms_models_testskill
                WHERE test_definition_id in (SELECT td.id 
                                            FROM {db_name}..cms_models_testdefinition td
                                            WHERE td.is_public = 0
                                            AND td.parent_code in ('654','659')
                                            AND td.test_code like 'F%')
                AND test_skill_type_id = (SELECT id 
                                        FROM {db_name}..cms_models_testskilltype 
                                        WHERE codename = 'none')

                -- initializing loop
                OPEN db_cursor_4
                FETCH NEXT FROM db_cursor_4 INTO @test_skill_id_4
                WHILE @@FETCH_STATUS = 0
                BEGIN
                    -- updating respective test skill to SLE
                    UPDATE {db_name}..cms_models_testskill 
                    SET test_skill_type_id = (SELECT id 
                                            FROM {db_name}..cms_models_testskilltype 
                                            WHERE codename = 'sle')
                    WHERE id = @test_skill_id_4

                    -- creating new test skill sle entry
                    INSERT INTO {db_name}..cms_models_testskillsle (test_skill_id, test_skill_sle_desc_id)
                    VALUES (@test_skill_id_4,
                            (SELECT id 
                            FROM {db_name}..cms_models_testskillsledesc 
                            WHERE codename = 'wf')
                            )

                    FETCH NEXT FROM db_cursor_4 INTO @test_skill_id_4
                END

                CLOSE db_cursor_4
                DEALLOCATE db_cursor_4
                -- ==================== FRENCH WRITING (END) ====================
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


def rollback_changes(apps, schema_editor):
    with connection.cursor() as cursor:
        cursor.execute(
            """
				-- ==================== ENGLISH READING ====================
                -- declaring needed variables
                DECLARE @test_skill_id INT

                -- looping in respective test skills
                DECLARE db_cursor CURSOR FOR
                SELECT id 
                FROM {db_name}..cms_models_testskill
                WHERE test_definition_id in (SELECT td.id 
                                            FROM {db_name}..cms_models_testdefinition td
                                            WHERE td.is_public = 0
                                            AND td.parent_code in ('634','639')
                                            AND td.test_code like 'E%')
                AND test_skill_type_id = (SELECT id 
                                        FROM {db_name}..cms_models_testskilltype 
                                        WHERE codename = 'sle')

                -- initializing loop
                OPEN db_cursor
                FETCH NEXT FROM db_cursor INTO @test_skill_id
                WHILE @@FETCH_STATUS = 0
                BEGIN
                    -- updating respective test skill to None
                    UPDATE {db_name}..cms_models_testskill 
                    SET test_skill_type_id = (SELECT id 
                                            FROM {db_name}..cms_models_testskilltype 
                                            WHERE codename = 'none')
                    WHERE id = @test_skill_id

                    -- deleting test skill sle entry
                    DELETE FROM {db_name}..cms_models_testskillsle
                    WHERE test_skill_id = @test_skill_id

                    FETCH NEXT FROM db_cursor INTO @test_skill_id
                END

                CLOSE db_cursor
                DEALLOCATE db_cursor
                -- ==================== ENGLISH READING (END) ====================
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """
                -- ==================== FRENCH READING ====================
                -- declaring needed variables
                DECLARE @test_skill_id_2 INT

                -- looping in respective test skills
                DECLARE db_cursor_2 CURSOR FOR
                SELECT id 
                FROM {db_name}..cms_models_testskill
                WHERE test_definition_id in (SELECT td.id 
                                            FROM {db_name}..cms_models_testdefinition td
                                            WHERE td.is_public = 0
                                            AND td.parent_code in ('634','639')
                                            AND td.test_code like 'F%')
                AND test_skill_type_id = (SELECT id 
                                        FROM {db_name}..cms_models_testskilltype 
                                        WHERE codename = 'sle')

                -- initializing loop
                OPEN db_cursor_2
                FETCH NEXT FROM db_cursor_2 INTO @test_skill_id_2
                WHILE @@FETCH_STATUS = 0
                BEGIN
                    -- updating respective test skill to None
                    UPDATE {db_name}..cms_models_testskill 
                    SET test_skill_type_id = (SELECT id 
                                            FROM {db_name}..cms_models_testskilltype 
                                            WHERE codename = 'none')
                    WHERE id = @test_skill_id_2

                    -- deleting test skill sle entry
                    DELETE FROM {db_name}..cms_models_testskillsle
                    WHERE test_skill_id = @test_skill_id_2

                    FETCH NEXT FROM db_cursor_2 INTO @test_skill_id_2
                END

                CLOSE db_cursor_2
                DEALLOCATE db_cursor_2
                -- ==================== FRENCH READING (END) ====================
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """
                -- ==================== ENGLISH WRITING ====================
                -- declaring needed variables
                DECLARE @test_skill_id_3 INT

                -- looping in respective test skills
                DECLARE db_cursor_3 CURSOR FOR
                SELECT id 
                FROM {db_name}..cms_models_testskill
                WHERE test_definition_id in (SELECT td.id 
                                            FROM {db_name}..cms_models_testdefinition td
                                            WHERE td.is_public = 0
                                            AND td.parent_code in ('654','659')
                                            AND td.test_code like 'E%')
                AND test_skill_type_id = (SELECT id 
                                        FROM {db_name}..cms_models_testskilltype 
                                        WHERE codename = 'sle')

                -- initializing loop
                OPEN db_cursor_3
                FETCH NEXT FROM db_cursor_3 INTO @test_skill_id_3
                WHILE @@FETCH_STATUS = 0
                BEGIN
                    -- updating respective test skill to None
                    UPDATE {db_name}..cms_models_testskill 
                    SET test_skill_type_id = (SELECT id 
                                            FROM {db_name}..cms_models_testskilltype 
                                            WHERE codename = 'none')
                    WHERE id = @test_skill_id_3

                    -- deleting test skill sle entry
                    DELETE FROM {db_name}..cms_models_testskillsle
                    WHERE test_skill_id = @test_skill_id_3

                    FETCH NEXT FROM db_cursor_3 INTO @test_skill_id_3
                END

                CLOSE db_cursor_3
                DEALLOCATE db_cursor_3
                -- ==================== ENGLISH WRITING (END) ====================
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )
        cursor.execute(
            """
                -- ==================== FRENCH WRITING ====================
                -- declaring needed variables
                DECLARE @test_skill_id_4 INT

                -- looping in respective test skills
                DECLARE db_cursor_4 CURSOR FOR
                SELECT id 
                FROM {db_name}..cms_models_testskill
                WHERE test_definition_id in (SELECT td.id 
                                            FROM {db_name}..cms_models_testdefinition td
                                            WHERE td.is_public = 0
                                            AND td.parent_code in ('654','659')
                                            AND td.test_code like 'F%')
                AND test_skill_type_id = (SELECT id 
                                        FROM {db_name}..cms_models_testskilltype 
                                        WHERE codename = 'sle')

                -- initializing loop
                OPEN db_cursor_4
                FETCH NEXT FROM db_cursor_4 INTO @test_skill_id_4
                WHILE @@FETCH_STATUS = 0
                BEGIN
                    -- updating respective test skill to None
                    UPDATE {db_name}..cms_models_testskill 
                    SET test_skill_type_id = (SELECT id 
                                            FROM {db_name}..cms_models_testskilltype 
                                            WHERE codename = 'none')
                    WHERE id = @test_skill_id_4

                    -- deleting test skill sle entry
                    DELETE FROM {db_name}..cms_models_testskillsle
                    WHERE test_skill_id = @test_skill_id_4

                    FETCH NEXT FROM db_cursor_4 INTO @test_skill_id_4
                END

                CLOSE db_cursor_4
                DEALLOCATE db_cursor_4
                -- ==================== FRENCH WRITING (END) ====================
        """.format(
                db_name=settings.DATABASES["default"]["NAME"]
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        (
            "cms_models",
            "0101_adding_and_updating_skill_types_data",
        ),
    ]
    operations = [migrations.RunPython(update_data, rollback_changes)]
