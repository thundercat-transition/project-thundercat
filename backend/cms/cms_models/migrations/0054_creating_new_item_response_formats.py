from django.db import migrations


def create_new_item_response_formats(apps, _):
    # get models
    item_response_formats = apps.get_model("cms_models", "itemresponseformats")
    # creating all needed item response formats
    response_format_1 = item_response_formats(
        codename="is_mc",
        en_name="MULTIPLE CHOICE",
        fr_name="FR MULTIPLE CHOICE",
    )
    response_format_1.save()


def rollback_changes(apps, schema_editor):
    # get models
    item_response_formats = apps.get_model("cms_models", "itemresponseformats")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the item response formats
    item_response_formats.objects.using(db_alias).filter(codename="is_mc").delete()


class Migration(migrations.Migration):
    dependencies = [("cms_models", "0053_auto_20221215_1204")]
    operations = [
        migrations.RunPython(create_new_item_response_formats, rollback_changes)
    ]
