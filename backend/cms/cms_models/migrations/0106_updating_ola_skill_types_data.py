from django.db import migrations
from backend.static.languages import Language_id, Languages
from backend.static.test_skill_codenames import (
    TestSkillOccupationalDescCodename,
    TestSkillTypeCodename,
    TestSkillSLEDescCodename,
)

##################################################
# OLD                                            #
##################################################
# OE
OLD_TEST_SKILL_TYPE_OE_EN = "English Oral"
OLD_TEST_SKILL_TYPE_OE_FR = "Anglais oral"

# OF
OLD_TEST_SKILL_TYPE_OF_EN = "French Oral"
OLD_TEST_SKILL_TYPE_OF_FR = "Français oral"
##################################################

##################################################
# NEW                                            #
##################################################
# OE
NEW_TEST_SKILL_TYPE_OE_EN = "English - Oral"
NEW_TEST_SKILL_TYPE_OE_FR = "Anglais - Oral"

# OF
NEW_TEST_SKILL_TYPE_OF_EN = "French - Oral"
NEW_TEST_SKILL_TYPE_OF_FR = "Français - Oral"
##################################################


def modify_ola_test_skill_types_data(apps, schema_editor):
    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # TestSkillSleDesc
    TestSkillSleDesc = apps.get_model("cms_models", "TestSkillSleDesc")

    # TestSkillSleDescText
    TestSkillSleDescText = apps.get_model("cms_models", "TestSkillSleDescText")

    # Language
    language = apps.get_model("custom_models", "language")

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # --------------------------------------------------------------------------------- #
    # Language Data - Getting the 2 Official Languages
    # --------------------------------------------------------------------------------- #
    # 1. English
    language_en = language.objects.using(db_alias).get(ISO_Code_1=Languages.EN)

    # 2. French
    language_fr = language.objects.using(db_alias).get(ISO_Code_1=Languages.FR)

    # --------------------------------------------------------------------------------- #
    # TestSkillSleDesc Data - Getting the Test Skill SLE Desc IDs
    # --------------------------------------------------------------------------------- #
    # 1. OE
    test_skill_sle_desc_id_oe = (
        TestSkillSleDesc.objects.using(db_alias)
        .get(codename=TestSkillSLEDescCodename.ORAL_EN)
        .id
    )

    # 2. OF
    test_skill_sle_desc_id_of = (
        TestSkillSleDesc.objects.using(db_alias)
        .get(codename=TestSkillSLEDescCodename.ORAL_FR)
        .id
    )

    # --------------------------------------------------------------------------------- #
    # TestSkillSleDescText Data - changing all ola skill Descriptions' Text
    # --------------------------------------------------------------------------------- #
    # 1. Oral English - English
    test_skill_sle_desc_text_oe_en = TestSkillSleDescText.objects.using(db_alias).get(
        language_id=language_en, test_skill_sle_desc_id=test_skill_sle_desc_id_oe
    )
    test_skill_sle_desc_text_oe_en.text = NEW_TEST_SKILL_TYPE_OE_EN
    test_skill_sle_desc_text_oe_en.save()

    # 2. Oral English - French
    test_skill_sle_desc_text_oe_fr = TestSkillSleDescText.objects.using(db_alias).get(
        language_id=language_fr, test_skill_sle_desc_id=test_skill_sle_desc_id_oe
    )
    test_skill_sle_desc_text_oe_fr.text = NEW_TEST_SKILL_TYPE_OE_FR
    test_skill_sle_desc_text_oe_fr.save()

    # 3. Oral French - English
    test_skill_sle_desc_text_of_en = TestSkillSleDescText.objects.using(db_alias).get(
        language_id=language_en, test_skill_sle_desc_id=test_skill_sle_desc_id_of
    )
    test_skill_sle_desc_text_of_en.text = NEW_TEST_SKILL_TYPE_OF_EN
    test_skill_sle_desc_text_of_en.save()

    # 4. Oral French - French
    test_skill_sle_desc_text_of_fr = TestSkillSleDescText.objects.using(db_alias).get(
        language_id=language_fr, test_skill_sle_desc_id=test_skill_sle_desc_id_of
    )
    test_skill_sle_desc_text_of_fr.text = NEW_TEST_SKILL_TYPE_OF_FR
    test_skill_sle_desc_text_of_fr.save()


def rollback_changes(apps, schema_editor):
    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # TestSkillSleDesc
    TestSkillSleDesc = apps.get_model("cms_models", "TestSkillSleDesc")

    # TestSkillSleDescText
    TestSkillSleDescText = apps.get_model("cms_models", "TestSkillSleDescText")

    # Language
    language = apps.get_model("custom_models", "language")

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # --------------------------------------------------------------------------------- #
    # Language Data - Getting the 2 Official Languages
    # --------------------------------------------------------------------------------- #
    # 1. English
    language_en = language.objects.using(db_alias).get(ISO_Code_1=Languages.EN)

    # 2. French
    language_fr = language.objects.using(db_alias).get(ISO_Code_1=Languages.FR)

    # --------------------------------------------------------------------------------- #
    # TestSkillSleDesc Data - Getting the Test Skill SLE Desc IDs
    # --------------------------------------------------------------------------------- #
    # 1. OE
    test_skill_sle_desc_id_oe = (
        TestSkillSleDesc.objects.using(db_alias)
        .get(codename=TestSkillSLEDescCodename.ORAL_EN)
        .id
    )

    # 2. OF
    test_skill_sle_desc_id_of = (
        TestSkillSleDesc.objects.using(db_alias)
        .get(codename=TestSkillSLEDescCodename.ORAL_FR)
        .id
    )

    # --------------------------------------------------------------------------------- #
    # TestSkillSleDescText Data - changing all ola skill Descriptions' Text
    # --------------------------------------------------------------------------------- #
    # 1. Oral English - English
    test_skill_sle_desc_text_oe_en = TestSkillSleDescText.objects.using(db_alias).get(
        language_id=language_en, test_skill_sle_desc_id=test_skill_sle_desc_id_oe
    )
    test_skill_sle_desc_text_oe_en.text = OLD_TEST_SKILL_TYPE_OE_EN
    test_skill_sle_desc_text_oe_en.save()

    # 2. Oral English - French
    test_skill_sle_desc_text_oe_fr = TestSkillSleDescText.objects.using(db_alias).get(
        language_id=language_fr, test_skill_sle_desc_id=test_skill_sle_desc_id_oe
    )
    test_skill_sle_desc_text_oe_fr.text = OLD_TEST_SKILL_TYPE_OE_FR
    test_skill_sle_desc_text_oe_fr.save()

    # 3. Oral French - English
    test_skill_sle_desc_text_of_en = TestSkillSleDescText.objects.using(db_alias).get(
        language_id=language_en, test_skill_sle_desc_id=test_skill_sle_desc_id_of
    )
    test_skill_sle_desc_text_of_en.text = OLD_TEST_SKILL_TYPE_OF_EN
    test_skill_sle_desc_text_of_en.save()

    # 4. Oral French - French
    test_skill_sle_desc_text_of_fr = TestSkillSleDescText.objects.using(db_alias).get(
        language_id=language_fr, test_skill_sle_desc_id=test_skill_sle_desc_id_of
    )
    test_skill_sle_desc_text_of_fr.text = OLD_TEST_SKILL_TYPE_OF_FR
    test_skill_sle_desc_text_of_fr.save()


class Migration(migrations.Migration):
    dependencies = [
        (
            "cms_models",
            "0105_historicalitembankrule_content_type_and_more",
        )
    ]
    operations = [
        migrations.RunPython(modify_ola_test_skill_types_data, rollback_changes)
    ]
