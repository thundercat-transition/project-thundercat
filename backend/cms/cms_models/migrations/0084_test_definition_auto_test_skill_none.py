from django.db import migrations
from backend.static.test_skill_codenames import TestSkillTypeCodename


def insert_none_test_skill_for_current_test_definitions(apps, schema_editor):
    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # TestDefinition
    TestDefinition = apps.get_model("cms_models", "TestDefinition")

    # TestSkill
    TestSkill = apps.get_model("cms_models", "TestSkill")

    # TestSkillType
    TestSkillType = apps.get_model("cms_models", "TestSkillType")

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #

    db_alias = schema_editor.connection.alias

    # --------------------------------------------------------------------------------- #
    # Test Skill Type Data - Getting the None Test Skill
    # --------------------------------------------------------------------------------- #

    test_skill_type_none = TestSkillType.objects.using(db_alias).get(
        codename=TestSkillTypeCodename.NONE
    )

    # --------------------------------------------------------------------------------- #
    # Test Skill Data - For Each Test Definition, Create TestSkill with "None" Test Skill Type
    # --------------------------------------------------------------------------------- #

    for td in TestDefinition.objects.using(db_alias).all():
        new_test_skill = TestSkill(
            test_skill_type=test_skill_type_none, test_definition=td
        )
        new_test_skill.save()


def rollback_changes(apps, schema_editor):
    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # TestSkill
    TestSkill = apps.get_model("cms_models", "TestSkill")

    # TestSkillType
    TestSkillType = apps.get_model("cms_models", "TestSkillType")

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #

    db_alias = schema_editor.connection.alias

    # --------------------------------------------------------------------------------- #
    # Test Skill Type Data - Getting the None Test Skill
    # --------------------------------------------------------------------------------- #

    test_skill_type_none = TestSkillType.objects.using(db_alias).get(
        codename=TestSkillTypeCodename.NONE
    )

    # --------------------------------------------------------------------------------- #
    # Test Skill Data - Delete All Rows Where Test Skill Type is None
    # --------------------------------------------------------------------------------- #

    TestSkill.objects.using(db_alias).filter(
        test_skill_type=test_skill_type_none
    ).delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "cms_models",
            "0083_adding_skill_types_data",
        )
    ]
    operations = [
        migrations.RunPython(
            insert_none_test_skill_for_current_test_definitions, rollback_changes
        )
    ]
