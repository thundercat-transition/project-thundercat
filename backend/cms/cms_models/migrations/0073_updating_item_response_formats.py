from django.db import migrations
from cms.static.items_utils import ItemResponseFormat


OLD_NAME_EN = "Mutliple-Choice"
NEW_NAME_EN = "Multiple-Choice"


def update_item_response_formats(apps, schema_editor):
    # get models
    item_response_formats = apps.get_model("cms_models", "itemresponseformats")
    # get db alias
    db_alias = schema_editor.connection.alias

    # updating respective item response formats
    response_format_1 = item_response_formats.objects.using(db_alias).get(
        codename=ItemResponseFormat.MULTIPLE_CHOICE
    )
    response_format_1.en_name = NEW_NAME_EN
    response_format_1.save()


def rollback_changes(apps, schema_editor):
    # get models
    item_response_formats = apps.get_model("cms_models", "itemresponseformats")
    # get db alias
    db_alias = schema_editor.connection.alias

    # updating respective item response formats
    response_format_1 = item_response_formats.objects.using(db_alias).get(
        codename=ItemResponseFormat.MULTIPLE_CHOICE
    )
    response_format_1.en_name = OLD_NAME_EN
    response_format_1.save()


class Migration(migrations.Migration):
    dependencies = [("cms_models", "0072_auto_20230601_1027")]
    operations = [migrations.RunPython(update_item_response_formats, rollback_changes)]
