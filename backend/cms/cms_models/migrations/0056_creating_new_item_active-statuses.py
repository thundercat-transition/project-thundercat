from django.db import migrations


def create_new_item_active_statuses(apps, _):
    # get models
    item_active_statuses = apps.get_model("cms_models", "itemactivestatuses")
    # creating all needed item development statuses
    activity_status_1 = item_active_statuses(
        codename="is_active",
        en_name="ACTIVE",
        fr_name="FR ACTIVE",
    )
    activity_status_1.save()

    activity_status_2 = item_active_statuses(
        codename="is_dormant",
        en_name="DORMANT",
        fr_name="FR DORMANT",
    )
    activity_status_2.save()


def rollback_changes(apps, schema_editor):
    # get models
    item_active_statuses = apps.get_model("cms_models", "itemactivestatuses")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the item development statuses
    item_active_statuses.objects.using(db_alias).filter(codename="is_active").delete()
    item_active_statuses.objects.using(db_alias).filter(codename="is_dormant").delete()


class Migration(migrations.Migration):
    dependencies = [("cms_models", "0055_creating_new_item_development_statuses")]
    operations = [
        migrations.RunPython(create_new_item_active_statuses, rollback_changes)
    ]
