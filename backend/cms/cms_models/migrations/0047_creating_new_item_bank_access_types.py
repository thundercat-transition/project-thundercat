from django.db import migrations


def create_new_item_bank_access_types(apps, _):
    # get models
    item_bank_access_types = apps.get_model("cms_models", "itembankaccesstypes")
    # creating all needed item bank access types
    access_type_1 = item_bank_access_types(
        codename="is_none",
        en_description="None (TEMP)",
        fr_description="FR None (TEMP)",
    )
    access_type_1.save()

    access_type_2 = item_bank_access_types(
        codename="is_owner",
        en_description="Owner (TEMP)",
        fr_description="FR Owner (TEMP)",
    )
    access_type_2.save()

    access_type_3 = item_bank_access_types(
        codename="is_contributor",
        en_description="Contributor (TEMP)",
        fr_description="FR Contributor (TEMP)",
    )
    access_type_3.save()


def rollback_changes(apps, schema_editor):
    # get models
    item_bank_access_types = apps.get_model("cms_models", "itembankaccesstypes")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the types
    item_bank_access_types.objects.using(db_alias).filter(codename="is_none").delete()
    item_bank_access_types.objects.using(db_alias).filter(codename="is_owner").delete()
    item_bank_access_types.objects.using(db_alias).filter(
        codename="is_contributor"
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("cms_models", "0046_auto_20221125_0857")]
    operations = [
        migrations.RunPython(create_new_item_bank_access_types, rollback_changes)
    ]
