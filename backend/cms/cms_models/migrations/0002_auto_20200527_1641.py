# Generated by Django 2.2.3 on 2020-05-27 20:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [("custom_models", "0001_initial"), ("cms_models", "0001_initial")]

    operations = [
        migrations.AddField(
            model_name="testdefinition",
            name="test_language",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.Language",
                to_field="ISO_Code_1",
            ),
        ),
        migrations.AddField(
            model_name="sectioncomponentpage",
            name="test_section_component",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.TestSectionComponent",
            ),
        ),
        migrations.AddField(
            model_name="questionsectiontypemarkdown",
            name="language",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.Language",
                to_field="ISO_Code_1",
            ),
        ),
        migrations.AddField(
            model_name="questionsectiontypemarkdown",
            name="question_section",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.QuestionSection",
            ),
        ),
        migrations.AddField(
            model_name="questionsection",
            name="question",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.NewQuestion",
            ),
        ),
        migrations.AddField(
            model_name="questionlistrule",
            name="question_block_type",
            field=models.ManyToManyField(to="cms_models.QuestionBlockType"),
        ),
        migrations.AddField(
            model_name="questionlistrule",
            name="test_section_component",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.TestSectionComponent",
            ),
        ),
        migrations.AddField(
            model_name="questionblocktype",
            name="test_definition",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.TestDefinition",
            ),
        ),
        migrations.AddField(
            model_name="pagesectiontypetreedescription",
            name="address_book_contact",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.AddressBookContact",
            ),
        ),
        migrations.AddField(
            model_name="pagesectiontypetreedescription",
            name="language",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.Language",
                to_field="ISO_Code_1",
            ),
        ),
        migrations.AddField(
            model_name="pagesectiontypetreedescription",
            name="page_section",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.PageSection",
            ),
        ),
        migrations.AddField(
            model_name="pagesectiontypesampletaskresponse",
            name="language",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.Language",
                to_field="ISO_Code_1",
            ),
        ),
        migrations.AddField(
            model_name="pagesectiontypesampletaskresponse",
            name="page_section",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.PageSection",
            ),
        ),
        migrations.AddField(
            model_name="pagesectiontypesampleemailresponse",
            name="language",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.Language",
                to_field="ISO_Code_1",
            ),
        ),
        migrations.AddField(
            model_name="pagesectiontypesampleemailresponse",
            name="page_section",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.PageSection",
            ),
        ),
        migrations.AddField(
            model_name="pagesectiontypesampleemail",
            name="language",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.Language",
                to_field="ISO_Code_1",
            ),
        ),
        migrations.AddField(
            model_name="pagesectiontypesampleemail",
            name="page_section",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.PageSection",
            ),
        ),
        migrations.AddField(
            model_name="pagesectiontypemarkdown",
            name="language",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.Language",
                to_field="ISO_Code_1",
            ),
        ),
        migrations.AddField(
            model_name="pagesectiontypemarkdown",
            name="page_section",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.PageSection",
            ),
        ),
        migrations.AddField(
            model_name="pagesectiontypeimagezoom",
            name="language",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.Language",
                to_field="ISO_Code_1",
            ),
        ),
        migrations.AddField(
            model_name="pagesectiontypeimagezoom",
            name="page_section",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.PageSection",
            ),
        ),
        migrations.AddField(
            model_name="pagesection",
            name="section_component_page",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.SectionComponentPage",
            ),
        ),
        migrations.AddField(
            model_name="nextsectionbuttontypeproceed",
            name="language",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.Language",
                to_field="ISO_Code_1",
            ),
        ),
        migrations.AddField(
            model_name="nextsectionbuttontypeproceed",
            name="test_section",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.TestSection",
            ),
        ),
        migrations.AddField(
            model_name="nextsectionbuttontypepopup",
            name="language",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.Language",
                to_field="ISO_Code_1",
            ),
        ),
        migrations.AddField(
            model_name="nextsectionbuttontypepopup",
            name="test_section",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.TestSection",
            ),
        ),
        migrations.AddField(
            model_name="newquestion",
            name="dependencies",
            field=models.ManyToManyField(
                related_name="_newquestion_dependencies_+", to="cms_models.NewQuestion"
            ),
        ),
        migrations.AddField(
            model_name="newquestion",
            name="question_block_type",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.QuestionBlockType",
            ),
        ),
        migrations.AddField(
            model_name="newquestion",
            name="test_section_component",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.TestSectionComponent",
            ),
        ),
        migrations.AddField(
            model_name="multiplechoicequestion",
            name="question",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.NewQuestion",
            ),
        ),
        migrations.AddField(
            model_name="fileresourcedetails",
            name="test_definition",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.TestDefinition",
            ),
        ),
        migrations.AddField(
            model_name="emailquestiondetails",
            name="email_question",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.EmailQuestion",
            ),
        ),
        migrations.AddField(
            model_name="emailquestiondetails",
            name="language",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.Language",
                to_field="ISO_Code_1",
            ),
        ),
        migrations.AddField(
            model_name="emailquestion",
            name="cc_field",
            field=models.ManyToManyField(
                blank=True,
                related_name="emailquestion_cc_field",
                to="cms_models.AddressBookContact",
            ),
        ),
        migrations.AddField(
            model_name="emailquestion",
            name="from_field",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                related_name="emailquestion_from_field",
                to="cms_models.AddressBookContact",
            ),
        ),
        migrations.AddField(
            model_name="emailquestion",
            name="question",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.NewQuestion",
            ),
        ),
        migrations.AddField(
            model_name="emailquestion",
            name="to_field",
            field=models.ManyToManyField(
                related_name="emailquestion_to_field",
                to="cms_models.AddressBookContact",
            ),
        ),
        migrations.AddField(
            model_name="answerdetails",
            name="answer",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING, to="cms_models.Answer"
            ),
        ),
        migrations.AddField(
            model_name="answerdetails",
            name="language",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.Language",
                to_field="ISO_Code_1",
            ),
        ),
        migrations.AddField(
            model_name="answer",
            name="question",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.NewQuestion",
            ),
        ),
        migrations.AddField(
            model_name="addressbookcontactdetails",
            name="contact",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.AddressBookContact",
            ),
        ),
        migrations.AddField(
            model_name="addressbookcontactdetails",
            name="language",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="custom_models.Language",
                to_field="ISO_Code_1",
            ),
        ),
        migrations.AddField(
            model_name="addressbookcontact",
            name="parent",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="cms_models.AddressBookContact",
            ),
        ),
        migrations.AddField(
            model_name="addressbookcontact",
            name="test_section",
            field=models.ManyToManyField(to="cms_models.TestSection"),
        ),
        migrations.AlterUniqueTogether(
            name="testdefinition", unique_together={("test_code", "version")}
        ),
        migrations.AlterUniqueTogether(
            name="fileresourcedetails", unique_together={("path", "test_definition")}
        ),
    ]
