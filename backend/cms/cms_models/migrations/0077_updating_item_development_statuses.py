from django.db import migrations
from cms.static.items_utils import ItemDevelopmentStatus

OLD_NAME_EN = "Regular"
OLD_NAME_FR = "Régulier"
NEW_NAME_EN = "Operational"
NEW_NAME_FR = "Opérationnel"


def update_item_development_statuses(apps, schema_editor):
    # get models
    item_development_statuses = apps.get_model("cms_models", "itemdevelopmentstatuses")
    # get db alias
    db_alias = schema_editor.connection.alias

    # updating respective item development statuses
    development_status_1 = item_development_statuses.objects.using(db_alias).get(
        codename=ItemDevelopmentStatus.REGULAR
    )
    development_status_1.en_name = NEW_NAME_EN
    development_status_1.fr_name = NEW_NAME_FR
    development_status_1.save()


def rollback_changes(apps, schema_editor):
    # get models
    item_development_statuses = apps.get_model("cms_models", "itemdevelopmentstatuses")
    # get db alias
    db_alias = schema_editor.connection.alias

    # updating respective item development statuses
    development_status_1 = item_development_statuses.objects.using(db_alias).get(
        codename=ItemDevelopmentStatus.REGULAR
    )
    development_status_1.en_name = OLD_NAME_EN
    development_status_1.fr_name = OLD_NAME_FR
    development_status_1.save()


class Migration(migrations.Migration):
    dependencies = [("cms_models", "0076_auto_20230712_1110")]
    operations = [
        migrations.RunPython(update_item_development_statuses, rollback_changes)
    ]
