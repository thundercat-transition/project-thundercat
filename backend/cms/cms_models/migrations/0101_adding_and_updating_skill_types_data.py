from django.db import migrations
from backend.static.languages import Language_id, Languages
from backend.static.test_skill_codenames import (
    TestSkillOccupationalDescCodename,
    TestSkillTypeCodename,
    TestSkillSLEDescCodename,
)

TEST_SKILL_TYPE_TEXT_OCC_OLD_NAME_EN = "Occupational"
TEST_SKILL_TYPE_TEXT_OCC_OLD_NAME_FR = "Aptitude professionnelle"

TEST_SKILL_TYPE_TEXT_OCC_NEW_NAME_EN = "Other Occupational Test"
TEST_SKILL_TYPE_TEXT_OCC_NEW_NAME_FR = "Autre test occupationnel"


def insert_test_skill_types_data(apps, schema_editor):
    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # TestSkillType
    TestSkillType = apps.get_model("cms_models", "TestSkillType")

    # TestSkillTypeText
    TestSkillTypeText = apps.get_model("cms_models", "TestSkillTypeText")

    # TestSkillOccupationalDesc
    TestSkillOccupationalDesc = apps.get_model(
        "cms_models", "TestSkillOccupationalDesc"
    )

    # TestSkillOccupationalDescText
    TestSkillOccupationalDescText = apps.get_model(
        "cms_models", "TestSkillOccupationalDescText"
    )

    # Language
    language = apps.get_model("custom_models", "language")

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # --------------------------------------------------------------------------------- #
    # Language Data - Getting the 2 Official Languages
    # --------------------------------------------------------------------------------- #
    # 1. English
    language_en = language.objects.using(db_alias).get(ISO_Code_1=Languages.EN)

    # 2. French
    language_fr = language.objects.using(db_alias).get(ISO_Code_1=Languages.FR)

    # --------------------------------------------------------------------------------- #
    # TestSkillTypeText Data - updating occupational skill types' text
    # --------------------------------------------------------------------------------- #
    test_skill_type__id_occ = (
        TestSkillType.objects.using(db_alias).get(codename=TestSkillTypeCodename.OCC).id
    )

    test_skill_type_occ_en = TestSkillTypeText.objects.using(db_alias).get(
        language_id=Language_id.EN, test_skill_type_id=test_skill_type__id_occ
    )
    test_skill_type_occ_en.text = TEST_SKILL_TYPE_TEXT_OCC_NEW_NAME_EN
    test_skill_type_occ_en.save()

    test_skill_type_occ_fr = TestSkillTypeText.objects.using(db_alias).get(
        language_id=Language_id.FR, test_skill_type_id=test_skill_type__id_occ
    )
    test_skill_type_occ_fr.text = TEST_SKILL_TYPE_TEXT_OCC_NEW_NAME_FR
    test_skill_type_occ_fr.save()

    # --------------------------------------------------------------------------------- #
    # TestSkillOccupationalDesc Data - creating all needed OCC Skill Descriptions' Codenames
    # --------------------------------------------------------------------------------- #
    # 1. Test of Judgement
    test_skill_occ_desc_1 = TestSkillOccupationalDesc(
        codename=TestSkillOccupationalDescCodename.TEST_OF_JUDGEMENT
    )
    test_skill_occ_desc_1.save()

    # 2. General Competency Test - Level 1
    test_skill_occ_desc_2 = TestSkillOccupationalDesc(
        codename=TestSkillOccupationalDescCodename.GENERAL_COMPETENCY_TEST_LEVEL_1
    )
    test_skill_occ_desc_2.save()

    # 3. General Competency Test - Level 2
    test_skill_occ_desc_3 = TestSkillOccupationalDesc(
        codename=TestSkillOccupationalDescCodename.GENERAL_COMPETENCY_TEST_LEVEL_2
    )
    test_skill_occ_desc_3.save()

    # --------------------------------------------------------------------------------- #
    # TestSkillOccupationalDescText Data - creating all needed OCC Skill Descriptions' Text
    # --------------------------------------------------------------------------------- #
    # 1. Test of Judgement - English
    test_skill_occ_desc_text_1 = TestSkillOccupationalDescText(
        text="Test of Judgement",
        language=language_en,
        test_skill_occupational_desc=test_skill_occ_desc_1,
    )
    test_skill_occ_desc_text_1.save()

    # 2. Test of Judgement - French
    test_skill_occ_desc_text_2 = TestSkillOccupationalDescText(
        text="Test de jugement",
        language=language_fr,
        test_skill_occupational_desc=test_skill_occ_desc_1,
    )
    test_skill_occ_desc_text_2.save()

    # 3. General Competency Test Level 1 - English
    test_skill_occ_desc_text_3 = TestSkillOccupationalDescText(
        text="General Competency Test - Level 1",
        language=language_en,
        test_skill_occupational_desc=test_skill_occ_desc_2,
    )
    test_skill_occ_desc_text_3.save()

    # 4. General Competency Test Level 1 - French
    test_skill_occ_desc_text_4 = TestSkillOccupationalDescText(
        text="Examen de compétence générale - Niveau 1",
        language=language_fr,
        test_skill_occupational_desc=test_skill_occ_desc_2,
    )
    test_skill_occ_desc_text_4.save()

    # 5. General Competency Test Level 2 - English
    test_skill_occ_desc_text_5 = TestSkillOccupationalDescText(
        text="General Competency Test - Level 2",
        language=language_en,
        test_skill_occupational_desc=test_skill_occ_desc_3,
    )
    test_skill_occ_desc_text_5.save()

    # 6. General Competency Test Level 2 - French
    test_skill_occ_desc_text_6 = TestSkillOccupationalDescText(
        text="Examen de compétence générale - Niveau 2",
        language=language_fr,
        test_skill_occupational_desc=test_skill_occ_desc_3,
    )
    test_skill_occ_desc_text_6.save()


def rollback_changes(apps, schema_editor):
    # --------------------------------------------------------------------------------- #
    # Get Models
    # --------------------------------------------------------------------------------- #

    # TestSkillType
    TestSkillType = apps.get_model("cms_models", "TestSkillType")

    # TestSkillTypeText
    TestSkillTypeText = apps.get_model("cms_models", "TestSkillTypeText")

    # TestSkillOccupationalDesc
    TestSkillOccupationalDesc = apps.get_model(
        "cms_models", "TestSkillOccupationalDesc"
    )

    # TestSkillOccupationalDescText
    TestSkillOccupationalDescText = apps.get_model(
        "cms_models", "TestSkillOccupationalDescText"
    )

    # --------------------------------------------------------------------------------- #
    # Get DB Alias
    # --------------------------------------------------------------------------------- #
    db_alias = schema_editor.connection.alias

    # --------------------------------------------------------------------------------- #
    # TestSkillTypeText Data - updating occupational skill types' text
    # --------------------------------------------------------------------------------- #
    test_skill_type__id_occ = (
        TestSkillType.objects.using(db_alias).get(codename=TestSkillTypeCodename.OCC).id
    )

    test_skill_type_occ_en = TestSkillTypeText.objects.using(db_alias).get(
        language_id=Language_id.EN, test_skill_type_id=test_skill_type__id_occ
    )
    test_skill_type_occ_en.text = TEST_SKILL_TYPE_TEXT_OCC_OLD_NAME_EN
    test_skill_type_occ_en.save()

    test_skill_type_occ_fr = TestSkillTypeText.objects.using(db_alias).get(
        language_id=Language_id.FR, test_skill_type_id=test_skill_type__id_occ
    )
    test_skill_type_occ_fr.text = TEST_SKILL_TYPE_TEXT_OCC_OLD_NAME_FR
    test_skill_type_occ_fr.save()

    # --------------------------------------------------------------------------------- #
    # TestSkillOccupationalDescText Data - Delete All The Occ Skill Descriptions' Text
    # --------------------------------------------------------------------------------- #
    # 1. Test of Judgement - English
    TestSkillOccupationalDescText.objects.using(db_alias).filter(
        text="Test of Judgement"
    ).delete()

    # 2. Test of Judgement - French
    TestSkillOccupationalDescText.objects.using(db_alias).filter(
        text="Test de jugement"
    ).delete()

    # 3. General Competency Test Level 1 - English
    TestSkillOccupationalDescText.objects.using(db_alias).filter(
        text="General Competency Test - Level 1"
    ).delete()

    # 4. General Competency Test Level 1 - French
    TestSkillOccupationalDescText.objects.using(db_alias).filter(
        text="Examen de compétence générale - Niveau 1"
    ).delete()

    # 5. General Competency Test Level 2 - English
    TestSkillOccupationalDescText.objects.using(db_alias).filter(
        text="General Competency Test - Level 2"
    ).delete()

    # 6. General Competency Test Level 2 - French
    TestSkillOccupationalDescText.objects.using(db_alias).filter(
        text="Examen de compétence générale - Niveau 2"
    ).delete()

    # --------------------------------------------------------------------------------- #
    # TestSkillOccupationalDesc - Delete All The OCC Skill Descriptions' Codenames
    # --------------------------------------------------------------------------------- #
    # 1. Test of Judgement
    TestSkillOccupationalDesc.objects.using(db_alias).filter(
        codename=TestSkillOccupationalDescCodename.TEST_OF_JUDGEMENT
    ).delete()

    # 2. General Competency Test - Level 1
    TestSkillOccupationalDesc.objects.using(db_alias).filter(
        codename=TestSkillOccupationalDescCodename.GENERAL_COMPETENCY_TEST_LEVEL_1
    ).delete()

    # 3. General Competency Test - Level 2
    TestSkillOccupationalDesc.objects.using(db_alias).filter(
        codename=TestSkillOccupationalDescCodename.GENERAL_COMPETENCY_TEST_LEVEL_2
    ).delete()


class Migration(migrations.Migration):
    dependencies = [
        (
            "cms_models",
            "0100_remove_historicalpagesectiontypeitembankinstruction_item_and_more",
        )
    ]
    operations = [migrations.RunPython(insert_test_skill_types_data, rollback_changes)]
