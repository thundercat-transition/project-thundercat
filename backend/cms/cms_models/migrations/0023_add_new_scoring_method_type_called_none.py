from django.db import migrations


def create_new_scoring_method_type_called_none(apps, _):
    # get models
    scoring_method_types = apps.get_model("cms_models", "scoringmethodtypes")
    # creating all needed TA Action Types
    method_type = scoring_method_types(
        method_type_codename="NONE", en_name="None", fr_name="FR None"
    )
    method_type.save()


def rollback_new_scoring_method_type(apps, schema_editor):
    # get models
    scoring_method_types = apps.get_model("cms_models", "scoringmethodtypes")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the types
    scoring_method_types.objects.using(db_alias).filter(
        method_type_codename="NONE"
    ).delete()


class Migration(migrations.Migration):
    dependencies = [("cms_models", "0022_auto_20210615_0823")]
    operations = [
        migrations.RunPython(
            create_new_scoring_method_type_called_none, rollback_new_scoring_method_type
        )
    ]
