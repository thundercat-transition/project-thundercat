from django.db import models
from simple_history.models import HistoricalRecords


class ItemBankAccessTypes(models.Model):
    en_description = models.CharField(max_length=150)
    fr_description = models.CharField(max_length=150)
    codename = models.CharField(max_length=25, unique=True, blank=False, null=False)
    priority = models.IntegerField(default=1, null=False, blank=False)

    # for auditing purpose and it stores create, update,delete actions on this model in a seperate historical table
    history = HistoricalRecords()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0}, codename: {1}, description: {2}".format(
            self.id, self.codename, self.en_description
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Item Bank Access Types"
        constraints = [
            models.UniqueConstraint(
                name="must_be_a_unique_item_bank_access_type_codename",
                fields=["codename"],
            )
        ]
