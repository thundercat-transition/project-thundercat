from operator import itemgetter
from rest_framework.response import Response
from rest_framework import status
from cms.cms_models.test_permissions_model import TestPermissions
from cms.views.utils import get_needed_parameters

# updating test permission
def update_test_permission(request):
    success, parameters = get_needed_parameters(
        ["test_permission_id", "expiry_date", "reason_for_modification"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_permission_id, expiry_date, reason_for_modification = itemgetter(
        "test_permission_id", "expiry_date", "reason_for_modification"
    )(parameters)

    try:
        # getting specified test permission
        test_permission = TestPermissions.objects.get(id=test_permission_id)
        # updating specified test permission
        test_permission.expiry_date = expiry_date
        test_permission.reason_for_modif_or_del = reason_for_modification
        test_permission.save()
        return Response(status=status.HTTP_200_OK)
    except TestPermissions.DoesNotExist:
        return Response(
            {"error": "the specified test permission does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )
