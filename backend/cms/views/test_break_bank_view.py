from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework import permissions
from cms.views.test_break_bank import (
    get_break_bank_remaining_time,
    trigger_break_bank_action,
)


class GetBreakBankRemainingTime(APIView):
    def get(self, request):
        return get_break_bank_remaining_time(request)

    def get_permissions(self):
        try:
            return [permissions.IsAuthenticated()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


class TriggerBreakBankAction(APIView):
    def post(self, request):
        return trigger_break_bank_action(request)

    def get_permissions(self):
        try:
            return [permissions.IsAuthenticated()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]
