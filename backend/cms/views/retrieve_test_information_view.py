from operator import itemgetter
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework import permissions
from cms.cms_models.test_skill_occupational import TestSkillOccupational
from cms.cms_models.test_skill_sle import TestSkillSLE
from cms.cms_models.test_skill_type import TestSkillType
from cms.cms_models.test_skill import TestSkill
from cms.serializers.test_section_serializer import TestSectionTitleDescSerializer
from cms.cms_models.test_section import TestSection
from cms.views.utils import get_needed_parameters, get_optional_parameters
from cms.serializers.get_test_information_serializer import (
    GetActiveNonPublicTestsSerializer,
)
from cms.cms_models.test_definition import TestDefinition
from backend.api_permissions.role_based_api_permissions import (
    HasSystemAdminPermission,
    HasAaePermission,
)
from backend.static.test_skill_codenames import TestSkillTypeCodename


# getting non public tests (where is_public is set to false)
class GetActiveNonPublicTests(APIView):
    def get(self, request):
        return Response(get_active_non_public_tests(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


def get_active_non_public_tests(request):
    optional_parameters = get_optional_parameters(
        ["test_skill_type_id", "test_skill_sub_type_id"], request
    )

    # test_skill_type_id is provided
    if optional_parameters["test_skill_type_id"] is not None:
        # test_skill_sub_type_id is provided
        if optional_parameters["test_skill_sub_type_id"] is not None:
            # getting sle_test_skill_type_id
            sle_test_skill_type_id = TestSkillType.objects.get(
                codename=TestSkillTypeCodename.SLE
            ).id
            occ_test_skill_type_id = TestSkillType.objects.get(
                codename=TestSkillTypeCodename.OCC
            ).id
            # Test Skill Type SLE
            if int(optional_parameters["test_skill_type_id"]) == sle_test_skill_type_id:
                # only getting related test skill IDs based on provided test_skill_sub_type_id
                related_test_skill_ids = TestSkillSLE.objects.filter(
                    test_skill_sle_desc_id=int(
                        optional_parameters["test_skill_sub_type_id"]
                    )
                ).values_list("test_skill_id", flat=True)
            # Test Skill Type OCC
            elif (
                int(optional_parameters["test_skill_type_id"]) == occ_test_skill_type_id
            ):
                # only getting related test skill IDs based on provided test_skill_sub_type_id
                related_test_skill_ids = TestSkillOccupational.objects.filter(
                    test_skill_occupational_desc_id=int(
                        optional_parameters["test_skill_sub_type_id"]
                    )
                ).values_list("test_skill_id", flat=True)

        # no test_skill_sub_type_id id provided (None test skill type)
        else:
            # only getting related test skill IDs based on provided test_skill_type_id
            related_test_skill_ids = TestSkill.objects.filter(
                test_skill_type_id=int(optional_parameters["test_skill_type_id"])
            ).values_list("id", flat=True)

        # getting test definition IDs linked to the related_test_skill_ids
        related_test_definition_ids = TestSkill.objects.filter(
            id__in=related_test_skill_ids
        ).values_list("test_definition_id", flat=True)

        # getting related active non public tests
        active_non_public_tests = TestDefinition.objects.filter(
            id__in=related_test_definition_ids,
            is_public=False,
            active=True,
            archived=False,
        )

    # test_skill_type_id is NOT provided (get all active non public tests)
    else:
        # getting all active non public tests
        active_non_public_tests = TestDefinition.objects.filter(
            is_public=False, active=True, archived=False
        )

    serialized = GetActiveNonPublicTestsSerializer(active_non_public_tests, many=True)
    return serialized.data


# getting default test time
class GetTestData(APIView):
    def get(self, request):
        return get_test_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasAaePermission()]


def get_test_data(request):
    success, parameters = get_needed_parameters(["test_definition_id"], request)
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    test_definition_id = itemgetter("test_definition_id")(parameters)

    # initializing default_test_time
    default_test_time = 0

    # getting timed test sections
    timed_test_sections = TestSection.objects.filter(
        test_definition_id=test_definition_id, default_time__isnull=False
    )

    # looping in timed_test_sections
    for timed_test_section in timed_test_sections:
        # incrementing default_test_time
        default_test_time += timed_test_section.default_time

    # serializing the timed test sections
    serialized_test_sections = TestSectionTitleDescSerializer(
        timed_test_sections, many=True
    ).data

    return Response(
        {
            "default_test_time": default_test_time,
            "test_sections": serialized_test_sections,
        }
    )
