from rest_framework import status
from rest_framework.response import Response
from cms.cms_models.test_section import TestSection
from cms.serializers.test_section_serializer import TestSectionSerializer
from cms.static.test_section_type import TestSectionType
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.custom_models.assigned_test import AssignedTest
from backend.views.utils import is_undefined
from backend.static.assigned_test_status import (
    AssignedTestStatus,
    get_assigned_test_status_id,
)
from backend.custom_models.user_accommodation_file_test_time import (
    UserAccommodationFileTestTime,
)
from backend.custom_models.user_accommodation_file import UserAccommodationFile
from backend.custom_models.user_accommodation_file_status import (
    UserAccommodationFileStatus,
)
from backend.static.user_accommodation_file_status_const import (
    UserAccommodationFileStatusConst,
)


def retrieve_test_section_data_by_id(assigned_test_id, section_id, language):
    context = {}
    context["language"] = language
    context["assigned_test_id"] = assigned_test_id
    # get the assigned test
    assigned_test = AssignedTest.objects.get(id=assigned_test_id)
    # fetch test section data
    test_section = TestSection.objects.get(id=section_id)

    # check if the test is still active or assigned
    checked_in_status_id = get_assigned_test_status_id(AssignedTestStatus.CHECKED_IN)
    pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
    active_status_id = get_assigned_test_status_id(AssignedTestStatus.ACTIVE)
    transition_status_id = get_assigned_test_status_id(AssignedTestStatus.TRANSITION)
    locked_status_id = get_assigned_test_status_id(AssignedTestStatus.LOCKED)
    paused_status_id = get_assigned_test_status_id(AssignedTestStatus.PAUSED)
    if assigned_test.status_id not in (
        pre_test_status_id,
        active_status_id,
        transition_status_id,
        checked_in_status_id,
        locked_status_id,
        paused_status_id,
    ) and test_section.section_type not in (
        TestSectionType.FINISH,
        TestSectionType.QUIT,
    ):
        return Response(
            {"error": "assigned test is in {} state".format(assigned_test.status_id)},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # fetch the next section time
    next_test_section = TestSection.objects.filter(
        test_definition_id=assigned_test.test_id, order=test_section.order + 1
    ).first()
    if next_test_section:
        next_section_time = next_test_section.default_time
        # there is an associated user accommodation file
        if assigned_test.user_accommodation_file_id is not None:
            # getting related user accommodation data
            related_user_accommodation_file_data = UserAccommodationFile.objects.get(
                id=assigned_test.user_accommodation_file_id
            )
            # making sure that this request has been CLOSED/COMPLETED before going further
            completed_status_id = UserAccommodationFileStatus.objects.get(
                codename=UserAccommodationFileStatusConst.COMPLETED
            ).id
            if related_user_accommodation_file_data.status_id == completed_status_id:
                # getting respective additional time
                respective_additional_time = UserAccommodationFileTestTime.objects.filter(
                    user_accommodation_file=assigned_test.user_accommodation_file_id,
                    test_section_id=next_test_section.id,
                )
                if respective_additional_time:
                    next_section_time = (
                        respective_additional_time.last().test_section_time
                    )
        # no user accommodation file, but might have additional time set from TA on the AssignedTestSection
        else:
            next_assigned_test_section = AssignedTestSection.objects.get(
                assigned_test_id=assigned_test.id, test_section=next_test_section
            )
            if next_assigned_test_section.test_section_time:
                next_section_time = next_assigned_test_section.test_section_time
        context["next_test_section_time"] = next_section_time

    # there is an associated user accommodation file
    if assigned_test.user_accommodation_file_id is not None:
        # getting related user accommodation data
        related_user_accommodation_file_data = UserAccommodationFile.objects.get(
            id=assigned_test.user_accommodation_file_id
        )
        # making sure that this request has been CLOSED/COMPLETED before going further
        completed_status_id = UserAccommodationFileStatus.objects.get(
            codename=UserAccommodationFileStatusConst.COMPLETED
        ).id
        if related_user_accommodation_file_data.status_id == completed_status_id:
            # getting respective additional time
            respective_additional_time = UserAccommodationFileTestTime.objects.filter(
                user_accommodation_file=assigned_test.user_accommodation_file_id,
                test_section_id=test_section.id,
            )
            if respective_additional_time:
                current_test_section_time = (
                    respective_additional_time.last().test_section_time
                )
    # no user accommodation file, but might have additional time set from TA on the AssignedTestSection
    else:
        current_test_section_time = AssignedTestSection.objects.get(
            assigned_test_id=assigned_test_id, test_section_id=test_section.id
        ).test_section_time

    # TA did not update the test time for the current section
    if current_test_section_time is None:
        # default time
        current_test_section_time = test_section.default_time

    # providing needed context
    context["current_test_section_time"] = current_test_section_time
    context["assigned_test_id"] = assigned_test_id
    # if next test section exists
    if next_test_section:
        context["next_test_section_id"] = next_test_section.id

    # serialize the data for return
    serialized = TestSectionSerializer(test_section, many=False, context=context).data

    return Response(serialized, status=status.HTTP_200_OK)


def retrieve_public_test_section_data_by_id(section_id, test_definition, language):
    context = {}
    context["language"] = language

    start_time = ""

    # fetch test section data
    test_section = TestSection.objects.get(id=section_id)
    # fetch the next section time
    next_test_section = TestSection.objects.filter(
        test_definition_id=test_definition.id, order=test_section.order + 1
    ).first()
    if next_test_section:
        next_test_section_time = next_test_section.default_time
        if not is_undefined(next_test_section_time):
            context["next_test_section_time"] = next_test_section_time

    serialized = TestSectionSerializer(test_section, many=False, context=context).data
    serialized["start_time"] = start_time

    return Response(serialized, status=status.HTTP_200_OK)
