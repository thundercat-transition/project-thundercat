from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from cms.serializers.test_definition_serializer import TestDefinitionSerializer
from cms.cms_models.test_definition import TestDefinition

# This is the new api for getting test sections related to the factory


class PublicTests(APIView):
    def get(self, request):
        public_tests = TestDefinition.objects.filter(is_public=True, active=True)

        return Response(TestDefinitionSerializer(public_tests, many=True).data)

    def get_permissions(self):
        return [permissions.IsAuthenticatedOrReadOnly()]
