from rest_framework.views import APIView
from rest_framework import permissions
from cms.views.grant_test_permissions import grant_test_permissions
from cms.views.get_test_permissions import (
    get_test_permissions,
    get_ta_orderless_test_permissions,
    get_ta_orderless_uit_test_permissions,
    get_selected_user_test_permissions,
    get_test_permission_financial_data,
    get_all_active_test_permissions,
    get_found_active_test_permissions,
    get_all_orderless_test_permissions,
    get_found_orderless_test_administrators,
    get_orderless_test_administrators,
    add_orderless_test_administrators,
    get_orderless_test_accesses,
    get_found_orderless_test_accesses,
    add_delete_orderless_test_access,
    delete_orderless_test_administrator,
    update_orderless_test_administrator_department,
)
from cms.views.delete_test_permission import delete_test_permission
from cms.views.update_test_permission import update_test_permission
from backend.api_permissions.role_based_api_permissions import (
    HasSystemAdminPermission,
    HasTestAdminPermission,
)

# assigning test permission to user
class GrantTestPermission(APIView):
    def post(self, request):
        return grant_test_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# getting test permissions of a specific user
class GetTestPermissions(APIView):
    permission_classes = (
        permissions.IsAuthenticated & HasSystemAdminPermission | HasTestAdminPermission,
    )

    def get(self, request):
        return get_test_permissions(request)


# getting orderless test permissions of a specific user
class GetTaOrderlessTestPermissions(APIView):
    permission_classes = (
        permissions.IsAuthenticated & HasSystemAdminPermission | HasTestAdminPermission,
    )

    def get(self, request):
        return get_ta_orderless_test_permissions(request)

# getting orderless uit test permissions of a specific user
class GetTaOrderlessUitTestPermissions(APIView):
    permission_classes = (
        permissions.IsAuthenticated & HasSystemAdminPermission | HasTestAdminPermission,
    )

    def get(self, request):
        return get_ta_orderless_uit_test_permissions(request)



# getting specific test permission's financial data
class GetTestPermissionFinancialData(APIView):
    def get(self, request):
        return get_test_permission_financial_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasTestAdminPermission()]


# getting all active test permissions
class GetAllActiveTestPermissions(APIView):
    def get(self, request):
        return get_all_active_test_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# getting found active test permissions
class GetFoundActiveTestPermissions(APIView):
    def get(self, request):
        return get_found_active_test_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# deleting test permission
class DeleteTestPermission(APIView):
    def post(self, request):
        return delete_test_permission(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# updating test permission
class UpdateTestPermission(APIView):
    def post(self, request):
        return update_test_permission(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# getting test permissions for selected user
class GetSelectedUserTestPermissions(APIView):
    def get(self, request):
        return get_selected_user_test_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# getting all orderless test administrators
class GetAllOrderlessTestAdministrators(APIView):
    def get(self, request):
        return get_all_orderless_test_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# getting found orderless test administrators
class GetFoundOrderlessTestAdministrators(APIView):
    def get(self, request):
        return get_found_orderless_test_administrators(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# getting orderless test administrators
class GetOrderlessTestAdministrators(APIView):
    def get(self, request):
        return get_orderless_test_administrators(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# adding orderless test administrators
class AddOrderlessTestAdministrators(APIView):
    def post(self, request):
        return add_orderless_test_administrators(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# getting orderless test accesses
class GetOrderlessTestAccesses(APIView):
    def get(self, request):
        return get_orderless_test_accesses(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# getting found orderless test accesses
class GetFoundOrderlessTestAccesses(APIView):
    def get(self, request):
        return get_found_orderless_test_accesses(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# adding/deleting orderless test access
class AddDeleteOrderlessTestAccess(APIView):
    def post(self, request):
        return add_delete_orderless_test_access(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# deleting orderless test administrator
class DeleteOrderlessTestAdministrator(APIView):
    def post(self, request):
        return delete_orderless_test_administrator(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]


# updating orderless test administrator department
class UpdateOrderlessTestAdministratorDepartment(APIView):
    def post(self, request):
        return update_orderless_test_administrator_department(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated(), HasSystemAdminPermission()]
