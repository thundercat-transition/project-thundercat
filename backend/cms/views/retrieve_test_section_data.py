from datetime import timedelta, datetime, date
import pytz
from operator import itemgetter
from django.utils import timezone
from rest_framework import status
from rest_framework.response import Response
from cms.cms_models.scoring_methods import ScoringMethods
from cms.static.test_section_component_type import TestSectionComponentType
from cms.views.test_break_bank import BreakBankActionsConstants
from cms.views.utils import get_needed_parameters
from cms.cms_models.test_section import TestSection
from cms.cms_models.test_definition import TestDefinition
from cms.serializers.test_section_serializer import TestSectionSerializer
from cms.static.test_section_type import TestSectionType
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.assigned_test_section_access_times import (
    AssignedTestSectionAccessTimes,
)
from backend.custom_models.ta_actions import TaActions
from backend.custom_models.lock_test_actions import LockTestActions
from backend.custom_models.user_accommodation_file import UserAccommodationFile
from backend.custom_models.additional_time import AdditionalTime
from backend.custom_models.user_accommodation_file_break_bank import (
    UserAccommodationFileBreakBank,
)
from backend.custom_models.user_accommodation_file_test_time import (
    UserAccommodationFileTestTime,
)
from backend.custom_models.user_accommodation_file_status import (
    UserAccommodationFileStatus,
)
from backend.static.user_accommodation_file_status_const import (
    UserAccommodationFileStatusConst,
)
from backend.views.utils import (
    is_undefined,
    handleViewedQuestionsLogic,
    get_new_break_bank_remaining_time,
    get_last_accessed_test_section,
)
from backend.views.test_administrator_actions import TaActionsConstants
from backend.custom_models.candidate_answers import CandidateAnswers

from backend.static.assigned_test_section_access_time_type import (
    AssignedTestSectionAccessTimeType,
)
from backend.static.assigned_test_status import (
    AssignedTestStatus,
    get_assigned_test_status_id,
)
from backend.views.uit_score_test import uit_scoring, Action
from backend.custom_models.accommodation_request import AccommodationRequest
from backend.custom_models.break_bank import BreakBank
from backend.custom_models.break_bank_actions import BreakBankActions
from backend.custom_models.uit_invites import UITInvites
from db_views.db_view_models.ta_assigned_candidates_vw import TaAssignedCandidatesVW
from db_views.db_view_models.test_center_test_sessions_vw import (
    TestCenterTestSessionsVW,
)
from dateutil.relativedelta import relativedelta


class SpecialSection:
    QUIT = "quit"


def is_test_public(test_id):
    # function that returns true if the test is public and false on the other hand
    return TestDefinition.objects.get(id=test_id).is_public


def retrieve_test_section_data(
    order,
    assigned_test_id,
    language,
    quit_section=False,
    timed_out=False,
    trigger_access_time_entry=True,
):
    context = {}
    context["language"] = language
    context["assigned_test_id"] = assigned_test_id
    # get the assigned test
    assigned_test = AssignedTest.objects.get(id=assigned_test_id)
    parameters = {"assigned_test_id": assigned_test.id}

    # check if the test is still active or assigned
    checked_in_status_id = get_assigned_test_status_id(AssignedTestStatus.CHECKED_IN)
    pre_test_status_id = get_assigned_test_status_id(AssignedTestStatus.PRE_TEST)
    unassigned_status_id = get_assigned_test_status_id(AssignedTestStatus.UNASSIGNED)
    active_status_id = get_assigned_test_status_id(AssignedTestStatus.ACTIVE)
    transition_status_id = get_assigned_test_status_id(AssignedTestStatus.TRANSITION)
    locked_status_id = get_assigned_test_status_id(AssignedTestStatus.LOCKED)
    paused_status_id = get_assigned_test_status_id(AssignedTestStatus.PAUSED)
    submitted_status_id = get_assigned_test_status_id(AssignedTestStatus.SUBMITTED)
    quit_status_id = get_assigned_test_status_id(AssignedTestStatus.QUIT)
    if assigned_test.status_id not in (
        pre_test_status_id,
        active_status_id,
        transition_status_id,
        checked_in_status_id,
        locked_status_id,
        paused_status_id,
    ):
        return Response(
            {"error": "assigned test is in {} state".format(assigned_test.status_id)},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # if we are in a check_in or pre_test status
    if assigned_test.status_id in (checked_in_status_id, pre_test_status_id):
        # making sure that we are respecting the validity end date (UIT tests) and the test session end time (supervised tests)
        # getting current date
        current_date = date.today()
        # getting current time (UTC)
        current_time = datetime.now(pytz.utc)
        # supervised test
        if assigned_test.uit_invite_id is None:
            # getting related test session data
            related_test_session_data = TestCenterTestSessionsVW.objects.get(
                id=assigned_test.test_session_id
            )
            # we are past the test session end time
            if current_time > related_test_session_data.end_time:
                # unassigning test
                temp_assigned_test = AssignedTest.objects.get(id=assigned_test.id)
                temp_assigned_test.status_id = unassigned_status_id
                temp_assigned_test.save()
                return Response({{}}, status=status.HTTP_200_OK)
        # unsupervised test
        elif assigned_test.uit_invite_id is not None:
            # getting related uit invite data
            related_uit_invite_data = UITInvites.objects.get(
                id=assigned_test.uit_invite_id
            )
            # we are past the validity end date
            if current_date > related_uit_invite_data.validity_end_date:
                # initializing need_to_unassign_test
                need_to_unassign_test = False
                # assigned test of current iteration is associated to a user accommodation file
                if assigned_test.user_accommodation_file_id is not None:
                    # getting cancelled related user accommodation file status IDs
                    cancelled_related_user_accommodation_file_status_ids = (
                        UserAccommodationFileStatus.objects.filter(
                            codename__in=(
                                UserAccommodationFileStatusConst.CANCELLED,
                                UserAccommodationFileStatusConst.CANCELLED_BY_CANDIDATE,
                                UserAccommodationFileStatusConst.EXPIRED,
                            )
                        ).values_list("id", flat=True)
                    )
                    # checking if test of current iteration is associated to an active AAE Request
                    # getting related user accommodation file data
                    related_user_accommodation_file_data = (
                        UserAccommodationFile.objects.filter(
                            id=assigned_test.user_accommodation_file_id
                        )
                        .order_by("id")
                        .last()
                    )
                    # status of related_user_accommodation_file_data is part of the cancelled_related_user_accommodation_file_status_ids
                    if (
                        related_user_accommodation_file_data.status_id
                        in cancelled_related_user_accommodation_file_status_ids
                    ):
                        need_to_unassign_test = True
                # assigned test of current iteration is not associated to a user accommodation file
                else:
                    need_to_unassign_test = True

                # need_to_unassign_test is set to True at this point
                if need_to_unassign_test:
                    # unassigning test
                    temp_assigned_test = AssignedTest.objects.get(id=assigned_test.id)
                    temp_assigned_test.status_id = unassigned_status_id
                    temp_assigned_test.save()
                    return Response({{}}, status=status.HTTP_200_OK)
        # should never go there
        else:
            pass

    # updating assigned test start date (only if null)
    if assigned_test.start_date is None:
        assigned_test.start_date = timezone.now()
        assigned_test.save()

    # initializing new_order
    new_order = None
    # getting assigned test secions
    assigned_test_section_object = AssignedTestSection.objects.filter(
        assigned_test_id=assigned_test.id
    )
    # populating assigned_test_sections_array
    assigned_test_sections_array = []
    for assigned_test_secion in assigned_test_section_object:
        assigned_test_sections_array.append(assigned_test_secion.id)
    # getting last assigned_test_setion_access_time (most recent one)
    assigned_test_setion_access_time = (
        AssignedTestSectionAccessTimes.objects.filter(
            assigned_test_section_id__in=assigned_test_sections_array,
            time_type=AssignedTestSectionAccessTimeType.START,
        )
        .order_by("time")
        .last()
    )

    if assigned_test_setion_access_time:
        # getting most recent assigned test section
        most_recent_assigned_test_section = AssignedTestSection.objects.get(
            id=assigned_test_setion_access_time.assigned_test_section_id
        )
        # getting order based on found mose recent assigned test section
        test_section_data = TestSection.objects.get(
            id=most_recent_assigned_test_section.test_section_id
        )
        new_order = test_section_data.order
        # if new_order greater than initial peovided order
        if new_order > int(order):
            # overriding order
            order = new_order - 1

    ###
    # finish current test section
    ###
    if int(order) != 0:
        # get the current test section
        test_section = TestSection.objects.get(
            test_definition_id=assigned_test.test_id, order=order
        )
        # get the assigned test section for the above
        assigned_test_section = AssignedTestSection.objects.get(
            assigned_test_id=assigned_test.id, test_section=test_section
        )
        # if timed_out flag is set to True and test_section_time is defined
        if timed_out == "true" and assigned_test_section.test_section_time is not None:
            # update assigned test section timed_out flag
            assigned_test_section.timed_out = True
            assigned_test_section.save()
        # check if there is already an existing access time for that assigned test section
        access_time = (
            AssignedTestSectionAccessTimes.objects.filter(
                assigned_test_section=assigned_test_section,
                time_type=AssignedTestSectionAccessTimeType.FINISH,
            )
            .order_by("time")
            .first()
        )
        # create a "finish" time stamp for the current test section only if the trigger_access_time_entry is set to "true"
        if trigger_access_time_entry == "true":
            access_time = AssignedTestSectionAccessTimes(
                assigned_test_section=assigned_test_section,
                time_type=AssignedTestSectionAccessTimeType.FINISH,
            )
            access_time.save()

        # score the current test section (adds together in final scoring)
        parameters["test_section_id"] = test_section.id
        uit_scoring(parameters, Action.TEST_SECTION)
    # change the test status to pre-test (first time accessing) + removing previous_state (if it has one)
    else:
        # if assigned test status is not LOCKED or PAUSED
        if (
            assigned_test.status_id is not locked_status_id
            and assigned_test.status_id is not paused_status_id
        ):
            assigned_test.status_id = pre_test_status_id
            assigned_test.previous_status_id = None
            assigned_test.save()

    ###
    # start new test section now
    ###
    next_test_section_order = ""

    if quit_section:
        # get the quit section of the test
        test_section = TestSection.objects.get(
            test_definition_id=assigned_test.test_id, section_type=TestSectionType.QUIT
        )

        next_test_section_order = str(test_section.order)

        try:
            # Adding an update to candidate answer historical data for question time spent report purposes
            last_candiate_answers_historical_data = (
                CandidateAnswers.history.filter(assigned_test_id=assigned_test.id)
                .order_by("history_date")
                .last()
            )

            # initializing the question_source to QUESTION_LIST
            question_source = TestSectionComponentType.QUESTION_LIST
            # question_id is NULL, that means the question source is from the ITEM BANK
            if last_candiate_answers_historical_data.question_id is None:
                # updating question_source to ITEM BANK
                question_source = TestSectionComponentType.ITEM_BANK

            # questions coming from test builder
            if question_source == TestSectionComponentType.QUESTION_LIST:
                candidate_answer = CandidateAnswers.objects.get(
                    assigned_test_id=assigned_test.id,
                    question_id=last_candiate_answers_historical_data.question_id,
                )
                candidate_answer.modify_date = timezone.now()
                candidate_answer.save()
            # questions coming from item bank
            else:
                candidate_answer = CandidateAnswers.objects.get(
                    assigned_test_id=assigned_test.id,
                    item_id=last_candiate_answers_historical_data.item_id,
                )
                candidate_answer.modify_date = timezone.now()
                candidate_answer.save()

            candidate_answer.modify_date = timezone.now()
            candidate_answer.save()
        except:
            pass

        # check if the candidate is on PAUSE
        if assigned_test.status_id == paused_status_id:
            # getting new break bank remaining time
            new_remaining_time = get_new_break_bank_remaining_time(
                assigned_test.accommodation_request_id
            )
            # creating new UNPAUSE action in Break Bank Actions table
            BreakBankActions.objects.create(
                action_type=BreakBankActionsConstants.UNPAUSE,
                new_remaining_time=new_remaining_time,
                break_bank_id=AccommodationRequest.objects.get(
                    id=assigned_test.accommodation_request_id
                ).break_bank_id,
                test_section_id=test_section_data.id,
            )
        # set the test to quit + update test section id + update submit date
        assigned_test.status_id = quit_status_id
        assigned_test.submit_date = timezone.now()
        assigned_test.test_section_id = test_section.id
        assigned_test.save()
        # checking if test is linked to an accommodation
        linked_user_accommodation_file = UserAccommodationFile.objects.filter(
            id=assigned_test.user_accommodation_file_id
        )
        # found linked accommodation
        if linked_user_accommodation_file:
            # updating user accommodation file status to ADMINISTERED
            user_accommodation_file_to_update = linked_user_accommodation_file.last()
            administered_status_id = UserAccommodationFileStatus.objects.get(
                codename=UserAccommodationFileStatusConst.ADMINISTERED
            ).id
            user_accommodation_file_to_update.status_id = administered_status_id
            user_accommodation_file_to_update.save()
        # handling viewed questions logic
        handleViewedQuestionsLogic(assigned_test.id)
    else:
        next_section_invalid = True
        # get a valid test section order number
        while next_section_invalid:
            next_test_section_order = str(int(order) + 1)
            # find the new test section
            test_section = TestSection.objects.get(
                test_definition_id=assigned_test.test_id, order=next_test_section_order
            )

            assigned_test_section = AssignedTestSection.objects.get(
                assigned_test_id=assigned_test.id, test_section=test_section
            )

            # update test section id
            assigned_test.test_section_id = test_section.id
            assigned_test.save()

            # if the next section is not timed, we can return this section
            if is_undefined(test_section.default_time):
                break

            # if the next section is timed and current status is PRE_TEST
            if (
                (
                    AssignedTestSection.objects.get(
                        assigned_test_id=assigned_test.id,
                        test_section_id=test_section.id,
                    ).test_section_time
                    is not None
                )
                or (TestSection.object.get(id=test_section.id).default_time is not None)
            ) and assigned_test.status_id == pre_test_status_id:
                # UIT Test + there is an existing user accommodations request
                if (
                    assigned_test.uit_invite_id is not None
                    and assigned_test.user_accommodation_file_id is not None
                ):
                    # getting completed status ID
                    completed_user_accommodation_file_status_id = (
                        UserAccommodationFileStatus.objects.get(
                            codename=UserAccommodationFileStatusConst.COMPLETED
                        ).id
                    )
                    # getting respective user accommodation file data
                    respective_user_accommodation_file_data = (
                        UserAccommodationFile.objects.get(
                            id=assigned_test.user_accommodation_file_id
                        )
                    )
                    # respective accommodations request is CLOSED/COMPLETED
                    if (
                        respective_user_accommodation_file_data.status_id
                        == completed_user_accommodation_file_status_id
                    ):
                        # checking if there is a break bank set
                        break_bank_data = UserAccommodationFileBreakBank.objects.filter(
                            user_accommodation_file_id=respective_user_accommodation_file_data.id
                        )
                        # checking if there is some additional time set
                        additional_time_data = UserAccommodationFileTestTime.objects.filter(
                            user_accommodation_file_id=respective_user_accommodation_file_data.id
                        )
                        # break bank and/or additional time set
                        if break_bank_data or additional_time_data:
                            # initializing new_break_bank
                            new_break_bank_id = None
                            # if break bank set
                            if break_bank_data:
                                # creating new break bank entry
                                new_break_bank = BreakBank.objects.create(
                                    # converting minutes to seconds (*60)
                                    break_time=break_bank_data.last().break_time
                                    * 60
                                )
                                new_break_bank_id = new_break_bank.id
                            # creating new AccommodationRequest entry
                            new_accommodation_request = (
                                AccommodationRequest.objects.create(
                                    break_bank_id=new_break_bank_id
                                )
                            )
                            # if additional time set
                            if additional_time_data:
                                # looping in additional_time_data
                                for time_data in additional_time_data:
                                    # creating new entry in Additional Time table
                                    AdditionalTime.objects.create(
                                        test_section_time=time_data.test_section_time,
                                        test_section_id=time_data.test_section_id,
                                        accommodation_request_id=new_accommodation_request.id,
                                    )
                                    # checking if there is a matching Assigned Test Section
                                    matching_assigned_test_section = (
                                        AssignedTestSection.objects.filter(
                                            assigned_test_id=assigned_test.id,
                                            test_section_id=time_data.test_section_id,
                                        )
                                    )
                                    # if there is a match
                                    if matching_assigned_test_section:
                                        # updating respective test_section_time
                                        test_section_to_update = (
                                            matching_assigned_test_section.last()
                                        )
                                        test_section_to_update.test_section_time = (
                                            time_data.test_section_time
                                        )
                                        test_section_to_update.save()
                            # assigning new accommodation request to User Accommodation File
                            respective_user_accommodation_file_data.accommodation_request_id = (
                                new_accommodation_request.id
                            )
                            respective_user_accommodation_file_data.save()
                            # assigning new accommodation request to Assigned Test
                            assigned_test.accommodation_request_id = (
                                new_accommodation_request.id
                            )
                    # respective accommodations request is other than CLOSED/COMPLETED
                    else:
                        # getting cancelled status ID
                        cancelled_by_candidate_user_accommodation_file_status_id = UserAccommodationFileStatus.objects.get(
                            codename=UserAccommodationFileStatusConst.CANCELLED_BY_CANDIDATE
                        ).id
                        # updating request status to CANCELLED
                        respective_user_accommodation_file_data.status_id = (
                            cancelled_by_candidate_user_accommodation_file_status_id
                        )
                        respective_user_accommodation_file_data.save()

                # update assigned test status to ACTIVE
                assigned_test.status_id = active_status_id
                assigned_test.save()

            # if the next test section is timed and already completed, get another one
            if (
                AssignedTestSectionAccessTimes.objects.filter(
                    assigned_test_section=assigned_test_section,
                    time_type=AssignedTestSectionAccessTimeType.FINISH,
                ).count()
                == 0
            ):
                break
            else:
                order = int(order) + 1

    # Check if the new test section is special (a finish section) and change test status
    if test_section.section_type == TestSectionType.FINISH:
        # Adding an update to candidate answer historical data for question time spent report purposes
        last_candiate_answers_historical_data = (
            CandidateAnswers.history.filter(assigned_test_id=assigned_test.id)
            .order_by("history_date")
            .last()
        )

        # initializing the question_source to QUESTION_LIST
        question_source = TestSectionComponentType.QUESTION_LIST
        # question_id is NULL, that means the question source is from the ITEM BANK
        if last_candiate_answers_historical_data.question_id is None:
            # updating question_source to ITEM BANK
            question_source = TestSectionComponentType.ITEM_BANK

        # questions coming from test builder
        if question_source == TestSectionComponentType.QUESTION_LIST:
            candidate_answer = CandidateAnswers.objects.get(
                assigned_test_id=assigned_test.id,
                question_id=last_candiate_answers_historical_data.question_id,
            )
            candidate_answer.modify_date = timezone.now()
            candidate_answer.save()
        # questions coming from item bank
        else:
            candidate_answer = CandidateAnswers.objects.get(
                assigned_test_id=assigned_test.id,
                item_id=last_candiate_answers_historical_data.item_id,
            )
            candidate_answer.modify_date = timezone.now()
            candidate_answer.save()

        # set the test to submitted
        assigned_test.status_id = submitted_status_id
        assigned_test.submit_date = timezone.now()
        # checking if test is linked to an accommodation
        linked_user_accommodation_file = UserAccommodationFile.objects.filter(
            id=assigned_test.user_accommodation_file_id
        )
        # found linked accommodation
        if linked_user_accommodation_file:
            # updating user accommodation file status to ADMINISTERED
            user_accommodation_file_to_update = linked_user_accommodation_file.last()
            administered_status_id = UserAccommodationFileStatus.objects.get(
                codename=UserAccommodationFileStatusConst.ADMINISTERED
            ).id
            user_accommodation_file_to_update.status_id = administered_status_id
            user_accommodation_file_to_update.save()
        # calculating test validity based on respective test definition
        scoring_method = ScoringMethods.objects.get(test_id=assigned_test.test_id)
        if scoring_method.validity_period is not None:
            assigned_test.score_valid_until = date.today() + relativedelta(
                months=+scoring_method.validity_period
            )
        assigned_test.save()
        # we need to score the whole test now
        uit_scoring(parameters, Action.TEST)

    # check for oldest existing start times on this section
    # so we can set the new section start time to this
    access_time = (
        AssignedTestSectionAccessTimes.objects.filter(
            assigned_test_section=assigned_test_section,
            time_type=AssignedTestSectionAccessTimeType.START,
        )
        .order_by("time")
        .first()
    )
    start_time = ""
    if not is_undefined(access_time):
        start_time = access_time.time

    # create a "start" time stamp for the new test section for records only if
    #   - test has been started ==> previous status is not CHECKED_IN
    #   - trigger_access_time_entry is set to "true"
    if (
        assigned_test.previous_status_id != checked_in_status_id
        and trigger_access_time_entry == "true"
    ):
        access_time = AssignedTestSectionAccessTimes(
            assigned_test_section=assigned_test_section,
            time_type=AssignedTestSectionAccessTimeType.START,
        )
        access_time.save()

    # fetch test section data
    test_section = TestSection.objects.get(
        test_definition_id=assigned_test.test_id, order=next_test_section_order
    )
    # fetch the next section time
    next_test_section = TestSection.objects.filter(
        test_definition_id=assigned_test.test_id,
        order=str(int(next_test_section_order) + 1),
    ).first()

    # if next test section exists
    if next_test_section:
        next_section_time = next_test_section.default_time
        # there is an associated user accommodation file
        if assigned_test.user_accommodation_file_id is not None:
            # getting related user accommodation data
            related_user_accommodation_file_data = UserAccommodationFile.objects.get(
                id=assigned_test.user_accommodation_file_id
            )
            # making sure that this request has been CLOSED/COMPLETED before going further
            completed_status_id = UserAccommodationFileStatus.objects.get(
                codename=UserAccommodationFileStatusConst.COMPLETED
            ).id
            if related_user_accommodation_file_data.status_id == completed_status_id:
                # getting respective additional time
                respective_additional_time = UserAccommodationFileTestTime.objects.filter(
                    user_accommodation_file_id=assigned_test.user_accommodation_file_id,
                    test_section_id=next_test_section.id,
                )
                if respective_additional_time:
                    next_section_time = (
                        respective_additional_time.last().test_section_time
                    )
        # no user accommodation file, but might have additional time set from TA on the AssignedTestSection
        else:
            next_assigned_test_section = AssignedTestSection.objects.get(
                assigned_test_id=assigned_test.id, test_section=next_test_section
            )
            if next_assigned_test_section.test_section_time:
                next_section_time = next_assigned_test_section.test_section_time
        context["next_test_section_time"] = next_section_time
        context["next_test_section_id"] = next_test_section.id

    # initializing current_test_section_time
    current_test_section_time = None

    # there is an associated user accommodation file
    if assigned_test.user_accommodation_file_id is not None:
        # getting related user accommodation data
        related_user_accommodation_file_data = UserAccommodationFile.objects.get(
            id=assigned_test.user_accommodation_file_id
        )
        # making sure that this request has been CLOSED/COMPLETED before going further
        completed_status_id = UserAccommodationFileStatus.objects.get(
            codename=UserAccommodationFileStatusConst.COMPLETED
        ).id
        if related_user_accommodation_file_data.status_id == completed_status_id:
            # getting respective additional time
            respective_additional_time = UserAccommodationFileTestTime.objects.filter(
                user_accommodation_file_id=assigned_test.user_accommodation_file_id,
                test_section_id=test_section.id,
            )
            if respective_additional_time:
                current_test_section_time = (
                    respective_additional_time.last().test_section_time
                )
    # no user accommodation file, but might have additional time set from TA on the AssignedTestSection
    else:
        current_test_section_time = AssignedTestSection.objects.get(
            assigned_test_id=assigned_test_id, test_section_id=test_section.id
        ).test_section_time

    # TA did not update the test time for the current section
    if current_test_section_time is None:
        # default time
        current_test_section_time = test_section.default_time

    # providing needed context
    context["assigned_test_id"] = assigned_test_id
    context["current_test_section_time"] = current_test_section_time

    # current test section is not timed (has a default time of NULL) and status is ACTIVE (while not being locked or paused)
    if (
        test_section.default_time is None
        and assigned_test.status_id == active_status_id
        and assigned_test.status_id != locked_status_id
        and assigned_test.status_id != paused_status_id
    ):
        # checking if timed section has been accessed
        if TaAssignedCandidatesVW.objects.get(
            id=assigned_test.id
        ).timed_section_accessed:
            # updating status to TRANSITION
            assigned_test.status_id = transition_status_id
            assigned_test.save()
    # current test section is timed (has a default time defined) and status is not ACTIVE (while not being locked or paused)
    elif (
        test_section.default_time is not None
        and assigned_test.status_id != active_status_id
        and assigned_test.status_id != locked_status_id
        and assigned_test.status_id != paused_status_id
    ):
        # updating status to ACTIVE
        assigned_test.status_id = active_status_id
        assigned_test.save()

    # serialize the data for return
    serialized = TestSectionSerializer(test_section, many=False, context=context).data
    serialized["start_time"] = start_time

    return Response(serialized, status=status.HTTP_200_OK)


def retrieve_public_test_section_data(
    order, test_definition, special_section, language
):
    context = {}
    context["language"] = language
    ###
    # start new test section now
    ###
    next_test_section_order = ""

    next_test_section_order = str(int(order) + 1)
    # find the new test section
    test_section = TestSection.objects.get(
        test_definition_id=test_definition.id, order=next_test_section_order
    )

    start_time = ""

    # fetch test section data
    test_section = TestSection.objects.get(
        test_definition_id=test_definition.id, order=next_test_section_order
    )
    # fetch the next section time
    next_test_section = TestSection.objects.filter(
        test_definition_id=test_definition.id,
        order=str(int(next_test_section_order) + 1),
    ).first()
    if next_test_section:
        next_test_section_time = next_test_section.default_time
        if not is_undefined(next_test_section_time):
            context["next_test_section_time"] = next_test_section_time

    serialized = TestSectionSerializer(test_section, many=False, context=context).data
    serialized["start_time"] = start_time

    return Response(serialized, status=status.HTTP_200_OK)


def getting_updated_time_after_lock_pause(request):
    success, parameters = get_needed_parameters(
        ["assigned_test_id", "test_section_id"], request
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    assigned_test_id, test_section_id = itemgetter(
        "assigned_test_id", "test_section_id"
    )(parameters)

    # initializing needed variables
    assigned_test_section_access_time = None
    initial_time = None
    updated_time = None
    locked_time = 0
    consumed_break_bank_time = 0

    try:
        # getting test_section to make sure that this is a timed section
        test_section = TestSection.objects.get(id=test_section_id)
        # this is a timed section (default_time exists) + defined assigned test ID (real test)
        if (
            test_section.default_time is not None
            and assigned_test_id != "null"
            and assigned_test_id is not None
        ):
            # getting assigned test section access time (first one)
            assigned_test_section = AssignedTestSection.objects.get(
                assigned_test_id=assigned_test_id,
                test_section_id=test_section_id,
            )
            assigned_test_section_access_time = (
                AssignedTestSectionAccessTimes.objects.filter(
                    assigned_test_section_id=assigned_test_section.id
                ).first()
            )
        else:
            return Response(
                {
                    "info": "test section id {} is not a timed section".format(
                        test_section.id
                    )
                },
                status.HTTP_200_OK,
            )

        # assigned test section access time exists
        if assigned_test_section_access_time is not None:
            # updating initial_time with assigned_test_section_access_time value
            initial_time = assigned_test_section_access_time.time
        else:
            return Response(
                {
                    "error": "unable to find any assigned test section access time based on provided parameters"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        # test section provided has not been started yet
        if initial_time is None:
            return Response(
                {
                    "error": "test section id {} has not been started yet".format(
                        test_section_id
                    )
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        # getting LOCKED ta actions based on provided assigned_test_id and test_section_id
        ta_actions = TaActions.objects.filter(
            assigned_test_id=assigned_test_id,
            test_section_id=test_section_id,
            action_type_id=TaActionsConstants.LOCK,
        )
        # there is at least one existing LOCKED ta action
        if ta_actions:
            # looping in filtered ta_actions
            for ta_action in ta_actions:
                # LOCK action
                if ta_action.action_type_id == TaActionsConstants.LOCK:
                    # getting lock test action object
                    lock_test_action = LockTestActions.objects.get(
                        ta_action_id=ta_action.id
                    )

                    # no laock test action end date (still on lock)
                    if lock_test_action.lock_end_date is None:
                        return Response(
                            {
                                "info": "there is not lock end date yet for this lock test action {}".format(
                                    lock_test_action.id
                                )
                            },
                            status=status.HTTP_200_OK,
                        )
                    else:
                        # calculating locked time in seconds (keeping only 3 digits after the decimal point)
                        # incrementing locked_pause_time value
                        locked_time += round(
                            (
                                lock_test_action.lock_end_date
                                - lock_test_action.lock_start_date
                            ).total_seconds(),
                            3,
                        )

        # getting assigned_test data
        assigned_test_data = AssignedTest.objects.get(id=assigned_test_id)

        # there is an accommodation request
        if assigned_test_data.accommodation_request_id is not None:
            # getting break bank ID
            break_bank_id = AccommodationRequest.objects.get(
                id=assigned_test_data.accommodation_request_id
            ).break_bank_id
            # existing break bank accommodation request
            if break_bank_id is not None:
                # getting total break bank time
                initial_time_to_consider = BreakBank.objects.get(
                    id=break_bank_id
                ).break_time
                # getting last accessed assigned test section ID
                last_accessed_test_section_id = get_last_accessed_test_section(
                    assigned_test_data.id
                )
                # getting unpause break bank actions related to current test section (if they exist)
                test_section_related_unpause_break_bank_actions = (
                    BreakBankActions.objects.filter(
                        break_bank_id=break_bank_id,
                        action_type=BreakBankActionsConstants.UNPAUSE,
                        test_section_id=last_accessed_test_section_id,
                    ).order_by("modify_date")
                )

                if test_section_related_unpause_break_bank_actions:
                    # getting last unpause break bank action
                    last_unpause_break_bank_action = (
                        test_section_related_unpause_break_bank_actions.last()
                    )
                    # getting unpause break actions excluding the ones for the current test section (useful for multi sections test)
                    excluding_current_test_section_unpause_break_bank_actions = (
                        BreakBankActions.objects.filter(
                            break_bank_id=break_bank_id,
                            action_type=BreakBankActionsConstants.UNPAUSE,
                        )
                        .order_by("modify_date")
                        .exclude(
                            test_section_id=last_accessed_test_section_id,
                        )
                    )
                    # excluding_current_test_section_unpause_break_bank_actions exists
                    if excluding_current_test_section_unpause_break_bank_actions:
                        # reinitializing initial_time_to_consider
                        initial_time_to_consider = (
                            excluding_current_test_section_unpause_break_bank_actions.last().new_remaining_time
                        )
                    # calculating consumed break bank time (initial time to consider - last unpause action new remaining time)
                    consumed_break_bank_time = (
                        initial_time_to_consider
                        - last_unpause_break_bank_action.new_remaining_time
                    )

        # calculatingthe total additional time (locked_time + consumed_break_bank_time)
        total_additional_time = locked_time + consumed_break_bank_time

        # getting updated time
        updated_time = initial_time + timedelta(seconds=total_additional_time)
        return Response(updated_time)

    except AssignedTestSection.DoesNotExist:
        return Response(
            {
                "error": "unable to find any assigned test section based on provided parameters"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )

    except AssignedTestSectionAccessTimes.DoesNotExist:
        return Response(
            {
                "error": "unable to find any assigned test section access time based on provided parameters"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


# this function is useful for celery tasks
# before calling this function, make sure that the test is already started to avoid unwanted returns
def get_updated_time_after_lock_pause_function(assigned_test_id):
    # initializing time calculation variables
    locked_time = 0
    consumed_break_bank_time = 0
    initial_time = None
    # getting timed assigned test sections
    timed_assigned_test_sections = AssignedTestSection.objects.filter(
        assigned_test_id=assigned_test_id, test_section_time__isnull=False
    )
    if timed_assigned_test_sections:
        time = AssignedTestSectionAccessTimes.objects.filter(
            # first assigned test section
            assigned_test_section_id=timed_assigned_test_sections.first().id,
            time_type=AssignedTestSectionAccessTimeType.START,
        ).first()
        if time:
            # updating initial_time
            initial_time = time.time
    timed_assigned_test_section_test_section_ids = []
    for timed_assigned_test_section in timed_assigned_test_sections:
        timed_assigned_test_section_test_section_ids.append(
            timed_assigned_test_section.test_section_id
        )
    # getting LOCKED ta actions based on provided assigned_test_id and test_section_id
    ta_actions = TaActions.objects.filter(
        assigned_test_id=assigned_test_id,
        test_section_id__in=timed_assigned_test_section_test_section_ids,
        action_type_id=TaActionsConstants.LOCK,
    )
    # there is at least one existing LOCKED ta action
    if ta_actions:
        # looping in filtered ta_actions
        for ta_action in ta_actions:
            # getting test_section to make sure that this is a timed section
            test_section = TestSection.objects.get(id=ta_action.test_section_id)
            # this is a timed section (default_time exists)
            if test_section.default_time is not None:
                # getting assigned test section access time (first one)
                assigned_test_section = AssignedTestSection.objects.get(
                    assigned_test_id=assigned_test_id,
                    test_section_id=ta_action.test_section_id,
                )
                assigned_test_section_access_time = (
                    AssignedTestSectionAccessTimes.objects.filter(
                        assigned_test_section_id=assigned_test_section.id
                    ).first()
                )

                # assigned test section access time exists
                if assigned_test_section_access_time is not None:
                    # LOCK action
                    if ta_action.action_type_id == TaActionsConstants.LOCK:
                        # getting lock test action object
                        lock_test_action = LockTestActions.objects.get(
                            ta_action_id=ta_action.id
                        )

                        # no laock test action end date (still on lock)
                        if lock_test_action.lock_end_date is not None:
                            # calculating locked time in seconds (keeping only 3 digits after the decimal point)
                            # incrementing locked_time value
                            locked_time += round(
                                (
                                    lock_test_action.lock_end_date
                                    - lock_test_action.lock_start_date
                                ).total_seconds(),
                                3,
                            )

    # getting assigned_test data
    assigned_test_data = AssignedTest.objects.get(id=assigned_test_id)

    # there is an accommodation request
    if assigned_test_data.accommodation_request_id is not None:
        # getting break bank ID
        break_bank_id = AccommodationRequest.objects.get(
            id=assigned_test_data.accommodation_request_id
        ).break_bank_id
        # existing break bank accommodation request
        if break_bank_id is not None:
            # getting total break bank time
            initial_time_to_consider = BreakBank.objects.get(
                id=break_bank_id
            ).break_time
            # getting last accessed assigned test section ID
            last_accessed_test_section_id = get_last_accessed_test_section(
                assigned_test_data.id
            )
            # getting unpause break bank actions related to current test section (if they exist)
            test_section_related_unpause_break_bank_actions = (
                BreakBankActions.objects.filter(
                    break_bank_id=break_bank_id,
                    action_type=BreakBankActionsConstants.UNPAUSE,
                    test_section_id=last_accessed_test_section_id,
                ).order_by("modify_date")
            )

            if test_section_related_unpause_break_bank_actions:
                # getting last unpause break bank action
                last_unpause_break_bank_action = (
                    test_section_related_unpause_break_bank_actions.last()
                )
                # getting unpause break actions excluding the ones for the current test section (useful for multi sections test)
                excluding_current_test_section_unpause_break_bank_actions = (
                    BreakBankActions.objects.filter(
                        break_bank_id=break_bank_id,
                        action_type=BreakBankActionsConstants.UNPAUSE,
                    )
                    .order_by("modify_date")
                    .exclude(
                        test_section_id=last_accessed_test_section_id,
                    )
                )
                # excluding_current_test_section_unpause_break_bank_actions exists
                if excluding_current_test_section_unpause_break_bank_actions:
                    # reinitializing initial_time_to_consider
                    initial_time_to_consider = (
                        excluding_current_test_section_unpause_break_bank_actions.last().new_remaining_time
                    )
                # calculating consumed break bank time (initial time to consider - last unpause action new remaining time)
                consumed_break_bank_time = (
                    initial_time_to_consider
                    - last_unpause_break_bank_action.new_remaining_time
                )

    # if initial_time is defined
    if initial_time is not None:
        # calculatingthe total additional time (locked_time + consumed_break_bank_time)
        total_additional_time = locked_time + consumed_break_bank_time

        # getting updated time
        updated_time = initial_time + timedelta(seconds=total_additional_time)
    else:
        # should only happen for tests from Jmeter
        updated_time = "no-access-times-test"
    return {
        "time": updated_time,
        "timed_assigned_test_section_ids": timed_assigned_test_section_test_section_ids,
    }


# this function is useful to get the updated test time of the candidate
def get_updated_time_during_lock(assigned_test_id, test_section_id):
    try:
        # getting LOCKED ta actions based on provided assigned_test_id and test_section_id
        ta_actions = TaActions.objects.filter(
            assigned_test_id=assigned_test_id,
            test_section_id=test_section_id,
            action_type_id=TaActionsConstants.LOCK,
        )
        # there is at least one existing PAUSED/LOCKED ta action
        if ta_actions:
            # initializing time calculation variables
            locked_paused_time = 0
            initial_time = None
            # looping in filtered ta_actions
            for ta_action in ta_actions:
                # getting test_section to make sure that this is a timed section
                test_section = TestSection.objects.get(id=ta_action.test_section_id)
                # this is a timed section (default_time exists)
                if test_section.default_time is not None:
                    # getting assigned test section access time (first one)
                    assigned_test_section = AssignedTestSection.objects.get(
                        assigned_test_id=assigned_test_id,
                        test_section_id=ta_action.test_section_id,
                    )
                    assigned_test_section_access_time = (
                        AssignedTestSectionAccessTimes.objects.filter(
                            assigned_test_section_id=assigned_test_section.id
                        ).first()
                    )

                    # assigned test section access time exists
                    if assigned_test_section_access_time is not None:
                        # updating initial_time with assigned_test_section_access_time value
                        initial_time = assigned_test_section_access_time.time

                        # LOCK action
                        if ta_action.action_type_id == TaActionsConstants.LOCK:
                            # getting lock test action object
                            lock_test_action = LockTestActions.objects.get(
                                ta_action_id=ta_action.id
                            )

                            # lock test action end date
                            if lock_test_action.lock_end_date is not None:
                                # calculating locked time in seconds (keeping only 3 digits after the decimal point)
                                # incrementing locked_pause_time value
                                locked_paused_time += round(
                                    (
                                        lock_test_action.lock_end_date
                                        - lock_test_action.lock_start_date
                                    ).total_seconds(),
                                    3,
                                )

            # initial_time has been updated at least once
            if initial_time is not None:
                # getting updated time
                updated_time = initial_time + timedelta(seconds=locked_paused_time)
                return updated_time
    except:
        pass


# getting total test time of an assigned test
def get_total_assigned_test_time_in_seconds(assigned_test_id):
    # initializing total_test_time
    total_test_time = 0
    # getting assigned test sections
    assigned_test_sections = AssignedTestSection.objects.filter(
        assigned_test_id=assigned_test_id
    )
    # looping in assigned_test_sections
    for assigned_test_section in assigned_test_sections:
        if assigned_test_section.test_section_time is not None:
            total_test_time += int(assigned_test_section.test_section_time)
    # returning total test time (in seconds)
    return total_test_time * 60


# getting total test time of a specified test (not necessarily assigned test)
# * does not consider accommodations
def get_total_test_time(test_id):
    # initializing total_test_time
    total_test_time = 0
    # getting test sections
    test_sections = TestSection.objects.filter(test_definition_id=test_id)
    # looping in test_sections
    for test_section in test_sections:
        if test_section.default_time is not None:
            total_test_time += int(test_section.default_time)
    # returning total test time
    return total_test_time


def validate_timeout_state(request):
    success, parameters = get_needed_parameters(
        ["assigned_test_id", "test_section_id"],
        request,
    )
    if not success:
        return Response(parameters, status=status.HTTP_400_BAD_REQUEST)
    # getting assigned test section data
    assigned_test_section_data = AssignedTestSection.objects.get(
        assigned_test_id=parameters["assigned_test_id"],
        test_section_id=parameters["test_section_id"],
    )
    # timed section
    if assigned_test_section_data.test_section_time is not None:
        # getting current time
        current_time = datetime.now(pytz.utc)
        # getting updated time after locks and pauses
        updated_time = getting_updated_time_after_lock_pause(request)

        # calculating time between current and updated time (in minutes)
        calculated_time = (current_time - updated_time.data).total_seconds() / 60

        # calculated time > test section time
        if calculated_time > int(assigned_test_section_data.test_section_time):
            return Response(
                {"info": "user should be timed out"}, status=status.HTTP_409_CONFLICT
            )
        else:
            return Response({"info": "no action needed"}, status=status.HTTP_200_OK)
    # not a timed section
    else:
        return Response({"info": "no action needed"}, status=status.HTTP_200_OK)
