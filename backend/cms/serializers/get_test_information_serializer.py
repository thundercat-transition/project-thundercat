from rest_framework import serializers
from cms.cms_models.test_skill import TestSkill
from cms.cms_models.orderless_test_permissions_model import (
    OrderlessTestPermissions,
)
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_permissions_model import TestPermissions
from user_management.user_management_models.user_models import User
from user_management.user_management_models.ta_extended_profile_models import (
    TaExtendedProfile,
)
from backend.ref_table_views.cat_ref_departments_vw import CatRefDepartmentsVW
from backend.serializers.cat_ref_departments_serializer import (
    CatRefDepartmentsSerializer,
)
from backend.static.psc_department import PscDepartment
from backend.views.utils import get_test_skill_sub_type_for_test_skill


class GetActiveNonPublicTestsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDefinition
        fields = "__all__"


class GetEnFrTestNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDefinition
        fields = ("id", "en_name", "fr_name", "version", "test_code")


class GetTestPermissionsSerializer(serializers.ModelSerializer):
    test = serializers.SerializerMethodField()
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    department_ministry_data = serializers.SerializerMethodField()

    def get_test(self, request):
        test = TestDefinition.objects.get(id=request.test_id)
        return GetEnFrTestNameSerializer(test).data

    # getting ta first name
    def get_first_name(self, request):
        first_name = User.objects.get(id=request.user_id).first_name
        return first_name

    # getting ta last name
    def get_last_name(self, request):
        get_last_name = User.objects.get(id=request.user_id).last_name
        return get_last_name

    # getting ta email
    def get_email(self, request):
        get_email = User.objects.get(id=request.user_id).email
        return get_email

    # getting ta last name
    def get_department_ministry_data(self, request):
        try:
            department_ministry_data = CatRefDepartmentsSerializer(
                CatRefDepartmentsVW.objects.get(
                    dept_id=request.department_ministry_code
                ),
                many=False,
            ).data
        except:
            # default option (PSC data)
            department_ministry_data = {
                "dept_id": PscDepartment.DEPT_ID,
                "eabrv": PscDepartment.EABRV,
                "fabrv": PscDepartment.FABRV,
                "edesc": PscDepartment.EDESC,
                "fdesc": PscDepartment.FDESC,
            }
        return department_ministry_data

    class Meta:
        model = TestPermissions
        fields = "__all__"


class GetOrderlessTestAdministratorSerializer(serializers.ModelSerializer):
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    dept_id = serializers.SerializerMethodField()
    abrv_en = serializers.SerializerMethodField()
    abrv_fr = serializers.SerializerMethodField()
    desc_en = serializers.SerializerMethodField()
    desc_fr = serializers.SerializerMethodField()

    # getting ta first name
    def get_first_name(self, request):
        first_name = User.objects.get(id=request.user_id).first_name
        return first_name

    # getting ta last name
    def get_last_name(self, request):
        get_last_name = User.objects.get(id=request.user_id).last_name
        return get_last_name

    # getting ta email
    def get_email(self, request):
        email = User.objects.get(id=request.user_id).email
        return email

    # getting department ID
    def get_dept_id(self, request):
        return request.department_id

    # getting department abrv (en)
    def get_abrv_en(self, request):
        get_abrv_en = CatRefDepartmentsVW.objects.get(
            dept_id=request.department_id
        ).eabrv
        return get_abrv_en

    # getting department abrv (fr)
    def get_abrv_fr(self, request):
        get_abrv_fr = CatRefDepartmentsVW.objects.get(
            dept_id=request.department_id
        ).fabrv
        return get_abrv_fr

    # getting department desc (en)
    def get_desc_en(self, request):
        get_desc_en = CatRefDepartmentsVW.objects.get(
            dept_id=request.department_id
        ).edesc
        return get_desc_en

    # getting department desc (fr)
    def get_desc_fr(self, request):
        get_desc_fr = CatRefDepartmentsVW.objects.get(
            dept_id=request.department_id
        ).fdesc
        return get_desc_fr

    class Meta:
        model = TaExtendedProfile
        fields = "__all__"


class GetOrderlessTestAccessesSerializer(serializers.ModelSerializer):
    parent_code = serializers.SerializerMethodField()
    test_code = serializers.SerializerMethodField()
    test_desc_en = serializers.SerializerMethodField()
    test_desc_fr = serializers.SerializerMethodField()
    has_test_access = serializers.SerializerMethodField()

    # getting parent code
    def get_parent_code(self, request):
        parent_code = request.split("/")[0]
        return parent_code

    # getting test code
    def get_test_code(self, request):
        test_code = request.split("/")[1]
        return test_code

    # getting test description (en)
    def get_test_desc_en(self, request):
        parent_code = request.split("/")[0]
        test_code = request.split("/")[1]
        test_desc_en = (
            TestDefinition.objects.filter(parent_code=parent_code, test_code=test_code)
            .last()
            .en_name
        )
        return test_desc_en

    # getting test description (fr)
    def get_test_desc_fr(self, request):
        parent_code = request.split("/")[0]
        test_code = request.split("/")[1]
        test_desc_fr = (
            TestDefinition.objects.filter(parent_code=parent_code, test_code=test_code)
            .last()
            .fr_name
        )
        return test_desc_fr

    # checking if provided TA has the test access
    def get_has_test_access(self, request):
        parent_code = request.split("/")[0]
        test_code = request.split("/")[1]
        ta_extended_profile_id = TaExtendedProfile.objects.get(
            user_id=self.context.get("ta_user_id", None)
        ).id
        # if TA has this specific test access
        if OrderlessTestPermissions.objects.filter(
            parent_code=parent_code,
            test_code=test_code,
            ta_extended_profile_id=ta_extended_profile_id,
        ):
            # return True
            return True
        return False

    class Meta:
        model = TestDefinition
        fields = [
            "parent_code",
            "test_code",
            "test_desc_en",
            "test_desc_fr",
            "has_test_access",
        ]


class GetOrderlessTestPermissionsSerializer(serializers.ModelSerializer):
    orderless_test_permission_id = serializers.SerializerMethodField()
    test_skill_type_id = serializers.SerializerMethodField()
    test_skill_sub_type_id = serializers.SerializerMethodField()

    # getting test description (fr)
    def get_orderless_test_permission_id(self, request):
        ta_extended_profile_id = TaExtendedProfile.objects.get(
            user_id=self.context.get("ta_user_id", None)
        )
        orderless_test_permission_id = OrderlessTestPermissions.objects.get(
            parent_code=request.parent_code,
            test_code=request.test_code,
            ta_extended_profile_id=ta_extended_profile_id,
        ).id
        return orderless_test_permission_id

    def get_test_skill_type_id(self, request):
        test_skill = TestSkill.objects.filter(test_definition=request)

        if test_skill:
            test_skill_type_id = test_skill.first().test_skill_type.id

            return test_skill_type_id

        return None

    def get_test_skill_sub_type_id(self, request):
        test_skill = TestSkill.objects.filter(test_definition=request)

        if test_skill:
            test_skill_sub_type = get_test_skill_sub_type_for_test_skill(
                test_skill.first()
            )

            if test_skill_sub_type:
                return test_skill_sub_type.id

        return None

    class Meta:
        model = TestDefinition
        fields = "__all__"
