from random import shuffle
from ast import literal_eval
from rest_framework import serializers
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.multiple_choice_question import MultipleChoiceQuestion
from cms.cms_models.question_section import QuestionSection
from cms.cms_models.email_question import EmailQuestion
from cms.cms_models.answer import Answer
from cms.cms_models.question_situation import QuestionSituation
from cms.cms_models.situation_example_rating import SituationExampleRating

from cms.serializers.answer_serializer import AnswerSerializer
from cms.serializers.multiple_choice_question_serializer import (
    MultipleChoiceQuestionSerializer,
)
from cms.serializers.question_section_serializer import QuestionSectionSerializer
from cms.serializers.email_question_serializer import (
    EmailQuestionSerializer,
    NoLanguageDataEmailQuestionSerializer,
)
from cms.serializers.situation_example_rating_serializer import (
    SituationExampleRatingSerializer,
)
from cms.serializers.question_situation_serializer import QuestionSituationSerializer
from cms.static.question_type import QuestionType
from backend.custom_models.assigned_answer_choices import AssignedAnswerChoices


class QuestionSerializer(serializers.ModelSerializer):
    sections = serializers.SerializerMethodField()
    details = serializers.SerializerMethodField()
    answers = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    situation = serializers.SerializerMethodField()
    example_ratings = serializers.SerializerMethodField()

    class Meta:
        model = NewQuestion
        fields = [
            "id",
            "sections",
            "question_type",
            "details",
            "answers",
            "email",
            "situation",
            "example_ratings",
            "order",
        ]

    def get_details(self, request):
        return_data = {}

        if request.question_type == QuestionType.MULTIPLE_CHOICE:
            # get the extended question info
            m_c_question = MultipleChoiceQuestion.objects.get(question=request.id)
            m_c_data = MultipleChoiceQuestionSerializer(m_c_question, many=False).data
            return_data["diffculty"] = m_c_data["question_difficulty_type"]
        return return_data

    def get_sections(self, request):
        sections = QuestionSection.objects.filter(question=request.id).order_by("order")
        return QuestionSectionSerializer(sections, many=True, context=self.context).data

    def get_situation(self, request):
        if not self.context.get("scorer", None):
            return {}

        situation = QuestionSituation.objects.filter(
            question=request.id, language=self.context["language"]
        ).first()

        if situation:
            return QuestionSituationSerializer(situation, context=self.context).data
        else:
            return {}

    def get_answers(self, request):
        answers = list(Answer.objects.filter(question=request.id).order_by("order"))
        # randomizing answer choices order (if shuffle_answer_choices is set to True)
        if request.shuffle_answer_choices:
            shuffle(answers)
        # if real test (assigned_test_id is defined)
        if self.context.get("assigned_test_id", None):
            # making sure that there is no assigned answer choices row already associated to this assigned_test_id + question_id
            if not AssignedAnswerChoices.objects.filter(
                assigned_test_id=self.context.get("assigned_test_id"),
                question_id=request.id,
            ):
                # initializing answer_choices_ids
                answer_choices_ids = []
                # creating needed rows in assignedanswerchoices table
                # looping in answers
                for answer in answers:
                    answer_choices_ids.append(answer.id)
                # creating new row in assignedanswerchoices table
                AssignedAnswerChoices.objects.create(
                    assigned_test_id=self.context.get("assigned_test_id"),
                    question_id=request.id,
                    item_id=None,
                    answer_choices=answer_choices_ids,
                )
            # answer choices already associated (need to get the same answer choices order as the one generated at first)
            else:
                # getting associated answer choice IDs (using filter to avoid any kind of errors)
                associated_answer_choice_ids = literal_eval(
                    AssignedAnswerChoices.objects.filter(
                        assigned_test_id=self.context.get("assigned_test_id"),
                        question_id=request.id,
                    )
                    .first()
                    .answer_choices
                )
                # reinitializing answers
                answers = []
                # looping in associated_answer_choice_ids
                for answer_choice_id in associated_answer_choice_ids:
                    # populating the new answers array
                    answers.append(Answer.objects.get(id=answer_choice_id))

        return AnswerSerializer(answers, many=True, context=self.context).data

    def get_email(self, request):
        email = EmailQuestion.objects.filter(question=request.id).first()
        return EmailQuestionSerializer(email, context=self.context).data

    def get_example_ratings(self, request):
        if not self.context.get("scorer", None):
            return []

        ratings = SituationExampleRating.objects.filter(question=request.id)

        if not ratings:
            return []

        return SituationExampleRatingSerializer(
            ratings, many=True, context=self.context
        ).data


class NoLanguageDataQuestionSerializer(serializers.ModelSerializer):
    email = serializers.SerializerMethodField()

    class Meta:
        model = NewQuestion
        fields = ["id", "question_type", "email"]

    def get_email(self, request):
        email = EmailQuestion.objects.get(question=request.id)
        return NoLanguageDataEmailQuestionSerializer(email).data
