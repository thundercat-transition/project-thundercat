from rest_framework import serializers
from cms.cms_models.address_book_contact_details import AddressBookContactDetails


class AddressBookContactDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AddressBookContactDetails
        fields = "__all__"
