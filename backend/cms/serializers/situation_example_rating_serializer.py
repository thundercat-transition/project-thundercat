from rest_framework import serializers
from cms.cms_models.situation_example_rating import SituationExampleRating
from cms.cms_models.situation_example_rating_details import (
    SituationExampleRatingDetails,
)
from cms.serializers.situation_example_rating_details_serializer import (
    SituationExampleRatingDetailsSerializer,
)


class SituationExampleRatingSerializer(serializers.ModelSerializer):
    details = serializers.SerializerMethodField()

    class Meta:
        model = SituationExampleRating
        fields = "__all__"

    def get_details(self, request):
        details = SituationExampleRatingDetails.objects.get(
            example_rating=request.id, language=self.context.get("language")
        )

        return SituationExampleRatingDetailsSerializer(details).data
