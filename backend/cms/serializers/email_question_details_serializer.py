from rest_framework import serializers
from cms.cms_models.email_question_details import EmailQuestionDetails


class EmailQuestionDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailQuestionDetails
        fields = "__all__"
