from rest_framework import serializers
from cms.cms_models.answer_details import AnswerDetails


class AnswerDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnswerDetails
        fields = "__all__"
