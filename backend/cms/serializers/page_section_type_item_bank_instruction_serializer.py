from rest_framework import serializers
from cms.cms_models.page_section_type_item_bank_instruction import (
    PageSectionTypeItemBankInstruction,
)
from cms.views.retrieve_item_bank_data import get_formatted_item_content
from backend.custom_models.language import Language
from db_views.db_view_models.item_latest_versions_data_vw import (
    ItemLatestVersionsDataVW,
)


class PageSectionTypeItemBankInstructionSerializer(serializers.ModelSerializer):
    content = serializers.SerializerMethodField()

    def get_content(self, request):
        # getting language data
        language_data = Language.objects.all()
        item_id = ItemLatestVersionsDataVW.objects.get(
            system_id=request.system_id
        ).item_id
        return get_formatted_item_content(item_id, language_data)

    class Meta:
        model = PageSectionTypeItemBankInstruction
        fields = "__all__"
