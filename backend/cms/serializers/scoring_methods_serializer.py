from rest_framework import serializers
from cms.cms_models.scoring_method_types import ScoringMethodTypes


class ScoringMethodTypesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScoringMethodTypes
        fields = "__all__"
