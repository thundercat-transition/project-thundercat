from rest_framework import serializers
from itertools import groupby
from cms.cms_models.page_section_type_survey_link import PageSectionTypeSurveyLink
from cms.cms_models.page_section_type_survey_link_text import (
    PageSectionTypeSurveyLinkText,
)
from backend.custom_models.language import Language


class PageSectionTypeSurveyLinkTextSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageSectionTypeSurveyLinkText
        fields = "__all__"


class PageSectionTypeSurveyLinkSerializer(serializers.ModelSerializer):
    content = serializers.SerializerMethodField()

    def get_content(self, request):
        # getting language data
        language_data = Language.objects.all()

        # getting related survey link text data
        survey_link_text_data = PageSectionTypeSurveyLinkText.objects.filter(
            page_section_type_survey_link_id=request.id
        ).order_by("language")

        # serializing data
        serialized_data = PageSectionTypeSurveyLinkTextSerializer(
            survey_link_text_data, many=True
        ).data

        # initializing final_data
        final_data = {}

        # grouping text by language
        for obj, inner_group in groupby(serialized_data, lambda x: x["language"]):
            for language in language_data:
                # matching language
                if obj == language.language_id:
                    # populating obj
                    final_data["{0}".format(language.ISO_Code_1)] = list(inner_group)

        return final_data

    class Meta:
        model = PageSectionTypeSurveyLink
        fields = "__all__"
