# Database Diagrams

</br>

## Images

---

</br>

### Full Project's Database Diagram

![database-diagram-full.png](database-diagrams/database-diagram-full.png)

</br>

### Project's Database Diagram Without Historical Tables

![database-diagram-without-historical.png](database-diagrams/database-diagram-without-historical.png)

</br>

### Project's Historical Tables Database Diagram

![database-diagram-only-historical.png](database-diagrams/database-diagram-only-historical.png)

</br>
</br>

## Generate Database Diagrams

---

</br>

### Documentation

Please read the [PyGraphViz's Documentation](docs/pygraphviz-documentation.md)

</br>

### Execute Runner

1. Open Git Bash
1. Navigate to the project's folder
1. Execute:
   ```Bash
   ./run-pygraphviz.sh
   ```

</br>

### Modify Runner

Modify the file: [run-pygraphviz.sh](run-pygraphviz.sh)
