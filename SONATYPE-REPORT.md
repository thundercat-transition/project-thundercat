# SONATYPE REPORTS

## Install Nexus Lifecycle / Sonatype

Note: refer to https://devsecops.psc-cfp.gc.ca/ASD/knowledgebase/-/wikis/Nexus-Lifecycle-Scan for any changes

1. Install [Nexus CLI](https://help.sonatype.com/iqserver/product-information/download-and-compatibility) on Windows:

   1. Under CLI select your platform
      ![CLI For Windows](docs/images/nexus-sonatype-cli-download-location.png)
   2. Extract the file you just downloaded (should only be one binary file) under C:/\_dev/nexus-iq-cli

1. Login to [Nexus](https://lifecycle.psc-cfp.gc.ca:8443/)
   ![Setup Sonatype 1&2](docs/images/setup-sonatype-1.png)

   1. Select Your Account
   2. Manage User Token
      ![Setup Sonatype 3](docs/images/setup-sonatype-2.png)
   3. Generate User Token
      ![Setup Sonatype 4&5](docs/images/setup-sonatype-3.png)
   4. Copy User Code
   5. Copy Passcode
   6. Close

1. Create a file "my-sonatype-parameters.sh" in the main directory
   1. NOTE this file is ignored by git as it will hold your personal credentials. Do not save it to git under any circumstances. Do not share it.
   2. Populate the file as follows, replacing the {} with the correct values from Nexus (See 4 and 5 above)
   ```shell
    #!/bin/bash
    sh run-sonatype-scan-all.sh '{user code}' '{passcode}' $1
   ```
   3. Save the file and DO NOT PUSH IT TO THE REPO. EVER. DON'T DO IT.

## Execute Sonatype Reports

### In PowerShell

1. Go to your project path:

   ```shell
   cd C:\_dev\Ideaprojects\project-thundercat
   ```

   1. To run a build, execute (choose GitBash as the executor):

      ```shell
      .\run-sonatype-build.sh
      ```

   1. To run a release, execute (choose GitBash as the executor):

      ```shell
      .\run-sonatype-release.sh
      ```

   These script will generate both backend and frontend sonatype reports

2. These reports will be created and saved on our [Sonatype Lifecycle Reports Page](https://lifecycle.psc-cfp.gc.ca:8443/assets/index.html#/reports/violations)

### In GitBash

1. Go to your project path:

   ```shell
   cd C:/_dev/Ideaprojects/project-thundercat
   ```

   1. To run a build, execute (choose GitBash as the executor):

      ```shell
      ./run-sonatype-build.sh
      ```

   2. To run a release, execute (choose GitBash as the executor):

      ```shell
      ./run-sonatype-release.sh
      ```

   These script will generate both backend and frontend sonatype reports

2. These reports will be created and saved on our [Sonatype Lifecycle Reports Page](https://lifecycle.psc-cfp.gc.ca:8443/assets/index.html#/reports/violations)
