# PERMISSIONS

## Available Roles/Permissions

- Superuser
- Operations Level 1 (ETTA)
- R&D Operations
- Test Builder (TB)
- Test Developer (TD)
- PPC R&D Level 1
- Test Scorer
- Test Administrator (TA)
- Test Center Coordinator (TCM)
- Assessment Accommodations (AAE)
- Consultation Services
- HR Coordinator

## Superuser

**Codename:** N/A<br>
**Description:** Highly priviledged account that has basically all existing permissions<br>
**Note:** This permission cannot be requested from the application. It needs to be created through Django commands

## Operations Level 1 (ETTA)

**Codename:** is_etta<br>
**Description:** Responsible for managing users’ accounts. May produce reports related to business operations.

## R&D Operations

**Codename:** is_rd_operations<br>
**Description:** Responsible for managing permissions and accesses related to test content and test development.

## Test Builder (TB)

**Codename:** is_tb<br>
**Description:** Responsible for creating and uploading test content.

## Test Developer (TD)

**Codename:** is_td<br>
**Description:** Responsible for managing test content, test assembly, data extractions, and statistical analyses.

## PPC R&D Level 1

**Codename:** is_ppc<br>
**Description:** Responsible for data extraction or perform statistical analysis and generate various reports. Create and upload test content.

## Test Scorer

**Codename:** is_scorer<br>
**Description:** Test scorers are responsible for scoring some of the tests taken in the Candidate Assessment Tool.

## Test Administrator (TA)

**Codename:** is_test_administrator<br>
**Description:** A public servant responsible for the administration of standardized PSC tests to candidates. Help candidates resolve technological issues during the test session, if needed. Produce reports relevant to the staffing process.

## Test Center Coordinator (TCM)

**Codename:** is_tcm<br>
**Description:** A public servant responsible for the management of a test centre.

## Assessment Accommodations (AAE)

**Codename:** is_aae<br>
**Description:** Responsible for managing accommodations for tests, including adjusting test parameters following the analysis of accommodation requests.

## Consultation Services

**Codename:** is_consultation_services<br>
**Description:** TO BE COMPLETED

## HR Coordinator

**Codename:** is_hr_coordinator<br>
**Description:** A Public Servant responsible for coordinating supervised tests.
