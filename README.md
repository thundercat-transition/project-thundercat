# Project ThunderCAT

The Candidate Assessment Tool (CAT) or Outil d’évaluation des candidats (OEC), is the Public Service Commission's latest platform for administering PSC test materials. A brief history of the application is available on Code for Canada's [historical project updates blog](https://code-for-canada.github.io/psc-updates/).

![Welcome Admin](/docs/images/welcome-admin.png)

CAT uses Docker to connect its React-Redux frontend, Django backend, Nginx proxy and Microsoft SQL Server database.

![Tech stack diagram](/docs/images/tech-stack.png)

## Quick start

1. Follow our [setup guide](./SETUP.md) to configure your workstation.
2. Clone the repository using [git](./doc/contributing-with-git.md).
3. `docker compose up --build` to build the application (the build is expected to fail).
4. `CTRL + C` + `docker compose down` to shut down the application.
5. If it's your first time running the environment, go in [backend/entrypoint.sh](backend/entrypoint.sh)
    - Uncomment the lines 16 to 25 and save
6. If you are starting the application for the first time, please follow the "Rebuild node_modules and start back the application" steps in the [NODE_MODULES_REBUILD.md](./NODE_MODULES_REBUILD.md) and skip the next step.
7. `docker compose up --build` to build the application.
8. Open your browser to [http://localhost:81/oec-cat/](http://localhost:81/oec-cat/).

To see a list of all build commands, run `docker` from the repository root or see our [notes-on-docker](./docs/notes-on-docker.md).

## Contributing

All changes to the application (no matter how small) require a pull request, with a filled out description (there's a template), passing CI runs, and at least one (preferably two) code review approval(s) from your team members. Sometimes a designer or PM review is also required.

## Style Guide

All changes should be checked against the [Airbnb style guide](https://github.com/airbnb/javascript/tree/master/react#basic-rules)

## What's in this repo?

Here's a quick overview of the major landmarks:

### [backend](./backend)

The [Django](https://www.djangoproject.com/) application that manages data and requests through [REST APIs](https://www.django-rest-framework.org/). Urls starting with `/api/` are sent to the backend. A tool called [Swagger UI](./docs/swagger.md) has been introduced to this project in order to see and manipulate the available APIs that the backend supports.

The backend is responsible for the direct interaction with Microsoft SQL Server, data model definition, migrations, and API logic for

- user management (candidates and admins) and sessions
- test definitions (ie versions of test type eMIB) and test security
- user test results

### [frontend](./frontend)

The [ReactJS](https://reactjs.org/) application that users will see. It uses the Airbnb Styleguide. It will get built into a static package that we serve through nginx in production.
We are using a combination of [React-Bootstrap](https://react-bootstrap.github.io/), [Aurora Design System](https://design.gccollab.ca/component), and custom PSC-specific styles for our components. It is localized into French and English and is in development to meet accessibility standards.

## A Note on Browser Translation:

Since CAT administers second-language tests, it is important to prevent users from translating test materials using Chrome or Edge.

Chrome language translation is disabled across the application by the following meta config in the Helmet of App.js:

- `<meta name="google" content="notranslate" />`

Blocking Edge language translation, however, necessitates adding the html attribute 'className="notranslate"' to translatable content rendered by frontend components. For example, we block translating the title of the Home page like this:

- `<title className="notranslate">{LOCALIZE.titles.home}</title>`

More information on Blocking Edge Language translation [here](https://learn.microsoft.com/en-us/azure/cognitive-services/translator/prevent-translation).

### Documentation

- [SETUP](./SETUP.md): Instructions to get everything up and running.
- [TESTING](./TESTING.md): How to be sure nothing broke.
- [NODE_MODULE_REBUILD](./NODE_MODULES_REBUILD.md): How to destroy your local node_modules and related frontend files/folders in order to rebuild a fresh one.
- [DEPLOYMENT](./DEPLOYMENT.md): How to prepare a release branch for a deployment.
- [LICENSE](./LICENSE.md): MIT
- [DOCKER_BASH](DOCKER_BASH.md): explaination of docker.sh and docker-trigger.sh
- [Docker Setup](./docs/notes-on-docker.md) - diagrams explaining the container setup
- [Generate a New Docker Image](./docs/Docker%20Images/scripts.md) - See how docker images are being used by the pipelines
- [GC Notify Integration](./docs/gc-notify-integration.md) - documentation about how to connect to and use GC Notify
- [GC Notify Templates](./docs/gc-notify-templates.md)
- [Available Roles/Permissions](./PERMISSIONS.md) - documentation about roles/permissions meaning
- There are many more topical guides in the [docs](./docs) folder.
- In addition, several sections of the repository have their own documentation:
  - [frontend/README](./frontend/README.md)
  - [backend/README](./backend/README.md)
  - [docs/Dev-Server](./docs/Dev-Server.md)

## Technical requirements

This product was built to be capable of running in the following environment, where candidates are tested.

- Internet Explorer 10 or later
- JavaScript enabled
- Secure Socket Layer (SSL) encryption enabled
- Screen resolution must be set to a minimum of 1024 x 768
- Full-screen mode enabled
- Copy + paste functionality enabled
- Accessibility functionality available to users
  - IE Internet Options > Colours enabled to users
  - IE Internet Options > Fonts enabled to users
  - IE Internet Options > Accessibility enabled to users

## CI Tools

Each pull request runs all tests via [GitLab CI/CD](https://gitlab.com/thundercat-transition/project-thundercat/-/pipelines). The application is then deployed to dev/test/prod environments via [Jenkins](https://devsecops.psc-cfp.gc.ca:8443/). Vulnerability scans of all packages are performed manually using Sonatype.

## UA Deploy

TODO flesh this out and/or add to wiki

Steps are documented here https://devops.psc-cfp.gc.ca/Middleware/middleware-knowledge-base/blob/master/Azure-Kubernetes-Service/AKS-UA-Manual-Deployment.md
