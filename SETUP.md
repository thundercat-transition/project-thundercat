# Setup Dev Environment

## Table of Content

1. [Before Starting](#Before-starting)
1. [List of Requirements](#List-of-requirements)
   1. [Git Bash](#git-bash)
   1. [Python](#python)
   1. [Pip](#pip)
   1. [Docker Desktop](#docker-desktop)
   1. [Visual Studio Code](#visual-studio-code)
   1. [Katalon](#katalon)
   1. [Browser Extensions](#browser-extensions)
      1. [Katalon Recorder](#katalon-recorder)
1. [Setting Up VSCode](#setting-up-vscode)
   1. [Connect VSCode to Gitlab](#Connect-VSCode-to-Gitlab)
   1. [VSCode Extensions to install](#VSCode-Extensions)
      1. [Install](#Install)
      1. [Configuring](#configuring)
         1. [SQL Server extension configuration](#SQL-Server-extension-configuration)
         1. [ESLint](#eslint)
         1. [Pylint](#pylint)
   1. [Needed Packages on Local Computer](#Needed-Packages-on-Local-Computer)
      1. [Prettier](#Prettier)
      1. [Snyk](#Snyk)
1. [Local automated validation](#Local-automated-validation)
   1. [Sonatype](#Sonatype)
   1. [Accessibility Check Tool](#Accessibility-Check-Tool)
1. [Troubleshooting](#Troubleshooting)

## Before Starting

1. Read our [Code of Conduct](CODE_OF_CONDUCT.md)

## List of Requirements

### Git Bash

- Open your Software Center application
- Install Git

### Node JS (if not available from Software Center)

- [Download Node JS](https://nodejs.org/en/download)
- Windows Installer (.msi)
- Install by clicking Next, Next, Next, Install, etc. (no particular steps)

### Yarn

- Open Windows PowerShell
- execute: `npm install --global yarn`

### Python

- [Download Python](https://www.python.org/downloads/)
  - Windows installer (64-bit)
  - Version 3.11.2
- Open the installation file and click “Install now”
- Select "Bypass character count limit option"
- Once installed, make sure to add/update those variables in the user & system environment variables:
  - On Windows search, search for "Edit the system environment variables"
    - The "System Properties" will open
    - Click on "Environment Variables" button
    - In "User variables for **Username**" and "System Variables", double-click on "Path"
    - If these lines were not added automatically, add:
      - C:\Users\\**Username**\AppData\Local\Programs\Python\Python311
      - C:\Users\\**Username**\AppData\Local\Programs\Python\Python311\Scripts
      - C:\Program Files\nodejs
      - C:\Users\fnormand\AppData\Local\Yarn\bin
    - Restart your computer, so all new lines will be updated on your machine

### Pip

- Open Windows PowerShell
- Make sure Python has been installed; execute: `python --help`
- Download the get-pip.py file: `curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py`
- Install pip: `python get-pip.py`
- Verify that it has been installed successfully: `pip help`
- Pip is used to:
  - install packages for Python

### Docker Desktop

- [Download Docker Desktop](https://www.docker.com/products/docker-desktop/)
  - Get and install the latest version of Docker (click on the "Windows" hyperlink on the main page)
- Ensure that Docker settings are defined as follow:

  ![docker-hyper-v-setting](docs/images/docker-settings.png)

  ![docker-hyper-v-setting](docs/images/docker-settings_2.png)

### Visual Studio Code

- Open your Software Center application
- Install Visual Studio Code

### Katalon

_Note: this is only for user interactions testing_

- [Create a Katalon account](https://www.katalon.com/create-account/)
- [Download Katalon](https://www.katalon.com/)
  - Place the zip file in the folder where you want to install it (C:/\_dev for example)
  - Unzip it
  - Run katalon.exe
- [Katalon Recorder](#katalon-recorder)
  - Accessing Katalon Recorder
    - Start a new project or open an existing one
    - Click on the Globe button with the record circle
    - The test will not start recording till you hit the record button
  - Recording tests with
    - Create a new Test Suite
    - Hit the record button
    - Begin recording. Note, EVERY mouse click will be recorded, even those unrelated to the browser
    - Hit Stop to save your test
  - Running a saved test
    - To run a saved test, or test suite, press play or play suite
  - Exporting test
    - Hit export
    - Select the scripting format for the export
      - Java
      - C#
      - Ruby
      - Python
      - XML
      - Katalon Studio file (.groovy).
- Other references:
  - See [Version Control Test Scripting Guide (Halifax)](https://gcdocs.gc.ca/psc-cfp/llisapi.dll?func=ll&objaction=overview&objid=9742266) on how to initialize Katalon scripts in a new repo

### Browser Extensions

#### Katalon Recorder

_Note: this is only used when recording tests for Katalon_

- Chrome add on
  - May not be allowed, based on permissions
  - [Visit Chrome Store](https://chrome.google.com/webstore/detail/katalon-recorder/ljdobmomdgdljniojadhoplhkpialdid) to add it to Chrome
- Internet Explorer Addon
  - Select the Internet Explorer icon in Katalon's web recorder
  - If the add on is not installed, you will be prompted to do so
  - This will require admin permission

## Setting Up VSCode

We use VS Code, and specific linting setup when developing the application. This ensures consistency in our code style.

### Connect VSCode to Gitlab

#### Easy Way

1. TODO

#### With SSH Key

1. [Generate a SSH Key Pair](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair)
   - You can put empty as the password so VSCode directly push/pull to/from Gitlab
2. On GitLab, go on: https://gitlab.com/-/profile/keys
   1. Click on "Add New Key"
   2. On your computer, go to "C:\Users\\**_username_**\.ssh"
   3. Open "id_ed25519.pub" with notepad++ and copy the all the text
   4. On Gitlab, paste the text from previous step in the "key" input
   5. Add a title, example: MyJobComputer
   6. Make sure the "Usage Type" is "Authentication & Signing"
   7. Put an expiration date of many years later, otherwise, you will have to re-generate a SSH key pair sooner.

### VSCode Extensions

#### Install

1. In VScode, Press Ctrl+Shift+X for the hotkey to select the tab on the left-hand side of the IDE
2. Install the following VSCode extensions:
   - Data Workspace, by Microsoft
   - Django, by Baptiste Darthenay
   - Django-intellisense, by shamanu4
   - Docker, by Microsoft
   - Docker Explorer, by Jun Han
   - ESLint, by Microsoft
   - Git Graph, by mhutchie
   - Git History Diff, by Hui Zhou
   - Jupyter, by Microsoft
   - Jupyter Keymap, by Microsoft
   - Jupyter Notebook Renderers, by Microsoft
   - Path Intellisense, by Christian Kohler
   - Prettier - Code formatter, by Prettier
   - Pylance, by Microsoft
   - Python, by Microsoft
   - React Redux ES6 Snippets, by Timothy McLane
   - SQL Database Projects, by Microsoft
   - SQL Server (mssql), by Microsoft
   - Black Formatter, by Microsoft
   - Pylint, by Microsoft

#### Configuring

We use VS Code, and specific linting setup when developing the application. This ensures consistency in our code style.

Linting and autoformatting settings are present in the repository, but the .vscode/settings.json file for your local project still needs to be configured properly.

If you have not done so already, open the project in VSCode by right-clicking on the folder in File Explorer and select "Open with Code".

##### SQL Server extension configuration

- Click on the "SQL Server" icon on the left-hand side of VSCode (looks like a cellphone)
- Click on "Add Connection" in the left-hand menu

Fill in the fields with the following information when prompted:

- `hostname`: localhost
- `Database name`: master
- `Authentication Type`: SQL Login
- `Username`: SA
- `Password`: someSecurePassword10!
- `Save Password`: Yes
- `Profile Name`: optional (name it whatever you want, or just press "Enter" to continue)

##### ESLint

- The file `.vscode/settings.json` should already exist & be in use as a workspace settings.json file. If not, make sure you currently have the same version of the `settings.json` as the `develop` branch.
- To ensure that ESLint works, open the Terminal window within VSCode by pressing `` Ctrl+Shift+` ``, then click on the "Output" tab and select "ESLint" from the dropdown menu. There should be no errors shown in the Output window if ESLint is working correctly when a .js, .jsx or .json file is open in another VSCode tab.
- ESLint is now installed and configured!

  - If ESLint is not working, do the following steps:
    1. Close Docker: Right Click on Docker icon + Quit Docker
    2. Navigate in your Windows explorer and delete the following folders under '.\\\\project-thundercat\\\\frontend'
       - '_node_modules_'
       - '_.yarn/cache/_'
       - '_.yarn/unplugged_'
    3. Start Docker
    4. Open a powershell window and do a '_docker compose up_'
    5. Wait a couple minutes to make sure that all packages have been downloaded
    6. Restart VS Code

- Click [here](https://eslint.org/) for more details about **ESLint** or [here](https://github.com/airbnb/javascript/tree/master/react#basic-rules) to learn more about the Airbnb React style.

##### Pylint:

1. To activate Linter, make sure the extensions Black Formatter and Pylint are installed on VSCode.
2. Open a python file, hit save and there should be a popup saying that _Linter pylint is not installed_. Hit _Install_.
3. Now, you can edit the desired settings from _.pylintrc_ file (to disable messages, see line 54 of the config file)
4. Click [here](http://pylint-messages.wikidot.com/all-messages) for more details on most of the Pylint errors/warnings

### Needed Packages on Local Computer

#### Prettier

1. Run this command on your machine (allows for automatic linting for frontend-related files): `npm install -g prettier`
2. If that does not work, try running `npx npm install -g prettier`.

#### Snyk

[Snyk](https://snyk.io/) is a security scanning tool that helps ensure the security of open source packages.

1. Run 'npm install -g snyk' in a terminal
1. Run 'npm install -g snyk-to-html' in a terminal
   1. This allows for clean output to html files when snyk is run locally
   1. Locally detected vulnerabilities can be documented in the repo and version controlled
1. See [snyk.md](docs/snyk.md) for an explanation of why snyk is installed locally rather than in the containers
1. Add the npm directory containing snyk to the path system environment variable (usually "C:\Users\<name>\AppData\Roaming\npm")
   - Note: You may need to restart your computer for this change to take effect
1. Create an account on snyk.io
1. Run 'snyk auth' in a terminal
1. Login when prompted (either in using the provided URL or in the opened browser)

## Local automated validation

Learn more about these tools in [REPORTING.md](./REPORTING.md)

### Sonatype

See "Install Nexus Lifecycle / Sonatype" in [SONATYPE-REPORT](SONATYPE-REPORT.md)

### Accessibility Check Tool

Accessibility requirements cannot be ensured entirely through automated checks, but they do help ensure we are meeting basic standards. [Pa11y](https://github.com/pa11y/pa11y) is a tool that helps us do this.

- Run 'run 'npm install -g pa11y' in a terminal
- Run also 'npm install -g pa11y pa11y-reporter-html' in a terminal
  - This allows for clean output to html file when this tool is run locally
  - It detects errors, warnings and notices related to accessibility of the application
- Make sure that the npm directory containing pa11y is part of the path system environment variable (usually "C:\Users\<name>\AppData\Roaming\npm")
- Click [here](https://github.com/pa11y/pa11y) to have more details about pa11y.

## Run

Once everything is installed and setup, it's now time to run the application locally. To do so:

- Open the [backend entrypoint](./backend/entrypoint.sh) file
- Temporarily uncomment line 16 to 25, so migrations will be executed in the right order (keep that uncommented just for the couple first executions)
- Open Windows PowerShell and access the project folder (`...\_dev\IdeaProjects\thundercat\project-thundercat`)
- Execute `docker compose up --build` (note that the first couple of runs might take a couple of minutes due to the amount of migrations and installations that your computer needs to do)
- If your frontend is failing and not starting properly, please follow those steps:
  - Open another Windows PowerShell
  - Go to the frontend directory of your project (`cd ...\_dev\IdeaProjects\thundercat\project-thundercat\frontend`)
  - Execute `yarn`
  - Execute `npm install --force`

## Troubleshooting

If you are having any issues with getting the project to run, please refer to the [troubleshooting.md document](docs/troubleshooting.md) to help resolve your issues.
