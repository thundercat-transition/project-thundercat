# Reference Doucmentation for docker.sh and docker-trigger.sh

## Summary

Two bash scripts, docker.sh and docker-trigger.sh, were created to streamline/minimize the number of repeatative commands devs need to use on a day-to-day basis.

### In PowerShell

1.  Go to your project path:

```shell
cd C:\_dev\Ideaprojects\project-thundercat
```

1.  To run a build, execute (choose GitBash as the executor):

    ```shell
    .\docker.sh {argument}
    ```

    or

    ```shell
    .\docker-trigger.sh {arguments}
    ```

### In GitBash

1.  Go to your project path:

```shell
cd C:/_dev/Ideaprojects/project-thundercat
```

1.  To run a build, execute (choose GitBash as the executor):

    ```shell
    ./docker.sh {argument}
    ```

    or

    ```shell
    ./docker-trigger.sh {arguments}
    ```

---

## docker.sh

Docker.sh attempts to summarize regularly used series of commands with miminal typing.

Feel free to update with reguarly used sequences of commands as needed.

It accepts a single argument from the following list:

- restart: docker compose down; clear; docker-compose up
- rebuild: docker compose down; clear; docker-compose up --build
- purge: docker compose down; docker system prune -f; docker image prune -a -f; clear; docker-compose up --build
- nuke: docker compose down; docker system prune -f; docker image prune -a -f; docker volume prune -f; clear; docker-compose up --build
- orbital-strike: nuke that first deletes C:/MSSQL/DockerFiles directory
- help: show help text
- h: show help text

Example in GitBash:

    ```shell
    ./docker.sh rebuild
    ```

is equivalent to:

    ```shell
    docker compose down
    clear
    docker-compose up --build
    ```

## docker-trigger.sh

Docker-trigger.sh loops on all inputs, executing individual commands back to back allowing for vitrually endless flexibility.

Feel free to add single-line commands here as needed.

It accepts and loops over successive agruments from the following list:

- up: docker compose up
- down: docker compose down
- build: docker compose up --build
- c: clear the console
- s: docker system prune -f
- i: docker image prune -a -f
- v: docker volume prune -f
- vd: rm -rf C:/MSSQL/DockerFiles
- help: show help text
- h: show help text

Example in GitBash:

    ```shell
    ./docker.sh rebuild
    ```

is equivalent to:

    ```shell
    docker-trigger.sh down c build
    ```

is equivalent to:

    ```shell
    docker compose down
    clear
    docker-compose up --build
    ```
